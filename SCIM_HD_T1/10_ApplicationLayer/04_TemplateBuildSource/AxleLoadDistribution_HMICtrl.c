/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  AxleLoadDistribution_HMICtrl.c
 *        Config:  C:/GIT/scim_external_git/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  AxleLoadDistribution_HMICtrl
 *  Generated at:  Sat Oct 19 00:22:43 2019
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <AxleLoadDistribution_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_AxleLoadDistribution_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T: Integer in interval [0...255]
 * SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * AltLoadDistribution_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   AltLoadDistribution_rqst_Idle (0U)
 *   AltLoadDistribution_rqst_ALDOn (1U)
 *   AltLoadDistribution_rqst_ALDOff (2U)
 *   AltLoadDistribution_rqst_Reserved_1 (3U)
 *   AltLoadDistribution_rqst_Reserved_2 (4U)
 *   AltLoadDistribution_rqst_Reserved_3 (5U)
 *   AltLoadDistribution_rqst_Error (6U)
 *   AltLoadDistribution_rqst_NotAvailable (7U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DualDeviceIndication_T: Enumeration of integer in interval [0...15] with enumerators
 *   DualDeviceIndication_UpperOffLowerOff (0U)
 *   DualDeviceIndication_UpperOnLowerOff (1U)
 *   DualDeviceIndication_UpperBlinkLowerOff (2U)
 *   DualDeviceIndication_UpperDontCareLowerOff (3U)
 *   DualDeviceIndication_UpperOffLowerOn (4U)
 *   DualDeviceIndication_UpperOnLowerOn (5U)
 *   DualDeviceIndication_UpperBlinkLowerOn (6U)
 *   DualDeviceIndication_UpperDontCareLowerOn (7U)
 *   DualDeviceIndication_UpperOffLowerBlink (8U)
 *   DualDeviceIndication_UpperOnLowerBlink (9U)
 *   DualDeviceIndication_UpperBlinkLowerBlink (10U)
 *   DualDeviceIndication_UpperDontCareLowerBlink (11U)
 *   DualDeviceIndication_UpperOffLowerDontCare (12U)
 *   DualDeviceIndication_UpperOnLowerDontCare (13U)
 *   DualDeviceIndication_UpperBlinkLowerDontCare (14U)
 *   DualDeviceIndication_UpperDontCareLowerDontCare (15U)
 * ECSStandByRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   ECSStandByRequest_NoRequest (0U)
 *   ECSStandByRequest_Initiate (1U)
 *   ECSStandByRequest_StandbyRequestedRCECS (2U)
 *   ECSStandByRequest_StandbyRequestedWRC (3U)
 *   ECSStandByRequest_Reserved (4U)
 *   ECSStandByRequest_Reserved_01 (5U)
 *   ECSStandByRequest_Error (6U)
 *   ECSStandByRequest_NotAvailable (7U)
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   InactiveActive_Inactive (0U)
 *   InactiveActive_Active (1U)
 *   InactiveActive_Error (2U)
 *   InactiveActive_NotAvailable (3U)
 * LiftAxleLiftPositionRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   LiftAxleLiftPositionRequest_Idle (0U)
 *   LiftAxleLiftPositionRequest_Down (1U)
 *   LiftAxleLiftPositionRequest_Up (2U)
 *   LiftAxleLiftPositionRequest_Reserved (3U)
 *   LiftAxleLiftPositionRequest_Reserved_01 (4U)
 *   LiftAxleLiftPositionRequest_Reserved_02 (5U)
 *   LiftAxleLiftPositionRequest_Error (6U)
 *   LiftAxleLiftPositionRequest_NotAvailable (7U)
 * LiftAxlePositionStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   LiftAxlePositionStatus_Lowered (0U)
 *   LiftAxlePositionStatus_Lifted (1U)
 *   LiftAxlePositionStatus_Lowering (2U)
 *   LiftAxlePositionStatus_Lifting (3U)
 *   LiftAxlePositionStatus_Reserved (4U)
 *   LiftAxlePositionStatus_Reserved_01 (5U)
 *   LiftAxlePositionStatus_Error (6U)
 *   LiftAxlePositionStatus_NotAvailable (7U)
 * LiftAxleUpRequestACK_T: Enumeration of integer in interval [0...7] with enumerators
 *   LiftAxleUpRequestACK_NoAction (0U)
 *   LiftAxleUpRequestACK_ChangeAcknowledged (1U)
 *   LiftAxleUpRequestACK_LiftDeniedFrontOverload (2U)
 *   LiftAxleUpRequestACK_LiftDeniedRearOverload (3U)
 *   LiftAxleUpRequestACK_LiftDeniedPBrakeActive (4U)
 *   LiftAxleUpRequestACK_SystemDeniedVersatile (5U)
 *   LiftAxleUpRequestACK_Error (6U)
 *   LiftAxleUpRequestACK_NotAvailable (7U)
 * LoadDistributionALDChangeACK_T: Enumeration of integer in interval [0...7] with enumerators
 *   LoadDistributionALDChangeACK_NoAction (0U)
 *   LoadDistributionALDChangeACK_ChangeAcknowledged (1U)
 *   LoadDistributionALDChangeACK_LoadShiftDeniedOverspeed (2U)
 *   LoadDistributionALDChangeACK_Spare (3U)
 *   LoadDistributionALDChangeACK_Spare_01 (4U)
 *   LoadDistributionALDChangeACK_SystemDeniedVersatile (5U)
 *   LoadDistributionALDChangeACK_Error (6U)
 *   LoadDistributionALDChangeACK_NotAvailable (7U)
 * LoadDistributionChangeACK_T: Enumeration of integer in interval [0...7] with enumerators
 *   LoadDistributionChangeACK_NoAction (0U)
 *   LoadDistributionChangeACK_ChangeAcknowledged (1U)
 *   LoadDistributionChangeACK_LoadShiftDeniedFALIMOverload (2U)
 *   LoadDistributionChangeACK_Reserved_1 (3U)
 *   LoadDistributionChangeACK_Reserved_2 (4U)
 *   LoadDistributionChangeACK_SystemDeniedVersatile (5U)
 *   LoadDistributionChangeACK_Error (6U)
 *   LoadDistributionChangeACK_NotAvailable (7U)
 * LoadDistributionChangeRequest_T: Enumeration of integer in interval [0...3] with enumerators
 *   LoadDistributionChangeRequest_Idle (0U)
 *   LoadDistributionChangeRequest_ChangeLoadDistribution (1U)
 *   LoadDistributionChangeRequest_Error (2U)
 *   LoadDistributionChangeRequest_NotAvailable (3U)
 * LoadDistributionFuncSelected_T: Enumeration of integer in interval [0...15] with enumerators
 *   LoadDistributionFuncSelected_NormalLoad (0U)
 *   LoadDistributionFuncSelected_OptimisedTraction (1U)
 *   LoadDistributionFuncSelected_MaximumTractionTractionHelp (2U)
 *   LoadDistributionFuncSelected_AlternativeLoad (3U)
 *   LoadDistributionFuncSelected_NoLoadDistribution (4U)
 *   LoadDistributionFuncSelected_VariableAxleLoad (5U)
 *   LoadDistributionFuncSelected_MaximumTractionStartingHelp (6U)
 *   LoadDistributionFuncSelected_SpareValue_02 (7U)
 *   LoadDistributionFuncSelected_SpareValue_03 (8U)
 *   LoadDistributionFuncSelected_SpareValue_04 (9U)
 *   LoadDistributionFuncSelected_SpareValue_05 (10U)
 *   LoadDistributionFuncSelected_SpareValue_06 (11U)
 *   LoadDistributionFuncSelected_SpareValue_07 (12U)
 *   LoadDistributionFuncSelected_SpareValue_08 (13U)
 *   LoadDistributionFuncSelected_ErrorIndicator (14U)
 *   LoadDistributionFuncSelected_NotAvaiable (15U)
 * LoadDistributionRequestedACK_T: Enumeration of integer in interval [0...7] with enumerators
 *   LoadDistributionRequestedACK_NoAction (0U)
 *   LoadDistributionRequestedACK_ChangeAcknowledged (1U)
 *   LoadDistributionRequestedACK_LoadShiftDeniedFALIMOverload (2U)
 *   LoadDistributionRequestedACK_Reserved1 (3U)
 *   LoadDistributionRequestedACK_Reserved2 (4U)
 *   LoadDistributionRequestedACK_SystemDeniedVersatile (5U)
 *   LoadDistributionRequestedACK_Error (6U)
 *   LoadDistributionRequestedACK_NotAvailable (7U)
 * LoadDistributionRequested_T: Enumeration of integer in interval [0...15] with enumerators
 *   LoadDistributionRequested_NoAction (0U)
 *   LoadDistributionRequested_DefaultLoadDistribution (1U)
 *   LoadDistributionRequested_NormalLoadDistribution (2U)
 *   LoadDistributionRequested_OptimizedTraction (3U)
 *   LoadDistributionRequested_MaximumTraction1 (4U)
 *   LoadDistributionRequested_MaximumTraction2 (5U)
 *   LoadDistributionRequested_AlternativeLoadDistribution (6U)
 *   LoadDistributionRequested_VariableLoadDistribution (7U)
 *   LoadDistributionRequested_DelpressLoadDistribution (8U)
 *   LoadDistributionRequested_Reserved1 (9U)
 *   LoadDistributionRequested_Reserved2 (10U)
 *   LoadDistributionRequested_Reserved3 (11U)
 *   LoadDistributionRequested_Reserved4 (12U)
 *   LoadDistributionRequested_Reserved5 (13U)
 *   LoadDistributionRequested_Error (14U)
 *   LoadDistributionRequested_NotAvailable (15U)
 * LoadDistributionSelected_T: Enumeration of integer in interval [0...15] with enumerators
 *   LoadDistributionSelected_NormalLoad (0U)
 *   LoadDistributionSelected_OptimisedTraction (1U)
 *   LoadDistributionSelected_StartingHelp (2U)
 *   LoadDistributionSelected_TractionHelp (3U)
 *   LoadDistributionSelected_AlternativeLoad (4U)
 *   LoadDistributionSelected_StartingHelpInterDiff (5U)
 *   LoadDistributionSelected_TractionHelpInterDiff (6U)
 *   LoadDistributionSelected_NoLoadDistribution (7U)
 *   LoadDistributionSelected_VariableAxleLoad (8U)
 *   LoadDistributionSelected_StartingHelp2 (9U)
 *   LoadDistributionSelected_TractionHelp2 (10U)
 *   LoadDistributionSelected_Reserved_03 (11U)
 *   LoadDistributionSelected_Reserved_04 (12U)
 *   LoadDistributionSelected_Reserved_05 (13U)
 *   LoadDistributionSelected_Error (14U)
 *   LoadDistributionSelected_NotAvailable (15U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1BOS_AxleLoad_AccessoryBoggieALD_v(void)
 *   boolean Rte_Prm_P1BOV_AxleLoad_RatioALD_v(void)
 *   boolean Rte_Prm_P1BOW_AxleLoad_ArideLiftAxle_v(void)
 *   boolean Rte_Prm_P1BOX_AxleLoad_TridemFirstAxleLift_v(void)
 *   boolean Rte_Prm_P1BOY_AxleLoad_TridemSecondAxleLift_v(void)
 *   boolean Rte_Prm_P1CZ0_AxleLoad_MaxTractionTag_v(void)
 *   boolean Rte_Prm_P1CZ1_AxleLoad_RatioTagOrLoadDistrib_v(void)
 *   boolean Rte_Prm_P1CZ2_AxleLoad_RatioPusherRocker_v(void)
 *   boolean Rte_Prm_P1CZ3_AxleLoad_RatioTagRocker_v(void)
 *   boolean Rte_Prm_P1CZW_AxleLoad_OneLiftPusher_v(void)
 *   boolean Rte_Prm_P1CZX_AxleLoad_OneLiftAxleMaxTraction_v(void)
 *   boolean Rte_Prm_P1CZY_AxleLoad_OneLiftTag_v(void)
 *   boolean Rte_Prm_P1CZZ_AxleLoad_MaxTractionPusher_v(void)
 *   boolean Rte_Prm_P1J6B_AxleLoad_AccessoryTridemALD_v(void)
 *   boolean Rte_Prm_P1J6C_AxleLoad_BoggieDualRatio_v(void)
 *   boolean Rte_Prm_P1J6D_AxleLoad_TridemDualRatio_v(void)
 *   boolean Rte_Prm_P1KN2_AxleLoad_CRideLiftAxle_v(void)
 *   boolean Rte_Prm_P1KN4_AxleLoad_CRideLEDlowerEnd_v(void)
 *   SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T Rte_Prm_P1KN5_AxleLoad_CRideLEDIndicationType_v(void)
 *   boolean Rte_Prm_P1M5B_AxleLoad_RatioRoadGripPusher_v(void)
 *
 *********************************************************************************************************************/


#define AxleLoadDistribution_HMICtrl_START_SEC_CODE
#include "AxleLoadDistribution_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AxleLoadDistribution_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ALDSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_ECSStandByRequest_ECSStandByRequest(ECSStandByRequest_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1PositionStatus_LiftAxle1PositionStatus(LiftAxlePositionStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1Switch2_Status_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1UpRequestACK_LiftAxle1UpRequestACK(LiftAxleUpRequestACK_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle2SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle2UpRequestACK_LiftAxle2UpRequestACK(LiftAxleUpRequestACK_T *data)
 *   Std_ReturnType Rte_Read_LoadDistributionALDChangeACK_LoadDistributionALDChangeACK(LoadDistributionALDChangeACK_T *data)
 *   Std_ReturnType Rte_Read_LoadDistributionChangeACK_LoadDistributionChangeACK(LoadDistributionChangeACK_T *data)
 *   Std_ReturnType Rte_Read_LoadDistributionFuncSelected_LoadDistributionFuncSelected(LoadDistributionFuncSelected_T *data)
 *   Std_ReturnType Rte_Read_LoadDistributionRequestedACK_LoadDistributionRequestedACK(LoadDistributionRequestedACK_T *data)
 *   Std_ReturnType Rte_Read_LoadDistributionSelected_LoadDistributionSelected(LoadDistributionSelected_T *data)
 *   Std_ReturnType Rte_Read_Ratio1SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_Ratio2SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_Ratio3SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_Ratio4SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_Ratio5SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_Ratio6SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_RatioALDSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_TridemALDSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ALD_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_AltLoadDistribution_rqst_AltLoadDistribution_rqst(AltLoadDistribution_rqst_T data)
 *   Std_ReturnType Rte_Write_BogieSwitch_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1AutoLiftRequest_LiftAxle1AutoLiftRequest(InactiveActive_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1DirectControl_LiftAxle1DirectControl(LiftAxleLiftPositionRequest_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1LiftPositionRequest_LiftAxle1LiftPositionRequest(LiftAxleLiftPositionRequest_T data)
 *   Std_ReturnType Rte_Write_LiftAxle2AutoLiftRequest_LiftAxle2AutoLiftRequest(InactiveActive_T data)
 *   Std_ReturnType Rte_Write_LiftAxle2LiftPositionRequest_LiftAxle2LiftPositionRequest(LiftAxleLiftPositionRequest_T data)
 *   Std_ReturnType Rte_Write_LoadDistributionChangeRequest_LoadDistributionChangeRequest(LoadDistributionChangeRequest_T data)
 *   Std_ReturnType Rte_Write_LoadDistributionRequested_LoadDistributionRequested(LoadDistributionRequested_T data)
 *   Std_ReturnType Rte_Write_MaxTract_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Ratio1_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Ratio2_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Ratio4_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Ratio5_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Ratio6_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_RatioALD_DualDeviceIndication_DualDeviceIndication(DualDeviceIndication_T data)
 *   Std_ReturnType Rte_Write_TridemALD_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AxleLoadDistribution_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, AxleLoadDistribution_HMICtrl_CODE) AxleLoadDistribution_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AxleLoadDistribution_HMICtrl_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AxleLoadDistribution_HMICtrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AxleLoadDistribution_HMICtrl_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, AxleLoadDistribution_HMICtrl_CODE) AxleLoadDistribution_HMICtrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AxleLoadDistribution_HMICtrl_Init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define AxleLoadDistribution_HMICtrl_STOP_SEC_CODE
#include "AxleLoadDistribution_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
