/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  CollisionMitigation_HMICtrl.c
 *        Config:  C:/GIT/scim_external_git/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  CollisionMitigation_HMICtrl
 *  Generated at:  Sat Oct 19 00:22:43 2019
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <CollisionMitigation_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_CM_Configuration_P1LGD_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_CM_Configuration_P1LGD_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FCW_ConfirmTimeout_P1LGF_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FCW_ConfirmTimeout_P1LGF_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FCW_LedLogic_P1LG1_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FCW_LedLogic_P1LG1_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FCW_SwPushThreshold_P1LGE_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FCW_SwPushThreshold_P1LGE_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FCW_SwStuckTimeout_P1LGG_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FCW_SwStuckTimeout_P1LGG_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_HeadwaySupport_P1BEX_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_HeadwaySupport_P1BEX_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_CollisionMitigation_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_CM_Configuration_P1LGD_T: Integer in interval [0...255]
 * SEWS_CM_Configuration_P1LGD_T: Integer in interval [0...255]
 * SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T: Integer in interval [0...255]
 * SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T: Integer in interval [0...255]
 * SEWS_FCW_ConfirmTimeout_P1LGF_T: Integer in interval [0...255]
 * SEWS_FCW_ConfirmTimeout_P1LGF_T: Integer in interval [0...255]
 * SEWS_FCW_LedLogic_P1LG1_T: Integer in interval [0...255]
 * SEWS_FCW_LedLogic_P1LG1_T: Integer in interval [0...255]
 * SEWS_FCW_SwPushThreshold_P1LGE_T: Integer in interval [0...255]
 * SEWS_FCW_SwPushThreshold_P1LGE_T: Integer in interval [0...255]
 * SEWS_FCW_SwStuckTimeout_P1LGG_T: Integer in interval [0...255]
 * SEWS_FCW_SwStuckTimeout_P1LGG_T: Integer in interval [0...255]
 * SEWS_HeadwaySupport_P1BEX_T: Integer in interval [0...255]
 * SEWS_HeadwaySupport_P1BEX_T: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * CM_Status_T: Enumeration of integer in interval [0...7] with enumerators
 *   CM_Status_CM_Disabled (0U)
 *   CM_Status_CM_Enabled (1U)
 *   CM_Status_CM_PreMitigationBraking (2U)
 *   CM_Status_CM_MitigationBraking (3U)
 *   CM_Status_CM_MitigationBrakingFinished (4U)
 *   CM_Status_CM_DisabledBySystem (5U)
 *   CM_Status_ErrorIndicator (6U)
 *   CM_Status_NotAvailable (7U)
 * CollSituationHMICtrlRequestVM_T: Enumeration of integer in interval [0...7] with enumerators
 *   CollSituationHMICtrlRequestVM_NoIndication (0U)
 *   CollSituationHMICtrlRequestVM_CM_FCWdisabledOnAftermarket (1U)
 *   CollSituationHMICtrlRequestVM_FCWdisabledOnAftermarket (2U)
 *   CollSituationHMICtrlRequestVM_Spare1 (3U)
 *   CollSituationHMICtrlRequestVM_Spare2 (4U)
 *   CollSituationHMICtrlRequestVM_Spare3 (5U)
 *   CollSituationHMICtrlRequestVM_Spare4 (6U)
 *   CollSituationHMICtrlRequestVM_NotAvailable (7U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DisableEnable_T: Enumeration of integer in interval [0...3] with enumerators
 *   DisableEnable_Disable (0U)
 *   DisableEnable_Enable (1U)
 *   DisableEnable_Error (2U)
 *   DisableEnable_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * SetCMOperation_T: Enumeration of integer in interval [0...7] with enumerators
 *   SetCMOperation_NoRequest (0U)
 *   SetCMOperation_SetEmergencyBrakeON (1U)
 *   SetCMOperation_SetEmergencyBrakeOFF (2U)
 *   SetCMOperation_Spare1 (3U)
 *   SetCMOperation_Spare2 (4U)
 *   SetCMOperation_Spare3 (5U)
 *   SetCMOperation_Error (6U)
 *   SetCMOperation_NotAvailable (7U)
 * SetFCWOperation_T: Enumeration of integer in interval [0...7] with enumerators
 *   SetFCWOperation_NoRequest (0U)
 *   SetFCWOperation_SetCollisionWarningON (1U)
 *   SetFCWOperation_SetCollisionWarningOFF (2U)
 *   SetFCWOperation_Spare1 (3U)
 *   SetFCWOperation_Spare2 (4U)
 *   SetFCWOperation_Spare3 (5U)
 *   SetFCWOperation_Error (6U)
 *   SetFCWOperation_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_HeadwaySupport_P1BEX_T Rte_Prm_P1BEX_HeadwaySupport_v(void)
 *   SEWS_FCW_LedLogic_P1LG1_T Rte_Prm_P1LG1_FCW_LedLogic_v(void)
 *   SEWS_CM_Configuration_P1LGD_T Rte_Prm_P1LGD_CM_Configuration_v(void)
 *   SEWS_FCW_SwPushThreshold_P1LGE_T Rte_Prm_P1LGE_FCW_SwPushThreshold_v(void)
 *   SEWS_FCW_ConfirmTimeout_P1LGF_T Rte_Prm_P1LGF_FCW_ConfirmTimeout_v(void)
 *   SEWS_FCW_SwStuckTimeout_P1LGG_T Rte_Prm_P1LGG_FCW_SwStuckTimeout_v(void)
 *   SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T Rte_Prm_P1MOT_CollSituationHMICtrlRequestVM_Time_v(void)
 *   boolean Rte_Prm_P1NT1_CM_DeviceType_v(void)
 *
 *********************************************************************************************************************/


#define CollisionMitigation_HMICtrl_START_SEC_CODE
#include "CollisionMitigation_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CollisionMitigation_HMICtrl_20ms_Runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AEBS_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_CM_Status_CM_Status(CM_Status_T *data)
 *   Std_ReturnType Rte_Read_FCWPushButton_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_FCW_Status_FCW_Status(DisableEnable_T *data)
 *   Std_ReturnType Rte_Read_FCW_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_CollSituationHMICtrlRequestVM_CollSituationHMICtrlRequestVM(CollSituationHMICtrlRequestVM_T data)
 *   Std_ReturnType Rte_Write_FCW_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_FCW_Enable_FCW_Enable(DisableEnable_T data)
 *   Std_ReturnType Rte_Write_SetCMOperation_SetCMOperation(SetCMOperation_T data)
 *   Std_ReturnType Rte_Write_SetFCWOperation_SetFCWOperation(SetFCWOperation_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CollisionMitigation_HMICtrl_20ms_Runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CollisionMitigation_HMICtrl_CODE) CollisionMitigation_HMICtrl_20ms_Runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CollisionMitigation_HMICtrl_20ms_Runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CollisionMitigation_HMICtrl_STOP_SEC_CODE
#include "CollisionMitigation_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
