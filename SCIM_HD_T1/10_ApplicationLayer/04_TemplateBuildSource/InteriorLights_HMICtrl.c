/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  InteriorLights_HMICtrl.c
 *        Config:  C:/GIT/scim_external_git/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  InteriorLights_HMICtrl
 *  Generated at:  Sat Oct 19 00:22:46 2019
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <InteriorLights_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * IntLghtLvlIndScaled_cmd_T
 *   14 Error; 15 Not Available
 *
 * Issm_IssStateType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Percent8bitNoOffset_T
 *   254 Error ; 255 Not available
 *
 * SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_IL_CtrlDeviceTypeFront_P1DKH_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_IL_CtrlDeviceTypeFront_P1DKH_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_IL_LockingCmdDelayOff_P1K7E_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_IL_LockingCmdDelayOff_P1K7E_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1DKF_Long_press_threshold_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1DKF_Long_press_threshold_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1DKF_Shut_off_threshold_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1DKF_Shut_off_threshold_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_InteriorLights_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * EventFlag_T: Boolean
 * IntLghtLvlIndScaled_cmd_T: Integer in interval [0...15]
 *   Unit: [Step], Factor: 1, Offset: 0
 * Percent8bitNoOffset_T: Integer in interval [0...255]
 *   Unit: [Percent], Factor: 1, Offset: 0
 * SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T: Integer in interval [0...255]
 * SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T: Integer in interval [0...255]
 * SEWS_IL_CtrlDeviceTypeFront_P1DKH_T: Integer in interval [0...255]
 * SEWS_IL_CtrlDeviceTypeFront_P1DKH_T: Integer in interval [0...255]
 * SEWS_IL_LockingCmdDelayOff_P1K7E_T: Integer in interval [0...255]
 * SEWS_IL_LockingCmdDelayOff_P1K7E_T: Integer in interval [0...255]
 * SEWS_P1DKF_Long_press_threshold_T: Integer in interval [0...255]
 * SEWS_P1DKF_Long_press_threshold_T: Integer in interval [0...255]
 * SEWS_P1DKF_Shut_off_threshold_T: Integer in interval [0...255]
 * SEWS_P1DKF_Shut_off_threshold_T: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 *   FreeWheel_Status_NoMovement (0U)
 *   FreeWheel_Status_1StepClockwise (1U)
 *   FreeWheel_Status_2StepsClockwise (2U)
 *   FreeWheel_Status_3StepsClockwise (3U)
 *   FreeWheel_Status_4StepsClockwise (4U)
 *   FreeWheel_Status_5StepsClockwise (5U)
 *   FreeWheel_Status_6StepsClockwise (6U)
 *   FreeWheel_Status_1StepCounterClockwise (7U)
 *   FreeWheel_Status_2StepsCounterClockwise (8U)
 *   FreeWheel_Status_3StepsCounterClockwise (9U)
 *   FreeWheel_Status_4StepsCounterClockwise (10U)
 *   FreeWheel_Status_5StepsCounterClockwise (11U)
 *   FreeWheel_Status_6StepsCounterClockwise (12U)
 *   FreeWheel_Status_Spare (13U)
 *   FreeWheel_Status_Error (14U)
 *   FreeWheel_Status_NotAvailable (15U)
 * IL_ModeReq_T: Enumeration of integer in interval [0...7] with enumerators
 *   IL_ModeReq_OFF (0U)
 *   IL_ModeReq_NightDriving (1U)
 *   IL_ModeReq_Resting (2U)
 *   IL_ModeReq_Max (3U)
 *   IL_ModeReq_NonHMIModeResting (4U)
 *   IL_ModeReq_NonHMIModeMax (5U)
 *   IL_ModeReq_ErrorIndicator (6U)
 *   IL_ModeReq_NotAvailable (7U)
 * IL_Mode_T: Enumeration of integer in interval [0...7] with enumerators
 *   IL_Mode_OFF (0U)
 *   IL_Mode_NightDriving (1U)
 *   IL_Mode_Resting (2U)
 *   IL_Mode_Max (3U)
 *   IL_Mode_spare_1 (4U)
 *   IL_Mode_spare_2 (5U)
 *   IL_Mode_ErrorIndicator (6U)
 *   IL_Mode_NotAvailable (7U)
 * InteriorLightDimming_rqst_T: Enumeration of integer in interval [0...31] with enumerators
 *   InteriorLightDimming_rqst_NoMovement (0U)
 *   InteriorLightDimming_rqst_Position1 (1U)
 *   InteriorLightDimming_rqst_Position2 (2U)
 *   InteriorLightDimming_rqst_Position3 (3U)
 *   InteriorLightDimming_rqst_Position4 (4U)
 *   InteriorLightDimming_rqst_Position5 (5U)
 *   InteriorLightDimming_rqst_Position6 (6U)
 *   InteriorLightDimming_rqst_Position7 (7U)
 *   InteriorLightDimming_rqst_Position8 (8U)
 *   InteriorLightDimming_rqst_Position9 (9U)
 *   InteriorLightDimming_rqst_Position10 (10U)
 *   InteriorLightDimming_rqst_Position11 (11U)
 *   InteriorLightDimming_rqst_Position12 (12U)
 *   InteriorLightDimming_rqst_Position13 (13U)
 *   InteriorLightDimming_rqst_DimUpContinously (14U)
 *   InteriorLightDimming_rqst_DimDownContinously (15U)
 *   InteriorLightDimming_rqst_Error (30U)
 *   InteriorLightDimming_rqst_NotAvailable (31U)
 * Issm_IssStateType: Enumeration of integer in interval [0...2] with enumerators
 *   ISSM_STATE_INACTIVE (0U)
 *   ISSM_STATE_PENDING (1U)
 *   ISSM_STATE_ACTIVE (2U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * Rte_DT_InteriorLightMode_rqst_T_1: Enumeration of integer in interval [0...1] with enumerators
 *   EventFlag_Low (0U)
 *   EventFlag_High (1U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 * Record Types:
 * =============
 * InteriorLightMode_T: Record with elements
 *   IL_Mode_RE of type IL_Mode_T
 *   EventFlag_RE of type EventFlag_T
 * InteriorLightMode_rqst_T: Record with elements
 *   IL_Mode_RE of type IL_ModeReq_T
 *   EventFlag_RE of type Rte_DT_InteriorLightMode_rqst_T_1
 * SEWS_IL_ShortLongPushThresholds_P1DKF_s_T: Record with elements
 *   Long_press_threshold of type SEWS_P1DKF_Long_press_threshold_T
 *   Shut_off_threshold of type SEWS_P1DKF_Shut_off_threshold_T
 * SEWS_IL_ShortLongPushThresholds_P1DKF_s_T: Record with elements
 *   Long_press_threshold of type SEWS_P1DKF_Long_press_threshold_T
 *   Shut_off_threshold of type SEWS_P1DKF_Shut_off_threshold_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_IL_CtrlDeviceTypeFront_P1DKH_T Rte_Prm_P1DKH_IL_CtrlDeviceTypeFront_v(void)
 *   SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T Rte_Prm_P1DKI_IL_CtrlDeviceTypeBunk_v(void)
 *   SEWS_IL_LockingCmdDelayOff_P1K7E_T Rte_Prm_P1K7E_IL_LockingCmdDelayOff_v(void)
 *   SEWS_IL_ShortLongPushThresholds_P1DKF_s_T *Rte_Prm_P1DKF_IL_ShortLongPushThresholds_v(void)
 *
 *********************************************************************************************************************/


#define InteriorLights_HMICtrl_START_SEC_CODE
#include "InteriorLights_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: InteriorLights_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_BnkH1IntLghtMMenu_stat_InteriorLightMode(InteriorLightMode_T *data)
 *   Std_ReturnType Rte_Read_BunkBIntLightActvnBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH1IntLghtActvnBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH1IntLghtDirAccsDnBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH1IntLghtDirAccsUpBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH2IntLightActvnBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_DoorAutoFuncBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtActvnBtn_stat_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtMaxModeFlxSw2_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_IntLghtNightModeBtn2_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtNightModeBtn_stat_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtNightModeFlxSw2_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtRestModeBtnPnl2_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLightMode_CoreRqst_InteriorLightMode_rqst(InteriorLightMode_rqst_T *data)
 *   Std_ReturnType Rte_Read_InteriorLightLevelInd_cmd_InteriorLightLevelInd_cmd(Percent8bitNoOffset_T *data)
 *   Std_ReturnType Rte_Read_KeyfobLockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   boolean Rte_IsUpdated_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status(void)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DoorAutoFuncInd_cmd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_DoorAutoFunction_rqst_DoorAutoFunction_rqst(OffOn_T data)
 *   Std_ReturnType Rte_Write_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(IntLghtLvlIndScaled_cmd_T data)
 *   Std_ReturnType Rte_Write_IntLghtModeInd_cmd_InteriorLightMode(const InteriorLightMode_T *data)
 *   Std_ReturnType Rte_Write_IntLghtOffModeInd_cmd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_IntLightMaxModeInd_cmd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_IntLightNightModeInd_cmd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_IntLightRestingModeInd_cmd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_InteriorLightDimming_rqst_InteriorLightDimming_rqst(InteriorLightDimming_rqst_T data)
 *   Std_ReturnType Rte_Write_InteriorLightMode_rqst_InteriorLightMode_rqst(const InteriorLightMode_rqst_T *data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_UR_ANW_DimmingAdjustment2_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_DimmingAdjustment2_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_DimmingAdjustment2_GetIssState(Issm_IssStateType *issState)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights3_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights3_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights3_GetIssState(Issm_IssStateType *issState)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: InteriorLights_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, InteriorLights_HMICtrl_CODE) InteriorLights_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: InteriorLights_HMICtrl_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define InteriorLights_HMICtrl_STOP_SEC_CODE
#include "InteriorLights_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
