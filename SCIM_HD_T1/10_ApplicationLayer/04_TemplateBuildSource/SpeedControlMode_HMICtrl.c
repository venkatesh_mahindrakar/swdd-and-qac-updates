/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  SpeedControlMode_HMICtrl.c
 *        Config:  C:/GIT/scim_external_git/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  SpeedControlMode_HMICtrl
 *  Generated at:  Sat Oct 19 00:22:47 2019
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <SpeedControlMode_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_HeadwaySupport_P1BEX_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_HeadwaySupport_P1BEX_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_SpeedControlMode_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_HeadwaySupport_P1BEX_T: Integer in interval [0...255]
 * SEWS_HeadwaySupport_P1BEX_T: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * CCIM_ACC_T: Enumeration of integer in interval [0...7] with enumerators
 *   CCIM_ACC_ACCInactive (0U)
 *   CCIM_ACC_ACC1Active (1U)
 *   CCIM_ACC_ACC2Active (2U)
 *   CCIM_ACC_ACC3Active (3U)
 *   CCIM_ACC_Reserved (4U)
 *   CCIM_ACC_Reserved_01 (5U)
 *   CCIM_ACC_Error (6U)
 *   CCIM_ACC_NotAvailable (7U)
 * CCStates_T: Enumeration of integer in interval [0...15] with enumerators
 *   CCStates_OffDisabled (0U)
 *   CCStates_Hold (1U)
 *   CCStates_Accelerate (2U)
 *   CCStates_Decelerate (3U)
 *   CCStates_Resume (4U)
 *   CCStates_Set (5U)
 *   CCStates_Driver_Override (6U)
 *   CCStates_Spare_1 (7U)
 *   CCStates_Spare_2 (8U)
 *   CCStates_Spare_3 (9U)
 *   CCStates_Spare_4 (10U)
 *   CCStates_Spare_5 (11U)
 *   CCStates_Spare_6 (12U)
 *   CCStates_Spare_7 (13U)
 *   CCStates_Error (14U)
 *   CCStates_NotAvailable (15U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DriverMemory_rqst_T: Enumeration of integer in interval [0...31] with enumerators
 *   DriverMemory_rqst_UseDefaultDriverMemory (0U)
 *   DriverMemory_rqst_UseDriverMemory1 (1U)
 *   DriverMemory_rqst_UseDriverMemory2 (2U)
 *   DriverMemory_rqst_UseDriverMemory3 (3U)
 *   DriverMemory_rqst_UseDriverMemory4 (4U)
 *   DriverMemory_rqst_UseDriverMemory5 (5U)
 *   DriverMemory_rqst_UseDriverMemory6 (6U)
 *   DriverMemory_rqst_UseDriverMemory7 (7U)
 *   DriverMemory_rqst_UseDriverMemory8 (8U)
 *   DriverMemory_rqst_UseDriverMemory9 (9U)
 *   DriverMemory_rqst_UseDriverMemory10 (10U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory1 (11U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory2 (12U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory3 (13U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory4 (14U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory5 (15U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory6 (16U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory7 (17U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory8 (18U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory9 (19U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory10 (20U)
 *   DriverMemory_rqst_ResetAllMemThenUseDefDriverMem (21U)
 *   DriverMemory_rqst_Spare (22U)
 *   DriverMemory_rqst_Spare_01 (23U)
 *   DriverMemory_rqst_Spare_02 (24U)
 *   DriverMemory_rqst_Spare_03 (25U)
 *   DriverMemory_rqst_Spare_04 (26U)
 *   DriverMemory_rqst_Spare_05 (27U)
 *   DriverMemory_rqst_Spare_06 (28U)
 *   DriverMemory_rqst_Spare_07 (29U)
 *   DriverMemory_rqst_NotAvailable (30U)
 *   DriverMemory_rqst_Error (31U)
 * FWSelectedSpeedControlMode_T: Enumeration of integer in interval [0...7] with enumerators
 *   FWSelectedSpeedControlMode_Off (0U)
 *   FWSelectedSpeedControlMode_CruiseControl (1U)
 *   FWSelectedSpeedControlMode_AdjustableSpeedLimiter (2U)
 *   FWSelectedSpeedControlMode_AdaptiveCruiseControl (3U)
 *   FWSelectedSpeedControlMode_Spare_01 (4U)
 *   FWSelectedSpeedControlMode_Spare_02 (5U)
 *   FWSelectedSpeedControlMode_Error (6U)
 *   FWSelectedSpeedControlMode_NotAvailable (7U)
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 *   FreeWheel_Status_NoMovement (0U)
 *   FreeWheel_Status_1StepClockwise (1U)
 *   FreeWheel_Status_2StepsClockwise (2U)
 *   FreeWheel_Status_3StepsClockwise (3U)
 *   FreeWheel_Status_4StepsClockwise (4U)
 *   FreeWheel_Status_5StepsClockwise (5U)
 *   FreeWheel_Status_6StepsClockwise (6U)
 *   FreeWheel_Status_1StepCounterClockwise (7U)
 *   FreeWheel_Status_2StepsCounterClockwise (8U)
 *   FreeWheel_Status_3StepsCounterClockwise (9U)
 *   FreeWheel_Status_4StepsCounterClockwise (10U)
 *   FreeWheel_Status_5StepsCounterClockwise (11U)
 *   FreeWheel_Status_6StepsCounterClockwise (12U)
 *   FreeWheel_Status_Spare (13U)
 *   FreeWheel_Status_Error (14U)
 *   FreeWheel_Status_NotAvailable (15U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * XRSLStates_T: Enumeration of integer in interval [0...15] with enumerators
 *   XRSLStates_Off_disabled (0U)
 *   XRSLStates_Hold (1U)
 *   XRSLStates_Accelerate (2U)
 *   XRSLStates_Decelerate (3U)
 *   XRSLStates_Resume (4U)
 *   XRSLStates_Set (5U)
 *   XRSLStates_Driver_override (6U)
 *   XRSLStates_Error (14U)
 *   XRSLStates_NotAvailable (15U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1B2C_CCFW_Installed_v(void)
 *   SEWS_HeadwaySupport_P1BEX_T Rte_Prm_P1BEX_HeadwaySupport_v(void)
 *
 *********************************************************************************************************************/


#define SpeedControlMode_HMICtrl_START_SEC_CODE
#include "SpeedControlMode_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SpeedControlMode_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_CCStates_CCStates(CCStates_T *data)
 *   Std_ReturnType Rte_Read_DriverMemory_rqst_DriverMemory_rqst(DriverMemory_rqst_T *data)
 *   Std_ReturnType Rte_Read_SpeedControlModeButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SpeedControlModeWheelStatus_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_XRSLStates_XRSLStates(XRSLStates_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ACCOrCCIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_ASLIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_FWSelectedACCMode_CCIM_ACC(CCIM_ACC_T data)
 *   Std_ReturnType Rte_Write_FWSelectedSpeedControlMode_FWSelectedSpeedControlMode(FWSelectedSpeedControlMode_T data)
 *   Std_ReturnType Rte_Write_FWSpeedControlEndStopEvent_FWSpeedControlEndStopEvent(OffOn_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SpeedControlMode_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SpeedControlMode_HMICtrl_CODE) SpeedControlMode_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SpeedControlMode_HMICtrl_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SpeedControlMode_HMICtrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SpeedControlMode_HMICtrl_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SpeedControlMode_HMICtrl_CODE) SpeedControlMode_HMICtrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SpeedControlMode_HMICtrl_Init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define SpeedControlMode_HMICtrl_STOP_SEC_CODE
#include "SpeedControlMode_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
