/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  ExtraBbTailLiftCrane_HMICtrl.c
 *        Config:  C:/GIT/scim_external_git/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  ExtraBbTailLiftCrane_HMICtrl
 *  Generated at:  Sat Oct 19 00:22:45 2019
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <ExtraBbTailLiftCrane_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_CraneHMIDeviceType_P1CXA_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_CraneHMIDeviceType_P1CXA_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_CraneSwIndicationType_P1CXC_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_CraneSwIndicationType_P1CXC_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_TailLiftHMIDeviceType_P1CW9_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_TailLiftHMIDeviceType_P1CW9_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_TailLiftTimeoutForRequest_P1DWA_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_TailLiftTimeoutForRequest_P1DWA_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_TailLift_Crane_Act_P1CXB_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_TailLift_Crane_Act_P1CXB_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *
 * Runnable Entities:
 * ==================
 * ExtraBbTailLiftCrane_HMICtrl_Crane_20ms_runnable
 *   2019.08.08 - Splitted runnable for Crane
 *
 * ExtraBbTailLiftCrane_HMICtrl_TailLift_20ms_runnable
 *   2019.08.08 - Splitted runnable for TailLift
 *
 *********************************************************************************************************************/

#include "Rte_ExtraBbTailLiftCrane_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_CraneHMIDeviceType_P1CXA_T: Integer in interval [0...255]
 * SEWS_CraneHMIDeviceType_P1CXA_T: Integer in interval [0...255]
 * SEWS_CraneSwIndicationType_P1CXC_T: Integer in interval [0...255]
 * SEWS_CraneSwIndicationType_P1CXC_T: Integer in interval [0...255]
 * SEWS_TailLiftHMIDeviceType_P1CW9_T: Integer in interval [0...255]
 * SEWS_TailLiftHMIDeviceType_P1CW9_T: Integer in interval [0...255]
 * SEWS_TailLiftTimeoutForRequest_P1DWA_T: Integer in interval [0...255]
 * SEWS_TailLiftTimeoutForRequest_P1DWA_T: Integer in interval [0...255]
 * SEWS_TailLift_Crane_Act_P1CXB_T: Integer in interval [0...255]
 * SEWS_TailLift_Crane_Act_P1CXB_T: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * ExtraBBCraneStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   ExtraBBCraneStatus_Disable (0U)
 *   ExtraBBCraneStatus_Enable (1U)
 *   ExtraBBCraneStatus_Active (2U)
 *   ExtraBBCraneStatus_Spare (3U)
 *   ExtraBBCraneStatus_Spare_01 (4U)
 *   ExtraBBCraneStatus_Spare_02 (5U)
 *   ExtraBBCraneStatus_Error (6U)
 *   ExtraBBCraneStatus_NotAvailable (7U)
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   InactiveActive_Inactive (0U)
 *   InactiveActive_Active (1U)
 *   InactiveActive_Error (2U)
 *   InactiveActive_NotAvailable (3U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1B9X_WirelessRC_Enable_v(void)
 *   SEWS_TailLiftHMIDeviceType_P1CW9_T Rte_Prm_P1CW9_TailLiftHMIDeviceType_v(void)
 *   SEWS_CraneHMIDeviceType_P1CXA_T Rte_Prm_P1CXA_CraneHMIDeviceType_v(void)
 *   SEWS_TailLift_Crane_Act_P1CXB_T Rte_Prm_P1CXB_TailLift_Crane_Act_v(void)
 *   SEWS_CraneSwIndicationType_P1CXC_T Rte_Prm_P1CXC_CraneSwIndicationType_v(void)
 *   SEWS_TailLiftTimeoutForRequest_P1DWA_T Rte_Prm_P1DWA_TailLiftTimeoutForRequest_v(void)
 *
 *********************************************************************************************************************/


#define ExtraBbTailLiftCrane_HMICtrl_START_SEC_CODE
#include "ExtraBbTailLiftCrane_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ExtraBbTailLiftCrane_HMICtrl_Crane_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_CranePushButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_CraneSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_ExtraBBCraneStatus_ExtraBBCraneStatus(ExtraBBCraneStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_WRCCraneRequest_WRCCraneRequest(PushButtonStatus_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_BBCraneRequest_BBCraneRequest(OffOn_T data)
 *   Std_ReturnType Rte_Write_CraneSupply_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ExtraBbTailLiftCrane_HMICtrl_Crane_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ExtraBbTailLiftCrane_HMICtrl_CODE) ExtraBbTailLiftCrane_HMICtrl_Crane_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ExtraBbTailLiftCrane_HMICtrl_Crane_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ExtraBbTailLiftCrane_HMICtrl_TailLift_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ExtraBBTailLiftStatus_ExtraBBTailLiftStatus(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_TailLiftPushButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_TailLiftSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_WRCTailLiftRequest_WRCTailLiftRequest(PushButtonStatus_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_BBTailLiftRequest_BBTailLiftRequest(OffOn_T data)
 *   Std_ReturnType Rte_Write_TailLift_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ExtraBbTailLiftCrane_HMICtrl_TailLift_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ExtraBbTailLiftCrane_HMICtrl_CODE) ExtraBbTailLiftCrane_HMICtrl_TailLift_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ExtraBbTailLiftCrane_HMICtrl_TailLift_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define ExtraBbTailLiftCrane_HMICtrl_STOP_SEC_CODE
#include "ExtraBbTailLiftCrane_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/



#if 0
/***  Start of saved code (symbol: runnable implementation:ExtraBbTailLiftCrane_HMICtrl_20ms_runnable)  *****/


/***  End of saved code  ************************************************************************************/
#endif

#if 0
/***  Start of saved code (symbol: documentation area:ExtraBbTailLiftCrane_HMICtrl_20ms_runnable_doc)  ******/


/***  End of saved code  ************************************************************************************/
#endif

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
