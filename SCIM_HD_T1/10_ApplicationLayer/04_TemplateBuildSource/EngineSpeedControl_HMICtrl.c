/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  EngineSpeedControl_HMICtrl.c
 *        Config:  C:/GIT/scim_external_git/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  EngineSpeedControl_HMICtrl
 *  Generated at:  Sat Oct 19 00:22:44 2019
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <EngineSpeedControl_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_BodybuilderAccessToAccelPedal_P1B72_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_BodybuilderAccessToAccelPedal_P1B72_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Speed16bit_T
 *   65024 - 65279 Error ; 65280 - 65535 Not available
 *
 *********************************************************************************************************************/

#include "Rte_EngineSpeedControl_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_BodybuilderAccessToAccelPedal_P1B72_T: Integer in interval [0...255]
 * SEWS_BodybuilderAccessToAccelPedal_P1B72_T: Integer in interval [0...255]
 * SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T: Integer in interval [0...255]
 * SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T: Integer in interval [0...255]
 * Speed16bit_T: Integer in interval [0...65535]
 *   Unit: [Km_per_h], Factor: 1, Offset: 0
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * AcceleratorPedalStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   AcceleratorPedalStatus_SecPedNotActPrimPedAct (0U)
 *   AcceleratorPedalStatus_SecPedActPrimPedNotAct (1U)
 *   AcceleratorPedalStatus_PrimAndSecPedalInhibited (2U)
 *   AcceleratorPedalStatus_Spare1 (3U)
 *   AcceleratorPedalStatus_Spare2 (4U)
 *   AcceleratorPedalStatus_Spare3 (5U)
 *   AcceleratorPedalStatus_Error (6U)
 *   AcceleratorPedalStatus_NotAvailable (7U)
 * ButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   ButtonStatus_Idle (0U)
 *   ButtonStatus_Pressed (1U)
 *   ButtonStatus_Error (2U)
 *   ButtonStatus_NotAvailable (3U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DisableEnable_T: Enumeration of integer in interval [0...3] with enumerators
 *   DisableEnable_Disable (0U)
 *   DisableEnable_Enable (1U)
 *   DisableEnable_Error (2U)
 *   DisableEnable_NotAvailable (3U)
 * EngineSpeedControlStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   EngineSpeedControlStatus_Inactive (0U)
 *   EngineSpeedControlStatus_ActiveManual (1U)
 *   EngineSpeedControlStatus_ActiveExternalSource (2U)
 *   EngineSpeedControlStatus_CrossCountryEngineSpeedControlActive (3U)
 *   EngineSpeedControlStatus_AutoPtoMin_AutoNeutral (4U)
 *   EngineSpeedControlStatus_Spare_01 (5U)
 *   EngineSpeedControlStatus_Error (6U)
 *   EngineSpeedControlStatus_NotAvailable (7U)
 * EngineSpeedRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   EngineSpeedRequest_NoRequest (0U)
 *   EngineSpeedRequest_Off (1U)
 *   EngineSpeedRequest_Activate (2U)
 *   EngineSpeedRequest_Increase (3U)
 *   EngineSpeedRequest_Decrease (4U)
 *   EngineSpeedRequest_Spare (5U)
 *   EngineSpeedRequest_Error (6U)
 *   EngineSpeedRequest_NotAvailable (7U)
 * EscActionRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   EscActionRequest_NoRequest (0U)
 *   EscActionRequest_Resume (1U)
 *   EscActionRequest_Increase (2U)
 *   EscActionRequest_Decrease (3U)
 *   EscActionRequest_Set (4U)
 *   EscActionRequest_Spare (5U)
 *   EscActionRequest_Error (6U)
 *   EscActionRequest_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * SWSpdCtrlButtonsStatus1_T: Enumeration of integer in interval [0...15] with enumerators
 *   SWSpdCtrlButtonsStatus1_None (0U)
 *   SWSpdCtrlButtonsStatus1_PauseOff (1U)
 *   SWSpdCtrlButtonsStatus1_Resume (2U)
 *   SWSpdCtrlButtonsStatus1_Increase (3U)
 *   SWSpdCtrlButtonsStatus1_Decrease (4U)
 *   SWSpdCtrlButtonsStatus1_Enter (5U)
 *   SWSpdCtrlButtonsStatus1_CC (6U)
 *   SWSpdCtrlButtonsStatus1_Overspeed (7U)
 *   SWSpdCtrlButtonsStatus1_ACC (8U)
 *   SWSpdCtrlButtonsStatus1_TimeGap (9U)
 *   SWSpdCtrlButtonsStatus1_Spare (10U)
 *   SWSpdCtrlButtonsStatus1_Spare_01 (11U)
 *   SWSpdCtrlButtonsStatus1_Spare_02 (12U)
 *   SWSpdCtrlButtonsStatus1_Spare_03 (13U)
 *   SWSpdCtrlButtonsStatus1_Error (14U)
 *   SWSpdCtrlButtonsStatus1_NotAvaillable (15U)
 * SWSpeedControlAdjustMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   SWSpeedControlAdjustMode_AdjustSpeed (0U)
 *   SWSpeedControlAdjustMode_AdjustOverspeed (1U)
 *   SWSpeedControlAdjustMode_AdjustTimeGap (2U)
 *   SWSpeedControlAdjustMode_Spare (3U)
 *   SWSpeedControlAdjustMode_Spare_01 (4U)
 *   SWSpeedControlAdjustMode_Spare_02 (5U)
 *   SWSpeedControlAdjustMode_Spare_03 (6U)
 *   SWSpeedControlAdjustMode_Spare_04 (7U)
 *   SWSpeedControlAdjustMode_Spare_05 (8U)
 *   SWSpeedControlAdjustMode_Spare_06 (9U)
 *   SWSpeedControlAdjustMode_Spare_07 (10U)
 *   SWSpeedControlAdjustMode_Spare_08 (11U)
 *   SWSpeedControlAdjustMode_Spare_09 (12U)
 *   SWSpeedControlAdjustMode_Spare_10 (13U)
 *   SWSpeedControlAdjustMode_Error (14U)
 *   SWSpeedControlAdjustMode_NotAvailable (15U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1B0W_CrossCountryCC_Act_v(void)
 *   boolean Rte_Prm_P1B0X_EngineSpeedControlSw_v(void)
 *   SEWS_BodybuilderAccessToAccelPedal_P1B72_T Rte_Prm_P1B72_BodybuilderAccessToAccelPedal_v(void)
 *   SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T Rte_Prm_P1IZ3_ESC_InhibitionByPrimaryPedal_v(void)
 *
 *********************************************************************************************************************/


#define EngineSpeedControl_HMICtrl_START_SEC_CODE
#include "EngineSpeedControl_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: EngineSpeedControl_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ACCEnableRqst_ACCEnableRqst(DisableEnable_T *data)
 *   Std_ReturnType Rte_Read_AcceleratorPedalStatus_AcceleratorPedalStatus(AcceleratorPedalStatus_T *data)
 *   Std_ReturnType Rte_Read_CCEnableRequest_CCEnableRequest(DisableEnable_T *data)
 *   Std_ReturnType Rte_Read_EngineSpeedControlStatus_EngineSpeedControlStatus(EngineSpeedControlStatus_T *data)
 *   Std_ReturnType Rte_Read_EscButtonMuddySiteStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_EscSwitchEnableStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_EscSwitchIncDecStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_EscSwitchMuddySiteStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_EscSwitchResumeStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SWSpdCtrlButtonsStatus1_SWSpdCtrlButtonsStatus1(SWSpdCtrlButtonsStatus1_T *data)
 *   Std_ReturnType Rte_Read_SWSpeedControlAdjustMode_SWSpeedControlAdjustMode(SWSpeedControlAdjustMode_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
 *   Std_ReturnType Rte_Read_WrcEngSpdCtrlDecreaseButtStat_WrcEngSpdCtrlDecreaseButtStat(ButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_WrcEngSpdCtrlEnableStatus_WrcEngSpdCtrlEnableStatus(DisableEnable_T *data)
 *   Std_ReturnType Rte_Read_WrcEngSpdCtrlIncreaseButtStat_WrcEngSpdCtrlIncreaseButtStat(ButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_WrcEngSpdCtrlResumeButtonStat_WrcEngSpdCtrlResumeButtonStat(ButtonStatus_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_AdjustRequestForIdle_AdjustRequestForIdle(EngineSpeedRequest_T data)
 *   Std_ReturnType Rte_Write_EscButtonMuddySiteDeviceInd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_EscCabActionRequest_EscCabActionRequest(EscActionRequest_T data)
 *   Std_ReturnType Rte_Write_EscCabEnable_EscCabEnable(DisableEnable_T data)
 *   Std_ReturnType Rte_Write_EscCrossCountryActivation_rqst_EscCrossCountryActivation_rqst(DisableEnable_T data)
 *   Std_ReturnType Rte_Write_EscSwitchEnableDeviceInd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_EscSwitchMuddySiteDeviceInd_DeviceIndication(DeviceIndication_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: EngineSpeedControl_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, EngineSpeedControl_HMICtrl_CODE) EngineSpeedControl_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: EngineSpeedControl_HMICtrl_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: EngineSpeedControl_HMICtrl_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: EngineSpeedControl_HMICtrl_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, EngineSpeedControl_HMICtrl_CODE) EngineSpeedControl_HMICtrl_init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: EngineSpeedControl_HMICtrl_init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define EngineSpeedControl_HMICtrl_STOP_SEC_CODE
#include "EngineSpeedControl_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
