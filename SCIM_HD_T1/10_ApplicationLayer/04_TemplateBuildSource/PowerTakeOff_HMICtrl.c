/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  PowerTakeOff_HMICtrl.c
 *        Config:  C:/GIT/scim_external_git/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  PowerTakeOff_HMICtrl
 *  Generated at:  Sat Oct 19 00:22:47 2019
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <PowerTakeOff_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PTO_EmergencyFilteringTimer_P1BD3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PTO_EmergencyFilteringTimer_P1BD3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PTO_RequestFilteringTimer_P1BD4_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PTO_RequestFilteringTimer_P1BD4_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_PowerTakeOff_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T: Integer in interval [0...255]
 * SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T: Integer in interval [0...255]
 * SEWS_PTO_EmergencyFilteringTimer_P1BD3_T: Integer in interval [0...255]
 * SEWS_PTO_EmergencyFilteringTimer_P1BD3_T: Integer in interval [0...255]
 * SEWS_PTO_RequestFilteringTimer_P1BD4_T: Integer in interval [0...255]
 * SEWS_PTO_RequestFilteringTimer_P1BD4_T: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * ButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   ButtonStatus_Idle (0U)
 *   ButtonStatus_Pressed (1U)
 *   ButtonStatus_Error (2U)
 *   ButtonStatus_NotAvailable (3U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   InactiveActive_Inactive (0U)
 *   InactiveActive_Active (1U)
 *   InactiveActive_Error (2U)
 *   InactiveActive_NotAvailable (3U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * Request_T: Enumeration of integer in interval [0...3] with enumerators
 *   Request_NotRequested (0U)
 *   Request_RequestActive (1U)
 *   Request_Error (2U)
 *   Request_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1AJL_PTO1_Act_v(void)
 *   boolean Rte_Prm_P1AJM_PTO2_Act_v(void)
 *   boolean Rte_Prm_P1B9X_WirelessRC_Enable_v(void)
 *   boolean Rte_Prm_P1BBG_PTO3_Act_v(void)
 *   boolean Rte_Prm_P1BBH_PTO4_Act_v(void)
 *   SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T Rte_Prm_P1BD2_PTO_EmergencyDeactivationTimer_v(void)
 *   SEWS_PTO_EmergencyFilteringTimer_P1BD3_T Rte_Prm_P1BD3_PTO_EmergencyFilteringTimer_v(void)
 *   SEWS_PTO_RequestFilteringTimer_P1BD4_T Rte_Prm_P1BD4_PTO_RequestFilteringTimer_v(void)
 *
 *********************************************************************************************************************/


#define PowerTakeOff_HMICtrl_START_SEC_CODE
#include "PowerTakeOff_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: PowerTakeOff_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Pto1Indication_Pto1Indication(Request_T *data)
 *   Std_ReturnType Rte_Read_Pto1Status_Pto1Status(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_Pto1SwitchStatus_Pto1SwitchStatus(OffOn_T *data)
 *   Std_ReturnType Rte_Read_Pto2Indication_Pto2Indication(Request_T *data)
 *   Std_ReturnType Rte_Read_Pto2Status_Pto2Status(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_Pto2SwitchStatus_Pto2SwitchStatus(OffOn_T *data)
 *   Std_ReturnType Rte_Read_Pto3Indication_Pto3Indication(Request_T *data)
 *   Std_ReturnType Rte_Read_Pto3Status_Pto3Status(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_Pto3SwitchStatus_Pto3SwitchStatus(OffOn_T *data)
 *   Std_ReturnType Rte_Read_Pto4Indication_Pto4Indication(Request_T *data)
 *   Std_ReturnType Rte_Read_Pto4Status_Pto4Status(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_Pto4SwitchStatus_Pto4SwitchStatus(OffOn_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_WrcPto1ButtonStatus_WrcPto1ButtonStatus(ButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_WrcPto2ButtonStatus_WrcPto2ButtonStatus(ButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_WrcPto3ButtonStatus_WrcPto3ButtonStatus(ButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_WrcPto4ButtonStatus_WrcPto4ButtonStatus(ButtonStatus_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PTO1_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_PTO2_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_PTO3_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_PTO4_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Pto1CabRequest_Pto1CabRequest(OffOn_T data)
 *   Std_ReturnType Rte_Write_Pto2CabRequest_Pto2CabRequest(OffOn_T data)
 *   Std_ReturnType Rte_Write_Pto3CabRequest_Pto3CabRequest(OffOn_T data)
 *   Std_ReturnType Rte_Write_Pto4CabRequest_Pto4CabRequest(OffOn_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: PowerTakeOff_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PowerTakeOff_HMICtrl_CODE) PowerTakeOff_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: PowerTakeOff_HMICtrl_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: PowerTakeOff_HMICtrl_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: PowerTakeOff_HMICtrl_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PowerTakeOff_HMICtrl_CODE) PowerTakeOff_HMICtrl_init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: PowerTakeOff_HMICtrl_init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define PowerTakeOff_HMICtrl_STOP_SEC_CODE
#include "PowerTakeOff_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
