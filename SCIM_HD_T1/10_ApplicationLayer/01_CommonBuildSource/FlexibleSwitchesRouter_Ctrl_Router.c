/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  FlexibleSwitchesRouter_Ctrl_Router.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  FlexibleSwitchesRouter_Ctrl_Router
 *  Generation Time:  2020-10-07 15:25:40
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <FlexibleSwitchesRouter_Ctrl_Router>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file FlexibleSwitchesRouter_Ctrl_Router.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_Platform_SCIM_LIN 
//! @{
//! @addtogroup FlexibleSwitchesRouter_Ctrl
//! @{
//!
//! \brief
//! FlexibleSwitchesRouter_Ctrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the FlexibleSwitchesRouter_Ctrl runnables.
//!======================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   Boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_InitMonitorReasonType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FS_DiagAct_ID001_ID009_P1EAA_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID010_ID019_P1EAB_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID02_ID029_P1EAC_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID030_ID039_P1EAD_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID040_ID049_P1EAE_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID050_ID059_P1EAF_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID060_ID069_P1EAG_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID070_ID079_P1EAH_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID080_ID089_P1EAI_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID100_ID109_P1EAK_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID110_ID119_P1EAL_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID120_ID129_P1EAM_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID130_ID139_P1EAN_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID140_ID149_P1EAO_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID150_ID159_P1EAP_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID170_ID179_P1EAR_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID180_ID189_P1EAS_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID190_ID199_P1EAT_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID200_ID209_P1EAU_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID210_ID219_P1EAV_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID220_ID229_P1EAW_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID230_ID239_P1EAX_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID240_ID249_P1EAY_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_LIN_topology_P1AJR_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_FlexibleSwitchesRouter_Ctrl_Router.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "FlexibleSwitchesRouter_Ctrl_Router.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_FS_DiagAct_ID001_ID009_P1EAA_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID010_ID019_P1EAB_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID02_ID029_P1EAC_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID030_ID039_P1EAD_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID040_ID049_P1EAE_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID050_ID059_P1EAF_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID060_ID069_P1EAG_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID070_ID079_P1EAH_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID080_ID089_P1EAI_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID100_ID109_P1EAK_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID110_ID119_P1EAL_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID120_ID129_P1EAM_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID130_ID139_P1EAN_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID140_ID149_P1EAO_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID150_ID159_P1EAP_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID170_ID179_P1EAR_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID180_ID189_P1EAS_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID190_ID199_P1EAT_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID200_ID209_P1EAU_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID210_ID219_P1EAV_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID220_ID229_P1EAW_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID230_ID239_P1EAX_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID240_ID249_P1EAY_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T: Integer in interval [0...65535]
 * SEWS_LIN_topology_P1AJR_T: Integer in interval [0...255]
 * SwitchDetectionNeeded_T: Boolean
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * Dem_InitMonitorReasonType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_INIT_MONITOR_CLEAR (1U)
 *   DEM_INIT_MONITOR_RESTART (2U)
 *   DEM_INIT_MONITOR_REENABLED (3U)
 *   DEM_INIT_MONITOR_STORAGE_REENABLED (4U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DualDeviceIndication_T: Enumeration of integer in interval [0...15] with enumerators
 *   DualDeviceIndication_UpperOffLowerOff (0U)
 *   DualDeviceIndication_UpperOnLowerOff (1U)
 *   DualDeviceIndication_UpperBlinkLowerOff (2U)
 *   DualDeviceIndication_UpperDontCareLowerOff (3U)
 *   DualDeviceIndication_UpperOffLowerOn (4U)
 *   DualDeviceIndication_UpperOnLowerOn (5U)
 *   DualDeviceIndication_UpperBlinkLowerOn (6U)
 *   DualDeviceIndication_UpperDontCareLowerOn (7U)
 *   DualDeviceIndication_UpperOffLowerBlink (8U)
 *   DualDeviceIndication_UpperOnLowerBlink (9U)
 *   DualDeviceIndication_UpperBlinkLowerBlink (10U)
 *   DualDeviceIndication_UpperDontCareLowerBlink (11U)
 *   DualDeviceIndication_UpperOffLowerDontCare (12U)
 *   DualDeviceIndication_UpperOnLowerDontCare (13U)
 *   DualDeviceIndication_UpperBlinkLowerDontCare (14U)
 *   DualDeviceIndication_UpperDontCareLowerDontCare (15U)
 * IndicationCmd_T: Enumeration of integer in interval [0...1] with enumerators
 *   IndicationCmd_OFFLEDNotActivated (0U)
 *   IndicationCmd_ONLEDActivated (1U)
 * NeutralPushed_T: Enumeration of integer in interval [0...3] with enumerators
 *   NeutralPushed_Neutral (0U)
 *   NeutralPushed_Pushed (1U)
 *   NeutralPushed_Error (2U)
 *   NeutralPushed_NotAvailable (3U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 * Array Types:
 * ============
 * Dcm_Data120ByteType: Array with 120 element(s) of type uint8
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data5ByteType: Array with 5 element(s) of type uint8
 * Dcm_Data8ByteType: Array with 8 element(s) of type uint8
 * FSPIndicationCmdArray_T: Array with 8 element(s) of type DeviceIndication_T
 * FSPSwitchStatusArray_T: Array with 8 element(s) of type PushButtonStatus_T
 * FSP_Array10_8: Array with 10 element(s) of type FSP_Array8
 * FSP_Array8: Array with 8 element(s) of type uint8
 * FlexibleSwDisableDiagPresence_Type: Array with 5 element(s) of type uint8
 * FlexibleSwitchesinFailure_Type: Array with 24 element(s) of type FlexibleSwitchesinFailure_T
 * SwitchDetectionResp_T: Array with 8 element(s) of type uint8
 *
 * Record Types:
 * =============
 * FlexibleSwitchesinFailure_T: Record with elements
 *   FlexibleSwitchFailureType of type uint8
 *   FlexibleSwitchID of type uint8
 *   FlexibleSwitchPosition of type uint8
 *   FlexibleSwitchPanel of type uint8
 *   LINbus of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   uint8 *Rte_Pim_Pim_P1DCT_Info(void)
 *   uint8 *Rte_Pim_Pim_FlexibleSwDisableDiagPresence(void)
 *     Returnvalue: uint8* is of type FlexibleSwDisableDiagPresence_Type
 *   FlexibleSwitchesinFailure_T *Rte_Pim_Pim_FlexibleSwFailureData(void)
 *     Returnvalue: FlexibleSwitchesinFailure_T* is of type FlexibleSwitchesinFailure_Type
 *   uint8 *Rte_Pim_Pim_FlexibleSwStatus(void)
 *     Returnvalue: uint8* is of type FSP_Array10_8
 *   uint8 *Rte_Pim_Pim_TableOfDetectedId(void)
 *     Returnvalue: uint8* is of type FSP_Array10_8
 *
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_FS_DiagAct_ID001_ID009_P1EAA_T Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v(void)
 *   SEWS_FS_DiagAct_ID010_ID019_P1EAB_T Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v(void)
 *   SEWS_FS_DiagAct_ID02_ID029_P1EAC_T Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v(void)
 *   SEWS_FS_DiagAct_ID030_ID039_P1EAD_T Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v(void)
 *   SEWS_FS_DiagAct_ID040_ID049_P1EAE_T Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v(void)
 *   SEWS_FS_DiagAct_ID050_ID059_P1EAF_T Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v(void)
 *   SEWS_FS_DiagAct_ID060_ID069_P1EAG_T Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v(void)
 *   SEWS_FS_DiagAct_ID070_ID079_P1EAH_T Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v(void)
 *   SEWS_FS_DiagAct_ID080_ID089_P1EAI_T Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v(void)
 *   SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v(void)
 *   SEWS_FS_DiagAct_ID100_ID109_P1EAK_T Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v(void)
 *   SEWS_FS_DiagAct_ID110_ID119_P1EAL_T Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v(void)
 *   SEWS_FS_DiagAct_ID120_ID129_P1EAM_T Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v(void)
 *   SEWS_FS_DiagAct_ID130_ID139_P1EAN_T Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v(void)
 *   SEWS_FS_DiagAct_ID140_ID149_P1EAO_T Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v(void)
 *   SEWS_FS_DiagAct_ID150_ID159_P1EAP_T Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v(void)
 *   SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v(void)
 *   SEWS_FS_DiagAct_ID170_ID179_P1EAR_T Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v(void)
 *   SEWS_FS_DiagAct_ID180_ID189_P1EAS_T Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v(void)
 *   SEWS_FS_DiagAct_ID190_ID199_P1EAT_T Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v(void)
 *   SEWS_FS_DiagAct_ID200_ID209_P1EAU_T Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v(void)
 *   SEWS_FS_DiagAct_ID210_ID219_P1EAV_T Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v(void)
 *   SEWS_FS_DiagAct_ID220_ID229_P1EAW_T Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v(void)
 *   SEWS_FS_DiagAct_ID230_ID239_P1EAX_T Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v(void)
 *   SEWS_FS_DiagAct_ID240_ID249_P1EAY_T Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v(void)
 *   SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v(void)
 *   SEWS_LIN_topology_P1AJR_T Rte_Prm_P1AJR_LIN_topology_v(void)
 *   boolean Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v(void)
 *
 *********************************************************************************************************************/


#define FlexibleSwitchesRouter_Ctrl_Router_START_SEC_CODE
#include "FlexibleSwitchesRouter_Ctrl_Router_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_FS_DiagAct_ID001_ID009      (Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v())
#define PCODE_FS_DiagAct_ID010_ID019      (Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v())
#define PCODE_FS_DiagAct_ID020_ID029      (Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v())
#define PCODE_FS_DiagAct_ID030_ID039      (Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v())
#define PCODE_FS_DiagAct_ID040_ID049      (Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v())
#define PCODE_FS_DiagAct_ID050_ID059      (Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v())
#define PCODE_FS_DiagAct_ID060_ID069      (Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v())
#define PCODE_FS_DiagAct_ID070_ID079      (Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v())
#define PCODE_FS_DiagAct_ID080_ID089      (Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v())
#define PCODE_FS_DiagAct_ID090_ID099      (Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v())
#define PCODE_FS_DiagAct_ID100_ID109      (Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v())
#define PCODE_FS_DiagAct_ID110_ID119      (Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v())
#define PCODE_FS_DiagAct_ID120_ID129      (Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v())
#define PCODE_FS_DiagAct_ID130_ID139      (Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v())
#define PCODE_FS_DiagAct_ID140_ID149      (Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v())
#define PCODE_FS_DiagAct_ID150_ID159      (Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v())
#define PCODE_FS_DiagAct_ID160_ID169      (Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v())
#define PCODE_FS_DiagAct_ID170_ID179      (Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v())
#define PCODE_FS_DiagAct_ID180_ID189      (Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v())
#define PCODE_FS_DiagAct_ID190_ID199      (Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v())
#define PCODE_FS_DiagAct_ID200_ID209      (Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v())
#define PCODE_FS_DiagAct_ID210_ID219      (Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v())
#define PCODE_FS_DiagAct_ID220_ID229      (Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v())
#define PCODE_FS_DiagAct_ID230_ID239      (Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v())
#define PCODE_FS_DiagAct_ID240_ID249      (Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v())
#define PCODE_FS_DiagAct_ID250_ID254      (Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v())

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBInitEvt_D1BUL_31_FlexSwEEPROM_InitMonitorForEvent
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <InitMonitorForEvent> of PortPrototype <CBInitEvt_D1BUL_31_FlexSwEEPROM>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BOX_4A_FS_NotSupportedID_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOY_4A_FS_NotSupportedDoubleID_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BUL_31_FlexSwEEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BUL_95_FlexSwConfigFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBInitEvt_D1BUL_31_FlexSwEEPROM_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CallbackInitMonitorForEvent_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBInitEvt_D1BUL_31_FlexSwEEPROM_InitMonitorForEvent_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) CBInitEvt_D1BUL_31_FlexSwEEPROM_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBInitEvt_D1BUL_31_FlexSwEEPROM_InitMonitorForEvent (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBInitEvt_D1BUL_95_FlexSwConfigFailure_InitMonitorForEvent
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <InitMonitorForEvent> of PortPrototype <CBInitEvt_D1BUL_95_FlexSwConfigFailure>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBInitEvt_D1BUL_95_FlexSwConfigFailure_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CallbackInitMonitorForEvent_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBInitEvt_D1BUL_95_FlexSwConfigFailure_InitMonitorForEvent_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) CBInitEvt_D1BUL_95_FlexSwConfigFailure_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBInitEvt_D1BUL_95_FlexSwConfigFailure_InitMonitorForEvent (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BW0_Data_P1BW0_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_FlexibleSwStatus();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP1][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BW1_Data_P1BW1_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_FlexibleSwStatus();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP2][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BW3_Data_P1BW3_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_FlexibleSwStatus();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP3][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BW4_Data_P1BW4_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_FlexibleSwStatus();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP4][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BW5_Data_P1BW5_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_FlexibleSwStatus();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP5][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BW6_Data_P1BW6_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/
    uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_FlexibleSwStatus();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP6][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BW8_Data_P1BW8_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/
    uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_FlexibleSwStatus();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
       Data[slotID] = (*ReadData)[FSP7][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BW9_Data_P1BW9_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/
    uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_FlexibleSwStatus();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP8][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWI_Data_P1BWI_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_TableOfDetectedId();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP1][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWJ_Data_P1BWJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_TableOfDetectedId();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP2][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWL_Data_P1BWL_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_TableOfDetectedId();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP3][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWM_Data_P1BWM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_TableOfDetectedId();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP4][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWN_Data_P1BWN_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_TableOfDetectedId();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP5][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWO_Data_P1BWO_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_TableOfDetectedId();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP6][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWQ_Data_P1BWQ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_TableOfDetectedId();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP7][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWR_Data_P1BWR_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_TableOfDetectedId();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP8][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWV_Data_P1BWV_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_TableOfDetectedId();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP9][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWX_Data_P1BWX_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_TableOfDetectedId();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSPB][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BXD_Data_P1BXD_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_CODE) DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_FlexibleSwStatus();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSP9][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BXF_Data_P1BXF_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_CODE) DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*ReadData)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   uint8 slotID = 0U;
   ReadData = (uint8(*)[MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_FlexibleSwStatus();
   for (slotID = 0U; slotID < MAX_SLOT_NO; slotID++)
   {
      Data[slotID] = (*ReadData)[FSPB][slotID];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1DCT_Data_P1DCT_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 *P1DCT_Value = 0;
   P1DCT_Value = Rte_Pim_Pim_P1DCT_Info();
   Data[0] = (uint8)*P1DCT_Value;
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data120ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1GCM_Data_P1GCM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData (returns application error)
 *********************************************************************************************************************/
   //Data : array with 24 Structs
   uint8 i = 0U;
   uint8 j = 0U;
   FlexibleSwitchesinFailure_T (*P1GCMarray)[24];
   P1GCMarray = (FlexibleSwitchesinFailure_T(*)[24])Rte_Pim_Pim_FlexibleSwFailureData();
   for (i = 0U; i < 24U; i++)
   {
     Data[j+0U] = (*P1GCMarray)[i].FlexibleSwitchFailureType;
     Data[j+1U] = (*P1GCMarray)[i].FlexibleSwitchID;
     Data[j+2U] = (*P1GCMarray)[i].FlexibleSwitchPosition;
     Data[j+3U] = (*P1GCMarray)[i].FlexibleSwitchPanel;
     Data[j+4U] = (*P1GCMarray)[i].LINbus;
     j= j + 5U;
   }
   
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data5ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1ILR_Data_P1ILR_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 (*FS_DisableDiagnosticsPresence)[5];
   uint8 i = 0U;
   FS_DisableDiagnosticsPresence = (uint8(*)[5])Rte_Pim_Pim_FlexibleSwDisableDiagPresence();
   for (i = 0U; i < 5U; i++)
   {
      Data[i] = (*FS_DisableDiagnosticsPresence)[i];
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: FlexibleSwitchesRouter_Ctrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ABSInhibit_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ABS_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ALD_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ASROff_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_AuxBbSwitch1_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_AuxBbSwitch2_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_AuxBbSwitch3_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_AuxBbSwitch4_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_AuxBbSwitch5_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_AuxBbSwitch6_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_BeaconSRocker_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_Beacon_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_BogieSwitch_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_BrakeBlending_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_CabTilt_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_CabWorkingLight_DevInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ChildLock_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN2_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN3_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN5_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ConstructionSwitch_DeviceInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ContUnlock_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_CraneSupply_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_DAS_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_DoorAutoFuncInd_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_DoorLockSwitch_DeviceIndic_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ESCOff_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ESC_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_EngineTmpryStopDisableDevInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_EquipmentLightInd_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_EscSwitchEnableDeviceInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_EscSwitchMuddySiteDeviceInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ExtraSideMarkers_DeviceInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_FCW_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_FPBR_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP2SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP3SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP4SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP5SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP6SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP7SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP8SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP9SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP_BSwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FerryFunction_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_FifthWheelLightInd_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_FrontDiffLock_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_FrontFog_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_FrtAxleHydro_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_HillStartAid_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_InhibRegeneration_DeviceInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_KneelDeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_LCS_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_LEDVega_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_LKSCS_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_LKS_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_MaxTract_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_MirrorHeatingDeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_PTO1_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_PTO2_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_PTO3_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_PTO4_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ParkingHeater_IndicationCmd_IndicationCmd(IndicationCmd_T *data)
 *   Std_ReturnType Rte_Read_PloughtLights_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_Ratio1_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_Ratio2_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_Ratio4_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_Ratio5_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_Ratio6_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RatioALD_DualDeviceIndication_DualDeviceIndication(DualDeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RearAxleSteeringDeviceInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RearDiffLockSwap_DeviceIndicat_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RearDiffLock_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RearFog_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ReducedSetMode_DevInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RegenerationIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_Regeneration_DeviceInd_DualDeviceIndication(DualDeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ReverseWarningInd_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RrAutoDifflck_DeviceIndication_RrAutoDifflck_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RrAxlDifflck_DeviceIndication_RrAxlDifflck_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RrWhl1Difflck_DeviceIndication_RrWhl1Difflck_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RrWhl2Difflck_DeviceIndication_RrWhl2Difflck_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_SideReverseLightInd_cmd_DualDeviceIndication(DualDeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_SideWheelLightInd_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_Slid5thWheel_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_SpecialLightSituation_ind_SpecialLightSituation_ind(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_LIN_SwcActivation_LIN(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded1_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded2_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded3_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded4_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded5_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded6_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded7_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded8_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded9_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeededB_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionResp1_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionResp2_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionResp3_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionResp4_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionResp5_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionResp6_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionResp7_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionResp8_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionResp9_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionRespB_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_TCS_DualDeviceIndication_DualDeviceIndication(DualDeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_TailLift_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_TemporaryRSLDeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_TransferCaseNeutral_DevInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_TridemALD_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_WarmUp_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_WorkingLight_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   boolean Rte_IsUpdated_SwitchDetectionResp1_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionResp2_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionResp3_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionResp4_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionResp5_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionResp6_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionResp7_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionResp8_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionResp9_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionRespB_SwitchDetectionResp(void)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ABSInhibitSwitchStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_ABSSwitchStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_AEBS_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_ALDSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_ASROffButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_AdjustFrontBeamInclination_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_AlternativeDriveLevelSw_stat_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_AuxSwitch1SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_AuxSwitch2SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_AuxSwitch3SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_AuxSwitch4SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_AuxSwitch5SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_AuxSwitch6SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_BeaconSRocker_DeviceEvent_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_Beacon_DeviceEven_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BlackOutConvoyModeSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_BrakeBlendingButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkB1ParkHeaterBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkB1ParkHeaterTempSw_stat_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_CabTilt_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_CabWorkLight_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_Construction_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_ContUnlockSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_CranePushButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_CraneSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_DAS_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_DRLandAHSinhib_stat_DRLandAHSinhib_stat(NeutralPushed_T data)
 *   Std_ReturnType Rte_Write_DashboardLockButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_ESCOff_SwitchStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_ESCReducedOff_Switch_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_EconomyPowerSwitch_status_EconomyPowerSwitch_status(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_EngineTmpryStopButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_EquipmentLight_DeviceEvent_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_EscSwitchEnableStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_EscSwitchIncDecStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_EscSwitchMuddySiteStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_EscSwitchResumeStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_ExtraMainbeam_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_ExtraSideMarkers_DeviceEvent_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_FCW_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_FPBRSwitchStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_FSP1IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP2IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP3IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP4IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP5IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP6IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP7IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP8IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP9IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP_BIndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FerryFunctionSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_FifthWheelLight_DeviceEvent_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_FlexSwitchChildLockButton_stat_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_FogLightFront_ButtonStatus_3_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_FogLightRear_ButtonStatus_3_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_FrtAxleHydro_ButtonPush_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_HillStartAidButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_InhibRegenerationSwitch_stat_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_InhibRegeneration_ButtonStat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_IntLghtActvnBtn_stat_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_IntLghtMaxModeFlxSw2_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_IntLghtNightModeBtn_stat_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_IntLghtNightModeFlxSw2_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_KneelSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LCS_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LEDVega_DeviceEvent_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_LKSCS_SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LKS_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1Switch2_Status_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LiftAxle2SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LoadingLevelSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_MirrorFoldingSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_MirrorHeatingSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_PloughLight_DeviceEvent_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_PloughtLightsPushButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_Pto1SwitchStatus_Pto1SwitchStatus(OffOn_T data)
 *   Std_ReturnType Rte_Write_Pto2SwitchStatus_Pto2SwitchStatus(OffOn_T data)
 *   Std_ReturnType Rte_Write_Pto3SwitchStatus_Pto3SwitchStatus(OffOn_T data)
 *   Std_ReturnType Rte_Write_Pto4SwitchStatus_Pto4SwitchStatus(OffOn_T data)
 *   Std_ReturnType Rte_Write_Ratio1SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_Ratio2SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_Ratio3SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_Ratio4SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_Ratio5SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_Ratio6SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_RatioALDSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_RearAxleDiffLock_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_RearAxleSteering_DeviceEvent_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_RearDifflockSwap_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_ReducedSetModeButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_Regeneration2PosSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_RegenerationPushButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_RegenerationSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_ReverseGearWarningBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_ReverseGearWarningSw_stat_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_RoofHatch_SwitchStatus_1_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_RoofHatch_SwitchStatus_2_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_RoofSignLight_DeviceEvent_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_RrAutoDifflockSwitch_stat_RrAutoDifflockSwitch_stat(OffOn_T data)
 *   Std_ReturnType Rte_Write_RrItrAxlDifflckSwitch_stat_RrItrAxlDifflckSwitch_stat(OffOn_T data)
 *   Std_ReturnType Rte_Write_RrItrWhl1DifflckSwitch_stat_RrItrWhl1DifflckSwitch_stat(OffOn_T data)
 *   Std_ReturnType Rte_Write_RrItrWhl2DifflckSwitch_stat_RrItrWhl2DifflckSwitch_stat(OffOn_T data)
 *   Std_ReturnType Rte_Write_SideReverseLight_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_SideReverseLight_SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_Slid5thWheelSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_SpotlightFront_DeviceEvent_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_SpotlightRoof_DeviceEvent_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_StatTrailerBrakeSwitchStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_StaticCornerLight_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_TailLiftPushButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_TailLiftSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_TemporaryRSLSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_ThreePosDifflockSwitch_stat_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_ThreePosFrDiffLockSwitch_stat_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_TractionControlSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_TransferCaseNeutral_SwitchStat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_TridemALDSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_TwoPosDifflockSwitch_stat_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_WarmUp_SwitchStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_WorkLight_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type(Boolean data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_UR_ANW_ASLight_InputFSP_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ASLight_InputFSP_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_AlarmSetUnset1_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_AlarmSetUnset1_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_BlackoutConvoyMode_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_BlackoutConvoyMode_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_CabTiltSwitchRequest_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_CabTiltSwitchRequest_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ExtraBBAuxiliarySwitches1_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ExtraBBAuxiliarySwitches1_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ExtraBBTailLiftFSP2_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ExtraBBTailLiftFSP2_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_FlexibleSwitchDetection_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_FlexibleSwitchDetection_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_InteriorLightsRqst1_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_InteriorLightsRqst1_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlCabRqst1_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlCabRqst1_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_PHActMaintainLiving2_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_PHActMaintainLiving2_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_RoofHatchRequest1_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_RoofHatchRequest1_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_WLight_InputFSP_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_WLight_InputFSP_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: FlexibleSwitchesRouter_Ctrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, FlexibleSwitchesRouter_Ctrl_Router_CODE) FlexibleSwitchesRouter_Ctrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
    /**********************************************************************************************************************
     * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
     * Symbol: FlexibleSwitchesRouter_Ctrl_20ms_runnable
     *********************************************************************************************************************/
     //! ###### Definition of the SWC input and ouput data structures
    static PushButtonStatus_T             RteInData_SwitchStatus[MAX_FSP_NO][MAX_SLOT_NO];
    static outputLocalstructure           FSR_output;
    static FlexibleSwitchRoutingMode_Enum smFSR = SM_FSR_Idle;
    static Boolean                        DetectionStatus = FALSE;
    static ComMode_LIN_Type               ComMode_LIN2_Prv = NotAvailable;
    static uint8                          (*FS_DisableDiagPresence)[5];
    static uint8                          (*TableOfDetectedId)[MAX_FSP_NO][MAX_SLOT_NO];
    static uint8                          RteInData_SwitchIndication[MAX_FLEXIBLESWITCHID];    // Indication 1~254
    static uint8                          FSPIndicationCmd[MAX_FSP_NO][sizeof(FSPIndicationCmdArray_T)] = {0U};
    static uint8                          FlexibleSwitchStatus[MAX_FLEXIBLESWITCHID] = {0U};
    static Boolean                        FlexibleSwPresence[MAX_FLEXIBLESWITCHID] = {FALSE}; 
    static FSR_Ctrl_Router_in_StructType  FSRCtrlRouterInData;   
    static uint8                          RteInData_SwitchDetectionResp[MAX_FSP_NO][sizeof(SwitchDetectionResp_T)];
    static uint8                          *P1DCT_data                 = 0;
    static FlexibleSwitchesinFailure_T    (*FlexibleSwFailureData)[24]  = {(FlexibleSwitchesinFailure_T(*)[24])255};
   Boolean           isDetectionCompleted    = FALSE;
   ComMode_LIN_Type  ComMode_LIN1            = NotAvailable;
   ComMode_LIN_Type  ComMode_LIN2            = NotAvailable;
   ComMode_LIN_Type  ComMode_LIN3            = NotAvailable;
   ComMode_LIN_Type  ComMode_LIN4            = NotAvailable;
   ComMode_LIN_Type  ComMode_LIN5            = NotAvailable;
   uint8             index                   = 0U;
   ComMode_LIN2_Prv = ComMode_LIN2;
   //! ###### Read ComMode_LIN% interface
   Rte_Read_ComMode_LIN1_ComMode_LIN(&ComMode_LIN1);
   Rte_Read_ComMode_LIN2_ComMode_LIN(&ComMode_LIN2);
   Rte_Read_ComMode_LIN3_ComMode_LIN(&ComMode_LIN3);
   Rte_Read_ComMode_LIN4_ComMode_LIN(&ComMode_LIN4);
   Rte_Read_ComMode_LIN5_ComMode_LIN(&ComMode_LIN5);

  P1DCT_data             = Rte_Pim_Pim_P1DCT_Info();
  FS_DisableDiagPresence = (uint8(*)[5])Rte_Pim_Pim_FlexibleSwDisableDiagPresence();
  FlexibleSwFailureData  = (FlexibleSwitchesinFailure_T(*)[24])Rte_Pim_Pim_FlexibleSwFailureData();
  TableOfDetectedId      = (uint8 (*) [MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_TableOfDetectedId();
   //! ###### Processing state transitions
   switch (smFSR)
   {
      case SM_FSR_SwitchDetectionProcessing:     
         //! ##### Process RTE read input logic: 'RteInDataRead_SwitchDetectionProcessing()'
         isDetectionCompleted = RteInDataRead_SwitchDetectionProcessing(RteInData_SwitchDetectionResp);
         //! ##### Check for switch detection is completed or not
         if (TRUE == isDetectionCompleted)
         {
            //! #### Initialize P1DCT 
            *P1DCT_data = 255U;
            for (index = 0U; index < 24U; index++)
            {
                (*FlexibleSwFailureData)[index].FlexibleSwitchFailureType = 255U;
                (*FlexibleSwFailureData)[index].FlexibleSwitchID          = 255U;
                (*FlexibleSwFailureData)[index].FlexibleSwitchPosition    = 255U;
                (*FlexibleSwFailureData)[index].FlexibleSwitchPanel       = 255U;
                (*FlexibleSwFailureData)[index].LINbus                    = 255U;
            }
            for (index = 0U; index < 5U; index++)
            {
                (*FS_DisableDiagPresence)[index] = 255U;
            }
            for (index = 0U; index < 255U; index++)
            {
                FlexibleSwPresence[index] = 0U;
            }
			             
            //! #### Process the flexible switch ID re-allocation : 'FlexibleSwitchesRouter_Ctrl_CopySwitchConfigTable()'
            FlexibleSwitchesRouter_Ctrl_CopySwitchConfigTable(RteInData_SwitchDetectionResp,
                                                              TableOfDetectedId,
                                                              FlexibleSwPresence,
                                                              P1DCT_data,
                                                              FlexibleSwFailureData);
            // To do: basic initialization, temporary behavior 
            //! #### Process initialization of output data : 'InitializeOutData()'
            InitializeOutData(FSPIndicationCmd,
                              (uint8*)&FlexibleSwitchStatus[0U], 
                              FlexibleSwPresence,
                              FALSE);
            //! ##### Process the fallback logic : 'FS_SwitchPresenceDiagnosticReport()'
            FS_SwitchPresenceDiagnosticReport(&FlexibleSwPresence[0],
                                              FlexibleSwFailureData,
                                              FS_DisableDiagPresence,
											  (uint8*)&FlexibleSwitchStatus[0U]);
            DetectionStatus = TRUE;
            smFSR           = SM_FSR_SwitchDetectionCompleted;
         }
         else
         {
            // Do nothing : wait for Switch Detection to complete
         }
      break;
      case SM_FSR_SwitchDetectionCompleted:
         //! ##### Check for communication mode of LIN2
         if ((SwitchDetection != ComMode_LIN1)
             && (SwitchDetection != ComMode_LIN2)
             && (SwitchDetection != ComMode_LIN3)
             && (SwitchDetection != ComMode_LIN4)
             && (SwitchDetection != ComMode_LIN5))
         {
            smFSR = SM_FSR_NormalRouting;
            DetectionStatus = FALSE;
         }
         else
         {
            // Do nothing:wait for COMMOde_LIN% changes from switch detection
         }
      break; 
      case SM_FSR_NormalRouting:
         //! ##### Process RTE read input common logic : 'RteInDataRead_Common()'
         RteInDataRead_Common(&FSRCtrlRouterInData);
         //! ##### Process RTE read input logic for switch status : 'RteInDataRead_FlexibleSwitchStatus()'
         RteInDataRead_FlexibleSwitchStatus(RteInData_SwitchStatus);

         //! ##### Process RTE read input logic for switch indication : 'RteInDataRead_FlexibleSwitchIndication()'
         RteInDataRead_FlexibleSwitchIndication(RteInData_SwitchIndication);
         //! ##### Process flexible switches router logic : 'FlexibleSwitchesRouter_Ctrl_StatusRouting()'
         FlexibleSwitchesRouter_Ctrl_StatusRouting(TableOfDetectedId, 
                                                   RteInData_SwitchStatus, 
                                                   (uint8*)&FlexibleSwitchStatus[0U],
                                                   RteInData_SwitchIndication,
                                                   FSPIndicationCmd);
         //! ##### Check for communication mode of LIN2 (ComMode_LIN2) status
         if ((ComMode_LIN2_Prv != ComMode_LIN2) 
            && (SwitchDetection == ComMode_LIN2))
         {
            smFSR                = SM_FSR_SwitchDetectionProcessing;
            DetectionStatus      = FALSE;
         }
         else
         {
            // Do nothing: perform routing process
         }
      break;
      default: //case SM_FSR_Idle:
        //! ##### Process initialization of output data : 'InitializeOutData()'
        InitializeOutData(FSPIndicationCmd,
                          (uint8*)&FlexibleSwitchStatus[0U],
                          FlexibleSwPresence,
                          TRUE);
        smFSR = SM_FSR_NormalRouting;
      break;
   }
   //! ###### Process the ANW core funtion logic : 'ANW_corefunctionlogic()'
   ANW_corefunctionlogic(&RteInData_SwitchIndication[180U],
                         FlexibleSwitchStatus,
                         &FSR_output,
                         &ComMode_LIN2,
                         DetectionStatus);
   //! ###### Process RTE write output logic : 'RteOutDataWrite_Common()'
   RteOutDataWrite_Common((uint8*)&FlexibleSwitchStatus[0], 
                          &FlexibleSwPresence[0],
                          &FSR_output,
                          FSPIndicationCmd);
   //! ###### Write the switch detection status
   Rte_Write_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type(DetectionStatus);
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: FlexibleSwitchesRouter_Ctrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: FlexibleSwitchesRouter_Ctrl_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, FlexibleSwitchesRouter_Ctrl_Router_CODE) FlexibleSwitchesRouter_Ctrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: FlexibleSwitchesRouter_Ctrl_Init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define FlexibleSwitchesRouter_Ctrl_Router_STOP_SEC_CODE
#include "FlexibleSwitchesRouter_Ctrl_Router_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the RteInDataRead_FlexibleSwitchStatus
//!
//! \param   pRteInData_SwitchStatus   Reads the signals of input switch status structure
//!
//!======================================================================================
static void RteInDataRead_FlexibleSwitchStatus(PushButtonStatus_T pRteInData_SwitchStatus[MAX_FSP_NO][MAX_SLOT_NO])
{
   //! ###### Processing RTE indata read ports of switch status logic
   (void)Rte_Read_FSP1SwitchStatus_FSPSwitchStatusArray(&pRteInData_SwitchStatus[FSP1][0U]);
   (void)Rte_Read_FSP2SwitchStatus_FSPSwitchStatusArray(&pRteInData_SwitchStatus[FSP2][0U]);
   (void)Rte_Read_FSP3SwitchStatus_FSPSwitchStatusArray(&pRteInData_SwitchStatus[FSP3][0U]);
   (void)Rte_Read_FSP4SwitchStatus_FSPSwitchStatusArray(&pRteInData_SwitchStatus[FSP4][0U]);
   (void)Rte_Read_FSP5SwitchStatus_FSPSwitchStatusArray(&pRteInData_SwitchStatus[FSP5][0U]);
   (void)Rte_Read_FSP6SwitchStatus_FSPSwitchStatusArray(&pRteInData_SwitchStatus[FSP6][0U]);
   (void)Rte_Read_FSP7SwitchStatus_FSPSwitchStatusArray(&pRteInData_SwitchStatus[FSP7][0U]);
   (void)Rte_Read_FSP8SwitchStatus_FSPSwitchStatusArray(&pRteInData_SwitchStatus[FSP8][0U]);
   (void)Rte_Read_FSP9SwitchStatus_FSPSwitchStatusArray(&pRteInData_SwitchStatus[FSP9][0U]);
   (void)Rte_Read_FSP_BSwitchStatus_FSPSwitchStatusArray(&pRteInData_SwitchStatus[FSPB][0U]);
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the RteInDataRead_FlexibleSwitchIndication
//!
//! \param   pRteInData_SwitchIndication   Read and update the input signals to the ports of input structure
//!
//!======================================================================================
static void RteInDataRead_FlexibleSwitchIndication(uint8 pRteInData_SwitchIndication[MAX_FLEXIBLESWITCHID])
{
   //! ###### Processing RTE read ports of device indication logic
   Rte_Read_ParkingHeater_IndicationCmd_IndicationCmd(&pRteInData_SwitchIndication[2U]);
   Rte_Read_Ratio4_DeviceIndication_DualDeviceIndication(&pRteInData_SwitchIndication[6U]);
   Rte_Read_TailLift_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[8U]); // 8, 181
   pRteInData_SwitchIndication[181U] = pRteInData_SwitchIndication[8U];
   Rte_Read_Slid5thWheel_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[9U]); // 9, 184
   pRteInData_SwitchIndication[184U] = pRteInData_SwitchIndication[9U];
   Rte_Read_ContUnlock_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[10U]);
   Rte_Read_Ratio1_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[11U]);
   Rte_Read_CabTilt_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[12U]);
   Rte_Read_ReducedSetMode_DevInd_DeviceIndication(&pRteInData_SwitchIndication[13U]); // 13, 178
   pRteInData_SwitchIndication[178U] = pRteInData_SwitchIndication[13U];
   Rte_Read_BeaconSRocker_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[16U]);
   Rte_Read_SideReverseLightInd_cmd_DualDeviceIndication(&pRteInData_SwitchIndication[21U]); // 21
   pRteInData_SwitchIndication[22U] = pRteInData_SwitchIndication[21U];
   Rte_Read_FifthWheelLightInd_cmd_DeviceIndication(&pRteInData_SwitchIndication[23U]); // 23, 200
   pRteInData_SwitchIndication[200U] = pRteInData_SwitchIndication[23U];
   Rte_Read_WorkingLight_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[24U]);
   Rte_Read_Ratio5_DeviceIndication_DualDeviceIndication(&pRteInData_SwitchIndication[25U]);
   Rte_Read_CraneSupply_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[27U]); // 27, 160
   pRteInData_SwitchIndication[160U] = pRteInData_SwitchIndication[27U];
   Rte_Read_TemporaryRSLDeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[28U]);
   Rte_Read_LKS_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[30U]); // 30, 194
   pRteInData_SwitchIndication[194U] = pRteInData_SwitchIndication[30U];
   Rte_Read_LCS_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[31U]);
   Rte_Read_FCW_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[32U]); // 32, 203
   pRteInData_SwitchIndication[203U] = pRteInData_SwitchIndication[32U];
   Rte_Read_DAS_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[33U]); // 33, 195
   pRteInData_SwitchIndication[195U] = pRteInData_SwitchIndication[33U];
   Rte_Read_RearAxleSteeringDeviceInd_DeviceIndication(&pRteInData_SwitchIndication[34U]); // 34, 187
   pRteInData_SwitchIndication[187U] = pRteInData_SwitchIndication[34U];
   Rte_Read_RearDiffLock_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[35U]); // 35, 36, 164
   pRteInData_SwitchIndication[36U] = pRteInData_SwitchIndication[35U];
   pRteInData_SwitchIndication[164U] = pRteInData_SwitchIndication[35U];
   Rte_Read_EscSwitchMuddySiteDeviceInd_DeviceIndication(&pRteInData_SwitchIndication[37U]); // 37, 168
   pRteInData_SwitchIndication[168U] = pRteInData_SwitchIndication[37U];
   Rte_Read_TCS_DualDeviceIndication_DualDeviceIndication(&pRteInData_SwitchIndication[38U]); // 38, 80
   pRteInData_SwitchIndication[80U] = pRteInData_SwitchIndication[38U];
   Rte_Read_HillStartAid_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[39U]); // 39, 190
   pRteInData_SwitchIndication[190U] = pRteInData_SwitchIndication[39U];
   Rte_Read_MaxTract_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[40U]); // 40, 43
   pRteInData_SwitchIndication[43U] = pRteInData_SwitchIndication[40U];
   Rte_Read_Ratio2_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[46U]);
   Rte_Read_RatioALD_DualDeviceIndication_DualDeviceIndication(&pRteInData_SwitchIndication[47U]);
   Rte_Read_PTO1_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[50U]); // 50, 169
   pRteInData_SwitchIndication[169U] = pRteInData_SwitchIndication[50U];
   Rte_Read_PTO2_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[51U]); // 51, 170
   pRteInData_SwitchIndication[170U] = pRteInData_SwitchIndication[51U];
   Rte_Read_PTO3_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[52U]); // 52, 171
   pRteInData_SwitchIndication[171U] = pRteInData_SwitchIndication[52U];
   Rte_Read_PTO4_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[53U]);
   Rte_Read_AuxBbSwitch1_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[54U]); // 54, 162
   pRteInData_SwitchIndication[162U] = pRteInData_SwitchIndication[54U];
   Rte_Read_AuxBbSwitch2_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[55U]); // 55, 185
   pRteInData_SwitchIndication[185U] = pRteInData_SwitchIndication[55U];
   Rte_Read_AuxBbSwitch3_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[56U]);
   Rte_Read_AuxBbSwitch4_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[57U]);
   Rte_Read_AuxBbSwitch5_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[58U]);
   Rte_Read_AuxBbSwitch6_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[59U]);
   Rte_Read_ALD_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[60U]);
   Rte_Read_KneelDeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[62U]);
   Rte_Read_FerryFunction_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[63U]);
   Rte_Read_FrontDiffLock_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[64U]);
   Rte_Read_BogieSwitch_DeviceIndication_DualDeviceIndication(&pRteInData_SwitchIndication[65U]);
   Rte_Read_InhibRegeneration_DeviceInd_DeviceIndication(&pRteInData_SwitchIndication[66U]); // 66, 213
   pRteInData_SwitchIndication[213U] = pRteInData_SwitchIndication[66U];
   (void)Rte_Read_Regeneration_DeviceInd_DualDeviceIndication(&pRteInData_SwitchIndication[67U]);
   (void)Rte_Read_RegenerationIndication_DeviceIndication(&pRteInData_SwitchIndication[70U]); // 70, 189, 214
   pRteInData_SwitchIndication[189U] = pRteInData_SwitchIndication[70U];
   pRteInData_SwitchIndication[214U] = pRteInData_SwitchIndication[70U];
   Rte_Read_TridemALD_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[71U]);
   Rte_Read_ESC_Indication_DeviceIndication(&pRteInData_SwitchIndication[75U]);
   Rte_Read_ABS_Indication_DeviceIndication(&pRteInData_SwitchIndication[76U]); // 76 ?
   Rte_Read_LKSCS_DeviceIndication_DualDeviceIndication(&pRteInData_SwitchIndication[77U]);
   Rte_Read_ChildLock_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[83U]);
   Rte_Read_EquipmentLightInd_cmd_DeviceIndication(&pRteInData_SwitchIndication[161U]);
   Rte_Read_ReverseWarningInd_cmd_DeviceIndication(&pRteInData_SwitchIndication[163U]);
   Rte_Read_ConstructionSwitch_DeviceInd_DeviceIndication(&pRteInData_SwitchIndication[165U]);
   Rte_Read_EscSwitchEnableDeviceInd_DeviceIndication(&pRteInData_SwitchIndication[166U]);
   Rte_Read_ASROff_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[174U]);
   (void)Rte_Read_BrakeBlending_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[175U]);
   Rte_Read_Beacon_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[179U]);
   Rte_Read_DoorLockSwitch_DeviceIndic_DeviceIndication(&pRteInData_SwitchIndication[180U]);
   (void)Rte_Read_ABSInhibit_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[182U]);
   Rte_Read_LEDVega_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[183U]);
   Rte_Read_PloughtLights_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[186U]);
   Rte_Read_CabWorkingLight_DevInd_DeviceIndication(&pRteInData_SwitchIndication[188U]);
   Rte_Read_FPBR_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[191U]);
   Rte_Read_TransferCaseNeutral_DevInd_DeviceIndication(&pRteInData_SwitchIndication[192U]);
   Rte_Read_Ratio6_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[193U]);
   Rte_Read_RearFog_Indication_DeviceIndication(&pRteInData_SwitchIndication[196U]);
   Rte_Read_FrontFog_Indication_DeviceIndication(&pRteInData_SwitchIndication[197U]);
   Rte_Read_FrtAxleHydro_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[198U]);
   Rte_Read_WarmUp_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[199U]);
   Rte_Read_SideWheelLightInd_cmd_DeviceIndication(&pRteInData_SwitchIndication[201U]); 
   Rte_Read_ExtraSideMarkers_DeviceInd_DeviceIndication(&pRteInData_SwitchIndication[202U]);
   (void)Rte_Read_SpecialLightSituation_ind_SpecialLightSituation_ind(&pRteInData_SwitchIndication[204U]);
   Rte_Read_MirrorHeatingDeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[205U]);
   Rte_Read_ESCOff_DeviceIndication_DeviceIndication(&pRteInData_SwitchIndication[206U]);
//   Rte_Read_DoorAutoFuncInd_cmd_DeviceIndication(&pFSRIndicationStatus[U]); // not use
//   Rte_Read_RearDiffLockSwap_DeviceIndicat_DeviceIndication(&pFSRIndicationStatus[U]); // not use
//   Rte_Read_SpecialLightSituation_ind_SpecialLightSituation_ind(&pFSRIndicationStatus[U]); // not use
   Rte_Read_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd(&pRteInData_SwitchIndication[215U]);
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the RteInDataRead_Common
//!
//! \param   *pFSRCtrlRouterInData   Reads the signals of input data structure
//!
//!======================================================================================
static void RteInDataRead_Common(FSR_Ctrl_Router_in_StructType *pFSRCtrlRouterInData)
{
   //! ###### Processing the RTE read of switch detection logic
   // Switch detection
   (void)Rte_Read_SwitchDetectionNeeded1_SwitchDetectionNeeded(&pFSRCtrlRouterInData->SwitchDetectionNeeded[FSP1]);
   (void)Rte_Read_SwitchDetectionNeeded2_SwitchDetectionNeeded(&pFSRCtrlRouterInData->SwitchDetectionNeeded[FSP2]);
   (void)Rte_Read_SwitchDetectionNeeded3_SwitchDetectionNeeded(&pFSRCtrlRouterInData->SwitchDetectionNeeded[FSP3]);
   (void)Rte_Read_SwitchDetectionNeeded4_SwitchDetectionNeeded(&pFSRCtrlRouterInData->SwitchDetectionNeeded[FSP4]);
   (void)Rte_Read_SwitchDetectionNeeded5_SwitchDetectionNeeded(&pFSRCtrlRouterInData->SwitchDetectionNeeded[FSP5]);
   (void)Rte_Read_SwitchDetectionNeeded6_SwitchDetectionNeeded(&pFSRCtrlRouterInData->SwitchDetectionNeeded[FSP6]);
   (void)Rte_Read_SwitchDetectionNeeded7_SwitchDetectionNeeded(&pFSRCtrlRouterInData->SwitchDetectionNeeded[FSP7]);
   (void)Rte_Read_SwitchDetectionNeeded8_SwitchDetectionNeeded(&pFSRCtrlRouterInData->SwitchDetectionNeeded[FSP8]);
   (void)Rte_Read_SwitchDetectionNeeded9_SwitchDetectionNeeded(&pFSRCtrlRouterInData->SwitchDetectionNeeded[FSP9]);
   (void)Rte_Read_SwitchDetectionNeededB_SwitchDetectionNeeded(&pFSRCtrlRouterInData->SwitchDetectionNeeded[FSPB]);
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the RteOutDataWrite_Common
//!
//! \param   pFlexibleSwitchStatus   Update the output data to the array
//! \param   pFlexibleSwPresence     Provides 'True' or 'False' values
//! \param   *pFSR_output            Update the output signals to ports of output structure
//! \param   pFSPIndicationCmd       Update the all FSP's indication command 
//!
//!======================================================================================
static void RteOutDataWrite_Common(const uint8                  pFlexibleSwitchStatus[255],
                                   const boolean                pFlexibleSwPresence[MAX_FLEXIBLESWITCHID],
                                         outputLocalstructure   *pFSR_output,
                                   const uint8                  pFSPIndicationCmd[MAX_FSP_NO][sizeof(FSPIndicationCmdArray_T)])
{
   //! ###### Processing the RTE write output logic
   Rte_Write_IntLghtActvnBtn_stat_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[1U]);
   Rte_Write_BunkB1ParkHeaterBtn_stat_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[2U]);
   Rte_Write_BunkB1ParkHeaterTempSw_stat_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[3U]);
   Rte_Write_RoofHatch_SwitchStatus_1_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[4U]);
   Rte_Write_RoofHatch_SwitchStatus_2_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[4U]); 
   Rte_Write_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[5U]);
   Rte_Write_Ratio4SwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[6U]);
   (void)Rte_Write_BlackOutConvoyModeSwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[7U]);
   Rte_Write_TailLiftSwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[8U]); 
   if (TRUE == pFlexibleSwPresence[9U])
   {
      Rte_Write_Slid5thWheelSwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[9U]); // same as 184
   }
   else
   {
      Rte_Write_Slid5thWheelSwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[184U]); // same as 9
   }
   Rte_Write_ContUnlockSwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[10U]);
   Rte_Write_Ratio1SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[11U]);
   Rte_Write_CabTilt_SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[12U]);
   if (TRUE == pFlexibleSwPresence[13U])
   {
      Rte_Write_ReducedSetModeButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[13U]); // same as 178
      pFSR_output->ReducedSetModeButtonStatuscurrent = pFlexibleSwitchStatus[13U];
   }
   else
   {
      Rte_Write_ReducedSetModeButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[178U]); // same as 13 
      pFSR_output->ReducedSetModeButtonStatuscurrent = pFlexibleSwitchStatus[178U];
   }
   (void)Rte_Write_ExtraMainbeam_SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[14U]);
   Rte_Write_RoofSignLight_DeviceEvent_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[15U]);   
   Rte_Write_BeaconSRocker_DeviceEvent_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[16U]);
   Rte_Write_SpotlightRoof_DeviceEvent_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[17U]);
   Rte_Write_SpotlightFront_DeviceEvent_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[18U]);
   Rte_Write_StaticCornerLight_SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[19U]);
   Rte_Write_PloughLight_DeviceEvent_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[20U]);   
   Rte_Write_SideReverseLight_SwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[21U]);
   if (TRUE == pFlexibleSwPresence[22U])
   {
      Rte_Write_SideReverseLight_ButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[22U]); // same as 201
      pFSR_output->SideReverseLight_ButtonStatuscurrent = pFlexibleSwitchStatus[22U];
   }
   else
   {
      Rte_Write_SideReverseLight_ButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[201U]); // same as 22
      pFSR_output->SideReverseLight_ButtonStatuscurrent = pFlexibleSwitchStatus[201U];
   }   
   if (TRUE == pFlexibleSwPresence[23U])
   {
      Rte_Write_FifthWheelLight_DeviceEvent_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[23U]); // same as 200
      pFSR_output->FifthWheelLight_DeviceEventcurrent = pFlexibleSwitchStatus[23U];
   }
   else
   {
      Rte_Write_FifthWheelLight_DeviceEvent_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[200U]); // same as 23
      pFSR_output->FifthWheelLight_DeviceEventcurrent = pFlexibleSwitchStatus[200U];
   } 
   if (TRUE == pFlexibleSwPresence[24U])
   {
      Rte_Write_CabWorkLight_ButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[24U]); // same as 188
      pFSR_output->WorkLight_ButtonStatuscurrent = pFlexibleSwitchStatus[24U];
   }
   else
   {
      Rte_Write_CabWorkLight_ButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[188U]); // same as 188
      pFSR_output->WorkLight_ButtonStatuscurrent = pFlexibleSwitchStatus[188U];
   }
   Rte_Write_Ratio5SwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[25U]);
   Rte_Write_IntLghtNightModeBtn_stat_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[26U]);
   Rte_Write_CraneSwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[27U]);
   Rte_Write_TemporaryRSLSwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[28U]);
   Rte_Write_ReverseGearWarningSw_stat_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[29U]);
   if (TRUE == pFlexibleSwPresence[30U])
   {
      Rte_Write_LKS_SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[30U]); // same as 194
   }
   else
   {
      Rte_Write_LKS_SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[194U]); // same as 30
   }
   Rte_Write_LCS_SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[31U]);
   Rte_Write_FCW_SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[32U]);
   if (TRUE == pFlexibleSwPresence[33U])
   {
      Rte_Write_DAS_SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[33U]); // same as 195
   }
   else
   {
      Rte_Write_DAS_SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[195U]); // same as 33
   }
   if (TRUE == pFlexibleSwPresence[34U])
   {
      Rte_Write_RearAxleSteering_DeviceEvent_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[34U]); // same as 187
   }
   else
   {
      Rte_Write_RearAxleSteering_DeviceEvent_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[187U]); // same as 34
   }
   Rte_Write_TwoPosDifflockSwitch_stat_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[35U]);
   Rte_Write_ThreePosDifflockSwitch_stat_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[36U]);
   if (TRUE == pFlexibleSwPresence[37U])
   {
      Rte_Write_EscSwitchMuddySiteStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[37U]); // same as 168
   }
   else
   {
      Rte_Write_EscSwitchMuddySiteStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[168U]); // same as 37
   }  
   if (TRUE == pFlexibleSwPresence[38U])
   {   
      Rte_Write_TractionControlSwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[38U]); // same as 80
   }
   else
   {
      Rte_Write_TractionControlSwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[80U]); // same as 38
   }
   if (TRUE == pFlexibleSwPresence[39U])
   {   
      Rte_Write_HillStartAidButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[39U]); // same as 190
   }
   else
   {
      Rte_Write_HillStartAidButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[190U]); // same as 39
   }
   Rte_Write_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[40U]);
   Rte_Write_LiftAxle1SwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[41U]);
   Rte_Write_LiftAxle2SwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[42U]);
   Rte_Write_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[43U]);
   Rte_Write_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[44U]);
   Rte_Write_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[45U]);
   Rte_Write_Ratio2SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[46U]);
   Rte_Write_RatioALDSwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[47U]);
   Rte_Write_AlternativeDriveLevelSw_stat_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[48U]);
   Rte_Write_EscSwitchIncDecStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[49U]);
   if (TRUE == pFlexibleSwPresence[50U])
   {
      Rte_Write_Pto1SwitchStatus_Pto1SwitchStatus((OffOn_T)pFlexibleSwitchStatus[50U]); // same as 169
   }
   else
   {
      Rte_Write_Pto1SwitchStatus_Pto1SwitchStatus((OffOn_T)pFlexibleSwitchStatus[169U]); // same as 50
   }
   if (TRUE == pFlexibleSwPresence[51U])
   {
      Rte_Write_Pto2SwitchStatus_Pto2SwitchStatus((OffOn_T)pFlexibleSwitchStatus[51U]); // same as 170
   }
   else
   {
      Rte_Write_Pto2SwitchStatus_Pto2SwitchStatus((OffOn_T)pFlexibleSwitchStatus[170U]); // same as 51
   }
   if (TRUE == pFlexibleSwPresence[52U])
   {
      Rte_Write_Pto3SwitchStatus_Pto3SwitchStatus((OffOn_T)pFlexibleSwitchStatus[52U]); // same as 171
   }
   else
   {
      Rte_Write_Pto3SwitchStatus_Pto3SwitchStatus((OffOn_T)pFlexibleSwitchStatus[171U]); // same as 52
   }
   Rte_Write_Pto4SwitchStatus_Pto4SwitchStatus((OffOn_T)pFlexibleSwitchStatus[53U]);
   if (TRUE == pFlexibleSwPresence[54U])
   {
      Rte_Write_AuxSwitch1SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[54U]); // same as 162
      pFSR_output->AuxSwitch1SwitchStatuscurrent = pFlexibleSwitchStatus[54U];     
   }
   else
   {
      Rte_Write_AuxSwitch1SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[162U]); // same as 54
      pFSR_output->AuxSwitch1SwitchStatuscurrent = pFlexibleSwitchStatus[162U];
   }
   if (TRUE == pFlexibleSwPresence[55U])
   { 
      Rte_Write_AuxSwitch2SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[55U]); // same as 185
      pFSR_output->AuxSwitch2SwitchStatuscurrent = pFlexibleSwitchStatus[55U];
   }
   else
   {
      Rte_Write_AuxSwitch2SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[185U]); // same as 55
      pFSR_output->AuxSwitch2SwitchStatuscurrent = pFlexibleSwitchStatus[185U];
   }
   Rte_Write_AuxSwitch3SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[56U]);
   Rte_Write_AuxSwitch4SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[57U]);
   Rte_Write_AuxSwitch5SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[58U]);
   Rte_Write_AuxSwitch6SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[59U]);
   Rte_Write_ALDSwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[60U]);
   Rte_Write_LoadingLevelSwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[61U]);
   Rte_Write_KneelSwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[62U]);
   Rte_Write_FerryFunctionSwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[63U]);
   Rte_Write_ThreePosFrDiffLockSwitch_stat_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[64U]);
   Rte_Write_LiftAxle1Switch2_Status_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[65U]);
   (void)Rte_Write_InhibRegenerationSwitch_stat_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[66U]);
   (void)Rte_Write_RegenerationSwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[67U]);
   Rte_Write_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[68U]);
   // 69 : ID reserved for Variable traction
   (void)Rte_Write_Regeneration2PosSwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[70U]);
   Rte_Write_TridemALDSwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[71U]);
   Rte_Write_MirrorFoldingSwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[73U]);
   Rte_Write_AdjustFrontBeamInclination_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[74U]);
   Rte_Write_ESCReducedOff_Switch_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[75U]);
   Rte_Write_ABSSwitchStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[76U]);
   Rte_Write_LKSCS_SwitchStatus_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[77U]);
   Rte_Write_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[81U]);
   Rte_Write_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[82U]);
   Rte_Write_FlexSwitchChildLockButton_stat_A3PosSwitchStatus((A3PosSwitchStatus_T)pFlexibleSwitchStatus[83U]);
   Rte_Write_CranePushButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[160U]);
   Rte_Write_EquipmentLight_DeviceEvent_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[161U]);
   Rte_Write_ReverseGearWarningBtn_stat_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[163U]);
   Rte_Write_RearAxleDiffLock_ButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[164U]);
   Rte_Write_Construction_SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[165U]);
   Rte_Write_EscSwitchEnableStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[166U]);
   Rte_Write_EscSwitchResumeStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[167U]);
   Rte_Write_Ratio3SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[172U]);
   (void)Rte_Write_StatTrailerBrakeSwitchStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[173U]);
   Rte_Write_ASROffButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[174U]);
   (void)Rte_Write_BrakeBlendingButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[175U]);
   Rte_Write_IntLghtNightModeFlxSw2_stat_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[176U]);
   Rte_Write_IntLghtMaxModeFlxSw2_stat_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[177U]);
   Rte_Write_Beacon_DeviceEven_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[179U]);
   Rte_Write_DashboardLockButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[180U]);
   Rte_Write_TailLiftPushButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[181U]);
   Rte_Write_ABSInhibitSwitchStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[182U]);
   Rte_Write_LEDVega_DeviceEvent_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[183U]);
   Rte_Write_PloughtLightsPushButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[186U]);
   if (TRUE == pFlexibleSwPresence[189U])
   {
      (void)Rte_Write_RegenerationPushButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[189U]); // same as 214
   }
   else
   {
      (void)Rte_Write_RegenerationPushButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[214U]);
   }
   Rte_Write_FPBRSwitchStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[191U]);
   Rte_Write_TransferCaseNeutral_SwitchStat_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[192U]);
   Rte_Write_Ratio6SwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[193U]);
   Rte_Write_FogLightRear_ButtonStatus_3_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[196U]);
   Rte_Write_FogLightFront_ButtonStatus_3_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[197U]);
   Rte_Write_FrtAxleHydro_ButtonPush_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[198U]); // 198 : FrtAxleHydroActiveButtonStatus
   Rte_Write_WarmUp_SwitchStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[199U]);
   Rte_Write_ExtraSideMarkers_DeviceEvent_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[202U]);
   Rte_Write_AEBS_ButtonStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[203U]);
   (void)Rte_Write_DRLandAHSinhib_stat_DRLandAHSinhib_stat((PushButtonStatus_T)pFlexibleSwitchStatus[204U]);
   Rte_Write_MirrorHeatingSwitchStatus_A2PosSwitchStatus((A2PosSwitchStatus_T)pFlexibleSwitchStatus[205U]);
   Rte_Write_ESCOff_SwitchStatus_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[206U]);
   (void)Rte_Write_InhibRegeneration_ButtonStat_PushButtonStatus((PushButtonStatus_T)pFlexibleSwitchStatus[213U]);
   Rte_Write_EconomyPowerSwitch_status_EconomyPowerSwitch_status((PushButtonStatus_T)pFlexibleSwitchStatus[215U]);
   (void)Rte_Write_FSP1IndicationCmd_FSPIndicationCmdArray(&pFSPIndicationCmd[FSP1][0U]);
   (void)Rte_Write_FSP2IndicationCmd_FSPIndicationCmdArray(&pFSPIndicationCmd[FSP2][0U]);
   (void)Rte_Write_FSP3IndicationCmd_FSPIndicationCmdArray(&pFSPIndicationCmd[FSP3][0U]);
   (void)Rte_Write_FSP4IndicationCmd_FSPIndicationCmdArray(&pFSPIndicationCmd[FSP4][0U]);
   (void)Rte_Write_FSP5IndicationCmd_FSPIndicationCmdArray(&pFSPIndicationCmd[FSP5][0U]);
   (void)Rte_Write_FSP6IndicationCmd_FSPIndicationCmdArray(&pFSPIndicationCmd[FSP6][0U]);
   (void)Rte_Write_FSP7IndicationCmd_FSPIndicationCmdArray(&pFSPIndicationCmd[FSP7][0U]);
   (void)Rte_Write_FSP8IndicationCmd_FSPIndicationCmdArray(&pFSPIndicationCmd[FSP8][0U]);
   (void)Rte_Write_FSP9IndicationCmd_FSPIndicationCmdArray(&pFSPIndicationCmd[FSP9][0U]);
   (void)Rte_Write_FSP_BIndicationCmd_FSPIndicationCmdArray(&pFSPIndicationCmd[FSPB][0U]);
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the InitializeOutData
//!
//! \param   pFSPIndicationCmd       Indicate the array values to off based on condition
//! \param   pFlexibleSwitchStatus   Update the array with initial data(Not Available)
//! \param   pIsFlexibleSwPresence   Check the Switch presence 
//! \param   isForced                Check for the forced intilaization
//!
//!======================================================================================
static void InitializeOutData(      uint8   pFSPIndicationCmd[MAX_FSP_NO][sizeof(FSPIndicationCmdArray_T)],
                                    uint8   pFlexibleSwitchStatus[MAX_FLEXIBLESWITCHID],
                              const Boolean pIsFlexibleSwPresence[MAX_FLEXIBLESWITCHID],
                              const Boolean isForced)
{
   uint8 fspIdx  = 0U;
   uint8 slotIdx = 0U;

   //! ###### Indicating all FSP'S indication command with 'off'
   for (fspIdx = 0U; fspIdx < MAX_FSP_NO; fspIdx++)
   {
      for (slotIdx = 0U; slotIdx < MAX_SLOT_NO; slotIdx++)
      {
         pFSPIndicationCmd[fspIdx][slotIdx] = DeviceIndication_Off;
      }
   }
   //! ###### Initialize all FSP'S switch status with 'not available'
   if (TRUE == isForced)
   {
      // Forced initialization
      for (fspIdx = 0U; fspIdx < 255U; fspIdx++)
      {
         if (TRUE == IsA3SwitchType(fspIdx))
         {
            pFlexibleSwitchStatus[fspIdx] = A3PosSwitchStatus_NotAvailable;
         }
         else
         {
            pFlexibleSwitchStatus[fspIdx] = A2PosSwitchStatus_NotAvailable;
         }     
      } 
   }
   else
   {
      // Initialization of not present/removed switches
      for (fspIdx = 0U; fspIdx < 255U; fspIdx++)
      {
         if (0 == pIsFlexibleSwPresence[fspIdx])
         {
            if (TRUE == IsA3SwitchType(fspIdx))
            {
               pFlexibleSwitchStatus[fspIdx] = A3PosSwitchStatus_NotAvailable;
            }
            else
            {
               pFlexibleSwitchStatus[fspIdx] = A2PosSwitchStatus_NotAvailable;
            }
         }
         else
         {
            // do nothing: keep the previously set status
         }
      }
   }  
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Get_RteInDataRead_SwitchDetectionProcessing
//!
//! \param   RteInData_SwitchDetectionResponse   Update and provide current value to 'Get_RteInDataRead_SwitchDetectionProcessing' function
//!
//! \return   isSwitchDetectionCompleted          Returns 'True' or 'False' value based on switch detection completion
//!
//!======================================================================================
static boolean RteInDataRead_SwitchDetectionProcessing(uint8 RteInData_SwitchDetectionResponse[MAX_FSP_NO][sizeof(SwitchDetectionResp_T)])
{
   //static uint8   retryCnt                                = 0U;
   //static uint8   receiveIndex                            = 0U;
   static uint8   DetectionProgressIndicators[MAX_FSP_NO] = { 0U };
   static uint8   SwitchDetectionTimeout                  = CONST_MaximumSwDetectionTime;
          uint8   FSP_Index                               = 0U;
          uint8   i                                       = 0U;
          boolean isSwitchDetectionCompleted              = TRUE;
          uint8   RteInData_SwitchDetectionResp_Check[MAX_FSP_NO][sizeof(SwitchDetectionResp_T)] = {{255U,255U,255U,255U,255U,255U,255U,255U},
                                                                                                    {255U,255U,255U,255U,255U,255U,255U,255U},
                                                                                                    {255U,255U,255U,255U,255U,255U,255U,255U},
                                                                                                    {255U,255U,255U,255U,255U,255U,255U,255U},
                                                                                                    {255U,255U,255U,255U,255U,255U,255U,255U},
                                                                                                    {255U,255U,255U,255U,255U,255U,255U,255U},
                                                                                                    {255U,255U,255U,255U,255U,255U,255U,255U},
                                                                                                    {255U,255U,255U,255U,255U,255U,255U,255U},
                                                                                                    {255U,255U,255U,255U,255U,255U,255U,255U},
                                                                                                    {255U,255U,255U,255U,255U,255U,255U,255U}};

   //! ###### Check for the SwitchDetectionResp[1 to B] individually and process switch detection : 'Get_RteInDataRead_SwitchDetectionSampleProcessing()'
   //! ##### Select SwitchDetectionResp1 parameter
   if (TRUE == Rte_IsUpdated_SwitchDetectionResp1_SwitchDetectionResp())
   {
      (void)Rte_Read_SwitchDetectionResp1_SwitchDetectionResp(&RteInData_SwitchDetectionResp_Check[FSP1][0U]);
      RteInDataRead_SwitchDetectionSampleProcessing(&RteInData_SwitchDetectionResp_Check[FSP1][0U],
                                                    &DetectionProgressIndicators[FSP1],
                                                    &RteInData_SwitchDetectionResponse[FSP1][0U]);
   }
   else
   {
      // Do nothing:wait for Switch Detection Response
   }
   //! ##### Select SwitchDetectionResp2 parameter
   if (TRUE == Rte_IsUpdated_SwitchDetectionResp2_SwitchDetectionResp())
   {
      (void)Rte_Read_SwitchDetectionResp2_SwitchDetectionResp(&RteInData_SwitchDetectionResp_Check[FSP2][0U]);
      RteInDataRead_SwitchDetectionSampleProcessing(&RteInData_SwitchDetectionResp_Check[FSP2][0U],
                                                    &DetectionProgressIndicators[FSP2],
                                                    &RteInData_SwitchDetectionResponse[FSP2][0U]);
   }
   else
   {
      // Do nothing:wait for Switch Detection Response
   }
   //! ##### Select SwitchDetectionResp3 parameter
   if (TRUE == Rte_IsUpdated_SwitchDetectionResp3_SwitchDetectionResp())
   {
      (void)Rte_Read_SwitchDetectionResp3_SwitchDetectionResp(&RteInData_SwitchDetectionResp_Check[FSP3][0U]);
      RteInDataRead_SwitchDetectionSampleProcessing(&RteInData_SwitchDetectionResp_Check[FSP3][0U],
                                                    &DetectionProgressIndicators[FSP3],
                                                    &RteInData_SwitchDetectionResponse[FSP3][0U]);
   }
   else
   {
      // Do nothing:wait for Switch Detection Response
   }
   //! ##### Select SwitchDetectionResp4 parameter
   if (TRUE == Rte_IsUpdated_SwitchDetectionResp4_SwitchDetectionResp())
   {
      (void)Rte_Read_SwitchDetectionResp4_SwitchDetectionResp(&RteInData_SwitchDetectionResp_Check[FSP4][0U]);
      RteInDataRead_SwitchDetectionSampleProcessing(&RteInData_SwitchDetectionResp_Check[FSP4][0U],
                                                    &DetectionProgressIndicators[FSP4],
                                                    &RteInData_SwitchDetectionResponse[FSP4][0U]);
   }
   else
   {
      // Do nothing:wait for Switch Detection Response
   }
   //! ##### Select SwitchDetectionResp5 parameter
   if (TRUE == Rte_IsUpdated_SwitchDetectionResp5_SwitchDetectionResp())
   {
      (void)Rte_Read_SwitchDetectionResp5_SwitchDetectionResp(&RteInData_SwitchDetectionResp_Check[FSP5][0U]);
      RteInDataRead_SwitchDetectionSampleProcessing(&RteInData_SwitchDetectionResp_Check[FSP5][0U],
                                                    &DetectionProgressIndicators[FSP5],
                                                    &RteInData_SwitchDetectionResponse[FSP5][0U]);
   }
   else
   {
      // Do nothing:wait for Switch Detection Response
   }
   //! ##### Select SwitchDetectionResp6 parameter
   if (TRUE == Rte_IsUpdated_SwitchDetectionResp6_SwitchDetectionResp())
   {
      (void)Rte_Read_SwitchDetectionResp6_SwitchDetectionResp(&RteInData_SwitchDetectionResp_Check[FSP6][0U]);
      RteInDataRead_SwitchDetectionSampleProcessing(&RteInData_SwitchDetectionResp_Check[FSP6][0U],
                                                    &DetectionProgressIndicators[FSP6],
                                                    &RteInData_SwitchDetectionResponse[FSP6][0U]);
   }
   else
   {
      // Do nothing:wait for Switch Detection Response
   }
   //! ##### Select SwitchDetectionResp7 parameter
   if (TRUE == Rte_IsUpdated_SwitchDetectionResp7_SwitchDetectionResp())
   {
      (void)Rte_Read_SwitchDetectionResp7_SwitchDetectionResp(&RteInData_SwitchDetectionResp_Check[FSP7][0U]);
      RteInDataRead_SwitchDetectionSampleProcessing(&RteInData_SwitchDetectionResp_Check[FSP7][0U],
                                                    &DetectionProgressIndicators[FSP7],
                                                    &RteInData_SwitchDetectionResponse[FSP7][0U]);
   }
   else
   {
      // Do nothing:wait for Switch Detection Response
   }
   //! ##### Select SwitchDetectionResp8 parameter
   if (TRUE == Rte_IsUpdated_SwitchDetectionResp8_SwitchDetectionResp())
   {
      (void)Rte_Read_SwitchDetectionResp8_SwitchDetectionResp(&RteInData_SwitchDetectionResp_Check[FSP8][0U]);
      RteInDataRead_SwitchDetectionSampleProcessing(&RteInData_SwitchDetectionResp_Check[FSP8][0U],
                                                    &DetectionProgressIndicators[FSP8],
                                                    &RteInData_SwitchDetectionResponse[FSP8][0U]);
   }
   else
   {
      // Do nothing:wait for Switch Detection Response
   }
   //! ##### Select SwitchDetectionResp9 parameter
   if (TRUE == Rte_IsUpdated_SwitchDetectionResp9_SwitchDetectionResp())
   {
      (void)Rte_Read_SwitchDetectionResp9_SwitchDetectionResp(&RteInData_SwitchDetectionResp_Check[FSP9][0U]);
      RteInDataRead_SwitchDetectionSampleProcessing(&RteInData_SwitchDetectionResp_Check[FSP9][0U],
                                                    &DetectionProgressIndicators[FSP9],
                                                    &RteInData_SwitchDetectionResponse[FSP9][0U]);
   }
   else
   {
      // Do nothing:wait for Switch Detection Response
   }
   //! ##### Select SwitchDetectionRespB parameter
   if (TRUE == Rte_IsUpdated_SwitchDetectionRespB_SwitchDetectionResp())
   {
      (void)Rte_Read_SwitchDetectionRespB_SwitchDetectionResp(&RteInData_SwitchDetectionResp_Check[FSPB][0U]);
      RteInDataRead_SwitchDetectionSampleProcessing(&RteInData_SwitchDetectionResp_Check[FSPB][0U],
                                                    &DetectionProgressIndicators[FSPB],
                                                    &RteInData_SwitchDetectionResponse[FSPB][0U]);
   }
   else
   {
      // Do nothing:wait for Switch Detection Response
   }

   // Read from RTE : need to check receive event ? 
   // also need to check no response or error status -> if error occured, retry instantly
   /*
   Rte_Read_SwitchDetectionResp2_SwitchDetectionResp(Read_SwitchDetectionResp2_SwitchDetectionResp[receiveIndex]);
   Rte_Read_SwitchDetectionResp3_SwitchDetectionResp(Read_SwitchDetectionResp3_SwitchDetectionResp[receiveIndex]);
   Rte_Read_SwitchDetectionResp4_SwitchDetectionResp(Read_SwitchDetectionResp4_SwitchDetectionResp[receiveIndex]);
   Rte_Read_SwitchDetectionResp5_SwitchDetectionResp(Read_SwitchDetectionResp5_SwitchDetectionResp[receiveIndex]);
   Rte_Read_SwitchDetectionResp6_SwitchDetectionResp(Read_SwitchDetectionResp6_SwitchDetectionResp[receiveIndex]);
   Rte_Read_SwitchDetectionResp7_SwitchDetectionResp(Read_SwitchDetectionResp7_SwitchDetectionResp[receiveIndex]);
   Rte_Read_SwitchDetectionResp8_SwitchDetectionResp(Read_SwitchDetectionResp8_SwitchDetectionResp[receiveIndex]);
   Rte_Read_SwitchDetectionResp9_SwitchDetectionResp(Read_SwitchDetectionResp9_SwitchDetectionResp[receiveIndex]);
   Rte_Read_SwitchDetectionRespB_SwitchDetectionResp(Read_SwitchDetectionRespB_SwitchDetectionResp[receiveIndex]);
*/
   
   //! ##### Check for switch detection timeout
   if (0U == SwitchDetectionTimeout)
   {
      isSwitchDetectionCompleted = TRUE;
   }
   else
   {
      SwitchDetectionTimeout = SwitchDetectionTimeout - 1U;
      
      if (SwitchDetectionTimeout < CONST_MinimumSwDetectionTime)
      {
        isSwitchDetectionCompleted = FALSE;
      }
      else
      {
         //! ##### Check for switch detection is completed or not
         isSwitchDetectionCompleted = TRUE;
         for (i = 0U; i < MAX_FSP_NO; i++)
         {
            if ((DetectionProgressIndicators[i] > 0U)
               && (DetectionProgressIndicators[i] < 5U))
            {
               isSwitchDetectionCompleted = FALSE;
            }
            else
            {
               // Do nothing: check for switch detection completion
            }
         }
      }
   }
   //! ##### Check whether switch detection is completed or not
   if (TRUE == isSwitchDetectionCompleted)
   {
      for (FSP_Index = 0U; FSP_Index < MAX_FSP_NO; FSP_Index++)
      {
         if (0U == DetectionProgressIndicators[FSP_Index])
         {
            for (i = 0U; i < MAX_SLOT_NO; i++)
            {
               RteInData_SwitchDetectionResponse[FSP_Index][i] = 255U;
            }
         }
         else
         {
            // Do nothing: Reset DetectionProgressIndicator array
         }
         DetectionProgressIndicators[FSP_Index] = 0U;
      }
      SwitchDetectionTimeout = CONST_MaximumSwDetectionTime;
   }
   else
   {
      // Do nothing:wait for switch detection completion
   }
   return isSwitchDetectionCompleted;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'RteInDataRead_SwitchDetectionSampleProcessing'
//!
//! \param   RteInData_SwitchDetectionResp_Check   Provides the present status for switch detection
//! \param   *pDetectionProgressIndicator          Indicates and update the detection progress 
//! \param   RteInData_SwitchDetectionRespns         Provides and update the switch detection response
//!
//!======================================================================================
static void RteInDataRead_SwitchDetectionSampleProcessing(const uint8   RteInData_SwitchDetectionResp_Check[sizeof(SwitchDetectionResp_T)],
                                                                uint8   *pDetectionProgressIndicator,
                                                                uint8   RteInData_SwitchDetectionRespns[sizeof(SwitchDetectionResp_T)])
{
   boolean isSwitchDetectionEqual = TRUE;
   uint8   i                      = 0U;

   //! ###### Processing Switch detection
   //! ##### Check for DetectionProgressIndicator value
   if (0U != *pDetectionProgressIndicator)
   {
      for (i = 0U; i < MAX_SLOT_NO; i++)
      {
         //! #### Compare the RteInData_SwitchDetectionResp_Check data with RteInData_SwitchDetectionRespns
         if (RteInData_SwitchDetectionRespns[i] != RteInData_SwitchDetectionResp_Check[i])
         {
            isSwitchDetectionEqual = FALSE;
         }
         else
         {
            // Do nothing: check next slot on FSP
         }
      }
      if (FALSE == isSwitchDetectionEqual)
      {
         *pDetectionProgressIndicator = 1U;
      }
      else
      {
         *pDetectionProgressIndicator = *pDetectionProgressIndicator + 1U;
      }
   }
   //! ##### Copy the RteInData_SwitchDetectionResp_Check data into RteInData_SwitchDetectionRespns
   else
   {
      for (i = 0U; i < MAX_SLOT_NO; i++)
      {
         RteInData_SwitchDetectionRespns[i] = RteInData_SwitchDetectionResp_Check[i];
      }
      *pDetectionProgressIndicator = 1U;
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the FlexibleSwitchesRouter_Ctrl_CopySwitchConfigTable
//!
//! \param   pRteInData_SwitchDetectionResp   Provides the present status of switch detection response
//! \param   *pTableOfDetectedId              Update the switch id when switch is detected
//! \param   pFlexibleSwPresence              Update the presence of switch based on conditions
//! \param   *pP1DCTdata                      Provides the switch detection response
//! \param   *pFlexibleSwitchFailData         Provide the switch failure data
//!
//!======================================================================================
static void FlexibleSwitchesRouter_Ctrl_CopySwitchConfigTable(const uint8                       pRteInData_SwitchDetectionResp[MAX_FSP_NO][sizeof(SwitchDetectionResp_T)],
                                                                    uint8                       (*pTableOfDetectedId)[MAX_FSP_NO][MAX_SLOT_NO],
                                                                    Boolean                     pFlexibleSwPresence[MAX_FLEXIBLESWITCHID],
                                                                    uint8                       *pP1DCTdata,
                                                                    FlexibleSwitchesinFailure_T (*pFlexibleSwitchFailData)[24])
{
   uint8   fspIdx                            = FSP1;
   uint8   slotIdx                           = SLOT0_UPPER;
   uint8   swIdx                             = 0U;
   Boolean isFlexSwEEPROMDetected            = FALSE;
   Boolean isNotSupportedIDDetected          = FALSE;
   Boolean isNotSupportedDoubleSwDetected    = FALSE;
   uint8   NotSupportedDoubleSwDetectedCheck = 0U;
   uint8   switchDetectionResp               = 255U;

   pFlexibleSwPresence[swIdx] = 0;
   for (fspIdx = FSP1; fspIdx < MAX_FSP_NO; fspIdx++)
   {
      for (slotIdx = SLOT0_UPPER; slotIdx < MAX_SLOT_NO; slotIdx++)
      {
         switchDetectionResp                    = pRteInData_SwitchDetectionResp[fspIdx][slotIdx];
         (*pTableOfDetectedId)[fspIdx][slotIdx] = switchDetectionResp;

         //! ###### Check if any EEPROM failure detected
         if (0U == switchDetectionResp)
         {
            isFlexSwEEPROMDetected = TRUE;
            //! ##### Process the update failure type DID logic: 'UpdateFailureTypeDID()'
            UpdateFailureTypeDID(0U,
                                 0U,
                                 slotIdx,
                                 fspIdx,
                                 pFlexibleSwitchFailData);//report : No signal, Switch ID, SWitch Position
         } 
         else
         {
            if (255U == switchDetectionResp)
            {
                //do nothing: no action
            }
            else
            {
               if (switchDetectionResp < 160U)
               {
                  if (SLOT0_LOWER <= slotIdx) // Id matching lower & upper for Rocker Switch
                  {
                     (*pTableOfDetectedId)[fspIdx][slotIdx - SLOT0_LOWER]                      = switchDetectionResp;
                     pFlexibleSwPresence[(*pTableOfDetectedId)[fspIdx][slotIdx - SLOT0_LOWER]] = pFlexibleSwPresence[(*pTableOfDetectedId)[fspIdx][slotIdx - SLOT0_LOWER]] + 1U;
                     (*pTableOfDetectedId)[fspIdx][slotIdx]                                    = 255U;
                     NotSupportedDoubleSwDetectedCheck                                         = pFlexibleSwPresence[(*pTableOfDetectedId)[fspIdx][slotIdx - SLOT0_LOWER]];
                  }
                  else
                  {
                     pFlexibleSwPresence[(*pTableOfDetectedId)[fspIdx][slotIdx]] = pFlexibleSwPresence[(*pTableOfDetectedId)[fspIdx][slotIdx]] + 1U;
                     NotSupportedDoubleSwDetectedCheck                           = pFlexibleSwPresence[(*pTableOfDetectedId)[fspIdx][slotIdx]];
                  } 
               }
               else //(switchDetectionResp < 255U)
               {
                  pFlexibleSwPresence[(*pTableOfDetectedId)[fspIdx][slotIdx]] = pFlexibleSwPresence[(*pTableOfDetectedId)[fspIdx][slotIdx]] + 1U;
                  NotSupportedDoubleSwDetectedCheck                           = pFlexibleSwPresence[(*pTableOfDetectedId)[fspIdx][slotIdx]];
               }
               
               //! ###### Check for not supported switchs
               if (((84U <= switchDetectionResp)
                  && (159U >= switchDetectionResp))
                  || (72U == switchDetectionResp)
                  || (78U == switchDetectionResp)
                  || (79U == switchDetectionResp)
                  || ((207U <= switchDetectionResp)
                  && (212U >= switchDetectionResp))
                  || ((216U <= switchDetectionResp)
                  && (254U >= switchDetectionResp)))
               {
                  isNotSupportedIDDetected = TRUE;
                  *pP1DCTdata              = switchDetectionResp;
                  //! ##### Process the update failure type DID logic : 'UpdateFailureTypeDID()'
                  UpdateFailureTypeDID(1U,
                                       switchDetectionResp,
                                       slotIdx,
                                       fspIdx,
                                       pFlexibleSwitchFailData);
               }
               else
               {
                  // Do nothing:check next panel if any not supported switch detected
               }              
               //! ##### Check if double switch is detected
               if ((1U < NotSupportedDoubleSwDetectedCheck)
                   && (switchDetectionResp != 4U))
               {
                  isNotSupportedDoubleSwDetected = TRUE;
                  *pP1DCTdata                    = switchDetectionResp;
                  //! ##### Process the update failure type DID logic : 'UpdateFailureTypeDID()'
                  UpdateFailureTypeDID(2U,
                                       switchDetectionResp,
                                       slotIdx,
                                       fspIdx,
                                       pFlexibleSwitchFailData);
               }
               else
               {
                   // Do nothing:check next panel if any double switch detected
               }
            }
         }
      }
   }
   //! ###### if EEPROM failure detected set D1BUL_31 DTC
   if (TRUE == isFlexSwEEPROMDetected)
   {
      // TODO: error indication
      (void)Rte_Call_Event_D1BUL_31_FlexSwEEPROM_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else
   {
      (void)Rte_Call_Event_D1BUL_31_FlexSwEEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   //! ###### if not supported switch detected set D1BOX_4A DTC
   if (TRUE == isNotSupportedIDDetected)
   {
      (void)Rte_Call_Event_D1BOX_4A_FS_NotSupportedID_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else
   {
      (void)Rte_Call_Event_D1BOX_4A_FS_NotSupportedID_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   //! ###### if Double switch detected set D1BOY_4A DTC
   if (TRUE == isNotSupportedDoubleSwDetected)
   {
      (void)Rte_Call_Event_D1BOY_4A_FS_NotSupportedDoubleID_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else
   {
      (void)Rte_Call_Event_D1BOY_4A_FS_NotSupportedDoubleID_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the UpdateFailureTypeDID
//!
//! \param   TypeofFailure           Providing the failure type to 'FlexibleSwitchFailureData' array
//! \param   SwitchID                Providing the switch id to 'FlexibleSwitchFailureData' array
//! \param   SlotNo                  Providing the slot number to 'FlexibleSwitchFailureData' array
//! \param   FspNo                   Providing the flexible switch panel number 
//! \param   *pFlexibleSwitchFailureData   Provide the switch failure data 
//!
//!======================================================================================
static void UpdateFailureTypeDID(const uint8                       TypeofFailure,
                                 const uint8                       SwitchID,
                                 const uint8                       SlotNo,
                                 const uint8                       FspNo,
                                       FlexibleSwitchesinFailure_T (*pFlexibleSwitchFailureData)[24])
{
   static uint8 Indx              = 0U;
   if (255U == (*pFlexibleSwitchFailureData)[0].FlexibleSwitchFailureType)
   {
     Indx = 0;
   }
   (*pFlexibleSwitchFailureData)[Indx].FlexibleSwitchFailureType = TypeofFailure;
   (*pFlexibleSwitchFailureData)[Indx].FlexibleSwitchID          = SwitchID;
   (*pFlexibleSwitchFailureData)[Indx].FlexibleSwitchPosition    = SlotNo;

    //! ###### Select the FSP(1-B) based on 'FspNo'
    switch (FspNo)
    {
       //! ##### Update FlexibleSwitchPanel and LINbus values based on FSP number
    case FSP1:
        (*pFlexibleSwitchFailureData)[Indx].FlexibleSwitchPanel = 1U;
        (*pFlexibleSwitchFailureData)[Indx].LINbus              = 1U;
       break;
    case FSP2:
        (*pFlexibleSwitchFailureData)[Indx].FlexibleSwitchPanel = 2U;
        (*pFlexibleSwitchFailureData)[Indx].LINbus              = 1U;
       break;
    case FSP3:
        (*pFlexibleSwitchFailureData)[Indx].FlexibleSwitchPanel = 1U;
        (*pFlexibleSwitchFailureData)[Indx].LINbus              = 2U;
       break;
    case FSP4:
        (*pFlexibleSwitchFailureData)[Indx].FlexibleSwitchPanel = 2U;
        (*pFlexibleSwitchFailureData)[Indx].LINbus              = 2U;
       break;
    case FSP5:
        (*pFlexibleSwitchFailureData)[Indx].FlexibleSwitchPanel = 3U;
        (*pFlexibleSwitchFailureData)[Indx].LINbus              = 2U;
       break;
    case FSP6:
        (*pFlexibleSwitchFailureData)[Indx].FlexibleSwitchPanel = 4U;
        (*pFlexibleSwitchFailureData)[Indx].LINbus              = 2U;
       break;
    case FSP7:
        (*pFlexibleSwitchFailureData)[Indx].FlexibleSwitchPanel = 1U;
        (*pFlexibleSwitchFailureData)[Indx].LINbus              = 3U;
       break;
    case FSP8:
        (*pFlexibleSwitchFailureData)[Indx].FlexibleSwitchPanel = 2U;
        (*pFlexibleSwitchFailureData)[Indx].LINbus              = 3U;
       break;
    case FSP9:
        (*pFlexibleSwitchFailureData)[Indx].FlexibleSwitchPanel = 1U;
        (*pFlexibleSwitchFailureData)[Indx].LINbus              = 4U;
       break;
    case FSPB:
        (*pFlexibleSwitchFailureData)[Indx].FlexibleSwitchPanel = 1U;
        (*pFlexibleSwitchFailureData)[Indx].LINbus              = 5U;
       break;
    default:
        (*pFlexibleSwitchFailureData)[Indx].FlexibleSwitchPanel = 255U;
        (*pFlexibleSwitchFailureData)[Indx].LINbus              = 255U;
       break;
    }
   
   if (Indx < 23U)
   {
      Indx = Indx + 1U;
   }
   else
   {
      // keep last slot: buffer is full
   }  
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the IsA3SwitchType
//!
//! \param   switchid   Provides the current switch ID
//!
//! \return   Boolean   Returns 'True' or 'False' values
//!
//! Currently, only RT variant has been applied.
//! This function should be updated in order to meet VT variant.
//!
//!======================================================================================
static Boolean IsA3SwitchType(uint8 switchid)
{
   //! ###### Check the switch is A3 type
         Boolean retValue      = FALSE;
         uint8   indexValue    = 0U;
   const uint8 A3TypeArray[30] = {3U, 4U, 5U, 6U, 7U, 21U, 25U, 36U, 38U, 40U, 
                                  41U, 42U, 43U, 44U, 45U, 47U, 48U, 49U, 61U, 64U, 
                                  65U, 67U, 68U, 73U, 74U, 77U, 80U, 81U, 82U, 83U};
   for (indexValue = 0U; indexValue < 30U; indexValue++)
   {
      //! ###### Check for switch type with switch ID
      if (A3TypeArray[indexValue] == switchid)
      {
         retValue = TRUE;
         break;
      }
      else
      {
         //wait for A3pos switch
      }
   }
   return retValue;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the FlexibleSwitchesRouter_Ctrl_StatusRouting
//!
//! \param   pTableOfDetectedId            Stores the detected switch ID while switch detection processing
//! \param   pRteInData_SwitchStatus       Provides input switch status to 'GetA3PosSwStatForRockerSw'
//! \param   pFlexibleSwitchStatus         Update the array based on switch ID
//! \param   pRteInData_SwitchIndication   Assigning  the array to 'pFSPIndicationCmd'
//! \param   pFSPIndicationCmd             Update the array as per conditions
//!
//!======================================================================================
static void FlexibleSwitchesRouter_Ctrl_StatusRouting(const uint8              (*pTableOfDetectedId)[MAX_FSP_NO][MAX_SLOT_NO],
                                                      const PushButtonStatus_T pRteInData_SwitchStatus[MAX_FSP_NO][MAX_SLOT_NO],
                                                            uint8              pFlexibleSwitchStatus[MAX_FLEXIBLESWITCHID],
                                                      const uint8              pRteInData_SwitchIndication[MAX_FLEXIBLESWITCHID],
                                                            uint8              pFSPIndicationCmd[MAX_FSP_NO][MAX_SLOT_NO])
{
   uint8               fspIdx         = 0U;
   uint8               slotIdx        = 0U;
   uint8               switchID       = 0U;
   A3PosSwitchStatus_T A3SWStatus     = A3PosSwitchStatus_Error;
   A2PosSwitchStatus_T A2SWStatus     = A2PosSwitchStatus_Error;
   Boolean             isA3Type       = FALSE;
   uint8               indicationType = LEDPosition_NotAvailable;
   PushButtonStatus_T (*RteInData_SwStatus)[MAX_FSP_NO][MAX_SLOT_NO] = { 0 };
   const  uint8     IndicationLedLocation[1U + MAX_ROCKERSWITCHID] =  // Indication Type of Rocker 1~159
{
   LEDPosition_NotAvailable, // 0U
   LEDPosition_NotAvailable, LEDPosition_Lower, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_Upper, // 1~5
   LEDPosition_Both, LEDPosition_NotAvailable, LEDPosition_Lower, LEDPosition_Lower, LEDPosition_Lower, // 6~10
   LEDPosition_Lower, LEDPosition_Lower, LEDPosition_Lower, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 11~15
   LEDPosition_Lower, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 16~20
   LEDPosition_Both, LEDPosition_Both, LEDPosition_Lower, LEDPosition_Lower, LEDPosition_Both, // 21~25
   LEDPosition_NotAvailable, LEDPosition_Lower, LEDPosition_Lower, LEDPosition_NotAvailable, LEDPosition_Lower, // 26~30
   LEDPosition_Lower, LEDPosition_Lower, LEDPosition_Lower, LEDPosition_Lower, LEDPosition_Lower, // 31~35
   LEDPosition_Lower, LEDPosition_Lower, LEDPosition_Both, LEDPosition_Lower, LEDPosition_Upper, // 36~40
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_Upper, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 41~45
   LEDPosition_Lower, LEDPosition_Both, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_Lower, // 46~50
   LEDPosition_Lower, LEDPosition_Lower, LEDPosition_Lower, LEDPosition_Lower, LEDPosition_Lower, // 51~55
   LEDPosition_Lower, LEDPosition_Lower, LEDPosition_Lower, LEDPosition_Lower, LEDPosition_Lower, // 56~60
   LEDPosition_NotAvailable, LEDPosition_Lower, LEDPosition_Lower, LEDPosition_Lower, LEDPosition_Both, // 61~65
   LEDPosition_Lower, LEDPosition_Both, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_Lower, // 66~70
   LEDPosition_Lower, LEDPosition_NotAvailable, LEDPosition_Lower, LEDPosition_NotAvailable, LEDPosition_Lower, // 71~75
   LEDPosition_Lower, LEDPosition_Both, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_Both, // 76~80
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_Lower, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 81~85
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 86~90
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 91~95
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 96~100
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 101~105
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 106~110
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 111~115
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 116~120
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 121~125
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 126~130
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 131~135
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 136~140
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 141~145
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 146~150
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, // 151~155
   LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable, LEDPosition_NotAvailable // 156~159
};
   RteInData_SwStatus = (uint8 (*) [MAX_FSP_NO][MAX_SLOT_NO]) Rte_Pim_Pim_FlexibleSwStatus();
   for (fspIdx = 0U; fspIdx < MAX_FSP_NO; fspIdx++)
   {
      for (slotIdx = 0U; slotIdx < MAX_SLOT_NO; slotIdx++)
      {
         switchID = (*pTableOfDetectedId)[fspIdx][slotIdx];
         //! ###### Check whether the switchid is neither 'Pushbutton' nor 'Rockerswitch' 
         if (255U == switchID)
         {
          (*RteInData_SwStatus)[fspIdx][slotIdx] = 255U;
         }
         //! ##### Check for switch ID is pushbutton type [160 ~ 254]
         else if (159U < switchID)
         {
            // Set status
            pFlexibleSwitchStatus[switchID] = pRteInData_SwitchStatus[fspIdx][slotIdx];
            if (0U == pFlexibleSwitchStatus[switchID])
            {
               (*RteInData_SwStatus)[fspIdx][slotIdx] = 16U;
            }
            else if (1U == pFlexibleSwitchStatus[switchID])
            {
               (*RteInData_SwStatus)[fspIdx][slotIdx] = 32U;
            }
            else
            {
               //Do nothing:no need to report for Error and NotAvailable
            }
            //! #### Set FSP indication 
            pFSPIndicationCmd[fspIdx][slotIdx] = pRteInData_SwitchIndication[switchID];
            if (0x03U == pFSPIndicationCmd[fspIdx][slotIdx])
            {
               pFSPIndicationCmd[fspIdx][slotIdx] = 0x00U;
            }
            else
            {
               //Route values for on and off indication
            }
         }
         //! ##### Check for switchid is RockerSwitch type [1~159]
         else if (0U < switchID)
         {
            //! #### Check whether the switch is A3PosSwitch (or) A2PosSwitch
            isA3Type = IsA3SwitchType(switchID);
            //! #### Based on LED position update the switch ID
            indicationType = IndicationLedLocation[switchID];
            if (slotIdx < 4U)
            {
               //! #### Process the GetA3PosSwStatForRockerSw logic : 'GetA3PosSwStatForRockerSw()'
               A3SWStatus = GetA3PosSwStatForRockerSw(pRteInData_SwitchStatus[fspIdx][slotIdx],
                                                      pRteInData_SwitchStatus[fspIdx][slotIdx + 4U]);
               //! #### Process the GetA2PosSwStatForRockerSw logic : 'GetA2PosSwStatForRockerSw()'
               A2SWStatus = GetA2PosSwStatForRockerSw(pRteInData_SwitchStatus[fspIdx][slotIdx],
                                                      pRteInData_SwitchStatus[fspIdx][slotIdx + 4U]);
               //! #### Check the LED indication position
               if (LEDPosition_Lower == indicationType)
               {
                  pFSPIndicationCmd[fspIdx][slotIdx + 4U] = pRteInData_SwitchIndication[switchID];
               }
               else if (LEDPosition_Upper == indicationType)
               {
                  pFSPIndicationCmd[fspIdx][slotIdx] = pRteInData_SwitchIndication[switchID];
               }
               else if (LEDPosition_Both == indicationType)
               {
                  pFSPIndicationCmd[fspIdx][slotIdx]      = pRteInData_SwitchIndication[switchID] & 0x3U;
                  pFSPIndicationCmd[fspIdx][slotIdx + 4U] = (pRteInData_SwitchIndication[switchID] >> 2U) & 0x3U;
               }
               else
               {
                  //LEDPosition NotAvailable or the other value
               }
            }
            else
            {
               //Routing completed for all slots
            }
            if (TRUE == isA3Type)
            {
               //! #### Update the A3 switch status if switch is A3 type
               pFlexibleSwitchStatus[switchID]        = A3SWStatus;
               (*RteInData_SwStatus)[fspIdx][slotIdx] = A3SWStatus;
            }
            else
            {
               //! #### Update the A2 switch status if switch is A2 type
               pFlexibleSwitchStatus[switchID]        = A2SWStatus;
               (*RteInData_SwStatus)[fspIdx][slotIdx] = A2SWStatus;
            }
         }
         else
         {
            // Do nothing: no signal routing. ID = 0U, error case
         }
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the GetA3PosSwStatForRockerSw
//!
//! \param   btnUpper   Specifies the present button status 
//! \param   btnLower   Specifies the present button status
//!
//! \return  A3PosSwitchStatus_T   Returns the updated value as per conditions
//!
//!======================================================================================
static A3PosSwitchStatus_T GetA3PosSwStatForRockerSw(const PushButtonStatus_T  btnUpper,
                                                     const PushButtonStatus_T  btnLower)
{
   //! ###### Processing A3Position switch status
   A3PosSwitchStatus_T result = A3PosSwitchStatus_Error;

   //! ##### Check for button lower and button upper status
   if ((PushButtonStatus_Neutral == btnUpper)
      && (PushButtonStatus_Neutral == btnLower))
   {
      result = A3PosSwitchStatus_Middle;
   }
   else if ((PushButtonStatus_Neutral == btnUpper)
           && (PushButtonStatus_Pushed == btnLower))
   {
      result = A3PosSwitchStatus_Lower;
   }
   else if ((PushButtonStatus_Pushed == btnUpper)
           && (PushButtonStatus_Neutral == btnLower))
   {
      result = A3PosSwitchStatus_Upper;
   }
   else if ((PushButtonStatus_Pushed == btnUpper)
           && (PushButtonStatus_Pushed == btnLower))
   {
      result = A3PosSwitchStatus_Error;
   }
   else if ((PushButtonStatus_NotAvailable == btnUpper) 
           && (PushButtonStatus_NotAvailable == btnLower))
   {
      result = A3PosSwitchStatus_NotAvailable;
   }
   else
   {
      result = A3PosSwitchStatus_Error;
   }
   return result;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the GetA2PosSwStatForRockerSw
//!
//! \param   btnUpper   Specifies the present button status 
//! \param   btnLower   Specifies the present button status
//!
//! \return  A3PosSwitchStatus_T   Returns the updated value as per conditions
//!
//!======================================================================================
static A2PosSwitchStatus_T GetA2PosSwStatForRockerSw(const PushButtonStatus_T  btnUpper,
                                                     const PushButtonStatus_T  btnLower)
{
   //! ###### Processing A3Position switch status
   A2PosSwitchStatus_T result = A2PosSwitchStatus_Error;

   //! ##### Check for button lower and button upper status
   if ((PushButtonStatus_Neutral == btnUpper)
      && (PushButtonStatus_Neutral == btnLower))
   {
      result = A2PosSwitchStatus_Off;
   }
   else if ((PushButtonStatus_Neutral == btnUpper)
           && (PushButtonStatus_Pushed == btnLower))
   {
      result = A2PosSwitchStatus_On;
   }
   else if ((PushButtonStatus_NotAvailable == btnUpper) 
           && (PushButtonStatus_NotAvailable == btnLower))
   {
      result = A2PosSwitchStatus_NotAvailable;
   }
   else
   {
      result = A2PosSwitchStatus_Error;
   }
   return result;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the FS_SwitchPresenceDiagnosticReport
//!
//! \param   pFlexibleSwPresence             Provides the current status of switch presence
//! \param   FlexibleSwitchFailedData        Stores data related to P1GCM
//! \param   pFS_DisableDiagnosticPresence   Stores data related to P1ILR
//! \param   pFlexibleSwitchStatus           Update the switch status according to switch type
//!
//!======================================================================================
static void FS_SwitchPresenceDiagnosticReport(const boolean                     pFlexibleSwPresence[MAX_FLEXIBLESWITCHID],
                                                    FlexibleSwitchesinFailure_T (*FlexibleSwitchFailedData)[24],
                                                    uint8                       (*pFS_DisableDiagnosticPresence)[5],
                                                    uint8                       pFlexibleSwitchStatus[MAX_FLEXIBLESWITCHID])
{
   uint8   SwitchGroup                                       = 0U;
   uint8   P1ExxPos                                          = 0U;
   uint8   SwitchId                                          = 0U;
   Boolean isMissingSwIdReport                               = FALSE;
   boolean FlexibleSwExpectation[MAX_FLEXIBLESWITCHID + 10U] = {FALSE};
   uint16  P1EAA_FS_DiagAct[26U]                             = {0U};
   uint8   DisableDiagSwIdReportPos                          = 0U;
   Boolean isA3Type                                          = FALSE;
   //! ###### Check the diagnostic for FS_DiagAct ID001 to ID254 :
   P1EAA_FS_DiagAct[0U]  = PCODE_FS_DiagAct_ID001_ID009;
   P1EAA_FS_DiagAct[1U]  = PCODE_FS_DiagAct_ID010_ID019;
   P1EAA_FS_DiagAct[2U]  = PCODE_FS_DiagAct_ID020_ID029;
   P1EAA_FS_DiagAct[3U]  = PCODE_FS_DiagAct_ID030_ID039;
   P1EAA_FS_DiagAct[4U]  = PCODE_FS_DiagAct_ID040_ID049;
   P1EAA_FS_DiagAct[5U]  = PCODE_FS_DiagAct_ID050_ID059;
   P1EAA_FS_DiagAct[6U]  = PCODE_FS_DiagAct_ID060_ID069;
   P1EAA_FS_DiagAct[7U]  = PCODE_FS_DiagAct_ID070_ID079;
   P1EAA_FS_DiagAct[8U]  = PCODE_FS_DiagAct_ID080_ID089;
   P1EAA_FS_DiagAct[9U]  = PCODE_FS_DiagAct_ID090_ID099; 
   P1EAA_FS_DiagAct[10U] = PCODE_FS_DiagAct_ID100_ID109;
   P1EAA_FS_DiagAct[11U] = PCODE_FS_DiagAct_ID110_ID119;
   P1EAA_FS_DiagAct[12U] = PCODE_FS_DiagAct_ID120_ID129;
   P1EAA_FS_DiagAct[13U] = PCODE_FS_DiagAct_ID130_ID139;
   P1EAA_FS_DiagAct[14U] = PCODE_FS_DiagAct_ID140_ID149;
   P1EAA_FS_DiagAct[15U] = PCODE_FS_DiagAct_ID150_ID159;
   P1EAA_FS_DiagAct[16U] = PCODE_FS_DiagAct_ID160_ID169;
   P1EAA_FS_DiagAct[17U] = PCODE_FS_DiagAct_ID170_ID179;
   P1EAA_FS_DiagAct[18U] = PCODE_FS_DiagAct_ID180_ID189;
   P1EAA_FS_DiagAct[19U] = PCODE_FS_DiagAct_ID190_ID199;
   P1EAA_FS_DiagAct[20U] = PCODE_FS_DiagAct_ID200_ID209;
   P1EAA_FS_DiagAct[21U] = PCODE_FS_DiagAct_ID210_ID219;  
   P1EAA_FS_DiagAct[22U] = PCODE_FS_DiagAct_ID220_ID229;
   P1EAA_FS_DiagAct[23U] = PCODE_FS_DiagAct_ID230_ID239;
   P1EAA_FS_DiagAct[24U] = PCODE_FS_DiagAct_ID240_ID249;
   P1EAA_FS_DiagAct[25U] = PCODE_FS_DiagAct_ID250_ID254;
   //! ###### stores the diagnostic data into 'FlexibleSwExpectation[]'
   for (SwitchGroup = 0U; SwitchGroup <= 25U; SwitchGroup++)
   {
      for (P1ExxPos = 1U; P1ExxPos < 11U; P1ExxPos++)
      {
         FlexibleSwExpectation[(SwitchGroup * 10U) + (P1ExxPos - 1U)] = (boolean)((P1EAA_FS_DiagAct[SwitchGroup] >> P1ExxPos) & 0x01U);
      }
   }
   /* Index position 0 is not used as there is no switch with id 0*/
   //! ###### check for diagnostic and switch presence process to updated P1GCM and P1ILR
   for (SwitchId = 1U; SwitchId < (uint8)MAX_FLEXIBLESWITCHID; SwitchId++)
   {
      if (FALSE != FlexibleSwExpectation[SwitchId])
      {
         if (0U != pFlexibleSwPresence[SwitchId])
         {
            // do nothing: switch is detected
         }
         else
         {
            //! ##### update P1GCM related to switch missing
            UpdateFailureTypeDID(3U,
                                 SwitchId,
                                 255U,
                                 255U,
                                 FlexibleSwitchFailedData);
            isMissingSwIdReport = TRUE;
            //! #### Check the type of the switch
            isA3Type = IsA3SwitchType(SwitchId);
            if (TRUE == isA3Type)
            {
               pFlexibleSwitchStatus[SwitchId] = A3PosSwitchStatus_Error;
            }
            else
            {
               pFlexibleSwitchStatus[SwitchId] = A2PosSwitchStatus_Error;
            }
         }
      }
      else
      {
         if (0U != pFlexibleSwPresence[SwitchId])
         {
            //! ##### update P1ILR related to switch missing
            if (DisableDiagSwIdReportPos < (uint8)5)
            {
               (*pFS_DisableDiagnosticPresence)[DisableDiagSwIdReportPos] = SwitchId;
               DisableDiagSwIdReportPos                                   = DisableDiagSwIdReportPos + (uint8)1;
            }
            else
            {
               // do nothing: buffer is full
            }  
         }
         else
         {
            // do nothing: nothing to report
            #if NotDuplicated
            isA3Type = IsA3SwitchType(SwitchId);
            if (TRUE == isA3Type)
            {
               pFlexibleSwitchStatus[SwitchId] = A3PosSwitchStatus_NotAvailable;
            }
            else
            {
               pFlexibleSwitchStatus[SwitchId] = A2PosSwitchStatus_NotAvailable;
            }
            #endif
         }
      }
   }
   //! ###### Atleast one switch is missing set DID
   if (TRUE == isMissingSwIdReport)
   {
      (void)Rte_Call_Event_D1BUL_95_FlexSwConfigFailure_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else
   {
      (void)Rte_Call_Event_D1BUL_95_FlexSwConfigFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
}

//!======================================================================================
//!
//! \brief
//! This function implements the logic for the ANW_corefunctionlogic
//!
//! \param   *pDoorLockSwitch_DeviceIndic   Indicates 'On' or 'Off' values
//! \param   FlexibleSwitchStat             Provides switch status of particular switch ID's
//! \param   *pFSR_output                   Providing current status of the output ports
//! \param   *pComMode_LIN2                 Provides the current status of ComMode
//! \param   DetectionStatus_switch         Provides 'True' or 'False' values
//!
//!======================================================================================
static void ANW_corefunctionlogic(const uint8                *pDoorLockSwitch_DeviceIndic,
                                  const uint8                FlexibleSwitchStat[MAX_FLEXIBLESWITCHID],
                                  const outputLocalstructure *pFSR_output,
                                  const ComMode_LIN_Type     *pComMode_LIN2,
                                  const Boolean              DetectionStatus_switch)
{
   FormalBoolean ActTrigger   = NO;
   FormalBoolean DeactTrigger = NO;

   //! ###### Process LockControlCabRqst1 activation trigger logic : 'ANW_LockControlCabRqst1ActTrigger()'
   ActTrigger   = ANW_LockControlCabRqst1ActTrigger(&FlexibleSwitchStat[180U],
                                                    pDoorLockSwitch_DeviceIndic);
   DeactTrigger = YES;
   //! ###### Process LockControlCabRqst logic : 'ANW_LockControlCabRqst()'
   ANW_LockControlCabRqst(&ActTrigger,
                          &DeactTrigger);
   //! ###### Process ASLight_InputFSP activation trigger logic : 'ANW_ASLight_InputFSPActTrigger()'
   ActTrigger = ANW_ASLight_InputFSPActTrigger(&FlexibleSwitchStat[15U],
                                               &FlexibleSwitchStat[183U],
                                               &FlexibleSwitchStat[20U],
                                               &FlexibleSwitchStat[186U]);
   DeactTrigger = YES;
   //! ###### Process ASLight_InputFSP logic : 'ANW_ASLight_InputFSP()'
   ANW_ASLight_InputFSP(&ActTrigger,
                        &DeactTrigger);
   //! ###### Process WLight_InputFSP activation trigger logic : 'ANW_WLight_InputFSPActTrigger()'
   ActTrigger = ANW_WLight_InputFSPActTrigger(&FlexibleSwitchStat[179U],
                                              &FlexibleSwitchStat[16U],
                                              &FlexibleSwitchStat[183U],
                                              &pFSR_output->WorkLight_ButtonStatuscurrent,
                                              &pFSR_output->FifthWheelLight_DeviceEventcurrent,
                                              &FlexibleSwitchStat[161U],
                                              &FlexibleSwitchStat[21U],
                                              &pFSR_output->SideReverseLight_ButtonStatuscurrent);
   DeactTrigger = YES;
   //! ###### Process WLight_InputFSP logic : 'ANW_WLight_InputFSP()'
   ANW_WLight_InputFSP(&ActTrigger,
                       &DeactTrigger);
   //! ###### Process AlarmSetUnset1 activation trigger logic : 'ANW_AlarmSetUnset1ActTrigger()'
   ActTrigger   = ANW_AlarmSetUnset1ActTrigger(&pFSR_output->ReducedSetModeButtonStatuscurrent);
   DeactTrigger = YES;
   //! ###### Process AlarmSetUnset1 logic : 'ANW_AlarmSetUnset1()'
   ANW_AlarmSetUnset1(&ActTrigger,
                      &DeactTrigger);
   //! ###### Process BlackoutConvoyMode activation trigger logic : 'ANW_BlackoutConvoyModeActTrigger()'
   ActTrigger   = ANW_BlackoutConvoyModeActTrigger(&FlexibleSwitchStat[7U]);
   DeactTrigger = YES;
   //! ###### Process BlackoutConvoyMode logic : 'ANW_BlackoutConvoyMode()'
   ANW_BlackoutConvoyMode(&ActTrigger,
                          &DeactTrigger);
   //! ###### Process CabTiltSwitchRequest activation trigger logic : 'ANW_CabTiltSwitchRequestActTrigger()'
   ActTrigger   = ANW_CabTiltSwitchRequestActTrigger(&FlexibleSwitchStat[12U]);
   DeactTrigger = YES;
   //! ###### Process CabTiltSwitchRequest logic : 'ANW_CabTiltSwitchRequest()'
   ANW_CabTiltSwitchRequest(&ActTrigger,
                            &DeactTrigger);
   //! ###### Process ExtraBBAuxiliarySwitches activation trigger logic : 'ANW_ExtraBBAuxiliarySwitchesActTrigger()'
   ActTrigger = ANW_ExtraBBAuxiliarySwitchesActTrigger(&pFSR_output->AuxSwitch1SwitchStatuscurrent,
                                                       &pFSR_output->AuxSwitch2SwitchStatuscurrent,
                                                       &FlexibleSwitchStat[56U],
                                                       &FlexibleSwitchStat[57U],
                                                       &FlexibleSwitchStat[58U],
                                                       &FlexibleSwitchStat[59U]);
   DeactTrigger = YES;
   //! ###### Process ExtraBBAuxiliarySwitches logic : 'ANW_ExtraBBAuxiliarySwitches()'
   ANW_ExtraBBAuxiliarySwitches(&ActTrigger,
                                &DeactTrigger);
   //! ###### Process ExtraBBTailLiftFSP2 activation trigger logic: 'ANW_ExtraBBTailLiftFSP2ActTrigger()'
   ActTrigger = ANW_ExtraBBTailLiftFSP2ActTrigger(&FlexibleSwitchStat[8U],
                                                  &FlexibleSwitchStat[181U],
                                                  &FlexibleSwitchStat[27U],
                                                  &FlexibleSwitchStat[160U]);
   DeactTrigger = YES;
   //! ###### Process ExtraBBTailLiftFSP2 logic : 'ANW_ExtraBBTailLiftFSP2()'
   ANW_ExtraBBTailLiftFSP2(&ActTrigger,
                           &DeactTrigger);
   //! ###### Process PHActMaintainLiving2 activation trigger logic : 'ANW_PHActMaintainLiving2ActTrigger()'
   ActTrigger = ANW_PHActMaintainLiving2ActTrigger(&FlexibleSwitchStat[2U],
                                                   &FlexibleSwitchStat[3U]);
   //! ###### Process PHActMaintainLiving2 deactivation trigger logic : 'ANW_PHActMaintainLiving2DeActTrigger()'
   DeactTrigger = ANW_PHActMaintainLiving2DeActTrigger(&FlexibleSwitchStat[3U]);
   //! ###### Process PHActMaintainLiving2 logic : 'ANW_PHActMaintainLiving2()'
   ANW_PHActMaintainLiving2(&ActTrigger,
                            &DeactTrigger);
   //! ###### Process RoofHatchRequest1 activation trigger logic : 'ANW_RoofHatchRequest1ActTrigger()'
   ActTrigger = ANW_RoofHatchRequest1ActTrigger(&FlexibleSwitchStat[4U],
                                                &FlexibleSwitchStat[4U]);
   //! ###### Process RoofHatchRequest1 deactivation trigger logic : 'ANW_RoofHatchRequest1DeActTrigger()'
   DeactTrigger = ANW_RoofHatchRequest1DeActTrigger(&FlexibleSwitchStat[4U],
                                                    &FlexibleSwitchStat[4U]);
   //! ###### Process RoofHatchRequest1 logic : 'ANW_RoofHatchRequest1()'
   ANW_RoofHatchRequest1(&ActTrigger,
                         &DeactTrigger);
   //! ###### Process flexible switch detection activation trigger logic : 'ANW_FlexibleSwitchDetectionActTrigger()'
   ActTrigger = ANW_FlexibleSwitchDetectionActTrigger(pComMode_LIN2);
   //! ###### Process flexible switch detection deactivation trigger logic: 'ANW_FlexibleSwitchDetectionDeActTrigger()'
   DeactTrigger = ANW_FlexibleSwitchDetectionDeActTrigger(pComMode_LIN2);
   //! ###### Process flexible switch detection logic : 'ANW_FlexibleSwitchDetection()'
   ANW_FlexibleSwitchDetection(&ActTrigger,
                               &DeactTrigger);
   //! ###### Process interiorLightsRqst1 activation trigger logic : 'ANW_InteriorLightsRqst1_ActTrigger()'
   ActTrigger = ANW_InteriorLightsRqst1_ActTrigger(&FlexibleSwitchStat[5U],
                                                   &FlexibleSwitchStat[1U],
                                                   &FlexibleSwitchStat[26U],
                                                   &FlexibleSwitchStat[176U], 
                                                   &FlexibleSwitchStat[177U]);
   DeactTrigger = YES;
   //! ###### Process interiorLightsRqst1 logic : 'ANW_InteriorLightsRqst1()'
   ANW_InteriorLightsRqst1(&ActTrigger,
                           &DeactTrigger);
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_LockControlCabRqst1ActTrigger
//!
//! \param   *pDashboardLockButtonStatus    Specifies the current Button status 
//! \param   *pDoorLockSwitch_DeviceIndic   Indicates 'On' or 'Off' values
//!
//! \return   FormalBoolean                 Returns Yes or No values
//!
//!======================================================================================
static FormalBoolean ANW_LockControlCabRqst1ActTrigger(const uint8   *pDashboardLockButtonStatus,
                                                       const uint8   *pDoorLockSwitch_DeviceIndic)
{
   //! ###### Processing ANW LockControlCabRqst1 activation trigger logic
   static PushButtonStatus_T  DashboardLockButtonStatusprevious;
   static DeviceIndication_T  DoorLockSwitch_DeviceIndicprevious;
          FormalBoolean       isActTriggerDetected = NO;

   //! ##### Select DashboardLock Button
   if ((DashboardLockButtonStatusprevious != *pDashboardLockButtonStatus)
      && (PushButtonStatus_Pushed == *pDashboardLockButtonStatus))
   {
      isActTriggerDetected = YES;
   }/* TO REMOVE AFTER REQ UPDATE: MOVE TO VEHICLE ACCESS CONTROL
   //! ##### Check for DoorLockSwitch DeviceIndication status
   else if ((DeviceIndication_On == DoorLockSwitch_DeviceIndicprevious)
           && (DeviceIndication_Off == *pDoorLockSwitch_DeviceIndic))
   {
      isActTriggerDetected = YES;
   }*/
   else
   {
      // No Activation trigger detected
   }
   DashboardLockButtonStatusprevious  = *pDashboardLockButtonStatus;
   //DoorLockSwitch_DeviceIndicprevious = *pDoorLockSwitch_DeviceIndic;
   return isActTriggerDetected;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_LockControlCabRqst
//!
//! \param  *pisActTriggerDetected     Provides 'yes' or 'no' values based on activation trigger
//! \param  *pisDeActTriggerDetected   Provides 'yes' or 'no' values based on deactivation trigger
//!
//!======================================================================================
static void ANW_LockControlCabRqst(const FormalBoolean  *pisActTriggerDetected,
                                   const FormalBoolean  *pisDeActTriggerDetected)
{
   static DeactivationTimer_Type LockControl_Deactivationtimer = 0U;
          FormalBoolean          isRestAct                     = NO;
   static AnwSM_States           ANW_SM_LockControlCabRqst     = { SM_ANW_Inactive };
          AnwAction_Enum         Anw_Action                    = ANW_Action_None;

   //! ###### Process ANW logic: 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(isRestAct,
                                *pisActTriggerDetected,
                                *pisDeActTriggerDetected,
                                CONST_LockControlCabRqst1,
                                &LockControl_Deactivationtimer,
                                &ANW_SM_LockControlCabRqst);
   //! ###### Check for Anw action is 'activate' or 'deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_LockControlCabRqst1_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_LockControlCabRqst1_DeactivateIss();
   }
   else
   {
      // Do nothing: keep previous state
   }
}
//!=======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_ASLight_InputFSPActTrigger
//!
//! \param    *pRoofSignLight_DeviceEvent       Specifies the current event status
//! \param    *pLEDVega_DeviceEvent             Specifies the current event status
//! \param    *pPloughLight_DeviceEvent         Specifies the current event status
//! \param    *pPloughtLightsPushButtonStatus   Provides the current button status
//!
//! \return   FormalBoolean                     Returns 'yes' or 'no' values
//!
//!======================================================================================
static FormalBoolean ANW_ASLight_InputFSPActTrigger(const uint8   *pRoofSignLight_DeviceEvent,
                                                    const uint8   *pLEDVega_DeviceEvent,
                                                    const uint8   *pPloughLight_DeviceEvent,
                                                    const uint8   *pPloughtLightsPushButtonStatus)
{
   //! ###### Processing ANW ASLight_InputFSP activation trigger logic
          FormalBoolean isActTriggerDetected = NO;
   static PushButtonStatus_T RoofSignLight_DeviceEventprevious;
   static PushButtonStatus_T LEDVega_DeviceEventprev;
   static PushButtonStatus_T PloughLight_DeviceEventprevious;
   static PushButtonStatus_T PloughtLightsPushButtonStatusprevious;

   //! ##### Select 'RoofSignLight_DeviceEvent'
   if ((RoofSignLight_DeviceEventprevious != *pRoofSignLight_DeviceEvent)
      && ((PushButtonStatus_Pushed == *pRoofSignLight_DeviceEvent)
      || (PushButtonStatus_Neutral == *pRoofSignLight_DeviceEvent)))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Select 'LEDVega_DeviceEvent'
   else if ((LEDVega_DeviceEventprev != *pLEDVega_DeviceEvent)
           && (PushButtonStatus_Pushed == *pLEDVega_DeviceEvent))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Select 'PloughLight_DeviceEvent'
   else if ((PloughLight_DeviceEventprevious != *pPloughLight_DeviceEvent)
           && ((PushButtonStatus_Pushed == *pPloughLight_DeviceEvent)
           || (PushButtonStatus_Neutral == *pPloughLight_DeviceEvent)))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Select 'PloughtLightsPushButton'
   else if ((PloughtLightsPushButtonStatusprevious != *pPloughtLightsPushButtonStatus)
           && (PushButtonStatus_Pushed == *pPloughtLightsPushButtonStatus))
   {
      isActTriggerDetected = YES;
   }
   else
   {
      //No Activation trigger detected
   }
   RoofSignLight_DeviceEventprevious     = *pRoofSignLight_DeviceEvent;
   LEDVega_DeviceEventprev               = *pLEDVega_DeviceEvent;
   PloughLight_DeviceEventprevious       = *pPloughLight_DeviceEvent;
   PloughtLightsPushButtonStatusprevious = *pPloughtLightsPushButtonStatus;

   return isActTriggerDetected;
}

//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_ASLight_InputFSP
//!
//! \param   *pisActTriggerDetected     Provides 'yes' or 'no' values based on activation trigger
//! \param   *pisDeActTriggerDetected   Provides 'yes' or 'no' values based on deactivation trigger
//!
//!======================================================================================
static void ANW_ASLight_InputFSP(const FormalBoolean  *pisActTriggerDetected,
                                 const FormalBoolean  *pisDeActTriggerDetected)
{
   static DeactivationTimer_Type ASLight_Deactivationtimer   = 0U;
          FormalBoolean          isRestAct                   = NO;
   static AnwSM_States           ANW_SM_ASLightActDeactivation;
          AnwAction_Enum         Anw_Action                  = ANW_Action_None;

   //! ###### Process ANW logic: 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(isRestAct,
                                *pisActTriggerDetected,
                                *pisDeActTriggerDetected,
                                CONST_ASLight_InputFSP,
                                &ASLight_Deactivationtimer,
                                &ANW_SM_ASLightActDeactivation);
   //! ###### Check for Anw action is 'activate' or 'deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_ASLight_InputFSP_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_ASLight_InputFSP_DeactivateIss();
   }
   else
   {
      // Do nothing: keep previous state
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_WLight_InputFSPActTrigger
//!
//! \param    *pBeacon_DeviceEven               Specifies the current button status
//! \param    *pBeaconSRocker_DeviceEvent       Indicates 'On' or 'Off' values 
//! \param    *pLEDVega_DeviceEvent             Specifies the current button status
//! \param    *pWorkLight_ButtonStatus          Specifies the current button status
//! \param    *pFifthWheelLight_DeviceEvent     Specifies the current button status
//! \param    *pEquipmentLight_DeviceEvent      Specifies the current button status
//! \param    *pSideReverseLight_SwitchStatus   Indicates the present switch status
//! \param    *pSideReverseLight_ButtonStatus   Specifies the current button status
//!
//! \return   FormalBoolean                     Return 'yes' or 'no' values
//!
//!======================================================================================
static FormalBoolean ANW_WLight_InputFSPActTrigger(const uint8                *pBeacon_DeviceEven,
                                                   const uint8                *pBeaconSRocker_DeviceEvent,
                                                   const uint8                *pLEDVega_DeviceEvent,
                                                   const PushButtonStatus_T   *pWorkLight_ButtonStatus,
                                                   const PushButtonStatus_T   *pFifthWheelLight_DeviceEvent,
                                                   const uint8                *pEquipmentLight_DeviceEvent,
                                                   const uint8                *pSideReverseLight_SwitchStatus,
                                                   const PushButtonStatus_T   *pSideReverseLight_ButtonStatus)
{
   //! ###### Processing ANW WLight_InputFSP activation trigger logic
   static  PushButtonStatus_T   Beacon_DeviceEvenprevious;
   static  A2PosSwitchStatus_T  BeaconSRocker_DeviceEventprevious;
   static  PushButtonStatus_T   WorkLight_ButtonStatusprevious;
   static  PushButtonStatus_T   FifthWheelLight_DeviceEventprevious;
   static  PushButtonStatus_T   EquipmentLight_DeviceEventprevious;
   static  A3PosSwitchStatus_T  SideReverseLight_SwitchStatusprevious;
   static  PushButtonStatus_T   SideReverseLight_ButtonStatusprevious;
   static  PushButtonStatus_T   LEDVega_DeviceEventprevious;
           FormalBoolean        isActTriggerDetected = NO;

   //! ##### Select 'Beacon_DeviceEven'
   if ((Beacon_DeviceEvenprevious != *pBeacon_DeviceEven)
      && (PushButtonStatus_Pushed == *pBeacon_DeviceEven))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Select 'BeaconSRocker_DeviceEvent'
   else if ((BeaconSRocker_DeviceEventprevious != *pBeaconSRocker_DeviceEvent)
           && (A2PosSwitchStatus_On == *pBeaconSRocker_DeviceEvent))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Select 'LEDVega_DeviceEvent'
   else if ((LEDVega_DeviceEventprevious != *pLEDVega_DeviceEvent)
           && (PushButtonStatus_Pushed == *pLEDVega_DeviceEvent))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Select 'WorkLight_ButtonStatus'
   else if ((WorkLight_ButtonStatusprevious != *pWorkLight_ButtonStatus)
           && (PushButtonStatus_Pushed == *pWorkLight_ButtonStatus))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Select 'FifthWheelLight_DeviceEvent'
   else if ((FifthWheelLight_DeviceEventprevious != *pFifthWheelLight_DeviceEvent)
           && (PushButtonStatus_Pushed == *pFifthWheelLight_DeviceEvent))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Select 'EquipmentLight_DeviceEvent'
   else if ((EquipmentLight_DeviceEventprevious != *pEquipmentLight_DeviceEvent)
           && (PushButtonStatus_Pushed == *pEquipmentLight_DeviceEvent))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Check for 'SideReverseLight_Switch' status
   else if ((SideReverseLight_SwitchStatusprevious != *pSideReverseLight_SwitchStatus)
           && ((A3PosSwitchStatus_Lower == *pSideReverseLight_SwitchStatus)
           || (A3PosSwitchStatus_Middle == *pSideReverseLight_SwitchStatus)
           || (A3PosSwitchStatus_Upper == *pSideReverseLight_SwitchStatus)))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Select 'SideReverseLight_Button'
   else if ((SideReverseLight_ButtonStatusprevious != *pSideReverseLight_ButtonStatus)
           && (PushButtonStatus_Pushed == *pSideReverseLight_ButtonStatus))
   {
      isActTriggerDetected = YES;
   }
   else
   {
      //No Activation trigger detected
   }
   BeaconSRocker_DeviceEventprevious     = *pBeaconSRocker_DeviceEvent;
   Beacon_DeviceEvenprevious             = *pBeacon_DeviceEven;
   WorkLight_ButtonStatusprevious        = *pWorkLight_ButtonStatus;
   SideReverseLight_ButtonStatusprevious = *pSideReverseLight_ButtonStatus;
   SideReverseLight_SwitchStatusprevious = *pSideReverseLight_SwitchStatus;
   EquipmentLight_DeviceEventprevious    = *pEquipmentLight_DeviceEvent;
   LEDVega_DeviceEventprevious           = *pLEDVega_DeviceEvent;
   FifthWheelLight_DeviceEventprevious   = *pFifthWheelLight_DeviceEvent;

   return isActTriggerDetected;
}

//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_WLight_InputFSP
//!
//! \param    *pisActTriggerDetected     Provides 'yes' or 'no' values based on activation trigger
//! \param    *pisDeActTriggerDetected   Provides 'yes' or 'no' values based on deactivation trigger
//!
//!======================================================================================
static void ANW_WLight_InputFSP(const FormalBoolean  *pisActTriggerDetected,
                                const FormalBoolean  *pisDeActTriggerDetected)
{
   static DeactivationTimer_Type WLight_Deactivationtimer  = 0U;
          FormalBoolean          isRestAct                 = NO;
   static AnwSM_States           ANW_SM_WLightActDeactivation;
          AnwAction_Enum         Anw_Action                = ANW_Action_None;
   //! ###### Process ANW logic :'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(isRestAct,
                                *pisActTriggerDetected,
                                *pisDeActTriggerDetected,
                                CONST_WLight_InputFSP,
                                &WLight_Deactivationtimer,
                                &ANW_SM_WLightActDeactivation);
   //! ###### Check for Anw action is 'activate' or 'deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_WLight_InputFSP_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_WLight_InputFSP_DeactivateIss();
   }
   else
   {
      // Do nothing: keep previous state
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_AlarmSetUnset1ActTrigger
//!
//! \param    *pReducedSetModeButtonStatus   Specifies the current button status
//!
//! \return   FormalBoolean                  Return 'yes' or 'no' values
//!
//!======================================================================================
static FormalBoolean ANW_AlarmSetUnset1ActTrigger(const PushButtonStatus_T   *pReducedSetModeButtonStatus)
{
   //! ###### Processing ANW AlarmSetUnset1 activation trigger logic
   static PushButtonStatus_T   ReducedSetModeButtonStatusprevious;
          FormalBoolean        isActTriggerDetected = NO;

   //! ##### Select 'ReducedSetModeButton'
   if ((ReducedSetModeButtonStatusprevious != *pReducedSetModeButtonStatus)
      && (PushButtonStatus_Pushed == *pReducedSetModeButtonStatus))
   {
      isActTriggerDetected = YES;
   }
   else
   {
      //No Activation trigger detected
   }
   ReducedSetModeButtonStatusprevious = *pReducedSetModeButtonStatus;
   return isActTriggerDetected;
}

//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_AlarmSetUnset1
//!
//! \param    *pisActTriggerDetected     Provides 'yes' or 'no' based on activation trigger
//! \param    *pisDeActTriggerDetected   Provides 'yes' or 'no' based on deactivation trigger
//!
//!======================================================================================
static void ANW_AlarmSetUnset1(const FormalBoolean  *pisActTriggerDetected,
                               const FormalBoolean  *pisDeActTriggerDetected)
{
   static DeactivationTimer_Type AlarmSetUnset_Deactivationtimer  = 0U;
          FormalBoolean     isRestAct                             = NO;
   static AnwSM_States      ANW_SM_AlarmActDeactivation;
          AnwAction_Enum    Anw_Action                            = ANW_Action_None;

   //! ###### Process ANW logic: 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(isRestAct,
                                *pisActTriggerDetected,
                                *pisDeActTriggerDetected,
                                CONST_AlarmSetUnset1,
                                &AlarmSetUnset_Deactivationtimer,
                                &ANW_SM_AlarmActDeactivation);
  //! ###### Check for Anw action is 'activate' or 'deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_AlarmSetUnset1_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_AlarmSetUnset1_DeactivateIss();
   }
   else
   {
      // Do nothing: keep previous state
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_BlackoutConvoyModeActTrigger
//!
//! \param    *pBlackOutConvoyModeSwitchStatus   Specifies the current switch status
//!
//! \return   FormalBoolean                      Return 'yes' or 'no' values
//!
//!======================================================================================
static FormalBoolean ANW_BlackoutConvoyModeActTrigger(const uint8   *pBlackOutConvoyModeSwitchStatus)
{
   //! ###### Processing ANW BlackoutConvoyMode activation trigger logic
   static A3PosSwitchStatus_T  BlackOutConvoyModeSwitchStatusprevious = A3PosSwitchStatus_NotAvailable;
          FormalBoolean        isActTriggerDetected = NO;

   //! ##### Select 'BlackOutConvoyModeSwitch'
   if ((BlackOutConvoyModeSwitchStatusprevious != *pBlackOutConvoyModeSwitchStatus)
      && ((A3PosSwitchStatus_Middle != *pBlackOutConvoyModeSwitchStatus)
      && (A3PosSwitchStatus_Lower != *pBlackOutConvoyModeSwitchStatus)
      && (A3PosSwitchStatus_Upper != *pBlackOutConvoyModeSwitchStatus)))
   {
      isActTriggerDetected = YES;
   }
   else
   {
      //No Activation trigger detected
   }
   BlackOutConvoyModeSwitchStatusprevious = *pBlackOutConvoyModeSwitchStatus;
   return isActTriggerDetected;
}

//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the ANW_BlackoutConvoyMode
//!
//! \param   *pisActTriggerDetected     Provides 'yes' or 'no' values based on activation trigger
//! \param   *pisDeActTriggerDetected   Provides 'yes' or 'no' values based on deactivation trigger
//!
//!======================================================================================
static void ANW_BlackoutConvoyMode(const FormalBoolean  *pisActTriggerDetected,
                                   const FormalBoolean  *pisDeActTriggerDetected)
{
   static DeactivationTimer_Type Blackout_Deactivationtimer = 0U;
          FormalBoolean          isRestAct                  = NO;
   static AnwSM_States           ANW_SM_BlackoutActDeactivation;
          AnwAction_Enum         Anw_Action                 = ANW_Action_None;

   //! ###### Process ANW logic: 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(isRestAct,
                                *pisActTriggerDetected,
                                *pisDeActTriggerDetected,
                                CONST_BlackoutConvoyMode,
                                &Blackout_Deactivationtimer,
                                &ANW_SM_BlackoutActDeactivation);
   //! ###### Check for Anw action is 'activate' or 'deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_BlackoutConvoyMode_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_BlackoutConvoyMode_DeactivateIss();
   }
   else
   {
      // Do nothing: keep previous state
   }
}
//!=======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_CabTiltSwitchRequestActTrigger
//!
//! \param    *pCabTilt_SwitchStatus   Indicates the current switch status of Cab_Tilt
//!
//! \return   FormalBoolean            Returns 'yes' or 'no'
//!
//!======================================================================================
static FormalBoolean ANW_CabTiltSwitchRequestActTrigger(const uint8  *pCabTilt_SwitchStatus)
{
   //! ###### Processing ANW CabTiltSwitchRequest activation trigger logic
   static A2PosSwitchStatus_T  CabTilt_SwitchStatusprevious;
          FormalBoolean        isActTriggerDetected = NO;

   //! ##### Select 'CabTilt_Switch'
   if ((CabTilt_SwitchStatusprevious != *pCabTilt_SwitchStatus)
      && ((A2PosSwitchStatus_Off == *pCabTilt_SwitchStatus)
      || (A2PosSwitchStatus_On == *pCabTilt_SwitchStatus)))
   {
      isActTriggerDetected = YES;
   }
   else
   {
      //No Activation trigger detected
   }
   CabTilt_SwitchStatusprevious = *pCabTilt_SwitchStatus;
   return isActTriggerDetected;
}

//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the ANW_CabTiltSwitchRequest
//!
//! \param    *pisActTriggerDetected     Provides 'yes' or 'no' values based on activation trigger
//! \param    *pisDeActTriggerDetected   Provides 'yes' or 'no' values based on deactivation trigger
//!
//!======================================================================================
static void ANW_CabTiltSwitchRequest(const FormalBoolean  *pisActTriggerDetected,
                                     const FormalBoolean  *pisDeActTriggerDetected)
{
   static DeactivationTimer_Type CabTilt_Deactivationtimer  = 0U;
          FormalBoolean          isRestAct                  = NO;
   static AnwSM_States           ANW_SM_CabTiltActDeactivation;
          AnwAction_Enum         Anw_Action                 = ANW_Action_None;

   //! ###### Process ANW logic: 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(isRestAct,
                                *pisActTriggerDetected,
                                *pisDeActTriggerDetected,
                                CONST_CabTiltSwitchRequest,
                                &CabTilt_Deactivationtimer,
                                &ANW_SM_CabTiltActDeactivation);
  //! ###### Check for Anw action is 'activate' or 'deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_CabTiltSwitchRequest_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_CabTiltSwitchRequest_DeactivateIss();
   }
   else
   {
      // Do nothing: keep previous state
   }
}
//!=======================================================================================
//!
//! \brief 
//! This function implements the logic for the ANW_ExtraBBAuxiliarySwitchesActTrigger
//!
//! \param   *pAuxSwitch1SwitchStatus   Indicates the current switch status of Auxiliary_switch1
//! \param   *pAuxSwitch2SwitchStatus   Indicates the current switch status of Auxiliary_switch2
//! \param   *pAuxSwitch3SwitchStatus   Indicates the current switch status of Auxiliary_switch3
//! \param   *pAuxSwitch4SwitchStatus   Indicates the current switch status of Auxiliary_switch4
//! \param   *pAuxSwitch5SwitchStatus   Indicates the current switch status of Auxiliary_switch5
//! \param   *pAuxSwitch6SwitchStatus   Indicates the current switch status of Auxiliary_switch6
//!
//! \return   FormalBoolean              Return 'yes' or 'no' values
//!
//!======================================================================================
static FormalBoolean ANW_ExtraBBAuxiliarySwitchesActTrigger(const A2PosSwitchStatus_T  *pAuxSwitch1SwitchStatus,
                                                            const A2PosSwitchStatus_T  *pAuxSwitch2SwitchStatus,
                                                            const uint8                *pAuxSwitch3SwitchStatus,
                                                            const uint8                *pAuxSwitch4SwitchStatus,
                                                            const uint8                *pAuxSwitch5SwitchStatus,
                                                            const uint8                *pAuxSwitch6SwitchStatus)
{
   //! ###### Processing ANW ExtraBBAuxiliarySwitches activation trigger logic
   static A2PosSwitchStatus_T  AuxSwitch1SwitchStatusprevious;
   static A2PosSwitchStatus_T  AuxSwitch2SwitchStatusprevious;
   static A2PosSwitchStatus_T  AuxSwitch3SwitchStatusprevious;
   static A2PosSwitchStatus_T  AuxSwitch4SwitchStatusprevious;
   static A2PosSwitchStatus_T  AuxSwitch5SwitchStatusprevious;
   static A2PosSwitchStatus_T  AuxSwitch6SwitchStatusprevious;
          FormalBoolean        isActTriggerDetected = NO;

   //! ##### Select 'AuxSwitch1Switch'
   if ((AuxSwitch1SwitchStatusprevious != *pAuxSwitch1SwitchStatus)
      && ((A2PosSwitchStatus_Off == *pAuxSwitch1SwitchStatus)
      || (A2PosSwitchStatus_On == *pAuxSwitch1SwitchStatus)))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Select 'AuxSwitch2Switch'
   else if ((AuxSwitch2SwitchStatusprevious != *pAuxSwitch2SwitchStatus)
           && ((A2PosSwitchStatus_Off == *pAuxSwitch2SwitchStatus)
           || (A2PosSwitchStatus_On == *pAuxSwitch2SwitchStatus)))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Select 'AuxSwitch3Switch'
   else if ((AuxSwitch3SwitchStatusprevious != *pAuxSwitch3SwitchStatus)
           && ((A2PosSwitchStatus_Off == *pAuxSwitch3SwitchStatus)
           || (A2PosSwitchStatus_On == *pAuxSwitch3SwitchStatus)))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Select 'AuxSwitch4Switch'
   else if ((AuxSwitch4SwitchStatusprevious != *pAuxSwitch4SwitchStatus)
           && ((A2PosSwitchStatus_Off == *pAuxSwitch4SwitchStatus)
           || (A2PosSwitchStatus_On == *pAuxSwitch4SwitchStatus)))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Select 'AuxSwitch5Switch'
   else if ((AuxSwitch5SwitchStatusprevious != *pAuxSwitch5SwitchStatus)
           && ((A2PosSwitchStatus_Off == *pAuxSwitch5SwitchStatus)
           || (A2PosSwitchStatus_On == *pAuxSwitch5SwitchStatus)))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Select 'AuxSwitch6Switch'
   else if ((AuxSwitch6SwitchStatusprevious != *pAuxSwitch6SwitchStatus)
           && ((A2PosSwitchStatus_Off == *pAuxSwitch6SwitchStatus)
           || (A2PosSwitchStatus_On == *pAuxSwitch6SwitchStatus)))
   {
      isActTriggerDetected = YES;
   }
   else
   {
      //No Activation trigger detected
   }
   AuxSwitch1SwitchStatusprevious = *pAuxSwitch1SwitchStatus;
   AuxSwitch2SwitchStatusprevious = *pAuxSwitch2SwitchStatus;
   AuxSwitch3SwitchStatusprevious = *pAuxSwitch3SwitchStatus;
   AuxSwitch4SwitchStatusprevious = *pAuxSwitch4SwitchStatus;
   AuxSwitch5SwitchStatusprevious = *pAuxSwitch5SwitchStatus;
   AuxSwitch6SwitchStatusprevious = *pAuxSwitch6SwitchStatus;

   return isActTriggerDetected;
}

//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the ANW_ExtraBBAuxiliarySwitches
//!
//! \param    *pisActTriggerDetected     Provides 'yes' or 'no' values based on activation trigger
//! \param    *pisDeActTriggerDetected   Provides 'yes' or 'no' values based on deactivation trigger
//!
//!======================================================================================
static void ANW_ExtraBBAuxiliarySwitches(const FormalBoolean  *pisActTriggerDetected,
                                         const FormalBoolean  *pisDeActTriggerDetected)
{
   static DeactivationTimer_Type ExtraBBAux_Deactivationtimer  = 0U;
          FormalBoolean          isRestAct                     = NO;
   static AnwSM_States           ANW_SM_AuxiliaryActDeactivation;
          AnwAction_Enum         Anw_Action                    = ANW_Action_None;

   //! ###### Process ANW logic: 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(isRestAct,
                                *pisActTriggerDetected,
                                *pisDeActTriggerDetected,
                                CONST_ExtraBBAuxiliarySwitches,
                                &ExtraBBAux_Deactivationtimer,
                                &ANW_SM_AuxiliaryActDeactivation);
   //! ###### Check for Anw action is 'activate' or 'deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_ExtraBBAuxiliarySwitches1_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_ExtraBBAuxiliarySwitches1_DeactivateIss();
   }
   else
   {
      // Do nothing: keep previous state
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_ExtraBBTailLiftFSP2ActTrigger
//!
//! \param    *pTailLiftSwitchStatus       Indicates 'On' or 'Off' values
//! \param    *pTailLiftPushButtonStatus   Specifies the current PushButton status
//! \param    *pCraneSwitchStatus          Indicates 'On' or 'Off' values
//! \param    *pCranePushButtonStatus      Specifies the current PushButton status
//!
//! \return   FormalBoolean                Return 'yes' or 'no' values
//!
//!======================================================================================
static FormalBoolean ANW_ExtraBBTailLiftFSP2ActTrigger(const uint8  *pTailLiftSwitchStatus,
                                                       const uint8  *pTailLiftPushButtonStatus,
                                                       const uint8  *pCraneSwitchStatus,
                                                       const uint8  *pCranePushButtonStatus)
{
   //! ###### Processing ANW ExtraBBTailLiftFSP2 activation trigger logic
   static A2PosSwitchStatus_T  TailLiftSwitchStatusprevious;
   static PushButtonStatus_T   TailLiftPushButtonStatusprevious;
   static A2PosSwitchStatus_T  CraneSwitchStatusprevious;
   static PushButtonStatus_T   CranePushButtonStatusprevious;
          FormalBoolean        isActTriggerDetected = NO;

   //! ##### Check for TailLiftSwitch status
   if ((TailLiftSwitchStatusprevious != *pTailLiftSwitchStatus)
      && ((A2PosSwitchStatus_Off == *pTailLiftSwitchStatus)
      || (A2PosSwitchStatus_On == *pTailLiftSwitchStatus)))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Check for TailLiftPushButton status
   else if ((TailLiftPushButtonStatusprevious != *pTailLiftPushButtonStatus)
           && (PushButtonStatus_Pushed == *pTailLiftPushButtonStatus))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Check for CraneSwitch status
   else if ((CraneSwitchStatusprevious != *pCraneSwitchStatus)
           && ((A2PosSwitchStatus_Off == *pCraneSwitchStatus)
           || (A2PosSwitchStatus_On == *pCraneSwitchStatus)))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Check for CranePushButton status
   else if ((CranePushButtonStatusprevious != *pCranePushButtonStatus)
           && (PushButtonStatus_Pushed == *pCranePushButtonStatus))
   {
      isActTriggerDetected = YES;
   }
   else
   {
      //No Activation trigger detected
   }
   TailLiftSwitchStatusprevious     = *pTailLiftSwitchStatus;
   TailLiftPushButtonStatusprevious = *pTailLiftPushButtonStatus;
   CraneSwitchStatusprevious        = *pCraneSwitchStatus;
   CranePushButtonStatusprevious    = *pCranePushButtonStatus;

   return isActTriggerDetected;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_ExtraBBTailLiftFSP2
//!
//! \param    *pisActTriggerDetected     Provides 'yes' or 'no' values based on activation trigger
//! \param    *pisDeActTriggerDetected   Provides 'yes' or 'no' values based on deactivation trigger
//!
//!======================================================================================
static void ANW_ExtraBBTailLiftFSP2(const FormalBoolean  *pisActTriggerDetected,
                                    const FormalBoolean  *pisDeActTriggerDetected)
{
   static DeactivationTimer_Type ExtraBBTail_Deactivationtimer = 0U;
          FormalBoolean          isRestAct                     = NO;
   static AnwSM_States           ANW_SM_TailLiftActDeactivation;
   AnwAction_Enum                Anw_Action                    = ANW_Action_None;

   //! ###### Process ANW logic: 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(isRestAct,
                                *pisActTriggerDetected,
                                *pisDeActTriggerDetected,
                                CONST_ExtraBBTailLiftFSP2,
                                &ExtraBBTail_Deactivationtimer,
                                &ANW_SM_TailLiftActDeactivation);
   //! ###### Check for Anw action is 'activate' or 'deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_ExtraBBTailLiftFSP2_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_ExtraBBTailLiftFSP2_DeactivateIss();
   }
   else
   {
      // Do nothing: keep previous state
   }
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the ANW_PHActMaintainLiving2ActTrigger
//!
//! \param    *pBunkB1ParkHeaterBtn_stat      Specifies the current push button status
//! \param    *pBunkB1ParkHeaterTempSw_stat   Indicates the current switch status
//!
//! \return   FormalBoolean                   Return 'yes' or 'no' values
//!
//!======================================================================================
static FormalBoolean ANW_PHActMaintainLiving2ActTrigger(const uint8   *pBunkB1ParkHeaterBtn_stat,
                                                        const uint8   *pBunkB1ParkHeaterTempSw_stat)
{
   //! ###### Processing ANW PHActMaintainLiving2 activation trigger logic
   static PushButtonStatus_T  BunkB1ParkHeaterBtn_statprevious;
   static A3PosSwitchStatus_T BunkB1ParkHeaterTempSw_statprevious;
          FormalBoolean       isActTriggerDetected = NO;

   //! ##### Select 'BunkB1ParkHeaterBtn_stat'
   if ((BunkB1ParkHeaterBtn_statprevious != *pBunkB1ParkHeaterBtn_stat)
      && (PushButtonStatus_Pushed == *pBunkB1ParkHeaterBtn_stat))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Select 'BunkB1ParkHeaterTempSw_stat'
   else if ((BunkB1ParkHeaterTempSw_statprevious != *pBunkB1ParkHeaterTempSw_stat)
           && ((A3PosSwitchStatus_Lower == *pBunkB1ParkHeaterTempSw_stat)
           || (A3PosSwitchStatus_Upper == *pBunkB1ParkHeaterTempSw_stat)))
   {
      isActTriggerDetected = YES;
   }
   else
   {
      //No Activation trigger detected
   }
   BunkB1ParkHeaterBtn_statprevious    = *pBunkB1ParkHeaterBtn_stat;
   BunkB1ParkHeaterTempSw_statprevious = *pBunkB1ParkHeaterTempSw_stat;

   return isActTriggerDetected;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_PHActMaintainLiving2DeActTrigger
//!
//! \param    *pBunkB1ParkHeaterTempSw_stat   Indicates the current switch status
//!
//! \return   FormalBoolean                   Returns 'yes' or 'no' values
//!
//!======================================================================================
static FormalBoolean ANW_PHActMaintainLiving2DeActTrigger(const uint8  *pBunkB1ParkHeaterTempSw_stat)
{
   //! ###### Processing ANW PHActMaintainLiving2 deactivation trigger logic
   FormalBoolean isDeActTriggerDetected = NO;

   //! ##### Check for 'BunkB1ParkHeaterTempSw_stat' status
   if ((A3PosSwitchStatus_Lower != *pBunkB1ParkHeaterTempSw_stat)
      && (A3PosSwitchStatus_Upper != *pBunkB1ParkHeaterTempSw_stat))
   {
      isDeActTriggerDetected = YES;
   }
   else
   {
      //No Deactivation trigger detected
   }
   return isDeActTriggerDetected;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_PHActMaintainLiving2
//!
//! \param    *pisActTriggerDetected      Provides 'yes' or 'no' values based on activation trigger
//! \param    *pisDeActTriggerDetected    Provides 'yes' or 'no' values based on deactivation trigger
//!
//!======================================================================================
static void ANW_PHActMaintainLiving2(const FormalBoolean  *pisActTriggerDetected,
                                     const FormalBoolean  *pisDeActTriggerDetected)
{
   static AnwSM_States           ANW_SM_PHActActDeactivation;
   static DeactivationTimer_Type PH_Deactivationtimer = 0U;
          FormalBoolean          isRestAct            = NO;
          AnwAction_Enum         Anw_Action           = ANW_Action_None;

   //! ###### Process ANW logic: 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(isRestAct,
                                *pisActTriggerDetected,
                                *pisDeActTriggerDetected,
                                CONST_PHActMaintainLiving2,
                                &PH_Deactivationtimer,
                                &ANW_SM_PHActActDeactivation);
   //! ###### Check for Anw action is 'activate' or 'deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_PHActMaintainLiving2_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_PHActMaintainLiving2_DeactivateIss();
   }
   else
   {
      // Do nothing: keep previous state
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_RoofHatchRequest1ActTrigger
//!
//! \param    *pRoofHatch_SwitchStatus_1   Specifies the current switch status1 of roofHatch 
//! \param    *pRoofHatch_SwitchStatus_2   Specifies the current switch status2 of roofHatch 
//!
//! \return    FormalBoolean               Return 'yes' or 'no' values
//!
//!======================================================================================
static FormalBoolean ANW_RoofHatchRequest1ActTrigger(const uint8     *pRoofHatch_SwitchStatus_1,
                                                     const uint8     *pRoofHatch_SwitchStatus_2)
{
   //! ###### Processing ANW RoofHatchRequest1 activation trigger logic
   static A3PosSwitchStatus_T  RoofHatch_SwitchStatus_1previous;
   static A3PosSwitchStatus_T  RoofHatch_SwitchStatus_2previous;
          FormalBoolean        isActTriggerDetected = NO;

   //! ##### Check for 'RoofHatch_SwitchStatus_1' status
   if ((RoofHatch_SwitchStatus_1previous != *pRoofHatch_SwitchStatus_1)
      && ((A3PosSwitchStatus_Lower == *pRoofHatch_SwitchStatus_1)
      || (A3PosSwitchStatus_Upper == *pRoofHatch_SwitchStatus_1)))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Check for 'RoofHatch_SwitchStatus_2' status
   else if ((RoofHatch_SwitchStatus_2previous != *pRoofHatch_SwitchStatus_2)
           && ((A3PosSwitchStatus_Lower == *pRoofHatch_SwitchStatus_2)
           || (A3PosSwitchStatus_Upper == *pRoofHatch_SwitchStatus_2)))
   {
      isActTriggerDetected = YES;
   }
   else
   {
      //No Activation trigger detected
   }
   RoofHatch_SwitchStatus_1previous = *pRoofHatch_SwitchStatus_1;
   RoofHatch_SwitchStatus_2previous = *pRoofHatch_SwitchStatus_2;

   return isActTriggerDetected;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_RoofHatchRequest1DeActTrigger
//!
//! \param    *pRoofHatch_SwitchStatus_1   Indicates the current switch status1 of roofHatch
//! \param    *pRoofHatch_SwitchStatus_2   Indicates the current switch status2 of roofHatch
//!
//! \return   FormalBoolean                Return 'yes' or 'no' values
//!
//!======================================================================================
static FormalBoolean ANW_RoofHatchRequest1DeActTrigger(const uint8  *pRoofHatch_SwitchStatus_1,
                                                       const uint8  *pRoofHatch_SwitchStatus_2)
{
   //! ###### Processing ANW RoofHatchRequest1 deactivation trigger logic
   FormalBoolean isDeActTriggerDetected = NO;

   //! ##### Check for 'RoofHatch_SwitchStatus_1' status
   if ((A3PosSwitchStatus_Lower != *pRoofHatch_SwitchStatus_1)
      && (A3PosSwitchStatus_Upper != *pRoofHatch_SwitchStatus_1))
   {
      isDeActTriggerDetected = YES;
   }
   //! ##### Check for 'RoofHatch_SwitchStatus_2' status
   else if ((A3PosSwitchStatus_Lower != *pRoofHatch_SwitchStatus_2)
           && (A3PosSwitchStatus_Upper != *pRoofHatch_SwitchStatus_2))
   {
      isDeActTriggerDetected = YES;
   }
   else
   {
      //No Deactivation trigger detected
   }
   return isDeActTriggerDetected;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_RoofHatchRequest1
//!
//! \param   *pisActTriggerDetected     Provides 'yes' or 'no' values based on activation trigger
//! \param   *pisDeActTriggerDetected   Provides 'yes' or 'no' values based on deactivation trigger
//!
//!======================================================================================
static void ANW_RoofHatchRequest1(const FormalBoolean  *pisActTriggerDetected,
                                  const FormalBoolean  *pisDeActTriggerDetected)
{
   static AnwSM_States           ANW_SM_RoofHatchActDeactivation;
   static DeactivationTimer_Type RoofHatch_Deactivationtimer = 0U;
          FormalBoolean          isRestAct                   = NO;
          AnwAction_Enum         Anw_Action                  = ANW_Action_None;

   //! ###### Process ANW logic: 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(isRestAct,
                                *pisActTriggerDetected,
                                *pisDeActTriggerDetected,
                                CONST_RoofHatchRequest1,
                                &RoofHatch_Deactivationtimer,
                                &ANW_SM_RoofHatchActDeactivation);
   //! ###### Check for Anw action is 'activate' or 'deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_RoofHatchRequest1_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_RoofHatchRequest1_DeactivateIss();
   }
   else
   {
      // Do nothing: keep previous state
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_FlexibleSwitchDetectionActTrigger
//!
//! \param   *pComMode_LIN2   Provides current status of ComMode_LIN2 
//!
//! \return   FormalBoolean   Return 'yes' or 'no' values
//!
//!======================================================================================
static FormalBoolean ANW_FlexibleSwitchDetectionActTrigger(const ComMode_LIN_Type   *pComMode_LIN2)
{
   //! ###### Processing ANW FlexibleSwitchDetection activation trigger logic
   FormalBoolean isActTriggerDetected = NO;

   //! ##### Check for communication mode of LIN2 status
   if (SwitchDetection == *pComMode_LIN2)
   {
      isActTriggerDetected = YES;
   }
   else
   {
      //No Activation trigger detected
   }
   return isActTriggerDetected;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_FlexibleSwitchDetectionDeActTrigger
//!
//! \param    pComMode_LIN2  Provides current status of ComMode_LIN2
//!
//! \return   FormalBoolean  Return 'yes' or 'no' values
//!
//!======================================================================================
static FormalBoolean ANW_FlexibleSwitchDetectionDeActTrigger(const ComMode_LIN_Type   *pComMode_LIN2)
{
   //! ###### Processing ANW FlexibleSwitchDetection deactivation trigger logic
   FormalBoolean isDeActTriggerDetected = NO;

   //! ##### Check for detection status
   if (SwitchDetection != *pComMode_LIN2)
   {
      isDeActTriggerDetected = YES;
   }
   else
   {
      //No Deactivation trigger detected
   }
   return isDeActTriggerDetected;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_FlexibleSwitchDetection
//!
//! \param   *pisActTriggerDetected     Provides 'yes' or 'no' values based on activation trigger
//! \param   *pisDeActTriggerDetected   Provides 'yes' or 'no' values based on deactivation trigger
//!
//!======================================================================================
static void ANW_FlexibleSwitchDetection(const FormalBoolean   *pisActTriggerDetected,
                                        const FormalBoolean   *pisDeActTriggerDetected)
{
   static AnwSM_States           ANW_SM_FlexibleSwActDeactivation;
   static DeactivationTimer_Type FlexibleSwitch_Deactivationtimer = 0U;
          FormalBoolean          isRestAct                        = NO;
          AnwAction_Enum         Anw_Action                       = ANW_Action_None;

   //! ###### Process ANW logic: 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(isRestAct,
                                *pisActTriggerDetected,
                                *pisDeActTriggerDetected,
                                CONST_FlexibleSwitchDetection,
                                &FlexibleSwitch_Deactivationtimer,
                                &ANW_SM_FlexibleSwActDeactivation);
   //! ###### Check for Anw action is 'activate' or 'deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_FlexibleSwitchDetection_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_FlexibleSwitchDetection_DeactivateIss();
   }
   else
   {
      // Do nothing: keep previous state
   }
}
//!=======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_InteriorLightsRqst1_ActTrigger
//!
//! \param   *pIntLghtDoorAutoMaxModeBtn_stat   Provides the switch status
//! \param   *pIntLghtActvnBtn_stat             Provides 'on' or 'off' button status
//! \param   *pIntLghtNightModeBtn_stat         Provides 'on' or 'off' button status
//! \param   *pIntLghtNightModeFixSw2_stat      Specifies the button status
//! \param   *pIntLghtMaxModeFixSw2_stat        Specifies the button status
//!
//! \return   FormalBoolean                     Returns 'yes' or 'no' values
//!
//!======================================================================================
static FormalBoolean ANW_InteriorLightsRqst1_ActTrigger(const A3PosSwitchStatus_T  *pIntLghtDoorAutoMaxModeBtn_stat,
                                                        const A2PosSwitchStatus_T  *pIntLghtActvnBtn_stat,
                                                        const A2PosSwitchStatus_T  *pIntLghtNightModeBtn_stat,
                                                        const PushButtonStatus_T   *pIntLghtNightModeFixSw2_stat,
                                                        const PushButtonStatus_T   *pIntLghtMaxModeFixSw2_stat)
{
   //! ###### Processing ANW InteriorLightsRqst1 activation trigger logic
   static A3PosSwitchStatus_T IntLghtDoorAutoMaxModeBtn_statprevious;
   static A2PosSwitchStatus_T IntLghtActvnBtn_statprevious;
   static A2PosSwitchStatus_T IntLghtNightModeBtn_statprevious;
   static PushButtonStatus_T  IntLghtNightModeFixSw2_statprevious;
   static PushButtonStatus_T  IntLghtMaxModeFixSw2_statprevious;
          FormalBoolean       isActTriggerDetected = NO;

   //! ##### Activate ANW based on 'IntLghtDoorAutoMaxModeButton' status
   if ((IntLghtDoorAutoMaxModeBtn_statprevious != *pIntLghtDoorAutoMaxModeBtn_stat)
      && ((A3PosSwitchStatus_Upper == *pIntLghtDoorAutoMaxModeBtn_stat)
      || (A3PosSwitchStatus_Lower == *pIntLghtDoorAutoMaxModeBtn_stat)))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Activate ANW based on 'IntLghtActivatonButton' status
   else if ((A2PosSwitchStatus_Off == IntLghtActvnBtn_statprevious)
           && (A2PosSwitchStatus_On == *pIntLghtActvnBtn_stat))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Activate ANW based on 'IntLghtActivatonButton' status
   else if ((A2PosSwitchStatus_On == IntLghtActvnBtn_statprevious)
           && (A2PosSwitchStatus_Off == *pIntLghtActvnBtn_stat))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Activate ANW based on 'IntLghtNightModeButton' status
   else if ((A2PosSwitchStatus_Off == IntLghtNightModeBtn_statprevious)
           && (A2PosSwitchStatus_On == *pIntLghtNightModeBtn_stat))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Activate ANW based on 'IntLghtNightModeButton' status
   else if ((A2PosSwitchStatus_On == IntLghtNightModeBtn_statprevious)
           && (A2PosSwitchStatus_Off == *pIntLghtNightModeBtn_stat))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Activate ANW based on 'IntLghtNightModeFixSw2' status
   else if ((IntLghtNightModeFixSw2_statprevious != *pIntLghtNightModeFixSw2_stat)
           && (PushButtonStatus_Pushed == *pIntLghtNightModeFixSw2_stat))
   {
      isActTriggerDetected = YES;
   }
   //! ##### Activate ANW based on 'IntLghtMaxModeFixSw2' status
   else if ((IntLghtMaxModeFixSw2_statprevious != *pIntLghtMaxModeFixSw2_stat)
           && (PushButtonStatus_Pushed == *pIntLghtMaxModeFixSw2_stat))
   {
      isActTriggerDetected = YES;
   }
   else
   {
      //No Activation trigger detected
   }
   IntLghtDoorAutoMaxModeBtn_statprevious = *pIntLghtDoorAutoMaxModeBtn_stat;
   IntLghtActvnBtn_statprevious           = *pIntLghtActvnBtn_stat;
   IntLghtNightModeBtn_statprevious       = *pIntLghtNightModeBtn_stat;
   IntLghtNightModeFixSw2_statprevious    = *pIntLghtNightModeFixSw2_stat;
   IntLghtMaxModeFixSw2_statprevious      = *pIntLghtMaxModeFixSw2_stat;

   return isActTriggerDetected;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_InteriorLightsRqst1
//!
//! \param   *pisActTriggerDetected     Provides 'yes' or 'no' values based on activation trigger
//! \param   *pisDeActTriggerDetected   Provides 'yes' or 'no' values based on deactivation trigger
//!
//!======================================================================================
static void ANW_InteriorLightsRqst1(const FormalBoolean  *pisActTriggerDetected,
                                    const FormalBoolean  *pisDeActTriggerDetected)
{
   static DeactivationTimer_Type InteriorLight_Deactivationtimer = 0U;
          FormalBoolean          isRestAct                       = NO;
   static AnwSM_States           ANW_SM_IntLightActDeactivation;
          AnwAction_Enum         Anw_Action                      = ANW_Action_None;

   //! ###### Process ANW logic : 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(isRestAct,
                                *pisActTriggerDetected,
                                *pisDeActTriggerDetected,
                                CONST_InteriorLightsRqst1,
                                &InteriorLight_Deactivationtimer,
                                &ANW_SM_IntLightActDeactivation);
   //! ###### Check for Anw action is 'activate' or 'deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_InteriorLightsRqst1_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_InteriorLightsRqst1_DeactivateIss();
   }
   else
   {
      // Do nothing: keep previous state
   }
}

//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
