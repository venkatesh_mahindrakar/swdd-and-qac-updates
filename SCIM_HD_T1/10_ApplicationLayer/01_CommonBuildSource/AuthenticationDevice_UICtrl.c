/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  AuthenticationDevice_UICtrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  AuthenticationDevice_UICtrl
 *  Generated at:  Fri Jun 12 17:01:39 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <AuthenticationDevice_UICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#define DISABLE_RTE_PTR2ARRAYBASETYPE_PASSING
/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file AuthenticationDevice_UICtrl.c
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup BodyAndSecurity 
//! @{
//! @addtogroup AuthenticationDevice_UICtrl
//! @{
//!
//! \brief
//! AuthenticationDevice_UICtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the AuthenticationDevice_UICtrl runnables.
//!======================================================================================


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_AuthenticationDevice_UICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "AuthenticationDevice_UICtrl.h"
#include "FuncLibrary_Timer_If.h"
#include "FuncLibrary_ScimStd_If.h"

#define PCODE_AuthenticationDeviceType Rte_Prm_P1T3W_VehSSButtonInstalled_v()

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Enumeration Types:
 * ==================
 * ButtonAuth_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 *   ButtonAuth_rqst_Idle (0U)
 *   ButtonAuth_rqst_RequestDeviceAuthentication (1U)
 *   ButtonAuth_rqst_Error (2U)
 *   ButtonAuth_rqst_NotAvailable (3U)
 * DeviceAuthentication_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   DeviceAuthentication_rqst_Idle (0U)
 *   DeviceAuthentication_rqst_DeviceAuthenticationRequest (1U)
 *   DeviceAuthentication_rqst_DeviceDeauthenticationRequest (2U)
 *   DeviceAuthentication_rqst_DeviceMatching (3U)
 *   DeviceAuthentication_rqst_Spare1 (4U)
 *   DeviceAuthentication_rqst_Spare2 (5U)
 *   DeviceAuthentication_rqst_Error (6U)
 *   DeviceAuthentication_rqst_NotAvailable (7U)
 * DoorsAjar_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorsAjar_stat_Idle (0U)
 *   DoorsAjar_stat_BothDoorsAreClosed (1U)
 *   DoorsAjar_stat_DriverDoorIsOpen (2U)
 *   DoorsAjar_stat_PassengerDoorIsOpen (3U)
 *   DoorsAjar_stat_BothDoorsAreOpen (4U)
 *   DoorsAjar_stat_Spare (5U)
 *   DoorsAjar_stat_Error (6U)
 *   DoorsAjar_stat_NotAvailable (7U)
 * DriverAuthDeviceMatching_T: Enumeration of integer in interval [0...3] with enumerators
 *   DriverAuthDeviceMatching_Idle (0U)
 *   DriverAuthDeviceMatching_DeviceToMatchIsPresent (1U)
 *   DriverAuthDeviceMatching_Error (2U)
 *   DriverAuthDeviceMatching_NotAvailable (3U)
 * KeyPosition_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyPosition_KeyOut (0U)
 *   KeyPosition_IgnitionKeyInOffPosition (1U)
 *   KeyPosition_IgnitionKeyInAccessoryPosition (2U)
 *   KeyPosition_IgnitionKeyIn15PositionNormalDrivingPosition (3U)
 *   KeyPosition_IgnitionKeyInPreheatPosition (4U)
 *   KeyPosition_IgnitionKeyInCrankPosition (5U)
 *   KeyPosition_ErrorIndicator (6U)
 *   KeyPosition_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *   FALSE (0U)
 *   TRUE (1U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1T3W_VehSSButtonInstalled_v(void)
 *
 *********************************************************************************************************************/


#define AuthenticationDevice_UICtrl_START_SEC_CODE
#include "AuthenticationDevice_UICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AuthenticationDevice_UICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ButtonAuth_rqst_ButtonAuth_rqst(ButtonAuth_rqst_T *data)
 *   Std_ReturnType Rte_Read_DoorsAjar_stat_DoorsAjar_stat(DoorsAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_KeyPosition_KeyPosition(KeyPosition_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DeviceAuthentication_rqst_DeviceAuthentication_rqst(DeviceAuthentication_rqst_T data)
 *   Std_ReturnType Rte_Write_DriverAuthDeviceMatching_DriverAuthDeviceMatching(DriverAuthDeviceMatching_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AuthenticationDevice_UICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, AuthenticationDevice_UICtrl_CODE) AuthenticationDevice_UICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AuthenticationDevice_UICtrl_20ms_runnable
 *********************************************************************************************************************/
 
 	//! ###### Definition of the SWC In/Out data structures

	// Define input RteInData for Start Button variant
	static ButtonAuth_rqst_T RteInData_StartPushButton;
	// Define input RteInData for Key Switch variant
	static ButtonStat_T RteInData_KeySwitch;
	// Define output for DeviceAuthentication_rqst
	static DeviceAuthentication_rqst_T RteOutData_DeviceAuthentication_rqst;
	// Define output for DriverAuthDeviceMatching
	static DriverAuthDeviceMatching_T RteOutData_DriverAuthDeviceMatching;
	// Define timers
	static uint16 Timers[CONST_NbOfTimers] = { CONST_TimerFunctionInactive };

    Std_ReturnType retValue = RTE_E_INVALID;
	
	TimerFunction_Tick(CONST_NbOfTimers,Timers);
	
	//! ###### Variant based logic processing
	//! ##### Select the Authentication device variant logic based on PCODE_AuthenticationDeviceType
	switch (PCODE_AuthenticationDeviceType)
	{
	case CONST_Key_Switch:
		//! ##### 0: Process Key switch variant logic
		//! #### Read the RTE port for KeyPosition and process fallback modes for RTE events
		retValue = Rte_Read_KeyPosition_KeyPosition(&RteInData_KeySwitch.Current_state);
		MACRO_StdRteRead_IntRPort(retValue, RteInData_KeySwitch.Current_state, KeyPosition_ErrorIndicator);
		
		//! #### Processing mode logic for Key switch device type, Function 'Processing_Keyswitch()'.
		Processing_Keyswitch(&RteInData_KeySwitch, &RteOutData_DeviceAuthentication_rqst,&RteOutData_DriverAuthDeviceMatching, Timers);
		// Save previous value of key switch
		RteInData_KeySwitch.Previous_state = RteInData_KeySwitch.Current_state;
		break;

	case CONST_Start_Button:
		//! ##### 1: Process Start Push Switch variant logic
		
		//! #### Read the RTE port for Start Push Switch and process fallback modes for RTE events
		retValue = Rte_Read_ButtonAuth_rqst_ButtonAuth_rqst(&RteInData_StartPushButton);
		MACRO_StdRteRead_IntRPort(retValue, RteInData_StartPushButton , ButtonAuth_rqst_Error);
		
		//! #### Processing mode logic for Start Push Switch device type: Function 'Processing_StartPushButton()' 
		Processing_StartPushButton(&RteInData_StartPushButton, &RteOutData_DeviceAuthentication_rqst,&RteOutData_DriverAuthDeviceMatching);
		break;

	default:
		break;
	}
	//! ###### Common output logic processing

	//! ##### Write DeviceAuthentication_rqst RTE port
	Rte_Write_DeviceAuthentication_rqst_DeviceAuthentication_rqst(RteOutData_DeviceAuthentication_rqst);
	//! ##### Write DriverAuthDeviceMatching RTE port
	Rte_Write_DriverAuthDeviceMatching_DriverAuthDeviceMatching(RteOutData_DriverAuthDeviceMatching);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define AuthenticationDevice_UICtrl_STOP_SEC_CODE
#include "AuthenticationDevice_UICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

//!===============================================================================================
//! \brief
//! This function implement the processing logic of Start button
//! 
//!  
//! 
//! \param ButtonAuth_rqst contain the authentication button status
//! \param pDeviceAuthentication_rqst is the pointer with value of the logic
//!
//!===============================================================================================
static void Processing_StartPushButton(ButtonAuth_rqst_T *pButtonAuth_rqst, DeviceAuthentication_rqst_T *pDeviceAuthentication_rqst,DriverAuthDeviceMatching_T *pRteOutData_DriverAuthDeviceMatching)
{
	//! ######  Processing Start Push Switch logic,
	if (ButtonAuth_rqst_RequestDeviceAuthentication == *pButtonAuth_rqst )
	{
		*pDeviceAuthentication_rqst           = DeviceAuthentication_rqst_DeviceAuthenticationRequest;
		*pRteOutData_DriverAuthDeviceMatching = DriverAuthDeviceMatching_DeviceToMatchIsPresent;
	}
	else
	{
		*pDeviceAuthentication_rqst           = DeviceAuthentication_rqst_Idle;
		*pRteOutData_DriverAuthDeviceMatching = DriverAuthDeviceMatching_Idle;
	}
}

//!===============================================================================================
//! \brief
//! This function implement the processing logic of Key Switch (KeyPosition)
//!
//!  
//! \param KeyPosition contain the position key status
//! \param pDeviceAuthentication_rqst is the pointer with value of the logic
//!
//!===============================================================================================
static void Processing_Keyswitch(ButtonStat_T *pKeyPosition, DeviceAuthentication_rqst_T *pDeviceAuthentication_rqst,DriverAuthDeviceMatching_T *pRteOutData_DriverAuthDeviceMatching, uint16 Timers[])
{
	static boolean isAuthenticationTimeoutActive = FALSE;
	static boolean isKeyOutTimeoutActive = FALSE;
	static boolean isDeathenticationTimeoutActive = FALSE;
	
	//! ###### Processing for Key Switch logic
	// KeyPosition authentication trigger: change of key position
	if (((KeyPosition_KeyOut  == (pKeyPosition->Previous_state)) ||
		(KeyPosition_ErrorIndicator == (pKeyPosition->Previous_state)) ||
		(KeyPosition_NotAvailable == (pKeyPosition->Previous_state))) &&
		((KeyPosition_IgnitionKeyInOffPosition == (pKeyPosition->Current_state)) ||
		(KeyPosition_IgnitionKeyInAccessoryPosition == (pKeyPosition->Current_state)) ||
		(KeyPosition_IgnitionKeyIn15PositionNormalDrivingPosition  == (pKeyPosition->Current_state)) ||
		(KeyPosition_IgnitionKeyInPreheatPosition  == (pKeyPosition->Current_state)) ||
		(KeyPosition_IgnitionKeyInCrankPosition  == (pKeyPosition->Current_state))
		))
	{
		// Set authentication processing
		isAuthenticationTimeoutActive = TRUE;
		// Set authentication processing timer
		Timers[CONST_IdTimerKeySwitch]=CONST_Timer_200ms;
		// Cancel the deauthentication timeout
		isDeathenticationTimeoutActive = FALSE;
	}
	else
	{
		//nothing
	}
	if (TRUE == isAuthenticationTimeoutActive)
	{
		if (0 == Timers[CONST_IdTimerKeySwitch])
		{
			*pDeviceAuthentication_rqst = DeviceAuthentication_rqst_Idle;
			isAuthenticationTimeoutActive = FALSE;
		}
		else
		{
			*pDeviceAuthentication_rqst = DeviceAuthentication_rqst_DeviceAuthenticationRequest;
		}
	}
	else
	{
		//nothing, Timeout is not active
	}
	// Deauthentication logic
	if ((KeyPosition_KeyOut == (pKeyPosition->Current_state )) &&
		((pKeyPosition->Previous_state) != (pKeyPosition->Current_state)))
	{
		// Set KeyOutTimeout processing
		isKeyOutTimeoutActive = TRUE;
		// Set authentication processing timer
		Timers[CONST_IdTimerKeySwitch]= CONST_Timer_8000ms;
		// Cancel the authentication processing
		isAuthenticationTimeoutActive = FALSE;
	}
	else
	{
		//nothing, Timeout is not active
	}
	// KeyOut logic
	if (TRUE == isKeyOutTimeoutActive)
	{
		if (KeyPosition_KeyOut == (pKeyPosition->Current_state))
		{
			if (Timers[CONST_IdTimerKeySwitch] == 0)
			{
				// Set Deathentication processing
				isDeathenticationTimeoutActive = TRUE;
				// Set authentication processing timer
				Timers[CONST_IdTimerKeySwitch]= CONST_Timer_200ms;
				// Cancel KeyOutTimeout processing
				isKeyOutTimeoutActive = FALSE;
			}
			else
			{
				// nothing, timer not elapse 
			}
		}
		else
		{
			*pDeviceAuthentication_rqst = DeviceAuthentication_rqst_Idle;
			// Cancel KeyOutTimeout processing
			isKeyOutTimeoutActive = FALSE;
			// Cancel Deathentication processing
			isDeathenticationTimeoutActive = FALSE;
			Timers[CONST_IdTimerKeySwitch] = CONST_TimerFunctionInactive;
		}
	}
	else
	{
		//nothing, Timeout is not active
	}
	// Deathentication logic
	if (TRUE == isDeathenticationTimeoutActive)
	{
		if (0 == Timers[CONST_IdTimerKeySwitch])
		{
			*pDeviceAuthentication_rqst = DeviceAuthentication_rqst_Idle;
			isDeathenticationTimeoutActive = FALSE;
			Timers[CONST_IdTimerKeySwitch] = CONST_TimerFunctionInactive;
		}
		else
		{
			*pDeviceAuthentication_rqst = DeviceAuthentication_rqst_DeviceDeauthenticationRequest;
		}
	}
	else
	{
		//nothing, Timeout is not active
	}
	// Keyfob Matching device presentation, key switch 
	if( ((KeyPosition_KeyOut  == (pKeyPosition->Previous_state)) ||
	    (KeyPosition_IgnitionKeyInOffPosition == (pKeyPosition->Previous_state)))&&
		((KeyPosition_IgnitionKeyInOffPosition == (pKeyPosition->Current_state)) ||
		(KeyPosition_IgnitionKeyInAccessoryPosition == (pKeyPosition->Current_state)) ||
		(KeyPosition_IgnitionKeyIn15PositionNormalDrivingPosition  == (pKeyPosition->Current_state)) ||
		(KeyPosition_IgnitionKeyInPreheatPosition  == (pKeyPosition->Current_state)) ||
		(KeyPosition_IgnitionKeyInCrankPosition  == (pKeyPosition->Current_state))))
	{
		*pRteOutData_DriverAuthDeviceMatching = DriverAuthDeviceMatching_DeviceToMatchIsPresent ;
	}
	else
	{
		// nothing, do not change the output DriverAuthDeviceMatching
	}
}
//! @}
//! @}
//! @}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/