/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  SCIM_PowerSupply12V_Hdlr.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  SCIM_PowerSupply12V_Hdlr
 *  Generated at:  Fri Jun 12 17:10:05 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <SCIM_PowerSupply12V_Hdlr>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   Boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * IOHWAB_BOOL
 *   boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * IOHWAB_UINT8
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinFaultStatus
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinVoltage_0V2
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_SCIM_PowerSupply12V_Hdlr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "FuncLibrary_Timer_If.h"
#include "SCIM_PowerSupply12V_Hdlr.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
	Living12VPowerStability living12VPowerStabilityVar = LIVING12VPOWERSTABILITY_INACTIVE;


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * IOHWAB_BOOL: Boolean
 * IOHWAB_UINT8: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * IOCtrlReq_T: Enumeration of integer in interval [0...255] with enumerators
 *   IOCtrl_AppRequest (0U)
 *   IOCtrl_DiagReturnCtrlToApp (1U)
 *   IOCtrl_DiagShortTermAdjust (2U)
 * Living12VPowerStability: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Active (1U)
 *   Stable (2U)
 *   Error (3U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *
 *********************************************************************************************************************/


#define SCIM_PowerSupply12V_Hdlr_START_SEC_CODE
#include "SCIM_PowerSupply12V_Hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_PowerSupply12V_Hdlr_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Living12VResetRequest_Living12VResetRequest(Boolean *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Living12VPowerStability_Living12VPowerStability(Living12VPowerStability data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(VGTT_EcuPinVoltage_0V2 *DcDc12vRefVoltage, IOHWAB_BOOL *IsDcDc12vActivated, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Issm_GetAllActiveIss_GetAllActiveIss(uint32 *activeIssField)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_GetAllActiveIss_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_PowerSupply12V_Hdlr_10ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SCIM_PowerSupply12V_Hdlr_CODE) SCIM_PowerSupply12V_Hdlr_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_PowerSupply12V_Hdlr_10ms_runnable
 *********************************************************************************************************************/
   static SM_PowerSupply12V_Hdlr_Enum smPowerSupply12V_Hdlr = SM_PowerSupply12V_Hdlr_NoInit;
   static BooleanVar_T resetRequest = {FALSE,FALSE};
   
   static IOHWAB_BOOL isOutputDcdc12VActive = FALSE;
   static IOHWAB_BOOL isOutputDo12VLivingActive = FALSE;
   static IOHWAB_BOOL isOutputDo12VParkedActive = FALSE;
   
   static uint16 Timers[CONST_NbOfTimers] = {CONST_12VLivingOutputDeactivatedTimeout, CONST_TimerFunctionInactive};

   VehicleMode_T vehicleModeInternal = Rte_InitValue_VehicleModeInternal_VehicleMode;
   Boolean needToOverwritebyResetRequest = FALSE;
   uint32 issmState = 0u;

   //! ##### Decrement the timers
   TimerFunction_Tick(CONST_NbOfTimers, Timers);

   // Read from RTE
   resetRequest.Current = resetRequest.New;
   Rte_Read_Living12VResetRequest_Living12VResetRequest(&resetRequest.New);
   Rte_Read_VehicleModeInternal_VehicleMode(&vehicleModeInternal);
   Rte_Call_Issm_GetAllActiveIss_GetAllActiveIss(&issmState);

    //! ###### Living 12V output activation conditions
    //! #### @VehicleModeInternal port is equal 'Living', 'Accessory', 'PreRunning', 'Cranking', 'Running'
    //! #### OR one of the following ISS is active: LIN1, LIN2, LIN3, LIN4, LIN5, LIN11
    if (((VehicleMode_Living == vehicleModeInternal)
       || (VehicleMode_Accessory == vehicleModeInternal)
       || (VehicleMode_PreRunning == vehicleModeInternal)
       || (VehicleMode_Cranking == vehicleModeInternal)
       || (VehicleMode_Running == vehicleModeInternal))
       || (0u < (issmState & CONST_LINIssActivate)))
    {
       //! ### DC/DC converter activation : If Living OR Parked activation is expected
       isOutputDcdc12VActive     = TRUE;
       isOutputDo12VLivingActive = TRUE;
       isOutputDo12VParkedActive = FALSE;
       Timers[CONST_12VLivingOutputDeactivate_Timer] = CONST_12VLivingOutputDeactivatedTimeout;
       
       if (CONST_TimerFunctionInactive != Timers[CONST_12VLivingOutputrResetRequest_Timer])
       { 
          // overwrite the activation request by the reset
          isOutputDo12VLivingActive = FALSE;
       }
       else
       {
         // no action: keep the outputs active
       }
    }
    //! #### otherwise, 12V Living output shall be deactivated after deactivation timeout.
    else if (CONST_TimerFunctionElapsed == Timers[CONST_12VLivingOutputDeactivate_Timer])
    {
       isOutputDcdc12VActive     = FALSE;
       isOutputDo12VLivingActive = FALSE;
       isOutputDo12VParkedActive = FALSE;
       Timers[CONST_12VLivingOutputDeactivate_Timer] = CONST_TimerFunctionInactive;
       Timers[CONST_12VLivingOutputrResetRequest_Timer] = CONST_TimerFunctionInactive;
    }
    else
    {
       // do nothing: keep current state
    }
    
   if ((resetRequest.Current != resetRequest.New)
      &&(TRUE == resetRequest.New))
   {
     Timers[CONST_12VLivingOutputrResetRequest_Timer] = CONST_12VLivingOutputResetRequestTimeout;
   }
   else
   {
     // no action: no reset action requested
   }


   
   (void)Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrl_AppRequest,isOutputDo12VLivingActive);
   (void)Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrl_AppRequest,isOutputDo12VParkedActive);


  living12VPowerStabilityVar = Living12VPowerStabilityHandler();
/*
  if((living12VPowerStabilityVar == LIVING12VPOWERSTABILITY_ACTIVE)
  	||(living12VPowerStabilityVar == LIVING12VPOWERSTABILITY_STABLE))
  {
     living12VPowerStabilityVar = LIVING12VPOWERSTABILITY_STABLE;
  }
  	 
   */
   // Write output
   (void)Rte_Write_Living12VPowerStability_Living12VPowerStability(living12VPowerStabilityVar);//living12VPowerStabilityVar.Current);
   
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static Living12VPowerStability Living12VPowerStabilityHandler(void)
{
   static Living12VPowerStability_States_Enum Living12VPowerStability_SM = SM_Living12VPowerStability_Inactive;
   static uint8 stableCnt = 0u;
      
   IOHWAB_UINT8 selectParkedOrLivingPin = 1u; // 0:parked, 1:living
   IOHWAB_BOOL isDo12VActivated = 0u;
   VGTT_EcuPinVoltage_0V2 do12VPinVoltage = 0u;
   VGTT_EcuPinVoltage_0V2 batteryVoltage = 0u;
   VGTT_EcuPinVoltage_0V2 dcdcRefVoltage = 0u;
   VGTT_EcuPinFaultStatus diagStatus = 0u;
	static Living12VPowerStability Living12VPowerStabilityVar;

   Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(selectParkedOrLivingPin, 
                                                &isDo12VActivated,
                                                &do12VPinVoltage,
                                                &batteryVoltage,
                                                &diagStatus);

   // check voltage and judge current state
   switch(Living12VPowerStability_SM)
   {


      case SM_Living12VPowerStability_Active: // CONST_DCDC12V_STABLE_TIME_CNT
         if (DCDC12V_VOLTAGE_18V_0V2 < do12VPinVoltage) // Higher than 18V
         {
            Living12VPowerStability_SM = SM_Living12VPowerStability_Error;
            stableCnt = 0;
         }
         else if (DCDC12V_VOLTAGE_8V_0V2 > do12VPinVoltage) // Lower than 8V
         {
            Living12VPowerStability_SM = SM_Living12VPowerStability_Inactive;
            stableCnt = 0;
         }
         else // between 8V ~ 18V
         {
            if (CONST_DCDC12V_STABLE_TIME_CNT > stableCnt) // stabilization is not confirmed
            {
               stableCnt = stableCnt + 1;
               Living12VPowerStabilityVar = LIVING12VPOWERSTABILITY_ACTIVE;
            }
            else
            {
               Living12VPowerStabilityVar = SM_Living12VPowerStability_Stable;
            }
         }
      break;
      
      case SM_Living12VPowerStability_Error:
         
         // check voltage is lower than limit
         // !processing order is important: for lower range!
         if (DCDC12V_VOLTAGE_8V_0V2 > do12VPinVoltage) // Lower than 8V
         {
            Living12VPowerStability_SM = SM_Living12VPowerStability_Inactive;
         }
         else if (DCDC12V_VOLTAGE_18V_0V2 > do12VPinVoltage) // Higher than 18V
         {
            Living12VPowerStability_SM = SM_Living12VPowerStability_Active;
         }
         else
         {
            // do nothing : stay in error state
         }
         Living12VPowerStabilityVar = LIVING12VPOWERSTABILITY_ERROR;
      break;

      default: //Living12VPowerStability_Inactive:
         if (DCDC12V_VOLTAGE_8V_0V2 < do12VPinVoltage) // Higher than 8V
         {
            Living12VPowerStability_SM = SM_Living12VPowerStability_Active;
         }
         else
         {
            // do nothing: stay in current state, output considered inactive
         }
         Living12VPowerStabilityVar = Inactive;
         stableCnt = 0;
      break;
   }

	return Living12VPowerStabilityVar;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#define SCIM_PowerSupply12V_Hdlr_STOP_SEC_CODE
#include "SCIM_PowerSupply12V_Hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
