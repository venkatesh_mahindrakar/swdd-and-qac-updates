/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  InteriorLightPanel_1_LINMastCtrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  InteriorLightPanel_1_LINMastCtrl
 *  Generated at:  Fri Jun 12 17:01:42 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <InteriorLightPanel_1_LINMastCtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
//!======================================================================================
//! \file InteriorLightPanel_1_LINMastCtrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer
//! @{
//! @addtogroup Application_BodyAndComfort
//! @{
//! @addtogroup Application_Living
//! @{
//! @addtogroup InteriorLightPanel_1_LINMastCtrl
//! @{
//!
//! \brief
//! InteriorLightPanel_1_LINMastCtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the InteriorLightPanel_1_LINMastCtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_InteriorLightPanel_1_LINMastCtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "FuncLibrary_AnwStateMachine_If.h"
#include "InteriorLightPanel_1_LINMastCtrl.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * ResponseErrorILCP1_T: Boolean
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Spare1 (4U)
 *   Spare2 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 *   FreeWheel_Status_NoMovement (0U)
 *   FreeWheel_Status_1StepClockwise (1U)
 *   FreeWheel_Status_2StepsClockwise (2U)
 *   FreeWheel_Status_3StepsClockwise (3U)
 *   FreeWheel_Status_4StepsClockwise (4U)
 *   FreeWheel_Status_5StepsClockwise (5U)
 *   FreeWheel_Status_6StepsClockwise (6U)
 *   FreeWheel_Status_1StepCounterClockwise (7U)
 *   FreeWheel_Status_2StepsCounterClockwise (8U)
 *   FreeWheel_Status_3StepsCounterClockwise (9U)
 *   FreeWheel_Status_4StepsCounterClockwise (10U)
 *   FreeWheel_Status_5StepsCounterClockwise (11U)
 *   FreeWheel_Status_6StepsCounterClockwise (12U)
 *   FreeWheel_Status_Spare (13U)
 *   FreeWheel_Status_Error (14U)
 *   FreeWheel_Status_NotAvailable (15U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1VR1_ILCP1_Installed_v(void)
 *
 *********************************************************************************************************************/

#define InteriorLightPanel_1_LINMastCtrl_START_SEC_CODE
#include "InteriorLightPanel_1_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_ILCP1_Installed   (Rte_Prm_P1VR1_ILCP1_Installed_v())
/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOU_Data_P1EOU_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, InteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/
   *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_SUBFUNCTIONNOTSUPPORTED;
   return RTE_E_DataServices_P1EOU_Data_P1EOU_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData_Irv_IOCTL_ILCP1Lin(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOU_Data_P1EOU_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, InteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData (returns application error)
 *********************************************************************************************************************/
   Data[0] = (uint8)((Rte_IrvRead_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData_Irv_IOCTL_ILCP1Lin()) & CONST_Tester_Notpresent);
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ILCP1Lin(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOU_Data_P1EOU_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, InteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/
   Rte_IrvWrite_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ILCP1Lin((Rte_IrvRead_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData_Irv_IOCTL_ILCP1Lin()) & CONST_Tester_Notpresent); // change 0x7F with Invert MSB Mask macro
   *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_POSITIVERESPONSE;
  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_ILCP1Lin(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOU_Data_P1EOU_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, InteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/
   uint8 retVal = RTE_E_OK;
   *ErrorCode   = (Dcm_NegativeResponseCodeType)DCM_E_POSITIVERESPONSE;
   if (Data[0] < 3U) // change 3 with Maximum Range value
   {
      Rte_IrvWrite_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ILCP1Lin(Data[0]| CONST_Tester_Present);// change 0x80 with MSB Mask macro
   }
   else
   {
      retVal     = RTE_E_DataServices_P1EOU_Data_P1EOU_E_NOT_OK;
      *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_REQUESTOUTOFRANGE;
   }
   return retVal;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: InteriorLightPanel_1_LINMastCtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_DoorAutoFuncInd_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_IntLghtOffModeInd_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_IntLightMaxModeInd_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_IntLightNightModeInd_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_IntLightRestingModeInd_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_LIN_DoorAutoFuncBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_IntLghtDimmingLvlDecBtn_s_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_IntLghtDimmingLvlIncBtn_s_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_IntLghtModeSelrFreeWheel_s_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_ResponseErrorILCP1_ResponseErrorILCP1(ResponseErrorILCP1_T *data)
 *   boolean Rte_IsUpdated_LIN_IntLghtModeSelrFreeWheel_s_FreeWheel_Status(void)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DoorAutoFuncBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status(FreeWheel_Status_T data)
 *   Std_ReturnType Rte_Write_LIN_DoorAutoFuncInd_cmd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_IntLghtOffModeInd_cmd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_IntLightMaxModeInd_cmd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_IntLightNightModeInd_cmd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_IntLightRestingModeInd_cmd_DeviceIndication(DeviceIndication_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_InteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ILCP1Lin(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_InteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ILCP1Lin(uint8 data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BKD_87_ILCP1Link_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights2_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights2_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: InteriorLightPanel_1_LINMastCtrl_20ms_runnable_doc
 *********************************************************************************************************************/
 
//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution runnable logic for the InteriorLightPanel_1_LINMastCtrl_20ms_runnable
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, InteriorLightPanel_1_LINMastCtrl_CODE) InteriorLightPanel_1_LINMastCtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: InteriorLightPanel_1_LINMastCtrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   // Define Rte input data common
   static InteriorLightPanel1LINMastCtrl_InData_T   RteInData_Common;
   // Define Rte output data common
   static InteriorLightPanel1LINMastCtrl_OutData_T  RteOutData_Common = { PushButtonStatus_NotAvailable ,
                                                                          PushButtonStatus_NotAvailable ,
                                                                          PushButtonStatus_NotAvailable ,
                                                                          FreeWheel_Status_NotAvailable , 
                                                                          DeviceIndication_Off, 
                                                                          DeviceIndication_Off,
                                                                          DeviceIndication_Off,
                                                                          DeviceIndication_Off,
                                                                          DeviceIndication_Off };
   // Local logic variables 
   Std_ReturnType retValue     = RTE_E_INVALID;
   FormalBoolean  isRestAct    = YES;
   FormalBoolean  isActTrigDet = NO;

   //! ###### Process the RTE read common logic: 'Get_RteDataRead_Common()'
   retValue = Get_RteDataRead_Common(&RteInData_Common);
   //! ###### Check for communication mode of LIN1 is error (LIN Busoff)
   if (Error == RteInData_Common.ComMode_LIN1)
   {
      //! ##### Process the InteriorLightPanel1LINMastCtrl fallback mode logic: 'Fallback_Logic_InteriorLightPanel1LINMastCtrl()'
      Fallback_Logic_InteriorLightPanel1LINMastCtrl(&RteOutData_Common.DoorAutoFuncBtn_stat,
                                                    &RteOutData_Common.IntLghtDimmingLvlDecBtn_stat,
                                                    &RteOutData_Common.IntLghtDimmingLvlIncBtn_stat,
                                                    &RteOutData_Common.IntLghtModeSelrFreeWheel_stat);
   }
   else
   {
      //! ###### Select 'ILCP1_Installed' parameter
      if (TRUE == PCODE_ILCP1_Installed)
      {
         //! ##### Check for communication mode of LIN1 status
         if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN1)
            || (SwitchDetection == RteInData_Common.ComMode_LIN1)
            || (Diagnostic == RteInData_Common.ComMode_LIN1))
         {
            // restriction 
            isRestAct = NO;
            //! ##### Check for response error ILCP1 (missing frame condition)
            if (RTE_E_MAX_AGE_EXCEEDED == retValue)
            {
               //! ##### Check for communication mode of LIN1 is 'applicationmonitoring' or 'switchdetection'
               if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN1)
                  || (SwitchDetection == RteInData_Common.ComMode_LIN1))
               {
                  // service call 
                  //! #### Invoke 'Rte_Call_Event_D1BKD_87_ILCP1Link_NoResp_SetEventStatus' with status 'failed'
                  Rte_Call_Event_D1BKD_87_ILCP1Link_NoResp_SetEventStatus(DEM_EVENT_STATUS_FAILED);
                  // fallback
                  //! #### Process the fallback mode logic: 'Fallback_Logic_InteriorLightPanel1LINMastCtrl()'
                  Fallback_Logic_InteriorLightPanel1LINMastCtrl(&RteOutData_Common.DoorAutoFuncBtn_stat,
                                                                &RteOutData_Common.IntLghtDimmingLvlDecBtn_stat,
                                                                &RteOutData_Common.IntLghtDimmingLvlIncBtn_stat,
                                                                &RteOutData_Common.IntLghtModeSelrFreeWheel_stat);
               }
               else
               {
                  // gateway logic
                  //! #### If LIN communication mode in diagnostic state
                  //! #### Process the gateway logic: 'Gateway_Logic_InteriorLightPanel1LINMastCtrl()'
                  Gateway_Logic_InteriorLightPanel1LINMastCtrl(&RteInData_Common,
                                                               &RteOutData_Common);
               }
            }
            //! ##### Check for response error ILCP1 is RTE_E_OK or RTE_E_COM_STOPPED or RTE_E_NEVER_RECEIVED
            else if ((RTE_E_COM_STOPPED == retValue)
                    || (RTE_E_OK == retValue)
                    || (RTE_E_NEVER_RECEIVED == retValue ))
            {
               //! #### LIN frame received without receive timeout
               //! #### Invoke 'Rte_Call_Event_D1BKD_87_ILCP1Link_NoResp_SetEventStatus' with status 'passed'(Remove the LIN missing frame fault)
               // service call 
               Rte_Call_Event_D1BKD_87_ILCP1Link_NoResp_SetEventStatus(DEM_EVENT_STATUS_PASSED);
               //! #### Process the gateway logic: 'Gateway_Logic_InteriorLightPanel1LINMastCtrl()'
               Gateway_Logic_InteriorLightPanel1LINMastCtrl(&RteInData_Common,
                                                            &RteOutData_Common);
            }
            else
            {
               // fall back
               //! #### Process fallback mode logic: 'Fallback_Logic_InteriorLightPanel1LINMastCtrl()'
               Fallback_Logic_InteriorLightPanel1LINMastCtrl(&RteOutData_Common.DoorAutoFuncBtn_stat,
                                                             &RteOutData_Common.IntLghtDimmingLvlDecBtn_stat,
                                                             &RteOutData_Common.IntLghtDimmingLvlIncBtn_stat,
                                                             &RteOutData_Common.IntLghtModeSelrFreeWheel_stat);
            }
         }
         else
         {
            // deactivation
            //! ##### Process deactivation logic: 'Deactivate_Logic_InteriorLightPanel1LINMastCtrl()'
            Deactivate_Logic_InteriorLightPanel1LINMastCtrl(&RteOutData_Common.DoorAutoFuncBtn_stat,
                                                            &RteOutData_Common.IntLghtDimmingLvlDecBtn_stat,
                                                            &RteOutData_Common.IntLghtDimmingLvlIncBtn_stat,
                                                            &RteOutData_Common.IntLghtModeSelrFreeWheel_stat);
         }
      }
      else
      {
         // Deactivation
         //! ###### Process deactivation logic: 'Deactivate_Logic_InteriorLightPanel1LINMastCtrl()'
         Deactivate_Logic_InteriorLightPanel1LINMastCtrl(&RteOutData_Common.DoorAutoFuncBtn_stat,
                                                         &RteOutData_Common.IntLghtDimmingLvlDecBtn_stat,
                                                         &RteOutData_Common.IntLghtDimmingLvlIncBtn_stat,
                                                         &RteOutData_Common.IntLghtModeSelrFreeWheel_stat);
      }
   }
   //! ###### Process OtherInterior light2 activation trigger logic: 'Generic_ANW_OtherInteriorLight2_CondCheck()'
   isActTrigDet = Generic_ANW_OtherInteriorLight2_CondCheck(&RteInData_Common.LIN_DoorAutoFuncBtn_stat,
                                                            &RteInData_Common.LIN_IntLghtDimmingLvlDecBtn_s,
                                                            &RteInData_Common.LIN_IntLghtDimmingLvlIncBtn_s,
                                                            &RteInData_Common.LIN_IntLghtModeSelrFreeWheel_s,
                                                            &RteInData_Common.IntLghtOffModeInd_cmd);
   //! ###### Activating Application Networks of OtherInterior light2: 'Generic_ANW_OtherInteriorLights2_Logic()'
   Generic_ANW_OtherInteriorLights2_Logic(isRestAct,
                                          isActTrigDet);
   if ((CONST_Tester_Present == ((Rte_IrvRead_InteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ILCP1Lin()) & CONST_Tester_Present))
      && ((uint8)Diag_Active_TRUE == RteInData_Common.isDiagActive))
   {
      IOCtrlService_LinOutputSignalControl((uint8)((Rte_IrvRead_InteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ILCP1Lin()) & CONST_Tester_Notpresent),
                                           &RteOutData_Common);
   }
   else
   {
      Rte_IrvWrite_InteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ILCP1Lin(DeviceIndication_SpareValue);
   }
   //! ###### Process the RTE write common logic: 'RteDataWrite_Common()'
   RteDataWrite_Common(&RteOutData_Common,
                       &RteInData_Common.IsUpdated_LIN_IntLghtModeSelrFreeWheel);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

#define InteriorLightPanel_1_LINMastCtrl_STOP_SEC_CODE
#include "InteriorLightPanel_1_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'IOCtrlService_LinOutputSignalControl'
//!
//! \param   OverrideData    Provide override data value
//! \param   *pOutData       Update output data structure for InteriorLightPanel1LINMastCtrl_GWLogic
//!
//!======================================================================================
static void IOCtrlService_LinOutputSignalControl(const uint8                                     OverrideData,
                                                       InteriorLightPanel1LINMastCtrl_OutData_T  *pOutData)
{
   //! ###### Override LIN output signals with IOControl service value
   pOutData->LIN_DoorAutoFuncInd_cmd        = OverrideData;
   pOutData->LIN_IntLghtOffModeInd_cmd      = OverrideData;
   pOutData->LIN_IntLightMaxModeInd_cmd     = OverrideData;
   pOutData->LIN_IntLightNightModeInd_cmd   = OverrideData;
   pOutData->LIN_IntLightRestingModeInd_cmd = OverrideData;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Get_RteDataRead_Common'
//!
//! \param   *pRteInData_Common   Examine and update the input signals based on RTE failure events
//! 
//! \return  Std_ReturnType    Returns 'retValue_InteriorLight1' value
//!
//!======================================================================================
static Std_ReturnType Get_RteDataRead_Common(InteriorLightPanel1LINMastCtrl_InData_T   *pRteInData_Common)
{
   //! ###### Processing RTE data read common logic
   Std_ReturnType  retValue                = RTE_E_INVALID;
   Std_ReturnType  retValue_InteriorLight1 = RTE_E_INVALID;

   //! ##### Read DoorAutoFuncInd_cmd interface
   retValue= Rte_Read_DoorAutoFuncInd_cmd_DeviceIndication(&pRteInData_Common->DoorAutoFuncInd_cmd);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->DoorAutoFuncInd_cmd),(DeviceIndication_Off))
   //! ##### Read IntLghtOffModeInd_cmd interface
   pRteInData_Common->IntLghtOffModeInd_cmd.Previous = pRteInData_Common->IntLghtOffModeInd_cmd.Current;
   retValue = Rte_Read_IntLghtOffModeInd_cmd_DeviceIndication(&pRteInData_Common->IntLghtOffModeInd_cmd.Current);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->IntLghtOffModeInd_cmd.Current),( DeviceIndication_Off))
   //! ##### Read IntLightMaxModeInd_cmd interface
   retValue = Rte_Read_IntLightMaxModeInd_cmd_DeviceIndication(&pRteInData_Common->IntLightMaxModeInd_cmd);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->IntLightMaxModeInd_cmd),(DeviceIndication_Off))
   //! ##### Read IntLightNightModeInd_cmd interface
   retValue = Rte_Read_IntLightNightModeInd_cmd_DeviceIndication(&pRteInData_Common->IntLightNightModeInd_cmd);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->IntLightNightModeInd_cmd),(DeviceIndication_Off))
   //! ##### Read IntLightRestingModeInd_cmd interface
   retValue = Rte_Read_IntLightRestingModeInd_cmd_DeviceIndication(&pRteInData_Common->IntLightRestingModeInd_cmd);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->IntLightRestingModeInd_cmd),(DeviceIndication_Off))
   //! ##### Read LIN_DoorAutoFuncBtn_stat interface
   pRteInData_Common->LIN_DoorAutoFuncBtn_stat.Previous = pRteInData_Common->LIN_DoorAutoFuncBtn_stat.Current;
   retValue = Rte_Read_LIN_DoorAutoFuncBtn_stat_PushButtonStatus(&pRteInData_Common->LIN_DoorAutoFuncBtn_stat.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->LIN_DoorAutoFuncBtn_stat.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))
   //! ##### Read LIN_IntLghtDimmingLvlDecBtn_s interface
   pRteInData_Common->LIN_IntLghtDimmingLvlDecBtn_s.Previous = pRteInData_Common->LIN_IntLghtDimmingLvlDecBtn_s.Current;
   retValue = Rte_Read_LIN_IntLghtDimmingLvlDecBtn_s_PushButtonStatus(&pRteInData_Common->LIN_IntLghtDimmingLvlDecBtn_s.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->LIN_IntLghtDimmingLvlDecBtn_s.Current),( PushButtonStatus_NotAvailable),(PushButtonStatus_Error))
   //! ##### Read LIN_IntLghtDimmingLvlIncBtn_s interface
   pRteInData_Common->LIN_IntLghtDimmingLvlIncBtn_s.Previous = pRteInData_Common->LIN_IntLghtDimmingLvlIncBtn_s.Current;
   retValue = Rte_Read_LIN_IntLghtDimmingLvlIncBtn_s_PushButtonStatus(&pRteInData_Common->LIN_IntLghtDimmingLvlIncBtn_s.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->LIN_IntLghtDimmingLvlIncBtn_s.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))   
   //! ##### Read IsUpdated_LIN_IntLghtModeSelrFreeWheel interface
   pRteInData_Common->IsUpdated_LIN_IntLghtModeSelrFreeWheel = Rte_IsUpdated_LIN_IntLghtModeSelrFreeWheel_s_FreeWheel_Status();
   //! ##### Read LIN_IntLghtModeSelrFreeWheel_s interface
   pRteInData_Common->LIN_IntLghtModeSelrFreeWheel_s.Previous = pRteInData_Common->LIN_IntLghtModeSelrFreeWheel_s.Current;
   retValue = Rte_Read_LIN_IntLghtModeSelrFreeWheel_s_FreeWheel_Status(&pRteInData_Common->LIN_IntLghtModeSelrFreeWheel_s.Current); 
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->LIN_IntLghtModeSelrFreeWheel_s.Current),( FreeWheel_Status_NotAvailable),(FreeWheel_Status_Error))
   //! ##### Read ComMode_LIN1 interface
   retValue = Rte_Read_ComMode_LIN1_ComMode_LIN(&pRteInData_Common->ComMode_LIN1);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->ComMode_LIN1),( Error))
   //! ##### Read isDiagActive interface
   retValue = Rte_Read_DiagActiveState_isDiagActive(&pRteInData_Common->isDiagActive);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->isDiagActive),(Diag_Active_FALSE))
   //! ##### Read ILCP1_ResponseError interface
   retValue_InteriorLight1 = Rte_Read_ResponseErrorILCP1_ResponseErrorILCP1(&pRteInData_Common->ILCP1_ResponseError);
   MACRO_StdRteRead_ExtRPort((retValue_InteriorLight1), (pRteInData_Common->ILCP1_ResponseError), (1U), (1U))

   return retValue_InteriorLight1;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'RteDataWrite_Common'
//!
//! \param   *pRteOutData_Common                        Update the output signals to ports of output structure
//! \param   *pIsUpdated_LIN_IntLghtModeSelrFreeWheel   Provide and update the IsUpdated_LIN_IntLghtModeSelrFreeWheel
//!
//!======================================================================================
static void RteDataWrite_Common(const InteriorLightPanel1LINMastCtrl_OutData_T    *pRteOutData_Common,
                                      boolean                                     *pIsUpdated_LIN_IntLghtModeSelrFreeWheel)									
{
   //! ###### Processing RTE data write common logic
   Rte_Write_DoorAutoFuncBtn_stat_PushButtonStatus(pRteOutData_Common->DoorAutoFuncBtn_stat);
   Rte_Write_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus(pRteOutData_Common->IntLghtDimmingLvlDecBtn_stat);
   Rte_Write_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus(pRteOutData_Common->IntLghtDimmingLvlIncBtn_stat);
   Rte_Write_LIN_DoorAutoFuncInd_cmd_DeviceIndication(pRteOutData_Common->LIN_DoorAutoFuncInd_cmd);
   Rte_Write_LIN_IntLghtOffModeInd_cmd_DeviceIndication(pRteOutData_Common->LIN_IntLghtOffModeInd_cmd);
   Rte_Write_LIN_IntLightMaxModeInd_cmd_DeviceIndication(pRteOutData_Common->LIN_IntLightMaxModeInd_cmd);
   Rte_Write_LIN_IntLightNightModeInd_cmd_DeviceIndication(pRteOutData_Common->LIN_IntLightNightModeInd_cmd);
   Rte_Write_LIN_IntLightRestingModeInd_cmd_DeviceIndication(pRteOutData_Common->LIN_IntLightRestingModeInd_cmd);
   //! ###### Processing the free wheel update logic
   if (TRUE == *pIsUpdated_LIN_IntLghtModeSelrFreeWheel)
   {
       *pIsUpdated_LIN_IntLghtModeSelrFreeWheel          = FALSE;
        Rte_Write_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status(pRteOutData_Common->IntLghtModeSelrFreeWheel_stat);
   }
   else if ((FreeWheel_Status_Error       == pRteOutData_Common->IntLghtModeSelrFreeWheel_stat)
           || (FreeWheel_Status_NotAvailable == pRteOutData_Common->IntLghtModeSelrFreeWheel_stat))
   {
      Rte_Write_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status(pRteOutData_Common->IntLghtModeSelrFreeWheel_stat);   
   }
   else
   {
      // LIN_IntLghtDimmingLvlFreeWhl_s is not send to the output signal
   } 
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'Fallback_Logic_InteriorLightPanel1LINMastCtrl'
//!
//! \param   *pDoorAutoFuncBtn_stat            Update the DoorAutoFunc button status to error
//! \param   *pIntLghtDimmingLvlDecBtn_stat    Update the IntLghtDimmingLvlDec button status to error
//! \param   *pIntLghtDimmingLvlIncBtn_stat    Update the IntLghtDimmingLvlInc button status to error
//! \param   *pIntLghtModeSelrFreeWheel_stat   Update the IntLghtModeSelrFree wheel status to error
//!
//!======================================================================================
static  void Fallback_Logic_InteriorLightPanel1LINMastCtrl(PushButtonStatus_T   *pDoorAutoFuncBtn_stat,
                                                           PushButtonStatus_T   *pIntLghtDimmingLvlDecBtn_stat,
                                                           PushButtonStatus_T   *pIntLghtDimmingLvlIncBtn_stat,
                                                           FreeWheel_Status_T   *pIntLghtModeSelrFreeWheel_stat)
{
   //! ###### Write all the output ports to value 'error'
   *pDoorAutoFuncBtn_stat          = PushButtonStatus_Error;
   *pIntLghtDimmingLvlDecBtn_stat  = PushButtonStatus_Error;
   *pIntLghtDimmingLvlIncBtn_stat  = PushButtonStatus_Error;
   *pIntLghtModeSelrFreeWheel_stat = FreeWheel_Status_Error;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'Deactivate_Logic_InteriorLightPanel1LINMastCtrl'
//!
//! \param   *pDoorAutoFuncBtn_stat            Update the DoorAutoFunc button status to NotAvailable
//! \param   *pIntLghtDimmingLvlDecBtn_stat    Update the IntLghtDimmingLvlDec button status to NotAvailable
//! \param   *pIntLghtDimmingLvlIncBtn_stat    Update the IntLghtDimmingLvlInc button status to NotAvailable
//! \param   *pIntLghtModeSelrFreeWheel_stat   Update the IntLghtModeSelrFree wheel status to NotAvailable
//!   
//!======================================================================================
static void Deactivate_Logic_InteriorLightPanel1LINMastCtrl(PushButtonStatus_T  *pDoorAutoFuncBtn_stat,
                                                            PushButtonStatus_T  *pIntLghtDimmingLvlDecBtn_stat,
                                                            PushButtonStatus_T  *pIntLghtDimmingLvlIncBtn_stat,
                                                            FreeWheel_Status_T  *pIntLghtModeSelrFreeWheel_stat)
{
   //! ###### Write all the output ports to value 'not available'
   *pDoorAutoFuncBtn_stat          = PushButtonStatus_NotAvailable;
   *pIntLghtDimmingLvlDecBtn_stat  = PushButtonStatus_NotAvailable;
   *pIntLghtDimmingLvlIncBtn_stat  = PushButtonStatus_NotAvailable;
   *pIntLghtModeSelrFreeWheel_stat = FreeWheel_Status_NotAvailable;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'Gateway_Logic_InteriorLightPanel1LINMastCtrl'
//!
//! \param     *pIn_Gateway      Providing the input signals to send to output ports
//! \param     *pOut_Gateway     Updating the output ports with input signals
//!
//!======================================================================================

static void Gateway_Logic_InteriorLightPanel1LINMastCtrl(const InteriorLightPanel1LINMastCtrl_InData_T   *pIn_Gateway,
                                                               InteriorLightPanel1LINMastCtrl_OutData_T  *pOut_Gateway)
{
   //! ###### Processing the gateway logic
   //! ##### Write the output ports with values read on input ports accordingly 
   pOut_Gateway->DoorAutoFuncBtn_stat           = pIn_Gateway->LIN_DoorAutoFuncBtn_stat.Current;
   pOut_Gateway->IntLghtDimmingLvlDecBtn_stat   = pIn_Gateway->LIN_IntLghtDimmingLvlDecBtn_s.Current;
   pOut_Gateway->IntLghtDimmingLvlIncBtn_stat   = pIn_Gateway->LIN_IntLghtDimmingLvlIncBtn_s.Current;
   pOut_Gateway->IntLghtModeSelrFreeWheel_stat  = pIn_Gateway->LIN_IntLghtModeSelrFreeWheel_s.Current;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'Generic_ANW_OtherInteriorLights2_Logic'
//!
//! \param     isRestAct       Provides 'yes' or 'no' values based on restriction activation
//! \param     isActTrigDet    Provides 'yes' or 'no' values based on activation trigger
//!
//!======================================================================================
static void Generic_ANW_OtherInteriorLights2_Logic(const FormalBoolean isRestAct,
                                                   const FormalBoolean isActTrigDet)
{
   //! ###### Processing the application network (activation/deactivation)conditions of OtherInteriorLights2
   static AnwSM_States AnwState_OtherIntLights                    = { SM_ANW_Inactive,
                                                                      SM_ANW_Inactive };
   static DeactivationTimer_Type DeactivationTimer_OtherIntLight  = 0U;
   AnwAction_Enum AnwActionstatus                                 = ANW_Action_None;

   //! ##### Process ANW logic : 'ProcessAnwLogic()'
   AnwActionstatus = ProcessAnwLogic(isRestAct,
                                     isActTrigDet,
                                     YES,
                                     CONST_AnwOtherIntLightDeactTimeout,
                                     &DeactivationTimer_OtherIntLight,
                                     &AnwState_OtherIntLights);
   //! ##### Check for Anw action is 'Activate' or 'Deactivate' for OtherInteriorLights2
   if (ANW_Action_Activate == AnwActionstatus)
   {
      //Service call
      Rte_Call_UR_ANW_OtherInteriorLights2_ActivateIss();
   }
   else if (ANW_Action_Deactivate == AnwActionstatus)
   {
      // Service call
      Rte_Call_UR_ANW_OtherInteriorLights2_DeactivateIss();
   }
   else
   {
      // Do nothing keep previous status
   }
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'Generic_ANW_OtherInteriorLight2_CondCheck'
//!
//! \param   *pLIN_DoorAutoFuncBtn_stat        Provide the DoorAutoFunc input button status
//! \param   *pLIN_IntLghtDimmingLvlDecBtn_s   Provide the IntLghtDimmingLvlDec input button status
//! \param   *pLIN_IntLghtDimmingLvlIncBtn_s   Provide the IntLghtDimmingLvlInc input button status
//! \param   *pLIN_IntLghtModeSelrFreeWheel_s  Provide the IntLghtModeSelrFree wheel status
//! \param   *pIntLghtOffModeInd_cmd           Provide the IntLghtOffMode indication status
//!
//! \return    FormalBoolean      Returns 'isActTrigDet' value
//!
//!======================================================================================
static FormalBoolean Generic_ANW_OtherInteriorLight2_CondCheck(const PushButtonStatus     *pLIN_DoorAutoFuncBtn_stat,
                                                               const PushButtonStatus     *pLIN_IntLghtDimmingLvlDecBtn_s,
                                                               const PushButtonStatus     *pLIN_IntLghtDimmingLvlIncBtn_s,
                                                               const FreeWheel_Status     *pLIN_IntLghtModeSelrFreeWheel_s,
                                                               const DeviceIndication     *pIntLghtOffModeInd_cmd)
{
   FormalBoolean isActTrigDet = NO;
   //! ###### Check for OtherInteriorLights2 conditions and return the trigger detection status
   if (((pLIN_DoorAutoFuncBtn_stat->Previous != pLIN_DoorAutoFuncBtn_stat->Current)
      && (PushButtonStatus_Pushed == pLIN_DoorAutoFuncBtn_stat->Current))
      || ((pLIN_IntLghtDimmingLvlDecBtn_s->Previous!= pLIN_IntLghtDimmingLvlDecBtn_s->Current)
      && (PushButtonStatus_Pushed == pLIN_IntLghtDimmingLvlDecBtn_s->Current))
      || ((pLIN_IntLghtDimmingLvlIncBtn_s->Previous != pLIN_IntLghtDimmingLvlIncBtn_s->Current)
      && (PushButtonStatus_Pushed == pLIN_IntLghtDimmingLvlIncBtn_s->Current))
      || ((pLIN_IntLghtModeSelrFreeWheel_s->Previous!= pLIN_IntLghtModeSelrFreeWheel_s->Current)
      && ((FreeWheel_Status_NoMovement != pLIN_IntLghtModeSelrFreeWheel_s->Current)
      && (FreeWheel_Status_Spare != pLIN_IntLghtModeSelrFreeWheel_s->Current)
      && (FreeWheel_Status_Error != pLIN_IntLghtModeSelrFreeWheel_s->Current)
      && (FreeWheel_Status_NotAvailable != pLIN_IntLghtModeSelrFreeWheel_s->Current)))
      || ((DeviceIndication_On == pIntLghtOffModeInd_cmd->Previous)
      && (DeviceIndication_Off == pIntLghtOffModeInd_cmd->Current)))
   {
      isActTrigDet = YES;
   }
   else
   {
      isActTrigDet = NO;
   }
   return isActTrigDet;
}

//! @}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
