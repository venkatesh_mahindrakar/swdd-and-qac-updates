//!======================================================================================
//! \file FuncLibrary_AnwStateMachine.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup LowPowerModeProcessing
//! @{
//! @addtogroup AnwStateMachine
//! @{
//!
//! \brief
//! FuncLibrary_AnwStateMachine SW module.
//! ASIL Level : QM.
//! This module implements the logic for the common behavior of ANW mechanisms
//!======================================================================================

//=======================================================================================
// Included header files
//=======================================================================================
#include "Platform_Types.h"
#include "FuncLibrary_AnwStateMachine.h"
#include "FuncLibrary_AnwStateMachine_If.h"

//!======================================================================================
//!
//! \brief
//! Implementation of the ANW logic according to LocalECU_Req-1 v1
//!
//! This function implements the ANW State Machine. At state transitions
//! the function returns a request to the caller to perform a specific action
//! (such as activating or deactivating an ANW). It is the callers responsibility
//! to act upon this request.
//! 
//! State transitions are controlled by the input parameters isRestrictionActivated,
//! isActivationTriggerDetected, isDeactivationConditionsFulfilled, and 
//! DeactivationTimeout according to the state machine.
//! 
//! Parameters:
//! \param isRestrictionActivated Status of restriction conditions
//! \param isActivationTriggerDetected Status of activation triggers
//! \param isDeactivationConditionsFulfilled Status of conditional deactivation
//! \param DeactivationTimeout Value of unconditional deactivation timer to process
//! \param DeactivationTimer Unconditional deactivation timer to process
//! \param AnwState ANW reference
//!
//! \return Updated ANW status
//!
//!======================================================================================

AnwAction_Enum ProcessAnwLogic(const FormalBoolean isRestrictionActivated,
                                 const FormalBoolean isActivationTriggerDetected,
                                 const FormalBoolean isDeactivationConditionsFulfilled,
                                 const DeactivationTimer_Type  DeactivationTimeout,
                                 DeactivationTimer_Type  *DeactivationTimer,
                                 AnwSM_States *AnwState)
{
   AnwAction_Enum anw_Action = ANW_Action_None;

   //! ###### Verify the triggers to change the ANW state
   
   //! ##### Verify if restriction conditions active
   if (YES != isRestrictionActivated)
   {
      //! ##### Check the ANW state change triggers and conditions
      switch (AnwState->Current)
      {
         case SM_ANW_Active:
            //! #### In active state: verify if deactivation conditions fulfilled
            if (YES == isDeactivationConditionsFulfilled)
            {
               AnwState->New = SM_ANW_Deactivation;
            }
            else
            {
               // do nothing: keep ANW active
            }
         break;
         case SM_ANW_Deactivation:
            //! #### In deactivation phase: verify if re-activation requested or deactivation can be completed
            if (YES == isActivationTriggerDetected)
            {
               AnwState->New = SM_ANW_Active;
            }
            else if (0 == *DeactivationTimer)
            {
               AnwState->New = SM_ANW_Inactive;
            }
            else
            {
               *DeactivationTimer = *DeactivationTimer - 1;
            }
         break;
         default: //SM_ANW_Inactive
            //! #### In inactive state: verify if activation triggers detected
            if (YES == isActivationTriggerDetected)
            {
               AnwState->New = SM_ANW_Active;
            }
            else
            {
               // do nothing: no ANW triggered
            }
         break;
      }
   }
   else
   {
      AnwState->New = SM_ANW_Inactive;
   }

   //! ###### Process the transition event actions on ANW state
   if (AnwState->New != AnwState->Current)
   {
      switch (AnwState->New)
      {
         case SM_ANW_Active:
            // request the ANW_ISS activation
            anw_Action = ANW_Action_Activate;
         break;
         case SM_ANW_Deactivation:
            // Initialize deactivation timeout
            *DeactivationTimer = DeactivationTimeout;
         break;
         default: //SM_ANW_Inactive
            // request the ANW_ISS deactivation
            anw_Action = ANW_Action_Deactivate;
         break;
      }
   }
   else
   {
      // Keep anw_Action default: no action
   }

   AnwState->Current = AnwState->New;

   return anw_Action;
}

//! @}
//! @}

//=======================================================================================
// End of file
//=======================================================================================