/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  InteriorLightPanel_2_LINMastCtrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  InteriorLightPanel_2_LINMastCtrl
 *  Generated at:  Fri Jun 12 17:01:42 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <InteriorLightPanel_2_LINMastCtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file InteriorLightPanel_2_LINMastCtrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_BodyAndComfort\Application_Living
//! @{
//! @addtogroup InteriorLightPanel_2_LINMastCtrl
//! @{
//!
//! \brief
//! InteriorLightPanel_2_LINMastCtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the InteriorLightPanel_2_LINMastCtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_InteriorLightPanel_2_LINMastCtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "FuncLibrary_AnwStateMachine_If.h"
#include "InteriorLightPanel_2_LINMastCtrl.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DiagInfo_T: Integer in interval [0...127]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * ResponseErrorILCP2_T: Boolean
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Spare1 (4U)
 *   Spare2 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 *   FreeWheel_Status_NoMovement (0U)
 *   FreeWheel_Status_1StepClockwise (1U)
 *   FreeWheel_Status_2StepsClockwise (2U)
 *   FreeWheel_Status_3StepsClockwise (3U)
 *   FreeWheel_Status_4StepsClockwise (4U)
 *   FreeWheel_Status_5StepsClockwise (5U)
 *   FreeWheel_Status_6StepsClockwise (6U)
 *   FreeWheel_Status_1StepCounterClockwise (7U)
 *   FreeWheel_Status_2StepsCounterClockwise (8U)
 *   FreeWheel_Status_3StepsCounterClockwise (9U)
 *   FreeWheel_Status_4StepsCounterClockwise (10U)
 *   FreeWheel_Status_5StepsCounterClockwise (11U)
 *   FreeWheel_Status_6StepsCounterClockwise (12U)
 *   FreeWheel_Status_Spare (13U)
 *   FreeWheel_Status_Error (14U)
 *   FreeWheel_Status_NotAvailable (15U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1VR2_ILCP2_Installed_v(void)
 *
 *********************************************************************************************************************/


#define InteriorLightPanel_2_LINMastCtrl_START_SEC_CODE
#include "InteriorLightPanel_2_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_ILCP2_Installed   (Rte_Prm_P1VR2_ILCP2_Installed_v())
/**********************************************************************************************************************
 *
 * Runnable Entity Name: InteriorLightPanel_2_LINMastCtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DiagInfoILCP2_DiagInfo(DiagInfo_T *data)
 *   Std_ReturnType Rte_Read_LIN_IntLghtCenterBtnFreeWhl_s_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_LIN_IntLghtMaxModeBtnPnl2_s_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_IntLghtNightModeBt2_s_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_IntLghtRestModeBtnPnl2_s_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_ResponseErrorILCP2_ResponseErrorILCP2(ResponseErrorILCP2_T *data)
 *   boolean Rte_IsUpdated_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status(void)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status(FreeWheel_Status_T data)
 *   Std_ReturnType Rte_Write_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_IntLghtNightModeBtn2_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_IntLghtRestModeBtnPnl2_stat_PushButtonStatus(PushButtonStatus_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BN4_16_ILCP_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN4_17_ILCP_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN4_44_ILCP_RAM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN4_45_ILCP_FLASH_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN4_46_ILCP_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN4_49_ILCP_HWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN4_94_ILCP_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F1F_87_ILCP2Link_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_DimmingAdjustment2_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_DimmingAdjustment2_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights3_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights3_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: InteriorLightPanel_2_LINMastCtrl_20ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution runnable logic for the InteriorLightPanel_2_LINMastCtrl_20ms_runnable
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, InteriorLightPanel_2_LINMastCtrl_CODE) InteriorLightPanel_2_LINMastCtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: InteriorLightPanel_2_LINMastCtrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   // Define input RteInData Common 
   static InteriorLightPanel2LINMastCtrl_InData_T  RteInData_Common;
   // Define input RteOutData Common 
   static InteriorLightPanel2LINMastCtrl_OutData_T RteOutData_Common = { PushButtonStatus_NotAvailable,
                                                                         FreeWheel_Status_NotAvailable,
                                                                         PushButtonStatus_NotAvailable,
                                                                         PushButtonStatus_NotAvailable,
                                                                         PushButtonStatus_NotAvailable };

   // Local logic variables 
   FormalBoolean  isActTrigDet_OtherInteriorLights3 = NO;
   FormalBoolean  isActTrigDet_DimmingAdjustment2   = NO;
   FormalBoolean  isRestAct_OtherInteriorLights3    = YES;
   FormalBoolean  isRestAct_DimmingAdjustment2      = YES;
   Std_ReturnType rteReturnRespErrILCP2             = RTE_E_INVALID;
   boolean        IsFciFaultActive                  = CONST_Fault_InActive;

   //! ###### Process the RTE read common logic: 'Get_RteDataRead_Common()'
   rteReturnRespErrILCP2 = Get_RteDataRead_Common(&RteInData_Common);
   //! ###### Check for communication mode of LIN4 is error (LIN Busoff)
   if (Error == RteInData_Common.ComMode_LIN4)
   { 
      //! ##### Process the fallback mode logic: 'FallbackLogic_InteriorLightPanel2LINMastCtrl()'
      FallbackLogic_InteriorLightPanel2LINMastCtrl(&RteOutData_Common);
   }
   else
   {
      //! ###### Select 'ILCP2_Installed' parameter
      if (TRUE == PCODE_ILCP2_Installed)
      {  
         //! ##### Check for communication mode of LIN4 status
         if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN4)
            || (SwitchDetection == RteInData_Common.ComMode_LIN4)
            || (Diagnostic == RteInData_Common.ComMode_LIN4))
         {
            isRestAct_OtherInteriorLights3 = NO;
            //! ##### Select 'CONST_HmiArchitecture' parameter
            if (TRUE == CONST_HmiArchitecture)         //HmiArchitecture calibration missing
            {
               isRestAct_DimmingAdjustment2 = NO;      //need confirmation
            }
            else
            {
               // Do nothing
            }          
            //! ##### Check for response error ILCP2 status (missing frame condition)
            if (RTE_E_MAX_AGE_EXCEEDED == rteReturnRespErrILCP2)
            {      
               //! ##### Check for communication mode of LIN4 is 'applicationmonitoring' or 'switchdetection'
               if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN4)
                  || (SwitchDetection == RteInData_Common.ComMode_LIN4))
               {
                  //! #### Invoke 'Rte_Call_Event_D1F1F_87_ILCP2Link_NoResp_SetEventStatus' with status 'failed'
                  Rte_Call_Event_D1F1F_87_ILCP2Link_NoResp_SetEventStatus(DEM_EVENT_STATUS_FAILED);
                  //! #### Process the fallback mode logic: 'FallbackLogic_InteriorLightPanel2LINMastCtrl()'
                  FallbackLogic_InteriorLightPanel2LINMastCtrl(&RteOutData_Common);
               }
               else
               {
                  //! ##### If LIN communication mode is in 'diagnostic' mode
                  //! #### Process the signals gateway logic: 'Signals_gateway_Logic_InteriorLightPanel2LINMastCtrl()'
                  Signals_gateway_Logic_InteriorLightPanel2LINMastCtrl(&RteInData_Common,
                                                                       &RteOutData_Common);
               }
            }
            //! ##### Check for response error ILCP2 is RTE_E_OK or RTE_E_COM_STOPPED or RTE_E_NEVER_RECEIVED
            else if ((RTE_E_OK == rteReturnRespErrILCP2)
                    || (RTE_E_COM_STOPPED == rteReturnRespErrILCP2)
                    || (RTE_E_NEVER_RECEIVED == rteReturnRespErrILCP2))
            {
               //! ##### Invoke 'Rte_Call_Event_D1F1F_87_ILCP2Link_NoResp_SetEventStatus' with status 'passed'(Remove the LIN missing frame fault)
               Rte_Call_Event_D1F1F_87_ILCP2Link_NoResp_SetEventStatus(DEM_EVENT_STATUS_PASSED);
               //! ##### Process fault code information(FCI) indication: 'FCI_Indication_InteriorLightPanel2LINMastCtrl()'
               IsFciFaultActive = FCI_Indication_InteriorLightPanel2LINMastCtrl(&RteInData_Common.DiagInfoILCP2_LIN4);
               //! #### Check for 'IsFciFaultActive' is active or not
               if (CONST_Fault_Active == IsFciFaultActive)
               {
                  //! #### Process fallback mode logic: 'FallbackLogic_InteriorLightPanel2LINMastCtrl()'
                  FallbackLogic_InteriorLightPanel2LINMastCtrl(&RteOutData_Common);
               }
               else
               {
                  //! #### Process the signals gateway logic: 'Signals_gateway_Logic_InteriorLightPanel2LINMastCtrl()'
                  Signals_gateway_Logic_InteriorLightPanel2LINMastCtrl(&RteInData_Common,
                                                                       &RteOutData_Common);
               }
            }
            else
            {
               //! ##### Process the fallback mode logic: 'FallbackLogic_InteriorLightPanel2LINMastCtrl()'
               FallbackLogic_InteriorLightPanel2LINMastCtrl(&RteOutData_Common);
            }
         }
         else
         {
            //! ##### Process deactivation logic: 'Deactivation_Logic_InteriorLightPanel2LINMastCtrl()'
            Deactivation_Logic_InteriorLightPanel2LINMastCtrl(&RteOutData_Common);
         }
      }
      else
      {
         //! ###### Process deactivation logic: 'Deactivation_Logic_InteriorLightPanel2LINMastCtrl()'
         Deactivation_Logic_InteriorLightPanel2LINMastCtrl(&RteOutData_Common);
      }
   }
   //! ###### Process other interior lights3 trigger logic: 'Generic_ANW_OtherInteriorLights3_Cond()'
   isActTrigDet_OtherInteriorLights3 = Generic_ANW_OtherInteriorLights3_Cond(&RteInData_Common);
   //! ###### Activating application networks by other interior lights3 condition: 'GenericANW_OtherInteriorLights3()'
   GenericANW_OtherInteriorLights3(isRestAct_OtherInteriorLights3,
                                   isActTrigDet_OtherInteriorLights3);
   //! ###### Process dimming adjustment trigger logic: 'Generic_ANW_DimmingAdj2_CondCheck()'
   isActTrigDet_DimmingAdjustment2 = Generic_ANW_DimmingAdj2_CondCheck(&RteInData_Common.LIN_IntLghtDimmingLvlFreeWhl_s);
   //! ###### Activating application networks by dimming adjustment condition logic: 'Generic_ANW_DimmingAdjustment2_Logic()'
   Generic_ANW_DimmingAdjustment2_Logic(isRestAct_DimmingAdjustment2,
                                  isActTrigDet_DimmingAdjustment2);
   //! ###### Process RTE output data write common logic: 'RteDataWrite_Common()'
   RteDataWrite_Common(&RteOutData_Common,
                       &RteInData_Common.IsUpdated_LIN_IntLghtDimmingLvl);
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define InteriorLightPanel_2_LINMastCtrl_STOP_SEC_CODE
#include "InteriorLightPanel_2_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'RteDataWrite_Common'
//!
//! \param    *pRteOutData_Common                 Update the output signals to ports of output structure
//! \param    *pIsUpdated_LIN_IntLghtDimmingLvl   Provide and update the IsUpdated_LIN_IntLghtDimmingLvl status
//!
//!======================================================================================
static void RteDataWrite_Common(const InteriorLightPanel2LINMastCtrl_OutData_T *pRteOutData_Common,
                                      boolean                                  *pIsUpdated_LIN_IntLghtDimmingLvl)
{
   //! ###### Write all the output ports
   Rte_Write_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus(pRteOutData_Common->IntLghtCenterBtnFreeWhl_stat);
   Rte_Write_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus(pRteOutData_Common->IntLghtMaxModeBtnPnl2_stat);
   Rte_Write_IntLghtNightModeBtn2_stat_PushButtonStatus(pRteOutData_Common->IntLghtNightModeBtn2_stat);
   Rte_Write_IntLghtRestModeBtnPnl2_stat_PushButtonStatus(pRteOutData_Common->IntLghtRestModeBtnPnl2_stat);
   //! ###### Processing the free wheel update logic
   if (TRUE == *pIsUpdated_LIN_IntLghtDimmingLvl)
   {
      *pIsUpdated_LIN_IntLghtDimmingLvl          = FALSE;
       Rte_Write_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status(pRteOutData_Common->IntLghtDimmingLvlFreeWhl_stat);
   }
   else if ((FreeWheel_Status_Error       == pRteOutData_Common->IntLghtDimmingLvlFreeWhl_stat)
           || (FreeWheel_Status_NotAvailable == pRteOutData_Common->IntLghtDimmingLvlFreeWhl_stat))
   {
      Rte_Write_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status(pRteOutData_Common->IntLghtDimmingLvlFreeWhl_stat);
   }
   else
   {
      // LIN_IntLghtDimmingLvlFreeWhl_s is not send to the output signal
   }
}
//!======================================================================================
//!
//! \brief
//!  This function implements the logic for the 'Get_RteDataRead_Common'
//!
//! \param    *pRteInData_Common   Examine and update the input signals based on RTE failure events
//!
//! \return   Std_ReturnType      Returns 'rteReturnRespErrILCP2' Variable value
//!
//!======================================================================================
static Std_ReturnType Get_RteDataRead_Common(InteriorLightPanel2LINMastCtrl_InData_T *pRteInData_Common)
{
   Std_ReturnType retValue              = RTE_E_INVALID;
   Std_ReturnType rteReturnRespErrILCP2 = RTE_E_INVALID;

   //! ###### Read common RTE interfaces, store the previous value and process fallback modes
   //! ##### Read ComMode_LIN4 interface
   retValue = Rte_Read_ComMode_LIN4_ComMode_LIN(&pRteInData_Common->ComMode_LIN4);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->ComMode_LIN4),(Error))
   //! ##### Read DiagInfoILCP2_LIN4 interface
   retValue = Rte_Read_DiagInfoILCP2_DiagInfo(&pRteInData_Common->DiagInfoILCP2_LIN4);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->DiagInfoILCP2_LIN4),(0U),(0U))
   pRteInData_Common->LIN_IntLghtCenterBtnFreeWhl_s.Previous = pRteInData_Common->LIN_IntLghtCenterBtnFreeWhl_s.Current;
   //! ##### Read LIN_IntLghtCenterBtnFreeWhl_s interface
   retValue = Rte_Read_LIN_IntLghtCenterBtnFreeWhl_s_PushButtonStatus(&pRteInData_Common->LIN_IntLghtCenterBtnFreeWhl_s.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->LIN_IntLghtCenterBtnFreeWhl_s.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))
   //! ##### Read IsUpdated_LIN_IntLghtDimmingLvlFreeWhl_s interface
   pRteInData_Common->IsUpdated_LIN_IntLghtDimmingLvl = Rte_IsUpdated_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status();
   //! ##### Read LIN_IntLghtDimmingLvlFreeWhl_s interface
   pRteInData_Common->LIN_IntLghtDimmingLvlFreeWhl_s.Previous = pRteInData_Common->LIN_IntLghtDimmingLvlFreeWhl_s.Current;
   retValue = Rte_Read_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status(&pRteInData_Common->LIN_IntLghtDimmingLvlFreeWhl_s.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->LIN_IntLghtDimmingLvlFreeWhl_s.Current),(FreeWheel_Status_NotAvailable),(FreeWheel_Status_Error))   
   pRteInData_Common->LIN_IntLghtMaxModeBtnPnl2_s.Previous = pRteInData_Common->LIN_IntLghtMaxModeBtnPnl2_s.Current;
   //! ##### Read LIN_IntLghtMaxModeBtnPnl2_s interface
   retValue = Rte_Read_LIN_IntLghtMaxModeBtnPnl2_s_PushButtonStatus(&pRteInData_Common->LIN_IntLghtMaxModeBtnPnl2_s.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->LIN_IntLghtMaxModeBtnPnl2_s.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))
   pRteInData_Common->LIN_IntLghtNightModeBt2_s.Previous = pRteInData_Common->LIN_IntLghtNightModeBt2_s.Current;
   //! ##### Read LIN_IntLghtNightModeBt2_s interface
   retValue = Rte_Read_LIN_IntLghtNightModeBt2_s_PushButtonStatus(&pRteInData_Common->LIN_IntLghtNightModeBt2_s.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->LIN_IntLghtNightModeBt2_s.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))
   pRteInData_Common->LIN_IntLghtRestModeBtnPnl2_s.Previous = pRteInData_Common->LIN_IntLghtRestModeBtnPnl2_s.Current;
   //! ##### Read LIN_IntLghtRestModeBtnPnl2_s interface
   retValue = Rte_Read_LIN_IntLghtRestModeBtnPnl2_s_PushButtonStatus(&pRteInData_Common->LIN_IntLghtRestModeBtnPnl2_s.Current);
   MACRO_StdRteRead_ExtRPort((retValue),( pRteInData_Common->LIN_IntLghtRestModeBtnPnl2_s.Current),(PushButtonStatus_NotAvailable),( PushButtonStatus_Error))
   //! ##### Read ILCP2_ResponseError interface
   rteReturnRespErrILCP2 = Rte_Read_ResponseErrorILCP2_ResponseErrorILCP2(&pRteInData_Common->ILCP2_ResponseError);
   MACRO_StdRteRead_ExtRPort((rteReturnRespErrILCP2),(pRteInData_Common->ILCP2_ResponseError),(0U),(0U))

   return rteReturnRespErrILCP2;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Generic_ANW_DimmingAdjustment2_Logic'
//!
//! \param     isRestAct_DimmingAdjustment2      Provides 'yes' or 'no' values based on restriction activation
//! \param     isActTrigDet_DimmingAdjustment2   Provides 'yes' or 'no' values based on activation trigger
//!
//!======================================================================================
static void Generic_ANW_DimmingAdjustment2_Logic(const FormalBoolean isRestAct_DimmingAdjustment2,
                                                 const FormalBoolean isActTrigDet_DimmingAdjustment2)
{
   //! ###### Processing the application network (activation/deactivation)conditions of DimmingAdjustment
   static AnwSM_States AnwState_DimingAdj                     = { SM_ANW_Inactive,
                                                                 SM_ANW_Inactive };
   static DeactivationTimer_Type DimmingAdj_DeactivationTimer = 0U;
   AnwAction_Enum AnwActionstatus                             = ANW_Action_None;

   //! ##### Process ANW logic : 'ProcessAnwLogic()'
   AnwActionstatus = ProcessAnwLogic(isRestAct_DimmingAdjustment2,
                                     isActTrigDet_DimmingAdjustment2,
                                     YES,
                                     CONST_AnwDimmingAdj_Timeout,
                                     &DimmingAdj_DeactivationTimer,
                                     &AnwState_DimingAdj);
   //! ##### Check for Anw action is 'Activate' or 'Deactivate' for DimmingAdjustment
   if (ANW_Action_Activate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_DimmingAdjustment2_ActivateIss();
   }
   else if (ANW_Action_Deactivate == AnwActionstatus)
   { 
      Rte_Call_UR_ANW_DimmingAdjustment2_DeactivateIss();
   }
   else
   {
      // Do nothing keep previous status
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Generic_ANW_DimmingAdj2_CondCheck'
//!
//! \param    *pInData_DimmingAdj   Providing current input status
//!
//! \return   FormalBoolean        returns 'isActTrigDet' variable value
//!
//!======================================================================================
static FormalBoolean Generic_ANW_DimmingAdj2_CondCheck(const FreeWheel_Status *pInData_DimmingAdj)
{
   FormalBoolean isActTrigDet = NO;
    
   //! ###### Check for dimming adjustment input status and return the trigger condtion
   if ((pInData_DimmingAdj->Previous != pInData_DimmingAdj->Current)
      && (FreeWheel_Status_NoMovement != pInData_DimmingAdj->Current)
      && (FreeWheel_Status_Spare != pInData_DimmingAdj->Current)
      && (FreeWheel_Status_Error != pInData_DimmingAdj->Current)
      && (FreeWheel_Status_NotAvailable != pInData_DimmingAdj->Current))
   {
      isActTrigDet = YES;
   }
   else
   {
      isActTrigDet = NO;
   }
   return isActTrigDet;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'GenericANW_OtherInteriorLights3'
//!
//! \param    isRestAct_OtherInteriorLights3      Provides 'yes' or 'no' values based on restriction activation
//! \param    isActTrigDet_OtherInteriorLights3   Provides 'yes' or 'no' values based on activation trigger
//!
//!======================================================================================
static void GenericANW_OtherInteriorLights3(const FormalBoolean  isRestAct_OtherInteriorLights3,
                                            const FormalBoolean  isActTrigDet_OtherInteriorLights3)
{
   //! ###### Processing the application network (activation/deactivation)conditions of OtherInteriorLights3
   static AnwSM_States AnwState_OtherInteriorLght            = { SM_ANW_Inactive,
                                                                 SM_ANW_Inactive };
   static DeactivationTimer_Type Interiror_DeactivationTimer = 0U;
   AnwAction_Enum AnwActionstatus                            = ANW_Action_None;

   //! ##### Process ANW logic : 'ProcessAnwLogic()'
   AnwActionstatus = ProcessAnwLogic(isRestAct_OtherInteriorLights3,
                                     isActTrigDet_OtherInteriorLights3,
                                     YES,
                                     CONST_AnwInteriorLights3_Timeout,
                                     &Interiror_DeactivationTimer,
                                     &AnwState_OtherInteriorLght);
   //! ##### Check for Anw action is 'Activate' or 'Deactivate' for OtherInteriorLights3
   if (ANW_Action_Activate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_OtherInteriorLights3_ActivateIss();
   }
   else if (ANW_Action_Deactivate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_OtherInteriorLights3_DeactivateIss();
   }
   else
   {
      // Do nothing keep previous value
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Generic_ANW_OtherInteriorLights3_Cond'
//!
//! \param    *pInData         Provides the input status
//!
//! \return   FormalBoolean   returns 'isActTrigDet' variable value
//!
//!======================================================================================
static FormalBoolean Generic_ANW_OtherInteriorLights3_Cond(const InteriorLightPanel2LINMastCtrl_InData_T *pInData)
{
   FormalBoolean isActTrigDet = NO;

   //! ###### Check for other interior lights 3 conditions and return the trigger condtion
   if (((pInData->LIN_IntLghtMaxModeBtnPnl2_s.Previous !=pInData->LIN_IntLghtMaxModeBtnPnl2_s.Current)
      && (PushButtonStatus_Pushed == pInData->LIN_IntLghtMaxModeBtnPnl2_s.Current))
      || ((pInData->LIN_IntLghtNightModeBt2_s.Previous !=pInData->LIN_IntLghtNightModeBt2_s.Current)
      && (PushButtonStatus_Pushed == pInData->LIN_IntLghtNightModeBt2_s.Current))
      || ((pInData->LIN_IntLghtCenterBtnFreeWhl_s.Previous !=pInData->LIN_IntLghtCenterBtnFreeWhl_s.Current)
      && (PushButtonStatus_Pushed == pInData->LIN_IntLghtCenterBtnFreeWhl_s.Current))
      || ((pInData->LIN_IntLghtRestModeBtnPnl2_s.Previous !=pInData->LIN_IntLghtRestModeBtnPnl2_s.Current)
      && (PushButtonStatus_Pushed == pInData->LIN_IntLghtRestModeBtnPnl2_s.Current))
      || ((pInData->LIN_IntLghtDimmingLvlFreeWhl_s.Previous != pInData->LIN_IntLghtDimmingLvlFreeWhl_s.Current)
      && (FreeWheel_Status_NoMovement != pInData->LIN_IntLghtDimmingLvlFreeWhl_s.Current)
      && (FreeWheel_Status_Spare != pInData->LIN_IntLghtDimmingLvlFreeWhl_s.Current)
      && (FreeWheel_Status_Error != pInData->LIN_IntLghtDimmingLvlFreeWhl_s.Current)
      && (FreeWheel_Status_NotAvailable != pInData->LIN_IntLghtDimmingLvlFreeWhl_s.Current)))
   {
      isActTrigDet = YES;
   }
   else
   {
      isActTrigDet = NO;
   }
   return isActTrigDet;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Signals_gateway_Logic_InteriorLightPanel2LINMastCtrl'
//!
//! \param    *pInData_GW    Providing the input signals to send to output ports
//! \param    *pOutData_GW   Updating the output ports with input signals
//!
//!======================================================================================
static void Signals_gateway_Logic_InteriorLightPanel2LINMastCtrl(const InteriorLightPanel2LINMastCtrl_InData_T  *pInData_GW,
                                                                       InteriorLightPanel2LINMastCtrl_OutData_T *pOutData_GW)
{  
   //! ###### Processing signals gateway logic
   //! ##### Update the output ports with values read on input ports
   pOutData_GW->IntLghtCenterBtnFreeWhl_stat  = pInData_GW->LIN_IntLghtCenterBtnFreeWhl_s.Current;
   pOutData_GW->IntLghtMaxModeBtnPnl2_stat    = pInData_GW->LIN_IntLghtMaxModeBtnPnl2_s.Current;
   pOutData_GW->IntLghtNightModeBtn2_stat     = pInData_GW->LIN_IntLghtNightModeBt2_s.Current;
   pOutData_GW->IntLghtRestModeBtnPnl2_stat   = pInData_GW->LIN_IntLghtRestModeBtnPnl2_s.Current; 
   pOutData_GW->IntLghtDimmingLvlFreeWhl_stat = pInData_GW->LIN_IntLghtDimmingLvlFreeWhl_s.Current;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'FallbackLogic_InteriorLightPanel2LINMastCtrl'
//!
//! \param    *pOutData_Fallback   Update the output ports to error
//!
//!======================================================================================
static void FallbackLogic_InteriorLightPanel2LINMastCtrl(InteriorLightPanel2LINMastCtrl_OutData_T *pOutData_Fallback)
{  
   //! ###### Processing the fallback mode logic
   //! ##### Update all output ports to value 'error'
   pOutData_Fallback->IntLghtCenterBtnFreeWhl_stat  = PushButtonStatus_Error;
   pOutData_Fallback->IntLghtDimmingLvlFreeWhl_stat = FreeWheel_Status_Error;
   pOutData_Fallback->IntLghtMaxModeBtnPnl2_stat    = PushButtonStatus_Error;
   pOutData_Fallback->IntLghtNightModeBtn2_stat     = PushButtonStatus_Error;
   pOutData_Fallback->IntLghtRestModeBtnPnl2_stat   = PushButtonStatus_Error;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'Deactivation_Logic_InteriorLightPanel2LINMastCtrl'
//!
//! \param    *pOutData_DeAct   Update the output ports to NotAvailable
//!
//!======================================================================================
static void Deactivation_Logic_InteriorLightPanel2LINMastCtrl(InteriorLightPanel2LINMastCtrl_OutData_T *pOutData_DeAct)
{
   //! ###### Processing the deactivation logic
   //! ##### Update all the output ports to value 'Notavailable'
   pOutData_DeAct->IntLghtCenterBtnFreeWhl_stat  = PushButtonStatus_NotAvailable;
   pOutData_DeAct->IntLghtDimmingLvlFreeWhl_stat = FreeWheel_Status_NotAvailable;
   pOutData_DeAct->IntLghtMaxModeBtnPnl2_stat    = PushButtonStatus_NotAvailable;
   pOutData_DeAct->IntLghtNightModeBtn2_stat     = PushButtonStatus_NotAvailable;
   pOutData_DeAct->IntLghtRestModeBtnPnl2_stat   = PushButtonStatus_NotAvailable;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'FCI_Indication_InteriorLightPanel2LINMastCtrl'
//!
//! \param    *pDiagInfoILCP2_LIN4   Providing the diagnostic information of ILCP2
//!
//! \return   boolean   Returns 'IsFciFaultActive' value
//!
//!======================================================================================
static boolean FCI_Indication_InteriorLightPanel2LINMastCtrl(const DiagInfo_T* pDiagInfoILCP2_LIN4)
{
   //! ###### Process the InteriorLightPanel2 FCI_Indication logic
   static uint8 SlaveFCIFault[7] = { CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive };
   boolean IsFciFaultActive      = CONST_Fault_InActive;
   uint8 Index                   = 0U;
   //! ##### Check the FCI Conditions and Log/Remove the Faults  
   if ((DiagInfo_T)0x41 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set battery voltage low fault event to fail
      Rte_Call_Event_D1BN4_16_ILCP_VBT_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x01 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set battery low fault event to pass 
      Rte_Call_Event_D1BN4_16_ILCP_VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x42 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set battery voltage high fault event to fail
      Rte_Call_Event_D1BN4_17_ILCP_VAT_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x02 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set battery voltage high fault event to pass
      Rte_Call_Event_D1BN4_17_ILCP_VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x43 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set ROM CS fault event to fail
      SlaveFCIFault[0] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BN4_46_ILCP_EEPROM_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x03 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set ROM CS fault event to pass
      SlaveFCIFault[0] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BN4_46_ILCP_EEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x44 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set RAM check fault event to fail
      SlaveFCIFault[1] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BN4_44_ILCP_RAM_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x04 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set RAM check fault event to pass
      SlaveFCIFault[1] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BN4_44_ILCP_RAM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x45 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set watchdog fault event to fail
      SlaveFCIFault[2] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BN4_94_ILCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x05 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set watchdog fault event to pass
      SlaveFCIFault[2] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BN4_94_ILCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x46 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set overrun fault event to fail
      SlaveFCIFault[3] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BN4_94_ILCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x06 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set overrun fault event to pass
      SlaveFCIFault[3] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BN4_94_ILCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x47 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set signal fault event to fail
      SlaveFCIFault[4] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BN4_94_ILCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x07 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set signal fault event to pass
      SlaveFCIFault[4] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BN4_94_ILCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x48 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set unexpected operation fault event to fail
      SlaveFCIFault[5] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BN4_94_ILCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x08 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set unexpected operation fault event to pass
      SlaveFCIFault[5] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BN4_94_ILCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x49 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set flash fault event to fail
      SlaveFCIFault[6] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BN4_45_ILCP_FLASH_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x09 == *pDiagInfoILCP2_LIN4)
   {
      //! #### Set flash fault event to pass
      SlaveFCIFault[6] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BN4_45_ILCP_FLASH_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x00 == *pDiagInfoILCP2_LIN4)
   {
      for (Index = 0U; Index < 7U; Index++)
      {
         SlaveFCIFault[Index] = CONST_SlaveFCIFault_InActive;
      }
      Rte_Call_Event_D1BN4_45_ILCP_FLASH_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BN4_94_ILCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BN4_44_ILCP_RAM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BN4_46_ILCP_EEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BN4_17_ILCP_VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BN4_16_ILCP_VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else
   {
      // Do nothing keep previous status
   }
   IsFciFaultActive = CONST_Fault_InActive;
   for (Index = 0U; Index < 7U; Index++)
   {
      if (CONST_SlaveFCIFault_Active == SlaveFCIFault[Index])
      {
         IsFciFaultActive = CONST_Fault_Active;
      }
      else
      {
         // Do Nothing
      }
   }
   return IsFciFaultActive;
}

//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#if 0
/***  Start of saved code (symbol: runnable implementation:DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment)  */

  return RTE_E_OK;

/***  End of saved code  ************************************************************************************/
#endif

#if 0
/***  Start of saved code (symbol: runnable implementation:DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData)  */

  return RTE_E_OK;

/***  End of saved code  ************************************************************************************/
#endif

#if 0
/***  Start of saved code (symbol: documentation area:DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU_doc)  */


/***  End of saved code  ************************************************************************************/
#endif

#if 0
/***  Start of saved code (symbol: runnable implementation:DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU)  */

  return RTE_E_OK;

/***  End of saved code  ************************************************************************************/
#endif

#if 0
/***  Start of saved code (symbol: documentation area:DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment_doc)  */


/***  End of saved code  ************************************************************************************/
#endif

#if 0
/***  Start of saved code (symbol: runnable implementation:DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_FreezeCurrentState)  */

  return RTE_E_OK;

/***  End of saved code  ************************************************************************************/
#endif

#if 0
/***  Start of saved code (symbol: documentation area:DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData_doc)  */


/***  End of saved code  ************************************************************************************/
#endif

#if 0
/***  Start of saved code (symbol: documentation area:DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_FreezeCurrentState_doc)  */


/***  End of saved code  ************************************************************************************/
#endif


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
