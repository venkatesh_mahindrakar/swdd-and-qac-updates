/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  FlexibleSwitchesRouter_Ctrl_LINMstr.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  FlexibleSwitchesRouter_Ctrl_LINMstr
 *  Generated at:  Fri Jun 12 17:01:41 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <FlexibleSwitchesRouter_Ctrl_LINMstr>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file FlexibleSwitchesRouter_Ctrl_LINMstr.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_Platform_SCIM_LIN 
//! @{
//! @addtogroup FlexibleSwitchesRouter_Ctrl_LINMstr
//! @{
//!
//! \brief
//! FlexibleSwitchesRouter_Ctrl_LINMstr SWC.
//! ASIL Level : QM.
//! This module implements the logic for the FlexibleSwitchesRouter_Ctrl_LINMstr runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   Boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_OpStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_InitMonitorReasonType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FSPConfigSettingsLIN1_P1EWZ_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FSPConfigSettingsLIN1_P1EWZ_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FSPConfigSettingsLIN2_P1EW0_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FSPConfigSettingsLIN2_P1EW0_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FSPConfigSettingsLIN3_P1EW1_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FSPConfigSettingsLIN3_P1EW1_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FSPConfigSettingsLIN4_P1EW2_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FSPConfigSettingsLIN4_P1EW2_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FSPConfigSettingsLIN5_P1EW3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FSPConfigSettingsLIN5_P1EW3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *
 * Runnable Entities:
 * ==================
 * FlexibleSwitchPanel_LINMastCtrl_20ms_runnable
 *   (*
 *       *
 *       * @par EntityName FlexibleSwitchPanel_LINMastCtrl_20ms_runnable
 *       *        
 *       * @par Purpose This runnable shall control input/output of flexible switch information from LIN bus.\n
 *       * 
 *       *  
 *       * @par Dataflow \n
 *       * Inputs : \n
 *       * Outputs : 
 *       *
 *       *@par Execution Event\n
 *       * Shall be executed if at least one of the following trigger conditions occurred:\n
 *       * -- triggered on TimingEvent every 20ms
 *       *
 *       *
 *       * @par Input Interfaces:
 *       * @par Explicit S/R API:
 *       *
 *       * @par Output Interfaces:
 *       * @par Explicit S/R API:
 *       *
 *       )
 *      
 *
 *********************************************************************************************************************/

#include "Rte_FlexibleSwitchesRouter_Ctrl_LINMstr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
// Include
#include "FlexibleSwitchesRouter_Ctrl_LINMstr.h"
#include "FuncLibrary_Timer_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * FSP1ResponseErrorL1_T: Boolean
 * FSP1ResponseErrorL2_T: Boolean
 * FSP1ResponseErrorL3_T: Boolean
 * FSP1ResponseErrorL4_T: Boolean
 * FSP1ResponseErrorL5_T: Boolean
 * FSP2ResponseErrorL1_T: Boolean
 * FSP2ResponseErrorL2_T: Boolean
 * FSP2ResponseErrorL3_T: Boolean
 * FSP3ResponseErrorL2_T: Boolean
 * FSP4ResponseErrorL2_T: Boolean
 * FSPDiagInfo_T: Integer in interval [0...63]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * FSPIndicationCmd_T: Integer in interval [0...65535]
 * FSPSwitchStatus_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN1_P1EWZ_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN1_P1EWZ_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN2_P1EW0_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN2_P1EW0_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN3_P1EW1_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN3_P1EW1_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN4_P1EW2_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN4_P1EW2_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN5_P1EW3_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN5_P1EW3_T: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Spare1 (4U)
 *   Spare2 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * Dem_InitMonitorReasonType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_INIT_MONITOR_CLEAR (1U)
 *   DEM_INIT_MONITOR_RESTART (2U)
 *   DEM_INIT_MONITOR_REENABLED (3U)
 *   DEM_INIT_MONITOR_STORAGE_REENABLED (4U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * LinDiagBusInfo: Enumeration of integer in interval [0...255] with enumerators
 *   None (0U)
 *   LinDiag_BUS1 (1U)
 *   LinDiag_BUS2 (2U)
 *   LinDiag_BUS3 (3U)
 *   LinDiag_BUS4 (4U)
 *   LinDiag_BUS5 (5U)
 *   LinDiag_SpareBUS1 (6U)
 *   LinDiag_SpareBUS2 (7U)
 *   LinDiag_ALL_BUSSES (8U)
 * LinDiagRequest_T: Enumeration of integer in interval [0...255] with enumerators
 *   NO_OPEARATION (0U)
 *   START_OPEARATION (1U)
 *   STOP_OPERATION (2U)
 * LinDiagServiceStatus: Enumeration of integer in interval [0...255] with enumerators
 *   LinDiagService_None (0U)
 *   LinDiagService_Pending (1U)
 *   LinDiagService_Completed (2U)
 *   LinDiagService_Error (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 * Dcm_Data5ByteType: Array with 5 element(s) of type uint8
 * Dcm_Data6ByteType: Array with 6 element(s) of type uint8
 * Dcm_Data7ByteType: Array with 7 element(s) of type uint8
 * FSPIndicationCmdArray_T: Array with 8 element(s) of type DeviceIndication_T
 * FSPSwitchStatusArray_T: Array with 8 element(s) of type PushButtonStatus_T
 * FSP_Array5: Array with 5 element(s) of type uint8
 * FspNVM_T: Array with 28 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_FSPConfigSettingsLIN2_P1EW0_T Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v(void)
 *   SEWS_FSPConfigSettingsLIN3_P1EW1_T Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v(void)
 *   SEWS_FSPConfigSettingsLIN4_P1EW2_T Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v(void)
 *   SEWS_FSPConfigSettingsLIN5_P1EW3_T Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v(void)
 *   SEWS_FSPConfigSettingsLIN1_P1EWZ_T Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v(void)
 *
 *********************************************************************************************************************/


#define FlexibleSwitchesRouter_Ctrl_LINMstr_START_SEC_CODE
#include "FlexibleSwitchesRouter_Ctrl_LINMstr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <InitMonitorForEvent> of PortPrototype <CBInitEvt_D1BN8_79_FlexSwLostFCI>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BK9_87_FSP_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN8_16_FSP_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN8_17_FSP_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN8_44_FSP_MemoryFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN8_79_FlexSwLostFCI_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CallbackInitMonitorForEvent_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent (returns application error)
 *********************************************************************************************************************/

   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1C1R_Data_P1C1R_NbFSP>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData_Irv_NbOfFspByCCNAD(uint8 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data6ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1C1R_Data_P1C1R_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData (returns application error)
 *********************************************************************************************************************/
   Rte_IrvRead_DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData_Irv_NbOfFspByCCNAD(&Data[1]);
   Data[0U] = Data[1U] + Data[2U] + Data[3U] + Data[4U] + Data[5U];
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData_Irv_NbOfFspInFailure(uint8 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data5ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1DCU_Data_P1DCU_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData (returns application error)
 *********************************************************************************************************************/
   Rte_IrvRead_DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData_Irv_NbOfFspInFailure(&Data[0]);
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/
   // LED IO control
   *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_SUBFUNCTIONNOTSUPPORTED;

   return RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData_Irv_IOCTL_FspLinCtrl(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData_doc
 *********************************************************************************************************************/
//uint8 P1EOW_ControlData = 3U;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData (returns application error)
 *********************************************************************************************************************/
   Data[0] = (uint8)((Rte_IrvRead_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData_Irv_IOCTL_FspLinCtrl()) &CONST_Tester_Notpresent);
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_FspLinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
static Boolean P1EOW_ShortTermOut = FALSE;

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/
   Rte_IrvWrite_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_FspLinCtrl((Rte_IrvRead_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData_Irv_IOCTL_FspLinCtrl()) & CONST_Tester_Notpresent); 
   *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_POSITIVERESPONSE;
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_FspLinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/
   uint8 retval = RTE_E_OK;
   *ErrorCode   = (Dcm_NegativeResponseCodeType)DCM_E_POSITIVERESPONSE;
   if (Data[0] < 3U) // change 3 with Maximum Range value
   {
      Rte_IrvWrite_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_FspLinCtrl(Data[0] | CONST_Tester_Present); // change 0x80 with MSB Mask macro
   }
   else
   {
      retval     = RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK;
      *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_REQUESTOUTOFRANGE;
   }
   return retval;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: FlexibleSwitchPanel_LINMastCtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN2_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN3_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN5_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_FSP1DiagInfoL1_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP1DiagInfoL2_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP1DiagInfoL3_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP1DiagInfoL4_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP1DiagInfoL5_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP1IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP1ResponseErrorL1_FSP1ResponseErrorL1(FSP1ResponseErrorL1_T *data)
 *   Std_ReturnType Rte_Read_FSP1ResponseErrorL2_FSP1ResponseErrorL2(FSP1ResponseErrorL2_T *data)
 *   Std_ReturnType Rte_Read_FSP1ResponseErrorL3_FSP1ResponseErrorL3(FSP1ResponseErrorL3_T *data)
 *   Std_ReturnType Rte_Read_FSP1ResponseErrorL4_FSP1ResponseErrorL4(FSP1ResponseErrorL4_T *data)
 *   Std_ReturnType Rte_Read_FSP1ResponseErrorL5_FSP1ResponseErrorL5(FSP1ResponseErrorL5_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatusL1_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatusL2_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatusL3_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatusL4_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatusL5_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP2DiagInfoL1_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP2DiagInfoL2_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP2DiagInfoL3_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP2IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP2ResponseErrorL1_FSP2ResponseErrorL1(FSP2ResponseErrorL1_T *data)
 *   Std_ReturnType Rte_Read_FSP2ResponseErrorL2_FSP2ResponseErrorL2(FSP2ResponseErrorL2_T *data)
 *   Std_ReturnType Rte_Read_FSP2ResponseErrorL3_FSP2ResponseErrorL3(FSP2ResponseErrorL3_T *data)
 *   Std_ReturnType Rte_Read_FSP2SwitchStatusL1_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP2SwitchStatusL2_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP2SwitchStatusL3_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP3DiagInfoL2_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP3IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP3ResponseErrorL2_FSP3ResponseErrorL2(FSP3ResponseErrorL2_T *data)
 *   Std_ReturnType Rte_Read_FSP3SwitchStatusL2_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP4DiagInfoL2_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP4IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP4ResponseErrorL2_FSP4ResponseErrorL2(FSP4ResponseErrorL2_T *data)
 *   Std_ReturnType Rte_Read_FSP4SwitchStatusL2_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP5IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP6IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP7IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP8IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP9IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP_BIndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FspNV_PR_FspNV(uint8 *data)
 *     Argument data: uint8* is of type FspNVM_T
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_FSP1IndicationCmdL1_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP1IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP1IndicationCmdL3_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP1IndicationCmdL4_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP1IndicationCmdL5_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP1SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP2IndicationCmdL1_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP2IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP2IndicationCmdL3_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP2SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP3IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP3SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP4IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP4SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP5SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP6SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP7SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP8SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP9SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP_BSwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FspNV_PR_FspNV(const uint8 *data)
 *     Argument data: uint8* is of type FspNVM_T
 *   Std_ReturnType Rte_Write_Living12VResetRequest_Living12VResetRequest(Boolean data)
 *   Std_ReturnType Rte_Write_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted(Boolean data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl(void)
 *   void Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(uint8 *data)
 *   void Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(uint8 *data)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl(uint8 data)
 *   void Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(const uint8 *data)
 *   void Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(const uint8 *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the FlexibleSwitchPanel_LINMastCtrl_20ms_runnable
//!
//!==================================================================================================================== 

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) FlexibleSwitchPanel_LINMastCtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: FlexibleSwitchPanel_LINMastCtrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   FSPSwitchStatus_T  RteInData_FSPSwStatus[MAX_FSP_NO]  = {0x00U};
   uint8              RteInData_FciCode[MAX_FSP_NO]      = {0x00U};
   uint8 RteInData_FSPIndicationCmd[MAX_FSP_NO][MAX_SLOT_NO] = {0x00U};
   uint8              FspPresenceStatus [MAX_LIN_NO]     = {0x00U};
   uint8              FspInFailureStatus [MAX_LIN_NO]    = {0x00U};
   uint8              RteInData_ComMode_LIN [MAX_LIN_NO] = {0x00U};
   Std_ReturnType     RteCode_FSPSwStatus[MAX_FSP_NO]    = {RTE_E_OK};
   static uint16      Timers[CONST_NbOfTimers]           = {CONST_TransitionFilterValue,
                                                            CONST_TransitionFilterValue,
                                                            CONST_TransitionFilterValue,
                                                            CONST_TransitionFilterValue,
                                                            CONST_TransitionFilterValue};
   static PushButtonStatus_T RteOutData_FSPSwStatus[MAX_FSP_NO][MAX_SLOT_NO] = { {PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable},
                                                                                 {PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable},
                                                                                 {PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable},
                                                                                 {PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable},
                                                                                 {PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable},
                                                                                 {PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable},
                                                                                 {PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable},
                                                                                 {PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable},
                                                                                 {PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable}, 
                                                                                 {PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable,PushButtonStatus_NotAvailable}};

   static FSPIndicationCmd_T  RteOutData_FspIndicationCmd[MAX_FSP_NO];
   uint8            FspId              = 0U;
   uint8            SwitchSlot         = 0U;
   uint16           IndicationBitfield = 0U;
   Std_ReturnType   retValue           = RTE_E_OK;
   
   DiagActiveState_T     isDiagActive;
   
   //! ###### Read DiagActiveState interface
   retValue = Rte_Read_DiagActiveState_isDiagActive(&isDiagActive);
   if (RTE_E_OK != retValue)
   {
      isDiagActive = Diag_Active_FALSE;
   }
   else
   {
      // RTE_E_OK : do nothing
   } 
   //! ##### Timers decrement logic : 'TimerFunction_Tick()'
   TimerFunction_Tick(CONST_NbOfTimers, Timers); 
   //! ##### Process RTE in data read common logic : 'Get_RteInDataRead_Common()'
   Get_RteInDataRead_Common(&RteCode_FSPSwStatus[0],
                            &RteInData_FSPSwStatus[0],
                            &RteInData_FciCode[0],
                            &RteInData_ComMode_LIN[0]);
   //! ##### Process gateway logic: 'Get_RteInDataRead_Common()'
   for (FspId = 0U; FspId < MAX_FSP_NO; FspId++)
   {
      //! ##### Process the FlexibleSwitchesRouter_Ctrl_LINMstr logic : 'FlexibleSwRouter_Ctrl_LINMstr_Logic()'
      FlexibleSwRouter_Ctrl_LINMstr_Logic(&RteInData_FSPSwStatus[FspId],
                                                &RteOutData_FSPSwStatus[FspId][0U]);
   }
   //! ###### Update FSP status data
   Update_Irv_data(&FspInFailureStatus[0],
                   &FspPresenceStatus[0]);  
   //! ###### Process fallback mode logic for missing communication: 'FallbackModeLogic_MissingMessage()'
   FallbackModeLogic_MissingMessage(&RteInData_ComMode_LIN[0],
                                    &RteCode_FSPSwStatus[0],
                                    &FspInFailureStatus[0],
                                    &FspPresenceStatus[0],
                                    RteOutData_FSPSwStatus,
                                    &Timers[0]);
   //! ###### Process fallback mode logic for Flexible Switch Contact Lost : 'FSP_FallbackModeProcessing_ContactLost()'
   FallbackProcessing_ContactLost(&RteInData_FciCode[0],
                                  RteOutData_FSPSwStatus);  
   //! ###### Process diagnostic report for FCI: 'FciToDtcConversion_FspSupply()'
   FciToDtcConversion_FspSupply(&RteInData_FciCode[0]);  
   //! ###### Write the output FSP%SwitchStatus ports
   Rte_Write_FSP1SwitchStatus_FSPSwitchStatusArray(&RteOutData_FSPSwStatus[FSP1][0U]);
   Rte_Write_FSP2SwitchStatus_FSPSwitchStatusArray(&RteOutData_FSPSwStatus[FSP2][0U]);
   Rte_Write_FSP3SwitchStatus_FSPSwitchStatusArray(&RteOutData_FSPSwStatus[FSP3][0U]);
   Rte_Write_FSP4SwitchStatus_FSPSwitchStatusArray(&RteOutData_FSPSwStatus[FSP4][0U]);
   Rte_Write_FSP5SwitchStatus_FSPSwitchStatusArray(&RteOutData_FSPSwStatus[FSP5][0U]);
   Rte_Write_FSP6SwitchStatus_FSPSwitchStatusArray(&RteOutData_FSPSwStatus[FSP6][0U]);
   Rte_Write_FSP7SwitchStatus_FSPSwitchStatusArray(&RteOutData_FSPSwStatus[FSP7][0U]);
   Rte_Write_FSP8SwitchStatus_FSPSwitchStatusArray(&RteOutData_FSPSwStatus[FSP8][0U]);
   Rte_Write_FSP9SwitchStatus_FSPSwitchStatusArray(&RteOutData_FSPSwStatus[FSP9][0U]);
   Rte_Write_FSP_BSwitchStatus_FSPSwitchStatusArray(&RteOutData_FSPSwStatus[FSPB][0U]);
   //! ###### Read FSP%IndicationCmd_1 from RTE and store in RteInData_FSPIndicationCmd[][]
   Rte_Read_FSP1IndicationCmd_FSPIndicationCmdArray((DeviceIndication_T*)&RteInData_FSPIndicationCmd[FSP1][0]);
   Rte_Read_FSP2IndicationCmd_FSPIndicationCmdArray((DeviceIndication_T*)&RteInData_FSPIndicationCmd[FSP2][0]);
   Rte_Read_FSP3IndicationCmd_FSPIndicationCmdArray((DeviceIndication_T*)&RteInData_FSPIndicationCmd[FSP3][0]);
   Rte_Read_FSP4IndicationCmd_FSPIndicationCmdArray((DeviceIndication_T*)&RteInData_FSPIndicationCmd[FSP4][0]);
   Rte_Read_FSP5IndicationCmd_FSPIndicationCmdArray((DeviceIndication_T*)&RteInData_FSPIndicationCmd[FSP5][0]);
   Rte_Read_FSP6IndicationCmd_FSPIndicationCmdArray((DeviceIndication_T*)&RteInData_FSPIndicationCmd[FSP6][0]);
   Rte_Read_FSP7IndicationCmd_FSPIndicationCmdArray((DeviceIndication_T*)&RteInData_FSPIndicationCmd[FSP7][0]);
   Rte_Read_FSP8IndicationCmd_FSPIndicationCmdArray((DeviceIndication_T*)&RteInData_FSPIndicationCmd[FSP8][0]);
   Rte_Read_FSP9IndicationCmd_FSPIndicationCmdArray((DeviceIndication_T*)&RteInData_FSPIndicationCmd[FSP9][0]);
   Rte_Read_FSP_BIndicationCmd_FSPIndicationCmdArray((DeviceIndication_T*)&RteInData_FSPIndicationCmd[FSPB][0]);

   //! ###### Process the FlexibleSwitchesRouter_Ctrl_LINMstr logic for FSPIndicationCmd
   for (FspId = 0U; FspId < MAX_FSP_NO; FspId++)
   {
      IndicationBitfield = 0U;
      //! ##### Based on FspId and switch slot, IndicationCmd converted from array to byte
      for (SwitchSlot = (MAX_SLOT_NO - 1U); SwitchSlot > 3U; SwitchSlot--)
      {
         if (0x02U == (RteInData_FSPIndicationCmd[FspId][SwitchSlot] & 0x03U))
         {
            IndicationBitfield = IndicationBitfield << (2U);
            IndicationBitfield = IndicationBitfield | 0x03U;
         }
         else
         {
            IndicationBitfield = IndicationBitfield << (2U);
            IndicationBitfield = IndicationBitfield | ((uint16)(RteInData_FSPIndicationCmd[FspId][SwitchSlot]) & 0x0001U);
         }
         if (0x02U == (RteInData_FSPIndicationCmd[FspId][SwitchSlot - 4U] & 0x03U))
         {
            IndicationBitfield = IndicationBitfield << (2U);
            IndicationBitfield = IndicationBitfield | 0x03U;  
         }
         else
         {
            IndicationBitfield = IndicationBitfield << (2U);
            IndicationBitfield = IndicationBitfield | ((uint16)(RteInData_FSPIndicationCmd[FspId][SwitchSlot - 4U]) & 0x0001U); 
         }
      }
      RteOutData_FspIndicationCmd[FspId] = IndicationBitfield;
   }
   if ((((Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl()) & CONST_Tester_Present) == CONST_Tester_Present)
       && (Diag_Active_TRUE == isDiagActive))
   {
      //! ###### Process input output control service logic : 'IOCtrlService_LinOutputSignalControl()'
      IOCtrlService_LinOutputSignalControl((uint8)((Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl()) & CONST_Tester_Notpresent),
                                            RteOutData_FspIndicationCmd);
   }
   else 
   {
      Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl(DeviceIndication_SpareValue);
   }
   //! ###### Write the output FSP%IndicationCmdL% ports
   Rte_Write_FSP1IndicationCmdL1_FSPIndicationCmd(RteOutData_FspIndicationCmd[FSP1]);
   Rte_Write_FSP2IndicationCmdL1_FSPIndicationCmd(RteOutData_FspIndicationCmd[FSP2]);
   Rte_Write_FSP1IndicationCmdL2_FSPIndicationCmd(RteOutData_FspIndicationCmd[FSP3]);
   Rte_Write_FSP2IndicationCmdL2_FSPIndicationCmd(RteOutData_FspIndicationCmd[FSP4]);
   Rte_Write_FSP3IndicationCmdL2_FSPIndicationCmd(RteOutData_FspIndicationCmd[FSP5]);
   Rte_Write_FSP4IndicationCmdL2_FSPIndicationCmd(RteOutData_FspIndicationCmd[FSP6]);
   Rte_Write_FSP1IndicationCmdL3_FSPIndicationCmd(RteOutData_FspIndicationCmd[FSP7]);
   Rte_Write_FSP2IndicationCmdL3_FSPIndicationCmd(RteOutData_FspIndicationCmd[FSP8]);
   Rte_Write_FSP1IndicationCmdL4_FSPIndicationCmd(RteOutData_FspIndicationCmd[FSP9]);
   Rte_Write_FSP1IndicationCmdL5_FSPIndicationCmd(RteOutData_FspIndicationCmd[FSPB]);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_R1AAI_FspAssignmentRoutine>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignReq(LinDiagBusInfo LinBusInfo, LinDiagRequest_T RequestType)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignResp(LinDiagServiceStatus *pDiagServiceStatus, uint8 *pAvailableFSPCount, uint8 *pFspErrorStatus, uint8 *pFspNvData)
 *     Synchronous Server Invocation. Timeout: None
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data7ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAI_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAI_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults (returns application error)
 *********************************************************************************************************************/
   LinDiagServiceStatus  CCNADlinStatus_U8 = LinDiagService_None;
   //after confirmation from ccNad completion
   uint8   i                  = 0U;
   Std_ReturnType retValue    = RTE_E_INVALID;
   uint8 FSPCountPerLIN[5U]   = {0U};
   uint8 FspNvData[28U]       = {0U};
   uint8 FspErrStatus[5U]     = {0U};
   uint8 ExpectedFSPCount[5U] = {0U};
   *ErrorCode               = (Dcm_NegativeResponseCodeType)DCM_E_POSITIVERESPONSE;
   Rte_Call_CddLinDiagServices_FSPAssignResp(&CCNADlinStatus_U8,
                                             &FSPCountPerLIN[0U],
                                             &FspErrStatus[0U],
                                             &FspNvData[0U]);
   //! ###### Check for CCNADlinStatus_U8 status
   if (LinDiagService_Pending == CCNADlinStatus_U8)
   {
      // Wait
      *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_BUSYREPEATREQUEST;
      retValue   = RTE_E_RoutineServices_R1AAI_E_NOT_OK;
   }
   else if ((LinDiagService_Completed == CCNADlinStatus_U8)
           || (LinDiagService_Error == CCNADlinStatus_U8))
   {
      retValue = RTE_E_OK;
   }
   else
   {
      CCNADlinStatus_U8 = LinDiagService_None;
      *ErrorCode        = (Dcm_NegativeResponseCodeType)DCM_E_GENERALREJECT;
      retValue          = RTE_E_RoutineServices_R1AAI_E_NOT_OK;
   }
   if (LinDiagService_Completed == CCNADlinStatus_U8)
   {
      //! ##### Store the CCNAD status in NVRAM
      Rte_Write_FspNV_PR_FspNV(&FspNvData[0U]);
      CCNADlinStatus_U8 = LinDiagService_None;
      /*
        ExpectedFSPCount[0]= Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v();
        ExpectedFSPCount[1]= Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v();
        ExpectedFSPCount[2]= Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v();
        ExpectedFSPCount[3]= Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v();
        ExpectedFSPCount[4]= Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v();
      */
      ExpectedFSPCount[0U] = 2U;
      ExpectedFSPCount[1U] = 4U;
      ExpectedFSPCount[2U] = 2U;
      ExpectedFSPCount[3U] = 1U;
      ExpectedFSPCount[4U] = 1U;
      Out_Common_Diagnostics_DataRecord[0U] = 0U;
      Out_Common_Diagnostics_DataRecord[1U] = 0U;
      for (i = 0U;i < 5U;i++)
      {
         if (0U == FspErrStatus[i])
         {
            Out_Common_Diagnostics_DataRecord[i + 2U] = 0U; //Successful
            /*
            if (0x255 == ExpectedFSPCount[i])
            {
               // Check disabled, keep successfull
            }
            if (FSPCountPerLIN[i] > ExpectedFSPCount[i])
            {
               Out_Common_Diagnostics_DataRecord[i + 2U] = 2U; //More then expected
            }
            else if (FSPCountPerLIN[i] < ExpectedFSPCount[i])
            {
               Out_Common_Diagnostics_DataRecord[i + 2U] = 3U; //Less then expected
            }
            else
            {
               // Check successfull: keep successfull status
            }*/
         }
         else
         {
            Out_Common_Diagnostics_DataRecord[i + 2U] = FspErrStatus[i]; // Failure in CCNAD or Uncompatable Topology Case
         }
      }
   }
   else if (CCNADlinStatus_U8 == LinDiagService_Error)
   {
      Out_Common_Diagnostics_DataRecord[0] = 0U;
      Out_Common_Diagnostics_DataRecord[1] = 0U;
      for (i = 0U;i < 5U;i++)
      {
         Out_Common_Diagnostics_DataRecord[i + 2U] = 4U; // Failure in CCNAD or Uncompatable Topology Case
      }
   }
   else
   {
      // Do nothing
   }
   return retValue;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAI_FspAssignmentRoutine_Start
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_R1AAI_FspAssignmentRoutine>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignReq(LinDiagBusInfo LinBusInfo, LinDiagRequest_T RequestType)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignResp(LinDiagServiceStatus *pDiagServiceStatus, uint8 *pAvailableFSPCount, uint8 *pFspErrorStatus, uint8 *pFspNvData)
 *     Synchronous Server Invocation. Timeout: None
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAI_FspAssignmentRoutine_Start(uint8 In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAI_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAI_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_Start(uint8 In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_Start (returns application error)
 *********************************************************************************************************************/
   Std_ReturnType retValue       = RTE_E_INVALID; 
   static uint8   FSPR1AAI_Byte1 = 0x02U;
   static uint8   FSPR1AAI_Byte2 = 0x00U;

   Out_Common_Diagnostics_DataRecord[0U] = FSPR1AAI_Byte1;
   Out_Common_Diagnostics_DataRecord[1U] = FSPR1AAI_Byte2;
   retValue                              = RTE_E_OK;
   Rte_Call_CddLinDiagServices_FSPAssignReq(In_Common_Diagnostics_DataRecord,START_OPEARATION);
   return retValue;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAI_FspAssignmentRoutine_Stop
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_R1AAI_FspAssignmentRoutine>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignReq(LinDiagBusInfo LinBusInfo, LinDiagRequest_T RequestType)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignResp(LinDiagServiceStatus *pDiagServiceStatus, uint8 *pAvailableFSPCount, uint8 *pFspErrorStatus, uint8 *pFspNvData)
 *     Synchronous Server Invocation. Timeout: None
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAI_FspAssignmentRoutine_Stop(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAI_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAI_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_Stop(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_Stop (returns application error)
 *********************************************************************************************************************/

   Rte_Call_CddLinDiagServices_FSPAssignReq(0xFF,STOP_OPERATION);

   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#define FlexibleSwitchesRouter_Ctrl_LINMstr_STOP_SEC_CODE
#include "FlexibleSwitchesRouter_Ctrl_LINMstr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'IOCtrlService_LinOutputSignalControl'
//!
//! \param     OverrideData    Provide the override Data value
//! \param     *pOutData       Update the output data structure for FlexibleSwitchesRouter_Ctrl_LINMstr
//!
//!======================================================================================
static void IOCtrlService_LinOutputSignalControl(const uint8              OverrideData,
                                                       FSPIndicationCmd_T pOutData[MAX_FSP_NO])
{
   uint16 i                 = 0U;
   uint16 StoreOverride     = 0U;
   uint16 data1     = 0U;
   uint16 data2     = 0U;
   uint16 LocalOverrideData = 0U;
   //! ###### to store led indication data into local variable
   if (OverrideData == 2U)
   {
      LocalOverrideData =0xffffU;
   }
   else
   {
      StoreOverride = (uint16)OverrideData;
      for (i = 0U; i < 16U; i = i + 2U)
      {
         data1 = (StoreOverride << i);
         data2 = ((uint16)(0x0003) << i);
         LocalOverrideData |= ((data1) & (data2));
      }
   }
   //! ###### Override LIN Output signals with IOControl service value
   for (i = 0U; i < 10U; i++)
   {
      pOutData[i] = LocalOverrideData;
   }
}
 //!======================================================================================
 //!
 //! \brief 
 //!  This function implements the logic for the 'Get_RteInDataRead_Common'
 //!
 //! \param     pRetValue                 Returns the input port connection status
 //! \param     pRteInData_FSPSwStatus    Stores data related to input ports 'FSP%SwitchStatusL%'
 //! \param     pFciData                  Stores data related to input ports 'FSP%DiagInfoL%'
 //! \param     pComMode_LIN              Stores data related to input ports 'ComMode_LIN%'
 //!
 //!======================================================================================
static void Get_RteInDataRead_Common (Std_ReturnType    pRetValue[MAX_FSP_NO],
                                      FSPSwitchStatus_T pRteInData_FSPSwStatus[MAX_FSP_NO],
                                      uint8             pFciData[MAX_FSP_NO],
                                      uint8             pComMode_LIN[MAX_LIN_NO])
{
   //! ###### Read FSP%SwitchStatusL% from RTE and store in RteInData_FSPSwStatus[]
   pRetValue[FSP1] = Rte_Read_FSP1SwitchStatusL1_FSPSwitchStatus(&pRteInData_FSPSwStatus[FSP1]);
   pRetValue[FSP2] = Rte_Read_FSP2SwitchStatusL1_FSPSwitchStatus(&pRteInData_FSPSwStatus[FSP2]);
   pRetValue[FSP3] = Rte_Read_FSP1SwitchStatusL2_FSPSwitchStatus(&pRteInData_FSPSwStatus[FSP3]);
   pRetValue[FSP4] = Rte_Read_FSP2SwitchStatusL2_FSPSwitchStatus(&pRteInData_FSPSwStatus[FSP4]);
   pRetValue[FSP5] = Rte_Read_FSP3SwitchStatusL2_FSPSwitchStatus(&pRteInData_FSPSwStatus[FSP5]);
   pRetValue[FSP6] = Rte_Read_FSP4SwitchStatusL2_FSPSwitchStatus(&pRteInData_FSPSwStatus[FSP6]);
   pRetValue[FSP7] = Rte_Read_FSP1SwitchStatusL3_FSPSwitchStatus(&pRteInData_FSPSwStatus[FSP7]);
   pRetValue[FSP8] = Rte_Read_FSP2SwitchStatusL3_FSPSwitchStatus(&pRteInData_FSPSwStatus[FSP8]);
   pRetValue[FSP9] = Rte_Read_FSP1SwitchStatusL4_FSPSwitchStatus(&pRteInData_FSPSwStatus[FSP9]);
   pRetValue[FSPB] = Rte_Read_FSP1SwitchStatusL5_FSPSwitchStatus(&pRteInData_FSPSwStatus[FSPB]);
   
   //! ###### Read ComMode_LIN% from RTE
   Rte_Read_ComMode_LIN1_ComMode_LIN(&pComMode_LIN[LIN1]);
   Rte_Read_ComMode_LIN2_ComMode_LIN(&pComMode_LIN[LIN2]);
   Rte_Read_ComMode_LIN3_ComMode_LIN(&pComMode_LIN[LIN3]);
   Rte_Read_ComMode_LIN4_ComMode_LIN(&pComMode_LIN[LIN4]);
   Rte_Read_ComMode_LIN5_ComMode_LIN(&pComMode_LIN[LIN5]);
   
   //! ###### Read ComMode_LIN% from RTE
   Rte_Read_FSP1DiagInfoL1_FSPDiagInfo(&pFciData[FSP1]);
   Rte_Read_FSP2DiagInfoL1_FSPDiagInfo(&pFciData[FSP2]);
   Rte_Read_FSP1DiagInfoL2_FSPDiagInfo(&pFciData[FSP3]);
   Rte_Read_FSP2DiagInfoL2_FSPDiagInfo(&pFciData[FSP4]);
   Rte_Read_FSP3DiagInfoL2_FSPDiagInfo(&pFciData[FSP5]);
   Rte_Read_FSP4DiagInfoL2_FSPDiagInfo(&pFciData[FSP6]);
   Rte_Read_FSP1DiagInfoL3_FSPDiagInfo(&pFciData[FSP7]);
   Rte_Read_FSP2DiagInfoL3_FSPDiagInfo(&pFciData[FSP8]);
   Rte_Read_FSP1DiagInfoL4_FSPDiagInfo(&pFciData[FSP9]);
   Rte_Read_FSP1DiagInfoL5_FSPDiagInfo(&pFciData[FSPB]);   
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Update_Irv_data'
//!
//! \param    pFspInFailureStatus         Stores data related to missing frames
//! \param    pNbOfFspDetectedByCCNAD     Stores data related to FSP Panels detected by CCNAD
//!
//!======================================================================================
static void Update_Irv_data (uint8 pFspInFailureStatus[MAX_LIN_NO],
                             uint8 pNbOfFspDetectedByCCNAD[MAX_LIN_NO])
{
   uint8 FspNVM_Data[sizeof(FspNVM_T)] = {0U};
   uint8 LinBus                        = 0U;
   uint8 FspPosNvram                   = 0U;
   const uint8 CONST_FspMaxCapacityNb  = 4U;
   
   Rte_Read_FspNV_PR_FspNV(&FspNVM_Data[0U]);
   //! ###### Processing the logic for NbOfFspDetectedByCCNAD
   for (LinBus = 0U; LinBus < 5U; LinBus++)
   {
      for (FspPosNvram = 0U; FspPosNvram < 4U; FspPosNvram++)
      {
         if (0U != FspNVM_Data[(LinBus*CONST_FspMaxCapacityNb) + FspPosNvram])
         {
            pNbOfFspDetectedByCCNAD[LinBus] = pNbOfFspDetectedByCCNAD[LinBus] + 1U;
         }
         else
         {
            // keep previous values
         }
      }
   }     
   Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(pNbOfFspDetectedByCCNAD);   
   Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(pFspInFailureStatus);   
}

//!======================================================================================
//!
//! \brief
//! This function is processing the FallbackMode Logic for FallbackModeLogic_MissingMessage
//!
//! \param  pComMode                       Provides input data related to input port 'commode_LIN%'
//! \param  pRetValue                      Provides return status for FSP_SwitchStatus
//! \param  pFspInFailureStatus            Based on condition check updates P1DCU data     
//! \param  pNbOfFspDetectedByCCNAD        Based on CCNAD detection updates P1DCU data          
//! \param  pRteOutData_FSPSwitchStatus    Based on condition check update FSPSwitchStatus output          
//! \param  pTimer                         Provides Timer value to perform Missing frame operation
//! 
//!======================================================================================
static void FallbackModeLogic_MissingMessage (const uint8              pComMode[MAX_LIN_NO],
                                              const Std_ReturnType     pRetValue[MAX_FSP_NO],
                                                    uint8              pFspInFailureStatus [MAX_LIN_NO],
                                              const uint8              pNbOfFspDetectedByCCNAD[MAX_LIN_NO],
                                                    PushButtonStatus_T pRteOutData_FSPSwitchStatus[MAX_FSP_NO][MAX_SLOT_NO],
                                                    uint16             pTimer[CONST_NbOfTimers])
{  
   uint8   LinId                    = MAX_LIN_NO;
   boolean isAtLeastOneFspInFailure = FALSE;

   //! ##### Reinitialize the status of FSP in failure
   for (LinId = LIN1; LinId < MAX_LIN_NO; LinId++)
   {
      pFspInFailureStatus[LinId] = 0U;
   }  
   //! ##### Process each RTE FSP status and update the output ports accordingly: 'FSP_FallbackModeProcessing_MissingCom()'
   FallbackMode_MissingMessage_FSP(&pComMode[0U], LIN1, FSP1, &pRetValue[0U], &pFspInFailureStatus[0U], pRteOutData_FSPSwitchStatus, &pTimer[CONST_TransitModeFilter_LIN1]);
   FallbackMode_MissingMessage_FSP(&pComMode[0U], LIN1, FSP2, &pRetValue[0U], &pFspInFailureStatus[0U], pRteOutData_FSPSwitchStatus, &pTimer[CONST_TransitModeFilter_LIN1]);
   FallbackMode_MissingMessage_FSP(&pComMode[0U], LIN2, FSP3, &pRetValue[0U], &pFspInFailureStatus[0U], pRteOutData_FSPSwitchStatus, &pTimer[CONST_TransitModeFilter_LIN2]);
   FallbackMode_MissingMessage_FSP(&pComMode[0U], LIN2, FSP4, &pRetValue[0U], &pFspInFailureStatus[0U], pRteOutData_FSPSwitchStatus, &pTimer[CONST_TransitModeFilter_LIN2]);
   FallbackMode_MissingMessage_FSP(&pComMode[0U], LIN2, FSP5, &pRetValue[0U], &pFspInFailureStatus[0U], pRteOutData_FSPSwitchStatus, &pTimer[CONST_TransitModeFilter_LIN2]);
   FallbackMode_MissingMessage_FSP(&pComMode[0U], LIN2, FSP6, &pRetValue[0U], &pFspInFailureStatus[0U], pRteOutData_FSPSwitchStatus, &pTimer[CONST_TransitModeFilter_LIN2]);
   FallbackMode_MissingMessage_FSP(&pComMode[0U], LIN3, FSP7, &pRetValue[0U], &pFspInFailureStatus[0U], pRteOutData_FSPSwitchStatus, &pTimer[CONST_TransitModeFilter_LIN3]);
   FallbackMode_MissingMessage_FSP(&pComMode[0U], LIN3, FSP8, &pRetValue[0U], &pFspInFailureStatus[0U], pRteOutData_FSPSwitchStatus, &pTimer[CONST_TransitModeFilter_LIN3]);
   FallbackMode_MissingMessage_FSP(&pComMode[0U], LIN4, FSP9, &pRetValue[0U], &pFspInFailureStatus[0U], pRteOutData_FSPSwitchStatus, &pTimer[CONST_TransitModeFilter_LIN4]);
   FallbackMode_MissingMessage_FSP(&pComMode[0U], LIN5, FSPB, &pRetValue[0U], &pFspInFailureStatus[0U], pRteOutData_FSPSwitchStatus, &pTimer[CONST_TransitModeFilter_LIN5]);

   //! ##### Process the FSP missing message error status
   // pFspInFailureStatus indicates the number of FSP in failure comparing to nominal LIN schedule table (Max nb of FSP in active LIN schedule)
   for (LinId = LIN1; LinId < MAX_LIN_NO; LinId++)
   {
      // get the status of number of actively communicating FSP
      pFspInFailureStatus[LinId] = MaxNbOfFspByBus[LinId] - pFspInFailureStatus[LinId];
      // pFspInFailureStatus indicates now the active FSP

      // get the difference of expected communicating FSP vs actively communicating
      if (pNbOfFspDetectedByCCNAD[LinId] > pFspInFailureStatus[LinId])
      {
         pFspInFailureStatus[LinId] = pNbOfFspDetectedByCCNAD[LinId] - pFspInFailureStatus[LinId];
         isAtLeastOneFspInFailure = TRUE;
      }
      else
      {
         pFspInFailureStatus[LinId] = 0U;
      }
   }
   // Com Mode is common for all LIN busses, only LIN2 can be considered
   if (ApplicationMonitoring == pComMode[LIN2])
   {
      if (TRUE == isAtLeastOneFspInFailure)
      {
         // number of FSP does not match the expectation
         //! ##### Invoke Rte_Call_Event_D1BK9_87_FSP_NoResp_SetEventStatus
         Rte_Call_Event_D1BK9_87_FSP_NoResp_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         // the missing FSP are not expected to be mounted
         //! ##### Reset the DTC, if all FSP assigned during the CCNAD are OK
         Rte_Call_Event_D1BK9_87_FSP_NoResp_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      }    
      //! ###### Share the status for DID_FspInFailure by Irv_DID_FspInFailure
      Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(pFspInFailureStatus);
   }
   else
   {
      // do nothing: DTC indication update is not active in Com LIN mode
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the FallbackMode Logic for Flexible switch connection lost
//!
//! \param  pRteInData_FciCode           Based on input data, check for FCI connection lost
//! \param  pRteOutData_FSPSwitchStatus  If FCI connection lost, set FSPSwitchStatus to Error
//!
//!======================================================================================
static void FallbackProcessing_ContactLost (const uint8              pRteInData_FciCode[MAX_FSP_NO],
                                                  PushButtonStatus_T pRteOutData_FSPSwitchStatus[MAX_FSP_NO][MAX_SLOT_NO])
{   
   uint8   fsp                  = 0U;
   uint8   slot                 = 0U;
   Boolean isActiveFaultPresent = FALSE;
   static boolean FciConnectionLost[MAX_FSP_NO][MAX_SLOT_NO] = {FALSE};
   
   //! ##### Filter FCI codes by switch connection mask 0x10
   for (fsp = FSP1; fsp < MAX_FSP_NO; fsp++)
   {
      //! #### Collect and update the status of FCI by switch
      if ((pRteInData_FciCode[fsp] & 0x10U) == 0x10U)
      {
         if ((pRteInData_FciCode[fsp] & 0x20U) == 0x20U)
         {
            FciConnectionLost[fsp][pRteInData_FciCode[fsp] & 0x0FU] = TRUE;
         }
         else
         {
            FciConnectionLost[fsp][pRteInData_FciCode[fsp] & 0x0FU] = FALSE;
         }
      }
      else
      {
         // no faults reported, reset the codes
         if (0x00U == pRteInData_FciCode[fsp])
         {
            for (slot = SLOT0; slot < MAX_SLOT_NO; slot++)
            {
               FciConnectionLost[fsp][slot] = FALSE;
            }
         }
      }    
      //! #### Apply the FCI error indication on output FSP signal
      for (slot = 0U; slot < MAX_SLOT_NO; slot++)
      {
         if (FciConnectionLost[fsp][slot] == TRUE)
         {
            pRteOutData_FSPSwitchStatus[fsp][slot] = PushButtonStatus_Error;
            isActiveFaultPresent                   = TRUE;
         }
         else
         {
            // no fault, no action
         }
      }
   } 
   //! ###### Invoke Rte_Call_Event_D1BN8_79_FlexSwLostFCI_SetEventStatus
   if (TRUE == isActiveFaultPresent)
   {
      Rte_Call_Event_D1BN8_79_FlexSwLostFCI_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else
   {
      Rte_Call_Event_D1BN8_79_FlexSwLostFCI_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the FlexibleSwRouter_Ctrl_LINMstr_Logic
//!
//! \param    *pRteInData_OneFSPSwStatus   Provides the switch status of FSP
//! \param    pRteOutData_OneFSPSwStatus   Update the value as per condition
//! 
//!======================================================================================
static void FlexibleSwRouter_Ctrl_LINMstr_Logic(const FSPSwitchStatus_T  *pRteInData_OneFSPSwStatus,
                                                      PushButtonStatus_T pRteOutData_OneFSPSwStatus[sizeof(FSPSwitchStatusArray_T)])
{
   uint8 slot_no = 0U;

   //! ###### By default switch status of FSP is initialized with neutral
   for (slot_no = 0U; slot_no < MAX_SLOT_NO; slot_no++)
   {
      pRteOutData_OneFSPSwStatus[slot_no] = PushButtonStatus_Neutral;
   }
   //! ##### Based on switch status, value present in 'Slot0 Upper' converted to array
   if ((*pRteInData_OneFSPSwStatus & 0x01U) > 0U)
   {
      pRteOutData_OneFSPSwStatus[0U] = PushButtonStatus_Pushed;
   }
   else
   {
      // Do nothing: maintain value neutral
   }
   //! ##### Based on switch status, value present in 'Slot1 Upper' converted to array
   if ((*pRteInData_OneFSPSwStatus & 0x04U) > 0U)
   {
      pRteOutData_OneFSPSwStatus[1U] = PushButtonStatus_Pushed;
   }
   else
   {
      // Do nothing: maintain value neutral
   }
   //! ##### Based on switch status, value present in 'Slot2 Upper' converted to array
   if ((*pRteInData_OneFSPSwStatus & 0x10U) > 0U)
   {
      pRteOutData_OneFSPSwStatus[2U] = PushButtonStatus_Pushed;
   }
   else
   {
      // Do nothing: maintain value neutral
   }
   //! ##### Based on switch status, value present in 'Slot3 Upper' converted to array
   if ((*pRteInData_OneFSPSwStatus & 0x40U) > 0U)
   {
      pRteOutData_OneFSPSwStatus[3U] = PushButtonStatus_Pushed;
   }
   else
   {
      // Do nothing: maintain value neutral
   }
   //! ##### Based on switch status, value present in 'Slot0 Lower' converted to array
   if ((*pRteInData_OneFSPSwStatus & 0x02U) > 0U)
   {
      pRteOutData_OneFSPSwStatus[4U] = PushButtonStatus_Pushed;
   }
   else
   {
      // Do nothing: maintain value neutral
   }
   //! ##### Based on switch status, value present in 'Slot1 Lower' converted to array
   if ((*pRteInData_OneFSPSwStatus & 0x08U) > 0U)
   {
      pRteOutData_OneFSPSwStatus[5U] = PushButtonStatus_Pushed;
   }
   else
   {
      // Do nothing : maintain value neutral
   }
   //! ##### Based on switch status, value present in 'Slot2 Lower' converted to array
   if ((*pRteInData_OneFSPSwStatus & 0x20U) > 0U)
   {
      pRteOutData_OneFSPSwStatus[6U] = PushButtonStatus_Pushed;
   }
   else
   {
      // Do nothing : maintain value neutral
   }
   //! ##### Based on switch status, value present in 'Slot3 Lower' converted to array
   if ((*pRteInData_OneFSPSwStatus & 0x80U) > 0U)
   {
      pRteOutData_OneFSPSwStatus[7U] = PushButtonStatus_Pushed;
   }
   else
   {
      // Do nothing : maintain value neutral
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the FallbackMode_MissingMessage_FSP
//!
//! \param    pComMode_LIN                  Provides the switch status of FSP
//! \param    LinBus                        Provides Linbus information
//! \param    FspId                         Provides information related to particular FSP on LIN
//! \param    pRetValue                     Provides information related to each FSP
//! \param    pFspInFailureStatus           Update the data related to P1DCU
//! \param    pRteOutData_FSPSwitchStatus   Update the value to ERROR as per condition
//! \param    pTimer                        Update the value as per condition
//! 
//!======================================================================================
static void FallbackMode_MissingMessage_FSP(const uint8              pComMode_LIN[MAX_LIN_NO],
                                                 const uint8              LinBus,
                                                 const uint8              FspId,
                                                 const Std_ReturnType     pRetValue[MAX_FSP_NO],
                                                       uint8              pFspInFailureStatus[MAX_LIN_NO],
                                                       PushButtonStatus_T pRteOutData_FSPSwitchStatus[MAX_FSP_NO][MAX_SLOT_NO],
                                                       uint16             *pTimer)
{
   boolean isFspInFailureStatus = FALSE;
   //boolean isFspKeepLastStatus  = FALSE;
   uint8   slot                 = 0U;
   
   if (ApplicationMonitoring != pComMode_LIN[LinBus])
   {
      *pTimer = CONST_TransitionFilterValue;
   }
   else
   {
      //Do:Nothing
   } 
   //! ##### Check the LIN bus communication status
   if (Error == pComMode_LIN[LinBus])
   {
      isFspInFailureStatus = TRUE;
   }
   else if (ApplicationMonitoring == pComMode_LIN[LinBus])
   {
      //! ##### Check the FSP communication status by return RTE status value of previously stored FSP%SwitchStatusL%
      if (RTE_E_MAX_AGE_EXCEEDED == pRetValue[FspId])
      {
         if (CONST_TimerFunctionInactive == *pTimer)
         {
            isFspInFailureStatus        = TRUE;
            //! #### Increase the counter of FSP in failure on LIN bus
            pFspInFailureStatus[LinBus] = pFspInFailureStatus[LinBus] + 1U;
         }
		 else
		 {
			 // Do nothing: perform gateway logic
		 }
      }
      else
      {
         // Do nothing: perform gateway logic
      }
   }
   else
   {
      // no action: missing message detection is disabled in Com Mode
      //isFspKeepLastStatus = TRUE;
   }  
   //! ##### Set the output signals to error, if LIN or FSP is in failure
   if (TRUE == isFspInFailureStatus)
   {
      for (slot = 0U; slot < MAX_SLOT_NO; slot++)
      {
         pRteOutData_FSPSwitchStatus[FspId][slot] = PushButtonStatus_Error;
      }
   }
   else
   {
      // Do nothing: perform gateway logic    
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the FciToDtcConversion_FspSupply
//!
//! \param   pRteInData_FciCode    Provides the diagnostic information of FSP
//! 
//!======================================================================================
static void  FciToDtcConversion_FspSupply (const uint8 pRteInData_FciCode[MAX_FSP_NO])
{
   static boolean isFspVbtActive_FCI[MAX_FSP_NO] = {FALSE};
   static boolean isFspVatActive_FCI[MAX_FSP_NO] = {FALSE};
          boolean isFspVbtActive_DTC             = FALSE;
          boolean isFspVatActive_DTC             = FALSE;
          uint8   fsp                            = 0U;
   
   //! ###### Based on diagnostic information, event status will be updated
   //! ##### Check for FSPDiagInfoLIN status  
   for (fsp = FSP1; fsp < MAX_FSP_NO; fsp++)
   {
      if ((uint8)0x21== pRteInData_FciCode[fsp])
      {
         isFspVbtActive_FCI[fsp] = TRUE;
      }
      else if ((uint8)0x01 == pRteInData_FciCode[fsp])
      {
         isFspVbtActive_FCI[fsp] = FALSE;
      }
      else if ((uint8)0x22 == pRteInData_FciCode[fsp])
      {
         isFspVatActive_FCI[fsp] = TRUE;
      }
      else if ((uint8)0x02 == pRteInData_FciCode[fsp])
      {
         isFspVatActive_FCI[fsp] = FALSE;
      }
      else if ((uint8)0x00 == pRteInData_FciCode[fsp])
      {
         isFspVatActive_FCI[fsp] = FALSE;
		 isFspVbtActive_FCI[fsp] = FALSE;
      }
	  else
	  {
		  // do nothing: keep previous state, no FCI update
	  }
   }  
   isFspVbtActive_DTC = FALSE;
   isFspVatActive_DTC = FALSE;  
   for (fsp = FSP1; fsp < MAX_FSP_NO; fsp++)
   {
      isFspVbtActive_DTC = isFspVbtActive_DTC | isFspVbtActive_FCI[fsp];
      isFspVatActive_DTC = isFspVatActive_DTC | isFspVatActive_FCI[fsp];
   }
   //! ###### Invoke Rte_Call_Event_D1BN8_16_FSP_VBT_SetEventStatus
   if (TRUE == isFspVbtActive_DTC)
   {
      Rte_Call_Event_D1BN8_16_FSP_VBT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else
   {
      Rte_Call_Event_D1BN8_16_FSP_VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   //! ###### Invoke Rte_Call_Event_D1BN8_17_FSP_VAT_SetEventStatus
   if (TRUE == isFspVatActive_DTC)
   {
      Rte_Call_Event_D1BN8_17_FSP_VAT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else
   {
      Rte_Call_Event_D1BN8_17_FSP_VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
}

//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
