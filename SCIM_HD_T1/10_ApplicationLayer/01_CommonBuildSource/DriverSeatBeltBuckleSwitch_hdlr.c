/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  DriverSeatBeltBuckleSwitch_hdlr.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  DriverSeatBeltBuckleSwitch_hdlr
 *  Generated at:  Mon Jun 15 11:05:49 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <DriverSeatBeltBuckleSwitch_hdlr>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file DriverSeatBeltBuckleSwitch_hdlr.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_BodyAndComfort 
//! @{
//! @addtogroup DriverSeatBeltBuckleSwitch_hdlr
//! @{
//!
//! \brief
//! DriverSeatBeltBuckleSwitch_hdlr SWC.
//! ASIL Level : QM.
//! This module implements the logic for the DriverSeatBeltBuckleSwitch_hdlr runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * IOHWAB_UINT8
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CY1_DigitalBiLevelHigh_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CY1_DigitalBiLevelLow_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinFaultStatus
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinVoltage_0V2
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_DriverSeatBeltBuckleSwitch_hdlr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
// Includes
#include "DriverSeatBeltBuckleSwitch_hdlr.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IOHWAB_UINT8: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelHigh_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelLow_T: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DriverSeatBeltSwitch_T: Enumeration of integer in interval [0...3] with enumerators
 *   DriverSeatBeltSwitch_SeatBeltUnbuckledUnfastened (0U)
 *   DriverSeatBeltSwitch_SeatBeltBuckledFastened (1U)
 *   DriverSeatBeltSwitch_Error (2U)
 *   DriverSeatBeltSwitch_NotAvailable (3U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 * Record Types:
 * =============
 * SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T: Record with elements
 *   DigitalBiLevelLow of type SEWS_X1CY1_DigitalBiLevelLow_T
 *   DigitalBiLevelHigh of type SEWS_X1CY1_DigitalBiLevelHigh_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void)
 *
 *********************************************************************************************************************/


#define DriverSeatBeltBuckleSwitch_hdlr_START_SEC_CODE
 #include "DriverSeatBeltBuckleSwitch_hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
 
 #define PCODE_DigitalBiLevelVoltageConfig   (*Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v())

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DriverSeatBeltBuckleSwitch_hdlr_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DriverSeatBeltSwitch_DriverSeatBeltSwitch(DriverSeatBeltSwitch_T data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DriverSeatBeltBuckleSwitch_hdlr_20ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution runnable logic for the DriverSeatBeltBuckleSwitch_hdlr_20ms_runnable
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DriverSeatBeltBuckleSwitch_hdlr_CODE) DriverSeatBeltBuckleSwitch_hdlr_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DriverSeatBeltBuckleSwitch_hdlr_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   static DriverSeatBeltBuckleSwitch_hdlr_out_StructType RteOutData_Common = { DriverSeatBeltSwitch_NotAvailable,
                                                                               DriverSeatBeltSwitch_NotAvailable };
   DriverSeatBeltBuckleSwitch_hdlr_in_StructType         RteInData_Common  = {(VGTT_EcuPinVoltage_0V2)0,
                                                                              (VGTT_EcuPinVoltage_0V2)0,
                                                                               NonOperational,
                                                                               (VGTT_EcuPinFaultStatus)0};
   Std_ReturnType                                        retValue          = RTE_E_INVALID;
   SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T            AdiBiLevelLogic   = PCODE_DigitalBiLevelVoltageConfig;
  
   //! ###### Read the RTE ports and process RTE failure events
   //! ##### Read 'SwcActivation_IgnitionOn' interface
   retValue = Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&RteInData_Common.SwcActivation_IgnitionOn);
   MACRO_StdRteRead_IntRPort((retValue),(RteInData_Common.SwcActivation_IgnitionOn),(NonOperational))
   retValue = Rte_Call_AdiInterface_P_GetAdiPinState_CS(CONST_AdiPin9_SeatBeltBuckle,
                                                        &RteInData_Common.DriverSeatBelt_AdiVoltage,
                                                        &RteInData_Common.LivingPullUpVoltage,
                                                        &RteInData_Common.FaultStatus);
   //! ###### Check for 'SwcActivation_IgnitionOn' port is equal to 'Operational'
   if (Operational == RteInData_Common.SwcActivation_IgnitionOn)
   {
      //! ##### Check for RTE status
      if ((RTE_E_LIMIT == retValue)
         || (RTE_E_TIMEOUT == retValue))
      {
         //! #### Keep last processed value of corresponding input port
         RteOutData_Common.DriverSeatBeltSwitch = RteOutData_Common.Old_DriverSeatBeltSwitch;
      }
      //! ##### Process RTE failure events for error indication
      else if (RTE_E_OK != retValue)
      {
         // Updating the output port value to error
         RteOutData_Common.DriverSeatBeltSwitch = DriverSeatBeltSwitch_Error;
      }
      else
      {
         //! ##### Process the main logic
         //! #### Check for Adi pin different voltage values
         if (CONST_AdiBiLevelLogicNotAvailable == RteInData_Common.DriverSeatBelt_AdiVoltage)
         {
            // Update the output port is to value 'not available'
            RteOutData_Common.DriverSeatBeltSwitch = DriverSeatBeltSwitch_NotAvailable;
         }
         else if ((VGTT_EcuPinVoltage_0V2)AdiBiLevelLogic.DigitalBiLevelLow > RteInData_Common.DriverSeatBelt_AdiVoltage)
         {
            // Update the output port is to value 'UnbuckledUnfastened'
            RteOutData_Common.DriverSeatBeltSwitch = DriverSeatBeltSwitch_SeatBeltUnbuckledUnfastened;
         }
         else if ((VGTT_EcuPinVoltage_0V2)AdiBiLevelLogic.DigitalBiLevelHigh < RteInData_Common.DriverSeatBelt_AdiVoltage)
         {
            // Update the output port is to value 'BuckledFastened'
            RteOutData_Common.DriverSeatBeltSwitch = DriverSeatBeltSwitch_SeatBeltBuckledFastened;
         }
         else
         {
            // Do nothing: Keep 'DriverSeatBeltSwitch' port with previous value
         }
      }
   }
   //! ###### If 'SwcActivation_IgnitionOn' port is not equal to 'Operational'
   else
   {
      //! ##### update the output port value to 'not available'
      RteOutData_Common.DriverSeatBeltSwitch = DriverSeatBeltSwitch_NotAvailable;
   }
   //! ###### Process RTE write port
   Rte_Write_DriverSeatBeltSwitch_DriverSeatBeltSwitch(RteOutData_Common.DriverSeatBeltSwitch);
   // Update current value to old_value
   RteOutData_Common.Old_DriverSeatBeltSwitch = RteOutData_Common.DriverSeatBeltSwitch;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

//! @}
//! @}
//! @}

#define DriverSeatBeltBuckleSwitch_hdlr_STOP_SEC_CODE
#include "DriverSeatBeltBuckleSwitch_hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
