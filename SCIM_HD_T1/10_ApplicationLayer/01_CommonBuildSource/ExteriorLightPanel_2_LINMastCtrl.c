/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  ExteriorLightPanel_2_LINMastCtrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  ExteriorLightPanel_2_LINMastCtrl
 *  Generated at:  Fri Jun 12 17:01:40 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <ExteriorLightPanel_2_LINMastCtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file ExteriorLightPanel_2_LINMastCtrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_VehicleControlAndActiveSafety 
//! @{
//! @addtogroup VehicleControl 
//! @{
//! @addtogroup ExteriorLightPanel_2_LINMastCtrl
//! @{
//!
//! \brief
//! ExteriorLightPanel_2_LINMastCtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the ExteriorLightPanel_2_LINMastCtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Issm_IssStateType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_ExteriorLightPanel_2_LINMastCtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
// Includes
#include "FuncLibrary_AnwStateMachine_If.h"
#include "ExteriorLightPanel_2_LINMastCtrl.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DiagInfo_T: Integer in interval [0...127]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * ResponseErrorELCP2_T: Boolean
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 *   FreeWheel_Status_NoMovement (0U)
 *   FreeWheel_Status_1StepClockwise (1U)
 *   FreeWheel_Status_2StepsClockwise (2U)
 *   FreeWheel_Status_3StepsClockwise (3U)
 *   FreeWheel_Status_4StepsClockwise (4U)
 *   FreeWheel_Status_5StepsClockwise (5U)
 *   FreeWheel_Status_6StepsClockwise (6U)
 *   FreeWheel_Status_1StepCounterClockwise (7U)
 *   FreeWheel_Status_2StepsCounterClockwise (8U)
 *   FreeWheel_Status_3StepsCounterClockwise (9U)
 *   FreeWheel_Status_4StepsCounterClockwise (10U)
 *   FreeWheel_Status_5StepsCounterClockwise (11U)
 *   FreeWheel_Status_6StepsCounterClockwise (12U)
 *   FreeWheel_Status_Spare (13U)
 *   FreeWheel_Status_Error (14U)
 *   FreeWheel_Status_NotAvailable (15U)
 * Issm_IssStateType: Enumeration of integer in interval [0...2] with enumerators
 *   ISSM_STATE_INACTIVE (0U)
 *   ISSM_STATE_PENDING (1U)
 *   ISSM_STATE_ACTIVE (2U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *   FALSE (0U)
 *   TRUE (1U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1VR5_ELCP2_Installed_v(void)
 *
 *********************************************************************************************************************/


#define ExteriorLightPanel_2_LINMastCtrl_START_SEC_CODE
#include "ExteriorLightPanel_2_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_ELCP2_Installed    (Rte_Prm_P1VR5_ELCP2_Installed_v())
/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOT_Data_P1EOT_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, ExteriorLightPanel_2_LINMastCtrl_CODE) DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_SUBFUNCTIONNOTSUPPORTED;
  
  return RTE_E_DataServices_P1EOT_Data_P1EOT_E_NOT_OK;


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP2LinCtrl(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOT_Data_P1EOT_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, ExteriorLightPanel_2_LINMastCtrl_CODE) DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData (returns application error)
 *********************************************************************************************************************/
  Data[0] = (uint8) ((Rte_IrvRead_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP2LinCtrl())&CONST_Tester_Notpresent);

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ELCP2LinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOT_Data_P1EOT_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, ExteriorLightPanel_2_LINMastCtrl_CODE) DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  Rte_IrvWrite_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ELCP2LinCtrl((Rte_IrvRead_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP2LinCtrl())&CONST_Tester_Notpresent);
  *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_POSITIVERESPONSE;

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_ELCP2LinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOT_Data_P1EOT_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, ExteriorLightPanel_2_LINMastCtrl_CODE) DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

   uint8 retval = RTE_E_OK;

   if(Data[0] < 3U) // change 3 with Maximum Range value
   {
      Rte_IrvWrite_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_ELCP2LinCtrl(Data[0] | CONST_Tester_Present);
   }
   else
   {
      retval     = RTE_E_DataServices_P1EOT_Data_P1EOT_E_NOT_OK;
      *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_REQUESTOUTOFRANGE;
   }
   return retval;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ExteriorLightPanel_2_LINMastCtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_DiagInfoELCP2_DiagInfo(DiagInfo_T *data)
 *   Std_ReturnType Rte_Read_LIN_DRL_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_FogLightFront_ButtonStat_2_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_FogLightRear_ButtonStat_2_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_HeadLampUpDown_SwitchStatu_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_LightMode_Status_2_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_LIN_RearWorkProjector_BtnStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_RearWorkProjector_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ResponseErrorELCP2_ResponseErrorELCP2(ResponseErrorELCP2_T *data)
 *   boolean Rte_IsUpdated_LIN_LightMode_Status_2_FreeWheel_Status(void)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DRL_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_FogLightFront_ButtonStatus_2_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_FogLightRear_ButtonStatus_2_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_HeadLampUpDown_SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LIN_RearWorkProjector_Indicati_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LightMode_Status_2_FreeWheel_Status(FreeWheel_Status_T data)
 *   Std_ReturnType Rte_Write_RearWorkProjector_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_ExteriorLightPanel_2_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP2LinCtrl(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_ExteriorLightPanel_2_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP2LinCtrl(uint8 data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1F0B_16_ELCP2_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F0B_17_ELCP__VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F0B_44_ELCP2_RAM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F0B_45_ELCP_FLASH_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F0B_46_ELCP_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F0B_49_ELCP2_HWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F0B_94_ELCP2_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F0C_87_ELCP2Link_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ExteriorLightsRequest2_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ExteriorLightsRequest2_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ExteriorLightsRequest2_GetIssState(Issm_IssStateType *issState)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_WLight_InputELCP_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_WLight_InputELCP_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_WLight_InputELCP_GetIssState(Issm_IssStateType *issState)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ExteriorLightPanel_2_LINMastCtrl_20ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//!
//! \brief
//! This function implements the cyclic execution runnable logic for the ExteriorLightPanel_2_LINMastCtrl_20ms_runnable
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ExteriorLightPanel_2_LINMastCtrl_CODE) ExteriorLightPanel_2_LINMastCtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ExteriorLightPanel_2_LINMastCtrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   // Define RTE input data common
   static ExteriorLP_2_LINMastCtrl_in_StructType     RteInData_Common;
   // Define RTE output data common 
   static ExtLightPanel_2_LINMastCtrl_out_StructType RteOutData_Common = { PushButtonStatus_NotAvailable,
                                                                           PushButtonStatus_NotAvailable,
                                                                           PushButtonStatus_NotAvailable,
                                                                           A3PosSwitchStatus_NotAvailable,
                                                                           DeviceIndication_Off,
                                                                           FreeWheel_Status_NotAvailable,
                                                                           PushButtonStatus_NotAvailable };

   // Local logic variables 
   FormalBoolean  isActTrig_ExteriorLightsRequest2   = NO;
   FormalBoolean  isActTrig_WLight_InputELCP         = NO;
   FormalBoolean  isRestAct_ExteriorLightsRequest2   = YES;
   FormalBoolean  isRestAct_WLight_InputELCP         = YES;
   Std_ReturnType retValueELCP                       = RTE_E_INVALID;
   boolean        IsFciFaultActive                   = CONST_Fault_InActive;

   //! ###### Process RTE read input data common logic : 'Get_RteInDataRead_Common()'
   retValueELCP = Get_RteInDataRead_Common(&RteInData_Common); 
   //! ###### Check for communication mode of LIN4 is equal to 'error' state (bus off)
   if (Error == RteInData_Common.comMode_LIN4)
   {
      //! ##### Process the fallback logic : 'ExteriorLightPanel_2_Fallback_Mode()'
      ExteriorLightPanel_2_Fallback_Mode(&RteOutData_Common);
   }
   else
   {
      //! ###### Select 'ELCP2_Installed' parameter
      if (TRUE == PCODE_ELCP2_Installed)
      {
         //! ##### Check for communication mode of LIN4 status
         if ((ApplicationMonitoring == RteInData_Common.comMode_LIN4)
            || (Diagnostic == RteInData_Common.comMode_LIN4)
            || (SwitchDetection == RteInData_Common.comMode_LIN4))
         {
            isRestAct_ExteriorLightsRequest2 = NO;
            isRestAct_WLight_InputELCP       = NO;
            //! ##### If LIN frame is not received for 3 times(Missing frame condition)
            //! ##### Check for the RTE failure event reported on response error ELCP2
            if (RTE_E_MAX_AGE_EXCEEDED == retValueELCP)
            {
               //! ##### Check for communication mode of LIN4 status is 'ApplicationMonitoring' or 'SwitchDetection'
               if ((ApplicationMonitoring == RteInData_Common.comMode_LIN4)
                  || (SwitchDetection == RteInData_Common.comMode_LIN4))
               {
                  //! #### Invoke 'Rte_Call_Event_D1F0C_87_ELCP2Link_NoResp_SetEventStatus' with status 'failed' for missing frame
                  Rte_Call_Event_D1F0C_87_ELCP2Link_NoResp_SetEventStatus(DEM_EVENT_STATUS_FAILED);
                  //! #### Process the fallback mode logic : 'ExteriorLightPanel_2_Fallback_Mode()'
                  ExteriorLightPanel_2_Fallback_Mode(&RteOutData_Common);
               }
               else
               {
                  //! #### Process the signals gateway logic : 'ExteriorLightPanel_2_Signals_Gateway_Logic()'
                  ExteriorLightPanel_2_Signals_Gateway_Logic(&RteInData_Common,
                                                             &RteOutData_Common);
               }
            }
            //! ##### Check for response error ELCP2 status
            else if ((RTE_E_OK == retValueELCP)
                    || (RTE_E_COM_STOPPED == retValueELCP)
                    || (RTE_E_NEVER_RECEIVED == retValueELCP))
            {
               //! #### If LIN frame received without Receive timeout(remove the LIN missing frame fault)
               //! #### Invoke 'Rte_Call_Event_D1F0C_87_ELCP2Link_NoResp_SetEventStatus' with status 'passed' 
               Rte_Call_Event_D1F0C_87_ELCP2Link_NoResp_SetEventStatus(DEM_EVENT_STATUS_PASSED);
               //! #### Process FCI to DTC conversion logic : 'ExteriorLightPanel2_FCItoDTC_conversion()'
               IsFciFaultActive = ExteriorLightPanel2_FCItoDTC_conversion(&RteInData_Common.DiagInfoELCP2);
               //! #### Check for 'IsFciFaultActive' is active or not
               if (CONST_Fault_Active == IsFciFaultActive)
               {
                  //! #### Process the fallback mode logic : 'ExteriorLightPanel_2_Fallback_Mode()'
                  ExteriorLightPanel_2_Fallback_Mode(&RteOutData_Common);
               }
               else
               {
                  //! #### Process the signals gateway logic : 'ExteriorLightPanel_2_Signals_Gateway_Logic()'
                  ExteriorLightPanel_2_Signals_Gateway_Logic(&RteInData_Common,
                                                             &RteOutData_Common);
               }
            }
            else
            {
               //! ##### Process the fallback mode logic : 'ExteriorLightPanel_2_Fallback_Mode()'
               ExteriorLightPanel_2_Fallback_Mode(&RteOutData_Common);
            }
         }
         else
         {
            //! ##### Process exterior light panel2 deactivation logic : 'ExteriorLightPanel2_Deactivate_Logic()'
            ExteriorLightPanel2_Deactivate_Logic(&RteOutData_Common);
         }
      }
      else
      {
         //! ###### Process ExteriorLightPanel_2 Deactivation logic : 'ExteriorLightPanel2_Deactivate_Logic()'
         ExteriorLightPanel2_Deactivate_Logic(&RteOutData_Common);
      }
   }
   //! ###### Process exterior lights request2 trigger logic : 'ANW_ExteriorLightsRequest2_Cond()'
   isActTrig_ExteriorLightsRequest2 = ANW_ExteriorLightsRequest2_Cond(&RteInData_Common.LIN_LightMode_Status_2);
   //! ###### Process ANW exterior lights request2 logic : 'ANW_ExteriorLightsRequest2_Logic()'
   ANW_ExteriorLightsRequest2_Logic(isRestAct_ExteriorLightsRequest2,
                                    isActTrig_ExteriorLightsRequest2);
   //! ###### Process  WLight input ELCP trigger logic : 'ANW_WLight_InputELCP_ChkCondtn()'
   isActTrig_WLight_InputELCP = ANW_WLight_InputELCP_ChkCondtn(&RteInData_Common.LIN_RearWorkProjector_BtnStat);
   //! ###### Process ANW wlight inputelcp logic : 'ANW_WLight_InputELCP_Logic()'
   ANW_WLight_InputELCP_Logic(isRestAct_WLight_InputELCP,
                              isActTrig_WLight_InputELCP);
   if ((CONST_Tester_Present == ((Rte_IrvRead_ExteriorLightPanel_2_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP2LinCtrl()) & CONST_Tester_Present))
      && ((uint8)Diag_Active_TRUE == RteInData_Common.isDiagActive))
   {
      //! ###### Process input output control service logic : 'IOCtrlService_LinOutputSignalControl()'
      IOCtrlService_LinOutputSignalControl((uint8)((Rte_IrvRead_ExteriorLightPanel_2_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP2LinCtrl()) & CONST_Tester_Notpresent),
                                           &RteOutData_Common);
   }
   else 
   {
      Rte_IrvWrite_ExteriorLightPanel_2_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP2LinCtrl((uint8)RteOutData_Common.LIN_RearWorkProjector_Indicati);
   }
   //! ###### Process RTE output data write common logic : 'RteInDataWrite_Common()'
   RteInDataWrite_Common(&RteOutData_Common,
                         &RteInData_Common.IsUpdated_LIN_LightMode_Status_2);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

#define ExteriorLightPanel_2_LINMastCtrl_STOP_SEC_CODE
#include "ExteriorLightPanel_2_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'IOCtrlService_LinOutputSignalControl'
//!
//! \param     OverrideData    Provide override data value
//! \param     *pOutData       Update output data structure for ExteriorLightPanel_2_Signals_Gateway_Logic
//!
//!======================================================================================
static void IOCtrlService_LinOutputSignalControl(const uint8                                       OverrideData,
                                                       ExtLightPanel_2_LINMastCtrl_out_StructType  *pOutData)
{
   //! ###### Override Lin output signals with IOControl service value
   pOutData->LIN_RearWorkProjector_Indicati   = OverrideData;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'Get_RteInDataRead_Common' logic
//!
//! \param   *pExterior_In_data   Examine and update the input signals based on RTE failure events
//!
//! \return   Std_ReturnType      Returns 'retELCP' signal RTE read error status
//!
//!======================================================================================
static Std_ReturnType Get_RteInDataRead_Common(ExteriorLP_2_LINMastCtrl_in_StructType *pExterior_In_data)
{
   //! ###### Processing Rte InData read logic for error indication
   Std_ReturnType retValue = RTE_E_INVALID;
   Std_ReturnType retELCP  = RTE_E_INVALID;
   //! ##### Read isDiagActive interface
   retValue = Rte_Read_DiagActiveState_isDiagActive(&pExterior_In_data->isDiagActive);
   MACRO_StdRteRead_IntRPort((retValue), (pExterior_In_data->isDiagActive),( Diag_Active_FALSE))
   //! ##### Read DiagInfoELCP2 interface
   retValue = Rte_Read_DiagInfoELCP2_DiagInfo(&pExterior_In_data->DiagInfoELCP2);
   MACRO_StdRteRead_ExtRPort((retValue), (pExterior_In_data->DiagInfoELCP2),( 0U), (0U))
   //! ##### Read comMode_LIN4 interface
   retValue = Rte_Read_ComMode_LIN4_ComMode_LIN(&pExterior_In_data->comMode_LIN4);
   MACRO_StdRteRead_IntRPort((retValue),( pExterior_In_data->comMode_LIN4),( Error))
   //! ##### Read LIN_DRL_ButtonStatus interface
   retValue = Rte_Read_LIN_DRL_ButtonStatus_PushButtonStatus(&pExterior_In_data->LIN_DRL_ButtonStatus);
   MACRO_StdRteRead_ExtRPort((retValue),( pExterior_In_data->LIN_DRL_ButtonStatus), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))
   //! ##### Read LIN_FogLightFront_ButtonStat_2 interface
   retValue = Rte_Read_LIN_FogLightFront_ButtonStat_2_PushButtonStatus(&pExterior_In_data->LIN_FogLightFront_ButtonStat_2);
   MACRO_StdRteRead_ExtRPort((retValue),( pExterior_In_data->LIN_FogLightFront_ButtonStat_2), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))
   //! ##### Read LIN_FogLightRear_ButtonStat_2 interface
   retValue = Rte_Read_LIN_FogLightRear_ButtonStat_2_PushButtonStatus(&pExterior_In_data->LIN_FogLightRear_ButtonStat_2);
   MACRO_StdRteRead_ExtRPort((retValue), (pExterior_In_data->LIN_FogLightRear_ButtonStat_2), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))
   //! ##### Read LIN_HeadLampUpDown_SwitchStatus interface
   retValue = Rte_Read_LIN_HeadLampUpDown_SwitchStatu_A3PosSwitchStatus(&pExterior_In_data->LIN_HeadLampUpDown_SwitchStatu);
   MACRO_StdRteRead_ExtRPort((retValue),( pExterior_In_data->LIN_HeadLampUpDown_SwitchStatu), (A3PosSwitchStatus_NotAvailable),( A3PosSwitchStatus_Error))
   //! ##### Read IsUpdated_LIN_LightMode_Status_2 interface
   pExterior_In_data->IsUpdated_LIN_LightMode_Status_2 = Rte_IsUpdated_LIN_LightMode_Status_2_FreeWheel_Status();
   //! ##### Read LIN_LightMode_Status_2 interface
   pExterior_In_data->LIN_LightMode_Status_2.Previous = pExterior_In_data->LIN_LightMode_Status_2.Current;
   retValue = Rte_Read_LIN_LightMode_Status_2_FreeWheel_Status(&pExterior_In_data->LIN_LightMode_Status_2.Current); 
   MACRO_StdRteRead_ExtRPort((retValue), (pExterior_In_data->LIN_LightMode_Status_2.Current), (FreeWheel_Status_NotAvailable), (FreeWheel_Status_Error))   
   //! ##### Read LIN_RearWorkProjector_BtnStat interface
   pExterior_In_data->LIN_RearWorkProjector_BtnStat.Previous = pExterior_In_data->LIN_RearWorkProjector_BtnStat.Current;
   retValue = Rte_Read_LIN_RearWorkProjector_BtnStat_PushButtonStatus(&pExterior_In_data->LIN_RearWorkProjector_BtnStat.Current);
   MACRO_StdRteRead_ExtRPort((retValue), (pExterior_In_data->LIN_RearWorkProjector_BtnStat.Current), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))
   //! ##### Read RearWorkProjector_Indication interface  
   retValue = Rte_Read_RearWorkProjector_Indication_DeviceIndication(&pExterior_In_data->RearWorkProjector_Indication);
   MACRO_StdRteRead_IntRPort((retValue),( pExterior_In_data->RearWorkProjector_Indication), (DeviceIndication_Off))
   //! ##### Read ResponseErrorELCP2_LIN4 interface
   retELCP = Rte_Read_ResponseErrorELCP2_ResponseErrorELCP2(&pExterior_In_data->ResponseErrorELCP2_LIN4);
   MACRO_StdRteRead_ExtRPort((retELCP),( pExterior_In_data->ResponseErrorELCP2_LIN4), (1U), (1U))

   return retELCP;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'ExteriorLightPanel_2_Signals_Gateway_Logic'
//!
//! \param   *pExteriorLight_In_data      Providing the input signals to send to output ports
//! \param   *pExteriorLight_Out_data     Updating the output ports with input signals
//!
//!======================================================================================
static void ExteriorLightPanel_2_Signals_Gateway_Logic(const ExteriorLP_2_LINMastCtrl_in_StructType      *pExteriorLight_In_data,
                                                             ExtLightPanel_2_LINMastCtrl_out_StructType  *pExteriorLight_Out_data)
{
   //! ###### Update the output ports with values read on input ports 
   // Signals gateway, HMI status from exterior light panel 
   pExteriorLight_Out_data->DRL_ButtonStatus               = pExteriorLight_In_data->LIN_DRL_ButtonStatus;
   pExteriorLight_Out_data->FogLightFront_ButtonStatus_2   = pExteriorLight_In_data->LIN_FogLightFront_ButtonStat_2;
   pExteriorLight_Out_data->FogLightRear_ButtonStatus_2    = pExteriorLight_In_data->LIN_FogLightRear_ButtonStat_2;
   pExteriorLight_Out_data->HeadLampUpDown_SwitchStatus    = pExteriorLight_In_data->LIN_HeadLampUpDown_SwitchStatu;
   pExteriorLight_Out_data->RearWorkProjector_ButtonStatus = pExteriorLight_In_data->LIN_RearWorkProjector_BtnStat.Current;
   pExteriorLight_Out_data->LIN_RearWorkProjector_Indicati = pExteriorLight_In_data->RearWorkProjector_Indication;
   pExteriorLight_Out_data->LightMode_Status_2             = pExteriorLight_In_data->LIN_LightMode_Status_2.Current;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'ExteriorLightPanel_2_Fallback_Mode'
//!
//! \param   *pExterior_Output_data   Update the output ports to Error
//!
//!======================================================================================
static void ExteriorLightPanel_2_Fallback_Mode(ExtLightPanel_2_LINMastCtrl_out_StructType *pExterior_Output_data)
{
   //! ###### Update the output ports with error
   pExterior_Output_data->LightMode_Status_2             = FreeWheel_Status_Error;
   pExterior_Output_data->DRL_ButtonStatus               = PushButtonStatus_Error;
   pExterior_Output_data->FogLightFront_ButtonStatus_2   = PushButtonStatus_Error;
   pExterior_Output_data->FogLightRear_ButtonStatus_2    = PushButtonStatus_Error;
   pExterior_Output_data->RearWorkProjector_ButtonStatus = PushButtonStatus_Error;
   pExterior_Output_data->HeadLampUpDown_SwitchStatus    = A3PosSwitchStatus_Error; 
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'RteInDataWrite_Common' logic
//!
//! \param   *pExterior_Out_data                  Update the output signals to ports of output structure
//! \param   *pIsUpdated_LIN_LightMode_Status_2   Provide and Update the current value
//!
//!======================================================================================
static void RteInDataWrite_Common(const ExtLightPanel_2_LINMastCtrl_out_StructType  *pExterior_Out_data,
                                        boolean                                     *pIsUpdated_LIN_LightMode_Status_2)
{
   //! ###### Write the output ports with the updated signals
   Rte_Write_DRL_ButtonStatus_PushButtonStatus(pExterior_Out_data->DRL_ButtonStatus);
   Rte_Write_FogLightFront_ButtonStatus_2_PushButtonStatus(pExterior_Out_data->FogLightFront_ButtonStatus_2);
   Rte_Write_FogLightRear_ButtonStatus_2_PushButtonStatus(pExterior_Out_data->FogLightRear_ButtonStatus_2);
   Rte_Write_HeadLampUpDown_SwitchStatus_A3PosSwitchStatus(pExterior_Out_data->HeadLampUpDown_SwitchStatus);
   Rte_Write_LIN_RearWorkProjector_Indicati_DeviceIndication(pExterior_Out_data->LIN_RearWorkProjector_Indicati);   
   Rte_Write_RearWorkProjector_ButtonStatus_PushButtonStatus(pExterior_Out_data->RearWorkProjector_ButtonStatus);
   //! ###### Processing the free wheel update logic
   if (TRUE == *pIsUpdated_LIN_LightMode_Status_2)
   {
      *pIsUpdated_LIN_LightMode_Status_2 = FALSE;
      Rte_Write_LightMode_Status_2_FreeWheel_Status(pExterior_Out_data->LightMode_Status_2);
   }
   else if ((FreeWheel_Status_Error == pExterior_Out_data->LightMode_Status_2)
           || (FreeWheel_Status_NotAvailable == pExterior_Out_data->LightMode_Status_2))
   {
      Rte_Write_LightMode_Status_2_FreeWheel_Status(pExterior_Out_data->LightMode_Status_2);
   }
   else
   {
      // LightMode_Status_2 is not send to the output signal
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'ExteriorLightPanel2_FCItoDTC_conversion' logic
//!
//! \param   *pDiagInfoELCP2   Providing the diagnostic information of ELCP2
//!
//! \return   boolean             Returns 'IsFciFaultActive' status
//!
//!======================================================================================
static boolean ExteriorLightPanel2_FCItoDTC_conversion(const DiagInfo_T *pDiagInfoELCP2)
{
   static uint8 SlaveFCIFault[8] = { CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive };
   boolean IsFciFaultActive      = CONST_Fault_InActive;
   uint8 Index                   = 0U;

   //! ###### Check the FCI conditions and log/remove the faults
   if ((DiagInfo_T)0x41  == *pDiagInfoELCP2)
   {
      //! ##### Set supply voltage too low fault event to fail
      Rte_Call_Event_D1F0B_16_ELCP2_VBT_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x01  == *pDiagInfoELCP2)
   {
      //! ##### Set supply voltage too low fault event to pass
      Rte_Call_Event_D1F0B_16_ELCP2_VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x42 == *pDiagInfoELCP2)
   {
      //! ##### Set supply voltage too low fault event to fail
      Rte_Call_Event_D1F0B_17_ELCP__VAT_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x02 == *pDiagInfoELCP2)
   {
      //! ##### Set supply voltage too high fault event to pass
      Rte_Call_Event_D1F0B_17_ELCP__VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x43 == *pDiagInfoELCP2)
   {
      SlaveFCIFault[0] = CONST_SlaveFCIFault_Active;
      //! ##### Set ROM CS error fault event to fail
      Rte_Call_Event_D1F0B_46_ELCP_EEPROM_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x03 == *pDiagInfoELCP2)
   {
      SlaveFCIFault[0] = CONST_SlaveFCIFault_InActive;
      //! ##### Set ROM CS error fault event to pass
      Rte_Call_Event_D1F0B_46_ELCP_EEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x44 == *pDiagInfoELCP2)
   {
      SlaveFCIFault[1] = CONST_SlaveFCIFault_Active;
      //! ##### Set RAM check fault event to fail
      Rte_Call_Event_D1F0B_94_ELCP2_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x04 == *pDiagInfoELCP2)
   {
      SlaveFCIFault[1] = CONST_SlaveFCIFault_InActive;
      //! ##### Set RAM check fault event to pass
      Rte_Call_Event_D1F0B_94_ELCP2_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x45 == *pDiagInfoELCP2)
   {
      SlaveFCIFault[2] = CONST_SlaveFCIFault_Active;
      //! ##### Set watchdog error fault event to fail
      Rte_Call_Event_D1F0B_94_ELCP2_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x05 == *pDiagInfoELCP2)
   {
      SlaveFCIFault[2] = CONST_SlaveFCIFault_InActive;
      //! ##### Set watchdog error fault event to pass
      Rte_Call_Event_D1F0B_94_ELCP2_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x46 == *pDiagInfoELCP2)
   {
      SlaveFCIFault[3] = CONST_SlaveFCIFault_Active;
      //! ##### Set overrun fault event to fail
      Rte_Call_Event_D1F0B_94_ELCP2_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x06 == *pDiagInfoELCP2)
   {
      SlaveFCIFault[3] = CONST_SlaveFCIFault_InActive;
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1F0B_94_ELCP2_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x47 == *pDiagInfoELCP2)
   {
      SlaveFCIFault[4] = CONST_SlaveFCIFault_Active;
      //! ##### Set signal error fault event to fail
      Rte_Call_Event_D1F0B_94_ELCP2_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x07 == *pDiagInfoELCP2)
   {
      SlaveFCIFault[4] = CONST_SlaveFCIFault_InActive;
      //! ##### Set signal error fault event to pass
      Rte_Call_Event_D1F0B_94_ELCP2_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x48 == *pDiagInfoELCP2)
   {
      SlaveFCIFault[5] = CONST_SlaveFCIFault_Active;
      //! ##### Set unexpected operation fault event to fail
      Rte_Call_Event_D1F0B_94_ELCP2_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x08 == *pDiagInfoELCP2)
   {
      SlaveFCIFault[5] = CONST_SlaveFCIFault_InActive;
      //! ##### Set unexpected operation fault event to pass
      Rte_Call_Event_D1F0B_94_ELCP2_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x49 == *pDiagInfoELCP2)
   {
      SlaveFCIFault[6] = CONST_SlaveFCIFault_Active;
      //! ##### Set flash error fault event to fail
      Rte_Call_Event_D1F0B_45_ELCP_FLASH_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x09 == *pDiagInfoELCP2)
   {
      SlaveFCIFault[6] = CONST_SlaveFCIFault_InActive;
      //! ##### Set flash error fault event to pass
      Rte_Call_Event_D1F0B_45_ELCP_FLASH_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x4A == *pDiagInfoELCP2)
   {
      SlaveFCIFault[7] = CONST_SlaveFCIFault_Active;
      //! ##### Set rocker switch error fault event to fail
      Rte_Call_Event_D1F0B_49_ELCP2_HWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x0A == *pDiagInfoELCP2)
   {
      SlaveFCIFault[7] = CONST_SlaveFCIFault_InActive;
      //! ##### Set rocker switch error fault event to pass
      Rte_Call_Event_D1F0B_49_ELCP2_HWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if((DiagInfo_T)0x00 == *pDiagInfoELCP2)
   {
      for(Index = 0U;Index < 8U;Index++)
      {
         SlaveFCIFault[Index] = CONST_SlaveFCIFault_InActive;
      }
      Rte_Call_Event_D1F0B_16_ELCP2_VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1F0B_17_ELCP__VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1F0B_46_ELCP_EEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1F0B_94_ELCP2_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1F0B_49_ELCP2_HWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1F0B_45_ELCP_FLASH_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else
   {
      // Do nothing, keep previous status
   }
   IsFciFaultActive = CONST_Fault_InActive;
   for(Index = 0U;Index < 8U;Index++)
   {
      if(CONST_SlaveFCIFault_Active == SlaveFCIFault[Index])
      {
         IsFciFaultActive = CONST_Fault_Active;
      }
      else
      {
         // Do Nothing
      }
   }
   return IsFciFaultActive;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'ExteriorLightPanel2_Deactivate_Logic'
//!
//! \param   *pExteriorLightpanel_Out_data   Update the output ports to off or notavailable
//!
//!======================================================================================
static void ExteriorLightPanel2_Deactivate_Logic(ExtLightPanel_2_LINMastCtrl_out_StructType *pExteriorLightpanel_Out_data)
{
   //! ###### Write all output ports to value 'Not available' or 'Off'
   pExteriorLightpanel_Out_data->LIN_RearWorkProjector_Indicati = DeviceIndication_Off;
   pExteriorLightpanel_Out_data->LightMode_Status_2             = FreeWheel_Status_NotAvailable;
   pExteriorLightpanel_Out_data->DRL_ButtonStatus               = PushButtonStatus_NotAvailable;
   pExteriorLightpanel_Out_data->FogLightFront_ButtonStatus_2   = PushButtonStatus_NotAvailable;
   pExteriorLightpanel_Out_data->FogLightRear_ButtonStatus_2    = PushButtonStatus_NotAvailable;
   pExteriorLightpanel_Out_data->RearWorkProjector_ButtonStatus = PushButtonStatus_NotAvailable;
   pExteriorLightpanel_Out_data->HeadLampUpDown_SwitchStatus    = A3PosSwitchStatus_NotAvailable;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'ANW_ExteriorLightsRequest2_Cond'
//!
//! \param   *pLIN_LightMode_Status_2   Providing LIN_LightMode_Status_2 value
//!
//! \return   FormalBoolean             Returns 'isActTrig_ExteriorLightsRequest2' status
//!
//!======================================================================================
static FormalBoolean ANW_ExteriorLightsRequest2_Cond(const FreeWheel_Status  *pLIN_LightMode_Status_2)
{
   FormalBoolean isActTrig_ExteriorLightsRequest2 = NO;

   //! ###### Check for the LIN_LightMode_Status_2 status
   if ((pLIN_LightMode_Status_2->Previous != pLIN_LightMode_Status_2->Current)
      && ((FreeWheel_Status_NoMovement != pLIN_LightMode_Status_2->Current)
      && (FreeWheel_Status_Spare != pLIN_LightMode_Status_2->Current)
      && (FreeWheel_Status_Error != pLIN_LightMode_Status_2->Current)
      && (FreeWheel_Status_NotAvailable != pLIN_LightMode_Status_2->Current)))
   {
      isActTrig_ExteriorLightsRequest2 = YES;
   }
   else
   {
      isActTrig_ExteriorLightsRequest2 = NO;
   }
   return isActTrig_ExteriorLightsRequest2;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'ANW_WLight_InputELCP_ChkCondtn'
//!
//! \param   *pLIN_RearWorkProjector_BtnStat   Providing LIN_RearWorkProjector button status
//!
//! \return   FormalBoolean                    Returns 'isActTrig_WLight_InputELCP' status
//!
//!======================================================================================
static FormalBoolean ANW_WLight_InputELCP_ChkCondtn(const PushButtonStatus  *pLIN_RearWorkProjector_BtnStat)
{
   FormalBoolean isActTrig_WLight_InputELCP = NO;

   //! ###### Check for LIN_RearWorkProjector button status
   if ((pLIN_RearWorkProjector_BtnStat->Previous != pLIN_RearWorkProjector_BtnStat->Current)
      && (PushButtonStatus_Pushed == pLIN_RearWorkProjector_BtnStat->Current))
   {
      isActTrig_WLight_InputELCP = YES;
   }
   else
   {
      isActTrig_WLight_InputELCP = NO;
   }
   return isActTrig_WLight_InputELCP;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'ANW_ExteriorLightsRequest2_Logic'
//!
//! \param   isRestAct_ExteriorLightsRequest2   Provides 'yes' or 'no' values based on restriction activation
//! \param   isActTrig_ExteriorLightsRequest2   Provides 'yes' or 'no' values based on activation trigger
//!
//!======================================================================================
static void ANW_ExteriorLightsRequest2_Logic(const FormalBoolean isRestAct_ExteriorLightsRequest2,
                                             const FormalBoolean isActTrig_ExteriorLightsRequest2)
{
   //! ###### Process the application network (Activation/Deactivation) conditions
   static AnwSM_States AnwState_ExtLightsRequest2                    = { SM_ANW_Inactive,
                                                                         SM_ANW_Inactive };
   static DeactivationTimer_Type ExtLightsRequest2_DeactivationTimer = 0U;
   AnwAction_Enum AnwActionstatus                                    = ANW_Action_None;
   //! ###### Trigger activation/deactivation of application network request to ISSM module
   //! ##### Process ANW logic : 'ProcessAnwLogic()'
   AnwActionstatus = ProcessAnwLogic(isRestAct_ExteriorLightsRequest2,
                                     isActTrig_ExteriorLightsRequest2,
                                     YES,
                                     CONST_ANW_ExtLightsRequest2_Timeout,
                                     &ExtLightsRequest2_DeactivationTimer,
                                     &AnwState_ExtLightsRequest2);
   //! ##### Check for Anw action is 'activate' or 'deactivate'
   if (ANW_Action_Activate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_ExteriorLightsRequest2_ActivateIss();
   }
   else if (ANW_Action_Deactivate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_ExteriorLightsRequest2_DeactivateIss();
   }
   else
   {
      // Do nothing, keep Previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'ANW_WLight_InputELCP_Logic' Trigger
//!
//! \param   isRestAct_WLight_InputELCP   Provides 'yes' or 'no' values based on restriction activation
//! \param   isActTrig_WLight_InputELCP   Provides 'yes' or 'no' values based on activation trigger
//!
//!======================================================================================
static void ANW_WLight_InputELCP_Logic(const FormalBoolean isRestAct_WLight_InputELCP,
                                       const FormalBoolean isActTrig_WLight_InputELCP)
{
   //! ###### Process the application network (Activation/Deactivation) conditions
   static AnwSM_States AnwState_WLight_InputELCP                    = { SM_ANW_Inactive,
                                                                        SM_ANW_Inactive };
   static DeactivationTimer_Type WLight_InputELCP_DeactivationTimer = 0U;
   AnwAction_Enum AnwActionstatus                                   = ANW_Action_None;
   //! ###### Trigger activation/deactivation of application network request to ISSM module
   //! ##### Process ANW logic : 'ProcessAnwLogic()'
   AnwActionstatus = ProcessAnwLogic(isRestAct_WLight_InputELCP,
                                     isActTrig_WLight_InputELCP,
                                     YES,
                                     CONST_ANW_WLight_InputELCP_Timeout,
                                     &WLight_InputELCP_DeactivationTimer,
                                     &AnwState_WLight_InputELCP);
   //! ##### Check for Anw action is 'activate' or 'deactivate'
   if (ANW_Action_Activate == AnwActionstatus)
   {
      // Invoke 'Rte_Call_UR_ANW_WLight_InputELCP_ActivateIss' on activation
      Rte_Call_UR_ANW_WLight_InputELCP_ActivateIss();
   }
   else if (ANW_Action_Deactivate == AnwActionstatus)
   {
      // Invoke 'Rte_Call_UR_ANW_WLight_InputELCP_DeactivateIss' on activation
      Rte_Call_UR_ANW_WLight_InputELCP_DeactivateIss();
   }
   else
   {
      // Do nothing, keep Previous status
   }
}

//! @}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#if 0
/***  Start of saved code (symbol: runnable implementation:DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU)  */

  return RTE_E_OK;

/***  End of saved code  ************************************************************************************/
#endif

#if 0
/***  Start of saved code (symbol: documentation area:DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData_doc)  */


/***  End of saved code  ************************************************************************************/
#endif

#if 0
/***  Start of saved code (symbol: documentation area:DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState_doc)  */


/***  End of saved code  ************************************************************************************/
#endif

#if 0
/***  Start of saved code (symbol: runnable implementation:DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData)  */

  return RTE_E_OK;

/***  End of saved code  ************************************************************************************/
#endif

#if 0
/***  Start of saved code (symbol: runnable implementation:DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState)  */

  return RTE_E_OK;

/***  End of saved code  ************************************************************************************/
#endif

#if 0
/***  Start of saved code (symbol: documentation area:DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment_doc)  */


/***  End of saved code  ************************************************************************************/
#endif

#if 0
/***  Start of saved code (symbol: runnable implementation:DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment)  */

  return RTE_E_OK;

/***  End of saved code  ************************************************************************************/
#endif

#if 0
/***  Start of saved code (symbol: documentation area:DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU_doc)  */


/***  End of saved code  ************************************************************************************/
#endif

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
