/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  ECSWiredRemote_LINMasterCtrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  ECSWiredRemote_LINMasterCtrl
 *  Generated at:  Fri Jun 12 17:01:40 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <ECSWiredRemote_LINMasterCtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file ECSWiredRemote_LINMasterCtrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_VehicleDynamics_Suspension 
//! @{
//! @addtogroup ECSWiredRemote_LINMasterCtrl
//! @{
//!
//! \brief
//! ECSWiredRemote_LINMasterCtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the ECSWiredRemote_LINMasterCtrl runnables.
//!======================================================================================


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_ECSWiredRemote_LINMasterCtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "FuncLibrary_AnwStateMachine_If.h"
#include "ECSWiredRemote_LINMasterCtrl.h"
#include "FuncLibrary_ScimStd_If.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DiagInfo_T: Integer in interval [0...127]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * ResponseErrorRCECS: Boolean
 * ShortPulseMaxLength_T: Integer in interval [0...15]
 *   Unit: [ms], Factor: 1, Offset: 0
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * EvalButtonRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   EvalButtonRequest_Neutral (0U)
 *   EvalButtonRequest_EvaluatingPush (1U)
 *   EvalButtonRequest_ContinuouslyPushed (2U)
 *   EvalButtonRequest_ShortPush (3U)
 *   EvalButtonRequest_Spare1 (4U)
 *   EvalButtonRequest_Spare2 (5U)
 *   EvalButtonRequest_Error (6U)
 *   EvalButtonRequest_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *   FALSE (0U)
 *   TRUE (1U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1ALT_ECS_PartialAirSystem_v(void)
 *   boolean Rte_Prm_P1ALU_ECS_FullAirSystem_v(void)
 *
 *********************************************************************************************************************/


#define ECSWiredRemote_LINMasterCtrl_START_SEC_CODE
#include "ECSWiredRemote_LINMasterCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_ECS_PartialAirSystem     (Rte_Prm_P1ALT_ECS_PartialAirSystem_v())
#define PCODE_ECS_FullAirSystem        (Rte_Prm_P1ALU_ECS_FullAirSystem_v())

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOV_Data_P1EOV_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, ECSWiredRemote_LINMasterCtrl_CODE) DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

   *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_SUBFUNCTIONNOTSUPPORTED;

   return RTE_E_DataServices_P1EOV_Data_P1EOV_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData_Irv_IOCTL_RCECSLinCtrl(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOV_Data_P1EOV_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, ECSWiredRemote_LINMasterCtrl_CODE) DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData (returns application error)
 *********************************************************************************************************************/
   Data[0] = (uint8)((Rte_IrvRead_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData_Irv_IOCTL_RCECSLinCtrl()) & CONST_Tester_Notpresent);

   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_RCECSLinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOV_Data_P1EOV_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, ECSWiredRemote_LINMasterCtrl_CODE) DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/
   Rte_IrvWrite_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_RCECSLinCtrl((Rte_IrvRead_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData_Irv_IOCTL_RCECSLinCtrl()) & CONST_Tester_Notpresent); 
   *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_POSITIVERESPONSE;
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_RCECSLinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOV_Data_P1EOV_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, ECSWiredRemote_LINMasterCtrl_CODE) DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

   uint8 retval = RTE_E_OK;
   *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_POSITIVERESPONSE;
   if (Data[0] < 3U) 
   {
      Rte_IrvWrite_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_RCECSLinCtrl(Data[0] | CONST_Tester_Present); // change 0x80 with MSB Mask macro
   }
   else
   {
      retval = RTE_E_DataServices_P1EOV_Data_P1EOV_E_NOT_OK;
      *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_REQUESTOUTOFRANGE;
   }

   return retval;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ECSWiredRemote_LINMasterCtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Adjust_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN5_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_DiagInfoRCECS_DiagInfo(DiagInfo_T *data)
 *   Std_ReturnType Rte_Read_Down_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_LIN_AdjustButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BackButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_MemButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_SelectButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_StopButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_WRDownButtonStatus_EvalButtonRequest(EvalButtonRequest_T *data)
 *   Std_ReturnType Rte_Read_LIN_WRUpButtonStatus_EvalButtonRequest(EvalButtonRequest_T *data)
 *   Std_ReturnType Rte_Read_M1_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_M2_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_M3_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ResponseErrorRCECS_ResponseErrorRCECS(ResponseErrorRCECS *data)
 *   Std_ReturnType Rte_Read_ShortPulseMaxLength_ShortPulseMaxLength(ShortPulseMaxLength_T *data)
 *   Std_ReturnType Rte_Read_Up_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_AdjustButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BackButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_LIN_Adjust_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_Down_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_M1_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_M2_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_M3_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_ShortPulseMaxLength_ShortPulseMaxLength(ShortPulseMaxLength_T data)
 *   Std_ReturnType Rte_Write_LIN_Up_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_MemButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_SelectButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_StopButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_WRDownButtonStatus_EvalButtonRequest(EvalButtonRequest_T data)
 *   Std_ReturnType Rte_Write_WRUpButtonStatus_EvalButtonRequest(EvalButtonRequest_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_ECSWiredRemote_LINMasterCtrl_20ms_runnable_Irv_IOCTL_RCECSLinCtrl(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_ECSWiredRemote_LINMasterCtrl_20ms_runnable_Irv_IOCTL_RCECSLinCtrl(uint8 data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BKE_87_RCECSLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOI_16_RCECS_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOI_17_RCECS_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOI_46_RCECS_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOI_94_RCECS_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ECSStandByTrigger1_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ECSStandByTrigger1_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ECSWiredRemote_LINMasterCtrl_20ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution runnable logic for the ECSWiredRemote_LINMasterCtrl_20ms_runnable
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ECSWiredRemote_LINMasterCtrl_CODE) ECSWiredRemote_LINMasterCtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ECSWiredRemote_LINMasterCtrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   static ECSWiredRemoteLINMasterCtrl_InData_T  RteInData_Common;
   static ECSWiredRemoteLINMasterCtrl_OutData_T RteOutData_Common = { PushButtonStatus_NotAvailable,
                                                                      PushButtonStatus_NotAvailable,
                                                                      PushButtonStatus_NotAvailable,
                                                                      PushButtonStatus_NotAvailable,
                                                                      PushButtonStatus_NotAvailable,
                                                                      EvalButtonRequest_NotAvailable,
                                                                      EvalButtonRequest_NotAvailable,
                                                                      DeviceIndication_Off,
                                                                      DeviceIndication_Off,
                                                                      DeviceIndication_Off,
                                                                      DeviceIndication_Off,
                                                                      DeviceIndication_Off,
                                                                      DeviceIndication_Off,
                                                                      0U };  // Enumeration is not available 
   // Local logic variables 
   FormalBoolean  isActTrigDet     = NO;
   FormalBoolean  isRestAct        = YES;
   Std_ReturnType retValueRCECS    = RTE_E_INVALID;
   boolean        IsFciFaultActive = CONST_Fault_InActive;

   //! ###### Process RTE input data read common logic : 'Get_RteDataRead_Common()'
   retValueRCECS = Get_RteDataRead_Common(&RteInData_Common);
   //! ###### Check for LIN bus status
   //! ###### Check for communication mode of LIN5 is equal to 'error' state (Bus off)
   if (Error == RteInData_Common.ComMode_LIN5)
   {
      //! ##### Process the fallback mode logic : 'ECSWiredRemoteLINMasterCtrl_FallbackLogic()'
      ECSWiredRemoteLINMasterCtrl_FallbackLogic(&RteOutData_Common);
   }
   else
   {
      //! ###### Select 'ECS_PartialAirSystem' or 'ECS_FullAirSystem' parameter
      if ((TRUE == PCODE_ECS_PartialAirSystem)
         || (TRUE == PCODE_ECS_FullAirSystem))
      {
         //! ##### Check for communication mode of LIN5(ComMode_LIN5) status
         if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN5)
            || (SwitchDetection == RteInData_Common.ComMode_LIN5)
            || (Diagnostic == RteInData_Common.ComMode_LIN5))
         {
            isRestAct = NO;
            //! ##### Check for the RTE failure event reported on ResponseErrorRCECS_LIN5 (missing frame condition)
            if (RTE_E_MAX_AGE_EXCEEDED == retValueRCECS)
            {
               //! ##### Check for communication mode of LIN5(ComMode_LIN5) status is 'ApplicationMonitoring' or 'SwitchDetection'
               if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN5)
                  || (SwitchDetection == RteInData_Common.ComMode_LIN5))
               {
                  //! #### Log the LIN missing frame fault
                  Rte_Call_Event_D1BKE_87_RCECSLink_NoResp_SetEventStatus(DEM_EVENT_STATUS_FAILED);
                  //! #### Process the fallback mode logic : 'ECSWiredRemoteLINMasterCtrl_FallbackLogic()'
                  ECSWiredRemoteLINMasterCtrl_FallbackLogic(&RteOutData_Common);
               }
               else
               {
                  //! ##### Status of communication mode of LIN5 is in diagnostic mode
                  //! #### Process the signals gateway logic : 'ECSWiredRemote_Signals_Gateway_Logic()'
                  ECSWiredRemote_Signals_Gateway_Logic(&RteInData_Common,
                                                       &RteOutData_Common);
               }
            }
            //! ##### Check for 'ResponseErrorRCECS_LIN5' is equal to RTE_OK or RTE_E_COM_STOPPED or RTE_E_NEVER_RECEIVED
            else if ((RTE_E_OK == retValueRCECS)
                    || (RTE_E_COM_STOPPED == retValueRCECS)
                    || (RTE_E_NEVER_RECEIVED == retValueRCECS))
            {
               //! ##### Remove the LIN missing frame fault
               Rte_Call_Event_D1BKE_87_RCECSLink_NoResp_SetEventStatus(DEM_EVENT_STATUS_PASSED);
               //! ##### Process fault code information(FCI) indication : 'ECSWiredRemote_LINMasterCtrl_FCI_Indication()'
               IsFciFaultActive = ECSWiredRemote_LINMasterCtrl_FCI_Indication(&RteInData_Common.DiagInfoRCECS_LIN5);
               if (CONST_Fault_Active == IsFciFaultActive)
               {
                  //! #### Process fallback mode logic : 'ECSWiredRemoteLINMasterCtrl_FallbackLogic()'
                  ECSWiredRemoteLINMasterCtrl_FallbackLogic(&RteOutData_Common);
               }
               else
               {
                  //! #### Process signals gateway logic : 'ECSWiredRemote_Signals_Gateway_Logic()'
                  ECSWiredRemote_Signals_Gateway_Logic(&RteInData_Common,
                                                       &RteOutData_Common);
               }
            }
            else
            {
               //! ##### Process fallback mode logic : 'ECSWiredRemoteLINMasterCtrl_FallbackLogic()'
               ECSWiredRemoteLINMasterCtrl_FallbackLogic(&RteOutData_Common);
            }
         }
         else
         {
            //! ##### Process deactivation logic : 'ECSWiredRemoteLINMasterCtrl_DeactivateLogic()'
            ECSWiredRemoteLINMasterCtrl_DeactivateLogic(&RteOutData_Common);
         }
      }
      else
      {
         //! ###### Process deactivation logic : 'ECSWiredRemoteLINMasterCtrl_DeactivateLogic()'
         ECSWiredRemoteLINMasterCtrl_DeactivateLogic(&RteOutData_Common);
      }
   }
   //! ###### Processing ECS standby trigger1 condition check logic : 'ANW_ECSStandByTrigger1_Cond()'
   isActTrigDet = ANW_ECSStandByTrigger1_Cond(&RteInData_Common);
   //! ###### Activating application networks by ECSStandbyTrigger condition : 'Generic_ANW_ECSStandByTrigger1()'
   Generic_ANW_ECSStandByTrigger1(isRestAct,
                                  isActTrigDet);
   if ((CONST_Tester_Present == ((Rte_IrvRead_ECSWiredRemote_LINMasterCtrl_20ms_runnable_Irv_IOCTL_RCECSLinCtrl()) & CONST_Tester_Present) )
      && ((uint8)Diag_Active_TRUE == RteInData_Common.isDiagActive))
   {
      //! ###### Process input output control service logic : 'IOCtrlService_LinOutputSignalControl()'
      IOCtrlService_LinOutputSignalControl((uint8)((Rte_IrvRead_ECSWiredRemote_LINMasterCtrl_20ms_runnable_Irv_IOCTL_RCECSLinCtrl()) & CONST_Tester_Notpresent),
                                           &RteOutData_Common);
   }
   else
   {
      Rte_IrvWrite_ECSWiredRemote_LINMasterCtrl_20ms_runnable_Irv_IOCTL_RCECSLinCtrl(DeviceIndication_SpareValue);
   }

   //! ###### Process RTE data write common logic : 'RteDataWrite_Common()'
   RteDataWrite_Common(&RteOutData_Common);
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

#define ECSWiredRemote_LINMasterCtrl_STOP_SEC_CODE
#include "ECSWiredRemote_LINMasterCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'IOCtrlService_LinOutputSignalControl'
//!
//! \param     OverrideData            Provide the override data value
//! \param     *pOutput_struct_Data    Update the output data structure for ECSWiredRemote_Signals_Gateway_Logic
//!
//!======================================================================================
static void IOCtrlService_LinOutputSignalControl(const uint8                                  OverrideData,
                                                       ECSWiredRemoteLINMasterCtrl_OutData_T  *pOutput_struct_Data)
{
   //! ###### Override LIN output signals with IOControl service value
   pOutput_struct_Data->LIN_Adjust_DeviceIndication = OverrideData;
   pOutput_struct_Data->LIN_Down_DeviceIndication   = OverrideData;
   pOutput_struct_Data->LIN_M1_DeviceIndication     = OverrideData;
   pOutput_struct_Data->LIN_M2_DeviceIndication     = OverrideData;
   pOutput_struct_Data->LIN_M3_DeviceIndication     = OverrideData;
   //pOutput_struct_Data->LIN_ShortPulseMaxLength = OverrideData;   //it is not device indication type
   pOutput_struct_Data->LIN_Up_DeviceIndication     = OverrideData;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'RteDataWrite_Common'
//!
//! \param   *pRteOutData_Common   Update the output signals to ports of output structure
//!
//!======================================================================================
static void RteDataWrite_Common(const ECSWiredRemoteLINMasterCtrl_OutData_T  *pRteOutData_Common)
{
   //! ###### Processing Rte data write common logic
   Rte_Write_AdjustButtonStatus_PushButtonStatus(pRteOutData_Common->AdjustButtonStatus);
   Rte_Write_BackButtonStatus_PushButtonStatus(pRteOutData_Common->BackButtonStatus);
   Rte_Write_MemButtonStatus_PushButtonStatus(pRteOutData_Common->MemButtonStatus);
   Rte_Write_SelectButtonStatus_PushButtonStatus(pRteOutData_Common->SelectButtonStatus);
   Rte_Write_StopButtonStatus_PushButtonStatus(pRteOutData_Common->StopButtonStatus);
   Rte_Write_WRDownButtonStatus_EvalButtonRequest(pRteOutData_Common->WRDownButtonStatus);
   Rte_Write_WRUpButtonStatus_EvalButtonRequest(pRteOutData_Common->WRUpButtonStatus);
   Rte_Write_LIN_Adjust_DeviceIndication_DeviceIndication(pRteOutData_Common->LIN_Adjust_DeviceIndication);
   Rte_Write_LIN_Down_DeviceIndication_DeviceIndication(pRteOutData_Common->LIN_Down_DeviceIndication);
   Rte_Write_LIN_Up_DeviceIndication_DeviceIndication(pRteOutData_Common->LIN_Up_DeviceIndication);
   Rte_Write_LIN_M1_DeviceIndication_DeviceIndication(pRteOutData_Common->LIN_M1_DeviceIndication);
   Rte_Write_LIN_M2_DeviceIndication_DeviceIndication(pRteOutData_Common->LIN_M2_DeviceIndication);
   Rte_Write_LIN_M3_DeviceIndication_DeviceIndication(pRteOutData_Common->LIN_M3_DeviceIndication);
   Rte_Write_LIN_ShortPulseMaxLength_ShortPulseMaxLength(pRteOutData_Common->LIN_ShortPulseMaxLength);
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'Get_RteDataRead_Common'
//!
//! \param   *pRteInData_Common   Examine and update the input signals based on RTE failure events
//! 
//! \return   Std_ReturnType       Returns 'retRCECS' signal RTE read error status
//!
//!======================================================================================
static Std_ReturnType Get_RteDataRead_Common(ECSWiredRemoteLINMasterCtrl_InData_T  *pRteInData_Common)
{
   Std_ReturnType retValue = RTE_E_INVALID;
   Std_ReturnType retRCECS = RTE_E_INVALID;
   //! ###### Read common RTE interfaces, store the previous value and process fallback modes
   //! ##### Read isDiagActive interface
   retValue = Rte_Read_DiagActiveState_isDiagActive(&pRteInData_Common->isDiagActive);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->isDiagActive), (Diag_Active_FALSE))
   pRteInData_Common->VarLIN_AdjustButtonStatus.Previous = pRteInData_Common->VarLIN_AdjustButtonStatus.Current;
   //! ##### Read VarLIN_AdjustButtonStatus interface
   retValue = Rte_Read_LIN_AdjustButtonStatus_PushButtonStatus(&pRteInData_Common->VarLIN_AdjustButtonStatus.Current);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->VarLIN_AdjustButtonStatus.Current), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))
   pRteInData_Common->VarLIN_BackButtonStatus.Previous = pRteInData_Common->VarLIN_BackButtonStatus.Current;
   //! ##### Read VarLIN_BackButtonStatus interface
   retValue = Rte_Read_LIN_BackButtonStatus_PushButtonStatus(&pRteInData_Common->VarLIN_BackButtonStatus.Current);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->VarLIN_BackButtonStatus.Current), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))
   pRteInData_Common->VarLIN_MemButtonStatus.Previous = pRteInData_Common->VarLIN_MemButtonStatus.Current;
   //! ##### Read VarLIN_MemButtonStatus interface
   retValue = Rte_Read_LIN_MemButtonStatus_PushButtonStatus(&pRteInData_Common->VarLIN_MemButtonStatus.Current);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->VarLIN_MemButtonStatus.Current), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))
   pRteInData_Common->VarLIN_SelectButtonStatus.Previous = pRteInData_Common->VarLIN_SelectButtonStatus.Current;
   //! ##### Read VarLIN_SelectButtonStatus interface
   retValue = Rte_Read_LIN_SelectButtonStatus_PushButtonStatus(&pRteInData_Common->VarLIN_SelectButtonStatus.Current);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->VarLIN_SelectButtonStatus.Current), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))
   pRteInData_Common->VarLIN_StopButtonStatus.Previous = pRteInData_Common->VarLIN_StopButtonStatus.Current;
   //! ##### Read VarLIN_StopButtonStatus interface
   retValue = Rte_Read_LIN_StopButtonStatus_PushButtonStatus(&pRteInData_Common->VarLIN_StopButtonStatus.Current);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->VarLIN_StopButtonStatus.Current), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))
   pRteInData_Common->VarLIN_WRDownButtonStatus.Previous = pRteInData_Common->VarLIN_WRDownButtonStatus.Current;
   //! ##### Read VarLIN_WRDownButtonStatus interface
   retValue = Rte_Read_LIN_WRDownButtonStatus_EvalButtonRequest(&pRteInData_Common->VarLIN_WRDownButtonStatus.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->VarLIN_WRDownButtonStatus.Current), (EvalButtonRequest_NotAvailable), (EvalButtonRequest_Error))
   pRteInData_Common->VarLIN_WRUpButtonStatus.Previous = pRteInData_Common->VarLIN_WRUpButtonStatus.Current;
   //! ##### Read VarLIN_WRUpButtonStatus interface
   retValue = Rte_Read_LIN_WRUpButtonStatus_EvalButtonRequest(&pRteInData_Common->VarLIN_WRUpButtonStatus.Current);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->VarLIN_WRUpButtonStatus.Current), (EvalButtonRequest_NotAvailable), (EvalButtonRequest_Error))
   //! ##### Read Adjust_DeviceIndication interface
   retValue = Rte_Read_Adjust_DeviceIndication_DeviceIndication(&pRteInData_Common->Adjust_DeviceIndication);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->Adjust_DeviceIndication), (DeviceIndication_Off))
   //! ##### Read Down_DeviceIndication interface
   retValue = Rte_Read_Down_DeviceIndication_DeviceIndication(&pRteInData_Common->Down_DeviceIndication);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->Down_DeviceIndication), (DeviceIndication_Off))
   //! ##### Read Up_DeviceIndication interface
   retValue = Rte_Read_Up_DeviceIndication_DeviceIndication(&pRteInData_Common->Up_DeviceIndication);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->Up_DeviceIndication), (DeviceIndication_Off))
   //! ##### Read M1_DeviceIndication interface
   retValue = Rte_Read_M1_DeviceIndication_DeviceIndication(&pRteInData_Common->M1_DeviceIndication);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->M1_DeviceIndication), (DeviceIndication_Off))
   //! ##### Read M2_DeviceIndication interface
   retValue = Rte_Read_M2_DeviceIndication_DeviceIndication(&pRteInData_Common->M2_DeviceIndication);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->M2_DeviceIndication), (DeviceIndication_Off))
   //! ##### Read M3_DeviceIndication interface
   retValue = Rte_Read_M3_DeviceIndication_DeviceIndication(&pRteInData_Common->M3_DeviceIndication);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->M3_DeviceIndication), (DeviceIndication_Off))
   //! ##### Read ShortPulseMaxLength interface
   retValue = Rte_Read_ShortPulseMaxLength_ShortPulseMaxLength(&pRteInData_Common->ShortPulseMaxLength);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->ShortPulseMaxLength), (0U))
   //! ##### Read ComMode_LIN5 interface
   retValue = Rte_Read_ComMode_LIN5_ComMode_LIN(&pRteInData_Common->ComMode_LIN5);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->ComMode_LIN5), (Error))
   //! ##### Read DiagInfoRCECS_LIN5 interface
   retValue = Rte_Read_DiagInfoRCECS_DiagInfo(&pRteInData_Common->DiagInfoRCECS_LIN5);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->DiagInfoRCECS_LIN5), (0U), (0U))
   //! ##### Read ResponseErrorRCECS_LIN5 interface
   retRCECS = Rte_Read_ResponseErrorRCECS_ResponseErrorRCECS(&pRteInData_Common->ResponseErrorRCECS_LIN5);
   MACRO_StdRteRead_ExtRPort((retRCECS), (pRteInData_Common->ResponseErrorRCECS_LIN5),(1U),(1U))
   return retRCECS;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'ECSWiredRemote_Signals_Gateway_Logic'
//!
//! \param   *pInData    Providing the input signals to send to output ports
//! \param   *pOutData   Updating the output ports with input signals
//!
//!======================================================================================
static void ECSWiredRemote_Signals_Gateway_Logic(const ECSWiredRemoteLINMasterCtrl_InData_T   *pInData,
                                                       ECSWiredRemoteLINMasterCtrl_OutData_T  *pOutData)
{
   //! ###### Processing the signals gateway logic
   //! ##### Update the output ports with values read on input ports 
   // Signals gateway, HMI status from ECS wired remote 
   pOutData->AdjustButtonStatus          = pInData->VarLIN_AdjustButtonStatus.Current;
   pOutData->BackButtonStatus            = pInData->VarLIN_BackButtonStatus.Current;
   pOutData->MemButtonStatus             = pInData->VarLIN_MemButtonStatus.Current;
   pOutData->SelectButtonStatus          = pInData->VarLIN_SelectButtonStatus.Current;
   pOutData->StopButtonStatus            = pInData->VarLIN_StopButtonStatus.Current;
   pOutData->WRDownButtonStatus          = pInData->VarLIN_WRDownButtonStatus.Current;
   pOutData->WRUpButtonStatus            = pInData->VarLIN_WRUpButtonStatus.Current;
   // Signals gateway, indication status on ECS wired remote
   pOutData->LIN_Adjust_DeviceIndication = pInData->Adjust_DeviceIndication;
   pOutData->LIN_Down_DeviceIndication   = pInData->Down_DeviceIndication;
   pOutData->LIN_Up_DeviceIndication     = pInData->Up_DeviceIndication;
   pOutData->LIN_M1_DeviceIndication     = pInData->M1_DeviceIndication;
   pOutData->LIN_M2_DeviceIndication     = pInData->M2_DeviceIndication;
   pOutData->LIN_M3_DeviceIndication     = pInData->M3_DeviceIndication;
   pOutData->LIN_ShortPulseMaxLength     = pInData->ShortPulseMaxLength;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'Generic_ANW_ECSStandByTrigger1'
//!
//! \param   isRestAct      Provides 'yes' or 'no' values based on restriction activation
//! \param   isActTrigDet   Provides 'yes' or 'no' values based on activation trigger
//!
//!======================================================================================
static void Generic_ANW_ECSStandByTrigger1(const FormalBoolean isRestAct,
                                           const FormalBoolean isActTrigDet)
{   
   static AnwSM_States State_ANW                       = { SM_ANW_Inactive,
                                                           SM_ANW_Inactive };
   static DeactivationTimer_Type ECS_DeactivationTimer = 0U;
   AnwAction_Enum AnwActionstatus                      = ANW_Action_None;
   //! ###### Process ANW logic : 'ProcessAnwLogic()'
   AnwActionstatus = ProcessAnwLogic(isRestAct,
                                     isActTrigDet,
                                     YES,
                                     PCODE_AnwEcsStandbyDeactTimeout,
                                     &ECS_DeactivationTimer,
                                     &State_ANW);
   //! ###### Check for Anw_Action is 'Activate' or 'Deactivate' for ECSStandByTrigger1
   if (ANW_Action_Activate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_ECSStandByTrigger1_ActivateIss();
   }
   else if (ANW_Action_Deactivate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_ECSStandByTrigger1_DeactivateIss();
   }
   else
   {
      // Do nothing keep previous status
   }
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'ANW_ECSStandByTrigger1_Cond'
//!
//! \param   *pInDataTrig     Providing the input data
//!
//! \return   FormalBoolean   Returns 'isActTrigDet' value
//!
//!======================================================================================
static FormalBoolean ANW_ECSStandByTrigger1_Cond(const ECSWiredRemoteLINMasterCtrl_InData_T  *pInDataTrig)
{
   //! ###### Processing ECSStandByTrigger1 activation trigger logic
   FormalBoolean isActTrigDet = NO;

   //! ##### Check for the input button status
   if (((pInDataTrig->VarLIN_AdjustButtonStatus.Current != pInDataTrig->VarLIN_AdjustButtonStatus.Previous)
      && (PushButtonStatus_Pushed == pInDataTrig->VarLIN_AdjustButtonStatus.Current))
      || ((pInDataTrig->VarLIN_BackButtonStatus.Current != pInDataTrig->VarLIN_BackButtonStatus.Previous)
      && (PushButtonStatus_Pushed == pInDataTrig->VarLIN_BackButtonStatus.Current))
      || ((pInDataTrig->VarLIN_MemButtonStatus.Current != pInDataTrig->VarLIN_MemButtonStatus.Previous)
      && (PushButtonStatus_Pushed == pInDataTrig->VarLIN_MemButtonStatus.Current))
      || ((pInDataTrig->VarLIN_SelectButtonStatus.Current != pInDataTrig->VarLIN_SelectButtonStatus.Previous)
      && (PushButtonStatus_Pushed == pInDataTrig->VarLIN_SelectButtonStatus.Current))
      || ((pInDataTrig->VarLIN_StopButtonStatus.Current != pInDataTrig->VarLIN_StopButtonStatus.Previous)
      && (PushButtonStatus_Pushed == pInDataTrig->VarLIN_StopButtonStatus.Current))
      || ((pInDataTrig->VarLIN_WRDownButtonStatus.Current != pInDataTrig->VarLIN_WRDownButtonStatus.Previous)
      && (EvalButtonRequest_EvaluatingPush == pInDataTrig->VarLIN_WRDownButtonStatus.Current)) 
      || ((pInDataTrig->VarLIN_WRUpButtonStatus.Current != pInDataTrig->VarLIN_WRUpButtonStatus.Previous)
      && (EvalButtonRequest_EvaluatingPush == pInDataTrig->VarLIN_WRUpButtonStatus.Current)))
   {
      isActTrigDet = YES;
   }
   else
   {
      isActTrigDet = NO;
   }
   return isActTrigDet;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'ECSWiredRemote_LINMasterCtrl_FCI_Indication'
//!
//! \param   *pDiagInfoRCECS_LIN5   Providing the diagnostic information of RCECS_LIN5
//!
//! \return   boolean   Returns 'IsFciFaultActive' value
//!
//!======================================================================================
static boolean ECSWiredRemote_LINMasterCtrl_FCI_Indication(const DiagInfo_T *pDiagInfoRCECS_LIN5)
{
   static uint8 SlaveFCIFault[2] = { CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive };
   boolean IsFciFaultActive      = CONST_Fault_InActive;
   uint8 Index                   = 0U;
   //! ###### Processing FCI indication logic
   //! ##### Check the FCI conditions and log/remove the faults
   if ((DiagInfo_T)0x41 == *pDiagInfoRCECS_LIN5)
   {
      Rte_Call_Event_D1BOI_16_RCECS_VBT_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x01 == *pDiagInfoRCECS_LIN5)
   {
      Rte_Call_Event_D1BOI_16_RCECS_VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x42 == *pDiagInfoRCECS_LIN5)
   {
      Rte_Call_Event_D1BOI_17_RCECS_VAT_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x02 == *pDiagInfoRCECS_LIN5)
   {
      Rte_Call_Event_D1BOI_17_RCECS_VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x43 == *pDiagInfoRCECS_LIN5)
   {
      SlaveFCIFault[0] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BOI_46_RCECS_EEPROM_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x03 == *pDiagInfoRCECS_LIN5)
   {
      SlaveFCIFault[0] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BOI_46_RCECS_EEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x44 == *pDiagInfoRCECS_LIN5)
   {
      SlaveFCIFault[1] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BOI_94_RCECS_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x04 == *pDiagInfoRCECS_LIN5)
   {
      SlaveFCIFault[1] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BOI_94_RCECS_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x00 == *pDiagInfoRCECS_LIN5)
   {
      for (Index = 0U;Index < 2U ;Index++)
      {
         SlaveFCIFault[Index] = CONST_SlaveFCIFault_InActive;
      }
      Rte_Call_Event_D1BOI_16_RCECS_VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BOI_17_RCECS_VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BOI_46_RCECS_EEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BOI_94_RCECS_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else
   {
      // Do nothing, keep previous status
   }
   IsFciFaultActive = CONST_Fault_InActive;
   for(Index = 0U;Index < 2U;Index++)
   {
      if(CONST_SlaveFCIFault_Active == SlaveFCIFault[Index])
      {
         IsFciFaultActive = CONST_Fault_Active;
      }
      else
      {
         // Do Nothing
      }
   }

   return IsFciFaultActive;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the  'ECSWiredRemoteLINMasterCtrl_FallbackLogic'
//!
//! \param   *pOutData_Fallback   Update the output ports to error
//!
//!======================================================================================
static void  ECSWiredRemoteLINMasterCtrl_FallbackLogic(ECSWiredRemoteLINMasterCtrl_OutData_T  *pOutData_Fallback)
{
   //! ###### Processing the fallback logic
   //! ##### Write all output ports to error
   pOutData_Fallback->AdjustButtonStatus   = PushButtonStatus_Error;
   pOutData_Fallback->BackButtonStatus     = PushButtonStatus_Error;
   pOutData_Fallback->MemButtonStatus      = PushButtonStatus_Error;
   pOutData_Fallback->SelectButtonStatus   = PushButtonStatus_Error;
   pOutData_Fallback->StopButtonStatus     = PushButtonStatus_Error;
   pOutData_Fallback->WRDownButtonStatus   = EvalButtonRequest_Error;
   pOutData_Fallback->WRUpButtonStatus     = EvalButtonRequest_Error;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'ECSWiredRemoteLINMasterCtrl_DeactivateLogic'
//!
//! \param   *pOutData_Deact   Update the output ports to off or not available
//!
//!======================================================================================
static void ECSWiredRemoteLINMasterCtrl_DeactivateLogic(ECSWiredRemoteLINMasterCtrl_OutData_T  *pOutData_Deact)
{
   //! ###### Processing deactivation logic 
   //! ##### Write all output ports with 'NotAvailable' or 'Off' values
   pOutData_Deact->AdjustButtonStatus           = PushButtonStatus_NotAvailable;
   pOutData_Deact->BackButtonStatus             = PushButtonStatus_NotAvailable;
   pOutData_Deact->MemButtonStatus              = PushButtonStatus_NotAvailable;
   pOutData_Deact->SelectButtonStatus           = PushButtonStatus_NotAvailable;
   pOutData_Deact->StopButtonStatus             = PushButtonStatus_NotAvailable;
   pOutData_Deact->WRDownButtonStatus           = EvalButtonRequest_NotAvailable;
   pOutData_Deact->WRUpButtonStatus             = EvalButtonRequest_NotAvailable;  
   pOutData_Deact->LIN_Adjust_DeviceIndication  = DeviceIndication_Off;
   pOutData_Deact->LIN_Down_DeviceIndication    = DeviceIndication_Off;
   pOutData_Deact->LIN_Up_DeviceIndication      = DeviceIndication_Off;
   pOutData_Deact->LIN_M1_DeviceIndication      = DeviceIndication_Off;
   pOutData_Deact->LIN_M2_DeviceIndication      = DeviceIndication_Off;
   pOutData_Deact->LIN_M3_DeviceIndication      = DeviceIndication_Off;
   pOutData_Deact->LIN_ShortPulseMaxLength      = 0U; // This need to be check with Volvo Team
}

//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
