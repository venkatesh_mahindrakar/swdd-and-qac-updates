/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  VEC_CryptoProxySenderSwc.c
 *        Config:  D:/GitSCIM/scim_seoyon/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  VEC_CryptoProxySenderSwc
 *  Generated at:  Tue Oct 15 21:51:02 2019
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <VEC_CryptoProxySenderSwc>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   Boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * SEWS_ComCryptoKey_P1DLX_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SymDecryptDataBuffer
 *   Used as Buffer for service.
 *
 * SymDecryptResultBuffer
 *   Used as Buffer for service.
 *
 * SymEncryptDataBuffer
 *   Used as Buffer for service.
 *
 * SymEncryptResultBuffer
 *   Used as Buffer for service.
 *
 * SymKeyType
 *   This type is used for Csm key parameters of type SymKey
 *
 * UInt16
 *   UInt16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on UInt16 is: x < y if y - x is positive.
 *      UInt16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * UInt32
 *   UInt32 represents integers with a minimum value of 0 and a maximum value 
 *      of 4294967295. The order-relation on UInt32 is: x < y if y - x is positive.
 *      UInt32 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39). 
 *      
 *      For example: 1, 0, 12234567, 104400.
 *
 *
 * Operation Prototypes:
 * =====================
 * SymDecryptFinish of Port Interface CsmSymDecrypt
 *   This interface shall be used to finish the symmetrical decryption service.
 *
 * SymDecryptStart of Port Interface CsmSymDecrypt
 *   This interface shall be used to initialize the symmetrical decryption service of the CSM module.
 *
 * SymDecryptUpdate of Port Interface CsmSymDecrypt
 *   This interface shall be used to feed the symmetrical decryption service with the input data.
 *
 * SymEncryptFinish of Port Interface CsmSymEncrypt
 *   This interface shall be used to finish the symmetrical encryption service.
 *
 * SymEncryptStart of Port Interface CsmSymEncrypt
 *   This interface shall be used to initialize the symmetrical encryption service of the CSM module.
 *
 * SymEncryptUpdate of Port Interface CsmSymEncrypt
 *   This interface shall be used to feed the symmetrical encryption service with the input data.
 *
 *********************************************************************************************************************/

#include "Rte_VEC_CryptoProxySenderSwc.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "VEC_SwcCrypto_Volvo_AB.h"

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/
/* vendor specific version information is BCD coded */
#if (  (VEC_CRYPTO_TRANSMISSION_MAJOR_VERSION != (0x02)) \
    || (VEC_CRYPTO_TRANSMISSION_MINOR_VERSION != (0x00)) \
    || (VEC_CRYPTO_TRANSMISSION_PATCH_VERSION != (0x01)) )
# error "Vendor specific version numbers of VEC_CryptoProxySenderSwc.c and VEC_SwcCrypto_Volvo_AB.h are inconsistent"
#endif

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_ComCryptoKey_P1DLX_T: Integer in interval [0...255]
 * SEWS_ComCryptoKey_P1DLX_T: Integer in interval [0...255]
 * UInt16: Integer in interval [0...65535]
 * UInt32: Integer in interval [0...4294967295]
 * UInt32_Length: Integer in interval [0...4294967295]
 * UInt8: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Array Types:
 * ============
 * CryptoIdKey_T: Array with 16 element(s) of type uint8
 * Encrypted128bit_T: Array with 16 element(s) of type uint8
 * Rte_DT_SymKeyType_1: Array with 256 element(s) of type UInt8
 * SEWS_ComCryptoKey_P1DLX_a_T: Array with 16 element(s) of type SEWS_ComCryptoKey_P1DLX_T
 * SEWS_ComCryptoKey_P1DLX_a_T: Array with 16 element(s) of type SEWS_ComCryptoKey_P1DLX_T
 * SymDecryptDataBuffer: Array with 128 element(s) of type UInt8
 * SymDecryptResultBuffer: Array with 128 element(s) of type UInt8
 * SymEncryptDataBuffer: Array with 128 element(s) of type UInt8
 * SymEncryptResultBuffer: Array with 128 element(s) of type UInt8
 * VEC_CryptoProxy_UserSignal: Array with 12 element(s) of type UInt8
 *
 * Record Types:
 * =============
 * SymKeyType: Record with elements
 *   length of type UInt32_Length
 *   data of type Rte_DT_SymKeyType_1
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   UInt16 *Rte_Pim_VEC_CryptoProxy_CycleTimer(Rte_Instance self)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   UInt16 Rte_CData_VEC_CryptoProxyCycleFactor(Rte_Instance self)
 *   UInt16 Rte_CData_VEC_CryptoProxyCycleOffset(Rte_Instance self)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_ComCryptoKey_P1DLX_T *Rte_Prm_ComCryptoKey_P1DLX_v(Rte_Instance self)
 *     Returnvalue: SEWS_ComCryptoKey_P1DLX_T* is of type SEWS_ComCryptoKey_P1DLX_a_T
 *
 *********************************************************************************************************************/


#define VEC_CryptoProxySenderSwc_START_SEC_CODE
#include "VEC_CryptoProxySenderSwc_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CryptoProxySenderConfirmation
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on DataSendCompletedEvent for DataElementPrototype <EncryptedSignal> of PortPrototype <VEC_EncryptedSignal>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   UInt32 Rte_IrvRead_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(Rte_Instance self)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(Rte_Instance self, UInt32 data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderConfirmation_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_CryptoProxySenderSwc_CODE) VEC_CryptoProxySenderConfirmation(Rte_Instance self) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderConfirmation
 *********************************************************************************************************************/

  UInt32 identificationNumber = Rte_IrvRead_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(self);
  identificationNumber++;
  if (identificationNumber >= (UInt32)VEC_CRYPTO_TRANSMISSION_ID_MAX)
  {
    identificationNumber = 0;
  }
  Rte_IrvWrite_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(self,identificationNumber);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CryptoProxySenderMainFunction
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(Rte_Instance self, uint8 *data)
 *     Argument data: uint8* is of type CryptoIdKey_T
 *   Std_ReturnType Rte_Read_CryptoTrigger_CryptoTrigger(Rte_Instance self, Boolean *data)
 *   Std_ReturnType Rte_Read_VEC_CryptoProxySerializedData_Crypto_Function_serialized(Rte_Instance self, UInt8 *data)
 *     Argument data: UInt8* is of type VEC_CryptoProxy_UserSignal
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Send_VEC_EncryptedSignal_EncryptedSignal(Rte_Instance self, const uint8 *data)
 *     Argument data: uint8* is of type Encrypted128bit_T
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   UInt32 Rte_IrvRead_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(Rte_Instance self)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(Rte_Instance self, UInt32 data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmSymEncrypt_SymEncryptFinish(Rte_Instance self, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type SymEncryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymEncrypt_CSM_E_BUSY, RTE_E_CsmSymEncrypt_CSM_E_NOT_OK, RTE_E_CsmSymEncrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmSymEncrypt_SymEncryptStart(Rte_Instance self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength)
 *     Argument InitVectorBuffer: UInt8* is of type SymEncryptDataBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymEncrypt_CSM_E_BUSY, RTE_E_CsmSymEncrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmSymEncrypt_SymEncryptUpdate(Rte_Instance self, const UInt8 *plainTextBuffer, UInt32_Length plainTextLength, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength)
 *     Argument plainTextBuffer: UInt8* is of type SymEncryptDataBuffer
 *     Argument cipherTextBuffer: UInt8* is of type SymEncryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymEncrypt_CSM_E_BUSY, RTE_E_CsmSymEncrypt_CSM_E_NOT_OK, RTE_E_CsmSymEncrypt_CSM_E_SMALL_BUFFER
 *
 * Status Interfaces:
 * ==================
 *   Tx Acknowledge:
 *   ----------------
 *   Std_ReturnType Rte_Feedback_VEC_EncryptedSignal_EncryptedSignal(Rte_Instance self)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderMainFunction_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_CryptoProxySenderSwc_CODE) VEC_CryptoProxySenderMainFunction(Rte_Instance self) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderMainFunction
 *********************************************************************************************************************/

  Std_ReturnType   retVal;
  SymKeyType       CsmSymKey;

  uint32  i;
  uint32  cipherTextLength;
  uint32  totalCipherTextLength = 0;
  uint32  identificationNumber;
  uint16  prevTimer;
  uint16* timer;
  uint8   plainText[16];
  uint8   cipherText[16];
  uint8   initVector[16] = {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0};

  identificationNumber = Rte_IrvRead_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(self);
  /* Check if at least one identificationNumber is received */
  
  if (identificationNumber != (uint32)VEC_CRYPTO_TRANSMISSION_ID_INVALID)
  {
	  identificationNumber = 0xABCD;
  }
  
  //if (identificationNumber != (uint32)VEC_CRYPTO_TRANSMISSION_ID_INVALID)
  {
    timer = Rte_Pim_VEC_CryptoProxy_CycleTimer(self);
    prevTimer = *timer;

    if (*timer > 0)
    {
      (*timer)--;
    }

    if (*timer == 0)
    {
      if (prevTimer == 0)
      {
        /* First cycle, start with offset */
        *timer = Rte_CData_VEC_CryptoProxyCycleOffset(self);
      }
      else
      {
        *timer = Rte_CData_VEC_CryptoProxyCycleFactor(self);
      }

      /* Offset of 4 because of first 4 bytes are for identificationNumber */
      retVal = Rte_Read_VEC_CryptoProxySerializedData_Crypto_Function_serialized(self, &plainText[4]); /* PRQA S 310 */ /* Casting byte pointer to explicit byte array pointer */

      if (retVal == E_OK)
      {
        identificationNumber = Rte_IrvRead_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(self);
        plainText[3]= (uint8)(identificationNumber >> 24);
        plainText[2]= (uint8)(identificationNumber >> 16);
        plainText[1]= (uint8)(identificationNumber >>  8);
        plainText[0]= (uint8)identificationNumber;

        /* Get calibration parameter KEY */
        CsmSymKey.length = 16;
        {
          P2CONST(SEWS_ComCryptoKey_P1DLX_a_T, AUTOMATIC, VEC_CryptoProxySenderSwc_CONST) keyPtr = (SEWS_ComCryptoKey_P1DLX_a_T*)Rte_Prm_ComCryptoKey_P1DLX_v(self);

          i = CsmSymKey.length;
          do
          {
            i--;
            CsmSymKey.data[i] = (uint8)(*keyPtr)[i];
          }
          while (i > 0);
        }

        /* Run encryption on cipher block */
        retVal = Rte_Call_CsmSymEncrypt_SymEncryptStart(self, &CsmSymKey, &initVector[0], 0);
        cipherTextLength = sizeof(cipherText);

        /* plainTextLength is cipherTextLength minus two because of one byte admin data of CAL and null terminated string type */
        retVal |= Rte_Call_CsmSymEncrypt_SymEncryptUpdate(self, (uint8*)&plainText[0], (cipherTextLength-1), (uint8*)&cipherText[0], &cipherTextLength);
        totalCipherTextLength += cipherTextLength;

        /* Finalize decryption */
        cipherTextLength = sizeof(cipherText) - totalCipherTextLength;
        retVal |= Rte_Call_CsmSymEncrypt_SymEncryptFinish(self, &cipherText[totalCipherTextLength], &cipherTextLength);
        totalCipherTextLength += cipherTextLength;

        if (retVal == E_OK)
        {
          (void)Rte_Send_VEC_EncryptedSignal_EncryptedSignal(self, cipherText);
        }
      }
    }
  }

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CryptoProxySenderReception
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on DataReceivedEvent for DataElementPrototype <CryptoIdKey> of PortPrototype <VEC_CryptoIdKey>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(Rte_Instance self, uint8 *data)
 *     Argument data: uint8* is of type CryptoIdKey_T
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   UInt32 Rte_IrvRead_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(Rte_Instance self)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(Rte_Instance self, UInt32 data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmSymDecrypt_SymDecryptFinish(Rte_Instance self, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type SymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymDecrypt_CSM_E_BUSY, RTE_E_CsmSymDecrypt_CSM_E_NOT_OK, RTE_E_CsmSymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmSymDecrypt_SymDecryptStart(Rte_Instance self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength)
 *     Argument InitVectorBuffer: UInt8* is of type SymDecryptDataBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymDecrypt_CSM_E_BUSY, RTE_E_CsmSymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmSymDecrypt_SymDecryptUpdate(Rte_Instance self, const UInt8 *cipherTextBuffer, UInt32_Length cipherTextLength, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type SymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type SymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymDecrypt_CSM_E_BUSY, RTE_E_CsmSymDecrypt_CSM_E_NOT_OK, RTE_E_CsmSymDecrypt_CSM_E_SMALL_BUFFER
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderReception_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_CryptoProxySenderSwc_CODE) VEC_CryptoProxySenderReception(Rte_Instance self) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderReception
 *********************************************************************************************************************/

  CryptoIdKey_T   cryptoSignal;
  Std_ReturnType  retVal;
  SymKeyType      CsmSymKey;

  uint32  i;
  uint32  plainTextLength;
  uint32  totalPlainTextLength = 0;
  uint32  identificationNumber;
  uint8   plainText[16];
  uint8   initVector[16] = {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0};
  uint16* timer = Rte_Pim_VEC_CryptoProxy_CycleTimer(self);

  retVal = Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(self, cryptoSignal);

  if (retVal == E_OK)
  {
    /* get calibration parameter KEY */
    CsmSymKey.length = 16;
    {
      P2CONST(SEWS_ComCryptoKey_P1DLX_a_T, AUTOMATIC, VEC_CryptoProxySenderSwc_CONST) keyPtr = (SEWS_ComCryptoKey_P1DLX_a_T*)Rte_Prm_ComCryptoKey_P1DLX_v(self);
      i = CsmSymKey.length;
      do
      {
        i--;
        CsmSymKey.data[i] = (uint8)(*keyPtr)[i];
      }
      while (i != 0);
    }

    /* Starting streaming approach interface of CAL */
    /* Initializing the crypto service. Config ID not used by CAL API */
    retVal = Rte_Call_CsmSymDecrypt_SymDecryptStart(self, &CsmSymKey, &initVector[0], 0);
    plainTextLength = sizeof(plainText);

    /* Provide a block of data. Complete block at once, can be splitted in appropriate blocks. */
    /* cipherTextLength is VEC_CryptoProxyEncryptionSignal_Type minus one because of null terminated string type */
    retVal |= Rte_Call_CsmSymDecrypt_SymDecryptUpdate(self, (uint8*)&cryptoSignal[0], 16, &plainText[0], &plainTextLength);
    totalPlainTextLength += plainTextLength;

    /* Finish the service */
    plainTextLength = sizeof(plainText) - totalPlainTextLength;
    retVal |= Rte_Call_CsmSymDecrypt_SymDecryptFinish(self, &plainText[totalPlainTextLength], &plainTextLength);
    totalPlainTextLength += plainTextLength;

    if ((retVal == E_OK) && (totalPlainTextLength <= sizeof(plainText)))
    {
      identificationNumber = (uint32)plainText[0] | (((uint32)plainText[1]) << 8) | (((uint32)plainText[2]) << 16) | (((uint32)plainText[3]) << 24);
      identificationNumber++;
      if (identificationNumber >= (uint32)VEC_CRYPTO_TRANSMISSION_ID_MAX)
      {
        identificationNumber = 0;
      }
      Rte_IrvWrite_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(self, identificationNumber);

      /* Immediately send out crypto response */
      *timer = 0;
    }
  }

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CryptoProxySenderSwc_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderSwc_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_CryptoProxySenderSwc_CODE) VEC_CryptoProxySenderSwc_Init(Rte_Instance self) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderSwc_Init
 *********************************************************************************************************************/

 uint16* timer = Rte_Pim_VEC_CryptoProxy_CycleTimer(self);

 /* Immediately send out crypto response */
 *timer = 0;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define VEC_CryptoProxySenderSwc_STOP_SEC_CODE
#include "VEC_CryptoProxySenderSwc_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
