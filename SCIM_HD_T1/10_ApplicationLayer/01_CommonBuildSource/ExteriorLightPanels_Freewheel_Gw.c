/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  ExteriorLightPanels_Freewheel_Gw.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  ExteriorLightPanels_Freewheel_Gw
 *  Generated at:  Fri Jun 12 17:01:41 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <ExteriorLightPanels_Freewheel_Gw>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file ExteriorLightPanels_Freewheel_Gw.c
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup CustomerAndTransportAdaptation 
//! @{
//! @addtogroup ExteriorLightPanels_Freewheel_Gw
//! @{
//!
//! \brief
//! ExteriorLightPanels_Freewheel_Gw SWC.
//! ASIL Level : QM.
//! This module implements the logic for the ExteriorLightPanels_Freewheel_Gw runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_ExteriorLightPanels_Freewheel_Gw.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "ExteriorLightPanels_Freewheel_Gw.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Enumeration Types:
 * ==================
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 *   FreeWheel_Status_NoMovement (0U)
 *   FreeWheel_Status_1StepClockwise (1U)
 *   FreeWheel_Status_2StepsClockwise (2U)
 *   FreeWheel_Status_3StepsClockwise (3U)
 *   FreeWheel_Status_4StepsClockwise (4U)
 *   FreeWheel_Status_5StepsClockwise (5U)
 *   FreeWheel_Status_6StepsClockwise (6U)
 *   FreeWheel_Status_1StepCounterClockwise (7U)
 *   FreeWheel_Status_2StepsCounterClockwise (8U)
 *   FreeWheel_Status_3StepsCounterClockwise (9U)
 *   FreeWheel_Status_4StepsCounterClockwise (10U)
 *   FreeWheel_Status_5StepsCounterClockwise (11U)
 *   FreeWheel_Status_6StepsCounterClockwise (12U)
 *   FreeWheel_Status_Spare (13U)
 *   FreeWheel_Status_Error (14U)
 *   FreeWheel_Status_NotAvailable (15U)
 * Freewheel_Status_Ctr_T: Enumeration of integer in interval [0...15] with enumerators
 *   Freewheel_Status_Ctr_NoMovement (0U)
 *   Freewheel_Status_Ctr_Position1 (1U)
 *   Freewheel_Status_Ctr_Position2 (2U)
 *   Freewheel_Status_Ctr_Position3 (3U)
 *   Freewheel_Status_Ctr_Position4 (4U)
 *   Freewheel_Status_Ctr_Position5 (5U)
 *   Freewheel_Status_Ctr_Position6 (6U)
 *   Freewheel_Status_Ctr_Position7 (7U)
 *   Freewheel_Status_Ctr_Position8 (8U)
 *   Freewheel_Status_Ctr_Position9 (9U)
 *   Freewheel_Status_Ctr_Position10 (10U)
 *   Freewheel_Status_Ctr_Position11 (11U)
 *   Freewheel_Status_Ctr_Position12 (12U)
 *   Freewheel_Status_Ctr_Position13 (13U)
 *   Freewheel_Status_Ctr_Error (14U)
 *   Freewheel_Status_Ctr_NotAvailable (15U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


#define ExteriorLightPanels_Freewheel_Gw_START_SEC_CODE
#include "ExteriorLightPanels_Freewheel_Gw_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ExteriorLightPanels_Freewheel_Gw_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_LightMode_Status_1_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_LightMode_Status_2_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   boolean Rte_IsUpdated_LightMode_Status_1_FreeWheel_Status(void)
 *   boolean Rte_IsUpdated_LightMode_Status_2_FreeWheel_Status(void)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_LightMode_Status_Ctr_1_Freewheel_Status_Ctr(Freewheel_Status_Ctr_T data)
 *   Std_ReturnType Rte_Write_LightMode_Status_Ctr_2_Freewheel_Status_Ctr(Freewheel_Status_Ctr_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ExteriorLightPanels_Freewheel_Gw_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ExteriorLightPanels_Freewheel_Gw_CODE) ExteriorLightPanels_Freewheel_Gw_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ExteriorLightPanels_Freewheel_Gw_20ms_runnable
 *********************************************************************************************************************/

	//! ###### Definition of the SWC In/Out data structures
	// Define input vehicule Mode distribution
	static VehicleModeDistribution_T RteInData_Vehicle_Mode_Distribution;
	// Define input RteInData_LightMode_Status_1_FreeWheel 
	static LightMode_Status RteInData_LightMode_Status_1_FreeWheel;
	// Define input RteInData_LightMode_Status_2_FreeWheel 
	static LightMode_Status RteInData_LightMode_Status_2_FreeWheel;
	// Define ouput RteOutData_LightMode_Status_Ctr_1
	static Freewheel_Status_Ctr_T RteOutData_LightMode_Status_Ctr_1;
	// Define ouput RteOutData_LightMode_Status_Ctr_1
	static Freewheel_Status_Ctr_T RteOutData_LightMode_Status_Ctr_2;
	
	//! ###### Vehicule Internal Mode
	//! ##### Read vehicule moode distritution : Read_InternalVehiculeModeDistribution()
	Read_InternalVehiculeModeDistribution(&RteInData_Vehicle_Mode_Distribution);
	//! ##### Processing vehicule mode distritution: IsActiveVehicleModeDistribution()
	if (TRUE == IsActiveVehicleModeDistribution(&RteInData_Vehicle_Mode_Distribution))
	{		
		//! ###### Read LightMode 1 FreeWheel Status : Read_RteInData_LightMode_Status_1_FreeWheel()
		Read_LightMode_Status_1_FreeWheel(&RteInData_LightMode_Status_1_FreeWheel);
		//! ###### Read LightMode 2 FreeWheel Status : Read_RteInData_LightMode_Status_2_FreeWheel()
		Read_LightMode_Status_2_FreeWheel(&RteInData_LightMode_Status_2_FreeWheel);
		//! ###### Processing LightMode 1 FreeWheel logic
		Processing_LightMode_1_FreeWheel(&RteInData_LightMode_Status_1_FreeWheel,&RteOutData_LightMode_Status_Ctr_1);
		//! ###### Processing LightMode 2 FreeWheel logic
		Processing_LightMode_2_FreeWheel(&RteInData_LightMode_Status_2_FreeWheel,&RteOutData_LightMode_Status_Ctr_2);
		//! ###### Processing freewheel movement conversion
		
		if( (FreeWheel_Status_NoMovement == RteInData_LightMode_Status_1_FreeWheel.LightMode_Status) ||
			(FreeWheel_Status_Error == RteInData_LightMode_Status_1_FreeWheel.LightMode_Status) ||
			(FreeWheel_Status_NotAvailable == RteInData_LightMode_Status_1_FreeWheel.LightMode_Status))
		{
			RteOutData_LightMode_Status_Ctr_1=RteInData_LightMode_Status_1_FreeWheel.LightMode_Status;
		}
		else if (FreeWheel_Status_Spare == RteInData_LightMode_Status_1_FreeWheel.LightMode_Status) 
		{
			RteOutData_LightMode_Status_Ctr_1=Freewheel_Status_Ctr_Error;
		}
		else
		{
			//nothing, not condition for this case
		}
			
		if( (FreeWheel_Status_NoMovement == RteInData_LightMode_Status_2_FreeWheel.LightMode_Status) ||
			(FreeWheel_Status_Error == RteInData_LightMode_Status_2_FreeWheel.LightMode_Status) ||
			(FreeWheel_Status_NotAvailable == RteInData_LightMode_Status_2_FreeWheel.LightMode_Status))
		{
			RteOutData_LightMode_Status_Ctr_2=RteInData_LightMode_Status_2_FreeWheel.LightMode_Status;
		}
		else if (FreeWheel_Status_Spare == RteInData_LightMode_Status_2_FreeWheel.LightMode_Status) 
		{
			RteOutData_LightMode_Status_Ctr_2=Freewheel_Status_Ctr_Error;
		}
		else
		{
			//nothing, not condition for this case
		}
	}
	else
	{
		RteOutData_LightMode_Status_Ctr_1 = Freewheel_Status_Ctr_NotAvailable;
		RteOutData_LightMode_Status_Ctr_2 = Freewheel_Status_Ctr_NotAvailable;
	}
	
	//! ###### Common outputs processing
	//! ##### Write the RTE output ports 	
	Rte_Write_LightMode_Status_Ctr_1_Freewheel_Status_Ctr(RteOutData_LightMode_Status_Ctr_1);
    Rte_Write_LightMode_Status_Ctr_2_Freewheel_Status_Ctr(RteOutData_LightMode_Status_Ctr_2);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define ExteriorLightPanels_Freewheel_Gw_STOP_SEC_CODE
#include "ExteriorLightPanels_Freewheel_Gw_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

 //!==============================================================================================================
//! \brief
//! This function Read the RTE vehicle mode distribution and return TRUE if Rte is equal to value RTE_E_OK otherwise FALSE.
//! If the RTE is not equal to value RTE_E_OK than vehicle is equal to value Error 
//!
//! \param pRteInData_vehicle_mode_distritution contain the status of vehicle mode distribution
//!
//!==============================================================================================================
static void Read_InternalVehiculeModeDistribution(VehicleModeDistribution_T *pRteInData_vehicle_mode_distritution)
{
	if (RTE_E_OK != Rte_Read_SwcActivation_Living_Living(pRteInData_vehicle_mode_distritution))
	{
		*pRteInData_vehicle_mode_distritution = NonOperational;
	}
	else
	{
		// condition valid, vehicle_mode_distritution contain the value of Rte
	}
}

//!===============================================================================================
//!
//! \brief
//! this function read data RTE for the LightMode Status 1 FreeWheel
//!
//! \param pRteInData_LightMode_Status_1_FreeWheel write the status of LightMode 1 FreeWheel
//!
//!===============================================================================================

static void Read_LightMode_Status_1_FreeWheel(LightMode_Status *pRteInData_LightMode_Status_1_FreeWheel)
{
	Std_ReturnType retValue = RTE_E_INVALID;
	
	pRteInData_LightMode_Status_1_FreeWheel->isLightMode_Status_Updated = Rte_IsUpdated_LightMode_Status_1_FreeWheel_Status();
	//! ###### Read the RTE data input of LightMode_Status_1_FreeWheel
	retValue = Rte_Read_LightMode_Status_1_FreeWheel_Status(&pRteInData_LightMode_Status_1_FreeWheel->LightMode_Status);
	MACRO_StdRteRead_IntRPort(retValue,pRteInData_LightMode_Status_1_FreeWheel->LightMode_Status, FreeWheel_Status_Error);
	
}

//!===============================================================================================
//!
//! \brief
//! this function read data RTE for the LightMode Status 1 FreeWheel
//!
//! \param pRteInData_LightMode_Status_2_FreeWheel write the status of LightMode 1 FreeWheel
//!
//!===============================================================================================

static void Read_LightMode_Status_2_FreeWheel(LightMode_Status *pRteInData_LightMode_Status_2_FreeWheel)
{
	Std_ReturnType retValue = RTE_E_INVALID;
	
	pRteInData_LightMode_Status_2_FreeWheel->isLightMode_Status_Updated = Rte_IsUpdated_LightMode_Status_2_FreeWheel_Status();
	//! ###### Read the RTE data input of LightMode_Status_2_FreeWheel
	retValue = Rte_Read_LightMode_Status_2_FreeWheel_Status(&pRteInData_LightMode_Status_2_FreeWheel->LightMode_Status);
	MACRO_StdRteRead_IntRPort(retValue,pRteInData_LightMode_Status_2_FreeWheel->LightMode_Status, FreeWheel_Status_Error);
}

//!===============================================================================================
//!
//! This function execute the logic of freewheel 1
//!
//!\param pRteInData_LightMode_Status_1_FreeWheel contain the value of freewheel
//!\param pRteOutData_LightMode_Status_Ctr_1 is the value that we write in the output
//!
//!===============================================================================================
static void Processing_LightMode_1_FreeWheel(LightMode_Status *pRteInData_LightMode_Status_1_FreeWheel, Freewheel_Status_Ctr_T *pRteOutData_LightMode_Status_Ctr_1)
{
	//! ###### Processing logic of freewheel 1
	static sint8 dimCounter = (sint8)FreeWheel_Status_NoMovement;

    if (pRteInData_LightMode_Status_1_FreeWheel->isLightMode_Status_Updated)
	{
		pRteInData_LightMode_Status_1_FreeWheel->isLightMode_Status_Updated = FALSE;
		if (0 == pRteInData_LightMode_Status_1_FreeWheel->LightMode_Status)
		{
			dimCounter = FreeWheel_Status_NoMovement;
		}
		else if (Freewheel_Status_Ctr_Position6 >= pRteInData_LightMode_Status_1_FreeWheel->LightMode_Status)
		{
			dimCounter = dimCounter + pRteInData_LightMode_Status_1_FreeWheel->LightMode_Status;
			if (Freewheel_Status_Ctr_Position13 < dimCounter)
			{
				dimCounter = dimCounter - Freewheel_Status_Ctr_Position13;
			}
		}
		else if ((7 <= pRteInData_LightMode_Status_1_FreeWheel->LightMode_Status) && (12 >= pRteInData_LightMode_Status_1_FreeWheel->LightMode_Status))
		{
			dimCounter = dimCounter - (pRteInData_LightMode_Status_1_FreeWheel->LightMode_Status - Freewheel_Status_Ctr_Position6);
			if (0 == dimCounter)
			{
				dimCounter = Freewheel_Status_Ctr_Position13;
			}
			else if (0 > dimCounter)
			{
				dimCounter = dimCounter + Freewheel_Status_Ctr_Position13 + CONST_OFFSET1;
			}
			else
			{
				//nothing, not condition for this case
			}
		}
		else
		{
			//nothing, not condition for this case
		}
		*pRteOutData_LightMode_Status_Ctr_1 = (uint8)dimCounter;
	}
	else
	{
		//nothing, wait is update bit active for LightMode_Status_1_FreeWheel
	}
}

//!===============================================================================================
//!
//! This function execute the logic of freewheel 2
//!
//!\param pRteInData_LightMode_Status_2_FreeWheel contain the value of freewheel
//!\param pRteOutData_LightMode_Status_Ctr_2 is the value that we write in the output
//!
//!===============================================================================================
static void Processing_LightMode_2_FreeWheel(LightMode_Status *pRteInData_LightMode_Status_2_FreeWheel, Freewheel_Status_Ctr_T *pRteOutData_LightMode_Status_Ctr_2)
{
	//! ###### Processing logic of freewheel 2
	static sint8 dimCounter = (sint8)FreeWheel_Status_NoMovement;

    if (pRteInData_LightMode_Status_2_FreeWheel->isLightMode_Status_Updated) 
	{
		pRteInData_LightMode_Status_2_FreeWheel->isLightMode_Status_Updated = FALSE;
		if (0 == pRteInData_LightMode_Status_2_FreeWheel->LightMode_Status)
		{
			dimCounter = FreeWheel_Status_NoMovement;
		}
		else if (Freewheel_Status_Ctr_Position6 >= pRteInData_LightMode_Status_2_FreeWheel->LightMode_Status)
		{
			dimCounter = dimCounter + pRteInData_LightMode_Status_2_FreeWheel->LightMode_Status;
			if (Freewheel_Status_Ctr_Position13 < dimCounter)
			{
				dimCounter = dimCounter - Freewheel_Status_Ctr_Position13;
			}
		}
		else if ((7 <= pRteInData_LightMode_Status_2_FreeWheel->LightMode_Status) && (12 >= pRteInData_LightMode_Status_2_FreeWheel->LightMode_Status))
		{
			dimCounter = dimCounter - (pRteInData_LightMode_Status_2_FreeWheel->LightMode_Status - Freewheel_Status_Ctr_Position6);
			if (0 == dimCounter)
			{
				dimCounter = Freewheel_Status_Ctr_Position13;
			}
			else if (0 > dimCounter)
			{
				dimCounter = dimCounter + Freewheel_Status_Ctr_Position13 + CONST_OFFSET1;
			}
			else
			{
				//nothing, not condition for this case
			}
		}
		else
		{
			//nothing, not condition for this case
		}
		*pRteOutData_LightMode_Status_Ctr_2 = (uint8)dimCounter;
	}
	else
	{
		//nothing, wait is update bit active for LightMode_Status_2_FreeWheel
	}
}

//!==============================================================================================================
//! \brief
//! This function check is the vehicle is active for this LDC
//! \param RteInData_vehicle_mode_distritution contain the value vehicle mode distribution
//!==============================================================================================================
static boolean IsActiveVehicleModeDistribution(VehicleModeDistribution_T *pRteInData_vehicle_mode_distritution)
{
	boolean isVehicleModeActive;
	if (Operational == *pRteInData_vehicle_mode_distritution)
	{
		isVehicleModeActive = TRUE;
	}
	else
	{
		isVehicleModeActive = FALSE;
	}
	return isVehicleModeActive;
}
		

//! @}
//! @}
//! @}
	

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
