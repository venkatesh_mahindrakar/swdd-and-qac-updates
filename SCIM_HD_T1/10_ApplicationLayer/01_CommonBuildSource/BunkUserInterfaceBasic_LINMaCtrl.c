/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  BunkUserInterfaceBasic_LINMaCtrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  BunkUserInterfaceBasic_LINMaCtrl
 *  Generated at:  Fri Jun 12 17:01:39 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <BunkUserInterfaceBasic_LINMaCtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
//!======================================================================================
//! \file BunkUserInterfaceBasic_LINMaCtrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_BodyAndComfort 
//! @{
//! @addtogroup Application_Living
//! @{
//! @addtogroup BunkUserInterfaceBasic_LINMaCtrl
//! @{
//!
//! \brief
//! BunkUserInterfaceBasic_LINMaCtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the BunkUserInterfaceBasic_LINMaCtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_BunkUserInterfaceBasic_LINMaCtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "FuncLibrary_AnwStateMachine_If.h"
#include "BunkUserInterfaceBasic_LINMaCtrl.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DiagInfo_T: Integer in interval [0...127]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * ResponseErrorLECMBasic_T: Boolean
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *   FALSE (0U)
 *   TRUE (1U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1CAQ_LECML_Installed_v(void)
 *
 *********************************************************************************************************************/


#define BunkUserInterfaceBasic_LINMaCtrl_START_SEC_CODE
#include "BunkUserInterfaceBasic_LINMaCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_LECML_Installed   (Rte_Prm_P1CAQ_LECML_Installed_v())

/**********************************************************************************************************************
 *
 * Runnable Entity Name: BunkUserInterfaceBasic_LINMaCtrl_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DiagInfoLECMBasic_DiagInfo(DiagInfo_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkBAudioOnOff_ButtonStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkBIntLightActvnBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkBParkHeater_ButtonStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkBTempDec_ButtonStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkBTempInc_ButtonStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkBVolumeDown_ButtonStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkBVolumeUp_ButtonStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_ResponseErrorLECMBasic_ResponseErrorLECMBasic(ResponseErrorLECMBasic_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_BunkBAudioOnOff_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkBIntLightActvnBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkBParkHeater_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkBTempDec_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkBTempInc_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkBVolumeDown_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkBVolumeUp_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BKG_87_LECMLowLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOG_16_LECMLow_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOG_17_LECMLow_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOG_46_LECMLow_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOG_94_LECMLow_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_AudioRadio3_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_AudioRadio3_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights5_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights5_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_PHActMaintainLiving5_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_PHActMaintainLiving5_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: BunkUserInterfaceBasic_LINMaCtrl_10ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//!
//! \brief
//! This function implements the cyclic execution runnable logic for the BunkUserInterfaceBasic_LINMaCtrl_10ms_runnable
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, BunkUserInterfaceBasic_LINMaCtrl_CODE) BunkUserInterfaceBasic_LINMaCtrl_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: BunkUserInterfaceBasic_LINMaCtrl_10ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   // Define  RTE input data common 
   static BUIBasic_LINMaCtrl_In_struct_Type  RteInData_Common;
   // Define  RTE output data common 
   static BUIBasic_LINMaCtrl_Out_struct_Type RteOutData_Common = { PushButtonStatus_NotAvailable,
                                                                   PushButtonStatus_NotAvailable,
                                                                   PushButtonStatus_NotAvailable,
                                                                   PushButtonStatus_NotAvailable,
                                                                   PushButtonStatus_NotAvailable,
                                                                   PushButtonStatus_NotAvailable,
                                                                   PushButtonStatus_NotAvailable };

   // Local logic variables 
   FormalBoolean  isActTrigDet_AudioRadio3     = NO;
   FormalBoolean  isActTrigDet_InteriorLights5 = NO;
   FormalBoolean  isActTrigDet_PHActMaintain   = NO;
   FormalBoolean  isDeActTrigDet_PHActMaintain = NO;
   FormalBoolean  isRestAct_AudioRadio3        = YES;
   FormalBoolean  isRestAct_InteriorLights5    = YES;
   FormalBoolean  isRestAct_PHActMaintain      = YES;
   Std_ReturnType respLECMBasic_RetValue       = RTE_E_INVALID;
   boolean        IsFciFaultActive             = CONST_Fault_InActive;
   
   //! ###### Process RTE read input data common logic : 'Get_RteDataRead_Common()'
   respLECMBasic_RetValue = Get_RteDataRead_Common(&RteInData_Common);
   //! ###### Check communication mode of LIN1 bus is in error state (LIN busoff)
   if (Error == RteInData_Common.ComMode_LIN1)
   {
      //! ##### Process fallback mode logic : 'FallbackLogic_BunkUserInterfaceBasic()'
      FallbackLogic_BunkUserInterfaceBasic(&RteOutData_Common);
   }
   else
   {
      //! ###### Select 'LECML_Installed' parameter
      if (TRUE == PCODE_LECML_Installed)
      {
         //! ##### Check for LIN1 communication mode status
         if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN1)
            || (SwitchDetection == RteInData_Common.ComMode_LIN1)
            || (Diagnostic == RteInData_Common.ComMode_LIN1))
         {
            isRestAct_AudioRadio3     = NO;
            isRestAct_InteriorLights5 = NO;
            isRestAct_PHActMaintain   = NO;
            //! ##### Check the missing frame condition for LIN frame is not received for 3 times
            //! ##### Check for response error LECM status
            if (RTE_E_MAX_AGE_EXCEEDED == respLECMBasic_RetValue)
            {
               //! ##### Check the LIN communication mode is in 'Application monitoring' or 'Switch detection'
               if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN1)
                  || (SwitchDetection == RteInData_Common.ComMode_LIN1))
               {
                  //! #### Invoke 'D1BKG_87_LECMLowLink_NoResp_SetEventStatus' with 'STATUS_FAILED' for missing frame fault
                  Rte_Call_Event_D1BKG_87_LECMLowLink_NoResp_SetEventStatus(DEM_EVENT_STATUS_FAILED);
                  //! #### Process the fallback mode logic: 'FallbackLogic_BunkUserInterfaceBasic()'
                  FallbackLogic_BunkUserInterfaceBasic(&RteOutData_Common);
               }
               else
               {
                  //! ##### If LIN communication mode is in 'diagnostic' state
                  //! #### Process the signals gateway logic: 'Signals_GatewayLogic_BunkUserInterfaceBasic()'
                  Signals_GatewayLogic_BunkUserInterfaceBasic(&RteInData_Common,
                                                              &RteOutData_Common);
               }
            }
            //! ##### Check for response error LECM basic is equal to RTE_E_OK or RTE_E_COM_STOPPED or RTE_E_NEVER_RECEIVED
            else if ((RTE_E_OK == respLECMBasic_RetValue)
                    || (RTE_E_COM_STOPPED == respLECMBasic_RetValue)
                    || (RTE_E_NEVER_RECEIVED == respLECMBasic_RetValue))
            {
               //! ##### Invoke 'D1BKG_87_LECMLowLink_NoResp_SetEventStatus' with 'STATUS_PASSED' for missing frame
               Rte_Call_Event_D1BKG_87_LECMLowLink_NoResp_SetEventStatus(DEM_EVENT_STATUS_PASSED);   
               //! ##### Process Fault Code Information(FCI) Indication logic:'BunkUserInterfaceBasic_FCI_Indication()'
               IsFciFaultActive = BunkUserInterfaceBasic_FCI_Indication(&RteInData_Common.DiagInfoLECM_Basic);
                 //! #### Check for 'IsFciFaultActive' value
               if (CONST_Fault_Active == IsFciFaultActive)
               {
                  //! #### Process fallback mode logic: 'FallbackLogic_BunkUserInterfaceBasic()'
                  FallbackLogic_BunkUserInterfaceBasic(&RteOutData_Common);
               }
               else
               {
                  //! #### Process the signals gateway logic: 'Signals_GatewayLogic_BunkUserInterfaceBasic()'
                  Signals_GatewayLogic_BunkUserInterfaceBasic(&RteInData_Common,
                                                              &RteOutData_Common);
               }
            }
            else
            {
               //! ##### Process fallback mode logic: 'FallbackLogic_BunkUserInterfaceBasic()'
               FallbackLogic_BunkUserInterfaceBasic(&RteOutData_Common);
            }
         }
         else
         {
            //! ##### Process deactivation logic: 'Deactivate_Logic_BunkUserInterfaceBasic()'
            Deactivate_Logic_BunkUserInterfaceBasic(&RteOutData_Common);
         }
      }
      else
      {
         //! ###### Process deactivation logic: 'Deactivate_Logic_BunkUserInterfaceBasic()'
         Deactivate_Logic_BunkUserInterfaceBasic(&RteOutData_Common);
      }
   }
   //! ###### Process audio radio3 condition logic: 'Generic_ANW_AudioRadio3_CondCheck()'
   isActTrigDet_AudioRadio3 = Generic_ANW_AudioRadio3_CondCheck(&RteInData_Common.LIN_BunkBVolumeUp_ButtonStat,
                                                                &RteInData_Common.LIN_BunkBVolumeDown_ButtonStat,
                                                                &RteInData_Common.LIN_BunkBAudioOnOff_ButtonStat);
   //! ###### Process ANW audio radio3 trigger condition logic: 'Generic_ANW_AudioRadio3_Trigger()'
   Generic_ANW_AudioRadio3_Trigger(isRestAct_AudioRadio3,
                                   isActTrigDet_AudioRadio3);
   //! ###### Process other interior lights5 condition logic: 'GenericANW_OtherIntLights5_Cond()'
   isActTrigDet_InteriorLights5 = GenericANW_OtherIntLights5_Cond(&RteInData_Common.LIN_BunkBIntLightActvnBtn_stat);
   //! ###### Process ANW other interior lights5 condition logic: 'GenericANW_OtherInteriorLights5()'
   GenericANW_OtherInteriorLights5(isRestAct_InteriorLights5,
                                            isActTrigDet_InteriorLights5);
   //! ###### Process ANW PH activation maintain living5 condition logic: 'ANW_PHActMaintain_ActiveCond()'
   isActTrigDet_PHActMaintain = ANW_PHActMaintain_ActiveCond(&RteInData_Common.LIN_BunkBTempDec_ButtonStat,
                                                                                 &RteInData_Common.LIN_BunkBTempInc_ButtonStat,
                                                                                 &RteInData_Common.LIN_BunkBParkHeater_ButtonStat);
   //! ###### Process ANW PH deactivation maintain living5 condition logic: 'ANW_PHActMaintain_DeActiveCond()'
   isDeActTrigDet_PHActMaintain = ANW_PHActMaintain_DeActiveCond(&RteInData_Common.LIN_BunkBTempDec_ButtonStat,
                                                                                     &RteInData_Common.LIN_BunkBTempInc_ButtonStat);
   //! ###### Process ANW PH activation maintain living5 logic: 'GenericANW_PHActMaintainLiving5()'
   GenericANW_PHActMaintainLiving5(isRestAct_PHActMaintain,
                                            isActTrigDet_PHActMaintain,
                                            isDeActTrigDet_PHActMaintain);
   //! ###### Process RTE output data write common logic: 'RteDataWrite_Common()'
   RteDataWrite_Common(&RteOutData_Common);
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define BunkUserInterfaceBasic_LINMaCtrl_STOP_SEC_CODE
#include "BunkUserInterfaceBasic_LINMaCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Get_RteDataRead_Common'
//!
//! \param   *pRteInput_Data    Examine and update the input signals based on RTE failure events
//!
//! \return  Std_ReturnType     Returns 'returnValue_LECM' signal RTE read error status
//!
//!======================================================================================
static Std_ReturnType Get_RteDataRead_Common(BUIBasic_LINMaCtrl_In_struct_Type *pRteInput_Data)
{
   //! ###### Processing Rte input data read common logic
   Std_ReturnType returnValue       = RTE_E_INVALID;
   Std_ReturnType returnValue_LECM  = RTE_E_INVALID;
   
   //! ##### Read ComMode_LIN1 Interface
   returnValue = Rte_Read_ComMode_LIN1_ComMode_LIN(&pRteInput_Data->ComMode_LIN1);
   MACRO_StdRteRead_IntRPort((returnValue), (pRteInput_Data->ComMode_LIN1), (Error))
   //! ##### Read DiagInfoLECM_Basic Interface
   returnValue = Rte_Read_DiagInfoLECMBasic_DiagInfo(&pRteInput_Data->DiagInfoLECM_Basic);
   MACRO_StdRteRead_ExtRPort((returnValue), (pRteInput_Data->DiagInfoLECM_Basic), (0U), (0U))
   //! ##### Read LIN_BunkBAudioOnOff_ButtonStat Interface
   pRteInput_Data->LIN_BunkBAudioOnOff_ButtonStat.Previous = pRteInput_Data->LIN_BunkBAudioOnOff_ButtonStat.Current;
   returnValue = Rte_Read_LIN_BunkBAudioOnOff_ButtonStat_PushButtonStatus(&pRteInput_Data->LIN_BunkBAudioOnOff_ButtonStat.Current);
   MACRO_StdRteRead_ExtRPort((returnValue),( pRteInput_Data->LIN_BunkBAudioOnOff_ButtonStat.Current), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))  
   //! ##### Read LIN_BunkBIntLightActvnBtn_stat Interface
   pRteInput_Data->LIN_BunkBIntLightActvnBtn_stat.Previous = pRteInput_Data->LIN_BunkBIntLightActvnBtn_stat.Current;
   returnValue = Rte_Read_LIN_BunkBIntLightActvnBtn_stat_PushButtonStatus(&pRteInput_Data->LIN_BunkBIntLightActvnBtn_stat.Current);
   MACRO_StdRteRead_ExtRPort((returnValue), (pRteInput_Data->LIN_BunkBIntLightActvnBtn_stat.Current), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))  
   //! ##### Read LIN_BunkBParkHeater_ButtonStat Interface
   pRteInput_Data->LIN_BunkBParkHeater_ButtonStat.Previous = pRteInput_Data->LIN_BunkBParkHeater_ButtonStat.Current;
   returnValue = Rte_Read_LIN_BunkBParkHeater_ButtonStat_PushButtonStatus(&pRteInput_Data->LIN_BunkBParkHeater_ButtonStat.Current);
   MACRO_StdRteRead_ExtRPort((returnValue), (pRteInput_Data->LIN_BunkBParkHeater_ButtonStat.Current),( PushButtonStatus_NotAvailable), (PushButtonStatus_Error))   
   //! ##### Read LIN_BunkBTempDec_ButtonStat Interface
   pRteInput_Data->LIN_BunkBTempDec_ButtonStat.Previous = pRteInput_Data->LIN_BunkBTempDec_ButtonStat.Current;
   returnValue = Rte_Read_LIN_BunkBTempDec_ButtonStat_PushButtonStatus(&pRteInput_Data->LIN_BunkBTempDec_ButtonStat.Current);
   MACRO_StdRteRead_ExtRPort((returnValue), (pRteInput_Data->LIN_BunkBTempDec_ButtonStat.Current), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))  
   //! ##### Read LIN_BunkBTempInc_ButtonStat Interface
   pRteInput_Data->LIN_BunkBTempInc_ButtonStat.Previous = pRteInput_Data->LIN_BunkBTempInc_ButtonStat.Current;
   returnValue = Rte_Read_LIN_BunkBTempInc_ButtonStat_PushButtonStatus(&pRteInput_Data->LIN_BunkBTempInc_ButtonStat.Current);
   MACRO_StdRteRead_ExtRPort((returnValue), (pRteInput_Data->LIN_BunkBTempInc_ButtonStat.Current), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))  
   //! ##### Read LIN_BunkBVolumeDown_ButtonStat Interface
   pRteInput_Data->LIN_BunkBVolumeDown_ButtonStat.Previous = pRteInput_Data->LIN_BunkBVolumeDown_ButtonStat.Current;
   returnValue = Rte_Read_LIN_BunkBVolumeDown_ButtonStat_PushButtonStatus(&pRteInput_Data->LIN_BunkBVolumeDown_ButtonStat.Current);
   MACRO_StdRteRead_ExtRPort((returnValue),(pRteInput_Data->LIN_BunkBVolumeDown_ButtonStat.Current), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))   
   //! ##### Read LIN_BunkBVolumeUp_ButtonStat Interface
   pRteInput_Data->LIN_BunkBVolumeUp_ButtonStat.Previous = pRteInput_Data->LIN_BunkBVolumeUp_ButtonStat.Current;
   returnValue = Rte_Read_LIN_BunkBVolumeUp_ButtonStat_PushButtonStatus(&pRteInput_Data->LIN_BunkBVolumeUp_ButtonStat.Current);
   MACRO_StdRteRead_ExtRPort((returnValue),( pRteInput_Data->LIN_BunkBVolumeUp_ButtonStat.Current), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))  
   //! ##### Read ResponseErrorLECM_Basic Interface
   returnValue_LECM = Rte_Read_ResponseErrorLECMBasic_ResponseErrorLECMBasic(&pRteInput_Data->ResponseErrorLECM_Basic);
   MACRO_StdRteRead_ExtRPort((returnValue_LECM),( pRteInput_Data->ResponseErrorLECM_Basic), (1U), (1U))
   return returnValue_LECM;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Signals_GatewayLogic_BunkUserInterfaceBasic'
//!
//! \param   *pInData    Providing the input signals to send to output ports
//! \param   *pOutData   Updating the output ports with input signals
//!
//!======================================================================================
static void Signals_GatewayLogic_BunkUserInterfaceBasic(const BUIBasic_LINMaCtrl_In_struct_Type  *pInData,
                                                              BUIBasic_LINMaCtrl_Out_struct_Type *pOutData)
{
   //! ###### Processing Signals gateway logic
   //! ##### Update the output ports with values read on input ports 
   // Signals gateway by COM, from LECM basic 
   pOutData->BunkBAudioOnOff_ButtonStatus = pInData->LIN_BunkBAudioOnOff_ButtonStat.Current;
   pOutData->BunkBIntLightActvnBtn_stat   = pInData->LIN_BunkBIntLightActvnBtn_stat.Current;
   pOutData->BunkBParkHeater_ButtonStatus = pInData->LIN_BunkBParkHeater_ButtonStat.Current;
   pOutData->BunkBTempDec_ButtonStatus    = pInData->LIN_BunkBTempDec_ButtonStat.Current;
   pOutData->BunkBTempInc_ButtonStatus    = pInData->LIN_BunkBTempInc_ButtonStat.Current;
   pOutData->BunkBVolumeDown_ButtonStatus = pInData->LIN_BunkBVolumeDown_ButtonStat.Current;
   pOutData->BunkBVolumeUp_ButtonStatus   = pInData->LIN_BunkBVolumeUp_ButtonStat.Current;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Generic_ANW_AudioRadio3_CondCheck'
//!
//! \param   *pLIN_BunkBVolumeUp_ButtonStat     Providing the LIN bunk basic volume up button status
//! \param   *pLIN_BunkBVolumeDown_ButtonStat   Providing the LIN bunk basic volume down button status
//! \param   *pLIN_BunkBAudioOnOff_ButtonStat   Providing LIN bunk basic audio on off button status
//!
//! \return   FormalBoolean                     Returns 'isActTrigDet' status
//!
//!======================================================================================
static FormalBoolean Generic_ANW_AudioRadio3_CondCheck(const PushButtonStatus *pLIN_BunkBVolumeUp_ButtonStat,
                                                       const PushButtonStatus *pLIN_BunkBVolumeDown_ButtonStat,
                                                       const PushButtonStatus *pLIN_BunkBAudioOnOff_ButtonStat)
{
   //! ###### Check the AudioRadio3 trigger conditions and return the activation status
   FormalBoolean isActTrigDet = NO;

   if (((pLIN_BunkBVolumeUp_ButtonStat->Current != pLIN_BunkBVolumeUp_ButtonStat->Previous) 
      && (PushButtonStatus_Pushed == pLIN_BunkBVolumeUp_ButtonStat->Current)) 
      || ((pLIN_BunkBVolumeDown_ButtonStat->Current != pLIN_BunkBVolumeDown_ButtonStat->Previous) 
      && (PushButtonStatus_Pushed == pLIN_BunkBVolumeDown_ButtonStat->Current)) 
      || ((pLIN_BunkBAudioOnOff_ButtonStat->Current != pLIN_BunkBAudioOnOff_ButtonStat->Previous) 
      && (PushButtonStatus_Pushed == pLIN_BunkBAudioOnOff_ButtonStat->Current)))
   {
      isActTrigDet = YES;
   }
   else
   {
      isActTrigDet = NO;
   }
   return isActTrigDet;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Generic_ANW_AudioRadio3_Trigger'
//!
//! \param   isRestAct_AudioRadio3      Provides 'yes' or 'no' values based on restriction activation
//! \param   isActTrigDet_AudioRadio3   Provides 'yes' or 'no' values based on activation trigger
//!
//!======================================================================================
static void Generic_ANW_AudioRadio3_Trigger(const FormalBoolean isRestAct_AudioRadio3,
                                            const FormalBoolean isActTrigDet_AudioRadio3)
{
   //! ###### Processing ANW audio radio3 trigger logic
   static AnwSM_States  AnwState_Audio                   = { SM_ANW_Inactive,
                                                             SM_ANW_Inactive };
   static DeactivationTimer_Type Audio_DeactivationTimer = 0U;
   AnwAction_Enum  AnwActionstatus                       = ANW_Action_None;

   //! ##### Trigger activation/deactivation of application network request to ISSM module 
   //! ##### Process ANW logic : 'ProcessAnwLogic()'
   AnwActionstatus = ProcessAnwLogic(isRestAct_AudioRadio3,
                                     isActTrigDet_AudioRadio3,
                                     YES,
                                     CONST_AudioRadio3Timeout,
                                     &Audio_DeactivationTimer,
                                     &AnwState_Audio);
   //! ##### Check for Anw action is 'Activate' or 'Deactivate' for ANW audio radio3 trigger
   if (ANW_Action_Activate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_AudioRadio3_ActivateIss();
   }
   else if (ANW_Action_Deactivate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_AudioRadio3_DeactivateIss();
   }
   else
   {
      // Do nothing, keep previous status
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'GenericANW_OtherIntLights5_Cond'
//!
//! \param   *pLIN_BunkBIntLightActvnBtn_stat   Providing LIN bunk basic interior light activation button status
//!
//! \return   FormalBoolean                     Returns the 'isActTrigDet' status
//!
//!======================================================================================
static FormalBoolean GenericANW_OtherIntLights5_Cond(const PushButtonStatus *pLIN_BunkBIntLightActvnBtn_stat)
{
   //! ###### Check the OtherInteriorLights5 trigger conditions and return the activation status
   FormalBoolean isActTrigDet = NO;
   
   //! ##### Select 'BunkBIntLightActvnBtn_stat' 
   if (((pLIN_BunkBIntLightActvnBtn_stat->Current != pLIN_BunkBIntLightActvnBtn_stat->Previous)
      && (PushButtonStatus_Pushed == pLIN_BunkBIntLightActvnBtn_stat->Current)))
   {   
      isActTrigDet = YES;
   }
   else
   {
      isActTrigDet = NO;
   }
   return isActTrigDet;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'GenericANW_OtherInteriorLights5'
//!
//! \param   isRestAct_InteriorLights5      Provides 'yes' or 'no' values based on restriction activation
//! \param   isActTrigDet_InteriorLights5   Provides 'yes' or 'no' values based on activation trigger
//!
//!======================================================================================
static void GenericANW_OtherInteriorLights5(const FormalBoolean isRestAct_InteriorLights5,
                                            const FormalBoolean isActTrigDet_InteriorLights5)
{
   //! ###### Processing the ANW OtherInterior Lights5 logic
   static AnwSM_States AnwState_Lights5                    = { SM_ANW_Inactive,
                                                               SM_ANW_Inactive };
   static DeactivationTimer_Type Lights5_DeactivationTimer = 0U;
   AnwAction_Enum  AnwActionstatus                         = ANW_Action_None;
   //! ##### Trigger activation/deactivation of application network request to ISSM module 
   //! ##### Process ANW logic : 'ProcessAnwLogic()'
   AnwActionstatus = ProcessAnwLogic(isRestAct_InteriorLights5,
                                     isActTrigDet_InteriorLights5,
                                     YES,
                                     CONST_OtherInteriorLights5Timeout,
                                     &Lights5_DeactivationTimer,
                                     &AnwState_Lights5);
   //! ##### Check for Anw action is 'Activate' or 'Deactivate' for ANW other interior lights5
   if (ANW_Action_Activate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_OtherInteriorLights5_ActivateIss();
   }
   else if (ANW_Action_Deactivate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_OtherInteriorLights5_DeactivateIss();
   }
   else
   {
      // Do nothing, keep previous status
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'ANW_PHActMaintain_ActiveCond'
//!
//! \param   *pDeAct_LIN_BunkBTempDec_ButtonStat   Providing DeAct_LIN bunk basic temperature decrease button status
//! \param   *pDeAct_LIN_BunkBTempInc_ButtonStat   Providing DeAct_LIN bunk basic temperature increase button status
//! \param   *pLIN_BunkBParkHeater_ButtonStat      Providing LIN bunk basic park heater button status
//!
//! \return   FormalBoolean                        Returns 'isActTrigDet' status
//!
//!======================================================================================
static FormalBoolean ANW_PHActMaintain_ActiveCond(const PushButtonStatus *pDeAct_LIN_BunkBTempDec_ButtonStat,
                                                  const PushButtonStatus *pDeAct_LIN_BunkBTempInc_ButtonStat,
                                                  const PushButtonStatus *pLIN_BunkBParkHeater_ButtonStat)
{
   //! ###### Check the PHActMaintainLiving5 trigger conditions and return the activation status
   FormalBoolean isActTrigDet = NO;

   if (((pDeAct_LIN_BunkBTempDec_ButtonStat->Current != pDeAct_LIN_BunkBTempDec_ButtonStat->Previous)
      && (PushButtonStatus_Pushed == pDeAct_LIN_BunkBTempDec_ButtonStat->Current))
      || ((pDeAct_LIN_BunkBTempInc_ButtonStat->Current != pDeAct_LIN_BunkBTempInc_ButtonStat->Previous)
      && (PushButtonStatus_Pushed == pDeAct_LIN_BunkBTempInc_ButtonStat->Current))
      || ((pLIN_BunkBParkHeater_ButtonStat->Current != pLIN_BunkBParkHeater_ButtonStat->Previous)
      && (PushButtonStatus_Pushed == pLIN_BunkBParkHeater_ButtonStat->Current)))
   {
      isActTrigDet = YES;
   }
   else
   {
      isActTrigDet = NO;
   }
   return isActTrigDet;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'ANW_PHActMaintain_DeActiveCond'
//!
//! \param   *pLIN_BunkBTempDec_ButtonStat   Providing LIN bunk basic temperature decrease button status
//! \param   *pLIN_BunkBTempInc_ButtonStat   Providing LIN bunk basic temperature increase button status
//!
//! \return   FormalBoolean                  Returns 'isDeActTrigDet' status
//!
//!======================================================================================
static FormalBoolean ANW_PHActMaintain_DeActiveCond(const PushButtonStatus *pLIN_BunkBTempDec_ButtonStat,
                                                    const PushButtonStatus *pLIN_BunkBTempInc_ButtonStat)
{
   //! ###### Check the PHDeActivationMaintainLiving5 trigger conditions and return the deactivation status
   FormalBoolean isDeActTrigDet = NO;

   if ((PushButtonStatus_Pushed != pLIN_BunkBTempInc_ButtonStat->Current)
      && (PushButtonStatus_Pushed != pLIN_BunkBTempDec_ButtonStat->Current))
   {
      isDeActTrigDet = YES;
   }
   else
   {
      isDeActTrigDet = NO;
   }
   return isDeActTrigDet;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'GenericANW_PHActMaintainLiving5'
//!
//! \param   isRestAct_PHActMaintain        Provides 'yes' or 'no' values based on restriction activation
//! \param   isActTrigDet_PHActMaintain     Provides 'yes' or 'no' values based on activation trigger
//! \param   isDeActTrigDet_PHActMaintain   Provides 'yes' or 'no' values based on deactivation trigger
//!
//!======================================================================================
static void GenericANW_PHActMaintainLiving5(const FormalBoolean isRestAct_PHActMaintain,
                                            const FormalBoolean isActTrigDet_PHActMaintain,
                                            const FormalBoolean isDeActTrigDet_PHActMaintain)
{
   //! ###### Processing the ANW PH activation maintain living5 logic
   static AnwSM_States PHACT_AnwState                    = { SM_ANW_Inactive,
                                                             SM_ANW_Inactive };
   static DeactivationTimer_Type PHAct_DeactivationTimer = 0U;
   AnwAction_Enum AnwActionstatus                        = ANW_Action_None;

   //! ##### Trigger activation/deactivation of application network request to ISSM module 
   //! ##### Process ANW logic : 'ProcessAnwLogic()'
   AnwActionstatus = ProcessAnwLogic(isRestAct_PHActMaintain,
                                     isActTrigDet_PHActMaintain,
                                     isDeActTrigDet_PHActMaintain,
                                     CONST_PHActMaintainLiving5Timeout,
                                     &PHAct_DeactivationTimer,
                                     &PHACT_AnwState);
   //! ##### Check for Anw_Action is 'Activate' or 'Deactivate' for ANW PH activation maintain living5
   if (ANW_Action_Activate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_PHActMaintainLiving5_ActivateIss();
   }
   else if (ANW_Action_Deactivate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_PHActMaintainLiving5_DeactivateIss();
   }
   else
   {
      //Do nothing, keep previous status
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Deactivate_Logic_BunkUserInterfaceBasic'
//!
//! \param   *pOutData_DeAct   Update the output ports to not available
//!
//!======================================================================================
static void Deactivate_Logic_BunkUserInterfaceBasic(BUIBasic_LINMaCtrl_Out_struct_Type *pOutData_DeAct)
{
   //! ###### Processing deactivation logic
   //! ##### Write the all output ports to value 'notavailable'
   pOutData_DeAct->BunkBAudioOnOff_ButtonStatus = PushButtonStatus_NotAvailable;
   pOutData_DeAct->BunkBIntLightActvnBtn_stat   = PushButtonStatus_NotAvailable;
   pOutData_DeAct->BunkBParkHeater_ButtonStatus = PushButtonStatus_NotAvailable;
   pOutData_DeAct->BunkBTempDec_ButtonStatus    = PushButtonStatus_NotAvailable;
   pOutData_DeAct->BunkBTempInc_ButtonStatus    = PushButtonStatus_NotAvailable;
   pOutData_DeAct->BunkBVolumeDown_ButtonStatus = PushButtonStatus_NotAvailable;
   pOutData_DeAct->BunkBVolumeUp_ButtonStatus   = PushButtonStatus_NotAvailable;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'BunkUserInterfaceBasic_FCI_Indication'
//!
//! \param   *pDiagInfoLECM_Basic   Providing the diagnostic information of LECM basic
//!
//!======================================================================================
static boolean BunkUserInterfaceBasic_FCI_Indication(const DiagInfo_T *pDiagInfoLECM_Basic)
{
   //! ###### Process the BunkUserInterfaceBasic_FCI_Indication logic
   static uint8 SlaveFCIFault[2] = { CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive };
   boolean IsFciFaultActive      = CONST_Fault_InActive;
   uint8 Index                   = 0U;

   //! ##### Check the FCI Conditions and Log/Remove the Faults
   if ((DiagInfo_T)0x41 == *pDiagInfoLECM_Basic)
   {
      //! #### Set low voltage fault event to fail
      Rte_Call_Event_D1BOG_16_LECMLow_VBT_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x01 == *pDiagInfoLECM_Basic)
   {
      //! #### Set low voltage fault event to pass
      Rte_Call_Event_D1BOG_16_LECMLow_VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x42 == *pDiagInfoLECM_Basic)
   {
      //! #### Set high voltage fault event to fail
      Rte_Call_Event_D1BOG_17_LECMLow_VAT_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x02 == *pDiagInfoLECM_Basic)
   {
      //! #### Set high voltage fault event to pass
      Rte_Call_Event_D1BOG_17_LECMLow_VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x43 == *pDiagInfoLECM_Basic)
   {
      //! #### Set EEPROM failure fault event to fail
      SlaveFCIFault[0] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BOG_46_LECMLow_EEPROM_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x03 == *pDiagInfoLECM_Basic)
   {
      //! #### Set EEPROM failure fault event to pass
      SlaveFCIFault[0] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BOG_46_LECMLow_EEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x44 == *pDiagInfoLECM_Basic)
   {
      //! #### Set unexpected operation fault event to fail
      SlaveFCIFault[1] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BOG_94_LECMLow_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x04 == *pDiagInfoLECM_Basic)
   {
      //! #### Set unexpected operation fault event to pass
      SlaveFCIFault[1] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BOG_94_LECMLow_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x00 == *pDiagInfoLECM_Basic)
   {
      for (Index = 0U; Index < 2U; Index++)
      {
         SlaveFCIFault[Index] = CONST_SlaveFCIFault_InActive;
      }
      Rte_Call_Event_D1BOG_16_LECMLow_VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BOG_17_LECMLow_VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BOG_46_LECMLow_EEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BOG_94_LECMLow_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else
   {
      // Do nothing, keep previous status
   }
   IsFciFaultActive = CONST_Fault_InActive;
   for (Index = 0U; Index < 2U; Index++)
   {
      if (CONST_SlaveFCIFault_Active ==  SlaveFCIFault[Index])
      {
         IsFciFaultActive = CONST_Fault_Active;
      }
      else
      {
         // Do Nothing
      }
   }

   return IsFciFaultActive;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'FallbackLogic_BunkUserInterfaceBasic'
//!
//! \param   *pOutData_Fallback   Update the output ports to error
//!
//!======================================================================================
static void FallbackLogic_BunkUserInterfaceBasic(BUIBasic_LINMaCtrl_Out_struct_Type *pOutData_Fallback)
{
   //! ###### Processing fallback mode logic
   //! ##### Update the output ports with error
   pOutData_Fallback->BunkBAudioOnOff_ButtonStatus  =  PushButtonStatus_Error;
   pOutData_Fallback->BunkBIntLightActvnBtn_stat    =  PushButtonStatus_Error;
   pOutData_Fallback->BunkBParkHeater_ButtonStatus  =  PushButtonStatus_Error;
   pOutData_Fallback->BunkBTempDec_ButtonStatus     =  PushButtonStatus_Error;
   pOutData_Fallback->BunkBTempInc_ButtonStatus     =  PushButtonStatus_Error;
   pOutData_Fallback->BunkBVolumeDown_ButtonStatus  =  PushButtonStatus_Error;
   pOutData_Fallback->BunkBVolumeUp_ButtonStatus    =  PushButtonStatus_Error;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'RteDataWrite_Common'
//!
//! \param   *pOutput_data   Update the output signals to ports of output structure
//!
//!======================================================================================
static void RteDataWrite_Common(const BUIBasic_LINMaCtrl_Out_struct_Type *pOutput_data)
{
   //! ###### Processing RTE data write common logic
   Rte_Write_BunkBAudioOnOff_ButtonStatus_PushButtonStatus(pOutput_data->BunkBAudioOnOff_ButtonStatus);
   Rte_Write_BunkBIntLightActvnBtn_stat_PushButtonStatus(pOutput_data->BunkBIntLightActvnBtn_stat);
   Rte_Write_BunkBParkHeater_ButtonStatus_PushButtonStatus(pOutput_data->BunkBParkHeater_ButtonStatus);
   Rte_Write_BunkBTempDec_ButtonStatus_PushButtonStatus(pOutput_data->BunkBTempDec_ButtonStatus);
   Rte_Write_BunkBTempInc_ButtonStatus_PushButtonStatus(pOutput_data->BunkBTempInc_ButtonStatus);
   Rte_Write_BunkBVolumeDown_ButtonStatus_PushButtonStatus(pOutput_data->BunkBVolumeDown_ButtonStatus);
   Rte_Write_BunkBVolumeUp_ButtonStatus_PushButtonStatus(pOutput_data->BunkBVolumeUp_ButtonStatus);
}

//! @}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
