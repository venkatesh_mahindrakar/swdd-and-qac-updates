/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  ExteriorLightPanel_1_LINMastCtrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  ExteriorLightPanel_1_LINMastCtrl
 *  Generated at:  Fri Jun 12 17:01:40 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <ExteriorLightPanel_1_LINMastCtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file ExteriorLightPanel_1_LINMastCtrl.c
//! \copyright Volvo Group Trucks Technology 2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_VehicleControlAndActiveSafety 
//! @{
//! @addtogroup VehicleControl
//! @{
//! @addtogroup ExteriorLightPanel_1_LINMastCtrl
//! @{
//!
//! \brief
//! ExteriorLightPanel_1_LINMastCtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the ExteriorLightPanel_1_LINMastCtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_ExteriorLightPanel_1_LINMastCtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "FuncLibrary_AnwStateMachine_If.h"
#include "ExteriorLightPanel_1_LINMastCtrl.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DiagInfo_T: Integer in interval [0...127]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * ResponseErrorELCP1_T: Boolean
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 *   FreeWheel_Status_NoMovement (0U)
 *   FreeWheel_Status_1StepClockwise (1U)
 *   FreeWheel_Status_2StepsClockwise (2U)
 *   FreeWheel_Status_3StepsClockwise (3U)
 *   FreeWheel_Status_4StepsClockwise (4U)
 *   FreeWheel_Status_5StepsClockwise (5U)
 *   FreeWheel_Status_6StepsClockwise (6U)
 *   FreeWheel_Status_1StepCounterClockwise (7U)
 *   FreeWheel_Status_2StepsCounterClockwise (8U)
 *   FreeWheel_Status_3StepsCounterClockwise (9U)
 *   FreeWheel_Status_4StepsCounterClockwise (10U)
 *   FreeWheel_Status_5StepsCounterClockwise (11U)
 *   FreeWheel_Status_6StepsCounterClockwise (12U)
 *   FreeWheel_Status_Spare (13U)
 *   FreeWheel_Status_Error (14U)
 *   FreeWheel_Status_NotAvailable (15U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * Thumbwheel_stat_T: Enumeration of integer in interval [0...31] with enumerators
 *   Thumbwheel_stat_ThumbWheelPos0 (0U)
 *   Thumbwheel_stat_ThumbWheelPos1 (1U)
 *   Thumbwheel_stat_ThumbWheelPos2 (2U)
 *   Thumbwheel_stat_ThumbWheelPos3 (3U)
 *   Thumbwheel_stat_ThumbWheelPos4 (4U)
 *   Thumbwheel_stat_ThumbWheelPos5 (5U)
 *   Thumbwheel_stat_ThumbWheelPos6 (6U)
 *   Thumbwheel_stat_ThumbWheelPos7 (7U)
 *   Thumbwheel_stat_ThumbWheelPos8 (8U)
 *   Thumbwheel_stat_ThumbWheelPos9 (9U)
 *   Thumbwheel_stat_ThumbWheelPos10 (10U)
 *   Thumbwheel_stat_ThumbWheelPos11 (11U)
 *   Thumbwheel_stat_ThumbWheelPos12 (12U)
 *   Thumbwheel_stat_ThumbWheelPos13 (13U)
 *   Thumbwheel_stat_ThumbWheelPos14 (14U)
 *   Thumbwheel_stat_ThumbWheelPos15 (15U)
 *   Thumbwheel_stat_ThumbWheelPos16 (16U)
 *   Thumbwheel_stat_Spare (17U)
 *   Thumbwheel_stat_Spare_01 (18U)
 *   Thumbwheel_stat_Spare_02 (19U)
 *   Thumbwheel_stat_Spare_03 (20U)
 *   Thumbwheel_stat_Spare_04 (21U)
 *   Thumbwheel_stat_Spare_05 (22U)
 *   Thumbwheel_stat_Spare_06 (23U)
 *   Thumbwheel_stat_Spare_07 (24U)
 *   Thumbwheel_stat_Spare_08 (25U)
 *   Thumbwheel_stat_Spare_09 (26U)
 *   Thumbwheel_stat_Spare_10 (27U)
 *   Thumbwheel_stat_Spare_11 (28U)
 *   Thumbwheel_stat_Spare_12 (29U)
 *   Thumbwheel_stat_Error (30U)
 *   Thumbwheel_stat_NotAvaliable (31U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *   FALSE (0U)
 *   TRUE (1U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1VR3_ELCP1_Installed_v(void)
 *
 *********************************************************************************************************************/


#define ExteriorLightPanel_1_LINMastCtrl_START_SEC_CODE
#include "ExteriorLightPanel_1_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_ELCP1_Installed   (Rte_Prm_P1VR3_ELCP1_Installed_v())
/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOS_Data_P1EOS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, ExteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/
   *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_SUBFUNCTIONNOTSUPPORTED;
   return RTE_E_DataServices_P1EOS_Data_P1EOS_E_NOT_OK;
   
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP1LinCtrl(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOS_Data_P1EOS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, ExteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData (returns application error)
 *********************************************************************************************************************/
   Data[0] = (uint8)((Rte_IrvRead_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP1LinCtrl()) & CONST_Tester_NotPresent);
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ELCP1LinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOS_Data_P1EOS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, ExteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/
   Rte_IrvWrite_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ELCP1LinCtrl((Rte_IrvRead_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP1LinCtrl()) & CONST_Tester_NotPresent); // change 0x7F with Invert MSB Mask macro
   *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_POSITIVERESPONSE;
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_ELCP1LinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOS_Data_P1EOS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, ExteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/
   uint8 retVal = RTE_E_OK;
   *ErrorCode   = (Dcm_NegativeResponseCodeType)DCM_E_POSITIVERESPONSE;
   if (Data[0] < 3U) // change 3 with Maximum Range value
   {
      Rte_IrvWrite_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ELCP1LinCtrl(Data[0] | CONST_Tester_Present);// change 0x80 with MSB Mask macro
   }
   else
   {
      retVal     = RTE_E_DataServices_P1EOS_Data_P1EOS_E_NOT_OK;
      *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_REQUESTOUTOFRANGE;
   }
  return retVal;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ExteriorLightPanel_1_LINMastCtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DaytimeRunningLight_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_DiagInfoELCP1_DiagInfo(DiagInfo_T *data)
 *   Std_ReturnType Rte_Read_DrivingLightPlus_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_DrivingLight_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_FrontFog_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_LIN_BackLightDimming_Status_Thumbwheel_stat(Thumbwheel_stat_T *data)
 *   Std_ReturnType Rte_Read_LIN_BlackPanelMode_ButtonStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_FogLightFront_ButtonStat_1_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_FogLightRear_ButtonStat_1_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_LevelingThumbwheel_stat_Thumbwheel_stat(Thumbwheel_stat_T *data)
 *   Std_ReturnType Rte_Read_LIN_LightMode_Status_1_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_ParkingLight_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RearFog_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ResponseErrorELCP1_ResponseErrorELCP1(ResponseErrorELCP1_T *data)
 *   boolean Rte_IsUpdated_LIN_LightMode_Status_1_FreeWheel_Status(void)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_BackLightDimming_Status_Thumbwheel_stat(Thumbwheel_stat_T data)
 *   Std_ReturnType Rte_Write_BlackPanelMode_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_FogLightFront_ButtonStatus_1_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_FogLightRear_ButtonStatus_1_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_LIN_DaytimeRunningLight_Indica_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_DrivingLightPlus_Indicatio_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_DrivingLight_Indication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_FrontFog_Indication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_ParkingLight_Indication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_RearFog_Indication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LevelingThumbwheel_stat_Thumbwheel_stat(Thumbwheel_stat_T data)
 *   Std_ReturnType Rte_Write_LightMode_Status_1_FreeWheel_Status(FreeWheel_Status_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl(uint8 data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BKC_87_ELCPLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN9_16_ELCP__VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN9_17_ELCP_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN9_44_ELCP_RAM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN9_45_ELCP_FLASH_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN9_46_ELCP_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN9_49_ELCP_HWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_DimmingAdjustment1_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_DimmingAdjustment1_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ExteriorLightsRequest1_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ExteriorLightsRequest1_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution runnable logic for the ExteriorLightPanel_1_LINMastCtrl_20ms_runnable
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ExteriorLightPanel_1_LINMastCtrl_CODE) ExteriorLightPanel_1_LINMastCtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ExteriorLightPanel_1_LINMastCtrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   boolean IsFciFaultActive = CONST_Fault_InActive;
   // Define input RteInData Common
   static ExteriorLightPanel_1_LINMastCtrl_In_StructType  RteInData_Common;
   // Define input RteOutData Common 
   static ExteriorLightPanel_1_LINMastCtrl_Out_StructType RteOutData_Common = { Thumbwheel_stat_NotAvaliable,
                                                                                PushButtonStatus_NotAvailable,
                                                                                FreeWheel_Status_NotAvailable,
                                                                                Thumbwheel_stat_NotAvaliable ,
                                                                                DeviceIndication_Off,
                                                                                DeviceIndication_Off,
                                                                                DeviceIndication_Off,
                                                                                DeviceIndication_Off,
                                                                                DeviceIndication_Off,
                                                                                DeviceIndication_Off,
                                                                                PushButtonStatus_NotAvailable,
                                                                                PushButtonStatus_NotAvailable };
   // Local logic variables 
   Std_ReturnType retValue_ExtLightPanel     = RTE_E_INVALID;
   FormalBoolean  isActTrigDet_DimmingAdj1   = NO;
   FormalBoolean  isActTrigDet_ExtLightsReq1 = NO;
   FormalBoolean  isRestAct_DimmingAdj1      = YES;
   FormalBoolean  isRestAct_ExtLightsReq1    = YES;

   //! ###### Process the RTE read common logic: 'Get_RteDataRead_Common()'
   retValue_ExtLightPanel = Get_RteDataRead_Common(&RteInData_Common);
   //! ###### Check for communication mode of LIN4 is error (LIN Busoff)
   if (Error == RteInData_Common.ComMode_LIN4)
   {
      //! ##### Process the fallback mode logic: 'FallbackLogic_ExteriorLightPanel_1()'
      FallbackLogic_ExteriorLightPanel_1(&RteOutData_Common);
   }
   else
   {
      //! ###### Select 'ELCP1_Installed' parameter
      if (TRUE == PCODE_ELCP1_Installed)
      {
         //! ##### Check for communication mode of LIN4 status
         if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN4)
            || (SwitchDetection == RteInData_Common.ComMode_LIN4)
            || (Diagnostic == RteInData_Common.ComMode_LIN4))
         {
            // restriction = NO;
            isRestAct_DimmingAdj1   = NO;
            isRestAct_ExtLightsReq1 = NO;
            //! ##### Check for response error ELCP1 status (missing frame condition)
            if (RTE_E_MAX_AGE_EXCEEDED == retValue_ExtLightPanel)
            {
               //! ##### Check for communication mode of LIN4 is 'applicationmonitoring' or 'switchdetection'
               if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN4)
                  || (SwitchDetection == RteInData_Common.ComMode_LIN4))
               {
                  //! #### Invoke 'Rte_Call_Event_D1BKC_87_ELCPLink_NoResp_SetEventStatus' with status 'prefailed'
                  Rte_Call_Event_D1BKC_87_ELCPLink_NoResp_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
                  //! #### Process the fallback mode logic: 'FallbackLogic_ExteriorLightPanel_1()'
                  FallbackLogic_ExteriorLightPanel_1(&RteOutData_Common);
               }
               else
               {
                  //! ##### If LIN communication mode is in 'diagnostic' mode
                  //! #### Process the signals gateway logic: 'Signals_GatewayLogic_ExteriorLightPanel_1()'
                  Signals_GatewayLogic_ExteriorLightPanel_1(&RteInData_Common,
                                                            &RteOutData_Common);
               }
            }
            //! ##### Check for response error ILCP2 is RTE_E_OK or RTE_E_COM_STOPPED or RTE_E_NEVER_RECEIVED
            else if ((RTE_E_OK == retValue_ExtLightPanel)
                    || (RTE_E_COM_STOPPED == retValue_ExtLightPanel)
                    || (RTE_E_NEVER_RECEIVED == retValue_ExtLightPanel))
            {
               //! #### Invoke 'Rte_Call_Event_D1BKC_87_ELCPLink_NoResp_SetEventStatus' with status 'passed'(Remove the LIN missing frame fault)
               Rte_Call_Event_D1BKC_87_ELCPLink_NoResp_SetEventStatus(DEM_EVENT_STATUS_PASSED);
               //! #### Process fault code information(FCI) indication: 'FCIToDTC_ConversionLogic_ExteriorLightPanel_1()'
               IsFciFaultActive = FCIToDTC_ConversionLogic_ExteriorLightPanel_1(&RteInData_Common.DiagInfoELCP1);
               //! #### Check for IsFciFaultActive
               if (CONST_Fault_Active == IsFciFaultActive)
               {
                  //! #### Process fallback mode logic: 'FallbackLogic_ExteriorLightPanel_1()'
                  FallbackLogic_ExteriorLightPanel_1(&RteOutData_Common);
               }
               else
               {
                  //! #### Process the signals gateway logic: 'Signals_GatewayLogic_ExteriorLightPanel_1()'
                  Signals_GatewayLogic_ExteriorLightPanel_1(&RteInData_Common,
                                                            &RteOutData_Common);
               }
            }
            else
            {
               //! ##### Process the fallback mode logic: 'FallbackLogic_ExteriorLightPanel_1()'
               FallbackLogic_ExteriorLightPanel_1(&RteOutData_Common);
            }
         }
         else
         {
            //! ##### Process deactivation logic: 'DeactivationLogic_ExteriorLightPanel_1()'
            DeactivationLogic_ExteriorLightPanel_1(&RteOutData_Common);
         }
      }
      else
      {
         //! ###### Process deactivation logic: 'DeactivationLogic_ExteriorLightPanel_1()'
         DeactivationLogic_ExteriorLightPanel_1(&RteOutData_Common);
      }
   }
   //! ###### Process Dimming Adjustment1 trigger Logic: 'ANWDimmingAdjustment1_CondCheck()'
   isActTrigDet_DimmingAdj1 = ANWDimmingAdjustment1_CondCheck(&RteInData_Common.LIN_BackLightDimming_Status,
                                                              &RteInData_Common.LIN_BlackPanelMode_ButtonStat);
   //! ###### Activating application networks by Dimming Adjustment1 condition: 'ANW_DimmingAdjustment1_logic()'
   ANW_DimmingAdjustment1_logic(isRestAct_DimmingAdj1,
                                isActTrigDet_DimmingAdj1);
   //! ###### Process ExtLightsReq1 trigger logic: 'ANW_ExteriorLightsRequest1_Cond()'
   isActTrigDet_ExtLightsReq1 = ANW_ExteriorLightsRequest1_Cond(&RteInData_Common.LIN_LightMode_Status_1);
   //! ###### Activating application networks by ExtLightsReq1 condition: 'ANWExteriorLightsRequest1_Logic()'
   ANWExteriorLightsRequest1_Logic(isRestAct_ExtLightsReq1,
                                   isActTrigDet_ExtLightsReq1);
   if ((CONST_Tester_Present == ((Rte_IrvRead_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl()) & CONST_Tester_Present))
      && ((uint8)Diag_Active_TRUE == RteInData_Common.isDiagActive))
   {
      //! ###### Process input output control service logic : 'IOCtrlService_LinOutputSignalControl()'
      IOCtrlService_LinOutputSignalControl((uint8)((Rte_IrvRead_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl()) & CONST_Tester_NotPresent),
                                           &RteOutData_Common);
   }
   else
   {
      Rte_IrvWrite_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl(DeviceIndication_SpareValue);
   }
   //! ###### Process write data common RTE ports logic: 'RteDataWrite_Common()'
   RteDataWrite_Common(&RteOutData_Common,
                       &RteInData_Common.IsUpdated_LIN_LightMode_Status_1);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define ExteriorLightPanel_1_LINMastCtrl_STOP_SEC_CODE
#include "ExteriorLightPanel_1_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'IOCtrlService_LinOutputSignalControl'
//!
//! \param   OverrideData    Provide override data value
//! \param   *pOutData       Update the output data structure for Signals_GatewayLogic_ExteriorLightPanel_1
//!
//!======================================================================================
static void IOCtrlService_LinOutputSignalControl(const uint8                                           OverrideData,
                                                       ExteriorLightPanel_1_LINMastCtrl_Out_StructType *pOutData)
{
   //! ###### Override Lin output signals with IOControl service value
   pOutData->LIN_DaytimeRunningLight_Indica = OverrideData;
   pOutData->LIN_DrivingLightPlus_Indicatio = OverrideData;
   pOutData->LIN_DrivingLight_Indication    = OverrideData;
   pOutData->LIN_FrontFog_Indication        = OverrideData;
   pOutData->LIN_ParkingLight_Indication    = OverrideData;
   pOutData->LIN_RearFog_Indication         = OverrideData;
}

//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'RteDataWrite_Common'
//!
//! \param    *pRteOutData_Common                  Update the output signals to ports of output structure
//! \param    *pIsUpdated_LIN_LightMode_Status_1   Provide the updated status of LIN light mode
//!
//!======================================================================================
static void RteDataWrite_Common(const ExteriorLightPanel_1_LINMastCtrl_Out_StructType *pRteOutData_Common,
                                      boolean                                         *pIsUpdated_LIN_LightMode_Status_1)
{
   //! ###### Write all the output ports
   Rte_Write_BackLightDimming_Status_Thumbwheel_stat(pRteOutData_Common->BackLightDimming_Status);
   Rte_Write_BlackPanelMode_ButtonStatus_PushButtonStatus(pRteOutData_Common->BlackPanelMode_ButtonStatus);
   Rte_Write_FogLightFront_ButtonStatus_1_PushButtonStatus(pRteOutData_Common->FogLightFront_ButtonStatus_1);
   Rte_Write_FogLightRear_ButtonStatus_1_PushButtonStatus(pRteOutData_Common->FogLightRear_ButtonStatus_1);
   Rte_Write_LIN_DaytimeRunningLight_Indica_DeviceIndication(pRteOutData_Common->LIN_DaytimeRunningLight_Indica);
   Rte_Write_LIN_DrivingLightPlus_Indicatio_DeviceIndication(pRteOutData_Common->LIN_DrivingLightPlus_Indicatio);
   Rte_Write_LIN_DrivingLight_Indication_DeviceIndication(pRteOutData_Common->LIN_DrivingLight_Indication);
   Rte_Write_LIN_FrontFog_Indication_DeviceIndication(pRteOutData_Common->LIN_FrontFog_Indication);
   Rte_Write_LIN_ParkingLight_Indication_DeviceIndication(pRteOutData_Common->LIN_ParkingLight_Indication);
   Rte_Write_LIN_RearFog_Indication_DeviceIndication(pRteOutData_Common->LIN_RearFog_Indication);
   Rte_Write_LevelingThumbwheel_stat_Thumbwheel_stat(pRteOutData_Common->LevelingThumbwheel_stat);
   //! ###### Processing the free wheel update logic
   if (TRUE == *pIsUpdated_LIN_LightMode_Status_1)
   {
      *pIsUpdated_LIN_LightMode_Status_1 = FALSE;
      Rte_Write_LightMode_Status_1_FreeWheel_Status(pRteOutData_Common->LightMode_Status_1);
   }
   else if ((FreeWheel_Status_Error == pRteOutData_Common->LightMode_Status_1)
           || (FreeWheel_Status_NotAvailable == pRteOutData_Common->LightMode_Status_1))
   {
      Rte_Write_LightMode_Status_1_FreeWheel_Status(pRteOutData_Common->LightMode_Status_1);
   }
   else
   {
      // LightMode_Status_1 is not send to the output signal      
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Get_RteDataRead_Common'
//!
//! \param    *pRteInData_Common   Examine and update the input signals based on RTE failure events
//!
//! \return   Std_ReturnType       Returns 'retValue_ExtLightPanel' value
//!
//!======================================================================================
static Std_ReturnType Get_RteDataRead_Common(ExteriorLightPanel_1_LINMastCtrl_In_StructType *pRteInData_Common)
{
   //! ###### Read common RTE interfaces, store the previous value and process fallback modes
   Std_ReturnType retValue_ExtLightPanel = RTE_E_INVALID;
   Std_ReturnType retValue               = RTE_E_INVALID;   
   //! ##### Read DaytimeRunningLight_Indication interface
   retValue = Rte_Read_DaytimeRunningLight_Indication_DeviceIndication(&pRteInData_Common->DaytimeRunningLight_Indication);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->DaytimeRunningLight_Indication),( DeviceIndication_Off),( DeviceIndication_Off))
   //! ##### Read DrivingLightPlus_Indication interface
   retValue = Rte_Read_DrivingLightPlus_Indication_DeviceIndication(&pRteInData_Common->DrivingLightPlus_Indication);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->DrivingLightPlus_Indication), (DeviceIndication_Off), (DeviceIndication_Off))
   //! ##### Read DrivingLight_Indication interface
   retValue = Rte_Read_DrivingLight_Indication_DeviceIndication(&pRteInData_Common->DrivingLight_Indication);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->DrivingLight_Indication), (DeviceIndication_Off),( DeviceIndication_Off))
   //! ##### Read FrontFog_Indication interface
   retValue = Rte_Read_FrontFog_Indication_DeviceIndication(&pRteInData_Common->FrontFog_Indication);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->FrontFog_Indication), (DeviceIndication_Off), (DeviceIndication_Off))
   pRteInData_Common->LIN_BackLightDimming_Status.PreviousValue = pRteInData_Common->LIN_BackLightDimming_Status.CurrentValue;
   //! ##### Read LIN_BackLightDimming_Status interface
   retValue = Rte_Read_LIN_BackLightDimming_Status_Thumbwheel_stat(&pRteInData_Common->LIN_BackLightDimming_Status.CurrentValue);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->LIN_BackLightDimming_Status.CurrentValue), (Thumbwheel_stat_NotAvaliable), (Thumbwheel_stat_Error))
   pRteInData_Common->LIN_BlackPanelMode_ButtonStat.PreviousValue = pRteInData_Common->LIN_BlackPanelMode_ButtonStat.CurrentValue;
   //! ##### Read LIN_BlackPanelMode_ButtonStat interface   
   retValue = Rte_Read_LIN_BlackPanelMode_ButtonStat_PushButtonStatus(&pRteInData_Common->LIN_BlackPanelMode_ButtonStat.CurrentValue);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->LIN_BlackPanelMode_ButtonStat.CurrentValue), (PushButtonStatus_NotAvailable),( PushButtonStatus_Error))   
   //! ##### Read  LIN_FogLightFront_ButtonStat_1 interface
   retValue = Rte_Read_LIN_FogLightFront_ButtonStat_1_PushButtonStatus(&pRteInData_Common->LIN_FogLightFront_ButtonStat_1);
   MACRO_StdRteRead_ExtRPort((retValue),( pRteInData_Common->LIN_FogLightFront_ButtonStat_1), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))   
   //! ##### Read LIN_FogLightRear_ButtonStat_1 interface
   retValue = Rte_Read_LIN_FogLightRear_ButtonStat_1_PushButtonStatus(&pRteInData_Common->LIN_FogLightRear_ButtonStat_1);
   MACRO_StdRteRead_ExtRPort((retValue),( pRteInData_Common->LIN_FogLightRear_ButtonStat_1),( PushButtonStatus_NotAvailable),( PushButtonStatus_Error))  
   //! ##### Read LIN_LevelingThumbwheel_stat interface
   retValue = Rte_Read_LIN_LevelingThumbwheel_stat_Thumbwheel_stat(&pRteInData_Common->LIN_LevelingThumbwheel_stat);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->LIN_LevelingThumbwheel_stat), (Thumbwheel_stat_NotAvaliable),( Thumbwheel_stat_Error))
   //! ##### Read IsUpdated_LIN_LightMode_Status_1 interface
   pRteInData_Common->IsUpdated_LIN_LightMode_Status_1 = Rte_IsUpdated_LIN_LightMode_Status_1_FreeWheel_Status();
   //! ##### Read LIN_LightMode_Status_1 interface
   pRteInData_Common->LIN_LightMode_Status_1.PreviousValue = pRteInData_Common->LIN_LightMode_Status_1.CurrentValue;
   retValue = Rte_Read_LIN_LightMode_Status_1_FreeWheel_Status(&pRteInData_Common->LIN_LightMode_Status_1.CurrentValue);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->LIN_LightMode_Status_1.CurrentValue), (FreeWheel_Status_NotAvailable), (FreeWheel_Status_Error))
   //! ##### Read ParkingLight_Indication interface
   retValue = Rte_Read_ParkingLight_Indication_DeviceIndication(&pRteInData_Common->ParkingLight_Indication);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->ParkingLight_Indication), (DeviceIndication_Off),( DeviceIndication_Off))
   //! ##### Read RearFog_Indication interface
   retValue = Rte_Read_RearFog_Indication_DeviceIndication(&pRteInData_Common->RearFog_Indication);
   MACRO_StdRteRead_ExtRPort((retValue),  (pRteInData_Common->RearFog_Indication), (DeviceIndication_Off), (DeviceIndication_Off))
   //! ##### Read DiagInfoELCP1 interface
   retValue = Rte_Read_DiagInfoELCP1_DiagInfo(&pRteInData_Common->DiagInfoELCP1);
   MACRO_StdRteRead_ExtRPort((retValue),( pRteInData_Common->DiagInfoELCP1),( 0U), (0U))
   //! ##### Read ComMode_LIN4 interface
   retValue = Rte_Read_ComMode_LIN4_ComMode_LIN(&pRteInData_Common->ComMode_LIN4);
   MACRO_StdRteRead_IntRPort((retValue),( pRteInData_Common->ComMode_LIN4),( Error)) 
   //! ##### Read isDiagActive interface 
   retValue = Rte_Read_DiagActiveState_isDiagActive(&pRteInData_Common->isDiagActive);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->isDiagActive), (Diag_Active_FALSE))
   //! ##### Read ELCP1_ResponseError interface 
   retValue_ExtLightPanel = Rte_Read_ResponseErrorELCP1_ResponseErrorELCP1(&pRteInData_Common->ELCP1_ResponseError);
   MACRO_StdRteRead_ExtRPort((retValue_ExtLightPanel), (pRteInData_Common->ELCP1_ResponseError), (1U), (1U))

   return retValue_ExtLightPanel;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'DeactivationLogic_ExteriorLightPanel_1'
//!
//! \param   *pOut_Deact    Update the output ports to off
//!
//!======================================================================================
static void DeactivationLogic_ExteriorLightPanel_1(ExteriorLightPanel_1_LINMastCtrl_Out_StructType *pOut_Deact)
{
   //! ###### Update all the output ports to 'Off'
   pOut_Deact->LIN_RearFog_Indication          = DeviceIndication_Off;
   pOut_Deact->LIN_DrivingLightPlus_Indicatio  = DeviceIndication_Off;
   pOut_Deact->LIN_ParkingLight_Indication     = DeviceIndication_Off;
   pOut_Deact->LIN_FrontFog_Indication         = DeviceIndication_Off;
   pOut_Deact->LIN_DrivingLight_Indication     = DeviceIndication_Off;
   pOut_Deact->LIN_DaytimeRunningLight_Indica  = DeviceIndication_Off; // NotAvailable value has not been given 
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'FallbackLogic_ExteriorLightPanel_1'
//!
//! \param   *pOut_Fallback   Update the output ports to error
//!
//!======================================================================================
static void FallbackLogic_ExteriorLightPanel_1(ExteriorLightPanel_1_LINMastCtrl_Out_StructType  *pOut_Fallback)
{
   //! ###### Update all the output ports to 'error'
   pOut_Fallback->LightMode_Status_1           = FreeWheel_Status_Error;
   pOut_Fallback->BackLightDimming_Status      = Thumbwheel_stat_Error;
   pOut_Fallback->BlackPanelMode_ButtonStatus  = PushButtonStatus_Error;
   pOut_Fallback->FogLightRear_ButtonStatus_1  = PushButtonStatus_Error;
   pOut_Fallback->FogLightFront_ButtonStatus_1 = PushButtonStatus_Error;
   pOut_Fallback->LevelingThumbwheel_stat      = Thumbwheel_stat_Error;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'Signals_GatewayLogic_ExteriorLightPanel_1'
//!
//! \param   *pInData_Gateway       Providing the input signals to send to output ports
//! \param   *pOutData_Gateway      Updating the output ports with input signals
//!
//!======================================================================================
static void Signals_GatewayLogic_ExteriorLightPanel_1(const ExteriorLightPanel_1_LINMastCtrl_In_StructType   *pInData_Gateway,
                                                            ExteriorLightPanel_1_LINMastCtrl_Out_StructType  *pOutData_Gateway)
{
   //! ###### Processing the gateway logic
   //! ##### Write the output ports with values read on input ports accordingly 
   pOutData_Gateway->BackLightDimming_Status        = pInData_Gateway->LIN_BackLightDimming_Status.CurrentValue;
   pOutData_Gateway->BlackPanelMode_ButtonStatus    = pInData_Gateway->LIN_BlackPanelMode_ButtonStat.CurrentValue;
   pOutData_Gateway->FogLightFront_ButtonStatus_1   = pInData_Gateway->LIN_FogLightFront_ButtonStat_1;
   pOutData_Gateway->FogLightRear_ButtonStatus_1    = pInData_Gateway->LIN_FogLightRear_ButtonStat_1;
   pOutData_Gateway->LevelingThumbwheel_stat        = pInData_Gateway->LIN_LevelingThumbwheel_stat;  
   pOutData_Gateway->LIN_DaytimeRunningLight_Indica = pInData_Gateway->DaytimeRunningLight_Indication;
   pOutData_Gateway->LIN_DrivingLight_Indication    = pInData_Gateway->DrivingLight_Indication;
   pOutData_Gateway->LIN_DrivingLightPlus_Indicatio = pInData_Gateway->DrivingLightPlus_Indication;
   pOutData_Gateway->LIN_FrontFog_Indication        = pInData_Gateway->FrontFog_Indication;
   pOutData_Gateway->LIN_ParkingLight_Indication    = pInData_Gateway->ParkingLight_Indication;
   pOutData_Gateway->LIN_RearFog_Indication         = pInData_Gateway->RearFog_Indication;
   pOutData_Gateway->LightMode_Status_1             = pInData_Gateway->LIN_LightMode_Status_1.CurrentValue;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'FCIToDTC_ConversionLogic_ExteriorLightPanel_1'
//!
//! \param   *pDiagInfoELCP1   Providing the diagnostic information of ELCP1
//!
//! \return   boolean          Returns 'IsFciFaultActive' value
//!
//!======================================================================================
static boolean FCIToDTC_ConversionLogic_ExteriorLightPanel_1(const DiagInfo_T *pDiagInfoELCP1)
{
   static uint8 SlaveFCIFault[8] = { CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive };
   boolean IsFciFaultActive      = CONST_Fault_InActive;
   uint8 Index                   = 0U;
   //! ###### Check the FCI conditions and log/remove the faults
   if ((DiagInfo_T)0x41 == *pDiagInfoELCP1)
   {
      //! ##### Set supply voltage too low fault event to fail
      Rte_Call_Event_D1BN9_16_ELCP__VBT_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x01 == *pDiagInfoELCP1)
   {
      //! ##### Set supply voltage too low fault event to pass
      Rte_Call_Event_D1BN9_16_ELCP__VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x42 == *pDiagInfoELCP1)
   {
      //! ##### Set supply voltage too high fault event to fail
      Rte_Call_Event_D1BN9_17_ELCP_VAT_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x02 == *pDiagInfoELCP1)
   {
      //! ##### Set supply voltage too high fault event to pass
      Rte_Call_Event_D1BN9_17_ELCP_VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x43 == *pDiagInfoELCP1)
   {
      //! ##### Set ROM CS error fault event to fail
      Rte_Call_Event_D1BN9_46_ELCP_EEPROM_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      SlaveFCIFault[0] = CONST_SlaveFCIFault_Active;
   }
   else if ((DiagInfo_T)0x03 == *pDiagInfoELCP1)
   {
      //! ##### Set ROM CS error fault event to pass
      Rte_Call_Event_D1BN9_46_ELCP_EEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      SlaveFCIFault[0] = CONST_SlaveFCIFault_InActive;
   }
   else if ((DiagInfo_T)0x44 == *pDiagInfoELCP1)
   {
      //! ##### Set RAM check fault event to fail
      Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      SlaveFCIFault[1] = CONST_SlaveFCIFault_Active;
   }
   else if ((DiagInfo_T)0x04 == *pDiagInfoELCP1)
   { 
      //! ##### Set RAM check fault event to pass
      Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      SlaveFCIFault[1] = CONST_SlaveFCIFault_InActive;
   }
   
   else if ((DiagInfo_T)0x45 == *pDiagInfoELCP1)
   {
      //! ##### Set overrun fault event to fail
      Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      SlaveFCIFault[2] = CONST_SlaveFCIFault_Active;
   }
   else if ((DiagInfo_T)0x05 == *pDiagInfoELCP1)
   {
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      SlaveFCIFault[2] = CONST_SlaveFCIFault_InActive;
   }
   else if ((DiagInfo_T)0x46 == *pDiagInfoELCP1)
   {
      //! ##### Set overrun fault event to fail
      Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      SlaveFCIFault[3] = CONST_SlaveFCIFault_Active;
   }
   else if ((DiagInfo_T)0x06 == *pDiagInfoELCP1)
   {
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      SlaveFCIFault[3] = CONST_SlaveFCIFault_InActive;
   }
   else if ((DiagInfo_T)0x47 == *pDiagInfoELCP1)
   {
      //! ##### Set overrun fault event to fail
      Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      SlaveFCIFault[4] = CONST_SlaveFCIFault_Active;
   }
   else if ((DiagInfo_T)0x07 == *pDiagInfoELCP1)
   {
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      SlaveFCIFault[4] = CONST_SlaveFCIFault_InActive;
   }
   else if ((DiagInfo_T)0x48 == *pDiagInfoELCP1)
   {
      //! ##### Set overrun fault event to fail
      Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      SlaveFCIFault[5] = CONST_SlaveFCIFault_Active;
   }
   else if ((DiagInfo_T)0x08 == *pDiagInfoELCP1)
   {
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      SlaveFCIFault[5] = CONST_SlaveFCIFault_InActive;
   }
   else if ((DiagInfo_T)0x49 == *pDiagInfoELCP1)
   {
      //! ##### Set watchdog error fault event to fail
      Rte_Call_Event_D1BN9_45_ELCP_FLASH_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      SlaveFCIFault[6] = CONST_SlaveFCIFault_Active;
   }
   else if ((DiagInfo_T)0x09 == *pDiagInfoELCP1)
   {
      //! ##### Set watchdog error fault event to pass
      Rte_Call_Event_D1BN9_45_ELCP_FLASH_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      SlaveFCIFault[6] = CONST_SlaveFCIFault_InActive;
   }
   else if ((DiagInfo_T)0x4A == *pDiagInfoELCP1)
   {
      //! ##### Set overrun fault event to fail
      Rte_Call_Event_D1BN9_49_ELCP_HWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      SlaveFCIFault[7] = CONST_SlaveFCIFault_Active;
   }
   else if ((DiagInfo_T)0x0A == *pDiagInfoELCP1)
   {
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1BN9_49_ELCP_HWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      SlaveFCIFault[7] = CONST_SlaveFCIFault_InActive;
   }
   else if ((DiagInfo_T)0x00 == *pDiagInfoELCP1)
   {
      for(Index = 0U;Index < 8U;Index++)
      {
         SlaveFCIFault[Index] = CONST_SlaveFCIFault_InActive;
      }
      Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BN9_49_ELCP_HWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BN9_45_ELCP_FLASH_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BN9_46_ELCP_EEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BN9_17_ELCP_VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BN9_16_ELCP__VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);   
   }
   else
   {
      // Do nothing
   }
   IsFciFaultActive = CONST_Fault_InActive;
   for(Index = 0U;Index < 8U;Index++)
   {
      if(CONST_SlaveFCIFault_Active == SlaveFCIFault[Index])
      {
         IsFciFaultActive = CONST_Fault_Active;
      }
      else
      {
         // Do Nothing
      }
   }
   return IsFciFaultActive;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'ANWDimmingAdjustment1_CondCheck'
//!
//! \param   *pLIN_BackLightDimming_Status     Provides the input status
//! \param   *pLIN_BlackPanelMode_ButtonStat   Provides the input button status
//!
//! \return   FormalBoolean                    Returns 'isActivationTriggerDetected' value
//!
//!======================================================================================
static FormalBoolean ANWDimmingAdjustment1_CondCheck(const Thumbwheel_stat  *pLIN_BackLightDimming_Status,
                                                     const PushButtonStatus *pLIN_BlackPanelMode_ButtonStat)
{
   FormalBoolean isActivationTriggerDetected = NO;
   //! ###### Check for Dimming Adjustment1 conditions and return the trigger detection status
   if (((pLIN_BackLightDimming_Status->PreviousValue != pLIN_BackLightDimming_Status->CurrentValue)
      && (((Thumbwheel_stat_Spare != pLIN_BackLightDimming_Status->CurrentValue)
      && (Thumbwheel_stat_Spare_01 != pLIN_BackLightDimming_Status->CurrentValue)
      && (Thumbwheel_stat_Spare_02 != pLIN_BackLightDimming_Status->CurrentValue)
      && (Thumbwheel_stat_Spare_03 != pLIN_BackLightDimming_Status->CurrentValue)
      && (Thumbwheel_stat_Spare_04 != pLIN_BackLightDimming_Status->CurrentValue)
      && (Thumbwheel_stat_Spare_05 != pLIN_BackLightDimming_Status->CurrentValue)
      && (Thumbwheel_stat_Spare_06 != pLIN_BackLightDimming_Status->CurrentValue)
      && (Thumbwheel_stat_Spare_07 != pLIN_BackLightDimming_Status->CurrentValue)
      && (Thumbwheel_stat_Spare_08 != pLIN_BackLightDimming_Status->CurrentValue)
      && (Thumbwheel_stat_Spare_09 != pLIN_BackLightDimming_Status->CurrentValue)
      && (Thumbwheel_stat_Spare_10 != pLIN_BackLightDimming_Status->CurrentValue)
      && (Thumbwheel_stat_Spare_11 != pLIN_BackLightDimming_Status->CurrentValue)
      && (Thumbwheel_stat_Spare_12 != pLIN_BackLightDimming_Status->CurrentValue))
      && (Thumbwheel_stat_Error != pLIN_BackLightDimming_Status->CurrentValue)
      && (Thumbwheel_stat_NotAvaliable != pLIN_BackLightDimming_Status->CurrentValue)))
      || ((PushButtonStatus_Pushed != pLIN_BlackPanelMode_ButtonStat->PreviousValue)
      && (PushButtonStatus_Pushed == pLIN_BlackPanelMode_ButtonStat->CurrentValue)))
   {
      isActivationTriggerDetected = YES;
   }
   else
   {
      isActivationTriggerDetected = NO;
   }
   return isActivationTriggerDetected;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'ANW_DimmingAdjustment1_logic'
//!
//! \param   isRestAct_DimmingAdj1      Provides 'yes' or 'no' values based on restriction activation
//! \param   isActTrigDet_DimmingAdj1   Provides 'yes' or 'no' values based on activation trigger
//!
//!======================================================================================
static void ANW_DimmingAdjustment1_logic(const FormalBoolean isRestAct_DimmingAdj1,
                                         const FormalBoolean isActTrigDet_DimmingAdj1)
{
   //! ###### Processing the application network (activation/deactivation)conditions of Dimming Adjustment1
   static DeactivationTimer_Type DeactivationTimer_DimmingAdj1       = 0U;
   static AnwSM_States ANW_SM_DimmingAdj1ActDeactivation             = { SM_ANW_Inactive,
                                                                         SM_ANW_Inactive };
   //! ##### Process ANW logic : 'ProcessAnwLogic()'
   AnwAction_Enum Anw_Action = ANW_Action_None;
   Anw_Action = ProcessAnwLogic(isRestAct_DimmingAdj1, 
                                isActTrigDet_DimmingAdj1,
                                YES, 
                                CONST_ANW_DimmingAdj1_Timeout, 
                                &DeactivationTimer_DimmingAdj1,
                                &ANW_SM_DimmingAdj1ActDeactivation);
   //! ##### Check for Anw action is 'Activate' or 'Deactivate' for Dimming Adjustment1
   if (ANW_Action_Activate == Anw_Action)
   {
      Rte_Call_UR_ANW_DimmingAdjustment1_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   { 
      Rte_Call_UR_ANW_DimmingAdjustment1_DeactivateIss();
   }
   else
   {
      // Do nothing, keep current state
   }
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'ANW_ExteriorLightsRequest1_Cond'
//!
//! \param   pLIN_LightMode_Status_1   Provides the input status
//!
//! \return  FormalBoolean             Returns 'isActivationTriggerDetected' value
//!
//!======================================================================================
static FormalBoolean ANW_ExteriorLightsRequest1_Cond(const FreeWheel_Status *pLIN_LightMode_Status_1)
{
   FormalBoolean isActivationTriggerDetected = NO;

   //! ###### Check for Exterior Lights Req1 conditions and return the trigger condtion
   if ((pLIN_LightMode_Status_1->PreviousValue != pLIN_LightMode_Status_1->CurrentValue)
      && ((FreeWheel_Status_NoMovement != pLIN_LightMode_Status_1->CurrentValue)
      && (FreeWheel_Status_Spare != pLIN_LightMode_Status_1->CurrentValue) 
      && (FreeWheel_Status_Error != pLIN_LightMode_Status_1->CurrentValue)
      && (FreeWheel_Status_NotAvailable != pLIN_LightMode_Status_1->CurrentValue)))
   {
      isActivationTriggerDetected = YES;
   }
   else
   {
      isActivationTriggerDetected = NO;
   }
   return isActivationTriggerDetected;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'ANWExteriorLightsRequest1_Logic'
//!
//! \param    isRestAct_ExtLightsReq1      Provides 'yes' or 'no' values based on restriction activation
//! \param    isActTrigDet_ExtLightsReq1   Provides 'yes' or 'no' values based on activation trigger
//!
//!======================================================================================
static void ANWExteriorLightsRequest1_Logic(const FormalBoolean isRestAct_ExtLightsReq1,
                                             const FormalBoolean isActTrigDet_ExtLightsReq1)
{
   //! ###### Processing the application network (activation/deactivation)conditions of Exterior Lights Req1
   static DeactivationTimer_Type DeactivationTimer_ExtLightsReq1     = 0U;
   static AnwSM_States ANW_SM_ExtLightsReq1ActDeactivation           = { SM_ANW_Inactive,
                                                                         SM_ANW_Inactive };
   //! ##### Process ANW logic : 'ProcessAnwLogic()'
   AnwAction_Enum Anw_Action                                         = ANW_Action_None;
   Anw_Action = ProcessAnwLogic(isRestAct_ExtLightsReq1, 
                                isActTrigDet_ExtLightsReq1, 
                                YES, 
                                CONST_ANW_ExtLightsReq1_Timeout, 
                                &DeactivationTimer_ExtLightsReq1, 
                                &ANW_SM_ExtLightsReq1ActDeactivation);
   //! ##### Check for Anw action is 'Activate' or 'Deactivate' for Exterior Lights Req1
   if (ANW_Action_Activate == Anw_Action)
   {
      Rte_Call_UR_ANW_ExteriorLightsRequest1_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      Rte_Call_UR_ANW_ExteriorLightsRequest1_DeactivateIss();
   }
   else
   {
      // Do nothing, keep current state
   }
}

//! @}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
