/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  DiffLockPanel_LINMasterCtrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  DiffLockPanel_LINMasterCtrl
 *  Generated at:  Fri Jun 12 17:01:40 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <DiffLockPanel_LINMasterCtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file DiffLockPanel_LINMasterCtrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_VehicleDynamics_Traction 
//! @{
//! @addtogroup DiffLockPanel_LINMasterCtrl
//! @{
//!
//! \brief
//! DiffLockPanel_LINMasterCtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the DiffLockPanel_LINMasterCtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_DiffLockPanel_LINMasterCtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "DiffLockPanel_LINMasterCtrl.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DiagInfo_T: Integer in interval [0...127]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * ResponseErrorDLFW_T: Boolean
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 *   FreeWheel_Status_NoMovement (0U)
 *   FreeWheel_Status_1StepClockwise (1U)
 *   FreeWheel_Status_2StepsClockwise (2U)
 *   FreeWheel_Status_3StepsClockwise (3U)
 *   FreeWheel_Status_4StepsClockwise (4U)
 *   FreeWheel_Status_5StepsClockwise (5U)
 *   FreeWheel_Status_6StepsClockwise (6U)
 *   FreeWheel_Status_1StepCounterClockwise (7U)
 *   FreeWheel_Status_2StepsCounterClockwise (8U)
 *   FreeWheel_Status_3StepsCounterClockwise (9U)
 *   FreeWheel_Status_4StepsCounterClockwise (10U)
 *   FreeWheel_Status_5StepsCounterClockwise (11U)
 *   FreeWheel_Status_6StepsCounterClockwise (12U)
 *   FreeWheel_Status_Spare (13U)
 *   FreeWheel_Status_Error (14U)
 *   FreeWheel_Status_NotAvailable (15U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *   FALSE (0U)
 *   TRUE (1U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1B04_DLFW_Installed_v(void)
 *
 *********************************************************************************************************************/


#define DiffLockPanel_LINMasterCtrl_START_SEC_CODE
#include "DiffLockPanel_LINMasterCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_DLFW_Installed   (Rte_Prm_P1B04_DLFW_Installed_v())
/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOR_Data_P1EOR_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiffLockPanel_LINMasterCtrl_CODE) DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

   *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_SUBFUNCTIONNOTSUPPORTED;

   return RTE_E_DataServices_P1EOR_Data_P1EOR_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData_Irv_IOCTL_DiffLockLinCtrl(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOR_Data_P1EOR_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiffLockPanel_LINMasterCtrl_CODE) DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData (returns application error)
 *********************************************************************************************************************/

   Data[0] = (uint8) ((Rte_IrvRead_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData_Irv_IOCTL_DiffLockLinCtrl())& CONST_Tester_Notpresent);

   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_DiffLockLinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOR_Data_P1EOR_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiffLockPanel_LINMasterCtrl_CODE) DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

   Rte_IrvWrite_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_DiffLockLinCtrl((Rte_IrvRead_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData_Irv_IOCTL_DiffLockLinCtrl())& CONST_Tester_Notpresent); // change 0x7F with Invert MSB Mask macro
   *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_POSITIVERESPONSE;
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_DiffLockLinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOR_Data_P1EOR_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiffLockPanel_LINMasterCtrl_CODE) DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/
   uint8 retval = RTE_E_OK;
   *ErrorCode   = (Dcm_NegativeResponseCodeType)DCM_E_POSITIVERESPONSE;
   if (Data[0] < 3U) // change 3 with Maximum Range value
   {
      Rte_IrvWrite_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_DiffLockLinCtrl(Data[0]| CONST_Tester_Present); // change 0x80 with MSB Mask macro
   }
   else
   {
      retval     = RTE_E_DataServices_P1EOR_Data_P1EOR_E_NOT_OK;
      *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_REQUESTOUTOFRANGE;
   }
   return retval;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DiffLockPanel_LINMasterCtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_DiagInfoDLFW_DiagInfo(DiagInfo_T *data)
 *   Std_ReturnType Rte_Read_DifflockOnOff_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_EscButtonMuddySiteDeviceInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_LIN_DifflockDeactivationBtn_st_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_DifflockMode_Wheelstatus_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_LIN_EscButtonMuddySiteStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_Offroad_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_Offroad_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ResponseErrorDLFW_ResponseErrorDLFW(ResponseErrorDLFW_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DifflockDeactivationBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_DifflockMode_Wheelstatus_FreeWheel_Status(FreeWheel_Status_T data)
 *   Std_ReturnType Rte_Write_EscButtonMuddySiteStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_LIN_DifflockOnOff_Indication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_EscButtonMuddySiteDeviceIn_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_Offroad_Indication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Offroad_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_DiffLockPanel_LINMasterCtrl_20ms_runnable_Irv_IOCTL_DiffLockLinCtrl(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DiffLockPanel_LINMasterCtrl_20ms_runnable_Irv_IOCTL_DiffLockLinCtrl(uint8 data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BKH_87_DLFWLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN2_16_DLFW_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN2_17_DLFW_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN2_44_DLFW_RAM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN2_45_DLFW_FLASH_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN2_46_DLFW_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN2_94_DLFW_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DiffLockPanel_LINMasterCtrl_20ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//!
//! \brief
//! This function implements the cyclic execution runnable logic for the DiffLockPanel_LINMasterCtrl_20m_runnable
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DiffLockPanel_LINMasterCtrl_CODE) DiffLockPanel_LINMasterCtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DiffLockPanel_LINMasterCtrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   // Define Rte input data common
   boolean IsFciFaultActive = CONST_Fault_InActive;
   static DiffLockPanel_LINMasterCtrl_In_StructType  RteInData_Common;
   // Define Rte output data Common
   static DiffLockPanel_LINMasterCtrl_Out_StructType RteOutData_Common = { PushButtonStatus_NotAvailable,
                                                                           FreeWheel_Status_NotAvailable,
                                                                           PushButtonStatus_NotAvailable,
                                                                           PushButtonStatus_NotAvailable,
                                                                           DeviceIndication_Off,
                                                                           DeviceIndication_Off,
                                                                           DeviceIndication_Off };
   Std_ReturnType retValue_RespDLFW = RTE_E_INVALID;

   //! ###### Process the RTE read common logic: 'Get_RteDataRead_Common()'
   retValue_RespDLFW = Get_RteDataRead_Common(&RteInData_Common);
   //! ###### Check for communication mode of LIN4 is error (LIN Busoff)
   if (Error == RteInData_Common.ComMode_LIN4)
   {
      //! ##### Process the fallback mode logic: 'DiffLckPanel_LINMastCtrl_FallbackLogic()'
      DiffLckPanel_LINMastCtrl_FallbackLogic(&RteOutData_Common.DifflockDeactivationBtn_st,
                                             &RteOutData_Common.DifflockMode_Wheelstatus,
                                             &RteOutData_Common.EscButtonMuddySiteStatus,
                                             &RteOutData_Common.Offroad_ButtonStatus);
   }
   else
   {
      //! ###### Select 'DLFW_Installed' parameter
      if (TRUE == PCODE_DLFW_Installed)
      {
         //! ##### Check for communication mode of LIN4 status
         if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN4)
            || (SwitchDetection == RteInData_Common.ComMode_LIN4)
            || (Diagnostic == RteInData_Common.ComMode_LIN4))
         {
            //! ##### If LIN frame is not received for 3 times
            //! ##### Check for response error of DLFW (missing frame condition)
            if (RTE_E_MAX_AGE_EXCEEDED == retValue_RespDLFW)
            {
               //! ##### Check for communication mode of LIN4 is 'applicationmonitoring' or 'switchdetection'
               if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN4)
                  || (SwitchDetection == RteInData_Common.ComMode_LIN4))
               {
                  //! #### Invoke 'Rte_Call_Event_D1BKH_87_DLFWLink_NoResp_SetEventStatus' with status 'failed'
                  Rte_Call_Event_D1BKH_87_DLFWLink_NoResp_SetEventStatus(DEM_EVENT_STATUS_FAILED);
                  //! #### Process the fallback mode logic: 'DiffLckPanel_LINMastCtrl_FallbackLogic()'
                  DiffLckPanel_LINMastCtrl_FallbackLogic(&RteOutData_Common.DifflockDeactivationBtn_st,
                                                         &RteOutData_Common.DifflockMode_Wheelstatus,
                                                         &RteOutData_Common.EscButtonMuddySiteStatus,
                                                         &RteOutData_Common.Offroad_ButtonStatus);
               }
               else
               {
                  //! ##### If LIN communication mode is in 'diagnostic' mode
                  //! #### Process the signals gateway logic: 'DiffLockPanel_LINMasterCtrl_Signals_GatewayLogic()'
                  DiffLockPanel_LINMasterCtrl_Signals_GatewayLogic(&RteInData_Common,
                                                                   &RteOutData_Common);
               }
            }
            //! ##### Check for response error DLFW is RTE_E_OK or RTE_E_COM_STOPPED or RTE_E_NEVER_RECEIVED
            else if ((RTE_E_OK == retValue_RespDLFW)
                    || (RTE_E_COM_STOPPED == retValue_RespDLFW)
                    || (RTE_E_NEVER_RECEIVED == retValue_RespDLFW))
            {
               //! #### LIN frame received without receive timeout
               //! #### Invoke 'Rte_Call_Event_D1BKH_87_DLFWLink_NoResp_SetEventStatus' with status 'passed'(Remove the LIN missing frame fault)
               Rte_Call_Event_D1BKH_87_DLFWLink_NoResp_SetEventStatus(DEM_EVENT_STATUS_PASSED); 
               //! #### Process fault code information(FCI) indication: 'DiffLockPanel_LINMasterCtrl_FCI_Indication()'
               IsFciFaultActive = DiffLockPanel_LINMasterCtrl_FCI_Indication(&RteInData_Common.DiagInfoDLFW);
               //! #### Check for IsFciFaultActive value
               if (CONST_Fault_Active == IsFciFaultActive)
               {
                  //! #### Process fallback mode logic: 'DiffLckPanel_LINMastCtrl_FallbackLogic()'
                  DiffLckPanel_LINMastCtrl_FallbackLogic(&RteOutData_Common.DifflockDeactivationBtn_st,
                                                         &RteOutData_Common.DifflockMode_Wheelstatus,
                                                         &RteOutData_Common.EscButtonMuddySiteStatus,
                                                         &RteOutData_Common.Offroad_ButtonStatus);
               }
               else
               {
                  //! #### Process the signals gateway logic: 'DiffLockPanel_LINMasterCtrl_Signals_GatewayLogic()'
               DiffLockPanel_LINMasterCtrl_Signals_GatewayLogic(&RteInData_Common,
                                                                &RteOutData_Common);
               }
            }
            else
            {
               //! ##### Process fallback mode logic: 'DiffLckPanel_LINMastCtrl_FallbackLogic()'
               DiffLckPanel_LINMastCtrl_FallbackLogic(&RteOutData_Common.DifflockDeactivationBtn_st,
                                                      &RteOutData_Common.DifflockMode_Wheelstatus,
                                                      &RteOutData_Common.EscButtonMuddySiteStatus,
                                                      &RteOutData_Common.Offroad_ButtonStatus);
            }
         }
         else
         {
            //! ##### Process deactivation logic: 'DiffLockPanel_LINMasterCtrl_DeactivateLogic()'
          DiffLockPanel_LINMasterCtrl_DeactivateLogic(&RteOutData_Common);
         }
      }
      else
      {
         //! ###### Process deactivation logic: 'DiffLockPanel_LINMasterCtrl_DeactivateLogic()'
         DiffLockPanel_LINMasterCtrl_DeactivateLogic(&RteOutData_Common);
      }
   }
   if ((CONST_Tester_Present == ((Rte_IrvRead_DiffLockPanel_LINMasterCtrl_20ms_runnable_Irv_IOCTL_DiffLockLinCtrl())& CONST_Tester_Present))
      && ((uint8)Diag_Active_TRUE == RteInData_Common.isDiagActive))
   {
      //! ###### Process input output control service logic : 'IOCtrlService_LinOutputSignalControl()'
      IOCtrlService_LinOutputSignalControl((uint8)((Rte_IrvRead_DiffLockPanel_LINMasterCtrl_20ms_runnable_Irv_IOCTL_DiffLockLinCtrl())& CONST_Tester_Notpresent),
                                           &RteOutData_Common);
   }
   else
   {
      Rte_IrvWrite_DiffLockPanel_LINMasterCtrl_20ms_runnable_Irv_IOCTL_DiffLockLinCtrl(DeviceIndication_SpareValue);
   }
   //! ###### Process the RTE write common logic: 'RteDataWrite_Common()'
   RteDataWrite_Common(&RteOutData_Common);
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

#define DiffLockPanel_LINMasterCtrl_STOP_SEC_CODE
#include "DiffLockPanel_LINMasterCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'IOCtrlService_LinOutputSignalControl'
//!
//! \param     OverrideData    Provide the override data value
//! \param     *pOutData       Update the output data structure for DiffLockPanel_LINMasterCtrl_Signals_GatewayLogic
//!
//!======================================================================================
static void IOCtrlService_LinOutputSignalControl(const uint8                                       OverrideData,
                                                       DiffLockPanel_LINMasterCtrl_Out_StructType  *pOutData)
{
   //! ###### Override Lin output signals with IOControl service value
   pOutData->LIN_DifflockOnOff_Indication   = OverrideData;
   pOutData->LIN_EscButtonMuddySiteDeviceIn = OverrideData;
   pOutData->LIN_Offroad_Indication         = OverrideData;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'RteDataWrite_Common'
//!
//! \param   *pRteOutData_Common   Update the output signals to ports of output structure
//!
//!======================================================================================
static void RteDataWrite_Common(const DiffLockPanel_LINMasterCtrl_Out_StructType  *pRteOutData_Common)
{
   //! ###### Write all the output ports
   Rte_Write_DifflockDeactivationBtn_stat_PushButtonStatus(pRteOutData_Common->DifflockDeactivationBtn_st);
   Rte_Write_DifflockMode_Wheelstatus_FreeWheel_Status(pRteOutData_Common->DifflockMode_Wheelstatus);
   Rte_Write_EscButtonMuddySiteStatus_PushButtonStatus(pRteOutData_Common->EscButtonMuddySiteStatus);
   Rte_Write_Offroad_ButtonStatus_PushButtonStatus(pRteOutData_Common->Offroad_ButtonStatus);
   Rte_Write_LIN_DifflockOnOff_Indication_DeviceIndication(pRteOutData_Common->LIN_DifflockOnOff_Indication);
   Rte_Write_LIN_EscButtonMuddySiteDeviceIn_DeviceIndication(pRteOutData_Common->LIN_EscButtonMuddySiteDeviceIn);
   Rte_Write_LIN_Offroad_Indication_DeviceIndication(pRteOutData_Common->LIN_Offroad_Indication);
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Get_RteDataRead_Common'
//!
//! \param    *pRteInData_Common   Examine and update the input signals based on RTE failure events
//!
//! \return   Std_ReturnType       Returns 'retValueDiffLock' signal RTE read error status
//!
//!======================================================================================
static Std_ReturnType Get_RteDataRead_Common(DiffLockPanel_LINMasterCtrl_In_StructType  *pRteInData_Common)
{
   //! ###### Processing RTE data read common logic
   Std_ReturnType retValue         = RTE_E_INVALID;
   Std_ReturnType retValueDiffLock = RTE_E_INVALID;

   //! ##### Read isDiagActive interface
   retValue = Rte_Read_DiagActiveState_isDiagActive(&pRteInData_Common->isDiagActive);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->isDiagActive), (Diag_Active_FALSE))
   //! ##### Read LIN_DifflockDeactivationBtn_st interface
   retValue = Rte_Read_LIN_DifflockDeactivationBtn_st_PushButtonStatus(&pRteInData_Common->LIN_DifflockDeactivationBtn_st);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->LIN_DifflockDeactivationBtn_st), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))
   //! ##### Read LIN_EscButtonMuddySiteStatus interface
   retValue = Rte_Read_LIN_EscButtonMuddySiteStatus_PushButtonStatus(&pRteInData_Common->LIN_EscButtonMuddySiteStatus);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->LIN_EscButtonMuddySiteStatus),( PushButtonStatus_NotAvailable), (PushButtonStatus_Error))
   //! ##### Read LIN_Offroad_ButtonStatus interface
   retValue = Rte_Read_LIN_Offroad_ButtonStatus_PushButtonStatus(&pRteInData_Common->LIN_Offroad_ButtonStatus);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->LIN_Offroad_ButtonStatus), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))
   //! ##### Read LIN_DifflockMode_Wheelstatus interface
   retValue = Rte_Read_LIN_DifflockMode_Wheelstatus_FreeWheel_Status(&pRteInData_Common->LIN_DifflockMode_Wheelstatus);
   MACRO_StdRteRead_ExtRPort((retValue),( pRteInData_Common->LIN_DifflockMode_Wheelstatus), (FreeWheel_Status_NotAvailable), (FreeWheel_Status_Error))
   //! ##### Read DifflockOnOff_Indication interface
   retValue = Rte_Read_DifflockOnOff_Indication_DeviceIndication(&pRteInData_Common->DifflockOnOff_Indication);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->DifflockOnOff_Indication),( DeviceIndication_Off))
   //! ##### Read EscButtonMuddySiteDeviceInd interface
   retValue = Rte_Read_EscButtonMuddySiteDeviceInd_DeviceIndication(&pRteInData_Common->EscButtonMuddySiteDeviceInd);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->EscButtonMuddySiteDeviceInd),( DeviceIndication_Off))
   //! ##### Read Offroad_Indication interface
   retValue = Rte_Read_Offroad_Indication_DeviceIndication(&pRteInData_Common->Offroad_Indication);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->Offroad_Indication),( DeviceIndication_Off))
   //! ##### Read ComMode_LIN4 interface
   retValue = Rte_Read_ComMode_LIN4_ComMode_LIN(&pRteInData_Common->ComMode_LIN4);
   MACRO_StdRteRead_IntRPort((retValue),( pRteInData_Common->ComMode_LIN4),( Error))
   //! ##### Read DiagInfoDLFW interface
   retValue = Rte_Read_DiagInfoDLFW_DiagInfo(&pRteInData_Common->DiagInfoDLFW);
   MACRO_StdRteRead_ExtRPort((retValue),( pRteInData_Common->DiagInfoDLFW),( 0U),( 0U))
   //! ##### Read DLFW_ResponseError interface
   retValueDiffLock = Rte_Read_ResponseErrorDLFW_ResponseErrorDLFW(&pRteInData_Common->DLFW_ResponseError);
   MACRO_StdRteRead_ExtRPort((retValueDiffLock),( pRteInData_Common->DLFW_ResponseError), (1U), (1U))
   return retValueDiffLock;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the  'DiffLockPanel_LINMasterCtrl_Signals_GatewayLogic'
//!
//! \param   *pInData    Providing the input signals to send to output ports
//! \param   *pOutData   Updating the output ports with input signals
//!
//!======================================================================================
static void DiffLockPanel_LINMasterCtrl_Signals_GatewayLogic(const DiffLockPanel_LINMasterCtrl_In_StructType   *pInData,
                                                                   DiffLockPanel_LINMasterCtrl_Out_StructType  *pOutData)
{
   //! ###### Update the ouputs with values read on input ports
   // Signals gateway, HMI status on DFLW 
   pOutData->DifflockMode_Wheelstatus       = pInData->LIN_DifflockMode_Wheelstatus;
   pOutData->DifflockDeactivationBtn_st     = pInData->LIN_DifflockDeactivationBtn_st;
   pOutData->Offroad_ButtonStatus           = pInData->LIN_Offroad_ButtonStatus;
   pOutData->EscButtonMuddySiteStatus       = pInData->LIN_EscButtonMuddySiteStatus;
   // Signals gateway, inidication status on DFLW
   pOutData->LIN_DifflockOnOff_Indication   = pInData->DifflockOnOff_Indication;
   pOutData->LIN_EscButtonMuddySiteDeviceIn = pInData->EscButtonMuddySiteDeviceInd;
   pOutData->LIN_Offroad_Indication         = pInData->Offroad_Indication;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'DiffLckPanel_LINMastCtrl_FallbackLogic'
//!
//! \param   *pDifflockDeactivationBtn_st   Update the DifflockDeactivation button status to error
//! \param   *pDifflockMode_Wheelstatus     Update the DifflockMode wheel status to error
//! \param   *pEscButtonMuddySiteStatus     Update the EscButtonMuddySite status to error
//! \param   *pOffroad_ButtonStatus         Update the Offroad button status to error
//!
//!======================================================================================
static void DiffLckPanel_LINMastCtrl_FallbackLogic(PushButtonStatus_T  *pDifflockDeactivationBtn_st,
                                                   FreeWheel_Status_T  *pDifflockMode_Wheelstatus,
                                                   PushButtonStatus_T  *pEscButtonMuddySiteStatus,
                                                   PushButtonStatus_T  *pOffroad_ButtonStatus)
{
   //! ###### Write all the output ports to value 'error'
   *pDifflockDeactivationBtn_st = PushButtonStatus_Error;
   *pDifflockMode_Wheelstatus   = FreeWheel_Status_Error;
   *pEscButtonMuddySiteStatus   = PushButtonStatus_Error;
   *pOffroad_ButtonStatus       = PushButtonStatus_Error;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'DiffLockPanel_LINMasterCtrl_DeactivateLogic'
//!
//! \param   *pOutData   Update the ports of output structure to off or not available
//!
//!======================================================================================
static void DiffLockPanel_LINMasterCtrl_DeactivateLogic(DiffLockPanel_LINMasterCtrl_Out_StructType  *pOutData)
{
   //! ###### Write all the output ports to value 'not available' or 'off'
   pOutData->DifflockDeactivationBtn_st     = PushButtonStatus_NotAvailable;
   pOutData->DifflockMode_Wheelstatus       = FreeWheel_Status_NotAvailable;
   pOutData->EscButtonMuddySiteStatus       = PushButtonStatus_NotAvailable;
   pOutData->Offroad_ButtonStatus           = PushButtonStatus_NotAvailable;
   pOutData->LIN_DifflockOnOff_Indication   = DeviceIndication_Off;
   pOutData->LIN_EscButtonMuddySiteDeviceIn = DeviceIndication_Off;
   pOutData->LIN_Offroad_Indication         = DeviceIndication_Off;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'DiffLockPanel_LINMasterCtrl_FCI_Indication'
//!
//! \param   *pDiagInfoDLFW   Provides the DiagInfoDLFW value
//!
//! \return   boolean    Returns 'IsFciFaultActive' value
//!
//!======================================================================================
static boolean DiffLockPanel_LINMasterCtrl_FCI_Indication(const DiagInfo_T  *pDiagInfoDLFW)
{

   static uint8 SlaveFCIFault[4] = { CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive };
   boolean IsFciFaultActive      = CONST_Fault_InActive;
   uint8 Index                   = 0U;

   //! ###### Check the FCI conditions and log or remove the faults
   if ((DiagInfo_T)0x41 == *pDiagInfoDLFW)
   {
      //! ##### Set supply voltage too low fault event to fail
      Rte_Call_Event_D1BN2_16_DLFW_VBT_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x01 == *pDiagInfoDLFW)
   {
      //! ##### Set supply voltage too low fault event to pass
      Rte_Call_Event_D1BN2_16_DLFW_VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x42 == *pDiagInfoDLFW)
   {
      //! ##### Set supply voltage too high fault event to fail
      Rte_Call_Event_D1BN2_17_DLFW_VAT_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x02 == *pDiagInfoDLFW)
   {
      //! ##### Set supply voltage too high fault event to pass
      Rte_Call_Event_D1BN2_17_DLFW_VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x43 == *pDiagInfoDLFW)
   {
      //! ##### Set ROM CS error fault event to fail
      Rte_Call_Event_D1BN2_44_DLFW_RAM_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      SlaveFCIFault[0] = CONST_SlaveFCIFault_Active;
   }
   else if ((DiagInfo_T)0x03 == *pDiagInfoDLFW)
   {
      //! ##### Set ROM CS error fault event to pass
      Rte_Call_Event_D1BN2_44_DLFW_RAM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      SlaveFCIFault[0] = CONST_SlaveFCIFault_InActive;
   }
   else if ((DiagInfo_T)0x44 == *pDiagInfoDLFW)
   {
      //! ##### Set RAM check fault event to fail
      Rte_Call_Event_D1BN2_45_DLFW_FLASH_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      SlaveFCIFault[1] = CONST_SlaveFCIFault_Active;
   }
   else if ((DiagInfo_T)0x04 == *pDiagInfoDLFW)
   {
      //! ##### Set RAM check fault event to pass
      Rte_Call_Event_D1BN2_45_DLFW_FLASH_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      SlaveFCIFault[1] = CONST_SlaveFCIFault_InActive;
   }
   else if ((DiagInfo_T)0x45 == *pDiagInfoDLFW)
   {
      //! ##### Set watchdog error fault event to fail
      Rte_Call_Event_D1BN2_46_DLFW_EEPROM_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      SlaveFCIFault[2] = CONST_SlaveFCIFault_Active;
   }
   else if ((DiagInfo_T)0x05 == *pDiagInfoDLFW)
   {
      //! ##### Set watchdog error fault event to pass
      Rte_Call_Event_D1BN2_46_DLFW_EEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      SlaveFCIFault[2] = CONST_SlaveFCIFault_InActive;
   }
   else if ((DiagInfo_T)0x46 == *pDiagInfoDLFW)
   {
      //! ##### Set overrun fault event to fail
      Rte_Call_Event_D1BN2_94_DLFW_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      SlaveFCIFault[3] = CONST_SlaveFCIFault_Active;
   }
   else if ((DiagInfo_T)0x06 == *pDiagInfoDLFW)
   {
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1BN2_94_DLFW_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      SlaveFCIFault[3] = CONST_SlaveFCIFault_InActive;
   }
   else if ((DiagInfo_T)0x00 == *pDiagInfoDLFW)
   {
      for(Index = 0U;Index < 4U;Index++)
      {
         SlaveFCIFault[Index] = CONST_SlaveFCIFault_InActive;
      }
      Rte_Call_Event_D1BN2_16_DLFW_VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BN2_17_DLFW_VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BN2_44_DLFW_RAM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BN2_45_DLFW_FLASH_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BN2_46_DLFW_EEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BN2_94_DLFW_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else
   {
      // Do nothing, keep previous status
   }
   IsFciFaultActive = CONST_Fault_InActive;
   for(Index = 0U;Index < 4U;Index++)
   {
      if (CONST_SlaveFCIFault_Active == SlaveFCIFault[Index])
      {
         IsFciFaultActive = CONST_Fault_Active;
      }
      else
      {
         // Do Nothing
      }
   }
   return IsFciFaultActive;
}

//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/



#if 0
/***  Start of saved code (symbol: documentation area:DiffLockPnale_LINMasterCtrl_20m_runnable_doc)  ********/


/***  End of saved code  ************************************************************************************/
#endif

#if 0
/***  Start of saved code (symbol: runnable implementation:DiffLockPnale_LINMasterCtrl_20m_runnable)  *******/


/***  End of saved code  ************************************************************************************/
#endif

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
