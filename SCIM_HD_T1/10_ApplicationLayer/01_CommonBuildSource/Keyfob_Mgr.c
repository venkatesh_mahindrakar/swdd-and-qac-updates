/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Keyfob_Mgr.c
 *           Config:  C:/GIT/scim_bsp_hd_t1_peps/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  Keyfob_Mgr
 *  Generation Time:  2020-11-03 12:57:43
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <Keyfob_Mgr>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   Boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_OpStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_CrankingLockActivation_P1DS3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_KeyMatchingIndicationTimeout_X1CV3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_KeyfobEncryptCode_P1DS4_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_Keyfob_Mgr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "Keyfob_Mgr.h"
#include "FuncLibrary_Timer_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#define PCODE_KeyMatchingReinforcedAuth       (Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v())


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_CrankingLockActivation_P1DS3_T: Integer in interval [0...255]
 * SEWS_KeyMatchingIndicationTimeout_X1CV3_T: Integer in interval [0...255]
 * SEWS_KeyfobEncryptCode_P1DS4_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * DriverAuthDeviceMatching_T: Enumeration of integer in interval [0...3] with enumerators
 *   DriverAuthDeviceMatching_Idle (0U)
 *   DriverAuthDeviceMatching_DeviceToMatchIsPresent (1U)
 *   DriverAuthDeviceMatching_Error (2U)
 *   DriverAuthDeviceMatching_NotAvailable (3U)
 * KeyfobAuth_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobAuth_rqst_Idle (0U)
 *   KeyfobAuth_rqst_RequestByPassiveMechanism (1U)
 *   KeyfobAuth_rqst_RequestByImmobilizerMechanism (2U)
 *   KeyfobAuth_rqst_Spare1 (3U)
 *   KeyfobAuth_rqst_Spare2 (4U)
 *   KeyfobAuth_rqst_Spare3 (5U)
 *   KeyfobAuth_rqst_Error (6U)
 *   KeyfobAuth_rqst_NotAavailable (7U)
 * KeyfobAuth_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobAuth_stat_Idle (0U)
 *   KeyfobAuth_stat_NokeyfobAuthenticated (1U)
 *   KeyfobAuth_stat_KeyfobAuthenticated (2U)
 *   KeyfobAuth_stat_Spare1 (3U)
 *   KeyfobAuth_stat_Spare2 (4U)
 *   KeyfobAuth_stat_Spare3 (5U)
 *   KeyfobAuth_stat_Error (6U)
 *   KeyfobAuth_stat_NotAvailable (7U)
 * KeyfobInCabLocation_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobInCabLocation_stat_Idle (0U)
 *   KeyfobInCabLocation_stat_NotDetected_Incab (1U)
 *   KeyfobInCabLocation_stat_Detected_Incab (2U)
 *   KeyfobInCabLocation_stat_Spare1 (3U)
 *   KeyfobInCabLocation_stat_Spare2 (4U)
 *   KeyfobInCabLocation_stat_Spare3 (5U)
 *   KeyfobInCabLocation_stat_Error (6U)
 *   KeyfobInCabLocation_stat_NotAvailable (7U)
 * KeyfobInCabPresencePS_T: Enumeration of integer in interval [0...3] with enumerators
 *   KeyfobInCabPresencePS_rqst_NoRequest (0U)
 *   KeyfobInCabPresencePS_rqst_RequestDetectionInCab (1U)
 *   KeyfobInCabPresencePS_rqst_Error (2U)
 *   KeyfobInCabPresencePS_rqst_NotAvailable (3U)
 * KeyfobLocation_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 *   KeyfobLocation_rqst_NoRequest (0U)
 *   KeyfobLocation_rqst_Request (1U)
 *   KeyfobLocation_rqst_Error (2U)
 *   KeyfobLocation_rqst_NotAvailable (3U)
 * KeyfobOutsideLocation_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobOutsideLocation_stat_Idle (0U)
 *   KeyfobOutsideLocation_stat_NotDetectedOutside (1U)
 *   KeyfobOutsideLocation_stat_DetectedOnRightSide (2U)
 *   KeyfobOutsideLocation_stat_DetectedOnLeftSide (3U)
 *   KeyfobOutsideLocation_stat_DetectedOnBothSides (4U)
 *   KeyfobOutsideLocation_stat_DetectedOutOfDoorAreas (5U)
 *   KeyfobOutsideLocation_stat_Error (6U)
 *   KeyfobOutsideLocation_stat_NotAvailable (7U)
 * SCIM_ImmoDriver_ProcessingStatus_T: Enumeration of integer in interval [0...255] with enumerators
 *   ImmoDriver_ProcessingStatus_Idle (0U)
 *   ImmoDriver_ProcessingStatus_Ongoing (1U)
 *   ImmoDriver_ProcessingStatus_LfSent (2U)
 *   ImmoDriver_ProcessingStatus_DecryptedInList (3U)
 *   ImmoDriver_ProcessingStatus_DecryptedNotInList (4U)
 *   ImmoDriver_ProcessingStatus_NotDecrypted (5U)
 *   ImmoDriver_ProcessingStatus_HwError (6U)
 * SCIM_ImmoType_T: Enumeration of integer in interval [0...255] with enumerators
 *   ImmoType_ATA5702 (0U)
 *   ImmoType_ATA5577 (1U)
 * SCIM_PassiveDriver_ProcessingStatus_T: Enumeration of integer in interval [0...255] with enumerators
 *   PassiveDriver_ProcessingStatus_Idle (0U)
 *   PassiveDriver_ProcessingStatus_Ongoing (1U)
 *   PassiveDriver_ProcessingStatus_LfSent (2U)
 *   PassiveDriver_ProcessingStatus_Detected (3U)
 *   PassiveDriver_ProcessingStatus_NotDetected (4U)
 *   PassiveDriver_ProcessingStatus_HwError (5U)
 * SCIM_PassiveSearchCoverage_T: Enumeration of integer in interval [0...255] with enumerators
 *   PassiveSearchCoverage_PassiveStart2Ant (0U)
 *   PassiveSearchCoverage_PassiveStart1Ant (1U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data256ByteType: Array with 256 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 * Dcm_Data3ByteType: Array with 3 element(s) of type uint8
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 * SEWS_KeyfobEncryptCode_P1DS4_a_T: Array with 24 element(s) of type SEWS_KeyfobEncryptCode_P1DS4_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_KeyMatchingIndicationTimeout_X1CV3_T Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v(void)
 *   boolean Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v(void)
 *   boolean Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v(void)
 *   boolean Rte_Prm_P1B2U_KeyfobPresent_v(void)
 *   SEWS_KeyfobEncryptCode_P1DS4_T *Rte_Prm_P1DS4_KeyfobEncryptCode_v(void)
 *     Returnvalue: SEWS_KeyfobEncryptCode_P1DS4_T* is of type SEWS_KeyfobEncryptCode_P1DS4_a_T
 *   SEWS_CrankingLockActivation_P1DS3_T Rte_Prm_P1DS3_CrankingLockActivation_v(void)
 *   boolean Rte_Prm_P1C54_FactoryModeActive_v(void)
 *
 *********************************************************************************************************************/


#define Keyfob_Mgr_START_SEC_CODE
#include "Keyfob_Mgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchedKeyfobCount(void)
 *   uint8 Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchingStatus(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1B0T_Data_P1B0T_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_doc
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8 P1B0T_MatchedKeyfobCount=0u;
   uint8 P1B0T_MatchingStatus=0u;
   Rte_Call_KeyfobMatchingOperations_GetMatchingStatus(&P1B0T_MatchingStatus, &P1B0T_MatchedKeyfobCount);

   P1B0T_MatchingStatus = Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchingStatus();

   *Data = ((P1B0T_MatchingStatus & 0x07) << 4) | (P1B0T_MatchedKeyfobCount & 0x0F);

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1DS3_Data_P1DS3_CrankingLockActivation>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1DS3_Data_P1DS3_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1FDL_Data_P1FDL_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1FM6_Data_P1FM6_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data256ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1KAO_Data_P1KAO_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <WriteData> of PortPrototype <DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data256ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1KAO_Data_P1KAO_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData(P2CONST(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VKK_Data_P1VKK_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Keyfob_Mgr_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_DriverAuthDeviceMatching_DriverAuthDeviceMatching(DriverAuthDeviceMatching_T *data)
 *   Std_ReturnType Rte_Read_KeyfobAuth_rqst_KeyfobAuth_rqst(KeyfobAuth_rqst_T *data)
 *   Std_ReturnType Rte_Read_KeyfobInCabPresencePS_rqst_KeyfobInCabPresencePS_rqst(KeyfobInCabPresencePS_T *data)
 *   Std_ReturnType Rte_Read_KeyfobLocation_rqst_KeyfobLocation_rqst(KeyfobLocation_rqst_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_AddrParP1DS4_stat_dataP1DS4(const SEWS_KeyfobEncryptCode_P1DS4_T *data)
 *     Argument data: SEWS_KeyfobEncryptCode_P1DS4_T* is of type SEWS_KeyfobEncryptCode_P1DS4_a_T
 *   Std_ReturnType Rte_Write_KeyfobAuth_stat_KeyfobAuth_stat(KeyfobAuth_stat_T data)
 *   Std_ReturnType Rte_Write_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat(KeyfobInCabLocation_stat_T data)
 *   Std_ReturnType Rte_Write_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle(KeyfobOutsideLocation_stat_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF(void)
 *   uint8 Rte_IrvRead_Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount(void)
 *   uint8 Rte_IrvRead_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF(uint8 data)
 *   void Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount(uint8 data)
 *   void Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus(uint8 data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_ImmoProcessingRqst_GetImmoCircuitProcessingResult(SCIM_ImmoDriver_ProcessingStatus_T *ImmoProcessingStatus)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_ImmoProcessingRqst_ImmoCircuitProcessing(SCIM_ImmoType_T ImmoType, Boolean AuthRequestedbyImmo)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_KeyfobMatchingOperations_GetMatchingStatus(uint8 *matchingStatus, uint8 *matchedKeyfobCount)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_KeyfobMatchingOperations_MatchKeyfobByLF(void)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_KeyfobMatchingOperations_ReinitializeMatchingList(void)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult(SCIM_PassiveDriver_ProcessingStatus_T *ProcessingStatus, KeyfobInCabLocation_stat_T *KeyfobLocationbyPassive_Incab, KeyfobOutsideLocation_stat_T *KeyfobLocationbyPassive_Outcab)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_KeyfobPassiveSearchRqst_KeyfobPassiveSearch(SCIM_PassiveSearchCoverage_T PassiveSearchCoverage, boolean SearchRequestedbyPassive)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Keyfob_Mgr_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Keyfob_Mgr_CODE) Keyfob_Mgr_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Keyfob_Mgr_20ms_runnable
 *********************************************************************************************************************/
   tVAR *PassiveSearchRequested;
   static uint16 Timers[CONST_NbOfTimer] = { 1000/CONST_Keyfob_Mgr_Func_Period };
   static SM_KeyfobOperational KeyfobOperational_SM = SM_KeyfobOperational_Functional;

   static Keyfob_Mgr_Logic_StructType     KFMgr_logic_data;

   static SM_PEPSProcessStates                     PEPS_State;
   static SM_ImmoProcessStates                     Immo_state;
   static SM_PEPSModeStates                        PEPSModeStates;

   static uint8                           IsSetInitialValueKeyfobRegisteredCount = 0u;
   boolean IsLfProcessingIdle = FALSE;
   boolean isDiagActive = FALSE;

   //! ###### Definition of the SWC In/Out data structures
   // Define input RteInData Common
   static Keyfob_Mgr_in_StructType        RteInData_Common;
   // Define input RteOutData Common
   static Keyfob_Mgr_out_StructType       RteOutData_Common;

   uint8 i;
   uint8 matchingStatus;
   uint8 matchedKeyfobCount;
   uint8 p1ds4[24] = {0};
   
   //! ##### Read the logic common RTE ports and process fallback modes for RTE events: 'RteInDataRead_Common'
   RteDataRead_Common(&RteInData_Common);
   Rte_Read_DiagActiveState_isDiagActive(&isDiagActive);
   
   for (i=0u; i<24u; i++)
   {
      p1ds4[i] = Rte_AddrPar_0x2B_P1DS4_KeyfobEncryptCode_v[i];
   }
   //! ##### Check if Diagnostic mode is on

   TimerFunction_Tick(CONST_NbOfTimer,Timers);

   switch(KeyfobOperational_SM)
   {
      case SM_KeyfobOperational_LFmatching:
         if ((CONST_Routine_R1AAA_isInactive == Rte_IrvRead_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF())
            || (CONST_TimerFunctionElapsed == Timers[CONST_MatchingRoutineTimer])
            || (FALSE == isDiagActive))
         {            
            Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF(CONST_Routine_R1AAA_isInactive);
            KeyfobOperational_SM = SM_KeyfobOperational_Functional;
         }
         else
         {
            // do nothing: keep current state machine state
         }
         
         Keyfob_Mgr_Logic_LFmatching(&RteInData_Common, &RteOutData_Common, Timers, &KeyfobOperational_SM);  
      break;
      case SM_KeyfobOperational_RFmatching:
         //! ##### Function Keyfob_Mgr_Logic_RFmatching()
         //Keyfob_Mgr_Logic_RFmatching(&RteInData_Common, &RteOutData_Common,Timers);
      break;

      default: // SM_KeyfobOperational_Functional
         Keyfob_Mgr_Logic_PassiveStart(&RteInData_Common, &RteOutData_Common, Timers, &IsLfProcessingIdle);
         if (CONST_Routine_R1AAA_isActive == Rte_IrvRead_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF())
         {
            //Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF(CONST_Routine_R1AAA_Started);
            //Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus(CONST_WaitingKeyPresentation);
            if (TRUE == IsLfProcessingIdle)
            {
               KeyfobOperational_SM = SM_KeyfobOperational_LFmatching;
               Timers[CONST_MatchingRoutineTimer] = CONST_ImmoMatchingRoutingTimeout;
               
               if (0 == PCODE_KeyMatchingReinforcedAuth)
               {
                  Rte_Call_KeyfobMatchingOperations_ReinitializeMatchingList();
               }
               else
               {
                  // do nothing: keep keyfob values
               }
            }
            else
            {
               // do nothing, wait the Lf operation is completed before to switch to matching mode
            }
         }
         else
         {
            //! ##### Function Keyfob_Mgr_Logic_PassiveStart()
            
         }
      break;
   }
   
   //! ##### Write the logic common RTE ports and process fallback modes for RTE events: 'RteInDataRead_Common'
   RteDataWrite_Common(&RteOutData_Common);

   Rte_Write_AddrParP1DS4_stat_dataP1DS4(Rte_Prm_P1DS4_KeyfobEncryptCode_v());

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_R1AAA_KeyfobMatchingRoutineByLF>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAA_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAA_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAA_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults (returns application error)
 *********************************************************************************************************************/
   uint8 retErrorCode = RTE_E_OK;

   if (CONST_Routine_R1AAA_isActive == Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF())
   {
      Out_Common_Diagnostics_DataRecord[0] = 0x03;
      Out_Common_Diagnostics_DataRecord[1] = 0x00; // Routine is active

      *ErrorCode = 0x21;
      retErrorCode = RTE_E_RoutineServices_R1AAA_E_NOT_OK;
   }
   else
   {
      // REQ-UDS2-175
      Out_Common_Diagnostics_DataRecord[0] = 0x03;
      Out_Common_Diagnostics_DataRecord[1] = 0x00; // Routine is inactive
      
      *ErrorCode = 0x00;
      retErrorCode = RTE_E_OK;
   }
  
   return retErrorCode;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_R1AAA_KeyfobMatchingRoutineByLF>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAA_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAA_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAA_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start (returns application error)
 *********************************************************************************************************************/
   uint8 retErrorCode = RTE_E_OK;

   if (CONST_Routine_R1AAA_isInactive == Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF())
   {
      Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF(CONST_Routine_R1AAA_isActive);

      Out_Common_Diagnostics_DataRecord[0] = 0x03;
      Out_Common_Diagnostics_DataRecord[1] = 0x00; // Routine stared as expected

      *ErrorCode = 0x00;
      retErrorCode = RTE_E_OK;
   }
   else
   {
      // REQ-UDS2-175
      Out_Common_Diagnostics_DataRecord[0] = 0x03;
      Out_Common_Diagnostics_DataRecord[1] = 0x24; // Execution in progress : already started
      *ErrorCode = 0x24;

      retErrorCode = RTE_E_RoutineServices_R1AAA_E_NOT_OK;
   }

   return retErrorCode;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_R1AAA_KeyfobMatchingRoutineByLF>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAA_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAA_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAA_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop (returns application error)
 *********************************************************************************************************************/
   uint8 retErrorCode = RTE_E_OK;

   if (CONST_Routine_R1AAA_isActive == Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF())
   {
      Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF(CONST_Routine_R1AAA_isInactive);

      *ErrorCode = 0x00;
      retErrorCode = RTE_E_OK;
   }
   else
   {
      // REQ-UDS2-175
      *ErrorCode = 0x24;
   }

   return retErrorCode;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Y1ABD_ClearECU_Start
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Y1ABD_ClearECU>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_RoutineServices_Y1ABD_ClearECU_Start_FlagKeyfobMatchingByLF(void)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_KeyfobMatchingOperations_ReinitializeMatchingList(void)
 *     Synchronous Server Invocation. Timeout: None
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Y1ABD_ClearECU_Start(const uint8 *In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument In_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data4ByteType
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Y1ABD_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Y1ABD_DCM_E_PENDING
 *   RTE_E_RoutineServices_Y1ABD_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Y1ABD_ClearECU_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_Y1ABD_ClearECU_Start(P2CONST(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_DATA) In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Y1ABD_ClearECU_Start (returns application error)
 *********************************************************************************************************************/
   
   uint8 retErrorCode = RTE_E_OK;

   if (CONST_Routine_R1AAA_isInactive == Rte_IrvRead_RoutineServices_Y1ABD_ClearECU_Start_FlagKeyfobMatchingByLF())
   {
      Rte_Call_KeyfobMatchingOperations_MatchKeyfobByLF();

      Out_Common_Diagnostics_DataRecord[0] = 0x03;
      Out_Common_Diagnostics_DataRecord[1] = 0x00; // Routine stared as expected

      *ErrorCode = 0x00;
      retErrorCode = RTE_E_OK;
   }
   else
   {
      // REQ-UDS2-175
      Out_Common_Diagnostics_DataRecord[0] = 0x03;
      Out_Common_Diagnostics_DataRecord[1] = 0x22; // Conditions not correct
      *ErrorCode = 0x24;

      retErrorCode = RTE_E_RoutineServices_R1AAA_E_NOT_OK;
   }

   return retErrorCode;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define Keyfob_Mgr_STOP_SEC_CODE
#include "Keyfob_Mgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
static void Keyfob_Mgr_Logic_PassiveStart(Keyfob_Mgr_in_StructType *RteInData_Common, 
                                          Keyfob_Mgr_out_StructType *RteOutData_Common,
                                          uint16 pTimers[CONST_NbOfTimer],
                                          boolean *pIsLfProcessingIdle)
{
   static uint8 LFAntDiagEnable = 1u; // todo : jinwoo - For LF antenna diagnostic trigger
   static uint8 Keyfob_Mgr_steps = CONST_Keyfob_Search_step_request;
   static uint8 PassiveSearchRetry = 3u;
   static uint8 ImmoCheckRetry = 3u;
   static boolean isBackUpModeActive = FALSE;
   static SM_PassiveProcess_Enum_Type PassiveProcess_SM = SM_PassiveProcess_Idle;
   static SCIM_ImmoDriver_ProcessingStatus_T immoDriverProcessingStatus = ImmoDriver_ProcessingStatus_Idle;
   SCIM_PassiveDriver_ProcessingStatus_T passiveDriverProcessingStatus = PassiveDriver_ProcessingStatus_Idle;
   KeyfobInCabLocation_stat_T keyfobInCabLocation = KeyfobInCabLocation_stat_Idle;
   KeyfobOutsideLocation_stat_T keyfobOutsideLocation = KeyfobOutsideLocation_stat_Idle;

   /** Passive keyfob search request */
   switch(PassiveProcess_SM.Current)
   {
      case SM_ImmoProcess_Ongoing:
         Rte_Call_ImmoProcessingRqst_GetImmoCircuitProcessingResult(&immoDriverProcessingStatus);
         if (ImmoDriver_ProcessingStatus_DecryptedInList == immoDriverProcessingStatus)
         {
            PassiveProcess_SM.New = SM_ImmoDecryptedInList;
         }
         else if (ImmoDriver_ProcessingStatus_DecryptedNotInList == immoDriverProcessingStatus)
         {
            PassiveProcess_SM.New = SM_ImmoNotValid;
         }
         else if (ImmoDriver_ProcessingStatus_NotDecrypted == immoDriverProcessingStatus)
         {
            PassiveProcess_SM.New = SM_ImmoNotValid;
         }
         else if (ImmoDriver_ProcessingStatus_HwError == immoDriverProcessingStatus)
         {
            PassiveProcess_SM.New = SM_ImmoNotValid;
         }
         else
         {
            // timeout 
            if (CONST_TimerFunctionElapsed == pTimers[CONST_PepsOperationTimer])
            {
               PassiveProcess_SM.New = SM_ImmoFail;
            }
            else
            {
               
            }
         }
      break;
      case SM_ImmoNotValid:
         Rte_Call_ImmoProcessingRqst_GetImmoCircuitProcessingResult(&immoDriverProcessingStatus);
         if (ImmoDriver_ProcessingStatus_Idle == immoDriverProcessingStatus)
         {
            if (0 < ImmoCheckRetry)
            {
               ImmoCheckRetry = ImmoCheckRetry - 1;
               PassiveProcess_SM.New = SM_ImmoProcess_Ongoing;
            }
            else
            {
               PassiveSearchRetry = 1;
               PassiveProcess_SM.New = SM_PassiveProcess_NotDetected;
            }
         }
         else
         {
            // do nothing: wait the PEPS driver is availables
         }
      break;
      case SM_PassiveProcess_Ongoing:
         Rte_Call_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult(&passiveDriverProcessingStatus, 
                                                                       &keyfobInCabLocation, 
                                                                       &keyfobOutsideLocation);
         if (PassiveDriver_ProcessingStatus_Detected == passiveDriverProcessingStatus)
         {
            if (KeyfobInCabLocation_stat_Detected_Incab == keyfobInCabLocation)
            {
               PassiveProcess_SM.New = SM_PassiveProcess_Detected;
            }
            else
            {
               PassiveProcess_SM.New = SM_PassiveProcess_NotDetected;
            }
         }
         else if (PassiveDriver_ProcessingStatus_NotDetected == passiveDriverProcessingStatus)
         {
            PassiveProcess_SM.New = SM_PassiveProcess_NotDetected;
         }
         else if (ImmoDriver_ProcessingStatus_HwError == passiveDriverProcessingStatus)
         {
            PassiveProcess_SM.New = SM_PassiveProcess_NotDetected_Fail;
         }
         else
         {
            // timeout 
            if (CONST_TimerFunctionElapsed == pTimers[CONST_PepsOperationTimer])
            {
               PassiveProcess_SM.New = SM_PassiveProcess_NotDetected_Fail;
            }
            else
            {
               
            }
         }
      break;
      case SM_PassiveProcess_NotDetected:
         Rte_Call_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult(&passiveDriverProcessingStatus, 
                                                                          &keyfobInCabLocation, 
                                                                          &keyfobOutsideLocation);
         if (PassiveDriver_ProcessingStatus_Idle == passiveDriverProcessingStatus)
         {
            if (0 < PassiveSearchRetry)
            {
               PassiveSearchRetry = PassiveSearchRetry - 1;
               PassiveProcess_SM.New = SM_PassiveProcess_Ongoing;
            }
            else
            {
               PassiveProcess_SM.New = SM_PassiveProcess_NotDetected_Fail;
            }
         }
         else
         {
            // do nothing: wait the PEPS driver is availables
         }
      break;
      case SM_ImmoFail:
         // explicit fall-through: same conditions as SM_PEPS_Idle
      case SM_PassiveProcess_NotDetected_Fail:
         // explicit fall-through: same conditions as SM_PEPS_Idle
      default: //SM_PEPS_Idle
         if (((RteInData_Common->KeyfobAuth_rqst.Current != RteInData_Common->KeyfobAuth_rqst.Previous)
               &&(KeyfobAuth_rqst_RequestByPassiveMechanism == RteInData_Common->KeyfobAuth_rqst.Current))
            ||((RteInData_Common->KeyfobInCabPresencePS_rqst.Current != RteInData_Common->KeyfobInCabPresencePS_rqst.Previous)
               && (KeyfobInCabPresencePS_rqst_RequestDetectionInCab == RteInData_Common->KeyfobInCabPresencePS_rqst.Current))
            ||((RteInData_Common->KeyfobLocation_rqst.Current != RteInData_Common->KeyfobLocation_rqst.Previous)
               && (KeyfobLocation_rqst_Request == RteInData_Common->KeyfobLocation_rqst.Current)))
         {
            if (TRUE == isBackUpModeActive)
            {
               PassiveProcess_SM.New = SM_ImmoProcess_Ongoing;
               ImmoCheckRetry = 1;
            }
            else
            {
               PassiveProcess_SM.New = SM_PassiveProcess_Ongoing;
               PassiveSearchRetry = 1;
            }
         }
         else 
         {
            // Do nothing
         }
      break;
   }

      
   // process new state
   if (PassiveProcess_SM.Current != PassiveProcess_SM.New)
   {
      PassiveProcess_SM.Current = PassiveProcess_SM.New;
      switch (PassiveProcess_SM.New)
      {
         case SM_PassiveProcess_NotDetected:
            Rte_Call_KeyfobPassiveSearchRqst_KeyfobPassiveSearch(PassiveSearchCoverage_PassiveStart2Ant, 
                                                                 FALSE);
         break;
         case SM_PassiveProcess_Ongoing:
            // request the passive search from PEPS driver
            pTimers[CONST_PepsOperationTimer] = 3000/CONST_Keyfob_Mgr_Func_Period;
            Rte_Call_KeyfobPassiveSearchRqst_KeyfobPassiveSearch(PassiveSearchCoverage_PassiveStart2Ant, 
                                                                 4);

            RteOutData_Common->KeyfobAuth_stat = KeyfobAuth_stat_Idle;
            RteOutData_Common->KeyfobInCabLocation_stat = KeyfobInCabLocation_stat_Idle;
            RteOutData_Common->KeyfobOutsideLocation_stat = KeyfobOutsideLocation_stat_Idle;
         break;
         case SM_PassiveProcess_Detected:
            Rte_Call_KeyfobPassiveSearchRqst_KeyfobPassiveSearch(PassiveSearchCoverage_PassiveStart2Ant, 
                                                                 FALSE);
         
            RteOutData_Common->KeyfobAuth_stat = KeyfobAuth_stat_KeyfobAuthenticated;
            RteOutData_Common->KeyfobInCabLocation_stat = KeyfobInCabLocation_stat_Detected_Incab;
            RteOutData_Common->KeyfobOutsideLocation_stat = KeyfobOutsideLocation_stat_Idle;
            
            isBackUpModeActive = FALSE;

            if (1u == LFAntDiagEnable)
            {
               Rte_Call_KeyfobPassiveSearchRqst_KeyfobPassiveSearch(PassiveSearchCoverage_PassiveStart2Ant, 
                                                                    0xFF); // LF antenna diag
            }
         break;
         case SM_PassiveProcess_NotDetected_Fail:
            Rte_Call_KeyfobPassiveSearchRqst_KeyfobPassiveSearch(PassiveSearchCoverage_PassiveStart2Ant, 
                                                                 FALSE);
                                                                 
            RteOutData_Common->KeyfobAuth_stat = KeyfobAuth_stat_NokeyfobAuthenticated;
            RteOutData_Common->KeyfobInCabLocation_stat = KeyfobInCabLocation_stat_NotDetected_Incab;
            RteOutData_Common->KeyfobOutsideLocation_stat = KeyfobOutsideLocation_stat_Idle;
            
            isBackUpModeActive = TRUE;
            
            if (1u == LFAntDiagEnable)
            {
               Rte_Call_KeyfobPassiveSearchRqst_KeyfobPassiveSearch(PassiveSearchCoverage_PassiveStart2Ant, 
                                                                    0xFF); // LF antenna diag
            }
         break;
         case SM_ImmoProcess_Ongoing:
            pTimers[CONST_PepsOperationTimer] = 3000/CONST_Keyfob_Mgr_Func_Period;
            Rte_Call_ImmoProcessingRqst_ImmoCircuitProcessing(ImmoType_ATA5702, TRUE);
            RteOutData_Common->KeyfobAuth_stat = KeyfobAuth_stat_Idle;
            RteOutData_Common->KeyfobInCabLocation_stat = KeyfobInCabLocation_stat_Idle;
            RteOutData_Common->KeyfobOutsideLocation_stat = KeyfobOutsideLocation_stat_Idle;
         break;
         case SM_ImmoNotValid:
            Rte_Call_ImmoProcessingRqst_ImmoCircuitProcessing(ImmoType_ATA5702, FALSE);
         break;
         case SM_ImmoFail:
            Rte_Call_ImmoProcessingRqst_ImmoCircuitProcessing(ImmoType_ATA5702, FALSE);
            RteOutData_Common->KeyfobAuth_stat = KeyfobAuth_stat_NokeyfobAuthenticated;
            RteOutData_Common->KeyfobInCabLocation_stat = KeyfobInCabLocation_stat_NotDetected_Incab;
            RteOutData_Common->KeyfobOutsideLocation_stat = KeyfobOutsideLocation_stat_Idle;
            if (1u == LFAntDiagEnable)
            {
               Rte_Call_KeyfobPassiveSearchRqst_KeyfobPassiveSearch(PassiveSearchCoverage_PassiveStart2Ant, 
                                                                    0xFF); // LF antenna diag
            }
         break;
         case SM_ImmoDecryptedInList:
            Rte_Call_ImmoProcessingRqst_ImmoCircuitProcessing(ImmoType_ATA5702, FALSE);
            RteOutData_Common->KeyfobAuth_stat = KeyfobAuth_stat_KeyfobAuthenticated;
            RteOutData_Common->KeyfobInCabLocation_stat = KeyfobInCabLocation_stat_Detected_Incab;
            RteOutData_Common->KeyfobOutsideLocation_stat = KeyfobOutsideLocation_stat_Idle;
            if (1u == LFAntDiagEnable)
            {
               Rte_Call_KeyfobPassiveSearchRqst_KeyfobPassiveSearch(PassiveSearchCoverage_PassiveStart2Ant, 
                                                                    0xFF); // LF antenna diag
            }
         break;
         default:
            // do nothing: no action
         break;
      }
   }
   
   // Notify if immobilizer or passive opreation is engaged
   if ((SM_ImmoProcess_Ongoing == PassiveProcess_SM.Current)
      || (SM_PassiveProcess_Ongoing == PassiveProcess_SM.Current))
   {
      *pIsLfProcessingIdle = FALSE;
   }
   else
   {
      *pIsLfProcessingIdle = TRUE;
   }
}






static void Keyfob_Mgr_Logic_LFmatching(Keyfob_Mgr_in_StructType *RteInData_Common, Keyfob_Mgr_out_StructType *RteOutData_Common, uint16 pTimers[CONST_NbOfTimer], const SM_KeyfobOperational *pKeyfobOperational_SM)
{
   static SM_LFmatching_Enum_Type LFmatching_SM = SM_LFmatching_Idle;
   //static uint8 FlagMatchingReinitializedAndMatched = 0u;
   //static uint8 FlagMatchingResultDisplayTimerElapsed = 0u;
   uint8 MatchingReport = Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchingStatus();
   SCIM_ImmoDriver_ProcessingStatus_T immoDriverProcessingStatus = ImmoDriver_ProcessingStatus_Idle;
   uint8 matchingStatus = 0x255u;
   uint8 matchedKeyfobCount = 0x255u;

   Rte_Call_KeyfobMatchingOperations_GetMatchingStatus(&matchingStatus, &matchedKeyfobCount);

   if (SM_KeyfobOperational_LFmatching == *pKeyfobOperational_SM)
   {
      switch(LFmatching_SM.Current)
      {         
         case SM_LFmatching_MatchingNotAuthorized:
            if ((DriverAuthDeviceMatching_Idle == RteInData_Common->DriverAuthDeviceMatching.Previous)
               && (DriverAuthDeviceMatching_DeviceToMatchIsPresent == RteInData_Common->DriverAuthDeviceMatching.Current))
            {
               LFmatching_SM.New = SM_LFmatching_MatchingAuthorizationCheck;
            }
            else
            {
               // wait keyfob presentation
            }
         break;
      
      case SM_LFmatching_MatchingAuthorizationCheck:
         // keyfob authentication check by LF
         Rte_Call_ImmoProcessingRqst_GetImmoCircuitProcessingResult(&immoDriverProcessingStatus);
         
         if (ImmoDriver_ProcessingStatus_DecryptedInList == immoDriverProcessingStatus)
         {
            LFmatching_SM.New = SM_LFmatching_MatchingAuthorized;
         }
         else if ((ImmoDriver_ProcessingStatus_DecryptedNotInList == immoDriverProcessingStatus)
                 || (ImmoDriver_ProcessingStatus_NotDecrypted == immoDriverProcessingStatus)
                 || (ImmoDriver_ProcessingStatus_HwError == immoDriverProcessingStatus)
                 || (CONST_TimerFunctionElapsed == pTimers[CONST_MatchingRoutineOperationTimer]))
         {
            if (0u == matchedKeyfobCount) // Case 4a
            {
               LFmatching_SM.New = SM_LFmatching_MatchingAuthorized;
            }
            else
            {
               LFmatching_SM.New = SM_LFmatching_MatchingNotAuthorized;
            }
         }
         else
         {
            // wait immobilizer processing or timeout elapsed
         }
      break;
      
      case SM_LFmatching_MatchingAuthorized:
         LFmatching_SM.New = SM_LFmatching_MatchingListReInitialization;
      break;
      
      case SM_LFmatching_MatchingListReInitialization:
         if (CONST_TimerFunctionElapsed == pTimers[CONST_MatchingRoutineOperationTimer])
         {
            LFmatching_SM.New = SM_LFmatching_MatchingKeyfob;
         }
         else
         {
            // do nothing: wait the timer elapsed
         }
      break;
      
      case SM_LFmatching_WaitingKeyfobPresentation:
         if ((DriverAuthDeviceMatching_Idle == RteInData_Common->DriverAuthDeviceMatching.Previous)
            && (DriverAuthDeviceMatching_DeviceToMatchIsPresent == RteInData_Common->DriverAuthDeviceMatching.Current))
         {
            LFmatching_SM.New = SM_LFmatching_MatchingKeyfob;
         }
      break;
      
      case SM_LFmatching_MatchingKeyfob:   
         if (CONST_P1B0T_KeyMatchedAndStored == matchingStatus)
         {
            LFmatching_SM.New = SM_LFmatching_MatchingSuccessful;
         }
         else if (CONST_P1B0T_NotIdentifiedKey == matchingStatus)
         {
            LFmatching_SM.New = SM_LFmatching_MatchingFail;
         }
         else if (CONST_P1B0T_AlreadyMatchedKey == matchingStatus)
         {
            LFmatching_SM.New = SM_LFmatching_MatchingFail;
         }
         else
         {
            // timeout 
            if (CONST_TimerFunctionElapsed == pTimers[CONST_MatchingRoutineOperationTimer])
            {
               LFmatching_SM.New = SM_LFmatching_MatchingFail;
            }
            else
            {
               // do nothing
            }
         }
      break;
      
      case SM_LFmatching_MatchingSuccessful:
         if ((CONST_TimerFunctionElapsed == pTimers[CONST_MatchingRoutineOperationTimer])
            && (DriverAuthDeviceMatching_Idle == RteInData_Common->DriverAuthDeviceMatching.Current))
         {
            if (CONST_MAX_KEYFOB > matchedKeyfobCount)
            {
               LFmatching_SM.New = SM_LFmatching_WaitingKeyfobPresentation;
            }
            else
            {
               LFmatching_SM.New = SM_LFmatching_MatchingListFull;
            }
         }
         else
         {
            //wait the operation status indication is processed
         }
      break;
      
      case SM_LFmatching_MatchingFail:
         if ((CONST_TimerFunctionElapsed == pTimers[CONST_MatchingRoutineOperationTimer])
            && (DriverAuthDeviceMatching_Idle == RteInData_Common->DriverAuthDeviceMatching.Current))
         {
            LFmatching_SM.New = SM_LFmatching_WaitingKeyfobPresentation;
         }
         else
         {
            //wait the operation status indication is processed
         }
      break;
      
      case SM_LFmatching_MatchingListFull:
         // wait R1AAA stop request
      break;
      
      case SM_LFmatching_MatchingStatus:
      break;
      
      default: // SM_LFmatching_Idle
         if ((DriverAuthDeviceMatching_Idle == RteInData_Common->DriverAuthDeviceMatching.Previous)
            && (DriverAuthDeviceMatching_DeviceToMatchIsPresent == RteInData_Common->DriverAuthDeviceMatching.Current))
         {
            LFmatching_SM.New = SM_LFmatching_MatchingAuthorizationCheck;
         }
         else
         {
            // wait keyfob presentation
            MatchingReport = CONST_P1B0T_WaitingKeyPresentation;
         }
      break;
      }

      // process new state
      if (LFmatching_SM.Current != LFmatching_SM.New)
      {
         LFmatching_SM.Current = LFmatching_SM.New;
         switch (LFmatching_SM.New)
         {
         case SM_LFmatching_MatchingNotAuthorized:
            MatchingReport = CONST_P1B0T_NotIdentifiedKey;
         break;
         
         case SM_LFmatching_MatchingAuthorizationCheck:
            // check auth by LF (immo back-up)
            pTimers[CONST_MatchingRoutineOperationTimer] = CONST_ImmoCheckOperationTimeout;
            Rte_Call_ImmoProcessingRqst_ImmoCircuitProcessing(ImmoType_ATA5702, TRUE);
            MatchingReport = CONST_P1B0T_OngoingDoNotRemoveTheKey;
         break;
         
         case SM_LFmatching_MatchingAuthorized:
            MatchingReport = CONST_P1B0T_OngoingDoNotRemoveTheKey;
         break;
         
         case SM_LFmatching_MatchingListReInitialization:
            Rte_Call_KeyfobMatchingOperations_ReinitializeMatchingList();
            MatchingReport = CONST_P1B0T_OngoingDoNotRemoveTheKey;
            pTimers[CONST_MatchingRoutineOperationTimer] = CONST_ImmoDriverOperationalTimeout;
         break;
         
         case SM_LFmatching_WaitingKeyfobPresentation:
            MatchingReport = CONST_P1B0T_WaitingKeyPresentation;
         break;
         
         case SM_LFmatching_MatchingKeyfob:     
            pTimers[CONST_MatchingRoutineTimer] = CONST_ImmoMatchingRoutingTimeout;
            pTimers[CONST_MatchingRoutineOperationTimer] = CONST_ImmoSingleMatchOperationTimeout;
            Rte_Call_KeyfobMatchingOperations_MatchKeyfobByLF();
            MatchingReport = CONST_P1B0T_OngoingDoNotRemoveTheKey;
         break;
         
         case SM_LFmatching_MatchingSuccessful:
            pTimers[CONST_MatchingRoutineOperationTimer] = CONST_MatchingSuccessResultDisplayTimeout;
            if (matchedKeyfobCount == 1)
            {
               MatchingReport = CONST_P1B0T_KeyListReinitialized;
            }
            else
            {
               MatchingReport = CONST_P1B0T_KeyMatchedAndStored;
            }
         break;
         
         case SM_LFmatching_MatchingFail:
            pTimers[CONST_MatchingRoutineOperationTimer] = CONST_MatchingFailResultDisplayTimeout;
            if (CONST_P1B0T_AlreadyMatchedKey == matchingStatus)
            {
               MatchingReport = CONST_P1B0T_AlreadyMatchedKey;
            }
            else
            {
               if (CONST_P1B0T_IvalidImmoDriverState == matchingStatus)
               {
                   MatchingReport = CONST_P1B0T_IvalidImmoDriverState;
               }
               else
               {
                   MatchingReport = CONST_P1B0T_NotIdentifiedKey;
               }
            }
         break;
         
         case SM_LFmatching_MatchingListFull:
            MatchingReport = CONST_P1B0T_NumberOfMaxKeyReached;
         break;
         
         default: // SM_LFmatching_Idle
            MatchingReport = CONST_P1B0T_WaitingKeyPresentation;
         break;
         }
      }
   }
   else
   {
      LFmatching_SM.New = SM_LFmatching_Idle;
      LFmatching_SM.Current = LFmatching_SM.New;
      pTimers[CONST_MatchingRoutineTimer] = CONST_TimerFunctionInactive;
      pTimers[CONST_MatchingRoutineOperationTimer] = CONST_TimerFunctionInactive;
      MatchingReport = CONST_P1B0T_MatchingProcedureNotStarted;
   }
   
   //Report status
   Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus(MatchingReport);
   
}


static void RteDataRead_Common(Keyfob_Mgr_in_StructType *RteInData_Common)
{
   RteInData_Common->DriverAuthDeviceMatching.Previous = RteInData_Common->DriverAuthDeviceMatching.Current ;
   if(Rte_Read_DriverAuthDeviceMatching_DriverAuthDeviceMatching(&RteInData_Common->DriverAuthDeviceMatching.Current) != RTE_E_OK)
   {
      RteInData_Common->DriverAuthDeviceMatching.Current = DriverAuthDeviceMatching_NotAvailable;
   }
   else
   {
      /* RTE_E_OK : do nothing */ 
   }

   RteInData_Common->KeyfobAuth_rqst.Previous = RteInData_Common->KeyfobAuth_rqst.Current ;
   if(Rte_Read_KeyfobAuth_rqst_KeyfobAuth_rqst(&RteInData_Common->KeyfobAuth_rqst.Current) != RTE_E_OK)
   {
      RteInData_Common->KeyfobAuth_rqst.Current = KeyfobAuth_rqst_NotAavailable;
   }
   else
   {
      /* RTE_E_OK : do nothing */ 
   }

   RteInData_Common->KeyfobInCabPresencePS_rqst.Previous = RteInData_Common->KeyfobInCabPresencePS_rqst.Current ;
   if(Rte_Read_KeyfobInCabPresencePS_rqst_KeyfobInCabPresencePS_rqst(&RteInData_Common->KeyfobInCabPresencePS_rqst.Current) != RTE_E_OK)
   {
      RteInData_Common->KeyfobInCabPresencePS_rqst.Current = KeyfobInCabPresencePS_rqst_NotAvailable;
   }
   else
   {
      /* RTE_E_OK : do nothing */ 
   } 

   RteInData_Common->KeyfobLocation_rqst.Previous = RteInData_Common->KeyfobLocation_rqst.Current ;
   if(Rte_Read_KeyfobLocation_rqst_KeyfobLocation_rqst(&RteInData_Common->KeyfobLocation_rqst.Current) != RTE_E_OK)
   {
      RteInData_Common->KeyfobLocation_rqst.Current = KeyfobLocation_rqst_NotAvailable;
   }
   else
   {
      /* RTE_E_OK : do nothing */ 
   }

   if(Rte_Read_SwcActivation_Security_SwcActivation_Security(&RteInData_Common->SwcActivation_Security) != RTE_E_OK)
   {
      RteInData_Common->SwcActivation_Security = NonOperational;
   }
   else
   {
      /* RTE_E_OK : do nothing */ 
   }
}

static void RteDataWrite_Common(Keyfob_Mgr_out_StructType *RteOutData_Common)
{
   Std_ReturnType retValue = RTE_E_INVALID;

   retValue = Rte_Write_KeyfobAuth_stat_KeyfobAuth_stat(RteOutData_Common->KeyfobAuth_stat);
   retValue |=Rte_Write_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat(RteOutData_Common->KeyfobInCabLocation_stat);
   retValue |=Rte_Write_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle(RteOutData_Common->KeyfobOutsideLocation_stat);

   if (retValue != RTE_E_OK)
   {
      // DET_report!!!
   }
   else
   {
      // RTE_E_OK : do nothing
   }
}


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
