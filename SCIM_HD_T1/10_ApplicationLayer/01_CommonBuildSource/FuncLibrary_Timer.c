//!======================================================================================
//! \file FuncLibrary_Timer.c
//!
//! @addtogroup Library
//! @{
//! @addtogroup FuncLibrary_Timer
//! @{
//!
//! \brief
//! FuncLibrary_Timer SWC.
//! ASIL Level : QM.
//! This module implements the logic for the timer library function.
//!
//! Individual timers can be accessed by reference to pTimer[TimerId], where TimerId is
//! CONST_TimerTag value.
//! Individual timers set/get operations shall be performed with usage of public keywords:
//! - CONST_TimerFunctionInactive;
//! - CONST_TimerFunctionElapsed;
//!
//! The timers decount shall be performed by call of 'TimerFunction_Tick()' function on
//! runnable level.
//! The timers intialization/reinitialization shall be performed by call of
//! 'TimerFunction_DeactivateAll()' function.
//!
//!======================================================================================

#include "Platform_Types.h"
#include "FuncLibrary_Timer.h"
#include "FuncLibrary_Timer_If.h"

//!======================================================================================
//!
//! \brief This function set all timers to value deactivating the timer
//!
//! \param          NbOfTimers defines the number of timers in matrix to re-initialize
//! \param          pTimer timers matrix
//!
//!====================================================================================== 

void TimerFunction_DeactivateAll(const uint8 NbOfTimers, uint16 *pTimer)
{
   uint8 TimerId;
   
   // set all timers to inactive
   for (TimerId = 0; TimerId < NbOfTimers; TimerId++)
   {
      pTimer[TimerId] = CONST_TimerFunctionInactive;
   }
}

//!======================================================================================
//!
//! \brief This function set the timer to value deactivating the timer
//!
//! \param          TimerId defines the timer id for operation
//! \param          pTimer value of timer
//!
//!====================================================================================== 

void TimerFunction_Deactivate(const uint8 TimerId, uint16 *pTimer)
{
	pTimer[TimerId] = CONST_TimerFunctionInactive;
}

//!======================================================================================
//!
//! \brief This function decriment all timers for 16bit timer
//!
//! Parameters:
//! \param          NbOfTimers defined in module
//! \param          pTimer timers matrix
//!
//!====================================================================================== 

void TimerFunction_Tick(const uint8 NbOfTimers, uint16 *pTimer)
{
   uint8 TimerId;
   
   // decrement all active timers
   for (TimerId = 0; TimerId < NbOfTimers; TimerId++)
   {
	   if (CONST_TimerFunctionInactive != pTimer[TimerId])
      {
         // if Timer is equal 0, it was processed in previous cycle as Elapsed Event and shall be deactivated
         if (pTimer[TimerId] > 0)
         {
            pTimer[TimerId] = pTimer[TimerId] - 1;
         }
         else
         {
            pTimer[TimerId] = CONST_TimerFunctionInactive;
         }
      }
      else
      {
         // time is inactive: do nothing
      }
   }
}

//!======================================================================================
//!
//! \brief This function decriment all timers for 32bit timer
//!
//! Parameters:
//! \param          NbOfTimers defined in module
//! \param          pTimer timers matrix
//!
//!====================================================================================== 
void TimerFunction_Tick_ThirtyTwo(const uint8 NbOfTimers, uint32 *pTimer)
{
    uint8 TimerId;

    // decrement all active timers 
    for (TimerId = 0; TimerId < NbOfTimers; TimerId++)
    {
        if (CONST_TimerFunctionInactive_32bit != pTimer[TimerId])
        {
            // if Timer is equal 0, it was processed in previous cycle as Elapsed Event and shall be deactivated
            if (pTimer[TimerId] > 0)
            {
                pTimer[TimerId] = pTimer[TimerId] - 1;
            }
            else
            {
                pTimer[TimerId] = CONST_TimerFunctionInactive_32bit;
            }
        }
        else
        {
            // time is inactive: do nothing
        }
    }
}

#ifdef VALIDATE_TEMPLATE
//!======================================================================================
//!
//! \brief This function is used to describe the usage and to verify the functionality
//!
//!====================================================================================== 

boolean Runnable (void)
{
   boolean status = FALSE;
   
   //! ##### timer matrix declaration: uint16 Timers[CONST_NbOfTimers]
   static uint16 Timers[CONST_NbOfTimers];
   
   //! ##### decriment all timers: TimerFunction_Tick (CONST_NbOfTimers, Timers)
   TimerFunction_Tick (CONST_NbOfTimers, Timers);
   
   //! ##### Check the timer is inactive: CONST_TimerFunctionInactive == Timers[CONST_Name1Timer]
   if (CONST_TimerFunctionInactive == Timers[CONST_Name1Timer])
   {
      TimerFunction_DeactivateAll (CONST_NbOfTimers, Timers);
      //! ##### Set the timer: Timers[CONST_Name2Timer] = CONST_TimeoutName3
      Timers[CONST_Name1Timer] = CONST_TimeoutName4;
   }
   else
   {
      // check if timer is elapsed
      if (CONST_TimerFunctionInactive == Timers[CONST_Name2Timer])
      {
         Timers[CONST_Name2Timer] = CONST_TimeoutName3;
      }
   }

   // UT condtion: timers expected to be elapsed in same time
   if ((CONST_TimerFunctionElapsed == Timers[CONST_Name1Timer]) &&
       (CONST_TimerFunctionElapsed == Timers[CONST_Name2Timer]))
   {
      Timers[CONST_Name1Timer] = CONST_TimerFunctionInactive;
      Timers[CONST_Name2Timer] = CONST_TimerFunctionInactive;
      
      status = TRUE;
   }
   else
   {
      // Do nothing
   }
   
   return status;
}
#endif

//! @}
//! @}

//=======================================================================================
// End of file
//=======================================================================================