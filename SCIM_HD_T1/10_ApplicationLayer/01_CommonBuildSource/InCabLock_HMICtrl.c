/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  InCabLock_HMICtrl.c
 *        Config:  C:/GIT/scim_ecu_hd_t1/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  InCabLock_HMICtrl
 *  Generated at:  Fri Dec 20 11:34:19 2019
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <InCabLock_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file InCabLock_HMICtrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_BodyAndSecurity 
//! @{
//! @addtogroup Application_VehicleAccess
//! @{
//! @addtogroup Application_IncabLock_HMICtrl
//! @{
//!
//! \brief
//! InCabLock_HMICtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the InCabLock_HMICtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_InCabLock_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "InCabLock_HMICtrl.h"
#include "FuncLibrary_Timer_If.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DoorLockUnlock_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorLockUnlock_Idle (0U)
 *   DoorLockUnlock_Unlock (1U)
 *   DoorLockUnlock_Lock (2U)
 *   DoorLockUnlock_MonoLockUnlock (3U)
 *   DoorLockUnlock_Spare1 (4U)
 *   DoorLockUnlock_Spare2 (5U)
 *   DoorLockUnlock_Error (6U)
 *   DoorLockUnlock_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 * Array Types:
 * ============
 * Crypto_Function_serialized_T: Array with 13 element(s) of type uint8
 * StandardNVM_T: Array with 4 element(s) of type uint8
 *
 *********************************************************************************************************************/


#define InCabLock_HMICtrl_START_SEC_CODE
#include "InCabLock_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: InCabLock_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_BunkH1LockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH1UnlockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH2LockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_DashboardLockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_InCabLock_HMICtrl_NVM_I_InCabLock_HMICtrl_NVM(uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Read_Locking_Switch_stat_serialized_Crypto_Function_serialized(uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_InCabLock_HMICtrl_NVM_I_InCabLock_HMICtrl_NVM(const uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Write_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst(DoorLockUnlock_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: InCabLock_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/
 
//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution runnable logic for the InCabLock_HMICtrl_20ms_runnable
//!
//!==================================================================================================================== 

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, InCabLock_HMICtrl_CODE) InCabLock_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: InCabLock_HMICtrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC In/Out data structures 
   // Define input RteOutData Common
   static InCabLock_HMICtrl_in_StructType   RteInData_Common;
   // Define output RteOutData Common
   static InCabLock_HMICtrl_out_StructType  RteOutData_Common = { DoorLockUnlock_NotAvailable };
   static uint16  Timers[CONST_NbOfTimers] = {PCODE_IncablockTimeout};

   //! ###### Processing RTE read Ports logic:'RteInDataRead_Common()'
   Get_RteInDataRead_Common(&RteInData_Common);

   // Decrement the timers
   TimerFunction_Tick(CONST_NbOfTimers,
                      Timers);

   //! ##### if SwcActivation_Living port is equal to Operational. 
   if (Operational == RteInData_Common.SwcActivation_Living)
   {
      //! #### Processing InCabLock logic : 'InCabLock_HMICtrl_Logic()'
      InCabLock_HMICtrl_Logic(&RteInData_Common,
                              Timers,
                              &RteOutData_Common);
   }
   //! ##### if SwcActivation_Living port is NOT equal to Operational
   else
   {
      RteOutData_Common.IncabDoorLockUnlock_rqst = DoorLockUnlock_NotAvailable;
   }

   //! ###### Write the RTE common Ports
   Rte_Write_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst(RteOutData_Common.IncabDoorLockUnlock_rqst);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define InCabLock_HMICtrl_STOP_SEC_CODE
#include "InCabLock_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief
//! This function is processing the Get_RteInDataRead_Common
//!
//! \param  *pRteInData_Common     RTE input data for data structure to fill with data
//!
//!======================================================================================
static void Get_RteInDataRead_Common(InCabLock_HMICtrl_in_StructType *pRteInData_Common)
{
   Std_ReturnType retvalue = RTE_E_INVALID;
   uint8 ReadCryptoSerialized[sizeof(Crypto_Function_serialized_T)];
   //! ###### Process Rte InDataRead logic
   //! ##### Read BunkH1LockButtonStatus interface
   pRteInData_Common->BunkH1LockButtonStatus.previousvalue = pRteInData_Common->BunkH1LockButtonStatus.currentvalue;
   retvalue = Rte_Read_BunkH1LockButtonStatus_PushButtonStatus(&pRteInData_Common->BunkH1LockButtonStatus.currentvalue);
   MACRO_StdRteRead_ExtRPort((retvalue),(pRteInData_Common->BunkH1LockButtonStatus.currentvalue),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))
   //! ##### Read BunkH1UnlockButtonStatus interface
   pRteInData_Common->BunkH1UnlockButtonStatus.previousvalue = pRteInData_Common->BunkH1UnlockButtonStatus.currentvalue;
   retvalue = Rte_Read_BunkH1UnlockButtonStatus_PushButtonStatus(&pRteInData_Common->BunkH1UnlockButtonStatus.currentvalue);
   MACRO_StdRteRead_ExtRPort((retvalue),(pRteInData_Common->BunkH1UnlockButtonStatus.currentvalue),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))
   //! ##### Read BunkH2LockButtonStatus interface
   pRteInData_Common->BunkH2LockButtonStatus.previousvalue = pRteInData_Common->BunkH2LockButtonStatus.currentvalue;
   retvalue = Rte_Read_BunkH2LockButtonStatus_PushButtonStatus(&pRteInData_Common->BunkH2LockButtonStatus.currentvalue);
   MACRO_StdRteRead_IntRPort((retvalue),(pRteInData_Common->BunkH2LockButtonStatus.currentvalue),(PushButtonStatus_Error))
   //! ##### Read DashboardLockButtonStatus interface
   pRteInData_Common->DashboardLockButtonStatus.previousvalue = pRteInData_Common->DashboardLockButtonStatus.currentvalue;
   retvalue = Rte_Read_DashboardLockButtonStatus_PushButtonStatus(&pRteInData_Common->DashboardLockButtonStatus.currentvalue);
   MACRO_StdRteRead_IntRPort((retvalue),(pRteInData_Common->DashboardLockButtonStatus.currentvalue),(PushButtonStatus_Error))
   //! ##### Read SwcActivation_Living interface
   retvalue = Rte_Read_SwcActivation_Living_Living(&pRteInData_Common->SwcActivation_Living);
   MACRO_StdRteRead_IntRPort((retvalue),(pRteInData_Common->SwcActivation_Living),(NonOperational))
   //! ##### Read Locking_Switch_stat_serialized interface
   pRteInData_Common->Locking_Switch_stat_serialized.previousvalue = pRteInData_Common->Locking_Switch_stat_serialized.currentvalue;
   retvalue = Rte_Read_Locking_Switch_stat_serialized_Crypto_Function_serialized(&ReadCryptoSerialized[0]);
   MACRO_StdRteRead_IntRPort((retvalue),(pRteInData_Common->Locking_Switch_stat_serialized.currentvalue),(DoorLockUnlock_Error))
   pRteInData_Common->Locking_Switch_stat_serialized.currentvalue = ReadCryptoSerialized[0];
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the InCabLock_HMICtrl_Logic,
//!
//! \param   *pInCabLck_in_data     Input data for data structure to fill with data
//! \param   Timer                  To check current Timer value and update       
//! \param   *pInCabLck_out_data    Output data for data structure to fill with data
//!
//!======================================================================================
static void InCabLock_HMICtrl_Logic(InCabLock_HMICtrl_in_StructType  *pInCabLck_in_data,
                                    uint16                           Timer[CONST_NbOfTimers],
                                    InCabLock_HMICtrl_out_StructType *pInCabLck_out_data)
{
   static uint8 IncabInitialize = CONST_Noninitialized;

   if (CONST_Noninitialized == IncabInitialize)
   {
      pInCabLck_in_data->Locking_Switch_stat_serialized.previousvalue = pInCabLck_in_data->Locking_Switch_stat_serialized.currentvalue;
      pInCabLck_in_data->BunkH1UnlockButtonStatus.previousvalue       = pInCabLck_in_data->BunkH1UnlockButtonStatus.currentvalue;
      pInCabLck_in_data->BunkH1LockButtonStatus.previousvalue         = pInCabLck_in_data->BunkH1LockButtonStatus.currentvalue;
      pInCabLck_in_data->DashboardLockButtonStatus.previousvalue      = pInCabLck_in_data->DashboardLockButtonStatus.currentvalue;
      pInCabLck_in_data->BunkH2LockButtonStatus.previousvalue         = pInCabLck_in_data->BunkH2LockButtonStatus.currentvalue;
      pInCabLck_out_data->IncabDoorLockUnlock_rqst                    = DoorLockUnlock_Idle;
      IncabInitialize                                                 = CONST_Initialized;
   }
   else
   {
      // Do Nothing: keep the previous state
   }
   //! ###### process InCabLock logic
   if (CONST_TimerFunctionInactive == Timer[CONST_IncablockTimer])
   {
      //! ###### Conditions for IncabDoorLockUnlock_rqst OUTPUT Port
      //! ##### condition for IncabDoorLockUnlock_rqst to value 'Unlock' for 40ms
      if (((DoorLockUnlock_Idle == pInCabLck_in_data->Locking_Switch_stat_serialized.previousvalue)
         && (DoorLockUnlock_Unlock == pInCabLck_in_data->Locking_Switch_stat_serialized.currentvalue))
         || ((PushButtonStatus_Pushed != pInCabLck_in_data->BunkH1UnlockButtonStatus.previousvalue)
         && (PushButtonStatus_Pushed == pInCabLck_in_data->BunkH1UnlockButtonStatus.currentvalue)))
      {
         // Start Timer
         Timer[CONST_IncablockTimer]                  = PCODE_IncablockTimeout;
         pInCabLck_out_data->IncabDoorLockUnlock_rqst = DoorLockUnlock_Unlock;
      }
      //! ##### condition for IncabDoorLockUnlock_rqst to value 'Lock' for 40ms
      else if (((DoorLockUnlock_Idle == pInCabLck_in_data->Locking_Switch_stat_serialized.previousvalue)
              && (DoorLockUnlock_Lock == pInCabLck_in_data->Locking_Switch_stat_serialized.currentvalue))
              || ((PushButtonStatus_Pushed != pInCabLck_in_data->BunkH1LockButtonStatus.previousvalue)
              && (PushButtonStatus_Pushed == pInCabLck_in_data->BunkH1LockButtonStatus.currentvalue))
              || ((PushButtonStatus_Neutral == pInCabLck_in_data->BunkH2LockButtonStatus.previousvalue)
              && (PushButtonStatus_Pushed == pInCabLck_in_data->BunkH2LockButtonStatus.currentvalue)))
      {
         // Start Timer
         Timer[CONST_IncablockTimer]                  = PCODE_IncablockTimeout;
         pInCabLck_out_data->IncabDoorLockUnlock_rqst = DoorLockUnlock_Lock;       
      }
      //! ##### condition for IncabDoorLockUnlock_rqst to value 'MonoLockUnlock'
      else if ((PushButtonStatus_Neutral == pInCabLck_in_data->DashboardLockButtonStatus.previousvalue)
              && (PushButtonStatus_Pushed == pInCabLck_in_data->DashboardLockButtonStatus.currentvalue))
      {
         // Start Timer
         Timer[CONST_IncablockTimer]                  = PCODE_IncablockTimeout;
         pInCabLck_out_data->IncabDoorLockUnlock_rqst = DoorLockUnlock_MonoLockUnlock;
      }
      else
      {
         // Do Nothing: keep the previous state 
      }
   }
   else 
   {   
      //! ##### Check the TimerElapsed value for Incablock Timer
      if (CONST_TimerFunctionElapsed == Timer[CONST_IncablockTimer])
      { 
          // Cancel Timer
          Timer[CONST_IncablockTimer]                  = CONST_TimerFunctionInactive;
          pInCabLck_out_data->IncabDoorLockUnlock_rqst = DoorLockUnlock_Idle; 
      }
      else
      {
         // Do Nothing: keep the previous state
      }
   }
}

//! @}
//! @}
//! @}
//! @}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
