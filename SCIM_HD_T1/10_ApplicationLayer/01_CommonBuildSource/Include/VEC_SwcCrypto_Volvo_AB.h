/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2010-2013 by Vector Informatik GmbH.                                             All rights reserved.
 * 
 * Please note, that this file contains an implementation example for 
 * VEC_SwcCrypto_Volvo_AB. This code may influence the behaviour of the VEC_SwcCrypto_Volvo_AB
 * in principle. Therefore, great care must be taken to verify
 * the correctness of the implementation.
 *
 * The contents of the originally delivered files are only examples resp. 
 * implementation proposals. With regard to the fact that these functions 
 * are meant for demonstration purposes only, Vector Informatik's 
 * liability shall be expressly excluded in cases of ordinary negligence, 
 * to the extent admissible by law or statute.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  VEC_SwcCrypto_Volvo_AB.h
 *      Project:  SysService_AsrSwcCrypto_Volvo_AB
 *       Module:  -
 *    Generator:  -
 *
 *  Description:  Implements interface to AES library.
 *  
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  -------------------------------------------------------------------------------------------------------------------
 *  Oliver Garnatz                Gz            Vector Informatik GmbH
 *  Patrick Markl                 Ml            Vector Informatik GmbH
 *  Alexander Zeeb                Azb           Vector Informatik GmbH
 *  Joern Herwig                  JHg           Vector Informatik GmbH
 *  Markus Schneider              Mss           Vector Informatik GmbH
 *  -------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  01.00.00  2010-01-27  Gz      -             Initial creation of template
 *  01.01.01  2010-06-09  Azb/Ml  -             Fixed array access, QAC findings
 *  01.02.00  2011-01-27  JHg     ESCAN00043956 Corrected handling of initial identification key greater max value
 *                        JHg     ESCAN00047263 Fixed identification key retransmission
 *                        JHg     ESCAN00047274 Immediate transmission of first message after key reception
 *                        JHg     ESCAN00047275 Use cycle offset after each key reception
 *                        JHg     -             QAC findings
 *  02.00.00  2013-09-19  Mss     ESCAN00069425 CSM and ASR4 support added
 *  02.00.01  2013-12-02  Mss     ESCAN00071941 Fixed compiler errors
 *  02.00.02  2014-11-10  Mss     ESCAN00079447 No changes on code
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _VEC_CRYPTOPROXYSENDER_H
# define _VEC_CRYPTOPROXYSENDER_H

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* ##V_CFG_MANAGEMENT ##CQProject : SysService_AsrSwcCrypto_Volvo_AB CQComponent : Implementation */
/* Version - BCD coded version number - Main- and Sub-Version - Release-Version */
#define SYSSERVICE_ASRSWCCRYPTO_VOLVO_AB_VERSION          (0x0200u)
#define SYSSERVICE_ASRSWCCRYPTO_VOLVO_AB_RELEASE_VERSION  (0x01u)

/* Component Version Information */
#define VEC_CRYPTO_TRANSMISSION_MAJOR_VERSION       (SYSSERVICE_ASRSWCCRYPTO_VOLVO_AB_VERSION >> 8u)
#define VEC_CRYPTO_TRANSMISSION_MINOR_VERSION       (SYSSERVICE_ASRSWCCRYPTO_VOLVO_AB_VERSION & 0x00FF)
#define VEC_CRYPTO_TRANSMISSION_PATCH_VERSION       SYSSERVICE_ASRSWCCRYPTO_VOLVO_AB_RELEASE_VERSION

/* Identification counter */
#define VEC_CRYPTO_TRANSMISSION_ID_INVALID    (0xFFFFFFFFul)
#define VEC_CRYPTO_TRANSMISSION_ID_RANGE      (0x03ul)
#define VEC_CRYPTO_TRANSMISSION_ID_MAX        (VEC_CRYPTO_TRANSMISSION_ID_INVALID - VEC_CRYPTO_TRANSMISSION_ID_RANGE - 0x03ul)

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

#endif  /* VEC_CRYPTO_TRANSMISSION_H */

/**********************************************************************************************************************
 *  END OF FILE: VEC_Crypto_Transmission.h
 *********************************************************************************************************************/
