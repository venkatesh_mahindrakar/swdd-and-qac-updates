#ifdef DRIVERSEATBELTBUCKLESW_H
#error DRIVERSEATBELTBUCKLESW_H unexpected multi-inclusion
#else
#define DRIVERSEATBELTBUCKLESW_H
//=====================================================================================
// Included header files
//=====================================================================================


//=======================================================================================
// Private constants and enum definitions
//=======================================================================================


//=======================================================================================
// Private macros
//=======================================================================================

#ifndef CONST_AdiBiLevelLogicNotAvailable
#define CONST_AdiBiLevelLogicNotAvailable     (VGTT_EcuPinVoltage_0V2)(255)
#endif

#ifndef CONST_AdiPin9_SeatBeltBuckle
#define CONST_AdiPin9_SeatBeltBuckle          (9U)
#endif

//=======================================================================================
// Private type definitions
//=======================================================================================

// Inputs structure 
typedef struct
{
   VGTT_EcuPinVoltage_0V2    DriverSeatBelt_AdiVoltage;
   VGTT_EcuPinVoltage_0V2    LivingPullUpVoltage;
   VehicleModeDistribution_T SwcActivation_IgnitionOn;
   VGTT_EcuPinFaultStatus    FaultStatus;
} DriverSeatBeltBuckleSwitch_hdlr_in_StructType;

// Output structure 
typedef struct
{
   DriverSeatBeltSwitch_T DriverSeatBeltSwitch;
   DriverSeatBeltSwitch_T Old_DriverSeatBeltSwitch;
} DriverSeatBeltBuckleSwitch_hdlr_out_StructType;

//=======================================================================================
// Private data declarations
//=======================================================================================
// Note: MUST BE static
// Note: Shall not be used/avoided with AUTOSAR SWC.


//=======================================================================================
// Private function prototypes
//=======================================================================================
// Note : MUST BE static

//=====================================================================================
// End of file
//=====================================================================================
 #endif
 