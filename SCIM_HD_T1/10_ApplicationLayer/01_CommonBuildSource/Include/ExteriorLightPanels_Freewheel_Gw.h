//!======================================================================================
//! \file ExteriorLightPanels_Freewheel_Gw.h
//!
//!======================================================================================

//=======================================================================================
// Private type definitions
//=======================================================================================


//=======================================================================================
// Private constants and enums definitions
//=======================================================================================

#define CONST_OFFSET1                                     1

typedef struct{
	FreeWheel_Status_T LightMode_Status;  
	boolean            isLightMode_Status_Updated ; 
}LightMode_Status;

//=======================================================================================
// Private data declarations
//=======================================================================================
// Note: Shall not be used with AUTOSAR SWC.

//=======================================================================================
// Private macros
//=======================================================================================
// Note, coding convention: shall be avoided to support debug capability.

//=======================================================================================
// Private function prototypes
//=======================================================================================

static void Read_InternalVehiculeModeDistribution(VehicleModeDistribution_T *pRteInData_vehicle_mode_distritution);
static void Read_LightMode_Status_1_FreeWheel(LightMode_Status *pLightMode_Status_1_FreeWheel);
static void Read_LightMode_Status_2_FreeWheel(LightMode_Status *pLightMode_Status_2_FreeWheel);
static void Processing_LightMode_1_FreeWheel(LightMode_Status *pLightMode_Status_1_FreeWheel, Freewheel_Status_Ctr_T *pLightMode_Status_Ctr_1);
static void Processing_LightMode_2_FreeWheel(LightMode_Status *pLightMode_Status_2_FreeWheel, Freewheel_Status_Ctr_T *pLightMode_Status_Ctr_2);
static boolean IsActiveVehicleModeDistribution(VehicleModeDistribution_T *pRteInData_vehicle_mode_distritution);
//=======================================================================================
// End of file
//=======================================================================================