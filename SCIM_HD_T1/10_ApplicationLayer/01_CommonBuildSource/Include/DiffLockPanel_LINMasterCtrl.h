#ifdef DIFFLOCKPANEL_LINMASTERCTRL_H
#error DIFFLOCKPANEL_LINMASTERCTRL_H unexpected multi-inclusion
#else
#define DIFFLOCKPANEL_LINMASTERCTRL_H
//=====================================================================================
// Included header files
//=====================================================================================

//=====================================================================================
// Private constants and enum definitions
//=====================================================================================

//=====================================================================================
// Private macros
//=====================================================================================

#define CONST_Tester_Present          (0x80U)
#define CONST_Tester_Notpresent       (0x7FU)
#define CONST_Fault_InActive          (0U)
#define CONST_Fault_Active            (1U)
#define CONST_SlaveFCIFault_InActive  (0U)
#define CONST_SlaveFCIFault_Active    (1U)

//=====================================================================================
// Private type definitions
//=====================================================================================
// Structure for inputs
typedef struct
{
   PushButtonStatus_T    LIN_DifflockDeactivationBtn_st;
   FreeWheel_Status_T    LIN_DifflockMode_Wheelstatus;
   PushButtonStatus_T    LIN_EscButtonMuddySiteStatus;
   PushButtonStatus_T    LIN_Offroad_ButtonStatus;
   DeviceIndication_T    DifflockOnOff_Indication;
   DeviceIndication_T    EscButtonMuddySiteDeviceInd;
   DeviceIndication_T    Offroad_Indication;
   ResponseErrorDLFW_T   DLFW_ResponseError;
   ComMode_LIN_Type      ComMode_LIN4;
   DiagInfo_T            DiagInfoDLFW;
   DiagActiveState_T     isDiagActive;
} DiffLockPanel_LINMasterCtrl_In_StructType;

// Structure for outputs
typedef struct
{
   PushButtonStatus_T    DifflockDeactivationBtn_st;
   FreeWheel_Status_T    DifflockMode_Wheelstatus;
   PushButtonStatus_T    EscButtonMuddySiteStatus;
   PushButtonStatus_T    Offroad_ButtonStatus;
   DeviceIndication_T    LIN_DifflockOnOff_Indication;
   DeviceIndication_T    LIN_EscButtonMuddySiteDeviceIn;
   DeviceIndication_T    LIN_Offroad_Indication;
} DiffLockPanel_LINMasterCtrl_Out_StructType;

//=====================================================================================
// Private data declarations
//=====================================================================================

//=====================================================================================
// Private function prototypes
//=====================================================================================

static void IOCtrlService_LinOutputSignalControl(const uint8                                       OverrideData,
                                                       DiffLockPanel_LINMasterCtrl_Out_StructType  *pOutData);
static void RteDataWrite_Common(const DiffLockPanel_LINMasterCtrl_Out_StructType *pRteOutData_Common);
static Std_ReturnType Get_RteDataRead_Common(DiffLockPanel_LINMasterCtrl_In_StructType *pRteInData_Common);
static void DiffLockPanel_LINMasterCtrl_Signals_GatewayLogic(const DiffLockPanel_LINMasterCtrl_In_StructType  *pInData,
                                                                   DiffLockPanel_LINMasterCtrl_Out_StructType *pOutData);
static void DiffLckPanel_LINMastCtrl_FallbackLogic(PushButtonStatus_T *pDifflockDeactivationBtn_st,
                                                   FreeWheel_Status_T *pDifflockMode_Wheelstatus,
                                                   PushButtonStatus_T *pEscButtonMuddySiteStatus,
                                                   PushButtonStatus_T *pOffroad_ButtonStatus);
static void DiffLockPanel_LINMasterCtrl_DeactivateLogic(DiffLockPanel_LINMasterCtrl_Out_StructType *pOutData);
static boolean DiffLockPanel_LINMasterCtrl_FCI_Indication(const DiagInfo_T *pDiagInfoDLFW);

//=====================================================================================
// End of file
//=====================================================================================
#endif

