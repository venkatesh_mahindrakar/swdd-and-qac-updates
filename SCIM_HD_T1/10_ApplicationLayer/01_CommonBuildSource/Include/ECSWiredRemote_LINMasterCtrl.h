#ifdef ECSWIREDREMOTE_LINMASTERCTRL_H
#error ECSWIREDREMOTE_LINMASTERCTRL_H unexpected multi-inclusion
#else
#define ECSWIREDREMOTE_LINMASTERCTRL_H
//=====================================================================================
// Included header files
//=====================================================================================

//=====================================================================================
// Private constants and enum definitions
//=====================================================================================

//=====================================================================================
// Private macros
//=====================================================================================

#define CONST_EcsWiredLinMasterCycleTime  (20U)   // 20ms
#define PCODE_AnwEcsStandbyDeactTimeout   ((10000U) / CONST_EcsWiredLinMasterCycleTime)   // 10s Deactivation Timeout value
#define CONST_Tester_Present              (0x80U)
#define CONST_Tester_Notpresent           (0x7FU)
#define CONST_Fault_InActive              (0U)
#define CONST_Fault_Active                (1U)
#define CONST_SlaveFCIFault_InActive      (0U)
#define CONST_SlaveFCIFault_Active        (1U)

//=====================================================================================
// Private type definitions
//=====================================================================================

typedef struct
{
   PushButtonStatus_T   Current;
   PushButtonStatus_T   Previous;
} VarPushButtonStatus_T;

typedef struct
{
   EvalButtonRequest_T   Current;
   EvalButtonRequest_T   Previous;
} VarEvalButtonRequest_T;

 
// Input Structure 
typedef struct
{
   VarPushButtonStatus_T     VarLIN_AdjustButtonStatus;
   VarPushButtonStatus_T     VarLIN_BackButtonStatus;
   VarPushButtonStatus_T     VarLIN_MemButtonStatus;
   VarPushButtonStatus_T     VarLIN_SelectButtonStatus;
   VarPushButtonStatus_T     VarLIN_StopButtonStatus;
   VarEvalButtonRequest_T    VarLIN_WRDownButtonStatus;
   VarEvalButtonRequest_T    VarLIN_WRUpButtonStatus;
   DeviceIndication_T        Adjust_DeviceIndication;
   DeviceIndication_T        Down_DeviceIndication;
   DeviceIndication_T        Up_DeviceIndication;
   DeviceIndication_T        M1_DeviceIndication;
   DeviceIndication_T        M2_DeviceIndication;
   DeviceIndication_T        M3_DeviceIndication;
   ShortPulseMaxLength_T     ShortPulseMaxLength;
   ComMode_LIN_Type          ComMode_LIN5;
   DiagInfo_T                DiagInfoRCECS_LIN5;
   ResponseErrorRCECS        ResponseErrorRCECS_LIN5;
   DiagActiveState_T         isDiagActive;
} ECSWiredRemoteLINMasterCtrl_InData_T;

 
// Output Structure 
typedef struct
{  
   PushButtonStatus_T       AdjustButtonStatus;
   PushButtonStatus_T       BackButtonStatus;
   PushButtonStatus_T       MemButtonStatus;
   PushButtonStatus_T       SelectButtonStatus;
   PushButtonStatus_T       StopButtonStatus;
   EvalButtonRequest_T      WRDownButtonStatus;
   EvalButtonRequest_T      WRUpButtonStatus;   
   DeviceIndication_T       LIN_Adjust_DeviceIndication;
   DeviceIndication_T       LIN_Down_DeviceIndication;
   DeviceIndication_T       LIN_Up_DeviceIndication;
   DeviceIndication_T       LIN_M1_DeviceIndication;
   DeviceIndication_T       LIN_M2_DeviceIndication;
   DeviceIndication_T       LIN_M3_DeviceIndication;
   ShortPulseMaxLength_T    LIN_ShortPulseMaxLength;
} ECSWiredRemoteLINMasterCtrl_OutData_T;

//=====================================================================================
// Private data declarations
//=====================================================================================

//=====================================================================================
// Private function prototypes
//=====================================================================================

static void IOCtrlService_LinOutputSignalControl(const uint8                                 OverrideData,
	                                                   ECSWiredRemoteLINMasterCtrl_OutData_T *pOutput_struct_Data);
static void RteDataWrite_Common(const ECSWiredRemoteLINMasterCtrl_OutData_T *pRteOutData_Common);
static Std_ReturnType Get_RteDataRead_Common(ECSWiredRemoteLINMasterCtrl_InData_T *pRteInData_Common);
static void ECSWiredRemote_Signals_Gateway_Logic(const ECSWiredRemoteLINMasterCtrl_InData_T  *pInData, 
                                                       ECSWiredRemoteLINMasterCtrl_OutData_T *pOutData);
static void Generic_ANW_ECSStandByTrigger1(const FormalBoolean isRestAct,
                                                 const FormalBoolean isActTrigDet);
static FormalBoolean ANW_ECSStandByTrigger1_Cond(const ECSWiredRemoteLINMasterCtrl_InData_T *pInDataTrig);
static boolean ECSWiredRemote_LINMasterCtrl_FCI_Indication(const DiagInfo_T     *pDiagInfoRCECS_LIN5);
static void ECSWiredRemoteLINMasterCtrl_FallbackLogic(ECSWiredRemoteLINMasterCtrl_OutData_T *pOutData_Fallback);
static void ECSWiredRemoteLINMasterCtrl_DeactivateLogic(ECSWiredRemoteLINMasterCtrl_OutData_T *pOutData_Deact);

//=====================================================================================
// End of file
//=====================================================================================
#endif

