//!======================================================================================
//! \file FuncLibrary_AnwStateMachine_If.h
//!
//!======================================================================================

#ifndef FuncLibrary_AnwStateMachine_If_H
// To protect against multi-inclusion : FuncLibrary_AnwStateMachine_If_H must be unique
#define FuncLibrary_AnwStateMachine_If_H

#include "FuncLibrary_ScimStd_If.h"
//=======================================================================================
// Public type definitions
//=======================================================================================

#define DeactivationTimer_Type uint16

//=======================================================================================
// Public constants and enums definitions
//=======================================================================================

   typedef enum
   {
      SM_ANW_Inactive,
      SM_ANW_Active,
      SM_ANW_Deactivation
   } AnwSM_Enum;

   typedef enum
   {
      ANW_Action_None,
      ANW_Action_Deactivate,
      ANW_Action_Activate
   } AnwAction_Enum;

   typedef struct
   {
      AnwSM_Enum Current;
      AnwSM_Enum New;
   } AnwSM_States;

//=======================================================================================
// Public data declarations
//=======================================================================================
// Note: Shall not be used with AUTOSAR SWC.

//=======================================================================================
// Public macros
//=======================================================================================
// Note, coding convention: shall be avoided to support debug capability.

//=======================================================================================
// Public function prototypes
//=======================================================================================

AnwAction_Enum ProcessAnwLogic(const FormalBoolean isRestrictionActivated,
                          const FormalBoolean isActivationTriggerDetected,
                          const FormalBoolean isDeactivationConditionsFulfilled,
                          const DeactivationTimer_Type  DeactivationTimeout,
                          DeactivationTimer_Type  *DeactivationTimer,
                          AnwSM_States *AnwState);

#endif // FuncLibrary_AnwStateMachine_If_H                              

//! @}
//! @}

//=======================================================================================
// End of file
//=======================================================================================