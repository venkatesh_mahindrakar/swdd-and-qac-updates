#ifdef VEHICLEACCESS_CTRL_H
#error VEHICLEACCESS_CTRL_H unexpected multi-inclusion
#else
#define VEHICLEACCESS_CTRL_H
//=====================================================================================
// Included header files
//=====================================================================================

//=======================================================================================
// Private constants and enum definitions
//=======================================================================================

//=======================================================================================
// Private macros
//=======================================================================================

#ifndef RESETFLAG
#define RESETFLAG                          (0U)
#endif 

#ifndef SETFLAG
#define SETFLAG                            (1U)
#endif 

// timers
#define CONST_RunnableTimeBase             (20U)
#define CONST_mstominutesconversion        (1000U * 60U)
#define CONST_DoorUnlock_Timer             (0U)
#define CONST_DoorLock_Timer               (1U)
#define CONST_HazardLightIndTimer          (2U)
#define CONST_DashboardLEDIndTimer         (3U)
#define CONST_AutoRelockTimer              (4U)
#define CONST_DriverLatchTimer             (5U)
#define CONST_PassengerLatchTimer          (6U)
#define CONST_DriverSelfLockTimer          (7U)
#define CONST_PassengerSelfLockTimer       (8U)
#define CONST_AlarmReactRqstTimer          (9U)
#define CONST_LatchProtectionTimer         (10U)
#define CONST_NbOfTimers                   (11U)
//#define CONST_AutoRelockMonitorTime                      ((7000U) / CONST_RunnableTimeBase) // configurable parameter
#define CONST_LatchIdleElectricalCtrlTransitionState     ((5000U) / CONST_RunnableTimeBase) // configurable parameter
#define CONST_ANWLockDeactivationTimeout                 ((10000U) / CONST_RunnableTimeBase)
#define CONST_ANWDashboardDeactivationTimeout            ((1000U) / CONST_RunnableTimeBase)
#define CONST_ANWDoorLatchDeactivationTimeout            ((3000U) / CONST_RunnableTimeBase)
// Functional constants
#define CONST_WheelBasedVehicleSpeedMax    (23040U)              // 90km/h
#define ACTIVE                             (TRUE)
#define INACTIVE                           (FALSE)
#define DEFAULT                            (3U)
#define DBC_UINT16_NOTAVAILABLE            (65535U)
#define DBC_UINT16_ERROR                   (65100U)

//=======================================================================================
// Private type definitions
//=======================================================================================

typedef enum
{
   CONST_SelfLock_B2S_Idle            = 0,
   CONST_SelfLock_B2S_Fail            = 1,
   CONST_SelfLock_B2S_PendingUnlock   = 2,
   CONST_SelfLock_B2S_RestoreUnlock   = 5
} SelfLockAction_Enum;

typedef enum
{
   SM_Lock_Idle                = 0,
   SM_Lock_UnlockDriverDoor    = 1,
   SM_Lock_UnlockPassengerDoor = 2,
   SM_Lock_UnlockBothDoors     = 3,
   SM_Lock_LockBothDoor        = 4,
   SM_Lock_OperationStatus     = 5,
   SM_Lock_Error               = 6
} LockStateMachine_Enum;

typedef enum
{
   SM_Autorelock_Idle    = 0,
   SM_Autorelock_Pending = 1,
   SM_Autorelock_Active  = 2
} AutorelockStateMachine_Enum;

typedef enum
{
   CONST_Idle_PassivEntry          = 0,
   CONST_UnlckEvt_PassivDrvDrEntry = 1,
   CONST_UnlckEvt_PassivPsngDrEtry = 2,
   CONST_LockEvt_PassiveDoorLock   = 3
} PassiveEntryLogic_Enum;

typedef enum
{
   SM_RemoteStatusIndication_Idle          = 0,
   SM_RemoteUnlockStatusIndication_Pending = 1,
   SM_RemoteLockStatusIndication_Pending   = 2,
   SM_RemoteUnlockStatusIndication_Request = 3,
   SM_RemoteLockStatusIndication_Request   = 4,
   SM_RemoteStatusIndication_RequestActive = 5
} RemoteStatusIndication_Enum;

// Added event Definitions
typedef enum
{
   CONST_IdleUnlockRequest          = 0,
   CONST_PassiveEntryUnlckReqDrvDr  = 1,
   CONST_KeyCyldrTurnedToUnlckPos   = 2,
   CONST_PassiveEntryUnlckReqPsgrDr = 3,
   CONST_KeyfobUnlockReq_PsgrDoor   = 4,
   CONST_KeyfobUnlockReq_DrvDoor    = 5,
   CONST_WrcUnlockReq_PsgrDoor      = 6,
   CONST_WrcUnlockReq_DrvDoor       = 7,
   CONST_InCabUnlockRequest         = 8,
   CONST_MechUnlockReqFromPsgrDoor  = 9,
   CONST_MechUnlockReqFromDrvDoor   = 10,
   CONST_MonoStableBtnReq_Unlock    = 11,
   CONST_EmergencyUnlocking         = 12
} EventUnlock_Enum;

typedef enum
{
   CONST_IdleLockRequest          = 0,
   CONST_PassiveEntryLockRequest  = 1,
   CONST_KeyfobLockRequest        = 2,
   CONST_KeyfobSuperLockRequest   = 3,
   CONST_WrcLockRequest           = 4,
   CONST_InCabLockRequest         = 5,
   CONST_MechanicalLockRequest    = 6,
   CONST_MonoStableButtonReq_Lock = 7
} EventLock_Enum;

typedef enum 
{ 
   CONST_DoorsOprtnIdle               = 0,
   CONST_DrvDoorUnlckOprtnTerminated  = 1,
   CONST_PsgrDoorUnlckOprtnTerminated = 2,
   CONST_DoorsUnlockOprtnTerminated   = 3,
   CONST_DoorsLockOprtnTerminated     = 4
} EventSysLatch_Enum;

typedef struct
{
   SelfLockAction_Enum DrvrLatch;
   SelfLockAction_Enum PsgrLatch;
} SelfLockAction_T;

typedef struct
{
   LockStateMachine_Enum currentValue;
   LockStateMachine_Enum newValue;
   boolean               isRemoteOperation;
} SM_States;

typedef struct
{
   PushButtonStatus_T currentValue;
   PushButtonStatus_T previousValue;
} PushButtonType;

typedef struct
{
   Synch_Unsynch_Mode_stat_T currentValue;
   Synch_Unsynch_Mode_stat_T previousValue;
} Synch_Unsynch_Mode_stat;

typedef struct
{
   DoorLockUnlock_T currentValue;
   DoorLockUnlock_T previousValue;
} IncabDoorLockUnlock;

typedef struct
{
   EmergencyDoorsUnlock_rqst_T currentValue;
   EmergencyDoorsUnlock_rqst_T previousValue;
} EmergencyDoorsUnlock_rqst;

typedef struct
{
   uint8 currentValue;
   uint8 previousValue;
} DoorLatchState;

typedef struct
{
   VehicleMode_T currentValue;
   VehicleMode_T previousValue;
} VehicleMode_Value;

typedef struct
{
   EventUnlock_Enum currentValue;
   EventUnlock_Enum previousValue;
} DriverDrKeyCylTrn;

typedef struct
{
   EventUnlock_Enum currentValue;
   EventUnlock_Enum previousValue;
} PassengerDrKeyCylTrn;

// Structure for inputs 
typedef struct
{
   DriverDrKeyCylTrn               DriverDrKeyCylTrn_stat;
   PassengerDrKeyCylTrn            PassengerDrKeyCylTrn_stat;
   EmergencyDoorsUnlock_rqst       EmergencyDoorsUnlock_rqst;
   IncabDoorLockUnlock             IncabDoorLockUnlock_rqst;
   PushButtonType                  KeyfobLockButton_Stat;
   PushButtonType                  KeyfobSuperLockButton_Stat;
   PushButtonType                  KeyfobUnlockButton_Stat;
   Synch_Unsynch_Mode_stat         Synch_Unsynch_Mode_stat;
   PushButtonType                  WRCLockButtonStat;
   PushButtonType                  WRCUnlockButtonStat;
   VehicleMode_Value               VehicleModeInternal;
   AutorelockingMovements_stat_T   AutorelockingMovements_Stat;
   SpeedLockingInhibition_stat_T   SpeedLockingInhibition_stat;
   Speed16bit_T                    WheelBasedVehicleSpeed;
} VehicleAccess_Ctrl_in_StructType;

typedef struct
{
   DoorLatch_stat_T                DriverDoorLatch_stat;
   DoorLatch_stat_T                PassengerDoorLatch_stat;
} LatchWithDoorModules_in_StructType;

typedef struct
{
   DoorAjar_stat_T                 DriverDoorAjar_stat;
   DoorAjar_stat_T                 PassengerDoorAjar_stat;
} AjarWithDoorModules_in_StructType;

// Structure for outputs 
typedef struct
{
   DoorLock_stat_T               DoorLock_stat;
   DoorsAjar_stat_T              DoorsAjar_stat;
   DoorLatch_rqst_decrypt_T      DriverDoorLatch_rqst;
   DoorLatch_rqst_decrypt_T      PassengerDoorLatch_rqst;
   KeyfobLocation_rqst_T         KeyfobLocation_rqst;
   DeviceIndication_T            DoorLockSwitch_DeviceIndication;
   uint8                         DriverDoorLatch_rqst_serialized;
   uint8                         PassengerDoorLatch_rqst_serialized;
   LockingIndication_rqst_T      LockingIndication_rqst;
   AutoRelock_rqst_T             AutoRelock_rqst;
} VehicleAccess_Ctrl_out_StructType;

//=======================================================================================
// Private data declarations
//=======================================================================================
// Note: MUST BE static
// Note: Shall not be used/avoided with AUTOSAR SWC.

//=======================================================================================
// Private function prototypes
//=======================================================================================
// Note : MUST BE static

static void SynchronuousModeProcessing(Synch_Unsynch_Mode_stat *pSynch_Unsynch_Mode);
static EventUnlock_Enum CheckUnlockEvents(const VehicleMode_T                *pVehicleModecurrent,
                                          const PushButtonType               *pKeyfobUnlockButton_Stat,
                                          const PushButtonType               *pWRCUnlockButtonStat,
                                          const IncabDoorLockUnlock          *pIncabDoorLockUnlockrequest,
                                          const DriverDrKeyCylTrn            *pDriverDrKeyCylTrnstatus,
                                          const PassengerDrKeyCylTrn         *pPassengerDrKeyCylTrnstatus,
                                          const EmergencyDoorsUnlock_rqst    *pEmergencyDoorsUnlock_rqst,
                                          const DoorLatch_stat_T             *pDoorLatchStat_current,
                                          const Synch_Unsynch_Mode_stat      *pSynch_Unsynch_Mode);
static EventLock_Enum CheckLockEvents(const PushButtonType         *pKeyfobLockButton_Stat,
                                      const PushButtonType         *pKeyfobSuperLockButton_Stat,
                                      const PushButtonType         *pWRCLockButtonStat,
                                      const VehicleMode_T          *pVehicleMode_current,
                                      const IncabDoorLockUnlock    *pIncabDoorLockUnlockrequest,
                                      const DoorLatchState         *pDoorLatchStatus);
static DoorsAjar_stat_T ProcessDoorAjarStateFromXDM(const DoorAjar_stat_T  *pDriverDoorAjar_stat,
                                                    const DoorAjar_stat_T  *pPassengerDoorAjar_stat);
static void ProcessDoorLatchesStateFromXDM(const DoorLatch_stat_T   *pDriverDoorLatch_stat,
                                           const DoorLatch_stat_T   *pPassengerDoorLatch_stat,
                                                 DoorLatchState     *pDoorLatchStatus);
static void DashboardLEDIndicating(const VehicleMode_Value   *pVehicleMode,
                                   const DoorLatchState      *pDoorLatchStatus,
                                         DeviceIndication_T  *pDoorLockSwitch_DeviceIndication,
                                         uint16              *pTimers);
static void isSystemEventAutorelock(const AutorelockingMovements_stat_T *pautorelockingMovements_Stat,
                                    const VehicleMode_T                 *pvehicleModecurrent,
                                    const EventUnlock_Enum              unlockEvent,
                                    const DoorsAjar_stat_T              doorAjarState,
                                    const SM_States                     *pLockCtrl_SM,
                                          uint16                        *pTimers,
                                          boolean                       *pisAutorelockTriggered);
static boolean isSpeedLockEvent(const Speed16bit_T            *pWheelBasedVehicleSpeed,
                                const EventUnlock_Enum        Unlock_event,
                                const DoorsAjar_stat_T        doorAjarState,
                                const DoorLatch_stat_T        *pdoorLatchStat_current);
static void LockingControlStateTransitions(const EventUnlock_Enum         Unlock_event,
                                           const EventLock_Enum           Lock_event,
                                           const boolean                  *pisAutorelockTriggered,
                                           const boolean                  isSysEventSpeedLock,
                                           const SelfLockAction_T         *pSelfLockAction,
                                           const boolean                  *pisLatchProtectionInactive,
                                           const Synch_Unsynch_Mode_stat  *pSynch_Unsynch_Mode,
                                           const DoorLatch_stat_T         *pDoorLatchStat_current,
                                                 SM_States                *pLockCtrl_SM,
                                           const uint16                   *pTimers_unlock,
                                           const uint16                   *pTimers_lock,
                                           const RemoteStatusIndication_Enum *pRemoteOperationStatusIndication);
static void LockingControlStateprocessing(SM_States                         *pLockCtrl_SM,
                                          VehicleAccess_Ctrl_out_StructType *pRteOutData_Common,
                                          uint16                            *pTimers_unlock,
                                          uint16                            *pTimers_lock);
static void Alarm_reactivationrequest(      boolean            *pisAutorelockTriggered,
                                      const SM_States          *pLockCtrl_SM,
                                            AutoRelock_rqst_T  *pAutoRelock_rqst,
                                            uint16             *pTimers_AutoRelock);
static void RemoteOperationStatusIndicating(const SM_States                 *pLockCtrl_SM,
                                            const DoorLatch_stat_T          *pDoorLatchStat_current,
                                            const VehicleMode_T             *pVehicleMode_current,
                                                  LockingIndication_rqst_T  *pLockingIndication_rqst,
                                                  uint16                    *pTimers,
                                                  RemoteStatusIndication_Enum *pRemoteOperationStatusIndication);
static void Get_RteInData_WithDoorModules(LatchWithDoorModules_in_StructType *pRteInData_WithDoorModulesLatch, 
                                          AjarWithDoorModules_in_StructType  *pRteInData_WithDoorModulesAjar);
static void Get_RteInDataRead_Common(VehicleAccess_Ctrl_in_StructType *pRteInData_Common);
static void RteInDataWrite_Common(const VehicleAccess_Ctrl_out_StructType *pRteOutData_common);
static void RteOutData_WithDoorModules(const VehicleAccess_Ctrl_out_StructType *pRteOutData_common);
static void ANW_LockControlActDeactivation(const SM_States *pLockCtrl_SM,
                                           const uint16    *pTimer);
static void ANW_UpdateDoorLatchStatus(const FormalBoolean *pisActivationTriggerDetected);
static void ANW_LockSwitchDashboardLedIndication(const uint16 *pTimer);
static void DoorLatchStateTransitionProcessing(const DoorLatch_stat_T      LatchStateIn,
                                                     DoorLatch_stat_T      *pLatchStateOut,
                                                     uint16                *pTimer_LatchIdleMonitor,
                                                     SelfLockAction_Enum   *pSelfLock_BackToState);
static void LatchProtectionProcessing(const SM_States *pLockCtrl_SM,
                                            boolean   *pLatchProtectionInactive,
									                 uint16    *ptimer_LatchProtection);

//=======================================================================================
// End of file
//=======================================================================================
#endif
