#ifdef FSPROUTER_CTRL_H
#error FSPROUTER_CTRL_H unexpected multi-inclusion
#else
#define FSPROUTER_CTRL_H
//=====================================================================================
// Included header files
//=====================================================================================

#include "FuncLibrary_AnwStateMachine_If.h"

//=======================================================================================
// Private constants and enum definitions
//=======================================================================================

//=======================================================================================
// Private macros
//=======================================================================================

#define CONST_TimeBase                 (20U)
#define CONST_LockControlCabRqst1      ((3000U) / CONST_TimeBase)
#define CONST_ASLight_InputFSP         ((3000U) / CONST_TimeBase)
#define CONST_WLight_InputFSP          ((3000U) / CONST_TimeBase)
#define CONST_InteriorLightsRqst1      ((3000U) / CONST_TimeBase)
#define CONST_FlexibleSwitchDetection  ((5000U) / CONST_TimeBase)
#define CONST_RoofHatchRequest1        ((30000U) / CONST_TimeBase)
#define CONST_PHActMaintainLiving2     ((10000U) / CONST_TimeBase)
#define CONST_ExtraBBTailLiftFSP2      ((10000U) / CONST_TimeBase)
#define CONST_ExtraBBAuxiliarySwitches ((10000U) / CONST_TimeBase)
#define CONST_CabTiltSwitchRequest     ((10000U) / CONST_TimeBase)
#define CONST_BlackoutConvoyMode       ((30000U) / CONST_TimeBase)
#define CONST_AlarmSetUnset1           ((45000U) / CONST_TimeBase)

#define CONST_MinimumSwDetectionTime   (uint8)((500U) / CONST_TimeBase)
#define CONST_MaximumSwDetectionTime   (uint8)((800U) / CONST_TimeBase)


#ifndef LEDPosition_Both
#define LEDPosition_Both              (0U)
#endif

#ifndef LEDPosition_Lower
#define LEDPosition_Lower             (1U)
#endif

#ifndef LEDPosition_Upper
#define LEDPosition_Upper             (2U)
#endif

#ifndef LEDPosition_NotAvailable
#define LEDPosition_NotAvailable      (3U)
#endif

#ifndef SLOT0_LOWER
#define SLOT0_LOWER                   (4U)
#endif

#ifndef SLOT0_UPPER
#define SLOT0_UPPER                   (0U)
#endif

#ifndef SLOT1_LOWER
#define SLOT1_LOWER                   (5U)
#endif

#ifndef SLOT1_UPPER
#define SLOT1_UPPER                   (1U)
#endif

#ifndef SLOT2_LOWER
#define SLOT2_LOWER                   (6U)
#endif 

#ifndef SLOT2_UPPER
#define SLOT2_UPPER                   (2U)
#endif

#ifndef SLOT3_LOWER
#define SLOT3_LOWER                   (7U)
#endif

#ifndef SLOT3_UPPER
#define SLOT3_UPPER                   (3U)
#endif

#ifndef MAX_SLOT_NO
#define MAX_SLOT_NO                   (8U)
#endif

#ifndef MAX_DETECTION_BUFFER_SIZE
#define MAX_DETECTION_BUFFER_SIZE     (3U)
#endif

#ifndef FSP1
#define FSP1                          (0U)
#endif

#ifndef FSP2
#define FSP2                          (1U)
#endif

#ifndef FSP3
#define FSP3                          (2U)
#endif

#ifndef FSP4
#define FSP4                          (3U)
#endif

#ifndef FSP5
#define FSP5                          (4U)
#endif

#ifndef FSP6
#define FSP6                          (5U)
#endif

#ifndef FSP7
#define FSP7                          (6U)
#endif

#ifndef FSP8
#define FSP8                          (7U)
#endif

#ifndef FSP9
#define FSP9                          (8U)
#endif

#ifndef FSPB
#define FSPB                          (9U)
#endif

#ifndef MAX_FSP_NO
#define MAX_FSP_NO                    (10U)
#endif

#ifndef MAX_FLEXIBLESWITCHID
#define MAX_FLEXIBLESWITCHID          (255U)
#endif

#ifndef MAX_ROCKERSWITCHID
#define MAX_ROCKERSWITCHID            (159U)
#endif

#ifndef MAX_PUSHSWITCHID
#define MAX_PUSHSWITCHID              ((MAX_FLEXIBLESWITCHID - MAX_ROCKERSWITCHID)U)
#endif

#ifndef MAX_SWITCHTYPE_BUFSIZE
#define MAX_SWITCHTYPE_BUFSIZE        (160U)
#endif

//=======================================================================================
// Private type definitions
//=======================================================================================

typedef enum
{ 
   SM_FSR_Idle,
   SM_FSR_SwitchDetectionProcessing,
   SM_FSR_SwitchDetectionCompleted,
   SM_FSR_NormalRouting
} FlexibleSwitchRoutingMode_Enum;

typedef struct
{
   uint8 FlexibleSwitchId[MAX_SLOT_NO];
} FlexibleSwitchConfigTable_T;

typedef struct
{
   SwitchDetectionNeeded_T SwitchDetectionNeeded[MAX_FSP_NO];
   PushButtonStatus_T      FSPSwStatus[MAX_FSP_NO][MAX_SLOT_NO]; // input from FlexibleSwitchesRouter_Ctrl_LINMstr
} FSR_Ctrl_Router_in_StructType;

typedef struct
{
   PushButtonStatus_T   WorkLight_ButtonStatuscurrent;
   PushButtonStatus_T   FifthWheelLight_DeviceEventcurrent;
   A3PosSwitchStatus_T  SideReverseLight_ButtonStatuscurrent;
   A2PosSwitchStatus_T  AuxSwitch1SwitchStatuscurrent;
   A2PosSwitchStatus_T  AuxSwitch2SwitchStatuscurrent;
   PushButtonStatus_T   ReducedSetModeButtonStatuscurrent;
} outputLocalstructure;

/*typedef struct
{
   DeviceIndication_T FSPIndicationCmd[MAX_FSP_NO][MAX_SLOT_NO]; // output to FlexibleSwitchesRouter_Ctrl_LINMstr
} FSR_Ctrl_Router_out_StructType;*/

//=======================================================================================
// Private data declarations
//=======================================================================================
// Note: MUST BE static
// Note: Shall not be used/avoided with AUTOSAR SWC.


//=======================================================================================
// Private function prototypes
//=======================================================================================
// Note : MUST BE static

// function declarations
static void RteInDataRead_Common(FSR_Ctrl_Router_in_StructType *pFSRCtrlRouterInData);
static boolean RteInDataRead_SwitchDetectionProcessing(uint8 RteInData_SwitchDetectionResponse[MAX_FSP_NO][sizeof(SwitchDetectionResp_T)]);
static void RteOutDataWrite_Common(const uint8                 pFlexibleSwitchStatus[255],
                                   const boolean               pFlexibleSwPresence[MAX_FLEXIBLESWITCHID],
                                         outputLocalstructure  *pFSR_output,
								   const uint8                  pFSPIndicationCmd[MAX_FSP_NO][sizeof(FSPIndicationCmdArray_T)]);
static void InitializeOutData(      uint8   pFSPIndicationCmd[MAX_FSP_NO][sizeof(FSPIndicationCmdArray_T)],
                                    uint8   pFlexibleSwitchStatus[MAX_FLEXIBLESWITCHID],
                              const Boolean pIsFlexibleSwPresence[MAX_FLEXIBLESWITCHID],
                              const Boolean isForced);
static void FlexibleSwitchesRouter_Ctrl_CopySwitchConfigTable(const uint8                       pRteInData_SwitchDetectionResp[MAX_FSP_NO][sizeof(SwitchDetectionResp_T)],
                                                                    uint8                       (*pTableOfDetectedId)[MAX_FSP_NO][MAX_SLOT_NO],
                                                                    Boolean                     pFlexibleSwPresence[MAX_FLEXIBLESWITCHID],
                                                                    uint8                       *pP1DCTdata,
                                                                    FlexibleSwitchesinFailure_T (*pFlexibleSwitchFailData)[24]);
static Boolean IsA3SwitchType(uint8 switchid);
static void FlexibleSwitchesRouter_Ctrl_StatusRouting(const uint8              (*pTableOfDetectedId)[MAX_FSP_NO][MAX_SLOT_NO],
                                                      const PushButtonStatus_T pRteInData_SwitchStatus[MAX_FSP_NO][MAX_SLOT_NO],
                                                            uint8              pFlexibleSwitchStatus[MAX_FLEXIBLESWITCHID],
                                                      const uint8              pRteInData_SwitchIndication[MAX_FLEXIBLESWITCHID],
                                                            uint8              pFSPIndicationCmd[MAX_FSP_NO][MAX_SLOT_NO]);
static A3PosSwitchStatus_T GetA3PosSwStatForRockerSw(const PushButtonStatus_T btnUpper,
                                                     const PushButtonStatus_T btnLower);
static void RteInDataRead_SwitchDetectionSampleProcessing(const uint8 RteInData_SwitchDetectionResp_Check[sizeof(Crypto_Function_serialized_T)],
                                                                uint8 *pDetectionProgressIndicator,
                                                                uint8 RteInData_SwitchDetectionRespns[sizeof(Crypto_Function_serialized_T)]);
static void RteInDataRead_FlexibleSwitchStatus(PushButtonStatus_T pRteInData_SwitchStatus[MAX_FSP_NO][MAX_SLOT_NO]);
static void RteInDataRead_FlexibleSwitchIndication(uint8 pRteInData_SwitchIndication[MAX_FLEXIBLESWITCHID]);
static void ANW_LockControlCabRqst(const FormalBoolean *pisActTriggerDetected,
                                   const FormalBoolean *pisDeActTriggerDetected);
static FormalBoolean ANW_LockControlCabRqst1ActTrigger(const uint8 *pDashboardLockButtonStatus,
                                                       const uint8 *pDoorLockSwitch_DeviceIndic);
static void ANW_ASLight_InputFSP(const FormalBoolean *pisActTriggerDetected,
                                 const FormalBoolean *pisDeActTriggerDetected);
static FormalBoolean ANW_ASLight_InputFSPActTrigger(const uint8 *pRoofSignLight_DeviceEvent,
                                                    const uint8 *pLEDVega_DeviceEvent,
                                                    const uint8 *pPloughLight_DeviceEvent,
                                                    const uint8 *pPloughtLightsPushButtonStatus);
static void ANW_WLight_InputFSP(const FormalBoolean *pisActTriggerDetected,
                                const FormalBoolean *pisDeActTriggerDetected);
static FormalBoolean ANW_WLight_InputFSPActTrigger(const uint8              *pBeacon_DeviceEven,
                                                   const uint8              *pBeaconSRocker_DeviceEvent,
                                                   const uint8              *pLEDVega_DeviceEvent,
                                                   const PushButtonStatus_T *pWorkLight_ButtonStatus,
                                                   const PushButtonStatus_T *pFifthWheelLight_DeviceEvent,
                                                   const uint8              *pEquipmentLight_DeviceEvent,
                                                   const uint8              *pSideReverseLight_SwitchStatus,
                                                   const PushButtonStatus_T *pSideReverseLight_ButtonStatus);
static void ANW_AlarmSetUnset1(const FormalBoolean *pisActTriggerDetected,
                               const FormalBoolean *pisDeActTriggerDetected);
static FormalBoolean ANW_AlarmSetUnset1ActTrigger(const PushButtonStatus_T *pReducedSetModeButtonStatus);
static void ANW_BlackoutConvoyMode(const FormalBoolean  *pisActTriggerDetected,
                                   const FormalBoolean  *pisDeActTriggerDetected);
static FormalBoolean ANW_BlackoutConvoyModeActTrigger(const uint8 *pBlackOutConvoyModeSwitchStatus);
static void ANW_CabTiltSwitchRequest(const FormalBoolean *pisActTriggerDetected,
                                     const FormalBoolean *pisDeActTriggerDetected);
static FormalBoolean ANW_CabTiltSwitchRequestActTrigger(const uint8 *pCabTilt_SwitchStatus);
static void ANW_ExtraBBAuxiliarySwitches(const FormalBoolean *pisActTriggerDetected,
                                         const FormalBoolean *pisDeActTriggerDetected);
static FormalBoolean ANW_ExtraBBAuxiliarySwitchesActTrigger(const A2PosSwitchStatus_T *pAuxSwitch1SwitchStatus,
                                                            const A2PosSwitchStatus_T *pAuxSwitch2SwitchStatus,
                                                            const uint8               *pAuxSwitch3SwitchStatus,
                                                            const uint8               *pAuxSwitch4SwitchStatus,
                                                            const uint8               *pAuxSwitch5SwitchStatus,
                                                            const uint8               *pAuxSwitch6SwitchStatus);
static void ANW_ExtraBBTailLiftFSP2(const FormalBoolean *pisActTriggerDetected,
                                    const FormalBoolean *pisDeActTriggerDetected);
static FormalBoolean ANW_ExtraBBTailLiftFSP2ActTrigger(const uint8 *pTailLiftSwitchStatus,
                                                       const uint8 *pTailLiftPushButtonStatus,
                                                       const uint8 *pCraneSwitchStatus,
                                                       const uint8 *pCranePushButtonStatus);
static void ANW_PHActMaintainLiving2(const FormalBoolean *pisActTriggerDetected,
                                     const FormalBoolean *pisDeActTriggerDetected);
static FormalBoolean ANW_PHActMaintainLiving2ActTrigger(const uint8 *pBunkB1ParkHeaterBtn_stat,
                                                        const uint8 *pBunkB1ParkHeaterTempSw_stat);
static FormalBoolean ANW_PHActMaintainLiving2DeActTrigger(const uint8 *pBunkB1ParkHeaterTempSw_stat);
static void ANW_RoofHatchRequest1(const FormalBoolean *pisActTriggerDetected,
                                  const FormalBoolean *pisDeActTriggerDetected);
static FormalBoolean ANW_RoofHatchRequest1ActTrigger(const uint8 *pRoofHatch_SwitchStatus_1,
                                                     const uint8 *pRoofHatch_SwitchStatus_2);
static FormalBoolean ANW_RoofHatchRequest1DeActTrigger(const uint8 *pRoofHatch_SwitchStatus_1,
                                                       const uint8 *pRoofHatch_SwitchStatus_2);
static void ANW_corefunctionlogic(const uint8                *pDoorLockSwitch_DeviceIndic,
                                  const uint8                FlexibleSwitchStat[MAX_FLEXIBLESWITCHID],
                                  const outputLocalstructure *pFSR_output,
                                  const ComMode_LIN_Type     *pComMode_LIN2,
                                  const Boolean              DetectionStatus_switch);
static void UpdateFailureTypeDID(const uint8                       TypeofFailure,
                                 const uint8                       SwitchID,
                                 const uint8                       SlotNo,
                                 const uint8                       FspNo,
                                       FlexibleSwitchesinFailure_T (*FlexibleSwitchFailureData)[24]);
static FormalBoolean ANW_FlexibleSwitchDetectionActTrigger(const ComMode_LIN_Type *pComMode_LIN2);
static FormalBoolean ANW_FlexibleSwitchDetectionDeActTrigger(const ComMode_LIN_Type   *pComMode_LIN2);
static void ANW_FlexibleSwitchDetection(const FormalBoolean *pisActTriggerDetected,
                                        const FormalBoolean *pisDeActTriggerDetected);
static void ANW_InteriorLightsRqst1(const FormalBoolean *pisActTriggerDetected,
                                    const FormalBoolean *pisDeActTriggerDetected);
static FormalBoolean ANW_InteriorLightsRqst1_ActTrigger(const A3PosSwitchStatus_T *pIntLghtDoorAutoMaxModeBtn_stat,
                                                        const A2PosSwitchStatus_T *pIntLghtActvnBtn_stat,
                                                        const A2PosSwitchStatus_T *pIntLghtNightModeBtn_stat,
                                                        const PushButtonStatus_T  *pIntLghtNightModeFixSw2_stat,
                                                        const PushButtonStatus_T  *pIntLghtMaxModeFixSw2_stat);
static void DoubleSwitchProcessing(const uint8                       pTableOfDetectedId[MAX_FSP_NO][sizeof(SwitchDetectionResp_T)],
                                         uint8                       *pP1DCTData,
                                         FlexibleSwitchesinFailure_T FlexibleSwFailData[24]);
static void FS_SwitchPresenceDiagnosticReport(const boolean                     pFlexibleSwPresence[MAX_FLEXIBLESWITCHID],
                                                    FlexibleSwitchesinFailure_T (*pFlexibleSwitchFailedData)[24],
                                                    uint8                       (*pFS_DisableDiagnosticPresence)[5],
													uint8                       pFlexibleSwitchStatus[MAX_FLEXIBLESWITCHID]);
static A2PosSwitchStatus_T GetA2PosSwStatForRockerSw(const PushButtonStatus_T  btnUpper,
                                                     const PushButtonStatus_T  btnLower);

//=======================================================================================
// End of file
//=======================================================================================
#endif

