#ifdef BUNKUSERINTERFACEHIGH2_LINMACTRL_H
#error BUNKUSERINTERFACEHIGH2_LINMACTRL_H unexpected multi-inclusion
#else
#define BUNKUSERINTERFACEHIGH2_LINMACTRL_H
//=====================================================================================
// Included header files
//=====================================================================================


//=====================================================================================
// Private constants and enum definitions
//=====================================================================================

//=====================================================================================
// Private macros
//=====================================================================================

 
#ifndef EventFlag_Error 
#define EventFlag_Error                          (1U)      //No Enum value for EventFlag Error(Need Clarification from Volvo Team)
#endif

#ifndef EventFlag_NotAvailable
#define EventFlag_NotAvailable                   (1U)      //No Enum value for EventFlag NotAvailable(Need Clarification from Volvo Team)
#endif

#ifndef TimeMinuteType_NotAvailable 
#define TimeMinuteType_NotAvailable              (63U)     //As per LDS
#endif

#ifndef TimeMinuteType_Error
#define TimeMinuteType_Error                     (62U)     //As per LDS
#endif

#ifndef VolumeValueType_Error
#define VolumeValueType_Error                    (63U)     //Taken spare value(Need Clarification from Volvo Team)
#endif

#ifndef VolumeValueType_NotAvailable   
#define VolumeValueType_NotAvailable             (63U)     //Taken spare value(Need Clarification from Volvo Team)
#endif

#ifndef Hours8bit_Error   
#define Hours8bit_Error                          (254U)    //As per LDS
#endif

#ifndef Hours8bit_NotAvailable
#define Hours8bit_NotAvailable                   (255U)    //As per LDS
#endif

#ifndef Minutes8bit_Error    
#define Minutes8bit_Error                        (254U)    //As per LDS
#endif

#ifndef Minutes8bit_NotAvailable
#define Minutes8bit_NotAvailable                 (255U)    //As per LDS
#endif

#ifndef TimesetHr_Error
#define TimesetHr_Error                          (30U)     //As per LDS
#endif

#ifndef TimesetHr_NotAvailable 
#define TimesetHr_NotAvailable                   (31U)     //As per LDS
#endif

#ifndef IntLghtLvlIndScaled_cmd_Error
#define IntLghtLvlIndScaled_cmd_Error            (14U)     //As per LDS
#endif

#ifndef IntLghtLvlIndScaled_cmd_NotAvailable
#define IntLghtLvlIndScaled_cmd_NotAvailable     (15U)     //As per LDS
#endif

#define CONST_Fault_InActive                      (0U)
#define CONST_Fault_Active                        (1U)
#define CONST_SlaveFCIFault_InActive              (0U)
#define CONST_SlaveFCIFault_Active                (1U)

//=====================================================================================
// Private type definitions
//=====================================================================================

//=====================================================================================
// Private data declarations
//=====================================================================================

//=====================================================================================
// Private function prototypes
//=====================================================================================

static void Get_RteDataRead_Common(BunkUserInterfaceHigh2_LINMaCtrl_in_StructType            *pInput_data,
                                   BunkUserInterfaceHigh2_LINMaCtrl_ButtonCOM_in_StructType  *pIn_ButtonCOM_data,
                                   BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_in_StructType *pInput_WindowLift_data,
                                   ReturnType                                                *pReturnValue);
static void RteDataWrite_Common(const BunkUserInterfaceHigh2_LINMaCtrl_out_StructType             *poutput_data,
                                const BunkUserInterfaceHigh2_LINMaCtrl_Button_out_StructType      *poutput_button_data,
                                const BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_out_StructType  *pOutput_window_data);
static void  Deactivation_Logic_BunkUserInterfaceHigh2(BunkUserInterfaceHigh2_LINMaCtrl_out_StructType            *poutputDeAct_data,
	                                                   BunkUserInterfaceHigh2_LINMaCtrl_Button_out_StructType     *pOutputDeAct_Button_data,
	                                                   BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_out_StructType *poutputDeAct_Windowlift_data);
static boolean  BunkUserInterfaceHigh2_LECM_H_FCI_To_DTC_conv(const DiagInfo_T *pDiagInforLECM2);
static void  FallbackLogic_BusOff_BunkUserInterfaceHigh2(BunkUserInterfaceHigh2_LINMaCtrl_Button_out_StructType *pOutput_Busoff_Button_data);
static void  FallbackLogic_ParkingHeater_BunkUserInterfaceHigh2(SetParkHtrTmr_rqst_T *pBunkH2PHTimer_rqst);
static void  FallbackLogic_WindowLift_BunkUserInterfaceHigh2(BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_out_StructType *pout_WindowLift_data);
static void  SignalsGateway_StatusIndication_BunkUserInterfaceHigh2(const BunkUserInterfaceHigh2_LINMaCtrl_in_StructType  *pBunkH2_Logic_in_data,
                                                                          BunkUserInterfaceHigh2_LINMaCtrl_out_StructType *pBunkH2_Logic_out_data);
static void  SignalsGateway_CabComfort_BunkUserInterfaceHigh2(const BunkUserInterfaceHigh2_LINMaCtrl_ButtonCOM_in_StructType *pBH2in_ButtonCOM_data,
                                                                    BunkUserInterfaceHigh2_LINMaCtrl_Button_out_StructType   *pBH2_Logic_out_data);
static void  SignalsGateway_WindowLift_BunkUserInterfaceHigh2(const BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_in_StructType  *pBunkH2_WLLogic_in_data,
                                                                    BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_out_StructType *pBunkH2_WLLogic_out_data);
static void  SignalsGateway_ParkingHeater_BunkUserInterfaceHigh2(const SetParkHtrTmr_rqst   *pInBunkH2PHTimer_rqst,
                                                                       SetParkHtrTmr_rqst_T *pBunkH2PHTimer_rqst);

//=====================================================================================
// End of file
//=====================================================================================
#endif
