#ifdef BUNKUSERINTERFACEBASIC_LINMACTRL_H
#error BUNKUSERINTERFACEBASIC_LINMACTRL_H unexpected multi-inclusion
#else
#define BUNKUSERINTERFACEBASIC_LINMACTRL_H
//=====================================================================================
// Included header files
//=====================================================================================


//=====================================================================================
// Private constants and enum definitions
//=====================================================================================


//=====================================================================================
// Private macros
//=====================================================================================


#define CONST_BunkUserIFBasicCycleTime    (10U)
#define CONST_AudioRadio3Timeout          ((10000U) / CONST_BunkUserIFBasicCycleTime)
#define CONST_OtherInteriorLights5Timeout ((30000U) / CONST_BunkUserIFBasicCycleTime)
#define CONST_PHActMaintainLiving5Timeout ((10000U) / CONST_BunkUserIFBasicCycleTime)
#define CONST_Fault_InActive              (0U)
#define CONST_Fault_Active                (1U)
#define CONST_SlaveFCIFault_InActive      (0U)
#define CONST_SlaveFCIFault_Active        (1U)

//=====================================================================================
// Private type definitions
//=====================================================================================
typedef struct
{
   PushButtonStatus_T Previous;
   PushButtonStatus_T Current;
} PushButtonStatus;

// Input Structure 
typedef struct
{
   PushButtonStatus           LIN_BunkBAudioOnOff_ButtonStat;
   PushButtonStatus           LIN_BunkBIntLightActvnBtn_stat;
   PushButtonStatus           LIN_BunkBParkHeater_ButtonStat;
   PushButtonStatus           LIN_BunkBTempDec_ButtonStat;
   PushButtonStatus           LIN_BunkBTempInc_ButtonStat;
   PushButtonStatus           LIN_BunkBVolumeDown_ButtonStat;
   PushButtonStatus           LIN_BunkBVolumeUp_ButtonStat;
   ComMode_LIN_Type           ComMode_LIN1;
   DiagInfo_T                 DiagInfoLECM_Basic;
   ResponseErrorLECMBasic_T   ResponseErrorLECM_Basic;
} BUIBasic_LINMaCtrl_In_struct_Type;

// Output Structure 
typedef struct
{
   PushButtonStatus_T  BunkBAudioOnOff_ButtonStatus;
   PushButtonStatus_T  BunkBIntLightActvnBtn_stat;
   PushButtonStatus_T  BunkBParkHeater_ButtonStatus;
   PushButtonStatus_T  BunkBTempDec_ButtonStatus;
   PushButtonStatus_T  BunkBTempInc_ButtonStatus;
   PushButtonStatus_T  BunkBVolumeDown_ButtonStatus;
   PushButtonStatus_T  BunkBVolumeUp_ButtonStatus;
} BUIBasic_LINMaCtrl_Out_struct_Type;

//=====================================================================================
// Private data declarations
//=====================================================================================

//=====================================================================================
// Private function prototypes
//=====================================================================================

static Std_ReturnType Get_RteDataRead_Common(BUIBasic_LINMaCtrl_In_struct_Type *pRteInput_Data);
static void Signals_GatewayLogic_BunkUserInterfaceBasic(const BUIBasic_LINMaCtrl_In_struct_Type  *pInData,
                                                              BUIBasic_LINMaCtrl_Out_struct_Type *pOutData);
static FormalBoolean Generic_ANW_AudioRadio3_CondCheck(const PushButtonStatus *pLIN_BunkBVolumeUp_ButtonStat,
                                                       const PushButtonStatus *pLIN_BunkBVolumeDown_ButtonStat,
                                                       const PushButtonStatus *pLIN_BunkBAudioOnOff_ButtonStat);
static void Generic_ANW_AudioRadio3_Trigger(const FormalBoolean isRestAct_AudioRadio3,
                                            const FormalBoolean isActTrigDet_AudioRadio3);
static FormalBoolean GenericANW_OtherIntLights5_Cond(const PushButtonStatus *pLIN_BunkBIntLightActvnBtn_stat);
static void GenericANW_OtherInteriorLights5(const FormalBoolean isRestAct_InteriorLights5,
                                            const FormalBoolean isActTrigDet_InteriorLights5);
static FormalBoolean ANW_PHActMaintain_ActiveCond(const PushButtonStatus *pDeAct_LIN_BunkBTempDec_ButtonStat,
                                                  const PushButtonStatus *pDeAct_LIN_BunkBTempInc_ButtonStat,
                                                  const PushButtonStatus *pLIN_BunkBParkHeater_ButtonStat);
static void GenericANW_PHActMaintainLiving5(const FormalBoolean isRestAct_PHActMaintain,
	                                                 const FormalBoolean isActTrigDet_PHActMaintain,
	                                                 const FormalBoolean isDeActTrigDet_PHActMaintain);
static boolean BunkUserInterfaceBasic_FCI_Indication(const DiagInfo_T *pDiagInfoLECM_Basic);
static void FallbackLogic_BunkUserInterfaceBasic(BUIBasic_LINMaCtrl_Out_struct_Type *pOutData_Fallback);
static void Deactivate_Logic_BunkUserInterfaceBasic(BUIBasic_LINMaCtrl_Out_struct_Type *pOutData_DeAct);
static void RteDataWrite_Common(const BUIBasic_LINMaCtrl_Out_struct_Type *pOutput_data);
static FormalBoolean ANW_PHActMaintain_DeActiveCond(const PushButtonStatus *pLIN_BunkBTempDec_ButtonStat,
                                                    const PushButtonStatus *pLIN_BunkBTempInc_ButtonStat);

//=====================================================================================
// End of file
//=====================================================================================
#endif

