/*
 ***************************************************************************************
 ***************************************************************************************
 ***
 ***     File: SCIM_PowerSupply12V_Hdlr.h
 ***
 ***     Project: SCIM
 ***
 ***     
 ***
 ***************************************************************************************
 ***************************************************************************************
 */

/*!
 ***************************************************************************************
 * \file
 *
 * \ingroup application_swc
 *
 * \subject
 * Definition file for the SCIM_PowerSupply12V_Hdlr runnables.
 *
 * \version
 * 
 *
 * \{
 ***************************************************************************************
 */
#ifndef SRE_SCIM_PowerSupply12V_Hdlr_H
#define SRE_SCIM_PowerSupply12V_Hdlr_H

/*
 **=====================================================================================
 ** Included header files
 **=====================================================================================
 */

#include "Rte_Type.h"

/*
 **=====================================================================================
 ** Public macros
 **=====================================================================================
 */


/*
 **=====================================================================================
 ** Public definitions
 **=====================================================================================
 */

#ifndef REVERSE_ON
   #define REVERSE_ON (FALSE)
#endif

#ifndef REVERSE_OFF
   #define REVERSE_OFF (TRUE)
#endif

// Living12VPowerStability: Enumeration of integer in interval [0...255] with enumerators
#ifndef LIVING12VPOWERSTABILITY_INACTIVE
   #define LIVING12VPOWERSTABILITY_INACTIVE     (0u)
#endif

#ifndef LIVING12VPOWERSTABILITY_ACTIVE
   #define LIVING12VPOWERSTABILITY_ACTIVE       (1u)
#endif

#ifndef LIVING12VPOWERSTABILITY_STABLE
   #define LIVING12VPOWERSTABILITY_STABLE       (2u)
#endif

#ifndef LIVING12VPOWERSTABILITY_ERROR
   #define LIVING12VPOWERSTABILITY_ERROR        (3u)
#endif

#define DCDC12V_VOLTAGE_8V_0V2   (40u)
#define DCDC12V_VOLTAGE_18V_0V2  (90u)

/* timers*/
#define CONST_RunnableTimeBase                     (10U)
#define CONST_12VLivingOutputDeactivate_Timer      (0U)
#define CONST_12VLivingOutputrResetRequest_Timer   (1U)
#define CONST_NbOfTimers                           (2U)

// Functional constants
#define CONST_12VLivingOutputDeactivatedTimeout    ((10000U)/CONST_RunnableTimeBase) /* 30 seconds */
#define CONST_12VLivingOutputResetRequestTimeout   ((2000U)/CONST_RunnableTimeBase) /* 2 seconds */
#define CONST_LINIssActivate                       (uint32)(0x80001F00)

#define CONST_DCDC12V_STABLE_TIME_CNT              ((200U)/CONST_RunnableTimeBase) /* 200 milli-seconds */

/*
 **=====================================================================================
 ** Public type definitions
 **=====================================================================================
 */

typedef enum
{
   SM_Living12VPowerStability_Inactive = 0,
   SM_Living12VPowerStability_Active,
   SM_Living12VPowerStability_Stable,
   SM_Living12VPowerStability_Error
} Living12VPowerStability_States_Enum;

typedef enum
{
   SM_PowerSupply12V_Hdlr_NoInit = 0,
   SM_PowerSupply12V_Hdlr_Running
} SM_PowerSupply12V_Hdlr_Enum;


typedef struct
{
   Boolean     Current;
   Boolean     New;
} BooleanVar_T;

typedef struct
{
   Living12VPowerStability Current;
   Living12VPowerStability Previous;
} Living12VPowerStabilityVar_T;



/**********************************************************************************************************************
 * Data type definitions 
 *********************************************************************************************************************/

 


/*
 **=====================================================================================
 ** Public data declarations
 **=====================================================================================
 */

/*
 **=====================================================================================
 ** Public function declarations
 **=====================================================================================
 */

static Living12VPowerStability Living12VPowerStabilityHandler(void);


/*! \cond SKIP_FUNCTION_PROTOTYPES */


/*! \endcond */
#endif /* SRE_SCIM_PowerSupply12V_Hdlr_H */

/*
 **=====================================================================================
 ** End of file
 **=====================================================================================
 */
/*!
 * \}
 ************************************ End of file **************************************
 */

