#ifdef FuncLibrary_FuncLibrary_Timer_H
#error FuncLibrary_FuncLibrary_Timer_H unexpected multi-inclusion
#else
#define FuncLibrary_FuncLibrary_Timer_H

#include "Platform_Types.h"
//=======================================================================================
// Public type definitions
//=======================================================================================

//=======================================================================================
// Public constants and enums definitions
//=======================================================================================

// Definions for the unitary test only: the application shall define those constants
// and variables in calling module.
#ifdef VALIDATE_TEMPLATE
   #define CONST_Name1Timer                     0
   #define CONST_Name2Timer                     1
   #define CONST_NbOfTimers                     2
   
   #define CONST_RunnableTimeBase               20 //ms
   #define CONST_TimeoutName3                   60/CONST_RunnableTimeBase 
   #define CONST_TimeoutName4                   80/CONST_RunnableTimeBase 
#endif

//=======================================================================================
// Public data declarations
//=======================================================================================
// Note: Shall not be used with AUTOSAR SWC.

//=======================================================================================
// Public macros
//=======================================================================================
// Note, coding convention: shall be avoided to support debug capability.

//=======================================================================================
// Public function prototypes
//=======================================================================================

#endif // FuncLibrary_FuncLibrary_Timer_If_H                              

//! @}
//! @}

//=======================================================================================
// End of file
//=======================================================================================