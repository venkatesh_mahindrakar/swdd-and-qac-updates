#ifndef FuncLibrary_FuncLibrary_Debug_If_H
// To protect against multi-inclusion : FuncLibrary_FuncLibrary_Debug_If_H must be unique
#define FuncLibrary_FuncLibrary_Debug_If_H

#include "Platform_Types.h"
//=======================================================================================
// Public type definitions
//=======================================================================================

//=======================================================================================
// Public constants and enums definitions
//=======================================================================================

//! This Debug value indicates/set the inactive Debug (will not be decremented).
#define Write_SCIM_RD_Generic1    (Debug_SCIM_RD_Generic1 & 0x3F)
#define Write_SCIM_RD_Generic2    (Debug_SCIM_RD_Generic2 & 0x0F)
#define Write_SCIM_RD_Generic3    (Debug_SCIM_RD_Generic3 & 0x3F)
#define Write_SCIM_RD_Generic4    (Debug_SCIM_RD_Generic4 & 0xFF)
#define Write_SCIM_RD_Generic5    (Debug_SCIM_RD_Generic5 & 0x01)
#define Write_SCIM_RD_Generic6    (Debug_SCIM_RD_Generic6 & 0x1F)

//=======================================================================================
// Public data declarations
//=======================================================================================
// Note: Shall not be used with AUTOSAR SWC.

//=======================================================================================
// Public macros
//=======================================================================================
// Note, coding convention: shall be avoided to support debug capability.

//=======================================================================================
// Public function prototypes
//=======================================================================================

#endif // FuncLibrary_FuncLibrary_Debug_If_H                              

//=======================================================================================
// End of file
//=======================================================================================