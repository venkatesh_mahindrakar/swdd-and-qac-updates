/*
 ***************************************************************************************
 ***************************************************************************************
 ***
 ***     File: FaultEventGateway_ctrl.h
 ***
 ***     Project: SCIM
 ***
 ***     
 ***
 ***************************************************************************************
 ***************************************************************************************
 */

/*!
 ***************************************************************************************
 * \file
 *
 * \ingroup application_swc
 *
 * \subject
 * Definition file for the FaultEventGateway_ctrl runnables.
 *
 * \version
 * 
 *
 * \{
 ***************************************************************************************
 */
 
#ifdef FaultEventGateway_ctrl_H
#error FaultEventGateway_ctrl_H unexpected multi-inclusion
#else
#define FaultEventGateway_ctrl_H

/*
 **=====================================================================================
 ** Included header files
 **=====================================================================================
 */
#include "Rte_Type.h"
/*
 **=====================================================================================
 ** Public definitions
 **=====================================================================================
 */
 
 /*
 **=====================================================================================
 ** Public macros
 **=====================================================================================
 */
#define CONST_NbOfTimers                 1U
#define CONST_Runnable_Time_Base         20U
#define CONST_Timer_100ms                (100U/CONST_Runnable_Time_Base)
#define CONST_Timer_1000ms               (1000U/CONST_Runnable_Time_Base)
#define DTC_DtcID_Shift                  8U
#define DTC_DtcID_MASK                   0xFFFF00U
#define DTC_FAILID_MASK                  0x0000FFU
#define MAX_NO_OF_REGISTER_DTC           30U
#define FAULT_DTC_MASK_BYTE              0x09U
#define ACTIVE_DTC_MASK_BYTE             0x01U
#define INACTIVE_DTC_MASK_BYTE           0x08U
#define MAX_NO_OF_CRITICAL_DTC           30U
#define Critical_DTC_ID_None             0u
#define NVM_WRITEALLREQ_FLAG_IDLE        0U
#define NVM_WRITEALLREQ_FLAG_TRIGGERED   1U
#define NVM_WRITEALLREQ_FLAG_COMPLETED   2U
#define NO_OF_DTC_TIMER_LIMIT            20U
#define SCIM_ECU_ID                      64U

/*
 **=====================================================================================
 ** Public type definitions
 **=====================================================================================
 */

typedef struct
{
   uint8  NoOfDTCs;
   uint32 DTC_ID[MAX_NO_OF_REGISTER_DTC];
}FaultEventGateway_RegDTC_Type;
/*
 **=====================================================================================
 ** Public function declarations
 **=====================================================================================
*/

static void SearchAndRemoveDtcElement(FaultEventGateway_RegDTC_Type *RegDTC,
                                      uint32                        DTC);
static void FEG_Ctrl_OutputProcessing(      uint8           Trigger ,
                                      const DiagFaultStat_T *DiagFaultStat);

#endif /* FaultEventGateway_ctrl_H */

/*
 **=====================================================================================
 ** End of file
 **=====================================================================================
 */
/*!
 * \}
 ************************************ End of file **************************************
 */

