#ifdef SPEEDCONTROLFREEWHEEL_LINMASCTRL_H
#error SPEEDCONTROLFREEWHEEL_LINMASCTRL_H unexpected multi-inclusion
#else
#define SPEEDCONTROLFREEWHEEL_LINMASCTRL_H
//=====================================================================================
// Included header files
//=====================================================================================

//=====================================================================================
// Private constants and enum definitions
//=====================================================================================

//=====================================================================================
// Private macros
//=====================================================================================
#define CONST_Tester_Present         (0x80U)
#define CONST_Tester_Notpresent      (0x7FU)
#define CONST_Fault_InActive         (0U)
#define CONST_Fault_Active           (1U)
#define CONST_SlaveFCIFault_InActive (0U)
#define CONST_SlaveFCIFault_Active   (1U)

//=====================================================================================
// Private type definitions
//=====================================================================================

// Input structure 
typedef struct
{
   PushButtonStatus_T   LIN_SpeedControlModeButtonStat;
   FreeWheel_Status_T   LIN_SpeedControlModeWheelStat;
   PushButtonStatus_T   LIN_LKSPushButton;
   PushButtonStatus_T   LIN_FCWPushButton;
   DeviceIndication_T   ACCOrCCIndication;
   DeviceIndication_T   ASLIndication;
   DeviceIndication_T   FCW_DeviceIndication;
   DeviceIndication_T   LKS_DeviceIndication;
   ResponseErrorCCFW    CCFW_ResponseError;
   DiagInfo_T           DiagInfoCCFW;
   ComMode_LIN_Type     ComMode_LIN4;
   DiagActiveState_T    isDiagActive;
} SpeedCtrlFreeWheel_LINMasCtrl_in_StructType;
 
// Output structure 
typedef struct
{
   PushButtonStatus_T   SpeedControlModeButtonStatus;
   FreeWheel_Status_T   SpeedControlModeWheelStatus;
   PushButtonStatus_T   LKSPushButton;
   PushButtonStatus_T   FCWPushButton;
   DeviceIndication_T   LIN_ACCOrCCIndication;
   DeviceIndication_T   LIN_ASLIndication;
   DeviceIndication_T   LIN_FCW_DeviceIndication;
   DeviceIndication_T   LIN_LKS_DeviceIndication;
} SpeedCtrlFreeWheel_LINMastCtrl_out_StructType;

//=====================================================================================
// Private data declarations
//=====================================================================================

//=====================================================================================
// Private function prototypes
//=====================================================================================

static void IOCtrlService_LinOutputSignalControl(const uint8                                         OverrideData,
                                                       SpeedCtrlFreeWheel_LINMastCtrl_out_StructType *pOut_Struct_Data);
static Std_ReturnType Get_RteDataRead_Common(SpeedCtrlFreeWheel_LINMasCtrl_in_StructType *pIn_data);
static void RteDataWrite_Common(const SpeedCtrlFreeWheel_LINMastCtrl_out_StructType *pOut_data);
static boolean SpeedCtrlFreeWheel_CCFW_FCI_to_DTC(const DiagInfo_T *pDiagInfoCCFW);
static void SpeedControlFreeWheel_FallbackLogic(PushButtonStatus_T  *pFCWPushButton,
                                                PushButtonStatus_T  *pLKSPushButton,
                                                PushButtonStatus_T  *pSpeedControlModeButtonStatus,
                                                FreeWheel_Status_T  *pSpeedControlModeWheelStatus);
static void SpeedControlFreeWheel_Signals_GatewayLogic(const SpeedCtrlFreeWheel_LINMasCtrl_in_StructType     *pSpeedCtrlFreeWheel_in_data,
                                                          SpeedCtrlFreeWheel_LINMastCtrl_out_StructType   *pSpeedCtrlFreeWheel_out_data);
static void SpeedControlFreeWheel_Deactivation_Logic(SpeedCtrlFreeWheel_LINMastCtrl_out_StructType *pOutput_data);

//=====================================================================================
// End of file
//=====================================================================================
#endif

