//!======================================================================================
//! \file VehicleModeDisgtribution.h
//!
//! @addtogroup Application
//! @{
//!
//! \brief
//! VehicleModeDisgtribution SWC.
//! ASIL Level : QM.
//! This module implements the logic for the timer library function.
//!======================================================================================

#ifndef FuncLibrary_FuncLibrary_Timer_H
// To protect against multi-inclusion : FuncLibrary_FuncLibrary_Timer_If_H must be unique
#define FuncLibrary_FuncLibrary_Timer_H

#include "Platform_Types.h"


//=======================================================================================
// define
//=======================================================================================

// declare temporary

// Living12VPowerStability: Enumeration of integer in interval [0...255] with enumerators
#ifndef LIVING12VPOWERSTABILITY_INACTIVE
   #define LIVING12VPOWERSTABILITY_INACTIVE     (0u)
#endif

#ifndef LIVING12VPOWERSTABILITY_ACTIVE
   #define LIVING12VPOWERSTABILITY_ACTIVE       (1u)
#endif

#ifndef LIVING12VPOWERSTABILITY_STABLE
   #define LIVING12VPOWERSTABILITY_STABLE       (2u)
#endif

#ifndef LIVING12VPOWERSTABILITY_ERROR
   #define LIVING12VPOWERSTABILITY_ERROR        (3u)
#endif

#define CONST_VehicleModeNvramValuePos (1u)
#define CONST_VehicleModeNvramVersion  (1u)

#define CONST_ANW_CIOMOperStateRedundancyTimeout (3u*1000u/20u)

//=======================================================================================
// Public type definitions
//=======================================================================================

typedef struct
{
   VehicleMode_T   Current;
   VehicleMode_T   Previous;
} VehicleModeVar_T;

typedef struct
{
   Living12VPowerStability Current;
   Living12VPowerStability Previous;
} Living12VPowerStabilityVar_T;

/*!
* Contains all data from the RTE to the DriverAuthentication2_Ctrl logical function
*/
typedef struct
{
   Fsc_OperationalMode_T         FscMode;
   Living12VPowerStabilityVar_T  Living12VPowerStabilityVar;
   VehicleMode_T                 VehicleMode;
} VehicleModeDistribution_InData_T;

/*!
* Contains all data from the DriverAuthentication2_Ctrl logical function to the RTE
*/
typedef struct
{
   VehicleModeDistribution_T  SwcActivation_Accessory;
   VehicleModeDistribution_T  SwcActivation_EngineRun;
   VehicleModeDistribution_T  SwcActivation_IgnitionOn;
   VehicleModeDistribution_T  SwcActivation_LIN;
   VehicleModeDistribution_T  SwcActivation_Living;
   VehicleModeDistribution_T  SwcActivation_Parked;
   VehicleModeDistribution_T  SwcActivation_Security;
   VehicleModeVar_T           VehicleModeInternal;
}VehicleModeDistribution_OutData_T;


//=======================================================================================
// Public constants and enums definitions
//=======================================================================================
// Definions for the unitary test only: the application shall define those constants
// and variables in calling module.

//=======================================================================================
// Public data declarations
//=======================================================================================
// Note: Shall not be used with AUTOSAR SWC.

//=======================================================================================
// Public macros
//=======================================================================================
// Note, coding convention: shall be avoided to support debug capability.

//=======================================================================================
// Public function prototypes
//=======================================================================================

static void RteDataRead_Common (VehicleModeDistribution_InData_T *pRteInData_Common);
static void RteDataWrite_Common (VehicleModeDistribution_OutData_T *pRteOutData_Common);

static void VehicleModeInternal_OutputProcessing (const VehicleMode_T *pVehicleMode, VehicleModeVar_T *pVehicleModeInternal);
static void SWCActivation_OutputProcessing (const VehicleModeDistribution_InData_T *RteInData_Common, VehicleModeDistribution_OutData_T *RteOutData_Common);
static void ANW_CIOMOperStateRedundancy (VehicleModeVar_T *pVehicleModeInternal);

#endif // FuncLibrary_FuncLibrary_Timer_If_H                              

//! @}
//! @}

//=======================================================================================
// End of file
//=======================================================================================
