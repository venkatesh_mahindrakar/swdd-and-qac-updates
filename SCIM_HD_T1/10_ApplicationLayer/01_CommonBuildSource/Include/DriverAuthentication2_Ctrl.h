#ifdef DRIVERAUTHENTICATION2_CTRL_H
#error DRIVERAUTHENTICATION2_CTRL_H unexpected multi-inclusion
#else
#define DRIVERAUTHENTICATION2_CTRL_H
//=====================================================================================
// Included header files
//=====================================================================================


//=======================================================================================
// Private constants and enum definitions
//=======================================================================================


//=======================================================================================
// Private macros
//=======================================================================================

#define CONST_KeyfobAuthRqst_TimePeriod         ((80U) / CONST_RunnableTimeBase)   // configurable parameter
#define CONST_KeyDeauthentication_Period        ((2000U) / CONST_RunnableTimeBase) // configurable parameter
#define CONST_VIN_CheckTimeout                  ((3000U) / CONST_RunnableTimeBase) // configurable parameter
#define CONST_ANW_ImmobilizerTimeout            ((10000U) / CONST_RunnableTimeBase) // configurable parameter


#define CONST_RunnableTimeBase                  (20U)
#define CONST_KeyfobAuth_Rqst_Timer             (0U)
#define CONST_KeyDeauthentication_Timer         (1U)
#define CONST_VIN_CheckTimer                    (2U)
#define CONST_NbOfTimers                        (3U)

#define VIN_size                                (11U)

//=======================================================================================
// Private type definitions
//=======================================================================================

typedef enum  
{
   SM_Authorisation_NotAuthorised_Idle  = 0,
   SM_Authorisation_NotAuthorised_Fail  = 1,
   SM_Authorisation_Authorised          = 2,
   SM_Authorisation_TemporaryAuthorised = 3
} AuthorisationState_Enum;

typedef enum
{
   EVENT_KeyAuthenticationIdle       = 0,
   EVENT_KeyAuthenticationFail       = 1,
   EVENT_KeyAuthenticationSuccessful = 2
} KeyAuthenticationEvent_Enum;

typedef enum
{
   SM_VINCheckIdle       = 0,
   SM_VINCheckPending    = 1,
   SM_VINCheckSuccessful = 2,
   SM_VINCheckFail       = 3
} VinCheckProcessing_Enum;

typedef enum
{
   SM_PinCodeInactive = 0,
   SM_PinCodeLocked   = 1,
   SM_PinCodeUnlocked = 2
} PinCodeProcessing_Enum;

typedef struct
{
   uint8  ECU1VIN_stat[sizeof(VIN_stat_T)];
   uint8  ECU2VIN_stat[sizeof(VIN_stat_T)];
   uint8  ECU3VIN_stat[sizeof(VIN_stat_T)];
   uint8  ECU4VIN_stat[sizeof(VIN_stat_T)];
   uint8  ECU5VIN_stat[sizeof(VIN_stat_T)];
} ECUsVIN_stat_Type;

typedef struct
{
   DoorsAjar_stat_T   Current;
   DoorsAjar_stat_T   Previous;
} DoorsAjar_status;

typedef struct
{
   AuthorisationState_Enum   currentValue;
   AuthorisationState_Enum   newValue;
} Authorisation_States;

typedef struct
{
   DeviceAuthentication_rqst_T   Current;
   DeviceAuthentication_rqst_T   Previous;
} DeviceAuthentication_request;

typedef struct
{
   VehicleMode_T   Current;
   VehicleMode_T   Previous;
} VehicleMode_Internal;

typedef struct
{
   KeyfobAuth_stat_T Current;
   KeyfobAuth_stat_T Previous;
} KeyfobAuth_stat_eT;

// Structure for inputs
typedef struct
{
   DeviceAuthentication_request   DeviceAuthentication_rqst;
   DoorsAjar_status               DoorsAjar_stat;
   ECUsVIN_stat_Type              ECUsVIN_stat;
   EngineStartAuth_rqst_T         EngineStartAuth_rqst;
   GearBoxUnlockAuth_rqst_T       GearBoxUnlockAuth_rqst;
   KeyAuthentication_rqst_T       KeyAuthentication_rqst;
   KeyfobAuth_stat_eT             KeyfobAuth_stat;
   VehicleMode_Internal           VehicleMode;
} DriverAuthentication2Ctrl_InData_T;

// Structure for outputs
typedef struct
{
   Boolean                          EngineStartAuth_stat_CryptTrig;
   Boolean                          GrbxUnlockAuth_stat_CryptTrig;
   Boolean                          KeyAuth_stat_CryptTrig;
   VIN_rqst_T                       VIN_rqst;
   DeviceInCab_stat_T               DeviceInCabInt_stat;
   //uint8                            EngineStartAuth_st_serialized; //currently we were not using in current Version
   EngineStartAuth_stat_decrypt_T   EngineStartAuth_stat;
   GearboxUnlockAuth_stat_decrypt_T GearboxUnlockAuth_stat;
   //uint8                            GrbxUnlockAuth_stat_serialized;  //currently we were not using in current Version
   uint8                            KeyAuth_stat_serialized;  
   KeyAuthentication_stat_decrypt_T KeyAuthentication_stat;
   KeyNotValid_T                    KeyNotValid;
   KeyfobAuth_rqst_T                KeyfobAuth_rqst;
   PinCode_rqst_T                   PinCode_rqst;
} DriverAuthentication2Ctrl_OutData_T;

//=======================================================================================
// Private data declarations
//=======================================================================================
// Note: MUST BE static
// Note: Shall not be used/avoided with AUTOSAR SWC.


//=======================================================================================
// Private function prototypes
//=======================================================================================
// Note : MUST BE static

static void RteInDataWrite_Common(const DriverAuthentication2Ctrl_OutData_T *pRteOutputData_common);
static void Get_RteInDataRead_Common(DriverAuthentication2Ctrl_InData_T *pRteInData_Common);
static boolean KeyDeauthenticationCheck(const VehicleMode_Internal         *pVehicle_mode,
                                        const DoorsAjar_status             *pDoorsAjar_stat,
                                        const DeviceAuthentication_request *pDeviceAuthenticate_rqst,
                                              uint16                       *pDeauthenticationTimer);
static void AuthorisationStateMachine(const boolean                     isAuthenticationRequestEvent,
                                      const KeyAuthenticationEvent_Enum KeyAuthenticationEvent,
                                      const boolean                     isDeauthenticationEvent,
                                            PinCodeProcessing_Enum      PinCode_SM_Auth,
                                            Authorisation_States        *pAuthorisation_SM);
static void AuthStateMachine_OutputsProcessing(Authorisation_States                *pAuthorisation_SM_State,
                                               DriverAuthentication2Ctrl_OutData_T  *pRteOutData_Common);
static void GearBoxLockLogic(const GearBoxUnlockAuth_rqst_T         *pGearBoxUnlckAuth_rqst,
                             const AuthorisationState_Enum          Authorisation_SM_current,
                             const VinCheckProcessing_Enum          VIN_Check_Stat,
                                   GearboxUnlockAuth_stat_decrypt_T *pGearboxUnlockAuth_stat);
static void CrankingAuthorisationLogic(const EngineStartAuth_rqst_T         *pEngineStart_rqst,
                                       const AuthorisationState_Enum        Authorisation_SM_current,
                                       const VinCheckProcessing_Enum        VIN_Check_Value,
                                             EngineStartAuth_stat_decrypt_T *pEngineStartAuth_stat);
static void ReportDriverAuthenticationStatus(const DeviceAuthentication_request    *pDeviceAuthenticate_request,
                                             const AuthorisationState_Enum         Authorisation_SM_current,
                                                   DeviceInCab_stat_T           *pDeviceInCab_stat);
static boolean AuthenticationOfDriverByKeyfobRqtTrigger(const DeviceAuthentication_request *pDeviceAuthentication_rqst,
                                                              uint16                       *pPassiveRequestTimer,
                                                              KeyfobAuth_rqst_T            *pKeyfobAuth_rqst);
static void VIN_Check(const ECUsVIN_stat_Type       ECUsVIN_status,
                      const VehicleMode_Internal    *pVehicleMode_Status,
                      const SEWS_VINCheckProcessing_P1VKG_s_T    *pVINCheckActivation,
                            uint16                  *pTimers_VIN_Check,
                            VIN_rqst_T              *pVIN_rqst,
                            VinCheckProcessing_Enum *pSM_VinCheck,
							VINCheckStatus_T        *pVINCheckStatus);
static FormalBoolean isVinEqualCheck(const SEWS_P1VKG_APM_Check_Active_T    is_CheckActive,
                                     const VIN_stat_T                       EcuVIN);
static FormalBoolean ANW_ImmobilizerPINCodeActivationCondition(const KeyNotValid_T  *pKeyNotValid);
static void ANW_ImmobilizerPINCode(const FormalBoolean  *pisActTriggerDetected,
                                   const FormalBoolean  *pisDeActTriggerDetected);

//=======================================================================================
// End of file
//=======================================================================================
#endif

