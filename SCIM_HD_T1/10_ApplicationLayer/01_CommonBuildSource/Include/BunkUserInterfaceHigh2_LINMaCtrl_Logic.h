#ifdef BUNKUSERINTERFACEHIGH2_LINMACTRL_LOGIC_H
#error BUNKUSERINTERFACEHIGH2_LINMACTRL_LOGIC_H unexpected multi-inclusion
#else
#define BUNKUSERINTERFACEHIGH2_LINMACTRL_LOGIC_H
//=====================================================================================
// Included header files
//=====================================================================================

//=======================================================================================
// Private macros
//=======================================================================================


//=======================================================================================
// Private type definitions
//=======================================================================================

//=======================================================================================
// Private constants and enum definitions
//=======================================================================================

#define CONST_runnableTimer                     (10U)
#define CONST_ANW_AudioRadio2_Timeout           ((60000U) / CONST_runnableTimer)
#define CONST_ANW_ClimPHTimerSettings3_Timeout  ((30000U) / CONST_runnableTimer)
#define CONST_ANW_LockControlCabRqst2_Timeout   ((30000U) / CONST_runnableTimer)
#define CONST_ANW_OtherInteriorLights4_Timeout  ((30000U) / CONST_runnableTimer)
#define CONST_ANW_PHActMaintainLiving6_Timeout  ((10000U) / CONST_runnableTimer)
#define CONST_ANW_PowerWindowsActivate3_Timeout ((30000U) / CONST_runnableTimer)
#define CONST_ANW_RoofHatchRequest3_Timeout     ((30000U) / CONST_runnableTimer)
#define CONST_LeftLimit_HR                            (0U)
#define CONST_RightLimit_HR                           (250U)
#define CONST_LeftLimit_MIN                           (0U)
#define CONST_RightLimit_MIN                          (59U)

//=======================================================================================
// Private data declarations
//=======================================================================================

//=======================================================================================
// Private function prototypes
//=======================================================================================

//=======================================================================================
// End of file
//=======================================================================================
#endif 
