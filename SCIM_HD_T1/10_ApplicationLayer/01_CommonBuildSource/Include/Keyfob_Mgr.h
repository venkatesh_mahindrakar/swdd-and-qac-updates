/*
 ***************************************************************************************
 ***************************************************************************************
 ***
 ***     File: Keyfob_Mgr.h
 ***
 ***     Project: SCIM
 ***
 ***     
 ***
 ***************************************************************************************
 ***************************************************************************************
 */

/*!
 ***************************************************************************************
 * \file
 *
 * \ingroup application_swc
 *
 * \subject
 * Definition file for the Keyfob_Mgr runnables.
 *
 * \version
 * 
 *
 * \{
 ***************************************************************************************
 */
#ifndef SRE_Keyfob_Mgr_H
#define SRE_Keyfob_Mgr_H

/*
 **=====================================================================================
 ** Included header files
 **=====================================================================================
 */

#include "Rte_Type.h"
#include "Platform_Types.h"


/*
 **=====================================================================================
 ** Public macros
 **=====================================================================================
 */

#define CONST_Keyfob_Immo_Search_Step_Idle 0
#define CONST_Keyfob_Immo_Search_Step_requested 1
#define CONST_Keyfob_Immo_Search_Step_Found 2
#define CONST_Keyfob_Immo_Search_Step_NotFound 3

/* Keyfob_Search_step*/
#define CONST_Keyfob_Search_step_request 0
#define CONST_Keyfob_Search_step_wait 1
#define CONST_Keyfob_Search_step_process 2
#define CONST_Keyfob_Search_step_ImmoProcess 3


/* Fobsearch */
#define CONST_FobSearch_Idle     0u
#define CONST_FobSearch_Progress 1u
#define CONST_FobSearch_LF_Sent  2u
#define CONST_FobSearch_Found    3u
#define CONST_FobSearch_NoFound  4u


/* Passive, Immo Request */
#define CONST_NotRequested        0
#define CONST_SearchRequested     1
#define CONST_SearchOngoing       2
#define CONST_SearchCompleted     3
#define CONST_NotAvailable        4

/* For  */
#define CONST_KeyfobLocation_stat_Idle                       0
#define CONST_KeyfobLocation_stat_NotDetectedInCab           1
#define CONST_KeyfobLocation_stat_DetectedInCab              2
#define CONST_KeyfobLocation_stat_NotDetectedOutside         3
#define CONST_KeyfobLocation_stat_DetectedOnRight            4
#define CONST_KeyfobLocation_stat_DetectedOnLeft             5
#define CONST_KeyfobLocation_stat_DetectedOnBoth             6
#define CONST_KeyfobLocation_stat_DetectedOuOfDoorAreas   7

#define CONST_KeyfobLocationbyPassive_Idle                       0
#define CONST_KeyfobLocationbyPassive_NotDetectedInCab           3
#define CONST_KeyfobLocationbyPassive_DetectedInCab              2
#define CONST_KeyfobLocationbyPassive_NotDetectedOutside         1
#define CONST_KeyfobLocationbyPassive_DetectedOnRight            4
#define CONST_KeyfobLocationbyPassive_DetectedOnLeft             5
#define CONST_KeyfobLocationbyPassive_DetectedOnBoth             6
#define CONST_KeyfobLocationbyPassive_DetectedOuOfDoorAreas     7

#define CONST_KeyfobLocationbyImmo_Idle                       0
#define CONST_KeyfobLocationbyImmo_NotDetectedInCab           3
#define CONST_KeyfobLocationbyImmo_DetectedInCab              2


/* For KeyfobInCabLocation_stat, KfobInCbLctn_stat */
#define CONST_KeyfobInCabLocation_stat_Idle               0
#define CONST_KeyfobInCabLocation_stat_NotDetectedInCab   1
#define CONST_KeyfobInCabLocation_stat_DetectedInCab      2

/* For KeyfobOutsideLocation_stat, KfobOsdLctn_stat */
#define CONST_KeyfobOutsideLocation_stat_Idle                   0
#define CONST_KeyfobOutsideLocation_stat_NotDetectedOutside     1
#define CONST_KeyfobOutsideLocation_stat_DetectedOnRightSide    2
#define CONST_KeyfobOutsideLocation_stat_DetectedOnLeftSide     3
#define CONST_KeyfobOutsideLocation_stat_DetectedOnBothSides    4
#define CONST_KeyfobOutsideLocation_stat_DetectedOutOfDoor      5

/* for KeyfobPassiveSearchRqst.PassiveSearchRequested */
#define CONST_KeyfobPassiveSearchRqst_Idle                            0
#define CONST_KeyfobPassiveSearchRqst_PassiveEntryPassiveStartSearch  1
#define CONST_KeyfobPassiveSearchRqst_PassiveEntrySearch              2
#define CONST_KeyfobPassiveSearchRqst_PassiveStartWithBackUpSearch    3
#define CONST_KeyfobPassiveSearchRqst_PassiveStartSearch              4
#define CONST_KeyfobPassiveSearchRqst_PassiveBackUpCheck              5

/* for KeyfobPassiveSearchRqst.PassiveSearchFinished */
#define CONST_KeyfobPassiveSearchRqst_PassiveEntryPassiveStartSearchFinished  1
#define CONST_KeyfobPassiveSearchRqst_PassiveEntrySearchFinished              2
#define CONST_KeyfobPassiveSearchRqst_PassiveStartSearchFinished              4
#define CONST_KeyfobPassiveSearchRqst_PassiveStartWithBackUpSearchFinished    3
#define CONST_KeyfobPassiveSearchRqst_PassiveBackUpCheckFinished              5

/* for retrying */
#define CONST_Keyfob_PEPS_Search_Retry 1
#define CONST_Keyfob_Immo_Search_Retry 2

#define CONST_Keyfob_Mgr_Func_Period               20 // 20ms
#define CONST_MatchingSuccessResultDisplayTimeout  2000/CONST_Keyfob_Mgr_Func_Period // 2000ms
#define CONST_MatchingFailResultDisplayTimeout     4000/CONST_Keyfob_Mgr_Func_Period // 4000ms
#define CONST_ImmoCheckOperationTimeout            3000/CONST_Keyfob_Mgr_Func_Period // 3000ms
#define CONST_ImmoSingleMatchOperationTimeout      5000/CONST_Keyfob_Mgr_Func_Period // 3000ms
#define CONST_ImmoMatchingRoutingTimeout           60000/CONST_Keyfob_Mgr_Func_Period // 30000ms
#define CONST_ImmoDriverOperationalTimeout         200/CONST_Keyfob_Mgr_Func_Period // 2000ms

#define CONST_PepsOperationTimer                   0
#define CONST_MatchingRoutineTimer                 1
#define CONST_MatchingRoutineOperationTimer        2
#define CONST_NbOfTimer                            3

/* for gRoutineR1AAAStatus // FlagKeyfobMatchingByLF */
#define CONST_Routine_R1AAA_isInactive               0u
//#define CONST_Routine_R1AAA_StartRequested         1u
#define CONST_Routine_R1AAA_isActive                 1u
//#define CONST_Routine_R1AAA_StopRequested          3u

// for MatchingStatus
#define CONST_P1B0T_MatchingProcedureNotStarted          0x00
#define CONST_P1B0T_WaitingKeyPresentation               0x01
#define CONST_P1B0T_KeyMatchedAndStored                  0x02
#define CONST_P1B0T_NotIdentifiedKey                     0x03
#define CONST_P1B0T_AlreadyMatchedKey                    0x04
#define CONST_P1B0T_NumberOfMaxKeyReached                0x05
#define CONST_P1B0T_KeyListReinitialized                 0x06
#define CONST_P1B0T_OngoingDoNotRemoveTheKey             0x07
#define CONST_P1B0T_IvalidImmoDriverState                0xFE

// for General
#define CONST_MAX_KEYFOB                     8u


/*
 **=====================================================================================
 ** Public definitions
 **=====================================================================================
 */


/*
 **=====================================================================================
 ** Public type definitions
 **=====================================================================================
 */


/*!
 * Contains all data from the RTE to the Keyfob_Mgr logical function
 */

typedef struct
{
   uint8                         Previous;
   uint8                         Current;
} Evt_T;


typedef struct
{
   DriverAuthDeviceMatching_T    Previous;
   DriverAuthDeviceMatching_T    Current;
} DriverAuthDeviceMatching_Evt_T;

typedef struct
{
   KeyfobAuth_rqst_T             Previous;
   KeyfobAuth_rqst_T             Current;
} KeyfobAuth_rqst_Evt_T;

typedef struct
{
   KeyfobInCabPresencePS_T  Previous;
   KeyfobInCabPresencePS_T  Current;
} KeyfobInCabPresencePS_Evt_T;

typedef struct
{
   KeyfobLocation_rqst_T        Previous;
   KeyfobLocation_rqst_T        Current;
} KeyfobLocation_rqst_Evt_T;

typedef struct
{
   KeyfobAuth_rqst_Evt_T         KeyfobAuth_rqst;
   KeyfobInCabPresencePS_Evt_T   KeyfobInCabPresencePS_rqst;
   KeyfobLocation_rqst_Evt_T     KeyfobLocation_rqst;
   Evt_T                         FobSearch;
   Evt_T                         SrchFnshbyImmo;
   uint8                         KfbLctnbyImmo;
   Evt_T                         RequestPassiveSearch;
/*
   KeyfobAuth_stat_T             KeyfobAuth_stat; //KfobAuth_stat
   KeyfobInCabLocation_stat_T    KeyfobInCabLocation_stat; //KfobInCbLctn_stat
   KeyfobOutsideLocation_stat_T  KeyfobOutsideLocation_stat; //KfobOsdLctn_stat
   uint8                         PsvSrchRqst;
   tVAR                          RequestPassiveSearch;
   uint8                         SearchRequestedbyImmo;// SrchRqstbyImmo;
*/
} Keyfob_Mgr_Logic_StructType;

typedef struct
{
   DriverAuthDeviceMatching_Evt_T   DriverAuthDeviceMatching;
   KeyfobAuth_rqst_Evt_T            KeyfobAuth_rqst;
   KeyfobInCabPresencePS_Evt_T      KeyfobInCabPresencePS_rqst;
   KeyfobLocation_rqst_Evt_T        KeyfobLocation_rqst;
   VehicleModeDistribution_T        SwcActivation_Security;
   
   uint8                         FobSearch;
   uint8                         KeyfobLocationbyImmo;
   uint8                         SearchFinishedbyImmo;
//   uint8                         KeyfobLocationbyPassive;
   uint8                         SearchFinishedbyPassive;
   boolean                       isDiagnostics;
   uint8                         KeyfobAuth_byImmo_rqst;
   uint8                         Keyfob_Mgr_steps;

//   uint8                         FlagKeyfobMatchingByLF;
//   uint8                         FlagKeyfobMatchingByRF;
} Keyfob_Mgr_in_StructType;

typedef struct
{
   KeyfobAuth_stat_T             KeyfobAuth_stat;
   KeyfobInCabLocation_stat_T    KeyfobInCabLocation_stat;
   KeyfobOutsideLocation_stat_T  KeyfobOutsideLocation_stat;
   uint8                         SearchRequestedbyImmo;

   uint8                         P1B0T_MatchedKeyfobCount;
   uint8                         P1B0T_MatchingStatus;
} Keyfob_Mgr_out_StructType;

typedef enum  
{ 
   SM_KeyfobOperational_Functional,
   SM_KeyfobOperational_LFmatching,
   SM_KeyfobOperational_RFmatching
} SM_KeyfobOperational;

typedef enum  
{ 
   PEPSMode_Diagnostics,
   PEPSMode_Nominal,
   PEPSMode_Fallback
} PEPSMode_Enum;

typedef struct
{
   PEPSMode_Enum   Current;
   PEPSMode_Enum   Previous;
} SM_PEPSModeStates;

typedef enum
{
   SM_PassiveProcess_Idle,
   SM_PassiveProcess_Ongoing,
   SM_PassiveProcess_Detected,
   SM_PassiveProcess_NotDetected,
   SM_PassiveProcess_NotDetected_Fail,
   SM_ImmoProcess_Ongoing,
   SM_ImmoDecryptedInList,
   SM_ImmoNotValid,
   SM_ImmoFail   
} SM_PassiveProcess_Enum;

typedef struct
{
   SM_PassiveProcess_Enum     Current;
   SM_PassiveProcess_Enum     New;
} SM_PassiveProcess_Enum_Type;

typedef enum  
{ 
   SM_PEPS_Initial,
   SM_PEPS_Idle, 
   SM_PEPS_Ongoing, 
   SM_PEPS_Completed
} PEPSProcessStateMachine_Enum;

typedef struct
{
   PEPSProcessStateMachine_Enum   Current;
   PEPSProcessStateMachine_Enum   Previous;
} SM_PEPSProcessStates;

typedef enum  
{ 
   SM_Immo_Initial,
   SM_Immo_Idle, 
   SM_Immo_Ongoing, 
   SM_Immo_Completed
} ImmoProcessStateMachine_Enum;

typedef struct
{
   ImmoProcessStateMachine_Enum   Current;
   ImmoProcessStateMachine_Enum   Previous;
} SM_ImmoProcessStates;

typedef enum  
{ 
   PassiveSearch_Idle, 
   PassiveSearch_requested
} SearchRequestedbyPassive_Enum;

typedef enum
{
   SM_LFmatching_Idle = 0,
   SM_LFmatching_MatchingNotAuthorized,
   SM_LFmatching_MatchingAuthorizationCheck,
   SM_LFmatching_MatchingAuthorized,
   SM_LFmatching_MatchingListReInitialization,
   SM_LFmatching_WaitingKeyfobPresentation,
   SM_LFmatching_MatchingKeyfob,
   SM_LFmatching_MatchingSuccessful,
   SM_LFmatching_MatchingFail,
   SM_LFmatching_MatchingListFull,
   SM_LFmatching_MatchingStatus
} SM_LFmatching_Enum;

typedef struct
{
   SM_LFmatching_Enum     Current;
   SM_LFmatching_Enum     New;
} SM_LFmatching_Enum_Type;


/*!
 * Contains all data from the Keyfob_Mgr logical function to the RTE
 */

/*
 **=====================================================================================
 ** Public data declarations
 **=====================================================================================
 */

/*
 **=====================================================================================
 ** Public function declarations
 **=====================================================================================
 */

static void RteDataRead_Common (Keyfob_Mgr_in_StructType *RteInData_Common);
static void RteDataWrite_Common (Keyfob_Mgr_out_StructType *RteOutData_Common);
static void Keyfob_Mgr_Logic_PassiveStart(Keyfob_Mgr_in_StructType *RteInData_Common, Keyfob_Mgr_out_StructType *RteOutData_Common, uint16 pTimers[CONST_NbOfTimer], boolean *pIsLfProcessingIdle);
static void Keyfob_Mgr_Logic_LFmatching(Keyfob_Mgr_in_StructType *RteInData_Common, Keyfob_Mgr_out_StructType *RteOutData_Common, uint16 pTimers[CONST_NbOfTimer], const SM_KeyfobOperational *pKeyfobOperational_SM);
//static void Keyfob_Mgr_Logic_RFmatching(Keyfob_Mgr_in_StructType *RteInData_Common, Keyfob_Mgr_out_StructType *RteOutData_Common, uint16 pTimers[CONST_NbOfTimer]);



//static void PEPSModeProcessing(Keyfob_Mgr_in_StructType *RteInData_Common, SM_PEPSModeStates *PEPSModeStates);



/*! \cond SKIP_FUNCTION_PROTOTYPES */


/*! \endcond */
#endif /* SRE_Keyfob_Mgr_H */

/*
 **=====================================================================================
 ** End of file
 **=====================================================================================
 */
/*!
 * \}
 ************************************ End of file **************************************
 */
