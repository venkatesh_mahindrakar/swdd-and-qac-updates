//!======================================================================================
//! \file FuncLibrary_Timer_If.h
//!
//! @addtogroup Library
//! @{
//! @addtogroup FuncLibrary_Timer
//! @{
//!
//! \brief
//! FuncLibrary_Timer SWC.
//! ASIL Level : QM.
//! This module implements the logic for the timer library function.
//!======================================================================================

#ifndef FuncLibrary_FuncLibrary_Timer_If_H
// To protect against multi-inclusion : FuncLibrary_FuncLibrary_Timer_If_H must be unique
#define FuncLibrary_FuncLibrary_Timer_If_H

#include "Platform_Types.h"
//=======================================================================================
// Public type definitions
//=======================================================================================

//=======================================================================================
// Public constants and enums definitions
//=======================================================================================

//! This timer value indicates/set the inactive timer (will not be decremented).
#define CONST_TimerFunctionInactive    0xFFFFu
//! This timer value indicates that timeout is elapsed, and timer will be deactivated.
#define CONST_TimerFunctionElapsed     0x0000u

//! This timer value indicates/set the inactive timer (will not be decremented).
#define CONST_TimerFunctionInactive_32bit    0xFFFFFFFFu

//=======================================================================================
// Public data declarations
//=======================================================================================
// Note: Shall not be used with AUTOSAR SWC.

//=======================================================================================
// Public macros
//=======================================================================================
// Note, coding convention: shall be avoided to support debug capability.

//=======================================================================================
// Public function prototypes
//=======================================================================================

// TimerFunction constants
void TimerFunction_DeactivateAll(const uint8 NbOfTimers, uint16 *pTimer);
void TimerFunction_Deactivate(const uint8 TimerId, uint16 *pTimer);
void TimerFunction_Tick(const uint8 NbOfTimers, uint16 *pTimer);
void TimerFunction_Tick_ThirtyTwo(const uint8 NbOfTimers, uint32 *pTimer);
#endif // FuncLibrary_FuncLibrary_Timer_If_H                              

//! @}
//! @}

//=======================================================================================
// End of file
//=======================================================================================