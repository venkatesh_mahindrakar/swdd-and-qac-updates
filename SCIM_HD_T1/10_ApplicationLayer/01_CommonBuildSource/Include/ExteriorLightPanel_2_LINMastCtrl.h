#ifdef EXTERIORLIGHTPANEL_2_LINMASTCTRL_H
#error EXTERIORLIGHTPANEL_2_LINMASTCTRL_H unexpected multi-inclusion
#else
#define EXTERIORLIGHTPANEL_2_LINMASTCTRL_H
//=====================================================================================
// Included header files
//=====================================================================================

//=======================================================================================
// Private constants and enum definitions
//=======================================================================================

//=======================================================================================
// Private macros
//=======================================================================================

#define CONST_RunnableTimer                   (20U)
#define CONST_ANW_ExtLightsRequest2_Timeout   ((3000U) / CONST_RunnableTimer)
#define CONST_ANW_WLight_InputELCP_Timeout    ((3000U) / CONST_RunnableTimer)
#define CONST_Tester_Present                  (0x80U)
#define CONST_Tester_Notpresent               (0x7FU)
#define CONST_Fault_InActive                  (0U)
#define CONST_Fault_Active                    (1U)
#define CONST_SlaveFCIFault_InActive          (0U)
#define CONST_SlaveFCIFault_Active            (1U)

//=======================================================================================
// Private type definitions
//=======================================================================================

typedef struct
{
   FreeWheel_Status_T   Previous;
   FreeWheel_Status_T   Current;
} FreeWheel_Status;

typedef struct
{
   PushButtonStatus_T   Previous;
   PushButtonStatus_T   Current;
} PushButtonStatus;
 
// Input structure 
typedef struct
{
   PushButtonStatus_T   LIN_DRL_ButtonStatus;
   PushButtonStatus_T   LIN_FogLightFront_ButtonStat_2;
   PushButtonStatus_T   LIN_FogLightRear_ButtonStat_2;
   A3PosSwitchStatus_T  LIN_HeadLampUpDown_SwitchStatu;
   FreeWheel_Status     LIN_LightMode_Status_2;
   PushButtonStatus     LIN_RearWorkProjector_BtnStat;
   DeviceIndication_T   RearWorkProjector_Indication;
   ComMode_LIN_Type     comMode_LIN4;
   DiagInfo_T           DiagInfoELCP2;
   ResponseErrorELCP2_T   ResponseErrorELCP2_LIN4;
   boolean              IsUpdated_LIN_LightMode_Status_2;
   DiagActiveState_T    isDiagActive;
} ExteriorLP_2_LINMastCtrl_in_StructType;

// Output structure 
typedef struct
{
   PushButtonStatus_T   DRL_ButtonStatus;
   PushButtonStatus_T   FogLightFront_ButtonStatus_2;
   PushButtonStatus_T   FogLightRear_ButtonStatus_2;
   A3PosSwitchStatus_T  HeadLampUpDown_SwitchStatus;
   DeviceIndication_T   LIN_RearWorkProjector_Indicati;
   FreeWheel_Status_T   LightMode_Status_2;
   PushButtonStatus_T   RearWorkProjector_ButtonStatus;
} ExtLightPanel_2_LINMastCtrl_out_StructType;

//=======================================================================================
// Private data declarations
//=======================================================================================

//=======================================================================================
// Private function prototypes
//=======================================================================================

static void IOCtrlService_LinOutputSignalControl(const uint8                                       OverrideData,
                                                       ExtLightPanel_2_LINMastCtrl_out_StructType  *pOutData);
static Std_ReturnType Get_RteInDataRead_Common(ExteriorLP_2_LINMastCtrl_in_StructType *pExterior_In_data);
static void ExteriorLightPanel_2_Signals_Gateway_Logic(const ExteriorLP_2_LINMastCtrl_in_StructType      *pExteriorLight_In_data,
                                                             ExtLightPanel_2_LINMastCtrl_out_StructType  *pExteriorLight_Out_data);
static void ExteriorLightPanel_2_Fallback_Mode(ExtLightPanel_2_LINMastCtrl_out_StructType *pExterior_Output_data);
static void RteInDataWrite_Common(const ExtLightPanel_2_LINMastCtrl_out_StructType *pExterior_Out_data,
                                        boolean                                    *pIsUpdated_LIN_LightMode_Status_2);
static boolean ExteriorLightPanel2_FCItoDTC_conversion(const DiagInfo_T *pDiagInfoELCP2);
static void ExteriorLightPanel2_Deactivate_Logic(ExtLightPanel_2_LINMastCtrl_out_StructType *pExteriorLightpanel_Out_data);
static FormalBoolean ANW_ExteriorLightsRequest2_Cond(const FreeWheel_Status *pLIN_LightMode_Status_2);
static FormalBoolean ANW_WLight_InputELCP_ChkCondtn(const PushButtonStatus *pLIN_RearWorkProjector_BtnStat);
static void ANW_ExteriorLightsRequest2_Logic(const FormalBoolean isRestAct_ExteriorLightsRequest2,
                                          const FormalBoolean isActTrig_ExteriorLightsRequest2);
static void ANW_WLight_InputELCP_Logic(const FormalBoolean isRestAct_WLight_InputELCP,
                                         const FormalBoolean isActTrig_WLight_InputELCP);

//=======================================================================================
// End of file
//=======================================================================================
#endif


