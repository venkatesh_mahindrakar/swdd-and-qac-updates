#ifdef INTERIORLIGHTPANEL_1_LINMASTCTRL_H
#error INTERIORLIGHTPANEL_1_LINMASTCTRL_H unexpected multi-inclusion
#else
#define INTERIORLIGHTPANEL_1_LINMASTCTRL_H
//=====================================================================================
// Included header files
//=====================================================================================

#include "Rte_Type.h"

//=====================================================================================
// Private constants and enum definitions
//=====================================================================================

#define CONST_Runnable                       (20U)
#define CONST_AnwOtherIntLightDeactTimeout   ((3000U)/(CONST_Runnable))
#define CONST_Tester_Present                 (0x80U)
#define CONST_Tester_Notpresent              (0x7FU)

//=====================================================================================
// Private macros
//=====================================================================================

//=====================================================================================
// Private type definitions
//=====================================================================================

typedef struct
{
   PushButtonStatus_T   Current;
   PushButtonStatus_T   Previous;
} PushButtonStatus;

typedef struct
{
   FreeWheel_Status_T    Current;
   FreeWheel_Status_T    Previous;
} FreeWheel_Status;

typedef struct
{
   DeviceIndication_T  Current;
   DeviceIndication_T  Previous;
} DeviceIndication;

typedef struct
{
   DeviceIndication_T    DoorAutoFuncInd_cmd;
   DeviceIndication      IntLghtOffModeInd_cmd;
   DeviceIndication_T    IntLightMaxModeInd_cmd;
   DeviceIndication_T    IntLightNightModeInd_cmd;
   DeviceIndication_T    IntLightRestingModeInd_cmd;
   PushButtonStatus      LIN_DoorAutoFuncBtn_stat;
   PushButtonStatus      LIN_IntLghtDimmingLvlDecBtn_s;
   PushButtonStatus      LIN_IntLghtDimmingLvlIncBtn_s;
   FreeWheel_Status      LIN_IntLghtModeSelrFreeWheel_s;
   ResponseErrorILCP1_T  ILCP1_ResponseError;
   ComMode_LIN_Type      ComMode_LIN1;
   DiagActiveState_T     isDiagActive;
   boolean               IsUpdated_LIN_IntLghtModeSelrFreeWheel;
} InteriorLightPanel1LINMastCtrl_InData_T;

typedef struct
{
   PushButtonStatus_T   DoorAutoFuncBtn_stat;
   PushButtonStatus_T   IntLghtDimmingLvlDecBtn_stat;
   PushButtonStatus_T   IntLghtDimmingLvlIncBtn_stat;
   FreeWheel_Status_T   IntLghtModeSelrFreeWheel_stat;
   DeviceIndication_T   LIN_DoorAutoFuncInd_cmd;
   DeviceIndication_T   LIN_IntLghtOffModeInd_cmd;
   DeviceIndication_T   LIN_IntLightMaxModeInd_cmd;
   DeviceIndication_T   LIN_IntLightNightModeInd_cmd;
   DeviceIndication_T   LIN_IntLightRestingModeInd_cmd;
} InteriorLightPanel1LINMastCtrl_OutData_T;

//=====================================================================================
// Private data declarations
//=====================================================================================

//=====================================================================================
// Private function prototypes
//=====================================================================================
static void IOCtrlService_LinOutputSignalControl(const uint8                                     OverrideData,
                                                       InteriorLightPanel1LINMastCtrl_OutData_T  *pOutData);
static Std_ReturnType Get_RteDataRead_Common(InteriorLightPanel1LINMastCtrl_InData_T *pRteInData_Common);
static void RteDataWrite_Common(const InteriorLightPanel1LINMastCtrl_OutData_T  *pRteOutData_Common, 
   	                                  boolean                                   *pIsUpdated_LIN_IntLghtModeSelrFreeWheel);
static  void Fallback_Logic_InteriorLightPanel1LINMastCtrl(PushButtonStatus_T *pDoorAutoFuncBtn_stat,
                                                           PushButtonStatus_T *pIntLghtDimmingLvlDecBtn_stat,
                                                           PushButtonStatus_T *pIntLghtDimmingLvlIncBtn_stat,
                                                           FreeWheel_Status_T *pIntLghtModeSelrFreeWheel_stat);
static  void Deactivate_Logic_InteriorLightPanel1LINMastCtrl(PushButtonStatus_T *pDoorAutoFuncBtn_stat,
                                                             PushButtonStatus_T *pIntLghtDimmingLvlDecBtn_stat,
                                                             PushButtonStatus_T *pIntLghtDimmingLvlIncBtn_stat,
                                                             FreeWheel_Status_T *pIntLghtModeSelrFreeWheel_stat);
static void Gateway_Logic_InteriorLightPanel1LINMastCtrl(const InteriorLightPanel1LINMastCtrl_InData_T  *pIn_Gateway,
                                                               InteriorLightPanel1LINMastCtrl_OutData_T *pOut_Gateway);
static void Generic_ANW_OtherInteriorLights2_Logic(const FormalBoolean isRestAct,
                                                   const FormalBoolean isActTrigDet);
static FormalBoolean Generic_ANW_OtherInteriorLight2_CondCheck(const PushButtonStatus     *pLIN_DoorAutoFuncBtn_stat,
      		                                                   const PushButtonStatus     *pLIN_IntLghtDimmingLvlDecBtn_s,
                                                               const PushButtonStatus     *pLIN_IntLghtDimmingLvlIncBtn_s,
                                                               const FreeWheel_Status     *pLIN_IntLghtModeSelrFreeWheel_s,
                                                               const DeviceIndication     *pIntLghtOffModeInd_cmd);
//=====================================================================================
// End of file
//=====================================================================================
#endif

