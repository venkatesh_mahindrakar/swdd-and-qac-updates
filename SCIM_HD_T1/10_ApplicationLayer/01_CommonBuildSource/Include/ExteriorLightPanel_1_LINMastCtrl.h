#ifdef EXTERIORLIGHTPANEL_1_LINMASTCTRL_H
#error EXTERIORLIGHTPANEL_1_LINMASTCTRL_H unexpected multi-inclusion
#else
#define EXTERIORLIGHTPANEL_1_LINMASTCTRL_H
//=====================================================================================
// Included header files
//=====================================================================================
#include "FuncLibrary_AnwStateMachine_If.h"
#include "Rte_Type.h"

//=====================================================================================
// Private constants and enum definitions
//=====================================================================================

//=====================================================================================
// Private macros
//=====================================================================================
//temperary timer defines
#define CONST_runnableTimer             (20U)
#define CONST_ANW_DimmingAdj1_Timeout   ((3000U) / CONST_runnableTimer)
#define CONST_ANW_ExtLightsReq1_Timeout ((3000U) / CONST_runnableTimer)
#define CONST_Tester_Present            (0x80U)
#define CONST_Tester_NotPresent         (0x7FU)
#define CONST_Fault_InActive            (0U)
#define CONST_Fault_Active              (1U)
#define CONST_SlaveFCIFault_InActive    (0U)
#define CONST_SlaveFCIFault_Active      (1U)

//=====================================================================================
// Private type definitions
//=====================================================================================
typedef struct
{
   Thumbwheel_stat_T CurrentValue;
   Thumbwheel_stat_T PreviousValue;
} Thumbwheel_stat;

typedef struct
{
   PushButtonStatus_T CurrentValue;
   PushButtonStatus_T PreviousValue;
} PushButtonStatus;

typedef struct
{
   FreeWheel_Status_T CurrentValue;
   FreeWheel_Status_T PreviousValue;
} FreeWheel_Status;

// Input Structure
typedef struct
{
   Thumbwheel_stat      LIN_BackLightDimming_Status;
   PushButtonStatus     LIN_BlackPanelMode_ButtonStat;
   FreeWheel_Status     LIN_LightMode_Status_1;
   ComMode_LIN_Type     ComMode_LIN4;
   DeviceIndication_T   DaytimeRunningLight_Indication;
   DiagInfo_T           DiagInfoELCP1;
   DeviceIndication_T   DrivingLightPlus_Indication;
   DeviceIndication_T   DrivingLight_Indication;
   DeviceIndication_T   FrontFog_Indication;
   PushButtonStatus_T   LIN_FogLightFront_ButtonStat_1;
   PushButtonStatus_T   LIN_FogLightRear_ButtonStat_1;
   Thumbwheel_stat_T    LIN_LevelingThumbwheel_stat;
   DeviceIndication_T   ParkingLight_Indication;
   DeviceIndication_T   RearFog_Indication;
   ResponseErrorELCP1_T ELCP1_ResponseError;
   DiagActiveState_T    isDiagActive;
   boolean              IsUpdated_LIN_LightMode_Status_1;
} ExteriorLightPanel_1_LINMastCtrl_In_StructType;

// Output Structure
typedef struct
{
   Thumbwheel_stat_T   BackLightDimming_Status;
   PushButtonStatus_T  BlackPanelMode_ButtonStatus;
   FreeWheel_Status_T  LightMode_Status_1;
   Thumbwheel_stat_T   LevelingThumbwheel_stat;
   DeviceIndication_T  LIN_RearFog_Indication;
   DeviceIndication_T  LIN_ParkingLight_Indication;
   DeviceIndication_T  LIN_FrontFog_Indication;
   DeviceIndication_T  LIN_DrivingLight_Indication;
   DeviceIndication_T  LIN_DrivingLightPlus_Indicatio;
   DeviceIndication_T  LIN_DaytimeRunningLight_Indica;
   PushButtonStatus_T  FogLightRear_ButtonStatus_1;
   PushButtonStatus_T  FogLightFront_ButtonStatus_1;
} ExteriorLightPanel_1_LINMastCtrl_Out_StructType;

//=====================================================================================
// Private data declarations
//=====================================================================================

//=====================================================================================
// Private function prototypes
//=====================================================================================

static void IOCtrlService_LinOutputSignalControl(const uint8                                           OverrideData,
                                                       ExteriorLightPanel_1_LINMastCtrl_Out_StructType *pOutData);
static Std_ReturnType Get_RteDataRead_Common(ExteriorLightPanel_1_LINMastCtrl_In_StructType *pRteInData_Common);
static void DeactivationLogic_ExteriorLightPanel_1(ExteriorLightPanel_1_LINMastCtrl_Out_StructType *pOut_Deact);
static void FallbackLogic_ExteriorLightPanel_1(ExteriorLightPanel_1_LINMastCtrl_Out_StructType *pOut_Fallback);
static void Signals_GatewayLogic_ExteriorLightPanel_1(const ExteriorLightPanel_1_LINMastCtrl_In_StructType  *pInData_Gateway,
                                                            ExteriorLightPanel_1_LINMastCtrl_Out_StructType *pOutData_Gateway);
static boolean FCIToDTC_ConversionLogic_ExteriorLightPanel_1(const DiagInfo_T  *pDiagInfoELCP1);
static FormalBoolean ANWDimmingAdjustment1_CondCheck(const Thumbwheel_stat  *pLIN_BackLightDimming_Status,
                                                     const PushButtonStatus *pLIN_BlackPanelMode_ButtonStat);
static void ANW_DimmingAdjustment1_logic(const FormalBoolean isRestAct_DimmingAdj1,
                                          const FormalBoolean isActTrigDet_DimmingAdj1);
static FormalBoolean ANW_ExteriorLightsRequest1_Cond(const FreeWheel_Status *pLIN_LightMode_Status_1);
static void ANWExteriorLightsRequest1_Logic(const FormalBoolean isRestAct_ExtLightsReq1,
                                             const FormalBoolean isActTrigDet_ExtLightsReq1);
static void RteDataWrite_Common(const ExteriorLightPanel_1_LINMastCtrl_Out_StructType *pRteOutData_Common,
	                                  boolean                                         *pIsUpdated_LIN_LightMode_Status_1);

//=====================================================================================
// End of file
//=====================================================================================
#endif

