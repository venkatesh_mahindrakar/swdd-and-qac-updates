//!======================================================================================
//! \file AuthenticationDevice_UICtrl.h
//!
//!======================================================================================

//=======================================================================================
// Private type definitions
//=======================================================================================

//=======================================================================================
// Private constants and enums definitions
//=======================================================================================

// Parameter defined values
#define CONST_Key_Switch   FALSE
#define CONST_Start_Button TRUE
// Define runnable time basis
#define CONST_Runnable_Time_Base   20
// Define relative timeouts
#define CONST_Timer_200ms    (uint16) (200/CONST_Runnable_Time_Base)
#define CONST_Timer_8000ms   (uint16) (8000/CONST_Runnable_Time_Base)
// Define timer
#define CONST_IdTimerKeySwitch    0
#define CONST_NbOfTimers          1

//=======================================================================================
// Private data declarations
//=======================================================================================
// Note: Shall not be used with AUTOSAR SWC.

typedef struct{
	KeyPosition_T            Current_state;    // Current status button 
	KeyPosition_T            Previous_state;   // Previous status button.
}ButtonStat_T;

//=======================================================================================
// Private macros
//=======================================================================================
// Note, coding convention: shall be avoided to support debug capability.

//=======================================================================================
// Private function prototypes
//=======================================================================================

static void Processing_StartPushButton(ButtonAuth_rqst_T *ButtonAuth_rqst, DeviceAuthentication_rqst_T *pDeviceAuthentication_rqst,DriverAuthDeviceMatching_T *pRteOutData_DriverAuthDeviceMatching);
static void Processing_Keyswitch(ButtonStat_T *pKeyPosition, DeviceAuthentication_rqst_T *pDeviceAuthentication_rqst,DriverAuthDeviceMatching_T *pRteOutData_DriverAuthDeviceMatching, uint16 Timers[]);

//=======================================================================================
// End of file
//=======================================================================================