#ifdef INCABLOCK_HMICTRL_H
#error INCABLOCK_HMICTRL_H unexpected multi-inclusion
#else
#define INCABLOCK_HMICTRL_H
//=====================================================================================
// Included header files
//=====================================================================================


//=======================================================================================
// Private constants and enum definitions
//=======================================================================================


//=======================================================================================
// Private macros
//=======================================================================================
#define Const_TimeBase                          (20U)
#define CONST_IncablockTimer                    (0U)
#define CONST_NbOfTimers                        (1U)
#define PCODE_IncablockTimeout                  ((40U) / Const_TimeBase)

#ifndef CONST_Noninitialized
#define CONST_Noninitialized                    (0U)
#endif

#ifndef CONST_Initialized
#define CONST_Initialized                       (1U)
#endif
//=======================================================================================
// Private type definitions
//=======================================================================================
typedef struct
{
   PushButtonStatus_T  previousvalue;
   PushButtonStatus_T  currentvalue;
} PushButtonStatus;

typedef struct
{
   uint8 previousvalue;
   uint8 currentvalue;
} Locking_Switch_stat;

// structure for inputs 
typedef struct
{
   PushButtonStatus          BunkH1LockButtonStatus;
   PushButtonStatus          BunkH1UnlockButtonStatus;
   PushButtonStatus          BunkH2LockButtonStatus;
   PushButtonStatus          DashboardLockButtonStatus;
   Locking_Switch_stat       Locking_Switch_stat_serialized;
   VehicleModeDistribution_T SwcActivation_Living;
} InCabLock_HMICtrl_in_StructType;

 // structure for outputs 
typedef struct
{
   DoorLockUnlock_T   IncabDoorLockUnlock_rqst;
} InCabLock_HMICtrl_out_StructType;

//=======================================================================================
// Private data declarations
//=======================================================================================



//=======================================================================================
// Private function prototypes
//=======================================================================================

static void Get_RteInDataRead_Common(InCabLock_HMICtrl_in_StructType *pRteInData_Common);
static void InCabLock_HMICtrl_Logic(InCabLock_HMICtrl_in_StructType  *pInCabLck_in_data,
                                    uint16                           Timer[CONST_NbOfTimers],
                                    InCabLock_HMICtrl_out_StructType *pInCabLck_out_data); 

//=====================================================================================
// End of file
//=====================================================================================
#endif
