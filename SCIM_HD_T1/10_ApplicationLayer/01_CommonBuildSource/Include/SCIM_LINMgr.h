/*
 ***************************************************************************************
 ***************************************************************************************
 ***
 ***     File: SCIM_LINMgr.h
 ***
 ***     Project: SCIM
 ***
 ***     
 ***
 ***************************************************************************************
 ***************************************************************************************
 */

#ifdef SRE_SCIM_LINMgr_H
#error SRE_SCIM_LINMgr_H unexpected multi-inclusion
#else
#define SRE_SCIM_LINMgr_H

/*
 **=====================================================================================
 ** Included header files
 **=====================================================================================
 */

/*
 **=====================================================================================
 ** Public macros
 **=====================================================================================
 */
 
/*
 **=====================================================================================
 ** Public definitions
 **=====================================================================================
 */

# ifndef TRUE
# define TRUE  (1U)     // Temp value, To be replaced
# endif

# ifndef FALSE
# define FALSE (0U)    // Temp value, To be replaced
# endif

#define CONST_NbOfTimers					   (2U)
#define CONST_InitTimer   					   (0U)
#define CONST_SwDetTimer   				       (1U)
#define CONST_RunnableTimeBase			       (20U) //ms
#define CONST_InitTimeOut					   ((100) / CONST_RunnableTimeBase)
#define CONST_SwDetTimeOut					   ((3000) / CONST_RunnableTimeBase)
#define CONST_SleepTimeOut					   ((100) / CONST_RunnableTimeBase)
#define CONST_LINSchTableChangeRequested       (0x1)
#define CONST_LINSchTableChangeNoRequest       (0x0)
#define CONST_NoOfLIN_Networks                 (8U)
#define LIN_SCHETABLE_MODESWITCH_TRANSISTION   (0xFFU)       

/*
 **=====================================================================================
 ** Public type definitions
 **=====================================================================================
 */
typedef struct
{
   uint8 FSPAssign;
   uint8 SlaveNodePN;
   uint8 FlexSwDetection;
   uint8 AppRun;
} LINScheduleModes_T;

typedef enum
{
   SM_LINMgr_Init            = 0,
   SM_LINMgr_AppRun          = 1,
   SM_LINMgr_FlexSwDetection = 2,
   SM_LINMgr_SlaveNodePN     = 3,
   SM_LINMgr_FSPAssign       = 4,
   SM_LINMgr_Inactive        = 5,
   SM_LINMgr_Sleep           = 6
} enLINMgrState_T;

typedef struct
{
   enLINMgrState_T Current;
   enLINMgrState_T New;
} VarLINMgrState_T;

typedef struct
{
   VehicleMode_T Current;
   VehicleMode_T Previous;
} VarVehicleMode_T;

typedef struct
{
   VehicleModeDistribution_T Current;
   VehicleModeDistribution_T Previous;
} VarVehicleModeDistribution_T;

typedef struct
{
   Boolean Current;
   Boolean Previous;
} VarBooleanType;

typedef struct
{
   IOHWAB_UINT8           SelectParkedOrLivingPin;
   IOHWAB_BOOL            IsDo12VActivated;
   VGTT_EcuPinVoltage_0V2 Do12VPinVoltage;
   VGTT_EcuPinVoltage_0V2 BatteryVoltage;
   VGTT_EcuPinFaultStatus  DiagStatus;
} SCIM_LINMgr_IO_T;

typedef struct
{
   ComM_ModeType Current;
   ComM_ModeType Previous;
} VarComM_Mode_T;

typedef struct
{
	uint8 Current;
	uint8 Previous;
}VarLinSchTable_T;

typedef struct
{
   VehicleModeDistribution_T     LIN_SwcActivation;
   VarBooleanType                VarIsFlexSwDetCmplted; 
   VarVehicleMode_T              VarVehicleMode; 
   uint8                         CCNADRequest;
   uint8                         PNSNRequest;
   VarLinSchTable_T              CurScheTable[CONST_NoOfLIN_Networks];
} SCIMLINMgr_InData_T;

typedef enum
{
   ScimLin_NormalCom =0,
   ScimLin_BusOff    =1
}Scim_LinMgr_BusStat_T;

/*
 **=====================================================================================
 ** Public data declarations
 **=====================================================================================
 */

/*
 **=====================================================================================
 ** Public function declarations
 **=====================================================================================
 */
 /* function declarations */
static uint8 Get_RteReadLIN1CurSchTable(void);
static uint8 Get_RteReadLIN2CurSchTable(void);
static uint8 Get_RteReadLIN3CurSchTable(void);
static uint8 Get_RteReadLIN4CurSchTable(void);
static uint8 Get_RteReadLIN5CurSchTable(void);
static uint8 Get_RteReadLIN6CurSchTable(void);
static uint8 Get_RteReadLIN7CurSchTable(void);
static uint8 Get_RteReadLIN8CurSchTable(void);
static void Get_RteDataRead_Common(SCIMLINMgr_InData_T RteOutData[8]);
static void Get_RteDataWrite_Common(ComMode_LIN_Type *RteOutData);
static void SCIM_LINMgr_StateMachine(const SCIMLINMgr_InData_T *pInData,
                                     const uint8               LinBusIndex,                                          
                                     const LINScheduleModes_T *LINSchedule,
                                           VarLINMgrState_T   *pSM_VarLINMgrState,
                                           ComMode_LIN_Type   *LINComMode,
                                           uint8              *LinSchTable,
                                           uint8              *LinReqFlag);
static void SCIM_LINMgr_LinBusOff_Handler(const uint8            LinBusIndex,
                                          const VarLINMgrState_T *pSM_VarLINMgrState,
                                                ComMode_LIN_Type *LINComMode);
static void SM_VarLINMgrStateProcessing(const SCIMLINMgr_InData_T *pInData,
                                              VarLINMgrState_T    *pSM_VarLINMgrState);

/*! \cond SKIP_FUNCTION_PROTOTYPES */

/*! \endcond */
#endif /* SRE_SCIM_LINMgr_H */

/*
 **=====================================================================================
 ** End of file
 **=====================================================================================
 */
/*!
 * \}
 ************************************ End of file **************************************
 */
