#ifdef BUNKUSERINTERFACEHIGH2_LINMACTRL_LOGIC_IF_H
#error BUNKUSERINTERFACEHIGH2_LINMACTRL_LOGIC_IF_H unexpected multi-inclusion
#else
#define BUNKUSERINTERFACEHIGH2_LINMACTRL_LOGIC_IF_H
//!======================================================================================
//!
//! \file BunkUserInterfaceHigh2_LINMaCtrl_Logic_If.h
//!
//!======================================================================================

//=======================================================================================
// Public macros
//=======================================================================================

//=======================================================================================
// Public constants and enums definitions
//=======================================================================================

//=======================================================================================
// Public type definitions
//=======================================================================================

//=======================================================================================
// Public data declarations
//=======================================================================================

//=======================================================================================
// Public function prototypes
//=======================================================================================

FormalBoolean Generic_ANW_AudioRadio2_CondCheck(const BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_in_StructType *pBunkH2_WindowLift_data,
                                                const BunkUserInterfaceHigh2_LINMaCtrl_ButtonCOM_in_StructType  *pIn_ButtonCOM_data);
FormalBoolean Generic_ANW_ClimPHTimerSettings3_CondCheck(const SetParkHtrTmr_rqst *pVarBunkH2PHTi_rqs);
FormalBoolean Generic_ANW_LockCntrlCabRqst2_CondCheck(const PushButtonStatus  *pLIN_BunkH2LockButtonStatus);
FormalBoolean Generic_ANW_OtherInteriorLights4_CondCheck(const PushButtonStatus *pLIN_BunkH2IntLightDecBtn_stat,
                                                         const PushButtonStatus *pLIN_BunkH2IntLightIncBtn_stat,
                                                         const PushButtonStatus *pLIN_BunkH2IntLightActvnBtn_sta);
FormalBoolean Generic_ANW_PHActMaintainLiving6_Activate_CondCheck(const PushButtonStatus *pLIN_BunkH2OnOFF_ButtonStatus,
                                                                  const PushButtonStatus *pLIN_BunkH2ParkHeater_ButtonSta,
                                                                  const PushButtonStatus *pLIN_BunkH2TempDec_ButtonStatus,
                                                                  const PushButtonStatus *pLIN_BunkH2TempInc_ButtonStatus);
FormalBoolean Generic_ANW_PHActMaintainLvng6_DeActivate_CondCheck(const PushButtonStatus *pLIN_BunkH2TempInc_ButtonStatus,
                                                                  const PushButtonStatus *pLIN_BunkH2TempDec_ButtonStatus);
FormalBoolean Genric_ANW_PowerWindowsActivate3_Activate_CondCheck(const BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_in_StructType   *pin_Button_data);
FormalBoolean Genric_ANW_PowerWindowActivate3_DeActivate_CondCheck(const BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_in_StructType *pin_ButtonCOM_data);
void Generic_ANW_OtherInteriorLight4_Trigger(const FormalBoolean isRestAct_OtherInteriorLights,
                                             const FormalBoolean isActTrig_OtherInteriorLights);
void Generic_ANW_PHActMaintainLvng6Trigger(const FormalBoolean isRestAct_PHActMaintainLiving,
                                           const FormalBoolean isActTrig_PHActMaintainLiving,
                                           const FormalBoolean isDeActTrig_PHActMaintainLiving);
void Generic_ANW_LockCntrlCabRqst2_Trigger(const FormalBoolean isRestAct_LockControlCabRqst,
                                           const FormalBoolean isActTrig_LockControlCabRqst);
void Generic_ANW_ClimPHTimerSetting3_Trigger(const FormalBoolean isRestAct_ClimPHTimerSettings,
                                             const FormalBoolean isActTrig_ClimPHTimerSettings);
void Generic_ANW_AudioRadio2_Trigger(const FormalBoolean isRestAct_AudioRadio,
                                     const FormalBoolean isActTrig_AudioRadio);
void Generic_ANW_RoofHatchRequest3_Trigger(const FormalBoolean isRestAct_RoofHatchRequest3,
                                           const FormalBoolean isActTrig_RoofHatchRequest3,
                                           const FormalBoolean isDeActTrig_RoofHatchRequest3);
void Generic_ANW_PowerWindowsActivate3_Trigger(const FormalBoolean isRestAct_PowerWindowsActivate3,
                                               const FormalBoolean isActTrig_PowerWindowsActivate3,
                                               const FormalBoolean isDeActTrig_PowerWindowsActivate3);
FormalBoolean Generic_ANW_RoofHatchRequest3_DeActivate_CondCheck(const PushButtonStatus *pBunkH2RoofhatchCloseBtn_St,
                                                                 const PushButtonStatus *pBunkH2RoofhatchOpenBtn_Sta);
FormalBoolean Generic_ANW_RoofHatchRequest3_Activate_CondCheck(const PushButtonStatus *pBunkH2RoofhatchCloseBtn_St,
                                                               const PushButtonStatus *pBunkH2RoofhatchOpenBtn_Sta);

//=======================================================================================
// End of file
//=======================================================================================
#endif
