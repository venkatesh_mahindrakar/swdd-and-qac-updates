#ifdef INTERIORLIGHTPANEL_2_LINMASTCTRL_H
#error INTERIORLIGHTPANEL_2_LINMASTCTRL_H unexpected multi-inclusion
#else
#define INTERIORLIGHTPANEL_2_LINMASTCTRL_H
//=====================================================================================
// Included header files
//=====================================================================================

#include "Rte_Type.h"

//=====================================================================================
// Private constants and enum definitions
//=====================================================================================

//=====================================================================================
// Private macros
//=====================================================================================

#define CONST_HmiArchitecture                       (TRUE)    // HmiArchitectureType calibration missing in template
#define CONST_InteriorLightPanel2LinMasterCycleTime (20U)     // 20ms
#define CONST_AnwDimmingAdj_Timeout                 ((3000U) / CONST_InteriorLightPanel2LinMasterCycleTime)  // 3s Deactivation Timeout value
#define CONST_AnwInteriorLights3_Timeout            ((3000U) / CONST_InteriorLightPanel2LinMasterCycleTime)  // 3s Deactivation Timeout value
#define CONST_Fault_InActive                        (0U)
#define CONST_Fault_Active                          (1U)
#define CONST_SlaveFCIFault_InActive                (0U)
#define CONST_SlaveFCIFault_Active                  (1U)

//=====================================================================================
// Private type definitions
//=====================================================================================

typedef struct
{
   PushButtonStatus_T   Current;
   PushButtonStatus_T   Previous;
} PushButtonStatus;

typedef struct
{
   FreeWheel_Status_T    Current;
   FreeWheel_Status_T    Previous;
} FreeWheel_Status;

// Input Structure 
typedef struct
{
   ComMode_LIN_Type       ComMode_LIN4;
   DiagInfo_T             DiagInfoILCP2_LIN4;
   PushButtonStatus       LIN_IntLghtCenterBtnFreeWhl_s;
   FreeWheel_Status       LIN_IntLghtDimmingLvlFreeWhl_s;
   PushButtonStatus       LIN_IntLghtMaxModeBtnPnl2_s;
   PushButtonStatus       LIN_IntLghtNightModeBt2_s;
   PushButtonStatus       LIN_IntLghtRestModeBtnPnl2_s;
   ResponseErrorILCP2_T   ILCP2_ResponseError;
   boolean                IsUpdated_LIN_IntLghtDimmingLvl;
} InteriorLightPanel2LINMastCtrl_InData_T;

// Output Structure 
typedef struct
{
   PushButtonStatus_T   IntLghtCenterBtnFreeWhl_stat;
   FreeWheel_Status_T   IntLghtDimmingLvlFreeWhl_stat;
   PushButtonStatus_T   IntLghtMaxModeBtnPnl2_stat;
   PushButtonStatus_T   IntLghtNightModeBtn2_stat;
   PushButtonStatus_T   IntLghtRestModeBtnPnl2_stat;
} InteriorLightPanel2LINMastCtrl_OutData_T;

//=====================================================================================
// Private data declarations
//=====================================================================================

//=====================================================================================
// Private function prototypes
//=====================================================================================

static void RteDataWrite_Common(const InteriorLightPanel2LINMastCtrl_OutData_T *pRteOutData_Common,
                                      boolean                                  *pIsUpdated_LIN_IntLghtDimmingLvl);
static Std_ReturnType Get_RteDataRead_Common(InteriorLightPanel2LINMastCtrl_InData_T *pRteInData_Common);
static void Generic_ANW_DimmingAdjustment2_Logic(const FormalBoolean isRestAct_DimmingAdjustment2,
                                                 const FormalBoolean isActTrigDet_DimmingAdjustment2);
static FormalBoolean Generic_ANW_DimmingAdj2_CondCheck(const FreeWheel_Status *pInData_DimmingAdj);
static void GenericANW_OtherInteriorLights3(const FormalBoolean  isRestAct_OtherInteriorLights3,
                                            const FormalBoolean  isActTrigDet_OtherInteriorLights3);
static FormalBoolean Generic_ANW_OtherInteriorLights3_Cond(const InteriorLightPanel2LINMastCtrl_InData_T *pInData);
static void Signals_gateway_Logic_InteriorLightPanel2LINMastCtrl(const InteriorLightPanel2LINMastCtrl_InData_T  *pInData_GW,
                                                                       InteriorLightPanel2LINMastCtrl_OutData_T *pOutData_GW);
static void FallbackLogic_InteriorLightPanel2LINMastCtrl(InteriorLightPanel2LINMastCtrl_OutData_T *pOutData_Fallback);
static void Deactivation_Logic_InteriorLightPanel2LINMastCtrl(InteriorLightPanel2LINMastCtrl_OutData_T  *pOutData_DeAct);
static boolean FCI_Indication_InteriorLightPanel2LINMastCtrl(const DiagInfo_T  *pDiagInfoILCP2_LIN4);

//=====================================================================================
// End of file
//=====================================================================================
#endif
