/*
 ***************************************************************************************
 ***************************************************************************************
 ***
 ***     File: Keyfob_UICtrl.h
 ***
 ***     Project: SCIM
 ***
 ***     
 ***
 ***************************************************************************************
 ***************************************************************************************
 */

/*!
 ***************************************************************************************
 * \file
 *
 * \ingroup application_swc
 *
 * \subject
 * Definition file for the Keyfob_UICtrl runnables.
 *
 * \version
 * 
 *
 * \{
 ***************************************************************************************
 */
#ifndef SRE_Keyfob_UICtrl_H
#define SRE_Keyfob_UICtrl_H

/*
 **=====================================================================================
 ** Included header files
 **=====================================================================================
 */

#include "Rte_Type.h"

/*
 **=====================================================================================
 ** macros
 **=====================================================================================
 */


/*
 **=====================================================================================
 ** definitions
 **=====================================================================================
 */

// RT Button ID
#define CONST_ButtonID_Empty           0
#define CONST_ButtonID_Lock            1
#define CONST_ButtonID_UnLock          2
#define CONST_ButtonID_ApproachLight   3
#define CONST_ButtonID_SuperLock       4
#define CONST_ButtonID_Spare1          5
#define CONST_ButtonID_Spare2          6

#define NbOfButtons                    6
#define ButtonBufferSize               (NbOfButtons + 1)

#define CONST_LockButtonSendTimer          0
#define CONST_UnlockButtonSendTimer        1
#define CONST_SuperLockButtonSendTimer     2
#define CONST_ApproachButtonSendTimer      3
#define CONST_SingleButtonSendTimer        4
#define CONST_DoubleButtonSendTimer        5
#define CONST_DoubleButtonTimeout          6
#define CONST_KeyfobUICtrl_NbOfTimers      7

#define CONST_KeyfobUICtrl_FuncPeriod  (20u)

#define TEMP_P1XXX_KeyfobDoublePressTimeout     (2000u/CONST_KeyfobUICtrl_FuncPeriod)
#define TEMP_P1XXX_KeyfobLockLongPressTimeout   (32u)
#define CONST_KeyfobButtonStuck                 (63u)

/*
 **=====================================================================================
 ** type definitions
 **=====================================================================================
 */

typedef struct
{
   uint16         PressCounterCurrent;
   uint16         PressCounterPrevious;
   uint16         PressTime;
   boolean        isShortPressDetected;
} ButtonData_StructType;

typedef struct
{
   uint16         KeyfobLockDoublePressTimeout;
   uint16         KeyfobLockLongPressTimeout;
   uint8          BatteryStatus;
   ButtonStatus   KeyfobButtonStatus;
} Keyfob_UICtrl_in_StructType;

typedef struct
{
   PushButtonStatus_T      ApproachLightButton_Status;
   NotDetected_T           KeyfobLockBtn_DoublePress;
   PushButtonStatus_T      KeyfobLockButton_Status;
   KeyfobPanicButton_Status_T KeyfobPanicButton_Status;
   PushButtonStatus_T      KeyfobSuperLockButton_Status;
   PushButtonStatus_T      KeyfobUnlockButton_Status;
   PushButtonStatus_T      MainSwitchButton_Status;
} Keyfob_UICtrl_out_StructType;

typedef enum  
{
   SM_KeyfobUICtrl_Idle = 0u,
   SM_KeyfobUICtrl_SinglePress,
   SM_KeyfobUICtrl_SinglePressIDChange,
   SM_KeyfobUICtrl_SingleLongPress,
   SM_KeyfobUICtrl_SingleStuck,
   SM_KeyfobUICtrl_DoublePress,
   SM_KeyfobUICtrl_DoublePressIDChange,
   SM_KeyfobUICtrl_DoubleLongPressStart,
   SM_KeyfobUICtrl_DoublePressStuck
} SM_KeyfobUICtrl_Enum;

typedef struct
{
   SM_KeyfobUICtrl_Enum     Current;
   SM_KeyfobUICtrl_Enum     New;
} SM_KeyfobUICtrl_Enum_Type;

/*
 **=====================================================================================
 ** data declarations
 **=====================================================================================
 */

/*
 **=====================================================================================
 ** function declarations
 **=====================================================================================
 */

static void ANW_ApproachLights1(boolean isButtonPressed);
static void ANW_LockControlKeyfobRqst(boolean isButtonPressed);
static void StoreKeyfobPreviousButtonStatus(ButtonStatus *pPrevBtnStatus, ButtonStatus *pCurBtnStatus);
static void CopyKeyfobPreviousButtonStatus(ButtonStatus *pBtnStatusCopyFrom, ButtonStatus *pBtnStatusCopyTo);


/*! \cond SKIP_FUNCTION_PROTOTYPES */


/*! \endcond */
#endif /* SRE_Keyfob_UICtrl_H */

/*
 **=====================================================================================
 ** End of file
 **=====================================================================================
 */
/*!
 * \}
 ************************************ End of file **************************************
 */
