#ifdef BUNKUSERINTERFACEHIGH2_LINMACTRL_IF_H
#error BUNKUSERINTERFACEHIGH2_LINMACTRL_IF_H unexpected multi-inclusion
#else
#define BUNKUSERINTERFACEHIGH2_LINMACTRL_IF_H
//!======================================================================================
//!
//! \file BunkUserInterfaceHigh2_LINMaCtrl_If.h
//!
//!======================================================================================

//=======================================================================================
// Public macros
//=======================================================================================

//=======================================================================================
// Public constants and enums definitions
//=======================================================================================

//=======================================================================================
// Public type definitions
//=======================================================================================
typedef struct
{
   PushButtonStatus_T Previous;
   PushButtonStatus_T Current;
} PushButtonStatus;

typedef struct
{
   ParkHeaterTimer_cmd_T previous;
   ParkHeaterTimer_cmd_T current;
} ParkHeaterTimer_cmd;

typedef struct
{
   Hours8bit_T previous;
   Hours8bit_T current;
} Hours8bit;

typedef struct
{
   Minutes8bit_T previous;
   Minutes8bit_T current;
} Minutes8bit;

typedef struct
{
   Std_ReturnType RetVal_LECM2;
   Std_ReturnType RetVal_BunkH2PowerWinOpenPSBtn_st;
   Std_ReturnType RetVal_BunkH2PHTi_rqs_Timer_cmd;
} ReturnType;

typedef struct
{
   ParkHeaterTimer_cmd  Timer_cmd_RE;
   Hours8bit            StartTimeHr_RE;
   Minutes8bit          StartTimeMin_RE;
   Hours8bit            DurnTimeHr_RE;
   Minutes8bit          DurnTimeMin_RE;
} SetParkHtrTmr_rqst;

// Structure For Inputs 
typedef struct
{
   PushButtonStatus   LIN_BunkH2PowerWinCloseDSBtn_s;
   PushButtonStatus   LIN_BunkH2PowerWinClosePSBtn_s;
   PushButtonStatus   LIN_BunkH2PowerWinOpenDSBtn_st;
   PushButtonStatus   LIN_BunkH2PowerWinOpenPSBtn_st;
} BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_in_StructType;

typedef struct
{
   IntLghtLvlIndScaled_cmd_T   IntLghtLvlIndScaled_cmd;
   InteriorLightMode_T         IntLghtModeInd_cmd;
   AlmClkCurAlarm_stat_T       AlmClkCurAlarm_stat;
   OffOn_T                     AudioSystemStatus;
   VolumeValueType_T           AudioVolumeIndicationCmd;
   BTStatus_T                  BTStatus;
   DeviceIndication_T          PhoneButtonIndication_cmd;
   SetParkHtrTmr_rqst_T        LIN_BunkH2PHTi_rqs;
   DiagInfo_T                  DiagInfoLECM2;
   ResponseErrorLECM2_T        LECM2_ResponseError;
   ComMode_LIN_Type            ComMode_LIN1;
} BunkUserInterfaceHigh2_LINMaCtrl_in_StructType;

typedef struct
{
   PushButtonStatus         LIN_BunkH2AudioOnOff_ButtonSta;
   PushButtonStatus         LIN_BunkH2Fade_ButtonStatus;
   PushButtonStatus         LIN_BunkH2OnOFF_ButtonStatus;
   PushButtonStatus         LIN_BunkH2ParkHeater_ButtonSta;
   PushButtonStatus         LIN_BunkH2Phone_ButtonStatus;
   PushButtonStatus         LIN_BunkH2TempDec_ButtonStatus;
   PushButtonStatus         LIN_BunkH2TempInc_ButtonStatus;
   PushButtonStatus         LIN_BunkH2VolumeDown_ButtonSta;
   PushButtonStatus         LIN_BunkH2VolumeUp_ButtonStatu;
   PushButtonStatus         LIN_BunkH2RoofhatchCloseBtn_St;
   PushButtonStatus         LIN_BunkH2RoofhatchOpenBtn_Sta;
   PushButtonStatus         LIN_BunkH2IntLightActvnBtn_sta;
   PushButtonStatus         LIN_BunkH2IntLightDecBtn_stat;
   PushButtonStatus         LIN_BunkH2IntLightIncBtn_stat;
   PushButtonStatus         LIN_BunkH2LockButtonStatus;
   AlmClkSetCurAlm_rqst_T   LIN_AlmClkSetCurAlm_rqst;
} BunkUserInterfaceHigh2_LINMaCtrl_ButtonCOM_in_StructType;

// Structure for outputs 
typedef struct
{
   PushButtonStatus_T    BunkH2OutPwrWinCloseDSBtn_stat;
   PushButtonStatus_T    BunkH2OutPwrWinClosePSBtn_stat;
   PushButtonStatus_T    BunkH2OutPwrWinOpenDSBtn_stat;
   PushButtonStatus_T    BunkH2OutPwrWinOpenPSBtn_stat;
} BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_out_StructType;

typedef struct
{
   IntLghtLvlIndScaled_cmd_T   LIN_IntLghtLvlIndScaled_cmd;
   InteriorLightMode_T         LIN_IntLghtModeInd_cmd; /* 3bit event, 1bit flag */
   AlmClkCurAlarm_stat_T       LIN_AlmClkCurAlarm_stat;
   OffOn_T                     LIN_AudioSystemStatus;
   VolumeValueType_T           LIN_AudioVolumeIndicationCmd;
   BTStatus_T                  LIN_BTStatus;
   DeviceIndication_T          LIN_PhoneButtonIndication_cmd;
   SetParkHtrTmr_rqst_T        BunkH2PHTimer_rqst;
} BunkUserInterfaceHigh2_LINMaCtrl_out_StructType;

// Output structure for 'Signals Gateway By Buttons'
typedef struct
{
   PushButtonStatus_T      BunkH2AudioOnOff_ButtonStatus;
   PushButtonStatus_T      BunkH2Fade_ButtonStatus;
   PushButtonStatus_T      BunkH2OnOFF_ButtonStatus;
   PushButtonStatus_T      BunkH2ParkHeater_ButtonStatus;
   PushButtonStatus_T      BunkH2Phone_ButtonStatus;
   PushButtonStatus_T      BunkH2TempDec_ButtonStatus;
   PushButtonStatus_T      BunkH2TempInc_ButtonStatus;
   PushButtonStatus_T      BunkH2VolumeDown_ButtonStatus;
   PushButtonStatus_T      BunkH2VolumeUp_ButtonStatus;
   PushButtonStatus_T      BunkH2RoofhatchCloseBtn_Stat;
   PushButtonStatus_T      BunkH2RoofhatchOpenBtn_Stat;
   PushButtonStatus_T      BunkH2IntLightActvnBtn_stat;
   PushButtonStatus_T      BunkH2IntLightDecreaseBtn_stat;
   PushButtonStatus_T      BunkH2IntLightIncreaseBtn_stat;
   PushButtonStatus_T      BunkH2LockButtonStatus;
   AlmClkSetCurAlm_rqst_T  AlmClkSetCurAlm_rqst;
} BunkUserInterfaceHigh2_LINMaCtrl_Button_out_StructType;


//=======================================================================================
// Public data declarations
//=======================================================================================

//=======================================================================================
// Public function prototypes
//=======================================================================================

//=======================================================================================
// End of file
//=======================================================================================
#endif 





















