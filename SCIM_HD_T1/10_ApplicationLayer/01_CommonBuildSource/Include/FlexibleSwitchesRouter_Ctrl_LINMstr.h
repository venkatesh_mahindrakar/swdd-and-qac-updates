#ifdef FSPROUTER_CTRL_LINMASTER_H
#error FSPROUTER_CTRL_LINMASTER_H unexpected multi-inclusion
#else
#define FSPROUTER_CTRL_LINMASTER_H
//=====================================================================================
// Included header files
//=====================================================================================
#include "FlexibleSwitchesRouter_Ctrl_If.h"
//=======================================================================================
// Private constants and enum definitions
//=======================================================================================

const uint8 MaxNbOfFspByBus[MAX_LIN_NO] = {2,4,2,1,1};

//=======================================================================================
// Private macros
//=======================================================================================

#define CONST_Tester_Present         (0x80U)
#define CONST_Tester_Notpresent      (0x7FU)

#define CONST_None                   (0x00U)
#define CONST_Inactive               (0x01U)
#define CONST_Active                 (0x02U)

//Timers
#define CONST_Runnable_CycleTIme       (20U)
#define CONST_TransitModeFilter_LIN1   (0U)
#define CONST_TransitModeFilter_LIN2   (1U)
#define CONST_TransitModeFilter_LIN3   (2U)
#define CONST_TransitModeFilter_LIN4   (3U)
#define CONST_TransitModeFilter_LIN5   (4U)
#define CONST_NbOfTimers               (5U)

#define CONST_TransitionFilterValue    (200U/20U)

//=======================================================================================
// Private type definitions
//=======================================================================================
/*typedef struct
{
   FSPSwitchStatus_T  FSPSwStatus[MAX_FSP_NO]; // input from LIN : FSP1 ~ FSPB
   DeviceIndication_T FSPIndicationCmd[MAX_FSP_NO][MAX_SLOT_NO]; // input from FlexibleSwitchesRouter_Ctrl_Router
   ComMode_LIN_Type   ComMode_LIN1;
   ComMode_LIN_Type   ComMode_LIN2;
   ComMode_LIN_Type   ComMode_LIN3;
   ComMode_LIN_Type   ComMode_LIN4;
   ComMode_LIN_Type   ComMode_LIN5;
} FlexibleSwitchesRouter_Ctrl_LINMstr_in_StructType;*/


/*typedef struct
{
   PushButtonStatus_T FSPSwStatus[MAX_FSP_NO][MAX_SLOT_NO]; // output to FlexibleSwitchesRouter_Ctrl_Router
   FSPIndicationCmd_T FSPIndicationCmd[MAX_FSP_NO]; // output to LIN : FSP1 ~ FSPB
} FlexibleSwitchesRouter_Ctrl_LINMstr_out_StructType;*/


//=======================================================================================
// Private data declarations
//=======================================================================================

//=======================================================================================
// Private function prototypes
//=======================================================================================
static void FlexibleSwRouter_Ctrl_LINMstr_Logic(const FSPSwitchStatus_T   *pRteInData_OneFSPSwStatus,
                                                            PushButtonStatus_T  pRteOutData_OneFSPSwStatus[sizeof(FSPSwitchStatusArray_T)]);
static void Get_RteInDataRead_Common (Std_ReturnType    pRetValue[MAX_FSP_NO],
                                      FSPSwitchStatus_T pRteInData_FSPSwStatus[MAX_FSP_NO],
                                      uint8             pFciData[MAX_FSP_NO],
                                      uint8             pComMode_LIN[MAX_LIN_NO]);
static void Update_Irv_data(uint8 pFspInFailureStatus [MAX_LIN_NO],
                            uint8 pNbOfFspDetectedByCCNAD [MAX_LIN_NO]);
static void FallbackModeLogic_MissingMessage(const uint8              pComMode[MAX_LIN_NO],
                                             const Std_ReturnType     pRetValue[MAX_FSP_NO],
                                                   uint8              pFspInFailureStatus [MAX_LIN_NO],
                                             const uint8              pNbOfFspDetectedByCCNAD [MAX_LIN_NO],
                                                   PushButtonStatus_T pRteOutData_FSPSwitchStatus[MAX_FSP_NO][MAX_SLOT_NO],
                                                   uint16             *pTimer);
static void FallbackProcessing_ContactLost(const uint8              pRteInData_FciCode [MAX_FSP_NO],
                                                 PushButtonStatus_T pRteOutData_FSPSwitchStatus[MAX_FSP_NO][MAX_SLOT_NO]);
static void  FciToDtcConversion_FspSupply(const uint8 pRteInData_FciCode [MAX_FSP_NO]);
static void FallbackMode_MissingMessage_FSP(const uint8              pComMode_LIN[MAX_LIN_NO],
                                            const uint8              LinBus,
                                            const uint8              FspId,
                                            const Std_ReturnType     pRetValue[MAX_FSP_NO],
                                                  uint8              pFspInFailureStatus [MAX_LIN_NO],
                                                  PushButtonStatus_T pRteOutData_FSPSwitchStatus[MAX_FSP_NO][MAX_SLOT_NO],
                                                  uint16             *pTimer);
static void FSR_Ctrl_LINMstr_Diagnosticindication(FSPDiagInfo_T   *pFSPDiagInfoLIN);
static void IOCtrlService_LinOutputSignalControl(const uint8              OverrideData,
                                                       FSPIndicationCmd_T pOutData[MAX_FSP_NO]);

//=======================================================================================
// End of file
//=======================================================================================/
#endif
