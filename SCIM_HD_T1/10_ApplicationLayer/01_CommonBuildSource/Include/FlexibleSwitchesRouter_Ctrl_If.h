#ifdef FSPROUTER_CTRL_IF_H
#error FSPROUTER_CTRL_IF_H unexpected multi-inclusion
#else
#define FSPROUTER_CTRL_IF_H
//=====================================================================================
// Included header files
//=====================================================================================


//=======================================================================================
// Public type definitions
//=======================================================================================

//=======================================================================================
// Public constants and enums definitions
//=======================================================================================

#define SLOT0_UPPER                   (0U)
#define SLOT1_UPPER                   (1U)
#define SLOT2_UPPER                   (2U)
#define SLOT3_UPPER                   (3U)
#define SLOT0_LOWER                   (4U)
#define SLOT1_LOWER                   (5U)
#define SLOT2_LOWER                   (6U)
#define SLOT3_LOWER                   (7U)

#define SLOT0                         (0U)
#define SLOT1                         (1U)
#define SLOT2                         (2U)
#define SLOT3                         (3U)
#define SLOT4                         (4U)
#define SLOT5                         (5U)
#define SLOT6                         (6U)
#define SLOT7                         (7U)
#define MAX_SLOT_NO                   (8U)

#define FSP1                          (0U)
#define FSP2                          (1U)
#define FSP3                          (2U)
#define FSP4                          (3U)
#define FSP5                          (4U)
#define FSP6                          (5U)
#define FSP7                          (6U)
#define FSP8                          (7U)
#define FSP9                          (8U)
#define FSPB                          (9U)
#define MAX_FSP_NO                    (10U)

#define LIN1                          (0U)
#define LIN2                          (1U)
#define LIN3                          (2U)
#define LIN4                          (3U)
#define LIN5                          (4U)
#define MAX_LIN_NO                    (5U)

//=======================================================================================
// Public macros
//=======================================================================================

                          

//=======================================================================================
// End of file
//=======================================================================================
# endif  