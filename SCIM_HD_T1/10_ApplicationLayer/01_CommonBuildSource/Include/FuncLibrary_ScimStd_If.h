//!======================================================================================
//!
//! \file FuncLibrary_ScimStd_If.h
//!
//!======================================================================================

#ifndef FuncLibrary_FuncLibrary_ScimStd_If_H
// To protect against multi-inclusion : FuncLibrary_FuncLibrary_ScimStd_If_H must be unique
#define FuncLibrary_FuncLibrary_ScimStd_If_H

#include "RTE_Type.h"
#include "Platform_Types.h"
//=======================================================================================
// Public type definitions
//=======================================================================================

typedef enum
{
   NO = 0,
   YES = 1
} FormalBoolean;

typedef uint8 NvramStdStorage_T[sizeof(StandardNVM_T)];
   
//=======================================================================================
// Public constants and enums definitions
//=======================================================================================

#define CONST_NvramVersionIndex (0u)

//=======================================================================================
// Public data declarations
//=======================================================================================
// Note: Shall not be used with AUTOSAR SWC.


//=======================================================================================
// Public macros
//=======================================================================================
// Note, coding convention: shall be avoided to support debug capability.

//!======================================================================================
//!
//! \brief This Macro processing the logic for 'Inter-communication' of input ports
//!
//! \param    RTE_return                Provides input port status
//! \param    extInputPortVar           Providing the input port read variable to check & update based on RTE Failure Events
//! \param    RteNotAvailableValue      Value to set in case of RTE_E_COM_BUSY, RTE_E_NEVER_RECEIVED
//! \param    RteComErrorValue          Value to set in case of RTE_E_INVALID, RTE_E_MAX_AGE_EXCEEDED, RTE_E_UNCONNECTED
//!
//!======================================================================================
#define MACRO_StdRteRead_ExtRPort(RTE_return,extInputPortVar,RteNotAvailableValue,RteComErrorValue) switch (RTE_return)\
                                                                       {\
                                                                          case RTE_E_COM_STOPPED:\
                                                                             /*explicit break through to RTE_E_OK: no fallback mode for bus sleep behavior*/ \
                                                                          case RTE_E_OK:\
                                                                            /*do nothing: no fallback mode*/ \
                                                                          break;\
                                                                          case RTE_E_NEVER_RECEIVED:\
                                                                             /*explicit break through to fallback processing: process as Not available*/ \
                                                                          case RTE_E_COM_BUSY:\
                                                                             extInputPortVar = RteNotAvailableValue;\
                                                                          break;\
                                                                          case RTE_E_INVALID:\
                                                                             /*explicit break through to fallback processing: process as Error*/ \
                                                                          case RTE_E_UNCONNECTED:\
                                                                          case RTE_E_MAX_AGE_EXCEEDED:\
                                                                             extInputPortVar = RteComErrorValue;\
                                                                          break;\
                                                                          default:\
                                                                             extInputPortVar = RteNotAvailableValue;\
                                                                          break;\
                                                                       }
//!======================================================================================
//!
//! \brief This Macro processing the logic for 'Intra-communication' of input ports
//!
//! \param    RTE_return        Provides input port status
//! \param    IntInputPortVar   Providing the input port to check & update based on RTE Failure Events
//! \param    RteError          Value to set in case of RTE error (RTE_E_NOK, RTE_E_UNCONNECTED, etc.)
//!
//!======================================================================================
#define MACRO_StdRteRead_IntRPort(RTE_return,IntInputPortVar,RteError) if (RTE_E_OK != RTE_return)\
                                                          {\
                                                             IntInputPortVar = RteError;\
                                                          }\
                                                          else\
                                                          {\
                                                             /*do nothing:keep previous value*/\
                                                          }
//=======================================================================================
// Public function prototypes
//=======================================================================================

#endif //FuncLibrary_FuncLibrary_ScimStd_If_H


//=======================================================================================
// End of file
//=======================================================================================