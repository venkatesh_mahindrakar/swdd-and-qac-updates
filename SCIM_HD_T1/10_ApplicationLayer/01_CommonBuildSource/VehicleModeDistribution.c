/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  VehicleModeDistribution.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  VehicleModeDistribution
 *  Generated at:  Fri Jun 12 17:10:06 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <VehicleModeDistribution>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_VehicleModeDistribution.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "VehicleModeDistribution.h"
#include "FuncLibrary_ScimStd_If.h"
#include "FuncLibrary_AnwStateMachine_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Fsc_OperationalMode_T: Enumeration of integer in interval [0...255] with enumerators
 *   FSC_ShutdownReady (0U)
 *   FSC_Reduced_12vDcDcLimit (1U)
 *   FSC_Reduced (2U)
 *   FSC_Operating (3U)
 *   FSC_Protecting (4U)
 *   FSC_Withstand (5U)
 *   FSC_NotAvailable (6U)
 * Living12VPowerStability: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Active (1U)
 *   Stable (2U)
 *   Error (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 * StandardNVM_T: Array with 4 element(s) of type uint8
 *
 *********************************************************************************************************************/


#define VehicleModeDistribution_START_SEC_CODE
#include "VehicleModeDistribution_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EIJ_Data_P1EIJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EIJ_Data_P1EIJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EIJ_Data_P1EIJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EIJ_Data_P1EIJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1QXJ_Data_P1QXJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1RG1_Data_P1RG1_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VehicleModeDistribution_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T *data)
 *   Std_ReturnType Rte_Read_Living12VPowerStability_Living12VPowerStability(Living12VPowerStability *data)
 *   Std_ReturnType Rte_Read_VehicleMode_VehicleMode(VehicleMode_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I(uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_SwcActivation_Accessory_Accessory(VehicleModeDistribution_T data)
 *   Std_ReturnType Rte_Write_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T data)
 *   Std_ReturnType Rte_Write_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T data)
 *   Std_ReturnType Rte_Write_SwcActivation_LIN_SwcActivation_LIN(VehicleModeDistribution_T data)
 *   Std_ReturnType Rte_Write_SwcActivation_Living_Living(VehicleModeDistribution_T data)
 *   Std_ReturnType Rte_Write_SwcActivation_Parked_Parked(VehicleModeDistribution_T data)
 *   Std_ReturnType Rte_Write_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T data)
 *   Std_ReturnType Rte_Write_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I(const uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Write_VehicleModeInternal_VehicleMode(VehicleMode_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_UR_ANW_CIOMOperStateRedundancy_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_CIOMOperStateRedundancy_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VehicleModeDistribution_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VehicleModeDistribution_CODE) VehicleModeDistribution_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VehicleModeDistribution_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC In/Out data structures   
   //Define input RteInData Common
   static VehicleModeDistribution_InData_T RteInData_Common = {FSC_Operating, 
                                                               {LIVING12VPOWERSTABILITY_INACTIVE}, 
                                                               VehicleMode_NotAvailable};
   // Define input RteOutData Common
   static VehicleModeDistribution_OutData_T RteOutData_Common = {NonOperational,
                                                                 NonOperational,
                                                                 NonOperational,
                                                                 NonOperational,
                                                                 NonOperational,
                                                                 NonOperational,
                                                                 NonOperational,
                                                                 {VehicleMode_NotAvailable}};

   //! ##### Read the logic common RTE ports and process fallback modes for RTE events: 'RteInDataRead_Common'
   RteDataRead_Common(&RteInData_Common);

   //! ##### Write the logic common RTE ports and process fallback modes for RTE events: 'RteInDataRead_Common'
   RteDataWrite_Common(&RteOutData_Common);

   //! ##### Process internal vehicle mode logic
   VehicleModeInternal_OutputProcessing (&RteInData_Common.VehicleMode, &RteOutData_Common.VehicleModeInternal);   

   //! ##### Process redundancy ANW network logic: 'ANW_CIOMOperStateRedundancy()'
   ANW_CIOMOperStateRedundancy (&RteOutData_Common.VehicleModeInternal);

   //! ##### Function SWCActivation_OutputProcessing(): Set SWCActivation outputs accordingly to VehicleMode
   SWCActivation_OutputProcessing(&RteInData_Common, &RteOutData_Common);

   RteOutData_Common.VehicleModeInternal.Previous = RteOutData_Common.VehicleModeInternal.Current;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define VehicleModeDistribution_STOP_SEC_CODE
#include "VehicleModeDistribution_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void VehicleModeInternal_OutputProcessing (const VehicleMode_T *pVehicleMode_BB2, VehicleModeVar_T *pVehicleModeInternal)
{
   uint8 vehicleMode_NvramValue[sizeof(NvramStdStorage_T)] = {0xFF};
   
   if (VehicleMode_Running >= *pVehicleMode_BB2)
   {
     pVehicleModeInternal->Current = *pVehicleMode_BB2;
   }
   else
   {
      // do nothing: filter the invalid Vehicle Mode
   }
   
   if (VehicleMode_Running < pVehicleModeInternal->Current)
   {
      Rte_Read_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I (vehicleMode_NvramValue);
      if ((CONST_VehicleModeNvramVersion == vehicleMode_NvramValue[CONST_NvramVersionIndex])
         && (VehicleMode_Running >= vehicleMode_NvramValue[CONST_VehicleModeNvramValuePos]))
      {
         pVehicleModeInternal->Current = vehicleMode_NvramValue[CONST_VehicleModeNvramValuePos];
      }
      else
      {
         // set default value: no NVRAM value storage or incompatible version
         pVehicleModeInternal->Current = VehicleMode_Living;
      }
   }
   else
   {
      // do nothing: no fallback mode
   }
   
   if ((pVehicleModeInternal->Current != pVehicleModeInternal->Previous)
      && (VehicleMode_Running >= pVehicleModeInternal->Current))
   
   {
      vehicleMode_NvramValue[CONST_NvramVersionIndex] = CONST_VehicleModeNvramVersion;
      vehicleMode_NvramValue[CONST_VehicleModeNvramValuePos] = (uint8) pVehicleModeInternal->Current;
      Rte_Write_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I(vehicleMode_NvramValue);
   }
   else
   {
      // do nothing: no value change to store in NVRAM
   }
}

static void RteDataRead_Common (VehicleModeDistribution_InData_T *pRteInData_Common)
{
   Std_ReturnType retValue = RTE_E_INVALID;

   (void)Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(&pRteInData_Common->FscMode);

   pRteInData_Common->Living12VPowerStabilityVar.Previous = pRteInData_Common->Living12VPowerStabilityVar.Current;
   retValue = Rte_Read_Living12VPowerStability_Living12VPowerStability(&pRteInData_Common->Living12VPowerStabilityVar.Current);
   if (RTE_E_OK != retValue)
   {
      pRteInData_Common->Living12VPowerStabilityVar.Current = LIVING12VPOWERSTABILITY_ERROR;
   }
   else
   {
      // Do nothing, connected port
   }
   
   retValue = Rte_Read_VehicleMode_VehicleMode(&pRteInData_Common->VehicleMode);
   switch (retValue)
   {
      case RTE_E_COM_STOPPED:
         // explicit break through to RTE_E_OK: no fallback mode for bus sleep behavior
      case RTE_E_OK:  
         // do nothing: no fallback mode
      break;
      case RTE_E_NEVER_RECEIVED:
         // explicit break through to fallback processing: process as Not available
      case RTE_E_UNCONNECTED:
         // explicit break through to fallback processing: process as Not available
      case RTE_E_COM_BUSY:
         pRteInData_Common->VehicleMode = VehicleMode_NotAvailable;
      break;
      case RTE_E_INVALID: 
         // explicit break through to fallback processing: process as Error
      case RTE_E_MAX_AGE_EXCEEDED: 
      default:
         pRteInData_Common->VehicleMode = VehicleMode_Error;
      break;    
   }
}

static void RteDataWrite_Common (VehicleModeDistribution_OutData_T *pRteOutData_Common)
{
   Std_ReturnType retValue = RTE_E_INVALID;
   
   retValue = Rte_Write_SwcActivation_Accessory_Accessory(pRteOutData_Common->SwcActivation_Accessory);
   retValue |= Rte_Write_SwcActivation_EngineRun_EngineRun(pRteOutData_Common->SwcActivation_EngineRun);
   retValue |= Rte_Write_SwcActivation_IgnitionOn_IgnitionOn(pRteOutData_Common->SwcActivation_IgnitionOn);
   retValue |= Rte_Write_SwcActivation_LIN_SwcActivation_LIN(pRteOutData_Common->SwcActivation_LIN);   
   retValue |= Rte_Write_SwcActivation_Living_Living(pRteOutData_Common->SwcActivation_Living);
   retValue |= Rte_Write_SwcActivation_Parked_Parked(pRteOutData_Common->SwcActivation_Parked);
   retValue |= Rte_Write_SwcActivation_Security_SwcActivation_Security(pRteOutData_Common->SwcActivation_Security);
   retValue |= Rte_Write_VehicleModeInternal_VehicleMode(pRteOutData_Common->VehicleModeInternal.Current);

   if (retValue != RTE_E_OK)
   {
      // DET_report!!!
   }
   else
   {
      // RTE_E_OK : do nothing
   }
}

/*!
*************************************************************************************
* \description
* This function implements the logic for the SWCActivation_OutputProcessing,
* ....
*
* \par in_ptr     - Contains signal data from the RTE.
* \par out_ptr    - Used to send signal data to the RTE.
*
* \return void..
*
* \{
***************************************************************************************
*/
static void SWCActivation_OutputProcessing (const VehicleModeDistribution_InData_T *pRteInData_Common, VehicleModeDistribution_OutData_T *pRteOutData_Common)
{
   VehicleMode_T vehicleMode = VehicleMode_NotAvailable;
   
   // Copy valid VehicleMode value to tmp. variable in order to use in logic
   vehicleMode = pRteOutData_Common->VehicleModeInternal.Current;

   //! ###### SWC activation handling
   // SwcActivation_EngineRun
   
   if (((FSC_Reduced == pRteInData_Common->FscMode)
      || (FSC_Operating == pRteInData_Common->FscMode)
      || (FSC_Protecting == pRteInData_Common->FscMode))
      || (1)) // !!!!!!!!! Pending FSC!!!!!!!!!!!!!!!!!!
   {
      
      if (VehicleMode_Running == vehicleMode)
      {
         if (NonOperational == pRteOutData_Common->SwcActivation_EngineRun)
         {
            pRteOutData_Common->SwcActivation_EngineRun = OperationalEntry;
         }
         else
         {
            pRteOutData_Common->SwcActivation_EngineRun = Operational;
         }
      }
      else
      {
         if (Operational == pRteOutData_Common->SwcActivation_EngineRun)
         {
            pRteOutData_Common->SwcActivation_EngineRun = OperationalExit;
         }
         else
         {
            pRteOutData_Common->SwcActivation_EngineRun = NonOperational;
         }
      }

      // SwcActivation_IgnitionOn
      if ((VehicleMode_PreRunning == vehicleMode)
         || (VehicleMode_Cranking == vehicleMode)
         || (VehicleMode_Running == vehicleMode))
      {
         if (NonOperational == pRteOutData_Common->SwcActivation_IgnitionOn)
         {
            pRteOutData_Common->SwcActivation_IgnitionOn = OperationalEntry;
         }
         else
         {
            pRteOutData_Common->SwcActivation_IgnitionOn = Operational;
         }
      }
      else
      {
         if (Operational == pRteOutData_Common->SwcActivation_IgnitionOn)
         {
            pRteOutData_Common->SwcActivation_IgnitionOn = OperationalExit;
         }
         else
         {
            pRteOutData_Common->SwcActivation_IgnitionOn = NonOperational;
         }
      }

      // SwcActivation_Accessory
      if ((VehicleMode_Accessory == vehicleMode)
         || (VehicleMode_PreRunning == vehicleMode)
         || (VehicleMode_Cranking == vehicleMode)
         || (VehicleMode_Running == vehicleMode))
      {
         if (NonOperational == pRteOutData_Common->SwcActivation_Accessory)
         {
            pRteOutData_Common->SwcActivation_Accessory = OperationalEntry;
         }
         else
         {
            pRteOutData_Common->SwcActivation_Accessory = Operational;
         }
      }
      else
      {
         if (Operational == pRteOutData_Common->SwcActivation_Accessory)
         {
            pRteOutData_Common->SwcActivation_Accessory = OperationalExit;
         }
         else
         {
            pRteOutData_Common->SwcActivation_Accessory = NonOperational;
         }
      }

      // SwcActivation_Living
      if ((VehicleMode_Living == vehicleMode)
         || (VehicleMode_Accessory == vehicleMode)
         || (VehicleMode_PreRunning == vehicleMode)
         || (VehicleMode_Cranking == vehicleMode)
         || (VehicleMode_Running == vehicleMode))
      {
         if (NonOperational == pRteOutData_Common->SwcActivation_Living)
         {
            pRteOutData_Common->SwcActivation_Living = OperationalEntry;
         }
         else
         {
            pRteOutData_Common->SwcActivation_Living = Operational;
         }
      }
      else
      {
         if (Operational == pRteOutData_Common->SwcActivation_Living)
         {
            pRteOutData_Common->SwcActivation_Living = OperationalExit;
         }
         else
         {
            pRteOutData_Common->SwcActivation_Living = NonOperational;
         }
      }

      // SwcActivation_Parked
      if ((VehicleMode_Parked == vehicleMode)
         || (VehicleMode_Living == vehicleMode)
         || (VehicleMode_Accessory == vehicleMode)
         || (VehicleMode_PreRunning == vehicleMode)
         || (VehicleMode_Cranking == vehicleMode)
         || (VehicleMode_Running == vehicleMode))
      {
         if (NonOperational == pRteOutData_Common->SwcActivation_Parked)
         {
            pRteOutData_Common->SwcActivation_Parked = OperationalEntry;
         }
         else
         {
            pRteOutData_Common->SwcActivation_Parked = Operational;
         }
      }
      else
      {
         if (Operational == pRteOutData_Common->SwcActivation_Parked)
         {
            pRteOutData_Common->SwcActivation_Parked = OperationalExit;
         }
         else
         {
            pRteOutData_Common->SwcActivation_Parked = NonOperational;
         }
      }

      // SwcActivation_LIN
      if (((VehicleMode_Parked == vehicleMode)
         || (VehicleMode_Living == vehicleMode)
         || (VehicleMode_Accessory == vehicleMode)
         || (VehicleMode_PreRunning == vehicleMode)
         || (VehicleMode_Cranking == vehicleMode)
         || (VehicleMode_Running == vehicleMode))
         && (LIVING12VPOWERSTABILITY_STABLE == pRteInData_Common->Living12VPowerStabilityVar.Current))
      {
         if (NonOperational == pRteOutData_Common->SwcActivation_LIN)
         {
            pRteOutData_Common->SwcActivation_LIN = OperationalEntry;
         }
         else
         {
            pRteOutData_Common->SwcActivation_LIN = Operational;
         }
      }
      else
      {
         if (Operational == pRteOutData_Common->SwcActivation_LIN)
         {
            pRteOutData_Common->SwcActivation_LIN = OperationalExit;
         }
         else
         {
            pRteOutData_Common->SwcActivation_LIN = NonOperational;
         }
      }

      // SwcActivation_Security
      if ((VehicleMode_Parked == vehicleMode)
         || (VehicleMode_Living == vehicleMode)
         || (VehicleMode_Accessory == vehicleMode)
         || (VehicleMode_PreRunning == vehicleMode)
         || (VehicleMode_Cranking == vehicleMode)
         || (VehicleMode_Running == vehicleMode))
      {
         if (NonOperational == pRteOutData_Common->SwcActivation_Security)
         {
            pRteOutData_Common->SwcActivation_Security = OperationalEntry;
         }
         else
         {
            pRteOutData_Common->SwcActivation_Security = Operational;
         }
      }
      else
      {
         if (Operational == pRteOutData_Common->SwcActivation_Security)
         {
            pRteOutData_Common->SwcActivation_Security = OperationalExit;
         }
         else
         {
            pRteOutData_Common->SwcActivation_Security = NonOperational;
         }
      }
   }
   else
   {
      pRteOutData_Common->SwcActivation_Accessory   = NonOperational;
      pRteOutData_Common->SwcActivation_EngineRun   = NonOperational;
      pRteOutData_Common->SwcActivation_IgnitionOn  = NonOperational;
      pRteOutData_Common->SwcActivation_LIN         = NonOperational;
      pRteOutData_Common->SwcActivation_Living      = NonOperational;
      pRteOutData_Common->SwcActivation_Parked      = NonOperational;
   }
}

//!======================================================================================
//!
//! \brief
//! This function is processing the ANW_CIOMOperStateRedundancy logic
//!
//! \param   *pTimer   To check current timer value
//!
//!======================================================================================
   
static void ANW_CIOMOperStateRedundancy (VehicleModeVar_T *pVehicleModeInternal)
{
   FormalBoolean isActivationTriggerDetected             = NO;
   FormalBoolean isDeactivationConditionsFulfilled       = NO;
   static DeactivationTimer_Type DeactivationTimer       = 0u;
   static AnwSM_States ANW_SM_CIOMOperStateRedundancy    = SM_ANW_Inactive;
   AnwAction_Enum Anw_Action                             = ANW_Action_None;
   
   //! ##### Check for LockCtrl_DashboardLedTimer status
   if (pVehicleModeInternal->Previous != pVehicleModeInternal->Current)
   {
      if (VehicleMode_PreRunning > pVehicleModeInternal->Previous)
      {
         if (VehicleMode_PreRunning <= pVehicleModeInternal->Current)
         {
            isActivationTriggerDetected = YES;
         }
         else
         {
            
         }
      }
      else
      {
         
      }
   }
   else 
   {
      // do nothing: no activation event
   }   
   
   if (VehicleMode_PreRunning > pVehicleModeInternal->Current)
   {
      isDeactivationConditionsFulfilled = YES;
   }
   else
   {
      // do nothing: no deaactivation conditions fulfilled
   }

   //! ##### Process ANW logic : 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(NO,
                               isActivationTriggerDetected,
                               isDeactivationConditionsFulfilled,
                               CONST_ANW_CIOMOperStateRedundancyTimeout,
                               &DeactivationTimer,
                               &ANW_SM_CIOMOperStateRedundancy);
   //! ##### Check for Anw_Action is 'Activate' or 'Deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      Rte_Call_UR_ANW_CIOMOperStateRedundancy_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      Rte_Call_UR_ANW_CIOMOperStateRedundancy_DeactivateIss();
   }
   else
   {
      // Do nothing, keep the current state
   }
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
