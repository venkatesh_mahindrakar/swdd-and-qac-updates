/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  LINMgr.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  LINMgr
 *  Generation Time:  2020-10-07 15:25:41
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <LINMgr>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
//!======================================================================================
//! \file SCIM_LINMgr.c
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Platform_SCIM_LIN
//! @{
//! @addtogroup Application_SCIM_LINMgr
//! @{
//!
//! \brief
//! SCIM_LINMgr SWC.
//! ASIL Level : QM.
//! This module implements the logic for the SCIM_LINMgr runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   Boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * BswM_BswMRteMDG_LIN1Schedule
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * BswM_BswMRteMDG_LIN2Schedule
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * BswM_BswMRteMDG_LIN3Schedule
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * BswM_BswMRteMDG_LIN4Schedule
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * BswM_BswMRteMDG_LIN5Schedule
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * BswM_BswMRteMDG_LIN6Schedule
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * BswM_BswMRteMDG_LIN7Schedule
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * BswM_BswMRteMDG_LIN8Schedule
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * ComM_ModeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_OpStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * IOHWAB_BOOL
 *   boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * IOHWAB_UINT8
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Issm_IssHandleType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_LIN_topology_P1AJR_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_LinInterfaces_X1CX0_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinFaultStatus
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinVoltage_0V2
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_LINMgr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 #include "SCIM_LINMgr.h"
 #include "FuncLibrary_Timer_If.h"
 #include "ComM.h" 
 #include "LinIf.h"
 
 /**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 //! #### Definition of Global MACROS
#define PCODE_X1CX0_PcbConfig_LinInterfaces (Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v())
#define PCODE_P1AJR_LIN_topology            (Rte_Prm_P1AJR_LIN_topology_v())
#define PCODE_P1WPP_IsSecurityLinActive     (Rte_Prm_P1WPP_isSecurityLinActive_v())
//! #### Definition of Global Variables 
static Scim_LinMgr_BusStat_T IrvLinBusStatus[CONST_NoOfLIN_Networks];
static uint8                 SCIM_LINmgrPresentLINSchTable[CONST_NoOfLIN_Networks];
static uint8                 SCIM_LINmgrFlag[CONST_NoOfLIN_Networks]                        = {CONST_LINSchTableChangeNoRequest,
                                                                                       CONST_LINSchTableChangeNoRequest,
                                                                                       CONST_LINSchTableChangeNoRequest,
                                                                                       CONST_LINSchTableChangeNoRequest,
                                                                                       CONST_LINSchTableChangeNoRequest,
                                                                                       CONST_LINSchTableChangeNoRequest,
                                                                                       CONST_LINSchTableChangeNoRequest,
                                                                                       CONST_LINSchTableChangeNoRequest};
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * DiagInfo_T: Integer in interval [0...127]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * IOHWAB_BOOL: Boolean
 * IOHWAB_UINT8: Integer in interval [0...255]
 * Issm_IssHandleType: Integer in interval [0...255]
 * SEWS_LIN_topology_P1AJR_T: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BswM_BswMRteMDG_LIN1Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN1_Table1 (1U)
 *   LIN1_Table2 (2U)
 *   LIN1_Table_E (3U)
 *   LIN1_MasterReq_SlaveResp_Table1 (4U)
 *   LIN1_MasterReq_SlaveResp_Table2 (5U)
 *   LIN1_NULL (0U)
 *   LIN1_MasterReq_SlaveResp (6U)
 * BswM_BswMRteMDG_LIN2Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN2_NULL (0U)
 *   LIN2_TABLE0 (1U)
 *   LIN2_TABLE_E (2U)
 *   LIN2_MasterReq_SlaveResp_TABLE0 (3U)
 *   LIN2_MasterReq_SlaveResp (4U)
 * BswM_BswMRteMDG_LIN3Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN3_NULL (0U)
 *   LIN3_TABLE1 (1U)
 *   LIN3_TABLE2 (2U)
 *   LIN3_TABLE_E (3U)
 *   LIN3_MasterReq_SlaveResp_Table1 (4U)
 *   LIN3_MasterReq_SlaveResp_Table2 (5U)
 *   LIN3_MasterReq_SlaveResp (6U)
 * BswM_BswMRteMDG_LIN4Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN4_NULL (0U)
 *   LIN4_MasterReq_SlaveResp_Table1 (4U)
 *   LIN4_TABLE1 (1U)
 *   LIN4_TABLE2 (2U)
 *   LIN4_TABLE_E (3U)
 *   LIN4_MasterReq_SlaveResp_Table2 (5U)
 *   LIN4_MasterReq_SlaveResp (6U)
 * BswM_BswMRteMDG_LIN5Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN5_NULL (0U)
 *   LIN5_TABLE1 (1U)
 *   LIN5_MasterReq_SlaveResp_Table1 (4U)
 *   LIN5_MasterReq_SlaveResp_Table2 (5U)
 *   LIN5_MasterReq_SlaveResp (6U)
 *   LIN5_TABLE2 (2U)
 *   LIN5_TABLE_E (3U)
 * BswM_BswMRteMDG_LIN6Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN6_NULL (0U)
 *   LIN6_TABLE0 (1U)
 *   LIN6_MasterReq_SlaveResp_Table0 (3U)
 *   LIN6_MasterReq_SlaveResp (2U)
 * BswM_BswMRteMDG_LIN7Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN7_NULL (0U)
 *   LIN7_TABLE0 (1U)
 *   LIN7_MasterReq_SlaveResp_Table0 (3U)
 *   LIN7_MasterReq_SlaveResp (2U)
 * BswM_BswMRteMDG_LIN8Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN8_NULL (0U)
 *   LIN8_TABLE0 (1U)
 *   LIN8_MasterReq_SlaveResp_Table0 (3U)
 *   LIN8_MasterReq_SlaveResp (2U)
 * ComM_ModeType: Enumeration of integer in interval [0...3] with enumerators
 *   COMM_NO_COMMUNICATION (0U)
 *   COMM_SILENT_COMMUNICATION (1U)
 *   COMM_FULL_COMMUNICATION (2U)
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * SEWS_PcbConfig_LinInterfaces_X1CX0_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T_NotPopulated (0U)
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated (1U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 * SEWS_PcbConfig_LinInterfaces_X1CX0_a_T: Array with 7 element(s) of type SEWS_PcbConfig_LinInterfaces_X1CX0_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T *Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(void)
 *     Returnvalue: SEWS_PcbConfig_LinInterfaces_X1CX0_T* is of type SEWS_PcbConfig_LinInterfaces_X1CX0_a_T
 *   SEWS_LIN_topology_P1AJR_T Rte_Prm_P1AJR_LIN_topology_v(void)
 *   boolean Rte_Prm_P1WPP_isSecurityLinActive_v(void)
 *
 *********************************************************************************************************************/


#define LINMgr_START_SEC_CODE
#include "LINMgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1CXF_Data_P1CXF_FCI_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1CXF_Data_P1CXF_FCI>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_DataServices_P1CXF_Data_P1CXF_FCI_ReadData_Irv_DID_FCI(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1CXF_Data_P1CXF_FCI_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1CXF_Data_P1CXF_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1CXF_Data_P1CXF_FCI_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the logic for reading P1CXF FCI data in the DataServices_P1CXF_Data_P1CXF_FCI_ReadData
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, LINMgr_CODE) DataServices_P1CXF_Data_P1CXF_FCI_ReadData(P2VAR(uint8, AUTOMATIC, RTE_LINMGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1CXF_Data_P1CXF_FCI_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Issm_IssStateChange_Issm_IssActivation
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Issm_IssActivation> of PortPrototype <Issm_IssStateChange>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void Issm_IssStateChange_Issm_IssActivation(Issm_IssHandleType issID)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Issm_IssStateChange_Issm_IssActivation_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the logic for the Issm_IssStateChange_Issm_IssActivation
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) Issm_IssStateChange_Issm_IssActivation(Issm_IssHandleType issID) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Issm_IssStateChange_Issm_IssActivation
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Issm_IssStateChange_Issm_IssDeactivation
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Issm_IssDeactivation> of PortPrototype <Issm_IssStateChange>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void Issm_IssStateChange_Issm_IssDeactivation(Issm_IssHandleType issID)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Issm_IssStateChange_Issm_IssDeactivation_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the logic for the Issm_IssStateChange_Issm_IssDeactivation
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) Issm_IssStateChange_Issm_IssDeactivation(Issm_IssHandleType issID) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Issm_IssStateChange_Issm_IssDeactivation
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_R1AAJ_FlexibleSwitchDetection>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAJ_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAJ_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, LINMgr_CODE) RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_LINMGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_LINMGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAJ_FlexibleSwitchDetection_Start
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_R1AAJ_FlexibleSwitchDetection>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAJ_FlexibleSwitchDetection_Start(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAJ_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAJ_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAJ_FlexibleSwitchDetection_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, LINMgr_CODE) RoutineServices_R1AAJ_FlexibleSwitchDetection_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_LINMGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_LINMGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAJ_FlexibleSwitchDetection_Start (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAJ_FlexibleSwitchDetection_Stop
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_R1AAJ_FlexibleSwitchDetection>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAJ_FlexibleSwitchDetection_Stop(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAJ_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAJ_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAJ_FlexibleSwitchDetection_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, LINMgr_CODE) RoutineServices_R1AAJ_FlexibleSwitchDetection_Stop(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_LINMGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAJ_FlexibleSwitchDetection_Stop (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_LinDiagRequestFlag_CCNADRequest(uint8 *data)
 *   Std_ReturnType Rte_Read_LinDiagRequestFlag_PNSNRequest(uint8 *data)
 *   Std_ReturnType Rte_Read_SwcActivation_LIN_SwcActivation_LIN(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *   Std_ReturnType Rte_Read_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted(Boolean *data)
 *   Std_ReturnType Rte_Read_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type(Boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type data)
 *   Std_ReturnType Rte_Write_ComMode_LIN2_ComMode_LIN(ComMode_LIN_Type data)
 *   Std_ReturnType Rte_Write_ComMode_LIN3_ComMode_LIN(ComMode_LIN_Type data)
 *   Std_ReturnType Rte_Write_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type data)
 *   Std_ReturnType Rte_Write_ComMode_LIN5_ComMode_LIN(ComMode_LIN_Type data)
 *   Std_ReturnType Rte_Write_ComMode_LIN6_ComMode_LIN(ComMode_LIN_Type data)
 *   Std_ReturnType Rte_Write_ComMode_LIN7_ComMode_LIN(ComMode_LIN_Type data)
 *   Std_ReturnType Rte_Write_ComMode_LIN8_ComMode_LIN(ComMode_LIN_Type data)
 *
 * Mode Interfaces:
 * ================
 *   uint8 Rte_Mode_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN1Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_NULL
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table1
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table2
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN1Schedule
 *   uint8 Rte_Mode_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN2Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_NULL
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE0
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN2Schedule
 *   uint8 Rte_Mode_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN3Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_NULL
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE1
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE2
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN3Schedule
 *   uint8 Rte_Mode_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN4Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_NULL
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE1
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE2
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN4Schedule
 *   uint8 Rte_Mode_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN5Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_NULL
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE1
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE2
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN5Schedule
 *   uint8 Rte_Mode_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN6Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp_Table0
 *   - RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_NULL
 *   - RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_TABLE0
 *   - RTE_TRANSITION_BswMRteMDG_LIN6Schedule
 *   uint8 Rte_Mode_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN7Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp_Table0
 *   - RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_NULL
 *   - RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_TABLE0
 *   - RTE_TRANSITION_BswMRteMDG_LIN7Schedule
 *   uint8 Rte_Mode_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN8Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp_Table0
 *   - RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_NULL
 *   - RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_TABLE0
 *   - RTE_TRANSITION_BswMRteMDG_LIN8Schedule
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_SCIM_LINMgr_20ms_runnable_Irv_DID_FCI(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_SCIM_LINMgr_20ms_runnable_Irv_DID_FCI(uint8 data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BJO_88_LIN1busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BJP_88_LIN2busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BJQ_88_LIN3busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BJR_88_LIN4busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BJS_88_LIN5busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_CN_LIN00_ace1a6ba_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_CN_LIN01_4323cd84_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_CN_LIN02_a8147687_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_CN_LIN03_47d61db9_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_CN_LIN04_a50a06c0_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_CN_LIN05_4ac86dfe_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_CN_LIN06_a1ffd6fd_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_CN_LIN07_4e3dbdc3_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_20ms_runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution runnable logic for the 'SCIM_LINMgr_20ms_runnable'
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC Input data structures
   // Define input RteInData Common
   static       SCIMLINMgr_InData_T  RteInData_Common;
   static const LINScheduleModes_T   *LINSchedule                                     = NULL_PTR;      
   static       ComMode_LIN_Type     RteOutData_LINComMode[CONST_NoOfLIN_Networks]    = {Inactive,
                                                                                         Inactive,
                                                                                         Inactive,
                                                                                         Inactive,
                                                                                         Inactive,
                                                                                         Inactive,
                                                                                         Inactive,
                                                                                         Inactive};   
   static       uint8                LinBusIndex = 0u;
   static       VarLINMgrState_T     SM_VarLINMgrState[CONST_NoOfLIN_Networks]        = {{SM_LINMgr_Init,SM_LINMgr_Init},
                                                                                        {SM_LINMgr_Init,SM_LINMgr_Init},
                                                                                        {SM_LINMgr_Init,SM_LINMgr_Init},
                                                                                        {SM_LINMgr_Init,SM_LINMgr_Init},
                                                                                        {SM_LINMgr_Init,SM_LINMgr_Init},
                                                                                        {SM_LINMgr_Init,SM_LINMgr_Init},
                                                                                        {SM_LINMgr_Init,SM_LINMgr_Init},
                                                                                        {SM_LINMgr_Init,SM_LINMgr_Init}};   
   static const LINScheduleModes_T   LINSchedule_LINTopology1[CONST_NoOfLIN_Networks] = {{LIN1_MasterReq_SlaveResp,LIN1_MasterReq_SlaveResp_Table1,LIN1_Table_E,LIN1_Table1},
                                                                                         {LIN2_MasterReq_SlaveResp,LIN2_MasterReq_SlaveResp_TABLE0,LIN2_TABLE_E,LIN2_TABLE0},
                                                                                         {LIN3_MasterReq_SlaveResp,LIN3_MasterReq_SlaveResp_Table1,LIN3_TABLE_E,LIN3_TABLE1},
                                                                                         {LIN4_MasterReq_SlaveResp,LIN4_MasterReq_SlaveResp_Table1,LIN4_TABLE_E,LIN4_TABLE1},
                                                                                         {LIN5_MasterReq_SlaveResp,LIN5_MasterReq_SlaveResp_Table1,LIN5_TABLE_E,LIN5_TABLE1},
                                                                                         {LIN6_MasterReq_SlaveResp,LIN6_MasterReq_SlaveResp_Table0,LIN6_TABLE0,LIN6_TABLE0},
                                                                                         {LIN7_MasterReq_SlaveResp,LIN7_MasterReq_SlaveResp_Table0,LIN7_TABLE0,LIN7_TABLE0},
                                                                                         {LIN8_MasterReq_SlaveResp,LIN8_MasterReq_SlaveResp_Table0,LIN8_TABLE0,LIN8_TABLE0}};   
   static const LINScheduleModes_T   LINSchedule_LINTopology2[CONST_NoOfLIN_Networks] = {{LIN1_MasterReq_SlaveResp,LIN1_MasterReq_SlaveResp_Table2,LIN1_Table2,LIN1_Table2},
                                                                                         {LIN2_MasterReq_SlaveResp,LIN2_MasterReq_SlaveResp_TABLE0,LIN2_TABLE_E,LIN2_TABLE0},
                                                                                         {LIN3_MasterReq_SlaveResp,LIN3_MasterReq_SlaveResp_Table2,LIN3_TABLE_E,LIN3_TABLE2},
                                                                                         {LIN4_MasterReq_SlaveResp,LIN4_MasterReq_SlaveResp_Table2,LIN4_TABLE2,LIN4_TABLE2},
                                                                                         {LIN5_MasterReq_SlaveResp,LIN5_MasterReq_SlaveResp_Table2,LIN5_TABLE2,LIN5_TABLE2},
                                                                                         {LIN6_MasterReq_SlaveResp,LIN6_MasterReq_SlaveResp_Table0,LIN6_TABLE0,LIN6_TABLE0},
                                                                                         {LIN7_MasterReq_SlaveResp,LIN7_MasterReq_SlaveResp_Table0,LIN7_TABLE0,LIN7_TABLE0},
                                                                                         {LIN8_MasterReq_SlaveResp,LIN8_MasterReq_SlaveResp_Table0,LIN8_TABLE0,LIN8_TABLE0}};
   SEWS_PcbConfig_LinInterfaces_X1CX0_T *PcbConfig_LinInterfaces = {NULL_PTR,};
   Boolean                              isSecurityLinActive;

   //! ##### Read the logic common RTE ports and process fallback modes for RTE events: 'Get_RteDataRead_Common()'
   Get_RteDataRead_Common(&RteInData_Common);     
   PcbConfig_LinInterfaces = (SEWS_PcbConfig_LinInterfaces_X1CX0_T*) PCODE_X1CX0_PcbConfig_LinInterfaces;
   isSecurityLinActive     = PCODE_P1WPP_IsSecurityLinActive;

   //! ###### Check the conditions to set the LIN Schedule table 
   if ((PCODE_P1AJR_LIN_topology == 1U)
      || (PCODE_P1AJR_LIN_topology == 2U))
   {
      //! ##### Check the 'LIN_Topology' is equal to value 1
      if (PCODE_P1AJR_LIN_topology == 1U)
      { 
         LINSchedule = &LINSchedule_LINTopology1[0]; 
      }
      else 
      {
       //! ##### Check the 'LIN_Topology' is equal to value 2 
         LINSchedule = &LINSchedule_LINTopology2[0]; 
      }
      //! ###### Processing the SCIM LINMgr StateMachine Transition logic : 'SCIM_LINMgr_StateMachine()'
      for(LinBusIndex =0U; LinBusIndex < CONST_NoOfLIN_Networks ;LinBusIndex++)
      {
        if (((LinBusIndex < (CONST_NoOfLIN_Networks-1))
           &&(TRUE == PcbConfig_LinInterfaces[LinBusIndex]))
           ||((LinBusIndex == (CONST_NoOfLIN_Networks-1))
           &&(TRUE == isSecurityLinActive)))
        {
          SCIM_LINMgr_StateMachine(&RteInData_Common,
                                   LinBusIndex,
                                   &LINSchedule[LinBusIndex],
                                   &SM_VarLINMgrState[LinBusIndex],                            
                                   &RteOutData_LINComMode[LinBusIndex],
                                   &SCIM_LINmgrPresentLINSchTable[LinBusIndex],
                                   &SCIM_LINmgrFlag[LinBusIndex]);
        }
        else
        {
            // Set to NULL Schedule Table
        }
      }             
      //! ##### Write the logic common RTE ports and process fallback modes for RTE events: 'Get_RteDataWrite_Common()'       
      Get_RteDataWrite_Common(&RteOutData_LINComMode[0]);
   }
   else
   {
      //? what is default Topology, If Parameter of LIN Topology Wrong.
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_LIN1SchEndNotif
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN1SchTableState>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Request_LIN1_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN1Schedule data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN1SchEndNotif_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the LIN1 End of schedule table Processing logic for the 'SCIM_LINMgr_LIN1SchEndNotif'
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN1SchEndNotif(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN1SchEndNotif
 *********************************************************************************************************************/
   //! ###### Processing the condition for Completion of 'LIN1_ScheduleTable' 
   //! ##### If the Flag is updated with CONST_LINSchTableChangeRequested 
   if (SCIM_LINmgrFlag[0] == CONST_LINSchTableChangeRequested)
   {  
      //! ####Then request the Lin Schedule Table
      SCIM_LINmgrFlag[0] = CONST_LINSchTableChangeNoRequest;
      (void)Rte_Write_Request_LIN1_ScheduleTableRequestMode_requestedMode(SCIM_LINmgrPresentLINSchTable[0]);
   }
   else
   {
      // do nothing:keep previous value
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_LIN2SchEndNotif
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN2SchTableState>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Request_LIN2_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN2Schedule data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN2SchEndNotif_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the LIN2 End of schedule table Processing logic for the 'SCIM_LINMgr_LIN2SchEndNotif'
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN2SchEndNotif(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN2SchEndNotif
 *********************************************************************************************************************/
   //! ###### Processing the condition for Completion of 'LIN2_ScheduleTable' 
   //! ##### If the Flag is updated with CONST_LINSchTableChangeRequested 
   if (SCIM_LINmgrFlag[1] == CONST_LINSchTableChangeRequested)
   {  
      //! ####Then request the Lin Schedule Table
      SCIM_LINmgrFlag[1] = CONST_LINSchTableChangeNoRequest;
      (void)Rte_Write_Request_LIN2_ScheduleTableRequestMode_requestedMode(SCIM_LINmgrPresentLINSchTable[1]);
   }
   else
   { 
      // do nothing:keep previous value
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_LIN3SchEndNotif
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN3SchTableState>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Request_LIN3_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN3Schedule data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN3SchEndNotif_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the LIN3 End of schedule table Processing logic for the 'SCIM_LINMgr_LIN3SchEndNotif'
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN3SchEndNotif(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN3SchEndNotif
 *********************************************************************************************************************/
   //! ###### Processing the condition for Completion of 'LIN3_ScheduleTable' 
   //! ##### If the Flag is updated with CONST_LINSchTableChangeRequested 
   if (SCIM_LINmgrFlag[2] == CONST_LINSchTableChangeRequested)
   { 
      //! ####Then request the Lin Schedule Table 
      SCIM_LINmgrFlag[2] = CONST_LINSchTableChangeNoRequest;
      (void)Rte_Write_Request_LIN3_ScheduleTableRequestMode_requestedMode(SCIM_LINmgrPresentLINSchTable[2]);
   }
   else
   {
      // do nothing:keep previous value
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_LIN4SchEndNotif
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN4SchTableState>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Request_LIN4_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN4Schedule data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN4SchEndNotif_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the LIN4 End of schedule table Processing logic for the 'SCIM_LINMgr_LIN4SchEndNotif'
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN4SchEndNotif(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN4SchEndNotif
 *********************************************************************************************************************/
   //! ###### Processing the condition for Completion of 'LIN4_ScheduleTable' 
   //! ##### If the Flag is updated with CONST_LINSchTableChangeRequested  
   if (SCIM_LINmgrFlag[3] == CONST_LINSchTableChangeRequested)
   {  
      //! ####Then request the Lin Schedule Table
      SCIM_LINmgrFlag[3] = CONST_LINSchTableChangeNoRequest;
      (void)Rte_Write_Request_LIN4_ScheduleTableRequestMode_requestedMode(SCIM_LINmgrPresentLINSchTable[3]);
   }
   else
   {
      // do nothing:keep previous value   
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_LIN5SchEndNotif
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN5SchTableState>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Request_LIN5_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN5Schedule data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN5SchEndNotif_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the LIN5 End of schedule table Processing logic for the 'SCIM_LINMgr_LIN5SchEndNotif'
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN5SchEndNotif(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN5SchEndNotif
 *********************************************************************************************************************/
   //! ###### Processing the condition for Completion of 'LIN5_ScheduleTable' 
   //! ##### If the Flag is updated with CONST_LINSchTableChangeRequested
   if (SCIM_LINmgrFlag[4] == CONST_LINSchTableChangeRequested)
   {  
      //! ####Then request the Lin Schedule Table
      SCIM_LINmgrFlag[4] = CONST_LINSchTableChangeNoRequest;
      (void)Rte_Write_Request_LIN5_ScheduleTableRequestMode_requestedMode(SCIM_LINmgrPresentLINSchTable[4]);
   }
   else
   {
      // do nothing:keep previous value
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_LIN6SchEndNotif
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN6SchTableState>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Request_LIN6_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN6Schedule data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN6SchEndNotif_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the LIN6 End of schedule table Processing logic for the 'SCIM_LINMgr_LIN6SchEndNotif'
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN6SchEndNotif(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN6SchEndNotif
 *********************************************************************************************************************/
   //! ###### Processing the condition for Completion of 'LIN6_ScheduleTable' 
   //! ##### If the Flag is updated with CONST_LINSchTableChangeRequested
   if (SCIM_LINmgrFlag[5] == CONST_LINSchTableChangeRequested)
   {  
      //! ####Then request the Lin Schedule Table
      SCIM_LINmgrFlag[5] = CONST_LINSchTableChangeNoRequest;
      (void)Rte_Write_Request_LIN6_ScheduleTableRequestMode_requestedMode(SCIM_LINmgrPresentLINSchTable[5]);
   }
   else
   {
      // do nothing:keep previous value
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_LIN7SchEndNotif
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN7SchTableState>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Request_LIN7_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN7Schedule data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN7SchEndNotif_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the LIN7 End of schedule table Processing logic for the 'SCIM_LINMgr_LIN7SchEndNotif'
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN7SchEndNotif(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN7SchEndNotif
 *********************************************************************************************************************/
   //! ###### Processing the condition for Completion of 'LIN7_ScheduleTable' 
   //! ##### If the Flag is updated with CONST_LINSchTableChangeRequested 
   if (SCIM_LINmgrFlag[6] == CONST_LINSchTableChangeRequested)
   {  
      //! ####Then request the Lin Schedule Table
      SCIM_LINmgrFlag[6] = CONST_LINSchTableChangeNoRequest;
      (void)Rte_Write_Request_LIN7_ScheduleTableRequestMode_requestedMode(SCIM_LINmgrPresentLINSchTable[6]);
   }
   else
   {
      // do nothing:keep previous value
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_LIN8SchEndNotif
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN8SchTableState>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Request_LIN8_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN8Schedule data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN8SchEndNotif_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the LIN8 End of schedule table Processing logic for the 'SCIM_LINMgr_LIN8SchEndNotif'
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN8SchEndNotif(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN8SchEndNotif
 *********************************************************************************************************************/
   //! ###### Processing the condition for Completion of 'LIN8_ScheduleTable' 
   //! ##### If the Flag is updated with CONST_LINSchTableChangeRequested
   if (SCIM_LINmgrFlag[7] == CONST_LINSchTableChangeRequested)
   {  
      //! ####Then request the Lin Schedule Table
      SCIM_LINmgrFlag[7] = CONST_LINSchTableChangeNoRequest;
      (void)Rte_Write_Request_LIN8_ScheduleTableRequestMode_requestedMode(SCIM_LINmgrPresentLINSchTable[7]);
   }
   else
   {
      // do nothing:keep previous value
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Get_RteDataRead_Common'
//!       
//! \param  *pRteInData_Common  RTE Read data for data structure to fill with data 
//!   
//!======================================================================================
static void Get_RteDataRead_Common(SCIMLINMgr_InData_T *pRteInData_Common)
{
   //! ###### Process the RTE Indata Read Common logic  
   Std_ReturnType retValue = RTE_E_INVALID;
   
   //! ##### Read LIN_SwcActivation port Interface
   retValue = Rte_Read_SwcActivation_LIN_SwcActivation_LIN(&pRteInData_Common->LIN_SwcActivation);
   if (RTE_E_OK != retValue)
   {
     pRteInData_Common->LIN_SwcActivation = NonOperational;
   }
   else
   {
      // RTE_E_OK : do nothing
   }
   //! ##### Read VehicleMode port Interface
   pRteInData_Common->VarVehicleMode.Previous = pRteInData_Common->VarVehicleMode.Current;
   retValue = Rte_Read_VehicleModeInternal_VehicleMode(&pRteInData_Common->VarVehicleMode.Current);
   if (RTE_E_OK != retValue)
   {
     pRteInData_Common->VarVehicleMode.Current = VehicleMode_Error;
   }
   else
   {
      // RTE_E_OK : do nothing
   }  
   //! ##### Read IsFlexSwDetCmplted port Interface   
   pRteInData_Common->VarIsFlexSwDetCmplted.Previous = pRteInData_Common->VarIsFlexSwDetCmplted.Current;
   retValue = Rte_Read_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type(&pRteInData_Common->VarIsFlexSwDetCmplted.Current);
   if (RTE_E_OK != retValue)
   {
     pRteInData_Common->VarIsFlexSwDetCmplted.Current = FALSE;
   }
   else
   {
      // RTE_E_OK : do nothing
   } 
   //! ##### Read CCNADRequest port Interface
   retValue = Rte_Read_LinDiagRequestFlag_CCNADRequest(&pRteInData_Common->CCNADRequest);
   if (RTE_E_OK != retValue)
   {
      pRteInData_Common->CCNADRequest = 0U;
   }
   else
   {
      // RTE_E_OK : do nothing
   }
   //! ##### Read PNSNRequest port Interface
   retValue = Rte_Read_LinDiagRequestFlag_PNSNRequest(&pRteInData_Common->PNSNRequest);
   if (RTE_E_OK != retValue)
   {
      pRteInData_Common->PNSNRequest = 0U;
   }
   else
   {
      // RTE_E_OK : do nothing
   }
   pRteInData_Common->CurScheTable[0].Previous = pRteInData_Common->CurScheTable[0].Current;
   pRteInData_Common->CurScheTable[0].Current  = Get_RteReadLIN1CurSchTable();    

   pRteInData_Common->CurScheTable[1].Previous = pRteInData_Common->CurScheTable[1].Current;
   pRteInData_Common->CurScheTable[1].Current  = Get_RteReadLIN2CurSchTable(); 

   pRteInData_Common->CurScheTable[2].Previous = pRteInData_Common->CurScheTable[2].Current;
   pRteInData_Common->CurScheTable[2].Current  = Get_RteReadLIN3CurSchTable(); 

   pRteInData_Common->CurScheTable[3].Previous = pRteInData_Common->CurScheTable[3].Current;
   pRteInData_Common->CurScheTable[3].Current  = Get_RteReadLIN4CurSchTable(); 
   
   pRteInData_Common->CurScheTable[4].Previous = pRteInData_Common->CurScheTable[4].Current;
   pRteInData_Common->CurScheTable[4].Current  = Get_RteReadLIN5CurSchTable(); 

   pRteInData_Common->CurScheTable[5].Previous = pRteInData_Common->CurScheTable[5].Current;
   pRteInData_Common->CurScheTable[5].Current  = Get_RteReadLIN6CurSchTable(); 

   pRteInData_Common->CurScheTable[6].Previous = pRteInData_Common->CurScheTable[6].Current;
   pRteInData_Common->CurScheTable[6].Current  = Get_RteReadLIN7CurSchTable(); 
   
   pRteInData_Common->CurScheTable[7].Previous = pRteInData_Common->CurScheTable[7].Current;
   pRteInData_Common->CurScheTable[7].Current  = Get_RteReadLIN8CurSchTable(); 
}  
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Get_RteReadLIN1CurSchTable'
//!
//! \return   ReturnMode    returns uint8 type 'ReturnMode' value
//!       
//!======================================================================================
static uint8 Get_RteReadLIN1CurSchTable(void)
{
   uint8 LocalMode;
   uint8 ReturnMode;
   //! ###### Processing the current Schedule table for LIN1
   LocalMode = Rte_Mode_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule();
   //! ##### Read the mode requested and accordingly update 
   switch(LocalMode)
   {
      case RTE_MODE_LINMgr_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp:
         ReturnMode = LIN1_MasterReq_SlaveResp;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1:
         ReturnMode = LIN1_MasterReq_SlaveResp_Table1;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2:
         ReturnMode = LIN1_MasterReq_SlaveResp_Table2;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN1Schedule_LIN1_NULL:
         ReturnMode = LIN1_NULL;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN1Schedule_LIN1_Table1:
         ReturnMode = LIN1_Table1;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN1Schedule_LIN1_Table2:
         ReturnMode = LIN1_Table2;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN1Schedule_LIN1_Table_E:
         ReturnMode = LIN1_Table_E;
      break;
      default: //RTE_TRANSITION_SCIM_LINMgr_BswMRteMDG_LIN1Schedule:
         ReturnMode = LIN_SCHETABLE_MODESWITCH_TRANSISTION;
      break;
   }
   return ReturnMode;  
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Get_RteReadLIN2CurSchTable'
//!
//! \return   ReturnMode    returns uint8 type 'ReturnMode' value
//!       
//!======================================================================================
static uint8 Get_RteReadLIN2CurSchTable(void)
{
   uint8 LocalMode;
   uint8 ReturnMode;
   //! ###### Processing the current Schedule table for LIN2 
   LocalMode = Rte_Mode_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule();
   //! ##### Read the mode requested and accordingly update 
   switch(LocalMode)
   {
      case RTE_MODE_LINMgr_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp:
         ReturnMode = LIN2_MasterReq_SlaveResp;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0:
         ReturnMode = LIN2_MasterReq_SlaveResp_TABLE0;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN2Schedule_LIN2_NULL:
         ReturnMode = LIN2_NULL;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN2Schedule_LIN2_TABLE0:
         ReturnMode = LIN2_TABLE0;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E:
         ReturnMode = LIN2_TABLE_E;
      break;      
      default: //RTE_TRANSITION_SCIM_LINMgr_BswMRteMDG_LIN2Schedule:
         ReturnMode = LIN_SCHETABLE_MODESWITCH_TRANSISTION;
      break;
   }
   return ReturnMode; 
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Get_RteReadLIN3CurSchTable'
//!
//! \return   ReturnMode    returns uint8 type 'ReturnMode' value
//!       
//!======================================================================================
static uint8 Get_RteReadLIN3CurSchTable(void)
{
   uint8 LocalMode;
   uint8 ReturnMode;
   //! ###### Processing the current Schedule table for LIN3 
   LocalMode = Rte_Mode_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule();
   //! ##### Read the mode requested and accordingly update 
   switch(LocalMode)
   {
      case RTE_MODE_LINMgr_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp:
         ReturnMode = LIN3_MasterReq_SlaveResp;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1:
         ReturnMode = LIN3_MasterReq_SlaveResp_Table1;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2:
         ReturnMode = LIN3_MasterReq_SlaveResp_Table2;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN3Schedule_LIN3_NULL:
         ReturnMode = LIN3_NULL;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN3Schedule_LIN3_TABLE1:
         ReturnMode = LIN3_TABLE1;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN3Schedule_LIN3_TABLE2:
         ReturnMode = LIN3_TABLE2;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E:
         ReturnMode = LIN3_TABLE_E;
      break;      
      default: //RTE_TRANSITION_SCIM_LINMgr_BswMRteMDG_LIN3Schedule:
         ReturnMode = LIN_SCHETABLE_MODESWITCH_TRANSISTION;
      break;
   }
   return ReturnMode;  
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Get_RteReadLIN4CurSchTable'
//!
//! \return   ReturnMode    returns uint8 type 'ReturnMode' value
//!       
//!======================================================================================
static uint8 Get_RteReadLIN4CurSchTable(void)
{
   uint8 LocalMode;
   uint8 ReturnMode;
   //! ###### Processing the current Schedule table for LIN4
   LocalMode = Rte_Mode_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule();
   //! ##### Read the mode requested and accordingly update 
   switch(LocalMode)
   {
      case RTE_MODE_LINMgr_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp:
         ReturnMode = LIN4_MasterReq_SlaveResp;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1:
         ReturnMode = LIN4_MasterReq_SlaveResp_Table1;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2:
         ReturnMode = LIN4_MasterReq_SlaveResp_Table2;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN4Schedule_LIN4_NULL:
         ReturnMode = LIN4_NULL;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN4Schedule_LIN4_TABLE1:
         ReturnMode = LIN4_TABLE1;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN4Schedule_LIN4_TABLE2:
         ReturnMode = LIN4_TABLE2;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E:
         ReturnMode = LIN4_TABLE_E;
      break;      
      default: //RTE_TRANSITION_SCIM_LINMgr_BswMRteMDG_LIN4Schedule:
         ReturnMode = LIN_SCHETABLE_MODESWITCH_TRANSISTION;
      break;
   }
   return ReturnMode;  
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Get_RteReadLIN5CurSchTable'
//!
//! \return   ReturnMode    returns uint8 type 'ReturnMode' value
//!       
//!======================================================================================
static uint8 Get_RteReadLIN5CurSchTable(void)
{
   uint8 LocalMode;
   uint8 ReturnMode;
   //! ###### Processing the current Schedule table for LIN5 
   LocalMode = Rte_Mode_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule();
   //! ##### Read the mode requested and accordingly update 
   switch(LocalMode)
   {
      case RTE_MODE_LINMgr_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp:
         ReturnMode = LIN5_MasterReq_SlaveResp;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1:
         ReturnMode = LIN5_MasterReq_SlaveResp_Table1;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2:
         ReturnMode = LIN5_MasterReq_SlaveResp_Table2;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN5Schedule_LIN5_NULL:
         ReturnMode = LIN5_NULL;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN5Schedule_LIN5_TABLE1:
         ReturnMode = LIN5_TABLE1;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN5Schedule_LIN5_TABLE2:
         ReturnMode = LIN5_TABLE2;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E:
         ReturnMode = LIN5_TABLE_E;
      break;      
      default: //RTE_TRANSITION_SCIM_LINMgr_BswMRteMDG_LIN5Schedule:
         ReturnMode = LIN_SCHETABLE_MODESWITCH_TRANSISTION;
      break;
   }
   return ReturnMode;  
}

//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Get_RteReadLIN6CurSchTable'
//!
//! \return   ReturnMode    returns uint8 type 'ReturnMode' value
//!       
//!======================================================================================
static uint8 Get_RteReadLIN6CurSchTable(void)
{
   uint8 LocalMode;
   uint8 ReturnMode;
   //! ###### Processing the current Schedule table for LIN6
   LocalMode = Rte_Mode_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule();
   //! ##### Read the mode requested and accordingly update 
   switch(LocalMode)
   {
      case RTE_MODE_LINMgr_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp:
         ReturnMode = LIN6_MasterReq_SlaveResp;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp_Table0:
         ReturnMode = LIN6_MasterReq_SlaveResp_Table0;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN6Schedule_LIN6_NULL:
         ReturnMode = LIN6_NULL;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN6Schedule_LIN6_TABLE0:
         ReturnMode = LIN6_TABLE0;
      break;      
      default: //RTE_TRANSITION_SCIM_LINMgr_BswMRteMDG_LIN6Schedule:
         ReturnMode = LIN_SCHETABLE_MODESWITCH_TRANSISTION;
      break;
   }
   return ReturnMode;  
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Get_RteReadLIN7CurSchTable'
//!
//! \return   ReturnMode    returns uint8 type 'ReturnMode' value
//!       
//!======================================================================================
static uint8 Get_RteReadLIN7CurSchTable(void)
{
   uint8 LocalMode;
   uint8 ReturnMode;
   //! ###### Processing the current Schedule table for LIN7 
   LocalMode = Rte_Mode_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule();
   //! ##### Read the mode requested and accordingly update 
   switch(LocalMode)
   {
      case RTE_MODE_LINMgr_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp:
         ReturnMode = LIN7_MasterReq_SlaveResp;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp_Table0:
         ReturnMode = LIN7_MasterReq_SlaveResp_Table0;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN7Schedule_LIN7_NULL:
         ReturnMode = LIN7_NULL;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN7Schedule_LIN7_TABLE0:
         ReturnMode = LIN7_TABLE0;
      break;   
      default: //RTE_TRANSITION_SCIM_LINMgr_BswMRteMDG_LIN7Schedule
         ReturnMode = LIN_SCHETABLE_MODESWITCH_TRANSISTION;
      break;
   }
   return ReturnMode;  
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Get_RteReadLIN8CurSchTable'
//!
//! \return   ReturnMode    returns uint8 type 'ReturnMode' value
//!       
//!======================================================================================
static uint8 Get_RteReadLIN8CurSchTable(void)
{
   uint8 LocalMode;
   uint8 ReturnMode;
   //! ###### Processing the current Schedule table for LIN8  
   LocalMode = Rte_Mode_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule();
   //! ##### Read the mode requested and accordingly update 
   switch(LocalMode)
   {
      case RTE_MODE_LINMgr_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp:
         ReturnMode = LIN8_MasterReq_SlaveResp;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp_Table0:
         ReturnMode = LIN8_MasterReq_SlaveResp_Table0;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN8Schedule_LIN8_NULL:
         ReturnMode = LIN8_NULL;
      break;
      case RTE_MODE_LINMgr_BswMRteMDG_LIN8Schedule_LIN8_TABLE0:
         ReturnMode = LIN8_TABLE0;
      break;      
      default: //RTE_TRANSITION_SCIM_LINMgr_BswMRteMDG_LIN8Schedule
         ReturnMode = LIN_SCHETABLE_MODESWITCH_TRANSISTION;
      break;
   }
   return ReturnMode;  
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Get_RteDataWrite_Common'
//!       
//!======================================================================================
static void Get_RteDataWrite_Common(ComMode_LIN_Type RteOutData[8])
{
   //! ###### Process RTE data Write Common logic
   Rte_Write_ComMode_LIN1_ComMode_LIN(RteOutData[0]);
   Rte_Write_ComMode_LIN2_ComMode_LIN(RteOutData[1]);
   Rte_Write_ComMode_LIN3_ComMode_LIN(RteOutData[2]);
   Rte_Write_ComMode_LIN4_ComMode_LIN(RteOutData[3]);
   Rte_Write_ComMode_LIN5_ComMode_LIN(RteOutData[4]);
   Rte_Write_ComMode_LIN6_ComMode_LIN(RteOutData[5]);
   Rte_Write_ComMode_LIN7_ComMode_LIN(RteOutData[6]);
   Rte_Write_ComMode_LIN8_ComMode_LIN(RteOutData[7]);
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'SCIM_LINMgr_StateMachine'
//!       
//! \param   *pInData                  Input data for data structure to fill with data
//! \param   LinBusIndex               Input data for check the Bus index
//! \param   *LINSchedule              Input data for LinSchedule Table structure
//! \param   *pSM_VarLINMgrState       Input data for Current LIN Manager State
//! \param   *LINComMode               Update and check the current mode of Lin
//! \param   *LinSchTable              Update and check the Lin Schedule table
//! \param   *LinReqFlag               Update and check the Lin request
//!
//!======================================================================================
static void SCIM_LINMgr_StateMachine(const SCIMLINMgr_InData_T *pInData,
                                     const uint8               LinBusIndex,                                          
                                     const LINScheduleModes_T  *LINSchedule,
                                           VarLINMgrState_T    *pSM_VarLINMgrState,
                                           ComMode_LIN_Type    *LINComMode,
                                           uint8               *LinSchTable,
                                           uint8               *LinReqFlag)
{ 
   //! ######Processing LinMgr States
   //! ##### Check the SwcActivation_LIN is 'OperationalExit' or 'NonOperational'
   if (pInData->LIN_SwcActivation == NonOperational)
   {
      pSM_VarLINMgrState->New  = SM_LINMgr_Inactive;
   }
   else
   {
      SM_VarLINMgrStateProcessing(pInData,pSM_VarLINMgrState);
   }   
   //! ##### If LIN Manager State Changes 
   if (pSM_VarLINMgrState->Current != pSM_VarLINMgrState->New)
   {
      //! #### Schedule Table Switching & Output Processing
      switch (pSM_VarLINMgrState->New)
      {
         //! #### Switching 'FlexSwitchDetectionActive' state to all LIN%(1 to 5)
         case SM_LINMgr_FlexSwDetection:
            *LinSchTable = LINSchedule->FlexSwDetection;
         break;
         //! #### Switching 'SlaveNodePNCollection' state to all LIN%(1 to 5)
         case SM_LINMgr_SlaveNodePN:
            *LinSchTable = LINSchedule->SlaveNodePN; 
         break;
         //! #### Switching 'FSPAssignment' state to all LIN%(1 to 5)
         case SM_LINMgr_FSPAssign:
            *LinSchTable = LINSchedule->FSPAssign;
         break;
         //! #### Switching 'ApplicationRun' state to all LIN%(1 to 5)
         case SM_LINMgr_AppRun:
            *LinSchTable = LINSchedule->AppRun;
         break;
         default: //SM_LINMgr_Inactive
           *LinSchTable = LINSchedule->FSPAssign; //Diagnostic Schedule Table
         break;                                                
      }
   }
   else
   {
      // do nothing: keep previous value 
   }
   pSM_VarLINMgrState->Current = pSM_VarLINMgrState->New;   
   if((CONST_LINSchTableChangeNoRequest == *LinReqFlag)
     &&(LIN_SCHETABLE_MODESWITCH_TRANSISTION!=pInData->CurScheTable[LinBusIndex].Current))
   {
      //! ##### Schedule Table Switching & Output Processing
      switch (pSM_VarLINMgrState->Current)
      {
         //! #### Actions for FlexSwDetection state to LIN%(1 to 5)
         case SM_LINMgr_FlexSwDetection:
            if(LINSchedule->FlexSwDetection != pInData->CurScheTable[LinBusIndex].Current)
            {               
               *LinReqFlag = CONST_LINSchTableChangeRequested;
            }
            else
            {
               *LINComMode = SwitchDetection;
            }         
         break;
         //! #### Actions for SlaveNodePN state to LIN%(1 to 5)
         //! #### Actions for FSPAssign state to LIN%(1 to 5)
         case SM_LINMgr_FSPAssign:          
            if(LINSchedule->FSPAssign != pInData->CurScheTable[LinBusIndex].Current)
            {              
               *LinReqFlag = CONST_LINSchTableChangeRequested;
            }
            else
            {
                *LINComMode = Calibration;
            }
         break;
         case SM_LINMgr_SlaveNodePN:
            if(LINSchedule->SlaveNodePN != pInData->CurScheTable[LinBusIndex].Current)
            {              
               *LinReqFlag = CONST_LINSchTableChangeRequested;
            }
            else
            {
               *LINComMode = Diagnostic;
            }
         //SCIM_LINMgr_SlaveNodePN();
         break;
         //! #### Check the currrent active schedule table match the LINmgr state machine 
         //! #### Request the switching of schedule table, if not match
         case SM_LINMgr_AppRun:
            if(LINSchedule->AppRun != pInData->CurScheTable[LinBusIndex].Current)
            {           
               *LinReqFlag = CONST_LINSchTableChangeRequested;
            }
            else
            {
               *LINComMode = ApplicationMonitoring;
            }
         break;
         default: //SM_LINMgr_Inactive
            *LINComMode = Inactive;
         break;                                                      
      }
   }
   else
   {
      // do nothing: schedule change is ongoing
   }
  //! #### Lin BusOff Processing
  SCIM_LINMgr_LinBusOff_Handler(LinBusIndex,
                                pSM_VarLINMgrState,
                                LINComMode);   
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'SM_VarLINMgrStateProcessing'
//!       
//! \param   *pInData             Data structure to check data  
//! \param   *pSM_VarLINMgrState  Input data for Current LIN Manager States
//!   
//!======================================================================================
static void SM_VarLINMgrStateProcessing(const SCIMLINMgr_InData_T *pInData,
                                              VarLINMgrState_T    *pSM_VarLINMgrState)
{
   switch (pSM_VarLINMgrState->Current)
   {
      case SM_LINMgr_Inactive:
         //! #### Check the SwcActivation_LIN changes to value 'OperationalEntry' or 'Operational'
         if ((pInData->LIN_SwcActivation == OperationalEntry)
            || (pInData->LIN_SwcActivation == Operational))
         {
            pSM_VarLINMgrState->New = SM_LINMgr_FlexSwDetection;
         }
         else
         {
           // do nothing: wait activation event 
         }
      break;      
      case SM_LINMgr_FlexSwDetection:
         //! #### Check IsFlexSwDetCmplted changes to 'TRUE'
         if (TRUE == pInData->VarIsFlexSwDetCmplted.Current)
         {
            pSM_VarLINMgrState->New  = SM_LINMgr_AppRun;
         }
         else
         {
            // keep state: wait flexible switch detection is complete
         }
      break;
      case SM_LINMgr_AppRun:
         //! #### Check the VehicleMode changes from low mode to high mode
         //! #### Check the SwcActivation_LIN changes to value 'OperationalEntry'
         if (((pInData->VarVehicleMode.Current <= VehicleMode_Running)
            && (pInData->VarVehicleMode.Current > VehicleMode_Living))
            && ((pInData->VarVehicleMode.Previous <= VehicleMode_Living)))
         {
            pSM_VarLINMgrState->New  = SM_LINMgr_FlexSwDetection;
         }
         //! #### Check DiagnosticRequest_ReadPN changes to value Active
         else if (pInData->PNSNRequest == 1U) //DiagnosticRequest_ReadPN changes to Active
         {
            pSM_VarLINMgrState->New  = SM_LINMgr_SlaveNodePN;
         }
         //! #### Check DiagnosticRequest_CCNAD changes to Active
         else if (pInData->CCNADRequest == 1U)//DiagnosticRequest_CCNAD changes to Active
         {
            pSM_VarLINMgrState->New  = SM_LINMgr_FSPAssign;
         }
         else
         {
            // do nothing: keep current state
         }
      break;
      case SM_LINMgr_SlaveNodePN:
         //! #### Check DiagnosticRequest_ReadSlaveData changes to Inactive 
         if (pInData->PNSNRequest == 0U) //DiagnosticRequest_ReadSlaveData changes to Inactive
         {
            pSM_VarLINMgrState->New = SM_LINMgr_AppRun;
         }
         else
         {
            // do nothing: wait PN readout completed
         }
      break;
      case SM_LINMgr_FSPAssign:
         //! #### Check DiagnosticRequest_CCNAD changes to value Inactive
         if (pInData->CCNADRequest == 0U) //DiagnosticRequest_CCNAD changes to value Inactive
         {
            pSM_VarLINMgrState->New  = SM_LINMgr_FlexSwDetection;
         }
         else
         {
            // do nothing: wait the CCNAD completed 
         }
      break;
      default: // SM_LINMgr_Init
         pSM_VarLINMgrState->New = SM_LINMgr_Inactive;
      break;
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'SCIM_LINMgr_LinBusOff_Handler'
//!       
//! \param   LinBusIndex          Input data for check the Bus index  
//! \param   *pSM_VarLINMgrState  Input data for Current LIN Manager State
//! \param   *LINComMode          Update and check the current mode of Lin
//!   
//!======================================================================================
static void SCIM_LINMgr_LinBusOff_Handler(const uint8            LinBusIndex,
                                          const VarLINMgrState_T *pSM_VarLINMgrState,
                                                ComMode_LIN_Type *LINComMode)
{

    //! #### Process for DTC log
   if(SM_LINMgr_AppRun==pSM_VarLINMgrState->Current)
   {
      if(ScimLin_BusOff==IrvLinBusStatus[LinBusIndex])
      {
         switch(LinBusIndex)
         {
            case 0u:
               Rte_Call_Event_D1BJO_88_LIN1busCIOM_BusOff_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
            break;
            case 1u:
               Rte_Call_Event_D1BJP_88_LIN2busCIOM_BusOff_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
            break;
            case 2u:
               Rte_Call_Event_D1BJQ_88_LIN3busCIOM_BusOff_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
            break;
            case 3u:
               Rte_Call_Event_D1BJR_88_LIN4busCIOM_BusOff_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
            break;
            case 4u:
               Rte_Call_Event_D1BJS_88_LIN5busCIOM_BusOff_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
            break;
            default:
               //Do nothing:MISRA
            break;
         }
      }
      else
      {
         switch(LinBusIndex)
         {
            case 0u:
               Rte_Call_Event_D1BJO_88_LIN1busCIOM_BusOff_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            break;
            case 1u:
               Rte_Call_Event_D1BJP_88_LIN2busCIOM_BusOff_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            break;
            case 2u:
               Rte_Call_Event_D1BJQ_88_LIN3busCIOM_BusOff_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            break;
            case 3u:
               Rte_Call_Event_D1BJR_88_LIN4busCIOM_BusOff_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            break;
            case 4u:
              Rte_Call_Event_D1BJS_88_LIN5busCIOM_BusOff_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            break;
            default:
              //Do nothing:MISRA
            break;
         }
      }
   }
   else
   {
      // Do nothing
   }
   //! #### Process for LINComMode Output override
   if(((SM_LINMgr_FlexSwDetection==pSM_VarLINMgrState->Current)
     ||(SM_LINMgr_SlaveNodePN==pSM_VarLINMgrState->Current)
     ||(SM_LINMgr_AppRun==pSM_VarLINMgrState->Current))
     &&(ScimLin_BusOff==IrvLinBusStatus[LinBusIndex]))
   {
      *LINComMode = Error;
   }
   else
   {
      // Do nothing
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Appl_ScimLinmgr_LinIfGetLinStatus'
//! This function is trigged by LinIf module with Lin Frame Id and status
//!
//! \param   Channel   Providing the information about Lin channel
//! \param   Pid       Providing the information about Lin Frame Identifier
//! \param   Status    Providing the status of Lin Frame
//!
//!======================================================================================
void Appl_ScimLinmgr_LinIfGetLinStatus(NetworkHandleType Channel,
                                       Lin_FramePidType  Pid,
                                       Lin_StatusType    Status)
{
   static Lin_StatusType ScimLinmgr_LocalStatus[CONST_NoOfLIN_Networks];
          uint8          LinBusIndex                                     = 0xFFU;
   //! #### Condition to check Pid status
   if(Pid!=0U)
   {
      //MISRA
   }
   //! ###### Processing of  Appl_LinIfGetLinStatus
   //! #### Findout the Lin bus number
   switch(Channel)
   {
      case ComMConf_ComMChannel_CN_LIN00_2cd9a7df:
         LinBusIndex = 0U;
      break;
      case ComMConf_ComMChannel_CN_LIN01_5bde9749:
         LinBusIndex = 1U;
      break;
      case ComMConf_ComMChannel_CN_LIN02_c2d7c6f3:
         LinBusIndex = 2U;
      break;
      case ComMConf_ComMChannel_CN_LIN03_b5d0f665:
         LinBusIndex = 3U;
      break;
      case ComMConf_ComMChannel_CN_LIN04_2bb463c6:
         LinBusIndex = 4U;
      break;
      case ComMConf_ComMChannel_CN_LIN05_5cb35350:
         LinBusIndex = 5U;
      break;
      case ComMConf_ComMChannel_CN_LIN06_c5ba02ea:
         LinBusIndex = 6U;
      break;
      case ComMConf_ComMChannel_CN_LIN07_b2bd327c:
         LinBusIndex = 7U;
      break;
      default:
         LinBusIndex = 0xFF;
      break;
   }
   //! #### Condition for LinBus index 
   if (0xFFU!=LinBusIndex)
   {
      ScimLinmgr_LocalStatus[LinBusIndex] = Status;      
      if ((LIN_TX_ERROR == ScimLinmgr_LocalStatus[LinBusIndex])
         || (LIN_TX_HEADER_ERROR == ScimLinmgr_LocalStatus[LinBusIndex])) 
      {
         IrvLinBusStatus[LinBusIndex] = ScimLin_BusOff;
      }
      else
      {
         IrvLinBusStatus[LinBusIndex] = ScimLin_NormalCom;
      }
   }
}
//! @}
//! @}
//! @}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#define LINMgr_STOP_SEC_CODE
#include "LINMgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
