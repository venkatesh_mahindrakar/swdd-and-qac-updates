/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  SCIM_PVTPT_FlexData.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  SCIM_PVTPT_FlexData
 *  Generated at:  Fri Jun 12 17:10:05 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <SCIM_PVTPT_FlexData>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_SCIM_PVTPT_FlexData.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Debug_PVT_SCIM_FlexArrayData1: Integer in interval [0...31]
 * uint16: Integer in interval [0...65535] (standard type)
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Debug_PVT_FlexDataRequest: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_Ctr_CyclicReport (0U)
 *   Cx1_Universal_debug_trace (1U)
 *   Cx2_Keyfob_RF_data1 (2U)
 *   Cx3_Keyfob_RF_data2 (3U)
 *   Cx4_Keyfob_LF_data1 (4U)
 *   Cx5_Keyfob_LF_data2 (5U)
 *   Cx6_SW_execution_statistics (6U)
 *   Cx7_SW_state_statistics (7U)
 * Debug_PVT_LF_Trig: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_P1 (1U)
 *   Cx2_P2 (2U)
 *   Cx3_P3 (3U)
 * Debug_PVT_SCIM_FlexArrayDataId: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_Predefined_Cyclic_Report (0U)
 *   Cx1_Universal_debug_trace (1U)
 *   Cx2_Keyfob_RF_data1 (2U)
 *   Cx3_Keyfob_RF_data2 (3U)
 *   Cx4_Keyfob_LF_data1 (4U)
 *   Cx5_Keyfob_LF_data2 (5U)
 *   Cx6_SW_execution_statistics (6U)
 *   Cx7_SW_state_statistics (7U)
 *
 * Array Types:
 * ============
 * Debug_PVT_SCIM_FlexArrayData: Array with 7 element(s) of type uint8
 *
 * Record Types:
 * =============
 * ButtonStatus: Record with elements
 *   Button1ID of type uint8
 *   Button1PressCounter of type uint8
 *   Button1PressTime of type uint16
 *   Button2ID of type uint8
 *   Button2PressCounter of type uint8
 *   Button2PressTime of type uint16
 * LfRssi: Record with elements
 *   AntennaPi of type uint16
 *   AntennaP1 of type uint16
 *   AntennaP2 of type uint16
 *   AntennaP3 of type uint16
 *   AntennaP4 of type uint16
 *
 *********************************************************************************************************************/


#define SCIM_PVTPT_FlexData_START_SEC_CODE
#include "SCIM_PVTPT_FlexData_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Runnable_PVTPT_Flex_Report
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Debug_PVT_SCIM_FlexArrayData_Debug_PVT_SCIM_FlexArrayData(const uint8 *data)
 *     Argument data: uint8* is of type Debug_PVT_SCIM_FlexArrayData
 *   Std_ReturnType Rte_Write_Debug_PVT_SCIM_FlexArrayData1_Debug_PVT_SCIM_FlexArrayData1(Debug_PVT_SCIM_FlexArrayData1 data)
 *   Std_ReturnType Rte_Write_Debug_PVT_SCIM_FlexArrayDataId_Debug_PVT_SCIM_FlexArrayDataId(Debug_PVT_SCIM_FlexArrayDataId data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_LfInterface_P_GetLfAntState(LfRssi *LfRssiStatus, uint8 *FobFound, uint8 *FobLocation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_LfInterface_I_RadioApplicationError
 *   Std_ReturnType Rte_Call_RkeInterface_P_GetFobRkeState(ButtonStatus *RkeButton, uint16 *FobID, uint32 *FobSN, uint16 *FobRollingCounter, uint16 *ScimRollingCounter, uint8 *FobBattery)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RkeInterface_I_RadioApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Runnable_PVTPT_Flex_Report_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SCIM_PVTPT_FlexData_CODE) Runnable_PVTPT_Flex_Report(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Runnable_PVTPT_Flex_Report
 *********************************************************************************************************************/

Debug_PVT_FlexDataRequest FlexDataRequest;
uint8 FlexArrayData[7] = {0,};
Debug_PVT_SCIM_FlexArrayData1 FlexArrayData1 = 0;
Debug_PVT_SCIM_FlexArrayDataId  FlexArrayDataId;
LfRssi LfRssiStatus;
uint8 FobFound;
uint8 FobLocation;
ButtonStatus RkeButton;
uint16 FobID;
uint32 FobSN;
uint16 FobRollingCounter;
uint16 ScimRollingCounter;
uint8 FobBattery;

Rte_Read_Debug_PVT_Flex_Request_Debug_PVT_FlexDataRequest(&FlexDataRequest);

FlexArrayDataId = FlexDataRequest;

Rte_Call_LfInterface_P_GetLfAntState(&LfRssiStatus, &FobFound, &FobLocation);
Rte_Call_RkeInterface_P_GetFobRkeState(&RkeButton, &FobID, &FobSN, &FobRollingCounter, &ScimRollingCounter, &FobBattery);

switch(FlexDataRequest)
{
  case 0:
    // Ctr_CyclicReport
    break;
  case 1:
    // Universal debug trace
    break;
  case 2:
    // Keyfob RF data1
    FlexArrayData[0] = (uint8)(FobSN);
    FlexArrayData[1] = (uint8)(FobSN >> 8);
    FlexArrayData[2] = (uint8)(FobSN >> 16);

    FlexArrayData[3] = (uint8)(ScimRollingCounter);
    FlexArrayData[4] = (uint8)(ScimRollingCounter >> 8);

    FlexArrayData[5] = (uint8)(FobRollingCounter);
    FlexArrayData[6] = (uint8)(FobRollingCounter >> 8);
    FlexArrayData1 = 0u;
    break;
  case 3:
    // Keyfob RF data2
    FlexArrayData[0] = (uint8)(FobID);
    FlexArrayData[1] = (uint8)(FobID >> 8);

    FlexArrayData[2] = (uint8)(RkeButton.Button1ID & 0x07);
    FlexArrayData[2] |= (uint8)((RkeButton.Button1PressTime << 3) & 0xF8);

    FlexArrayData[3] = (uint8)((RkeButton.Button1PressTime >> 5) & 0x03);
    FlexArrayData[3] |= (uint8)((RkeButton.Button1PressCounter << 2) & 0x3C);
    FlexArrayData[3] |= (uint8)((RkeButton.Button2ID << 6) & 0xC0);

    FlexArrayData[4] = (uint8)((RkeButton.Button2ID >> 2) & 0x01);
    FlexArrayData[4] |= (uint8)((RkeButton.Button2PressTime << 1) & 0xFE);

    FlexArrayData[5] = (uint8)((RkeButton.Button2PressCounter) & 0x0F);
    FlexArrayData[5] |= (uint8)((FobBattery << 4) & 0x30);
    FlexArrayData1 = 0u;
    break;
  case 4:
    // Keyfob LF data1
    FlexArrayData[0] = FobLocation;
    FlexArrayData[1] = FobFound;
    FlexArrayData[2] = (uint8)(LfRssiStatus.AntennaPi);
    FlexArrayData[3] = (uint8)((LfRssiStatus.AntennaPi >> 8) & 0x01);
    FlexArrayData[3] |= (uint8)((LfRssiStatus.AntennaP1 << 1) & 0xFE);
    FlexArrayData[4] = (uint8)((LfRssiStatus.AntennaP1 >> 7) & 0x03);
    FlexArrayData[4] |= (uint8)((LfRssiStatus.AntennaP2 << 2) & 0xFC);
    FlexArrayData[5] = (uint8)((LfRssiStatus.AntennaP2 >> 6) & 0x07);
    FlexArrayData[5] |= (uint8)((LfRssiStatus.AntennaP3 << 3) & 0xF8);
    FlexArrayData[6] = (uint8)((LfRssiStatus.AntennaP3 >> 5) & 0x0F);
    FlexArrayData[6] |= (uint8)((LfRssiStatus.AntennaP4 << 4) & 0xF0);
    FlexArrayData1 = (uint8)((LfRssiStatus.AntennaP4 >> 4) & 0x1F);
    break;
  case 5:
    // Keyfob LF data2
    break;
  case 6:
    // SW execution statistics
    break;
  case 7:
    // SW state statistics
    break;
  default:
    break;
}

Rte_Write_Debug_PVT_SCIM_FlexArrayData_Debug_PVT_SCIM_FlexArrayData(FlexArrayData);
Rte_Write_Debug_PVT_SCIM_FlexArrayData1_Debug_PVT_SCIM_FlexArrayData1(FlexArrayData1);
Rte_Write_Debug_PVT_SCIM_FlexArrayDataId_Debug_PVT_SCIM_FlexArrayDataId(FlexArrayDataId);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Runnable_PVTPT_Flex_Request
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Debug_PVT_Flex_Request_Debug_PVT_FlexDataRequest(Debug_PVT_FlexDataRequest *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_LF_Trig_Debug_PVT_LF_Trig(Debug_PVT_LF_Trig *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Runnable_PVTPT_Flex_Request_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SCIM_PVTPT_FlexData_CODE) Runnable_PVTPT_Flex_Request(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Runnable_PVTPT_Flex_Request
 *********************************************************************************************************************/

Debug_PVT_LF_Trig LF_Trig;

Rte_Read_Debug_PVT_LF_Trig_Debug_PVT_LF_Trig(&LF_Trig);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define SCIM_PVTPT_FlexData_STOP_SEC_CODE
#include "SCIM_PVTPT_FlexData_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
