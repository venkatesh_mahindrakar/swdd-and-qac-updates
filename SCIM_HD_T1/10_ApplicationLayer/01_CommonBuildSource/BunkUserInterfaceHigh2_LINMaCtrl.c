/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  BunkUserInterfaceHigh2_LINMaCtrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  BunkUserInterfaceHigh2_LINMaCtrl
 *  Generated at:  Fri Jun 12 17:01:39 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <BunkUserInterfaceHigh2_LINMaCtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file BunkUserInterfaceHigh2_LINMaCtrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer
//! @{
//! @addtogroup Application_BodyAndComfort
//! @{
//! @addtogroup Application_Living
//! @{
//! @addtogroup BunkUserInterfaceHigh2_LINMaCtrl
//! @{
//!
//! \brief
//! BunkUserInterfaceHigh2_LINMaCtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the BunkUserInterfaceHigh2_LINMaCtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Hours8bit_T
 *   254 Error ; 255 Not available
 *
 * IntLghtLvlIndScaled_cmd_T
 *   14 Error; 15 Not Available
 *
 * Minutes8bit_T
 *   254 Error ; 255 Not available
 *
 * TimesetHr_T
 *   30 Error ; 31 Not Available
 *
 *********************************************************************************************************************/

#include "Rte_BunkUserInterfaceHigh2_LINMaCtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
// Includes
#include "FuncLibrary_AnwStateMachine_If.h"
#include "BunkUserInterfaceHigh2_LINMaCtrl_If.h"
#include "BunkUserInterfaceHigh2_LINMaCtrl_Logic_If.h"
#include "BunkUserInterfaceHigh2_LINMaCtrl.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DiagInfo_T: Integer in interval [0...127]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * EventFlag_T: Boolean
 * Hours8bit_T: Integer in interval [0...255]
 *   Unit: [h], Factor: 1, Offset: 0
 * IntLghtLvlIndScaled_cmd_T: Integer in interval [0...15]
 *   Unit: [Step], Factor: 1, Offset: 0
 * Minutes8bit_T: Integer in interval [0...255]
 *   Unit: [min], Factor: 1, Offset: 0
 * ResponseErrorLECM2_T: Boolean
 * TimeMinuteType_T: Integer in interval [0...63]
 *   Unit: [min], Factor: 1, Offset: 0
 * TimesetHr_T: Integer in interval [0...31]
 *   Unit: [h], Factor: 1, Offset: 0
 * VolumeValueType_T: Integer in interval [0...63]
 *   Unit: [Step], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * AlarmClkID_T: Enumeration of integer in interval [0...127] with enumerators
 *   AlarmClkID_NotUsed (0U)
 *   AlarmClkID_Alarm1 (1U)
 *   AlarmClkID_Alarm2 (2U)
 *   AlarmClkID_Alarm3 (3U)
 *   AlarmClkID_Alarm4 (4U)
 *   AlarmClkID_Alarm5 (5U)
 *   AlarmClkID_Alarm6 (6U)
 *   AlarmClkID_Alarm7 (7U)
 *   AlarmClkID_Alarm8 (8U)
 *   AlarmClkID_Alarm9 (9U)
 *   AlarmClkID_Alarm10 (10U)
 *   AlarmClkID_Error (126U)
 *   AlarmClkID_NotAvailable (127U)
 * AlarmClkStat_T: Enumeration of integer in interval [0...3] with enumerators
 *   AlarmClkStat_Inactive (0U)
 *   AlarmClkStat_Active (1U)
 *   AlarmClkStat_NoUsed (2U)
 *   AlarmClkStat_Spare (3U)
 * AlarmClkType_T: Enumeration of integer in interval [0...7] with enumerators
 *   AlarmClkType_NoAudibleNotification (0U)
 *   AlarmClkType_Buzzer (1U)
 *   AlarmClkType_Radio (2U)
 *   AlarmClkType_Reserved (3U)
 *   AlarmClkType_Reserved_01 (4U)
 *   AlarmClkType_Reserved_02 (5U)
 *   AlarmClkType_Error (6U)
 *   AlarmClkType_NotAvailable (7U)
 * BTStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   BTStatus_BTOff (0U)
 *   BTStatus_BTOnAndNoDeviceConnected (1U)
 *   BTStatus_BTOnAndDeviceConnected (2U)
 *   BTStatus_NotAvailable (3U)
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Spare1 (4U)
 *   Spare2 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * IL_Mode_T: Enumeration of integer in interval [0...7] with enumerators
 *   IL_Mode_OFF (0U)
 *   IL_Mode_NightDriving (1U)
 *   IL_Mode_Resting (2U)
 *   IL_Mode_Max (3U)
 *   IL_Mode_spare_1 (4U)
 *   IL_Mode_spare_2 (5U)
 *   IL_Mode_ErrorIndicator (6U)
 *   IL_Mode_NotAvailable (7U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * ParkHeaterTimer_cmd_T: Enumeration of integer in interval [0...7] with enumerators
 *   ParkHeaterTimer_cmd_NoAction (0U)
 *   ParkHeaterTimer_cmd_TimerEnable (1U)
 *   ParkHeaterTimer_cmd_TimerDisable (2U)
 *   ParkHeaterTimer_cmd_Spare (3U)
 *   ParkHeaterTimer_cmd_Spare_01 (4U)
 *   ParkHeaterTimer_cmd_Spare_02 (5U)
 *   ParkHeaterTimer_cmd_Error (6U)
 *   ParkHeaterTimer_cmd_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 *
 * Record Types:
 * =============
 * AlmClkCurAlarm_stat_T: Record with elements
 *   ID_RE of type AlarmClkID_T
 *   SetHr_RE of type TimesetHr_T
 *   SetMin_RE of type TimeMinuteType_T
 *   Stat_RE of type AlarmClkStat_T
 *   Type_RE of type AlarmClkType_T
 * AlmClkSetCurAlm_rqst_T: Record with elements
 *   ID_RE of type AlarmClkID_T
 *   SetHr_RE of type TimesetHr_T
 *   SetMin_RE of type TimeMinuteType_T
 *   Type_RE of type AlarmClkType_T
 *   Stat_RE of type AlarmClkStat_T
 * InteriorLightMode_T: Record with elements
 *   IL_Mode_RE of type IL_Mode_T
 *   EventFlag_RE of type EventFlag_T
 * SetParkHtrTmr_rqst_T: Record with elements
 *   Timer_cmd_RE of type ParkHeaterTimer_cmd_T
 *   StartTimeHr_RE of type Hours8bit_T
 *   StartTimeMin_RE of type Minutes8bit_T
 *   DurnTimeHr_RE of type Hours8bit_T
 *   DurnTimeMin_RE of type Minutes8bit_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1B2G_LECMH_Installed_v(void)
 *
 *********************************************************************************************************************/

#define BunkUserInterfaceHigh2_LINMaCtrl_START_SEC_CODE
#include "BunkUserInterfaceHigh2_LINMaCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_LECMH_Installed  (Rte_Prm_P1B2G_LECMH_Installed_v())
/**********************************************************************************************************************
 *
 * Runnable Entity Name: BunkUserInterfaceHigh2_LINMaCtrl_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat(AlmClkCurAlarm_stat_T *data)
 *   Std_ReturnType Rte_Read_AudioSystemStatus_AudioSystemStatus(OffOn_T *data)
 *   Std_ReturnType Rte_Read_AudioVolumeIndicationCmd_VolumeValueType(VolumeValueType_T *data)
 *   Std_ReturnType Rte_Read_BTStatus_BTStatus(BTStatus_T *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DiagInfoLECM2_DiagInfo(DiagInfo_T *data)
 *   Std_ReturnType Rte_Read_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(IntLghtLvlIndScaled_cmd_T *data)
 *   Std_ReturnType Rte_Read_IntLghtModeInd_cmd_InteriorLightMode(InteriorLightMode_T *data)
 *   Std_ReturnType Rte_Read_LIN_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst(AlmClkSetCurAlm_rqst_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2AudioOnOff_ButtonSta_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2Fade_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2IntLightActvnBtn_sta_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2IntLightDecBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2IntLightIncBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2LockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2OnOFF_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2PHTi_rqs_SetParkHtrTmr_rqst(SetParkHtrTmr_rqst_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2ParkHeater_ButtonSta_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2Phone_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2PowerWinCloseDSBtn_s_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2PowerWinClosePSBtn_s_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2PowerWinOpenDSBtn_st_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2PowerWinOpenPSBtn_st_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2RoofhatchCloseBtn_St_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2RoofhatchOpenBtn_Sta_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2TempDec_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2TempInc_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2VolumeDown_ButtonSta_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2VolumeUp_ButtonStatu_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_PhoneButtonIndication_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ResponseErrorLECM2_ResponseErrorLECM2(ResponseErrorLECM2_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst(const AlmClkSetCurAlm_rqst_T *data)
 *   Std_ReturnType Rte_Write_BunkH2AudioOnOff_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2Fade_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2IntLightActvnBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2LockButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2OnOFF_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2PHTimer_rqst_SetParkHtrTmr_rqst(const SetParkHtrTmr_rqst_T *data)
 *   Std_ReturnType Rte_Write_BunkH2ParkHeater_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2Phone_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2TempDec_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2TempInc_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2VolumeDown_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2VolumeUp_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_LIN_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat(const AlmClkCurAlarm_stat_T *data)
 *   Std_ReturnType Rte_Write_LIN_AudioSystemStatus_AudioSystemStatus(OffOn_T data)
 *   Std_ReturnType Rte_Write_LIN_AudioVolumeIndicationCmd_VolumeValueType(VolumeValueType_T data)
 *   Std_ReturnType Rte_Write_LIN_BTStatus_BTStatus(BTStatus_T data)
 *   Std_ReturnType Rte_Write_LIN_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(IntLghtLvlIndScaled_cmd_T data)
 *   Std_ReturnType Rte_Write_LIN_IntLghtModeInd_cmd_InteriorLightMode(const InteriorLightMode_T *data)
 *   Std_ReturnType Rte_Write_LIN_PhoneButtonIndication_cmd_DeviceIndication(DeviceIndication_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BKF_87_LECMHighLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOH_16_LECMHigh_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOH_17_LECMHigh_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOH_44_LECMHigh_RAM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOH_45_LECMHigh_FLASH_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOH_46_LECMHigh_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOH_49_LECMHigh_HWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOH_94_LECMHigh_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_AudioRadio2_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_AudioRadio2_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ClimPHTimerSettings3_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ClimPHTimerSettings3_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlCabRqst2_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlCabRqst2_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights4_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights4_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_PHActMaintainLiving6_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_PHActMaintainLiving6_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_PowerWindowsActivate3_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_PowerWindowsActivate3_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_RoofHatchRequest3_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_RoofHatchRequest3_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: BunkUserInterfaceHigh2_LINMaCtrl_10ms_runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution runnable logic for the BunkUserInterfaceHigh2_LINMaCtrl_10ms_runnable
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, BunkUserInterfaceHigh2_LINMaCtrl_CODE) BunkUserInterfaceHigh2_LINMaCtrl_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
    * Symbol: BunkUserInterfaceHigh2_LINMaCtrl_10ms_runnable
    *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   static BunkUserInterfaceHigh2_LINMaCtrl_in_StructType              RteInData_Common;
   static BunkUserInterfaceHigh2_LINMaCtrl_ButtonCOM_in_StructType    RteInData_ButtonCOM;
   static BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_in_StructType   RteInData_WindowLift;
   static BunkUserInterfaceHigh2_LINMaCtrl_out_StructType             RteOutData_Common   = { 0U,
                                                                                              { 7U, 0U },
                                                                                              { AlarmClkID_NotAvailable,
																							    TimesetHr_NotAvailable, 
																							    TimeMinuteType_NotAvailable, 
																							    AlarmClkStat_Inactive,
																							    AlarmClkType_NotAvailable },
                                                                                              OffOn_NotAvailable,
                                                                                              0U,
                                                                                              BTStatus_NotAvailable,
                                                                                              DeviceIndication_Off,
                                                                                              { ParkHeaterTimer_cmd_NotAvailable,
																							    Hours8bit_NotAvailable,
																							    Minutes8bit_NotAvailable,
																							    Hours8bit_NotAvailable,
																							    Minutes8bit_NotAvailable } };     // Initial value & Enumeration is not available for LIN_AlmClkCurAlarm_stat & BunkH2PHTimer_rqst (Taken 0U as of now need to discuss with volvo team)
                                                                                                                           // Enumeration is not available for LIN_IntLghtLvlIndScaled_cmd, LIN_IntLghtModeInd_cmd & LIN_AudioVolumeIndicationCmd (Taken 0U as of now need to discuss with volvo team)
   static BunkUserInterfaceHigh2_LINMaCtrl_Button_out_StructType      RteOutData_ButtonCom = { PushButtonStatus_NotAvailable,
                                                                                               PushButtonStatus_NotAvailable,
                                                                                               PushButtonStatus_NotAvailable,
                                                                                               PushButtonStatus_NotAvailable,
                                                                                               PushButtonStatus_NotAvailable,
                                                                                               PushButtonStatus_NotAvailable,
                                                                                               PushButtonStatus_NotAvailable,
                                                                                               PushButtonStatus_NotAvailable,
                                                                                               PushButtonStatus_NotAvailable,
                                                                                               PushButtonStatus_NotAvailable,
                                                                                               PushButtonStatus_NotAvailable,
                                                                                               PushButtonStatus_NotAvailable,
                                                                                               PushButtonStatus_NotAvailable,
                                                                                               PushButtonStatus_NotAvailable,
                                                                                               PushButtonStatus_NotAvailable,
                                                                                               { AlarmClkID_NotAvailable,
																							     TimesetHr_NotAvailable,
																							     TimeMinuteType_NotAvailable,
																							     AlarmClkStat_Inactive, 
																							     AlarmClkType_NotAvailable } };  // Enumeration is not available for AlmClkSetCurAlm_rqst (Taken 0U as of now need to discuss with volvo team)      
   static BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_out_StructType  RteOutData_WindowLift = { PushButtonStatus_NotAvailable,
                                                                                                PushButtonStatus_NotAvailable,
                                                                                                PushButtonStatus_NotAvailable,
                                                                                                PushButtonStatus_NotAvailable };
   static SetParkHtrTmr_rqst Var_BunkH2PHTi_rqs;
       
   // Local logic variables
   FormalBoolean isActTrig_AudioRadio               = NO;
   FormalBoolean isActTrig_ClimPHTimerSettings      = NO;
   FormalBoolean isActTrig_LockControlCabRqst       = NO;
   FormalBoolean isActTrig_PHActMaintainLiving      = NO;
   FormalBoolean isDeActTrig_PHActMaintainLiving    = NO;
   FormalBoolean isActTrig_OtherInteriorLights      = NO;
   FormalBoolean isActTrig_PowerWindowsActivate3    = NO;
   FormalBoolean isDeActTrig_PowerWindowsActivate3  = NO;
   FormalBoolean isActTrig_RoofHatchRequest3        = NO;
   FormalBoolean isDeActTrig_RoofHatchRequest3      = NO;
   FormalBoolean isRestAct_AudioRadio               = YES;
   FormalBoolean isRestAct_ClimPHTimerSettings      = YES;
   FormalBoolean isRestAct_LockControlCabRqst       = YES;
   FormalBoolean isRestAct_PHActMaintainLiving      = YES;
   FormalBoolean isRestAct_OtherInteriorLights      = YES;
   FormalBoolean isRestAct_PowerWindowsActivate3    = YES;
   FormalBoolean isRestAct_RoofHatchRequest3        = YES;
   boolean        IsFciFaultActive                  = CONST_Fault_InActive;

   ReturnType ReturnValue = { RTE_E_INVALID,
                           RTE_E_INVALID,
                           RTE_E_INVALID };
   
   Var_BunkH2PHTi_rqs.DurnTimeHr_RE.previous   = Var_BunkH2PHTi_rqs.DurnTimeHr_RE.current;
   Var_BunkH2PHTi_rqs.Timer_cmd_RE.previous    = Var_BunkH2PHTi_rqs.Timer_cmd_RE.current;
   Var_BunkH2PHTi_rqs.StartTimeHr_RE.previous  = Var_BunkH2PHTi_rqs.StartTimeHr_RE.current;
   Var_BunkH2PHTi_rqs.StartTimeMin_RE.previous = Var_BunkH2PHTi_rqs.StartTimeMin_RE.current;
   Var_BunkH2PHTi_rqs.DurnTimeMin_RE.previous  = Var_BunkH2PHTi_rqs.DurnTimeMin_RE.current;
   //! ###### Process RTE read input data common logic: 'Get_RteDataRead_Common()'
   Get_RteDataRead_Common(&RteInData_Common,
                          &RteInData_ButtonCOM,
                          &RteInData_WindowLift,
                          &ReturnValue);
   Var_BunkH2PHTi_rqs.DurnTimeHr_RE.current   = RteInData_Common.LIN_BunkH2PHTi_rqs.DurnTimeHr_RE;
   Var_BunkH2PHTi_rqs.Timer_cmd_RE.current    = RteInData_Common.LIN_BunkH2PHTi_rqs.Timer_cmd_RE;
   Var_BunkH2PHTi_rqs.StartTimeHr_RE.current  = RteInData_Common.LIN_BunkH2PHTi_rqs.StartTimeHr_RE;
   Var_BunkH2PHTi_rqs.StartTimeMin_RE.current = RteInData_Common.LIN_BunkH2PHTi_rqs.StartTimeMin_RE;
   Var_BunkH2PHTi_rqs.DurnTimeMin_RE.current  = RteInData_Common.LIN_BunkH2PHTi_rqs.DurnTimeMin_RE;
   
   //! ###### Check LIN bus status is in error state (LIN busoff)
   if (Error == RteInData_Common.ComMode_LIN1)
   {
      //! ##### Process the bunk user interface high2 LIN master control busoff fallback logic: 'FallbackLogic_BusOff_BunkUserInterfaceHigh2()'
      FallbackLogic_BusOff_BunkUserInterfaceHigh2(&RteOutData_ButtonCom);
      //! ##### Process the bunk user interface high2 LIN master control window lift fallback logic: 'FallbackLogic_WindowLift_BunkUserInterfaceHigh2()'
      FallbackLogic_WindowLift_BunkUserInterfaceHigh2(&RteOutData_WindowLift);
      //! ##### Process the bunk user interface high2 LIN master control parking heater fallback logic: 'FallbackLogic_ParkingHeater_BunkUserInterfaceHigh2()'
      FallbackLogic_ParkingHeater_BunkUserInterfaceHigh2(&RteOutData_Common.BunkH2PHTimer_rqst);
   }
   else
   {
      //! ###### Processing the bunk user interface high2 LIN master control logic
      //! ###### Select 'LECMH_Installed' parameter
      if (TRUE == PCODE_LECMH_Installed)
      {
         //! ##### Check for communication mode of LIN1 status
         if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN1)
            || (Diagnostic == RteInData_Common.ComMode_LIN1)
            || (SwitchDetection == RteInData_Common.ComMode_LIN1))
         {
            isRestAct_AudioRadio             = NO;
            isRestAct_ClimPHTimerSettings    = NO;
            isRestAct_LockControlCabRqst     = NO;
            isRestAct_PHActMaintainLiving    = NO;
            isRestAct_OtherInteriorLights    = NO;
            isRestAct_PowerWindowsActivate3  = NO;
            isRestAct_RoofHatchRequest3      = NO;
            //! ##### Check the missing frame condition for LIN frame is not received for 3 times
            //! ##### Check for response error LECM status
            if (RTE_E_MAX_AGE_EXCEEDED == ReturnValue.RetVal_LECM2)
            {
               //! ##### Check the LIN1 communication mode is in 'ApplicationMonitoring' or 'SwitchDetection'
               if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN1)
                  || (SwitchDetection == RteInData_Common.ComMode_LIN1))
               {
                  //! #### Invoke 'D1BKF_87_LECMHighLink_NoResp_SetEventStatus' with 'failed' for missing frame
                  Rte_Call_Event_D1BKF_87_LECMHighLink_NoResp_SetEventStatus(DEM_EVENT_STATUS_FAILED);
                  //! #### Process the fallback logic for bus off: 'FallbackLogic_BusOff_BunkUserInterfaceHigh2()'
                  FallbackLogic_BusOff_BunkUserInterfaceHigh2(&RteOutData_ButtonCom);
               }
               else
               {
                  //! #### Process the signal gateway logic for cab comfort : 'SignalsGateway_CabComfort_BunkUserInterfaceHigh2()'
                  SignalsGateway_CabComfort_BunkUserInterfaceHigh2(&RteInData_ButtonCOM,
                                                                   &RteOutData_ButtonCom);
                  //! #### Process the gateway logic for status indication : 'SignalsGateway_StatusIndication_BunkUserInterfaceHigh2()'
                  SignalsGateway_StatusIndication_BunkUserInterfaceHigh2(&RteInData_Common,
                                                                         &RteOutData_Common);
               }
            }
            else if ((RTE_E_OK == ReturnValue.RetVal_LECM2) 
                    || (RTE_E_COM_STOPPED == ReturnValue.RetVal_LECM2) 
                    || (RTE_E_NEVER_RECEIVED == ReturnValue.RetVal_LECM2))
            {
               //! ##### Invoke 'D1BKF_87_LECMHighLink_NoResp_SetEventStatus' with 'passed' for missing frame
               Rte_Call_Event_D1BKF_87_LECMHighLink_NoResp_SetEventStatus(DEM_EVENT_STATUS_PASSED);
               //! ##### If Fault Code Information(FCI) is in Error state 
               IsFciFaultActive = BunkUserInterfaceHigh2_LECM_H_FCI_To_DTC_conv(&RteInData_Common.DiagInfoLECM2);
               if (CONST_Fault_Active == IsFciFaultActive)
               {
                  //! #### Process bus off fallback logic : 'FallbackLogic_BusOff_BunkUserInterfaceHigh2()'
                  FallbackLogic_BusOff_BunkUserInterfaceHigh2(&RteOutData_ButtonCom);
               }
               else
               {
                  //! #### Process gateway logic for cab comfort : 'SignalsGateway_CabComfort_BunkUserInterfaceHigh2()'
                  SignalsGateway_CabComfort_BunkUserInterfaceHigh2(&RteInData_ButtonCOM,
                                                                   &RteOutData_ButtonCom);
                  //! #### Process gateway logic for status indication : 'SignalsGateway_StatusIndication_BunkUserInterfaceHigh2()'
                  SignalsGateway_StatusIndication_BunkUserInterfaceHigh2(&RteInData_Common,
                                                                         &RteOutData_Common);
               }
            }
            else
            {
               //! ##### Process the bus off fallback mode logic : 'FallbackLogic_BusOff_BunkUserInterfaceHigh2()'
               FallbackLogic_BusOff_BunkUserInterfaceHigh2(&RteOutData_ButtonCom);
            }
            //! ##### Check for bunk high2 power window open push button status
            if (RTE_E_MAX_AGE_EXCEEDED == ReturnValue.RetVal_BunkH2PowerWinOpenPSBtn_st)
            {
               //! ##### Check for LIN1 communication mode staus
               if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN1)
                  || (SwitchDetection == RteInData_Common.ComMode_LIN1))
               {
                  //! #### Process the fallback mode logic for window lift : 'FallbackLogic_WindowLift_BunkUserInterfaceHigh2()'
                  FallbackLogic_WindowLift_BunkUserInterfaceHigh2(&RteOutData_WindowLift);
               }
               else
               {  
                  //! ####  Process the window lift gateway logic : 'SignalsGateway_WindowLift_BunkUserInterfaceHigh2()'
                  SignalsGateway_WindowLift_BunkUserInterfaceHigh2(&RteInData_WindowLift,
                                                                   &RteOutData_WindowLift);
               }
            }
            //! ##### Check for bunk high2 power window open pushbutton status
            else if ((RTE_E_OK == ReturnValue.RetVal_BunkH2PowerWinOpenPSBtn_st)
                    || (RTE_E_COM_STOPPED == ReturnValue.RetVal_BunkH2PowerWinOpenPSBtn_st)
                    || (RTE_E_NEVER_RECEIVED == ReturnValue.RetVal_BunkH2PowerWinOpenPSBtn_st))
            {
               //! ####  Process the window lift gateway logic : 'SignalsGateway_WindowLift_BunkUserInterfaceHigh2()'
               SignalsGateway_WindowLift_BunkUserInterfaceHigh2(&RteInData_WindowLift,
                                                                &RteOutData_WindowLift);
            }
            else
            {
               //! ##### Process the fallback mode logic for window lift: 'FallbackLogic_WindowLift_BunkUserInterfaceHigh2()'
               FallbackLogic_WindowLift_BunkUserInterfaceHigh2(&RteOutData_WindowLift);
            }
            //! ##### Check for BunkH2PHTi_rqs_Timer_cmd status
            if (RTE_E_MAX_AGE_EXCEEDED == ReturnValue.RetVal_BunkH2PHTi_rqs_Timer_cmd)
            {
               //! ##### Check for LIN1 communication mode
               if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN1)
                  || (SwitchDetection == RteInData_Common.ComMode_LIN1))
               {
                  //! #### Process the fallback mode logic for parking heater : 'FallbackLogic_ParkingHeater_BunkUserInterfaceHigh2()'
                  FallbackLogic_ParkingHeater_BunkUserInterfaceHigh2(&RteOutData_Common.BunkH2PHTimer_rqst);
               }
               else
               {  
                  //! ####  Process the parking heater gateway logic : 'SignalsGateway_ParkingHeater_BunkUserInterfaceHigh2()'
                  SignalsGateway_ParkingHeater_BunkUserInterfaceHigh2(&Var_BunkH2PHTi_rqs,
                                                                      &RteOutData_Common.BunkH2PHTimer_rqst);
               }
            }
            //! ##### Check for BunkH2PHTi_rqs_Timer_cmd is equal RTE_E_OK,RTE_E_COM_STOPPED or RTE_E_NEVER_RECEIVED
            else if ((RTE_E_OK == ReturnValue.RetVal_BunkH2PHTi_rqs_Timer_cmd)
                    || (RTE_E_COM_STOPPED == ReturnValue.RetVal_BunkH2PHTi_rqs_Timer_cmd)
                    || (RTE_E_NEVER_RECEIVED == ReturnValue.RetVal_BunkH2PHTi_rqs_Timer_cmd))
            {
               //! ####  Process the parking heater gateway logic : 'SignalsGateway_ParkingHeater_BunkUserInterfaceHigh2()'
               SignalsGateway_ParkingHeater_BunkUserInterfaceHigh2(&Var_BunkH2PHTi_rqs,
                                                                   &RteOutData_Common.BunkH2PHTimer_rqst);
            }
            else
            {
               //! #### Process the bunk user interface high2 LIN master control fallback mode logic: 'FallbackLogic_ParkingHeater_BunkUserInterfaceHigh2()'
               FallbackLogic_ParkingHeater_BunkUserInterfaceHigh2(&RteOutData_Common.BunkH2PHTimer_rqst);
            }
         }
         else
         {
            //! ##### Process deactivation logic : 'Deactivation_Logic_BunkUserInterfaceHigh2()'
            Deactivation_Logic_BunkUserInterfaceHigh2(&RteOutData_Common,
                                                      &RteOutData_ButtonCom,
                                                      &RteOutData_WindowLift);
         }
      }    
      else
      {
         //! ###### Process deactivation logic : 'Deactivation_Logic_BunkUserInterfaceHigh2()'
         Deactivation_Logic_BunkUserInterfaceHigh2(&RteOutData_Common,
                                                   &RteOutData_ButtonCom,
                                                   &RteOutData_WindowLift);
      }
   }
   //! ###### Process audio radio2 trigger logic : 'Generic_ANW_AudioRadio2_CondCheck()'
   isActTrig_AudioRadio = Generic_ANW_AudioRadio2_CondCheck(&RteInData_WindowLift,
                                                            &RteInData_ButtonCOM);
   //! ###### Process ANW audio radio2 trigger logic : 'Generic_ANW_AudioRadio2_Trigger()'
   Generic_ANW_AudioRadio2_Trigger(isRestAct_AudioRadio,
                                   isActTrig_AudioRadio);
   //! ###### Process climate PH timer settings3 trigger logic: 'Generic_ANW_ClimPHTimerSettings3_CondCheck()'
   isActTrig_ClimPHTimerSettings = Generic_ANW_ClimPHTimerSettings3_CondCheck(&Var_BunkH2PHTi_rqs);
   //! ###### Process ANW climate PH timer settings3 trigger logic : 'Generic_ANW_ClimPHTimerSetting3_Trigger()'
   Generic_ANW_ClimPHTimerSetting3_Trigger(isRestAct_ClimPHTimerSettings,
                                            isActTrig_ClimPHTimerSettings);
   //! ###### Process lock control cab request2 trigger logic: 'Generic_ANW_LockCntrlCabRqst2_CondCheck()'
   isActTrig_LockControlCabRqst = Generic_ANW_LockCntrlCabRqst2_CondCheck(&RteInData_ButtonCOM.LIN_BunkH2LockButtonStatus);
   //! ###### Process ANW lock control cab request2 trigger logic: 'Generic_ANW_LockCntrlCabRqst2_Trigger()'
   Generic_ANW_LockCntrlCabRqst2_Trigger(isRestAct_LockControlCabRqst,
                                           isActTrig_LockControlCabRqst);
   //! ###### Process PH activation maintain living6 activation trigger logic: 'Generic_ANW_PHActMaintainLiving6_Activate_CondCheck()'
   isActTrig_PHActMaintainLiving = Generic_ANW_PHActMaintainLiving6_Activate_CondCheck(&RteInData_ButtonCOM.LIN_BunkH2OnOFF_ButtonStatus,
                                                                                       &RteInData_ButtonCOM.LIN_BunkH2ParkHeater_ButtonSta,
                                                                                       &RteInData_ButtonCOM.LIN_BunkH2TempDec_ButtonStatus,
                                                                                       &RteInData_ButtonCOM.LIN_BunkH2TempInc_ButtonStatus);
   //! ###### Process PH activation maintain living6 deactivation trigger logic: 'Generic_ANW_PHActMaintainLvng6_DeActivate_CondCheck()'
   isDeActTrig_PHActMaintainLiving = Generic_ANW_PHActMaintainLvng6_DeActivate_CondCheck(&RteInData_ButtonCOM.LIN_BunkH2TempInc_ButtonStatus,
                                                                                           &RteInData_ButtonCOM.LIN_BunkH2TempDec_ButtonStatus);
   //! ###### Process ANW PH activation maintainliving6 trigger logic: 'Generic_ANW_PHActMaintainLvng6Trigger()'
   Generic_ANW_PHActMaintainLvng6Trigger(isRestAct_PHActMaintainLiving,
                                         isActTrig_PHActMaintainLiving,
                                         isDeActTrig_PHActMaintainLiving);
   //! ###### Process other interior lights4 trigger logic: 'Generic_ANW_OtherInteriorLights4_CondCheck()'
   isActTrig_OtherInteriorLights = Generic_ANW_OtherInteriorLights4_CondCheck(&RteInData_ButtonCOM.LIN_BunkH2IntLightDecBtn_stat,
                                                                              &RteInData_ButtonCOM.LIN_BunkH2IntLightIncBtn_stat,
                                                                              &RteInData_ButtonCOM.LIN_BunkH2IntLightActvnBtn_sta);
   //! ###### Process ANW other interior lights4 trigger logic: 'Generic_ANW_OtherInteriorLight4_Trigger()'
   Generic_ANW_OtherInteriorLight4_Trigger(isRestAct_OtherInteriorLights,
                                            isActTrig_OtherInteriorLights);
   //! ###### Process power windows activate3 activation trigger logic: 'Genric_ANW_PowerWindowsActivate3_Activate_CondCheck()'
   isActTrig_PowerWindowsActivate3 = Genric_ANW_PowerWindowsActivate3_Activate_CondCheck(&RteInData_WindowLift);
   //! ###### Process power windows activate3 deactivation trigger logic: 'Genric_ANW_PowerWindowActivate3_DeActivate_CondCheck()'
   isDeActTrig_PowerWindowsActivate3 = Genric_ANW_PowerWindowActivate3_DeActivate_CondCheck(&RteInData_WindowLift);
   //! ###### Process ANW power windows activate3 trigger logic: 'Generic_ANW_PowerWindowsActivate3_Trigger()'
   Generic_ANW_PowerWindowsActivate3_Trigger(isRestAct_PowerWindowsActivate3,
                                             isActTrig_PowerWindowsActivate3,
                                             isDeActTrig_PowerWindowsActivate3);
   //! ###### Process roofhatch request3 activation trigger logic: 'Generic_ANW_RoofHatchRequest3_Activate_CondCheck()'
   isActTrig_RoofHatchRequest3 = Generic_ANW_RoofHatchRequest3_Activate_CondCheck(&RteInData_ButtonCOM.LIN_BunkH2RoofhatchCloseBtn_St,
                                                                                  &RteInData_ButtonCOM.LIN_BunkH2RoofhatchOpenBtn_Sta);
   //! ###### Process roofhatch request3 deactivation trigger logic: 'Generic_ANW_RoofHatchRequest3_DeActivate_CondCheck()'
   isDeActTrig_RoofHatchRequest3 = Generic_ANW_RoofHatchRequest3_DeActivate_CondCheck(&RteInData_ButtonCOM.LIN_BunkH2RoofhatchCloseBtn_St,
                                                                                      &RteInData_ButtonCOM.LIN_BunkH2RoofhatchOpenBtn_Sta);
   //! ###### Process ANW roofhatch request3 trigger logic: 'Generic_ANW_RoofHatchRequest3_Trigger()'
   Generic_ANW_RoofHatchRequest3_Trigger(isRestAct_RoofHatchRequest3,
                                         isActTrig_RoofHatchRequest3,
                                         isDeActTrig_RoofHatchRequest3);
   //! ###### Process RTE output data write common logic: 'RteDataWrite_Common()'
   RteDataWrite_Common(&RteOutData_Common,
                       &RteOutData_ButtonCom,
                       &RteOutData_WindowLift);
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define BunkUserInterfaceHigh2_LINMaCtrl_STOP_SEC_CODE
#include "BunkUserInterfaceHigh2_LINMaCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Get_RteDataRead_Common'
//!
//! \param   *pInput_data              Examine and update the input signals based on RTE failure events
//! \param   *pIn_ButtonCOM_data       Examine and update the input signals based on RTE failure events
//! \param   *pInput_WindowLift_data   Examine and update the input signals based on RTE failure events
//! \param   *pReturnValue             Providing the type of RTE error
//!
//!======================================================================================
static void Get_RteDataRead_Common(BunkUserInterfaceHigh2_LINMaCtrl_in_StructType            *pInput_data,
                                   BunkUserInterfaceHigh2_LINMaCtrl_ButtonCOM_in_StructType  *pIn_ButtonCOM_data,
                                   BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_in_StructType *pInput_WindowLift_data,
                                   ReturnType                                                *pReturnValue)
{
   //! ###### Processing Rte input data read common logic
   Std_ReturnType retValue = RTE_E_INVALID;
   //! ##### Read LIN_BunkH2PowerWinCloseDSBtn_s interface
   pInput_WindowLift_data->LIN_BunkH2PowerWinCloseDSBtn_s.Previous = pInput_WindowLift_data->LIN_BunkH2PowerWinCloseDSBtn_s.Current;
   retValue = Rte_Read_LIN_BunkH2PowerWinCloseDSBtn_s_PushButtonStatus(&pInput_WindowLift_data->LIN_BunkH2PowerWinCloseDSBtn_s.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pInput_WindowLift_data->LIN_BunkH2PowerWinCloseDSBtn_s.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))  
   //! ##### Read LIN_BunkH2PowerWinClosePSBtn_s interface
   pInput_WindowLift_data->LIN_BunkH2PowerWinClosePSBtn_s.Previous = pInput_WindowLift_data->LIN_BunkH2PowerWinClosePSBtn_s.Current;
   retValue = Rte_Read_LIN_BunkH2PowerWinClosePSBtn_s_PushButtonStatus(&pInput_WindowLift_data->LIN_BunkH2PowerWinClosePSBtn_s.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pInput_WindowLift_data->LIN_BunkH2PowerWinClosePSBtn_s.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))   
   //! ##### Read LIN_BunkH2PowerWinOpenDSBtn_st interface
   pInput_WindowLift_data->LIN_BunkH2PowerWinOpenDSBtn_st.Previous = pInput_WindowLift_data->LIN_BunkH2PowerWinOpenDSBtn_st.Current;
   retValue = Rte_Read_LIN_BunkH2PowerWinOpenDSBtn_st_PushButtonStatus(&pInput_WindowLift_data->LIN_BunkH2PowerWinOpenDSBtn_st.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pInput_WindowLift_data->LIN_BunkH2PowerWinOpenDSBtn_st.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))  
   //! ##### Read LIN_BunkH2PowerWinOpenPSBtn_st interface
   pInput_WindowLift_data->LIN_BunkH2PowerWinOpenPSBtn_st.Previous = pInput_WindowLift_data->LIN_BunkH2PowerWinOpenPSBtn_st.Current;
   pReturnValue->RetVal_BunkH2PowerWinOpenPSBtn_st = Rte_Read_LIN_BunkH2PowerWinOpenPSBtn_st_PushButtonStatus(&pInput_WindowLift_data->LIN_BunkH2PowerWinOpenPSBtn_st.Current);
   MACRO_StdRteRead_ExtRPort((pReturnValue->RetVal_BunkH2PowerWinOpenPSBtn_st),(pInput_WindowLift_data->LIN_BunkH2PowerWinOpenPSBtn_st.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))  
   //! ##### Read LIN_BunkH2IntLightActvnBtn_sta interface
   pIn_ButtonCOM_data->LIN_BunkH2IntLightActvnBtn_sta.Previous = pIn_ButtonCOM_data->LIN_BunkH2IntLightActvnBtn_sta.Current;
   retValue = Rte_Read_LIN_BunkH2IntLightActvnBtn_sta_PushButtonStatus(&pIn_ButtonCOM_data->LIN_BunkH2IntLightActvnBtn_sta.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pIn_ButtonCOM_data->LIN_BunkH2IntLightActvnBtn_sta.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))  
   //! ##### Read LIN_BunkH2IntLightDecBtn_stat interface
   pIn_ButtonCOM_data->LIN_BunkH2IntLightDecBtn_stat.Previous = pIn_ButtonCOM_data->LIN_BunkH2IntLightDecBtn_stat.Current;
   retValue = Rte_Read_LIN_BunkH2IntLightDecBtn_stat_PushButtonStatus(&pIn_ButtonCOM_data->LIN_BunkH2IntLightDecBtn_stat.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pIn_ButtonCOM_data->LIN_BunkH2IntLightDecBtn_stat.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))  
   //! ##### Read LIN_BunkH2IntLightIncBtn_stat interface
   pIn_ButtonCOM_data->LIN_BunkH2IntLightIncBtn_stat.Previous = pIn_ButtonCOM_data->LIN_BunkH2IntLightIncBtn_stat.Current;
   retValue = Rte_Read_LIN_BunkH2IntLightIncBtn_stat_PushButtonStatus(&pIn_ButtonCOM_data->LIN_BunkH2IntLightIncBtn_stat.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pIn_ButtonCOM_data->LIN_BunkH2IntLightIncBtn_stat.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))   
   //! ##### Read LIN_BunkH2LockButtonStatus interface
   pIn_ButtonCOM_data->LIN_BunkH2LockButtonStatus.Previous = pIn_ButtonCOM_data->LIN_BunkH2LockButtonStatus.Current;
   retValue = Rte_Read_LIN_BunkH2LockButtonStatus_PushButtonStatus(&pIn_ButtonCOM_data->LIN_BunkH2LockButtonStatus.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pIn_ButtonCOM_data->LIN_BunkH2LockButtonStatus.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))   
   //! ##### Read LIN_BunkH2RoofhatchCloseBtn_St interface
   pIn_ButtonCOM_data->LIN_BunkH2RoofhatchCloseBtn_St.Previous = pIn_ButtonCOM_data->LIN_BunkH2RoofhatchCloseBtn_St.Current;
   retValue = Rte_Read_LIN_BunkH2RoofhatchCloseBtn_St_PushButtonStatus(&pIn_ButtonCOM_data->LIN_BunkH2RoofhatchCloseBtn_St.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pIn_ButtonCOM_data->LIN_BunkH2RoofhatchCloseBtn_St.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))  
   //! ##### Read LIN_BunkH2RoofhatchOpenBtn_Sta interface
   pIn_ButtonCOM_data->LIN_BunkH2RoofhatchOpenBtn_Sta.Previous = pIn_ButtonCOM_data->LIN_BunkH2RoofhatchOpenBtn_Sta.Current;
   retValue = Rte_Read_LIN_BunkH2RoofhatchOpenBtn_Sta_PushButtonStatus(&pIn_ButtonCOM_data->LIN_BunkH2RoofhatchOpenBtn_Sta.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pIn_ButtonCOM_data->LIN_BunkH2RoofhatchOpenBtn_Sta.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))   
   //! ##### Read IntLghtLvlIndScaled_cmd interface
   retValue = Rte_Read_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(&pInput_data->IntLghtLvlIndScaled_cmd);
   MACRO_StdRteRead_IntRPort((retValue),(pInput_data->IntLghtLvlIndScaled_cmd),(IntLghtLvlIndScaled_cmd_Error))  
   //! ##### Read IntLghtModeInd_cmd interface
   retValue = Rte_Read_IntLghtModeInd_cmd_InteriorLightMode(&pInput_data->IntLghtModeInd_cmd);
   if (RTE_E_OK != retValue)
   {
      pInput_data->IntLghtModeInd_cmd.IL_Mode_RE   = IL_Mode_ErrorIndicator;  
      pInput_data->IntLghtModeInd_cmd.EventFlag_RE = EventFlag_Error; //No Enum value for EventFlag Error
   }
   else
   {
      // RTE_E_OK : Do nothing
   }
   //! ##### Read LIN_AlmClkSetCurAlm_rqst Interface
   retValue = Rte_Read_LIN_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst(&pIn_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst);
   switch (retValue)
   {
      case RTE_E_COM_STOPPED:
         // explicit break through to RTE_E_OK: no fallback mode for bus sleep behavior
      case RTE_E_OK:
         // do nothing: no fallback mode
      break;
      case RTE_E_NEVER_RECEIVED:
         // explicit break through to fallback processing: process as Not available
      case RTE_E_UNCONNECTED:
         // explicit break through to fallback processing: process as Not available
      case RTE_E_COM_BUSY:
         pIn_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.ID_RE     = AlarmClkID_NotAvailable;
         pIn_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.SetHr_RE  = TimesetHr_NotAvailable;       //As per LDS
         pIn_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.SetMin_RE = TimeMinuteType_NotAvailable;  //As per LDS
         pIn_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.Stat_RE   = AlarmClkStat_Inactive;        //As per the Requirement it changes to Inactive(AlarmClkstat_T type doesn't have "NotAvailable" Value)
         pIn_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.Type_RE   = AlarmClkType_NotAvailable ;
      break;
      case RTE_E_INVALID:
         // explicit break through to fallback processing: process as Error
      case RTE_E_MAX_AGE_EXCEEDED:
         pIn_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.ID_RE     = AlarmClkID_Error;
         pIn_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.SetHr_RE  = TimesetHr_Error;      //As per LDS
         pIn_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.SetMin_RE = TimeMinuteType_Error; //As per LDS
         pIn_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.Stat_RE   = AlarmClkStat_Inactive;//As per the Requirement it changes to Inactive(AlarmClkstat_T type doesn't have "Error" Value)
         pIn_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.Type_RE   = AlarmClkType_Error;
      break;
      default:
         pIn_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.ID_RE     = AlarmClkID_Error;
         pIn_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.SetHr_RE  = TimesetHr_Error;      //As per LDS
         pIn_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.SetMin_RE = TimeMinuteType_Error; //As per LDS
         pIn_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.Stat_RE   = AlarmClkStat_Inactive;//As per the Requirement it changes to Inactive(AlarmClkstat_T type doesn't have "Error" Value)
         pIn_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.Type_RE   = AlarmClkType_Error;
      break;
   }
   //! ##### Read LIN_BunkH2AudioOnOff_ButtonSta interface
   pIn_ButtonCOM_data->LIN_BunkH2AudioOnOff_ButtonSta.Previous = pIn_ButtonCOM_data->LIN_BunkH2AudioOnOff_ButtonSta.Current;
   retValue = Rte_Read_LIN_BunkH2AudioOnOff_ButtonSta_PushButtonStatus(&pIn_ButtonCOM_data->LIN_BunkH2AudioOnOff_ButtonSta.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pIn_ButtonCOM_data->LIN_BunkH2AudioOnOff_ButtonSta.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))  
   //! ##### Read LIN_BunkH2Fade_ButtonStatus interface
   pIn_ButtonCOM_data->LIN_BunkH2Fade_ButtonStatus.Previous = pIn_ButtonCOM_data->LIN_BunkH2Fade_ButtonStatus.Current;
   retValue = Rte_Read_LIN_BunkH2Fade_ButtonStatus_PushButtonStatus(&pIn_ButtonCOM_data->LIN_BunkH2Fade_ButtonStatus.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pIn_ButtonCOM_data->LIN_BunkH2Fade_ButtonStatus.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))  
   //! ##### Read LIN_BunkH2OnOFF_ButtonStatus interface
   pIn_ButtonCOM_data->LIN_BunkH2OnOFF_ButtonStatus.Previous = pIn_ButtonCOM_data->LIN_BunkH2OnOFF_ButtonStatus.Current;
   retValue = Rte_Read_LIN_BunkH2OnOFF_ButtonStatus_PushButtonStatus(&pIn_ButtonCOM_data->LIN_BunkH2OnOFF_ButtonStatus.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pIn_ButtonCOM_data->LIN_BunkH2OnOFF_ButtonStatus.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))  
   //! ##### Read LIN_BunkH2PHTi_rqs interface  
   pReturnValue->RetVal_BunkH2PHTi_rqs_Timer_cmd = Rte_Read_LIN_BunkH2PHTi_rqs_SetParkHtrTmr_rqst(&pInput_data->LIN_BunkH2PHTi_rqs);
   switch (pReturnValue->RetVal_BunkH2PHTi_rqs_Timer_cmd)
   {
      case RTE_E_COM_STOPPED:
         // explicit break through to RTE_E_OK: no fallback mode for bus sleep behavior
      case RTE_E_OK:      
      break;
      case RTE_E_NEVER_RECEIVED:
         // explicit break through to fallback processing: process as Not available
      case RTE_E_UNCONNECTED:
         // explicit break through to fallback processing: process as Not available
      case RTE_E_COM_BUSY:
         pInput_data->LIN_BunkH2PHTi_rqs.Timer_cmd_RE    = ParkHeaterTimer_cmd_NotAvailable;
         pInput_data->LIN_BunkH2PHTi_rqs.StartTimeHr_RE  = Hours8bit_NotAvailable;    //Acc to LDS value
         pInput_data->LIN_BunkH2PHTi_rqs.StartTimeMin_RE = Minutes8bit_NotAvailable;  //Acc to LDS value
         pInput_data->LIN_BunkH2PHTi_rqs.DurnTimeHr_RE   = Hours8bit_NotAvailable;    //Acc to LDS value
         pInput_data->LIN_BunkH2PHTi_rqs.DurnTimeMin_RE  = Minutes8bit_NotAvailable;  //Acc to LDS value
      break;
      case RTE_E_INVALID:
         // explicit break through to fallback processing: process as Error
      case RTE_E_MAX_AGE_EXCEEDED:
         pInput_data->LIN_BunkH2PHTi_rqs.Timer_cmd_RE    = ParkHeaterTimer_cmd_Error;
         pInput_data->LIN_BunkH2PHTi_rqs.StartTimeHr_RE  = Hours8bit_Error;     //Acc to LDS value
         pInput_data->LIN_BunkH2PHTi_rqs.StartTimeMin_RE = Minutes8bit_Error;  //Acc to LDS value
         pInput_data->LIN_BunkH2PHTi_rqs.DurnTimeHr_RE   = Hours8bit_Error;      //Acc to LDS value
         pInput_data->LIN_BunkH2PHTi_rqs.DurnTimeMin_RE  = Minutes8bit_Error;   //Acc to LDS value
      break;
      default:
         pInput_data->LIN_BunkH2PHTi_rqs.Timer_cmd_RE    = ParkHeaterTimer_cmd_Error;
         pInput_data->LIN_BunkH2PHTi_rqs.StartTimeHr_RE  = Hours8bit_Error;     //Acc to LDS value
         pInput_data->LIN_BunkH2PHTi_rqs.StartTimeMin_RE = Minutes8bit_Error;   //Acc to LDS value
         pInput_data->LIN_BunkH2PHTi_rqs.DurnTimeHr_RE   = Hours8bit_Error;     //Acc to LDS value
         pInput_data->LIN_BunkH2PHTi_rqs.DurnTimeMin_RE  = Minutes8bit_Error;   //Acc to LDS value
      break;
   }
   //! ##### Read LIN_BunkH2ParkHeater_ButtonSta interface
   pIn_ButtonCOM_data->LIN_BunkH2ParkHeater_ButtonSta.Previous = pIn_ButtonCOM_data->LIN_BunkH2ParkHeater_ButtonSta.Current;
   retValue = Rte_Read_LIN_BunkH2ParkHeater_ButtonSta_PushButtonStatus(&pIn_ButtonCOM_data->LIN_BunkH2ParkHeater_ButtonSta.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pIn_ButtonCOM_data->LIN_BunkH2ParkHeater_ButtonSta.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))  
   //! ##### Read LIN_BunkH2Phone_ButtonStatus interface
   pIn_ButtonCOM_data->LIN_BunkH2Phone_ButtonStatus.Previous = pIn_ButtonCOM_data->LIN_BunkH2Phone_ButtonStatus.Current;
   retValue = Rte_Read_LIN_BunkH2Phone_ButtonStatus_PushButtonStatus(&pIn_ButtonCOM_data->LIN_BunkH2Phone_ButtonStatus.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pIn_ButtonCOM_data->LIN_BunkH2Phone_ButtonStatus.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))  
   //! ##### Read LIN_BunkH2TempDec_ButtonStatus interface
   pIn_ButtonCOM_data->LIN_BunkH2TempDec_ButtonStatus.Previous = pIn_ButtonCOM_data->LIN_BunkH2TempDec_ButtonStatus.Current;
   retValue = Rte_Read_LIN_BunkH2TempDec_ButtonStatus_PushButtonStatus(&pIn_ButtonCOM_data->LIN_BunkH2TempDec_ButtonStatus.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pIn_ButtonCOM_data->LIN_BunkH2TempDec_ButtonStatus.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))  
   //! ##### Read LIN_BunkH2TempInc_ButtonStatus interface
   pIn_ButtonCOM_data->LIN_BunkH2TempInc_ButtonStatus.Previous = pIn_ButtonCOM_data->LIN_BunkH2TempInc_ButtonStatus.Current;
   retValue = Rte_Read_LIN_BunkH2TempInc_ButtonStatus_PushButtonStatus(&pIn_ButtonCOM_data->LIN_BunkH2TempInc_ButtonStatus.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pIn_ButtonCOM_data->LIN_BunkH2TempInc_ButtonStatus.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))   
   //! ##### Read LIN_BunkH2VolumeDown_ButtonSta interface
   pIn_ButtonCOM_data->LIN_BunkH2VolumeDown_ButtonSta.Previous = pIn_ButtonCOM_data->LIN_BunkH2VolumeDown_ButtonSta.Current;
   retValue = Rte_Read_LIN_BunkH2VolumeDown_ButtonSta_PushButtonStatus(&pIn_ButtonCOM_data->LIN_BunkH2VolumeDown_ButtonSta.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pIn_ButtonCOM_data->LIN_BunkH2VolumeDown_ButtonSta.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))  
   //! ##### Read LIN_BunkH2VolumeUp_ButtonStatu interface
   pIn_ButtonCOM_data->LIN_BunkH2VolumeUp_ButtonStatu.Previous = pIn_ButtonCOM_data->LIN_BunkH2VolumeUp_ButtonStatu.Current;
   retValue = Rte_Read_LIN_BunkH2VolumeUp_ButtonStatu_PushButtonStatus(&pIn_ButtonCOM_data->LIN_BunkH2VolumeUp_ButtonStatu.Current);
   MACRO_StdRteRead_ExtRPort((retValue),(pIn_ButtonCOM_data->LIN_BunkH2VolumeUp_ButtonStatu.Current),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))  
   //! ##### Read LECM2_ResponseError interface
   pReturnValue->RetVal_LECM2 = Rte_Read_ResponseErrorLECM2_ResponseErrorLECM2(&pInput_data->LECM2_ResponseError);
   MACRO_StdRteRead_ExtRPort((pReturnValue->RetVal_LECM2),( pInput_data->LECM2_ResponseError),(1U),(1U)) 
   //! ##### Read AudioSystemStatus interface
   retValue = Rte_Read_AudioSystemStatus_AudioSystemStatus(&pInput_data->AudioSystemStatus);
   MACRO_StdRteRead_ExtRPort((retValue),(pInput_data->AudioSystemStatus),(OffOn_NotAvailable),(OffOn_Error))  
   //! ##### Read AudioVolumeIndicationCmd interface
   retValue = Rte_Read_AudioVolumeIndicationCmd_VolumeValueType(&pInput_data->AudioVolumeIndicationCmd);
   MACRO_StdRteRead_ExtRPort((retValue),(pInput_data->AudioVolumeIndicationCmd),(VolumeValueType_NotAvailable),(VolumeValueType_Error))   
   //! ##### Read BTStatus interface
   retValue = Rte_Read_BTStatus_BTStatus(&pInput_data->BTStatus);
   MACRO_StdRteRead_ExtRPort((retValue),(pInput_data->BTStatus),(BTStatus_NotAvailable),(BTStatus_BTOff))  
   //! ##### Read ComMode_LIN1 interface
   retValue = Rte_Read_ComMode_LIN1_ComMode_LIN(&pInput_data->ComMode_LIN1);
   MACRO_StdRteRead_IntRPort((retValue),(pInput_data->ComMode_LIN1),(Error))
   //! ##### Read DiagInfoLECM2 interface
   retValue = Rte_Read_DiagInfoLECM2_DiagInfo(&pInput_data->DiagInfoLECM2);
   MACRO_StdRteRead_ExtRPort((retValue),(pInput_data->DiagInfoLECM2),(0U),(0U))
   //! ##### Read AlmClkCurAlarm_stat interface
   retValue = Rte_Read_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat(&pInput_data->AlmClkCurAlarm_stat);
   switch (retValue)
   {
      case RTE_E_COM_STOPPED:
         // explicit break through to RTE_E_OK: no fallback mode for bus sleep behavior
      case RTE_E_OK:
         // do nothing: no fallback mode
      break;
      case RTE_E_NEVER_RECEIVED:
         // explicit break through to fallback processing: process as Not available
      case RTE_E_UNCONNECTED:
         // explicit break through to fallback processing: process as Not available
      case RTE_E_COM_BUSY:
         pInput_data->AlmClkCurAlarm_stat.ID_RE     = AlarmClkID_NotAvailable;
         pInput_data->AlmClkCurAlarm_stat.SetHr_RE  = TimesetHr_NotAvailable;       //As per LDS
         pInput_data->AlmClkCurAlarm_stat.SetMin_RE = TimeMinuteType_NotAvailable;  //As per LDS
         pInput_data->AlmClkCurAlarm_stat.Stat_RE   = AlarmClkStat_Inactive;        //As per the Requirement it changes to Inactive(AlarmClkstat_T type doesn't have "Notavailable" Value)
         pInput_data->AlmClkCurAlarm_stat.Type_RE   = AlarmClkType_NotAvailable;
      break;
      case RTE_E_INVALID:
         // explicit break through to fallback processing: process as Error
      case RTE_E_MAX_AGE_EXCEEDED:
         pInput_data->AlmClkCurAlarm_stat.ID_RE     = AlarmClkID_Error;
         pInput_data->AlmClkCurAlarm_stat.SetHr_RE  = TimesetHr_Error;          //As per LDS
         pInput_data->AlmClkCurAlarm_stat.SetMin_RE = TimeMinuteType_Error;     //As per LDS
         pInput_data->AlmClkCurAlarm_stat.Stat_RE   = AlarmClkStat_Inactive;    //As per the Requirement it changes to Inactive(AlarmClkstat_T type doesn't have "Error" Value)
         pInput_data->AlmClkCurAlarm_stat.Type_RE   = AlarmClkType_Error;
      break;
      default:
         pInput_data->AlmClkCurAlarm_stat.ID_RE     = AlarmClkID_Error;
         pInput_data->AlmClkCurAlarm_stat.SetHr_RE  = TimesetHr_Error;          //As per LDS
         pInput_data->AlmClkCurAlarm_stat.SetMin_RE = TimeMinuteType_Error;     //As per LDS
         pInput_data->AlmClkCurAlarm_stat.Stat_RE   = AlarmClkStat_Inactive;    //As per the Requirement it changes to Inactive(AlarmClkstat_T type doesn't have "Error" Value)
         pInput_data->AlmClkCurAlarm_stat.Type_RE   = AlarmClkType_Error;
      break;
   }
   //! ##### Read PhoneButtonIndication_cmd
   retValue = Rte_Read_PhoneButtonIndication_cmd_DeviceIndication(&pInput_data->PhoneButtonIndication_cmd);
   MACRO_StdRteRead_ExtRPort((retValue),(pInput_data->PhoneButtonIndication_cmd),(DeviceIndication_Off),(DeviceIndication_Off))
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'SignalsGateway_CabComfort_BunkUserInterfaceHigh2'
//!
//! \param   *pBH2in_ButtonCOM_data   Providing the input signals to send to output ports
//! \param   *pBH2_Logic_out_data     Updating the output ports with input signals
//!
//!======================================================================================
static void SignalsGateway_CabComfort_BunkUserInterfaceHigh2(const BunkUserInterfaceHigh2_LINMaCtrl_ButtonCOM_in_StructType *pBH2in_ButtonCOM_data,
                                                                   BunkUserInterfaceHigh2_LINMaCtrl_Button_out_StructType   *pBH2_Logic_out_data)
{
   //! ###### Processing signal gateway logic for cab comfort
   //! ##### Update the output ports with values, read on input ports
   // Signals gateway, cab comfort from LECM high 
   pBH2_Logic_out_data->BunkH2AudioOnOff_ButtonStatus  = pBH2in_ButtonCOM_data->LIN_BunkH2AudioOnOff_ButtonSta.Current;
   pBH2_Logic_out_data->BunkH2Fade_ButtonStatus        = pBH2in_ButtonCOM_data->LIN_BunkH2Fade_ButtonStatus.Current;
   pBH2_Logic_out_data->BunkH2OnOFF_ButtonStatus       = pBH2in_ButtonCOM_data->LIN_BunkH2OnOFF_ButtonStatus.Current;
   pBH2_Logic_out_data->BunkH2ParkHeater_ButtonStatus  = pBH2in_ButtonCOM_data->LIN_BunkH2ParkHeater_ButtonSta.Current;
   pBH2_Logic_out_data->BunkH2Phone_ButtonStatus       = pBH2in_ButtonCOM_data->LIN_BunkH2Phone_ButtonStatus.Current;
   pBH2_Logic_out_data->BunkH2TempDec_ButtonStatus     = pBH2in_ButtonCOM_data->LIN_BunkH2TempDec_ButtonStatus.Current;
   pBH2_Logic_out_data->BunkH2TempInc_ButtonStatus     = pBH2in_ButtonCOM_data->LIN_BunkH2TempInc_ButtonStatus.Current;
   pBH2_Logic_out_data->BunkH2VolumeDown_ButtonStatus  = pBH2in_ButtonCOM_data->LIN_BunkH2VolumeDown_ButtonSta.Current;
   pBH2_Logic_out_data->BunkH2VolumeUp_ButtonStatus    = pBH2in_ButtonCOM_data->LIN_BunkH2VolumeUp_ButtonStatu.Current;
   pBH2_Logic_out_data->AlmClkSetCurAlm_rqst.SetMin_RE = pBH2in_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.SetMin_RE;
   pBH2_Logic_out_data->AlmClkSetCurAlm_rqst.Type_RE   = pBH2in_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.Type_RE;
   pBH2_Logic_out_data->AlmClkSetCurAlm_rqst.ID_RE     = pBH2in_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.ID_RE;
   pBH2_Logic_out_data->AlmClkSetCurAlm_rqst.SetHr_RE  = pBH2in_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.SetHr_RE;
   pBH2_Logic_out_data->AlmClkSetCurAlm_rqst.Stat_RE   = pBH2in_ButtonCOM_data->LIN_AlmClkSetCurAlm_rqst.Stat_RE;
   pBH2_Logic_out_data->BunkH2RoofhatchCloseBtn_Stat   = pBH2in_ButtonCOM_data->LIN_BunkH2RoofhatchCloseBtn_St.Current;
   pBH2_Logic_out_data->BunkH2RoofhatchOpenBtn_Stat    = pBH2in_ButtonCOM_data->LIN_BunkH2RoofhatchOpenBtn_Sta.Current;
   pBH2_Logic_out_data->BunkH2IntLightActvnBtn_stat    = pBH2in_ButtonCOM_data->LIN_BunkH2IntLightActvnBtn_sta.Current;
   pBH2_Logic_out_data->BunkH2IntLightDecreaseBtn_stat = pBH2in_ButtonCOM_data->LIN_BunkH2IntLightDecBtn_stat.Current;
   pBH2_Logic_out_data->BunkH2IntLightIncreaseBtn_stat = pBH2in_ButtonCOM_data->LIN_BunkH2IntLightIncBtn_stat.Current;
   pBH2_Logic_out_data->BunkH2LockButtonStatus         = pBH2in_ButtonCOM_data->LIN_BunkH2LockButtonStatus.Current;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'SignalsGateway_StatusIndication_BunkUserInterfaceHigh2'
//!
//! \param   *pBunkH2_Logic_in_data    Providing the input signals to send to output ports
//! \param   *pBunkH2_Logic_out_data   Updating the output ports with input signals
//!
//!======================================================================================
static void SignalsGateway_StatusIndication_BunkUserInterfaceHigh2(const BunkUserInterfaceHigh2_LINMaCtrl_in_StructType  *pBunkH2_Logic_in_data,
                                                                         BunkUserInterfaceHigh2_LINMaCtrl_out_StructType *pBunkH2_Logic_out_data)
{
   //! ###### Processing signal gateway logic for status indication
   //! ##### Update the output ports with values read on input ports
   // Signals gateway, status indication on LECM high 
   pBunkH2_Logic_out_data->LIN_IntLghtLvlIndScaled_cmd             = pBunkH2_Logic_in_data->IntLghtLvlIndScaled_cmd;
   pBunkH2_Logic_out_data->LIN_IntLghtModeInd_cmd.IL_Mode_RE       = pBunkH2_Logic_in_data->IntLghtModeInd_cmd.IL_Mode_RE;
   pBunkH2_Logic_out_data->LIN_IntLghtModeInd_cmd.EventFlag_RE     = pBunkH2_Logic_in_data->IntLghtModeInd_cmd.EventFlag_RE;
   pBunkH2_Logic_out_data->LIN_AlmClkCurAlarm_stat.Stat_RE         = pBunkH2_Logic_in_data->AlmClkCurAlarm_stat.Stat_RE;
   pBunkH2_Logic_out_data->LIN_AlmClkCurAlarm_stat.SetHr_RE        = pBunkH2_Logic_in_data->AlmClkCurAlarm_stat.SetHr_RE;
   pBunkH2_Logic_out_data->LIN_AlmClkCurAlarm_stat.Type_RE         = pBunkH2_Logic_in_data->AlmClkCurAlarm_stat.Type_RE;
   pBunkH2_Logic_out_data->LIN_AlmClkCurAlarm_stat.SetMin_RE       = pBunkH2_Logic_in_data->AlmClkCurAlarm_stat.SetMin_RE;
   pBunkH2_Logic_out_data->LIN_AlmClkCurAlarm_stat.ID_RE           = pBunkH2_Logic_in_data->AlmClkCurAlarm_stat.ID_RE;
   pBunkH2_Logic_out_data->LIN_AudioSystemStatus                   = pBunkH2_Logic_in_data->AudioSystemStatus;
   pBunkH2_Logic_out_data->LIN_AudioVolumeIndicationCmd            = pBunkH2_Logic_in_data->AudioVolumeIndicationCmd;
   pBunkH2_Logic_out_data->LIN_BTStatus                            = pBunkH2_Logic_in_data->BTStatus;
   pBunkH2_Logic_out_data->LIN_PhoneButtonIndication_cmd           = pBunkH2_Logic_in_data->PhoneButtonIndication_cmd;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'SignalsGateway_WindowLift_BunkUserInterfaceHigh2'
//!
//! \param   *pBunkH2_WLLogic_in_data    Providing the input signals to send to output ports
//! \param   *pBunkH2_WLLogic_out_data   Updating the output ports with input signals
//!
//!======================================================================================
static void SignalsGateway_WindowLift_BunkUserInterfaceHigh2(const BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_in_StructType  *pBunkH2_WLLogic_in_data,
                                                                   BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_out_StructType *pBunkH2_WLLogic_out_data)
{ 
   //! ###### Processing signal gateway logic for window lift
   //! ##### Update the output ports with values, read on input ports
   // Signals gateway, window lift from LECM high 
   pBunkH2_WLLogic_out_data->BunkH2OutPwrWinCloseDSBtn_stat = pBunkH2_WLLogic_in_data->LIN_BunkH2PowerWinCloseDSBtn_s.Current;
   pBunkH2_WLLogic_out_data->BunkH2OutPwrWinClosePSBtn_stat = pBunkH2_WLLogic_in_data->LIN_BunkH2PowerWinClosePSBtn_s.Current;
   pBunkH2_WLLogic_out_data->BunkH2OutPwrWinOpenDSBtn_stat  = pBunkH2_WLLogic_in_data->LIN_BunkH2PowerWinOpenDSBtn_st.Current;
   pBunkH2_WLLogic_out_data->BunkH2OutPwrWinOpenPSBtn_stat  = pBunkH2_WLLogic_in_data->LIN_BunkH2PowerWinOpenPSBtn_st.Current;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'SignalsGateway_ParkingHeater_BunkUserInterfaceHigh2'
//!
//! \param   *pInBunkH2PHTimer_rqst   Providing the input signals to send to output ports
//! \param   *pBunkH2PHTimer_rqst     Updating the output ports with input signals
//!
//!======================================================================================
static void SignalsGateway_ParkingHeater_BunkUserInterfaceHigh2(const SetParkHtrTmr_rqst   *pInBunkH2PHTimer_rqst,
                                                                      SetParkHtrTmr_rqst_T *pBunkH2PHTimer_rqst)
{
   //! ###### Processing signals gateway logic for parking heater
   //! ##### Update the output ports with values read on input ports
   // Signals gateway, parking heater from LECM high 
   pBunkH2PHTimer_rqst->Timer_cmd_RE    = pInBunkH2PHTimer_rqst->Timer_cmd_RE.current;
   pBunkH2PHTimer_rqst->StartTimeMin_RE = pInBunkH2PHTimer_rqst->StartTimeMin_RE.current;
   pBunkH2PHTimer_rqst->StartTimeHr_RE  = pInBunkH2PHTimer_rqst->StartTimeHr_RE.current;
   pBunkH2PHTimer_rqst->DurnTimeMin_RE  = pInBunkH2PHTimer_rqst->DurnTimeMin_RE.current;
   pBunkH2PHTimer_rqst->DurnTimeHr_RE   = pInBunkH2PHTimer_rqst->DurnTimeHr_RE.current;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'BunkUserInterfaceHigh2_LECM_H_FCI_To_DTC_conv'
//!
//! \param   *pDiagInforLECM2   Providing the diagnostic information of LECM2
//!
//!======================================================================================
static boolean BunkUserInterfaceHigh2_LECM_H_FCI_To_DTC_conv(const DiagInfo_T *pDiagInforLECM2)
{
   static uint8 SlaveFCIFault[5] = { CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive };
   boolean IsFciFaultActive      = CONST_Fault_InActive;
   uint8 Index                   = 0U;

   //! ###### Process BunkUserInterfaceHigh2 FCI Indication logic
   //! ##### Check the FCI Conditions and Log/Remove the Faults
   if ((DiagInfo_T)0x41 == *pDiagInforLECM2)
   {
      //! #### Set supply_undervoltage fault event to fail
      Rte_Call_Event_D1BOH_16_LECMHigh_VBT_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x01 == *pDiagInforLECM2)
   {
      //! #### Set supply_undervoltage fault event to pass
      Rte_Call_Event_D1BOH_16_LECMHigh_VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x42 == *pDiagInforLECM2)
   {
      //! #### Set supply_overvoltage fault event to fail
      Rte_Call_Event_D1BOH_17_LECMHigh_VAT_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x02 == *pDiagInforLECM2)
   {
      //! #### Set supply_overvoltage fault event to pass
      Rte_Call_Event_D1BOH_17_LECMHigh_VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x43 == *pDiagInforLECM2)
   {
        SlaveFCIFault[0] = CONST_SlaveFCIFault_Active;
      //! #### Set RAM_checksum_error Fault Event to FAIL
      Rte_Call_Event_D1BOH_44_LECMHigh_RAM_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x03 == *pDiagInforLECM2)
   {
      SlaveFCIFault[0] = CONST_SlaveFCIFault_InActive;
      //! #### Set RAM_checksum_error Fault Event to PASS
      Rte_Call_Event_D1BOH_44_LECMHigh_RAM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x44 == *pDiagInforLECM2)
   {
      SlaveFCIFault[1] = CONST_SlaveFCIFault_Active;
      //! #### Set Program_checksum_error Fault Event to FAIL
      Rte_Call_Event_D1BOH_45_LECMHigh_FLASH_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x04 == *pDiagInforLECM2)
   {
      SlaveFCIFault[1] = CONST_SlaveFCIFault_InActive;
      //! #### Set Program_checksum_error Fault Event to PASS
      Rte_Call_Event_D1BOH_45_LECMHigh_FLASH_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x45 == *pDiagInforLECM2)
   {
        SlaveFCIFault[2] = CONST_SlaveFCIFault_Active;
      //! #### Set Data_checksum_error(EEPROM) Fault Event to FAIL
      Rte_Call_Event_D1BOH_46_LECMHigh_EEPROM_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x05 == *pDiagInforLECM2)
   {
        SlaveFCIFault[2] = CONST_SlaveFCIFault_InActive;
      //! #### Set Data_checksum_error(EEPROM) Fault Event to PASS
      Rte_Call_Event_D1BOH_46_LECMHigh_EEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x46 == *pDiagInforLECM2)
   {
        SlaveFCIFault[3] = CONST_SlaveFCIFault_Active;
      //! #### Set Unexpected_operation(watchDog, overrun) Fault Event to FAIL
      Rte_Call_Event_D1BOH_94_LECMHigh_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x06 == *pDiagInforLECM2)
   {
      SlaveFCIFault[3] = CONST_SlaveFCIFault_InActive;
      //! #### Set Unexpected_operation(watchDog, overrun) Fault Event to PASS
      Rte_Call_Event_D1BOH_94_LECMHigh_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x47 == *pDiagInforLECM2)
   {
        SlaveFCIFault[4] = CONST_SlaveFCIFault_Active;
      //! #### Set Internal_error(sensors, switches, LEDs) Fault Event to FAIL
      Rte_Call_Event_D1BOH_49_LECMHigh_HWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x07 == *pDiagInforLECM2)
   {
        SlaveFCIFault[4] = CONST_SlaveFCIFault_InActive;
      //! #### Set Internal_error(sensors, switches, LEDs) Fault Event to PASS
      Rte_Call_Event_D1BOH_49_LECMHigh_HWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if((DiagInfo_T)0x00 == *pDiagInforLECM2)
   {
      for(Index = 0U;Index < 5U;Index++)
      {
         SlaveFCIFault[Index] = CONST_SlaveFCIFault_InActive;
      }
      Rte_Call_Event_D1BOH_16_LECMHigh_VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BOH_17_LECMHigh_VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BOH_44_LECMHigh_RAM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BOH_45_LECMHigh_FLASH_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BOH_46_LECMHigh_EEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BOH_94_LECMHigh_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1BOH_49_LECMHigh_HWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else
   {
      // Do nothing, keep previous status
   }
   IsFciFaultActive = CONST_Fault_InActive;
   for(Index = 0U;Index < 5U;Index++)
   {
      if(CONST_SlaveFCIFault_Active == SlaveFCIFault[Index])
      {
         IsFciFaultActive = CONST_Fault_Active;
      }
      else
      {
         // Do Nothing
      }
   }
   return IsFciFaultActive;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'Deactivation_Logic_BunkUserInterfaceHigh2'
//!
//! \param   *poutputDeAct_data              Update the output ports to not available
//! \param   *pOutputDeAct_Button_data       Update the output of button status to not available
//! \param   *poutputDeAct_Windowlift_data   Update the output of window lift ports to not available
//!
//!======================================================================================
static void Deactivation_Logic_BunkUserInterfaceHigh2(BunkUserInterfaceHigh2_LINMaCtrl_out_StructType            *poutputDeAct_data,
                                                      BunkUserInterfaceHigh2_LINMaCtrl_Button_out_StructType     *pOutputDeAct_Button_data,
                                                      BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_out_StructType *poutputDeAct_Windowlift_data)
{ 
   //! ###### Processing the bunk user interface high2 LIN master control deactivation logic
   //! ##### Update the all output ports to 'not available' 
   pOutputDeAct_Button_data->AlmClkSetCurAlm_rqst.ID_RE           = AlarmClkID_NotAvailable;
   pOutputDeAct_Button_data->AlmClkSetCurAlm_rqst.SetHr_RE        = TimesetHr_NotAvailable;          //according to LDS
   pOutputDeAct_Button_data->AlmClkSetCurAlm_rqst.SetMin_RE       = TimeMinuteType_NotAvailable;     //according to LDS
   pOutputDeAct_Button_data->AlmClkSetCurAlm_rqst.Stat_RE         = AlarmClkStat_Inactive;                 //As per the Requirement it changes to Inactive(AlarmClkstat_T type doesn't have "NotAvailable" Value)
   pOutputDeAct_Button_data->AlmClkSetCurAlm_rqst.Type_RE         = AlarmClkType_NotAvailable;
   pOutputDeAct_Button_data->BunkH2AudioOnOff_ButtonStatus        = PushButtonStatus_NotAvailable;
   pOutputDeAct_Button_data->BunkH2Fade_ButtonStatus              = PushButtonStatus_NotAvailable;
   pOutputDeAct_Button_data->BunkH2IntLightActvnBtn_stat          = PushButtonStatus_NotAvailable;
   pOutputDeAct_Button_data->BunkH2IntLightDecreaseBtn_stat       = PushButtonStatus_NotAvailable;
   pOutputDeAct_Button_data->BunkH2IntLightIncreaseBtn_stat       = PushButtonStatus_NotAvailable;
   pOutputDeAct_Button_data->BunkH2LockButtonStatus               = PushButtonStatus_NotAvailable;
   pOutputDeAct_Button_data->BunkH2OnOFF_ButtonStatus             = PushButtonStatus_NotAvailable;
   poutputDeAct_Windowlift_data->BunkH2OutPwrWinCloseDSBtn_stat   = PushButtonStatus_NotAvailable;
   poutputDeAct_Windowlift_data->BunkH2OutPwrWinClosePSBtn_stat   = PushButtonStatus_NotAvailable;
   poutputDeAct_Windowlift_data->BunkH2OutPwrWinOpenDSBtn_stat    = PushButtonStatus_NotAvailable;
   poutputDeAct_Windowlift_data->BunkH2OutPwrWinOpenPSBtn_stat    = PushButtonStatus_NotAvailable;
   pOutputDeAct_Button_data->BunkH2ParkHeater_ButtonStatus        = PushButtonStatus_NotAvailable;
   pOutputDeAct_Button_data->BunkH2Phone_ButtonStatus             = PushButtonStatus_NotAvailable;
   pOutputDeAct_Button_data->BunkH2RoofhatchCloseBtn_Stat         = PushButtonStatus_NotAvailable;
   pOutputDeAct_Button_data->BunkH2RoofhatchOpenBtn_Stat          = PushButtonStatus_NotAvailable;
   pOutputDeAct_Button_data->BunkH2TempDec_ButtonStatus           = PushButtonStatus_NotAvailable;
   pOutputDeAct_Button_data->BunkH2TempInc_ButtonStatus           = PushButtonStatus_NotAvailable;
   poutputDeAct_data->BunkH2PHTimer_rqst.DurnTimeHr_RE            = Hours8bit_NotAvailable;              //As per LDS
   poutputDeAct_data->BunkH2PHTimer_rqst.DurnTimeMin_RE           = Minutes8bit_NotAvailable;            //As per LDS
   poutputDeAct_data->BunkH2PHTimer_rqst.StartTimeHr_RE           = Hours8bit_NotAvailable;              //As per LDS
   poutputDeAct_data->BunkH2PHTimer_rqst.StartTimeMin_RE          = Minutes8bit_NotAvailable;            //As per LDS
   poutputDeAct_data->BunkH2PHTimer_rqst.Timer_cmd_RE             = ParkHeaterTimer_cmd_NotAvailable;
   pOutputDeAct_Button_data->BunkH2VolumeDown_ButtonStatus        = PushButtonStatus_NotAvailable;
   pOutputDeAct_Button_data->BunkH2VolumeUp_ButtonStatus          = PushButtonStatus_NotAvailable;
   poutputDeAct_data->LIN_AlmClkCurAlarm_stat.ID_RE               = AlarmClkID_NotAvailable;
   poutputDeAct_data->LIN_AlmClkCurAlarm_stat.SetHr_RE            = TimesetHr_NotAvailable;               //according to LDS
   poutputDeAct_data->LIN_AlmClkCurAlarm_stat.SetMin_RE           = TimeMinuteType_NotAvailable;          //according to LDS
   poutputDeAct_data->LIN_AlmClkCurAlarm_stat.Stat_RE             = AlarmClkStat_Inactive;               //As per the Requirement it changes to Inactive(AlarmClkstat_T type doesn't have "NotAvailable" Value)
   poutputDeAct_data->LIN_AlmClkCurAlarm_stat.Type_RE             = AlarmClkType_NotAvailable;
   poutputDeAct_data->LIN_AudioSystemStatus                       = OffOn_NotAvailable;                           //written by considering the type  of s/g
   poutputDeAct_data->LIN_AudioVolumeIndicationCmd                = DeviceIndication_Off;                         //based on name of port
   poutputDeAct_data->LIN_BTStatus                                = BTStatus_NotAvailable;
   poutputDeAct_data->LIN_IntLghtLvlIndScaled_cmd                 = IntLghtLvlIndScaled_cmd_NotAvailable;   //according to LDS
   poutputDeAct_data->LIN_IntLghtModeInd_cmd.EventFlag_RE         = EventFlag_NotAvailable;                 //No Enum value for EventFlag NotAvailable                 
   poutputDeAct_data->LIN_IntLghtModeInd_cmd.IL_Mode_RE           = IL_Mode_NotAvailable;
   poutputDeAct_data->LIN_PhoneButtonIndication_cmd               = DeviceIndication_Off;                         //according to LDS
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'FallbackLogic_BusOff_BunkUserInterfaceHigh2'
//!
//! \param   *pOutput_Busoff_Button_data   Update the output ports to error
//!
//!======================================================================================
static void FallbackLogic_BusOff_BunkUserInterfaceHigh2(BunkUserInterfaceHigh2_LINMaCtrl_Button_out_StructType  *pOutput_Busoff_Button_data)
{
   //! ###### Processing the fallback mode logic for bus off
   //! ##### Update the all output ports to 'error' 
   pOutput_Busoff_Button_data->AlmClkSetCurAlm_rqst.ID_RE       = AlarmClkID_Error;
   pOutput_Busoff_Button_data->AlmClkSetCurAlm_rqst.SetHr_RE    = TimesetHr_Error;                   //according to LDS
   pOutput_Busoff_Button_data->AlmClkSetCurAlm_rqst.SetMin_RE   = TimeMinuteType_Error;              //according to LDS
   pOutput_Busoff_Button_data->AlmClkSetCurAlm_rqst.Stat_RE     = AlarmClkStat_Inactive;             //As per the Requirement it changes to Inactive(AlarmClkstat_T type doesn't have "Error" Value)
   pOutput_Busoff_Button_data->AlmClkSetCurAlm_rqst.Type_RE     = AlarmClkType_Error;
   pOutput_Busoff_Button_data->BunkH2AudioOnOff_ButtonStatus    = PushButtonStatus_Error;
   pOutput_Busoff_Button_data->BunkH2Fade_ButtonStatus          = PushButtonStatus_Error;
   pOutput_Busoff_Button_data->BunkH2IntLightActvnBtn_stat      = PushButtonStatus_Error;
   pOutput_Busoff_Button_data->BunkH2IntLightDecreaseBtn_stat   = PushButtonStatus_Error;
   pOutput_Busoff_Button_data->BunkH2IntLightIncreaseBtn_stat   = PushButtonStatus_Error;
   pOutput_Busoff_Button_data->BunkH2LockButtonStatus           = PushButtonStatus_Error;
   pOutput_Busoff_Button_data->BunkH2OnOFF_ButtonStatus         = PushButtonStatus_Error;
   pOutput_Busoff_Button_data->BunkH2ParkHeater_ButtonStatus    = PushButtonStatus_Error;
   pOutput_Busoff_Button_data->BunkH2Phone_ButtonStatus         = PushButtonStatus_Error;
   pOutput_Busoff_Button_data->BunkH2RoofhatchCloseBtn_Stat     = PushButtonStatus_Error;
   pOutput_Busoff_Button_data->BunkH2RoofhatchOpenBtn_Stat      = PushButtonStatus_Error;
   pOutput_Busoff_Button_data->BunkH2TempDec_ButtonStatus       = PushButtonStatus_Error;
   pOutput_Busoff_Button_data->BunkH2TempInc_ButtonStatus       = PushButtonStatus_Error;
   pOutput_Busoff_Button_data->BunkH2VolumeDown_ButtonStatus    = PushButtonStatus_Error;
   pOutput_Busoff_Button_data->BunkH2VolumeUp_ButtonStatus      = PushButtonStatus_Error;

}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'FallbackLogic_WindowLift_BunkUserInterfaceHigh2'
//!
//! \param   *pout_WindowLift_data   Update the output ports to error
//!
//!======================================================================================
static void FallbackLogic_WindowLift_BunkUserInterfaceHigh2(BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_out_StructType *pout_WindowLift_data)
{
   //! ###### Processing the fallback logic for window lift
   //! ##### Update the all output ports to 'error'
   pout_WindowLift_data->BunkH2OutPwrWinCloseDSBtn_stat = PushButtonStatus_Error;
   pout_WindowLift_data->BunkH2OutPwrWinClosePSBtn_stat = PushButtonStatus_Error;
   pout_WindowLift_data->BunkH2OutPwrWinOpenDSBtn_stat  = PushButtonStatus_Error;
   pout_WindowLift_data->BunkH2OutPwrWinOpenPSBtn_stat  = PushButtonStatus_Error;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'FallbackLogic_ParkingHeater_BunkUserInterfaceHigh2'
//!
//! \param   *pBunkH2PHTimer_rqst   Update the output ports to error
//!
//!======================================================================================
static void FallbackLogic_ParkingHeater_BunkUserInterfaceHigh2(SetParkHtrTmr_rqst_T *pBunkH2PHTimer_rqst)
{
   //! ###### Processing the fallback logic for parking heater
   //! ##### Update the all output ports to 'error'
   pBunkH2PHTimer_rqst->DurnTimeHr_RE   = Hours8bit_Error;           //As per LDS
   pBunkH2PHTimer_rqst->DurnTimeMin_RE  = Minutes8bit_Error;         //As per LDS
   pBunkH2PHTimer_rqst->StartTimeHr_RE  = Hours8bit_Error;           //As per LDS
   pBunkH2PHTimer_rqst->StartTimeMin_RE = Minutes8bit_Error;         //As per LDS
   pBunkH2PHTimer_rqst->Timer_cmd_RE    = ParkHeaterTimer_cmd_Error;  
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'RteDataWrite_Common'
//! 
//! \param   *poutput_data          Update the output signals to ports of output structure
//! \param   *poutput_button_data   Update the output signals of buttons to the ports of output structure
//! \param   *pOutput_window_data   Update the output signals of window lift to ports of output structure
//!
//!======================================================================================
static void RteDataWrite_Common(const BunkUserInterfaceHigh2_LINMaCtrl_out_StructType            *poutput_data,
                                const BunkUserInterfaceHigh2_LINMaCtrl_Button_out_StructType     *poutput_button_data,
                                const BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_out_StructType *pOutput_window_data)
{
   //! ###### Processing the RTE data common write logic
   Rte_Write_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus(pOutput_window_data->BunkH2OutPwrWinCloseDSBtn_stat);
   Rte_Write_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus(pOutput_window_data->BunkH2OutPwrWinClosePSBtn_stat);
   Rte_Write_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus(pOutput_window_data->BunkH2OutPwrWinOpenDSBtn_stat);
   Rte_Write_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus(pOutput_window_data->BunkH2OutPwrWinOpenPSBtn_stat);
   Rte_Write_BunkH2IntLightActvnBtn_stat_PushButtonStatus(poutput_button_data->BunkH2IntLightActvnBtn_stat);
   Rte_Write_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus(poutput_button_data->BunkH2IntLightDecreaseBtn_stat);
   Rte_Write_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus(poutput_button_data->BunkH2IntLightIncreaseBtn_stat);
   Rte_Write_BunkH2LockButtonStatus_PushButtonStatus(poutput_button_data->BunkH2LockButtonStatus);
   Rte_Write_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus(poutput_button_data->BunkH2RoofhatchCloseBtn_Stat);
   Rte_Write_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus(poutput_button_data->BunkH2RoofhatchOpenBtn_Stat);
   Rte_Write_LIN_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(poutput_data->LIN_IntLghtLvlIndScaled_cmd);
   Rte_Write_LIN_IntLghtModeInd_cmd_InteriorLightMode(&poutput_data->LIN_IntLghtModeInd_cmd);
   Rte_Write_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst(&poutput_button_data->AlmClkSetCurAlm_rqst);
   Rte_Write_BunkH2AudioOnOff_ButtonStatus_PushButtonStatus(poutput_button_data->BunkH2AudioOnOff_ButtonStatus);
   Rte_Write_BunkH2Fade_ButtonStatus_PushButtonStatus(poutput_button_data->BunkH2Fade_ButtonStatus);
   Rte_Write_BunkH2OnOFF_ButtonStatus_PushButtonStatus(poutput_button_data->BunkH2OnOFF_ButtonStatus);
   Rte_Write_BunkH2PHTimer_rqst_SetParkHtrTmr_rqst(&poutput_data->BunkH2PHTimer_rqst);
   Rte_Write_BunkH2ParkHeater_ButtonStatus_PushButtonStatus(poutput_button_data->BunkH2ParkHeater_ButtonStatus);
   Rte_Write_BunkH2Phone_ButtonStatus_PushButtonStatus(poutput_button_data->BunkH2Phone_ButtonStatus);
   Rte_Write_BunkH2TempDec_ButtonStatus_PushButtonStatus(poutput_button_data->BunkH2TempDec_ButtonStatus);
   Rte_Write_BunkH2TempInc_ButtonStatus_PushButtonStatus(poutput_button_data->BunkH2TempInc_ButtonStatus);
   Rte_Write_BunkH2VolumeDown_ButtonStatus_PushButtonStatus(poutput_button_data->BunkH2VolumeDown_ButtonStatus);
   Rte_Write_BunkH2VolumeUp_ButtonStatus_PushButtonStatus(poutput_button_data->BunkH2VolumeUp_ButtonStatus);
   Rte_Write_LIN_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat(&poutput_data->LIN_AlmClkCurAlarm_stat);
   Rte_Write_LIN_AudioSystemStatus_AudioSystemStatus(poutput_data->LIN_AudioSystemStatus);
   Rte_Write_LIN_AudioVolumeIndicationCmd_VolumeValueType(poutput_data->LIN_AudioVolumeIndicationCmd);
   Rte_Write_LIN_BTStatus_BTStatus(poutput_data->LIN_BTStatus);
   Rte_Write_LIN_PhoneButtonIndication_cmd_DeviceIndication(poutput_data->LIN_PhoneButtonIndication_cmd);
}

//! @}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
