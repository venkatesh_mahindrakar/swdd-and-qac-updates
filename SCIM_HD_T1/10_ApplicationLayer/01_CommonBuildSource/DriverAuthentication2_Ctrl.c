/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  DriverAuthentication2_Ctrl.c
 *        Config:  C:/Vector/scim_sip/CBD1800194_D02_Mpc57xx/volvoscim-Hardware-Ver4_3/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  DriverAuthentication2_Ctrl
 *  Generated at:  Thu Aug 13 16:16:33 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <DriverAuthentication2_Ctrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file DriverAuthentication2_Ctrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_BodyAndSecurity 
//! @{
//! @addtogroup Application_DriverAuthentication
//! @{
//! @addtogroup DriverAuthentication2_Ctrl
//! @{
//!
//! \brief
//! DriverAuthentication2_Ctrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the DriverAuthentication2_Ctrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   Boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * SEWS_CrankingLockActivation_P1DS3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1VKG_APM_Check_Active_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1VKG_DISPLAY_Check_Active_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1VKG_EMS_Check_Active_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1VKG_MVUC_Check_Active_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1VKG_TECU_Check_Active_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_DriverAuthentication2_Ctrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
// Includes
#include "FuncLibrary_AnwStateMachine_If.h"
#include "DriverAuthentication2_Ctrl.h"
#include "FuncLibrary_Timer_If.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
static PinCodeProcessing_Enum  PinCode_SM = SM_PinCodeInactive;
/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_CrankingLockActivation_P1DS3_T: Integer in interval [0...255]
 * SEWS_P1VKG_DISPLAY_Check_Active_T: Integer in interval [0...255]
 * SEWS_P1VKG_EMS_Check_Active_T: Integer in interval [0...255]
 * SEWS_P1VKG_MVUC_Check_Active_T: Integer in interval [0...255]
 * SEWS_P1VKG_TECU_Check_Active_T: Integer in interval [0...255]
 * VIN_rqst_T: Boolean
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DeviceAuthentication_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   DeviceAuthentication_rqst_Idle (0U)
 *   DeviceAuthentication_rqst_DeviceAuthenticationRequest (1U)
 *   DeviceAuthentication_rqst_DeviceDeauthenticationRequest (2U)
 *   DeviceAuthentication_rqst_DeviceMatching (3U)
 *   DeviceAuthentication_rqst_Spare1 (4U)
 *   DeviceAuthentication_rqst_Spare2 (5U)
 *   DeviceAuthentication_rqst_Error (6U)
 *   DeviceAuthentication_rqst_NotAvailable (7U)
 * DeviceInCab_stat_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceInCab_stat_Idle (0U)
 *   DeviceInCab_stat_DeviceNotAuthenticated (1U)
 *   DeviceInCab_stat_DeviceAuthenticated (2U)
 *   DeviceInCab_stat_NotAvailable (3U)
 * DoorsAjar_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorsAjar_stat_Idle (0U)
 *   DoorsAjar_stat_BothDoorsAreClosed (1U)
 *   DoorsAjar_stat_DriverDoorIsOpen (2U)
 *   DoorsAjar_stat_PassengerDoorIsOpen (3U)
 *   DoorsAjar_stat_BothDoorsAreOpen (4U)
 *   DoorsAjar_stat_Spare (5U)
 *   DoorsAjar_stat_Error (6U)
 *   DoorsAjar_stat_NotAvailable (7U)
 * EngineStartAuth_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   EngineStartAuth_rqst_StartingOfTheTruckNotRequested (0U)
 *   EngineStartAuth_rqst_StartingOfTheTruckRequested (1U)
 *   EngineStartAuth_rqst_Spare (2U)
 *   EngineStartAuth_rqst_Spare_01 (3U)
 *   EngineStartAuth_rqst_Spare_02 (4U)
 *   EngineStartAuth_rqst_Spare_03 (5U)
 *   EngineStartAuth_rqst_Error (6U)
 *   EngineStartAuth_rqst_NotAvailable (7U)
 * EngineStartAuth_stat_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 *   EngineStartAuth_stat_decrypt_Idle (0U)
 *   EngineStartAuth_stat_decrypt_CrankingIsAuthorized (1U)
 *   EngineStartAuth_stat_decrypt_CrankingIsProhibited (2U)
 *   EngineStartAuth_stat_decrypt_Spare1 (3U)
 *   EngineStartAuth_stat_decrypt_Spare2 (4U)
 *   EngineStartAuth_stat_decrypt_Spare3 (5U)
 *   EngineStartAuth_stat_decrypt_Error (6U)
 *   EngineStartAuth_stat_decrypt_NotAvailable (7U)
 * GearBoxUnlockAuth_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 *   GearBoxUnlockAuth_rqst_GearEngagementNotRequested (0U)
 *   GearBoxUnlockAuth_rqst_GearEngagementRequested (1U)
 *   GearBoxUnlockAuth_rqst_Error (2U)
 *   GearBoxUnlockAuth_rqst_NotAvaiable (3U)
 * GearboxUnlockAuth_stat_decrypt_T: Enumeration of integer in interval [0...3] with enumerators
 *   GearboxUnlockAuth_stat_decrypt_Idle (0U)
 *   GearboxUnlockAuth_stat_decrypt_GearEngagementAllowed (1U)
 *   GearboxUnlockAuth_stat_decrypt_GearEngagementRefused (2U)
 *   GearboxUnlockAuth_stat_decrypt_Spare1 (3U)
 *   GearboxUnlockAuth_stat_decrypt_Spare2 (4U)
 *   GearboxUnlockAuth_stat_decrypt_Spare3 (5U)
 *   GearboxUnlockAuth_stat_decrypt_Error (6U)
 *   GearboxUnlockAuth_stat_decrypt_NotAvailable (7U)
 * KeyAuthentication_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyAuthentication_rqst_KeyNotPresent (0U)
 *   KeyAuthentication_rqst_KeyIsInserted (1U)
 *   KeyAuthentication_rqst_RequestAuthentication (2U)
 *   KeyAuthentication_rqst_Spare (3U)
 *   KeyAuthentication_rqst_Spare01 (4U)
 *   KeyAuthentication_rqst_Spare02 (5U)
 *   KeyAuthentication_rqst_Error (6U)
 *   KeyAuthentication_rqst_NotAvailable (7U)
 * KeyAuthentication_stat_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyAuthentication_stat_decrypt_KeyNotAuthenticated (0U)
 *   KeyAuthentication_stat_decrypt_KeyAuthenticated (1U)
 *   KeyAuthentication_stat_decrypt_Spare1 (2U)
 *   KeyAuthentication_stat_decrypt_Spare2 (3U)
 *   KeyAuthentication_stat_decrypt_Spare3 (4U)
 *   KeyAuthentication_stat_decrypt_Spare4 (5U)
 *   KeyAuthentication_stat_decrypt_Error (6U)
 *   KeyAuthentication_stat_decrypt_NotAvailable (7U)
 * KeyNotValid_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyNotValid_Idle (0U)
 *   KeyNotValid_KeyNotValid (1U)
 *   KeyNotValid_Error (6U)
 *   KeyNotValid_NotAvailable (7U)
 * KeyfobAuth_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobAuth_rqst_Idle (0U)
 *   KeyfobAuth_rqst_RequestByPassiveMechanism (1U)
 *   KeyfobAuth_rqst_RequestByImmobilizerMechanism (2U)
 *   KeyfobAuth_rqst_Spare1 (3U)
 *   KeyfobAuth_rqst_Spare2 (4U)
 *   KeyfobAuth_rqst_Spare3 (5U)
 *   KeyfobAuth_rqst_Error (6U)
 *   KeyfobAuth_rqst_NotAavailable (7U)
 * KeyfobAuth_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobAuth_stat_Idle (0U)
 *   KeyfobAuth_stat_NokeyfobAuthenticated (1U)
 *   KeyfobAuth_stat_KeyfobAuthenticated (2U)
 *   KeyfobAuth_stat_Spare1 (3U)
 *   KeyfobAuth_stat_Spare2 (4U)
 *   KeyfobAuth_stat_Spare3 (5U)
 *   KeyfobAuth_stat_Error (6U)
 *   KeyfobAuth_stat_NotAvailable (7U)
 * PinCode_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   PinCode_rqst_Idle (0U)
 *   PinCode_rqst_PINCodeNotNeeded (1U)
 *   PinCode_rqst_StatusOfThePinCodeRequested (2U)
 *   PinCode_rqst_PinCodeNeeded (3U)
 *   PinCode_rqst_ResetPinCodeStatus (4U)
 *   PinCode_rqst_Spare (5U)
 *   PinCode_rqst_Error (6U)
 *   PinCode_rqst_NotAvailable (7U)
 * PinCode_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   PinCode_stat_Idle (0U)
 *   PinCode_stat_NoPinCodeEntered (1U)
 *   PinCode_stat_WrongPinCode (2U)
 *   PinCode_stat_GoodPinCode (3U)
 *   PinCode_stat_Error (6U)
 *   PinCode_stat_NotAvailable (7U)
 * SEWS_P1VKG_APM_Check_Active_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_P1VKG_APM_Check_Active_T_No (0U)
 *   SEWS_P1VKG_APM_Check_Active_T_Yes (1U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Crypto_Function_serialized_T: Array with 12 element(s) of type uint8
 * Dcm_Data5ByteType: Array with 5 element(s) of type uint8
 * SEWS_ChassisId_CHANO_T: Array with 16 element(s) of type uint8
 * StandardNVM_T: Array with 4 element(s) of type uint8
 * VIN_stat_T: Array with 11 element(s) of type uint8
 *
 * Record Types:
 * =============
 * SEWS_VINCheckProcessing_P1VKG_s_T: Record with elements
 *   APM_Check_Active of type SEWS_P1VKG_APM_Check_Active_T
 *   MVUC_Check_Active of type SEWS_P1VKG_MVUC_Check_Active_T
 *   EMS_Check_Active of type SEWS_P1VKG_EMS_Check_Active_T
 *   TECU_Check_Active of type SEWS_P1VKG_TECU_Check_Active_T
 *   DISPLAY_Check_Active of type SEWS_P1VKG_DISPLAY_Check_Active_T
 * VINCheckStatus_T: Record with elements
 *   isDISPLAY_CheckPassed of type boolean
 *   isAPM_CheckPassed of type boolean
 *   isVMCU_CheckPassed of type boolean
 *   isEMS_CheckPassed of type boolean
 *   isTECU_CheckPassed of type boolean
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1VKI_PassiveStart_Installed_v(void)
 *   SEWS_VINCheckProcessing_P1VKG_s_T *Rte_Prm_P1VKG_VINCheckProcessing_v(void)
 *   boolean Rte_Prm_P1TTA_GearBoxLockActivation_v(void)
 *   SEWS_CrankingLockActivation_P1DS3_T Rte_Prm_P1DS3_CrankingLockActivation_v(void)
 *   uint8 *Rte_Prm_CHANO_ChassisId_v(void)
 *     Returnvalue: uint8* is of type SEWS_ChassisId_CHANO_T
 *
 *********************************************************************************************************************/


#define DriverAuthentication2_Ctrl_START_SEC_CODE
#include "DriverAuthentication2_Ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_CHANO_ChassisId          (*Rte_Prm_CHANO_ChassisId_v())
#define PCODE_GearBoxLockActivation    (Rte_Prm_P1TTA_GearBoxLockActivation_v())
#define PCODE_CrankingLockActivation   (Rte_Prm_P1DS3_CrankingLockActivation_v())
#define PCODE_VINCheckProcessing       (*Rte_Prm_P1VKG_VINCheckProcessing_v())

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VKH_Data_P1VKH_VINCheck_Status>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData_Vincheckstatus(VINCheckStatus_T *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data5ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VKH_Data_P1VKH_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DriverAuthentication2_Ctrl_CODE) DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData (returns application error)
 *********************************************************************************************************************/
   VINCheckStatus_T VINCheckStat;
   Rte_IrvRead_DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData_Vincheckstatus(&VINCheckStat);
   Data[0U] = (uint8)VINCheckStat.isAPM_CheckPassed;
   Data[1U] = (uint8)VINCheckStat.isVMCU_CheckPassed;
   Data[2U] = (uint8)VINCheckStat.isEMS_CheckPassed;
   Data[3U] = (uint8)VINCheckStat.isTECU_CheckPassed;
   Data[4U] = (uint8)VINCheckStat.isDISPLAY_CheckPassed;

   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DriverAuthentication2_Ctrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_DeviceAuthentication_rqst_DeviceAuthentication_rqst(DeviceAuthentication_rqst_T *data)
 *   Std_ReturnType Rte_Read_DoorsAjar_stat_DoorsAjar_stat(DoorsAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM(uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Receive_ECU1VIN_stat_VIN_stat(uint8 *data)
 *     Argument data: uint8* is of type VIN_stat_T
 *   Std_ReturnType Rte_Receive_ECU2VIN_stat_VIN_stat(uint8 *data)
 *     Argument data: uint8* is of type VIN_stat_T
 *   Std_ReturnType Rte_Receive_ECU3VIN_stat_VIN_stat(uint8 *data)
 *     Argument data: uint8* is of type VIN_stat_T
 *   Std_ReturnType Rte_Receive_ECU4VIN_stat_VIN_stat(uint8 *data)
 *     Argument data: uint8* is of type VIN_stat_T
 *   Std_ReturnType Rte_Receive_ECU5VIN_stat_VIN_stat(uint8 *data)
 *     Argument data: uint8* is of type VIN_stat_T
 *   Std_ReturnType Rte_Read_EngineStartAuth_rqst_EngineStartAuth_rqst(EngineStartAuth_rqst_T *data)
 *   Std_ReturnType Rte_Read_GearBoxUnlockAuth_rqst_GearBoxUnlockAuth_rqst(GearBoxUnlockAuth_rqst_T *data)
 *   Std_ReturnType Rte_Read_KeyAuthentication_rqst_KeyAuthentication_rqst(KeyAuthentication_rqst_T *data)
 *   Std_ReturnType Rte_Read_KeyfobAuth_stat_KeyfobAuth_stat(KeyfobAuth_stat_T *data)
 *   Std_ReturnType Rte_Read_PinCode_stat_PinCode_stat(PinCode_stat_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DeviceInCab_stat_DeviceInCab_stat(DeviceInCab_stat_T data)
 *   Std_ReturnType Rte_Write_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM(const uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Write_EngineStartAuth_st_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_EngineStartAuth_stat_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_EngineStartAuth_stat_decrypt_EngineStartAuth_stat_decrypt(EngineStartAuth_stat_decrypt_T data)
 *   Std_ReturnType Rte_Write_GearboxUnlockAuth_stat_decrypt_GearboxUnlockAuth_stat_decrypt(GearboxUnlockAuth_stat_decrypt_T data)
 *   Std_ReturnType Rte_Write_GrbxUnlockAuth_stat_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_GrbxUnlockAuth_stat_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_KeyAuth_stat_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_KeyAuth_stat_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt(KeyAuthentication_stat_decrypt_T data)
 *   Std_ReturnType Rte_Write_KeyNotValid_KeyNotValid(KeyNotValid_T data)
 *   Std_ReturnType Rte_Write_KeyfobAuth_rqst_KeyfobAuth_rqst(KeyfobAuth_rqst_T data)
 *   Std_ReturnType Rte_Write_PinCode_rqst_PinCode_rqst(PinCode_rqst_T data)
 *   Std_ReturnType Rte_Write_VIN_rqst_VIN_rqst(VIN_rqst_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DriverAuthentication2_Ctrl_20ms_runnable_Vincheckstatus(const VINCheckStatus_T *data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_UR_ANW_ImmobilizerPINCode_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ImmobilizerPINCode_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DriverAuthentication2_Ctrl_20ms_runnable_doc
 *********************************************************************************************************************/ 
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the DriverAuthentication2_Ctrl_20ms_runnable
//!
//!==================================================================================================================== 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DriverAuthentication2_Ctrl_CODE) DriverAuthentication2_Ctrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DriverAuthentication2_Ctrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   static DriverAuthentication2Ctrl_InData_T  RteInData_Common;
   static DriverAuthentication2Ctrl_OutData_T RteOutData_Common = { FALSE,
                                                                    FALSE,
                                                                    FALSE,
                                                                    FALSE,
                                                                    DeviceInCab_stat_NotAvailable,
                                                                    EngineStartAuth_rqst_NotAvailable,
                                                                    GearboxUnlockAuth_stat_decrypt_Spare1,
                                                                    KeyfobAuth_stat_NotAvailable,
                                                                    KeyAuthentication_stat_decrypt_NotAvailable,
                                                                    KeyNotValid_NotAvailable,
                                                                    KeyfobAuth_rqst_NotAavailable,
                                                                    PinCode_rqst_NotAvailable};
   static Authorisation_States                SM_States         = { SM_Authorisation_NotAuthorised_Fail,
                                                                    SM_Authorisation_NotAuthorised_Idle };
   // Local logic variables  
   //static PinCodeProcessing_Enum  PinCode_SM                   = SM_PinCodeInactive; //Made gloabal variable temporary
   static VinCheckProcessing_Enum SM_VinCheck                       = SM_VINCheckIdle;
   KeyAuthenticationEvent_Enum        keyAuthenticationEvent        = EVENT_KeyAuthenticationIdle;
   boolean                            isDeauthenticationEvent       = FALSE;
   boolean                            isAuthenticationRequestEvent  = FALSE;
   SEWS_VINCheckProcessing_P1VKG_s_T  VINCheckActivation            =  PCODE_VINCheckProcessing;
   static VINCheckStatus_T VINCheckStatus;
   static uint16 Timers[CONST_NbOfTimers];
   FormalBoolean actTrigger   = FALSE;
   FormalBoolean deactTrigger = FALSE;

   //! ###### Process RTE in data read common logic : 'Get_RteInDataRead_Common()'
   Get_RteInDataRead_Common(&RteInData_Common);
   //! ###### Timers decrement logic : 'TimerFunction_Tick()'
   TimerFunction_Tick(CONST_NbOfTimers,
                      Timers);
   //! ###### Process authentication of the driver by keyfob request trigger logic : 'AuthenticationOfDriverByKeyfobRqtTrigger()'
   isAuthenticationRequestEvent = AuthenticationOfDriverByKeyfobRqtTrigger(&RteInData_Common.DeviceAuthentication_rqst,
                                                                           &Timers[CONST_KeyfobAuth_Rqst_Timer],
                                                                           &RteOutData_Common.KeyfobAuth_rqst);

   //! ###### Process the key deauthentication logic : 'KeyDeauthenticationCheck()'
   isDeauthenticationEvent = KeyDeauthenticationCheck(&RteInData_Common.VehicleMode,
                                                      &RteInData_Common.DoorsAjar_stat,
                                                      &RteInData_Common.DeviceAuthentication_rqst,
                                                      &Timers[CONST_KeyDeauthentication_Timer]);
   //! ###### Check for keyfob authentication status to update key authentication event 
   if (RteInData_Common.KeyfobAuth_stat.Current != RteInData_Common.KeyfobAuth_stat.Previous)
   {
      if (KeyfobAuth_stat_KeyfobAuthenticated == RteInData_Common.KeyfobAuth_stat.Current)
      {
         keyAuthenticationEvent = EVENT_KeyAuthenticationSuccessful;
      }
      else if (KeyfobAuth_stat_NokeyfobAuthenticated == RteInData_Common.KeyfobAuth_stat.Current)
      {
         keyAuthenticationEvent = EVENT_KeyAuthenticationFail;
      }
      else
      {
         keyAuthenticationEvent = EVENT_KeyAuthenticationIdle;
      }
   }
   else
   {
      // Do nothing: keep previous status
   }
   //! ###### Process the authorisation state transition logic : 'AuthorisationStateMachine()'
   AuthorisationStateMachine(isAuthenticationRequestEvent,
                             keyAuthenticationEvent,
                             isDeauthenticationEvent,
                             PinCode_SM,
                             &SM_States);
   //! ###### Process the output actions for authorisation state transition logic : 'AuthStateMachine_OutputsProcessing()'
   AuthStateMachine_OutputsProcessing(&SM_States,
                                      &RteOutData_Common);
   //! ###### Process report driver authentication status logic : 'ReportDriverAuthenticationStatus()'
   ReportDriverAuthenticationStatus(&RteInData_Common.DeviceAuthentication_rqst,
                                    SM_States.newValue,
                                    &RteOutData_Common.DeviceInCabInt_stat);
   //! ###### Process VIN check logic : 'VIN_Check()'
   VIN_Check(RteInData_Common.ECUsVIN_stat,
             &RteInData_Common.VehicleMode,
             &VINCheckActivation,
             &Timers[CONST_VIN_CheckTimer],
             &RteOutData_Common.VIN_rqst,
             &SM_VinCheck,
			 &VINCheckStatus);
   //! ###### Select the GearBoxLockActivation parameter
   if (TRUE == PCODE_GearBoxLockActivation)
   {
      //! ##### Process the gear box logic : 'GearBoxLockLogic()'
      GearBoxLockLogic(&RteInData_Common.GearBoxUnlockAuth_rqst,
                       SM_States.newValue,
                       SM_VinCheck,
                       &RteOutData_Common.GearboxUnlockAuth_stat);
   }
   else
   {
      RteOutData_Common.GearboxUnlockAuth_stat = GearboxUnlockAuth_stat_decrypt_GearEngagementAllowed;
   }
   //! ###### Check for 'ImmobilizerActivationStatus' parameter status
   if (TRUE == PCODE_CrankingLockActivation)
   {
      //! ##### Process cranking authorisation logic : 'CrankingAuthorisationLogic()'
      CrankingAuthorisationLogic(&RteInData_Common.EngineStartAuth_rqst,
                                 SM_States.newValue,
                                 SM_VinCheck,
                                 &RteOutData_Common.EngineStartAuth_stat);
   }
   else
   {
      RteOutData_Common.EngineStartAuth_stat = EngineStartAuth_stat_decrypt_CrankingIsAuthorized;
   }
   //! ###### Process ANW_ImmobilizerPINCode activation trigger logic : 'ANW_ImmobilizerPINCodeActivationCondition()'
   actTrigger   = ANW_ImmobilizerPINCodeActivationCondition(&RteOutData_Common.KeyNotValid);
   deactTrigger = YES;
   //! ###### Process ANW_ImmobilizerPINCode logic : 'ANW_ImmobilizerPINCode()'
   ANW_ImmobilizerPINCode(&actTrigger,
                          &deactTrigger);
   //! ###### Process RTE write common logic : 'RteInDataWrite_Common()'
   RteInDataWrite_Common(&RteOutData_Common);
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define DriverAuthentication2_Ctrl_STOP_SEC_CODE
#include "DriverAuthentication2_Ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief
//!  This function implements the logic for the Get_RteInDataRead_Common
//!
//! \param   *pRteInData_Common   Examine and update the input signals based on RTE failure events
//!
//!======================================================================================
static void Get_RteInDataRead_Common(DriverAuthentication2Ctrl_InData_T *pRteInData_Common)
{
   Std_ReturnType retValue = RTE_E_INVALID;
   
   //! ##### Read DeviceAuthentication_rqst interface
   pRteInData_Common->DeviceAuthentication_rqst.Previous = pRteInData_Common->DeviceAuthentication_rqst.Current;
   retValue = Rte_Read_DeviceAuthentication_rqst_DeviceAuthentication_rqst(&pRteInData_Common->DeviceAuthentication_rqst.Current);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->DeviceAuthentication_rqst.Current),(DeviceAuthentication_rqst_Error))
   //! ##### Read DoorsAjar_stat interface
   pRteInData_Common->DoorsAjar_stat.Previous = pRteInData_Common->DoorsAjar_stat.Current;
   retValue = Rte_Read_DoorsAjar_stat_DoorsAjar_stat(&pRteInData_Common->DoorsAjar_stat.Current);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->DoorsAjar_stat.Current),(DoorsAjar_stat_Error))
   //! ##### Read ECU1VIN_stat interface
   retValue = Rte_Receive_ECU1VIN_stat_VIN_stat(&pRteInData_Common->ECUsVIN_stat.ECU1VIN_stat[0]);
   if (RTE_E_OK != retValue)
   {
      // RTE_E_OK : do nothing
   }
   else
   {
      // RTE_E_OK : do nothing
   } 
   //! ##### Read ECU2VIN_stat interface
   retValue = Rte_Receive_ECU2VIN_stat_VIN_stat(&pRteInData_Common->ECUsVIN_stat.ECU2VIN_stat[0]); 
   if (RTE_E_OK != retValue)
   {
      // RTE_E_OK : do nothing
   }
   else
   {
      // RTE_E_OK : do nothing
   }
   //! ##### Read ECU3VIN_stat interface
   retValue = Rte_Receive_ECU3VIN_stat_VIN_stat(&pRteInData_Common->ECUsVIN_stat.ECU3VIN_stat[0]);
   if (RTE_E_OK != retValue)
   {
      // RTE_E_OK : do nothing
   }
   else
   {
      // RTE_E_OK : do nothing
   }
   //! ##### Read ECU4VIN_stat interface
   retValue = Rte_Receive_ECU4VIN_stat_VIN_stat(&pRteInData_Common->ECUsVIN_stat.ECU4VIN_stat[0]);
   if (RTE_E_OK != retValue)
   {
      // RTE_E_OK : do nothing
   }
   else
   {
      // RTE_E_OK : do nothing
   }
   //! ##### Read ECU5VIN_stat interface
   retValue = Rte_Receive_ECU5VIN_stat_VIN_stat(&pRteInData_Common->ECUsVIN_stat.ECU5VIN_stat[0]);
   if (RTE_E_OK != retValue)
   {
      // RTE_E_OK : do nothing
   }
   else
   {
      // RTE_E_OK : do nothing
   }
   //! ##### Read EngineStartAuth_rqst interface
   retValue = Rte_Read_EngineStartAuth_rqst_EngineStartAuth_rqst(&pRteInData_Common->EngineStartAuth_rqst);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->EngineStartAuth_rqst),(EngineStartAuth_rqst_NotAvailable),(EngineStartAuth_rqst_Error))
   //! ##### Read GearBoxUnlockAuth_rqst interface
   retValue = Rte_Read_GearBoxUnlockAuth_rqst_GearBoxUnlockAuth_rqst(&pRteInData_Common->GearBoxUnlockAuth_rqst);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->GearBoxUnlockAuth_rqst),(GearBoxUnlockAuth_rqst_NotAvaiable),(GearBoxUnlockAuth_rqst_Error))
   //! ##### Read KeyAuthentication_rqst interface
   retValue = Rte_Read_KeyAuthentication_rqst_KeyAuthentication_rqst(&pRteInData_Common->KeyAuthentication_rqst);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->KeyAuthentication_rqst),(KeyAuthentication_rqst_NotAvailable),(KeyAuthentication_rqst_Error))
   //! ##### Read KeyfobAuth_stat interface
   pRteInData_Common->KeyfobAuth_stat.Previous = pRteInData_Common->KeyfobAuth_stat.Current;
   retValue = Rte_Read_KeyfobAuth_stat_KeyfobAuth_stat(&pRteInData_Common->KeyfobAuth_stat.Current);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->KeyfobAuth_stat.Current),(KeyfobAuth_stat_Error))
   //! ##### Read VehicleMode interface
   pRteInData_Common->VehicleMode.Previous = pRteInData_Common->VehicleMode.Current;
   retValue = Rte_Read_VehicleModeInternal_VehicleMode(&pRteInData_Common->VehicleMode.Current);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->VehicleMode.Current),(VehicleMode_Error))
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the RteInDataWrite_Common
//!     
//! \param    *pRteOutputData_common   Update the output signals to ports of output structure
//!
//!======================================================================================
static void RteInDataWrite_Common(const DriverAuthentication2Ctrl_OutData_T *pRteOutputData_common)
{
   //! ###### Write all the output ports
   uint8 pCrypto_rqst_serialized[sizeof(Crypto_Function_serialized_T)];

   (void)Rte_Write_DeviceInCab_stat_DeviceInCab_stat(pRteOutputData_common->DeviceInCabInt_stat);
   pCrypto_rqst_serialized[0] = pRteOutputData_common->EngineStartAuth_stat;
   (void)Rte_Write_EngineStartAuth_st_serialized_Crypto_Function_serialized(&pCrypto_rqst_serialized[0]);
   Rte_Write_EngineStartAuth_stat_CryptTrig_CryptoTrigger(pRteOutputData_common->EngineStartAuth_stat_CryptTrig);
   Rte_Write_EngineStartAuth_stat_decrypt_EngineStartAuth_stat_decrypt(pRteOutputData_common->EngineStartAuth_stat);
   Rte_Write_GearboxUnlockAuth_stat_decrypt_GearboxUnlockAuth_stat_decrypt(pRteOutputData_common->GearboxUnlockAuth_stat);
   Rte_Write_GrbxUnlockAuth_stat_CryptTrig_CryptoTrigger(pRteOutputData_common->GrbxUnlockAuth_stat_CryptTrig);
   pCrypto_rqst_serialized[0] = pRteOutputData_common->GearboxUnlockAuth_stat;
   (void)Rte_Write_GrbxUnlockAuth_stat_serialized_Crypto_Function_serialized(&pCrypto_rqst_serialized[0]);
   Rte_Write_KeyAuth_stat_CryptTrig_CryptoTrigger(pRteOutputData_common->KeyAuth_stat_CryptTrig);
   pCrypto_rqst_serialized[0] = pRteOutputData_common->KeyAuthentication_stat;
   (void)Rte_Write_KeyAuth_stat_serialized_Crypto_Function_serialized(&pCrypto_rqst_serialized[0]);
   Rte_Write_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt(pRteOutputData_common->KeyAuthentication_stat);
   (void)Rte_Write_KeyNotValid_KeyNotValid(pRteOutputData_common->KeyNotValid);
   Rte_Write_KeyfobAuth_rqst_KeyfobAuth_rqst(pRteOutputData_common->KeyfobAuth_rqst);
   Rte_Write_PinCode_rqst_PinCode_rqst(pRteOutputData_common->PinCode_rqst);
   (void)Rte_Write_VIN_rqst_VIN_rqst(pRteOutputData_common->VIN_rqst);
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the KeyDeauthenticationCheck
//!
//! \param   *pVehicle_mode              Providing the status of vehicle mode
//! \param   *pDoorsAjar_stat            Providing the input status of door ajar status
//! \param   *pDeviceAuthenticate_rqst   Indicating the input data of device authentication request
//! \param   *pDeauthenticationTimer     To check current timer value and update 
//!
//! \return   Boolean                    Return 'True' or 'False'
//!
//!======================================================================================
static boolean KeyDeauthenticationCheck(const VehicleMode_Internal         *pVehicle_mode,
                                        const DoorsAjar_status             *pDoorsAjar_stat,
                                        const DeviceAuthentication_request *pDeviceAuthenticate_rqst,
                                              uint16                       *pDeauthenticationTimer)
{
   boolean isDeauthenticationEvent = FALSE;

   //! ##### Check for internal changes of vehicle mode
   if ((VehicleMode_Living == pVehicle_mode->Current)
      || (VehicleMode_Parked == pVehicle_mode->Current))
   {
      if ((VehicleMode_Running == pVehicle_mode->Previous)
         || (VehicleMode_Cranking == pVehicle_mode->Previous)
         || (VehicleMode_PreRunning == pVehicle_mode->Previous)
         || (VehicleMode_Accessory == pVehicle_mode->Previous))
      {
         *pDeauthenticationTimer = CONST_KeyDeauthentication_Period;
      }
      else
      {
         if (CONST_TimerFunctionElapsed == *pDeauthenticationTimer)
         {
            isDeauthenticationEvent = TRUE;
         }
         else
         {
            // Do nothing: keep previous status
         }
      }
   }
   else
   {
      *pDeauthenticationTimer = CONST_TimerFunctionInactive;
   }
   if ((VehicleMode_Parked == pVehicle_mode->Current)
      || (VehicleMode_Living == pVehicle_mode->Current)
      || (VehicleMode_Accessory == pVehicle_mode->Current)
      || (VehicleMode_PreRunning == pVehicle_mode->Current))
   {
      //! #### Check for DoorsAjar status
      if (DoorsAjar_stat_BothDoorsAreClosed == pDoorsAjar_stat->Previous)
      {
         if ((DoorsAjar_stat_DriverDoorIsOpen == pDoorsAjar_stat->Current)
            || (DoorsAjar_stat_PassengerDoorIsOpen == pDoorsAjar_stat->Current))
         {
            isDeauthenticationEvent = TRUE;
         }
         else
         {
            // No action: waiting for opening the door 
         }
      }
      else
      {
         // No action: event not confirmed
      }
   }
   else
   {
      // Do nothing: keep previous status
   }
   if (pVehicle_mode->Current != pVehicle_mode->Previous)
   {
      if (VehicleMode_Hibernate == pVehicle_mode->Current)
      {
         isDeauthenticationEvent = TRUE;
      }
      else
      {
         // No action: event not confirmed
      }
   }
   else
   {
      // Do nothing: keep previous status
   }
   //! ##### Check for DeviceAuthenticate_rqst status
   if ((pDeviceAuthenticate_rqst->Current != pDeviceAuthenticate_rqst->Previous)
      && (DeviceAuthentication_rqst_DeviceDeauthenticationRequest == pDeviceAuthenticate_rqst->Current))
   {
      isDeauthenticationEvent = TRUE;
   }
   else
   {
      // Do nothing: keep previous status
   }
   return isDeauthenticationEvent;
}
//!======================================================================================
//!
//! \brief
//!  This function implements the logic for the AuthorisationStateMachine
//!
//! \param   isAuthenticationRequestEvent    Provides the authentication request event is true or false
//! \param   KeyAuthenticationEvent          Provides the key authentication event is successful or failed
//! \param   isDeauthenticationEvent         Provides the deauthentication event is true or false
//! \param   PinCode_SM_Auth                 Provides the pincode mode is unlocked or inactive
//! \param   *pAuthorisation_SM              Provides and updates the current state
//!
//!======================================================================================
static void AuthorisationStateMachine(const boolean                     isAuthenticationRequestEvent,
                                      const KeyAuthenticationEvent_Enum KeyAuthenticationEvent,
                                      const boolean                     isDeauthenticationEvent,
                                            PinCodeProcessing_Enum      PinCode_SM_Auth,
                                            Authorisation_States        *pAuthorisation_SM)
{
   switch (pAuthorisation_SM->newValue)
   {
      //! ##### Check for state change from 'NotAuthorised Fail' to 'Authorised','TemporaryAuthorised' or 'NotAuthorised_Idle'
      case SM_Authorisation_NotAuthorised_Fail:
         //! #### Check for PIN code mode value
         if (SM_PinCodeUnlocked == PinCode_SM_Auth)
         {
            pAuthorisation_SM->newValue = SM_Authorisation_TemporaryAuthorised;
         }
         //! #### Check for authentication request status
         else if (TRUE == isAuthenticationRequestEvent)
         {
            pAuthorisation_SM->newValue = SM_Authorisation_NotAuthorised_Idle;
         }
         //! #### Check for deauthentication request or deauthentication conditions
         else if (TRUE == isDeauthenticationEvent)
         {
            pAuthorisation_SM->newValue = SM_Authorisation_NotAuthorised_Idle;
         }
         else
         {
            //Do nothing: ignore value keep previous state
         }
      break;
      //! ##### Check for state change from 'Authorised' to 'NotAuthorised_Idle'
      case SM_Authorisation_Authorised:
         //! #### Check for authentication request status  
         if (TRUE == isAuthenticationRequestEvent)
         {
            pAuthorisation_SM->newValue = SM_Authorisation_NotAuthorised_Idle;
         }
         //! #### Check for deauthentication request or deauthentication conditions 
         else if (TRUE == isDeauthenticationEvent)
         {
            pAuthorisation_SM->newValue = SM_Authorisation_NotAuthorised_Idle;
         }
         else
         {
            //Do nothing: ignore value keep previous state
         }
      break;
      //! ##### Check for state change from 'TemporaryAuthorised' to 'NotAuthorised Fail' or 'NotAuthorised_Idle'
      case SM_Authorisation_TemporaryAuthorised:
         //! #### Check for PIN code mode status 
         if (SM_PinCodeInactive == PinCode_SM_Auth)
         {
            pAuthorisation_SM->newValue = SM_Authorisation_NotAuthorised_Fail;
         }
         //! #### Check for authentication request status
         else if (TRUE == isAuthenticationRequestEvent)
         {
            pAuthorisation_SM->newValue = SM_Authorisation_NotAuthorised_Idle;
         }
         //! #### Check for deauthentication request or deauthentication conditions 
         else if (TRUE == isDeauthenticationEvent)
         {
            pAuthorisation_SM->newValue = SM_Authorisation_NotAuthorised_Idle;
         }
         else
         {
            // Do nothing: ignore value keep previous state
         }
      break;
      //! ##### Check for state change from 'NotAuthorised_Idle' to 'TemporaryAuthorised','NotAuthorised Fail' or 'Authorised'
      default: // SM_Authorisation_NotAuthorised_Idle
         //! #### Check for PIN code mode value
         if (SM_PinCodeUnlocked == PinCode_SM_Auth)
         {
            pAuthorisation_SM->newValue = SM_Authorisation_TemporaryAuthorised;
         }
         //! #### Check for key authentication successful event status
         else if (EVENT_KeyAuthenticationSuccessful == KeyAuthenticationEvent)
         {
            pAuthorisation_SM->newValue = SM_Authorisation_Authorised;
         }
         //! #### Check for key authentication fail event status
         else if (EVENT_KeyAuthenticationFail == KeyAuthenticationEvent)
         {
            pAuthorisation_SM->newValue = SM_Authorisation_NotAuthorised_Fail;
         }
         else
         {
            // Do nothing: remains in same state 
         }
      break;
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the AuthStateMachine_OutputsProcessing
//!
//! \param   *pAuthorisation_SM_State  Providing the current state of authorisation state machine
//! \param   *pRteOutData_Common       Updating the output signals based on the current state
//!
//!======================================================================================
static void AuthStateMachine_OutputsProcessing(Authorisation_States                *pAuthorisation_SM_State,
                                               DriverAuthentication2Ctrl_OutData_T *pRteOutData_Common)
{
   //! ###### Processing output actions for current state 
   if (pAuthorisation_SM_State->currentValue != pAuthorisation_SM_State->newValue)
   {
      switch (pAuthorisation_SM_State->newValue)
      {
         //! ##### Output actions for 'NotAuthorised_Fail' state
         case SM_Authorisation_NotAuthorised_Fail:
            pRteOutData_Common->KeyNotValid             = KeyNotValid_KeyNotValid;
            pRteOutData_Common->KeyAuthentication_stat  = KeyAuthentication_stat_decrypt_KeyNotAuthenticated;
            pRteOutData_Common->KeyAuth_stat_serialized = KeyfobAuth_stat_NokeyfobAuthenticated;
            pRteOutData_Common->PinCode_rqst            = PinCode_rqst_PinCodeNeeded;
         break;
         //! ##### Output actions for 'Authorised' state
         case SM_Authorisation_Authorised:
            pRteOutData_Common->KeyNotValid             = KeyNotValid_Idle;
            pRteOutData_Common->KeyAuthentication_stat  = KeyAuthentication_stat_decrypt_KeyAuthenticated;
            pRteOutData_Common->KeyAuth_stat_serialized = KeyfobAuth_stat_KeyfobAuthenticated;
            pRteOutData_Common->PinCode_rqst            = PinCode_rqst_PINCodeNotNeeded;
         break;
         //! ##### Output actions for 'TemporaryAuthorised' state
         case SM_Authorisation_TemporaryAuthorised:
            pRteOutData_Common->KeyNotValid             = KeyNotValid_KeyNotValid;
            pRteOutData_Common->KeyAuthentication_stat  = KeyAuthentication_stat_decrypt_KeyAuthenticated;
            pRteOutData_Common->KeyAuth_stat_serialized = KeyfobAuth_stat_KeyfobAuthenticated;
            pRteOutData_Common->PinCode_rqst            = PinCode_rqst_PinCodeNeeded;
         break;
         //! ##### Output actions for 'NotAuthorised_Idle' state
         default:
            pRteOutData_Common->KeyNotValid             = KeyNotValid_Idle;
            pRteOutData_Common->KeyAuthentication_stat  = KeyAuthentication_stat_decrypt_KeyNotAuthenticated;
            pRteOutData_Common->KeyAuth_stat_serialized = KeyfobAuth_stat_NokeyfobAuthenticated;
            pRteOutData_Common->PinCode_rqst            = PinCode_rqst_Idle;
         break;
      }
   }
   else
   {
      // Do nothing: keep previous status
   }
   pAuthorisation_SM_State->currentValue = pAuthorisation_SM_State->newValue;
}
//!======================================================================================
//!
//! \brief
//!  This function implements the logic for the GearBoxLockLogic
//!
//! \param   *pGearBoxUnlckAuth_rqst    Providing the input request for gearbox unlock authorization
//! \param   Authorisation_SM_current   Providing the current state of authorisation state machine
//! \param   VIN_Check_Stat             Providing the VIN check status is successful or fail
//! \param   *pGearboxUnlockAuth_stat   Updating the output signal based on the current state
//!
//!======================================================================================
static void GearBoxLockLogic(const GearBoxUnlockAuth_rqst_T         *pGearBoxUnlckAuth_rqst,
                             const AuthorisationState_Enum          Authorisation_SM_current,
                             const VinCheckProcessing_Enum          VIN_Check_Stat,
                                   GearboxUnlockAuth_stat_decrypt_T *pGearboxUnlockAuth_stat)
{
   //! ##### Check for GearBoxUnlckAuth request value
   if (GearBoxUnlockAuth_rqst_GearEngagementRequested == *pGearBoxUnlckAuth_rqst)
   {
      //! #### Check for authorisation state machine status
      //! #### Check for VIN check status
      if (SM_Authorisation_NotAuthorised_Idle == Authorisation_SM_current)
      {
         *pGearboxUnlockAuth_stat = GearboxUnlockAuth_stat_decrypt_Idle;
      }
      else if (((SM_Authorisation_Authorised == Authorisation_SM_current)
              || (SM_Authorisation_TemporaryAuthorised == Authorisation_SM_current))
              && (SM_VINCheckSuccessful == VIN_Check_Stat))
      {
         *pGearboxUnlockAuth_stat = GearboxUnlockAuth_stat_decrypt_GearEngagementAllowed;
      }
      else if ((SM_Authorisation_NotAuthorised_Fail == Authorisation_SM_current)
              || (SM_VINCheckFail == VIN_Check_Stat))
      {
         *pGearboxUnlockAuth_stat = GearboxUnlockAuth_stat_decrypt_GearEngagementRefused;
      }
      else
      {
         *pGearboxUnlockAuth_stat = GearboxUnlockAuth_stat_decrypt_Idle;
      }
   }
   else
   {
      *pGearboxUnlockAuth_stat = GearboxUnlockAuth_stat_decrypt_Idle;
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the CrankingAuthorisationLogic
//!
//! \param   *pEngineStart_rqst         Providing the status of the truck is requested or not
//! \param   Authorisation_SM_current   Providing the current state of authorisation state machine
//! \param   VIN_Check_Value            Providing the VIN check status is successful or fail
//! \param   *pEngineStartAuth_stat     Updating the output signal based on the current state
//!
//!======================================================================================
static void CrankingAuthorisationLogic(const EngineStartAuth_rqst_T         *pEngineStart_rqst,
                                       const AuthorisationState_Enum        Authorisation_SM_current,
                                       const VinCheckProcessing_Enum        VIN_Check_Value,
                                             EngineStartAuth_stat_decrypt_T *pEngineStartAuth_stat)
{
   //! ##### Check for EngineStartAuth_rqst status
   if (EngineStartAuth_rqst_StartingOfTheTruckRequested == *pEngineStart_rqst)
   {
      //! #### Check for authorisation state machine status
      //! #### Check for VIN check status
      if (SM_Authorisation_NotAuthorised_Idle == Authorisation_SM_current)
      {
         *pEngineStartAuth_stat = EngineStartAuth_stat_decrypt_Idle;
      }
      else if (((SM_Authorisation_Authorised == Authorisation_SM_current)
              || (SM_Authorisation_TemporaryAuthorised == Authorisation_SM_current)) 
              && (SM_VINCheckSuccessful == VIN_Check_Value))
      {
         *pEngineStartAuth_stat = EngineStartAuth_stat_decrypt_CrankingIsAuthorized;
      }
      else if ((SM_Authorisation_NotAuthorised_Fail == Authorisation_SM_current) 
              || (SM_VINCheckFail == VIN_Check_Value))
      {
         *pEngineStartAuth_stat = EngineStartAuth_stat_decrypt_CrankingIsProhibited;
      }
      else
      {
         *pEngineStartAuth_stat = EngineStartAuth_stat_decrypt_Idle;
      }
   }
   else
   {
      *pEngineStartAuth_stat = EngineStartAuth_stat_decrypt_Idle;
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ReportDriverAuthenticationStatus
//!
//! \param   *pDeviceAuthenticate_request   Providing device authentication request value
//! \param   Authorisation_SM_current       Providing the current state of authorisation state machine
//! \param   *pDeviceInCab_stat             Updating the output signal based on the current state
//!
//!======================================================================================
static void ReportDriverAuthenticationStatus(const DeviceAuthentication_request   *pDeviceAuthenticate_request,
                                             const AuthorisationState_Enum        Authorisation_SM_current,
                                                   DeviceInCab_stat_T             *pDeviceInCab_stat)
{
   //! ##### Check for device authentication request status
   if (DeviceAuthentication_rqst_Idle == pDeviceAuthenticate_request->Current)
   {
      *pDeviceInCab_stat = DeviceInCab_stat_Idle;
   }
   else if (DeviceAuthentication_rqst_DeviceAuthenticationRequest == pDeviceAuthenticate_request->Current)
   {
      //! #### Check for authorisation state machine status
      if (SM_Authorisation_NotAuthorised_Fail == Authorisation_SM_current)
      {
         *pDeviceInCab_stat = DeviceInCab_stat_DeviceNotAuthenticated; 
      }
      else if ((SM_Authorisation_Authorised == Authorisation_SM_current)
              || (SM_Authorisation_TemporaryAuthorised == Authorisation_SM_current))
      {
         *pDeviceInCab_stat = DeviceInCab_stat_DeviceAuthenticated;
      }
      else
      {
         *pDeviceInCab_stat = DeviceInCab_stat_Idle;
      }
   }
   else
   {
      *pDeviceInCab_stat = DeviceInCab_stat_Idle;
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the AuthenticationOfDriverByKeyfobRqtTrigger
//!
//! \param   *pDeviceAuthentication_rqst   Providing device authentication request value
//! \param   *pPassiveRequestTimer         To check current timer value and update
//! \param   *pKeyfobAuth_rqst             Updating the output port based on conditions
//!
//!======================================================================================
static boolean AuthenticationOfDriverByKeyfobRqtTrigger(const DeviceAuthentication_request *pDeviceAuthentication_rqst,
                                                              uint16                       *pPassiveRequestTimer,
                                                              KeyfobAuth_rqst_T            *pKeyfobAuth_rqst)
{
   boolean isAuthenticationRequestEvent = FALSE;
   if (CONST_TimerFunctionElapsed == *pPassiveRequestTimer)
   {
      *pPassiveRequestTimer = CONST_TimerFunctionInactive;
   }
   else
   {
      //Do Nothing:keep previous value
   }
   //! ##### Check for device authentication request status
   if ((DeviceAuthentication_rqst_DeviceAuthenticationRequest != pDeviceAuthentication_rqst->Previous)
      && (DeviceAuthentication_rqst_DeviceAuthenticationRequest == pDeviceAuthentication_rqst->Current))
   {
      isAuthenticationRequestEvent = TRUE;
      *pKeyfobAuth_rqst            = KeyfobAuth_rqst_RequestByPassiveMechanism;
      // Start timer 
      *pPassiveRequestTimer        = CONST_KeyfobAuthRqst_TimePeriod;
   }
   //! ##### Check for the timer value
   else if (CONST_TimerFunctionInactive == *pPassiveRequestTimer)
   {
      *pKeyfobAuth_rqst     = KeyfobAuth_rqst_Idle;
   }
   else
   {
      // Do nothing: keep previous state until the timer is elapsed 
   }
   return isAuthenticationRequestEvent;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the VIN_Check
//!
//! \param   ECUsVIN_status         Providing the input values of ECUsVIN_stat structure
//! \param   *pVehicleMode_Status   Provides the status of vehicle mode
//! \param   *pVINCheckActivation   Provides the input status
//! \param   *pTimers_VIN_Check     To check current timer value and update
//! \param   *pVIN_rqst             Updating value based on vehicle mode and VIN check status
//! \param   *pSM_VinCheck          Updating the state based on vehicle mode and VIN check status
//!
//!======================================================================================
static void VIN_Check(const ECUsVIN_stat_Type                    ECUsVIN_status,
                      const VehicleMode_Internal                 *pVehicleMode_Status,
                      const SEWS_VINCheckProcessing_P1VKG_s_T    *pVINCheckActivation,
                            uint16                               *pTimers_VIN_Check,
                            VIN_rqst_T                           *pVIN_rqst,
                            VinCheckProcessing_Enum              *pSM_VinCheck,
							VINCheckStatus_T                     *pVINCheckStatus)
{
   
    FormalBoolean isVinCheck_rtrn = NO;

   //! ###### Check for vehicle mode status
   if (pVehicleMode_Status->Current != pVehicleMode_Status->Previous)
   {
      if ((VehicleMode_Running != pVehicleMode_Status->Previous)
         && ((VehicleMode_Cranking == pVehicleMode_Status->Current)
         || (VehicleMode_PreRunning == pVehicleMode_Status->Current)))
      {
         *pSM_VinCheck      = SM_VINCheckPending;
         *pTimers_VIN_Check = CONST_VIN_CheckTimeout;
         *pVIN_rqst         = TRUE;
      }
      else if ((VehicleMode_PreRunning != pVehicleMode_Status->Current) 
              && (VehicleMode_Cranking != pVehicleMode_Status->Current) 
              && (VehicleMode_Running != pVehicleMode_Status->Current))
      {
         *pSM_VinCheck = SM_VINCheckIdle;
      }
      else
      {
         // Do nothing: keep previous status
      }
   }
   else
   {
      // Do nothing: keep previous status
   }
   //! ###### Check for VIN check status
   if (SM_VINCheckPending == *pSM_VinCheck)
   {
      if (CONST_TimerFunctionElapsed == *pTimers_VIN_Check)
      {
         *pSM_VinCheck = SM_VINCheckFail;
      }
      else
      {
         //! ##### Process the 'VIN check by ECU' logic
         //! #### Check for display check active result and ECU chassis id
         isVinCheck_rtrn = isVinEqualCheck(pVINCheckActivation->DISPLAY_Check_Active,
                                           ECUsVIN_status.ECU3VIN_stat);
         if (YES == isVinCheck_rtrn)
         {
            pVINCheckStatus->isDISPLAY_CheckPassed = (boolean)YES;
         }
         else
         {
            // VIN check status is pending or fail
         }
         //! #### Check for APM check active result and ECU chassis id
         isVinCheck_rtrn = isVinEqualCheck(pVINCheckActivation->APM_Check_Active,
                                           ECUsVIN_status.ECU2VIN_stat);
         if (YES == isVinCheck_rtrn)
         {
            pVINCheckStatus->isAPM_CheckPassed = (boolean)YES;
         }
         else
         {
            // VIN check status is pending or fail
         }
         //! #### Check for VMCU check active result and ECU chassis id
         isVinCheck_rtrn = isVinEqualCheck(pVINCheckActivation->MVUC_Check_Active,
                                           ECUsVIN_status.ECU1VIN_stat);
         if (YES == isVinCheck_rtrn)
         {
            pVINCheckStatus->isVMCU_CheckPassed = (boolean)YES;
         }
         else
         {
            // VIN check status is pending or fail
         }
         //! #### Check for EMS check active result and ECU chassis id
         isVinCheck_rtrn = isVinEqualCheck(pVINCheckActivation->EMS_Check_Active,
                                           ECUsVIN_status.ECU4VIN_stat);
         if (YES == isVinCheck_rtrn)
         {
            pVINCheckStatus->isEMS_CheckPassed = (boolean)YES;
         }
         else
         {
            // VIN check status is pending or fail
         }
         //! #### Check for TECU check active result and ECU chassis id
         isVinCheck_rtrn = isVinEqualCheck(pVINCheckActivation->TECU_Check_Active,
                                           ECUsVIN_status.ECU5VIN_stat);
         if (YES == isVinCheck_rtrn)
         {
            pVINCheckStatus->isTECU_CheckPassed = (boolean)YES;
         }
         else
         {
            // VIN check status is pending or fail
         }
         //! #### Check for VIN check status
         if (((boolean)YES == pVINCheckStatus->isDISPLAY_CheckPassed) 
            && ((boolean)YES == pVINCheckStatus->isAPM_CheckPassed)
            && ((boolean)YES == pVINCheckStatus->isVMCU_CheckPassed)
            && ((boolean)YES == pVINCheckStatus->isEMS_CheckPassed)
            && ((boolean)YES == pVINCheckStatus->isTECU_CheckPassed))
         {
            *pSM_VinCheck      = SM_VINCheckSuccessful;
            *pTimers_VIN_Check = CONST_TimerFunctionInactive;
            *pVIN_rqst         = FALSE;
         }
         else
         {
            // Do nothing: keep previous status
         }
      }
   }
   else
   {
      // Do nothing: keep previous status
   }
   //! ###### Check for 'VIN check by ECU' logic status
   if ((SM_VINCheckFail == *pSM_VinCheck) 
      || (SM_VINCheckIdle == *pSM_VinCheck))
   {
      pVINCheckStatus->isDISPLAY_CheckPassed = (boolean)NO;
      pVINCheckStatus->isAPM_CheckPassed     = (boolean)NO;
      pVINCheckStatus->isVMCU_CheckPassed    = (boolean)NO;
      pVINCheckStatus->isEMS_CheckPassed     = (boolean)NO;
      pVINCheckStatus->isTECU_CheckPassed    = (boolean)NO;
      *pTimers_VIN_Check                   = CONST_TimerFunctionInactive;
      *pVIN_rqst                           = FALSE;
   }
   else
   {
      // Do nothing: keep previous status
   }
   Rte_IrvWrite_DriverAuthentication2_Ctrl_20ms_runnable_Vincheckstatus(pVINCheckStatus);
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the isVinEqualCheck
//!
//! \param   is_CheckActive   Providing the current value is YES or NO
//! \param   EcuVIN           Providing the input status of ECUsVIN_status
//!
//! \return   boolean         Returns 'isVinEqual' status
//!
//!======================================================================================
static FormalBoolean isVinEqualCheck(const SEWS_P1VKG_APM_Check_Active_T    is_CheckActive,
                                     const VIN_stat_T                       EcuVIN)
{
   uint8         i          = 0U;
   FormalBoolean isVinEqual = YES;
   
   const uint8 *ChassisID = &PCODE_CHANO_ChassisId;

   //! ##### Check for 'is_CheckActive' status
   if (YES == is_CheckActive)
   {
      for (i = 0U; i < VIN_size; i++)
      {
         //! #### Check for 'EcuVIN' status
         if (ChassisID[i] != EcuVIN[i])
         {
            isVinEqual = NO;
         }
         else 
         {
            // Do nothing: keep previous status
         }
      }
   }
   else
   {
      isVinEqual = YES;
   }
   return isVinEqual;
}
//!=======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_ImmobilizerPINCodeActivationCondition
//!
//! \param    *pKeyNotValid       Specifies the current event status
//!
//! \return   FormalBoolean       Returns 'yes' or 'no' values
//!
//!======================================================================================
static FormalBoolean ANW_ImmobilizerPINCodeActivationCondition(const KeyNotValid_T  *pKeyNotValid)
{
   //! ###### Processing ANW_ImmobilizerPINCode activation trigger logic
   FormalBoolean isActTriggerDetected = NO;
   static KeyNotValid_T KeyNotValidprevious;

   //! ##### Select 'pKeyNotValid'
   if ((KeyNotValidprevious != *pKeyNotValid)
      && (KeyNotValid_KeyNotValid == *pKeyNotValid))
   {
      isActTriggerDetected = YES;
   }
   else
   {
      // Do nothing: keep previous value
   }
   KeyNotValidprevious = *pKeyNotValid;

   return isActTriggerDetected;
}

//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_ImmobilizerPINCode
//!
//! \param   *pisActTriggerDetected     Provides 'yes' or 'no' values based on activation trigger
//! \param   *pisDeActTriggerDetected   Provides 'yes' or 'no' values based on deactivation trigger
//!
//!======================================================================================
static void ANW_ImmobilizerPINCode(const FormalBoolean  *pisActTriggerDetected,
                                   const FormalBoolean  *pisDeActTriggerDetected)
{
   static DeactivationTimer_Type Immobilizer_Deactivationtimer   = 0U;
   static AnwSM_States           ANW_SM_ECSActDeactivation;
          FormalBoolean          isRestAct                       = NO;   
          AnwAction_Enum         Anw_Action                      = ANW_Action_None;

   //! ###### Process ANW logic: 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(isRestAct,
                                *pisActTriggerDetected,
                                *pisDeActTriggerDetected,
                                CONST_ANW_ImmobilizerTimeout,
                                &Immobilizer_Deactivationtimer,
                                &ANW_SM_ECSActDeactivation);
   //! ###### Check for Anw action is 'activate' or 'deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_ImmobilizerPINCode_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      (void)Rte_Call_UR_ANW_ImmobilizerPINCode_DeactivateIss();
   }
   else
   {
      // Do nothing: keep previous state
   }
}

//! @}
//! @}
//! @}
//! @}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
