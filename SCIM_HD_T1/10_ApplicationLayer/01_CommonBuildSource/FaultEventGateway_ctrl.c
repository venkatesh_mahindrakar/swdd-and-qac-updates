/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  FaultEventGateway_ctrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  FaultEventGateway_ctrl
 *  Generated at:  Mon Jun 15 11:00:05 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <FaultEventGateway_ctrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
//!======================================================================================
//! \file FaultEventGateway_Ctrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup Platform_SCIM
//! @{
//! @addtogroup FaultEventGateway_Ctrl
//! @{
//! @addtogroup FaultEventGateway_Ctrl
//! @{
//!
//! \brief
//! FaultEventGateway_Ctrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the FaultEventGateway_Ctrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * BswM_BswMRteMDG_NvmWriteAllRequest
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_DTCFormatType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_UdsStatusByteType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DwmVehicleModes_P1BDU_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_FaultEventGateway_ctrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "FuncLibrary_Timer_If.h"
#include "FuncLibrary_ScimStd_If.h"
#include "FaultEventGateway_ctrl.h"

//!##### Definition of global data
static       FaultEventGateway_RegDTC_Type ActiveDTCs;
static       FaultEventGateway_RegDTC_Type InActiveDTCs;
static       uint8                         IrvNvmWriteAllReqFlag                  = NVM_WRITEALLREQ_FLAG_IDLE;
static       uint8                         IrvImmediateNvmWriteReqFlag            = NVM_WRITEALLREQ_FLAG_IDLE;
static const uint32                        CriticalDTCList[MAX_NO_OF_CRITICAL_DTC]={Critical_DTC_ID_None,   /*Critical_DTC_ID_01*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_02*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_03*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_04*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_05*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_06*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_07*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_08*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_09*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_10*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_11*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_12*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_13*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_14*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_15*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_16*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_17*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_18*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_19*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_20*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_21*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_22*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_23*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_24*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_25*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_26*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_27*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_28*/
                                                                                    Critical_DTC_ID_None,   /*Critical_DTC_ID_29*/
                                                                                    Critical_DTC_ID_None};  /*Critical_DTC_ID_30*/
//! ##### Definition of global Macro
#define PCODE_P1BDU_DwmVehicleModes_v (Rte_Prm_P1BDU_DwmVehicleModes_v())

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DtcIdA_T: Integer in interval [0...65535]
 *   Factor: 1, Offset: 0
 * DtcIdB_T: Integer in interval [0...65535]
 *   Factor: 1, Offset: 0
 * EcuAdr_T: Integer in interval [0...255]
 *   Factor: 1, Offset: 0
 * FailTA_T: Integer in interval [0...255]
 *   Factor: 1, Offset: 0
 * FailTB_T: Integer in interval [0...255]
 *   Factor: 1, Offset: 0
 * SEWS_DwmVehicleModes_P1BDU_T: Integer in interval [0...255]
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BswM_BswMRteMDG_NvmWriteAllRequest: Enumeration of integer in interval [0...255] with enumerators
 *   NvmWriteAll_NoRequest (0U)
 *   NvmWriteAll_Request (1U)
 * Dem_DTCFormatType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_DTC_FORMAT_OBD (0U)
 *   DEM_DTC_FORMAT_UDS (1U)
 *   DEM_DTC_FORMAT_J1939 (2U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * Dem_UdsStatusByteType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_UDS_STATUS_TF (1U)
 *   DEM_UDS_STATUS_TF_BflMask 1U (0b00000001)
 *   DEM_UDS_STATUS_TF_BflPn 0
 *   DEM_UDS_STATUS_TF_BflLn 1
 *   DEM_UDS_STATUS_TFTOC (2U)
 *   DEM_UDS_STATUS_TFTOC_BflMask 2U (0b00000010)
 *   DEM_UDS_STATUS_TFTOC_BflPn 1
 *   DEM_UDS_STATUS_TFTOC_BflLn 1
 *   DEM_UDS_STATUS_PDTC (4U)
 *   DEM_UDS_STATUS_PDTC_BflMask 4U (0b00000100)
 *   DEM_UDS_STATUS_PDTC_BflPn 2
 *   DEM_UDS_STATUS_PDTC_BflLn 1
 *   DEM_UDS_STATUS_CDTC (8U)
 *   DEM_UDS_STATUS_CDTC_BflMask 8U (0b00001000)
 *   DEM_UDS_STATUS_CDTC_BflPn 3
 *   DEM_UDS_STATUS_CDTC_BflLn 1
 *   DEM_UDS_STATUS_TNCSLC (16U)
 *   DEM_UDS_STATUS_TNCSLC_BflMask 16U (0b00010000)
 *   DEM_UDS_STATUS_TNCSLC_BflPn 4
 *   DEM_UDS_STATUS_TNCSLC_BflLn 1
 *   DEM_UDS_STATUS_TFSLC (32U)
 *   DEM_UDS_STATUS_TFSLC_BflMask 32U (0b00100000)
 *   DEM_UDS_STATUS_TFSLC_BflPn 5
 *   DEM_UDS_STATUS_TFSLC_BflLn 1
 *   DEM_UDS_STATUS_TNCTOC (64U)
 *   DEM_UDS_STATUS_TNCTOC_BflMask 64U (0b01000000)
 *   DEM_UDS_STATUS_TNCTOC_BflPn 6
 *   DEM_UDS_STATUS_TNCTOC_BflLn 1
 *   DEM_UDS_STATUS_WIR (128U)
 *   DEM_UDS_STATUS_WIR_BflMask 128U (0b10000000)
 *   DEM_UDS_STATUS_WIR_BflPn 7
 *   DEM_UDS_STATUS_WIR_BflLn 1
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *
 * Record Types:
 * =============
 * DiagFaultStat_T: Record with elements
 *   EcuAdr_RE of type EcuAdr_T
 *   DtcIdA_RE of type DtcIdA_T
 *   FailTA_RE of type FailTA_T
 *   DtcIdB_RE of type DtcIdB_T
 *   FailTB_RE of type FailTB_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_DwmVehicleModes_P1BDU_T Rte_Prm_P1BDU_DwmVehicleModes_v(void)
 *
 *********************************************************************************************************************/


#define FaultEventGateway_ctrl_START_SEC_CODE
#include "FaultEventGateway_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: FaultEventGateway_ctrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DiagFaultStat_DiagFaultStat(const DiagFaultStat_T *data)
 *   Std_ReturnType Rte_Write_ModeRequest_NvmWriteAllRequest_requestedMode(BswM_BswMRteMDG_NvmWriteAllRequest data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_DemServices_SynchronizeNvData(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DemServices_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: FaultEventGateway_ctrl_20ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the 'FaultEventGateway_ctrl_20ms_runnable'
//! 
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, FaultEventGateway_ctrl_CODE) FaultEventGateway_ctrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: FaultEventGateway_ctrl_20ms_runnable
 *********************************************************************************************************************/
   //! ####  Internal Data
   static uint8           CurrentIndex             = 0U;
   static uint16          Timers[CONST_NbOfTimers];
   static DiagFaultStat_T DiagFaultStat;   
          uint8           retValue                 = RTE_E_INVALID;
          uint8           TriggerRteWrite          = 0U;  
          VehicleMode_T   VehicleModeInternal;  

   //! ##### Reading Vehicle mode data 
   retValue = Rte_Read_VehicleModeInternal_VehicleMode(&VehicleModeInternal);
   MACRO_StdRteRead_IntRPort(retValue,VehicleModeInternal,VehicleMode_NotAvailable)   
   //! ##### process the TimerFunction_Tick to decrementing timer ticks  : 'TimerFunction_Tick()' 
   TimerFunction_Tick(CONST_NbOfTimers,
                      Timers);
   //! ##### Giving ecu ID to the diagnostic fault state structure
   DiagFaultStat.EcuAdr_RE = SCIM_ECU_ID;
   //! ##### Checking for DWM vehicle mode and if this is true then configuring timers
   if (((1U == PCODE_P1BDU_DwmVehicleModes_v)
      && (VehicleMode_Running == VehicleModeInternal))
      || ((2U == PCODE_P1BDU_DwmVehicleModes_v)
      && (VehicleMode_PreRunning == VehicleModeInternal))
      || ((3U == PCODE_P1BDU_DwmVehicleModes_v)
      && ((VehicleMode_Running == VehicleModeInternal)
      || (VehicleMode_PreRunning == VehicleModeInternal)))
      || (255U == PCODE_P1BDU_DwmVehicleModes_v))
   {
      //! ##### Checking if the software timer 0 is elapsed or inactive if this is true then if number of dtcs are less than 20 then configuring timers for 1000ms else 100ms
      if ((CONST_TimerFunctionElapsed  == Timers[0U])
         || (CONST_TimerFunctionInactive == Timers[0U]))   
      {
         TriggerRteWrite = 1U;
         //! ##### Condition to check the activation of DTC
         if(ActiveDTCs.NoOfDTCs == 0U)
         {            
            Timers[0U] = CONST_Timer_1000ms;
            DiagFaultStat.DtcIdA_RE = 0x00;
            DiagFaultStat.FailTA_RE = 0x00;
            DiagFaultStat.DtcIdB_RE = 0x00;
            DiagFaultStat.FailTB_RE = 0x00;
         }
         else 
         {
            //if the no of dtc are less than 20 then the timer is configured with 1000ms time i.e. Diag info is sent every 1000ms
            if (ActiveDTCs.NoOfDTCs <= NO_OF_DTC_TIMER_LIMIT)
            {
               Timers[0U] = CONST_Timer_1000ms;
            }
            else
            {
               Timers[0U] = CONST_Timer_100ms;
            }  
            if((ActiveDTCs.NoOfDTCs-CurrentIndex)>1)
            {
               if (CurrentIndex < MAX_NO_OF_REGISTER_DTC)
               {
                  //masking data for fault type and Dtc id 
                  DiagFaultStat.DtcIdA_RE = ((DtcIdA_T)((ActiveDTCs.DTC_ID[CurrentIndex] & DTC_DtcID_MASK)>>DTC_DtcID_Shift));
                  DiagFaultStat.FailTA_RE = ((FailTA_T)(ActiveDTCs.DTC_ID[CurrentIndex] & DTC_FAILID_MASK));
                  //if number of DTC are less than current index then increment current index, this is for cyclic execution 
                  if (CurrentIndex < (ActiveDTCs.NoOfDTCs-1U))
                  {
                     CurrentIndex++;      
                  }
                  else
                  {
                     CurrentIndex = 0U;
                  }
                  //masking 2nd DTC fault and DTC id
                  DiagFaultStat.DtcIdB_RE = ((DtcIdA_T)((ActiveDTCs.DTC_ID[CurrentIndex] & DTC_DtcID_MASK)>>DTC_DtcID_Shift));
                  DiagFaultStat.FailTB_RE = ((FailTA_T)(ActiveDTCs.DTC_ID[CurrentIndex] & DTC_FAILID_MASK));
                  if (CurrentIndex < (ActiveDTCs.NoOfDTCs-1U))
                  {
                     CurrentIndex++;      
                  }
                  else
                  {
                     CurrentIndex = 0U;
                  }
               }
            }
            else
            {
               if (CurrentIndex < MAX_NO_OF_REGISTER_DTC)
               {
                  DiagFaultStat.DtcIdA_RE = ((DtcIdA_T)((ActiveDTCs.DTC_ID[CurrentIndex] & DTC_DtcID_MASK)>>DTC_DtcID_Shift));
                  DiagFaultStat.FailTA_RE = ((FailTA_T)(ActiveDTCs.DTC_ID[CurrentIndex] & DTC_FAILID_MASK));
                  DiagFaultStat.DtcIdB_RE = 0U;
                  DiagFaultStat.FailTB_RE = 0U;
                  CurrentIndex            = 0U;
               }
               else
               {
                   // Do nothing
               }
            } 
         }
      }
      else
      {
         // Do nothing
      }
   }
   else
   {      
      //! #### Checking timer elapsed or inactive
      if ((CONST_TimerFunctionElapsed  == Timers[0U])
         ||(CONST_TimerFunctionInactive == Timers[0U]))   
      {
         TriggerRteWrite         = 1U;         
         Timers[0U]              = CONST_Timer_1000ms;         
         DiagFaultStat.DtcIdA_RE = 0x00;
         DiagFaultStat.FailTA_RE = 0x00;
         DiagFaultStat.DtcIdB_RE = 0x00;
         DiagFaultStat.FailTB_RE = 0x00;
      }
      else
      {
         //Do nothing
      }
   }  
   //! ##### Process the output with Trigger and Diag fault status 
   FEG_Ctrl_OutputProcessing(TriggerRteWrite,
                             &DiagFaultStat);      

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: FaultEventGateway_ctrl_DTCStatusChangedNotification
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <DTCStatusChanged> of PortPrototype <CBStatusDTC_DemCallbackDTCStatusChanged>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BR9_68_Notification_LimitReached_GetDTCOfEvent(Dem_DTCFormatType DTCFormat, uint32 *DTCOfEvent)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_DEM_E_NO_DTC_AVAILABLE, RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BR9_68_Notification_LimitReached_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BR9_68_Notification_LimitReached_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType FaultEventGateway_ctrl_DTCStatusChangedNotification(uint32 DTC, Dem_UdsStatusByteType DTCStatusOld, Dem_UdsStatusByteType DTCStatusNew)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CallbackDTCStatusChange_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: FaultEventGateway_ctrl_DTCStatusChangedNotification_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the DTC status change Notification runnable logic for the 'FaultEventGateway_ctrl_DTCStatusChangedNotification' 
//! 
//! \param  DTC              Current DTC ID
//! \param  DTCStatusOld     Previously active DTC
//! \param  DTCStatusNew     Current active DTC
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FaultEventGateway_ctrl_CODE) FaultEventGateway_ctrl_DTCStatusChangedNotification(uint32 DTC, Dem_UdsStatusByteType DTCStatusOld, Dem_UdsStatusByteType DTCStatusNew) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: FaultEventGateway_ctrl_DTCStatusChangedNotification (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for FaultEventGateway_ctrl_DTCStatusChangedNotification
   boolean EventFailed            = 0U;
   uint32  LimitReachedEventDtcId = 0U;
   uint8   Index                  = 0u;

   //! ##### Pushing the FEW data into the buffer if the old dtc and new dtc are different
   if ((DTCStatusNew & FAULT_DTC_MASK_BYTE) !=  (DTCStatusOld & FAULT_DTC_MASK_BYTE))
   {
     //! #### NvmWriteAll Operation Control (For Fault Data Storage) 
      if (IrvNvmWriteAllReqFlag == NVM_WRITEALLREQ_FLAG_IDLE)
      {
         IrvNvmWriteAllReqFlag = NVM_WRITEALLREQ_FLAG_TRIGGERED;
      }
      else
      {
         //Do nothing
      }
      //! #### Immediate NvmWrite Operation Control (For Critical Fault Data Storage) 
      if (IrvImmediateNvmWriteReqFlag == NVM_WRITEALLREQ_FLAG_IDLE)
      {
         // Search if DTC is Critical DTC or not
         for(Index=0;Index<MAX_NO_OF_CRITICAL_DTC;Index++)
         {
            if ((DTC == CriticalDTCList[Index])
               &&(Critical_DTC_ID_None != CriticalDTCList[Index]))
            {
               IrvImmediateNvmWriteReqFlag = NVM_WRITEALLREQ_FLAG_TRIGGERED;
               break;
            }
            else
            {
               //Do nothing
            }
         }
      }
      else
      {
         //Do nothing
      }   
      //! #### If fault is occured and the number of DTC are less than 29 then pushing the dtc in the buffer
      if ((DTCStatusNew & ACTIVE_DTC_MASK_BYTE) == ACTIVE_DTC_MASK_BYTE)
      {
         //! #### Process the SearchAndRemoveDtcElement :'SearchAndRemoveDtcElement()'
         SearchAndRemoveDtcElement(&InActiveDTCs,
                                   DTC);
         if (ActiveDTCs.NoOfDTCs < (MAX_NO_OF_REGISTER_DTC-1U))
         {
            ActiveDTCs.DTC_ID[ActiveDTCs.NoOfDTCs]=DTC;  
            ActiveDTCs.NoOfDTCs++;
         }
         else
         {
            //else giving notification of fault state storing limit reached
            Rte_Call_Event_D1BR9_68_Notification_LimitReached_GetEventFailed(&EventFailed);
            (void)Rte_Call_Event_D1BR9_68_Notification_LimitReached_GetDTCOfEvent(DEM_DTC_FORMAT_UDS,
                                                                                  &LimitReachedEventDtcId);
            //!#### Condition to check Event status as failed:Max buffer limit reached indication is stored and transmitted in last butffer
            if (1U == EventFailed)
            {               
               if ((LimitReachedEventDtcId == DTC)
                  &&(MAX_NO_OF_REGISTER_DTC != ActiveDTCs.NoOfDTCs))   
               {
                  ActiveDTCs.DTC_ID[ActiveDTCs.NoOfDTCs] = DTC;
                  ActiveDTCs.NoOfDTCs                    = MAX_NO_OF_REGISTER_DTC;
               }
               else
               {
                  // Do nothing
               }
            }
            else
            {
               Rte_Call_Event_D1BR9_68_Notification_LimitReached_SetEventStatus(DEM_EVENT_STATUS_FAILED);
            }
         }
      }
      else 
      {          
         //! #### If the DTC status is "passed" and if it was failed previously then serach for DTC Number and remove from buffer 
         //! #### processing SearchAndRemoveDtcElement : 'SearchAndRemoveDtcElement()'
         SearchAndRemoveDtcElement(&ActiveDTCs,
                                   DTC);
         Rte_Call_Event_D1BR9_68_Notification_LimitReached_GetEventFailed(&EventFailed);
         //! #### Condition to check Event status as failed
         if (1U == EventFailed)
         {
            Rte_Call_Event_D1BR9_68_Notification_LimitReached_SetEventStatus(DEM_EVENT_STATUS_PASSED);
         }
         else
         {
            // Do nothing
         }          
      }
      //! #### If INACTIVE_DTC_MASK_BYTE is masked 
      if ((DTCStatusNew & FAULT_DTC_MASK_BYTE) == INACTIVE_DTC_MASK_BYTE)
      {
         // InActive DTC List
         if (InActiveDTCs.NoOfDTCs < MAX_NO_OF_REGISTER_DTC)
         {
            InActiveDTCs.DTC_ID[InActiveDTCs.NoOfDTCs] = DTC;
            InActiveDTCs.NoOfDTCs++;
         }
         else
         {
            // Do nothing
         }
      }
      else if ((DTCStatusNew & FAULT_DTC_MASK_BYTE) == 0x00U)
      {
         SearchAndRemoveDtcElement(&InActiveDTCs,
                                    DTC);
      }
      else
      {
         //Do nothing
      }     
   }
   else
   {
      // Do nothing
   }
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: FaultEventGateway_ctrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BR9_68_Notification_LimitReached_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: FaultEventGateway_ctrl_Init_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the Initialization runnable logic for the FaultEventGateway_ctrl_Init
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, FaultEventGateway_ctrl_CODE) FaultEventGateway_ctrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: FaultEventGateway_ctrl_Init
 *********************************************************************************************************************/
   //! ###### Implementation for FaultEventGateway_ctrl Module Initialization
   //! ##### (Re)Initialized the Irv NVM Request variables to IDLE State
   IrvNvmWriteAllReqFlag       = NVM_WRITEALLREQ_FLAG_IDLE;
   IrvImmediateNvmWriteReqFlag = NVM_WRITEALLREQ_FLAG_IDLE;

   //! #### Initialized the LimitReached Event status to Passed: 'Rte_Call_Event_D1BR9_68_Notification_LimitReached_SetEventStatus()'
   Rte_Call_Event_D1BR9_68_Notification_LimitReached_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   //! #### Processing Rte_Write_ModeRequest_NvmWriteAllRequest_requestedMode : 'Rte_Write_ModeRequest_NvmWriteAllRequest_requestedMode()'
   Rte_Write_ModeRequest_NvmWriteAllRequest_requestedMode(NvmWriteAll_NoRequest);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

#define FaultEventGateway_ctrl_STOP_SEC_CODE
#include "FaultEventGateway_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'SearchAndRemoveDtcElement'
//! 
//! \param  *RegDTC  previously registered DTC in DTC array
//! \param  DTC      Current DTC
//! 
//!====================================================================================================================
static void SearchAndRemoveDtcElement(FaultEventGateway_RegDTC_Type *RegDTC,
                                      uint32                        DTC)
{
   //!##### This function implements the logic for SearchAndRemoveDtcElement
   uint8 i        = 0U;
   uint8 found    = 0U;
   uint8 Position = 0U;
   //! ##### checking wether the current DTC is prensent in the buffer or not 
   for (i=0;i < RegDTC->NoOfDTCs;i++)
   {      
      if (RegDTC->DTC_ID[i] == DTC)
      {
         found    = 1U;
         Position = i;
         break;
      }
      else
      {
         //Do nothing
      }
   }
   //! ##### if the current dtc is found 
   if (1U == found)
   {
      //! #### deleting that dtc and shifting the buffer elements to the left
      for (i=Position;i<RegDTC->NoOfDTCs;i++)
      {
         if (i<(MAX_NO_OF_REGISTER_DTC-1U))
         {
            RegDTC->DTC_ID[i] = RegDTC->DTC_ID[i+1];
         }
         else if((MAX_NO_OF_REGISTER_DTC-1) == i)
         {
            RegDTC->DTC_ID[i] = 0U;
         }     
         else
         {
            //Do nothing
         }         
      }
      //! #### freeing the 28th element
      if (RegDTC->NoOfDTCs < MAX_NO_OF_REGISTER_DTC)
      {
         RegDTC->DTC_ID[RegDTC->NoOfDTCs] = 0U;
         RegDTC->NoOfDTCs--;
      }
      else
      {
          // Do nothing
      }
   } 
   else
   {
       // Do nothing
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'FEG_Ctrl_OutputProcessing'
//! 
//! \param  Trigger          Triggers the output
//! \param  *DiagFaultStat   Updates the Diagnostic fault status to RTE port 
//! 
//!====================================================================================================================
static void FEG_Ctrl_OutputProcessing(      uint8           Trigger ,
                                      const DiagFaultStat_T *DiagFaultStat)
{
   //!##### After timer 0 elapsed write the Diag Fault status 
   if (1U == Trigger)
   {
      //! #### Process the Rte_Write_DiagFaultStat_DiagFaultStat: 'Rte_Write_DiagFaultStat_DiagFaultStat()'
      (void)Rte_Write_DiagFaultStat_DiagFaultStat(DiagFaultStat);
   }
   else
   {
      // Do nothing
   }
   //!##### If NVM_WRITEALLREQ_FLAG_TRIGGERED is equal to IrvNvmWriteAllReqFlag
   if (NVM_WRITEALLREQ_FLAG_TRIGGERED == IrvNvmWriteAllReqFlag)
   {
      IrvNvmWriteAllReqFlag = NVM_WRITEALLREQ_FLAG_COMPLETED;
      //!#### Processing Rte_Write_ModeRequest_NvmWriteAllRequest_requestedMode : 'Rte_Write_ModeRequest_NvmWriteAllRequest_requestedMode()'
      Rte_Write_ModeRequest_NvmWriteAllRequest_requestedMode(NvmWriteAll_Request);
   }
   else
   {
      // Do nothing
   }
   //!##### If NVM_WRITEALLREQ_FLAG_TRIGGERED is equal to IrvImmediateNvmWriteReqFlag
   if (NVM_WRITEALLREQ_FLAG_TRIGGERED == IrvImmediateNvmWriteReqFlag)
   {
      IrvImmediateNvmWriteReqFlag = NVM_WRITEALLREQ_FLAG_COMPLETED;
      //!#### Processing Rte_Call_DemServices_SynchronizeNvData : 'Rte_Call_DemServices_SynchronizeNvData()'
      (void)Rte_Call_DemServices_SynchronizeNvData();
   }
   else
   {
      // Do nothing
   }
}
//! @}
//! @}
//! @}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
