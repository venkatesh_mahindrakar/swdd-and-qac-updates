/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  SCIM_PVTPT_IO.c
 *           Config:  C:/Personal/GIT/ExternalGit/MSW/BSP_scim/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  SCIM_PVTPT_IO
 *  Generation Time:  2020-10-20 17:45:21
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <SCIM_PVTPT_IO>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * BswM_BswMRteMDG_PvtReport_X1C14
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * IOHWAB_BOOL
 *   boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * IOHWAB_SINT8
 *   sint8 represents integers with a minimum value of -128 and a maximum value of 127.
 *      The order-relation on sint8 is: x < y if y - x is positive.
 *      sint8 has a lexical representation consisting of an optional sign followed 
 *      by a finite-length sequence of decimal digits (#x30-#x39). If the sign is 
 *      omitted, "+" is assumed. 
 *      
 *      For example: -1, 0, 12678, +10000.
 *
 * IOHWAB_UINT16
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * IOHWAB_UINT8
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_Pvt_ActivateReporting_X1C14_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinFaultStatus
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinVoltage_0V2
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPwmDutycycle
 *   sint8 represents integers with a minimum value of -128 and a maximum value of 127.
 *      The order-relation on sint8 is: x < y if y - x is positive.
 *      sint8 has a lexical representation consisting of an optional sign followed 
 *      by a finite-length sequence of decimal digits (#x30-#x39). If the sign is 
 *      omitted, "+" is assumed. 
 *      
 *      For example: -1, 0, 12678, +10000.
 *
 * VGTT_EcuPwmPeriod
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 *********************************************************************************************************************/

#include "Rte_SCIM_PVTPT_IO.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 

#define DOWxSx_DISABLED 0U
#define DOWxSx_PWM_MODE 1U
#define DOWxSx_IO_MODE  2U



#define PCODE_DOWHS1_MODE DOWxSx_PWM_MODE // Need to replace with Address Parameter Interface
#define PCODE_DOWHS2_MODE DOWxSx_PWM_MODE // Need to replace with Address Parameter Interface
#define PCODE_DOWLS2_MODE DOWxSx_PWM_MODE // Need to replace with Address Parameter Interface
#define PCODE_DOWLS3_MODE DOWxSx_PWM_MODE// Need to replace with Address Parameter Interface


#define DOWxSx_REPORT_DUTY 0U
#define DOWxSx_REPORT_VOLT 1U
 
 
 
typedef struct
{
   Debug_PVT_SCIM_Ctrl_12VDCDC New;
   Debug_PVT_SCIM_Ctrl_12VDCDC Prev;     
}Debug_PVT_SCIM_Ctrl_12VDCDC_Type;


typedef struct
{
   Debug_PVT_SCIM_Ctrl_12VLiving New;
   Debug_PVT_SCIM_Ctrl_12VLiving Prev;     
}Debug_PVT_SCIM_Ctrl_12VLiving_Type;


typedef struct
{
   Debug_PVT_SCIM_Ctrl_12VParked New;
   Debug_PVT_SCIM_Ctrl_12VParked Prev;     
}Debug_PVT_SCIM_Ctrl_12VParked_Type;


typedef struct
{
   Debug_PVT_SCIM_Ctrl_BHS1 New;
   Debug_PVT_SCIM_Ctrl_BHS1 Prev;     
}Debug_PVT_SCIM_Ctrl_BHS1_Type;



typedef struct
{
   Debug_PVT_SCIM_Ctrl_BHS2 New;
   Debug_PVT_SCIM_Ctrl_BHS2 Prev;     
}Debug_PVT_SCIM_Ctrl_BHS2_Type;


typedef struct
{
   Debug_PVT_SCIM_Ctrl_BHS3 New;
   Debug_PVT_SCIM_Ctrl_BHS3 Prev;     
}Debug_PVT_SCIM_Ctrl_BHS3_Type;



typedef struct
{
   Debug_PVT_SCIM_Ctrl_BHS4 New;
   Debug_PVT_SCIM_Ctrl_BHS4 Prev;     
}Debug_PVT_SCIM_Ctrl_BHS4_Type;



typedef struct
{
   Debug_PVT_SCIM_Ctrl_BLS1 New;
   Debug_PVT_SCIM_Ctrl_BLS1 Prev;     
}Debug_PVT_SCIM_Ctrl_BLS1_Type;


typedef struct
{
   Debug_PVT_SCIM_Ctrl_DAIPullUp New;
   Debug_PVT_SCIM_Ctrl_DAIPullUp Prev;     
}Debug_PVT_SCIM_Ctrl_DAIPullUp_Type;



typedef struct
{
   Debug_PVT_SCIM_Ctrl_LivingPullUp New;
   Debug_PVT_SCIM_Ctrl_LivingPullUp Prev;     
}Debug_PVT_SCIM_Ctrl_LivingPullUp_Type;



typedef struct
{
   Debug_PVT_SCIM_Ctrl_ParkedPullUp New;
   Debug_PVT_SCIM_Ctrl_ParkedPullUp Prev;     
}Debug_PVT_SCIM_Ctrl_ParkedPullUp_Type;



typedef struct
{
   Debug_PVT_SCIM_Ctrl_WHS1 New;
   Debug_PVT_SCIM_Ctrl_WHS1 Prev;     
}Debug_PVT_SCIM_Ctrl_WHS1_Type;


typedef struct
{
   Debug_PVT_SCIM_Ctrl_WHS2 New;
   Debug_PVT_SCIM_Ctrl_WHS2 Prev;     
}Debug_PVT_SCIM_Ctrl_WHS2_Type;


typedef struct
{
   Debug_PVT_SCIM_Ctrl_WLS1 New;
   Debug_PVT_SCIM_Ctrl_WLS1 Prev;     
}Debug_PVT_SCIM_Ctrl_WLS1_Type;


typedef struct
{
   Debug_PVT_SCIM_Ctrl_WLS2 New;
   Debug_PVT_SCIM_Ctrl_WLS2 Prev;     
}Debug_PVT_SCIM_Ctrl_WLS2_Type;



typedef struct
{
   Debug_PVT_SCIM_Ctrl_WLS3 New;
   Debug_PVT_SCIM_Ctrl_WLS3 Prev;     
}Debug_PVT_SCIM_Ctrl_WLS3_Type;



typedef struct
{
   Debug_PVT_ScimHwSelect_WHS1 New;
   Debug_PVT_ScimHwSelect_WHS1 Prev;     
}Debug_PVT_ScimHwSelect_WHS1_Type;

typedef struct
{
   Debug_PVT_ScimHwSelect_WHS2 New;
   Debug_PVT_ScimHwSelect_WHS2 Prev;     
}Debug_PVT_ScimHwSelect_WHS2_Type;


typedef struct
{
   Debug_PVT_ScimHwSelect_WLS2 New;
   Debug_PVT_ScimHwSelect_WLS2 Prev;     
}Debug_PVT_ScimHwSelect_WLS2_Type;



typedef struct
{
   Debug_PVT_ScimHwSelect_WLS3 New;
   Debug_PVT_ScimHwSelect_WLS3 Prev;      
}Debug_PVT_ScimHwSelect_WLS3_Type;


	uint8 AdiPinRef = 1;
	uint8 PWM_CS = 1;
	uint8 AdiPinRef_Flag = 1;
	uint8 PWM_CS_Flag = 1;
	uint16 CyclicCounter = 0;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Debug_PVT_DOWHS1_ReportedValue: Boolean
 * Debug_PVT_DOWHS2_ReportedValue: Boolean
 * Debug_PVT_DOWLS2_ReportedValue: Boolean
 * Debug_PVT_DOWLS3_ReportedValue: Boolean
 * Debug_PVT_SCIM_Ctrl_Generic1: Integer in interval [0...3]
 * Debug_PVT_SCIM_RD_12VDCDCFault: Boolean
 * Debug_PVT_SCIM_RD_12VDCDCVolt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_12VLivingVolt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_12VParkedVolt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI01_7: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI02_8: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI03_9: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI04_10: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI05_11: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI06_12: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_BHS1_Volt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_BHS2_Volt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_BHS3_Volt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_BHS4_Volt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_BLS1_Volt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_DAI1_2: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_VBAT: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_WHS1_Freq: Integer in interval [0...2047]
 * Debug_PVT_SCIM_RD_WHS1_VD: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_WHS2_Freq: Integer in interval [0...2047]
 * Debug_PVT_SCIM_RD_WHS2_VD: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_WLS2_Freq: Integer in interval [0...2047]
 * Debug_PVT_SCIM_RD_WLS2_VD: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_WLS3_Freq: Integer in interval [0...2047]
 * Debug_PVT_SCIM_RD_WLS3_VD: Integer in interval [0...255]
 * Debug_PVT_SCIM_TSincePwrOn_Long: Integer in interval [0...65535]
 * Debug_PVT_SCIM_TSincePwrOn_Short: Integer in interval [0...255]
 * Debug_PVT_SCIM_TSinceWkUp_Short: Integer in interval [0...255]
 * Debug_PVT_ScimHwSelect_WHS1: Boolean
 * Debug_PVT_ScimHwSelect_WHS2: Boolean
 * Debug_PVT_ScimHwSelect_WLS2: Boolean
 * Debug_PVT_ScimHwSelect_WLS3: Boolean
 * Debug_PVT_ScimHw_W_Duty: Integer in interval [0...127]
 * Debug_PVT_ScimHw_W_Freq: Integer in interval [0...2047]
 * Debug_SCIM_RD_Generic1: Integer in interval [0...31]
 * Debug_SCIM_RD_Generic2: Integer in interval [0...15]
 * Debug_SCIM_RD_Generic3: Integer in interval [0...63]
 * Debug_SCIM_RD_Generic4: Integer in interval [0...255]
 * Debug_SCIM_RD_Generic5: Boolean
 * Debug_SCIM_RD_Generic6: Integer in interval [0...31]
 * IOHWAB_BOOL: Boolean
 * IOHWAB_SINT8: Integer in interval [-128...127]
 * IOHWAB_UINT16: Integer in interval [0...65535]
 * IOHWAB_UINT8: Integer in interval [0...255]
 * SEWS_Pvt_ActivateReporting_X1C14_T: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * VGTT_EcuPwmDutycycle: Integer in interval [-128...127]
 * VGTT_EcuPwmPeriod: Integer in interval [0...65535]
 * boolean: Boolean (standard type)
 * sint8: Integer in interval [-128...127] (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BswM_BswMRteMDG_PvtReport_X1C14: Enumeration of integer in interval [0...255] with enumerators
 *   PvtReport_Enabled (0U)
 *   PvtReport_Disabled (1U)
 * Debug_PVT_ADI_ReportGroup: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Ctrl_CyclicReport (0U)
 *   Cx1_ADI_01To06_DAI1 (1U)
 *   Cx2_ADI_07To12_DAI2 (2U)
 *   Cx3_ADI_13To16_DAI (3U)
 * Debug_PVT_ADI_ReportRequest: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Ctrl_CyclicReport (0U)
 *   Cx1_ADI_01To06_DAI1 (1U)
 *   Cx2_ADI_07To12_DAI2 (2U)
 *   Cx3_ADI_13To16_DAI (3U)
 * Debug_PVT_FlexDataRequest: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_Ctr_CyclicReport (0U)
 *   Cx1_Universal_debug_trace (1U)
 *   Cx2_Keyfob_RF_data1 (2U)
 *   Cx3_Keyfob_RF_data2 (3U)
 *   Cx4_Keyfob_LF_data1 (4U)
 *   Cx5_Keyfob_LF_data2 (5U)
 *   Cx6_SW_execution_statistics (6U)
 *   Cx7_SW_state_statistics (7U)
 * Debug_PVT_SCIM_Ctrl_12VDCDC: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_12VLiving: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_12VParked: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_BHS1: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_BHS2: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_BHS3: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_BHS4: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_BLS1: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_DAIPullUp: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_WHS1: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_WHS2: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_WLS2: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_WLS3: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_RD_12VLivingFault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_12VParkedFault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_BHS1_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_BHS2_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_BHS3_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_BHS4_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_BLS1_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_VBAT_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_WHS1_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_PWM_failure (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_WHS2_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_PWM_failure (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_WLS2_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_PWM_failure (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_WLS3_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_PWM_failure (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * IOCtrlReq_T: Enumeration of integer in interval [0...255] with enumerators
 *   IOCtrl_AppRequest (0U)
 *   IOCtrl_DiagReturnCtrlToApp (1U)
 *   IOCtrl_DiagShortTermAdjust (2U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_Pvt_ActivateReporting_X1C14_T Rte_Prm_X1C14_Pvt_ActivateReporting_v(void)
 *
 *********************************************************************************************************************/


#define SCIM_PVTPT_IO_START_SEC_CODE
#include "SCIM_PVTPT_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Runnable_PVTPT_CtrlIo
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VDCDC(Debug_PVT_SCIM_Ctrl_12VDCDC *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VLiving(Debug_PVT_SCIM_Ctrl_12VLiving *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VParked(Debug_PVT_SCIM_Ctrl_12VParked *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS1(Debug_PVT_SCIM_Ctrl_BHS1 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS2(Debug_PVT_SCIM_Ctrl_BHS2 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS3(Debug_PVT_SCIM_Ctrl_BHS3 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS4(Debug_PVT_SCIM_Ctrl_BHS4 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BLS1(Debug_PVT_SCIM_Ctrl_BLS1 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_DAIPullUp(Debug_PVT_SCIM_Ctrl_DAIPullUp *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_Generic1(Debug_PVT_SCIM_Ctrl_Generic1 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS1(Debug_PVT_SCIM_Ctrl_WHS1 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS2(Debug_PVT_SCIM_Ctrl_WHS2 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS2(Debug_PVT_SCIM_Ctrl_WLS2 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS3(Debug_PVT_SCIM_Ctrl_WLS3 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS1(Debug_PVT_ScimHwSelect_WHS1 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS2(Debug_PVT_ScimHwSelect_WHS2 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS2(Debug_PVT_ScimHwSelect_WLS2 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS3(Debug_PVT_ScimHwSelect_WLS3 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Duty(Debug_PVT_ScimHw_W_Duty *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Freq(Debug_PVT_ScimHw_W_Freq *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ScimPvtControl_P_Status(uint8 data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_SetPullUp_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL ActivateStrongPullUp, IOHWAB_BOOL ActivateWeakPullUp, IOHWAB_BOOL ActivateDAIPullUp)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Runnable_PVTPT_CtrlIo_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SCIM_PVTPT_IO_CODE) Runnable_PVTPT_CtrlIo(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Runnable_PVTPT_CtrlIo
 *********************************************************************************************************************/

	static Debug_PVT_SCIM_Ctrl_12VDCDC_Type       Ctrl_12VDCDC_data = {Cx1_Deactivate,Cx1_Deactivate};
	static Debug_PVT_SCIM_Ctrl_12VLiving_Type     Ctrl_12VLiving_data = {Cx1_Deactivate,Cx1_Deactivate};
	static Debug_PVT_SCIM_Ctrl_12VParked_Type     Ctrl_12VParked_data = {Cx1_Deactivate,Cx1_Deactivate};
	static Debug_PVT_SCIM_Ctrl_BHS1_Type          Ctrl_BHS1_data = {Cx1_Deactivate,Cx1_Deactivate};
	static Debug_PVT_SCIM_Ctrl_BHS2_Type          Ctrl_BHS2_data = {Cx1_Deactivate,Cx1_Deactivate};
	static Debug_PVT_SCIM_Ctrl_BHS3_Type          Ctrl_BHS3_data = {Cx1_Deactivate,Cx1_Deactivate};
	static Debug_PVT_SCIM_Ctrl_BHS4_Type          Ctrl_BHS4_data = {Cx1_Deactivate,Cx1_Deactivate};
	static Debug_PVT_SCIM_Ctrl_DAIPullUp_Type     Ctrl_DAIPullUp_data = {Cx1_Deactivate,Cx1_Deactivate};
	static Debug_PVT_SCIM_Ctrl_WHS1_Type          Ctrl_WHS1_data = {Cx1_Deactivate,Cx1_Deactivate};
	static Debug_PVT_SCIM_Ctrl_WHS2_Type          Ctrl_WHS2_data = {Cx1_Deactivate,Cx1_Deactivate};
	static Debug_PVT_SCIM_Ctrl_BLS1_Type          Ctrl_BLS1_data = {Cx1_Deactivate,Cx1_Deactivate};
	static Debug_PVT_SCIM_Ctrl_WLS2_Type          Ctrl_WLS2_data = {Cx1_Deactivate,Cx1_Deactivate};
	static Debug_PVT_SCIM_Ctrl_WLS3_Type          Ctrl_WLS3_data = {Cx1_Deactivate,Cx1_Deactivate};
	static Debug_PVT_ScimHwSelect_WHS1_Type       HwSelect_WHS1_data = {0U,0U};
	static Debug_PVT_ScimHwSelect_WHS2_Type       HwSelect_WHS2_data = {0U,0U};
	static Debug_PVT_ScimHwSelect_WLS2_Type       HwSelect_WLS2_data = {0U,0U};
	static Debug_PVT_ScimHwSelect_WLS3_Type       HwSelect_WLS3_data = {0U,0U};
	static uint8 PvtCtrl = 1U;
	IOHWAB_BOOL ActivateDAIPullUp    = 0xffu;
	IOHWAB_BOOL ActivateWeakPullUp   = 0xffu;
	IOHWAB_BOOL ActivateStrongPullUp = 0xffu;
	IOHWAB_UINT8 OutputId;
	IOHWAB_UINT16 Hw_W_Freq_data;
	IOHWAB_UINT8 Hw_W_Duty_data;
	

  if(0U != Rte_Prm_X1C14_Pvt_ActivateReporting_v())
  {
     
   Ctrl_12VDCDC_data.Prev = Ctrl_12VDCDC_data.New;
   Ctrl_12VLiving_data.Prev = Ctrl_12VLiving_data.New;
   Ctrl_12VParked_data.Prev = Ctrl_12VParked_data.New;
   Ctrl_BHS1_data.Prev = Ctrl_BHS1_data.New;
   Ctrl_BHS2_data.Prev = Ctrl_BHS2_data.New;
   Ctrl_BHS3_data.Prev = Ctrl_BHS3_data.New;
   Ctrl_BHS4_data.Prev = Ctrl_BHS4_data.New;
   Ctrl_BLS1_data.Prev = Ctrl_BLS1_data.New;
   Ctrl_DAIPullUp_data.Prev = Ctrl_DAIPullUp_data.New;
   Ctrl_WHS1_data.Prev = Ctrl_WHS1_data.New;
   Ctrl_WHS2_data.Prev = Ctrl_WHS2_data.New;
   Ctrl_WLS2_data.Prev = Ctrl_WLS2_data.New;
   Ctrl_WLS3_data.Prev = Ctrl_WLS3_data.New;
   HwSelect_WHS1_data.Prev = HwSelect_WHS1_data.New;
   HwSelect_WHS2_data.Prev = HwSelect_WHS2_data.New;
   HwSelect_WLS2_data.Prev = HwSelect_WLS2_data.New;
   HwSelect_WLS3_data.Prev = HwSelect_WLS3_data.New;

   Rte_Write_ScimPvtControl_P_Status(PvtCtrl);

   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VDCDC(&Ctrl_12VDCDC_data.New);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VLiving(&Ctrl_12VLiving_data.New);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VParked(&Ctrl_12VParked_data.New);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_DAIPullUp(&Ctrl_DAIPullUp_data.New);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS1(&Ctrl_BHS1_data.New);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS2(&Ctrl_BHS2_data.New);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS3(&Ctrl_BHS3_data.New);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS4(&Ctrl_BHS4_data.New);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS1(&Ctrl_WHS1_data.New);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS2(&Ctrl_WHS2_data.New);	
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BLS1(&Ctrl_BLS1_data.New);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS2(&Ctrl_WLS2_data.New);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS3(&Ctrl_WLS3_data.New);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS1(&HwSelect_WHS1_data.New);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS2(&HwSelect_WHS2_data.New);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS2(&HwSelect_WLS2_data.New);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS3(&HwSelect_WLS3_data.New);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Duty(&Hw_W_Duty_data);
   Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Freq(&Hw_W_Freq_data);


   if((Ctrl_12VDCDC_data.New == Cx1_Deactivate)||(Ctrl_12VDCDC_data.New == Cx2_Activate))
   {
      Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrl_DiagShortTermAdjust, Ctrl_12VDCDC_data.New-1);
   }
   else if((Ctrl_12VDCDC_data.New == Cx0_Idle)&&(Ctrl_12VDCDC_data.Prev != Ctrl_12VDCDC_data.New))
   {
      Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrl_DiagReturnCtrlToApp, 0);
   }   


   if((Ctrl_12VLiving_data.New == Cx1_Deactivate)||(Ctrl_12VLiving_data.New == Cx2_Activate))
   {
      Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrl_DiagShortTermAdjust, Ctrl_12VLiving_data.New-1);
   }
   else if((Ctrl_12VLiving_data.New == Cx0_Idle)&&(Ctrl_12VLiving_data.Prev != Ctrl_12VLiving_data.New))
   {
      Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrl_DiagReturnCtrlToApp, 0);
   }   


   if((Ctrl_12VParked_data.New == Cx1_Deactivate)||(Ctrl_12VParked_data.New == Cx2_Activate))
   {
      Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrl_DiagShortTermAdjust, Ctrl_12VParked_data.New-1);
   }
   else if((Ctrl_12VParked_data.New == Cx0_Idle)&&(Ctrl_12VParked_data.Prev != Ctrl_12VParked_data.New))
   {
      Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrl_DiagReturnCtrlToApp, 0);
   }




   /* DOBHS */

   if((Ctrl_BHS1_data.New == Cx1_Deactivate)||(Ctrl_BHS1_data.New == Cx2_Activate))
   {
      Rte_Call_DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrl_DiagShortTermAdjust, Ctrl_BHS1_data.New-1);
   }
   else if((Ctrl_BHS1_data.New == Cx0_Idle)&&(Ctrl_BHS1_data.Prev != Ctrl_BHS1_data.New))
   {
      Rte_Call_DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrl_DiagReturnCtrlToApp, 0);
   }



   if((Ctrl_BHS2_data.New == Cx1_Deactivate)||(Ctrl_BHS2_data.New == Cx2_Activate))
   {
      Rte_Call_DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrl_DiagShortTermAdjust, Ctrl_BHS2_data.New-1);
   }
   else if((Ctrl_BHS2_data.New == Cx0_Idle)&&(Ctrl_BHS2_data.Prev != Ctrl_BHS2_data.New))
   {
      Rte_Call_DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrl_DiagReturnCtrlToApp, 0);
   }


   if((Ctrl_BHS3_data.New == Cx1_Deactivate)||(Ctrl_BHS3_data.New == Cx2_Activate))
   {
      Rte_Call_DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrl_DiagShortTermAdjust, Ctrl_BHS3_data.New-1);
   }
   else if((Ctrl_BHS3_data.New == Cx0_Idle)&&(Ctrl_BHS3_data.Prev != Ctrl_BHS3_data.New))
   {
      Rte_Call_DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrl_DiagReturnCtrlToApp, 0);
   }



   if((Ctrl_BHS4_data.New == Cx1_Deactivate)||(Ctrl_BHS4_data.New == Cx2_Activate))
   {
      Rte_Call_DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrl_DiagShortTermAdjust, Ctrl_BHS4_data.New-1);
   }
   else if((Ctrl_BHS4_data.New == Cx0_Idle)&&(Ctrl_BHS4_data.Prev != Ctrl_BHS4_data.New))
   {
      Rte_Call_DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrl_DiagReturnCtrlToApp, 0);
   }




   /* DOBLS */

   if((Ctrl_BLS1_data.New == Cx1_Deactivate)||(Ctrl_BLS1_data.New == Cx2_Activate))
   {
      Rte_Call_DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrl_DiagShortTermAdjust, Ctrl_BLS1_data.New-1);
   }
   else if((Ctrl_BLS1_data.New == Cx0_Idle)&&(Ctrl_BLS1_data.Prev != Ctrl_BLS1_data.New))
   {
      Rte_Call_DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrl_DiagReturnCtrlToApp, 0);
   }



   /* DAI */


   if((Ctrl_DAIPullUp_data.New == Cx1_Deactivate)||(Ctrl_DAIPullUp_data.New == Cx2_Activate))
   {
      ActivateDAIPullUp    = (IOHWAB_BOOL)(Ctrl_DAIPullUp_data.New-1U);
      ActivateWeakPullUp   = 0xffu;
      ActivateStrongPullUp = 0xffu;
      Rte_Call_AdiInterface_P_SetPullUp_CS(IOCtrl_DiagShortTermAdjust, ActivateStrongPullUp, ActivateWeakPullUp, ActivateDAIPullUp);
   }
   else if((Ctrl_DAIPullUp_data.New == Cx0_Idle)&&(Ctrl_DAIPullUp_data.Prev != Ctrl_DAIPullUp_data.New))
   {
      ActivateDAIPullUp    = 0u;
      ActivateWeakPullUp   = 0xffu;
      ActivateStrongPullUp = 0xffu;
      Rte_Call_AdiInterface_P_SetPullUp_CS(IOCtrl_DiagReturnCtrlToApp, ActivateStrongPullUp, ActivateWeakPullUp, ActivateDAIPullUp);
   }   


   /* DOWHS, DOWLS */

   if((Ctrl_WHS1_data.New == Cx0_Idle)&&(Ctrl_WHS1_data.Prev != Ctrl_WHS1_data.New))
   {
      Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrl_DiagReturnCtrlToApp, 1U, Hw_W_Freq_data, Hw_W_Duty_data,0);
   }
   else if(Ctrl_WHS1_data.New == Cx1_Deactivate)
   {
      Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrl_DiagShortTermAdjust, 1U, 0U, 0U, 0U);
   }
   else if((Ctrl_WHS1_data.New == Cx2_Activate)&&(HwSelect_WHS1_data.New == 1U))
   {
      Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrl_DiagShortTermAdjust, 1U, Hw_W_Freq_data, Hw_W_Duty_data,1U);
   }
   else
   {
      /* code */
   }


   if((Ctrl_WHS2_data.New == Cx0_Idle)&&(Ctrl_WHS2_data.Prev != Ctrl_WHS2_data.New))
   {
      Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrl_DiagReturnCtrlToApp, 2U, 0U, 0U, 0U);
   }
   else if(Ctrl_WHS2_data.New == Cx1_Deactivate)
   {
      Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrl_DiagShortTermAdjust, 2U, 0U, 0U, 0U);
   }
   else if((Ctrl_WHS2_data.New == Cx2_Activate)&&(HwSelect_WHS2_data.New == 1U))
   {
      Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrl_DiagShortTermAdjust, 2U, Hw_W_Freq_data, Hw_W_Duty_data, 1U);
   }
   else
   {
      /* code */
   }



   if((Ctrl_WLS2_data.New == Cx0_Idle)&&(Ctrl_WLS2_data.Prev != Ctrl_WLS2_data.New))
   {
      Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrl_DiagReturnCtrlToApp, 2U,  0U, 0U, 0U);
   }
   else if(Ctrl_WLS2_data.New == Cx1_Deactivate)
   {
      Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrl_DiagShortTermAdjust, 2U, 0U, 0U, 0U);
   }
   else if((Ctrl_WLS2_data.New == Cx2_Activate)&&(HwSelect_WLS2_data.New == 1U))
   {
      Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrl_DiagShortTermAdjust, 2U, Hw_W_Freq_data, Hw_W_Duty_data, 1U);
   }
   else
   {
      /* code */
   }



   if((Ctrl_WLS3_data.New == Cx0_Idle)&&(Ctrl_WLS3_data.Prev != Ctrl_WLS3_data.New))
   {
      Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrl_DiagReturnCtrlToApp, 3U,  0U, 0U, 0U);
   }
   else if(Ctrl_WLS3_data.New == Cx1_Deactivate)
   {
      Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrl_DiagShortTermAdjust, 3U, 0U, 0U, 0U);
   }
   else if((Ctrl_WLS3_data.New == Cx2_Activate)&&(HwSelect_WLS3_data.New == 1U))
   {
      Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrl_DiagShortTermAdjust, 3U, Hw_W_Freq_data, Hw_W_Duty_data, 1U);
   }
   else
   {
      /* code */
   }     
     
  }
  else
  {
   /* code */    
  }

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Runnable_PVTPT_ReadIo
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Debug_PVT_ADI_ReportRequest_Debug_PVT_ADI_ReportRequest(Debug_PVT_ADI_ReportRequest *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_ADI_ReportGroup(Debug_PVT_ADI_ReportGroup data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWHS1_ReportedValue(Debug_PVT_DOWHS1_ReportedValue data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWHS2_ReportedValue(Debug_PVT_DOWHS2_ReportedValue data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWLS2_ReportedValue(Debug_PVT_DOWLS2_ReportedValue data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWLS3_ReportedValue(Debug_PVT_DOWLS3_ReportedValue data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCFault(Debug_PVT_SCIM_RD_12VDCDCFault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCVolt(Debug_PVT_SCIM_RD_12VDCDCVolt data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingFault(Debug_PVT_SCIM_RD_12VLivingFault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingVolt(Debug_PVT_SCIM_RD_12VLivingVolt data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedFault(Debug_PVT_SCIM_RD_12VParkedFault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedVolt(Debug_PVT_SCIM_RD_12VParkedVolt data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI01_7(Debug_PVT_SCIM_RD_ADI01_7 data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI02_8(Debug_PVT_SCIM_RD_ADI02_8 data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI03_9(Debug_PVT_SCIM_RD_ADI03_9 data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI04_10(Debug_PVT_SCIM_RD_ADI04_10 data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI05_11(Debug_PVT_SCIM_RD_ADI05_11 data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI06_12(Debug_PVT_SCIM_RD_ADI06_12 data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Fault(Debug_PVT_SCIM_RD_BHS1_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Volt(Debug_PVT_SCIM_RD_BHS1_Volt data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Fault(Debug_PVT_SCIM_RD_BHS2_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Volt(Debug_PVT_SCIM_RD_BHS2_Volt data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Fault(Debug_PVT_SCIM_RD_BHS3_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Volt(Debug_PVT_SCIM_RD_BHS3_Volt data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Fault(Debug_PVT_SCIM_RD_BHS4_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Volt(Debug_PVT_SCIM_RD_BHS4_Volt data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Fault(Debug_PVT_SCIM_RD_BLS1_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Volt(Debug_PVT_SCIM_RD_BLS1_Volt data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_DAI1_2(Debug_PVT_SCIM_RD_DAI1_2 data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT(Debug_PVT_SCIM_RD_VBAT data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT_Fault(Debug_PVT_SCIM_RD_VBAT_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Fault(Debug_PVT_SCIM_RD_WHS1_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Freq(Debug_PVT_SCIM_RD_WHS1_Freq data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_VD(Debug_PVT_SCIM_RD_WHS1_VD data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Fault(Debug_PVT_SCIM_RD_WHS2_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Freq(Debug_PVT_SCIM_RD_WHS2_Freq data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_VD(Debug_PVT_SCIM_RD_WHS2_VD data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Fault(Debug_PVT_SCIM_RD_WLS2_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Freq(Debug_PVT_SCIM_RD_WLS2_Freq data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_VD(Debug_PVT_SCIM_RD_WLS2_VD data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Fault(Debug_PVT_SCIM_RD_WLS3_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Freq(Debug_PVT_SCIM_RD_WLS3_Freq data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_VD(Debug_PVT_SCIM_RD_WLS3_VD data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Long(Debug_PVT_SCIM_TSincePwrOn_Long data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Short(Debug_PVT_SCIM_TSincePwrOn_Short data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSinceWkUp_Short(Debug_PVT_SCIM_TSinceWkUp_Short data)
 *   Std_ReturnType Rte_Write_Request_SwcModeRequest_PvtReportCtrl_requestedMode(BswM_BswMRteMDG_PvtReport_X1C14 data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetPullUpState_CS(IOHWAB_BOOL *isPullUpActive_Strong, IOHWAB_BOOL *isPullUpActive_Weak, IOHWAB_BOOL *isPullUpActive_DAI)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(VGTT_EcuPinVoltage_0V2 *DcDc12vRefVoltage, IOHWAB_BOOL *IsDcDc12vActivated, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_1_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_2_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_3_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_4_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_VbatInterface_P_GetVbatVoltage_CS(VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_VbatInterface_I_AdcInFailure, RTE_E_VbatInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Runnable_PVTPT_ReadIo_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SCIM_PVTPT_IO_CODE) Runnable_PVTPT_ReadIo(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Runnable_PVTPT_ReadIo
 *********************************************************************************************************************/

	VGTT_EcuPinVoltage_0V2 AdiPinVoltage;
	IOHWAB_BOOL isPullUpActive_Strong = 0;
	IOHWAB_BOOL isPullUpActive_Weak = 0;
	IOHWAB_BOOL isPullUpActive_DAI = 0;
	VGTT_EcuPinVoltage_0V2 DcDc12vRefVoltage;
	IOHWAB_BOOL IsDcDc12vActivated;
	VGTT_EcuPinFaultStatus FaultStatus;
	VGTT_EcuPinFaultStatus FaultStatusDcDc=0;
	VGTT_EcuPinFaultStatus FaultStatusL=0;
	VGTT_EcuPinFaultStatus	FaultStatusP=0;
	IOHWAB_UINT8 SelectParkedOrLivingPin;
	IOHWAB_BOOL IsDo12VActivated;
	VGTT_EcuPinVoltage_0V2 Do12VPinVoltageL;
	VGTT_EcuPinVoltage_0V2 Do12VPinVoltageP;
	IOHWAB_UINT8 DoPinRef;
	IOHWAB_BOOL isDioActivated;
	IOHWAB_BOOL IsDoActivated;
	VGTT_EcuPinVoltage_0V2 Adi1PinVoltage;
	VGTT_EcuPinVoltage_0V2 Adi2PinVoltage;
	VGTT_EcuPinVoltage_0V2 Adi3PinVoltage;
	VGTT_EcuPinVoltage_0V2 Adi4PinVoltage;
	VGTT_EcuPinVoltage_0V2 Adi5PinVoltage;
	VGTT_EcuPinVoltage_0V2 Adi6PinVoltage;
	VGTT_EcuPinVoltage_0V2 Adi7PinVoltage;
	VGTT_EcuPinVoltage_0V2 Adi8PinVoltage;
	VGTT_EcuPinVoltage_0V2 Adi9PinVoltage;
	VGTT_EcuPinVoltage_0V2 Adi10PinVoltage;
	VGTT_EcuPinVoltage_0V2 Adi11PinVoltage;
	VGTT_EcuPinVoltage_0V2 Adi12PinVoltage;
	VGTT_EcuPinVoltage_0V2 Adi13PinVoltage;
	VGTT_EcuPinVoltage_0V2 Adi14PinVoltage;
	VGTT_EcuPinVoltage_0V2 Adi15PinVoltage;
	VGTT_EcuPinVoltage_0V2 Adi16PinVoltage;
	VGTT_EcuPinVoltage_0V2 Dai1PinVoltage;
	VGTT_EcuPinVoltage_0V2 Dai2PinVoltage;
	VGTT_EcuPinFaultStatus ADI1FaultStatus;
	VGTT_EcuPinFaultStatus ADI2FaultStatus;
	VGTT_EcuPinFaultStatus ADI3FaultStatus;
	VGTT_EcuPinFaultStatus ADI4FaultStatus;
	VGTT_EcuPinFaultStatus ADI5FaultStatus;
	VGTT_EcuPinFaultStatus ADI6FaultStatus;
	VGTT_EcuPinFaultStatus ADI7FaultStatus;
	VGTT_EcuPinFaultStatus ADI8FaultStatus;
	VGTT_EcuPinFaultStatus ADI9FaultStatus;
	VGTT_EcuPinFaultStatus ADI10FaultStatus;
	VGTT_EcuPinFaultStatus ADI11FaultStatus;
	VGTT_EcuPinFaultStatus ADI12FaultStatus;
	VGTT_EcuPinFaultStatus ADI13FaultStatus;
	VGTT_EcuPinFaultStatus ADI14FaultStatus;
	VGTT_EcuPinFaultStatus ADI15FaultStatus;
	VGTT_EcuPinFaultStatus ADI16FaultStatus;
	VGTT_EcuPinFaultStatus DAI1FaultStatus;
	VGTT_EcuPinFaultStatus DAI2FaultStatus;
	VGTT_EcuPinVoltage_0V2 BHS1_Voltdata;
	VGTT_EcuPinVoltage_0V2 BHS2_Voltdata;
	VGTT_EcuPinVoltage_0V2 BHS3_Voltdata;
	VGTT_EcuPinVoltage_0V2 BHS4_Voltdata;
	VGTT_EcuPinVoltage_0V2 BHS1_Faultdata;
	VGTT_EcuPinVoltage_0V2 BHS2_Faultdata;
	VGTT_EcuPinVoltage_0V2 BHS3_Faultdata;
	VGTT_EcuPinVoltage_0V2 BHS4_Faultdata;	
	VGTT_EcuPinVoltage_0V2 BLS1_Voltdata;
	VGTT_EcuPinVoltage_0V2 BLS1_Faultdata;
	VGTT_EcuPinVoltage_0V2 WHS1_DoPinVoltage;
	VGTT_EcuPinVoltage_0V2 WHS2_DoPinVoltage;
	VGTT_EcuPinVoltage_0V2 BatteryVoltage;
	VGTT_EcuPwmDutycycle WHS1_DutyCycle = 0;
	VGTT_EcuPwmDutycycle WHS2_DutyCycle=0;
	VGTT_EcuPwmPeriod WHS1_Period = 0;
	VGTT_EcuPwmPeriod WHS2_Period = 0;
	VGTT_EcuPinFaultStatus WHS1_FaultStatus=0;
	VGTT_EcuPinFaultStatus WHS2_FaultStatus=0;	
	VGTT_EcuPinVoltage_0V2 WLS2_DoPinVoltage;
	VGTT_EcuPinVoltage_0V2 WLS3_DoPinVoltage;	
	VGTT_EcuPwmDutycycle WLS1_DutyCycle = 0;
	VGTT_EcuPwmDutycycle WLS2_DutyCycle=0;
	VGTT_EcuPwmDutycycle WLS3_DutyCycle=0;
	VGTT_EcuPwmPeriod WLS2_Period = 0;
	VGTT_EcuPwmPeriod WLS3_Period = 0;
	VGTT_EcuPinFaultStatus WLS2_FaultStatus=0;
	VGTT_EcuPinFaultStatus WLS3_FaultStatus=0;
   Debug_PVT_SCIM_TSincePwrOn_Long  TSincePwrOn_Long  = 0U;
   Debug_PVT_SCIM_TSincePwrOn_Short TSincePwrOn_Short = 0U;
   Debug_PVT_SCIM_TSinceWkUp_Short  TSinceWkUp_Short  = 0U; 
   Debug_PVT_ADI_ReportRequest AdiPinRef;


  if(0U != Rte_Prm_X1C14_Pvt_ActivateReporting_v())
  {
     
      Rte_Write_Request_SwcModeRequest_PvtReportCtrl_requestedMode(PvtReport_Enabled);
      
      Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(&DcDc12vRefVoltage, &IsDcDc12vActivated, &FaultStatusDcDc);
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCFault((FaultStatusDcDc& 0x07));		//3bit fault signal
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCVolt(DcDc12vRefVoltage);


      Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(1, &IsDo12VActivated, &Do12VPinVoltageL, &BatteryVoltage, &FaultStatusL);
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingFault((FaultStatusL& 0x07));		//3bit fault signal
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingVolt(Do12VPinVoltageL);

      Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(0, &IsDo12VActivated, &Do12VPinVoltageP,&BatteryVoltage, &FaultStatusP);
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedFault((FaultStatusP& 0x07));		//3bit fault signal
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedVolt(Do12VPinVoltageP);


      Rte_Call_VbatInterface_P_GetVbatVoltage_CS(&BatteryVoltage, &FaultStatus);
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT(BatteryVoltage);
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT_Fault((FaultStatus& 0x07));		//3bit fault signal


      /* Strong/Weak/DAI PullUp */
      Rte_Call_AdiInterface_P_GetPullUpState_CS(&isPullUpActive_Strong, &isPullUpActive_Weak, &isPullUpActive_DAI);		// There are no CAN signals


      /* ADI */
      Rte_Read_Debug_PVT_ADI_ReportRequest_Debug_PVT_ADI_ReportRequest(&AdiPinRef);

      if(AdiPinRef == Cx0_Ctrl_CyclicReport)
      {
      /* Ctrl Cyclic Report */
      }
      else if(AdiPinRef == Cx1_ADI_01To06_DAI1)
      {
         /* ADI01 to ADI06, DAI1 */
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 1U, &Adi1PinVoltage, &BatteryVoltage, &ADI1FaultStatus);	// ADI1
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 2U, &Adi2PinVoltage, &BatteryVoltage, &ADI2FaultStatus);	// ADI2
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 3U, &Adi3PinVoltage, &BatteryVoltage, &ADI3FaultStatus);	// ADI3
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 4U, &Adi4PinVoltage, &BatteryVoltage, &ADI4FaultStatus);	// ADI4
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 5U, &Adi5PinVoltage, &BatteryVoltage, &ADI5FaultStatus);	// ADI5
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 6U, &Adi6PinVoltage, &BatteryVoltage, &ADI6FaultStatus);	// ADI6
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 17U, &Dai1PinVoltage, &BatteryVoltage, &DAI1FaultStatus);	// DAI1

         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI01_7(Adi1PinVoltage);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI02_8(Adi2PinVoltage);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI03_9(Adi3PinVoltage);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI04_10(Adi4PinVoltage);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI05_11(Adi5PinVoltage);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI06_12(Adi6PinVoltage);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_DAI1_2(Dai1PinVoltage);
      }
      else if(AdiPinRef == Cx2_ADI_07To12_DAI2)
      {
         /* ADI07 to ADI12, DAI2 */
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 7U, &Adi7PinVoltage, &BatteryVoltage, &ADI7FaultStatus);		// ADI7
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 8U, &Adi8PinVoltage, &BatteryVoltage, &ADI8FaultStatus);		// ADI8
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 9U, &Adi9PinVoltage, &BatteryVoltage, &ADI9FaultStatus);		// ADI9
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 10U, &Adi10PinVoltage, &BatteryVoltage, &ADI10FaultStatus);	// ADI10
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 11U, &Adi11PinVoltage, &BatteryVoltage, &ADI11FaultStatus);	// ADI11
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 12U, &Adi12PinVoltage, &BatteryVoltage, &ADI12FaultStatus);	// ADI12
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 18U, &Dai2PinVoltage, &BatteryVoltage, &DAI2FaultStatus);		// DAI2

         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI01_7(Adi7PinVoltage);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI02_8(Adi8PinVoltage);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI03_9(Adi9PinVoltage);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI04_10(Adi10PinVoltage);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI05_11(Adi11PinVoltage);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI06_12(Adi12PinVoltage);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_DAI1_2(Dai2PinVoltage);
      }
      else if(AdiPinRef == Cx3_ADI_13To16_DAI)
      {
         /* ADI07 to ADI12, DAI2 */
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 13U, &Adi13PinVoltage, &BatteryVoltage, &ADI13FaultStatus);  // ADI13
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 14U, &Adi14PinVoltage, &BatteryVoltage, &ADI14FaultStatus);  // ADI14
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 15U, &Adi15PinVoltage, &BatteryVoltage, &ADI15FaultStatus);  // ADI15
         Rte_Call_AdiInterface_P_GetAdiPinState_CS( 16U, &Adi16PinVoltage, &BatteryVoltage, &ADI16FaultStatus);  // ADI16
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI01_7(Adi13PinVoltage);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI02_8(Adi14PinVoltage);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI03_9(Adi15PinVoltage);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI04_10(Adi16PinVoltage);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI05_11(0);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI06_12(0);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_DAI1_2(0);
      }
      else
      {
      /* code */
      }

      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_ADI_ReportGroup(AdiPinRef);
      /* DOBHS */

      Rte_Call_DobhsCtrlInterface_P_1_GetDobhsPinState_CS(&isDioActivated, &BHS1_Voltdata, &BatteryVoltage, &BHS1_Faultdata);
      Rte_Call_DobhsCtrlInterface_P_2_GetDobhsPinState_CS(&isDioActivated, &BHS2_Voltdata, &BatteryVoltage, &BHS2_Faultdata);
      Rte_Call_DobhsCtrlInterface_P_3_GetDobhsPinState_CS(&isDioActivated, &BHS3_Voltdata, &BatteryVoltage, &BHS3_Faultdata);
      Rte_Call_DobhsCtrlInterface_P_4_GetDobhsPinState_CS(&isDioActivated, &BHS4_Voltdata, &BatteryVoltage, &BHS4_Faultdata);


      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Fault((BHS1_Faultdata & 0x07));
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Fault((BHS2_Faultdata & 0x07));
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Fault((BHS3_Faultdata & 0x07));
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Fault((BHS4_Faultdata & 0x07));
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Volt(BHS1_Voltdata);
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Volt(BHS2_Voltdata);
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Volt(BHS3_Voltdata);
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Volt(BHS4_Voltdata);


      /* DOBLS */

      Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(&isDioActivated, &BLS1_Voltdata, &BatteryVoltage, &BLS1_Faultdata);

      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Fault((BLS1_Faultdata & 0x07));
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Volt(BLS1_Voltdata);

      /* DOWHS, DOWLS */
      Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(1, &IsDoActivated, &WHS1_DoPinVoltage, &BatteryVoltage, &WHS1_DutyCycle, &WHS1_Period, &WHS1_FaultStatus);
      Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(2, &IsDoActivated, &WHS2_DoPinVoltage, &BatteryVoltage, &WHS2_DutyCycle, &WHS2_Period, &WHS2_FaultStatus);


      Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(2, &IsDoActivated, &WLS2_DoPinVoltage, &BatteryVoltage, &WLS2_DutyCycle, &WLS2_Period, &WLS2_FaultStatus);
      Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(3, &IsDoActivated, &WLS3_DoPinVoltage, &BatteryVoltage, &WLS3_DutyCycle, &WLS3_Period, &WLS3_FaultStatus);

      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Fault((WHS1_FaultStatus& 0x07));
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Fault((WHS2_FaultStatus& 0x07));
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Fault((WLS2_FaultStatus& 0x07));
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Fault((WLS3_FaultStatus& 0x07));
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Freq(WHS1_Period);
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Freq(WHS2_Period);
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Freq(WLS2_Period);
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Freq(WLS3_Period);


      if(PCODE_DOWHS1_MODE== DOWxSx_PWM_MODE)
      {
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWHS1_ReportedValue(DOWxSx_REPORT_DUTY);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_VD(WHS1_DutyCycle);       
      }
      else if(PCODE_DOWHS1_MODE== DOWxSx_IO_MODE)
      {
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWHS1_ReportedValue(DOWxSx_REPORT_VOLT);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_VD(WHS1_DoPinVoltage);         
      }
      else
      {

      }      



      if(PCODE_DOWHS2_MODE== DOWxSx_PWM_MODE)
      {     
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWHS2_ReportedValue(DOWxSx_REPORT_DUTY);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_VD(WHS2_DutyCycle); 
       
      }
      else if(PCODE_DOWHS2_MODE== DOWxSx_IO_MODE)
      {
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWHS2_ReportedValue(DOWxSx_REPORT_VOLT);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_VD(WHS2_DoPinVoltage);       
      }
      else
      {

      } 


      if(PCODE_DOWLS2_MODE== DOWxSx_PWM_MODE)
      {
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWLS2_ReportedValue(DOWxSx_REPORT_DUTY);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_VD(WLS2_DutyCycle);
             
      }
      else if(PCODE_DOWLS2_MODE== DOWxSx_IO_MODE)
      {
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWLS2_ReportedValue(DOWxSx_REPORT_VOLT);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_VD(WLS2_DoPinVoltage);         
      }
      else
      {

      } 



      if(PCODE_DOWLS3_MODE== DOWxSx_PWM_MODE)
      {
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWLS3_ReportedValue(DOWxSx_REPORT_DUTY);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_VD(WLS3_DutyCycle);  

      }
      else if(PCODE_DOWLS3_MODE== DOWxSx_IO_MODE)
      {
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWLS3_ReportedValue(DOWxSx_REPORT_VOLT);
         Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_VD(WLS3_DoPinVoltage); 
      }
      else
      {

      } 

      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Long(TSincePwrOn_Long);
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Short(TSincePwrOn_Short);
      Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSinceWkUp_Short(TSinceWkUp_Short);      
    
  }
  else
  {
      Rte_Write_Request_SwcModeRequest_PvtReportCtrl_requestedMode(PvtReport_Disabled);   
  }

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define SCIM_PVTPT_IO_STOP_SEC_CODE
#include "SCIM_PVTPT_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
