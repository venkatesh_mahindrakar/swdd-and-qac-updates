/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  SpeedControlFreeWheel_LINMasCtrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  SpeedControlFreeWheel_LINMasCtrl
 *  Generated at:  Fri Jun 12 17:01:43 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <SpeedControlFreeWheel_LINMasCtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file SpeedControlFreeWheel_LINMasCtrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_VehicleControlAndActiveSafety 
//! @{
//! @addtogroup ActiveSafety 
//! @{
//! @addtogroup SpeedControlFreeWheel_LINMasCtrl
//! @{
//!
//! \brief
//! SpeedControlFreeWheel_LINMasCtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the SpeedControlFreeWheel_LINMasCtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_SpeedControlFreeWheel_LINMasCtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 
#include "SpeedControlFreeWheel_LINMasCtrl.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DiagInfo_T: Integer in interval [0...127]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * ResponseErrorCCFW: Boolean
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 *   FreeWheel_Status_NoMovement (0U)
 *   FreeWheel_Status_1StepClockwise (1U)
 *   FreeWheel_Status_2StepsClockwise (2U)
 *   FreeWheel_Status_3StepsClockwise (3U)
 *   FreeWheel_Status_4StepsClockwise (4U)
 *   FreeWheel_Status_5StepsClockwise (5U)
 *   FreeWheel_Status_6StepsClockwise (6U)
 *   FreeWheel_Status_1StepCounterClockwise (7U)
 *   FreeWheel_Status_2StepsCounterClockwise (8U)
 *   FreeWheel_Status_3StepsCounterClockwise (9U)
 *   FreeWheel_Status_4StepsCounterClockwise (10U)
 *   FreeWheel_Status_5StepsCounterClockwise (11U)
 *   FreeWheel_Status_6StepsCounterClockwise (12U)
 *   FreeWheel_Status_Spare (13U)
 *   FreeWheel_Status_Error (14U)
 *   FreeWheel_Status_NotAvailable (15U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *   FALSE (0U)
 *   TRUE (1U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1B2C_CCFW_Installed_v(void)
 *
 *********************************************************************************************************************/


#define SpeedControlFreeWheel_LINMasCtrl_START_SEC_CODE
#include "SpeedControlFreeWheel_LINMasCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_CCFW_Installed   (Rte_Prm_P1B2C_CCFW_Installed_v())

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_FreezeCurrentState_Irv_IOCTL_CCFWLinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOQ_Data_P1EOQ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, SpeedControlFreeWheel_LINMasCtrl_CODE) DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

   *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_SUBFUNCTIONNOTSUPPORTED;

   return RTE_E_DataServices_P1EOQ_Data_P1EOQ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData_Irv_IOCTL_CCFWLinCtrl(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOQ_Data_P1EOQ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, SpeedControlFreeWheel_LINMasCtrl_CODE) DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData (returns application error)
 *********************************************************************************************************************/

   Data[0] = (uint8)((Rte_IrvRead_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData_Irv_IOCTL_CCFWLinCtrl()) & CONST_Tester_Notpresent);

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_CCFWLinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOQ_Data_P1EOQ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, SpeedControlFreeWheel_LINMasCtrl_CODE) DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

   Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_CCFWLinCtrl((Rte_IrvRead_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData_Irv_IOCTL_CCFWLinCtrl()) & CONST_Tester_Notpresent);
   *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_POSITIVERESPONSE;
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_CCFWLinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOQ_Data_P1EOQ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, SpeedControlFreeWheel_LINMasCtrl_CODE) DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

   uint8 retval = RTE_E_OK;
   *ErrorCode   = (Dcm_NegativeResponseCodeType)DCM_E_POSITIVERESPONSE;
   if (Data[0] < 3U) // change 3 with Maximum Range value
   {
      Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_CCFWLinCtrl(Data[0] | CONST_Tester_Present); // change 0x80 with MSB Mask macro
   }
   else
   {
      retval     = RTE_E_DataServices_P1EOQ_Data_P1EOQ_E_NOT_OK;
      *ErrorCode = (Dcm_NegativeResponseCodeType)DCM_E_REQUESTOUTOFRANGE;
   }

   return retval;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SpeedControlFreeWheel_LINMasCtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ACCOrCCIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ASLIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_DiagInfoCCFW_DiagInfo(DiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FCW_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_LIN_FCWPushButton_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_LKSPushButton_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_SpeedControlModeButtonStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_SpeedControlModeWheelStat_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_LKS_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ResponseErrorCCFW_ResponseErrorCCFW(ResponseErrorCCFW *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_FCWPushButton_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_LIN_ACCOrCCIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_ASLIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_FCW_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_LKS_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LKSPushButton_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_SpeedControlModeButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_SpeedControlModeWheelStatus_FreeWheel_Status(FreeWheel_Status_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_SpeedControlFreeWheel_LINMasCtrl_20ms_runnable_Irv_IOCTL_CCFWLinCtrl(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_SpeedControlFreeWheel_LINMasCtrl_20ms_runnable_Irv_IOCTL_CCFWLinCtrl(uint8 data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BKB_87_CCFWLink_NoResp_ResetEventStatus(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BKB_87_CCFWLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN1_16_CCFW_VBT_ResetEventStatus(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN1_16_CCFW_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN1_17_CCFW_VAT_ResetEventStatus(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN1_17_CCFW_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN1_44_CCFW_RAM_ResetEventStatus(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN1_44_CCFW_RAM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN1_45_CCFW_FLASH_ResetEventStatus(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN1_45_CCFW_FLASH_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN1_46_CCFW_EEPROM_ResetEventStatus(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN1_46_CCFW_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_ResetEventStatus(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SpeedControlFreeWheel_LINMasCtrl_20ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution runnable logic for the SpeedControlFreeWheel_LINMasCtrl_20ms_runnable
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SpeedControlFreeWheel_LINMasCtrl_CODE) SpeedControlFreeWheel_LINMasCtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SpeedControlFreeWheel_LINMasCtrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   static SpeedCtrlFreeWheel_LINMasCtrl_in_StructType   RteInData_Common;
   static SpeedCtrlFreeWheel_LINMastCtrl_out_StructType RteOutData_Common = { PushButtonStatus_NotAvailable,
                                                                              FreeWheel_Status_NotAvailable,
                                                                              PushButtonStatus_NotAvailable,
                                                                              PushButtonStatus_NotAvailable,
                                                                              DeviceIndication_Off,
                                                                              DeviceIndication_Off,
                                                                              DeviceIndication_Off,
                                                                              DeviceIndication_Off };
   Std_ReturnType retValue_Resp_CCFW                                      = RTE_E_INVALID;
   boolean        IsFciFaultActive                                        = CONST_Fault_InActive;

   //! ###### Process RTE input data read common logic : 'Get_RteDataRead_Common()'
   retValue_Resp_CCFW = Get_RteDataRead_Common(&RteInData_Common);
   //! ###### Check LIN bus status
   //! ###### Check for communication mode of LIN4 is equal to 'error' state (Bus off) 
   if (Error == RteInData_Common.ComMode_LIN4)
   {
      //! ##### Process the fallback mode logic : 'SpeedControlFreeWheel_FallbackLogic()'
      SpeedControlFreeWheel_FallbackLogic(&RteOutData_Common.FCWPushButton,
                                          &RteOutData_Common.LKSPushButton, 
                                          &RteOutData_Common.SpeedControlModeButtonStatus,
                                          &RteOutData_Common.SpeedControlModeWheelStatus);
   }
   else
   {
      //! ###### Select 'CCFW_Installed' parameter
      if (TRUE == PCODE_CCFW_Installed)
      {
         //! ##### Check for communication mode of LIN4(ComMode_LIN4) status
         if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN4)
            || (SwitchDetection == RteInData_Common.ComMode_LIN4)
            || (Diagnostic == RteInData_Common.ComMode_LIN4))
         {
            //! ##### Check for the RTE failure event reported on ResponseErrorCCFW (missing frame condition)
            if (RTE_E_MAX_AGE_EXCEEDED == retValue_Resp_CCFW)
            {
               //! ##### Check for communication mode of LIN4(ComMode_LIN4) status is 'ApplicationMonitoring' or 'SwitchDetection'
               if ((ApplicationMonitoring == RteInData_Common.ComMode_LIN4)
                  || (SwitchDetection == RteInData_Common.ComMode_LIN4))
               {
                  //! #### Invoke 'Rte_Call_Event_D1BKB_87_CCFWLink_NoResp_SetEventStatus' with status 'failed' for missing frame
                  Rte_Call_Event_D1BKB_87_CCFWLink_NoResp_SetEventStatus(DEM_EVENT_STATUS_FAILED);
                  //! #### Process the fallback logic : 'SpeedControlFreeWheel_FallbackLogic()'
                  SpeedControlFreeWheel_FallbackLogic(&RteOutData_Common.FCWPushButton,
                                                      &RteOutData_Common.LKSPushButton,
                                                      &RteOutData_Common.SpeedControlModeButtonStatus,
                                                      &RteOutData_Common.SpeedControlModeWheelStatus);
               }
               else
               {
                  //! ##### If LIN communication mode in diagnostic state
                  //! #### Process the signal gateway logic : 'SpeedControlFreeWheel_Signals_GatewayLogic()'
                  SpeedControlFreeWheel_Signals_GatewayLogic(&RteInData_Common,
                                                             &RteOutData_Common);
               }
            }
            //! ##### Check for 'ResponseErrorCCFW' is equal to RTE_OK or RTE_E_COM_STOPPED or RTE_E_NEVER_RECEIVED
            else if ((RTE_E_OK == retValue_Resp_CCFW)
                    || (RTE_E_COM_STOPPED == retValue_Resp_CCFW)
                    || (RTE_E_NEVER_RECEIVED == retValue_Resp_CCFW))
            {
               //! #### Invoke 'Rte_Call_Event_D1BKB_87_CCFWLink_NoResp_SetEventStatus' with status 'passed' to remove the LIN missing frame fault
               Rte_Call_Event_D1BKB_87_CCFWLink_NoResp_SetEventStatus(DEM_EVENT_STATUS_PASSED);
               //! #### Process fault code information(FCI) to DTC logic : 'SpeedCtrlFreeWheel_CCFW_FCI_to_DTC()'
               IsFciFaultActive = SpeedCtrlFreeWheel_CCFW_FCI_to_DTC(&RteInData_Common.DiagInfoCCFW);
               //! #### Check for 'IsFciFaultActive'
               if (CONST_Fault_Active == IsFciFaultActive)
               {
                  //! #### Process fallback mode logic : 'SpeedControlFreeWheel_FallbackLogic()'
                  SpeedControlFreeWheel_FallbackLogic(&RteOutData_Common.FCWPushButton,
                                                      &RteOutData_Common.LKSPushButton,
                                                      &RteOutData_Common.SpeedControlModeButtonStatus,
                                                      &RteOutData_Common.SpeedControlModeWheelStatus);
               }
               else
               {
                  //! #### Process the signal gateway logic : 'SpeedControlFreeWheel_Signals_GatewayLogic()'
                  SpeedControlFreeWheel_Signals_GatewayLogic(&RteInData_Common,
                                                             &RteOutData_Common);
               }
            }
            else
            {
               //! ##### Process fallback logic : 'SpeedControlFreeWheel_FallbackLogic()'
               SpeedControlFreeWheel_FallbackLogic(&RteOutData_Common.FCWPushButton,
                                                   &RteOutData_Common.LKSPushButton,
                                                   &RteOutData_Common.SpeedControlModeButtonStatus,
                                                   &RteOutData_Common.SpeedControlModeWheelStatus);
            }
         }
         else
         {
            //! ##### Process deactivation logic : 'SpeedControlFreeWheel_Deactivation_Logic()'
            SpeedControlFreeWheel_Deactivation_Logic(&RteOutData_Common);
         }
      }
      else
      {
         //! ###### Process deactivation logic : 'SpeedControlFreeWheel_Deactivation_Logic()'
         SpeedControlFreeWheel_Deactivation_Logic(&RteOutData_Common);
      }
   }
   if ((CONST_Tester_Present == ((Rte_IrvRead_SpeedControlFreeWheel_LINMasCtrl_20ms_runnable_Irv_IOCTL_CCFWLinCtrl()) & CONST_Tester_Present))
      && ((uint8)Diag_Active_TRUE == RteInData_Common.isDiagActive))
   {
      //! ###### Process input output control service logic : 'IOCtrlService_LinOutputSignalControl()'
      IOCtrlService_LinOutputSignalControl((uint8)((Rte_IrvRead_SpeedControlFreeWheel_LINMasCtrl_20ms_runnable_Irv_IOCTL_CCFWLinCtrl()) & CONST_Tester_Notpresent),
                                            &RteOutData_Common);
   }
   else 
   {
      Rte_IrvWrite_SpeedControlFreeWheel_LINMasCtrl_20ms_runnable_Irv_IOCTL_CCFWLinCtrl(DeviceIndication_SpareValue);
   }
   //! ###### Process RTE data write common logic : 'RteDataWrite_Common()'
   RteDataWrite_Common(&RteOutData_Common);
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

#define SpeedControlFreeWheel_LINMasCtrl_STOP_SEC_CODE
#include "SpeedControlFreeWheel_LINMasCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'IOCtrlService_LinOutputSignalControl'
//!
//! \param   OverrideData           Provide override data value
//! \param   *pOut_Struct_Data      Update the output data structure for SpeedControlFreeWheel_LINMastCtrl_Logic
//!
//!======================================================================================
static void IOCtrlService_LinOutputSignalControl(const uint8                                         OverrideData,
                                                       SpeedCtrlFreeWheel_LINMastCtrl_out_StructType *pOut_Struct_Data)
{
   //! ###### Override LIN output signals with IOControl service value
   pOut_Struct_Data->LIN_ACCOrCCIndication    = OverrideData;
   pOut_Struct_Data->LIN_ASLIndication        = OverrideData;
   pOut_Struct_Data->LIN_FCW_DeviceIndication = OverrideData;
   pOut_Struct_Data->LIN_LKS_DeviceIndication = OverrideData;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Get_RteDataRead_Common'
//!
//! \param   *pIn_data   Examine and update the input signals based on RTE failure events
//!
//! \return   Std_ReturnType   Returns 'retValueCCFW' signal RTE read error status
//!
//!======================================================================================
static Std_ReturnType Get_RteDataRead_Common(SpeedCtrlFreeWheel_LINMasCtrl_in_StructType *pIn_data)
{
   //! ###### Read the common RTE interfaces, store the previous value and process fallback modes
   Std_ReturnType retValue     = RTE_E_INVALID;
   Std_ReturnType retValueCCFW = RTE_E_INVALID;

   //! ##### Read isDiagActive interface
   retValue = Rte_Read_DiagActiveState_isDiagActive(&pIn_data->isDiagActive);
   MACRO_StdRteRead_IntRPort((retValue), (pIn_data->isDiagActive), (Diag_Active_FALSE))
   //! ##### Read LIN_SpeedControlModeButtonStat interface
   retValue = Rte_Read_LIN_SpeedControlModeButtonStat_PushButtonStatus(&pIn_data->LIN_SpeedControlModeButtonStat);
   MACRO_StdRteRead_ExtRPort((retValue),(pIn_data->LIN_SpeedControlModeButtonStat), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))
   //! ##### Read LIN_SpeedControlModeWheelStat interface
   retValue = Rte_Read_LIN_SpeedControlModeWheelStat_FreeWheel_Status(&pIn_data->LIN_SpeedControlModeWheelStat);
   MACRO_StdRteRead_ExtRPort((retValue), (pIn_data->LIN_SpeedControlModeWheelStat), (FreeWheel_Status_NotAvailable), (FreeWheel_Status_Error))
   //! ##### Read LIN_LKSPushButton interface
   retValue = Rte_Read_LIN_LKSPushButton_PushButtonStatus(&pIn_data->LIN_LKSPushButton);
   MACRO_StdRteRead_ExtRPort((retValue), (pIn_data->LIN_LKSPushButton), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))
   //! ##### Read LIN_FCWPushButton interface
   retValue = Rte_Read_LIN_FCWPushButton_PushButtonStatus(&pIn_data->LIN_FCWPushButton);
   MACRO_StdRteRead_ExtRPort((retValue), (pIn_data->LIN_FCWPushButton), (PushButtonStatus_NotAvailable), (PushButtonStatus_Error))
   //! ##### Read ACCOrCCIndication interface
   retValue = Rte_Read_ACCOrCCIndication_DeviceIndication(&pIn_data->ACCOrCCIndication);
   MACRO_StdRteRead_IntRPort((retValue), (pIn_data->ACCOrCCIndication), (DeviceIndication_Off))
   //! ##### Read ASLIndication interface
   retValue = Rte_Read_ASLIndication_DeviceIndication(&pIn_data->ASLIndication);
   MACRO_StdRteRead_IntRPort((retValue), (pIn_data->ASLIndication),( DeviceIndication_Off))
   //! ##### Read FCW_DeviceIndication interface
   retValue = Rte_Read_FCW_DeviceIndication_DeviceIndication(&pIn_data->FCW_DeviceIndication);
   MACRO_StdRteRead_IntRPort((retValue), (pIn_data->FCW_DeviceIndication),( DeviceIndication_Off))
   //! ##### Read LKS_DeviceIndication interface
   retValue = Rte_Read_LKS_DeviceIndication_DeviceIndication(&pIn_data->LKS_DeviceIndication);
   MACRO_StdRteRead_IntRPort((retValue), (pIn_data->LKS_DeviceIndication), (DeviceIndication_Off))
   //! ##### Read ResponseErrorCCFW interface
   retValueCCFW = Rte_Read_ResponseErrorCCFW_ResponseErrorCCFW(&pIn_data->CCFW_ResponseError);
   MACRO_StdRteRead_ExtRPort((retValueCCFW), (pIn_data->CCFW_ResponseError), (1U), (1U))
   //! ##### Read DiagInfoCCFW interface
   retValue = Rte_Read_DiagInfoCCFW_DiagInfo(&pIn_data->DiagInfoCCFW);
   MACRO_StdRteRead_ExtRPort((retValue), (pIn_data->DiagInfoCCFW), (0U), (0U))
   //! ##### Read ComMode_LIN4 interface
   retValue = Rte_Read_ComMode_LIN4_ComMode_LIN(&pIn_data->ComMode_LIN4);
   MACRO_StdRteRead_IntRPort((retValue), (pIn_data->ComMode_LIN4), (Error))
   return retValueCCFW;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'RteDataWrite_Common'
//!
//! \param   *pOut_data   Update the output signals to ports of output structure
//!
//!======================================================================================
static void RteDataWrite_Common(const SpeedCtrlFreeWheel_LINMastCtrl_out_StructType *pOut_data)
{
   //! ###### Write all the output ports
   Rte_Write_SpeedControlModeButtonStatus_PushButtonStatus(pOut_data->SpeedControlModeButtonStatus);
   Rte_Write_SpeedControlModeWheelStatus_FreeWheel_Status(pOut_data->SpeedControlModeWheelStatus);
   Rte_Write_LKSPushButton_PushButtonStatus(pOut_data->LKSPushButton);
   Rte_Write_FCWPushButton_PushButtonStatus(pOut_data->FCWPushButton);
   Rte_Write_LIN_ACCOrCCIndication_DeviceIndication(pOut_data->LIN_ACCOrCCIndication);
   Rte_Write_LIN_ASLIndication_DeviceIndication(pOut_data->LIN_ASLIndication);
   Rte_Write_LIN_FCW_DeviceIndication_DeviceIndication(pOut_data->LIN_FCW_DeviceIndication);
   Rte_Write_LIN_LKS_DeviceIndication_DeviceIndication(pOut_data->LIN_LKS_DeviceIndication);
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'SpeedControlFreeWheel_Signals_GatewayLogic'
//!
//! \param   *pSpeedCtrlFreeWheel_in_data    Providing the input signals to send to output ports
//! \param   *pSpeedCtrlFreeWheel_out_data   Updating the output ports with input signals
//!
//!======================================================================================
static void SpeedControlFreeWheel_Signals_GatewayLogic(const SpeedCtrlFreeWheel_LINMasCtrl_in_StructType   *pSpeedCtrlFreeWheel_in_data,
                                                             SpeedCtrlFreeWheel_LINMastCtrl_out_StructType *pSpeedCtrlFreeWheel_out_data)
{
   //! ###### Update the output ports with values read on input ports 
   //Signals gateway, HMI status from CCFW 
   pSpeedCtrlFreeWheel_out_data->SpeedControlModeButtonStatus = pSpeedCtrlFreeWheel_in_data->LIN_SpeedControlModeButtonStat;
   pSpeedCtrlFreeWheel_out_data->SpeedControlModeWheelStatus  = pSpeedCtrlFreeWheel_in_data->LIN_SpeedControlModeWheelStat;
   pSpeedCtrlFreeWheel_out_data->LKSPushButton                = pSpeedCtrlFreeWheel_in_data->LIN_LKSPushButton;  
   pSpeedCtrlFreeWheel_out_data->FCWPushButton                = pSpeedCtrlFreeWheel_in_data->LIN_FCWPushButton;  
   //Signals gateway, status indication on CCFW 
   pSpeedCtrlFreeWheel_out_data->LIN_ACCOrCCIndication        = pSpeedCtrlFreeWheel_in_data->ACCOrCCIndication;
   pSpeedCtrlFreeWheel_out_data->LIN_ASLIndication            = pSpeedCtrlFreeWheel_in_data->ASLIndication;
   pSpeedCtrlFreeWheel_out_data->LIN_FCW_DeviceIndication     = pSpeedCtrlFreeWheel_in_data->FCW_DeviceIndication;
   pSpeedCtrlFreeWheel_out_data->LIN_LKS_DeviceIndication     = pSpeedCtrlFreeWheel_in_data->LKS_DeviceIndication;  
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'SpeedControlFreeWheel_FallbackLogic'
//!
//! \param   *pFCWPushButton                 Update the FCW push button value to error
//! \param   *pLKSPushButton                 Update the LKS push button value to error
//! \param   *pSpeedControlModeButtonStatus  Update the speed control mode button status value to error
//! \param   *pSpeedControlModeWheelStatus   Update the speed control mode wheel status value to error
//!
//!======================================================================================
static void SpeedControlFreeWheel_FallbackLogic(PushButtonStatus_T  *pFCWPushButton,
                                                PushButtonStatus_T  *pLKSPushButton,
                                                PushButtonStatus_T  *pSpeedControlModeButtonStatus,
                                                FreeWheel_Status_T  *pSpeedControlModeWheelStatus)
{
   //! ###### Updating the output ports with error
   *pFCWPushButton                = PushButtonStatus_Error;
   *pLKSPushButton                = PushButtonStatus_Error;
   *pSpeedControlModeButtonStatus = PushButtonStatus_Error;
   *pSpeedControlModeWheelStatus  = FreeWheel_Status_Error;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'SpeedCtrlFreeWheel_CCFW_FCI_to_DTC'
//!
//! \param   *pDiagInfoCCFW   Providing the diagnostic information of CCFW
//!
//! \return  boolean    Returns 'IsFciFaultActive' value
//!
//!======================================================================================
static boolean SpeedCtrlFreeWheel_CCFW_FCI_to_DTC(const DiagInfo_T *pDiagInfoCCFW)
{

   static uint8 SlaveFCIFault[7] = { CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive,
                                     CONST_SlaveFCIFault_InActive };
   boolean IsFciFaultActive      = CONST_Fault_InActive;
   uint8 Index                   = 0U;

   //! ###### Check the FCI conditions and log/remove the faults
   if ((DiagInfo_T)0x41 == *pDiagInfoCCFW)
   {
      //! ##### Set supply voltage too low fault event to fail
      Rte_Call_Event_D1BN1_16_CCFW_VBT_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x01 == *pDiagInfoCCFW)
   {
      //! ##### Set supply voltage too low fault event to pass
      Rte_Call_Event_D1BN1_16_CCFW_VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x42 == *pDiagInfoCCFW)
   {
      //! ##### Set supply voltage too high fault event to fail
      Rte_Call_Event_D1BN1_17_CCFW_VAT_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x02 == *pDiagInfoCCFW)
   {
      //! ##### Set supply voltage too high fault event to pass
      Rte_Call_Event_D1BN1_17_CCFW_VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
    else if ((DiagInfo_T)0x43 == *pDiagInfoCCFW)
   {
      //! ##### Set ROM CS error fault event to fail
      SlaveFCIFault[0] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BN1_46_CCFW_EEPROM_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x03 == *pDiagInfoCCFW)
   {
      //! ##### Set ROM CS error fault event to pass
      SlaveFCIFault[0] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BN1_46_CCFW_EEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x44 == *pDiagInfoCCFW)
   {
      //! ##### Set RAM check fault event to fail
      SlaveFCIFault[1] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BN1_44_CCFW_RAM_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      // set Failure Active Flag 
   }
   else if ((DiagInfo_T)0x04 == *pDiagInfoCCFW)
   {
      //! ##### Set RAM check fault event to pass
      SlaveFCIFault[1] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BN1_44_CCFW_RAM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x45 == *pDiagInfoCCFW)
   {
      //! ##### Set watchdog error fault event to fail
      SlaveFCIFault[2] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x05 == *pDiagInfoCCFW)
   {
      //! ##### Set watchdog error fault event to pass
      SlaveFCIFault[2] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x46 == *pDiagInfoCCFW)
   {
      //! ##### Set overrun fault event to fail
      SlaveFCIFault[3] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x06 == *pDiagInfoCCFW)
   {
      //! ##### Set overrun fault event to pass
      SlaveFCIFault[3] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x47 == *pDiagInfoCCFW)
   {
      //! ##### Set signal error fault event to fail
      SlaveFCIFault[4] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x07 == *pDiagInfoCCFW)
   {
      //! ##### Set signal error fault event to pass
       SlaveFCIFault[4] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x48 == *pDiagInfoCCFW)
   {
      //! ##### Set unexpected operation fault event to fail
      SlaveFCIFault[5] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x08 == *pDiagInfoCCFW)
   {
      //! ##### Set unexpected operation fault event to pass
      SlaveFCIFault[5] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x49 == *pDiagInfoCCFW)
   {
      //! ##### Set flash error fault event to fail
      SlaveFCIFault[6] = CONST_SlaveFCIFault_Active;
      Rte_Call_Event_D1BN1_45_CCFW_FLASH_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x09 == *pDiagInfoCCFW)
   {
      //! ##### Set flash error fault event to pass
      SlaveFCIFault[6] = CONST_SlaveFCIFault_InActive;
      Rte_Call_Event_D1BN1_45_CCFW_FLASH_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if((DiagInfo_T)0x00 == *pDiagInfoCCFW)
   {

     for(Index = 0U;Index < 7U;Index++)
     {
         SlaveFCIFault[Index] = CONST_SlaveFCIFault_InActive;
     }
     Rte_Call_Event_D1BN1_45_CCFW_FLASH_SetEventStatus(DEM_EVENT_STATUS_PASSED);
     Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_SetEventStatus(DEM_EVENT_STATUS_PASSED);
     Rte_Call_Event_D1BN1_44_CCFW_RAM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
     Rte_Call_Event_D1BN1_46_CCFW_EEPROM_SetEventStatus(DEM_EVENT_STATUS_PASSED);
     Rte_Call_Event_D1BN1_17_CCFW_VAT_SetEventStatus(DEM_EVENT_STATUS_PASSED);
     Rte_Call_Event_D1BN1_16_CCFW_VBT_SetEventStatus(DEM_EVENT_STATUS_PASSED);    
   }
   else
   {
      // Do nothing, keep previous status
   }
   IsFciFaultActive = CONST_Fault_InActive;
   for(Index = 0U;Index < 7U;Index++)
   {
      if(CONST_SlaveFCIFault_Active == SlaveFCIFault[Index])
      {
         IsFciFaultActive = CONST_Fault_Active;
      }
      else
      {
         // Do Nothing
      }
   }
   return IsFciFaultActive;
}
//!======================================================================================
//!
//! \brief 
//! This function implements the logic for the 'SpeedControlFreeWheel_Deactivation_Logic'
//!
//! \param   *pOutput_data   Update the output structure to off or notavailable
//!
//!======================================================================================
static void SpeedControlFreeWheel_Deactivation_Logic(SpeedCtrlFreeWheel_LINMastCtrl_out_StructType *pOutput_data)
{
   //! ###### Write all the output ports to value 'Not available' or 'Off'
   pOutput_data->FCWPushButton                = PushButtonStatus_NotAvailable;
   pOutput_data->LKSPushButton                = PushButtonStatus_NotAvailable;
   pOutput_data->SpeedControlModeButtonStatus = PushButtonStatus_NotAvailable;
   pOutput_data->SpeedControlModeWheelStatus  = FreeWheel_Status_NotAvailable;
   pOutput_data->LIN_ACCOrCCIndication        = DeviceIndication_Off;
   pOutput_data->LIN_ASLIndication            = DeviceIndication_Off;
   pOutput_data->LIN_FCW_DeviceIndication     = DeviceIndication_Off;
   pOutput_data->LIN_LKS_DeviceIndication     = DeviceIndication_Off;
}

//! @}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
