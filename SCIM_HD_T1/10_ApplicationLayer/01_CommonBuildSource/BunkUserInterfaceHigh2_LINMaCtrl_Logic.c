/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file BunkUserInterfaceHigh2_LINMaCtrl_Logic.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer
//! @{
//! @addtogroup Application_BodyAndComfort
//! @{
//! @addtogroup Application_Living
//! @{
//! @addtogroup BunkUserInterfaceHigh2_LINMaCtrl
//! @{
//!
//! \brief
//! BunkUserInterfaceHigh2_LINMaCtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the BunkUserInterfaceHigh2_LINMaCtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
// Includes
 
#include "Rte_BunkUserInterfaceHigh2_LINMaCtrl.h"
#include "FuncLibrary_AnwStateMachine_If.h"
#include "BunkUserInterfaceHigh2_LINMaCtrl_If.h"
#include "BunkUserInterfaceHigh2_LINMaCtrl_Logic_If.h"
#include "BunkUserInterfaceHigh2_LINMaCtrl_Logic.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Generic_ANW_AudioRadio2_CondCheck'
//!
//! \param   *pBunkH2_WindowLift_data   Providing the current status of input data
//! \param   *pIn_ButtonCOM_data        Providing the current button status 
//!
//! \return   FormalBoolean             Returns 'isActTrigDet' status
//! 
//!======================================================================================
FormalBoolean Generic_ANW_AudioRadio2_CondCheck(const BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_in_StructType *pBunkH2_WindowLift_data,
                                                const BunkUserInterfaceHigh2_LINMaCtrl_ButtonCOM_in_StructType  *pIn_ButtonCOM_data)
{
   FormalBoolean isActTrigDet = NO;

   //! ###### Check if any button is selected
   if (((pBunkH2_WindowLift_data->LIN_BunkH2PowerWinCloseDSBtn_s.Current != pBunkH2_WindowLift_data->LIN_BunkH2PowerWinCloseDSBtn_s.Previous)
      && (PushButtonStatus_Pushed == pBunkH2_WindowLift_data->LIN_BunkH2PowerWinCloseDSBtn_s.Current))
      || ((pBunkH2_WindowLift_data->LIN_BunkH2PowerWinClosePSBtn_s.Current != pBunkH2_WindowLift_data->LIN_BunkH2PowerWinClosePSBtn_s.Previous)
      && (PushButtonStatus_Pushed == pBunkH2_WindowLift_data->LIN_BunkH2PowerWinClosePSBtn_s.Current))
      || ((pBunkH2_WindowLift_data->LIN_BunkH2PowerWinOpenDSBtn_st.Current != pBunkH2_WindowLift_data->LIN_BunkH2PowerWinOpenDSBtn_st.Previous)
      && (PushButtonStatus_Pushed == pBunkH2_WindowLift_data->LIN_BunkH2PowerWinOpenDSBtn_st.Current))
      || ((pBunkH2_WindowLift_data->LIN_BunkH2PowerWinOpenPSBtn_st.Current != pBunkH2_WindowLift_data->LIN_BunkH2PowerWinOpenPSBtn_st.Previous)
      && (PushButtonStatus_Pushed == pBunkH2_WindowLift_data->LIN_BunkH2PowerWinOpenPSBtn_st.Current))
      || ((pIn_ButtonCOM_data->LIN_BunkH2IntLightActvnBtn_sta.Current != pIn_ButtonCOM_data->LIN_BunkH2IntLightActvnBtn_sta.Previous)
      && (PushButtonStatus_Pushed == pIn_ButtonCOM_data->LIN_BunkH2IntLightActvnBtn_sta.Current))
      || ((pIn_ButtonCOM_data->LIN_BunkH2IntLightDecBtn_stat.Current != pIn_ButtonCOM_data->LIN_BunkH2IntLightDecBtn_stat.Previous)
      && (PushButtonStatus_Pushed == pIn_ButtonCOM_data->LIN_BunkH2IntLightDecBtn_stat.Current))
      || ((pIn_ButtonCOM_data->LIN_BunkH2IntLightIncBtn_stat.Current != pIn_ButtonCOM_data->LIN_BunkH2IntLightIncBtn_stat.Previous)
      && (PushButtonStatus_Pushed == pIn_ButtonCOM_data->LIN_BunkH2IntLightIncBtn_stat.Current))
      ||((pIn_ButtonCOM_data->LIN_BunkH2LockButtonStatus.Current != pIn_ButtonCOM_data->LIN_BunkH2LockButtonStatus.Previous)
      && (PushButtonStatus_Pushed == pIn_ButtonCOM_data->LIN_BunkH2LockButtonStatus.Current))
      || ((pIn_ButtonCOM_data->LIN_BunkH2RoofhatchCloseBtn_St.Current != pIn_ButtonCOM_data->LIN_BunkH2RoofhatchCloseBtn_St.Previous)
      && (PushButtonStatus_Pushed == pIn_ButtonCOM_data->LIN_BunkH2RoofhatchCloseBtn_St.Current))
      || ((pIn_ButtonCOM_data->LIN_BunkH2RoofhatchOpenBtn_Sta.Current != pIn_ButtonCOM_data->LIN_BunkH2RoofhatchOpenBtn_Sta.Previous)
      && (PushButtonStatus_Pushed == pIn_ButtonCOM_data->LIN_BunkH2RoofhatchOpenBtn_Sta.Current))
      || ((pIn_ButtonCOM_data->LIN_BunkH2OnOFF_ButtonStatus.Current != pIn_ButtonCOM_data->LIN_BunkH2OnOFF_ButtonStatus.Previous)
      && (PushButtonStatus_Pushed == pIn_ButtonCOM_data->LIN_BunkH2OnOFF_ButtonStatus.Current))
      || ((pIn_ButtonCOM_data->LIN_BunkH2ParkHeater_ButtonSta.Current != pIn_ButtonCOM_data->LIN_BunkH2ParkHeater_ButtonSta.Previous)
      && (PushButtonStatus_Pushed == pIn_ButtonCOM_data->LIN_BunkH2ParkHeater_ButtonSta.Current))
      || ((pIn_ButtonCOM_data->LIN_BunkH2TempDec_ButtonStatus.Current != pIn_ButtonCOM_data->LIN_BunkH2TempDec_ButtonStatus.Previous)
      && (PushButtonStatus_Pushed == pIn_ButtonCOM_data->LIN_BunkH2TempDec_ButtonStatus.Current))
      || ((pIn_ButtonCOM_data->LIN_BunkH2TempInc_ButtonStatus.Current != pIn_ButtonCOM_data->LIN_BunkH2TempInc_ButtonStatus.Previous)
      && (PushButtonStatus_Pushed == pIn_ButtonCOM_data->LIN_BunkH2TempInc_ButtonStatus.Current))
      || ((pIn_ButtonCOM_data->LIN_BunkH2AudioOnOff_ButtonSta.Current != pIn_ButtonCOM_data->LIN_BunkH2AudioOnOff_ButtonSta.Previous)
      && (PushButtonStatus_Pushed == pIn_ButtonCOM_data->LIN_BunkH2AudioOnOff_ButtonSta.Current))
      || ((pIn_ButtonCOM_data->LIN_BunkH2Fade_ButtonStatus.Current != pIn_ButtonCOM_data->LIN_BunkH2Fade_ButtonStatus.Previous)
      && (PushButtonStatus_Pushed == pIn_ButtonCOM_data->LIN_BunkH2Fade_ButtonStatus.Current))
      || ((pIn_ButtonCOM_data->LIN_BunkH2Phone_ButtonStatus.Current != pIn_ButtonCOM_data->LIN_BunkH2Phone_ButtonStatus.Previous)
      && (PushButtonStatus_Pushed == pIn_ButtonCOM_data->LIN_BunkH2Phone_ButtonStatus.Current))
      || ((pIn_ButtonCOM_data->LIN_BunkH2VolumeDown_ButtonSta.Current != pIn_ButtonCOM_data->LIN_BunkH2VolumeDown_ButtonSta.Previous)
      && (PushButtonStatus_Pushed == pIn_ButtonCOM_data->LIN_BunkH2VolumeDown_ButtonSta.Current))
      || ((pIn_ButtonCOM_data->LIN_BunkH2VolumeUp_ButtonStatu.Current != pIn_ButtonCOM_data->LIN_BunkH2VolumeUp_ButtonStatu.Previous)
      && (PushButtonStatus_Pushed == pIn_ButtonCOM_data->LIN_BunkH2VolumeUp_ButtonStatu.Current)))
   {
      isActTrigDet = YES;
   }
   else
   {
      isActTrigDet = NO;
   }
   return isActTrigDet;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Generic_ANW_ClimPHTimerSettings3_CondCheck'
//!
//! \param   *pVarBunkH2PHTi_rqs   Providing the current values of input data
//!
//! \return   FormalBoolean        Returns 'isActTrigDet' status
//!
//!======================================================================================
FormalBoolean Generic_ANW_ClimPHTimerSettings3_CondCheck(const SetParkHtrTmr_rqst *pVarBunkH2PHTi_rqs)
{
   FormalBoolean isActTrigDet = NO;

   //! ###### Check LIN bunk high2 PH timer request button status
   if (((ParkHeaterTimer_cmd_TimerEnable == pVarBunkH2PHTi_rqs->Timer_cmd_RE.previous)
      && (ParkHeaterTimer_cmd_TimerDisable == pVarBunkH2PHTi_rqs->Timer_cmd_RE.current))
      || ((pVarBunkH2PHTi_rqs->StartTimeMin_RE.previous != pVarBunkH2PHTi_rqs->StartTimeMin_RE.current)
      && ((pVarBunkH2PHTi_rqs->StartTimeMin_RE.current >= CONST_LeftLimit_HR)
      && (pVarBunkH2PHTi_rqs->StartTimeMin_RE.current <= CONST_RightLimit_HR)))
      || ((pVarBunkH2PHTi_rqs->StartTimeHr_RE.previous != pVarBunkH2PHTi_rqs->StartTimeHr_RE.current)
      && ((pVarBunkH2PHTi_rqs->StartTimeHr_RE.current >= CONST_LeftLimit_MIN)
      && (pVarBunkH2PHTi_rqs->StartTimeHr_RE.current <= CONST_RightLimit_MIN)))
      || ((pVarBunkH2PHTi_rqs->DurnTimeHr_RE.previous != pVarBunkH2PHTi_rqs->DurnTimeHr_RE.current)
      && ((pVarBunkH2PHTi_rqs->DurnTimeHr_RE.current >= CONST_LeftLimit_HR)
      && (pVarBunkH2PHTi_rqs->DurnTimeHr_RE.current <= CONST_RightLimit_HR)))
      || ((pVarBunkH2PHTi_rqs->DurnTimeMin_RE.previous != pVarBunkH2PHTi_rqs->DurnTimeMin_RE.current)
      && (pVarBunkH2PHTi_rqs->DurnTimeMin_RE.current >= CONST_LeftLimit_MIN)
      && (pVarBunkH2PHTi_rqs->DurnTimeMin_RE.current <= CONST_RightLimit_MIN)))
   {
      isActTrigDet = YES;
   }
   else
   {
      isActTrigDet = NO;
   }
   return isActTrigDet;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Generic_ANW_LockCntrlCabRqst2_CondCheck'
//!
//! \param   *pLIN_BunkH2LockButtonStatus   Providing the bunk high2 lock button status
//!
//! \return   FormalBoolean                 Returns 'isActTrigDet' status
//!
//!======================================================================================
FormalBoolean Generic_ANW_LockCntrlCabRqst2_CondCheck(const PushButtonStatus  *pLIN_BunkH2LockButtonStatus)
{
   FormalBoolean isActTrigDet = NO;

   //! ###### Select the lock control cab request2 button
   if ((pLIN_BunkH2LockButtonStatus->Current != pLIN_BunkH2LockButtonStatus->Previous)
      && (PushButtonStatus_Pushed == pLIN_BunkH2LockButtonStatus->Current))
   {
      isActTrigDet = YES;
   }
   else
   {
      isActTrigDet = NO;
   }
   return isActTrigDet;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Generic_ANW_OtherInteriorLights4_CondCheck'
//!
//! \param   *pLIN_BunkH2IntLightDecBtn_stat    Providing the LIN_BunkH2IntLightDec Button Status
//! \param   *pLIN_BunkH2IntLightIncBtn_stat    Providing the LIN_BunkH2IntLightInc Button Status
//! \param   *pLIN_BunkH2IntLightActvnBtn_sta   Providing the LIN_BunkH2IntLightActvn Button Status
//!
//! \return   FormalBoolean                    Returns 'isActTrigDet' status
//!
//!======================================================================================
FormalBoolean Generic_ANW_OtherInteriorLights4_CondCheck(const PushButtonStatus *pLIN_BunkH2IntLightDecBtn_stat,
                                                         const PushButtonStatus *pLIN_BunkH2IntLightIncBtn_stat,
                                                         const PushButtonStatus *pLIN_BunkH2IntLightActvnBtn_sta)
{
   FormalBoolean isActTrigDet = NO;

   //! ###### Select the other interior lights4 button
   if (((pLIN_BunkH2IntLightDecBtn_stat->Current != pLIN_BunkH2IntLightDecBtn_stat->Previous)
      && (PushButtonStatus_Pushed == pLIN_BunkH2IntLightDecBtn_stat->Current))
      || ((pLIN_BunkH2IntLightIncBtn_stat->Current != pLIN_BunkH2IntLightIncBtn_stat->Previous)
      && (PushButtonStatus_Pushed == pLIN_BunkH2IntLightIncBtn_stat->Current))
      || ((pLIN_BunkH2IntLightActvnBtn_sta->Current != pLIN_BunkH2IntLightActvnBtn_sta->Previous)
      && (PushButtonStatus_Pushed == pLIN_BunkH2IntLightActvnBtn_sta->Current)))
   {
      isActTrigDet = YES;
   }
   else
   {
      isActTrigDet = NO;
   }
   return isActTrigDet;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Generic_ANW_PHActMaintainLiving6_Activate_CondCheck'
//!
//! \param   *pLIN_BunkH2OnOFF_ButtonStatus     Providing the current Button Status
//! \param   *pLIN_BunkH2ParkHeater_ButtonSta   Providing the LIN_BunkH2ParkHeater button status
//! \param   *pLIN_BunkH2TempDec_ButtonStatus   Providing the LIN_BunkH2TempDec button status
//! \param   *pLIN_BunkH2TempInc_ButtonStatus   Providing the LIN_BunkH2TempInc button status
//!
//! \return   FormalBoolean                     Returns 'isActTrigDet' status
//!
//!======================================================================================
FormalBoolean Generic_ANW_PHActMaintainLiving6_Activate_CondCheck(const PushButtonStatus  *pLIN_BunkH2OnOFF_ButtonStatus,
                                                                  const PushButtonStatus  *pLIN_BunkH2ParkHeater_ButtonSta,
                                                                  const PushButtonStatus  *pLIN_BunkH2TempDec_ButtonStatus,
                                                                  const PushButtonStatus  *pLIN_BunkH2TempInc_ButtonStatus)
{
   FormalBoolean isActTrigDet = NO;

   //! ###### Select the PHActMaintainLiving6Act button 
   if (((pLIN_BunkH2OnOFF_ButtonStatus->Current != pLIN_BunkH2OnOFF_ButtonStatus->Previous)
      && (PushButtonStatus_Pushed == pLIN_BunkH2OnOFF_ButtonStatus->Current))
      || ((pLIN_BunkH2ParkHeater_ButtonSta->Current != pLIN_BunkH2ParkHeater_ButtonSta->Previous)
      && (PushButtonStatus_Pushed == pLIN_BunkH2ParkHeater_ButtonSta->Current))
      || ((pLIN_BunkH2TempDec_ButtonStatus->Current != pLIN_BunkH2TempDec_ButtonStatus->Previous)
      && (PushButtonStatus_Pushed == pLIN_BunkH2TempDec_ButtonStatus->Current))
      || ((pLIN_BunkH2TempInc_ButtonStatus->Current != pLIN_BunkH2TempInc_ButtonStatus->Previous)
      && (PushButtonStatus_Pushed == pLIN_BunkH2TempInc_ButtonStatus->Current)))
   {
      isActTrigDet = YES;
   }
   else
   {
      isActTrigDet = NO;
   }
   return isActTrigDet;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Generic_ANW_PHActMaintainLvng6_DeActivate_CondCheck'
//!
//! \param   *pLIN_BunkH2TempInc_ButtonStatus   Providing the LIN_BunkH2TempInc button status
//! \param   *pLIN_BunkH2TempDec_ButtonStatus   Providing the LIN_BunkH2TempDec button status
//!
//! \return   FormalBoolean                      Returns 'isDeActTrigDet' Status
//!
//!======================================================================================
FormalBoolean Generic_ANW_PHActMaintainLvng6_DeActivate_CondCheck(const PushButtonStatus *pLIN_BunkH2TempInc_ButtonStatus,
                                                                  const PushButtonStatus *pLIN_BunkH2TempDec_ButtonStatus)
{
   FormalBoolean isDeActTrigDet = NO;

   //! ###### Check PHActMaintainLiving6 button status
   if ((PushButtonStatus_Pushed != pLIN_BunkH2TempInc_ButtonStatus->Current)
      && (PushButtonStatus_Pushed != pLIN_BunkH2TempDec_ButtonStatus->Current))
   {
      isDeActTrigDet = YES;
   }
   else
   {
      isDeActTrigDet = NO;
   }
   return isDeActTrigDet;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Genric_ANW_PowerWindowsActivate3_Activate_CondCheck'
//!
//! \param   *pin_Button_data   Providing the current button status
//!
//! \return   FormalBoolean     Returns 'isActTrigDet' status
//!
//!======================================================================================
FormalBoolean Genric_ANW_PowerWindowsActivate3_Activate_CondCheck(const BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_in_StructType *pin_Button_data)
{
   FormalBoolean isActTrigDet = NO;
   
   //! ###### Select the power windows button 
   if (((pin_Button_data->LIN_BunkH2PowerWinOpenDSBtn_st.Current != pin_Button_data->LIN_BunkH2PowerWinOpenDSBtn_st.Previous)
      && (PushButtonStatus_Pushed == pin_Button_data->LIN_BunkH2PowerWinOpenDSBtn_st.Current))
      || ((pin_Button_data->LIN_BunkH2PowerWinCloseDSBtn_s.Current!= pin_Button_data->LIN_BunkH2PowerWinCloseDSBtn_s.Previous)
      && (PushButtonStatus_Pushed == pin_Button_data->LIN_BunkH2PowerWinCloseDSBtn_s.Current))
      || ((pin_Button_data->LIN_BunkH2PowerWinOpenPSBtn_st.Current != pin_Button_data->LIN_BunkH2PowerWinOpenPSBtn_st.Previous)
      && (PushButtonStatus_Pushed == pin_Button_data->LIN_BunkH2PowerWinOpenPSBtn_st.Current))
      || ((pin_Button_data->LIN_BunkH2PowerWinClosePSBtn_s.Current != pin_Button_data->LIN_BunkH2PowerWinClosePSBtn_s.Previous)
      && (PushButtonStatus_Pushed == pin_Button_data->LIN_BunkH2PowerWinClosePSBtn_s.Current)))
   {
      isActTrigDet = YES;
   }
   else
   {
      isActTrigDet = NO;
   }
   return isActTrigDet;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Genric_ANW_PowerWindowActivate3_DeActivate_CondCheck'
//!
//! \param   *pin_ButtonCOM_data   Providing the current button status
//!
//! \return   FormalBoolean        Returns 'isDeActTrigDet' status
//!
//!======================================================================================
FormalBoolean Genric_ANW_PowerWindowActivate3_DeActivate_CondCheck(const BunkUserInterfaceHigh2_LINMaCtrl_WindowLift_in_StructType *pin_ButtonCOM_data)
{
   FormalBoolean isDeActTrigDet = NO;

   //! ###### Check for power windows button status
   if ((PushButtonStatus_Pushed != pin_ButtonCOM_data->LIN_BunkH2PowerWinOpenDSBtn_st.Current)
      && (PushButtonStatus_Pushed != pin_ButtonCOM_data->LIN_BunkH2PowerWinCloseDSBtn_s.Current)
      && (PushButtonStatus_Pushed != pin_ButtonCOM_data->LIN_BunkH2PowerWinOpenPSBtn_st.Current)
      && (PushButtonStatus_Pushed != pin_ButtonCOM_data->LIN_BunkH2PowerWinClosePSBtn_s.Current))
   {
      isDeActTrigDet = YES;
   }
   else
   {
      isDeActTrigDet = NO;
   }
   return isDeActTrigDet;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Generic_ANW_PowerWindowsActivate3_Trigger'
//!
//! \param   isRestAct_PowerWindowsActivate3     Provides 'yes' or 'no' values based on restriction activation
//! \param   isActTrig_PowerWindowsActivate3     Provides 'yes' or 'no' values based on activation trigger
//! \param   isDeActTrig_PowerWindowsActivate3   Provides 'yes' or 'no' values based on deactivation trigger
//!
//!======================================================================================
void Generic_ANW_PowerWindowsActivate3_Trigger(const FormalBoolean isRestAct_PowerWindowsActivate3,
                                               const FormalBoolean isActTrig_PowerWindowsActivate3,
                                               const FormalBoolean isDeActTrig_PowerWindowsActivate3)
{
   //! ###### Processing the application network (activation/deactivation)conditions
   static AnwSM_States AnwState_Powerwindow                    = { SM_ANW_Inactive,
                                                                   SM_ANW_Inactive };
   static DeactivationTimer_Type PowerWindow_DeactivationTimer = 0U;
   AnwAction_Enum AnwActionstatus                              = ANW_Action_None;
   //! ##### Process ANW logic : 'ProcessAnwLogic()'
   AnwActionstatus = ProcessAnwLogic(isRestAct_PowerWindowsActivate3,
                                     isActTrig_PowerWindowsActivate3,
                                     isDeActTrig_PowerWindowsActivate3,
                                     CONST_ANW_PowerWindowsActivate3_Timeout,
                                     &PowerWindow_DeactivationTimer,
                                     &AnwState_Powerwindow);
   //! ##### Check for Anw action is 'Activate' or 'Deactivate' for generic ANW power windows activate3 trigger
   if (ANW_Action_Activate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_PowerWindowsActivate3_ActivateIss();
   }
   else if (ANW_Action_Deactivate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_PowerWindowsActivate3_DeactivateIss();
   }
   else
   {
      // Do nothing, keep Previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Generic_ANW_RoofHatchRequest3_Activate_CondCheck'
//!
//! \param   *pBunkH2RoofhatchCloseBtn_St   Providing BunkH2RoofhatchClose button status
//! \param   *pBunkH2RoofhatchOpenBtn_Sta   Providing BunkH2RoofhatchOpen button status
//!
//! \return   FormalBoolean                  Returns 'isActTrigDet' Status
//!
//!======================================================================================
FormalBoolean Generic_ANW_RoofHatchRequest3_Activate_CondCheck(const PushButtonStatus *pBunkH2RoofhatchCloseBtn_St,
                                                               const PushButtonStatus *pBunkH2RoofhatchOpenBtn_Sta)
{
   FormalBoolean isActTrigDet = NO;

   //! ###### Select BunkH2RoofhatchClose or BunkH2RoofhatchOpen button
   if (((pBunkH2RoofhatchCloseBtn_St->Current != pBunkH2RoofhatchCloseBtn_St->Previous)
      && (PushButtonStatus_Pushed == pBunkH2RoofhatchCloseBtn_St->Current))
      || ((pBunkH2RoofhatchOpenBtn_Sta->Current != pBunkH2RoofhatchOpenBtn_Sta->Previous)
      && (PushButtonStatus_Pushed == pBunkH2RoofhatchOpenBtn_Sta->Current)))
   {
      isActTrigDet = YES;
   }
   else
   {
      isActTrigDet = NO;
   }
   return isActTrigDet;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Generic_ANW_RoofHatchRequest3_DeActivate_CondCheck'
//!
//! \param   *pBunkH2RoofhatchCloseBtn_St   Providing bunk high2 roof hatch close button status
//! \param   *pBunkH2RoofhatchOpenBtn_Sta   Providing bunk high2 roof hatch open button status
//!
//! \return   FormalBoolean                 Returns 'isDeActTrigDet' status
//!
//!======================================================================================
FormalBoolean Generic_ANW_RoofHatchRequest3_DeActivate_CondCheck(const PushButtonStatus *pBunkH2RoofhatchCloseBtn_St,
                                                                 const PushButtonStatus *pBunkH2RoofhatchOpenBtn_Sta)
{
   FormalBoolean isDeActTrigDet = NO;
   
   //! ###### Check for BunkH2RoofhatchClose and BunkH2RoofhatchOpen button status
   if ((PushButtonStatus_Pushed != pBunkH2RoofhatchCloseBtn_St->Current)
      && (PushButtonStatus_Pushed != pBunkH2RoofhatchOpenBtn_Sta->Current))
   {
      isDeActTrigDet = YES;
   }
   else
   {
      isDeActTrigDet = NO;
   }
   return isDeActTrigDet;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Generic_ANW_RoofHatchRequest3_Trigger'
//!
//! \param   isRestAct_RoofHatchRequest3     Provides 'yes' or 'no' values based on restriction activation
//! \param   isActTrig_RoofHatchRequest3     Provides 'yes' or 'no' values based on activation trigger
//! \param   isDeActTrig_RoofHatchRequest3   Provides 'yes' or 'no' values based on deactivation trigger
//!
//!======================================================================================
void Generic_ANW_RoofHatchRequest3_Trigger(const FormalBoolean isRestAct_RoofHatchRequest3,
                                           const FormalBoolean isActTrig_RoofHatchRequest3,
                                           const FormalBoolean isDeActTrig_RoofHatchRequest3)
{
   //! ###### Processing the application network (activation/deactivation)conditions
   static AnwSM_States AnwState_RoofHatchGen                 = { SM_ANW_Inactive,
                                                                 SM_ANW_Inactive };
   static DeactivationTimer_Type RoofHatch_DeactivationTimer = 0U;
   AnwAction_Enum AnwActionstatus                            = ANW_Action_None;
   //! ##### Process ANW logic : 'ProcessAnwLogic()'
   AnwActionstatus = ProcessAnwLogic(isRestAct_RoofHatchRequest3,
                                     isActTrig_RoofHatchRequest3,
                                     isDeActTrig_RoofHatchRequest3,
                                     CONST_ANW_RoofHatchRequest3_Timeout,
                                     &RoofHatch_DeactivationTimer,
                                     &AnwState_RoofHatchGen);
    //! ##### Check for Anw action is 'activate' or 'deactivate' for generic ANW roofhatch request3 trigger
   if (ANW_Action_Activate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_RoofHatchRequest3_ActivateIss();
   }
   else if (ANW_Action_Deactivate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_RoofHatchRequest3_DeactivateIss();
   }
   else
   {
      // Do nothing, keep Previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Generic_ANW_AudioRadio2_Trigger'
//!
//! \param   isRestAct_AudioRadio   Provides 'yes' or 'no' values based on restriction activation
//! \param   isActTrig_AudioRadio   Provides 'yes' or 'no' values based on activation trigger
//!
//!======================================================================================
void Generic_ANW_AudioRadio2_Trigger(const FormalBoolean isRestAct_AudioRadio,
                                     const FormalBoolean isActTrig_AudioRadio)
{
   //! ###### Processing the Application network (activation/deactivation)conditions
   static AnwSM_States AnwState_AudioRadio                     = {SM_ANW_Inactive,
                                                                  SM_ANW_Inactive };
   static DeactivationTimer_Type AudioRadio2_DeactivationTimer = 0U;
   AnwAction_Enum AnwActionstatus                              = ANW_Action_None;
   //! ##### Process ANW logic: 'ProcessAnwLogic()'
   AnwActionstatus = ProcessAnwLogic(isRestAct_AudioRadio,
                                     isActTrig_AudioRadio,
                                     YES,
                                     CONST_ANW_AudioRadio2_Timeout,
                                     &AudioRadio2_DeactivationTimer,
                                     &AnwState_AudioRadio);
   //! ##### Check for Anw action is 'activate' or 'deactivate' for generic ANW audio radio2 trigger
   if (ANW_Action_Activate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_AudioRadio2_ActivateIss();
   }
   else if (ANW_Action_Deactivate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_AudioRadio2_DeactivateIss();
   }
   else
   {
      // Do nothing, keep Previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Generic_ANW_ClimPHTimerSetting3_Trigger'
//!
//! \param   isRestAct_ClimPHTimerSettings   Provides 'yes' or 'no' values based on restriction activation
//! \param   isActTrig_ClimPHTimerSettings   Provides 'yes' or 'no' values based on activation trigger
//!
//!======================================================================================
void Generic_ANW_ClimPHTimerSetting3_Trigger(const FormalBoolean isRestAct_ClimPHTimerSettings,
                                             const FormalBoolean isActTrig_ClimPHTimerSettings)
{
   //! ###### Processing the application network (activation/deactivation)conditions
   static AnwSM_States AnwState_ClimPHT                        = { SM_ANW_Inactive,
                                                                   SM_ANW_Inactive };
   static DeactivationTimer_Type ClimPHTimer_DeactivationTimer = 0U;
   AnwAction_Enum AnwActionstatus                              = ANW_Action_None;
   //! ##### Process ANW logic: 'ProcessAnwLogic()'
   AnwActionstatus = ProcessAnwLogic(isRestAct_ClimPHTimerSettings,
                                     isActTrig_ClimPHTimerSettings,
                                     YES, 
                                     CONST_ANW_ClimPHTimerSettings3_Timeout, 
                                     &ClimPHTimer_DeactivationTimer, 
                                     &AnwState_ClimPHT);
   //! ##### Check for Anw action is 'activate' or 'deactivate' for generic ANW climate ph timer settings3 trigger
   if (ANW_Action_Activate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_ClimPHTimerSettings3_ActivateIss();
   }
   else if (ANW_Action_Deactivate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_ClimPHTimerSettings3_DeactivateIss();
   }
   else
   {
      // Do nothing, keep Previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Generic_ANW_LockCntrlCabRqst2_Trigger'
//!
//! \param   isRestAct_LockControlCabRqst   Provides 'yes' or 'no' values based on restriction activation
//! \param   isActTrig_LockControlCabRqst   Provides 'yes' or 'no' values based on activation trigger
//!
//!======================================================================================
void Generic_ANW_LockCntrlCabRqst2_Trigger(const FormalBoolean isRestAct_LockControlCabRqst,
                                           const FormalBoolean isActTrig_LockControlCabRqst)
{
   //! ###### Processing the application network (activation/deactivation)conditions
   static AnwSM_States AnwState_LockControl                    = { SM_ANW_Inactive,
                                                                   SM_ANW_Inactive };
   static DeactivationTimer_Type LockControl_DeactivationTimer = 0U;
   AnwAction_Enum AnwActionstatus                              = ANW_Action_None;
   //! ##### Process ANW logic: 'ProcessAnwLogic()'
   AnwActionstatus = ProcessAnwLogic(isRestAct_LockControlCabRqst,
                                     isActTrig_LockControlCabRqst,
                                     YES, 
                                     CONST_ANW_LockControlCabRqst2_Timeout, 
                                     &LockControl_DeactivationTimer, 
                                     &AnwState_LockControl);
   //! ##### Check for Anw action is 'activate' or 'deactivate' for generic ANW lock control cab request2 trigger
   if (ANW_Action_Activate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_LockControlCabRqst2_ActivateIss();
   }
   else if (ANW_Action_Deactivate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_LockControlCabRqst2_DeactivateIss();
   }
   else
   {
      // Do nothing, keep Previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Generic_ANW_PHActMaintainLvng6Trigger'
//!
//! \param   isRestAct_PHActMaintainLiving     Provides 'yes' or 'no' values based on restriction activation
//! \param   isActTrig_PHActMaintainLiving     Provides 'yes' or 'no' values based on activation trigger
//! \param   isDeActTrig_PHActMaintainLiving   Provides 'yes' or 'no' values based on deactivation trigger
//!
//!======================================================================================
void Generic_ANW_PHActMaintainLvng6Trigger(const FormalBoolean isRestAct_PHActMaintainLiving,
                                           const FormalBoolean isActTrig_PHActMaintainLiving,
                                           const FormalBoolean isDeActTrig_PHActMaintainLiving)
{
   //! ###### Processing the application network (activation/deactivation)conditions
   static AnwSM_States AnwState_PHAct                    = { SM_ANW_Inactive,
                                                             SM_ANW_Inactive };
   static DeactivationTimer_Type PHAct_DeactivationTimer = 0U;
   AnwAction_Enum AnwActionstatus                        = ANW_Action_None;
   //! ##### Process ANW logic: 'ProcessAnwLogic()'
   AnwActionstatus = ProcessAnwLogic(isRestAct_PHActMaintainLiving,
                                     isActTrig_PHActMaintainLiving,
                                     isDeActTrig_PHActMaintainLiving,
                                     CONST_ANW_PHActMaintainLiving6_Timeout,
                                     &PHAct_DeactivationTimer,
                                     &AnwState_PHAct);
   //! ##### Check for Anw action is 'activate' or 'deactivate' for generic ANW PH activation maintain living6 trigger
   if (ANW_Action_Activate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_PHActMaintainLiving6_ActivateIss();
   }
   else if (ANW_Action_Deactivate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_PHActMaintainLiving6_DeactivateIss();
   }
   else
   {
      // Do nothing, keep Previous status
   }
} 
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Generic_ANW_OtherInteriorLight4_Trigger'
//!
//! \param   isRestAct_OtherInteriorLights   Passing as an argument to 'ProcessAnwLogic' function
//! \param   isActTrig_OtherInteriorLights   Passing as an argument to 'ProcessAnwLogic' function
//!
//!======================================================================================
void Generic_ANW_OtherInteriorLight4_Trigger(const FormalBoolean isRestAct_OtherInteriorLights,
                                             const FormalBoolean isActTrig_OtherInteriorLights)
{
   //! ###### Processing the application network (activation/deactivation)conditions
   static AnwSM_States AnwState_InteriorLignt                    = { SM_ANW_Inactive,
                                                                     SM_ANW_Inactive };
   static DeactivationTimer_Type OtherInterior_DeactivationTimer = 0U;
   AnwAction_Enum  AnwActionstatus                               = ANW_Action_None;
   //! ##### Process ANW logic: 'ProcessAnwLogic()'
   AnwActionstatus = ProcessAnwLogic(isRestAct_OtherInteriorLights,
                                     isActTrig_OtherInteriorLights,
                                     YES,
                                     CONST_ANW_OtherInteriorLights4_Timeout,
                                     &OtherInterior_DeactivationTimer,
                                     &AnwState_InteriorLignt);
   //! ##### Check for Anw action is 'activate' or 'deactivate' for generic ANW other interior lights4 trigger
   if (ANW_Action_Activate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_OtherInteriorLights4_ActivateIss();
   }
   else if (ANW_Action_Deactivate == AnwActionstatus)
   {
      Rte_Call_UR_ANW_OtherInteriorLights4_DeactivateIss();
   }
   else
   {
      // Do nothing, keep Previous status
   }
}

//! @}
//! @}
//! @}
//! @}