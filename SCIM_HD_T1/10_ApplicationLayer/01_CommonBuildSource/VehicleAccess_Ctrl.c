/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  VehicleAccess_Ctrl.c
 *        Config:  C:/GIT/scim_ecu_hd_t1/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  VehicleAccess_Ctrl
 *  Generated at:  Sat Jan 18 19:02:08 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <VehicleAccess_Ctrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file VehicleAccess_Ctrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_BodyAndSecurity 
//! @{
//! @addtogroup Application_VehicleAccess
//! @{
//! @addtogroup VehicleAccess_Ctrl
//! @{
//!
//! \brief
//! VehicleAccess_Ctrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the VehicleAccess_Ctrl runnables.
//!======================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   Boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * Issm_IssStateType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AlarmAutoRelockRequestDuration_X1CYA_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AlarmAutoRelockRequestDuration_X1CYA_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AutoAlarmReactivationTimeout_P1B2S_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AutoAlarmReactivationTimeout_P1B2S_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DashboardLedTimeout_P1IZ4_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DashboardLedTimeout_P1IZ4_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorAutoLockingSpeed_P1B2Q_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorAutoLockingSpeed_P1B2Q_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorIndicationReqDuration_P1DWP_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorIndicationReqDuration_P1DWP_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorLatchProtectMaxOperation_P1DXA_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorLatchProtectMaxOperation_P1DXA_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorLatchProtectionRestingTime_P1DW9_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorLatchProtectionRestingTime_P1DW9_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorLatchProtectionTimeWindow_P1DW8_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorLatchProtectionTimeWindow_P1DW8_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_DoorLockingFailureTimeout_X1CX9_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorLockingFailureTimeout_X1CX9_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PassiveEntryFunction_Type_P1VKF_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PassiveEntryFunction_Type_P1VKF_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_SpeedRelockingReinitThreshold_P1H55_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_SpeedRelockingReinitThreshold_P1H55_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Speed16bit_T
 *   65024 - 65279 Error ; 65280 - 65535 Not available
 *
 *********************************************************************************************************************/

#include "Rte_VehicleAccess_Ctrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

// Includes
#include "FuncLibrary_Timer_If.h"
#include "FuncLibrary_AnwStateMachine_If.h"
#include "VehicleAccess_Ctrl.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_AlarmAutoRelockRequestDuration_X1CYA_T: Integer in interval [0...255]
 * SEWS_AlarmAutoRelockRequestDuration_X1CYA_T: Integer in interval [0...255]
 * SEWS_AutoAlarmReactivationTimeout_P1B2S_T: Integer in interval [0...255]
 * SEWS_AutoAlarmReactivationTimeout_P1B2S_T: Integer in interval [0...255]
 * SEWS_DashboardLedTimeout_P1IZ4_T: Integer in interval [0...255]
 * SEWS_DashboardLedTimeout_P1IZ4_T: Integer in interval [0...255]
 * SEWS_DoorAutoLockingSpeed_P1B2Q_T: Integer in interval [0...255]
 * SEWS_DoorAutoLockingSpeed_P1B2Q_T: Integer in interval [0...255]
 * SEWS_DoorIndicationReqDuration_P1DWP_T: Integer in interval [0...255]
 * SEWS_DoorIndicationReqDuration_P1DWP_T: Integer in interval [0...255]
 * SEWS_DoorLatchProtectMaxOperation_P1DXA_T: Integer in interval [0...255]
 * SEWS_DoorLatchProtectMaxOperation_P1DXA_T: Integer in interval [0...255]
 * SEWS_DoorLatchProtectionRestingTime_P1DW9_T: Integer in interval [0...255]
 * SEWS_DoorLatchProtectionRestingTime_P1DW9_T: Integer in interval [0...255]
 * SEWS_DoorLatchProtectionTimeWindow_P1DW8_T: Integer in interval [0...255]
 * SEWS_DoorLatchProtectionTimeWindow_P1DW8_T: Integer in interval [0...255]
 * SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T: Integer in interval [0...65535]
 * SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T: Integer in interval [0...65535]
 * SEWS_DoorLockingFailureTimeout_X1CX9_T: Integer in interval [0...255]
 * SEWS_DoorLockingFailureTimeout_X1CX9_T: Integer in interval [0...255]
 * SEWS_PassiveEntryFunction_Type_P1VKF_T: Integer in interval [0...255]
 * SEWS_PassiveEntryFunction_Type_P1VKF_T: Integer in interval [0...255]
 * SEWS_SpeedRelockingReinitThreshold_P1H55_T: Integer in interval [0...255]
 * SEWS_SpeedRelockingReinitThreshold_P1H55_T: Integer in interval [0...255]
 * Speed16bit_T: Integer in interval [0...65535]
 *   Unit: [Km_per_h], Factor: 1, Offset: 0
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * AutoRelock_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 *   AutoRelock_rqst_Idle (0U)
 *   AutoRelock_rqst_AutoRelockActivated (1U)
 *   AutoRelock_rqst_Error (2U)
 *   AutoRelock_rqst_NotAvailable (3U)
 * AutorelockingMovements_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   AutorelockingMovements_stat_Idle (0U)
 *   AutorelockingMovements_stat_NoMovementHasBeenDetected (1U)
 *   AutorelockingMovements_stat_MovementHasBeenDetected (2U)
 *   AutorelockingMovements_stat_Spare (3U)
 *   AutorelockingMovements_stat_Spare_01 (4U)
 *   AutorelockingMovements_stat_Spare_02 (5U)
 *   AutorelockingMovements_stat_Error (6U)
 *   AutorelockingMovements_stat_NotAvailable (7U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DoorAjar_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorAjar_stat_Idle (0U)
 *   DoorAjar_stat_DoorClosed (1U)
 *   DoorAjar_stat_DoorOpen (2U)
 *   DoorAjar_stat_Error (6U)
 *   DoorAjar_stat_NotAvailable (7U)
 * DoorLatch_rqst_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorLatch_rqst_decrypt_Idle (0U)
 *   DoorLatch_rqst_decrypt_LockDoorCommand (1U)
 *   DoorLatch_rqst_decrypt_UnlockDoorCommand (2U)
 *   DoorLatch_rqst_decrypt_EmergencyUnlockRequest (3U)
 *   DoorLatch_rqst_decrypt_Spare1 (4U)
 *   DoorLatch_rqst_decrypt_Spare2 (5U)
 *   DoorLatch_rqst_decrypt_Error (6U)
 *   DoorLatch_rqst_decrypt_NotAvailable (7U)
 * DoorLatch_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorLatch_stat_Idle (0U)
 *   DoorLatch_stat_LatchUnlockedFiltered (1U)
 *   DoorLatch_stat_LatchLockedFiltered (2U)
 *   DoorLatch_stat_Error (6U)
 *   DoorLatch_stat_NotAvailable (7U)
 * DoorLockUnlock_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorLockUnlock_Idle (0U)
 *   DoorLockUnlock_Unlock (1U)
 *   DoorLockUnlock_Lock (2U)
 *   DoorLockUnlock_MonoLockUnlock (3U)
 *   DoorLockUnlock_Spare1 (4U)
 *   DoorLockUnlock_Spare2 (5U)
 *   DoorLockUnlock_Error (6U)
 *   DoorLockUnlock_NotAvailable (7U)
 * DoorLock_stat_T: Enumeration of integer in interval [0...15] with enumerators
 *   DoorLock_stat_Idle (0U)
 *   DoorLock_stat_BothDoorsAreUnlocked (1U)
 *   DoorLock_stat_DriverDoorIsUnlocked (2U)
 *   DoorLock_stat_PassengerDoorIsUnlocked (3U)
 *   DoorLock_stat_BothDoorsAreLocked (4U)
 *   DoorLock_stat_Error (14U)
 *   DoorLock_stat_NotAvailable (15U)
 * DoorsAjar_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorsAjar_stat_Idle (0U)
 *   DoorsAjar_stat_BothDoorsAreClosed (1U)
 *   DoorsAjar_stat_DriverDoorIsOpen (2U)
 *   DoorsAjar_stat_PassengerDoorIsOpen (3U)
 *   DoorsAjar_stat_BothDoorsAreOpen (4U)
 *   DoorsAjar_stat_Spare (5U)
 *   DoorsAjar_stat_Error (6U)
 *   DoorsAjar_stat_NotAvailable (7U)
 * EmergencyDoorsUnlock_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   EmergencyDoorsUnlock_rqst_Idle (0U)
 *   EmergencyDoorsUnlock_rqst_Spare (1U)
 *   EmergencyDoorsUnlock_rqst_EmergencyUnlockRequest (2U)
 *   EmergencyDoorsUnlock_rqst_Error (6U)
 *   EmergencyDoorsUnlock_rqst_NotAvailable (7U)
 * FrontLidLatch_cmd_T: Enumeration of integer in interval [0...7] with enumerators
 *   FrontLidLatch_cmd_Idle (0U)
 *   FrontLidLatch_cmd_LockFrontLidCommand (1U)
 *   FrontLidLatch_cmd_UnlockFrontLidCommand (2U)
 *   FrontLidLatch_cmd_Error (6U)
 *   FrontLidLatch_cmd_NotAvailable (7U)
 * FrontLidLatch_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   FrontLidLatch_stat_Idle (0U)
 *   FrontLidLatch_stat_LatchUnlocked (1U)
 *   FrontLidLatch_stat_LatchLocked (2U)
 *   FrontLidLatch_stat_Error (6U)
 *   FrontLidLatch_stat_NotAvailable (7U)
 * Issm_IssStateType: Enumeration of integer in interval [0...2] with enumerators
 *   ISSM_STATE_INACTIVE (0U)
 *   ISSM_STATE_PENDING (1U)
 *   ISSM_STATE_ACTIVE (2U)
 * KeyfobInCabLocation_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobInCabLocation_stat_Idle (0U)
 *   KeyfobInCabLocation_stat_NotDetected_Incab (1U)
 *   KeyfobInCabLocation_stat_Detected_Incab (2U)
 *   KeyfobInCabLocation_stat_Spare1 (3U)
 *   KeyfobInCabLocation_stat_Spare2 (4U)
 *   KeyfobInCabLocation_stat_Spare3 (5U)
 *   KeyfobInCabLocation_stat_Error (6U)
 *   KeyfobInCabLocation_stat_NotAvailable (7U)
 * KeyfobLocation_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 *   KeyfobLocation_rqst_NoRequest (0U)
 *   KeyfobLocation_rqst_Request (1U)
 *   KeyfobLocation_rqst_Error (2U)
 *   KeyfobLocation_rqst_NotAvailable (3U)
 * KeyfobOutsideLocation_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobOutsideLocation_stat_Idle (0U)
 *   KeyfobOutsideLocation_stat_NotDetectedOutside (1U)
 *   KeyfobOutsideLocation_stat_DetectedOnRightSide (2U)
 *   KeyfobOutsideLocation_stat_DetectedOnLeftSide (3U)
 *   KeyfobOutsideLocation_stat_DetectedOnBothSides (4U)
 *   KeyfobOutsideLocation_stat_DetectedOutOfDoorAreas (5U)
 *   KeyfobOutsideLocation_stat_Error (6U)
 *   KeyfobOutsideLocation_stat_NotAvailable (7U)
 * LockingIndication_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   LockingIndication_rqst_Idle (0U)
 *   LockingIndication_rqst_Lock (1U)
 *   LockingIndication_rqst_Unlock (2U)
 *   LockingIndication_rqst_Spare (3U)
 *   LockingIndication_rqst_Spare01 (4U)
 *   LockingIndication_rqst_Spare02 (5U)
 *   LockingIndication_rqst_Error (6U)
 *   LockingIndication_rqst_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * SpeedLockingInhibition_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   SpeedLockingInhibition_stat_Idle (0U)
 *   SpeedLockingInhibition_stat_SpeedLockingActivate (1U)
 *   SpeedLockingInhibition_stat_SpeedLockingDeactivate (2U)
 *   SpeedLockingInhibition_stat_Error (6U)
 *   SpeedLockingInhibition_stat_NotAvailable (7U)
 * Synch_Unsynch_Mode_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   Synch_Unsynch_Mode_stat_Idle (0U)
 *   Synch_Unsynch_Mode_stat_SynchronizedMode (1U)
 *   Synch_Unsynch_Mode_stat_UnsynchronizedMode (2U)
 *   Synch_Unsynch_Mode_stat_Spare (3U)
 *   Synch_Unsynch_Mode_stat_Spare_01 (4U)
 *   Synch_Unsynch_Mode_stat_Spare_02 (5U)
 *   Synch_Unsynch_Mode_stat_Error (6U)
 *   Synch_Unsynch_Mode_stat_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *
 * Array Types:
 * ============
 * Crypto_Function_serialized_T: Array with 13 element(s) of type uint8
 * StandardNVM_T: Array with 4 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T Rte_Prm_P1O8Q_DoorLockHazardIndicationRqstDelay_v(void)
 *   SEWS_DoorAutoLockingSpeed_P1B2Q_T Rte_Prm_P1B2Q_DoorAutoLockingSpeed_v(void)
 *   SEWS_AutoAlarmReactivationTimeout_P1B2S_T Rte_Prm_P1B2S_AutoAlarmReactivationTimeout_v(void)
 *   SEWS_DoorLatchProtectionTimeWindow_P1DW8_T Rte_Prm_P1DW8_DoorLatchProtectionTimeWindow_v(void)
 *   SEWS_DoorLatchProtectionRestingTime_P1DW9_T Rte_Prm_P1DW9_DoorLatchProtectionRestingTime_v(void)
 *   SEWS_DoorIndicationReqDuration_P1DWP_T Rte_Prm_P1DWP_DoorIndicationReqDuration_v(void)
 *   SEWS_DoorLatchProtectMaxOperation_P1DXA_T Rte_Prm_P1DXA_DoorLatchProtectMaxOperation_v(void)
 *   SEWS_SpeedRelockingReinitThreshold_P1H55_T Rte_Prm_P1H55_SpeedRelockingReinitThreshold_v(void)
 *   SEWS_DashboardLedTimeout_P1IZ4_T Rte_Prm_P1IZ4_DashboardLedTimeout_v(void)
 *   boolean Rte_Prm_P1NE9_KeyInsertDetection_Enabled_v(void)
 *   boolean Rte_Prm_P1NQE_LockModeHandling_v(void)
 *   SEWS_PassiveEntryFunction_Type_P1VKF_T Rte_Prm_P1VKF_PassiveEntryFunction_Type_v(void)
 *   boolean Rte_Prm_P1VKI_PassiveStart_Installed_v(void)
 *   SEWS_DoorLockingFailureTimeout_X1CX9_T Rte_Prm_X1CX9_DoorLockingFailureTimeout_v(void)
 *   SEWS_AlarmAutoRelockRequestDuration_X1CYA_T Rte_Prm_X1CYA_AlarmAutoRelockRequestDuration_v(void)
 *
 *********************************************************************************************************************/


#define VehicleAccess_Ctrl_START_SEC_CODE
#include "VehicleAccess_Ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_DoorLockingFailureTimeout       ((((uint16)Rte_Prm_X1CX9_DoorLockingFailureTimeout_v()) * 20U) / CONST_RunnableTimeBase)
#define PCODE_DoorLockHazardIndRqstDelay      ((((uint16)Rte_Prm_P1O8Q_DoorLockHazardIndicationRqstDelay_v()) * 20U) / CONST_RunnableTimeBase)
#define PCODE_DoorIndReqDuration              (((((uint16)Rte_Prm_P1DWP_DoorIndicationReqDuration_v()) * 50U) + 50U) / CONST_RunnableTimeBase)
#define PCODE_LockCtrl_DashboardLedTimeout    ((((uint16)Rte_Prm_P1IZ4_DashboardLedTimeout_v()) * CONST_mstominutesconversion) / CONST_RunnableTimeBase)
#define PCODE_SpeedRelockingReinitThreshold   (Rte_Prm_P1H55_SpeedRelockingReinitThreshold_v())
#define PCODE_DoorAutoLockingSpeed            (Rte_Prm_P1B2Q_DoorAutoLockingSpeed_v())
#define PCODE_DoorLatchProtectionTimeWindow   ((uint16)(Rte_Prm_P1DW8_DoorLatchProtectionTimeWindow_v() * (1000U/CONST_RunnableTimeBase)))
#define PCODE_DoorLatchProtectMaxOperation    (Rte_Prm_P1DXA_DoorLatchProtectMaxOperation_v())
#define PCODE_DoorLatchProtectionRestingTime  ((uint16)(Rte_Prm_P1DW9_DoorLatchProtectionRestingTime_v() * (1000U / CONST_RunnableTimeBase)))
#define PCODE_AutoAlarmReactivationTimeout    (((uint16)Rte_Prm_P1B2S_AutoAlarmReactivationTimeout_v() * 10U * 1000U) / CONST_RunnableTimeBase)
#define PCODE_AlarmAutoRelockRequestDuration  (((uint16)Rte_Prm_X1CYA_AlarmAutoRelockRequestDuration_v() * 20U) / CONST_RunnableTimeBase)
 // Temporary inputs need to get clarification
PassiveEntryLogic_Enum PassiveEntryLogic    = CONST_Idle_PassivEntry; 
//uint8 SpeedRelockingOnHold = FALSE;
// varient Selection
#define WITH_DOOR_MODULES                  (1U)
#define WITHOUT_DOOR_MODULES               (2U)
#define CONST_LockFunctionHwInterface      (1U)
#define CONST_LockModeHandling             (1U)

//#define PEPS_Monitor_ON 1

#if PEPS_Monitor_ON
KeyfobInCabLocation_stat_T PEPS_Monitor;
#endif

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VehicleAccess_Ctrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AutorelockingMovements_stat_AutorelockingMovements_stat(AutorelockingMovements_stat_T *data)
 *   Std_ReturnType Rte_Read_DriverDoorAjarInternal_stat_DoorAjar_stat(DoorAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_DriverDoorAjar_stat_DoorAjar_stat(DoorAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_DriverDoorLatchInternal_stat_DoorLatch_stat(DoorLatch_stat_T *data)
 *   Std_ReturnType Rte_Read_DriverDoorLatch_stat_DoorLatch_stat(DoorLatch_stat_T *data)
 *   Std_ReturnType Rte_Read_DrivrDrKeyCylTrn_st_serialized_Crypto_Function_serialized(uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Read_EmergencyDoorsUnlock_rqst_EmergencyDoorsUnlock_rqst(EmergencyDoorsUnlock_rqst_T *data)
 *   Std_ReturnType Rte_Read_FrontLidLatch_stat_FrontLidLatch_stat(FrontLidLatch_stat_T *data)
 *   Std_ReturnType Rte_Read_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst(DoorLockUnlock_T *data)
 *   Std_ReturnType Rte_Read_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat(KeyfobInCabLocation_stat_T *data)
 *   Std_ReturnType Rte_Read_KeyfobLockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle(KeyfobOutsideLocation_stat_T *data)
 *   Std_ReturnType Rte_Read_KeyfobSuperLockButton_Sta_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_KeyfobUnlockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LeftDoorButton_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_PassengerDoorAjar_stat_DoorAjar_stat(DoorAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_PassengerDoorLatch_stat_DoorLatch_stat(DoorLatch_stat_T *data)
 *   Std_ReturnType Rte_Read_PsgDoorKeyCylTrn_st_serialized_Crypto_Function_serialized(uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Read_PsngDoorLatchInternal_stat_DoorLatch_stat(DoorLatch_stat_T *data)
 *   Std_ReturnType Rte_Read_PsngrDoorAjarInternal_stat_DoorAjar_stat(DoorAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_RightDoorButton_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SpeedLockingInhibition_stat_SpeedLockingInhibition_stat(SpeedLockingInhibition_stat_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_Synch_Unsynch_Mode_stat_Synch_Unsynch_Mode_stat(Synch_Unsynch_Mode_stat_T *data)
 *   Std_ReturnType Rte_Read_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I(uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *   Std_ReturnType Rte_Read_WRCLockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_WRCUnlockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_AutoRelock_rqst_AutoRelock_rqst(AutoRelock_rqst_T data)
 *   Std_ReturnType Rte_Write_DashboardLockSwitch_Devic_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_DoorLock_stat_DoorLock_stat(DoorLock_stat_T data)
 *   Std_ReturnType Rte_Write_DoorsAjar_stat_DoorsAjar_stat(DoorsAjar_stat_T data)
 *   Std_ReturnType Rte_Write_DriverDoorLatch_rqst_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(DoorLatch_rqst_decrypt_T data)
 *   Std_ReturnType Rte_Write_DriverDoorLatch_rqt_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_FrontLidLatch_cmd_FrontLidLatch_cmd(FrontLidLatch_cmd_T data)
 *   Std_ReturnType Rte_Write_KeyfobLocation_rqst_KeyfobLocation_rqst(KeyfobLocation_rqst_T data)
 *   Std_ReturnType Rte_Write_LockingIndication_rqst_LockingIndication_rqst(LockingIndication_rqst_T data)
 *   Std_ReturnType Rte_Write_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(DoorLatch_rqst_decrypt_T data)
 *   Std_ReturnType Rte_Write_PsngrDoorLatch_rqst_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_PsngrDoorLatch_rqst_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I(const uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlActDeactivation_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlActDeactivation_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlActDeactivation_GetIssState(Issm_IssStateType *issState)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlCabRqst2_Pending_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlCabRqst2_Pending_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VehicleAccess_Ctrl_20ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the VehicleAccess_Ctrl_20ms_runnable
//!==================================================================================================================== 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#ifndef PEPS_Monitor_ON
#ifdef PEPS_Monitor_ON
static uint8                 isPeriodicalPepsCheckActive            = 0U;
static uint8                 isPepsCheckOngoing                     = 0U;
static uint8                 Timeout                                = (1000U / 20U);
       KeyfobLocation_rqst_T KeyfobLocation_rqst;
       DoorLockUnlock_T      IncabDoorLockUnlock_rqst_previousValue = DoorLockUnlock_Idle;
       DoorLockUnlock_T      IncabDoorLockUnlock_rqst_currentValue  = DoorLockUnlock_Idle;
#endif
#endif

FUNC(void, VehicleAccess_Ctrl_CODE) VehicleAccess_Ctrl_20ms_runnable(void) /* PRQA S 0850 *//* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VehicleAccess_Ctrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   static AjarWithDoorModules_in_StructType   RteInData_WithDoorModulesAjar;
   static VehicleAccess_Ctrl_in_StructType    RteInData_Common;
   static VehicleAccess_Ctrl_out_StructType   RteOutData_Common              = { DoorLock_stat_NotAvailable,
                                                                                 DoorsAjar_stat_NotAvailable,
                                                                                 DoorLatch_rqst_decrypt_NotAvailable,
                                                                                 DoorLatch_rqst_decrypt_NotAvailable,
                                                                                 KeyfobLocation_rqst_NotAvailable,
                                                                                 DeviceIndication_SpareValue,
                                                                                 DoorLatch_rqst_decrypt_NotAvailable,
                                                                                 DoorLatch_rqst_decrypt_NotAvailable,
                                                                                 LockingIndication_rqst_NotAvailable,
                                                                                 AutoRelock_rqst_NotAvailable};
   static DoorLatchState                      DoorLatchStatus                 = { DoorLatch_stat_NotAvailable,
                                                                                  DoorLatch_stat_NotAvailable };
   static SM_States                           LockCtrlSM_State                = { SM_Lock_OperationStatus,
                                                                                  SM_Lock_Idle,
                                                                                  FALSE };
   //static Synch_Unsynch_Mode_stat_T           SelectedSynchronuousMode        = Synch_Unsynch_Mode_stat_UnsynchronizedMode; 
   static SelfLockAction_T                    SelfLock_BackToState            = {CONST_SelfLock_B2S_Idle};
   static RemoteStatusIndication_Enum         RemoteOperationStatusIndication = SM_RemoteStatusIndication_Idle;
   static boolean                             isLatchProtectionInactive       = FALSE;
   static boolean                             isAutorelockTriggered           = FALSE;
          DoorsAjar_stat_T                    DoorAjarState                   = DoorsAjar_stat_NotAvailable;
          EventUnlock_Enum                    Unlock_event                    = CONST_IdleUnlockRequest;
          EventLock_Enum                      Lock_event                      = CONST_IdleLockRequest;
          EventSysLatch_Enum                  SystemEventLatches              = CONST_DoorsOprtnIdle;
          boolean                             isSysEventSpeedLock             = FALSE;  
         // FormalBoolean                       isActivationTriggerDetected     = NO;
        LatchWithDoorModules_in_StructType  RteInData_WithDoorModulesLatch  = {DoorLatch_stat_NotAvailable,
                                                                               DoorLatch_stat_NotAvailable};
   static LatchWithDoorModules_in_StructType  LatchesStateFiltered_WithDoorModules = {DoorLatch_stat_NotAvailable,
                                                                                      DoorLatch_stat_NotAvailable};
   static uint16                              Timers[CONST_NbOfTimers]        = {CONST_TimerFunctionInactive,
                                                                                 CONST_TimerFunctionInactive,
                                                                                 CONST_TimerFunctionInactive,
                                                                                 CONST_TimerFunctionInactive,
                                                                                 CONST_TimerFunctionInactive,
                                                                                 CONST_TimerFunctionInactive,
                                                                                 CONST_TimerFunctionInactive,
                                                                                 CONST_TimerFunctionInactive,
                                                                                 CONST_TimerFunctionInactive,
                                                                                 CONST_TimerFunctionInactive,
                                                                                 CONST_TimerFunctionInactive};

   //! ###### Processing door system variant logic 
   //! ###### Select the door system variant logic based on PCODE_LockFunctionHardware interface
   switch (CONST_LockFunctionHwInterface)
   {
      //! ##### Processing system with door modules variant logic
      case WITH_DOOR_MODULES:
         //! #### Process RTE indata read with door modules logic : 'Get_RteInData_WithDoorModules()'
         Get_RteInData_WithDoorModules(&RteInData_WithDoorModulesLatch,
                                       &RteInData_WithDoorModulesAjar);
         //! #### Process driver door latch state transition logic : 'DoorLatchStateTransitionProcessing()'
         DoorLatchStateTransitionProcessing(RteInData_WithDoorModulesLatch.DriverDoorLatch_stat,
                                            &LatchesStateFiltered_WithDoorModules.DriverDoorLatch_stat,
                                            &Timers[CONST_DriverSelfLockTimer],
                                            &SelfLock_BackToState.DrvrLatch);         
         //! #### Process passenger door latch state transition logic : 'DoorLatchStateTransitionProcessing()'`
         DoorLatchStateTransitionProcessing(RteInData_WithDoorModulesLatch.PassengerDoorLatch_stat,
                                            &LatchesStateFiltered_WithDoorModules.PassengerDoorLatch_stat,
                                            &Timers[CONST_PassengerSelfLockTimer],
                                            &SelfLock_BackToState.PsgrLatch);         
         //! #### Process door ajar logic for the system with door modules : 'ProcessDoorAjarStateFromXDM()'
         DoorAjarState = ProcessDoorAjarStateFromXDM(&RteInData_WithDoorModulesAjar.DriverDoorAjar_stat,
                                                     &RteInData_WithDoorModulesAjar.PassengerDoorAjar_stat);
         //! #### Process door latch state logic for system with door modules : 'ProcessDoorLatchesStateFromXDM()'
         ProcessDoorLatchesStateFromXDM(&LatchesStateFiltered_WithDoorModules.DriverDoorLatch_stat,
                                        &LatchesStateFiltered_WithDoorModules.PassengerDoorLatch_stat,
                                        &DoorLatchStatus);
      break;
      //! ##### Processing system without door modules variant logic
      case WITHOUT_DOOR_MODULES:
      break;
      default:
         //! #### Read the RTE ports for system without xDM and process fallback modes for RTE events : 'RteInData_WithoutDoorModules()'
         // RteInData_WithoutDoorModules(&VAC_in_data);

         //! #### Process door ajar logic for system without door modules : 'DoorAjarStateFromHwProcessing()': 
         // DoorAjarStateFromHwProcessing();

         //! #### Process door latch state logic for system without door modules : 'DoorLatchesStateFromHwProcessing()'
         // DoorLatchesStateFromHwProcessing();
      break;
   }
   //! ##### Process RTE in data read common logic : 'Get_RteInDataRead_Common()'
   Get_RteInDataRead_Common(&RteInData_Common);
   //! ##### Timers decrement logic : 'TimerFunction_Tick()'
   TimerFunction_Tick(CONST_NbOfTimers, 
                      Timers);
   
   if (TRUE == CONST_LockModeHandling)
   {
      //! ##### Process synchronuous mode selection: 'SynchronuousModeProcessing()'
      SynchronuousModeProcessing(&RteInData_Common.Synch_Unsynch_Mode_stat);
   }
   //! ##### Process unlock events logic : 'CheckUnlockEvents()'
   Unlock_event = CheckUnlockEvents(&RteInData_Common.VehicleModeInternal.currentValue,
                                    &RteInData_Common.KeyfobUnlockButton_Stat,
                                    &RteInData_Common.WRCUnlockButtonStat,
                                    &RteInData_Common.IncabDoorLockUnlock_rqst,
                                    &RteInData_Common.DriverDrKeyCylTrn_stat,
                                    &RteInData_Common.PassengerDrKeyCylTrn_stat,
                                    &RteInData_Common.EmergencyDoorsUnlock_rqst,
                                    &DoorLatchStatus.currentValue,
                                    &RteInData_Common.Synch_Unsynch_Mode_stat);
   
   //! ##### Process lock events logic: 'CheckLockEvents()'
   Lock_event = CheckLockEvents(&RteInData_Common.KeyfobLockButton_Stat,
                                &RteInData_Common.KeyfobSuperLockButton_Stat,
                                &RteInData_Common.WRCLockButtonStat,
                                &RteInData_Common.VehicleModeInternal.currentValue,
                                &RteInData_Common.IncabDoorLockUnlock_rqst,
                                &DoorLatchStatus);
   
   // SystemEventLatches = lockControl_SystemEventsLatches(&DoorLatchStatus);
   //! ##### Check for PCODE_AutoAlarmReactivationTimeout parameter
   if (0U < (uint16)PCODE_AutoAlarmReactivationTimeout)  //need to check it will execute every time
   {
      //! #### Process system event auto relock logic: 'isSystemEventAutorelock()'
      isSystemEventAutorelock(&RteInData_Common.AutorelockingMovements_Stat,
                              &RteInData_Common.VehicleModeInternal.currentValue,
                              Unlock_event,
                              DoorAjarState,
                              &LockCtrlSM_State,
                              &Timers[CONST_AutoRelockTimer],
                              &isAutorelockTriggered);
     Alarm_reactivationrequest(&isAutorelockTriggered,
                               &LockCtrlSM_State,
                               &RteOutData_Common.AutoRelock_rqst,
                               &Timers[CONST_AlarmReactRqstTimer]);
   }
   else
   {
      // Do nothing
   }
   //! ##### Check for SpeedLocking_P1B2Q parameter
   if (0U < PCODE_DoorAutoLockingSpeed)
   {
      //! #### Check for speed locking inhibition status
      if (SpeedLockingInhibition_stat_SpeedLockingActivate == RteInData_Common.SpeedLockingInhibition_stat)
      {
         //! #### Process speed lock event logic : 'isSpeedLockEvent()'
         isSysEventSpeedLock = isSpeedLockEvent(&RteInData_Common.WheelBasedVehicleSpeed,
                                                Unlock_event,
                                                DoorAjarState,
                                                &DoorLatchStatus.currentValue);
      }
      else
      {
         // Do nothing: functionality is disabled 
      }
   }
   else
   {
      // Do nothing
   }
      
   //! ##### Process locking control state transitions : 'LockingControlStateTransitions()'
   LockingControlStateTransitions(Unlock_event,
                                  Lock_event,
                                  &isAutorelockTriggered,
                                  isSysEventSpeedLock,
                                  &SelfLock_BackToState,
                                  &isLatchProtectionInactive,
                                  &RteInData_Common.Synch_Unsynch_Mode_stat,
                                  &DoorLatchStatus.currentValue,
                                  &LockCtrlSM_State,
                                  &Timers[CONST_DoorUnlock_Timer],
                                  &Timers[CONST_DoorLock_Timer],
                                  &RemoteOperationStatusIndication);   
   //! ##### Process locking control state output logic : 'LockingControlStateprocessing()'
   LockingControlStateprocessing(&LockCtrlSM_State,
                                 &RteOutData_Common,
                                 &Timers[CONST_DoorUnlock_Timer],
                                 &Timers[CONST_DoorLock_Timer]);
   //! ##### Process the Latch Protection logic: 'LatchProtectionProcessing()'
   LatchProtectionProcessing(&LockCtrlSM_State,
                             &isLatchProtectionInactive,
                             &Timers[CONST_LatchProtectionTimer]);
   //! ##### Process the Lock/Unlock remote operation indication : 'RemoteOperationStatusIndicating()'
   RemoteOperationStatusIndicating(&LockCtrlSM_State,
                                   &DoorLatchStatus.currentValue,
                                   &RteInData_Common.VehicleModeInternal.currentValue,
                                   &RteOutData_Common.LockingIndication_rqst,
                                   &Timers[CONST_HazardLightIndTimer],
                                   &RemoteOperationStatusIndication);
   //! ##### Process dashboard LED indication logic : 'DashboardLEDIndicating()'
   DashboardLEDIndicating(&RteInData_Common.VehicleModeInternal,
                          &DoorLatchStatus,
                          &RteOutData_Common.DoorLockSwitch_DeviceIndication,
                          &Timers[CONST_DashboardLEDIndTimer]);
   RteOutData_Common.DoorLock_stat  = DoorLatchStatus.currentValue;
   RteOutData_Common.DoorsAjar_stat = DoorAjarState; 
   //! ##### Process Rte In data write common logic : 'RteInDataWrite_Common()'
   RteInDataWrite_Common(&RteOutData_Common);
   //! ##### Process Rte Out data with door modules logic : 'RteOutData_WithDoorModules()'
   RteOutData_WithDoorModules(&RteOutData_Common);
   //! ##### Process ANW lock control activation deactivation logic : 'ANW_LockControlActDeactivation()'
   ANW_LockControlActDeactivation(&LockCtrlSM_State,
                                  &Timers[CONST_AutoRelockTimer]);
   //! ##### Process ANW lock switch dashboard LED indication logic : 'ANW_LockSwitchDashboardLedIndication()'
   ANW_LockSwitchDashboardLedIndication(&Timers[CONST_DashboardLEDIndTimer]);
   //! ##### Process ANW update door latch status logic : 'ANW_UpdateDoorLatchStatus()'
   //ANW_UpdateDoorLatchStatus(&isActivationTriggerDetected);   
   LockCtrlSM_State.currentValue = LockCtrlSM_State.newValue;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VehicleAccess_Ctrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VehicleAccess_Ctrl_Init_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution runnable logic for the VehicleAccess_Ctrl_Init
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VehicleAccess_Ctrl_CODE) VehicleAccess_Ctrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VehicleAccess_Ctrl_Init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

#define VehicleAccess_Ctrl_STOP_SEC_CODE
#include "VehicleAccess_Ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

//!======================================================================================
//!
//! \brief
//! This function is processing the 'Get_RteInData_WithDoorModules'
//!
//! \param   *pRteInData_WithDoorModulesLatch  Examine and update the input signals for 'WithDoorModulesLatch' varient based on RTE failure events
//! \param   *pRteInData_WithDoorModulesAjar   Examine and update the input signals for 'WithDoorModulesAjar' varient based on RTE failure events
//!
//!======================================================================================
static void Get_RteInData_WithDoorModules(LatchWithDoorModules_in_StructType *pRteInData_WithDoorModulesLatch, 
                                          AjarWithDoorModules_in_StructType  *pRteInData_WithDoorModulesAjar)
{
   Std_ReturnType retValue = RTE_E_INVALID;
   //! ###### Read RTE interfaces related to door modules and process fallback modes
   //! ##### Read DriverDoorAjar_stat interface
   retValue = Rte_Read_DriverDoorAjar_stat_DoorAjar_stat(&pRteInData_WithDoorModulesAjar->DriverDoorAjar_stat);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_WithDoorModulesAjar->DriverDoorAjar_stat),(DoorAjar_stat_NotAvailable),(DoorAjar_stat_Error))
   //! ##### Read DriverDoorLatch_stat interface
   retValue = Rte_Read_DriverDoorLatch_stat_DoorLatch_stat(&pRteInData_WithDoorModulesLatch->DriverDoorLatch_stat);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_WithDoorModulesLatch->DriverDoorLatch_stat),(DoorLatch_stat_NotAvailable),(DoorLatch_stat_Error))
   //! ##### Read PassengerDoorAjar_stat interface
   retValue = Rte_Read_PassengerDoorAjar_stat_DoorAjar_stat(&pRteInData_WithDoorModulesAjar->PassengerDoorAjar_stat);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_WithDoorModulesAjar->PassengerDoorAjar_stat),(DoorAjar_stat_NotAvailable),(DoorAjar_stat_Error))
   //! ##### Read PassengerDoorLatch_stat interface
   retValue = Rte_Read_PassengerDoorLatch_stat_DoorLatch_stat(&pRteInData_WithDoorModulesLatch->PassengerDoorLatch_stat);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_WithDoorModulesLatch->PassengerDoorLatch_stat),(DoorLatch_stat_NotAvailable),(DoorLatch_stat_Error))
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'Get_RteInDataRead_Common'
//!
//! \param  *pRteInData_Common   Examine and update the input signals based on RTE failure events
//!
//!======================================================================================
static void Get_RteInDataRead_Common(VehicleAccess_Ctrl_in_StructType *pRteInData_Common)
{
   Std_ReturnType retValue = RTE_E_INVALID;
   uint8 pReadCryptoSerialized[sizeof(Crypto_Function_serialized_T)];

   //! ###### Read common RTE interfaces, store the previous value and process fallback modes
   //! ##### Read VehicleModeInternal interface
   pRteInData_Common->VehicleModeInternal.previousValue = pRteInData_Common->VehicleModeInternal.currentValue;
   retValue = Rte_Read_VehicleModeInternal_VehicleMode(&pRteInData_Common->VehicleModeInternal.currentValue);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->VehicleModeInternal.currentValue),(VehicleMode_NotAvailable))
   //! ##### Read DriverDrKeyCylTrn_stat decrypted interface
   pRteInData_Common->DriverDrKeyCylTrn_stat.previousValue = pRteInData_Common->DriverDrKeyCylTrn_stat.currentValue;
   retValue = Rte_Read_DrivrDrKeyCylTrn_st_serialized_Crypto_Function_serialized(&pReadCryptoSerialized[0]);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->DriverDrKeyCylTrn_stat.currentValue),(0U))
   pRteInData_Common->DriverDrKeyCylTrn_stat.currentValue = pReadCryptoSerialized[0];
   //! ##### Read PassengerDrKeyCylTrn_stat decrypted interface
   pRteInData_Common->PassengerDrKeyCylTrn_stat.previousValue = pRteInData_Common->PassengerDrKeyCylTrn_stat.currentValue;
   retValue = Rte_Read_PsgDoorKeyCylTrn_st_serialized_Crypto_Function_serialized(&pReadCryptoSerialized[0]);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->PassengerDrKeyCylTrn_stat.currentValue),(0U))
   pRteInData_Common->PassengerDrKeyCylTrn_stat.currentValue = pReadCryptoSerialized[0];
   //! ##### Read EmergencyDoorsUnlock_rqst decrypted interface
   pRteInData_Common->EmergencyDoorsUnlock_rqst.previousValue = pRteInData_Common->EmergencyDoorsUnlock_rqst.currentValue;
   retValue = Rte_Read_EmergencyDoorsUnlock_rqst_EmergencyDoorsUnlock_rqst(&pRteInData_Common->EmergencyDoorsUnlock_rqst.currentValue);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->EmergencyDoorsUnlock_rqst.currentValue),
                                        (EmergencyDoorsUnlock_rqst_NotAvailable),(EmergencyDoorsUnlock_rqst_Error))
   
   //! ##### Read IncabDoorLockUnlock_rqst interface
   pRteInData_Common->IncabDoorLockUnlock_rqst.previousValue = pRteInData_Common->IncabDoorLockUnlock_rqst.currentValue;
   retValue = Rte_Read_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst(&pRteInData_Common->IncabDoorLockUnlock_rqst.currentValue);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->IncabDoorLockUnlock_rqst.currentValue),(DoorLockUnlock_NotAvailable))
   //! ##### Read KeyfobLockButton_Stat interface
   pRteInData_Common->KeyfobLockButton_Stat.previousValue = pRteInData_Common->KeyfobLockButton_Stat.currentValue;
   retValue = Rte_Read_KeyfobLockButton_Status_PushButtonStatus(&pRteInData_Common->KeyfobLockButton_Stat.currentValue);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->KeyfobLockButton_Stat.currentValue),(PushButtonStatus_NotAvailable))
   //! ##### Read KeyfobSuperLockButton_Stat interface
   pRteInData_Common->KeyfobSuperLockButton_Stat.previousValue = pRteInData_Common->KeyfobSuperLockButton_Stat.currentValue;
   retValue = Rte_Read_KeyfobSuperLockButton_Sta_PushButtonStatus(&pRteInData_Common->KeyfobSuperLockButton_Stat.currentValue);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->KeyfobSuperLockButton_Stat.currentValue),(PushButtonStatus_NotAvailable))
   //! ##### Read KeyfobUnlockButton_Stat interface
   pRteInData_Common->KeyfobUnlockButton_Stat.previousValue = pRteInData_Common->KeyfobUnlockButton_Stat.currentValue;
   retValue = Rte_Read_KeyfobUnlockButton_Status_PushButtonStatus(&pRteInData_Common->KeyfobUnlockButton_Stat.currentValue);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->KeyfobUnlockButton_Stat.currentValue),(PushButtonStatus_NotAvailable))
   //! ##### Read Synch_Unsynch_Mode_stat interface
   pRteInData_Common->Synch_Unsynch_Mode_stat.previousValue = pRteInData_Common->Synch_Unsynch_Mode_stat.currentValue;
   retValue = Rte_Read_Synch_Unsynch_Mode_stat_Synch_Unsynch_Mode_stat(&pRteInData_Common->Synch_Unsynch_Mode_stat.currentValue);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->Synch_Unsynch_Mode_stat.currentValue),
                                        (Synch_Unsynch_Mode_stat_NotAvailable),(Synch_Unsynch_Mode_stat_Error))
   //! ##### Read WRCLockButtonStat interface
   pRteInData_Common->WRCLockButtonStat.previousValue = pRteInData_Common->WRCLockButtonStat.currentValue;
   retValue = Rte_Read_WRCLockButtonStatus_PushButtonStatus(&pRteInData_Common->WRCLockButtonStat.currentValue);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->WRCLockButtonStat.currentValue),
                                        (PushButtonStatus_NotAvailable),(PushButtonStatus_Error))
   //! ##### Read WRCUnlockButtonStat interface
   pRteInData_Common->WRCUnlockButtonStat.previousValue = pRteInData_Common->WRCUnlockButtonStat.currentValue;
   retValue = Rte_Read_WRCUnlockButtonStatus_PushButtonStatus(&pRteInData_Common->WRCUnlockButtonStat.currentValue);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->WRCUnlockButtonStat.currentValue),
                                        (PushButtonStatus_NotAvailable),(PushButtonStatus_Error))
   //! ##### Read WheelBasedVehicleSpeed interface
   retValue = Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(&pRteInData_Common->WheelBasedVehicleSpeed);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->WheelBasedVehicleSpeed),(DBC_UINT16_NOTAVAILABLE),(DBC_UINT16_ERROR))
   //! ##### Read SpeedLockingInhibition_stat interface
   retValue = Rte_Read_SpeedLockingInhibition_stat_SpeedLockingInhibition_stat(&pRteInData_Common->SpeedLockingInhibition_stat);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->SpeedLockingInhibition_stat),(SpeedLockingInhibition_stat_NotAvailable),(SpeedLockingInhibition_stat_Error))
   //! ##### Read AutorelockingMovements_Stat interface
   retValue = Rte_Read_AutorelockingMovements_stat_AutorelockingMovements_stat(&pRteInData_Common->AutorelockingMovements_Stat);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->AutorelockingMovements_Stat),(AutorelockingMovements_stat_NotAvailable),(AutorelockingMovements_stat_Error))
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'RteInDataWrite_Common'
//!
//! \param   *pRteOutData_common   Updating the output signals to the ports of output structure
//!
//!======================================================================================
static void RteInDataWrite_Common(const VehicleAccess_Ctrl_out_StructType *pRteOutData_common)
{
   //! ###### Write all the common output ports
   Rte_Write_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(pRteOutData_common->DriverDoorLatch_rqst);
   Rte_Write_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(pRteOutData_common->PassengerDoorLatch_rqst);
   Rte_Write_DoorLock_stat_DoorLock_stat(pRteOutData_common->DoorLock_stat);
   Rte_Write_DoorsAjar_stat_DoorsAjar_stat(pRteOutData_common->DoorsAjar_stat);
   Rte_Write_KeyfobLocation_rqst_KeyfobLocation_rqst(pRteOutData_common->KeyfobLocation_rqst);
   Rte_Write_DashboardLockSwitch_Devic_DeviceIndication(pRteOutData_common->DoorLockSwitch_DeviceIndication);
   Rte_Write_LockingIndication_rqst_LockingIndication_rqst(pRteOutData_common->LockingIndication_rqst);
   Rte_Write_AutoRelock_rqst_AutoRelock_rqst(pRteOutData_common->AutoRelock_rqst);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'RteOutData_WithDoorModules'
//! 
//! \param   *pRteOutData_common   Updating the output signals for the system with door modules
//!
//!======================================================================================
static void RteOutData_WithDoorModules(const VehicleAccess_Ctrl_out_StructType *pRteOutData_common)
{
   //! ###### Write all the output ports related to with door modules varient
   //uint8 pDoorLatch_rqst_serialized[sizeof(Crypto_Function_serialized_T)]; //need to check about array size
   uint8 pDoorLatch_rqst_serialized[1U];

   pDoorLatch_rqst_serialized[0] = pRteOutData_common->DriverDoorLatch_rqst;
   Rte_Write_DriverDoorLatch_rqt_serialized_Crypto_Function_serialized(&pDoorLatch_rqst_serialized[0]);
   pDoorLatch_rqst_serialized[0] = pRteOutData_common->PassengerDoorLatch_rqst;
   Rte_Write_PsngrDoorLatch_rqst_serialized_Crypto_Function_serialized(&pDoorLatch_rqst_serialized[0]);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the SynchronuousModeProcessing logic
//!
//! \param  *pSynch_Unsynch_Mode   Providing the current value and updating the previous value
//!
//!======================================================================================
static void SynchronuousModeProcessing(Synch_Unsynch_Mode_stat *pSynch_Unsynch_Mode)
{
   if (pSynch_Unsynch_Mode->previousValue != pSynch_Unsynch_Mode->currentValue)
   {
      //! ##### Check for synchronuous mode status
      if (Synch_Unsynch_Mode_stat_UnsynchronizedMode == pSynch_Unsynch_Mode->currentValue)
      {
         pSynch_Unsynch_Mode->previousValue = pSynch_Unsynch_Mode->currentValue;
      }
      else if (Synch_Unsynch_Mode_stat_SynchronizedMode == pSynch_Unsynch_Mode->currentValue)
      {
         // SynchronuousMode set to value Asynchronous
         pSynch_Unsynch_Mode->previousValue = pSynch_Unsynch_Mode->currentValue;
      }
      else
      {
         // Do nothing: ignore the value, keep previous status
      }
   }
   else
   {
      // Do nothing: keep previous status
   }
   // TODO: "SynchronuousMode" value shall be stored in NVRAM (VehicleMode trigger) 
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'DoorLatchStateTransitionProcessing' logic
//!
//! \param  LatchStateIn              Provide the door latch state
//! \param  *pLatchStateOut           Update the door latch state
//! \param  *pTimer_LatchIdleMonitor  To check driver latch timer value and update
//! \param  *pSelfLock_BackToState    Check and update the self lock state
//!
//!======================================================================================
static void DoorLatchStateTransitionProcessing(const DoorLatch_stat_T      LatchStateIn,
                                                     DoorLatch_stat_T      *pLatchStateOut,
                                                     uint16                *pTimer_LatchIdleMonitor,
                                                     SelfLockAction_Enum   *pSelfLock_BackToState)
{  
   //! ##### Process the door "Idle" latch status
   if (DoorLatch_stat_Idle == LatchStateIn)
   {
      //! #### Process the SelfLock actions
      switch (*pSelfLock_BackToState)
      {
         case CONST_SelfLock_B2S_Idle:
            //! #### - start the action pending timeout
            *pTimer_LatchIdleMonitor = CONST_LatchIdleElectricalCtrlTransitionState;
            *pSelfLock_BackToState   = CONST_SelfLock_B2S_PendingUnlock;
         break;
         case CONST_SelfLock_B2S_PendingUnlock:
            //! #### - when the action pending timeout elapsed, trig the back action
            if (CONST_TimerFunctionElapsed == *pTimer_LatchIdleMonitor)
            {
               *pTimer_LatchIdleMonitor = CONST_LatchIdleElectricalCtrlTransitionState;
               *pSelfLock_BackToState   = CONST_SelfLock_B2S_RestoreUnlock;
            }
            else
            {
               // do nothing: continue monitoring and keep previous value
            }
         break;
         case CONST_SelfLock_B2S_RestoreUnlock:
            //! #### - if back action is not executed/not successful in defined timeout, switch the self mechanism to fail
            if (CONST_TimerFunctionElapsed == *pTimer_LatchIdleMonitor)
            {
               *pSelfLock_BackToState = CONST_SelfLock_B2S_Fail;
            }
            else
            {
               // do nothing: continue monitoring and keep previous value
            }
         break;
         default: //CONST_SelfLockEvent_Fail
            //! #### - report the latch in failure state
            *pLatchStateOut = DoorLatch_stat_Error;
         break;
      }
      if (CONST_SelfLock_B2S_Fail == *pSelfLock_BackToState)
      {
         *pLatchStateOut = DoorLatch_stat_Error;
      }
      else
      {
         // do nothing: keep previous value of signal, filtering the Idle state
      }
   }
   else
   {
      //! #### if latch is back to nominal state, leaving the Idle, reinitialize the filtering and self protection mechanism
      *pTimer_LatchIdleMonitor = CONST_TimerFunctionInactive;
      *pSelfLock_BackToState   = CONST_SelfLock_B2S_Idle;
      *pLatchStateOut          = LatchStateIn;
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'LatchProtectionProcessing'
//!
//! \param  *pLockCtrl_SM                Providing the current state
//! \param  *pLatchProtectionInactive    check whether LatchProtection is Active or Inactive
//! \param  *ptimer_LatchProtection      Timer related to latch protection status
//!
//!======================================================================================
static void LatchProtectionProcessing(const SM_States *pLockCtrl_SM,
                                            boolean   *pLatchProtectionInactive,
                                            uint16    *ptimer_LatchProtection)
{
   static sint16 LatchProtectionTimeout = 30000U/20U;
   
   // ##### Increase the protection timeout
   if ((sint16)(PCODE_DoorLatchProtectionTimeWindow) >= LatchProtectionTimeout)
   {
      // do nothing: maximum timeout value reached
   }
   else
   {
      // increase the counter for 20ms
      LatchProtectionTimeout = LatchProtectionTimeout + 1;
   }
   if (((pLockCtrl_SM->currentValue != pLockCtrl_SM->newValue)
      && ((pLockCtrl_SM->newValue == SM_Lock_UnlockDriverDoor)
      || (pLockCtrl_SM->newValue == SM_Lock_UnlockPassengerDoor)
      || (pLockCtrl_SM->newValue == SM_Lock_UnlockBothDoors)))
      && (CONST_TimerFunctionInactive == *ptimer_LatchProtection))
   {
      // operation performed: reduce the protection timeout margines  
      if (LatchProtectionTimeout >= 0U)
      {   
         LatchProtectionTimeout    = LatchProtectionTimeout - ((sint16)PCODE_DoorLatchProtectionTimeWindow / (sint16)PCODE_DoorLatchProtectMaxOperation);
       *pLatchProtectionInactive = FALSE;
      }
      else
      {
         *pLatchProtectionInactive = TRUE;
         *ptimer_LatchProtection   = PCODE_DoorLatchProtectionRestingTime;
      }
   }
   else
   {
      // do nothing: no operation
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'CheckUnlockEvents' logic
//!
//! \param   *pVehicleModecurrent           Providing the current mode of a vehicle
//! \param   *pKeyfobUnlockButton_Stat      Providing keyfob unlock button status
//! \param   *pWRCUnlockButtonStat          Providing WRC unlock button status
//! \param   *pIncabDoorLockUnlockrequest   Providing incab door lock unlock request status
//! \param   *pDriverDrKeyCylTrnstatus      Providing driver door key cylinder door turned status
//! \param   *pPassengerDrKeyCylTrnstatus   Providing passenger door key cylinder door turned status
//! \param   *pEmergencyDoorsUnlock_rqst    Providing emergency doors unlock request status
//! \param   *pDoorLatchStat_current        Providing door latch status
//! \param   *pSynch_Unsynch_Mode           Providing synchronuous mode value
//!
//! \return   EventUnlock_Enum              Returns 'unlockEvent' value
//!
//!======================================================================================
static EventUnlock_Enum CheckUnlockEvents(const VehicleMode_T                *pVehicleModecurrent,
                                          const PushButtonType               *pKeyfobUnlockButton_Stat,
                                          const PushButtonType               *pWRCUnlockButtonStat,
                                          const IncabDoorLockUnlock          *pIncabDoorLockUnlockrequest,
                                          const DriverDrKeyCylTrn            *pDriverDrKeyCylTrnstatus,
                                          const PassengerDrKeyCylTrn         *pPassengerDrKeyCylTrnstatus,
                                          const EmergencyDoorsUnlock_rqst    *pEmergencyDoorsUnlock_rqst,
                                          const DoorLatch_stat_T             *pDoorLatchStat_current,
                                          const Synch_Unsynch_Mode_stat      *pSynch_Unsynch_Mode)
{
   //! ###### Check unlock request events
   EventUnlock_Enum unlockEvent = CONST_IdleUnlockRequest;
   //! ##### Check Passive entry logic status
   if (CONST_UnlckEvt_PassivDrvDrEntry == PassiveEntryLogic)
   {
      unlockEvent = CONST_PassiveEntryUnlckReqDrvDr;
   }
   else if (CONST_UnlckEvt_PassivPsngDrEtry == PassiveEntryLogic)
   {
      unlockEvent = CONST_PassiveEntryUnlckReqPsgrDr;
   }
   //! ##### Processing keyfob unlock request events
   //! ##### Check for keyfob unlock button status
   else if ((PushButtonStatus_Pushed == pKeyfobUnlockButton_Stat->currentValue) 
           && (PushButtonStatus_Pushed != pKeyfobUnlockButton_Stat->previousValue))
   {
      //! #### Check if driver door is already unlocked (asynchronuous mode) 
      if (DoorLock_stat_DriverDoorIsUnlocked == *pDoorLatchStat_current)
      {
         unlockEvent = CONST_KeyfobUnlockReq_PsgrDoor;
      }
      //! #### Check if driver door is not equal to unlocked
      //else if (DoorLock_stat_DriverDoorIsUnlocked != *pDoorLatchStat_current)
      //{
        // unlockEvent = CONST_KeyfobUnlockReq_DrvDoor;
      //}
      else
      {
         unlockEvent = CONST_KeyfobUnlockReq_DrvDoor;
      }
   }
   //! ##### Processing WRC unlock request events
   //! ##### Check WRC unlock button status
   else if ((PushButtonStatus_Pushed == pWRCUnlockButtonStat->currentValue) 
           && (PushButtonStatus_Pushed != pWRCUnlockButtonStat->previousValue))
   {
      //! #### Check for internal changes of vehicle mode
      if ((VehicleMode_Accessory == *pVehicleModecurrent) 
         || (VehicleMode_PreRunning == *pVehicleModecurrent) 
         || (VehicleMode_Running == *pVehicleModecurrent))
      {    
         //! #### Check if driver door is not equal to unlocked 
         if (DoorLock_stat_DriverDoorIsUnlocked != *pDoorLatchStat_current)
         {
            unlockEvent = CONST_WrcUnlockReq_DrvDoor;
         }
         //else if (DoorLock_stat_DriverDoorIsUnlocked == *pDoorLatchStat_current)
         //{
            //unlockEvent = CONST_WrcUnlockReq_PsgrDoor;
         //}
         else
         {
            unlockEvent = CONST_WrcUnlockReq_PsgrDoor;
         }
      }
      else
      {
         // Do nothing: request in not authorised Vehicle mode
      }
   }
   //! ##### Processing incab unlock request event
   //! ##### Check for incab door lock unlock request status
   else if ((DoorLockUnlock_Unlock == pIncabDoorLockUnlockrequest->currentValue) 
           && (DoorLockUnlock_Unlock != pIncabDoorLockUnlockrequest->previousValue))
   {
      unlockEvent = CONST_InCabUnlockRequest;
   }
   //! ##### Processing monostable button request event
   //! ##### Check for incab door lock unlock request status
   else if ((DoorLockUnlock_MonoLockUnlock == pIncabDoorLockUnlockrequest->currentValue) 
           && (DoorLockUnlock_MonoLockUnlock != pIncabDoorLockUnlockrequest->previousValue))
   {
      //! #### Check if one of the doors is locked 
      if ((DoorLock_stat_BothDoorsAreLocked == *pDoorLatchStat_current) 
         || (DoorLock_stat_DriverDoorIsUnlocked == *pDoorLatchStat_current) 
         || (DoorLock_stat_PassengerDoorIsUnlocked == *pDoorLatchStat_current))
      {
         unlockEvent = CONST_MonoStableBtnReq_Unlock;
      }
      else 
      {
         // Do nothing
      }
   }
   //! ##### Processing mechanical unlock request events
   //! ##### Check for sycnhronuous mode status
   else if (Synch_Unsynch_Mode_stat_SynchronizedMode == pSynch_Unsynch_Mode->currentValue)
   {
      // Check key is turn in driver door: passenger door unlock synchronization event
      //! #### Check for driver door key cylinder turned status
      if ((CONST_KeyCyldrTurnedToUnlckPos == pDriverDrKeyCylTrnstatus->currentValue)
         && (CONST_KeyCyldrTurnedToUnlckPos != pDriverDrKeyCylTrnstatus->previousValue))
      { 
         unlockEvent  = CONST_MechUnlockReqFromDrvDoor;
      }
      else
      {
         // Do nothing
      }
      // Check key is turn in passenger door: driver door unlock synchronization event 
      //! #### Check for passenger door key cylinder turned status
      if ((CONST_KeyCyldrTurnedToUnlckPos == pPassengerDrKeyCylTrnstatus->currentValue)
         && (CONST_KeyCyldrTurnedToUnlckPos != pPassengerDrKeyCylTrnstatus->previousValue))
      {  
         unlockEvent = CONST_MechUnlockReqFromPsgrDoor;
      }
      else
      {
         //Do nothing
      }
   }
   else
   {
      // Do nothing: not unlock event detected
   }
   //! ##### Processing emergency unlocking event
   // Overwrite the unlock HMI request, if Emergency request is present 
   if ((EmergencyDoorsUnlock_rqst_EmergencyUnlockRequest == pEmergencyDoorsUnlock_rqst->currentValue) 
      && (EmergencyDoorsUnlock_rqst_EmergencyUnlockRequest != pEmergencyDoorsUnlock_rqst->previousValue))
   {
      unlockEvent = CONST_EmergencyUnlocking;
   }
   else
   {
      // Do nothing
   }
   return unlockEvent;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'CheckLockEvents' logic
//!
//! \param   *pKeyfobLockButton_Stat        Providing keyfob lock button status
//! \param   *pKeyfobSuperLockButton_Stat   Providing keyfob super lock button status
//! \param   *pWRCLockButtonStat            Providing the status of WRC lock button 
//! \param   *pVehicleMode_current          Providing the current mode of a vehicle
//! \param   *pIncabDoorLockUnlockrequest   Providing incab door lock unlock request value
//! \param   *pDoorLatchStatus              Providing the status of door latch
//!
//! \return  EventLock_Enum                 Returns 'lockEvent' value
//!
//!======================================================================================
static EventLock_Enum CheckLockEvents(const PushButtonType         *pKeyfobLockButton_Stat,
                                      const PushButtonType         *pKeyfobSuperLockButton_Stat,
                                      const PushButtonType         *pWRCLockButtonStat,
                                      const VehicleMode_T          *pVehicleMode_current,
                                      const IncabDoorLockUnlock    *pIncabDoorLockUnlockrequest,
                                      const DoorLatchState         *pDoorLatchStatus)

{
   EventLock_Enum lockEvent = CONST_IdleLockRequest;

   //! ###### Check for passive entry logic status
   if (CONST_LockEvt_PassiveDoorLock == PassiveEntryLogic)
   {
      lockEvent = CONST_PassiveEntryLockRequest;
   }
   //! ###### Check for keyfob lock button status
   else if ((PushButtonStatus_Pushed != pKeyfobLockButton_Stat->previousValue) 
           && (PushButtonStatus_Pushed == pKeyfobLockButton_Stat->currentValue))
   {
      lockEvent = CONST_KeyfobLockRequest;
   }  
   //! ###### Check for keyfob super lock button status
   else if ((PushButtonStatus_Pushed != pKeyfobSuperLockButton_Stat->previousValue) 
           && (PushButtonStatus_Pushed == pKeyfobSuperLockButton_Stat->currentValue))
   {
      lockEvent = CONST_KeyfobSuperLockRequest;
   }
   //! ###### Check for WRC lock button status
   else if ((PushButtonStatus_Pushed != pWRCLockButtonStat->previousValue) 
           && (PushButtonStatus_Pushed == pWRCLockButtonStat->currentValue))
   {
      //! ##### Check for any internal changes of vehicle mode
      if ((VehicleMode_Accessory == *pVehicleMode_current)
         || (VehicleMode_PreRunning == *pVehicleMode_current)
         || (VehicleMode_Running == *pVehicleMode_current))
      {  
         lockEvent = CONST_WrcLockRequest;
      }
      else
      {
         // Do nothing: WRC operation is not allowed
      }
   }
   //! ###### Check for incab lock unlock button status
   else if ((DoorLockUnlock_Lock != pIncabDoorLockUnlockrequest->previousValue) 
           && (DoorLockUnlock_Lock == pIncabDoorLockUnlockrequest->currentValue))
   {
      lockEvent = CONST_InCabLockRequest;
   }
   //! ###### Check for monostable button request status
   else if (((DoorLockUnlock_MonoLockUnlock != pIncabDoorLockUnlockrequest->previousValue) 
           && (DoorLockUnlock_MonoLockUnlock == pIncabDoorLockUnlockrequest->currentValue)) 
           && (DoorLock_stat_BothDoorsAreUnlocked == pDoorLatchStatus->currentValue))
   {
      lockEvent = CONST_MonoStableButtonReq_Lock;
   }   
   //! ###### Check for door latch status
   else if (pDoorLatchStatus->currentValue != pDoorLatchStatus->previousValue)
   {
      if (DoorLock_stat_BothDoorsAreUnlocked == pDoorLatchStatus->previousValue)
      {
         if ((DoorLock_stat_DriverDoorIsUnlocked == pDoorLatchStatus->currentValue)
            || (DoorLock_stat_PassengerDoorIsUnlocked == pDoorLatchStatus->currentValue))
         {
            lockEvent = CONST_MechanicalLockRequest;
         }
         else
         {
            // Do nothing 
         }
      }
      else
      {
         // Do nothing 
      }
   }
   else
   {
      // Do nothing: no lock event
   }
   return lockEvent;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the lockControl_SystemEventsLatches
//!
//! \param   *pDoorLatchStatus   Providing current value for input data
//!
//! \return   EventSysLatch_Enum   Returns 'systemEventLatches' value
//!
//!======================================================================================
/*static EventSysLatch_Enum lockControl_SystemEventsLatches(const DoorLatchState *pDoorLatchStatus)
{
   EventSysLatch_Enum systemEventLatches = CONST_IdleLockRequest;

   if (pDoorLatchStatus->previousValue != pDoorLatchStatus->currentValue)
   {
      //! ###### Check for door latch status
      if (DoorLock_stat_BothDoorsAreLocked == pDoorLatchStatus->currentValue)
      {
         systemEventLatches = CONST_DoorsLockOprtnTerminated;
      }
      else if (DoorLock_stat_DriverDoorIsUnlocked == pDoorLatchStatus->currentValue)
      {
         systemEventLatches = CONST_DrvDoorUnlckOprtnTerminated;
      }
      else if (DoorLock_stat_BothDoorsAreUnlocked == pDoorLatchStatus->currentValue)
      {
         systemEventLatches = CONST_DoorsUnlockOprtnTerminated;
      }
      else if (DoorLock_stat_PassengerDoorIsUnlocked == pDoorLatchStatus->currentValue)
      {
         systemEventLatches = CONST_PsgrDoorUnlckOprtnTerminated;
      }
      else
      {
         // Do nothing: no valid status change
      }
   }
   else
   {
      // Do nothing: no door status change
   }
   return systemEventLatches;
}*/
//!======================================================================================
//!
//! \brief
//! This function is processing the LockingControlStateTransitions
//!
//! \param   Unlock_event                           Providing the unlock event occured
//! \param   Lock_event                             Providing the lock event occured
//! \param   *pisAutorelockTriggered                Providing the auto relock event occured
//! \param   isSysEventSpeedLock                    Providing the speed relock event occured
//! \param   *pSelfLockAction                       Providing the SelfLockDefense event occured
//! \param   *pisLatchProtectionInactive            Providing the PreconditionLatchProtection event occured
//! \param   *pSynch_Unsynch_Mode                   Providing the current mode of Synch_Unsynch
//! \param   *pDoorLatchStat_current                Providing the current status of door latch
//! \param   *pLockCtrl_SM                          Providing and updating the current state value
//! \param   *pTimers_unlock                        To check current timer value and update
//! \param   *pTimers_lock                          To check current timer value and update
//! \param   *pRemoteOperationStatusIndication      To check current state value and update
//!
//!======================================================================================
static void LockingControlStateTransitions(const EventUnlock_Enum            Unlock_event,
                                           const EventLock_Enum              Lock_event,
                                           const boolean                     *pisAutorelockTriggered,
                                           const boolean                     isSysEventSpeedLock,
                                           const SelfLockAction_T            *pSelfLockAction,
                                           const boolean                     *pisLatchProtectionInactive,
                                           const Synch_Unsynch_Mode_stat     *pSynch_Unsynch_Mode,
                                           const DoorLatch_stat_T            *pDoorLatchStat_current,
                                                 SM_States                   *pLockCtrl_SM,
                                           const uint16                      *pTimers_unlock,
                                           const uint16                      *pTimers_lock,
                                           const RemoteStatusIndication_Enum *pRemoteOperationStatusIndication)
{
   //! ###### Processing state transitions for locking control
   switch (pLockCtrl_SM->currentValue)
   {
      //! ##### Check for state change from 'UnlockDriverDoor' to 'UnlockBothDoors' or 'Idle'
      case SM_Lock_UnlockDriverDoor:
         //! #### Check unlock request event or synchronuous mode active status
         if ((Synch_Unsynch_Mode_stat_SynchronizedMode == pSynch_Unsynch_Mode->currentValue) 
            || (CONST_PassiveEntryUnlckReqPsgrDr == Unlock_event) 
            || (CONST_KeyfobUnlockReq_DrvDoor == Unlock_event) 
            || (CONST_WrcUnlockReq_DrvDoor == Unlock_event))
         {
            pLockCtrl_SM->newValue = SM_Lock_UnlockBothDoors;
         }
         //! #### Check for door latch status
         else if ((DoorLock_stat_DriverDoorIsUnlocked == *pDoorLatchStat_current) 
                 || (DoorLock_stat_BothDoorsAreUnlocked == *pDoorLatchStat_current))
         {                
            pLockCtrl_SM->newValue = SM_Lock_OperationStatus;
         }
         //! #### Check for Timers_unlock value
         else if (CONST_TimerFunctionElapsed == *pTimers_unlock)
         {              
            pLockCtrl_SM->newValue = SM_Lock_OperationStatus;
         }
         else
         {
            // No event, keep the state until timeout occurs
         }
      break;
      //! ##### Check for state change from 'UnlockPassengerDoor' to 'UnlockBothDoors' or 'Idle'
      case SM_Lock_UnlockPassengerDoor:
            //! #### Check for unlock request event or synchronuous mode status
            if ((Synch_Unsynch_Mode_stat_SynchronizedMode == pSynch_Unsynch_Mode->currentValue) 
               || (CONST_PassiveEntryUnlckReqDrvDr == Unlock_event) 
               || (CONST_KeyfobUnlockReq_PsgrDoor == Unlock_event) 
               || (CONST_WrcUnlockReq_PsgrDoor == Unlock_event))
            {                
               pLockCtrl_SM->newValue = SM_Lock_UnlockBothDoors;
            }
            //! #### Check for door latch status
            else if ((DoorLock_stat_PassengerDoorIsUnlocked == *pDoorLatchStat_current) 
                    || (DoorLock_stat_BothDoorsAreUnlocked == *pDoorLatchStat_current))
            {              
               pLockCtrl_SM->newValue = SM_Lock_OperationStatus;
            }
            //! #### Check for timer value
            else if (CONST_TimerFunctionElapsed == *pTimers_unlock)
            {                
               pLockCtrl_SM->newValue = SM_Lock_OperationStatus;
            }
            else
            {
               // No event, keep the state until timeout occurs
            }
      break;
      //! ##### Check for state change from 'UnlockBothDoors' to 'Idle'
      case SM_Lock_UnlockBothDoors:
         //! #### Check for door latch status
         if (DoorLock_stat_BothDoorsAreUnlocked == *pDoorLatchStat_current)
         {
             pLockCtrl_SM->newValue = SM_Lock_OperationStatus;
         }
         //! #### Check for elapse of DoorUnlock Timer for 'Idle' state
         else if (CONST_TimerFunctionElapsed == *pTimers_unlock)
         {
            pLockCtrl_SM->newValue = SM_Lock_OperationStatus;
         }
         else
         {
            // No event, keep the state until timeout occurs
         }
      break;
      //! ##### Check for state change from 'LockBothDoor' to 'Idle' or 'UnlockBothDoors'
      case SM_Lock_LockBothDoor:
         //! #### Check for door latch status
         if (DoorLock_stat_BothDoorsAreLocked == *pDoorLatchStat_current)
         {
            // Lock operation completed
            pLockCtrl_SM->newValue = SM_Lock_OperationStatus;
         }
         else if (CONST_TimerFunctionElapsed == *pTimers_lock)
         {
            if (TRUE == pLockCtrl_SM->isRemoteOperation)
            {
               // Remote unlock operation: unlock doors
               pLockCtrl_SM->newValue = SM_Lock_UnlockBothDoors;
            } 
            else
            {
               // Mechanical/Incab unlock operation: keep current latch states
               pLockCtrl_SM->newValue = SM_Lock_OperationStatus;
            }
         }
         else
         {
            // No event, keep the state until timeout occurs
         }
      break;
      //! ##### Check for state change from 'Idle' to all states
      case SM_Lock_Idle:
         //! #### Check for unlock request event status
         if ((CONST_InCabUnlockRequest == Unlock_event) 
            || (CONST_MonoStableBtnReq_Unlock == Unlock_event) 
            || (CONST_EmergencyUnlocking == Unlock_event)
            || (CONST_SelfLock_B2S_RestoreUnlock == pSelfLockAction->DrvrLatch))
         {               
            pLockCtrl_SM->newValue = SM_Lock_UnlockBothDoors;
         }
         //! #### Check Unlock request event for 'UnlockDriverDoor' state
         //! #### Remote Unlock request for driver door
         else if ((CONST_PassiveEntryUnlckReqDrvDr == Unlock_event) 
                 || (CONST_KeyfobUnlockReq_DrvDoor == Unlock_event) 
                 || (CONST_WrcUnlockReq_DrvDoor == Unlock_event))
         {
            pLockCtrl_SM->newValue          = SM_Lock_UnlockDriverDoor;
            pLockCtrl_SM->isRemoteOperation = TRUE;
         }
         //! #### Manual Unlock request for driver door
         else if (CONST_MechUnlockReqFromDrvDoor == Unlock_event)
         {
            pLockCtrl_SM->newValue = SM_Lock_UnlockDriverDoor;
         }
         //! #### Check Unlock request event for 'UnlockPassengerDoor' state
         else if ((CONST_PassiveEntryUnlckReqPsgrDr == Unlock_event) 
                 || (CONST_KeyfobUnlockReq_PsgrDoor == Unlock_event) 
                 || (CONST_WrcUnlockReq_PsgrDoor == Unlock_event))
         {
            pLockCtrl_SM->newValue          = SM_Lock_UnlockPassengerDoor;
            pLockCtrl_SM->isRemoteOperation = TRUE;
         }
         //! #### Manual Unlock request for passenger door
         else if ((CONST_MechUnlockReqFromPsgrDoor == Unlock_event)
                 || (CONST_SelfLock_B2S_RestoreUnlock == pSelfLockAction->PsgrLatch))
         {
            pLockCtrl_SM->newValue = SM_Lock_UnlockPassengerDoor;
         }
         //! #### Check Lock request event for 'LockBothDoor' state
         //! #### Remote Lock request for both doors
         else if (((CONST_PassiveEntryLockRequest == Lock_event) 
                 || (CONST_KeyfobLockRequest == Lock_event) 
                 || (CONST_KeyfobSuperLockRequest == Lock_event) 
                 || (CONST_WrcLockRequest == Lock_event))
                 || (TRUE == *pisLatchProtectionInactive))
         {
            pLockCtrl_SM->newValue          = SM_Lock_LockBothDoor;
            pLockCtrl_SM->isRemoteOperation = TRUE;
         }
         //! #### Manual & automatic Lock request for both doors
         else if (((CONST_MechanicalLockRequest == Lock_event) 
                 || (CONST_MonoStableButtonReq_Lock == Lock_event) 
                 || (CONST_InCabLockRequest == Lock_event) 
                 || (TRUE == *pisAutorelockTriggered) 
                 || (TRUE == isSysEventSpeedLock))
                 || (TRUE == *pisLatchProtectionInactive))
         {
            pLockCtrl_SM->newValue = SM_Lock_LockBothDoor;
         }
         else
         {
            // Do nothing: no lock events, keep the idle state
         }
      break;
      case SM_Lock_Error:
       //need to implement
         pLockCtrl_SM->newValue = SM_Lock_Idle;
      break;
      case SM_Lock_OperationStatus:
         if (SM_RemoteStatusIndication_Idle == *pRemoteOperationStatusIndication)
         {
            pLockCtrl_SM->newValue = SM_Lock_Idle;
         }
         else
         {
            // wait indication operation is completed
         }
      break;
      default: //SM_Lock_OperationStatus
          //system Ready transition need to implement
         pLockCtrl_SM->newValue = SM_Lock_Idle;
      break;
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'LockingControlStateprocessing' logic
//!
//! \param   *pLockCtrl_SM         Providing the current state
//! \param   *pRteOutData_Common   Updating output based on current state
//! \param   *pTimers_unlock       To check current timer value and update
//! \param   *pTimers_lock         To check current timer value and update
//!
//!======================================================================================
static void LockingControlStateprocessing(SM_States                         *pLockCtrl_SM,
                                          VehicleAccess_Ctrl_out_StructType *pRteOutData_Common,
                                          uint16                            *pTimers_unlock,
                                          uint16                            *pTimers_lock)
{
   //! ###### Processing state output actions based on current state
   if (pLockCtrl_SM->currentValue != pLockCtrl_SM->newValue)
   {
      switch (pLockCtrl_SM->newValue)
      {
         //! ##### Output actions for UnlockDriverDoor state
         case SM_Lock_UnlockDriverDoor:
            pRteOutData_Common->DriverDoorLatch_rqst    = DoorLatch_rqst_decrypt_UnlockDoorCommand;
            *pTimers_unlock                             = PCODE_DoorLockingFailureTimeout;
            *pTimers_lock                               = CONST_TimerFunctionInactive;
         break;
         //! ##### Output actions for UnlockPassengerDoor state
         case SM_Lock_UnlockPassengerDoor:
            pRteOutData_Common->PassengerDoorLatch_rqst = DoorLatch_rqst_decrypt_UnlockDoorCommand;
            *pTimers_unlock                             = PCODE_DoorLockingFailureTimeout;
            *pTimers_lock                               = CONST_TimerFunctionInactive;
         break;
         //! ##### Output actions for UnlockBothDoors state
         case SM_Lock_UnlockBothDoors:
            pRteOutData_Common->DriverDoorLatch_rqst    = DoorLatch_rqst_decrypt_UnlockDoorCommand;
            pRteOutData_Common->PassengerDoorLatch_rqst = DoorLatch_rqst_decrypt_UnlockDoorCommand;
            *pTimers_unlock                             = PCODE_DoorLockingFailureTimeout;
            *pTimers_lock                               = CONST_TimerFunctionInactive;
         break;
         //! ##### Output actions for LockBothDoor state
         case SM_Lock_LockBothDoor:
            pRteOutData_Common->DriverDoorLatch_rqst    = DoorLatch_rqst_decrypt_LockDoorCommand;
            pRteOutData_Common->PassengerDoorLatch_rqst = DoorLatch_rqst_decrypt_LockDoorCommand;
            *pTimers_lock                               = PCODE_DoorLockingFailureTimeout;
            *pTimers_unlock                             = CONST_TimerFunctionInactive;
         break;
         //! ##### Output actions for Idle state & "operation status" states
         default:
            pRteOutData_Common->DriverDoorLatch_rqst    = DoorLatch_rqst_decrypt_Idle;
            pRteOutData_Common->PassengerDoorLatch_rqst = DoorLatch_rqst_decrypt_Idle;
            *pTimers_unlock                             = CONST_TimerFunctionInactive;
            *pTimers_lock                               = CONST_TimerFunctionInactive;
            pLockCtrl_SM->isRemoteOperation             = FALSE;
         break;
      }
   }
   else
   {
      // Do nothing 
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'DashboardLEDIndicating' logic
//!
//! \param   *pVehicleMode                       Providing the mode of the vehicle
//! \param   *pDoorLatchStatus                   Providing the status of door latch 
//! \param   *pDoorLockSwitch_DeviceIndication   Updating the device indication to on or off
//! \param   *pTimers                            To check current timer value and update
//!
//!======================================================================================
static void DashboardLEDIndicating(const VehicleMode_Value   *pVehicleMode,
                                   const DoorLatchState      *pDoorLatchStatus,
                                         DeviceIndication_T  *pDoorLockSwitch_DeviceIndication,
                                         uint16              *pTimers)
{
   //! ##### Check for door latch state
   // device indication off
   if (DoorLock_stat_BothDoorsAreLocked != pDoorLatchStatus->currentValue)
   {
      *pDoorLockSwitch_DeviceIndication  = DeviceIndication_Off;
      *pTimers                           = CONST_TimerFunctionInactive;
   }
   else // DoorLock_stat_BothDoorsAreLocked
   {
      //! ##### Check for any internal changes of vehicle mode
      // device indication on
      if ((VehicleMode_Accessory == pVehicleMode->currentValue)
         || (VehicleMode_PreRunning == pVehicleMode->currentValue)
         || (VehicleMode_Cranking == pVehicleMode->currentValue)
         || (VehicleMode_Running == pVehicleMode->currentValue))
      {
         *pDoorLockSwitch_DeviceIndication = DeviceIndication_On;
      }
      else if (VehicleMode_Living == pVehicleMode->currentValue)
      {
         if (VehicleMode_Living != pVehicleMode->previousValue)
         {
            *pTimers = PCODE_LockCtrl_DashboardLedTimeout;
         }
         else
         {
            // Keep previous LED indication logic state
         }
         //! #### Check for door latch status
         if (DoorLock_stat_BothDoorsAreLocked != pDoorLatchStatus->previousValue)
         {
            *pTimers = PCODE_LockCtrl_DashboardLedTimeout;
         }
         else
         {
            // Keep previous LED indication logic state
         }
         //! #### Check for timer value
         if (CONST_TimerFunctionInactive != *pTimers)
         {
            *pDoorLockSwitch_DeviceIndication = DeviceIndication_On;
         }
         else
         {
            *pDoorLockSwitch_DeviceIndication = DeviceIndication_Off;
         }
      }
      else
      {
         // Not applicable vehicle mode, switch off the LED
         *pDoorLockSwitch_DeviceIndication = DeviceIndication_Off;
      }
   } 
   
#if PEPS_Monitor_ON
   Rte_Read_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst(&IncabDoorLockUnlock_rqst_currentValue);
   //! ##### Check for incab door lock unlock_rqst status
   if ((DoorLockUnlock_MonoLockUnlock == IncabDoorLockUnlock_rqst_currentValue)
      && (DoorLockUnlock_MonoLockUnlock != IncabDoorLockUnlock_rqst_previousValue))
   {
      if (TRUE == isPeriodicalPepsCheckActive)
      {
         isPeriodicalPepsCheckActive = FALSE;
         isPepsCheckOngoing          = FALSE;
         KeyfobLocation_rqst         = KeyfobLocation_rqst_NoRequest;
      }
      else
      {
         isPeriodicalPepsCheckActive = TRUE;
      }
   }
   else
   {
      // Do nothing 
   }
   IncabDoorLockUnlock_rqst_previousValue = IncabDoorLockUnlock_rqst_currentValue;
   if (TRUE == isPeriodicalPepsCheckActive)
   {
      if (Timeout > 0U)
      {
         Timeout--;
      }
      else
      {
         Timeout = 1000U / 20U;
         if (isPepsCheckOngoing == TRUE)
         {
            isPepsCheckOngoing  = FALSE;
            KeyfobLocation_rqst = KeyfobLocation_rqst_NoRequest;
         }
         else
         {
            isPepsCheckOngoing  = TRUE;
            KeyfobLocation_rqst = KeyfobLocation_rqst_Request;
         }
      }
   }
   else
   {
      // Do nothing 
   }
   Rte_Write_KeyfobLocation_rqst_KeyfobLocation_rqst(KeyfobLocation_rqst);
   //! ##### Check for PEPS monitor status
   Rte_Read_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat(&PEPS_Monitor);
   if ((PEPS_Monitor == KeyfobInCabLocation_stat_Spare1) 
      || (PEPS_Monitor == KeyfobInCabLocation_stat_Spare2)
      || (PEPS_Monitor == KeyfobInCabLocation_stat_Spare3))
   {
      *pDoorLockSwitch_DeviceIndication = DeviceIndication_Blink;
   }
   else if (PEPS_Monitor == KeyfobInCabLocation_stat_Detected_Incab)
   {
      *pDoorLockSwitch_DeviceIndication = DeviceIndication_On;
   }
   else
   {
      *pDoorLockSwitch_DeviceIndication = DeviceIndication_Off;
   }
#endif
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'ProcessDoorLatchesStateFromXDM' logic
//!
//! \param   *pDriverDoorLatch_stat      Providing the status of driver door latch 
//! \param   *pPassengerDoorLatch_stat   Providing the status passenger door latch
//! \param   *pDoorLatchStatus           Updating door latch status based on conditions
//!
//!======================================================================================
static void ProcessDoorLatchesStateFromXDM(const DoorLatch_stat_T   *pDriverDoorLatch_stat,
                                           const DoorLatch_stat_T   *pPassengerDoorLatch_stat,
                                                 DoorLatchState     *pDoorLatchStatus)
{
   pDoorLatchStatus->previousValue = pDoorLatchStatus->currentValue;
   //! ###### Check for driver and passenger door latch status
   if ((DoorLatch_stat_Error == *pDriverDoorLatch_stat) 
      || (DoorLatch_stat_Error == *pPassengerDoorLatch_stat)) // Door latch state error 
   {
       pDoorLatchStatus->currentValue = DoorLock_stat_Error;
   }
   else if ((DoorLatch_stat_NotAvailable == *pDriverDoorLatch_stat) 
           || (DoorLatch_stat_NotAvailable == *pPassengerDoorLatch_stat)) // Door latch state not available
   // RTE_E_NEVER_RECEIVED is reported for ports ($DriverDoorLatch_stat OR $DriverDoorLatch_stat)
   {
      pDoorLatchStatus->currentValue = DoorLock_stat_NotAvailable;
      // If "DoorLatchesState" is equal NotAvailable, corresponding ANW shall be activated in order to update the status (rationale: update is needed due to reset or power on)
   }
   else
   {
      //! ##### Process nominal mode for door latch sensors
      //! ##### Check for driver door latch status  is LockedFiltered
      if (DoorLatch_stat_LatchLockedFiltered == *pDriverDoorLatch_stat)
      {
         //! #### Check for passenger door latch status
         // if PassengerDoorLatch_stat is DoorLatch_stat_LatchLockedFiltered
         if (DoorLatch_stat_LatchLockedFiltered == *pPassengerDoorLatch_stat)
         {
            pDoorLatchStatus->currentValue = DoorLock_stat_BothDoorsAreLocked;
         }
         else if (DoorLatch_stat_LatchUnlockedFiltered == *pPassengerDoorLatch_stat)
         {
            pDoorLatchStatus->currentValue = DoorLock_stat_PassengerDoorIsUnlocked;
         }
         else
         {
            // Exception handling
            pDoorLatchStatus->currentValue = DoorLock_stat_Error;
         }
      }
      //! ##### Check for driver door latch status is UnlockedFiltered
      // if DriverDoorLatch_stat is DoorLatch_stat_LatchUnlockedFiltered
      else if (DoorLatch_stat_LatchUnlockedFiltered == *pDriverDoorLatch_stat)
      {
         //! #### Check for passenger door latch status
         // if pPassengerDoorLatch_stat is DoorLatch_stat_LatchUnlockedFiltered
         if (DoorLatch_stat_LatchUnlockedFiltered == *pPassengerDoorLatch_stat) 
         {
            pDoorLatchStatus->currentValue = DoorLock_stat_BothDoorsAreUnlocked;
         }
         else if (DoorLatch_stat_LatchLockedFiltered == *pPassengerDoorLatch_stat)
         {
            pDoorLatchStatus->currentValue = DoorLock_stat_DriverDoorIsUnlocked;
         }
         else
         {
            // Exception handling
            pDoorLatchStatus->currentValue = DoorLock_stat_Error;
         }
      }
      else
      {
         // Exception handling
         pDoorLatchStatus->currentValue = DoorLock_stat_Error;
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'ProcessDoorAjarStateFromXDM' logic
//!
//! \param   *pDriverDoorAjar_stat      Providing the status of driver door ajar 
//! \param   *pPassengerDoorAjar_stat   Providing the status of passenger door ajar
//!
//! \return   DoorsAjar_stat_T          Returns 'doorAjarState' value
//!
//!======================================================================================
static DoorsAjar_stat_T ProcessDoorAjarStateFromXDM(const DoorAjar_stat_T  *pDriverDoorAjar_stat,
                                                    const DoorAjar_stat_T  *pPassengerDoorAjar_stat)
{
   DoorsAjar_stat_T doorAjarState = DoorAjar_stat_Idle;

   //! ###### Check for driver and passenger door ajar status
   if ((DoorAjar_stat_Error == *pDriverDoorAjar_stat) 
      || (DoorAjar_stat_Error == *pPassengerDoorAjar_stat)) // Door ajar state Error
   {
      doorAjarState = DoorsAjar_stat_Error;
   }
   else if ((DoorAjar_stat_NotAvailable == *pDriverDoorAjar_stat) 
           || (DoorAjar_stat_NotAvailable == *pPassengerDoorAjar_stat))
   // RTE_E_NEVER_RECEIVED is reported for ($DriverDoorAjar_stat OR $PassengerDoorAjar_stat)  Door ajar state Not Available
   {
      doorAjarState = DoorsAjar_stat_NotAvailable;
      // If "DoorLatchesState" is equal NotAvailable, corresponding ANW shall be activated in order to update the status (rationale: update is needed due to reset or power on).
   }
   else
   { 
      //! ##### Process nominal case for doors ajar interfaces
      //! ##### Check door ajar state driver doors is open 
      if (DoorAjar_stat_DoorOpen == *pDriverDoorAjar_stat)
      {
         //! #### Check door ajar state passenger doors status
         if (DoorAjar_stat_DoorOpen == *pPassengerDoorAjar_stat)
         {
            doorAjarState = DoorsAjar_stat_BothDoorsAreOpen;
         }
         else if (DoorAjar_stat_DoorClosed == *pPassengerDoorAjar_stat)
         {
            doorAjarState = DoorsAjar_stat_DriverDoorIsOpen;
         }
         else
         {
            // Exception handling
            doorAjarState = DoorsAjar_stat_Error;
         }
      }
      //! ##### Check door ajar state driver doors is closed 
      else if (DoorAjar_stat_DoorClosed == *pDriverDoorAjar_stat)
      { 
         //! #### Check door ajar state passenger doors status
         if (DoorAjar_stat_DoorOpen == *pPassengerDoorAjar_stat)
         {
            doorAjarState = DoorsAjar_stat_PassengerDoorIsOpen;
         }
         else if (DoorAjar_stat_DoorClosed == *pPassengerDoorAjar_stat)
         {
            doorAjarState = DoorsAjar_stat_BothDoorsAreClosed;
         }
         else
         {
            // Exception handling
            doorAjarState = DoorsAjar_stat_Error;
         }
      }
      else
      {
         // Exception handling
         doorAjarState = DoorsAjar_stat_Error;
      }
   }
   return doorAjarState;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'isSystemEventAutorelock logic'
//!
//! \param  *pautorelockingMovements_Stat   Providing the status of auto relocking movement 
//! \param  *pvehicleModecurrent            Providing the mode of vehicle
//! \param  unlockEvent                     Providing the status of unlock event
//! \param  doorAjarState                   Providing the door ajar state value
//! \param  *pLockCtrl_SM                   Providing current state for input data
//! \param  *pTimers                        To check current timer value and update
//! \param  *pisAutorelockTriggered         To check whether the Autorelock event is Triggered or Not
//!
//!======================================================================================
static void isSystemEventAutorelock(const AutorelockingMovements_stat_T *pautorelockingMovements_Stat,
                                    const VehicleMode_T                 *pvehicleModecurrent,
                                    const EventUnlock_Enum              unlockEvent,
                                    const DoorsAjar_stat_T              doorAjarState,
                                    const SM_States                     *pLockCtrl_SM,
                                          uint16                        *pTimers,
                                          boolean                       *pisAutorelockTriggered)
{
   static AutorelockStateMachine_Enum  autoRelockSM          = SM_Autorelock_Idle;

   switch (autoRelockSM)
   {
      //! ##### Check for state change from 'Pending'
      case SM_Autorelock_Pending:
         // Expicit break through: 80% of common processing between pending & active
      //! ##### Check for state change from 'Active'
      case SM_Autorelock_Active:
         //! #### Check for door ajar state
         if (DoorsAjar_stat_BothDoorsAreClosed != doorAjarState)
         {
            autoRelockSM = SM_Autorelock_Idle;
         }
         else
         {
            //! #### Check no movement in cab autorelock condition is fulfilled
            if (AutorelockingMovements_stat_MovementHasBeenDetected == *pautorelockingMovements_Stat)
            {
               autoRelockSM = SM_Autorelock_Idle;
            }
            else
            {
               //! #### Check for any internal changes of vehicle mode
               if ((VehicleMode_Parked != *pvehicleModecurrent)
                  && (VehicleMode_Living != *pvehicleModecurrent))
               {
                  autoRelockSM = SM_Autorelock_Idle;
               }
               else
               {
                  //! #### Wait the Idle state is reached in Pending Autorelock state
                  if (SM_Autorelock_Pending == autoRelockSM)
                  {
                     if (SM_Lock_Idle == pLockCtrl_SM->currentValue)
                     {
                        autoRelockSM = SM_Autorelock_Active;
                        *pTimers     = (uint16)PCODE_AutoAlarmReactivationTimeout;
                     }
                     else
                     {
                        // Do nothing: wait the unlock operation is completed (Idle state)
                     }
                  }
                  else // SM_Autorelock_Active state
                  {
                     //! #### Process the autorelock timeout if the system remains in Idle
                     if (SM_Lock_Idle != pLockCtrl_SM->currentValue)
                     {
                        autoRelockSM = SM_Autorelock_Idle;
                     }
                     else
                     {
                        //! #### Activate the autorelock timeout
                        if (CONST_TimerFunctionElapsed == *pTimers)
                        {
                           *pisAutorelockTriggered = TRUE;
                           autoRelockSM             = SM_Autorelock_Idle;
                        }
                        else
                        {
                           // Do nothing: wait the timeout elapsed
                        }
                     }
                  }
               }
            }
         }
      break;
      default: 
         //! ##### Process Autorelock_Idle state
         if ((CONST_PassiveEntryUnlckReqDrvDr == unlockEvent) 
            || (CONST_PassiveEntryUnlckReqPsgrDr == unlockEvent) 
            || (CONST_KeyfobUnlockReq_DrvDoor == unlockEvent) 
            || (CONST_KeyfobUnlockReq_PsgrDoor == unlockEvent) 
            || (CONST_WrcUnlockReq_DrvDoor == unlockEvent) 
            || (CONST_WrcUnlockReq_PsgrDoor == unlockEvent))
         {
            //! #### Move state machine to pending
            autoRelockSM = SM_Autorelock_Pending;
            *pTimers     = CONST_TimerFunctionInactive;
         }
         else
         {
            // Do nothing: no autorelock activation event
         }
      break;
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'Alarm_reactivationrequest' logic
//!
//! \param  isAutorelockTriggered   Providing the triggered value of autorelock
//! \param  *pLockCtrl_SM           Providing the current state
//! \param  *pAutoRelock_rqst       Updating the autorelock request
//! \param  *pTimers_AutoRelock     To check current timer value and update
//!
//!======================================================================================
static void Alarm_reactivationrequest(      boolean            *pisAutorelockTriggered,
                                      const SM_States          *pLockCtrl_SM,
                                            AutoRelock_rqst_T  *pAutoRelock_rqst,
                                            uint16             *pTimers_AutoRelock)
{
   if (SM_Lock_OperationStatus == pLockCtrl_SM->newValue)
   {
      //! ##### Check the trigger value of autorelock 
      if (TRUE == *pisAutorelockTriggered)
      {
         if (CONST_TimerFunctionInactive == *pTimers_AutoRelock)
         { 
            *pTimers_AutoRelock     = (uint16)PCODE_AlarmAutoRelockRequestDuration;
            *pisAutorelockTriggered = FALSE;
         }
      }
      else
      {
         // Do nothing: 
      }
   }
   else
   {
    // Do nothing: 
   }
   //! ##### Check the timer status
   if (CONST_TimerFunctionElapsed == *pTimers_AutoRelock)
   {
      *pAutoRelock_rqst = AutoRelock_rqst_Idle;
   }
   else
   {
      *pAutoRelock_rqst   = AutoRelock_rqst_AutoRelockActivated;
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'isSpeedLockEvent' logic
//!
//! \param  *pWheelBasedVehicleSpeed   Providing current value of wheel based vehicle speed
//! \param  Unlock_event               Providing current event value for input
//! \param  doorAjarState              Providing current state of door ajar
//! \param  *pdoorLatchStat_current    Providing current value of door latch
//!
/// \return  boolean                   Returns 'isSpeedLockingTriggered' value
//!
//!======================================================================================
static boolean isSpeedLockEvent(const Speed16bit_T       *pWheelBasedVehicleSpeed,
                                const EventUnlock_Enum   Unlock_event,
                                const DoorsAjar_stat_T   doorAjarState,
                                const DoorLatch_stat_T   *pdoorLatchStat_current)
{
          boolean isSpeedLockingTriggered = FALSE;
   static boolean isSpeedRelockingOnHold  = FALSE;

   //! ###### Check for unlock event status
   if (CONST_IdleUnlockRequest != Unlock_event)
   {
      isSpeedRelockingOnHold = TRUE;
   }
   else
   {
      // Do nothing
   }
   //! ###### Check for speed relocking on hold status
   if (FALSE == isSpeedRelockingOnHold)
   {
      //! ##### Check for doorAjar and doorLatch status
      if ((DoorsAjar_stat_BothDoorsAreClosed == doorAjarState) 
         && (DoorLock_stat_BothDoorsAreLocked != *pdoorLatchStat_current))
      {
         //! #### Check for wheel based vehicle speed 
         if ((*pWheelBasedVehicleSpeed > PCODE_DoorAutoLockingSpeed)
            && (*pWheelBasedVehicleSpeed < CONST_WheelBasedVehicleSpeedMax))
         {
            isSpeedLockingTriggered = TRUE;
         }
         else
         {
            // Do nothing: speed locking threshold is not reached
         }
      }
      else
      {
         // Do nothing: door open is detected
      }
   }
   else // isSpeedRelockingOnHold is TRUE
   {
      if (*pWheelBasedVehicleSpeed <= PCODE_SpeedRelockingReinitThreshold)
      {
         isSpeedRelockingOnHold = FALSE;
      }
      else
      {
         // Do nothing: wait the speed relocking reinitizalition threshold is reached
      }
   }
   return isSpeedLockingTriggered;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'RemoteOperationStatusIndicating logic'
//!
//! \param   *pLockCtrl_SM                      Providing the present state
//! \param   *pDoorLatchStat_current            Providing the current status of door latch
//! \param   *pVehicleMode_current              Providing the current mode of vehicle
//! \param   *pLockingIndication_rqst           Updating the locking indication based on conditions
//! \param   *pTimers                           To check current timer value and update
//! \param   *pRemoteOperationStatusIndication  To know the status of Remote operation
//!
//!======================================================================================
static void RemoteOperationStatusIndicating(const SM_States                   *pLockCtrl_SM,
                                            const DoorLatch_stat_T            *pDoorLatchStat_current,
                                            const VehicleMode_T               *pVehicleMode_current,
                                                  LockingIndication_rqst_T    *pLockingIndication_rqst,
                                                  uint16                      *pTimers,
                                                  RemoteStatusIndication_Enum *pRemoteOperationStatusIndication)
{

   //! ###### Check unlock request event and state machine for 'unlock status indication'
   //! ###### Select the state based on '*pRemoteOperationStatusIndication'
   switch (*pRemoteOperationStatusIndication)
   {
      //! ##### Check for state change from 'RemoteUnlockStatusIndication_Pending' to 'RemoteUnlockStatusIndication_Request' or 'RemoteStatusIndication_Idle'
      case SM_RemoteUnlockStatusIndication_Pending:
         //! #### Check for lock control state 
         if (SM_Lock_OperationStatus == pLockCtrl_SM->newValue)
         {
            //! #### Check for door latch status
            if (DoorLock_stat_BothDoorsAreLocked != *pDoorLatchStat_current)
            {
               *pRemoteOperationStatusIndication   = SM_RemoteUnlockStatusIndication_Request;
               *pTimers                            = 1U + PCODE_DoorLockHazardIndRqstDelay;
            }
            else
            {
               // unlocking status is not confirmed: indication cancelled
               *pRemoteOperationStatusIndication = SM_RemoteStatusIndication_Idle;
            }
         }
         else
         {
            // Do nothing: wait the operation is completed
         }
      break;
      //! ##### Check for state change from 'RemoteLockStatusIndication_Pending' to 'RemoteLockStatusIndication_Request' or 'RemoteStatusIndication_Idle'
      case SM_RemoteLockStatusIndication_Pending:
         if (SM_Lock_OperationStatus == pLockCtrl_SM->newValue)
         {
            //! #### Check for door latch status
            if (DoorLock_stat_BothDoorsAreLocked == *pDoorLatchStat_current)
            {
               *pRemoteOperationStatusIndication   = SM_RemoteLockStatusIndication_Request;
               *pTimers                            = 1U + PCODE_DoorLockHazardIndRqstDelay;
            }
            else
            {
               // Locking status is not confirmed: indication cancelled
               *pRemoteOperationStatusIndication = SM_RemoteStatusIndication_Idle;
            }
         }
         else
         {
            // Do nothing: wait the operation is completed
         }
      break;
      //! ##### Check for state change from 'RemoteUnlockStatusIndication_Request'
      case SM_RemoteUnlockStatusIndication_Request:
      // Explicit break through: common processing
      //! ##### Check for state change from 'RemoteLockStatusIndication_Request' to 'RemoteStatusIndication_RequestActive' or 'RemoteStatusIndication_Idle'
      case SM_RemoteLockStatusIndication_Request:
         if (CONST_TimerFunctionElapsed == *pTimers)
         {
            //! #### Check for internal changes of vehicle mode
            if (VehicleMode_Running != *pVehicleMode_current)
            {
               if (SM_RemoteUnlockStatusIndication_Request == *pRemoteOperationStatusIndication)
               {
                  *pLockingIndication_rqst = LockingIndication_rqst_Unlock;
               }
               else // CONST_RemoteLockStatusIndication_Request
               {
                  *pLockingIndication_rqst = LockingIndication_rqst_Lock;
               }
               *pTimers                          = 1U + PCODE_DoorIndReqDuration;
               *pRemoteOperationStatusIndication = SM_RemoteStatusIndication_RequestActive;
            }
            else
            {
               // Indication is not allowed in running vehicle mode
               *pRemoteOperationStatusIndication = SM_RemoteStatusIndication_Idle;
            }
         }
         else
         {
            // Do nothing, wait timer elapsed
         }
      break;
      //! ##### Check for state change from 'RemoteStatusIndication_RequestActive' to 'RemoteStatusIndication_Idle'
      case SM_RemoteStatusIndication_RequestActive:
         if (CONST_TimerFunctionElapsed == *pTimers)
         {
            *pLockingIndication_rqst          = LockingIndication_rqst_Idle;
            *pRemoteOperationStatusIndication = SM_RemoteStatusIndication_Idle;
         }
         else
         {
            // Do nothing, wait timer elapsed
         }
      break;
      default: // SM_RemoteUnlockStatusIndication_Idle
         //! ##### Check for remote operation value
         if (TRUE == pLockCtrl_SM->isRemoteOperation)
         {
            //! #### Check for lock control value
            if ((SM_Lock_UnlockDriverDoor == pLockCtrl_SM->newValue)
               || (SM_Lock_UnlockPassengerDoor == pLockCtrl_SM->newValue)
               || (SM_Lock_UnlockBothDoors == pLockCtrl_SM->newValue))
            {
               // Check if unlock remove event detected or in origin of the unlock request 
               *pRemoteOperationStatusIndication = SM_RemoteUnlockStatusIndication_Pending;
            }
            // Check if lock operation is engaged 
            else if (SM_Lock_LockBothDoor == pLockCtrl_SM->newValue)
            {
               *pRemoteOperationStatusIndication = SM_RemoteLockStatusIndication_Pending;
            }
            else
            {
               // Do nothing: no operation
            }
         }
         else
         {
            // Do nothing: no operation
         }
      break;
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'ANW_LockControlActDeactivation logic'
//!
//! \param   *pLockCtrl_SM   Providing the current state for input
//! \param   *pTimer         To check current timer value
//!
//!======================================================================================
static void ANW_LockControlActDeactivation(const SM_States *pLockCtrl_SM,
                                           const uint16    *pTimer)
{
   static DeactivationTimer_Type LockControlDeactivationTimer      = 0U;
   static AnwSM_States           ANW_SM_LockCtrlActDeactivation    = { SM_ANW_Inactive };
          FormalBoolean          isActivationTriggerDetected       = NO;
          FormalBoolean          isDeactivationConditionsFulfilled = NO;
          AnwAction_Enum         Anw_Action                        = ANW_Action_None;

   //! ##### Check lock control state machine status
   if ((pLockCtrl_SM->currentValue != pLockCtrl_SM->newValue)
      && (SM_Lock_Idle != pLockCtrl_SM->newValue))
   {
      isActivationTriggerDetected = YES;
   }
   else
   {
      isActivationTriggerDetected = NO; // Do nothing 
   }   
   //! ##### Check lock control state machine status
   //! ##### Check for autorelock timer status   
   if ((SM_Lock_Idle == pLockCtrl_SM->currentValue)
      && (CONST_TimerFunctionInactive == *pTimer))
   {
      isDeactivationConditionsFulfilled = YES;
   }
   else
   {
      isDeactivationConditionsFulfilled = NO; // Do nothing 
   }
   //! ##### Process ANW logic : 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(NO,
                                isActivationTriggerDetected,
                                isDeactivationConditionsFulfilled,
                                CONST_ANWLockDeactivationTimeout,
                                &LockControlDeactivationTimer,
                                &ANW_SM_LockCtrlActDeactivation);
   //! ##### Check for Anw_Action is 'Activate' or 'Deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      Rte_Call_UR_ANW_LockControlActDeactivation_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      Rte_Call_UR_ANW_LockControlActDeactivation_DeactivateIss();
   }
   else
   {
      // Do nothing, keep the current state
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'ANW_LockSwitchDashboardLedIndication logic'
//!
//! \param   *pTimer   To check current timer value
//!
//!======================================================================================
static void ANW_LockSwitchDashboardLedIndication(const uint16 *pTimer)
{
   static DeactivationTimer_Type LockSwitchDeactivationTimer             = 0U;
   static AnwSM_States           ANW_SM_LockSwitchDashboardLedIndication = {SM_ANW_Inactive};
          FormalBoolean          isActivationTriggerDetected             = NO;
          FormalBoolean          isDeactivationConditionsFulfilled       = NO;
          AnwAction_Enum         Anw_Action                              = ANW_Action_None;
   
   //! ##### Check for LockCtrl_DashboardLedTimer status
   if (PCODE_LockCtrl_DashboardLedTimeout == *pTimer)
   {
      isActivationTriggerDetected = YES;
   }
   else
   {
      isActivationTriggerDetected = NO;
   }
   
   if (CONST_TimerFunctionInactive == *pTimer)
   {
      isDeactivationConditionsFulfilled = YES;
   }
   else
   {
      isDeactivationConditionsFulfilled = NO;
   }
   //! ##### Process ANW logic : 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(NO,
                                isActivationTriggerDetected,
                                isDeactivationConditionsFulfilled,
                                CONST_ANWDashboardDeactivationTimeout,
                                &LockSwitchDeactivationTimer,
                                &ANW_SM_LockSwitchDashboardLedIndication);   
   //! ##### Check for Anw_Action is 'Activate' or 'Deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      Rte_Call_UR_ANW_LockControlCabRqst2_Pending_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      Rte_Call_UR_ANW_LockControlCabRqst2_Pending_DeactivateIss();
   }
   else
   {
      // Do nothing, keep the current state
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'ANW_UpdateDoorLatchStatus logic'
//!
//! \param   *pisActivationTriggerDetected   Providing the trigger detection status
//!
//!======================================================================================
static void ANW_UpdateDoorLatchStatus(const FormalBoolean *pisActivationTriggerDetected)
{
   static DeactivationTimer_Type UpdateDoorLatchDeactivationTimer  = 0U;
   static AnwSM_States           ANW_SM_LockControlActDeactivation = { SM_ANW_Inactive };
          AnwAction_Enum         Anw_Action                        = ANW_Action_None;
    
   //! ##### Process ANW logic : 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(NO,
                                *pisActivationTriggerDetected,
                                YES,
                                CONST_ANWDoorLatchDeactivationTimeout,
                                &UpdateDoorLatchDeactivationTimer,
                                &ANW_SM_LockControlActDeactivation);
   //! ##### Check for Anw_Action is 'Activate' or 'Deactivate'
   if (ANW_Action_Activate == Anw_Action)
   {
      //Rte_Call_UR_ANW_LockControlActDeactivation_ActivateIss();
   }
   else if (ANW_Action_Deactivate == Anw_Action)
   {
      //Rte_Call_UR_ANW_LockControlActDeactivation_DeactivateIss();
   }
   else
   {
      // Do nothing, keep the current state
   }
}
//! @}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
