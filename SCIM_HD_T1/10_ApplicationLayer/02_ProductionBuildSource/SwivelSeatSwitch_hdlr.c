/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  SwivelSeatSwitch_hdlr.c
 *        Config:  C:/Personal/GIT/Prj/SCIM-BSP_20200224_R01/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  SwivelSeatSwitch_hdlr
 *  Generated at:  Wed Feb 26 10:49:32 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <SwivelSeatSwitch_hdlr>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
//!======================================================================================
//! \file SwivelSeatSwitch_hdlr.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup SwivelSeatSwitch_hdlr
//! @{
//!
//! \brief
//! SwivelSeatSwitch_hdlr SWC.
//! ASIL Level : QM.
//! This module implements the logic for the SwivelSeatSwitch_hdlr runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * IOHWAB_UINT8
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CY1_DigitalBiLevelHigh_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CY1_DigitalBiLevelLow_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinFaultStatus
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinVoltage_0V2
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_SwivelSeatSwitch_hdlr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "SwivelSeatSwitch_hdlr.h"
#include"FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IOHWAB_UINT8: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelHigh_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelLow_T: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * SeatSwivelStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   SeatSwivelStatus_SeatIsEmptyOrSeatHaveaCorrectPosition (0U)
 *   SeatSwivelStatus_SeatHaveAnIncorrectPosition (1U)
 *   SeatSwivelStatus_Error (2U)
 *   SeatSwivelStatus_NotAvailable (3U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *   FALSE (0U)
 *   TRUE (1U)
 *
 * Record Types:
 * =============
 * SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T: Record with elements
 *   DigitalBiLevelLow of type SEWS_X1CY1_DigitalBiLevelLow_T
 *   DigitalBiLevelHigh of type SEWS_X1CY1_DigitalBiLevelHigh_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void)
 *   boolean Rte_Prm_P1CUD_SwivelSeatWarning_Act_v(void)
 *
 *********************************************************************************************************************/


#define SwivelSeatSwitch_hdlr_START_SEC_CODE
#include "SwivelSeatSwitch_hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_X1CY1_DigitalBiLevelVoltageConfig    (*Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v())
#define PCODE_XP1CUD_SwivelSeatWarning_Act         (Rte_Prm_P1CUD_SwivelSeatWarning_Act_v())

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SwivelSeatSwitch_hdlr_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_SeatSwivelStatus_SeatSwivelStatus(SeatSwivelStatus_T data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BUP_12_SwivelSeatSwitch_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SwivelSeatSwitch_hdlr_20ms_runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//!
//! \brief
//! This function implements the cyclic execution runnable logic for the SwivelSeatSwitch_hdlr_20ms_runnable
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SwivelSeatSwitch_hdlr_CODE) SwivelSeatSwitch_hdlr_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
* DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
* Symbol: SwivelSeatSwitch_hdlr_20ms_runnable
*********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   static SwivelSeatSwitch_hdlr_out_StructType         RteOutData_Common = { {SeatSwivelStatus_NotAvailable,
                                                                              SeatSwivelStatus_NotAvailable} };
   static SwivelSeatSwitch_hdlr_in_StructType          RteInData_Common;
          SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T   DigitalBiLevelVoltage = PCODE_X1CY1_DigitalBiLevelVoltageConfig;
          Std_ReturnType                               retValue              = RTE_E_INVALID;
          VGTT_EcuPinFaultStatus                       FaultStatus           = 0U;
		  
   //! ###### Read the RTE ports and process RTE failure events
   //! ##### Read 'SwcActivation_EngineRun' interface
   retValue = Rte_Read_SwcActivation_EngineRun_EngineRun(&RteInData_Common.SwcActivation_EngineRun);
   MACRO_StdRteRead_IntRPort((retValue),(RteInData_Common.SwcActivation_EngineRun),(NonOperational))

   //! ###### Check for 'SwcActivation_EngineRun' port is equal to 'Operational'
   if (Operational == RteInData_Common.SwcActivation_EngineRun)
   {
      //! ##### Select the 'SwivelSeatWarning_Act' parameter
      if (TRUE == PCODE_XP1CUD_SwivelSeatWarning_Act)
      {
         retValue = Rte_Call_AdiInterface_P_GetAdiPinState_CS(CONST_AdiPin8_SwivelSeat_Hdlr,
                                                              &RteInData_Common.AdiPinVoltage_SwivelSeatHdlr,
                                                              &RteInData_Common.LivingPullUpVoltage_SwivelSeatHdlr,
                                                              &FaultStatus);
         //! #### Process RTE failure events for error indication
         if (RTE_E_OK != retValue)
         {
            // Updating the output port value to error
            Rte_Call_Event_D1BUP_12_SwivelSeatSwitch_SetEventStatus(DEM_EVENT_STATUS_FAILED);
            RteOutData_Common.SeatSwivelStatus.newvalue = SeatSwivelStatus_Error;
         }
         else
         {
            Rte_Call_Event_D1BUP_12_SwivelSeatSwitch_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            //! #### Check for Adi pin different voltage values
            if ((RteInData_Common.AdiPinVoltage_SwivelSeatHdlr > (VGTT_EcuPinVoltage_0V2)DigitalBiLevelVoltage.DigitalBiLevelLow)
                && (RteInData_Common.AdiPinVoltage_SwivelSeatHdlr < (VGTT_EcuPinVoltage_0V2)DigitalBiLevelVoltage.DigitalBiLevelHigh))
            {
               // Update the output port is to previous value                 
               RteOutData_Common.SeatSwivelStatus.newvalue = RteOutData_Common.SeatSwivelStatus.preValue;
            }
            else if (RteInData_Common.AdiPinVoltage_SwivelSeatHdlr < (VGTT_EcuPinVoltage_0V2)DigitalBiLevelVoltage.DigitalBiLevelLow)
            {
               // Update the output port is to value  'Seat have an incorrect position'
               RteOutData_Common.SeatSwivelStatus.newvalue = SeatSwivelStatus_SeatHaveAnIncorrectPosition;
            }
            else if (RteInData_Common.AdiPinVoltage_SwivelSeatHdlr > (VGTT_EcuPinVoltage_0V2)DigitalBiLevelVoltage.DigitalBiLevelHigh)
            {
               // Update the output port is to value 'Seat is empty or seat have a correct position'
               RteOutData_Common.SeatSwivelStatus.newvalue = SeatSwivelStatus_SeatIsEmptyOrSeatHaveaCorrectPosition;
            }
            else
            {
               // do nothing
            }
         }
      }
      else
      {
         //Process if SwivelSeatWarning_Act is equals to False
         // Update the output port is to value 'not available'
         RteOutData_Common.SeatSwivelStatus.newvalue = SeatSwivelStatus_NotAvailable;
      }
   }
   else
   {
      // Deactivation
      RteOutData_Common.SeatSwivelStatus.newvalue = SeatSwivelStatus_NotAvailable;
   }
   //! ###### Process RTE write port
   Rte_Write_SeatSwivelStatus_SeatSwivelStatus(RteOutData_Common.SeatSwivelStatus.newvalue);
   RteOutData_Common.SeatSwivelStatus.preValue = RteOutData_Common.SeatSwivelStatus.newvalue;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define SwivelSeatSwitch_hdlr_STOP_SEC_CODE
#include "SwivelSeatSwitch_hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

//! @}
//! @}
//! @}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
