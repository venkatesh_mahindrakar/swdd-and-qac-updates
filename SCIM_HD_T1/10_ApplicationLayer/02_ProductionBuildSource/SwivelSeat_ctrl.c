/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  SwivelSeat_ctrl.c
 *        Config:  C:/Personal/GIT/Prj/SCIM-BSP_20200224_R01/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  SwivelSeat_ctrl
 *  Generated at:  Wed Feb 26 10:49:32 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <SwivelSeat_ctrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
//!======================================================================================
//! \file SwivelSeat_ctrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup SwivelSeat_ctrl
//! @{
//!
//! \brief
//! SwivelSeat_ctrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the SwivelSeat_ctrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Speed16bit_T
 *   65024 - 65279 Error ; 65280 - 65535 Not available
 *
 *********************************************************************************************************************/

#include "Rte_SwivelSeat_ctrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "SwivelSeat_Ctrl.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T: Integer in interval [0...255]
 * SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T: Integer in interval [0...255]
 * Speed16bit_T: Integer in interval [0...65535]
 *   Unit: [Km_per_h], Factor: 1, Offset: 0
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   InactiveActive_Inactive (0U)
 *   InactiveActive_Active (1U)
 *   InactiveActive_Error (2U)
 *   InactiveActive_NotAvailable (3U)
 * SeatSwivelStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   SeatSwivelStatus_SeatIsEmptyOrSeatHaveaCorrectPosition (0U)
 *   SeatSwivelStatus_SeatHaveAnIncorrectPosition (1U)
 *   SeatSwivelStatus_Error (2U)
 *   SeatSwivelStatus_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1CUD_SwivelSeatWarning_Act_v(void)
 *   SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T Rte_Prm_P1DWQ_SwivelSeatCheck_SetSpeed_v(void)
 *
 *********************************************************************************************************************/


#define SwivelSeat_ctrl_START_SEC_CODE
#include "SwivelSeat_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_P1CUD_SwivelSeatWarning_Act    (Rte_Prm_P1CUD_SwivelSeatWarning_Act_v())
#define PCODE_SwivelSeatCheck_SetSpeed       (Rte_Prm_P1DWQ_SwivelSeatCheck_SetSpeed_v())

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SwivelSeat_ctrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_SeatSwivelStatus_SeatSwivelStatus(SeatSwivelStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_SeatSwivelWarning_rqst_SeatSwivelWarning_rqst(InactiveActive_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SwivelSeat_ctrl_20ms_runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//!
//! \brief
//! This function implements the cyclic execution runnable logic for the SwivelSeat_ctrl_20ms_runnable
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SwivelSeat_ctrl_CODE) SwivelSeat_ctrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SwivelSeat_ctrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   static SwivelSeatCtrl_in_StructType  RteInData_Common;
          InactiveActive_T              SeatSwivelWarning_request = InactiveActive_NotAvailable;
		  float32                       WheelSpeed_data           = 0.0f;
		  
   //! ###### Process RTE Read common logic: 'Get_RteDataRead_Common()'
   Get_RteDataRead_Common(&RteInData_Common);
   WheelSpeed_data = ((float32)RteInData_Common.WheelBasedVehicleSpeed * CONST_WheelSpeed_Resl);
  
   //! ###### Check for 'SwcActivation_EngineRun' port is operational
   if (Operational == RteInData_Common.SwcActivation_EngineRun)
   {
      //! ##### Select the 'SwivelSeatWarning_Act' parameter
      if (TRUE == PCODE_P1CUD_SwivelSeatWarning_Act)
      {
         //! #### Check the SeatSwivelStatus and WheelBasedVehicleSpeed
         if ((SeatSwivelStatus_SeatHaveAnIncorrectPosition == RteInData_Common.SeatSwivelStatus)
            && (0U <= (uint16)WheelSpeed_data)
            && ((uint16)WheelSpeed_data <= 250U)
            && ((uint16)WheelSpeed_data > PCODE_SwivelSeatCheck_SetSpeed))
         {
            // Update the output port is to value 	'Active' 
            SeatSwivelWarning_request = InactiveActive_Active;
         }
         else
         {
            // Update the output port is to value 	'Inactive' 
            SeatSwivelWarning_request = InactiveActive_Inactive;
         }
      }
      else
      {
         // process if PCODE_P1CUD_SwivelSeatWarning_Act equals to FALSE 
         SeatSwivelWarning_request = InactiveActive_NotAvailable;
      }
   }
   else
   {
      // Deactivation
      SeatSwivelWarning_request = InactiveActive_NotAvailable;
   }
   //! ###### Process RTE write port
   Rte_Write_SeatSwivelWarning_rqst_SeatSwivelWarning_rqst(SeatSwivelWarning_request);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define SwivelSeat_ctrl_STOP_SEC_CODE
#include "SwivelSeat_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 //!======================================================================================
//!
//! \brief
//! This function implements the logic for the Get_RteDataRead_Common
//!
//! \param   *pInData_Common    Examine and update the input signals based on RTE failure events
//!
//!======================================================================================
static void Get_RteDataRead_Common(SwivelSeatCtrl_in_StructType  *pInData_Common)
{
   //! ###### Read RTE ports and process RTE failure events for error indication mode
    Std_ReturnType retValue = RTE_E_INVALID;
    //! ##### Read 'SeatSwivelStatus' interface
    retValue = Rte_Read_SeatSwivelStatus_SeatSwivelStatus(&pInData_Common->SeatSwivelStatus);
    MACRO_StdRteRead_IntRPort((retValue),(pInData_Common->SeatSwivelStatus),(SeatSwivelStatus_Error))
    //! ##### Read 'SwcActivation_EngineRun' interface
    retValue = Rte_Read_SwcActivation_EngineRun_EngineRun(&pInData_Common->SwcActivation_EngineRun);
    MACRO_StdRteRead_IntRPort((retValue),(pInData_Common->SwcActivation_EngineRun),(NonOperational))
    //! ##### Read 'WheelBasedVehicleSpeed' interface
    retValue = Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(&pInData_Common->WheelBasedVehicleSpeed);
    MACRO_StdRteRead_ExtRPort((retValue),(pInData_Common->WheelBasedVehicleSpeed),(MAX_NOTAVAILABLE),(MAX_ERRORINDICATOR))
}

//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
