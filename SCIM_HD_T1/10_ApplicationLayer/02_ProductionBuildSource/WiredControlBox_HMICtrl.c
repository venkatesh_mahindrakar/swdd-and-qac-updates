/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  WiredControlBox_HMICtrl.c
 *        Config:  D:/GitSCIM/scim_seoyon/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  WiredControlBox_HMICtrl
 *  Generated at:  Tue Oct 15 21:51:08 2019
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <WiredControlBox_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file WiredControlBox_HMICtrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_VehicleDynamics_Suspension 
//! @{
//! @addtogroup WiredControlBox
//! @{
//!
//! \brief
//! WiredControlBox_HMICtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the WiredControlBox_HMICtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_ECSStopButtonHoldTimeout_P1DWI_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_ECSStopButtonHoldTimeout_P1DWI_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1BWF_Level_Memorization_Min_Press_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1BWF_Level_Memorization_Min_Press_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1BWF_Memory_Recall_Max_Press_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1BWF_Memory_Recall_Max_Press_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1BWF_Memory_Recall_Min_Press_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1BWF_Memory_Recall_Min_Press_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1JSY_DriveStroke_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1JSY_DriveStroke_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1JSY_RampStroke_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1JSY_RampStroke_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1JSZ_DriveStroke_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1JSZ_DriveStroke_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1JSZ_RampStroke_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1JSZ_RampStroke_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_RCECSButtonStucked_P1DWJ_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_RCECSButtonStucked_P1DWJ_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_RCECSUpDownStucked_P1DWK_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_RCECSUpDownStucked_P1DWK_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_RCECS_HoldCircuitTimer_P1IUS_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_RCECS_HoldCircuitTimer_P1IUS_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_WiredControlBox_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
// Includes
#include "WiredControlBox_HMICtrl.h"
#include "WiredControlBox_HMICtrl_Logic_If.h"
#include "FuncLibrary_Timer_If.h"
#include "FuncLibrary_ScimStd_If.h"

#define WiredControlBox_HMICtrl_START_SEC_CODE
#define WiredControlBox_HMICtrl_STOP_SEC_CODE
#include "WiredControlBox_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_ECSStopButtonHoldTimeout_P1DWI_T: Integer in interval [0...255]
 * SEWS_ECSStopButtonHoldTimeout_P1DWI_T: Integer in interval [0...255]
 * SEWS_P1BWF_Level_Memorization_Min_Press_T: Integer in interval [0...255]
 * SEWS_P1BWF_Level_Memorization_Min_Press_T: Integer in interval [0...255]
 * SEWS_P1BWF_Memory_Recall_Max_Press_T: Integer in interval [0...255]
 * SEWS_P1BWF_Memory_Recall_Max_Press_T: Integer in interval [0...255]
 * SEWS_P1BWF_Memory_Recall_Min_Press_T: Integer in interval [0...255]
 * SEWS_P1BWF_Memory_Recall_Min_Press_T: Integer in interval [0...255]
 * SEWS_P1JSY_DriveStroke_T: Integer in interval [0...255]
 * SEWS_P1JSY_DriveStroke_T: Integer in interval [0...255]
 * SEWS_P1JSY_RampStroke_T: Integer in interval [0...255]
 * SEWS_P1JSY_RampStroke_T: Integer in interval [0...255]
 * SEWS_P1JSZ_DriveStroke_T: Integer in interval [0...255]
 * SEWS_P1JSZ_DriveStroke_T: Integer in interval [0...255]
 * SEWS_P1JSZ_RampStroke_T: Integer in interval [0...255]
 * SEWS_P1JSZ_RampStroke_T: Integer in interval [0...255]
 * SEWS_RCECSButtonStucked_P1DWJ_T: Integer in interval [0...255]
 * SEWS_RCECSButtonStucked_P1DWJ_T: Integer in interval [0...255]
 * SEWS_RCECSUpDownStucked_P1DWK_T: Integer in interval [0...255]
 * SEWS_RCECSUpDownStucked_P1DWK_T: Integer in interval [0...255]
 * SEWS_RCECS_HoldCircuitTimer_P1IUS_T: Integer in interval [0...255]
 * SEWS_RCECS_HoldCircuitTimer_P1IUS_T: Integer in interval [0...255]
 * ShortPulseMaxLength_T: Integer in interval [0...15]
 *   Unit: [ms], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * ECSStandByReq_T: Enumeration of integer in interval [0...7] with enumerators
 *   ECSStandByReq_Idle (0U)
 *   ECSStandByReq_StandbyRequested (1U)
 *   ECSStandByReq_StopStandby (2U)
 *   ECSStandByReq_Reserved (3U)
 *   ECSStandByReq_Reserved_01 (4U)
 *   ECSStandByReq_Reserved_02 (5U)
 *   ECSStandByReq_Error (6U)
 *   ECSStandByReq_NotAvailable (7U)
 * EvalButtonRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   EvalButtonRequest_Neutral (0U)
 *   EvalButtonRequest_EvaluatingPush (1U)
 *   EvalButtonRequest_ContinuouslyPushed (2U)
 *   EvalButtonRequest_ShortPush (3U)
 *   EvalButtonRequest_Spare1 (4U)
 *   EvalButtonRequest_Spare2 (5U)
 *   EvalButtonRequest_Error (6U)
 *   EvalButtonRequest_NotAvailable (7U)
 * FalseTrue_T: Enumeration of integer in interval [0...3] with enumerators
 *   FalseTrue_False (0U)
 *   FalseTrue_True (1U)
 *   FalseTrue_Error (2U)
 *   FalseTrue_NotAvaiable (3U)
 * LevelAdjustmentAction_T: Enumeration of integer in interval [0...15] with enumerators
 *   LevelAdjustmentAction_Idle (0U)
 *   LevelAdjustmentAction_UpBasic (1U)
 *   LevelAdjustmentAction_DownBasic (2U)
 *   LevelAdjustmentAction_UpShortMovement (3U)
 *   LevelAdjustmentAction_DownShortMovement (4U)
 *   LevelAdjustmentAction_Reserved (5U)
 *   LevelAdjustmentAction_GotoDriveLevel (6U)
 *   LevelAdjustmentAction_Reserved_01 (7U)
 *   LevelAdjustmentAction_Ferry (8U)
 *   LevelAdjustmentAction_Reserved_02 (9U)
 *   LevelAdjustmentAction_Reserved_03 (10U)
 *   LevelAdjustmentAction_Reserved_04 (11U)
 *   LevelAdjustmentAction_Reserved_05 (12U)
 *   LevelAdjustmentAction_Reserved_06 (13U)
 *   LevelAdjustmentAction_Error (14U)
 *   LevelAdjustmentAction_NotAvailable (15U)
 * LevelAdjustmentAxles_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelAdjustmentAxles_Rear (0U)
 *   LevelAdjustmentAxles_Front (1U)
 *   LevelAdjustmentAxles_Parallel (2U)
 *   LevelAdjustmentAxles_Reserved (3U)
 *   LevelAdjustmentAxles_Reserved_01 (4U)
 *   LevelAdjustmentAxles_Reserved_02 (5U)
 *   LevelAdjustmentAxles_Error (6U)
 *   LevelAdjustmentAxles_NotAvailable (7U)
 * LevelAdjustmentStroke_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelAdjustmentStroke_DriveStroke (0U)
 *   LevelAdjustmentStroke_DockingStroke (1U)
 *   LevelAdjustmentStroke_Reserved (2U)
 *   LevelAdjustmentStroke_Reserved_01 (3U)
 *   LevelAdjustmentStroke_Reserved_02 (4U)
 *   LevelAdjustmentStroke_Reserved_03 (5U)
 *   LevelAdjustmentStroke_Error (6U)
 *   LevelAdjustmentStroke_NotAvailable (7U)
 * LevelControlInformation_T: Enumeration of integer in interval [0...15] with enumerators
 *   LevelControlInformation_Prohibit (0U)
 *   LevelControlInformation_HeightAdjustment (1U)
 *   LevelControlInformation_DockingHeight (2U)
 *   LevelControlInformation_DriveHeight (3U)
 *   LevelControlInformation_KneelingHeight (4U)
 *   LevelControlInformation_FerryFunction (5U)
 *   LevelControlInformation_LimpHome (6U)
 *   LevelControlInformation_Reserved (7U)
 *   LevelControlInformation_Reserved_01 (8U)
 *   LevelControlInformation_Reserved_02 (9U)
 *   LevelControlInformation_Reserved_03 (10U)
 *   LevelControlInformation_Reserved_04 (11U)
 *   LevelControlInformation_Reserved_05 (12U)
 *   LevelControlInformation_Reserved_06 (13U)
 *   LevelControlInformation_Error (14U)
 *   LevelControlInformation_NotAvailable (15U)
 * LevelUserMemoryAction_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelUserMemoryAction_Inactive (0U)
 *   LevelUserMemoryAction_Recall (1U)
 *   LevelUserMemoryAction_Store (2U)
 *   LevelUserMemoryAction_Default (3U)
 *   LevelUserMemoryAction_Reserved (4U)
 *   LevelUserMemoryAction_Reserved_01 (5U)
 *   LevelUserMemoryAction_Error (6U)
 *   LevelUserMemoryAction_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * RampLevelRequest_T: Enumeration of integer in interval [0...15] with enumerators
 *   RampLevelRequest_TakeNoAction (0U)
 *   RampLevelRequest_RampLevelM1 (1U)
 *   RampLevelRequest_RampLevelM2 (2U)
 *   RampLevelRequest_RampLevelM3 (3U)
 *   RampLevelRequest_RampLevelM4 (4U)
 *   RampLevelRequest_RampLevelM5 (5U)
 *   RampLevelRequest_RampLevelM6 (6U)
 *   RampLevelRequest_RampLevelM7 (7U)
 *   RampLevelRequest_NotDefined_04 (8U)
 *   RampLevelRequest_NotDefined_05 (9U)
 *   RampLevelRequest_NotDefined_06 (10U)
 *   RampLevelRequest_NotDefined_07 (11U)
 *   RampLevelRequest_NotDefined_08 (12U)
 *   RampLevelRequest_NotDefined_09 (13U)
 *   RampLevelRequest_ErrorIndicator (14U)
 *   RampLevelRequest_NotAvailable (15U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 * WiredLevelUserMemory_T: Enumeration of integer in interval [0...15] with enumerators
 *   WiredLevelUserMemory_MemOff (0U)
 *   WiredLevelUserMemory_M1 (1U)
 *   WiredLevelUserMemory_M2 (2U)
 *   WiredLevelUserMemory_M3 (3U)
 *   WiredLevelUserMemory_M4 (4U)
 *   WiredLevelUserMemory_M5 (5U)
 *   WiredLevelUserMemory_M6 (6U)
 *   WiredLevelUserMemory_M7 (7U)
 *   WiredLevelUserMemory_Spare (8U)
 *   WiredLevelUserMemory_Spare01 (9U)
 *   WiredLevelUserMemory_Spare02 (10U)
 *   WiredLevelUserMemory_Spare03 (11U)
 *   WiredLevelUserMemory_Spare04 (12U)
 *   WiredLevelUserMemory_Spare05 (13U)
 *   WiredLevelUserMemory_Error (14U)
 *   WiredLevelUserMemory_NotAvailable (15U)
 *
 * Record Types:
 * =============
 * SEWS_ECS_MemSwTimings_P1BWF_s_T: Record with elements
 *   Level_Memorization_Min_Press of type SEWS_P1BWF_Level_Memorization_Min_Press_T
 *   Memory_Recall_Min_Press of type SEWS_P1BWF_Memory_Recall_Min_Press_T
 *   Memory_Recall_Max_Press of type SEWS_P1BWF_Memory_Recall_Max_Press_T
 * SEWS_ECS_MemSwTimings_P1BWF_s_T: Record with elements
 *   Level_Memorization_Min_Press of type SEWS_P1BWF_Level_Memorization_Min_Press_T
 *   Memory_Recall_Min_Press of type SEWS_P1BWF_Memory_Recall_Min_Press_T
 *   Memory_Recall_Max_Press of type SEWS_P1BWF_Memory_Recall_Max_Press_T
 * SEWS_ForcedPositionStatus_Front_P1JSY_s_T: Record with elements
 *   DriveStroke of type SEWS_P1JSY_DriveStroke_T
 *   RampStroke of type SEWS_P1JSY_RampStroke_T
 * SEWS_ForcedPositionStatus_Front_P1JSY_s_T: Record with elements
 *   DriveStroke of type SEWS_P1JSY_DriveStroke_T
 *   RampStroke of type SEWS_P1JSY_RampStroke_T
 * SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T: Record with elements
 *   DriveStroke of type SEWS_P1JSZ_DriveStroke_T
 *   RampStroke of type SEWS_P1JSZ_RampStroke_T
 * SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T: Record with elements
 *   DriveStroke of type SEWS_P1JSZ_DriveStroke_T
 *   RampStroke of type SEWS_P1JSZ_RampStroke_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_ECSStopButtonHoldTimeout_P1DWI_T Rte_Prm_P1DWI_ECSStopButtonHoldTimeout_v(void)
 *   SEWS_RCECSButtonStucked_P1DWJ_T Rte_Prm_P1DWJ_RCECSButtonStucked_v(void)
 *   SEWS_RCECSUpDownStucked_P1DWK_T Rte_Prm_P1DWK_RCECSUpDownStucked_v(void)
 *   SEWS_RCECS_HoldCircuitTimer_P1IUS_T Rte_Prm_P1IUS_RCECS_HoldCircuitTimer_v(void)
 *   boolean Rte_Prm_P1ALT_ECS_PartialAirSystem_v(void)
 *   boolean Rte_Prm_P1ALU_ECS_FullAirSystem_v(void)
 *   SEWS_ECS_MemSwTimings_P1BWF_s_T *Rte_Prm_P1BWF_ECS_MemSwTimings_v(void)
 *   SEWS_ForcedPositionStatus_Front_P1JSY_s_T *Rte_Prm_P1JSY_ForcedPositionStatus_Front_v(void)
 *   SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T *Rte_Prm_P1JSZ_ForcedPositionStatus_Rear_v(void)
 *
 *********************************************************************************************************************/

#define WiredControlBox_HMICtrl_START_SEC_CODE
#include "WiredControlBox_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


#define PCODE_P1ALT_ECS_PartialAirSystem           (Rte_Prm_P1ALT_ECS_PartialAirSystem_v())
#define PCODE_P1ALU_ECS_FullAirSystem              (Rte_Prm_P1ALU_ECS_FullAirSystem_v())
#define PCODE_P1BWF_ECS_MemSwTimings               (*Rte_Prm_P1BWF_ECS_MemSwTimings_v())
#define PCODE_P1JSY_ForcedPositionStatus_Front     (*Rte_Prm_P1JSY_ForcedPositionStatus_Front_v())
#define PCODE_P1JSZ_ForcedPositionStatus_Rear      (*Rte_Prm_P1JSZ_ForcedPositionStatus_Rear_v())

/**********************************************************************************************************************
 *
 * Runnable Entity Name: WiredControlBox_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AdjustButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BackButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BlinkECSWiredLEDs_BlinkECSWiredLEDs(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_LevelControlInformation_LevelControlInformation(LevelControlInformation_T *data)
 *   Std_ReturnType Rte_Read_MemButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_RampLevelRequest_RampLevelRequest(RampLevelRequest_T *data)
 *   Std_ReturnType Rte_Read_SelectButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_StopButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *   Std_ReturnType Rte_Read_WRDownButtonStatus_EvalButtonRequest(EvalButtonRequest_T *data)
 *   Std_ReturnType Rte_Read_WRUpButtonStatus_EvalButtonRequest(EvalButtonRequest_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Adjust_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Down_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_ECSStandByReqRCECS_ECSStandByReqRCECS(ECSStandByReq_T data)
 *   Std_ReturnType Rte_Write_M1_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_M2_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_M3_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_ShortPulseMaxLength_ShortPulseMaxLength(ShortPulseMaxLength_T data)
 *   Std_ReturnType Rte_Write_Up_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_WiredAirSuspensionStopRequest_AirSuspensionStopRequest(FalseTrue_T data)
 *   Std_ReturnType Rte_Write_WiredLevelAdjustmentAction_LevelAdjustmentAction(LevelAdjustmentAction_T data)
 *   Std_ReturnType Rte_Write_WiredLevelAdjustmentAxles_LevelAdjustmentAxles(LevelAdjustmentAxles_T data)
 *   Std_ReturnType Rte_Write_WiredLevelAdjustmentStroke_LevelAdjustmentStroke(LevelAdjustmentStroke_T data)
 *   Std_ReturnType Rte_Write_WiredLevelUserMemory_WiredLevelUserMemory(WiredLevelUserMemory_T data)
 *   Std_ReturnType Rte_Write_WiredLevelUserMemoryAction_LevelUserMemoryAction(LevelUserMemoryAction_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BOI_63_RCECS_ButtonStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
 /**********************************************************************************************************************
  * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
  * Symbol: WiredControlBox_HMICtrl_20ms_runnable_doc
  *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution runnable logic for the WiredControlBox_HMICtrl_20ms_runnable
//!
//!====================================================================================================================

/**********************************************************************************************************************
* DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
*********************************************************************************************************************/
FUNC(void, WiredControlBox_HMICtrl_CODE) WiredControlBox_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
* DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
* Symbol: WiredControlBox_HMICtrl_20ms_runnable
*********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures  
   static WiredControlBox_HMICtrl_in_StructType       RteInData_Common;
   static WiredControlBox_HMICtrl_in_ButtonStructType RteInData_Buttondata;
   static WiredControlBox_HMICtrl_out_StructType      RteOutData_Common = { DeviceIndication_SpareValue,
                                                                            DeviceIndication_SpareValue,
                                                                            ECSStandByReq_NotAvailable,
                                                                            DeviceIndication_SpareValue,
                                                                            DeviceIndication_SpareValue,
                                                                            DeviceIndication_SpareValue,
                                                                            15U,
                                                                            DeviceIndication_SpareValue,
                                                                            FalseTrue_False,
                                                                            LevelAdjustmentAction_NotAvailable,
                                                                            LevelAdjustmentAxles_NotAvailable,
                                                                            LevelAdjustmentStroke_NotAvailable,
                                                                            WiredLevelUserMemory_NotAvailable,
                                                                            LevelUserMemoryAction_NotAvailable };
   static Wcb_Fullaircontrolbox_logic       FullAirControl_States       = { SM_WCB_FullAirControlBox_FrontorM1,
                                                                            SM_WCB_FullAirControlBox_Off };
   static StateInitializationType           stateInitialization;
   static Wcb_LevelAdjustment_FSM           SM_States                   = { SM_WCB_LA_Init,
                                                                            SM_WCB_LA_Init };
   static Wcb_AdjustDriveOrLoadingLevel_FSM SMStates_AdjustDrive        = { SM_WCB_ADLLFSM_Off,
                                                                            SM_WCB_ADLLFSM_Off };
   static uint16 Timers[CONST_NbOfTimers];
   static SEWS_ForcedPositionStatus_Front_P1JSY_s_T front;
   static SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T  rear;
   static SEWS_ECS_MemSwTimings_P1BWF_s_T           ECS_MemSwTimings;
   uint8 rtrn = 0U;
   RteOutData_Common.ShortPulseMaxLength = SHORTPULSEMAXLENGTH;
   ECS_MemSwTimings                      = PCODE_P1BWF_ECS_MemSwTimings;

   //! ###### Process RTE read input logic : 'Get_RteInDataRead_Common()'
   Get_RteInDataRead_Common(&RteInData_Common,
                            &RteInData_Buttondata);
   //! ###### Check for 'SwcActivation_Living' port status is non-operational
   if (NonOperational == RteInData_Common.SwcActivation_Living)
   {
      //! ##### Write all the output ports to not available
      RteOutData_Common.Adjust_DeviceIndication       = DeviceIndication_Off;
      RteOutData_Common.Down_DeviceIndication         = DeviceIndication_Off;
      RteOutData_Common.ECSStandByReqRCECS            = ECSStandByReq_NotAvailable;
      RteOutData_Common.M1_DeviceIndication           = DeviceIndication_Off;
      RteOutData_Common.M2_DeviceIndication           = DeviceIndication_Off;
      RteOutData_Common.M3_DeviceIndication           = DeviceIndication_Off;
      RteOutData_Common.ShortPulseMaxLength           = 0U;
      RteOutData_Common.Up_DeviceIndication           = DeviceIndication_Off;
      RteOutData_Common.WiredAirSuspensionStopRequest = FalseTrue_NotAvaiable;
      RteOutData_Common.WiredLevelAdjustmentAction    = LevelAdjustmentAction_NotAvailable;
      RteOutData_Common.WiredLevelAdjustmentAxles     = LevelAdjustmentAxles_NotAvailable;
      RteOutData_Common.WiredLevelAdjustmentStroke    = LevelAdjustmentStroke_NotAvailable;
      RteOutData_Common.WiredLevelUserMemory          = WiredLevelUserMemory_NotAvailable;
      RteOutData_Common.WiredLevelUserMemoryAction    = LevelUserMemoryAction_NotAvailable;
   }
   //! ###### Check for 'SwcActivation_Living' port status is operational entry
   else if (OperationalEntry == RteInData_Common.SwcActivation_Living)
   {
      //! ##### Process WiredControlBox_HMICtrl default start-up values : 'WiredControlBox_HMICtrl_default_Startup_values()'
      WiredControlBox_HMICtrl_default_Startup_values(&RteInData_Buttondata.StopButtonStatus.currentvalue,
                                                     &RteOutData_Common);
   }
   //! ###### Check for 'SwcActivation_Living' port status is operational
   else if (Operational == RteInData_Common.SwcActivation_Living)
   {
      //! ##### Process WiredControlBox common logic : 'WiredControlBox_common_logic()'
      WiredControlBox_common_logic(&RteInData_Common,
                                   &RteInData_Buttondata,
                                   &RteOutData_Common,
                                   &SM_States,
                                   &SMStates_AdjustDrive,
                                   &stateInitialization,
                                   &ECS_MemSwTimings,
                                   &Timers[CONST_StopLevelTimer],
                                   &Timers[CONST_LevelAdjTimer]);
	     //! #### Check for back button status is pushed
         if (PushButtonStatus_Pushed == RteInData_Buttondata.BackButtonStatus.currentvalue)
         {
            RteOutData_Common.WiredLevelUserMemoryAction = LevelUserMemoryAction_Inactive;
         }
         else
         {
            // Do nothing:keep previous value 
         }
         //! #### Process the output store level logic : 'Storelevel_OutputStorelevel()'
         Storelevel_OutputStorelevel(&RteInData_Buttondata,
                                     &RteOutData_Common.WiredLevelUserMemoryAction,
                                     &ECS_MemSwTimings,
                                     &SM_States,
                                     &Timers[CONST_MemButtonTimer]);
         //! #### Process the output recall level logic : 'Recall_OutputRecalllevel()'
         Recall_OutputRecalllevel(&RteInData_Buttondata,
                                  &RteOutData_Common.WiredLevelUserMemoryAction,
                                  &ECS_MemSwTimings,
                                  &Timers[CONST_MemButtonTimer]);
      // ECS_PARTIAL_AIR_SYSTEM 
      //! ##### Check for 'ECS_PartialAirSystem' parameter status
      if (TRUE == PCODE_P1ALT_ECS_PartialAirSystem)
      {
         // Select axle / memory : partial air RC-ECS 
         RteOutData_Common.WiredLevelUserMemory      = WiredLevelUserMemory_M1;
         RteOutData_Common.WiredLevelAdjustmentAxles = LevelAdjustmentAxles_Rear;
         RteOutData_Common.M1_DeviceIndication       = DeviceIndication_Off;
         RteOutData_Common.M2_DeviceIndication       = DeviceIndication_Off;
         RteOutData_Common.M3_DeviceIndication       = DeviceIndication_Off;
      }
      else
      {
         // Do nothing: keep previous value
      }
      front = PCODE_P1JSY_ForcedPositionStatus_Front;
      rear  = PCODE_P1JSZ_ForcedPositionStatus_Rear;
      //! ##### Check for 'ECS_FullAirSystem' parameter status
      if (TRUE == PCODE_P1ALU_ECS_FullAirSystem)
      {
         //! #### Check for wired level adjustment stroke, front and rear axles
         if ((LevelAdjustmentStroke_DockingStroke == RteOutData_Common.WiredLevelAdjustmentStroke)
            || ((LevelAdjustmentStroke_DriveStroke == RteOutData_Common.WiredLevelAdjustmentStroke)
            && ((FALSE == front.DriveStroke)
            && (FALSE == front.RampStroke))
            && ((FALSE == rear.DriveStroke)
            && (FALSE == rear.RampStroke))))
         {
            //! #### Process the full air control box selection logic : 'Fullaircontrolboxselectionlogic()'
            Fullaircontrolboxselectionlogic(&RteInData_Buttondata,
                                            &RteInData_Common.SelectButtonStatus,
                                            &FullAirControl_States,
                                            &stateInitialization,
                                            &RteOutData_Common);
         }
         //! #### Check the required conditions to select axle/memory : full air, front forced
         else if ((LevelAdjustmentStroke_DriveStroke == RteOutData_Common.WiredLevelAdjustmentStroke)
                 && ((TRUE == front.DriveStroke)
                 && (TRUE == front.RampStroke))
                 && ((FALSE == rear.DriveStroke)
                 && (FALSE == rear.RampStroke)))
         {
            RteOutData_Common.WiredLevelUserMemory      = WiredLevelUserMemory_M3;
            RteOutData_Common.WiredLevelAdjustmentAxles = LevelAdjustmentAxles_Rear;
            RteOutData_Common.M1_DeviceIndication       = DeviceIndication_Off;
            RteOutData_Common.M2_DeviceIndication       = DeviceIndication_Off;
            RteOutData_Common.M3_DeviceIndication       = DeviceIndication_On;
         }
         //! #### Check the required conditions to select axle/memory : full air, rear forced
         else if ((LevelAdjustmentStroke_DriveStroke == RteOutData_Common.WiredLevelAdjustmentStroke)
                 && ((FALSE == front.DriveStroke)
                 && (FALSE == front.RampStroke))
                 && ((TRUE == rear.DriveStroke)
                 && (TRUE == rear.RampStroke)))
         {
            RteOutData_Common.WiredLevelUserMemory      = WiredLevelUserMemory_M1;
            RteOutData_Common.WiredLevelAdjustmentAxles = LevelAdjustmentAxles_Front;
            RteOutData_Common.M1_DeviceIndication       = DeviceIndication_On;
            RteOutData_Common.M2_DeviceIndication       = DeviceIndication_Off;
            RteOutData_Common.M3_DeviceIndication       = DeviceIndication_Off;
         }
         else
         {
            // Do nothing
         }
      }
      else
      {
         // Do nothing: keep previous value
      }
      // Function disable logic 
      //! ##### Check for ECS_PartialAirSystem and ECS_FullAirSystem parameters status
      if ((FALSE == PCODE_P1ALT_ECS_PartialAirSystem) 
         && (FALSE == PCODE_P1ALU_ECS_FullAirSystem))
      {
         // Function disable logic
      }
      else
      {
         //! #### Process the fallback logic : 'WiredControlBox_HMICtrl_Fallback_modes()'
         WiredControlBox_HMICtrl_Fallback_modes(&RteInData_Common.SelectButtonStatus,
         										&RteInData_Buttondata,
                                                &RteOutData_Common.WiredLevelAdjustmentAction,
                                                &RteOutData_Common.WiredLevelAdjustmentAxles);
      }
	  //! ##### Process height adjustment allowed logic : 'WiredControlBox_HMICtrl_behavior_HeightAdjustmentAllowed()'
	  WiredControlBox_HMICtrl_behavior_HeightAdjustmentAllowed(&RteInData_Buttondata.StopButtonStatus.currentvalue, 
                                                               &RteInData_Common.HeightAdjustmentAllowed,
                                                               &stateInitialization,
                                                               &RteOutData_Common);
	  
      //! ##### Process the FunctionButtonStucked logic: 'FunctionButtonStucked()'
      rtrn = FunctionButtonStucked(&RteInData_Buttondata,
                                   &RteInData_Common.SelectButtonStatus,
                                   Timers,
                                   &RteOutData_Common.WiredAirSuspensionStopRequest,
                                   &RteOutData_Common.WiredLevelAdjustmentStroke,
                                   &RteOutData_Common.WiredLevelAdjustmentAction,
                                   &RteOutData_Common.WiredLevelUserMemoryAction,
                                   &RteOutData_Common.WiredLevelUserMemory,
                                   &RteOutData_Common.WiredLevelAdjustmentAxles);
      //! ##### Process ECSStandby request activation logic: 'ECSStandbyrequestactivation()'
      ECSStandbyrequestactivation(&RteInData_Buttondata,
                                  rtrn,
                                  &RteInData_Common.SelectButtonStatus,
                                  &Timers[CONST_StopButtonTimer],
                                  &Timers[RCECS_HoldCircuitId],
                                  &RteOutData_Common.ECSStandByReqRCECS);
	  //! ##### Process blink all LEDs request logic : 'Blink_all_LEDs_rquest()'
	  //! ##### Check for blink ECS wired LEDs
      if (FalseTrue_True == RteInData_Common.BlinkECSWiredLEDs)
      {
         RteOutData_Common.Adjust_DeviceIndication = DeviceIndication_Blink;
         RteOutData_Common.M1_DeviceIndication     = DeviceIndication_Blink;
         RteOutData_Common.M2_DeviceIndication     = DeviceIndication_Blink;
         RteOutData_Common.M3_DeviceIndication     = DeviceIndication_Blink;
         RteOutData_Common.Up_DeviceIndication     = DeviceIndication_Blink;
         RteOutData_Common.Down_DeviceIndication   = DeviceIndication_Blink;
      }
      else
      {
         // Do nothing: keep previous value
      }
   }
   else
   {
       // Do nothing: keep previous value
   }
   //! ###### Timers decrement logic: 'TimerFunction_Tick()'
   // Timers decrement 
   TimerFunction_Tick(CONST_NbOfTimers,
                      Timers);
   //! ###### Process RTE write input logic: 'RteInDatawrite_Common()'
   RteInDatawrite_Common(&RteOutData_Common);
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
/**********************************************************************************************************************
 *
 * Runnable Entity Name: WiredControlBox_HMICtrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: WiredControlBox_HMICtrl_Init_doc
 *********************************************************************************************************************/
 
//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution runnable logic for the WiredControlBox_HMICtrl_Init
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
FUNC(void, WiredControlBox_HMICtrl_CODE) WiredControlBox_HMICtrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: WiredControlBox_HMICtrl_Init
 *********************************************************************************************************************/   
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#define WiredControlBox_HMICtrl_STOP_SEC_CODE
#include "WiredControlBox_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Get_RteInDataRead_Common
//!
//! \param   *pWiredControlBox_HMICtrl_input_data   Reads the signals from the input ports
//! \param   *pWCB_HMIControl_in_Buttondata         Reads the button status from the input ports
//!
//!======================================================================================
static void Get_RteInDataRead_Common(WiredControlBox_HMICtrl_in_StructType       *pWiredControlBox_HMICtrl_input_data,
                                     WiredControlBox_HMICtrl_in_ButtonStructType *pWCB_HMIControl_in_Buttondata)
{
   //! ###### Processing fallback_modes
   Std_ReturnType retValue = RTE_E_INVALID;
   //! ##### Read SwcActivation_Living port interface
   retValue = Rte_Read_SwcActivation_Living_Living(&pWiredControlBox_HMICtrl_input_data->SwcActivation_Living);
   MACRO_StdRteRead_IntRPort((retValue),(pWiredControlBox_HMICtrl_input_data->SwcActivation_Living),(NonOperational))
   //! ##### Read AdjustButtonStatus interface
   pWCB_HMIControl_in_Buttondata->AdjustButtonStatus.previousValue = pWCB_HMIControl_in_Buttondata->AdjustButtonStatus.currentvalue;
   retValue = Rte_Read_AdjustButtonStatus_PushButtonStatus(&pWCB_HMIControl_in_Buttondata->AdjustButtonStatus.currentvalue);
   MACRO_StdRteRead_IntRPort((retValue),(pWCB_HMIControl_in_Buttondata->AdjustButtonStatus.currentvalue),(PushButtonStatus_Error))
   //! ##### Read BackButtonStatus interface
   pWCB_HMIControl_in_Buttondata->BackButtonStatus.previousValue = pWCB_HMIControl_in_Buttondata->BackButtonStatus.currentvalue;
   retValue = Rte_Read_BackButtonStatus_PushButtonStatus(&pWCB_HMIControl_in_Buttondata->BackButtonStatus.currentvalue);
   MACRO_StdRteRead_IntRPort((retValue),(pWCB_HMIControl_in_Buttondata->BackButtonStatus.currentvalue),(PushButtonStatus_Error))
   //! ##### Read BlinkECSWiredLEDs interface
   retValue = Rte_Read_BlinkECSWiredLEDs_BlinkECSWiredLEDs(&pWiredControlBox_HMICtrl_input_data->BlinkECSWiredLEDs);
   MACRO_StdRteRead_IntRPort((retValue),(pWiredControlBox_HMICtrl_input_data->BlinkECSWiredLEDs),(FalseTrue_False))
   //! ##### Read HeightAdjustmentAllowed interface
   pWiredControlBox_HMICtrl_input_data->HeightAdjustmentAllowed.previousValue = pWiredControlBox_HMICtrl_input_data->HeightAdjustmentAllowed.currentvalue;
   retValue = Rte_Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed(&pWiredControlBox_HMICtrl_input_data->HeightAdjustmentAllowed.currentvalue);
   MACRO_StdRteRead_ExtRPort((retValue),(pWiredControlBox_HMICtrl_input_data->HeightAdjustmentAllowed.currentvalue),(FalseTrue_NotAvaiable),(FalseTrue_Error))
   //! ##### Read LevelControlInformation interface
   retValue = Rte_Read_LevelControlInformation_LevelControlInformation(&pWiredControlBox_HMICtrl_input_data->LevelControlInformation);
   MACRO_StdRteRead_ExtRPort((retValue),(pWiredControlBox_HMICtrl_input_data->LevelControlInformation),(FalseTrue_NotAvaiable),(FalseTrue_Error))
   //! ##### Read MemButtonStatus interface
   pWCB_HMIControl_in_Buttondata->MemButtonStatus.previousValue = pWCB_HMIControl_in_Buttondata->MemButtonStatus.currentvalue;
   retValue = Rte_Read_MemButtonStatus_PushButtonStatus(&pWCB_HMIControl_in_Buttondata->MemButtonStatus.currentvalue);
   MACRO_StdRteRead_IntRPort((retValue),(pWCB_HMIControl_in_Buttondata->MemButtonStatus.currentvalue),(PushButtonStatus_Error))
   //! ##### Read RampLevelRequest interface
   pWiredControlBox_HMICtrl_input_data->RampLevelRequest.previousValue = pWiredControlBox_HMICtrl_input_data->RampLevelRequest.currentvalue;
   retValue = Rte_Read_RampLevelRequest_RampLevelRequest(&pWiredControlBox_HMICtrl_input_data->RampLevelRequest.currentvalue);
   MACRO_StdRteRead_IntRPort((retValue),(pWiredControlBox_HMICtrl_input_data->RampLevelRequest.currentvalue),(RampLevelRequest_TakeNoAction))
   //! ##### Read SelectButtonStatus interface
   pWiredControlBox_HMICtrl_input_data->SelectButtonStatus.previousValue = pWiredControlBox_HMICtrl_input_data->SelectButtonStatus.currentvalue;
   retValue = Rte_Read_SelectButtonStatus_PushButtonStatus(&pWiredControlBox_HMICtrl_input_data ->SelectButtonStatus.currentvalue);
   MACRO_StdRteRead_IntRPort((retValue),(pWiredControlBox_HMICtrl_input_data->SelectButtonStatus.currentvalue),(PushButtonStatus_Error))
   //! ##### Read StopButtonStatus interface
   pWCB_HMIControl_in_Buttondata->StopButtonStatus.previousValue = pWCB_HMIControl_in_Buttondata->StopButtonStatus.currentvalue;
   retValue = Rte_Read_StopButtonStatus_PushButtonStatus(&pWCB_HMIControl_in_Buttondata->StopButtonStatus.currentvalue);
   MACRO_StdRteRead_IntRPort((retValue),(pWCB_HMIControl_in_Buttondata->StopButtonStatus.currentvalue),(PushButtonStatus_Error))
   //! ##### Read VehicleMode interface
   pWiredControlBox_HMICtrl_input_data->VehicleMode.previousValue = pWiredControlBox_HMICtrl_input_data->VehicleMode.currentvalue;
   retValue = Rte_Read_VehicleModeInternal_VehicleMode(&pWiredControlBox_HMICtrl_input_data ->VehicleMode.currentvalue);
   MACRO_StdRteRead_IntRPort((retValue),(pWiredControlBox_HMICtrl_input_data->VehicleMode.currentvalue),(VehicleMode_Error))
   //! ##### Read WRDownButtonStatus interface
   pWCB_HMIControl_in_Buttondata->WRDownButtonStatus.previousValue = pWCB_HMIControl_in_Buttondata->WRDownButtonStatus.currentvalue;
   retValue = Rte_Read_WRDownButtonStatus_EvalButtonRequest(&pWCB_HMIControl_in_Buttondata->WRDownButtonStatus.currentvalue);
   MACRO_StdRteRead_IntRPort((retValue),(pWCB_HMIControl_in_Buttondata->WRDownButtonStatus.currentvalue),(EvalButtonRequest_Error))
   //! ##### Read WRUpButtonStatus interface
   pWCB_HMIControl_in_Buttondata->WRUpButtonStatus.previousValue = pWCB_HMIControl_in_Buttondata->WRUpButtonStatus.currentvalue;
   retValue = Rte_Read_WRUpButtonStatus_EvalButtonRequest(&pWCB_HMIControl_in_Buttondata->WRUpButtonStatus.currentvalue);
   MACRO_StdRteRead_IntRPort((retValue),(pWCB_HMIControl_in_Buttondata->WRUpButtonStatus.currentvalue),(EvalButtonRequest_Error))
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the RteInDatawrite_Common
//!
//! \param   *pWiredControlBox_output_data   Update the output signals to ports of output structure
//!
//!======================================================================================
static void RteInDatawrite_Common(const WiredControlBox_HMICtrl_out_StructType *pWiredControlBox_output_data)
{
   //! ###### Processing RTE Indata write common logic
   Rte_Write_Adjust_DeviceIndication_DeviceIndication(pWiredControlBox_output_data->Adjust_DeviceIndication);
   Rte_Write_Down_DeviceIndication_DeviceIndication(pWiredControlBox_output_data->Down_DeviceIndication);
   Rte_Write_ECSStandByReqRCECS_ECSStandByReqRCECS(pWiredControlBox_output_data->ECSStandByReqRCECS);
   Rte_Write_M1_DeviceIndication_DeviceIndication(pWiredControlBox_output_data->M1_DeviceIndication);
   Rte_Write_M2_DeviceIndication_DeviceIndication(pWiredControlBox_output_data->M2_DeviceIndication);
   Rte_Write_M3_DeviceIndication_DeviceIndication(pWiredControlBox_output_data->M3_DeviceIndication);
   Rte_Write_ShortPulseMaxLength_ShortPulseMaxLength(pWiredControlBox_output_data->ShortPulseMaxLength);
   Rte_Write_Up_DeviceIndication_DeviceIndication(pWiredControlBox_output_data->Up_DeviceIndication);
   Rte_Write_WiredAirSuspensionStopRequest_AirSuspensionStopRequest(pWiredControlBox_output_data->WiredAirSuspensionStopRequest);
   Rte_Write_WiredLevelAdjustmentAction_LevelAdjustmentAction(pWiredControlBox_output_data->WiredLevelAdjustmentAction);
   Rte_Write_WiredLevelAdjustmentAxles_LevelAdjustmentAxles(pWiredControlBox_output_data->WiredLevelAdjustmentAxles);
   Rte_Write_WiredLevelAdjustmentStroke_LevelAdjustmentStroke(pWiredControlBox_output_data->WiredLevelAdjustmentStroke);
   Rte_Write_WiredLevelUserMemory_WiredLevelUserMemory(pWiredControlBox_output_data->WiredLevelUserMemory);
   Rte_Write_WiredLevelUserMemoryAction_LevelUserMemoryAction(pWiredControlBox_output_data->WiredLevelUserMemoryAction);
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Simultaneous_button_pushes
//!
//! \param   *pbutton_StopButtonStatuscurrent     Provides the current status of stop button status
//! \param   *pbutton_AdjustButtonStatuscurrent   Provides and updates the current adjust button status
//! \param   *pbutton_BackButtonStatuscurrent     Provides and updates the current back button status
//! \param   *pbutton_MemButtonStatuscurrent      Provides and updates the current memory button status
//! \param   *pbutton_SelectButtonStatuscurrent   Provides and updates the current select button status
//! \param   *pbutton_WRDownButtonStatuscurrent   Provides and updates the current WRDown button status
//! \param   *pbutton_WRUpButtonStatuscurrent     Provides and updates the current WRUp button status
//!
//!======================================================================================
static void Simultaneous_button_pushes(const PushButtonStatus_T  *pbutton_StopButtonStatuscurrent,
                                             PushButtonStatus_T  *pbutton_AdjustButtonStatuscurrent,
                                             PushButtonStatus_T  *pbutton_BackButtonStatuscurrent,
                                             PushButtonStatus_T  *pbutton_MemButtonStatuscurrent,
                                             PushButtonStatus_T  *pbutton_SelectButtonStatuscurrent,
                                             EvalButtonRequest_T *pbutton_WRDownButtonStatuscurrent,
                                             EvalButtonRequest_T *pbutton_WRUpButtonStatuscurrent)
{
   //! ###### Processing simultaneous button pushes logic
   uint8 buttonPushedCount   = 0U;
   boolean AdjustButtonPress = FALSE;
   boolean BackButtonPress   = FALSE;
   boolean SelectButtonPress = FALSE;
   boolean MemButtonPress    = FALSE;
   //! ##### Check for stop button current status
   if (PushButtonStatus_Pushed == *pbutton_StopButtonStatuscurrent)
   {
      *pbutton_AdjustButtonStatuscurrent = PushButtonStatus_Neutral;
      *pbutton_SelectButtonStatuscurrent = PushButtonStatus_Neutral;
      *pbutton_BackButtonStatuscurrent   = PushButtonStatus_Neutral;
      *pbutton_MemButtonStatuscurrent    = PushButtonStatus_Neutral;
      *pbutton_WRUpButtonStatuscurrent   = EvalButtonRequest_Neutral;
      *pbutton_WRDownButtonStatuscurrent = EvalButtonRequest_Neutral;
   }
   //! ##### Check for 'WRUp', 'Memory', 'WRDown' button status
   else if (((PushButtonStatus_Pushed == *pbutton_MemButtonStatuscurrent)
           && ((EvalButtonRequest_EvaluatingPush == *pbutton_WRUpButtonStatuscurrent)
           || (EvalButtonRequest_ContinuouslyPushed == *pbutton_WRUpButtonStatuscurrent)
           || (EvalButtonRequest_ShortPush == *pbutton_WRUpButtonStatuscurrent)))
           || ((PushButtonStatus_Pushed == *pbutton_MemButtonStatuscurrent) 
           && ((EvalButtonRequest_EvaluatingPush == *pbutton_WRDownButtonStatuscurrent)
           || (EvalButtonRequest_ContinuouslyPushed == *pbutton_WRDownButtonStatuscurrent)
           || (EvalButtonRequest_ShortPush == *pbutton_WRDownButtonStatuscurrent))))
   {
      // Do nothing: keep the previous status
   }
   else
   {
      //! ##### Check the AdjustButton, BackButton, SelectButton, MemButton status
      AdjustButtonPress = CheckForButtonPushed(pbutton_AdjustButtonStatuscurrent,
                                               &buttonPushedCount);
      BackButtonPress   = CheckForButtonPushed(pbutton_BackButtonStatuscurrent,
                                               &buttonPushedCount);
      SelectButtonPress = CheckForButtonPushed(pbutton_SelectButtonStatuscurrent,
                                               &buttonPushedCount);
      MemButtonPress    = CheckForButtonPushed(pbutton_MemButtonStatuscurrent,
                                               &buttonPushedCount);
      //! #### Check for WRUp button status
      if ((EvalButtonRequest_EvaluatingPush == *pbutton_WRUpButtonStatuscurrent)
         || (EvalButtonRequest_ContinuouslyPushed == *pbutton_WRUpButtonStatuscurrent)
         || (EvalButtonRequest_ShortPush == *pbutton_WRUpButtonStatuscurrent))
      {
         buttonPushedCount = buttonPushedCount + 1U; 
      }
      else
      {
         // Do nothing: keep previous value
      }
      //! #### Check for WRDown button status
      if ((EvalButtonRequest_EvaluatingPush == *pbutton_WRDownButtonStatuscurrent)
         || (EvalButtonRequest_ContinuouslyPushed == *pbutton_WRDownButtonStatuscurrent)
         || (EvalButtonRequest_ShortPush == *pbutton_WRDownButtonStatuscurrent))
      {
         buttonPushedCount = buttonPushedCount + 1U;
      }
      else
      {
         // Do nothing: keep previous value
      }
      //! #### Check for number of buttons 'Pushed' simultaneously
      if (buttonPushedCount > 1U)
      {
         if (TRUE == AdjustButtonPress)
         {
            *pbutton_AdjustButtonStatuscurrent = PushButtonStatus_Neutral;
         }
         else
         {
            // Do nothing: keep previous value
         }
         if (TRUE == BackButtonPress)
         {
            *pbutton_BackButtonStatuscurrent = PushButtonStatus_Neutral;
         }
         else
         {
            // Do nothing: keep previous value
         }
         if (TRUE == SelectButtonPress)
         {
            *pbutton_SelectButtonStatuscurrent = PushButtonStatus_Neutral;
         }
         else
         {
            // Do nothing: keep previous value
         }
         // 'MEM + WRUp' or 'MEM + WRDown' 
         if (TRUE == MemButtonPress)
         {
            *pbutton_MemButtonStatuscurrent = PushButtonStatus_Neutral;
         }
         else
         {
            // Do nothing: keep previous value
         }
         // WRUpButtonStatus is equal to EvaluatingPush, ContinuouslyPushed, ShortPush 
         if ((EvalButtonRequest_EvaluatingPush == *pbutton_WRUpButtonStatuscurrent)
            || (EvalButtonRequest_ContinuouslyPushed == *pbutton_WRUpButtonStatuscurrent)
            || (EvalButtonRequest_ShortPush == *pbutton_WRUpButtonStatuscurrent))
         {
            *pbutton_WRUpButtonStatuscurrent = EvalButtonRequest_Neutral;
         }
         else
         {
            // Do nothing: keep previous value
         }
         // WRDownButtonStatus is equal to EvaluatingPush, ContinuouslyPushed, ShortPush
         if ((EvalButtonRequest_EvaluatingPush == *pbutton_WRDownButtonStatuscurrent)
            || (EvalButtonRequest_ContinuouslyPushed == *pbutton_WRDownButtonStatuscurrent)
            || (EvalButtonRequest_ShortPush == *pbutton_WRDownButtonStatuscurrent))
         {
            *pbutton_WRDownButtonStatuscurrent = EvalButtonRequest_Neutral;
         }
         else
         {
            // Do nothing: keep previous value
         }
      }
      else
      {
         // Do nothing: keep previous value
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the CheckForButtonPushed
//!
//! \param    *pcurrentinput        Check whether any button is pushed
//! \param    *pbuttonPushedCount   Temporary variable to check for simultaneous button push
//!
//! \return   ifbuttonpushed        Returns the buttonpushed return value
//!
//!======================================================================================
static boolean CheckForButtonPushed(const PushButtonStatus_current *pcurrentinput,
                                          uint8                    *pbuttonPushedCount)
{
   //! ###### Counting the input button's which are pushed
   boolean ifbuttonpushed = FALSE;

   if (PushButtonStatus_Pushed == *pcurrentinput)
   {
      *pbuttonPushedCount = *pbuttonPushedCount + 1U;
      ifbuttonpushed      = TRUE;
   }
   else
   {
      // do nothing:keep previous value
   }
   return ifbuttonpushed;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the WiredControlBox_HMICtrl_default_Startup_values
//!
//! \param   *pStopButtonStatuscurrent             Provides the current button status of Stop button
//! \param   *pWiredCntrlBox_HMICtrl_output_data   Updating the output ports with default values
//!
//!======================================================================================
static void WiredControlBox_HMICtrl_default_Startup_values(const PushButtonStatus_current               *pStopButtonStatuscurrent,
                                                                 WiredControlBox_HMICtrl_out_StructType *pWiredCntrlBox_HMICtrl_output_data)
{
   //! ##### Check for stop button current status
   if ((PushButtonStatus_NotAvailable == *pStopButtonStatuscurrent)
      || (PushButtonStatus_Error == *pStopButtonStatuscurrent))
   {
      pWiredCntrlBox_HMICtrl_output_data->WiredAirSuspensionStopRequest = FalseTrue_NotAvaiable;
   }
   else
   {
      // Do nothing: keep previous value
   }
   //! ##### Updating the output ports with Default startup values 
   pWiredCntrlBox_HMICtrl_output_data->WiredLevelAdjustmentAction = LevelAdjustmentAction_Idle;
   pWiredCntrlBox_HMICtrl_output_data->WiredLevelAdjustmentAxles  = LevelAdjustmentAxles_Parallel;
   pWiredCntrlBox_HMICtrl_output_data->WiredLevelAdjustmentStroke = LevelAdjustmentStroke_DockingStroke;
   pWiredCntrlBox_HMICtrl_output_data->WiredLevelUserMemoryAction = LevelUserMemoryAction_Inactive;
   pWiredCntrlBox_HMICtrl_output_data->WiredLevelAdjustmentAxles  = LevelAdjustmentAxles_Parallel;
   pWiredCntrlBox_HMICtrl_output_data->WiredLevelUserMemory       = WiredLevelUserMemory_MemOff;
   pWiredCntrlBox_HMICtrl_output_data->Adjust_DeviceIndication    = DeviceIndication_Off;
   pWiredCntrlBox_HMICtrl_output_data->M1_DeviceIndication        = DeviceIndication_Off;
   pWiredCntrlBox_HMICtrl_output_data->M2_DeviceIndication        = DeviceIndication_Off;
   pWiredCntrlBox_HMICtrl_output_data->M3_DeviceIndication        = DeviceIndication_Off;
   pWiredCntrlBox_HMICtrl_output_data->Up_DeviceIndication        = DeviceIndication_Off;
   pWiredCntrlBox_HMICtrl_output_data->Down_DeviceIndication      = DeviceIndication_Off;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the WiredControlBox_HMICtrl_behavior_HeightAdjustmentAllowed
//!
//! \param   *pbutton_StopButtonStatuscurrent   Provides the current button status of the stopbutton
//! \param   *pHeightAdjustmentAllowed          Provides the current value of the height adjustment allowed
//! \param   *pInitializationState              Provides and specifies the intialization status
//! \param   *pWiredCntrlBox_output_data        Updating output signals based on conditions
//!
//!======================================================================================
static void WiredControlBox_HMICtrl_behavior_HeightAdjustmentAllowed(const      PushButtonStatus_T               *pbutton_StopButtonStatuscurrent,
                                                                     const FalseTrue                              *pHeightAdjustmentAllowed,
                                                                           StateInitializationType                *pInitializationState,
                                                                           WiredControlBox_HMICtrl_out_StructType *pWiredCntrlBox_output_data)
{
   //! ##### Check for height adjustment allowed status
   if (FalseTrue_False == pHeightAdjustmentAllowed->currentvalue)
   {
      pWiredCntrlBox_output_data->Adjust_DeviceIndication = DeviceIndication_Off;
      pWiredCntrlBox_output_data->M1_DeviceIndication     = DeviceIndication_Off;
      pWiredCntrlBox_output_data->M2_DeviceIndication     = DeviceIndication_Off;
      pWiredCntrlBox_output_data->M3_DeviceIndication     = DeviceIndication_Off;
      pWiredCntrlBox_output_data->Up_DeviceIndication     = DeviceIndication_Off;
      pWiredCntrlBox_output_data->Down_DeviceIndication   = DeviceIndication_Off;
   }
   else
   {
      // Do nothing: keep previous value
   }
   if ((FalseTrue_False == pHeightAdjustmentAllowed->previousValue) 
      && (FalseTrue_True == pHeightAdjustmentAllowed->currentvalue))
   {
      //! #### Check for StopButton current status
      if ((PushButtonStatus_NotAvailable == *pbutton_StopButtonStatuscurrent)
         || (PushButtonStatus_Error == *pbutton_StopButtonStatuscurrent))
      {
         pWiredCntrlBox_output_data->WiredAirSuspensionStopRequest = FalseTrue_NotAvaiable;
      }
      else
      {
         // Do nothing:keep previous value
      }
      //! #### Update the output ports to intial values
      pWiredCntrlBox_output_data->WiredLevelAdjustmentAxles   = LevelAdjustmentAxles_Parallel;
      pWiredCntrlBox_output_data->WiredLevelAdjustmentStroke  = LevelAdjustmentStroke_DockingStroke;
      pWiredCntrlBox_output_data->WiredLevelUserMemoryAction  = LevelUserMemoryAction_Inactive;
      pWiredCntrlBox_output_data->WiredLevelAdjustmentAxles   = LevelAdjustmentAxles_Parallel;
      pWiredCntrlBox_output_data->WiredLevelUserMemory        = WiredLevelUserMemory_MemOff;
      pWiredCntrlBox_output_data->Adjust_DeviceIndication     = DeviceIndication_Off;
      pWiredCntrlBox_output_data->M1_DeviceIndication         = DeviceIndication_Off;
      pWiredCntrlBox_output_data->M2_DeviceIndication         = DeviceIndication_Off;
      pWiredCntrlBox_output_data->M3_DeviceIndication         = DeviceIndication_Off;
      pInitializationState->FSM_Initialization                = CONST_Noninitialized;
      pInitializationState->Adjust_Initialization             = CONST_Noninitialized;
      pInitializationState->Fullaircontrolbox_Initialization  = CONST_Noninitialized;
   }
   else
   {
      // Do nothing:keep previous value
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the AdjustDriveOrLoadingLevel_FSM
//!
//! \param   *pAdjustBtnStatus               Providing the button status of adjust button
//! \param   *pBackBtnStatus                 Providing the button status of back button
//! \param   *pRampLevelRequest              Providing ramp level Request value
//! \param   *pStopButtonStatus              Providing the button status of StopButton
//! \param   *pSMStates_AdjustDrive          Providing and updating the current state of AdjustDriveOrLoadingLevel FSM
//! \param   *pstateInitialize_Adjust        Providing and updating the initialization value
//! \param   *pAdjust_DeviceIndication       Updating device indication current value
//! \param   *pWiredLevelAdjustmentStroke    Updating to docking stroke, drive stroke or error value
//! \param   *pFSM_WiredLevelUserMemAction   Updating current UserMemAction value
//!
//!======================================================================================
static void AdjustDriveOrLoadingLevel_FSM(const PushButtonStatus                   *pAdjustBtnStatus,
                                          const PushButtonStatus                   *pBackBtnStatus,
                                          const RampLevelRequest                   *pRampLevelRequest,
                                          const PushButtonStatus                   *pStopButtonStatus,
                                                Wcb_AdjustDriveOrLoadingLevel_FSM  *pSMStates_AdjustDrive,
                                                StateInitializationType            *pstateInitialize_Adjust,
                                                DeviceIndication_T                 *pAdjust_DeviceIndication,
                                                LevelAdjustmentStroke_T            *pWiredLevelAdjustmentStroke,
                                                LevelUserMemoryAction_T            *pFSM_WiredLevelUserMemAction)
{
   //! ###### Processing state transitions 
   if (CONST_Noninitialized == pstateInitialize_Adjust->Adjust_Initialization)
   {
      pstateInitialize_Adjust->Adjust_Initialization = CONST_Initialized;
      pSMStates_AdjustDrive->newValue                = SM_WCB_ADLLFSM_Off;
      *pAdjust_DeviceIndication                      = DeviceIndication_Off;
      *pWiredLevelAdjustmentStroke                   = LevelAdjustmentStroke_DockingStroke;
   }
   else
   {
      switch (pSMStates_AdjustDrive->newValue)
      {
         case SM_WCB_ADLLFSM_On:
            //! ##### Conditions for state change from ADLLFSM_ON to ADLLFSM_OFF state 
            //! #### Check for adjust button, back button, stop button and ramp level request status
            if (((PushButtonStatus_Neutral == pAdjustBtnStatus->previousValue)
               && (PushButtonStatus_Pushed == pAdjustBtnStatus->currentvalue))
               || ((PushButtonStatus_Neutral == pBackBtnStatus->previousValue)
               && (PushButtonStatus_Pushed == pBackBtnStatus->currentvalue))
               || ((PushButtonStatus_Neutral == pStopButtonStatus->previousValue)
               && (PushButtonStatus_Pushed == pStopButtonStatus->currentvalue))
               || ((RampLevelRequest_TakeNoAction == pRampLevelRequest->previousValue)
               && ((RampLevelRequest_RampLevelM1 == pRampLevelRequest->currentvalue)
               || (RampLevelRequest_RampLevelM2 == pRampLevelRequest->currentvalue)
               || (RampLevelRequest_RampLevelM3 == pRampLevelRequest->currentvalue)
               || (RampLevelRequest_RampLevelM4 == pRampLevelRequest->currentvalue)
               || (RampLevelRequest_RampLevelM5 == pRampLevelRequest->currentvalue)
               || (RampLevelRequest_RampLevelM6 == pRampLevelRequest->currentvalue)
               || (RampLevelRequest_RampLevelM7 == pRampLevelRequest->currentvalue))))
            {
               pSMStates_AdjustDrive->newValue = SM_WCB_ADLLFSM_Off;
            }
            else
            {
               // Do nothing: keep previous value
            }
         break;
		 case SM_WCB_ADLLFSM_FALLBACK:
		    //! ##### Condition for the Adjust button recover
            if ((PushButtonStatus_Error == pAdjustBtnStatus->previousValue)
               && ((PushButtonStatus_Pushed == pAdjustBtnStatus->currentvalue)
		       || (PushButtonStatus_Neutral == pAdjustBtnStatus->currentvalue)))
            {
               pSMStates_AdjustDrive->newValue = SM_WCB_ADLLFSM_Off;
            }
            else
            {
               // Do nothing: keep previous value
            }
		 break;
         default: // SM_WCB_ADLLFSM_Off
            //! ##### Condition for the state change from ADLLFSM_Off to ADLLFSM_ON 
            if ((PushButtonStatus_Neutral == pAdjustBtnStatus->previousValue)
               && (PushButtonStatus_Pushed == pAdjustBtnStatus->currentvalue))
            {
               pSMStates_AdjustDrive->newValue = SM_WCB_ADLLFSM_On;
            }
            else
            {
               // Do nothing: keep previous value
            }
         break;
      }
   }
   //! ###### Check for adjust button status
   if (PushButtonStatus_Error == pAdjustBtnStatus->currentvalue)
   {
	   pSMStates_AdjustDrive->newValue = SM_WCB_ADLLFSM_FALLBACK;
   }
   else
   {
      // Do nothing: keep previous value
   }
   AdjustDriveOrLoadingLevel_OutputProcessing(pAdjust_DeviceIndication,
											  pWiredLevelAdjustmentStroke,
											  pFSM_WiredLevelUserMemAction,
											  pSMStates_AdjustDrive);
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the AdjustDriveOrLoadingLevel_OutputProcessing
//!
//! \param   *pAdjust_DeviceIndication       Updating device indication current value
//! \param   *pWiredLevelAdjustmentStroke    Updating to docking stroke, drive stroke or error value
//! \param   *pFSM_WiredLevelUserMemAction   Updating current UserMemAction value
//! \param   *pSMStates_AdjustDrive          Providing and updating the current state of AdjustDriveOrLoadingLevel FSM
//!
//!======================================================================================
static void AdjustDriveOrLoadingLevel_OutputProcessing(DeviceIndication_T                 *pAdjust_DeviceIndication,
                                                       LevelAdjustmentStroke_T            *pWiredLevelAdjustmentStroke,
                                                       LevelUserMemoryAction_T            *pFSM_WiredLevelUserMemAction,
													   Wcb_AdjustDriveOrLoadingLevel_FSM  *pSMStates_AdjustDrive)
{
	switch (pSMStates_AdjustDrive->newValue)
    {
        case SM_WCB_ADLLFSM_On:
		    *pAdjust_DeviceIndication       = DeviceIndication_On;
            *pWiredLevelAdjustmentStroke    = LevelAdjustmentStroke_DriveStroke;
            *pFSM_WiredLevelUserMemAction   = LevelUserMemoryAction_Inactive;
		break;
		case SM_WCB_ADLLFSM_FALLBACK:
		    *pWiredLevelAdjustmentStroke = LevelAdjustmentStroke_Error;
			*pAdjust_DeviceIndication    = DeviceIndication_Off;   //Query raised to customer
		break;
		default: // SM_WCB_ADLLFSM_Off
		    *pAdjust_DeviceIndication       = DeviceIndication_Off;
            *pWiredLevelAdjustmentStroke    = LevelAdjustmentStroke_DockingStroke;
		break;
	}
		 
}

//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Request_stop_level_change
//!
//! \param   *pStopBtnStatus                   Indicating current status of stop button
//! \param   *pWiredAirSuspensionStopRequest   Updating the AirSuspensionStopRequest for wired
//! \param   *pRequest_WiredLevelUserMemActn   Updating the LevelUserMemoryAction for wired
//! \param   *pRequest_Timers                  Checking and updating the current value of Timer
//!
//!======================================================================================
static void Request_stop_level_change(const PushButtonStatus        *pStopBtnStatus,
                                            FalseTrue_T             *pWiredAirSuspensionStopRequest,
                                            LevelUserMemoryAction_T *pRequest_WiredLevelUserMemActn,
                                            uint16                  *pRequest_Timers)
{
   static uint8 flagReleasedStopBtnBeforeTimerElapsed = 0U;

   //! ###### Check for time elapsed value of stop level timer
   if (CONST_TimerFunctionElapsed == *pRequest_Timers)
   {
      *pRequest_Timers = CONST_TimerFunctionInactive;
   }
   else
   {
      //! ##### Check for stop button status
      if ((PushButtonStatus_Pushed == pStopBtnStatus->previousValue)
         && (PushButtonStatus_Neutral == pStopBtnStatus->currentvalue))
      {
         flagReleasedStopBtnBeforeTimerElapsed = 1U;
      }
      else
      {
         // Do nothing: keep previous value
      }
   }
   //! ###### Check weather the stop level timer is inactive or not
   if (CONST_TimerFunctionInactive == *pRequest_Timers)
   {
      //! ##### Check for stop button status
      if (PushButtonStatus_Error == pStopBtnStatus->currentvalue)
      {
         *pWiredAirSuspensionStopRequest = FalseTrue_Error;
      }
      else if (PushButtonStatus_NotAvailable == pStopBtnStatus->currentvalue)
      {
         *pWiredAirSuspensionStopRequest = FalseTrue_NotAvaiable;
      }
      else if ((PushButtonStatus_Neutral == pStopBtnStatus->previousValue)
              && (PushButtonStatus_Pushed == pStopBtnStatus->currentvalue))
      {
         *pWiredAirSuspensionStopRequest = FalseTrue_True;
         *pRequest_WiredLevelUserMemActn = LevelUserMemoryAction_Inactive;
         *pRequest_Timers                = CONST_StopLevelTimeout;
      }
      else if (((PushButtonStatus_Pushed == pStopBtnStatus->previousValue)
              && (PushButtonStatus_Neutral == pStopBtnStatus->currentvalue))
              || (1U == flagReleasedStopBtnBeforeTimerElapsed))
      {
         *pWiredAirSuspensionStopRequest       = FalseTrue_False;
         flagReleasedStopBtnBeforeTimerElapsed = 0U;
      }
      else
      {
         // Do nothing: if other value of stop button status
      }
   }
   else
   {
      // Do nothing: keep previous value
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the LevelAdjustment_State_Machine
//!
//! \param   *pWiredCntrlBox_HMICtrl_in_data   Indicating height adjustment and vehicle mode present status
//! \param   *pWCB_HMICtrl_in_SMButtondata     Specifying all buttons current status
//! \param   *pSMLevel_States                  Providing and updating the State value 
//! \param   *pSMLevelStates_AdjustDrive       Providing current value for input
//! \param   *pstateInitialize                 Checking and updating the current intialization value
//! \param   *pECS_MemSwTimings                Providing the input to the timer
//! \param   *pLevel_Timers                    Checking and updating the current value of timer
//! \param   *pWiredCntrlBox_out_data          Updating the output based on current state
//!
//!======================================================================================
static void LevelAdjustment_State_Machine(const WiredControlBox_HMICtrl_in_StructType        *pWiredCntrlBox_HMICtrl_in_data,
                                          const WiredControlBox_HMICtrl_in_ButtonStructType  *pWCB_HMICtrl_in_SMButtondata,
                                                Wcb_LevelAdjustment_FSM                      *pSMLevel_States,
                                          const Wcb_AdjustDriveOrLoadingLevel_FSM            *pSMLevelStates_AdjustDrive,
                                                StateInitializationType                      *pstateInitialize,
                                          const SEWS_ECS_MemSwTimings_P1BWF_s_T              *pECS_MemSwTimings,
                                                uint16                                       *pLevel_Timers,
                                                WiredControlBox_HMICtrl_out_StructType       *pWiredCntrlBox_out_data)
{
   //! ###### Processing state transitions
   static uint8 state_Flag       = CONST_Resetflag;
   pSMLevel_States->currentvalue = pSMLevel_States->newValue;  

   if (CONST_Noninitialized == pstateInitialize->FSM_Initialization)
   {
      pWiredCntrlBox_out_data->Down_DeviceIndication      = DeviceIndication_Off;
      pWiredCntrlBox_out_data->Up_DeviceIndication        = DeviceIndication_Off;
      pWiredCntrlBox_out_data->WiredLevelAdjustmentAction = LevelAdjustmentAction_Idle;
      pSMLevel_States->newValue                           = SM_WCB_LA_Init;
      pstateInitialize->FSM_Initialization                = CONST_Initialized;
   }
   else
   {
      switch (pSMLevel_States->newValue)
      {
         //! ##### Conditions for state change from 'Idle'
         case SM_WCB_LA_Idle:
            //! #### Check height adjustment allowed status for state changes to 'Init'
            if ((FalseTrue_False == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.previousValue)
               && (FalseTrue_True == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Init;
            }
            //! #### Check WRUp button status for state changes to 'Up_Basic'
            else if (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue)
            {
               pSMLevel_States->newValue = SM_WCB_LA_Up_Basic;
            }
            //! #### Check WRUp button status for state changes to 'Evaluating_Up'
            else if ((EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.previousValue) 
                    && (EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Evaluating_Up;
            }
            //! #### Check WRDown button status for state changes to 'Down_Basic'
            else if (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue)
            {
               pSMLevel_States->newValue = SM_WCB_LA_Down_Basic;
            }
            //! #### Check WRDown button status for state changes to 'Evaluating_Down'
            else if ((EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.previousValue) 
                    && (EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Evaluating_Down;
            }
            //! #### Check WRUp button status for state changes to 'Up_Short_Movement'
            else if (EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue)
            {
               pSMLevel_States->newValue = SM_WCB_LA_Up_Short_Movement;
            }
            //! #### Check WRDown button status for state changes to 'Down_Short_Movement'
            else if (EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue)
            {
               pSMLevel_States->newValue = SM_WCB_LA_Down_Short_Movement;
            }
            //! #### Check back button status for state changes to 'GotoDrive'
            else if ((PushButtonStatus_Neutral == pWCB_HMICtrl_in_SMButtondata->BackButtonStatus.previousValue) 
                    && (PushButtonStatus_Pushed == pWCB_HMICtrl_in_SMButtondata->BackButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_GotoDrive;
            }
            else
            {
               // Do nothing: Keep the previous state
            }
         break;
         //! ##### Conditions for state change from 'Up_Auto'
         case SM_WCB_LA_Up_Auto:
            //! #### Check height adjustment allowed status for state changes to 'Init'
            if ((FalseTrue_False == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.previousValue) 
               && (FalseTrue_True == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Init;
            }
            //! #### Check WRUp button status for state changes to 'Idle'
            else if ((EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.previousValue) 
                    && ((EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue) 
                    || (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue) 
                    || (EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue)))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check for the WRDown button status
            else if (((EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.previousValue) 
                    && ((EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue) 
                    || (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue) 
                    || (EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue))))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check for the adjust button status
            else if ((PushButtonStatus_Neutral == pWCB_HMICtrl_in_SMButtondata->AdjustButtonStatus.previousValue) 
                    && (PushButtonStatus_Pushed == pWCB_HMICtrl_in_SMButtondata->AdjustButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check for the back button status
            else if ((PushButtonStatus_Neutral == pWCB_HMICtrl_in_SMButtondata->BackButtonStatus.previousValue) 
                    && (PushButtonStatus_Pushed == pWCB_HMICtrl_in_SMButtondata->BackButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check for the stop button status
            else if ((PushButtonStatus_Neutral == pWCB_HMICtrl_in_SMButtondata->StopButtonStatus.previousValue) 
                    && (PushButtonStatus_Pushed == pWCB_HMICtrl_in_SMButtondata->StopButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check for any internal changes of vehicle mode
            else if (((VehicleMode_PreRunning == pWiredCntrlBox_HMICtrl_in_data->VehicleMode.previousValue)
                    || (VehicleMode_Running == pWiredCntrlBox_HMICtrl_in_data->VehicleMode.previousValue)
                    || (VehicleMode_Cranking == pWiredCntrlBox_HMICtrl_in_data->VehicleMode.previousValue)) 
                    && ((VehicleMode_Accessory == pWiredCntrlBox_HMICtrl_in_data->VehicleMode.currentvalue)
                    || (VehicleMode_Living == pWiredCntrlBox_HMICtrl_in_data->VehicleMode.currentvalue) 
                    || (VehicleMode_Parked == pWiredCntrlBox_HMICtrl_in_data->VehicleMode.currentvalue)))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check for the level control information status
            else if (LevelControlInformation_LimpHome == pWiredCntrlBox_HMICtrl_in_data->LevelControlInformation)
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            else
            {
               // Do nothing: Keep the previous state
            }
         break;
         //! ##### Conditions for state change from 'Up_Basic'
         case SM_WCB_LA_Up_Basic:
            //! #### Check height adjustment allowed status for state changes to 'Init'
            if ((FalseTrue_False == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.previousValue) 
               && (FalseTrue_True == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Init;
            }
            //! #### Check WRUp button status for state changes to 'Idle'
            else if (((EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.previousValue) 
                    || (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.previousValue) 
                    || (EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.previousValue)) 
                    && (EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check memory button status for state changes to 'Up_Auto'
            else if (PushButtonStatus_Pushed == pWCB_HMICtrl_in_SMButtondata->MemButtonStatus.currentvalue)
            {
               pSMLevel_States->newValue = SM_WCB_LA_Up_Auto;
            }
            else
            {
               // Do nothing: keep the previous state
            }
         break;
         //! ##### Check for state change from 'Evaluating_Up'
         case SM_WCB_LA_Evaluating_Up:
            //! #### Check height adjustment allowed status for state changes to 'Init'
            if ((FalseTrue_False == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.previousValue) 
               && (FalseTrue_True == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Init; 
            }
            //! #### Check WRUp button status for state changes to 'Idle'
            else if (((EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.previousValue) 
                    || (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.previousValue) 
                    || (EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.previousValue)) 
                    && (EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check WRUp button status for state changes to 'Up_Basic'
            else if (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue)
            {
               pSMLevel_States->newValue = SM_WCB_LA_Up_Basic; 
            }      
            //! #### Check WRUp button status for state changes to 'Up_Short_Movement'
            else if (EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue)
            {
               pSMLevel_States->newValue = SM_WCB_LA_Up_Short_Movement;
            }
            else
            {
               // Do nothing: keep the previous state
            }
         break;
         //! ##### Conditions for the state change from 'Down_Auto'
         case SM_WCB_LA_Down_Auto:
            //! #### Check height adjustment allowed status for state changes to 'Init'
            if ((FalseTrue_False == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.previousValue)
               && (FalseTrue_True == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Init;
            }
            //! #### Check WRUp button status for state changes to 'Idle'
            else if (((EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.previousValue) 
                    && ((EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue) 
                    || (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue) 
                    || (EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue))))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check for the WRDown button status
            else if (((EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.previousValue) 
                    && ((EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue) 
                    || (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue) 
                    || (EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue))))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check for the adjust button Status
            else if ((PushButtonStatus_Neutral == pWCB_HMICtrl_in_SMButtondata->AdjustButtonStatus.previousValue) 
                    && (PushButtonStatus_Pushed == pWCB_HMICtrl_in_SMButtondata->AdjustButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check for the back button Status
            else if ((PushButtonStatus_Neutral == pWCB_HMICtrl_in_SMButtondata->BackButtonStatus.previousValue) 
                    && (PushButtonStatus_Pushed == pWCB_HMICtrl_in_SMButtondata->BackButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check for the stop button Status
            else if ((PushButtonStatus_Neutral == pWCB_HMICtrl_in_SMButtondata->StopButtonStatus.previousValue) 
                    && (PushButtonStatus_Pushed == pWCB_HMICtrl_in_SMButtondata->StopButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check for any internal changes in vehicle mode
            else if (((VehicleMode_PreRunning == pWiredCntrlBox_HMICtrl_in_data->VehicleMode.previousValue)
                    || (VehicleMode_Running == pWiredCntrlBox_HMICtrl_in_data->VehicleMode.previousValue)
                    || (VehicleMode_Cranking == pWiredCntrlBox_HMICtrl_in_data->VehicleMode.previousValue))
                    && ((VehicleMode_Accessory == pWiredCntrlBox_HMICtrl_in_data->VehicleMode.currentvalue)
                    || (VehicleMode_Living == pWiredCntrlBox_HMICtrl_in_data->VehicleMode.currentvalue)
                    || (VehicleMode_Parked == pWiredCntrlBox_HMICtrl_in_data->VehicleMode.currentvalue)))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check for the level control information Status
            else if (LevelControlInformation_LimpHome == pWiredCntrlBox_HMICtrl_in_data->LevelControlInformation)
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check for the memory button status
            else if (PushButtonStatus_Pushed == pWCB_HMICtrl_in_SMButtondata->MemButtonStatus.currentvalue) // MEM long press 
            {
               if (CONST_Setflag != state_Flag)
               {
                  *pLevel_Timers = (((uint16)(pECS_MemSwTimings->Level_Memorization_Min_Press) * (100U))/ CONST_TimeBase);
                  state_Flag     = CONST_Setflag;
               }
               else
               {
                  // Check for the timer is inactive or not
                  if (CONST_TimerFunctionInactive == *pLevel_Timers)
                  {
                     pSMLevel_States->newValue = SM_WCB_LA_Ferry;
                     state_Flag                = CONST_Resetflag;
                  }
                  else
                  {
                     // Do nothing:wait for Timeout
                  }
               }
            }
            else
            {
               *pLevel_Timers = CONST_TimerFunctionInactive;
               state_Flag     = CONST_Resetflag;
            }
            if (SM_WCB_LA_Down_Auto != pSMLevel_States->newValue) // cancel timer before leave this state 
            {
               *pLevel_Timers = CONST_TimerFunctionInactive;
               state_Flag     = CONST_Resetflag;
            }
            else
            {
               // Do nothing: keep previous value
            }
         break;
         //! ##### Conditions for state change from 'Down_Basic'
         case SM_WCB_LA_Down_Basic:
            //! #### Check height adjustment allowed status for state changes to 'Init'
            if ((FalseTrue_False == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.previousValue) 
               && (FalseTrue_True == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Init;
            }
            //! #### Check WRDown button status for state changes to 'Idle'
            else if (((EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.previousValue) 
                    || (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.previousValue) 
                    || (EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.previousValue)) 
                    && (EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check memory button status for state changes to 'Down_Auto'
            else if (PushButtonStatus_Pushed == pWCB_HMICtrl_in_SMButtondata->MemButtonStatus.currentvalue)
            {
               pSMLevel_States->newValue = SM_WCB_LA_Down_Auto;
            }
            else
            {
               // Do nothing: keep the previous state
            }
         break;
         //! ##### Conditions for state change from 'Evaluating_Down'
         case SM_WCB_LA_Evaluating_Down:
            //! #### Check height adjustment allowed status for state changes to 'Init'
            if ((FalseTrue_False == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.previousValue) 
               && (FalseTrue_True == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Init;
            }
            //! #### Check WRDown button status for state changes to 'Idle'
            else if (((EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.previousValue) 
                    || (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.previousValue) 
                    || (EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.previousValue)) 
                    && (EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check WRDown button status for state changes to 'Down_Basic'
            else if (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue)
            {
               pSMLevel_States->newValue = SM_WCB_LA_Down_Basic;
            }
            //! #### Check WRDown button status for state changes to 'Down_Short_Movement'
            else if (EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue)
            {
               pSMLevel_States->newValue = SM_WCB_LA_Down_Short_Movement;
            }
            else
            {
               // Do nothing: keep the previous state
            }
         break;
         //! ##### Conditions for state change from 'Up_Short_Movement'
         case SM_WCB_LA_Up_Short_Movement:
            //! #### Check height adjustment allowed status for state changes to 'Init'
            if ((FalseTrue_False == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.previousValue) 
               && (FalseTrue_True == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Init;
            }
            //! #### Check WRUp button status for state changes to 'Idle'
            else if (((EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.previousValue) 
                    || (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.previousValue) 
                    || (EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.previousValue)) 
                    && (EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            else
            {
               // Do nothing: keep the previous state
            }
         break;
         //! ##### Conditions for state change from 'Down_Short_Movement'
         case SM_WCB_LA_Down_Short_Movement:
            //! #### Check height adjustment allowed status for state changes to 'Init'
            if ((FalseTrue_False == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.previousValue) 
               && (FalseTrue_True == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Init;
            }
            //! #### Check WRDownButton status for state changes to 'Idle'
            else if (((EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.previousValue) 
                    || (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.previousValue) 
                    || (EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.previousValue)) 
                    && (EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            else
            {
               // Do nothing: keep the previous state
            }
         break;
         //! ##### Conditions for state change from 'GotoDrive'
         case SM_WCB_LA_GotoDrive:
            //! #### Check height adjustment allowed status for state changes to 'Init'
            if ((FalseTrue_False == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.previousValue)
               && (FalseTrue_True == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Init;
            }
            //! #### Check adjust button and back button status for state changes to 'Idle'
            else if (((PushButtonStatus_Pushed == pWCB_HMICtrl_in_SMButtondata->BackButtonStatus.previousValue)
                    && (PushButtonStatus_Neutral == pWCB_HMICtrl_in_SMButtondata->BackButtonStatus.currentvalue))
                    || (((PushButtonStatus_Neutral== pWCB_HMICtrl_in_SMButtondata->AdjustButtonStatus.previousValue)
                    && (PushButtonStatus_Pushed == pWCB_HMICtrl_in_SMButtondata->AdjustButtonStatus.currentvalue))
                    && (SM_WCB_ADLLFSM_On == pSMLevelStates_AdjustDrive->newValue)))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            else
            {
               // Do nothing: keep the previous state
            }
         break;
         //! ##### Conditions for state change from 'Ferry'
         case SM_WCB_LA_Ferry:
            //! #### Check height adjustment allowed status for state changes to 'Init'
            if ((FalseTrue_False == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.previousValue) 
               && (FalseTrue_True == pWiredCntrlBox_HMICtrl_in_data->HeightAdjustmentAllowed.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Init;
            }
            //! #### Check stopbutton status for state changes to 'Idle'
            else if ((PushButtonStatus_Neutral == pWCB_HMICtrl_in_SMButtondata->StopButtonStatus.previousValue) 
                    && (PushButtonStatus_Pushed == pWCB_HMICtrl_in_SMButtondata->StopButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check WRUp button status for state changes to 'Up_Basic'
            else if ((EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.previousValue)
                    && (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Up_Basic;
            }
            //! #### Check WRUp button status for state changes to 'Evaluating_Up'
            else if ((EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.previousValue) 
                    && (EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue))
            { 
               pSMLevel_States->newValue = SM_WCB_LA_Evaluating_Up;
            }
            //! #### Check WRDown button status for state changes to 'Down_Basic'
            else if ((EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.previousValue)
                    && (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Down_Basic;
            }
            //! #### Check WRDown button status for state changes to 'Evaluating_Down'
            else if ((EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.previousValue) 
                    && (EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Evaluating_Down;
            }
            //! #### Check WRUp button status for state changes to 'Up_Short_Movement'
            else if ((EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.previousValue) 
                    && (EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Up_Short_Movement;
            }
            //! #### Check WRDown button status for state changes to 'Down_Short_Movement'
            else if ((EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.previousValue) 
                    && (EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Down_Short_Movement;
            }
            //! #### Check back button status for state changes to 'GotoDrive'
            else if ((PushButtonStatus_Neutral == pWCB_HMICtrl_in_SMButtondata->BackButtonStatus.previousValue) 
                    && (PushButtonStatus_Pushed == pWCB_HMICtrl_in_SMButtondata->BackButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_GotoDrive;
            }
            else
            {
               // Do nothing: keep the previous state
            }
         break;
            //! ##### Conditions for the state change from 'Init'
         default:  // SM_WCB_LA_Init
            //! #### Check stop button status for state changes to 'Idle'
            if ((PushButtonStatus_Neutral == pWCB_HMICtrl_in_SMButtondata->StopButtonStatus.previousValue) 
               && (PushButtonStatus_Pushed == pWCB_HMICtrl_in_SMButtondata->StopButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Idle;
            }
            //! #### Check WRUp button status for state changes to 'Up_Basic'
            else if (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue)
            {
               pSMLevel_States->newValue = SM_WCB_LA_Up_Basic;
            }
            //! #### Check WRUp button status for state changes to 'Evaluating_Up'
            else if ((EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.previousValue) 
                    && (EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Evaluating_Up;
            }
            //! #### Check WRDown button status for state changes to 'Down_Basic'
            else if (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue)
            {
               pSMLevel_States->newValue = SM_WCB_LA_Down_Basic;
            }
            //! #### Check WRDown button status for state changes to 'Evaluating_Down'
            else if ((EvalButtonRequest_Neutral == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.previousValue) 
                    && (EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_Evaluating_Down;
            }
            //! #### Check WRUp button status for state changes to 'Up_Short_Movement'
            else if (EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRUpButtonStatus.currentvalue)
            {
               pSMLevel_States->newValue = SM_WCB_LA_Up_Short_Movement;
            }
            //! #### Check WRDown button status for state changes to 'Down_Short_Movement'
            else if (EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_SMButtondata->WRDownButtonStatus.currentvalue)
            {
               pSMLevel_States->newValue = SM_WCB_LA_Down_Short_Movement;
            }
            //! #### Check back button status for state changes to 'GotoDrive'
            else if ((PushButtonStatus_Neutral == pWCB_HMICtrl_in_SMButtondata->BackButtonStatus.previousValue) 
                    && (PushButtonStatus_Pushed == pWCB_HMICtrl_in_SMButtondata->BackButtonStatus.currentvalue))
            {
               pSMLevel_States->newValue = SM_WCB_LA_GotoDrive;
            }
            else
            {
               // Do nothing: keep the previous state
            }
         break;
      }  
   }
      //! ##### Process the output logic for Level adjustment_FSM : 'LevelAdjustment_FSM_Output()'
      LevelAdjustment_FSM_Output(pSMLevel_States, 
                                 pWiredCntrlBox_out_data);
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the LevelAdjustment_FSM_Output
//!
//! \param   *pLevelAdjustment_FSM   Providing the current state of LevelAdjustment_FSM
//! \param   *pRteOutData_Common     Updating the output signals based on current state
//!
//!======================================================================================
static void LevelAdjustment_FSM_Output(const Wcb_LevelAdjustment_FSM                *pLevelAdjustment_FSM,
                                             WiredControlBox_HMICtrl_out_StructType *pRteOutData_Common)
{
   //! ###### Processing output actions based on current state
   switch (pLevelAdjustment_FSM->newValue)
   {
      //! ##### Output actions for 'Idle' state 
      case SM_WCB_LA_Idle:
         pRteOutData_Common->WiredLevelAdjustmentAction = LevelAdjustmentAction_Idle;
         pRteOutData_Common->Up_DeviceIndication        = DeviceIndication_Off;
         pRteOutData_Common->Down_DeviceIndication      = DeviceIndication_Off;
      break;
      //! ##### Output actions for 'Up_auto' state 
      case SM_WCB_LA_Up_Auto:
         pRteOutData_Common->WiredLevelAdjustmentAction = LevelAdjustmentAction_UpBasic;
         pRteOutData_Common->Up_DeviceIndication        = DeviceIndication_On;
      break;
      //! ##### Output actions for 'Up_basic' state 
      case SM_WCB_LA_Up_Basic:
         pRteOutData_Common->WiredLevelAdjustmentAction = LevelAdjustmentAction_UpBasic;
         pRteOutData_Common->Up_DeviceIndication        = DeviceIndication_On;
      break;
      //! ##### Output actions for 'Evaluating_up' state 
      case SM_WCB_LA_Evaluating_Up:
         pRteOutData_Common->Up_DeviceIndication = DeviceIndication_On;
      break;
      //! ##### Output actions for 'Down_auto' state 
      case SM_WCB_LA_Down_Auto:
         pRteOutData_Common->WiredLevelAdjustmentAction = LevelAdjustmentAction_DownBasic;
         pRteOutData_Common->Down_DeviceIndication      = DeviceIndication_On;
      break;
      //! ##### Output actions for 'Down_basic' state 
      case SM_WCB_LA_Down_Basic:
         pRteOutData_Common->WiredLevelAdjustmentAction = LevelAdjustmentAction_DownBasic;
         pRteOutData_Common->Down_DeviceIndication      = DeviceIndication_On;
      break;
      //! ##### Output actions for 'Evaluating_down' state 
      case SM_WCB_LA_Evaluating_Down:
         pRteOutData_Common->Down_DeviceIndication = DeviceIndication_On;
      break;
      //! ##### Output actions for 'Up_short_movement' state 
      case SM_WCB_LA_Up_Short_Movement:
         pRteOutData_Common->WiredLevelAdjustmentAction = LevelAdjustmentAction_UpShortMovement;
         pRteOutData_Common->Up_DeviceIndication        = DeviceIndication_On;
      break;
      //! ##### Output actions for 'Down_short_movement' state 
      case SM_WCB_LA_Down_Short_Movement:
         pRteOutData_Common->WiredLevelAdjustmentAction = LevelAdjustmentAction_DownShortMovement;
         pRteOutData_Common->Down_DeviceIndication      = DeviceIndication_On;
      break;
      //! ##### Output actions for 'Gotodrive' state 
      case SM_WCB_LA_GotoDrive:
         pRteOutData_Common->WiredLevelAdjustmentAction = LevelAdjustmentAction_GotoDriveLevel;
      break;
      //! ##### Output actions for 'Ferry' state 
      case SM_WCB_LA_Ferry:
         pRteOutData_Common->WiredLevelAdjustmentAction = LevelAdjustmentAction_Ferry;
      break;
      //! ##### Output actions for 'Init' state 
      default:
         pRteOutData_Common->WiredLevelAdjustmentAction = LevelAdjustmentAction_Idle;
         pRteOutData_Common->Up_DeviceIndication        = DeviceIndication_Off;
         pRteOutData_Common->Down_DeviceIndication      = DeviceIndication_Off;
      break;
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the WiredControlBox_common_logic
//!
//! \param   *pRteInData_Common                 Providing the current input status
//! \param   *pWCB_HMICtrl_in_Buttonlogic       Providing the current button status
//! \param   *pWiredControlBox_HMICtrl_output   Upadate and provide the current output status
//! \param   *pSM_commonStates                  Providing the current input state
//! \param   *pSMCommonStates_AdjustDrive       Providing the current input status
//! \param   *pstateInitialization              Providing the intialization status
//! \param   *pECS_MemSwTimings                 Providing the current status of ECS_MemSwTimings
//! \param   *pTimers_stop                      To check current timer value and update
//! \param   *pTimers_Adj                       To check current timer value and update
//!
//!======================================================================================
static void WiredControlBox_common_logic(      WiredControlBox_HMICtrl_in_StructType        *pRteInData_Common,
                                               WiredControlBox_HMICtrl_in_ButtonStructType  *pWCB_HMICtrl_in_Buttonlogic,
                                               WiredControlBox_HMICtrl_out_StructType       *pWiredControlBox_HMICtrl_output,
                                               Wcb_LevelAdjustment_FSM                      *pSM_commonStates,
                                               Wcb_AdjustDriveOrLoadingLevel_FSM            *pSMCommonStates_AdjustDrive,
                                               StateInitializationType                      *pstateInitialization,
                                         const SEWS_ECS_MemSwTimings_P1BWF_s_T              *pECS_MemSwTimings,
                                               uint16                                       *pTimers_stop,
                                               uint16                                       *pTimers_Adj)
{
   //! ##### Process simultaneous button pushes logic : 'Simultaneous_button_pushes()'
   Simultaneous_button_pushes(&pWCB_HMICtrl_in_Buttonlogic->StopButtonStatus.currentvalue,
                              &pWCB_HMICtrl_in_Buttonlogic->AdjustButtonStatus.currentvalue,
                              &pWCB_HMICtrl_in_Buttonlogic->BackButtonStatus.currentvalue,
                              &pWCB_HMICtrl_in_Buttonlogic->MemButtonStatus.currentvalue,
                              &pRteInData_Common->SelectButtonStatus.currentvalue,
                              &pWCB_HMICtrl_in_Buttonlogic->WRDownButtonStatus.currentvalue,
                              &pWCB_HMICtrl_in_Buttonlogic->WRUpButtonStatus.currentvalue);
   //! ##### Process AdjustDrive Or LoadingLevel state transition logic : 'AdjustDriveOrLoadingLevel_FSM()'
   AdjustDriveOrLoadingLevel_FSM(&pWCB_HMICtrl_in_Buttonlogic->AdjustButtonStatus,
                                 &pWCB_HMICtrl_in_Buttonlogic->BackButtonStatus,
                                 &pRteInData_Common->RampLevelRequest,
                                 &pWCB_HMICtrl_in_Buttonlogic->StopButtonStatus,
                                 pSMCommonStates_AdjustDrive,
                                 pstateInitialization,
                                 &pWiredControlBox_HMICtrl_output->Adjust_DeviceIndication,
                                 &pWiredControlBox_HMICtrl_output->WiredLevelAdjustmentStroke,
                                 &pWiredControlBox_HMICtrl_output->WiredLevelUserMemoryAction);
   //! ##### Process shortpulse maximum length
   pWiredControlBox_HMICtrl_output->ShortPulseMaxLength = SHORTPULSEMAXLENGTH;
   //! ##### Process request stop level change logic : 'Request_stop_level_change()'
   Request_stop_level_change(&pWCB_HMICtrl_in_Buttonlogic->StopButtonStatus,
                             &pWiredControlBox_HMICtrl_output->WiredAirSuspensionStopRequest,
                             &pWiredControlBox_HMICtrl_output->WiredLevelUserMemoryAction,
                             pTimers_stop);
   //! ##### Process level adjustment state transition logic : 'LevelAdjustment_State_Machine()'
   LevelAdjustment_State_Machine(pRteInData_Common,
                                 pWCB_HMICtrl_in_Buttonlogic,
                                 pSM_commonStates,
                                 pSMCommonStates_AdjustDrive,
                                 pstateInitialization,
                                 pECS_MemSwTimings,
                                 pTimers_Adj,
                                 pWiredControlBox_HMICtrl_output);
}
//! @}
//! @}
//! @}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
