/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  RoofHatch_HMICtrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  RoofHatch_HMICtrl
 *  Generated at:  Fri Jun 12 17:10:04 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <RoofHatch_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file RoofHatch_HMICtrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_BodyAndComfort 
//! @{
//! @addtogroup Application_Living 
//! @{
//! @addtogroup RoofHatch_HMICtrl
//! @{
//!
//! \brief
//! RoofHatch_HMICtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the RoofHatch_HMICtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_RoofHatch_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
// Includes
#include "RoofHatch_HMICtrl.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * RoofHatch_HMI_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   RoofHatch_HMI_rqst_NoAction (0U)
 *   RoofHatch_HMI_rqst_Close (1U)
 *   RoofHatch_HMI_rqst_Open (2U)
 *   RoofHatch_HMI_rqst_SpareValue (3U)
 *   RoofHatch_HMI_rqst_SpareValue_01 (4U)
 *   RoofHatch_HMI_rqst_SpareValue_02 (5U)
 *   RoofHatch_HMI_rqst_ErrorIndicator (6U)
 *   RoofHatch_HMI_rqst_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *   FALSE (0U)
 *   TRUE (1U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1CW8_RoofHatchCtrl_Act_v(void)
 *   SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T Rte_Prm_P1GCD_RoofHatch_FlexibleSwitchLogic_v(void)
 *
 *********************************************************************************************************************/


#define RoofHatch_HMICtrl_START_SEC_CODE
#include "RoofHatch_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_RoofHatch_FlexibleSwitchLogic   (Rte_Prm_P1GCD_RoofHatch_FlexibleSwitchLogic_v())
#define PCODE_RoofHatchCtrl_Act               (Rte_Prm_P1CW8_RoofHatchCtrl_Act_v())

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoofHatch_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_BunkH1RoofhatchCloseBtn_Stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH1RoofhatchOpenBtn_Stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_RoofHatch_SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_RoofHatch_HMI_rqst_RoofHatch_HMI_rqst(RoofHatch_HMI_rqst_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoofHatch_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the RoofHatch_HMICtrl
//!==================================================================================================================== 

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, RoofHatch_HMICtrl_CODE) RoofHatch_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoofHatch_HMICtrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   static RoofHatch_HMICtrl_InStructType  RteInData_Common;
   static RoofHatch_HMICtrl_OutStructType RteOutData_Common = { RoofHatch_HMI_rqst_NotAvailable };

   //! ###### Read input RTE ports : 'Get_RteInDataRead_Common()'
   Get_RteInDataRead_Common(&RteInData_Common);
   //! ###### Check for SwcActivation_Living is operational
   if (Operational == RteInData_Common.SwcActivation_Living)
   {
      //! ##### Check for 'RoofHatchCtrl_Act' parameter status
      if (TRUE == PCODE_RoofHatchCtrl_Act)
      {
         //! #### Process rocker switch logic : 'InputLogicRockerSwitch()'
         InputLogicRockerSwitch(&RteInData_Common,
                                &RteOutData_Common);
         //! ##### Check for roof hatch switch status
         if ((A3PosSwitchStatus_Upper != RteInData_Common.RoofHatch_SwitchStatus)
            && (A3PosSwitchStatus_Lower != RteInData_Common.RoofHatch_SwitchStatus))
         {
            //! #### Process the LIN logic : 'InputLogicLECMLIN()'
            InputLogicLECMLIN(&RteInData_Common,
                              &RteOutData_Common);
            //! #### Process the CAN logic : 'InputLogicLECMCAN()'
            InputLogicLECMCAN(&RteInData_Common,
                              &RteOutData_Common);
         }
         else
         {
            // Do Nothing
         }
         //! #### Process fallback logic : 'RoofHatch_HMI_FallbackLogic()'
         RoofHatch_HMI_FallbackLogic(&RteInData_Common,
                                     &RteOutData_Common);
      }
      //! ##### Processing RoofHatch_HMICtrl disable logic
      else
      {
         RteOutData_Common.RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_NotAvailable;
      }
   }
   else
   {
      RteOutData_Common.RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_NotAvailable;
   }
   //! ###### Process RTE write output logic : 'RteOutDataWrite_Common()'
   RteOutDataWrite_Common(&RteOutData_Common);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoofHatch_HMICtrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoofHatch_HMICtrl_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, RoofHatch_HMICtrl_CODE) RoofHatch_HMICtrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoofHatch_HMICtrl_Init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define RoofHatch_HMICtrl_STOP_SEC_CODE
#include "RoofHatch_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

//!======================================================================================
//!
//! \brief
//!  This function implements the logic for the Get_RteInDataRead_Common
//!
//! \param   *pRteInData_Common   Examine and update the input signals based on RTE failure events
//!
//!======================================================================================
static void Get_RteInDataRead_Common(RoofHatch_HMICtrl_InStructType *pRteInData_Common)
{
   Std_ReturnType retValue = RTE_E_INVALID;

   //! ###### Read RTE ports and process RTE failure events for error indication mode
   //! ##### Read BunkH1RoofhatchCloseBtn_Stat interface
   retValue = Rte_Read_BunkH1RoofhatchCloseBtn_Stat_PushButtonStatus(&pRteInData_Common->BunkH1RoofhatchCloseBtn_Stat);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->BunkH1RoofhatchCloseBtn_Stat),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))
   //! ##### Read BunkH1RoofhatchOpenBtn_Stat interface
   retValue = Rte_Read_BunkH1RoofhatchOpenBtn_Stat_PushButtonStatus(&pRteInData_Common->BunkH1RoofhatchOpenBtn_Stat);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->BunkH1RoofhatchOpenBtn_Stat),(PushButtonStatus_NotAvailable),(PushButtonStatus_Error))
   //! ##### Read BunkH2RoofhatchCloseBtn_Stat interface
   retValue = Rte_Read_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus(&pRteInData_Common->BunkH2RoofhatchCloseBtn_Stat);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->BunkH2RoofhatchCloseBtn_Stat),(PushButtonStatus_Error))
   //! ##### Read BunkH2RoofhatchOpenBtn_Stat interface
   retValue = Rte_Read_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus(&pRteInData_Common->BunkH2RoofhatchOpenBtn_Stat);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->BunkH2RoofhatchOpenBtn_Stat),(PushButtonStatus_Error))
   //! ##### Read RoofHatch_SwitchStatus interface
   retValue = Rte_Read_RoofHatch_SwitchStatus_A3PosSwitchStatus(&pRteInData_Common->RoofHatch_SwitchStatus);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->RoofHatch_SwitchStatus),(A3PosSwitchStatus_Error))
   //! ##### Read SwcActivation_Living interface
   retValue = Rte_Read_SwcActivation_Living_Living(&pRteInData_Common->SwcActivation_Living);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->SwcActivation_Living),(NonOperational))
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the RteOutDataWrite_Common
//!
//! \param   *pRteOutData_Common   Update the output signals to ports of output structure
//!
//!======================================================================================
static void RteOutDataWrite_Common(const RoofHatch_HMICtrl_OutStructType* pRteOutData_Common)
{
   //! ###### Processing RTE write port logic
   Rte_Write_RoofHatch_HMI_rqst_RoofHatch_HMI_rqst(pRteOutData_Common->RoofHatch_HMI_rqst);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the InputLogicLECMLIN
//!
//! \param   *pIn_Data_LIN    Indicating the current button status of input data
//! \param   *pOut_Data_LIN   Updating the output data to 'RoofHatch_HMI_rqst'
//!
//!======================================================================================
static void InputLogicLECMLIN(const RoofHatch_HMICtrl_InStructType  *pIn_Data_LIN,
                                    RoofHatch_HMICtrl_OutStructType *pOut_Data_LIN)
{
   //! ###### Processing LIN logic
   //! ##### Check for BunkH2Roofhatch_Open and BunkH2Roofhatch_Close button status
   // BunkH2RoofhatchOpenBtn_Stat is equal to pushed and BunkH2RoofHatchCloseBtn_Stat is equal to neutral
   if ((PushButtonStatus_Pushed == pIn_Data_LIN ->BunkH2RoofhatchOpenBtn_Stat)
      && (PushButtonStatus_Neutral == pIn_Data_LIN ->BunkH2RoofhatchCloseBtn_Stat))
   {
      pOut_Data_LIN->RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_Open;
   }
   // BunkH2RoofhatchOpenBtn_Stat is equal to neutral and BunkH2RoofHatchCloseBtn_Stat is equal to pushed
   else if ((PushButtonStatus_Pushed == pIn_Data_LIN ->BunkH2RoofhatchCloseBtn_Stat)
           && (PushButtonStatus_Neutral == pIn_Data_LIN ->BunkH2RoofhatchOpenBtn_Stat))
   {
      pOut_Data_LIN->RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_Close;
   }
   // BunkH2RoofhatchOpenBtn_Stat and BunkH2RoofHatchCloseBtn_Stat are equal to pushed or neutral 
   else if (((PushButtonStatus_Pushed == pIn_Data_LIN ->BunkH2RoofhatchCloseBtn_Stat)
           && (PushButtonStatus_Pushed == pIn_Data_LIN ->BunkH2RoofhatchOpenBtn_Stat))
           || ((PushButtonStatus_Neutral == pIn_Data_LIN ->BunkH2RoofhatchCloseBtn_Stat)
           && (PushButtonStatus_Neutral == pIn_Data_LIN ->BunkH2RoofhatchOpenBtn_Stat)))
   {
      pOut_Data_LIN->RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_NoAction;
   }
   else
   {
      //Do nothing, keep previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the InputLogicLECMCAN
//!
//! \param   *pIn_Data_CAN    Indicating the current button status of input data
//! \param   *pOut_Data_CAN   Updating the output data to 'RoofHatch_HMI_rqst'
//!
//!======================================================================================
static void InputLogicLECMCAN(const RoofHatch_HMICtrl_InStructType  *pIn_Data_CAN,
                                    RoofHatch_HMICtrl_OutStructType *pOut_Data_CAN)
{
   //! ###### Processing CAN logic
   //! ##### Check BunkH1Roofhatch_Open and BunkH1Roofhatch_Close button status
   // BunkH1RoofhatchOpenBtn_Stat is equal to pushed and BunkH1RoofHatchCloseBtn_Stat is equal to neutral
   if ((PushButtonStatus_Pushed == pIn_Data_CAN->BunkH1RoofhatchOpenBtn_Stat)
      && (PushButtonStatus_Neutral == pIn_Data_CAN->BunkH1RoofhatchCloseBtn_Stat))
   {
      pOut_Data_CAN->RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_Open;
   }
   // BunkH1RoofhatchOpenBtn_Stat is equal to neutral and BunkH1RoofHatchCloseBtn_Stat is equal to pushed
   else if ((PushButtonStatus_Neutral == pIn_Data_CAN->BunkH1RoofhatchOpenBtn_Stat)
           && (PushButtonStatus_Pushed == pIn_Data_CAN->BunkH1RoofhatchCloseBtn_Stat))
   {
      pOut_Data_CAN->RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_Close;
   }
   // BunkH1RoofhatchOpenBtn_Stat and BunkH1RoofHatchCloseBtn_Stat are equal to pushed or neutral
   else if (((PushButtonStatus_Pushed == pIn_Data_CAN->BunkH1RoofhatchOpenBtn_Stat)
           && (PushButtonStatus_Pushed == pIn_Data_CAN->BunkH1RoofhatchCloseBtn_Stat))
           || ((PushButtonStatus_Neutral == pIn_Data_CAN->BunkH1RoofhatchOpenBtn_Stat)
           && (PushButtonStatus_Neutral == pIn_Data_CAN->BunkH1RoofhatchCloseBtn_Stat)))
   {
      pOut_Data_CAN->RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_NoAction;
   }
   else
   {
      //Do nothing, keep previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the RoofHatch_HMI_FallbackLogic
//!
//! \param   *pFallback_in_Data   Specifies the current switch status of input data
//! \param   *pOut_Data           Updates the output data to 'RoofHatch_HMI_rqst'
//!
//!======================================================================================
static void RoofHatch_HMI_FallbackLogic(const RoofHatch_HMICtrl_InStructType  *pFallback_in_Data,
                                              RoofHatch_HMICtrl_OutStructType *pOut_Data)
{
   boolean isNormalStatus_BunkH1RoofhatchCloseBtn = FALSE;
   boolean isNormalStatus_BunkH1RoofhatchOpenBtn  = FALSE;
   boolean isNormalStatus_BunkH2RoofhatchCloseBtn = FALSE;
   boolean isNormalStatus_BunkH2RoofhatchOpenBtn  = FALSE;
   boolean isNormalStatus_RoofHatchSwitchStatus   = FALSE;

   //! ###### Check for Roofhatch switch status, BunkH1 and BunkH2 Roofhatch button status
   if ((A3PosSwitchStatus_NotAvailable == pFallback_in_Data->RoofHatch_SwitchStatus)
      && (PushButtonStatus_NotAvailable == pFallback_in_Data->BunkH1RoofhatchOpenBtn_Stat)
      && (PushButtonStatus_NotAvailable == pFallback_in_Data->BunkH1RoofhatchCloseBtn_Stat)
      && (PushButtonStatus_NotAvailable == pFallback_in_Data->BunkH2RoofhatchOpenBtn_Stat)
      && (PushButtonStatus_NotAvailable == pFallback_in_Data->BunkH2RoofhatchCloseBtn_Stat))
   {
      pOut_Data->RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_NotAvailable;
   }
   else 
   { 
      // ErrorIndicator
      isNormalStatus_BunkH1RoofhatchCloseBtn = isNormalStatusBunkH1RoofhatchCloseBtn(&pFallback_in_Data->BunkH1RoofhatchCloseBtn_Stat);
      isNormalStatus_BunkH1RoofhatchOpenBtn  = isNormalStatusBunkH1RoofhatchOpenBtn(&pFallback_in_Data->BunkH1RoofhatchOpenBtn_Stat);
      isNormalStatus_BunkH2RoofhatchCloseBtn = isNormalStatusBunkH2RoofhatchCloseBtn(&pFallback_in_Data->BunkH2RoofhatchCloseBtn_Stat);
      isNormalStatus_BunkH2RoofhatchOpenBtn  = isNormalStatusBunkH2RoofhatchOpenBtn(&pFallback_in_Data->BunkH2RoofhatchOpenBtn_Stat);
      isNormalStatus_RoofHatchSwitchStatus   = isNormalStatusRoofHatchSwitchStatus(&pFallback_in_Data->RoofHatch_SwitchStatus);
      //! ##### Check for BunkH1RoofhatchCloseButton status 
      if (PushButtonStatus_Error == pFallback_in_Data->BunkH1RoofhatchCloseBtn_Stat)
      {
         if ((FALSE == isNormalStatus_BunkH1RoofhatchOpenBtn)
            && (FALSE == isNormalStatus_BunkH2RoofhatchCloseBtn)
            && (FALSE == isNormalStatus_BunkH2RoofhatchOpenBtn)
            && (FALSE == isNormalStatus_RoofHatchSwitchStatus))
         {
            pOut_Data->RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_ErrorIndicator;
         }
         else
         {
            // Do nothing, keep previous status
         }
      }
      //! ##### Check for BunkH1RoofhatchOpenButton status
      else if (PushButtonStatus_Error == pFallback_in_Data->BunkH1RoofhatchOpenBtn_Stat)
      {
         if ((FALSE == isNormalStatus_BunkH1RoofhatchCloseBtn)
            && (FALSE == isNormalStatus_BunkH2RoofhatchCloseBtn)
            && (FALSE == isNormalStatus_BunkH2RoofhatchOpenBtn)
            && (FALSE == isNormalStatus_RoofHatchSwitchStatus))
         {
            pOut_Data->RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_ErrorIndicator;
         }
         else
         {
            // Do nothing, keep previous status
         }
      }
      //! ##### Check for BunkH2RoofhatchCloseButton status
      else if (PushButtonStatus_Error == pFallback_in_Data->BunkH2RoofhatchCloseBtn_Stat)
      {
         if ((FALSE == isNormalStatus_BunkH1RoofhatchCloseBtn)
            && (FALSE == isNormalStatus_BunkH1RoofhatchOpenBtn)
            && (FALSE == isNormalStatus_BunkH2RoofhatchOpenBtn)
            && (FALSE == isNormalStatus_RoofHatchSwitchStatus))
         {
            pOut_Data->RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_ErrorIndicator;
         }
         else
         {
            // Do nothing, keep previous status
         }
      }
      //! ##### Check for BunkH2RoofhatchOpenButton status
      else if (PushButtonStatus_Error == pFallback_in_Data->BunkH2RoofhatchOpenBtn_Stat)
      {
         if ((FALSE == isNormalStatus_BunkH1RoofhatchCloseBtn)
            && (FALSE == isNormalStatus_BunkH1RoofhatchOpenBtn)
            && (FALSE == isNormalStatus_BunkH2RoofhatchCloseBtn)
            && (FALSE == isNormalStatus_RoofHatchSwitchStatus))
         {
            pOut_Data->RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_ErrorIndicator;
         }
         else
         {
            // Do nothing, keep previous status
         }
      }
      //! ##### Check for RoofHatch_SwitchStatus status
      else if (A3PosSwitchStatus_Error == pFallback_in_Data->RoofHatch_SwitchStatus)
      {
         if ((FALSE == isNormalStatus_BunkH1RoofhatchCloseBtn)
            && (FALSE == isNormalStatus_BunkH1RoofhatchOpenBtn)
            && (FALSE == isNormalStatus_BunkH2RoofhatchCloseBtn)
            && (FALSE == isNormalStatus_BunkH2RoofhatchOpenBtn))
         {
            pOut_Data->RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_ErrorIndicator;
         }
         else
         {
            // Do nothing, keep previous status
         }
      }
      else
      {
         // Do nothing, keep previous status
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the InputLogicRockerSwitch
//!
//! \param   *pRockerSwitch_in_Data   Specifies the current switch status of input data
//! \param   *pRteOutData_Common      Updates the output data to 'RoofHatch_HMI_rqst' 
//!
//!======================================================================================
static void InputLogicRockerSwitch(const RoofHatch_HMICtrl_InStructType  *pRockerSwitch_in_Data,
                                         RoofHatch_HMICtrl_OutStructType *pRteOutData_Common)
{
   //! ##### Check for Roofhatch switch status and Roofhatch FlexibleSwitch parameter status
   // RoofHatch_SwitchStatus = Lower and RoofHatch_FlexibleSwitch = False  or 
   // RoofHatch_SwitchStatus = Upper and RoofHatch_FlexibleSwitch = True
   if (((A3PosSwitchStatus_Lower == pRockerSwitch_in_Data->RoofHatch_SwitchStatus)
      && (FALSE == PCODE_RoofHatch_FlexibleSwitchLogic))
      || ((A3PosSwitchStatus_Upper == pRockerSwitch_in_Data->RoofHatch_SwitchStatus)
      && (TRUE == PCODE_RoofHatch_FlexibleSwitchLogic)))
   {
      pRteOutData_Common->RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_Open;
   }
   // RoofHatch_SwitchStatus = Upper and RoofHatch_FlexibleSwitch = False  or 
   // RoofHatch_SwitchStatus = Lower and RoofHatch_FlexibleSwitch = True
   else if (((A3PosSwitchStatus_Upper == pRockerSwitch_in_Data->RoofHatch_SwitchStatus)
           && (FALSE == PCODE_RoofHatch_FlexibleSwitchLogic))
           || ((A3PosSwitchStatus_Lower == pRockerSwitch_in_Data->RoofHatch_SwitchStatus)
           && (TRUE == PCODE_RoofHatch_FlexibleSwitchLogic)))
   {
      pRteOutData_Common->RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_Close;
   }
   //! ##### Check for Roofhatch switch status, BunkH1 and BunkH2 Roofhatch button status
   // RoofHatch_SwitchStatus is middle
   else if ((A3PosSwitchStatus_Middle == pRockerSwitch_in_Data->RoofHatch_SwitchStatus)
           && (PushButtonStatus_Pushed != pRockerSwitch_in_Data->BunkH1RoofhatchOpenBtn_Stat)
           && (PushButtonStatus_Pushed != pRockerSwitch_in_Data->BunkH1RoofhatchCloseBtn_Stat)
           && (PushButtonStatus_Pushed != pRockerSwitch_in_Data->BunkH2RoofhatchOpenBtn_Stat)
           && (PushButtonStatus_Pushed != pRockerSwitch_in_Data->BunkH2RoofhatchCloseBtn_Stat))
   {
      pRteOutData_Common->RoofHatch_HMI_rqst = RoofHatch_HMI_rqst_NoAction;
   }
   //! ##### Check for Roofhatch switch status
   // RoofHatch_SwitchStatus is spare values
   else if ((A3PosSwitchStatus_Spare == pRockerSwitch_in_Data->RoofHatch_SwitchStatus)
           || (A3PosSwitchStatus_Spare_01 == pRockerSwitch_in_Data->RoofHatch_SwitchStatus)
           || (A3PosSwitchStatus_Spare_02 == pRockerSwitch_in_Data->RoofHatch_SwitchStatus))
   {
      // Maintain previous value
   }
   else
   {
      // Do nothing, keep previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the isNormalStatusBunkH1RoofhatchCloseBtn
//!
//! \param   *pBunkH1RoofhatchCloseBtn_Stat   Provides the current  button status
//!
//! \return   boolean                         Returns 'TRUE' or 'FALSE' value
//!
//!======================================================================================
static boolean isNormalStatusBunkH1RoofhatchCloseBtn(const PushButtonStatus_T *pBunkH1RoofhatchCloseBtn_Stat)
{
   //! ###### Check for 'BunkH1RoofhatchCloseButton' Status 
   boolean isNormal = FALSE;

   if ((PushButtonStatus_Neutral == *pBunkH1RoofhatchCloseBtn_Stat)
      || (PushButtonStatus_Pushed == *pBunkH1RoofhatchCloseBtn_Stat))
   {
      isNormal = TRUE;
   }
   else
   {
      isNormal = FALSE;
   }
   return isNormal;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the isNormalStatusBunkH1RoofhatchOpenBtn
//!
//! \param   *pBunkH1RoofhatchOpenBtn_Stat   Provides the current button status
//!
//! \return   boolean                        Returns 'TRUE' or 'FALSE'
//!
//!======================================================================================
static boolean isNormalStatusBunkH1RoofhatchOpenBtn(const PushButtonStatus_T *pBunkH1RoofhatchOpenBtn_Stat)
{
   //! ###### Check for 'BunkH1RoofhatchOpenButton' Status
   boolean isNormal = FALSE;

   if ((PushButtonStatus_Neutral == *pBunkH1RoofhatchOpenBtn_Stat)
      || (PushButtonStatus_Pushed == *pBunkH1RoofhatchOpenBtn_Stat))
   {
      isNormal = TRUE;
   }
   else
   {
      isNormal = FALSE;
   }
   return isNormal;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the isNormalStatusBunkH2RoofhatchCloseBtn
//!
//! \param   *pBunkH2RoofhatchCloseBtn_Stat   Provides the current button status
//!
//! \return   boolean                         Returns 'TRUE' or 'FALSE'
//!
//!======================================================================================
static boolean isNormalStatusBunkH2RoofhatchCloseBtn(const PushButtonStatus_T *pBunkH2RoofhatchCloseBtn_Stat)
{
   //! ###### Check for 'BunkH2RoofhatchCloseButton' Status
   boolean isNormal = FALSE;

   if ((PushButtonStatus_Neutral == *pBunkH2RoofhatchCloseBtn_Stat)
      || (PushButtonStatus_Pushed == *pBunkH2RoofhatchCloseBtn_Stat))
   {
      isNormal = TRUE;
   }
   else
   {
      isNormal = FALSE;
   }
   return isNormal;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the isNormalStatusBunkH2RoofhatchOpenBtn
//!
//! \param   *pBunkH2RoofhatchOpenBtn_Stat   Provides the current button status.
//!
//! \return   boolean                        Return 'TRUE' or 'FALSE'
//!
//!======================================================================================
static boolean isNormalStatusBunkH2RoofhatchOpenBtn(const PushButtonStatus_T *pBunkH2RoofhatchOpenBtn_Stat)
{
   //! ###### Check for 'BunkH2RoofhatchOpenButton' Status
   boolean isNormal = FALSE;

   if ((PushButtonStatus_Neutral == *pBunkH2RoofhatchOpenBtn_Stat)
      || (PushButtonStatus_Pushed == *pBunkH2RoofhatchOpenBtn_Stat))
   {
      isNormal = TRUE;
   }
   else
   {
      isNormal = FALSE;
   }
   return isNormal;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the isNormalStatusRoofHatchSwitchStatus
//!
//! \param   *pRoofHatch_SwitchStatus   Provides the current switch status
//!
//! \return   boolean                   Return 'TRUE' or 'FALSE'
//!
//!======================================================================================
static boolean isNormalStatusRoofHatchSwitchStatus(const A3PosSwitchStatus_T  *pRoofHatch_SwitchStatus)
{
   //! ###### Check for RoofHatch_Switch status
   boolean isNormal = FALSE;

   if ((A3PosSwitchStatus_Middle == *pRoofHatch_SwitchStatus)
      || (A3PosSwitchStatus_Lower == *pRoofHatch_SwitchStatus)
      || (A3PosSwitchStatus_Upper == *pRoofHatch_SwitchStatus))
   {
      isNormal = TRUE;
   }
   else
   {
      isNormal = FALSE;
   }
   return isNormal;
}

//! @}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
