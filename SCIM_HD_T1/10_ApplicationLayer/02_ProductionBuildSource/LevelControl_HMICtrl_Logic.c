/**********************************************************************************************************************
* DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
*********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
//!======================================================================================
//! \file LevelControl_HMICtrl_Logic.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_VehicleDynamics_Suspension 
//! @{
//! @addtogroup LevelControl_HMICtrl
//! @{
//!
//! \brief
//! LevelControl_HMICtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the LevelControl_HMICtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
* DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
*********************************************************************************************************************/
#include "Rte_LevelControl_HMICtrl.h"
#include "LevelControl_HMICtrl_If.h"
#include "LevelControl_HMICtrl_Logic_If.h"
#include "LevelControl_HMICtrl_Logic.h"
#include "FuncLibrary_Timer_If.h"

#define PCODE_LoadingLvlAdjSwFulltimer       (((uint16)(Rte_Prm_P1IZ2_LoadingLevelAdjSwStuckTimeout_v()) * (10U) * CONST_ConversionFactor)/ CONST_RunnableTimeBase)
#define PCODE_FPBRSwitchRequestACKTime       (((uint16)(Rte_Prm_P1LXR_FPBRSwitchRequestACKTime_v()) * (20U))/ CONST_RunnableTimeBase)
#define PCODE_FPBRSwitchStuckedTimeout       (((uint16)(Rte_Prm_P1LXQ_FPBRSwitchStuckedTimeout_v()) * (10U) * CONST_ConversionFactor)/ CONST_RunnableTimeBase)
#define PCODE_FerryFuncSwStuckedTimeout      (((uint16)(Rte_Prm_P1EXK_FerryFuncSwStuckedTimeout_v()) * (10U) * CONST_ConversionFactor)/ CONST_RunnableTimeBase)
#define PCODE_KneelButtonStuckedTimeout      (((uint16)(Rte_Prm_P1DWD_KneelButtonStuckedTimeout_v()) * (10U) * CONST_ConversionFactor)/ CONST_RunnableTimeBase)
#define PCODE_LoadingLevelSwStuckedTimeout   (((uint16)(Rte_Prm_P1CUF_LoadingLevelSwStuckedTimeout_v()) * (10U) * CONST_ConversionFactor)/ CONST_RunnableTimeBase)
#define PCODE_ECSActiveStateTimeout          (((uint32)(Rte_Prm_P1CUE_ECSActiveStateTimeout_v()) * CONST_ConversionFactor)/ CONST_RunnableTimeBase) 
#define PCODE_ECS_StandbyBlinkTime           (((uint16)(Rte_Prm_P1GCL_ECS_StandbyBlinkTime_v()) * (20U))/ CONST_RunnableTimeBase)
#define PCODE_ECSStandbyExtendedActTimeout   (((uint32)(Rte_Prm_P1CUB_ECSStandbyExtendedActTimeout_v()) * CONST_ConversionFactor)/ CONST_RunnableTimeBase)
#define PCODE_ECSStandbyActivationTimeout    (((uint32)(Rte_Prm_P1CUA_ECSStandbyActivationTimeout_v()) * CONST_ConversionFactor)/ CONST_RunnableTimeBase)
//!======================================================================================
//!
//! \brief
//! This function is processing the ECS_StandByLogic
//!
//! \param   *pVehicleMode                Provides the vehicle mode status
//! \param   *pSM_ECS_StandbyFSM          Provides the ECS standby state
//! \param   *pECSStandByReqRCECS_FSM     Provides the ECSStandByReqRCECS status
//! \param   *pECSStandByReqWRC           Specifies the current value
//! \param   *pECSStandbyAllowed          Provides the ECS standby allowed status
//! \param   *pECS_StandbyFSM_Type        Provides the intialization status
//! \param   *ptimers_ECSActive           To check current timer value and update
//! \param   *ptimers_ECSBlinkTime        To check current timer value and update
//! \param   *pECSStandByRequest          Update ECSStandByRequest based on input conditions
//! \param   *pBlinkECSWiredLEDs          Indicate the LEDs based on input conditions
//!
//!======================================================================================
void ECS_StandByLogic(const VehicleMode_T        *pVehicleMode,
						    ECS_StandbyFSM       *pSM_ECS_StandbyFSM,
					  const ECSStandByReq        *pECSStandByReqRCECS_FSM,
					  const ECSStandByReq        *pECSStandByReqWRC,
					  const FalseTrue_T          *pECSStandbyAllowed,
						    uint8                *pECS_StandbyFSM_Type,
						    uint32               *ptimers_ECSActive,
						    uint16               *ptimers_ECSBlinkTime,
						    ECSStandByRequest_T  *pECSStandByRequest,
                            FalseTrue_T          *pBlinkECSWiredLEDs)
{
	FormalBoolean actTrigger   = NO;
    FormalBoolean deactTrigger = NO;
	//! ##### Process ECS standby FSM transition logic : 'ECS_StandbyFSM_Transitions()'
      ECS_StandbyFSM_Transitions(pVehicleMode,
                                 pSM_ECS_StandbyFSM,
                                 pECSStandByReqRCECS_FSM,
                                 pECSStandByReqWRC,
                                 pECSStandbyAllowed, 
                                 pECS_StandbyFSM_Type,
                                 ptimers_ECSActive,
                                 ptimers_ECSBlinkTime,
								 pBlinkECSWiredLEDs);
      //! ##### Process ECS standby FSM output actions : 'ECS_StandbyFSM_Outprocessing()'
      ECS_StandbyFSM_Outprocessing(pSM_ECS_StandbyFSM,
                                   pECSStandByRequest,
                                   pBlinkECSWiredLEDs,
                                   pECSStandByReqRCECS_FSM);
      //! ##### Process ECS activation trigger logic : 'ECS_ActivationTrigger()'
      actTrigger = ECS_ActivationTrigger(pSM_ECS_StandbyFSM);
      //! ##### Process ECS deactivation trigger logic : 'ECS_DeactivationTrigger()'
      deactTrigger = ECS_DeactivationTrigger(pSM_ECS_StandbyFSM);
      //! ##### Activating application networks by ECS standby active condition : 'ANW_ECSStandByActive()'
      ANW_ECSStandByActive(&actTrigger,
                           &deactTrigger);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the Ferryswitch_StateTransitions
//!
//! \param   *pFerryFunctionSwitchStatus      Provides the input switch status
//! \param   *pFerryFunctionSwitchChangeACK   Specifies the acknowledgement status of FerryFunctionSwitch
//! \param   *pFerrySwitch_SM                 Provide and update the current state based on conditions
//! \param   *pFerrySwitch_type               Specify and update the current value
//! \param   *pFerryTimers                    To check current timer value and update
//! \param   *pFerryTimers_ChangeAck          To check current timer value and update
//! \param   *pFerryFunctionSwitchChangeReq   Update output based on input conditions
//! \param   *pFerryRampLevelRequest          Update output based on input conditions
//!
//!======================================================================================
void Ferryswitch_StateTransitions(const A2PosSwitchStatus_T  *pFerryFunctionSwitchStatus,
                                  const Ack2Bit_T            *pFerryFunctionSwitchChangeACK,
                                        SM_Ferryswitch       *pFerrySwitch_SM,
                                        uint8                *pFerrySwitch_type,
                                        uint16               *pFerryTimers,
                                        uint16               *pFerryTimers_ChangeAck,
										ChangeRequest2Bit_T  *pFerryFunctionSwitchChangeReq,
                                        RampLevelRequest_T   *pFerryRampLevelRequest)
{
   pFerrySwitch_SM->PreviousValue = pFerrySwitch_SM->CurrentValue;
   if (CONST_NotInitialize == *pFerrySwitch_type)
   {
      *pFerrySwitch_type            = CONST_Initialize;
      pFerrySwitch_SM->CurrentValue = SM_FerrySwitch_Default;
      *pFerryTimers                 = CONST_TimerFunctionInactive;
      *pFerryTimers_ChangeAck       = CONST_TimerFunctionInactive;
   }
   else
   {
      //! ###### Select the state based on 'pFerrySwitch_SM->CurrentValue'
      switch (pFerrySwitch_SM->CurrentValue)
      {
         //! ##### Check the conditions for state change from 'StandBy' to 'SwitchOff'
         case SM_FerrySwitch_StandBy:
            //! #### Check for FerryFunction switch status(Not pressed event)
            if (A2PosSwitchStatus_Off == *pFerryFunctionSwitchStatus)
            {
               pFerrySwitch_SM->CurrentValue = SM_FerrySwitch_SwitchOff;
               *pFerryTimers                 = CONST_TimerFunctionInactive;
            }
            //! #### Check for time elapsed value for timers to change to 'Stuck' state
            //Button Stuck Event
            else if (*pFerryTimers == CONST_TimerFunctionElapsed)
            {
               pFerrySwitch_SM->CurrentValue = SM_FerrySwitch_Stuck;
               (void)Rte_Call_Event_D1CXA_63_FerryFlexibleSwitchStuck_SetEventStatus(DEM_EVENT_STATUS_FAILED);
            }
            else
            {
               // Do nothing: keep the previous state value
            }
         break;
         //! ##### Check the conditions for state change from 'SwitchOff' to 'ChangeFerryFunction'
         case  SM_FerrySwitch_SwitchOff:
            //! #### Check for FerryFunction switch status
            //! #### Check for FerryFunctionSwitchChangeACK status
            // Button pressed event
            if ((A2PosSwitchStatus_On == *pFerryFunctionSwitchStatus)
               && (Ack2Bit_NoAction == *pFerryFunctionSwitchChangeACK))
            {
               pFerrySwitch_SM->CurrentValue = SM_FerrySwitch_ChangeFerryFunction;
               *pFerryTimers                 = PCODE_FerryFuncSwStuckedTimeout;
               *pFerryTimers_ChangeAck       = CONST_FerryFunc_NoAckTimeout;
            }
            else
            {
               // Do nothing : keep previous value
            }
         break;
         //! ##### Check the conditions for state change from 'ChangeFerryFunction' to 'StandBy'
         case SM_FerrySwitch_ChangeFerryFunction:
            //! #### Check for FerryFunctionSwitchChangeACK status (Acknowledge event)
            if (Ack2Bit_ChangeAcknowledged == *pFerryFunctionSwitchChangeACK)
            {
               *pFerryTimers_ChangeAck       = CONST_TimerFunctionInactive;
               pFerrySwitch_SM->CurrentValue = SM_FerrySwitch_StandBy;
            }
            //! #### Check time elapsed value for ChangeAck timer(No Acknowledge event)
            else if (*pFerryTimers_ChangeAck == CONST_TimerFunctionElapsed)
            {
               pFerrySwitch_SM->CurrentValue = SM_FerrySwitch_StandBy;
            }
            else
            {
               // Do nothing: keep the previous state value
            }
         break; 
         //! ##### Check the conditions for state change from 'Stuck' to 'SwitchOff'
         case SM_FerrySwitch_Stuck:
            //! #### Check for FerryFunction switch status(Not pressed event)
            if (A2PosSwitchStatus_Off == *pFerryFunctionSwitchStatus)
            {
               pFerrySwitch_SM->CurrentValue = SM_FerrySwitch_SwitchOff;
               (void)Rte_Call_Event_D1CXA_63_FerryFlexibleSwitchStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            }
            else
            {
               // Do nothing : keep previous value
            }
         break;
         //! ##### Check the conditions for state change from 'Default' to 'StandBy' State
         default:
            //! #### Check for FerryFunctionSwitchStatus and FerryFunctionSwitchChangeACK (No Action event)
            if ((A2PosSwitchStatus_Off == *pFerryFunctionSwitchStatus)
               && (Ack2Bit_NotAvailable != *pFerryFunctionSwitchChangeACK))
            {
               pFerrySwitch_SM->CurrentValue = SM_FerrySwitch_StandBy;
            }
            else
            {
               // Do nothing : keep previous value
            }
         break;
      }
   }
   	  //! #### Process ferry switch state machine output actions : 'Ferryfun_OutputProcessing()'
        Ferryfun_OutputProcessing(pFerrySwitch_SM,
                                  pFerryFunctionSwitchChangeReq,
                                  pFerryRampLevelRequest);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the Ferryfun_OutputProcessing logic
//!
//! \param   *pOutFerryswitch_SM               Provide the current state of ferry function FSM 
//! \param   *pFerryFunctionSwitchChangeReq    Update the output signals based on current state
//! \param   *pFerryRampLevelRequest           Update the output signals based on current state
//!
//!======================================================================================
static void Ferryfun_OutputProcessing(const SM_Ferryswitch       *pOutFerryswitch_SM,
                                            ChangeRequest2Bit_T  *pFerryFunctionSwitchChangeReq,
                                            RampLevelRequest_T   *pFerryRampLevelRequest)
{
   if (pOutFerryswitch_SM->PreviousValue != pOutFerryswitch_SM->CurrentValue)
   {
      //! ##### Select the state based on 'Ferryswitch_SM->CurrentValue'
      switch (pOutFerryswitch_SM->CurrentValue)
      {
         //! #### Output actions for 'StandBy' state
         case SM_FerrySwitch_StandBy:
            *pFerryFunctionSwitchChangeReq = ChangeRequest2Bit_TakeNoAction;
         break;
         //! #### No output actions for 'SwitchOff' state
         case SM_FerrySwitch_SwitchOff:
         //No action
         break;
         //! #### Output actions for 'ChangeFerryFunction' state
         case SM_FerrySwitch_ChangeFerryFunction:
            *pFerryRampLevelRequest        = RampLevelRequest_TakeNoAction;
            *pFerryFunctionSwitchChangeReq = ChangeRequest2Bit_Change;
         break;
         //! #### Output actions for 'Stuck' state
         case SM_FerrySwitch_Stuck:
            *pFerryFunctionSwitchChangeReq = ChangeRequest2Bit_Error;
         // Testfailed condition missing 
         break;
         //! #### Output actions for 'Default' state
         default: //case SM_FerrySwitch_Default:
          *pFerryFunctionSwitchChangeReq = ChangeRequest2Bit_TakeNoAction;
         break;
      }
   }
   else
   {
      // Do nothing: keep previous status value
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the Ferryfun_Indication logic
//!
//! \param   *pFerryFunctionStatus              Provides the input status of FerryFunction
//! \param   *pFerryFunction_DeviceIndication   Update the device indication to 'On' or 'Off'
//!
//!======================================================================================
void Ferryfun_Indication(const FerryFunctionStatus_T *pFerryFunctionStatus,
                               DeviceIndication_T    *pFerryFunction_DeviceIndication)
{
   //! ##### Check for FerryFunction status
   if ((FerryFunctionStatus_FerryFunctionActiveLevelReached == *pFerryFunctionStatus)
      || (FerryFunctionStatus_FerryFunctionActiveLeveNotlReached == *pFerryFunctionStatus))
   {
      // contradictory statement 
      *pFerryFunction_DeviceIndication = DeviceIndication_On;
   }
   else 
   {
      *pFerryFunction_DeviceIndication = DeviceIndication_Off;
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the KneelingSwitch_StateTransitions logic
//!
//! \param   *pKneelingSwitchStatus       Provides the input switch status
//! \param   *pChangeKneelACK             Specifies the acknowledgement status of ChangeKneel
//! \param   *pSM_KneelingSwitch          Provides and updates the present state based on conditions
//! \param   *pKneelingSwitch_Type        Specify and update the current value
//! \param   *pKneelingSwitchTimers       To check current timer value and update
//! \param   *pKneelingTimers_changeAck   To check current timer value and update
//! \param   *pKneelingChangeRequest      Update the KneelingChangeRequest status
//!
//!======================================================================================
void KneelingSwitch_StateTransitions(const A2PosSwitchStatus        *pKneelingSwitchStatus,
                                     const ChangeKneelACK_T         *pChangeKneelACK,
                                           SM_KneelingSwitch        *pSM_KneelingSwitch,
                                           uint8                    *pKneelingSwitch_Type,
                                           uint16                   *pKneelingSwitchTimers,
                                           uint16                   *pKneelingTimers_changeAck,
										   KneelingChangeRequest_T  *pKneelingChangeRequest)
{
   pSM_KneelingSwitch->PreviousValue = pSM_KneelingSwitch->CurrentValue;
   if (CONST_NotInitialize == *pKneelingSwitch_Type)
   {
      *pKneelingSwitch_Type            = CONST_Initialize;
      pSM_KneelingSwitch->CurrentValue = SM_KneelingSwitch_Default;
      *pKneelingSwitchTimers           = CONST_TimerFunctionInactive;
      *pKneelingTimers_changeAck       = CONST_TimerFunctionInactive;
   }
   else
   {
      // Do nothing
   }
   //! ###### Select the State based on SM_KneelingSwitch->CurrentValue 
   switch (pSM_KneelingSwitch->CurrentValue)
   {
      //! ##### Check the conditions for state change from 'StandBy'
      case SM_KneelingSwitch_StandBy:
         //! #### Check for KneelingSwitchStatus, for the state change to 'SwitchOff'(button released event)
         if (A2PosSwitchStatus_Off == pKneelingSwitchStatus->CurrentValue)
         {
            *pKneelingSwitchTimers           = CONST_TimerFunctionInactive;
            pSM_KneelingSwitch->CurrentValue = SM_KneelingSwitch_SwitchOff;
            (void)Rte_Call_Event_D1CXB_63_KneelingFlexibleSwitchStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
         }
         //! #### Check for time elapsed value for timers
         else if (CONST_TimerFunctionElapsed == *pKneelingSwitchTimers)
         {
            pSM_KneelingSwitch->CurrentValue = SM_KneelingSwitch_Error;
            (void)Rte_Call_Event_D1CXB_63_KneelingFlexibleSwitchStuck_SetEventStatus(DEM_EVENT_STATUS_FAILED);
         }
         else
         {
            // Do nothing: keep the previous state value
         }
      break;
      //! ##### Check the conditions for state change from 'SwitchOff'
      case SM_KneelingSwitch_SwitchOff:
         //! #### Check for KneelingSwitchStatus and ChangeKneelACK statusfor the state change to 'ChangeKneelFunction'(Button press event)
         if ((A2PosSwitchStatus_On == pKneelingSwitchStatus->CurrentValue))
         {
            if (ChangeKneelACK_NoAction == *pChangeKneelACK)
            {
               *pKneelingTimers_changeAck       = CONST_kneelFunc_NoAckTimeout;
               pSM_KneelingSwitch->CurrentValue = SM_KneelingSwitch_ChangeKneelFunction;
            }
            else
            {
               // Do nothing : keep previous value
            }
            //! #### Check for KneelingSwitchTimer for the state change to 'Error'
            if (*pKneelingSwitchTimers > PCODE_KneelButtonStuckedTimeout)
            {
               *pKneelingSwitchTimers = PCODE_KneelButtonStuckedTimeout;
            }
            else
            {
               // Do nothing: keep the previous state value
            }
         }
         //! #### Check for KneelingSwitchStatus, for state change to 'Error'
         else if (A2PosSwitchStatus_Error == pKneelingSwitchStatus->CurrentValue)
         {
            pSM_KneelingSwitch->CurrentValue = SM_KneelingSwitch_Error;
            *pKneelingSwitchTimers           = CONST_TimerFunctionInactive;
         }
         else
         {
            // Do nothing: keep the previous state value
         }
      break;
      //! ##### Check the conditions for state change from 'ChangeKneelFunction' state
      case SM_KneelingSwitch_ChangeKneelFunction:
         //! #### Check for the state change to 'StandBy'
         //! #### Check for ChangeKneelACK status(Acknowledge event)
         if (ChangeKneelACK_ChangeAcknowledged == *pChangeKneelACK)
         {
            pSM_KneelingSwitch->CurrentValue = SM_KneelingSwitch_StandBy;
            *pKneelingTimers_changeAck       = CONST_TimerFunctionInactive;
         }
         //! #### Check for time elapsed value for ChangeAck timer
         //ChangeKneelACK is not equal to 'ChangeAcknowledged' within 5 seconds monitoring timeout.
         else if (CONST_TimerFunctionElapsed == *pKneelingTimers_changeAck)
         {
           //if this status does not change within 5 seconds
            pSM_KneelingSwitch->CurrentValue = SM_KneelingSwitch_StandBy;
         }
         //! #### Check for time elapsed value for KneelingSwitchTimers
         else
         {
            // Do nothing: keep the previous state value
         }
      break;
      //! ##### Check the conditions for state change from 'Error'
      case SM_KneelingSwitch_Error:
         //! #### Check for the state change to 'StandBy'
         //! #### Check for KneelingSwitchStatus and ChangeKneelACK status
         // No Action Event
         if (A2PosSwitchStatus_Off == pKneelingSwitchStatus->CurrentValue)
       {
            (void)Rte_Call_Event_D1CXB_63_KneelingFlexibleSwitchStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            if (ChangeKneelACK_NotAvailable != *pChangeKneelACK)
            {
               *pKneelingSwitchTimers           = CONST_TimerFunctionInactive;
               pSM_KneelingSwitch->CurrentValue = SM_KneelingSwitch_StandBy;
            }
         //! #### Check for the state change to 'SwitchOff'
          //button released Event
         else
         {
            *pKneelingSwitchTimers           = CONST_TimerFunctionInactive;
            pSM_KneelingSwitch->CurrentValue = SM_KneelingSwitch_SwitchOff;
         }
       }
         //! #### Check for the state change to 'ChangeKneelFunction'
         //! #### Check for KneelingSwitchStatus is equal to On and ChangeKneelACK is equal to TakeNoAction
         //button pressed event
         else if (((A2PosSwitchStatus_On == pKneelingSwitchStatus->CurrentValue)
			     && (pKneelingSwitchStatus->PreviousValue != pKneelingSwitchStatus->CurrentValue))
                 && (ChangeKneelACK_NoAction == *pChangeKneelACK))
         {
            *pKneelingTimers_changeAck       = CONST_kneelFunc_NoAckTimeout;
            *pKneelingSwitchTimers           = PCODE_KneelButtonStuckedTimeout;
            pSM_KneelingSwitch->CurrentValue = SM_KneelingSwitch_ChangeKneelFunction;
         }
         else
         {
            //do nothing: Keep previous state 
         }
      break;
     //! ##### Check the conditions for state change from 'Default'
      default:
         //! #### Check for kneeling switch status and ChangeKneelACK status, for the state change to 'StandBy'
         //No action Event
         if ((A2PosSwitchStatus_Off == pKneelingSwitchStatus->CurrentValue)
            && (ChangeKneelACK_NotAvailable != *pChangeKneelACK))
         {
            pSM_KneelingSwitch->CurrentValue = SM_KneelingSwitch_StandBy;
         }
         else
         {
            // Do nothing: keep the previous state value
         }
      break;
   }
   //! #### Check for KneelingSwitchStatus, for the state change to 'Error'
         if (A2PosSwitchStatus_Error == pKneelingSwitchStatus->CurrentValue)
         {
            pSM_KneelingSwitch->CurrentValue = SM_KneelingSwitch_Error;
			*pKneelingSwitchTimers           = CONST_TimerFunctionInactive;
         }
   //! #### Process kneeling switch state machine output actions : 'KneelingSwitch_Outprocessing()'
         KneelingSwitch_Outprocessing(pKneelingChangeRequest,
                                      pSM_KneelingSwitch);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the KneelingSwitch_Outprocessing logic
//!
//! \param   *pKneelingChangeRequest          Update the output signal based on current state
//! \param   *pStateMachine_KneelingSwitch    Provides the current state of Kneeling switch statemachine
//!
//!======================================================================================
static void KneelingSwitch_Outprocessing(      KneelingChangeRequest_T  *pKneelingChangeRequest,
                                  const SM_KneelingSwitch        *pStateMachine_KneelingSwitch)

{
   if (pStateMachine_KneelingSwitch->CurrentValue != pStateMachine_KneelingSwitch->PreviousValue)
   {
      //! ##### Select the state based on 'StateMachine_KneelingSwitch->CurrentValue'
      switch (pStateMachine_KneelingSwitch->CurrentValue)
      {
         //! #### Output actions for 'StandBy' state
         case SM_KneelingSwitch_StandBy:
            *pKneelingChangeRequest  = KneelingChangeRequest_TakeNoAction;
         break;
         //! #### No output actions for 'SwitchOff' state
         case SM_KneelingSwitch_SwitchOff:
            // NO ACTION
         break;
         //! #### Output actions for 'ChangeKneelFunction' state
         case SM_KneelingSwitch_ChangeKneelFunction:
            *pKneelingChangeRequest  = KneelingChangeRequest_ChangeKneelFunction;
         break;
         //! #### Output actions for 'Error' state
         case SM_KneelingSwitch_Error:
            *pKneelingChangeRequest  = KneelingChangeRequest_Error;
         break;
         default:
            *pKneelingChangeRequest = KneelingChangeRequest_TakeNoAction;
         break;
      }
   }
   else
   {
      // Do nothing: keep previous status value
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the KneelingSwitch_DeviceIndication logic
//!
//! \param   *pKneelingStatusHMI       Provides the input status is Active or Inactive
//! \param   *pKneelDeviceIndication   Update the device indication to 'On' or 'Off' based on conditions
//!
//!======================================================================================
void KneelingSwitch_DeviceIndication(const KneelingStatusHMI_T *pKneelingStatusHMI,
                                           DeviceIndication_T  *pKneelDeviceIndication)
{
   //! ##### Check for KneelingStatusHMI status
   if (KneelingStatusHMI_Active == *pKneelingStatusHMI)
   {
      *pKneelDeviceIndication = DeviceIndication_On;
   }
   else if (KneelingStatusHMI_Inactive == *pKneelingStatusHMI)
   {
      *pKneelDeviceIndication = DeviceIndication_Off;
   }
   else
   {
      // Do nothing: Keep previous status value
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the RampLevel_StateTransitions logic
//!
//! \param   *pSM_Ramplevel               Provide and update the current state value
//! \param   *pLoadingLevelSwitchStatus   Provides the input switch status of LoadingLevelSwitch
//! \param   *pRampLevelRequestACK        Specifies the acknowledgement status of RampLevelRequest
//! \param   *pRamplevel_Type             Specify and update the current value
//! \param   *pRampLeveltimers            To check current timer value and update
//! \param   *ptimers_rampAck             To check current timer value and update
//! \param   *pRampLevelRequest           Update the Ramp Level request value
//! \param   *pRampLevelStrokeRequest     Update the Ramp Level Stroke request value
//!
//!======================================================================================
switch_sm_states RampLevel_StateTransitions(      Ramplevel_Switch     *pSM_Ramplevel,
                                            const A3PosSwitchStatus    *pLoadingLevelSwitchStatus,
                                            const ChangeRequest2Bit_T  *pRampLevelRequestACK,
                                                  uint8                *pRamplevel_Type,
                                                  uint16               *pRampLeveltimers,
                                                  uint16               *ptimers_rampAck,
												  RampLevelRequest_T   *pRampLevelRequest,
                                                  LevelStrokeRequest_T *pRampLevelStrokeRequest)
{
   switch_sm_states localrtrn   = SM_Ramplevel_NoAction;
   pSM_Ramplevel->PreviousValue = pSM_Ramplevel->CurrentValue;
   if (CONST_NotInitialize == *pRamplevel_Type)
   {
      *pRamplevel_Type            = CONST_Initialize;
      pSM_Ramplevel->CurrentValue = SM_Ramplevel_NoAction;
      *pRampLeveltimers           = CONST_TimerFunctionInactive;
      *ptimers_rampAck            = CONST_TimerFunctionInactive;
   }
   else
   {
      //! ###### Select the state based on 'pSM_Ramplevel->CurrentValue'
      switch (pSM_Ramplevel->CurrentValue)
      {
         //! ##### Check the conditions for state change from 'Ramplevel_M1' state
         case SM_Ramplevel_M1:
            //! #### Check for the state change to Ramplevel_NoAction
            //! #### Check for 'RampLevelRequestACK' status(Acknowledge event)
            if (ChangeRequest2Bit_Change == *pRampLevelRequestACK)
            {
               pSM_Ramplevel->CurrentValue = SM_Ramplevel_NoAction;
            }
            //! #### Check for time elapsed value for rampAck timer(No Acknowledge event)
            else if ((CONST_TimerFunctionElapsed == *ptimers_rampAck)
				    || (CONST_TimerFunctionInactive == *ptimers_rampAck))
            {
               pSM_Ramplevel->CurrentValue = SM_Ramplevel_NoAction;
            }
            else
            {
               // No event, keep the state until timeout occurs
            }
         break;
         //! ##### Check the conditiions for state change from 'Ramplevel_M2'
         case SM_Ramplevel_M2:
            //! #### Check for the state change to 'Ramplevel_NoAction'
            //! #### Check for 'RampLevelRequestACK' status(Acknowledge event)
            if (ChangeRequest2Bit_Change == *pRampLevelRequestACK)
            {
               pSM_Ramplevel->CurrentValue = SM_Ramplevel_NoAction;
            }
            //! #### Check for time elapsed value for rampAck timer(No Acknowledge event)
            else if ((CONST_TimerFunctionElapsed == *ptimers_rampAck)
				    || (CONST_TimerFunctionInactive == *ptimers_rampAck))
            {
               pSM_Ramplevel->CurrentValue = SM_Ramplevel_NoAction;
            }
            else
            {
               // No event, keep the state until timeout occurs
            }
         break;
         //! ##### Check the conditions for state change from 'Ramplevel_Stuck' state
         case SM_Ramplevel_Stuck:
            //! #### Check for the state change to 'Ramplevel_NoAction'
            //! #### Check for LoadingLevelSwitch status (Not Pressed Event)
            if (A3PosSwitchStatus_Middle == pLoadingLevelSwitchStatus->CurrentValue)
            {
               pSM_Ramplevel->CurrentValue = SM_Ramplevel_NoAction;
               (void)Rte_Call_Event_D1BUO_63_LoadingLevelSw_stuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            }
            else
            {
               // Do nothing : keep previous value
            }
         break;
       //! ##### Check the conditions for state change from 'No Action'
         default: //case  SM_Ramplevel_NoAction:
          //! #### Check Loading level switch status 
          //! #### Check for the state change to 'Ramplevel_M1'         
          //Up_pressed
          if (((A3PosSwitchStatus_Middle == pLoadingLevelSwitchStatus->PreviousValue)
             || (A3PosSwitchStatus_NotAvailable == pLoadingLevelSwitchStatus->PreviousValue)
             || (A3PosSwitchStatus_Lower == pLoadingLevelSwitchStatus->PreviousValue))
             && (A3PosSwitchStatus_Upper == pLoadingLevelSwitchStatus->CurrentValue))
          {
             *pRampLeveltimers           = PCODE_LoadingLevelSwStuckedTimeout;
             pSM_Ramplevel->CurrentValue = SM_Ramplevel_M1;
             *ptimers_rampAck            = CONST_LoadingLevelAckTimeout;
          }
          //! #### Check for the state change to 'Ramplevel_M2'
          //Down_pressed
          else if (((A3PosSwitchStatus_Middle == pLoadingLevelSwitchStatus->PreviousValue)
                  || (A3PosSwitchStatus_NotAvailable == pLoadingLevelSwitchStatus->PreviousValue)
                  || (A3PosSwitchStatus_Upper == pLoadingLevelSwitchStatus->PreviousValue))
                  && (A3PosSwitchStatus_Lower == pLoadingLevelSwitchStatus->CurrentValue))
          {
             pSM_Ramplevel->CurrentValue = SM_Ramplevel_M2;
             *pRampLeveltimers           = PCODE_LoadingLevelSwStuckedTimeout;
             *ptimers_rampAck            = CONST_LoadingLevelAckTimeout;
          }
          //! #### Check for the state change to 'Ramplevel_Stuck'
          //! #### Check for time elapsed value of 'RampLeveltimers'
          //Button_stuck
          else if (CONST_TimerFunctionElapsed == *pRampLeveltimers)
          {
             pSM_Ramplevel->CurrentValue = SM_Ramplevel_Stuck;
             (void)Rte_Call_Event_D1BUO_63_LoadingLevelSw_stuck_SetEventStatus(DEM_EVENT_STATUS_FAILED);
          }
          else
          {
             // No event, keep the state until timeout occurs
          }
         break;
      }
   }
	// cancel timer condition
	if ((A3PosSwitchStatus_Middle == pLoadingLevelSwitchStatus->CurrentValue)
	   || (A3PosSwitchStatus_NotAvailable == pLoadingLevelSwitchStatus->CurrentValue)
	   || (A3PosSwitchStatus_Error == pLoadingLevelSwitchStatus->CurrentValue))
	{
		*pRampLeveltimers = CONST_TimerFunctionInactive;
	}
	else
	{
		//Do:Nothing
    }
	 //! #### Process ramp level state machine output actions : 'RampLevel_OutputProcessing()'                              
	 RampLevel_OutputProcessing(pSM_Ramplevel,
								pRampLevelRequest,
								pRampLevelStrokeRequest);
   localrtrn = pSM_Ramplevel->CurrentValue;
   return localrtrn;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the RampLevel_OutputProcessing logic
//!
//! \param   *pOutSM_Ramplevel                 Provides the current state of RampLevel FSM
//! \param   *pRampLevelRequest                Update the output signals based on current state
//! \param   *pRampLevelStrokeRequest          Update the output signals based on current state
//!
//!======================================================================================
static void RampLevel_OutputProcessing(const Ramplevel_Switch     *pOutSM_Ramplevel,
                                             RampLevelRequest_T   *pRampLevelRequest,
                                             LevelStrokeRequest_T *pRampLevelStrokeRequest)
{
   if (pOutSM_Ramplevel->PreviousValue != pOutSM_Ramplevel->CurrentValue)
   {
      //! ##### Select the state based on 'SM_Ramplevel->CurrentValue' to update output actions
      switch (pOutSM_Ramplevel->CurrentValue)
      {
         //! #### Output actions for 'Stuck' state 
         case SM_Ramplevel_Stuck:
            //testfailed event missing
            *pRampLevelRequest       = RampLevelRequest_ErrorIndicator;
            *pRampLevelStrokeRequest = LevelStrokeRequest_ErrorIndicator;
         break;
         //! #### Output actions for 'Ramplevel_M1' state 
         case SM_Ramplevel_M1:
            *pRampLevelRequest       = RampLevelRequest_RampLevelM1;
            *pRampLevelStrokeRequest = LevelStrokeRequest_DockingLevelControlStroke;
         break;
         //! #### Output actions for 'Ramplevel_M2' state 
         case SM_Ramplevel_M2:
            *pRampLevelRequest       = RampLevelRequest_RampLevelM2;
            *pRampLevelStrokeRequest = LevelStrokeRequest_DockingLevelControlStroke;
         break;
       //! #### Output actions for 'NoAction' state 
         default: //case  SM_Ramplevel_NoAction:
          *pRampLevelRequest       = RampLevelRequest_TakeNoAction;
          *pRampLevelStrokeRequest = LevelStrokeRequest_DockingLevelControlStroke;
         break;
      }
   }
   else
   {
      // Do nothing: keep previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the AltDriveLevelSwitch_Transition logic
//!
//! \param   *pAlternativeDriveLevelSw_stat   Provides the input switch status
//! \param   *pTransitionState                Provides and updates the current state based on conditions
//! \param   *pADLS_Type                      Specify and update the current value
//! \param   *pRideHeightFunctionRequest      Update the Ride height function request value
//!
//!====================================================================================== 
void AltDriveLevelSwitch_Transition(const A3PosSwitchStatus     *pAlternativeDriveLevelSw_stat,
                                          SM_ADLS               *pTransitionState,
                                          uint8                 *pADLS_Type,
										  RideHeightFunction_T  *pRideHeightFunctionRequest)
{
   pTransitionState->PreviousValue = pTransitionState->CurrentValue;
   if (CONST_NotInitialize == *pADLS_Type)
   {
      *pADLS_Type                     = CONST_Initialize;
      pTransitionState->CurrentValue  = SM_ADLS_StandardDrivePosition;
   }
   else
   { 
      //! ###### Select the state based on 'TransitionState->CurrentValue' 
      switch (pTransitionState->CurrentValue)
      {
         //! ##### Check the conditions for State change from 'AlternativeDerivePosition1' to 'StandardDrivePosition' or 'AlternativeDrivePosition2'
         case SM_ADLS_AlternativeDrivePosition1:
            //! #### Check for AlternativeDriveLevelSw_stat status
            // Middle pressed event
            if ((pAlternativeDriveLevelSw_stat->CurrentValue != pAlternativeDriveLevelSw_stat->PreviousValue)
               && (A3PosSwitchStatus_Middle == pAlternativeDriveLevelSw_stat->CurrentValue))
            {
               pTransitionState->CurrentValue = SM_ADLS_StandardDrivePosition;
            }
            // Lower pressed event
            else if ((pAlternativeDriveLevelSw_stat->CurrentValue != pAlternativeDriveLevelSw_stat->PreviousValue)
                    && (A3PosSwitchStatus_Lower == pAlternativeDriveLevelSw_stat->CurrentValue))
            {
               pTransitionState->CurrentValue = SM_ADLS_AlternativeDrivePosition2;
            }
            else
            {
               // No event, keep previous state value
            }
         break;
         //! ##### Check the conditions for state change from 'AlternativeDerivePosition2' to 'StandardDrivePosition' or 'AlternativeDrivePosition1'
         case SM_ADLS_AlternativeDrivePosition2:
            //! #### Check for  AlternativeDriveLevelSw_stat status
            // Middle pressed event
            if ((pAlternativeDriveLevelSw_stat->CurrentValue != pAlternativeDriveLevelSw_stat->PreviousValue)
               && (A3PosSwitchStatus_Middle == pAlternativeDriveLevelSw_stat->CurrentValue))
            {
               pTransitionState->CurrentValue = SM_ADLS_StandardDrivePosition;
            }
            // Upper press event
            else if ((pAlternativeDriveLevelSw_stat->CurrentValue != pAlternativeDriveLevelSw_stat->PreviousValue)
                    && (A3PosSwitchStatus_Upper == pAlternativeDriveLevelSw_stat->CurrentValue))
            {
               pTransitionState->CurrentValue = SM_ADLS_AlternativeDrivePosition1;
            }
            else
            {
               // No event, keep previous state value
            }
         break;
       //! ##### Check the conditions for state change from 'StandardDerivePosition' to 'AlternativeDrivePosition1' or 'AlternativeDrivePosition2'
         default: //case SM_ADLS_StandardDrivePosition:
          //! #### Check for AlternativeDriveLevelSw_stat status
          // Upper press event
          if ((pAlternativeDriveLevelSw_stat->CurrentValue != pAlternativeDriveLevelSw_stat->PreviousValue)
            && (A3PosSwitchStatus_Upper == pAlternativeDriveLevelSw_stat->CurrentValue))
          {
             pTransitionState->CurrentValue = SM_ADLS_AlternativeDrivePosition1;
          }
          //Lower pressed event
          else if ((pAlternativeDriveLevelSw_stat->CurrentValue != pAlternativeDriveLevelSw_stat->PreviousValue)
             && (A3PosSwitchStatus_Lower == pAlternativeDriveLevelSw_stat->CurrentValue))
          {
             pTransitionState->CurrentValue = SM_ADLS_AlternativeDrivePosition2;
          }
          else
          {
             // No event, keep previous state value
          }
         break;
      }
   }
   	  //! #### Process alternative drive level switch state machine output actions : 'AltDriveLevelSwitch_OutputProcessing()'
      AltDriveLevelSwitch_OutputProcessing(pTransitionState,
                                           pRideHeightFunctionRequest);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the AltDriveLevelSwitch_OutputProcessing logic
//!
//! \param   *pOutTransitionState          Provides the current state value
//! \param   *pRideHeightFunctionRequest   Update the output based on current state
//!
//!======================================================================================
static void  AltDriveLevelSwitch_OutputProcessing(const SM_ADLS               *pOutTransitionState,
                                                        RideHeightFunction_T  *pRideHeightFunctionRequest)
{
   if (pOutTransitionState->PreviousValue != pOutTransitionState->CurrentValue)
   {
      //! ##### Select the state based on 'TransitionState->CurrentValue'
      switch (pOutTransitionState->CurrentValue)
      {
         //! #### Output action for 'AlternativeDrivePosition1' state
         case SM_ADLS_AlternativeDrivePosition1:
            *pRideHeightFunctionRequest = RideHeightFunction_AlternativeDrivePosition1;
         break;
         //! #### Output action for 'AlternativeDrivePosition2' state
         case SM_ADLS_AlternativeDrivePosition2:
            *pRideHeightFunctionRequest = RideHeightFunction_AlternativeDrivePosition2;
         break;
       //! #### Output action for 'StandardDrivePosition' state
         default: //case SM_ADLS_StandardDrivePosition:  
          *pRideHeightFunctionRequest = RideHeightFunction_StandardDrivePosition;
         break;
      }
   }
   else
   {
      // Do nothing: keep previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the FPBR_Switch_StateTransition logic
//!
//! \param   *pSM_FPBR_Switch         Provide and update the current state value
//! \param   *pFPBRSwitchStatus       Provides the FPBR switch status
//! \param   *pFPBRMMIStat            Provides the FPBRMMIStat status
//! \param   *pFPBR_Type              Specify and update the current value
//! \param   *pFPBRtimers             To check current timer value and update
//! \param   *pFPBR_DeviceIndication  Indicate the Device based on inputs 
//! \param   *pFPBRChangeReq          Update the FPBR change request value
//!
//!======================================================================================
void FPBR_Switch_StateTransition(      FPBR_Switch         *pSM_FPBR_Switch,
                                 const PushButtonStatus    *pFPBRSwitchStatus,
                                 const FPBRMMIStat_T       *pFPBRMMIStat,
                                       uint8               *pFPBR_Type,
                                       uint16              *pFPBRtimers,
									   DeviceIndication_T  *pFPBR_DeviceIndication,
                                       FPBRChangeReq_T     *pFPBRChangeReq)
{
   pSM_FPBR_Switch->PreviousValue = pSM_FPBR_Switch->CurrentValue;
   if (CONST_NotInitialize == *pFPBR_Type)
   {
      *pFPBR_Type                   = CONST_Initialize;
      pSM_FPBR_Switch->CurrentValue = SM_FPBRSwitch_Init;
      *pFPBRtimers                  = CONST_TimerFunctionInactive;  
    }
   else
   {
      // Do nothing : keep previous value
   }
   //! ###### Select the state based on 'SM_FPBR_Switch->currentValue'
   switch (pSM_FPBR_Switch->CurrentValue)
   {
      //! ##### Check the conditions for state change from 'Neutral'
      case SM_FPBRSwitch_Neutral:
         //! #### Check for FPBRChangeAck_RE and FPBRSwitch status for state change to 'Pushed'
         if (((PushButtonStatus_Neutral == pFPBRSwitchStatus->PreviousValue)
            && (PushButtonStatus_Pushed == pFPBRSwitchStatus->CurrentValue)))
         {
            if (Ack2Bit_NoAction == pFPBRMMIStat->FPBRChangeAck_RE)
            {
               pSM_FPBR_Switch->CurrentValue = SM_FPBRSwitch_Pushed;
               *pFPBRtimers                  = PCODE_FPBRSwitchRequestACKTime;
            }
            else
            {
               // Do nothing : keep previous value
            }
         }
         else
         {
            //Do:Nothing
         }
      break;
      //! ##### Check the conditions for state change from 'Pushed'
      case SM_FPBRSwitch_Pushed:
         //! #### Check for FPBRChangeAck_RE and FPBRSwitch status for state change to 'Neutral'
         if (Ack2Bit_ChangeAcknowledged == pFPBRMMIStat->FPBRChangeAck_RE)
         {
            pSM_FPBR_Switch->CurrentValue = SM_FPBRSwitch_Neutral;
            *pFPBRtimers                  = CONST_TimerFunctionInactive;
         }
         //! #### Check time elapsed value for timer
         else if (CONST_TimerFunctionElapsed == *pFPBRtimers)
         {
            pSM_FPBR_Switch->CurrentValue = SM_FPBRSwitch_Neutral;
         }
       else
       {
          //Do:Nothing
       }        
      break;
     //! ##### Check the condition for state change from 'Init'
      default: //case SM_FPBRSwitch_Init:
        //! #### Check for FPBRChangeAck status for state change to Neutral 
        if (Ack2Bit_NoAction == pFPBRMMIStat->FPBRChangeAck_RE)
        {
           pSM_FPBR_Switch->CurrentValue = SM_FPBRSwitch_Neutral;
        }
        else
        {
           // No event, keep previous state value
        }
      break;
   }
   //! #### Process FPBR switch state machine output actions :'FPBR_Switch_OutputProcessing()'
    FPBR_Switch_OutputProcessing(pFPBRMMIStat,
                                 pSM_FPBR_Switch,
                                 pFPBR_DeviceIndication,
                                 pFPBRChangeReq);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the FPBR_Switch_OutputProcessing logic
//!
//! \param   *pOutSM_FPBR_Switch          Provides the FPBRMMIStatus
//! \param   *pStateMachine_FPBR_Switch   Provides the current state of FPBR switch statemachine
//! \param   *pFPBR_DeviceIndication      Update the device indication values to 'On' or 'Off' or 'Blink' as per conditions
//! \param   *pFPBRChangeReq              Update FPBRChangeReq value as per conditions
//!
//!======================================================================================
static void FPBR_Switch_OutputProcessing(const FPBRMMIStat_T       *pOutSM_FPBR_Switch,
                                         const FPBR_Switch         *pStateMachine_FPBR_Switch,
                                               DeviceIndication_T  *pFPBR_DeviceIndication,
                                               FPBRChangeReq_T     *pFPBRChangeReq)
{
   //! ##### Select the state based on 'StateMachine_FPBR_Switch->CurrentValue'
   switch (pStateMachine_FPBR_Switch->CurrentValue)
   {
      //! #### Output actions for 'Neutral' state
      case SM_FPBRSwitch_Neutral:
         *pFPBRChangeReq = FPBRChangeReq_TakeNoAction;
         if (FPBRStatusInd_Off == pOutSM_FPBR_Switch->FPBRStatusInd_RE)
         {
            *pFPBR_DeviceIndication = DeviceIndication_Blink;
         }
         else if (FPBRStatusInd_Changing == pOutSM_FPBR_Switch->FPBRStatusInd_RE) //Blink enum not given in FPBRStatusInd_T
         {
            *pFPBR_DeviceIndication = DeviceIndication_Off;
         }
         else if (FPBRStatusInd_On == pOutSM_FPBR_Switch->FPBRStatusInd_RE)
         {
            *pFPBR_DeviceIndication = DeviceIndication_Blink;
         }
         else
         {
            // Do nothing: keep previous status
         }
      break;
      //! #### Output actions for 'Pushed' state
      case SM_FPBRSwitch_Pushed:
         *pFPBRChangeReq = FPBRChangeReq_ChangeFPBRFunction;
         if (FPBRStatusInd_Off == pOutSM_FPBR_Switch->FPBRStatusInd_RE)
         {
            *pFPBR_DeviceIndication = DeviceIndication_Blink;
         }
         else if (FPBRStatusInd_Changing == pOutSM_FPBR_Switch->FPBRStatusInd_RE) //Blink enum not given in FPBRStatusInd_T
         {
            *pFPBR_DeviceIndication = DeviceIndication_Off;
         }
         else if (FPBRStatusInd_On == pOutSM_FPBR_Switch->FPBRStatusInd_RE)
         {
            *pFPBR_DeviceIndication = DeviceIndication_Blink;
         }
         else
         {
            // Do nothing: keep previous status
         }
      break;
     //! #### Output actions for 'Init' state
      default:
        *pFPBRChangeReq         = FPBRChangeReq_TakeNoAction;
        *pFPBR_DeviceIndication = DeviceIndication_Off;
      break;
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the FPBR_SwitchstuckProcessing logic
//!
//! \param   *pFPBRSwitchStatus   Provides the FPBR switch status
//! \param   *pFPBRtimers_Stuck   To check current timer value and update
//!
//!======================================================================================
void FPBR_SwitchstuckProcessing(const PushButtonStatus *pFPBRSwitchStatus,
                                      uint16           *pFPBRtimers_Stuck)
{
   //! ###### if button changes from pushed cancel button stuck timer
   if ((PushButtonStatus_Neutral == pFPBRSwitchStatus->CurrentValue)
      || (PushButtonStatus_Error == pFPBRSwitchStatus->CurrentValue)
      || (PushButtonStatus_NotAvailable == pFPBRSwitchStatus->CurrentValue))
    {
       *pFPBRtimers_Stuck = CONST_TimerFunctionInactive;
       (void)Rte_Call_Event_D1DOO_63_FPBRSwitchStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
    }
   else if (PushButtonStatus_Pushed == pFPBRSwitchStatus->CurrentValue)
   {
      if (CONST_TimerFunctionInactive == *pFPBRtimers_Stuck)
      {
         *pFPBRtimers_Stuck = PCODE_FPBRSwitchStuckedTimeout;
      }
      //! ###### if button gets stucked raise DTC 'D1DOO_63'
      else if (CONST_TimerFunctionElapsed == *pFPBRtimers_Stuck)
      {
         (void)Rte_Call_Event_D1DOO_63_FPBRSwitchStuck_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         // No event, keep the state until timeout occurs
      }
   }   
   else
   {
       // No event, keep the state until timeout occurs
    }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the LoadingLvlAdj_SwitchLogic logic
//!
//! \param   *pLevelStrokeRequest               Update the LevelStrokeRequest value as per conditions
//! \param   *pLevelRequest                     Update the output signals of LevelRequest as per conditions
//! \param   *pLoadingLevelAdjSwitchStatus      Provides the switch status
//! \param   *pLoadingLvlAdjSwitchtimer_Stuck   To check current timer value and update
//!
//!======================================================================================
uint8 LoadingLvlAdj_SwitchLogic(LevelStrokeRequest_T   *pLevelStrokeRequest,
                                LevelRequest_T         *pLevelRequest,
                                A3PosSwitchStatus      *pLoadingLevelAdjSwitchStatus,
                                uint16                 *pLoadingLvlAdjSwitchtimer_Stuck)
{
   static uint8 LoadingLvlAdj_SwitchFlag = CONST_ResetFlag;
   uint8 isLoadingLvlAdj_SwitchChangedTo = 0U;
   
   if (CONST_ResetFlag == LoadingLvlAdj_SwitchFlag)
   {
      pLoadingLevelAdjSwitchStatus->PreviousValue = pLoadingLevelAdjSwitchStatus->CurrentValue;
      *pLoadingLvlAdjSwitchtimer_Stuck            = CONST_TimerFunctionInactive;
      LoadingLvlAdj_SwitchFlag                    = CONST_SetFlag;
   }
   else
   {
      // Do nothing
   }
   //! ##### Check for LoadingLevelAdjSwitch status
   if ((A3PosSwitchStatus_Middle == pLoadingLevelAdjSwitchStatus->PreviousValue)
      && (A3PosSwitchStatus_Upper == pLoadingLevelAdjSwitchStatus->CurrentValue))
   {
      //Stuck timer ON
      *pLoadingLvlAdjSwitchtimer_Stuck = PCODE_LoadingLvlAdjSwFulltimer;
      *pLevelStrokeRequest             = LevelStrokeRequest_DockingLevelControlStroke; 
	  isLoadingLvlAdj_SwitchChangedTo  = CONST_SWITCHSTATUSUPPER;
   }
   else if ((pLoadingLevelAdjSwitchStatus->CurrentValue != pLoadingLevelAdjSwitchStatus->PreviousValue)
           && ((A3PosSwitchStatus_Error == pLoadingLevelAdjSwitchStatus->CurrentValue)
           ||  (A3PosSwitchStatus_NotAvailable == pLoadingLevelAdjSwitchStatus->CurrentValue)))
   {
      // Stuck timer inactive
      *pLoadingLvlAdjSwitchtimer_Stuck = CONST_TimerFunctionInactive;
      pLevelRequest->FrontAxle_RE      = LevelChangeRequest_TakeNoAction;
      pLevelRequest->RearAxle_RE       = LevelChangeRequest_TakeNoAction;
   }  
   else if ((pLoadingLevelAdjSwitchStatus->CurrentValue != pLoadingLevelAdjSwitchStatus->PreviousValue)
           && (A3PosSwitchStatus_Middle == pLoadingLevelAdjSwitchStatus->CurrentValue))
   {
      // Stuck timer inactive
      *pLoadingLvlAdjSwitchtimer_Stuck = CONST_TimerFunctionInactive;
      pLevelRequest->FrontAxle_RE      = LevelChangeRequest_TakeNoAction;
      pLevelRequest->RearAxle_RE       = LevelChangeRequest_TakeNoAction;
      (void)Rte_Call_Event_D1CXC_63_LoadingLevelAdjSwitchStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   } 
   else if ((A3PosSwitchStatus_Middle == pLoadingLevelAdjSwitchStatus->PreviousValue)
           && (A3PosSwitchStatus_Lower == pLoadingLevelAdjSwitchStatus->CurrentValue))
   {
      // Stuck timer ON
      *pLoadingLvlAdjSwitchtimer_Stuck = PCODE_LoadingLvlAdjSwFulltimer;
      *pLevelStrokeRequest             = LevelStrokeRequest_DockingLevelControlStroke;
	  isLoadingLvlAdj_SwitchChangedTo  = CONST_SWITCHSTATUSLOWER;
   }
   else
   {    
      //! ##### Check for time elapsed value of Stuck timer
      if (CONST_TimerFunctionElapsed == *pLoadingLvlAdjSwitchtimer_Stuck)
      {
         pLevelRequest->FrontAxle_RE = LevelChangeRequest_TakeNoAction;
         pLevelRequest->RearAxle_RE  = LevelChangeRequest_TakeNoAction;
         (void)Rte_Call_Event_D1CXC_63_LoadingLevelAdjSwitchStuck_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         // Do nothing : keep previous value
      }
   }
   return isLoadingLvlAdj_SwitchChangedTo;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the Output_InhibitWRCECS logic
//!
//! \param   *pLoadingLevelAdjSwitchStatus      Provide the current switch status
//! \param   *pLoadingLevelSwitchStatus         Provide the current switch status
//! \param   *pSM_KneelingSwitch                Provide the current state of kneeling switch statemachine
//! \param   *pFerrySwitch_SM                   Provide the current state of ferry switch statemachine
//! \param   *pRamplevelSwitch_SM               Provide the current state of ramp level switch statemachine
//! \param   *pLoadingLvlAdjSwitch_Stucktimer   To check current timer value and update
//! \param   *pRamptimer                        To check current timer value and update
//! \param   *pFerryimer                        To check current timer value and update
//! \param   *pKneelingtimer                    To check current timer value and update
//! \param   *pInhibitWRCECSMenuCmd             Update the InhibitWRCECSMenuCmd based on conditions
//!
//!======================================================================================
void Output_InhibitWRCECS(const A3PosSwitchStatus   *pLoadingLevelAdjSwitchStatus,
                          const A3PosSwitchStatus   *pLoadingLevelSwitchStatus,
                          const SM_KneelingSwitch   *pSM_KneelingSwitch,
                          const SM_Ferryswitch      *pFerrySwitch_SM,
                          const Ramplevel_Switch    *pRamplevelSwitch_SM,
                          const uint16              *pLoadingLvlAdjSwitch_Stucktimer,
                          const uint16              *pRamptimer,
                          const uint16              *pFerryimer,
                          const uint16              *pKneelingtimer,
                               InactiveActive_T    *pInhibitWRCECSMenuCmd)
{
   //! ##### Check whether any switch is Active or not
   if ((SM_KneelingSwitch_ChangeKneelFunction == pSM_KneelingSwitch->CurrentValue)
      || (SM_FerrySwitch_ChangeFerryFunction == pFerrySwitch_SM->CurrentValue)
      || ((SM_Ramplevel_M1 == pRamplevelSwitch_SM->CurrentValue)
      || (SM_Ramplevel_M2 == pRamplevelSwitch_SM->CurrentValue)))
   {
      *pInhibitWRCECSMenuCmd = InactiveActive_Active;
   }
   //! ##### Check for LoadingLevelSwitch status and LoadingLevelAdjSwitch status 
   else if (((A3PosSwitchStatus_Lower == pLoadingLevelSwitchStatus->CurrentValue)
           || (A3PosSwitchStatus_Upper == pLoadingLevelSwitchStatus->CurrentValue))
           || ((A3PosSwitchStatus_Lower == pLoadingLevelAdjSwitchStatus->CurrentValue)
           || (A3PosSwitchStatus_Upper == pLoadingLevelAdjSwitchStatus->CurrentValue)))
   {
      *pInhibitWRCECSMenuCmd = InactiveActive_Active;
   }
   else
   {
      *pInhibitWRCECSMenuCmd = InactiveActive_Inactive;
   }
   //! ##### Check for LoadingLvlAdjSwitch_Stuck timer or RamplevelTimer or FerrySwitchTimer or KneelingSwitchTimer
   if ((CONST_TimerFunctionElapsed == *pLoadingLvlAdjSwitch_Stucktimer)
      || (CONST_TimerFunctionElapsed == *pRamptimer)
      || (CONST_TimerFunctionElapsed == *pFerryimer)
      || (CONST_TimerFunctionElapsed == *pKneelingtimer))
   {
      *pInhibitWRCECSMenuCmd = InactiveActive_Inactive;
   }
   else
   {
      // Do nothing : keep previous value
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the ECS_StandbyFSM_Transitions logic
//!
//! \param   *pVehicleMode              Provides the status of the vehicle mode
//! \param   *pSM_ECS_StandbyFSM        Provide and update the present state
//! \param   *pECSStandByReqRCECS_FSM   Provides the input status of ECSStandByReqRCECS
//! \param   *pECSStandByReqWRC         Provides the input status of ECSStandByReqWRC
//! \param   *pECSStandbyAllowed        Provides the input status of ECSStandbyAllowed
//! \param   *pECS_StandbyFSM_Type      Specify and update the current value
//! \param   *ptimers_ECSActive         Controls action related to Extendedstandby state
//! \param   *ptimers_ECSBlinkTime      Updates the ECSBlinkTime when state changes to StandbyActive
//! \param   *pBlinkECSWiredLEDs        Updating the output signal to True or False based on current state
//!
//!======================================================================================
static void ECS_StandbyFSM_Transitions(const VehicleMode_T   *pVehicleMode,
                                             ECS_StandbyFSM  *pSM_ECS_StandbyFSM,
                                       const ECSStandByReq   *pECSStandByReqRCECS_FSM,
                                       const ECSStandByReq   *pECSStandByReqWRC,
                                       const FalseTrue_T     *pECSStandbyAllowed,
                                             uint8           *pECS_StandbyFSM_Type,
                                             uint32          *ptimers_ECSActive,
                                             uint16          *ptimers_ECSBlinkTime,
											 FalseTrue_T     *pBlinkECSWiredLEDs)
{
   static boolean CommonCondition = FALSE;
   pSM_ECS_StandbyFSM->PreviousValue = pSM_ECS_StandbyFSM->CurrentValue;


   if (CONST_NotInitialize == *pECS_StandbyFSM_Type)
   {
      *pECS_StandbyFSM_Type            = CONST_Initialize;
      pSM_ECS_StandbyFSM->CurrentValue = SM_ECSStandby_StandByDisabled;
      *ptimers_ECSActive               = CONST_TimerFunctionInactive;
      *ptimers_ECSBlinkTime            = CONST_TimerFunctionInactive;
   }
   else
   {
      // Do nothing : keep previous value
   }
   //! ###### Select the state based on 'SM_ECS_StandbyFSM->CurrentValue'
   switch (pSM_ECS_StandbyFSM->CurrentValue)
   {
      //! ##### Check the conditions for state change from 'StandByDisabled' state
      case SM_ECSStandby_InitStandBy:
         //! #### Check for ECSStandByReqRCECS, ECSStandByReqWRC and vehicle mode status for the state change to 'StandByDisabled' 
         //! #### Check for ECSStandByReqRCECS, ECSStandByReqWRC and ECSStandbyAllowed status for state change to 'StandByIdle'
         if ((ECSStandByReq_StopStandby == pECSStandByReqRCECS_FSM->CurrentValue)
            || (ECSStandByReq_StopStandby == pECSStandByReqWRC->CurrentValue)
            || (FalseTrue_False == *pECSStandbyAllowed))
         {
            pSM_ECS_StandbyFSM->CurrentValue = SM_ECSStandby_StandByIdle;
         }
         //! #### Check for ECSStandByReqRCECS, ECSStandByReqWRC and ECSStandbyAllowed status for state change to 'WaitForAllowed'
         else if ((ECSStandByReq_StandbyRequested == pECSStandByReqRCECS_FSM->CurrentValue)
                 || (ECSStandByReq_StandbyRequested == pECSStandByReqWRC->CurrentValue))
         {
            pSM_ECS_StandbyFSM->CurrentValue = SM_ECSStandby_WaitForAllowed;
         }
         else
         {
            // Do nothing, keep previous state 
         }
      break;
      //! ##### Check the conditions for state change from 'StandByDisabled'
      case SM_ECSStandby_StandByIdle:
         //! #### Check for ECSStandByReqRCECS, ECSStandByReqWRC and vehicle mode status for the state change to 'StandByDisabled' 
         //! #### Examine the changes in ECSStandByReqRCECS, ECSStandByReqWRC and ECSStandbyAllowed for the state change to 'WaitForAllowed'
         if ((((ECSStandByReq_Idle == pECSStandByReqRCECS_FSM->PreviousValue)
            && (ECSStandByReq_StandbyRequested == pECSStandByReqRCECS_FSM->CurrentValue))
            || ((ECSStandByReq_Idle == pECSStandByReqWRC->PreviousValue)
            && (ECSStandByReq_StandbyRequested == pECSStandByReqWRC->CurrentValue)))
            && (FalseTrue_False != *pECSStandbyAllowed))
         {
            pSM_ECS_StandbyFSM->CurrentValue = SM_ECSStandby_WaitForAllowed;
         }
         else
         {
            // Do nothing, keep previous state 
         }
        // *ptimers_ECSstandby = CONST_TimerFunctionInactive;
      break;
      //! ##### Check the conditions for state change from 'StandByDisabled'
      case SM_ECSStandby_WaitForAllowed:
         //! #### Check for ECSStandByReqRCECS, ECSStandByReqWRC status for the state change to 'StandByDisabled'
         //! #### Check ECSStandByReqRCECS, ECSStandByReqWRC, and ECSStandbyAllowed status for the state change to 'StandByIdle'
         if ((ECSStandByReq_StopStandby == pECSStandByReqRCECS_FSM->CurrentValue)
            || (ECSStandByReq_StopStandby == pECSStandByReqWRC->CurrentValue)
            || (FalseTrue_False == *pECSStandbyAllowed))
         {
            pSM_ECS_StandbyFSM->CurrentValue = SM_ECSStandby_StandByIdle;
         }
         //! #### Check for ECSStandbyAllowed status for state change to 'StandByActive'
         else
         {
            if (FalseTrue_True == *pECSStandbyAllowed)
            {
               pSM_ECS_StandbyFSM->CurrentValue = SM_ECSStandby_StandByActive;
               *ptimers_ECSActive               = PCODE_ECSActiveStateTimeout;
               *ptimers_ECSBlinkTime            = PCODE_ECS_StandbyBlinkTime;
            }
            else
            {
               // Do nothing : keep previous value
            }
         }
      break;
      //! ##### Check the conditions for state change from 'StandByDisabled'
      case SM_ECSStandby_StandByActive:
         //! #### Check ECSStandByReqRCECS, ECSStandByReqWRC and vehiclemode status for the state change to 'StandByDisabled'
         //! #### Check for ECSStandByReqRCECS, ECSStandByReqWRC and ECSStandbyAllowed status for the state change to 'StandByIdle'
         if ((ECSStandByReq_StopStandby == pECSStandByReqRCECS_FSM->CurrentValue)
                 || (ECSStandByReq_StopStandby == pECSStandByReqWRC->CurrentValue) 
                 || (FalseTrue_True != *pECSStandbyAllowed))
         {
            pSM_ECS_StandbyFSM->CurrentValue = SM_ECSStandby_StandByIdle;
            *ptimers_ECSActive               = CONST_TimerFunctionInactive;
         }
         //! #### Check for Timeout value of ECSstandby timer for the state change to 'ExtendedStandBy'
         else
         {
            if (*ptimers_ECSActive <= PCODE_ECSStandbyExtendedActTimeout)
            {
               pSM_ECS_StandbyFSM->CurrentValue = SM_ECSStandby_ExtendedStandBy;
               *ptimers_ECSActive               = PCODE_ECSStandbyActivationTimeout;
            }
            else
            {
               // Do nothing : keep previous value
            }
         }
		 //Blink Led's for 800ms
		 if ((*ptimers_ECSBlinkTime <= PCODE_ECS_StandbyBlinkTime)
            && (*ptimers_ECSBlinkTime > CONST_TimerFunctionElapsed))
         {
            *pBlinkECSWiredLEDs = FalseTrue_True;
         }
         else if (CONST_TimerFunctionInactive == *ptimers_ECSBlinkTime)
         {
            *pBlinkECSWiredLEDs = FalseTrue_False;
         }
         else
         {
            // Do nothing : keep previous value
         }
      break        ;
      //! ##### Check the conditions for state change from 'ExtendedStandBy'
      case SM_ECSStandby_ExtendedStandBy:
         //! #### Check for the state change to 'StandByDisabled'
         //! #### Check for the state change to 'StandByIdle'
         //! #### Check for ECSStandByReqRCECS, ECSStandByReqWRC, ECSStandbyAllowed and ECSstandby timer status
         if ((ECSStandByReq_StopStandby == pECSStandByReqRCECS_FSM->CurrentValue)
            || (ECSStandByReq_StopStandby == pECSStandByReqWRC->CurrentValue)
            || (FalseTrue_True != *pECSStandbyAllowed)
            || (CONST_TimerFunctionElapsed == *ptimers_ECSActive))
         {
            pSM_ECS_StandbyFSM->CurrentValue = SM_ECSStandby_StandByIdle;
         }
         //! #### Check for the state change to 'ExtendedStandBy'
         else
         {
            //! #### Examine the changes in ECSStandByReqRCECS and ECSStandByReqWRC status
            if (((ECSStandByReq_Idle == pECSStandByReqRCECS_FSM->PreviousValue)
               && (ECSStandByReq_StandbyRequested == pECSStandByReqRCECS_FSM->CurrentValue))
               || ((ECSStandByReq_Idle == pECSStandByReqWRC->PreviousValue)
               && (ECSStandByReq_StandbyRequested == pECSStandByReqWRC->CurrentValue)))
            {
               pSM_ECS_StandbyFSM->CurrentValue = SM_ECSStandby_ExtendedStandBy;
               *ptimers_ECSActive               = PCODE_ECSStandbyActivationTimeout;
            }
            else
            {
               // Do nothing : keep previous value
            }
         }
      break;
     //! ##### Check the conditions for state change from 'StandByDisabled' to 'InitStandBy'
      default:// case SM_ECSStandby_StandByDisabled:
        //! #### Check for ECSStandByReqRCECS and ECSStandByReqWRC status
        if (((VehicleMode_Living == *pVehicleMode)
           || (VehicleMode_Accessory == *pVehicleMode))
           && (ECSStandByReq_Error != pECSStandByReqRCECS_FSM->CurrentValue)
           && (ECSStandByReq_Error != pECSStandByReqWRC->CurrentValue))
        {
           pSM_ECS_StandbyFSM->CurrentValue = SM_ECSStandby_InitStandBy;
		   CommonCondition = TRUE;
        }
        else
        {
           // Do nothing : keep previous value
        }
      break;
   }
   if (TRUE == CommonCondition)
   {
	    //! #### Check for ECSStandByReqRCECS, ECSStandByReqWRC and vehicle mode status
        if (((VehicleMode_Living != *pVehicleMode)
           && (VehicleMode_Accessory != *pVehicleMode))
           || (ECSStandByReq_Error == pECSStandByReqRCECS_FSM->CurrentValue)
           || (ECSStandByReq_Error == pECSStandByReqWRC->CurrentValue))
        {
            pSM_ECS_StandbyFSM->CurrentValue = SM_ECSStandby_StandByDisabled;
            *ptimers_ECSActive               = CONST_TimerFunctionInactive;
        }
		else
		{
		   //Do Nothing
		}
   }
   else
   {
       //Do Nothing
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the ECS_StandbyFSM_Outprocessing logic
//!
//! \param   *pOutSM_ECS_StandbyFSM   Provides the current state of ECS_StandbyFSM
//! \param   *pECSStandByRequest      Updating the output signal based on current state
//! \param   *pBlinkECSWiredLEDs      Updating the output signal to True or False based on current state
//! \param   *pECSStandByReqRCECS     Providing the present status
//!
//!======================================================================================
static void ECS_StandbyFSM_Outprocessing(const ECS_StandbyFSM       *pOutSM_ECS_StandbyFSM,
                                               ECSStandByRequest_T  *pECSStandByRequest,
                                               FalseTrue_T          *pBlinkECSWiredLEDs,
                                         const ECSStandByReq        *pECSStandByReqRCECS)
{
	if (pOutSM_ECS_StandbyFSM->CurrentValue != pOutSM_ECS_StandbyFSM->PreviousValue)
	{
		//! ##### Select the state based on 'SM_ECS_StandbyFSM->CurrentValue'
	   switch (pOutSM_ECS_StandbyFSM->CurrentValue)
	   {
		  //! #### Output actions for 'InitStandBy' state
		  case SM_ECSStandby_InitStandBy:
			 *pBlinkECSWiredLEDs = FalseTrue_False;
			 *pECSStandByRequest = ECSStandByRequest_NoRequest;
		  break;
		  //! #### Output actions for 'StandByIdle' state
		  case SM_ECSStandby_StandByIdle:
			 *pBlinkECSWiredLEDs = FalseTrue_False;
			 *pECSStandByRequest = ECSStandByRequest_NoRequest;
		  break;
		  //! #### Output actions for 'WaitForAllowed' state
		  case SM_ECSStandby_WaitForAllowed:
			 *pBlinkECSWiredLEDs = FalseTrue_False;
			 *pECSStandByRequest = ECSStandByRequest_Initiate;
		  break;
		  //! #### Output actions for 'StandByActive' state
		  case SM_ECSStandby_StandByActive:
			 if ((TRUE == PCODE_ECS_PartialAirSystem)
                || (TRUE == PCODE_ECS_FullAirSystem))
			 {
		 		*pECSStandByRequest = ECSStandByRequest_StandbyRequestedRCECS;
			 }
			 else if (TRUE == PCODE_WirelessRC_Enable)
			 {
				*pECSStandByRequest = ECSStandByRequest_StandbyRequestedWRC;
			 }
			 else
			 {
				 // Do:Nothing Keep Previous value
			 }		 
			 // code need to implement
		  break;
		  //! #### Output actions for 'SM_Extendedstandby' state
		  case SM_ECSStandby_ExtendedStandBy:
			 *pBlinkECSWiredLEDs = FalseTrue_False;
			 if ((TRUE == PCODE_ECS_PartialAirSystem)
                || (TRUE == PCODE_ECS_FullAirSystem))
			 {
				*pECSStandByRequest = ECSStandByRequest_StandbyRequestedRCECS;
			 }
			 else if (TRUE == PCODE_WirelessRC_Enable)
			 {
				*pECSStandByRequest = ECSStandByRequest_StandbyRequestedWRC;
			 }
			 else
			 {
				 // Do:Nothing Keep Previous value
			 }	
		  break;
		 //! #### Output actions for 'StandByDisabled' state
		  default: //case SM_ECSStandby_StandByDisabled:
			*pBlinkECSWiredLEDs = FalseTrue_False;
			*pECSStandByRequest = ECSStandByRequest_NoRequest;
		  break;
   }
	}
	else
	{
	   //Do:Nothing keep previous value
	}
   
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ECS_ActivationTrigger
//!
//! \param   *pECSANWControl_SM   Provides the input status of ECS_StandbyFSM_State   
//!
//! \return   FormalBoolean       Returns 'Yes' or 'No' value
//!
//!======================================================================================
static FormalBoolean ECS_ActivationTrigger(const ECS_StandbyFSM  *pECSANWControl_SM)
{
   FormalBoolean isActTriggerDetected = NO;

   //! ##### Check for ECS_StandbyFSM current State 
   if ((pECSANWControl_SM->PreviousValue != pECSANWControl_SM->CurrentValue)
      && ((SM_ECSStandby_StandByActive == pECSANWControl_SM->CurrentValue)
      || (SM_ECSStandby_ExtendedStandBy == pECSANWControl_SM->CurrentValue)))
   {
      isActTriggerDetected = YES;
   }
   else
   {
      // Do nothing : keep previous value
   }
   return isActTriggerDetected;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ECS_DeactivationTrigger
//!
//! \param   *pECSANWCtrl_SM   Provides the input status of ECS_StandbyFSM_State
//!
//! \return   FormalBoolean    Returns 'Yes' or 'No' value
//!
//!======================================================================================
static FormalBoolean ECS_DeactivationTrigger(const ECS_StandbyFSM  *pECSANWCtrl_SM)
{
   FormalBoolean isDeActTriggerDetected = NO;

   //! ##### Check for ECS_StandbyFSM curent State 
   if ((SM_ECSStandby_StandByIdle == pECSANWCtrl_SM->CurrentValue)
      || (SM_ECSStandby_StandByDisabled == pECSANWCtrl_SM->CurrentValue))
   {
      isDeActTriggerDetected = YES;
   }
   else
   {
      // Do nothing : keep previous value
   }
   return isDeActTriggerDetected;
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ANW_ECSStandByActive
//!
//! \param   *pisActTriggerDetected     Provides 'YES' or 'NO' values based on Activation trigger
//! \param   *pisDeActTriggerDetected   Provides 'YES' or 'NO' values based on Deactivation trigger
//!
//!======================================================================================
static void ANW_ECSStandByActive(const FormalBoolean  *pisActTriggerDetected,
                                 const FormalBoolean  *pisDeActTriggerDetected)
{
   static AnwSM_States           ANW_SM_ECSActDeactivation;
   static DeactivationTimer_Type ECSActive_Deactivationtimer = 0U;
          FormalBoolean isRestAct                            = NO;
          AnwAction_Enum Anw_Action                          = ANW_Action_None;

   //! ##### Process ANW logic for ECSStandByActive : 'ProcessAnwLogic()'
   Anw_Action = ProcessAnwLogic(isRestAct, 
                                *pisActTriggerDetected, 
                                *pisDeActTriggerDetected,
                                CONST_AnwEcsStandbyDeactTimeout,
                                &ECSActive_Deactivationtimer,
                                &ANW_SM_ECSActDeactivation);
   //! ##### Check for Anw_Action is 'Activate' or 'Deactivate' for ECSStandByActive
    if (ANW_Action_Activate == Anw_Action)
    {
      (void)Rte_Call_UR_ANW_ECSStandByActive_ActivateIss();
    }
    else if (ANW_Action_Deactivate == Anw_Action)
    {
      (void)Rte_Call_UR_ANW_ECSStandByActive_DeactivateIss();
    }
    else
    {
      // Do nothing, keep current state
    }
}
//! @}
//! @}
//! @}