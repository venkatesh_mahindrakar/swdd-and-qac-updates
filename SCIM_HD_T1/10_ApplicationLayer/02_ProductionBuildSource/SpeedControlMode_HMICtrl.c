/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  SpeedControlMode_HMICtrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  SpeedControlMode_HMICtrl
 *  Generated at:  Mon Jun 29 13:56:43 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <SpeedControlMode_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file SpeedControlMode_HMICtrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer
//! @{
//! @addtogroup Application_VehicleControlAndActiveSafety
//! @{
//! @addtogroup ActiveSafety
//! @{
//! @addtogroup SpeedContrlMode_HMICtrl
//! @{
//!
//! \brief
//! SpeedControlMode_HMICtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the SpeedControlMode_HMICtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_HeadwaySupport_P1BEX_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_SpeedControlMode_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
// Includes 
#include "SpeedControlMode_HMICtrl.h"
#include "FuncLibrary_ScimStd_If.h"
#include "FuncLibrary_Timer_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_HeadwaySupport_P1BEX_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * CCIM_ACC_T: Enumeration of integer in interval [0...7] with enumerators
 *   CCIM_ACC_ACCInactive (0U)
 *   CCIM_ACC_ACC1Active (1U)
 *   CCIM_ACC_ACC2Active (2U)
 *   CCIM_ACC_ACC3Active (3U)
 *   CCIM_ACC_Reserved (4U)
 *   CCIM_ACC_Reserved_01 (5U)
 *   CCIM_ACC_Error (6U)
 *   CCIM_ACC_NotAvailable (7U)
 * CCStates_T: Enumeration of integer in interval [0...15] with enumerators
 *   CCStates_OffDisabled (0U)
 *   CCStates_Hold (1U)
 *   CCStates_Accelerate (2U)
 *   CCStates_Decelerate (3U)
 *   CCStates_Resume (4U)
 *   CCStates_Set (5U)
 *   CCStates_Driver_Override (6U)
 *   CCStates_Spare_1 (7U)
 *   CCStates_Spare_2 (8U)
 *   CCStates_Spare_3 (9U)
 *   CCStates_Spare_4 (10U)
 *   CCStates_Spare_5 (11U)
 *   CCStates_Spare_6 (12U)
 *   CCStates_Spare_7 (13U)
 *   CCStates_Error (14U)
 *   CCStates_NotAvailable (15U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DriverMemory_rqst_T: Enumeration of integer in interval [0...31] with enumerators
 *   DriverMemory_rqst_UseDefaultDriverMemory (0U)
 *   DriverMemory_rqst_UseDriverMemory1 (1U)
 *   DriverMemory_rqst_UseDriverMemory2 (2U)
 *   DriverMemory_rqst_UseDriverMemory3 (3U)
 *   DriverMemory_rqst_UseDriverMemory4 (4U)
 *   DriverMemory_rqst_UseDriverMemory5 (5U)
 *   DriverMemory_rqst_UseDriverMemory6 (6U)
 *   DriverMemory_rqst_UseDriverMemory7 (7U)
 *   DriverMemory_rqst_UseDriverMemory8 (8U)
 *   DriverMemory_rqst_UseDriverMemory9 (9U)
 *   DriverMemory_rqst_UseDriverMemory10 (10U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory1 (11U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory2 (12U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory3 (13U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory4 (14U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory5 (15U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory6 (16U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory7 (17U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory8 (18U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory9 (19U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory10 (20U)
 *   DriverMemory_rqst_ResetAllMemThenUseDefDriverMem (21U)
 *   DriverMemory_rqst_Spare (22U)
 *   DriverMemory_rqst_Spare_01 (23U)
 *   DriverMemory_rqst_Spare_02 (24U)
 *   DriverMemory_rqst_Spare_03 (25U)
 *   DriverMemory_rqst_Spare_04 (26U)
 *   DriverMemory_rqst_Spare_05 (27U)
 *   DriverMemory_rqst_Spare_06 (28U)
 *   DriverMemory_rqst_Spare_07 (29U)
 *   DriverMemory_rqst_NotAvailable (30U)
 *   DriverMemory_rqst_Error (31U)
 * FWSelectedSpeedControlMode_T: Enumeration of integer in interval [0...7] with enumerators
 *   FWSelectedSpeedControlMode_Off (0U)
 *   FWSelectedSpeedControlMode_CruiseControl (1U)
 *   FWSelectedSpeedControlMode_AdjustableSpeedLimiter (2U)
 *   FWSelectedSpeedControlMode_AdaptiveCruiseControl (3U)
 *   FWSelectedSpeedControlMode_Spare_01 (4U)
 *   FWSelectedSpeedControlMode_Spare_02 (5U)
 *   FWSelectedSpeedControlMode_Error (6U)
 *   FWSelectedSpeedControlMode_NotAvailable (7U)
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 *   FreeWheel_Status_NoMovement (0U)
 *   FreeWheel_Status_1StepClockwise (1U)
 *   FreeWheel_Status_2StepsClockwise (2U)
 *   FreeWheel_Status_3StepsClockwise (3U)
 *   FreeWheel_Status_4StepsClockwise (4U)
 *   FreeWheel_Status_5StepsClockwise (5U)
 *   FreeWheel_Status_6StepsClockwise (6U)
 *   FreeWheel_Status_1StepCounterClockwise (7U)
 *   FreeWheel_Status_2StepsCounterClockwise (8U)
 *   FreeWheel_Status_3StepsCounterClockwise (9U)
 *   FreeWheel_Status_4StepsCounterClockwise (10U)
 *   FreeWheel_Status_5StepsCounterClockwise (11U)
 *   FreeWheel_Status_6StepsCounterClockwise (12U)
 *   FreeWheel_Status_Spare (13U)
 *   FreeWheel_Status_Error (14U)
 *   FreeWheel_Status_NotAvailable (15U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * XRSLStates_T: Enumeration of integer in interval [0...15] with enumerators
 *   XRSLStates_Off_disabled (0U)
 *   XRSLStates_Hold (1U)
 *   XRSLStates_Accelerate (2U)
 *   XRSLStates_Decelerate (3U)
 *   XRSLStates_Resume (4U)
 *   XRSLStates_Set (5U)
 *   XRSLStates_Driver_override (6U)
 *   XRSLStates_Error (14U)
 *   XRSLStates_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * SpeedControl_NVM_T: Array with 16 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_HeadwaySupport_P1BEX_T Rte_Prm_P1BEX_HeadwaySupport_v(void)
 *   boolean Rte_Prm_P1B2C_CCFW_Installed_v(void)
 *   boolean Rte_Prm_P1EXP_PersonalSettingsForCC_v(void)
 *
 *********************************************************************************************************************/


#define SpeedControlMode_HMICtrl_START_SEC_CODE
#include "SpeedControlMode_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_CCFW_Installed           (Rte_Prm_P1B2C_CCFW_Installed_v())
#define PCODE_HeadwaySupport           (Rte_Prm_P1BEX_HeadwaySupport_v())
#define PCODE_PersonalSettingsForCC    (Rte_Prm_P1EXP_PersonalSettingsForCC_v())

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SpeedControlMode_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_CCStates_CCStates(CCStates_T *data)
 *   Std_ReturnType Rte_Read_DriverMemory_rqst_DriverMemory_rqst(DriverMemory_rqst_T *data)
 *   Std_ReturnType Rte_Read_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I(uint8 *data)
 *     Argument data: uint8* is of type SpeedControl_NVM_T
 *   Std_ReturnType Rte_Read_SpeedControlModeButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SpeedControlModeWheelStatus_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_XRSLStates_XRSLStates(XRSLStates_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ACCOrCCIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_ASLIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_FWSelectedACCMode_CCIM_ACC(CCIM_ACC_T data)
 *   Std_ReturnType Rte_Write_FWSelectedSpeedControlMode_FWSelectedSpeedControlMode(FWSelectedSpeedControlMode_T data)
 *   Std_ReturnType Rte_Write_FWSpeedControlEndStopEvent_FWSpeedControlEndStopEvent(OffOn_T data)
 *   Std_ReturnType Rte_Write_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I(const uint8 *data)
 *     Argument data: uint8* is of type SpeedControl_NVM_T
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SpeedControlMode_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the SpeedControlMode_HMICtrl_20ms_runnable
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SpeedControlMode_HMICtrl_CODE) SpeedControlMode_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SpeedControlMode_HMICtrl_20ms_runnable
 *********************************************************************************************************************/
   static SCM_SM_States SpeedControlModeSM = { SM_SCM_Invalid,
                                               SM_SCM_Invalid };

   //! ###### Definition of the SWC input and output data structures
   static SpeedControlMode_HMICtrl_in_StructType  RteInData_Common;
   static SpeedControlMode_HMICtrl_out_StructType RteOutData_Common = { DeviceIndication_SpareValue,
                                                                        DeviceIndication_SpareValue,
                                                                        CCIM_ACC_NotAvailable,
                                                                        FWSelectedSpeedControlMode_NotAvailable,
                                                                        OffOn_NotAvailable };
   static uint8 arSpeedControlMode[Maximum_Memoryblocks] = { SM_SCM_Off };
   uint8_least i = 0U;
   static uint16 Timers[CONST_NbOfTimers];
   //! ###### Process RTE InData Read Common logic : 'Get_RteInDataRead_Common()'
   Get_RteInDataRead_Common(&RteInData_Common);
   //! ###### Process disable personal settings for speed control mode
   if (((DriverMemory_rqst_Spare != RteInData_Common.DriverMemory_rqst.newValue)
      && (DriverMemory_rqst_Spare_01 != RteInData_Common.DriverMemory_rqst.newValue)
      && (DriverMemory_rqst_Spare_02 != RteInData_Common.DriverMemory_rqst.newValue)
      && (DriverMemory_rqst_Spare_03 != RteInData_Common.DriverMemory_rqst.newValue)
      && (DriverMemory_rqst_Spare_04 != RteInData_Common.DriverMemory_rqst.newValue)
      && (DriverMemory_rqst_Spare_05 != RteInData_Common.DriverMemory_rqst.newValue)
      && (DriverMemory_rqst_Spare_06 != RteInData_Common.DriverMemory_rqst.newValue)
      && (DriverMemory_rqst_Spare_07 != RteInData_Common.DriverMemory_rqst.newValue)
      && (DriverMemory_rqst_NotAvailable != RteInData_Common.DriverMemory_rqst.newValue)
      && (DriverMemory_rqst_Error != RteInData_Common.DriverMemory_rqst.newValue))
      && (FALSE == PCODE_PersonalSettingsForCC))
   {
      RteInData_Common.DriverMemory_rqst.newValue = DriverMemory_rqst_UseDefaultDriverMemory;
   }
   else
   {
      // Do nothing: ignore value, keep previous status 
   }
   //! ###### Check for 'SwcActivation_IgnitionOn' port is equal to operational entry
   if (OperationalEntry == RteInData_Common.SwcActivation_IgnitionOn)
   {
      //! ##### Read the data stored in NVRAM
      Rte_Read_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I(arSpeedControlMode);
      if (255U == arSpeedControlMode[0U])
      {
         for (i = 0U; i < Maximum_Memoryblocks; i++)
         {
            arSpeedControlMode[i] = SM_SCM_Off;
         }
      }
      else
      {
         // Do nothing: ignore value, keep previous status
      }
   }
   //! ###### Check for 'SwcActivation_IgnitionOn' port is equal to operational
   else if (Operational == RteInData_Common.SwcActivation_IgnitionOn)
   {
      //! ##### Select 'CCFW_Installed' parameter
      if (TRUE == PCODE_CCFW_Installed)
      {
         //! #### Check for 'SpeedControlModeWheelStatus' and 'SpeedControlModeButtonStatus'
         if ((FreeWheel_Status_Error == RteInData_Common.SpeedControlModeWheelStatus.newValue)
            || (FreeWheel_Status_Spare == RteInData_Common.SpeedControlModeWheelStatus.newValue)
            || (FreeWheel_Status_NotAvailable == RteInData_Common.SpeedControlModeWheelStatus.newValue)
            || (PushButtonStatus_Error == RteInData_Common.SpeedControlModeButtonStatus.newValue)
            || (PushButtonStatus_NotAvailable == RteInData_Common.SpeedControlModeButtonStatus.newValue))
         {
            RteOutData_Common.ACCOrCCIndication          = DeviceIndication_Off;
            RteOutData_Common.ASLIndication              = DeviceIndication_Off;
            RteOutData_Common.FWSelectedACCMode          = CCIM_ACC_Error;
            RteOutData_Common.FWSelectedSpeedControlMode = FWSelectedSpeedControlMode_Error;
            RteOutData_Common.FWSpeedControlEndStopEvent = OffOn_Error;
         }
         else
         {
            //! #### Process SpeedControlMode Core Logic : 'SpeedControlMode_HMICtrl_CoreLogic()'
            SpeedControlMode_HMICtrl_CoreLogic(&RteInData_Common,
                                               &SpeedControlModeSM,
                                               &RteOutData_Common,
                                               arSpeedControlMode,
				                               &Timers[CONST_EndStopEventTimer]);
         }
      }
      else
      {
         //! ##### Process fallback function, if CCFW_Installed is not selected : 'fallbacklogic()'
         fallbacklogic(&RteOutData_Common);
      }
   }
   //! ###### Check for 'SwcActivation_IgnitionOn' port is equal to operational exit
   else if (OperationalExit == RteInData_Common.SwcActivation_IgnitionOn)
   {
      //! ##### Store the data in NVRAM
      Rte_Write_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I(arSpeedControlMode);
      //! ##### Process fallback function : 'fallbacklogic()'
      fallbacklogic(&RteOutData_Common);
   }
   else
   {
      //! ###### Process fallback function, if SwcActivation_IgnitionOn is not operational : 'fallbacklogic()'
      fallbacklogic(&RteOutData_Common);
   }
   //! ###### Timer decrement logic : 'TimerFunction_Tick()'
   TimerFunction_Tick(CONST_NbOfTimers,
                      Timers);
   //! ###### Processing RTE Write common ports logic:'Get_RteInDataWrite_Common()'
   Get_RteInDataWrite_Common(&RteOutData_Common);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SpeedControlMode_HMICtrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SpeedControlMode_HMICtrl_Init_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the SpeedControlMode_HMICtrl_Init
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SpeedControlMode_HMICtrl_CODE) SpeedControlMode_HMICtrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SpeedControlMode_HMICtrl_Init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define SpeedControlMode_HMICtrl_STOP_SEC_CODE
#include "SpeedControlMode_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Get_RteInDataRead_Common'
//!
//! \param    *pRteInData_Common   Examine and update the input signals based on RTE Failure Events
//!
//!======================================================================================
static void Get_RteInDataRead_Common(SpeedControlMode_HMICtrl_in_StructType *pRteInData_Common)
{
   //! ###### Processing Rte InDataRead logic for Error indication
   Std_ReturnType retValue = RTE_E_INVALID;

   //! ##### Read 'CCStates' interface
   retValue = Rte_Read_CCStates_CCStates(&pRteInData_Common->CCStates);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->CCStates),(CCStates_NotAvailable),(CCStates_Error))
   //! ##### Read 'DriverMemory_rqst' interface
   pRteInData_Common->DriverMemory_rqst.currentValue = pRteInData_Common->DriverMemory_rqst.newValue;
   retValue = Rte_Read_DriverMemory_rqst_DriverMemory_rqst(&pRteInData_Common->DriverMemory_rqst.newValue);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->DriverMemory_rqst.newValue),(DriverMemory_rqst_NotAvailable),(DriverMemory_rqst_Error))
   //! ##### Read 'SpeedControlModeButtonStatus' interface
   pRteInData_Common->SpeedControlModeButtonStatus.currentValue = pRteInData_Common->SpeedControlModeButtonStatus.newValue;
   retValue = Rte_Read_SpeedControlModeButtonStatus_PushButtonStatus(&pRteInData_Common->SpeedControlModeButtonStatus.newValue);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->SpeedControlModeButtonStatus.newValue),(PushButtonStatus_Error))
   //! ##### Read 'SpeedControlModeWheelStatus' interface
   pRteInData_Common->SpeedControlModeWheelStatus.currentValue = pRteInData_Common->SpeedControlModeWheelStatus.newValue;
   retValue = Rte_Read_SpeedControlModeWheelStatus_FreeWheel_Status(&pRteInData_Common->SpeedControlModeWheelStatus.newValue);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->SpeedControlModeWheelStatus.newValue),(FreeWheel_Status_Error))
   //! ##### Read 'XRSLStates' interface
   retValue = Rte_Read_XRSLStates_XRSLStates(&pRteInData_Common->XRSLStates);
   MACRO_StdRteRead_ExtRPort((retValue),(pRteInData_Common->XRSLStates),(XRSLStates_NotAvailable),(XRSLStates_Error))
   //! ##### Read 'SwcActivation_IgnitionOn' interface
   retValue = Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&pRteInData_Common->SwcActivation_IgnitionOn);
   MACRO_StdRteRead_IntRPort((retValue),(pRteInData_Common->SwcActivation_IgnitionOn),(NonOperational))
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Get_RteInDataWrite_Common'
//!
//! \param    *pRteOutData_Common   Updating the output signals to the ports of output structure
//!
//!======================================================================================
static void Get_RteInDataWrite_Common(const SpeedControlMode_HMICtrl_out_StructType *pRteOutData_Common)
{
   //! ###### Processing RTE OutData Write common logic
   Rte_Write_ACCOrCCIndication_DeviceIndication(pRteOutData_Common->ACCOrCCIndication);
   Rte_Write_ASLIndication_DeviceIndication(pRteOutData_Common->ASLIndication);
   Rte_Write_FWSelectedACCMode_CCIM_ACC(pRteOutData_Common->FWSelectedACCMode);
   Rte_Write_FWSelectedSpeedControlMode_FWSelectedSpeedControlMode(pRteOutData_Common->FWSelectedSpeedControlMode);
   Rte_Write_FWSpeedControlEndStopEvent_FWSpeedControlEndStopEvent(pRteOutData_Common->FWSpeedControlEndStopEvent);
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'FreeWheelOperation'
//!
//! \param    *pSpeedControlModeState   Providing current state of input
//! \param    turntype                  Providing the turn type as clockwise or counter-clockwise value
//!
//! \return   uint8                     Returns 'rtrn' value
//!
//!======================================================================================
static uint8 FreeWheelOperation(      SCM_SM_States    *pSpeedControlModeState,
                                const SCMTurnType_Enum turntype)
{
   static uint8 MaxCCW_SpeedControlModeVal = SM_SCM_CC;
   static uint8 rtrn                       = CONST_ResetFlag;
   
   pSpeedControlModeState->currentValue = pSpeedControlModeState->newValue;
   //! ###### Check for 'HeadwaySupport' parameter status
   if ((0U == PCODE_HeadwaySupport)
      || (1U == PCODE_HeadwaySupport)
      || (3U == PCODE_HeadwaySupport))
   {
      MaxCCW_SpeedControlModeVal = SM_SCM_CC;
   }
   else if ((2U == PCODE_HeadwaySupport)
           || (4U == PCODE_HeadwaySupport))
   {
      MaxCCW_SpeedControlModeVal = SM_SCM_ACC3;
   }
   else
   {
      // Do nothing: Keep Previous state
   }
   //! ###### Check for 'turntype' status 
   if (CONST_COUNTERCLOCKWISE == turntype)
   {
      //! ##### Process counterclockwise logic : 'counterclockwiselogic()'
      rtrn = counterclockwiselogic(pSpeedControlModeState,
                                   MaxCCW_SpeedControlModeVal);
   }
   else
   {
      //! ##### Process clockwise logic : 'clockwiselogic()'
      rtrn = clockwiselogic(pSpeedControlModeState,
                            MaxCCW_SpeedControlModeVal);
   }
   return rtrn;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'counterclockwiselogic'
//!
//! \param    *pSpeedModeSM                 Updating the state for 'SpeedControlStateMachine'
//! \param    MaxCCW_SpeedControlModeValue  Indicates the state of the state machine
//!
//! \return   uint8                         Returns 'SpeedControlEndStopFlag' value
//!
//!======================================================================================
static uint8 counterclockwiselogic(      SCM_SM_States   *pSpeedModeSM,
                                   const uint8           MaxCCW_SpeedControlModeValue)
{
   uint8 SpeedControlEndStopFlag = CONST_ResetFlag;
   //! ###### Process the state transition based on free wheel operation
   switch (pSpeedModeSM->newValue)
   {
      //! ##### State change from 'SM_SCM_ACC3' to 'SM_SCM_ACC3'
      case SM_SCM_ACC3:
         pSpeedModeSM->newValue   = SM_SCM_ACC3;
         SpeedControlEndStopFlag  = CONST_SetFlag;
      break;
      //! ##### State change from 'SM_SCM_ACC2' to 'SM_SCM_ACC3'
      case SM_SCM_ACC2:
         pSpeedModeSM->newValue = SM_SCM_ACC3;
      break;
      //! ##### State change from 'SM_SCM_ACC1' to 'SM_SCM_ACC2'
      case SM_SCM_ACC1:
         pSpeedModeSM->newValue = SM_SCM_ACC2;
      break;
      //! ##### State change from 'SM_SCM_CC' to 'SM_SCM_ACC1'
      case SM_SCM_CC:
         if (pSpeedModeSM->newValue == MaxCCW_SpeedControlModeValue)
         {
            pSpeedModeSM->currentValue = pSpeedModeSM->newValue;
            pSpeedModeSM->newValue     = MaxCCW_SpeedControlModeValue;
            SpeedControlEndStopFlag    = CONST_SetFlag;
         }
         else
         {
            pSpeedModeSM->newValue = SM_SCM_ACC1;
         }
      break;
      //! ##### State change from 'SM_SCM_ASL' to 'SM_SCM_CC'
      case SM_SCM_ASL:
         pSpeedModeSM->currentValue = pSpeedModeSM->newValue;
         SpeedControlEndStopFlag    = CONST_SetFlag;
         pSpeedModeSM->newValue     = SM_SCM_CC;
      break;
      default:
         pSpeedModeSM->newValue = SM_SCM_CC;
      break;
   }
   return SpeedControlEndStopFlag;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'clockwiselogic'
//!
//! \param   *pSpeedControlModeSM         Updating the state for 'SpeedControlStateMachine'
//! \param   MaxCCW_SpeedControlModeValue Indicates the state of the state machine
//!
//! \return  uint8                        Returns 'SCMEndStopFlag' value
//!
//!======================================================================================
static uint8 clockwiselogic(       SCM_SM_States   *pSpeedControlModeSM,
                            const  uint8           MaxCCW_SpeedControlModeValue)
{
   uint8 SCMEndStopFlag = CONST_ResetFlag;
   //! ###### Process the state transition based on free wheel operation
   switch (pSpeedControlModeSM->newValue)
   {
      //! ##### State change from 'SM_SCM_ACC3' to 'SM_SCM_ACC2'
      case SM_SCM_ACC3:
         pSpeedControlModeSM->newValue = SM_SCM_ACC2;
      break;
      //! ##### State change from 'SM_SCM_ACC2' to 'SM_SCM_ACC1'
      case SM_SCM_ACC2:
         pSpeedControlModeSM->newValue = SM_SCM_ACC1;
      break;
      //! ##### State change from 'SM_SCM_ACC1' to 'SM_SCM_CC'
      case SM_SCM_ACC1:
         pSpeedControlModeSM->newValue = SM_SCM_CC;
      break;
      //! ##### State change from 'SM_SCM_CC' to 'SM_SCM_ASL'
      case SM_SCM_CC:
         if (pSpeedControlModeSM->newValue == MaxCCW_SpeedControlModeValue)
         {
            pSpeedControlModeSM->currentValue = pSpeedControlModeSM->newValue;
            pSpeedControlModeSM->newValue     = MaxCCW_SpeedControlModeValue;
            SCMEndStopFlag                    = CONST_SetFlag;
         }
         else
         {
            // Do nothing
         }
         pSpeedControlModeSM->newValue = SM_SCM_ASL;
      break;
      //! ##### State change from 'SM_SCM_ASL' to 'SM_SCM_ASL'
      case SM_SCM_ASL:
         pSpeedControlModeSM->currentValue = pSpeedControlModeSM->newValue;
         SCMEndStopFlag                    = CONST_SetFlag;
         pSpeedControlModeSM->newValue     = SM_SCM_ASL;
      break;
      default:
         pSpeedControlModeSM->newValue = SM_SCM_ASL;
      break;
   }
   return SCMEndStopFlag;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'SpeedControlMode_HMICtrl_StateMachine'
//!
//! \param    *pSpeedControlMode_in_data    Providing current values for input data
//! \param    *pSpeedModeState              Indicates the state of the state machine
//! \param    *pTimers                      controls the end stop event status
//! \param    *pFWSpeedControlEndStopEvent  updates the end stop event indication
//!
//!======================================================================================
static void SpeedControlMode_HMICtrl_StateMachine(SpeedControlMode_HMICtrl_in_StructType *pSpeedControlMode_in_data,
                                                   SCM_SM_States                         *pSpeedModeState,
	                                               uint16                                *pTimers,
	                                               OffOn_T                               *pFWSpeedControlEndStopEvent) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
   //! ###### Processing state transitions
   static FreeWheel_Status_T modewheelStatus   = FreeWheel_Status_NoMovement;
   static uint8              speedCntrl_Flag   = CONST_ResetFlag;
   static uint8              isEndStateReached = CONST_ResetFlag;
   static uint8              Endstop_Flag      = CONST_ResetFlag;
   static boolean            isTimerStarted    = FALSE;
   SCMTurnType_Enum turntype                   = CONST_CLOCKWISE;

   if (CONST_ResetFlag == speedCntrl_Flag)
   {
      pSpeedControlMode_in_data->SpeedControlModeWheelStatus.currentValue = pSpeedControlMode_in_data->SpeedControlModeWheelStatus.newValue;
      speedCntrl_Flag                                                     = CONST_SetFlag;
   }
   else
   {
      // Do nothing
   }
   //! ##### Examine the changes in SpeedControlMode wheel status
   if (((((uint8_least)pSpeedControlMode_in_data->SpeedControlModeWheelStatus.newValue >= FreeWheel_Status_1StepClockwise)
      && ((uint8_least)pSpeedControlMode_in_data->SpeedControlModeWheelStatus.newValue < FreeWheel_Status_1StepCounterClockwise))
      && ((pSpeedControlMode_in_data->SpeedControlModeWheelStatus.currentValue == FreeWheel_Status_NoMovement)
      || ((uint8_least)pSpeedControlMode_in_data->SpeedControlModeWheelStatus.currentValue >= FreeWheel_Status_1StepCounterClockwise)))
      || (((uint8_least)pSpeedControlMode_in_data->SpeedControlModeWheelStatus.newValue >= FreeWheel_Status_1StepCounterClockwise)
      && ((pSpeedControlMode_in_data->SpeedControlModeWheelStatus.currentValue == FreeWheel_Status_NoMovement)
      || ((uint8_least)pSpeedControlMode_in_data->SpeedControlModeWheelStatus.currentValue < FreeWheel_Status_1StepCounterClockwise))))
   {
	  Endstop_Flag    = CONST_SetFlag;
      modewheelStatus = pSpeedControlMode_in_data->SpeedControlModeWheelStatus.newValue;
      //! #### Providing and updating modewheel status
      while (FreeWheel_Status_NoMovement != modewheelStatus)
      {
         if ((uint8_least)modewheelStatus >= FreeWheel_Status_1StepCounterClockwise)
         {
            turntype = CONST_COUNTERCLOCKWISE;
			isEndStateReached = FreeWheelOperation(pSpeedModeState,
                                          turntype);
            if ((uint8_least)modewheelStatus <= FreeWheel_Status_1StepCounterClockwise)
            {
               modewheelStatus = FreeWheel_Status_NoMovement;
            }
            else
            {
               modewheelStatus = modewheelStatus - 1U;
            }
         }
         else
         {
            turntype = CONST_CLOCKWISE;
			isEndStateReached = FreeWheelOperation(pSpeedModeState,
                                                   turntype);
            modewheelStatus = modewheelStatus - 1U;
         }
      }
   }
   else
   {
      // do nothing
   }
   //! ##### Check for SpeedControlMode button status
   if ((PushButtonStatus_Neutral == pSpeedControlMode_in_data->SpeedControlModeButtonStatus.currentValue)
      && (PushButtonStatus_Pushed == pSpeedControlMode_in_data->SpeedControlModeButtonStatus.newValue))
   {
      pSpeedModeState->currentValue = pSpeedModeState->newValue;
      pSpeedModeState->newValue     = SM_SCM_Off;
	  isEndStateReached = CONST_ResetFlag;
   }
   else
   {
      // Do nothing: keep previous state value
   }
   if ((pSpeedControlMode_in_data->SpeedControlModeButtonStatus.currentValue != pSpeedControlMode_in_data->SpeedControlModeButtonStatus.newValue)
      && ((PushButtonStatus_Neutral == pSpeedControlMode_in_data->SpeedControlModeButtonStatus.newValue)
      || (PushButtonStatus_Pushed == pSpeedControlMode_in_data->SpeedControlModeButtonStatus.newValue)))
   {
	   isEndStateReached = CONST_ResetFlag;
   }
   else
   {
      // Do nothing: keep previous state value
   }
   //! ##### Processing conditions for EndStopEvent indication logic
   if ((CONST_SetFlag == isEndStateReached)
	  && (pSpeedModeState->newValue == pSpeedModeState->currentValue))
   {
	   if ((FALSE == isTimerStarted) 
		  && (CONST_SetFlag == Endstop_Flag))
	   {
		   *pTimers                     = CONST_EndStopEventTimeout;
		   isTimerStarted               = TRUE;
		   *pFWSpeedControlEndStopEvent = OffOn_On;
	   }
	   else if ((CONST_TimerFunctionElapsed == *pTimers)
		       && (TRUE == isTimerStarted))
	   {
		   *pFWSpeedControlEndStopEvent = OffOn_Off;
		   *pTimers                     = CONST_TimerFunctionInactive;
		   Endstop_Flag                 = CONST_ResetFlag;
		   isTimerStarted               = FALSE;
	   }
	   else
	   {
		   //Do nothing
	   }
   }
   else
   {
	   *pFWSpeedControlEndStopEvent = OffOn_Off;
	   *pTimers                     = CONST_TimerFunctionInactive;
	   isTimerStarted               = FALSE;
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the SpeedControlMode_Restore_and_Store_Operation
//!
//! \param   *pSpeedControlMode_input_data   Indicating current values for input data
//! \param   *pSpeedControlState             Updating the output signal based on current state
//! \param   parSpeedCntrlMode               Updating the output signal based on current state
//!
//!======================================================================================
static void SpeedControlMode_Restore_and_Store_Operation(const SpeedControlMode_HMICtrl_in_StructType *pSpeedControlMode_input_data,
                                                               SCM_SM_States                          *pSpeedControlState,
                                                         const uint8                                  parSpeedCntrlMode[Maximum_Memoryblocks]) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
   //! ###### Check for 'XRSLStates' and 'CCStates' status
   if (((XRSLStates_Off_disabled == pSpeedControlMode_input_data->XRSLStates)
      || (XRSLStates_Error == pSpeedControlMode_input_data->XRSLStates)
      || (XRSLStates_NotAvailable == pSpeedControlMode_input_data->XRSLStates)
      || (XRSLStates_SpareValues1 == pSpeedControlMode_input_data->XRSLStates)
      || (XRSLStates_SpareValues2 == pSpeedControlMode_input_data->XRSLStates)
      || (XRSLStates_SpareValues3 == pSpeedControlMode_input_data->XRSLStates)
      || (XRSLStates_SpareValues4 == pSpeedControlMode_input_data->XRSLStates)
      || (XRSLStates_SpareValues5 == pSpeedControlMode_input_data->XRSLStates)
      || (XRSLStates_SpareValues6 == pSpeedControlMode_input_data->XRSLStates)
      || (XRSLStates_SpareValues7 == pSpeedControlMode_input_data->XRSLStates))
      && ((CCStates_OffDisabled == pSpeedControlMode_input_data->CCStates)
      || (CCStates_Error == pSpeedControlMode_input_data->CCStates)
      || (CCStates_NotAvailable == pSpeedControlMode_input_data->CCStates)
      || (CCStates_Spare_1 == pSpeedControlMode_input_data->CCStates)
      || (CCStates_Spare_2 == pSpeedControlMode_input_data->CCStates)
      || (CCStates_Spare_3 == pSpeedControlMode_input_data->CCStates)
      || (CCStates_Spare_4 == pSpeedControlMode_input_data->CCStates)
      || (CCStates_Spare_5 == pSpeedControlMode_input_data->CCStates)
      || (CCStates_Spare_6 == pSpeedControlMode_input_data->CCStates)
      || (CCStates_Spare_7 == pSpeedControlMode_input_data->CCStates)))
   {
      //! ##### Check for 'DriverMemory_request' status
      if (pSpeedControlMode_input_data->DriverMemory_rqst.currentValue != pSpeedControlMode_input_data->DriverMemory_rqst.newValue)
      {
         switch (pSpeedControlMode_input_data->DriverMemory_rqst.newValue)
         {
            //! #### Set the 'SpeedControlMode' StateMachine value based on 'UseDefaultDriverMemory'
            case DriverMemory_rqst_UseDefaultDriverMemory:
               pSpeedControlState->currentValue = pSpeedControlState->newValue;
               pSpeedControlState->newValue     = parSpeedCntrlMode[0];
            break;
            //! #### Set the 'SpeedControlMode' StateMachine value based on 'UseDriverMemory_1-10'
            case DriverMemory_rqst_UseDriverMemory1:
               pSpeedControlState->currentValue = pSpeedControlState->newValue;
               pSpeedControlState->newValue     = parSpeedCntrlMode[1];
            break;
            case DriverMemory_rqst_UseDriverMemory2:
               pSpeedControlState->currentValue = pSpeedControlState->newValue;
               pSpeedControlState->newValue     = parSpeedCntrlMode[2];
            break;
            case DriverMemory_rqst_UseDriverMemory3:
               pSpeedControlState->currentValue = pSpeedControlState->newValue;
               pSpeedControlState->newValue     = parSpeedCntrlMode[3];
            break;
            case DriverMemory_rqst_UseDriverMemory4:
               pSpeedControlState->currentValue = pSpeedControlState->newValue;
               pSpeedControlState->newValue     = parSpeedCntrlMode[4];
            break;
            case DriverMemory_rqst_UseDriverMemory5:
               pSpeedControlState->currentValue = pSpeedControlState->newValue;
               pSpeedControlState->newValue     = parSpeedCntrlMode[5];
            break;
            case DriverMemory_rqst_UseDriverMemory6:
               pSpeedControlState->currentValue = pSpeedControlState->newValue;
               pSpeedControlState->newValue     = parSpeedCntrlMode[6];
            break;
            case DriverMemory_rqst_UseDriverMemory7:
               pSpeedControlState->currentValue = pSpeedControlState->newValue;
               pSpeedControlState->newValue     = parSpeedCntrlMode[7];
            break;
            case DriverMemory_rqst_UseDriverMemory8:
               pSpeedControlState->currentValue = pSpeedControlState->newValue;
               pSpeedControlState->newValue     = parSpeedCntrlMode[8];
            break;
            case DriverMemory_rqst_UseDriverMemory9:
               pSpeedControlState->currentValue = pSpeedControlState->newValue;
               pSpeedControlState->newValue     = parSpeedCntrlMode[9];
            break;
            case DriverMemory_rqst_UseDriverMemory10:
               pSpeedControlState->currentValue = pSpeedControlState->newValue;
               pSpeedControlState->newValue     = parSpeedCntrlMode[10];
            break;
            //! #### Set 'SpeedControlMode' StateMachine value based on 'ResetAndThenUseDriverMemory_1-10' and 'ResetAllMemThenUseDefDriverMem'
            case DriverMemory_rqst_ResetAndThenUseDriverMemory1:
            case DriverMemory_rqst_ResetAndThenUseDriverMemory2:
            case DriverMemory_rqst_ResetAndThenUseDriverMemory3:
            case DriverMemory_rqst_ResetAndThenUseDriverMemory4:
            case DriverMemory_rqst_ResetAndThenUseDriverMemory5:
            case DriverMemory_rqst_ResetAndThenUseDriverMemory6:
            case DriverMemory_rqst_ResetAndThenUseDriverMemory7:
            case DriverMemory_rqst_ResetAndThenUseDriverMemory8:
            case DriverMemory_rqst_ResetAndThenUseDriverMemory9:
            case DriverMemory_rqst_ResetAndThenUseDriverMemory10:
            case DriverMemory_rqst_ResetAllMemThenUseDefDriverMem:
               pSpeedControlState->currentValue = pSpeedControlState->newValue;
               pSpeedControlState->newValue     = SM_SCM_Off;
            break;
            default:
               // Do nothing: keep the previous state
            break;
         }
      }
      else
      {
         // Do nothing: keep the previous state
      }
   }
   else
   {
      // Do nothing: Keep the previous state
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'SpeedControlMode_Store_Operation_OnSpeedModeChange'
//!
//! \param   *pVarDriverMem_rqst    Providing the current state value
//! \param   *pSpeedControlModeSM   Providing the present state value
//! \param   pSpeedControlMode      Updating the output signal based on current state
//!
//!======================================================================================
static void SpeedControlMode_Store_Operation_OnSpeedModeChange(const DriverMemoryReq       *pVarDriverMem_rqst,
                                                               const SCM_SM_States         *pSpeedControlModeSM,
                                                                     uint8                 pSpeedControlMode[Maximum_Memoryblocks]) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
	//! ###### Processing the store operation based on speed mode change
    switch (pVarDriverMem_rqst->newValue)
    {
        //! ##### Check 'DriverMemory_request' is equal to UseDefaultDriverMemory or ResetAllMemThenUseDefDriverMem 
        case DriverMemory_rqst_UseDefaultDriverMemory:
        case DriverMemory_rqst_ResetAllMemThenUseDefDriverMem:
             pSpeedControlMode[0] = pSpeedControlModeSM->newValue;
        break;
        //! ##### Check 'DriverMemory_request' is equal to UseDriverMemory_1-10 or ResetAndThenUseDriverMemory_1-10
        case DriverMemory_rqst_UseDriverMemory1:
        case DriverMemory_rqst_ResetAndThenUseDriverMemory1:
             pSpeedControlMode[1] = pSpeedControlModeSM->newValue;
        break;
        case DriverMemory_rqst_UseDriverMemory2:
        case DriverMemory_rqst_ResetAndThenUseDriverMemory2:
             pSpeedControlMode[2] = pSpeedControlModeSM->newValue;
        break;
        case DriverMemory_rqst_UseDriverMemory3:
        case DriverMemory_rqst_ResetAndThenUseDriverMemory3:
             pSpeedControlMode[3] = pSpeedControlModeSM->newValue;
        break;
        case DriverMemory_rqst_UseDriverMemory4:
        case DriverMemory_rqst_ResetAndThenUseDriverMemory4:
             pSpeedControlMode[4] = pSpeedControlModeSM->newValue;
        break;
        case DriverMemory_rqst_UseDriverMemory5:
        case DriverMemory_rqst_ResetAndThenUseDriverMemory5:
             pSpeedControlMode[5] = pSpeedControlModeSM->newValue;
        break;
        case DriverMemory_rqst_UseDriverMemory6:
        case DriverMemory_rqst_ResetAndThenUseDriverMemory6:
             pSpeedControlMode[6] = pSpeedControlModeSM->newValue;
        break;
        case DriverMemory_rqst_UseDriverMemory7:
        case DriverMemory_rqst_ResetAndThenUseDriverMemory7:
             pSpeedControlMode[7] = pSpeedControlModeSM->newValue;
        break;
        case DriverMemory_rqst_UseDriverMemory8:
        case DriverMemory_rqst_ResetAndThenUseDriverMemory8:
             pSpeedControlMode[8] = pSpeedControlModeSM->newValue;
        break;
        case DriverMemory_rqst_UseDriverMemory9:
        case DriverMemory_rqst_ResetAndThenUseDriverMemory9:
             pSpeedControlMode[9] = pSpeedControlModeSM->newValue;
        break;
        case DriverMemory_rqst_UseDriverMemory10:
        case DriverMemory_rqst_ResetAndThenUseDriverMemory10:
             pSpeedControlMode[10] = pSpeedControlModeSM->newValue;
        break;
        default:
           // Do nothing: keep the previous state
        break;
    }
      
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'SCM_Store_Operation_XRSLState_OnDriverMemoryRequestChange'
//!
//! \param   *pSpeedCntrlMode_in_data   Providing current values for input data
//! \param   *pVarDriverMem_request     Providing the current state value
//! \param   *pSpeedControlSM           Providing the current state value
//! \param   parSpeedControlMode        Updating output signal based on current state
//!
//!======================================================================================
static void SCM_Store_Operation_XRSLState_OnDriverMemoryRequestChange(const SpeedControlMode_HMICtrl_in_StructType *pSpeedCntrlMode_in_data,
                                                                      const DriverMemoryReq                        *pVarDriverMem_request,
                                                                      const SCM_SM_States                          *pSpeedControlSM,
                                                                            uint8                                  parSpeedControlMode[Maximum_Memoryblocks]) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
   //! ###### Check for 'ASLStates' and 'CCStates' status
   if ((XRSLStates_Hold == pSpeedCntrlMode_in_data->XRSLStates)
      || (XRSLStates_Accelerate == pSpeedCntrlMode_in_data->XRSLStates)
      || (XRSLStates_Decelerate == pSpeedCntrlMode_in_data->XRSLStates)
      || (XRSLStates_Resume == pSpeedCntrlMode_in_data->XRSLStates)
      || (XRSLStates_Set == pSpeedCntrlMode_in_data->XRSLStates)
      || (XRSLStates_Driver_override == pSpeedCntrlMode_in_data->XRSLStates)
      || (CCStates_Hold == pSpeedCntrlMode_in_data->CCStates)
      || (CCStates_Accelerate == pSpeedCntrlMode_in_data->CCStates)
      || (CCStates_Decelerate == pSpeedCntrlMode_in_data->CCStates)
      || (CCStates_Resume == pSpeedCntrlMode_in_data->CCStates)
      || (CCStates_Set == pSpeedCntrlMode_in_data->CCStates)
      || (CCStates_Driver_Override == pSpeedCntrlMode_in_data->CCStates))
   {
      if (pVarDriverMem_request->currentValue != pVarDriverMem_request->newValue)
      {
         switch (pVarDriverMem_request->newValue)
         {
            //! ##### Check for 'DriverMemory_request' is equal to UseDefaultDriverMemory
            case DriverMemory_rqst_UseDefaultDriverMemory:
               parSpeedControlMode[0] = pSpeedControlSM->newValue;
            break;
            //! ##### Check for 'DriverMemory_request' is equal to UseDriverMemory_1-10
            case DriverMemory_rqst_UseDriverMemory1:
               parSpeedControlMode[1] = pSpeedControlSM->newValue;
            break;
            case DriverMemory_rqst_UseDriverMemory2:
               parSpeedControlMode[2] = pSpeedControlSM->newValue;
            break;
            case DriverMemory_rqst_UseDriverMemory3:
               parSpeedControlMode[3] = pSpeedControlSM->newValue;
            break;
            case DriverMemory_rqst_UseDriverMemory4:
               parSpeedControlMode[4] = pSpeedControlSM->newValue;
            break;
            case DriverMemory_rqst_UseDriverMemory5:
               parSpeedControlMode[5] = pSpeedControlSM->newValue;
            break;
            case DriverMemory_rqst_UseDriverMemory6:
               parSpeedControlMode[6] = pSpeedControlSM->newValue;
            break;
            case DriverMemory_rqst_UseDriverMemory7:
               parSpeedControlMode[7] = pSpeedControlSM->newValue;
            break;
            case DriverMemory_rqst_UseDriverMemory8:
               parSpeedControlMode[8] = pSpeedControlSM->newValue;
            break;
            case DriverMemory_rqst_UseDriverMemory9:
               parSpeedControlMode[9] = pSpeedControlSM->newValue;
            break;
            case DriverMemory_rqst_UseDriverMemory10:
               parSpeedControlMode[10] = pSpeedControlSM->newValue;
            break;
            default:
               // Do nothing: keep previous state value
            break;
         }
      }
      else
      {
         // Do nothing: keep the previous state
      }
   }
   else
   {
      // Do nothing: keep the previous state
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the SCM_Store_Operation_OnDriverMemoryRequestChange
//!
//! \param   *pVarDriverMemory_rqst   Providing the current state value
//! \param   *pSpeedModeSM            Updating output signal based on current state
//! \param   pSpeedCntrlMode          Updating output signal based on current state
//!
//!======================================================================================
static void SCM_Store_Operation_OnDriverMemoryRequestChange(const DriverMemoryReq       *pVarDriverMemory_rqst,
                                                            const SCM_SM_States         *pSpeedModeSM,
                                                                  uint8                 pSpeedCntrlMode[Maximum_Memoryblocks]) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
   uint8_least i = 0U;

   if (pVarDriverMemory_rqst->currentValue != pVarDriverMemory_rqst->newValue)
   {
      //! ###### Processing the store operation based on driver memory request
      switch (pVarDriverMemory_rqst->newValue)
      {
         //! ##### Check for 'DriverMemory_request' is equal to ResetAndThenUseDriverMemory_1-10
         case DriverMemory_rqst_ResetAndThenUseDriverMemory1:
            pSpeedCntrlMode[1] = pSpeedModeSM->newValue;
         break;
         case DriverMemory_rqst_ResetAndThenUseDriverMemory2:
            pSpeedCntrlMode[2] = pSpeedModeSM->newValue;
         break;
         case DriverMemory_rqst_ResetAndThenUseDriverMemory3:
            pSpeedCntrlMode[3] = pSpeedModeSM->newValue;
         break;
         case DriverMemory_rqst_ResetAndThenUseDriverMemory4:
            pSpeedCntrlMode[4] = pSpeedModeSM->newValue;
         break;
         case DriverMemory_rqst_ResetAndThenUseDriverMemory5:
            pSpeedCntrlMode[5] = pSpeedModeSM->newValue;
         break;
         case DriverMemory_rqst_ResetAndThenUseDriverMemory6:
            pSpeedCntrlMode[6] = pSpeedModeSM->newValue;
         break;
         case DriverMemory_rqst_ResetAndThenUseDriverMemory7:
            pSpeedCntrlMode[7] = pSpeedModeSM->newValue;
         break;
         case DriverMemory_rqst_ResetAndThenUseDriverMemory8:
            pSpeedCntrlMode[8] = pSpeedModeSM->newValue;
         break;
         case DriverMemory_rqst_ResetAndThenUseDriverMemory9:
            pSpeedCntrlMode[9] = pSpeedModeSM->newValue;
         break;
         case DriverMemory_rqst_ResetAndThenUseDriverMemory10:
            pSpeedCntrlMode[10] = pSpeedModeSM->newValue;
         break;
         //! ##### Check for 'DriverMemory_request' is equal to ResetAllMemThenUseDefDriverMem
         case DriverMemory_rqst_ResetAllMemThenUseDefDriverMem:
            for (i = 0U ; i < Maximum_Memoryblocks ; i++)
            {
               pSpeedCntrlMode[i] = SM_SCM_Off;
            }
            pSpeedCntrlMode[0] = pSpeedModeSM->newValue;
         break;
         default:
            // Do nothing: keep previous state value
         break;
      }
   }
   else
   {
      // Do nothing: keep the previous state
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'SpeedControlMode_HMICtrl_OutSignalProcByStateMachine'
//!
//! \param    *pSpeedControlMode_output_data   Updating output signal based on current state
//! \param    *pSpeedCntrlModeSM               Providing the current state value
//!
//!======================================================================================
static void SpeedControlMode_HMICtrl_OutSignalProcByStateMachine(      SpeedControlMode_HMICtrl_out_StructType *pSpeedControlMode_output_data,
                                                                 const SCM_SM_States                           *pSpeedCntrlModeSM) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
   //! ###### Processing output actions based on current state
   switch (pSpeedCntrlModeSM->newValue)
   {
      //! ##### Output actions for ACC1 state
      case SM_SCM_ACC1:
         pSpeedControlMode_output_data->FWSelectedACCMode          = CCIM_ACC_ACC1Active;
         pSpeedControlMode_output_data->FWSelectedSpeedControlMode = FWSelectedSpeedControlMode_AdaptiveCruiseControl;
         pSpeedControlMode_output_data->ACCOrCCIndication          = DeviceIndication_On;
         pSpeedControlMode_output_data->ASLIndication              = DeviceIndication_Off;
      break;
      //! ##### Output actions for ACC2 state
      case SM_SCM_ACC2:
         pSpeedControlMode_output_data->FWSelectedACCMode          = CCIM_ACC_ACC2Active;
         pSpeedControlMode_output_data->FWSelectedSpeedControlMode = FWSelectedSpeedControlMode_AdaptiveCruiseControl;
         pSpeedControlMode_output_data->ACCOrCCIndication          = DeviceIndication_On;
         pSpeedControlMode_output_data->ASLIndication              = DeviceIndication_Off;
      break;
      //! ##### Output actions for ACC3 state
      case SM_SCM_ACC3:
         pSpeedControlMode_output_data->FWSelectedACCMode          = CCIM_ACC_ACC3Active;
         pSpeedControlMode_output_data->FWSelectedSpeedControlMode = FWSelectedSpeedControlMode_AdaptiveCruiseControl;
         pSpeedControlMode_output_data->ACCOrCCIndication          = DeviceIndication_On;
         pSpeedControlMode_output_data->ASLIndication              = DeviceIndication_Off;
      break;
      //! ##### Output actions for CC state
      case SM_SCM_CC:
         pSpeedControlMode_output_data->FWSelectedACCMode          = CCIM_ACC_ACCInactive;
         pSpeedControlMode_output_data->FWSelectedSpeedControlMode = FWSelectedSpeedControlMode_CruiseControl;
         pSpeedControlMode_output_data->ACCOrCCIndication          = DeviceIndication_On;
         pSpeedControlMode_output_data->ASLIndication              = DeviceIndication_Off;
      break;
      //! ##### Output actions for ASL state
      case SM_SCM_ASL:
         pSpeedControlMode_output_data->FWSelectedACCMode          = CCIM_ACC_ACCInactive;
         pSpeedControlMode_output_data->FWSelectedSpeedControlMode = FWSelectedSpeedControlMode_AdjustableSpeedLimiter;
         pSpeedControlMode_output_data->ACCOrCCIndication          = DeviceIndication_Off;
         pSpeedControlMode_output_data->ASLIndication              = DeviceIndication_On;
      break;
      //! ##### Output actions for default state
      default:
         pSpeedControlMode_output_data->FWSelectedACCMode          = CCIM_ACC_ACCInactive;
         pSpeedControlMode_output_data->FWSelectedSpeedControlMode = FWSelectedSpeedControlMode_Off;
         pSpeedControlMode_output_data->ACCOrCCIndication          = DeviceIndication_Off;
         pSpeedControlMode_output_data->ASLIndication              = DeviceIndication_Off;
      break;
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'fallbacklogic'
//!
//! \param    *pSpeedControlModel_output_data   Updating the output signals to the ports of output structure
//!
//!======================================================================================
static void fallbacklogic(SpeedControlMode_HMICtrl_out_StructType *pSpeedControlModel_output_data)
{
   //! ###### Upadte the output ports with off or Not Available
   pSpeedControlModel_output_data->ACCOrCCIndication          = DeviceIndication_Off;
   pSpeedControlModel_output_data->ASLIndication              = DeviceIndication_Off;
   pSpeedControlModel_output_data->FWSelectedACCMode          = CCIM_ACC_NotAvailable;
   pSpeedControlModel_output_data->FWSelectedSpeedControlMode = FWSelectedSpeedControlMode_NotAvailable;
   pSpeedControlModel_output_data->FWSpeedControlEndStopEvent = OffOn_NotAvailable;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'SpeedControlMode_HMICtrl_CoreLogic'
//!
//! \param   *pRteInData_Common            Providing current input status
//! \param   *pSpeedCntrlModeState         Providing current input state
//! \param   *pSpeedControlMode_out_data   Updating 'FWSpeedControlEndStopEvent' to On or Off
//! \param   parSpeedControlMode           Provide the current value
//! \param   *pTimer                       controls the status of end stop event
//!
//!======================================================================================
static void SpeedControlMode_HMICtrl_CoreLogic(SpeedControlMode_HMICtrl_in_StructType  *pRteInData_Common,
                                               SCM_SM_States                           *pSpeedCntrlModeState,
                                               SpeedControlMode_HMICtrl_out_StructType *pSpeedControlMode_out_data,
                                               uint8                                   parSpeedControlMode[Maximum_Memoryblocks],
	                                           uint16                                   *pTimer) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
   //! ###### Process SpeedControlMode CoreLogic  
   
   //! ##### Process memory restore operation logic : 'SpeedControlMode_Restore_and_Store_Operation()'
   SpeedControlMode_Restore_and_Store_Operation(pRteInData_Common,
                                                pSpeedCntrlModeState,
                                                parSpeedControlMode);
   //! ##### Process SpeedControlMode StateMachine transitions logic : 'SpeedControlMode_HMICtrl_StateMachine()'
   SpeedControlMode_HMICtrl_StateMachine(pRteInData_Common,
                                         pSpeedCntrlModeState,
	                                      pTimer,
	                                     &pSpeedControlMode_out_data->FWSpeedControlEndStopEvent);
   //! ##### Process store operation logic based on SpeedModeChange : 'SpeedControlMode_Store_Operation_OnSpeedModeChange()'
   SpeedControlMode_Store_Operation_OnSpeedModeChange(&pRteInData_Common->DriverMemory_rqst,
                                                      pSpeedCntrlModeState,
                                                      parSpeedControlMode);
   //! ##### Process store operation logic based on XRSLStateChange : 'SCM_Store_Operation_XRSLState_OnDriverMemoryRequestChange()'
   SCM_Store_Operation_XRSLState_OnDriverMemoryRequestChange(pRteInData_Common,
                                                             &pRteInData_Common->DriverMemory_rqst,
                                                             pSpeedCntrlModeState,
                                                             parSpeedControlMode);
   //! ##### Process store operation logic based on DriverMemoryRequestChange : 'SCM_Store_Operation_OnDriverMemoryRequestChange()'
   SCM_Store_Operation_OnDriverMemoryRequestChange(&pRteInData_Common->DriverMemory_rqst,
                                                   pSpeedCntrlModeState,
                                                   parSpeedControlMode);
   //! ##### Process state machine output actions logic : 'SpeedControlMode_HMICtrl_OutSignalProcByStateMachine()'
   SpeedControlMode_HMICtrl_OutSignalProcByStateMachine(pSpeedControlMode_out_data,
                                                        pSpeedCntrlModeState);
}

//! @}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
