/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  LevelControl_HMICtrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  LevelControl_HMICtrl
 *  Generated at:  Fri Jun 12 18:00:03 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <LevelControl_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
//!======================================================================================
//! \file LevelControl_HMICtrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_VehicleDynamics_Suspension 
//! @{
//! @addtogroup LevelControl_HMICtrl
//! @{
//!
//! \brief
//! LevelControl_HMICtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the LevelControl_HMICtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_ECSActiveStateTimeout_P1CUE_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_ECSStandbyActivationTimeout_P1CUA_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_ECSStandbyExtendedActTimeout_P1CUB_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_ECS_StandbyBlinkTime_P1GCL_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FPBRSwitchRequestACKTime_P1LXR_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FerryFuncSwStuckedTimeout_P1EXK_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FrontSuspensionType_P1JBR_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_KneelButtonStuckedTimeout_P1DWD_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_LevelControl_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
// Includes
#include "LevelControl_HMICtrl_If.h"
#include "LevelControl_HMICtrl_Logic_If.h"
#include "LevelControl_HMICtrl.h"
#include "FuncLibrary_Timer_If.h"
#include "FuncLibrary_ScimStd_If.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_ECSActiveStateTimeout_P1CUE_T: Integer in interval [0...65535]
 * SEWS_ECSStandbyActivationTimeout_P1CUA_T: Integer in interval [0...65535]
 * SEWS_ECSStandbyExtendedActTimeout_P1CUB_T: Integer in interval [0...65535]
 * SEWS_ECS_StandbyBlinkTime_P1GCL_T: Integer in interval [0...255]
 * SEWS_FPBRSwitchRequestACKTime_P1LXR_T: Integer in interval [0...255]
 * SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T: Integer in interval [0...255]
 * SEWS_FerryFuncSwStuckedTimeout_P1EXK_T: Integer in interval [0...255]
 * SEWS_FrontSuspensionType_P1JBR_T: Integer in interval [0...255]
 * SEWS_KneelButtonStuckedTimeout_P1DWD_T: Integer in interval [0...255]
 * SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T: Integer in interval [0...255]
 * SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T: Integer in interval [0...255]
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * Ack2Bit_T: Enumeration of integer in interval [0...3] with enumerators
 *   Ack2Bit_NoAction (0U)
 *   Ack2Bit_ChangeAcknowledged (1U)
 *   Ack2Bit_Error (2U)
 *   Ack2Bit_NotAvailable (3U)
 * BackToDriveReqACK_T: Enumeration of integer in interval [0...3] with enumerators
 *   BackToDriveReqACK_TakNoAction (0U)
 *   BackToDriveReqACK_ChangeAcknowledged (1U)
 *   BackToDriveReqACK_Error (2U)
 *   BackToDriveReqACK_NotAvailable (3U)
 * BackToDriveReq_T: Enumeration of integer in interval [0...3] with enumerators
 *   BackToDriveReq_Idle (0U)
 *   BackToDriveReq_B2DRequested (1U)
 *   Requested_Error (2U)
 *   Requested_NotAvailable (3U)
 * ChangeKneelACK_T: Enumeration of integer in interval [0...3] with enumerators
 *   ChangeKneelACK_NoAction (0U)
 *   ChangeKneelACK_ChangeAcknowledged (1U)
 *   ChangeKneelACK_Error (2U)
 *   ChangeKneelACK_NotAvailable (3U)
 * ChangeRequest2Bit_T: Enumeration of integer in interval [0...3] with enumerators
 *   ChangeRequest2Bit_TakeNoAction (0U)
 *   ChangeRequest2Bit_Change (1U)
 *   ChangeRequest2Bit_Error (2U)
 *   ChangeRequest2Bit_NotAvailable (3U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * ECSStandByReq_T: Enumeration of integer in interval [0...7] with enumerators
 *   ECSStandByReq_Idle (0U)
 *   ECSStandByReq_StandbyRequested (1U)
 *   ECSStandByReq_StopStandby (2U)
 *   ECSStandByReq_Reserved (3U)
 *   ECSStandByReq_Reserved_01 (4U)
 *   ECSStandByReq_Reserved_02 (5U)
 *   ECSStandByReq_Error (6U)
 *   ECSStandByReq_NotAvailable (7U)
 * ECSStandByRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   ECSStandByRequest_NoRequest (0U)
 *   ECSStandByRequest_Initiate (1U)
 *   ECSStandByRequest_StandbyRequestedRCECS (2U)
 *   ECSStandByRequest_StandbyRequestedWRC (3U)
 *   ECSStandByRequest_Reserved (4U)
 *   ECSStandByRequest_Reserved_01 (5U)
 *   ECSStandByRequest_Error (6U)
 *   ECSStandByRequest_NotAvailable (7U)
 * ElectricalLoadReduction_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   ElectricalLoadReduction_rqst_NoRequest (0U)
 *   ElectricalLoadReduction_rqst_Level1Request (1U)
 *   ElectricalLoadReduction_rqst_Level2Request (2U)
 *   ElectricalLoadReduction_rqst_SpareValue (3U)
 *   ElectricalLoadReduction_rqst_SpareValue_01 (4U)
 *   ElectricalLoadReduction_rqst_SpareValue_02 (5U)
 *   ElectricalLoadReduction_rqst_Error (6U)
 *   ElectricalLoadReduction_rqst_NotAvailable (7U)
 * FPBRChangeReq_T: Enumeration of integer in interval [0...3] with enumerators
 *   FPBRChangeReq_TakeNoAction (0U)
 *   FPBRChangeReq_ChangeFPBRFunction (1U)
 *   FPBRChangeReq_Error (2U)
 *   FPBRChangeReq_NotAvailable (3U)
 * FPBRStatusInd_T: Enumeration of integer in interval [0...3] with enumerators
 *   FPBRStatusInd_Off (0U)
 *   FPBRStatusInd_On (1U)
 *   FPBRStatusInd_Changing (2U)
 *   FPBRStatusInd_NotAvailable (3U)
 * FalseTrue_T: Enumeration of integer in interval [0...3] with enumerators
 *   FalseTrue_False (0U)
 *   FalseTrue_True (1U)
 *   FalseTrue_Error (2U)
 *   FalseTrue_NotAvaiable (3U)
 * FerryFunctionStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   FerryFunctionStatus_FerryFunctionNotActive (0U)
 *   FerryFunctionStatus_FerryFunctionActiveLevelReached (1U)
 *   FerryFunctionStatus_FerryFunctionActiveLeveNotlReached (2U)
 *   FerryFunctionStatus_Spare_01 (3U)
 *   FerryFunctionStatus_Spare_02 (4U)
 *   FerryFunctionStatus_Spare_03 (5U)
 *   FerryFunctionStatus_Error (6U)
 *   FerryFunctionStatus_NotAvailable (7U)
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   InactiveActive_Inactive (0U)
 *   InactiveActive_Active (1U)
 *   InactiveActive_Error (2U)
 *   InactiveActive_NotAvailable (3U)
 * KneelingChangeRequest_T: Enumeration of integer in interval [0...3] with enumerators
 *   KneelingChangeRequest_TakeNoAction (0U)
 *   KneelingChangeRequest_ChangeKneelFunction (1U)
 *   KneelingChangeRequest_Error (2U)
 *   KneelingChangeRequest_NotAvailable (3U)
 * KneelingStatusHMI_T: Enumeration of integer in interval [0...3] with enumerators
 *   KneelingStatusHMI_Inactive (0U)
 *   KneelingStatusHMI_Active (1U)
 *   KneelingStatusHMI_Error (2U)
 *   KneelingStatusHMI_NotAvailable (3U)
 * LevelAdjustmentAction_T: Enumeration of integer in interval [0...15] with enumerators
 *   LevelAdjustmentAction_Idle (0U)
 *   LevelAdjustmentAction_UpBasic (1U)
 *   LevelAdjustmentAction_DownBasic (2U)
 *   LevelAdjustmentAction_UpShortMovement (3U)
 *   LevelAdjustmentAction_DownShortMovement (4U)
 *   LevelAdjustmentAction_Reserved (5U)
 *   LevelAdjustmentAction_GotoDriveLevel (6U)
 *   LevelAdjustmentAction_Reserved_01 (7U)
 *   LevelAdjustmentAction_Ferry (8U)
 *   LevelAdjustmentAction_Reserved_02 (9U)
 *   LevelAdjustmentAction_Reserved_03 (10U)
 *   LevelAdjustmentAction_Reserved_04 (11U)
 *   LevelAdjustmentAction_Reserved_05 (12U)
 *   LevelAdjustmentAction_Reserved_06 (13U)
 *   LevelAdjustmentAction_Error (14U)
 *   LevelAdjustmentAction_NotAvailable (15U)
 * LevelAdjustmentAxles_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelAdjustmentAxles_Rear (0U)
 *   LevelAdjustmentAxles_Front (1U)
 *   LevelAdjustmentAxles_Parallel (2U)
 *   LevelAdjustmentAxles_Reserved (3U)
 *   LevelAdjustmentAxles_Reserved_01 (4U)
 *   LevelAdjustmentAxles_Reserved_02 (5U)
 *   LevelAdjustmentAxles_Error (6U)
 *   LevelAdjustmentAxles_NotAvailable (7U)
 * LevelAdjustmentStroke_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelAdjustmentStroke_DriveStroke (0U)
 *   LevelAdjustmentStroke_DockingStroke (1U)
 *   LevelAdjustmentStroke_Reserved (2U)
 *   LevelAdjustmentStroke_Reserved_01 (3U)
 *   LevelAdjustmentStroke_Reserved_02 (4U)
 *   LevelAdjustmentStroke_Reserved_03 (5U)
 *   LevelAdjustmentStroke_Error (6U)
 *   LevelAdjustmentStroke_NotAvailable (7U)
 * LevelChangeRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelChangeRequest_TakeNoAction (0U)
 *   LevelChangeRequest_VehicleBodyUpLifting (1U)
 *   LevelChangeRequest_VehicleBodyDownLowering (2U)
 *   LevelChangeRequest_VehicleBodyUpMinimumMovementLifting (3U)
 *   LevelChangeRequest_VehicleBodyDownMinimumMovementLowering (4U)
 *   LevelChangeRequest_NotDefined (5U)
 *   LevelChangeRequest_ErrorIndicator (6U)
 *   LevelChangeRequest_NotAvailable (7U)
 * LevelStrokeRequest_T: Enumeration of integer in interval [0...3] with enumerators
 *   LevelStrokeRequest_DockingLevelControlStroke (0U)
 *   LevelStrokeRequest_DriveLevelControlStroke (1U)
 *   LevelStrokeRequest_ErrorIndicator (2U)
 *   LevelStrokeRequest_NotAvailable (3U)
 * LevelUserMemoryAction_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelUserMemoryAction_Inactive (0U)
 *   LevelUserMemoryAction_Recall (1U)
 *   LevelUserMemoryAction_Store (2U)
 *   LevelUserMemoryAction_Default (3U)
 *   LevelUserMemoryAction_Reserved (4U)
 *   LevelUserMemoryAction_Reserved_01 (5U)
 *   LevelUserMemoryAction_Error (6U)
 *   LevelUserMemoryAction_NotAvailable (7U)
 * LevelUserMemory_T: Enumeration of integer in interval [0...15] with enumerators
 *   LevelUserMemory_NotUsed (0U)
 *   LevelUserMemory_M1 (1U)
 *   LevelUserMemory_M2 (2U)
 *   LevelUserMemory_M3 (3U)
 *   LevelUserMemory_M4 (4U)
 *   LevelUserMemory_M5 (5U)
 *   LevelUserMemory_M6 (6U)
 *   LevelUserMemory_M7 (7U)
 *   LevelUserMemory_Spare (8U)
 *   LevelUserMemory_Spare01 (9U)
 *   LevelUserMemory_Spare02 (10U)
 *   LevelUserMemory_Spare03 (11U)
 *   LevelUserMemory_Spare04 (12U)
 *   LevelUserMemory_Spare05 (13U)
 *   LevelUserMemory_Error (14U)
 *   LevelUserMemory_NotAvailable (15U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * RampLevelRequest_T: Enumeration of integer in interval [0...15] with enumerators
 *   RampLevelRequest_TakeNoAction (0U)
 *   RampLevelRequest_RampLevelM1 (1U)
 *   RampLevelRequest_RampLevelM2 (2U)
 *   RampLevelRequest_RampLevelM3 (3U)
 *   RampLevelRequest_RampLevelM4 (4U)
 *   RampLevelRequest_RampLevelM5 (5U)
 *   RampLevelRequest_RampLevelM6 (6U)
 *   RampLevelRequest_RampLevelM7 (7U)
 *   RampLevelRequest_NotDefined_04 (8U)
 *   RampLevelRequest_NotDefined_05 (9U)
 *   RampLevelRequest_NotDefined_06 (10U)
 *   RampLevelRequest_NotDefined_07 (11U)
 *   RampLevelRequest_NotDefined_08 (12U)
 *   RampLevelRequest_NotDefined_09 (13U)
 *   RampLevelRequest_ErrorIndicator (14U)
 *   RampLevelRequest_NotAvailable (15U)
 * Request_T: Enumeration of integer in interval [0...3] with enumerators
 *   Request_NotRequested (0U)
 *   Request_RequestActive (1U)
 *   Request_Error (2U)
 *   Request_NotAvailable (3U)
 * RideHeightFunction_T: Enumeration of integer in interval [0...15] with enumerators
 *   RideHeightFunction_Inactive (0U)
 *   RideHeightFunction_StandardDrivePosition (1U)
 *   RideHeightFunction_AlternativeDrivePosition (2U)
 *   RideHeightFunction_SpeedDependentDrivePosition (3U)
 *   RideHeightFunction_HighDrivePosition (4U)
 *   RideHeightFunction_LowDrivePosition (5U)
 *   RideHeightFunction_AlternativeDrivePosition1 (6U)
 *   RideHeightFunction_AlternativeDrivePosition2 (7U)
 *   RideHeightFunction_NotDefined (8U)
 *   RideHeightFunction_NotDefined_01 (9U)
 *   RideHeightFunction_NotDefined_02 (10U)
 *   RideHeightFunction_NotDefined_03 (11U)
 *   RideHeightFunction_NotDefined_04 (12U)
 *   RideHeightFunction_NotDefined_05 (13U)
 *   RideHeightFunction_ErrorIndicator (14U)
 *   RideHeightFunction_NotAvailable (15U)
 * RideHeightStorageRequest_T: Enumeration of integer in interval [0...15] with enumerators
 *   RideHeightStorageRequest_TakeNoAction (0U)
 *   RideHeightStorageRequest_ResetToStoreFactoryDrivePosition (1U)
 *   RideHeightStorageRequest_StoreStandardUserDrivePosition (2U)
 *   RideHeightStorageRequest_StoreLowDrivePosition (3U)
 *   RideHeightStorageRequest_StoreHighDrivePosition (4U)
 *   RideHeightStorageRequest_ResetLowDrivePosition (5U)
 *   RideHeightStorageRequest_ResetHighDrivePosition (6U)
 *   RideHeightStorageRequest_NotDefined (7U)
 *   RideHeightStorageRequest_NotDefined_01 (8U)
 *   RideHeightStorageRequest_NotDefined_02 (9U)
 *   RideHeightStorageRequest_NotDefined_03 (10U)
 *   RideHeightStorageRequest_NotDefined_04 (11U)
 *   RideHeightStorageRequest_NotDefined_05 (12U)
 *   RideHeightStorageRequest_NotDefined_06 (13U)
 *   RideHeightStorageRequest_ErrorIndicator (14U)
 *   RideHeightStorageRequest_NotAvailable (15U)
 * RollRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   RollRequest_Idle (0U)
 *   RollRequest_Clockwise (1U)
 *   RollRequest_CounterClockwise (2U)
 *   RollRequest_ClockwiseShort (3U)
 *   RollRequest_CounterClockwiseShort (4U)
 *   RollRequest_Reserved (5U)
 *   RollRequest_Error (6U)
 *   RollRequest_NotAvailable (7U)
 * StopLevelChangeStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   StopLevelChangeStatus_NoStopRequest (0U)
 *   StopLevelChangeStatus_LevelChangedStopped (1U)
 *   StopLevelChangeStatus_ErrorIndicator (2U)
 *   StopLevelChangeStatus_NotAvailable (3U)
 * StorageAck_T: Enumeration of integer in interval [0...3] with enumerators
 *   StorageAck_TakeNoAction (0U)
 *   StorageAck_ChangeAcknowledged (1U)
 *   StorageAck_Error (2U)
 *   StorageAck_NotAvaiable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 * WiredLevelUserMemory_T: Enumeration of integer in interval [0...15] with enumerators
 *   WiredLevelUserMemory_MemOff (0U)
 *   WiredLevelUserMemory_M1 (1U)
 *   WiredLevelUserMemory_M2 (2U)
 *   WiredLevelUserMemory_M3 (3U)
 *   WiredLevelUserMemory_M4 (4U)
 *   WiredLevelUserMemory_M5 (5U)
 *   WiredLevelUserMemory_M6 (6U)
 *   WiredLevelUserMemory_M7 (7U)
 *   WiredLevelUserMemory_Spare (8U)
 *   WiredLevelUserMemory_Spare01 (9U)
 *   WiredLevelUserMemory_Spare02 (10U)
 *   WiredLevelUserMemory_Spare03 (11U)
 *   WiredLevelUserMemory_Spare04 (12U)
 *   WiredLevelUserMemory_Spare05 (13U)
 *   WiredLevelUserMemory_Error (14U)
 *   WiredLevelUserMemory_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 *
 * Record Types:
 * =============
 * FPBRMMIStat_T: Record with elements
 *   FPBRStatusInd_RE of type FPBRStatusInd_T
 *   FPBRChangeAck_RE of type Ack2Bit_T
 * LevelRequest_T: Record with elements
 *   FrontAxle_RE of type LevelChangeRequest_T
 *   RearAxle_RE of type LevelChangeRequest_T
 *   RollRequest_RE of type RollRequest_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_ECSStandbyActivationTimeout_P1CUA_T Rte_Prm_P1CUA_ECSStandbyActivationTimeout_v(void)
 *   SEWS_ECSStandbyExtendedActTimeout_P1CUB_T Rte_Prm_P1CUB_ECSStandbyExtendedActTimeout_v(void)
 *   SEWS_ECSActiveStateTimeout_P1CUE_T Rte_Prm_P1CUE_ECSActiveStateTimeout_v(void)
 *   SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T Rte_Prm_P1CUF_LoadingLevelSwStuckedTimeout_v(void)
 *   SEWS_KneelButtonStuckedTimeout_P1DWD_T Rte_Prm_P1DWD_KneelButtonStuckedTimeout_v(void)
 *   SEWS_FerryFuncSwStuckedTimeout_P1EXK_T Rte_Prm_P1EXK_FerryFuncSwStuckedTimeout_v(void)
 *   SEWS_ECS_StandbyBlinkTime_P1GCL_T Rte_Prm_P1GCL_ECS_StandbyBlinkTime_v(void)
 *   SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T Rte_Prm_P1IZ2_LoadingLevelAdjSwStuckTimeout_v(void)
 *   SEWS_FrontSuspensionType_P1JBR_T Rte_Prm_P1JBR_FrontSuspensionType_v(void)
 *   SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T Rte_Prm_P1LXQ_FPBRSwitchStuckedTimeout_v(void)
 *   SEWS_FPBRSwitchRequestACKTime_P1LXR_T Rte_Prm_P1LXR_FPBRSwitchRequestACKTime_v(void)
 *   boolean Rte_Prm_P1A12_ADL_Sw_v(void)
 *   boolean Rte_Prm_P1ALT_ECS_PartialAirSystem_v(void)
 *   boolean Rte_Prm_P1ALU_ECS_FullAirSystem_v(void)
 *   boolean Rte_Prm_P1B9X_WirelessRC_Enable_v(void)
 *   boolean Rte_Prm_P1CT4_FerrySw_Installed_v(void)
 *   boolean Rte_Prm_P1CT9_LoadingLevelSw_Installed_v(void)
 *   boolean Rte_Prm_P1EXH_KneelingSwitchInstalled_v(void)
 *   boolean Rte_Prm_P1IZ1_LoadingLevelAdjSwitchInstalled_v(void)
 *   boolean Rte_Prm_P1LXP_FPBRSwitchInstalled_v(void)
 *
 *********************************************************************************************************************/

#define LevelControl_HMICtrl_START_SEC_CODE
#include "LevelControl_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_FrontSuspensionType                (Rte_Prm_P1JBR_FrontSuspensionType_v())
#define PCODE_ADL_Sw                             (Rte_Prm_P1A12_ADL_Sw_v())
#define PCODE_FerrySw_Installed                  (Rte_Prm_P1CT4_FerrySw_Installed_v())
#define PCODE_LoadingLevelSw_Installed           (Rte_Prm_P1CT9_LoadingLevelSw_Installed_v())
#define PCODE_KneelingSwitchInstalled            (Rte_Prm_P1EXH_KneelingSwitchInstalled_v())
#define PCODE_LoadingLevelAdjSwitchInstalled     (Rte_Prm_P1IZ1_LoadingLevelAdjSwitchInstalled_v())
#define PCODE_FPBRSwitchInstalled                (Rte_Prm_P1LXP_FPBRSwitchInstalled_v())

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint32 Rte_IrvRead_DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData_IRV_ECS_StandbyTimer(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1DVZ_Data_P1DVZ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, LevelControl_HMICtrl_CODE) DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData(P2VAR(uint8, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData (returns application error)
 *********************************************************************************************************************/
  uint32 ECS_StandByTimerValue = 0U;
  uint16 ECS_TimerValue        = 0U;
  ECS_StandByTimerValue        = Rte_IrvRead_DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData_IRV_ECS_StandbyTimer();
  ECS_TimerValue               = (ECS_StandByTimerValue/50U);  //convertion from ms to seconds
  Data[0]                      = (uint8)((ECS_TimerValue&0xff00)>>8U);
  Data[1]                      = (uint8)(ECS_TimerValue & 0xff);
  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: LevelControl_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AlternativeDriveLevelSw_stat_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_BackToDriveReqACK_BackToDriveReqACK(BackToDriveReqACK_T *data)
 *   Std_ReturnType Rte_Read_ChangeKneelACK_ChangeKneelACK(ChangeKneelACK_T *data)
 *   Std_ReturnType Rte_Read_ECSStandByReqRCECS_ECSStandByReqRCECS(ECSStandByReq_T *data)
 *   Std_ReturnType Rte_Read_ECSStandByReqWRC_ECSStandByReqWRC(ECSStandByReq_T *data)
 *   Std_ReturnType Rte_Read_ECSStandbyAllowed_ECSStandbyAllowed(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_ElectricalLoadReduction_rqst_ElectricalLoadReduction_rqst(ElectricalLoadReduction_rqst_T *data)
 *   Std_ReturnType Rte_Read_FPBRMMIStat_FPBRMMIStat(FPBRMMIStat_T *data)
 *   Std_ReturnType Rte_Read_FPBRSwitchStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_FerryFunctionStatus_FerryFunctionStatus(FerryFunctionStatus_T *data)
 *   Std_ReturnType Rte_Read_FerryFunctionSwitchChangeACK_FerryFunctionSwitchChangeACK(Ack2Bit_T *data)
 *   Std_ReturnType Rte_Read_FerryFunctionSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_KneelSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_KneelingStatusHMI_KneelingStatusHMI(KneelingStatusHMI_T *data)
 *   Std_ReturnType Rte_Read_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LoadingLevelSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_RampLevelRequestACK_RampLevelRequestACK(ChangeRequest2Bit_T *data)
 *   Std_ReturnType Rte_Read_RampLevelStorageAck_RampLevelStorageAck(StorageAck_T *data)
 *   Std_ReturnType Rte_Read_RideHeightStorageAck_RideHeightStorageAck(StorageAck_T *data)
 *   Std_ReturnType Rte_Read_StopLevelChangeAck_StopLevelChangeStatus(StopLevelChangeStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *   Std_ReturnType Rte_Read_WRCAirSuspensionStopRequest_AirSuspensionStopRequest(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_WRCLevelAdjustmentAction_LevelAdjustmentAction(LevelAdjustmentAction_T *data)
 *   Std_ReturnType Rte_Read_WRCLevelAdjustmentAxles_LevelAdjustmentAxles(LevelAdjustmentAxles_T *data)
 *   Std_ReturnType Rte_Read_WRCLevelAdjustmentStroke_LevelAdjustmentStroke(LevelAdjustmentStroke_T *data)
 *   Std_ReturnType Rte_Read_WRCLevelUserMemory_LevelUserMemory(LevelUserMemory_T *data)
 *   Std_ReturnType Rte_Read_WRCLevelUserMemoryAction_LevelUserMemoryAction(LevelUserMemoryAction_T *data)
 *   Std_ReturnType Rte_Read_WRCRollRequest_WRCRollRequest(RollRequest_T *data)
 *   Std_ReturnType Rte_Read_WiredAirSuspensionStopRequest_AirSuspensionStopRequest(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_WiredLevelAdjustmentAction_LevelAdjustmentAction(LevelAdjustmentAction_T *data)
 *   Std_ReturnType Rte_Read_WiredLevelAdjustmentAxles_LevelAdjustmentAxles(LevelAdjustmentAxles_T *data)
 *   Std_ReturnType Rte_Read_WiredLevelAdjustmentStroke_LevelAdjustmentStroke(LevelAdjustmentStroke_T *data)
 *   Std_ReturnType Rte_Read_WiredLevelUserMemory_WiredLevelUserMemory(WiredLevelUserMemory_T *data)
 *   Std_ReturnType Rte_Read_WiredLevelUserMemoryAction_LevelUserMemoryAction(LevelUserMemoryAction_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_BackToDriveReq_BackToDriveReq(BackToDriveReq_T data)
 *   Std_ReturnType Rte_Write_BlinkECSWiredLEDs_BlinkECSWiredLEDs(FalseTrue_T data)
 *   Std_ReturnType Rte_Write_ECSStandByRequest_ECSStandByRequest(ECSStandByRequest_T data)
 *   Std_ReturnType Rte_Write_ECSStandbyActive_ECSStandbyActive(FalseTrue_T data)
 *   Std_ReturnType Rte_Write_FPBRChangeReq_FPBRChangeReq(FPBRChangeReq_T data)
 *   Std_ReturnType Rte_Write_FPBR_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_FerryFunctionRequest_FerryFunctionRequest(Request_T data)
 *   Std_ReturnType Rte_Write_FerryFunctionSwitchChangeReq_FerryFunctionSwitchChangeReq(ChangeRequest2Bit_T data)
 *   Std_ReturnType Rte_Write_FerryFunction_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_InhibitWRCECSMenuCmd_InhibitWRCECSMenuCmd(InactiveActive_T data)
 *   Std_ReturnType Rte_Write_KneelDeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_KneelingChangeRequest_KneelingChangeRequest(KneelingChangeRequest_T data)
 *   Std_ReturnType Rte_Write_LevelRequest_LevelRequest(const LevelRequest_T *data)
 *   Std_ReturnType Rte_Write_LevelStrokeRequest_LevelStrokeRequest(LevelStrokeRequest_T data)
 *   Std_ReturnType Rte_Write_RampLevelRequest_RampLevelRequest(RampLevelRequest_T data)
 *   Std_ReturnType Rte_Write_RampLevelStorageRequest_RampLevelStorageRequest(RampLevelRequest_T data)
 *   Std_ReturnType Rte_Write_RideHeightFunctionRequest_RideHeightFunctionRequest(RideHeightFunction_T data)
 *   Std_ReturnType Rte_Write_RideHeightStorageRequest_RideHeightStorageRequest(RideHeightStorageRequest_T data)
 *   Std_ReturnType Rte_Write_StopLevelChangeRequest_StopLevelChangeRequest(Request_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_LevelControl_HMICtrl_20ms_runnable_IRV_ECS_StandbyTimer(uint32 data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BUO_63_LoadingLevelSw_stuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1CXA_63_FerryFlexibleSwitchStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1CXB_63_KneelingFlexibleSwitchStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1CXC_63_LoadingLevelAdjSwitchStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1DOO_63_FPBRSwitchStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ECSStandByActive_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ECSStandByActive_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: LevelControl_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution runnable logic for the LevelControl_HMICtrl_20ms_runnable
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LevelControl_HMICtrl_CODE) LevelControl_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: LevelControl_HMICtrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input data structures
   static LevelControl_Switches_StructType_T         RteInData_Switches;
   static LevelControl_CommonInput_StructType_T      RteInData_Common;
   static LevelControl_In_CntrlBox_StructType_T      RteInData_Wired;
   static LevelControl_In_CntrlBox_StructType_T      RteInData_WRC;
   static LevelControl_CBCommon_InputStructType_T    RteInData_CBCommon;
   static LevelControl_Out_StructType_T              RteOutData_Common = { RampLevelRequest_NotAvailable,
                                                                           RampLevelRequest_NotAvailable,
                                                                           Request_NotAvailable,
                                                                           Requested_NotAvailable,
                                                                           RideHeightStorageRequest_NotAvailable,
                                                                           LevelStrokeRequest_NotAvailable,
                                                                           Request_NotAvailable,
                                                                           RideHeightFunction_NotAvailable,
                                                                           ChangeRequest2Bit_NotAvailable,
                                                                           InactiveActive_NotAvailable,
                                                                           DeviceIndication_SpareValue,
                                                                           KneelingChangeRequest_NotAvailable,
                                                                           DeviceIndication_SpareValue,
                                                                           FPBRChangeReq_NotAvailable,
                                                                           DeviceIndication_Off,
                                                                           { LevelChangeRequest_NotAvailable,
                                                                             LevelChangeRequest_NotAvailable,
                                                                             RollRequest_NotAvailable },
                                                                           ECSStandByRequest_NotAvailable,
                                                                           FalseTrue_NotAvaiable }; 
   // Local variables
   static state_StructType_T                  state_Type;
   static ECS_StandbyFSM                      ECS_StandbyFSM_State;
   static StateMachine_StrctType_T            StateMachine_Type;  
   static switch_sm_states                    RecallRamp_switch           = SM_Ramplevel_NoAction;
   static Lvlcntrl_SM_States                  ControlBoxRampLevel_State   = SM_Ramp_NoAction;
   static uint16        Timers[CONST_NbOfTimers];
   static uint32        TimersTT[CONST_NbofTimersTT];

   //! ###### Process RTE read input data common logic : 'Get_RteInDataRead_Common()'
   Get_RteInDataRead_Common(&RteInData_Common,
                            &RteInData_CBCommon,
							&RteInData_Switches);
   //! ###### Check for 'SwcActivation_Living' port is equal to 'OperationalEntry'
   //! ##### Processing height adjustment allowed logic
   if ((OperationalEntry == RteInData_Common.SwcActivation_Living)
	  || ((FalseTrue_False == RteInData_Common.HeightAdjustmentAllowed.Previous)
      && (FalseTrue_True == RteInData_Common.HeightAdjustmentAllowed.Current)))
   {
      //! ##### Process local init logic to re-initialize the output ports : 'LevelControl_HMICtrl_Loc_Init()'
      LevelControl_HMICtrl_Loc_Init(&RteInData_Wired.AirSuspensionStopRequest,
                                    &RteInData_WRC.AirSuspensionStopRequest,
                                    &RteOutData_Common,
                                    &state_Type,
                                    &StateMachine_Type,
                                    Timers);
   }
   //! ###### Check for 'SwcActivation_Living' port is 'Operational'
   else if (Operational == RteInData_Common.SwcActivation_Living)
   {
	   //! #### Process logic for switch varient: 'ControlLogicForSwitches()' 
	   ControlLogicForSwitches(&RteInData_Switches,
	                           &RteInData_CBCommon.RampLevelRequestACK,
							   &StateMachine_Type,
							   &RteOutData_Common,
							   Timers,
							   &ControlBoxRampLevel_State,
							   &RecallRamp_switch);
		//! #### Process ECS standby logic: 'ECS_StandByLogic()'
		ECS_StandByLogic(&RteInData_Common.VehicleMode,
                         &ECS_StandbyFSM_State,
                         &RteInData_Common.ECSStandByReqRCECS,
                         &RteInData_Common.ECSStandByReqWRC,
                         &RteInData_Common.ECSStandbyAllowed, 
                         &StateMachine_Type.is_active_ECS,
                         &TimersTT[CONST_ECSTimerTT],
                         &Timers[CONST_ECSBlinkTime],
						 &RteOutData_Common.ECSStandByRequest,
                         &RteOutData_Common.BlinkECSWiredLEDs);
		Rte_IrvWrite_LevelControl_HMICtrl_20ms_runnable_IRV_ECS_StandbyTimer(TimersTT[0]);
      //! ###### Processing varient selection for wired or wireless(WRC) from control box
      //! ###### Processing wired control box logic
      //! ##### Check for 'P1ALT_ECS_PartialAirSystem' or 'P1ALU_ECS_FullAirSystem' status
      if ((TRUE == PCODE_ECS_PartialAirSystem)
         || (TRUE == PCODE_ECS_FullAirSystem))
      {
         //! #### Process RTE input data read logic for wired: 'Get_RteInDataRead_Wired()' 
         Get_RteInDataRead_Wired(&RteInData_Wired);
		 //! #### Process control box commoon logic for wired varient: 'ControlBoxCommonLogic()' 
		 ControlBoxCommonLogic(&RteInData_Wired,
		                       &RteInData_CBCommon,
							   &state_Type,
							   Timers,
							   &RteOutData_Common,
							   &RecallRamp_switch,
							   &ControlBoxRampLevel_State);
      }
      //! ###### Processing WRC control logic
      //! ##### Check for 'P1B9X_WirelessRC_Enable' status
      else if (TRUE == PCODE_WirelessRC_Enable)
      {
		  RollRequest_T  WRCRollRequest;
          //! #### Process Rte InData read logic for WRC : 'Get_RteInDataRead_WRC()'
          Get_RteInDataRead_WRC(&RteInData_WRC,
		                        &WRCRollRequest);
		  //! #### Process control box commoon logic for WRC varient: 'ControlBoxCommonLogic()'
		  ControlBoxCommonLogic(&RteInData_WRC,
		                        &RteInData_CBCommon,
							    &state_Type,
							    Timers,
							    &RteOutData_Common,
							    &RecallRamp_switch,
							    &ControlBoxRampLevel_State);    
          //! #### Process Roll request logic for WRC 
		  //! ##### Check for WRC RollRequest status
	      if ((RollRequest_Error == WRCRollRequest)
		  || (RollRequest_NotAvailable == WRCRollRequest))
	      {
		     RteOutData_Common.LevelRequest.RollRequest_RE = RollRequest_Idle;
	      }
	      else
	      {
		     RteOutData_Common.LevelRequest.RollRequest_RE = WRCRollRequest;
	      }
      }
      else
      {
         // Do nothing: ignore value, varient selection is nothing
      }
   }
   //! ###### Check for 'SwcActivation_Living' port is not equal to 'Operational' or 'OperationalEntry'
   else
   {
      //! ##### Write all the output ports to NotAvailable
      RteOutData_Common.StopLevelChangeRequest      = Request_NotAvailable;
      RteOutData_Common.RampLevelStorageRequest     = RampLevelRequest_NotAvailable;
      RteOutData_Common.RampLevelRequest            = RampLevelRequest_NotAvailable;
      RteOutData_Common.FerryFunctionRequest        = Request_NotAvailable;
      RteOutData_Common.BackToDriveReq              = Requested_NotAvailable;
      RteOutData_Common.RideHeightStorageRequest    = RideHeightStorageRequest_NotAvailable;
      RteOutData_Common.RideHeightFunctionRequest   = RideHeightFunction_NotAvailable;
      RteOutData_Common.LevelStrokeRequest          = LevelStrokeRequest_NotAvailable;
      RteOutData_Common.LevelRequest.FrontAxle_RE   = LevelChangeRequest_NotAvailable;      
      RteOutData_Common.LevelRequest.RearAxle_RE    = LevelChangeRequest_NotAvailable;   
      RteOutData_Common.LevelRequest.RollRequest_RE = RollRequest_NotAvailable;
   }
   //! ###### Timer decrement logic : 'TimerFunction_Tick()'
   TimerFunction_Tick(CONST_NbOfTimers,
                      Timers);
   //! ###### Timer decrement logic : 'TimerFunction_Tick_ThirtyTwo()'
   TimerFunction_Tick_ThirtyTwo(CONST_NbofTimersTT,
                                TimersTT);
   //! ###### Process RTE InData write common logic : 'RteInDataWrite_Common()'
   RteInDataWrite_Common(&RteOutData_Common);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: LevelControl_HMICtrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: LevelControl_HMICtrl_Init_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution runnable logic for the LevelControl_HMICtrl_Init
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LevelControl_HMICtrl_CODE) LevelControl_HMICtrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: LevelControl_HMICtrl_Init
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

#define LevelControl_HMICtrl_STOP_SEC_CODE
#include "LevelControl_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#define LevelControl_HMICtrl_START_SEC_CODE
#include "LevelControl_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

//!======================================================================================
//!
//! \brief
//! This function is processing the Get_RteInDataRead_Common logic
//!
//! \param   *pLevelControl_InDataCommon   Examine and update the input signals based on RTE failure events
//! \param   *pRteInData_CBCommon          Examine and update the input signals based on RTE failure events
//! \param   *pRteInData_Switches          Examine and update the input signals based on RTE failure events
//!
//!======================================================================================
static void Get_RteInDataRead_Common(LevelControl_CommonInput_StructType_T     *pLevelControl_InDataCommon,
                                     LevelControl_CBCommon_InputStructType_T   *pRteInData_CBCommon,
									 LevelControl_Switches_StructType_T        *pRteInData_Switches)
{
   //! ###### Processing RTE In data common read logic
   Std_ReturnType retvalue = RTE_E_INVALID;

   //! ##### Read SwcActivation_Living port interface
   retvalue = Rte_Read_SwcActivation_Living_Living(&pLevelControl_InDataCommon->SwcActivation_Living);
   MACRO_StdRteRead_IntRPort((retvalue),(pLevelControl_InDataCommon->SwcActivation_Living),(NonOperational))
   //! ##### Read HeightAdjustmentAllowed interface
   pLevelControl_InDataCommon->HeightAdjustmentAllowed.Previous = pLevelControl_InDataCommon->HeightAdjustmentAllowed.Current;
   retvalue = Rte_Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed(&pLevelControl_InDataCommon->HeightAdjustmentAllowed.Current);
   MACRO_StdRteRead_ExtRPort((retvalue),(pLevelControl_InDataCommon->HeightAdjustmentAllowed.Current),(FalseTrue_NotAvaiable),(FalseTrue_Error))
   //! ##### Read RampLevelStorageAck interface
   retvalue = Rte_Read_RampLevelStorageAck_RampLevelStorageAck(&pRteInData_CBCommon->RampLevelStorageAck);
   MACRO_StdRteRead_ExtRPort((retvalue),(pRteInData_CBCommon->RampLevelStorageAck),(StorageAck_NotAvaiable),(StorageAck_Error))
   //! ##### Read RampLevelRequestACK interface
   retvalue = Rte_Read_RampLevelRequestACK_RampLevelRequestACK(&pRteInData_CBCommon->RampLevelRequestACK);
   MACRO_StdRteRead_ExtRPort((retvalue),(pRteInData_CBCommon->RampLevelRequestACK),(ChangeRequest2Bit_NotAvailable),(ChangeRequest2Bit_Error))
   //! ##### Read BackToDriveReqACK interface
   retvalue = Rte_Read_BackToDriveReqACK_BackToDriveReqACK(&pRteInData_CBCommon->BackToDriveReqACK);
   MACRO_StdRteRead_ExtRPort((retvalue),(pRteInData_CBCommon->BackToDriveReqACK),(BackToDriveReqACK_NotAvailable),(BackToDriveReqACK_Error))
   //! ##### Read StopLevelChangeAck interface
   retvalue = Rte_Read_StopLevelChangeAck_StopLevelChangeStatus(&pRteInData_CBCommon->StopLevelChangeAck);
   MACRO_StdRteRead_ExtRPort((retvalue),(pRteInData_CBCommon->StopLevelChangeAck),(StopLevelChangeStatus_NotAvailable),(StopLevelChangeStatus_ErrorIndicator))
   //! ##### Read RideHeightStorageAck interface
   retvalue = Rte_Read_RideHeightStorageAck_RideHeightStorageAck(&pRteInData_CBCommon->RideHeightStorageAck);
   MACRO_StdRteRead_ExtRPort((retvalue),(pRteInData_CBCommon->RideHeightStorageAck),(StorageAck_NotAvaiable),(StorageAck_Error))
   //! ##### Read FerryFunctionSwitchStatus interface
   retvalue = Rte_Read_FerryFunctionSwitchStatus_A2PosSwitchStatus(&pRteInData_Switches->FerryFunctionSwitchStatus);
   MACRO_StdRteRead_IntRPort((retvalue),(pRteInData_Switches->FerryFunctionSwitchStatus),(A2PosSwitchStatus_Error))
   //! ##### Read FerryFunctionSwitchChangeACK interface
   retvalue = Rte_Read_FerryFunctionSwitchChangeACK_FerryFunctionSwitchChangeACK(&pRteInData_Switches->FerryFunctionSwitchChangeACK);
   MACRO_StdRteRead_ExtRPort((retvalue),(pRteInData_Switches->FerryFunctionSwitchChangeACK),(Ack2Bit_NotAvailable),(Ack2Bit_Error))
   //! ##### Read FerryFunctionStatus interface
   retvalue = Rte_Read_FerryFunctionStatus_FerryFunctionStatus(&pRteInData_Switches->FerryFunctionStatus);
   MACRO_StdRteRead_ExtRPort((retvalue),(pRteInData_Switches->FerryFunctionStatus),(FerryFunctionStatus_NotAvailable),(FerryFunctionStatus_Error))
   //! ##### Read KneelingSwitchStatus interface
   pRteInData_Switches->KneelingSwitchStatus.PreviousValue = pRteInData_Switches->KneelingSwitchStatus.CurrentValue;
   retvalue = Rte_Read_KneelSwitchStatus_A2PosSwitchStatus(&pRteInData_Switches->KneelingSwitchStatus.CurrentValue);
   MACRO_StdRteRead_IntRPort((retvalue),(pRteInData_Switches->KneelingSwitchStatus.CurrentValue),(A2PosSwitchStatus_Error))
   //! ##### Read ChangeKneelACK interface
   retvalue = Rte_Read_ChangeKneelACK_ChangeKneelACK(&pRteInData_Switches->ChangeKneelACK);
   MACRO_StdRteRead_ExtRPort((retvalue),(pRteInData_Switches->ChangeKneelACK),(ChangeKneelACK_NotAvailable),(ChangeKneelACK_Error))
   //! ##### Read KneelingStatusHMI interface
   retvalue = Rte_Read_KneelingStatusHMI_KneelingStatusHMI(&pRteInData_Switches->KneelingStatusHMI);
   MACRO_StdRteRead_ExtRPort((retvalue),(pRteInData_Switches->KneelingStatusHMI),(KneelingStatusHMI_NotAvailable),(KneelingStatusHMI_Error))
   //! ##### Read LoadingLevelSwitchStatus interface
   pRteInData_Switches->LoadingLevelSwitchStatus.PreviousValue = pRteInData_Switches->LoadingLevelSwitchStatus.CurrentValue;
   retvalue = Rte_Read_LoadingLevelSwitchStatus_A3PosSwitchStatus(&pRteInData_Switches->LoadingLevelSwitchStatus.CurrentValue);
   MACRO_StdRteRead_IntRPort((retvalue),(pRteInData_Switches->LoadingLevelSwitchStatus.CurrentValue),(A3PosSwitchStatus_Error))
   //! ##### Read LoadingLevelAdjSwitchStatus interface
   pRteInData_Switches->LoadingLevelAdjSwitchStatus.PreviousValue = pRteInData_Switches->LoadingLevelAdjSwitchStatus.CurrentValue;
   retvalue = Rte_Read_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus(&pRteInData_Switches->LoadingLevelAdjSwitchStatus.CurrentValue);
   MACRO_StdRteRead_IntRPort((retvalue),(pRteInData_Switches->LoadingLevelAdjSwitchStatus.CurrentValue),(A3PosSwitchStatus_Error))
   //! ##### Read FPBRSwitchStatus interface
   pRteInData_Switches->FPBRSwitchStatus.PreviousValue = pRteInData_Switches->FPBRSwitchStatus.CurrentValue;
   retvalue = Rte_Read_FPBRSwitchStatus_PushButtonStatus(&pRteInData_Switches->FPBRSwitchStatus.CurrentValue);
   MACRO_StdRteRead_IntRPort((retvalue),(pRteInData_Switches->FPBRSwitchStatus.CurrentValue),(PushButtonStatus_Error))
   //! ##### Read FPBRMMIStat interface
   retvalue = Rte_Read_FPBRMMIStat_FPBRMMIStat(&pRteInData_Switches->FPBRMMIStat);
   MACRO_StdRteRead_IntRPort((retvalue),(pRteInData_Switches->FPBRMMIStat.FPBRChangeAck_RE),(Ack2Bit_Error))
   MACRO_StdRteRead_IntRPort((retvalue),(pRteInData_Switches->FPBRMMIStat.FPBRStatusInd_RE),(FPBRStatusInd_Off))
   //! ##### Read AlternativeDriveLevelSw_stat interface
   pRteInData_Switches->AlternativeDriveLevelSw_stat.PreviousValue = pRteInData_Switches->AlternativeDriveLevelSw_stat.CurrentValue;
   retvalue = Rte_Read_AlternativeDriveLevelSw_stat_A3PosSwitchStatus(&pRteInData_Switches->AlternativeDriveLevelSw_stat.CurrentValue);
   MACRO_StdRteRead_IntRPort((retvalue),(pRteInData_Switches->AlternativeDriveLevelSw_stat.CurrentValue),(A3PosSwitchStatus_Error))
   //! ##### Read ECSStandByReqRCECS interface
   pLevelControl_InDataCommon->ECSStandByReqRCECS.PreviousValue = pLevelControl_InDataCommon->ECSStandByReqRCECS.CurrentValue;
   retvalue = Rte_Read_ECSStandByReqRCECS_ECSStandByReqRCECS(&pLevelControl_InDataCommon->ECSStandByReqRCECS.CurrentValue);
   MACRO_StdRteRead_IntRPort((retvalue),(pLevelControl_InDataCommon->ECSStandByReqRCECS.CurrentValue),(ECSStandByReq_Error))
   //! ##### Read ECSStandByReqWRC interface
   pLevelControl_InDataCommon->ECSStandByReqWRC.PreviousValue = pLevelControl_InDataCommon->ECSStandByReqWRC.CurrentValue;
   retvalue = Rte_Read_ECSStandByReqWRC_ECSStandByReqWRC(&pLevelControl_InDataCommon->ECSStandByReqWRC.CurrentValue);
   MACRO_StdRteRead_ExtRPort((retvalue),(pLevelControl_InDataCommon->ECSStandByReqWRC.CurrentValue),(ECSStandByReq_NotAvailable),(ECSStandByReq_Error))
   //! ##### Read ECSStandbyAllowed interface
   retvalue = Rte_Read_ECSStandbyAllowed_ECSStandbyAllowed(&pLevelControl_InDataCommon->ECSStandbyAllowed);
   MACRO_StdRteRead_ExtRPort((retvalue),(pLevelControl_InDataCommon->ECSStandbyAllowed),(FalseTrue_NotAvaiable),(FalseTrue_Error))
   //! ##### Read VehicleMode interface
   retvalue = Rte_Read_VehicleModeInternal_VehicleMode(&pLevelControl_InDataCommon->VehicleMode);
   MACRO_StdRteRead_IntRPort((retvalue),(pLevelControl_InDataCommon->VehicleMode),(VehicleMode_Error))
}
//!======================================================================================
//!
//! \brief
//! This function is processing the Get_RteInDataRead_Wired logic
//!
//! \param  *pLevelControl_InDataWired    Examine and update the input signals based on RTE failure events
//!
//!======================================================================================
static void Get_RteInDataRead_Wired(LevelControl_In_CntrlBox_StructType_T *pLevelControl_InDataWired)
{
   //! ###### Processing RTE In data read logic for wired
   Std_ReturnType wiredretvalue = RTE_E_INVALID;

   pLevelControl_InDataWired->LevelAdjustmentAction.Previous = pLevelControl_InDataWired->LevelAdjustmentAction.Current;
   //! ##### Read LevelAdjustmentAction interface for wired varient
   wiredretvalue = Rte_Read_WiredLevelAdjustmentAction_LevelAdjustmentAction(&pLevelControl_InDataWired->LevelAdjustmentAction.Current);
   MACRO_StdRteRead_IntRPort((wiredretvalue),(pLevelControl_InDataWired->LevelAdjustmentAction.Current),(LevelAdjustmentAction_Error))

   pLevelControl_InDataWired->LevelUserMemoryAction.Previous = pLevelControl_InDataWired->LevelUserMemoryAction.Current;
   //! ##### Read LevelUserMemoryAction interface for wired varient
   wiredretvalue = Rte_Read_WiredLevelUserMemoryAction_LevelUserMemoryAction(&pLevelControl_InDataWired->LevelUserMemoryAction.Current);
   MACRO_StdRteRead_IntRPort((wiredretvalue),(pLevelControl_InDataWired->LevelUserMemoryAction.Current),(LevelUserMemoryAction_Error))

   pLevelControl_InDataWired->LevelAdjustmentStroke.Previous = pLevelControl_InDataWired->LevelAdjustmentStroke.Current;
   //! ##### Read LevelAdjustmentStroke interface for wired varient
   wiredretvalue = Rte_Read_WiredLevelAdjustmentStroke_LevelAdjustmentStroke(&pLevelControl_InDataWired->LevelAdjustmentStroke.Current);
   MACRO_StdRteRead_IntRPort((wiredretvalue),(pLevelControl_InDataWired->LevelAdjustmentStroke.Current),(LevelAdjustmentStroke_Error))

   //! ##### Read LevelUserMemory interface for wired varient
   wiredretvalue = Rte_Read_WiredLevelUserMemory_WiredLevelUserMemory(&pLevelControl_InDataWired->LevelUserMemory);
   MACRO_StdRteRead_IntRPort((wiredretvalue),(pLevelControl_InDataWired->LevelUserMemory), (LevelUserMemory_Error))

   //! ##### Read LevelAdjustmentAxles interface for wired varient
   wiredretvalue = Rte_Read_WiredLevelAdjustmentAxles_LevelAdjustmentAxles(&pLevelControl_InDataWired->LevelAdjustmentAxles);
   MACRO_StdRteRead_IntRPort((wiredretvalue),(pLevelControl_InDataWired->LevelAdjustmentAxles),(LevelAdjustmentAxles_Error))

   pLevelControl_InDataWired->AirSuspensionStopRequest.Previous = pLevelControl_InDataWired->AirSuspensionStopRequest.Current;
   //! ##### Read AirSuspensionStopRequest interface for wired varient
   wiredretvalue = Rte_Read_WiredAirSuspensionStopRequest_AirSuspensionStopRequest(&pLevelControl_InDataWired->AirSuspensionStopRequest.Current);
   MACRO_StdRteRead_IntRPort((wiredretvalue),(pLevelControl_InDataWired->AirSuspensionStopRequest.Current),(FalseTrue_Error))
}
//!======================================================================================
//!
//! \brief
//! This function is processing the Get_RteInDataRead_WRC logic
//!
//! \param   *pLevelControl_InDataWRC    Examine and update the input signals based on RTE failure events
//! \param   *pWRCRollRequest            Examine and update the WRCRollRequest based on RTE failure events
//!
//!======================================================================================
static void Get_RteInDataRead_WRC(LevelControl_In_CntrlBox_StructType_T *pLevelControl_InDataWRC,
                                  RollRequest_T                         *pWRCRollRequest)
{
   //! ###### Processing RTE In data read WRC logic
   Std_ReturnType wrc_retvalue = RTE_E_INVALID;

   pLevelControl_InDataWRC->AirSuspensionStopRequest.Previous = pLevelControl_InDataWRC->AirSuspensionStopRequest.Current;
   //! ##### Read AirSuspensionStopRequest interface for WRC varient
   wrc_retvalue = Rte_Read_WRCAirSuspensionStopRequest_AirSuspensionStopRequest(&pLevelControl_InDataWRC->AirSuspensionStopRequest.Current);
   MACRO_StdRteRead_ExtRPort((wrc_retvalue),(pLevelControl_InDataWRC->AirSuspensionStopRequest.Current),(FalseTrue_NotAvaiable),(FalseTrue_Error))
  
   pLevelControl_InDataWRC->LevelAdjustmentAction.Previous = pLevelControl_InDataWRC->LevelAdjustmentAction.Current;
   //! ##### Read LevelAdjustmentAction interface for WRC varient
   wrc_retvalue = Rte_Read_WRCLevelAdjustmentAction_LevelAdjustmentAction(&pLevelControl_InDataWRC->LevelAdjustmentAction.Current);
   MACRO_StdRteRead_ExtRPort((wrc_retvalue),(pLevelControl_InDataWRC->LevelAdjustmentAction.Current),(LevelAdjustmentAction_NotAvailable),(LevelAdjustmentAction_Error))
   
   //! ##### Read LevelAdjustmentAxles interface for WRC varient
   wrc_retvalue = Rte_Read_WRCLevelAdjustmentAxles_LevelAdjustmentAxles(&pLevelControl_InDataWRC->LevelAdjustmentAxles);
   MACRO_StdRteRead_ExtRPort((wrc_retvalue),(pLevelControl_InDataWRC->LevelAdjustmentAxles),(LevelAdjustmentAxles_NotAvailable),(LevelAdjustmentAxles_Error))
   
   pLevelControl_InDataWRC->LevelAdjustmentStroke.Previous = pLevelControl_InDataWRC->LevelAdjustmentStroke.Current;
   //! ##### Read LevelAdjustmentStroke interface for WRC varient
   wrc_retvalue = Rte_Read_WRCLevelAdjustmentStroke_LevelAdjustmentStroke(&pLevelControl_InDataWRC->LevelAdjustmentStroke.Current);
   MACRO_StdRteRead_ExtRPort((wrc_retvalue),(pLevelControl_InDataWRC->LevelAdjustmentStroke.Current),(LevelAdjustmentStroke_NotAvailable),(LevelAdjustmentStroke_Error))
   
   //! ##### Read LevelUserMemory interface for WRC varient
   wrc_retvalue = Rte_Read_WRCLevelUserMemory_LevelUserMemory(&pLevelControl_InDataWRC->LevelUserMemory);
   MACRO_StdRteRead_ExtRPort((wrc_retvalue),(pLevelControl_InDataWRC->LevelUserMemory),(LevelUserMemory_NotAvailable),(LevelUserMemory_Error))
   
   pLevelControl_InDataWRC->LevelUserMemoryAction.Previous = pLevelControl_InDataWRC->LevelUserMemoryAction.Current;
   //! ##### Read LevelUserMemoryAction interface for WRC varient
   wrc_retvalue = Rte_Read_WRCLevelUserMemoryAction_LevelUserMemoryAction(&pLevelControl_InDataWRC->LevelUserMemoryAction.Current);
   MACRO_StdRteRead_ExtRPort((wrc_retvalue),(pLevelControl_InDataWRC->LevelUserMemoryAction.Current),(LevelUserMemoryAction_NotAvailable),(LevelUserMemoryAction_Error))
   
   //! ##### Read RollRequest interface for WRC varient
   wrc_retvalue = Rte_Read_WRCRollRequest_WRCRollRequest(pWRCRollRequest);
   MACRO_StdRteRead_ExtRPort((wrc_retvalue),(*pWRCRollRequest),(RollRequest_NotAvailable),(RollRequest_Error))
}
//!======================================================================================
//!
//! \brief
//! This function is processing the RteInDataWrite_Common logic
//!
//! \param   *pLevelControl_OutData   Updating the output signals to the ports of output structure
//!
//!======================================================================================
static void RteInDataWrite_Common(const LevelControl_Out_StructType_T  *pLevelControl_OutData)
{
   //! ###### Processing RTE In data common write logic
   //! ##### Write all the output ports
   (void)Rte_Write_FerryFunctionRequest_FerryFunctionRequest(pLevelControl_OutData->FerryFunctionRequest);
   (void)Rte_Write_RampLevelRequest_RampLevelRequest(pLevelControl_OutData->RampLevelRequest);
   (void)Rte_Write_RampLevelStorageRequest_RampLevelStorageRequest(pLevelControl_OutData->RampLevelStorageRequest);
   (void)Rte_Write_RideHeightFunctionRequest_RideHeightFunctionRequest(pLevelControl_OutData->RideHeightFunctionRequest);
   (void)Rte_Write_RideHeightStorageRequest_RideHeightStorageRequest(pLevelControl_OutData->RideHeightStorageRequest);
   (void)Rte_Write_StopLevelChangeRequest_StopLevelChangeRequest(pLevelControl_OutData->StopLevelChangeRequest);
   (void)Rte_Write_BackToDriveReq_BackToDriveReq(pLevelControl_OutData->BackToDriveReq);
   (void)Rte_Write_LevelRequest_LevelRequest(&pLevelControl_OutData->LevelRequest);
   (void)Rte_Write_LevelStrokeRequest_LevelStrokeRequest(pLevelControl_OutData->LevelStrokeRequest);
   (void)Rte_Write_FerryFunctionSwitchChangeReq_FerryFunctionSwitchChangeReq(pLevelControl_OutData->FerryFunctionSwitchChangeReq);
   (void)Rte_Write_InhibitWRCECSMenuCmd_InhibitWRCECSMenuCmd(pLevelControl_OutData->InhibitWRCECSMenuCmd);
   Rte_Write_FerryFunction_DeviceIndication_DeviceIndication(pLevelControl_OutData->FerryFunction_DeviceIndication);
   Rte_Write_KneelDeviceIndication_DeviceIndication(pLevelControl_OutData->KneelDeviceIndication);
   (void)Rte_Write_KneelingChangeRequest_KneelingChangeRequest(pLevelControl_OutData->KneelingChangeRequest);
   (void)Rte_Write_FPBRChangeReq_FPBRChangeReq(pLevelControl_OutData->FPBRChangeReq);
   Rte_Write_FPBR_DeviceIndication_DeviceIndication(pLevelControl_OutData->FPBR_DeviceIndication);
   (void)Rte_Write_ECSStandByRequest_ECSStandByRequest(pLevelControl_OutData->ECSStandByRequest);
   Rte_Write_BlinkECSWiredLEDs_BlinkECSWiredLEDs(pLevelControl_OutData->BlinkECSWiredLEDs);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the LevelControl_HMICtrl_Loc_Init
//!
//! \param   *pRteInData_Switches         Provides the status of switches inputs
//! \param   *pRampLevelRequestACK        Provides the status of RamplevelrequestAck
//! \param   *pStateMachine_Type          Provides status of current state in statemachine logic
//! \param   *pRteOutData_Common          Updates output structure based on input operation
//! \param   pTimers                      Controls Timer values based on conditions
//! \param   *pControlBoxRampLevel_State  To check priority of Ramp level switch
//! \param   *pRecallRamp_switch          To collect the current state of Ramp level switch
//!
//!======================================================================================
static void ControlLogicForSwitches(      LevelControl_Switches_StructType_T  *pRteInData_Switches,
                                    const ChangeRequest2Bit_T                 *pRampLevelRequestACK,
									      StateMachine_StrctType_T            *pStateMachine_Type,
									      LevelControl_Out_StructType_T       *pRteOutData_Common,
									      uint16                              pTimers[CONST_NbOfTimers],
									const Lvlcntrl_SM_States                  *pControlBoxRampLevel_State,
									      switch_sm_states                    *pRecallRamp_switch)
{
	static FPBR_Switch         FPBR_Switch_State;
	static SM_ADLS             ADLS_State;
	static SM_KneelingSwitch   KneelingSwitch_state;
	static SM_Ferryswitch      Ferryswitch_State;
	static Ramplevel_Switch    Ramplevel_State = { SM_Ramplevel_NoAction,
                                                   SM_Ramplevel_NoAction };
		   uint8 isLoadingLvlAdj_SwitchChangedTo = 0U;
	//! ##### Select 'P1LXP_FPBRSwitchInstalled' parameter
      if (TRUE == PCODE_FPBRSwitchInstalled)
      {
         //! #### Process FPBR switch state transition logic :'FPBR_Switch_StateTransition()'
         FPBR_Switch_StateTransition(&FPBR_Switch_State,
                                     &pRteInData_Switches->FPBRSwitchStatus,
                                     &pRteInData_Switches->FPBRMMIStat,
                                     &pStateMachine_Type->is_active_FPBR,
                                     &pTimers[CONST_FPBRAck],
									 &pRteOutData_Common->FPBR_DeviceIndication,
                                     &pRteOutData_Common->FPBRChangeReq);
         //! #### Process FPBR switch stuck :'FPBR_SwitchstuckProcessing()'  
         FPBR_SwitchstuckProcessing(&pRteInData_Switches->FPBRSwitchStatus,
                                    &pTimers[CONST_FPBRStuck]);
      }
      else
      {
         // Do Nothing: ignore value, keep Previous status
      }
	  //! ##### Check for 'P1A12_ADL_Sw' status 
      if (TRUE == PCODE_ADL_Sw)
      {
         //! #### Process alternative drive level switch state transition logic : 'AltDriveLevelSwitch_Transition()'
         AltDriveLevelSwitch_Transition(&pRteInData_Switches->AlternativeDriveLevelSw_stat,    
                                        &ADLS_State,
                                        &pStateMachine_Type->is_active_ADLS,
										&pRteOutData_Common->RideHeightFunctionRequest);
      }
      else
      {
         pRteOutData_Common->RideHeightFunctionRequest = RideHeightFunction_StandardDrivePosition;
      }
	  //! ##### Select 'P1EXH_KneelingSwitchInstalled' parameter
	  if (TRUE == PCODE_KneelingSwitchInstalled)
      {
         //! #### Process kneeling switch state transition logic : 'KneelingSwitch_StateTransitions()'
         KneelingSwitch_StateTransitions(&pRteInData_Switches->KneelingSwitchStatus,
                                         &pRteInData_Switches->ChangeKneelACK,
                                         &KneelingSwitch_state,
                                         &pStateMachine_Type->is_active_KneelingSwitch,
                                         &pTimers[CONST_KneelTimer],
                                         &pTimers[CONST_KneelACK],
										 &pRteOutData_Common->KneelingChangeRequest);
         //! #### Process kneeling switch device indication logic : 'KneelingSwitch_DeviceIndication()'
         KneelingSwitch_DeviceIndication(&pRteInData_Switches->KneelingStatusHMI,
                                         &pRteOutData_Common->KneelDeviceIndication);
      }
      else
      {
         // Do nothing: ignore value, keep previous status
      }
	  //! ##### Select 'P1CT4_FerrySw_Installed' parameter
	  if (TRUE == PCODE_FerrySw_Installed)
      {
         //! #### Process ferry switch state transition logic : 'Ferryswitch_StateTransitions()'
         Ferryswitch_StateTransitions(&pRteInData_Switches->FerryFunctionSwitchStatus,
                                      &pRteInData_Switches->FerryFunctionSwitchChangeACK,
                                      &Ferryswitch_State,
                                      &pStateMachine_Type->is_active_FerrySwitch,
                                      &pTimers[CONST_SwitchFerryTimer],
                                      &pTimers[CONST_FerryAck],
									  &pRteOutData_Common->FerryFunctionSwitchChangeReq,
                                      &pRteOutData_Common->RampLevelRequest);
         //! #### Process ferry switch device indication logic : 'Ferryfun_Indication()'
         Ferryfun_Indication(&pRteInData_Switches->FerryFunctionStatus,
                             &pRteOutData_Common->FerryFunction_DeviceIndication);
      }
      else
      {
         // Do nothing: ignore value, keep previous status
      }
	  //! ##### Select 'P1CT9_LoadingLevelSw_Installed' parameter
	   if (TRUE == PCODE_LoadingLevelSw_Installed)
      {
         //! #### Check for ControlBoxRampLevel state
         if (*pControlBoxRampLevel_State == SM_Ramp_NoAction)
         {
            //! #### Process ramp level state transition logic : 'RampLevel_StateTransitions()' 
            *pRecallRamp_switch = RampLevel_StateTransitions(&Ramplevel_State,
                                                             &pRteInData_Switches->LoadingLevelSwitchStatus,
                                                             pRampLevelRequestACK,
                                                             &pStateMachine_Type->is_active_Ramplevel,
                                                             &pTimers[CONST_RampLevelTimer],
                                                             &pTimers[CONST_RampLevelAck],
															 &pRteOutData_Common->RampLevelRequest,
                                                             &pRteOutData_Common->LevelStrokeRequest);
         }
         else
         {
            // Do nothing: ignore value, keep previous status
         }
      }
      else
      {
         // Do nothing: ignore value, keep previous status
      }
	  //! ##### Select 'P1IZ1_LoadingLevelAdjSwitchInstalled' parameter
      if (TRUE == PCODE_LoadingLevelAdjSwitchInstalled)
      {
		  //! #### Process loading level adjustment switch logic for full air : 'LoadingLvlAdj_SwitchLogic()'
           isLoadingLvlAdj_SwitchChangedTo = LoadingLvlAdj_SwitchLogic(&pRteOutData_Common->LevelStrokeRequest,
                                                                       &pRteOutData_Common->LevelRequest,
                                                                       &pRteInData_Switches->LoadingLevelAdjSwitchStatus,
                                                                       &pTimers[CONST_LoadingLevelStuckTimer]);
         //! ##### Select 'P1JBR_FrontSuspensionType' parameter
         if (TRUE == PCODE_FrontSuspensionType)
         {
            //! #### Process loading level adjustment switch logic for full air
            if (CONST_SWITCHSTATUSUPPER == isLoadingLvlAdj_SwitchChangedTo)
			{
				pRteOutData_Common->LevelRequest.FrontAxle_RE = LevelChangeRequest_VehicleBodyUpLifting;
                pRteOutData_Common->LevelRequest.RearAxle_RE  = LevelChangeRequest_VehicleBodyUpLifting;
			}
			else if (CONST_SWITCHSTATUSLOWER == isLoadingLvlAdj_SwitchChangedTo)
			{
				pRteOutData_Common->LevelRequest.FrontAxle_RE  = LevelChangeRequest_VehicleBodyDownLowering;
                pRteOutData_Common->LevelRequest.RearAxle_RE   = LevelChangeRequest_VehicleBodyDownLowering;
			}
			else
			{
				//Do:Nothing
			}
         }
         else
         {
            //! #### Process loading level adjustment switch logic for rear air
            if (CONST_SWITCHSTATUSUPPER == isLoadingLvlAdj_SwitchChangedTo)
			{
				pRteOutData_Common->LevelRequest.FrontAxle_RE = LevelChangeRequest_TakeNoAction;
                pRteOutData_Common->LevelRequest.RearAxle_RE  = LevelChangeRequest_VehicleBodyUpLifting;
			}
			else if (CONST_SWITCHSTATUSLOWER == isLoadingLvlAdj_SwitchChangedTo)
			{
				pRteOutData_Common->LevelRequest.FrontAxle_RE  = LevelChangeRequest_TakeNoAction;
                pRteOutData_Common->LevelRequest.RearAxle_RE   = LevelChangeRequest_VehicleBodyDownLowering;
			}
			else
			{
				//Do:Nothing
			}
         }
      }
      else
      {
         // Do nothing: ignore value, keep previous status
      }
	  //! ##### Process inhibit WRC ECS menu cmd logic : 'Output_InhibitWRCECS()'
      Output_InhibitWRCECS(&pRteInData_Switches->LoadingLevelAdjSwitchStatus, 
                           &pRteInData_Switches->LoadingLevelSwitchStatus, 
                           &KneelingSwitch_state, 
                           &Ferryswitch_State, 
                           &Ramplevel_State,
                           &pTimers[CONST_LoadingLevelStuckTimer],
                           &pTimers[CONST_RampLevelTimer],
                           &pTimers[CONST_SwitchFerryTimer],
                           &pTimers[CONST_KneelTimer],
                           &pRteOutData_Common->InhibitWRCECSMenuCmd);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the ControlBoxCommonLogic
//!
//! \param   *pLevelControlCBInput        Provides the status of inputs
//! \param   *pRteInData_CBCommon         Provides information related to input data
//! \param   *pstate_Type                 Controls all state machines state 
//! \param   pTimers[CONST_NbOfTimers]    Controls timer values based on functionality
//! \param   *pRteOutData_Common          Updates output structure based on Input conditions
//! \param   *pRecallRamp_switch          To check priority of Ramp level switch
//! \param   *pControlBoxRampLevel_State  To collect the current state of Ramp level switch
//!
//!======================================================================================
static void ControlBoxCommonLogic(       LevelControl_In_CntrlBox_StructType_T     *pLevelControlCBInput,
                                  const  LevelControl_CBCommon_InputStructType_T    *pRteInData_CBCommon,
								         state_StructType_T                        *pstate_Type,
								         uint16                                    pTimers[CONST_NbOfTimers],
								         LevelControl_Out_StructType_T             *pRteOutData_Common,
								   const switch_sm_states                          *pRecallRamp_switch,
								         Lvlcntrl_SM_States                        *pControlBoxRampLevel_State)
{
	static Lvlcntrl_SM_States   is_StopRequest = SM_StopRequest_NoAction;
	//! #### Process ferry function logic: 'ControlBoxLogicForFerryFunction()'
         ControlBoxLogicForFerryFunction(&pLevelControlCBInput->LevelAdjustmentAction,
                                         &pRteOutData_Common->FerryFunctionRequest,
                                         &pTimers[CONST_FerryTimer]);
         //! #### Process store ramp level logic: 'ControlBoxLogicForStoreRampLevel()'
         ControlBoxLogicForStoreRampLevel(pControlBoxRampLevel_State,
                                          &pLevelControlCBInput->LevelUserMemoryAction,
                                          &pLevelControlCBInput->LevelAdjustmentStroke.Current,
                                          &pRteInData_CBCommon->RampLevelStorageAck,
                                          &pLevelControlCBInput->LevelUserMemory,
                                          &pstate_Type->is_active_StoreRamplevel,
                                          &pRteOutData_Common->RampLevelStorageRequest,
                                          &pTimers[CONST_StoreRecallRampTimer]);
         //! #### Check for recall ramp switch is in No_Action state  
         if (SM_Ramplevel_NoAction == *pRecallRamp_switch)
         {
            //! #### Process recall ramp level logic : 'ControlBoxLogicForRecallRampLevel()'
            ControlBoxLogicForRecallRampLevel(pControlBoxRampLevel_State,
                                              &pLevelControlCBInput->LevelUserMemoryAction,
                                              &pLevelControlCBInput->LevelAdjustmentStroke.Current,
                                              &pLevelControlCBInput->LevelUserMemory,
                                              &pRteInData_CBCommon->RampLevelRequestACK,
                                              &pstate_Type->is_active_RecallRampLevel,
                                              &pRteOutData_Common->RampLevelRequest,
                                              &pTimers[CONST_StoreRecallRampTimer]);
         }
         else
         {
            // Do nothing: ignore value, keep previous status
         }
		 //! #### Process StoreRestoreStandard logic : 'ControlBoxLogicForStoreRestoreStandard()'
        ControlBoxLogicForStoreRestoreStandard(&pLevelControlCBInput->LevelUserMemoryAction,
                                               &pLevelControlCBInput->LevelAdjustmentStroke.Current,
                                               &pRteInData_CBCommon->RideHeightStorageAck,
                                               pControlBoxRampLevel_State,
                                               &pstate_Type->is_active_StoreRestoreStandard,
                                               &pRteOutData_Common->RideHeightStorageRequest,
                                               &pTimers[CONST_StoreRestoreStandard]);
         //! #### Process AdjustLevelMain logic : 'ControlBoxLogicForAdjustLevelMain()'
         ControlBoxLogicForAdjustLevelMain(&pLevelControlCBInput->LevelAdjustmentAction,
                                           &pLevelControlCBInput->LevelAdjustmentAxles,
                                           is_StopRequest,
                                           &pRteOutData_Common->LevelRequest,
                                           &pTimers[CONST_AdjustFront],
                                           &pTimers[CONST_AdjustRear]);
         //! #### Process back to drive level request logic : 'ControlBoxLogicForBackToDriveLevelRequest()'
         ControlBoxLogicForBackToDriveLevelRequest(&pLevelControlCBInput->LevelAdjustmentAction,
                                                   &pRteInData_CBCommon->BackToDriveReqACK,
                                                   &pstate_Type->is_active_BackToDriveLevelReq,
                                                   &pRteOutData_Common->BackToDriveReq,
                                                   &pTimers[CONST_B2D]);
         //! #### Process stop request logic : 'ControlBoxLogicForStopRequest()'
         is_StopRequest = ControlBoxLogicForStopRequest(&pLevelControlCBInput->AirSuspensionStopRequest,
                                                        &pRteInData_CBCommon->StopLevelChangeAck,
                                                        &pstate_Type->is_active_StopRequestLevel,
                                                        &pRteOutData_Common->StopLevelChangeRequest,
                                                        &pRteOutData_Common->LevelRequest,
                                                        &pTimers[CONST_StopReq]);
         //! #### Process adjust stroke request logic : 'ControlBoxLogicForAdjustStrokeRequest()'
         ControlBoxLogicForAdjustStrokeRequest(&pLevelControlCBInput->LevelAdjustmentStroke,
                                               &pRteOutData_Common->LevelStrokeRequest,
                                               &pTimers[CONST_AdjustStroke]);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the ControlBoxLogicForFerryFunction logic
//!
//! \param   *pLvlAdjustmentAction        Providing the status of LevelAdjusmentAction 
//! \param   *pFerryFuncRequest           Update FerryFunctionRequest value as per conditions
//! \param   *pTimers                     To check current timer value and update
//!
//!======================================================================================
static void ControlBoxLogicForFerryFunction(LevelAdjustmentActionType *pLvlAdjustmentAction,
                                            Request_T                 *pFerryFuncRequest,
                                            uint16                    *pTimers)
{
   static uint8 ferryFlag       = CONST_ResetFlag;
   static uint8 ferryinitialize = CONST_NotInitialize;
   
   if (CONST_NotInitialize == ferryinitialize)
   {
      pLvlAdjustmentAction->Previous  = pLvlAdjustmentAction->Current;
      ferryinitialize                 = CONST_Initialize;
   }
   else
   {
      // Do nothing : keep previous value
   }
   //! ##### Examine the changes in LevelAdjustmentAction
   //! #### Update FerryFunctionRequest value
   if ((pLvlAdjustmentAction->Current != pLvlAdjustmentAction->Previous)
      && (LevelAdjustmentAction_Ferry == pLvlAdjustmentAction->Current))
   {
      *pFerryFuncRequest = Request_RequestActive;
      *pTimers           = CONST_FerryFunctionReqTimeout;
      ferryFlag          = CONST_SetFlag;
   }
   else if ((pLvlAdjustmentAction->Current != pLvlAdjustmentAction->Previous)
           && (LevelAdjustmentAction_Ferry == pLvlAdjustmentAction->Previous))
   {
       *pFerryFuncRequest = Request_NotRequested;
       *pTimers           = CONST_TimerFunctionInactive;
       ferryFlag          = CONST_ResetFlag;
   }
   //! ##### Check for time elapsed value
   else
   {
      if ((CONST_TimerFunctionElapsed == *pTimers)
         && (CONST_SetFlag == ferryFlag))
      {
        *pFerryFuncRequest = Request_NotRequested;
      }
      else
      {
         // Do nothing: ignore value, keep the previous status
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the ControlBoxLogicForStoreRampLevel logic
//!
//! \param   *pRampLevelState            Provides and updates the present state based on conditions 
//! \param   *pLvlUserMemoryAction       Provides the status of the LevelUserMemoryAction  
//! \param   *pLvlAdjustmentStroke_cur   Provides the current status of the LevelAdjustmentStroke 
//! \param   *pRampLvlStorageAck         Specifies the acknowledgement status of RampLevelStorage
//! \param   *pLvlUserMemory             Provides the status of LevelUserMemory 
//! \param   *pStoreRampData             Specify and update the current value
//! \param   *pRampLvlStorageRequest     Updating the output signals based on current state
//! \param   *pTimers_StoreRamp          To check current timer value and update
//!
//!======================================================================================
static void ControlBoxLogicForStoreRampLevel(      Lvlcntrl_SM_States         *pRampLevelState,
                                             const LevelUserMemoryActionType  *pLvlUserMemoryAction,
                                             const LevelAdjustmentStroke_T    *pLvlAdjustmentStroke_cur,
                                             const StorageAck_T               *pRampLvlStorageAck,
                                             const LevelUserMemory_T          *pLvlUserMemory,
                                                   uint8                      *pStoreRampData,
                                                   RampLevelRequest_T         *pRampLvlStorageRequest,
                                                   uint16                     *pTimers_StoreRamp)
{
   if (CONST_NotInitialize == *pStoreRampData)
   {
      *pStoreRampData         = CONST_Initialize;
      *pRampLvlStorageRequest = RampLevelRequest_TakeNoAction;
      *pRampLevelState        = SM_Ramp_NoAction;
      *pTimers_StoreRamp      = CONST_TimerFunctionInactive;
   }
   else
   {
      //! ###### Select the state based on 'RampLevelState' value
      switch (*pRampLevelState)
      { 
         //! ##### Check the conditions for state change from 'StoreRampLevel Noaction'
         case SM_Ramp_NoAction:
            //! #### Check the conditions for state change to 'StoreLevelRequest'
            //! #### Check for LevelUserMemoryAction, LevelAdjustmentStroke and LevelUserMemory status
            if ((((LevelUserMemoryAction_Inactive == pLvlUserMemoryAction->Previous)
               || (LevelUserMemoryAction_Recall == pLvlUserMemoryAction->Previous))
               && (LevelUserMemoryAction_Store == pLvlUserMemoryAction->Current))
               && (LevelAdjustmentStroke_DockingStroke == *pLvlAdjustmentStroke_cur)
               && (*pLvlUserMemory >= LevelUserMemory_M1)
               && (*pLvlUserMemory <= LevelUserMemory_M7))
            {
               *pRampLvlStorageRequest = (RampLevelRequest_T)*pLvlUserMemory;
               *pTimers_StoreRamp      = CONST_RampLevelStorageTimeout;
               *pRampLevelState        = SM_StoreRampLevel_StoreLvlReq;
            }
            else
            {
               // Do Nothing : Keep previous value
            }
         break;
         //! ##### Check the conditions for state change from 'Store Level Request'
         case SM_StoreRampLevel_StoreLvlReq:
            //! #### Check the conditions for state change to 'StoreRampLevel_NoAction'
            //! #### Check for RampLevelStorageAck status 
            if (StorageAck_ChangeAcknowledged == *pRampLvlStorageAck)
            {
               *pRampLvlStorageRequest = RampLevelRequest_TakeNoAction;
               *pTimers_StoreRamp      = CONST_TimerFunctionInactive;
               *pRampLevelState        = SM_Ramp_NoAction;
            }
            //! #### Check for time elapse value of StoreRamp timer 
            else
            {
               if (CONST_TimerFunctionElapsed == *pTimers_StoreRamp)
               {
                  *pRampLvlStorageRequest = RampLevelRequest_TakeNoAction;
                  *pRampLevelState        = SM_Ramp_NoAction;
               }
               else
               {
                  // Do nothing: ignore value, keep the previous status
               }
            }
         break;
         default:
            // Do nothing: keep previous state value
         break;
      }
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ControlBoxLogicForRecallRampLevel logic
//!
//! \param   *pRampLevelState          Provides and updates the present state based on conditions 
//! \param   *pLevelUserMemoryAction   Provides the status of the LevelUserMemoryAction 
//! \param   *pLevelAdjustStroke_cur   Provides the current status of the LevelAdjustmentStroke
//! \param   *pLevelUserMemory         Provides the status of LevelUserMemory
//! \param   *pRampLvlRequestACK       Specifies the acknowledgement status of RampLevelStorage
//! \param   *pRecallRampData          Specify and update the current value
//! \param   *pRampLvlRequest          Update the status as per conditions
//! \param   *pTimers_RecallRamp       To check current timer value and update
//!
//!======================================================================================
static void ControlBoxLogicForRecallRampLevel(      Lvlcntrl_SM_States        *pRampLevelState,
                                              const LevelUserMemoryActionType *pLevelUserMemoryAction,
                                              const LevelAdjustmentStroke_T   *pLevelAdjustStroke_cur,
                                              const LevelUserMemory_T         *pLevelUserMemory,
                                              const ChangeRequest2Bit_T       *pRampLvlRequestACK,
                                                    uint8                     *pRecallRampData,
                                                    RampLevelRequest_T        *pRampLvlRequest,
                                                    uint16                    *pTimers_RecallRamp)
{  
   if (CONST_NotInitialize == *pRecallRampData)
   {
      *pRecallRampData = CONST_Initialize;
      *pRampLvlRequest = RampLevelRequest_TakeNoAction;
   }
   else
   {
      //! ###### Select the state based on 'pRampLevelState'
      switch (*pRampLevelState)
      {
         //! ##### Check the conditions for state change from 'RecallRamp Noaction' state
         case SM_Ramp_NoAction:
            //! #### Check the conditions for state change to 'RecallRamp_RampLevelReq'
            //! #### Check for LevelUserMemoryAction,LevelAdjustmentStroke and LevelUserMemory status
            if (((LevelUserMemoryAction_Inactive == pLevelUserMemoryAction->Previous)
               && (LevelUserMemoryAction_Recall == pLevelUserMemoryAction->Current))
               && (LevelAdjustmentStroke_DockingStroke == *pLevelAdjustStroke_cur)
               && (*pLevelUserMemory >= LevelUserMemory_M1)
               && (*pLevelUserMemory <= LevelUserMemory_M7))
            {
               *pRampLvlRequest       = (RampLevelRequest_T)*pLevelUserMemory;
               *pTimers_RecallRamp    = CONST_RampLevelRequestTimeout;
               *pRampLevelState       = SM_RecallRamp_RampLvlRequest;
            }
            else
            {
               // Do Nothing : Keep previous value
            }
         break;
        //! ##### Check the conditions for state change from 'Recall RampLevelRequest' state
         case SM_RecallRamp_RampLvlRequest:
            //! #### Check the conditions for state change to 'RecallRamp_NoAction'
           //! #### Check for RampLevelRequestAck status 
            if (ChangeRequest2Bit_Change == *pRampLvlRequestACK)
            {
               *pRampLvlRequest      = RampLevelRequest_TakeNoAction;
               *pTimers_RecallRamp   = CONST_TimerFunctionInactive;
               *pRampLevelState      = SM_Ramp_NoAction;
            }
            //! #### Check for time elapse value of RecallRamp timer
            // if timer reaches to 500ms
            else
            {
               if (CONST_TimerFunctionElapsed == *pTimers_RecallRamp)
               {
                  *pRampLvlRequest      = RampLevelRequest_TakeNoAction;
                  *pRampLevelState      = SM_Ramp_NoAction;
               }
               else
               {
                  // Do Nothing : Keep previous value
               }
            }
        break;
        default:
            // Do nothing: keep previous state value
        break;
      }
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ControlBoxLogicForStoreRestoreStandard logic
//!
//! \param   *pLevelUserMemoryAction        Provides the status of the LevelUserMemoryAction    
//! \param   *pLevelAdjustStroke_cur        Provides the current status of the LevelAdjustmentStroke 
//! \param   *pRideHeightStoreAck           Specifies the acknowledgement status of RideHeightStorage
//! \param   *pRampLevelState               Provides and updates the present state based on conditions 
//! \param   *pStoreRestoreRampData         Specify and update the current value
//! \param   *pRideHghtStoreRequest         Updating the output signals based on current state
//! \param   *pTimers_StoreRestoreStandard  To check current timer value and update
//!
//!======================================================================================
static void ControlBoxLogicForStoreRestoreStandard(const LevelUserMemoryActionType      *pLevelUserMemoryAction,
                                      const LevelAdjustmentStroke_T    *pLevelAdjustStroke_cur,
                                      const StorageAck_T               *pRideHeightStoreAck,
                                            Lvlcntrl_SM_States         *pRampLevelState,
                                            uint8                      *pStoreRestoreRampData,
                                            RideHeightStorageRequest_T *pRideHghtStoreRequest,
                                            uint16                     *pTimers_StoreRestoreStandard)
{
	if (CONST_NotInitialize == *pStoreRestoreRampData)
	{
		*pStoreRestoreRampData        = CONST_Initialize;
		*pRideHghtStoreRequest        = RideHeightStorageRequest_TakeNoAction;
		*pTimers_StoreRestoreStandard = CONST_TimerFunctionInactive;
	}
	else
	{
		//! ###### Select the state based on 'pRampLevelState'
		switch (*pRampLevelState)
		{
			//! ##### Check the conditions for state change from 'RecallRamp Noaction' state
		case SM_Ramp_NoAction:
			if (((LevelUserMemoryAction_Inactive == pLevelUserMemoryAction->Previous)
				&& (LevelUserMemoryAction_Recall == pLevelUserMemoryAction->Current))
				&& (LevelAdjustmentStroke_DriveStroke == *pLevelAdjustStroke_cur))
			{
				*pTimers_StoreRestoreStandard = CONST_RestoreFactoryTimeout;
				*pRideHghtStoreRequest        = RideHeightStorageRequest_ResetToStoreFactoryDrivePosition;
				*pRampLevelState              = SM_RecallRamp_RestoreFactory;
			}
			else
			{
				//! #### Check the conditions for state change to 'StoreStandard'
				//! #### Check for LevelUserMemoryAction and LevelAdjustmentStroke status
				if ((((LevelUserMemoryAction_Inactive == pLevelUserMemoryAction->Previous)
					|| (LevelUserMemoryAction_Recall == pLevelUserMemoryAction->Previous))
					&& (LevelUserMemoryAction_Store == pLevelUserMemoryAction->Current))
					&& (LevelAdjustmentStroke_DriveStroke == *pLevelAdjustStroke_cur))
				{
					*pTimers_StoreRestoreStandard = CONST_StoreStandardTimeout;
					*pRideHghtStoreRequest        = RideHeightStorageRequest_StoreStandardUserDrivePosition;
					*pRampLevelState              = SM_StoreRamp_StoreStandard;
				}
				else
				{
					// Do Nothing : Keep previous value
				}
			}
		break;
			//! ##### Check the conditions for state change from 'Recall Restore Factory'
		case SM_RecallRamp_RestoreFactory:
			//! #### Check the conditions for state change to 'RecallRamp_NoAction'
			//! #### Check for RideHghtStoreAck status 
			if (StorageAck_ChangeAcknowledged == *pRideHeightStoreAck)
			{
				*pTimers_StoreRestoreStandard = CONST_TimerFunctionInactive;
				*pRideHghtStoreRequest        = RideHeightStorageRequest_TakeNoAction;
				*pRampLevelState              = SM_Ramp_NoAction;
			}
			//! #### Check the conditions for state change to 'Store Standard'
			//! #### Check for 'LevelUserMemoryAction' status
			else if ((((LevelUserMemoryAction_Inactive == pLevelUserMemoryAction->Previous)
				|| (LevelUserMemoryAction_Recall == pLevelUserMemoryAction->Previous))
				&& (LevelUserMemoryAction_Store == pLevelUserMemoryAction->Current))
				&& (LevelAdjustmentStroke_DriveStroke == *pLevelAdjustStroke_cur))
			{
				*pTimers_StoreRestoreStandard = CONST_StoreStandardTimeout;
				*pRideHghtStoreRequest        = RideHeightStorageRequest_StoreStandardUserDrivePosition;
				*pRampLevelState              = SM_StoreRamp_StoreStandard;
			}
			//! #### Check the conditions for state change to 'RecallRamp_NoAction'
			// if timer reaches to 500ms
			else
			{
				//! #### Check for time elapse of RestoreStandard timer
				if (CONST_TimerFunctionElapsed == *pTimers_StoreRestoreStandard)
				{
					*pRideHghtStoreRequest = RideHeightStorageRequest_TakeNoAction;
					*pRampLevelState       = SM_Ramp_NoAction;
				}
				else
				{
					// Do nothing : keep previous value
				}
			}
		break;
			//! ##### Check the conditions for state change from 'Store Standard'
		case SM_StoreRamp_StoreStandard:
			//! #### Check the conditions for state change to 'StoreRampLevel_NoAction'
			//! #### Check for RideHeightStorageAcknowledgement status
			if (StorageAck_ChangeAcknowledged == *pRideHeightStoreAck)
			{
				*pTimers_StoreRestoreStandard = CONST_TimerFunctionInactive;
				*pRideHghtStoreRequest        = RideHeightStorageRequest_TakeNoAction;
				*pRampLevelState              = SM_Ramp_NoAction;
			}
			//! #### Check for time elapse of StoreStandard timer
			else
			{
				if (CONST_TimerFunctionElapsed == *pTimers_StoreRestoreStandard)
				{
					*pRideHghtStoreRequest = RideHeightStorageRequest_TakeNoAction;
					*pRampLevelState       = SM_Ramp_NoAction;
				}
				else
				{
					// Do nothing : keep previous value
				}
			}
		break;
		default:
			// Do nothing: keep previous state value
		break;
		}
	}
}
//!======================================================================================
//!
//! \brief
//! This function is processing the ControlBoxLogicForAdjustStrokeRequest logic
//!
//! \param   *pLevelAdjustmentStroke   Provides the status of LevelAdjustmentStroke 
//! \param   *pLvlStrokeRequest        Update 'LevelStrokeRequest' value as per conditions
//! \param   *pTimers_AdjustStroke     To check current timer value and update
//!
//!======================================================================================
static void ControlBoxLogicForAdjustStrokeRequest(const LevelAdjustmentStrokeType *pLevelAdjustmentStroke,
                                                        LevelStrokeRequest_T      *pLvlStrokeRequest,
                                                        uint16                    *pTimers_AdjustStroke)
{
   static uint8 strokeflag = CONST_ResetFlag;

   //! ##### Check the conditions for 'LevelStrokeRequest' output based on timer 
   //! #### Check for the AdjustStroke_Timer status
   if (CONST_TimerFunctionElapsed == *pTimers_AdjustStroke)
   {
      *pTimers_AdjustStroke = CONST_TimerFunctionInactive;
   }
   else
   {
      // Do nothing : keep previous value
   }
   if (CONST_TimerFunctionInactive != *pTimers_AdjustStroke)
   {
      *pLvlStrokeRequest = LevelStrokeRequest_NotAvailable;
      strokeflag         = CONST_SetFlag;
   }
   else
   {
      if (CONST_SetFlag == strokeflag)
      {
         *pLvlStrokeRequest    = LevelStrokeRequest_DockingLevelControlStroke;
         *pTimers_AdjustStroke = CONST_TimerFunctionInactive;
         strokeflag            = CONST_ResetFlag;
      }
      //! #### Examine the changes in LevelAdjustmentStroke 
      else if ((pLevelAdjustmentStroke->Current != pLevelAdjustmentStroke->Previous)
              && (LevelAdjustmentStroke_DriveStroke == pLevelAdjustmentStroke->Current))
      {
         *pLvlStrokeRequest = LevelStrokeRequest_DriveLevelControlStroke;
      }
      else if ((pLevelAdjustmentStroke->Current != pLevelAdjustmentStroke->Previous)
              && (LevelAdjustmentStroke_DockingStroke == pLevelAdjustmentStroke->Current))
      {
         *pLvlStrokeRequest = LevelStrokeRequest_DockingLevelControlStroke;
      }
      else
      {
         if ((pLevelAdjustmentStroke->Current != pLevelAdjustmentStroke->Previous)
            && (LevelAdjustmentStroke_NotAvailable == pLevelAdjustmentStroke->Current))
         {
             *pLvlStrokeRequest = LevelStrokeRequest_NotAvailable;
         }
         else
         {
            // Do nothing : keep previous value
         }
      }
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ControlBoxLogicForAdjustLevelMain logic
//!
//! \param   *pLevelAdjustmentAction   Provides the status of LevelAdjustmentActionType
//! \param   *pLevelAdjustmentAxles     Provides the status of LevelAdjustmentAxles 
//! \param   StopRequest_state         Specifies the current state
//! \param   *pLvlRequest              Update the values as per conditions
//! \param   *pTimers_AdjustFront      To check current timer value and update
//! \param   *pTimers_AdjustRear       To check current timer value and update
//! 
//!======================================================================================
static void ControlBoxLogicForAdjustLevelMain(const LevelAdjustmentActionType *pLevelAdjustmentAction,
                                              const LevelAdjustmentAxles_T    *pLevelAdjustmentAxles,
                                              const Lvlcntrl_SM_States        StopRequest_state,
                                                    LevelRequest_T            *pLvlRequest,
                                                    uint16                    *pTimers_AdjustFront,
                                                    uint16                    *pTimers_AdjustRear)
{
   //! ##### Check LevelAdjustmentAxles is equal to front or parallel  
   if ((LevelAdjustmentAxles_Front == *pLevelAdjustmentAxles)
      || (LevelAdjustmentAxles_Parallel == *pLevelAdjustmentAxles))
   {
      //! #### Check for 'StopRequest_state' to process the 'AdjustLevelFront' function
      if (SM_StopRequest_NoAction == StopRequest_state)
      {
         //! #### Process the adjust level front function : 'AdjustLevelFront()'
         AdjustLevelFront(pLevelAdjustmentAction,
                          pLvlRequest,
                          pTimers_AdjustFront);
      }
      else
      {
         // Do nothing : keep previous value
      }
   } 
   else
   {
      pLvlRequest->FrontAxle_RE = LevelChangeRequest_TakeNoAction;
      *pTimers_AdjustFront      = CONST_TimerFunctionInactive;
   }
   //! ##### Check the wired LevelAdjustmentAxles is equal to rear or parallel
   if ((LevelAdjustmentAxles_Rear == *pLevelAdjustmentAxles)
      || (LevelAdjustmentAxles_Parallel == *pLevelAdjustmentAxles))
   {
      //! #### Check the 'StopRequest_state' to process the 'AdjustLevelRear' function
      if (SM_StopRequest_NoAction == StopRequest_state)
      {
         //! #### Process the adjust level rear for wired function : 'AdjustLevelRear()'
         AdjustLevelRear(pLevelAdjustmentAction,
                         pLvlRequest,
                         pTimers_AdjustRear);
      }
      else
      {
         // Do nothing : keep previous value
      }
   }
   else
   {
      pLvlRequest->RearAxle_RE = LevelChangeRequest_TakeNoAction;
      *pTimers_AdjustRear      = CONST_TimerFunctionInactive;
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the AdjustLevelFront
//!
//! \param   *pAdjustLevelData      Provides the status of LevelAdjustmentActionType 
//! \param   *pLvlRqst              Updating the status as per conditions
//! \param   *pTimers_AdjustFront   To check current timer value and update
//!
//!======================================================================================
static void AdjustLevelFront(const LevelAdjustmentActionType   *pAdjustLevelData,
                                   LevelRequest_T              *pLvlRqst,
                                   uint16                      *pTimers_AdjustFront)
{ 
   //! ##### Process the conditions to update 'LevelRequest.FrontAxle_RE' output port based on 'LevelAdjustmentActionType'
   //! #### Check for the WiredLevelAdjustmentAction status
   if ((pAdjustLevelData->Previous != pAdjustLevelData->Current)
      && (LevelAdjustmentAction_UpBasic == pAdjustLevelData->Current))
   {
      pLvlRqst->FrontAxle_RE   = LevelChangeRequest_VehicleBodyUpLifting;
      *pTimers_AdjustFront     = CONST_TimerFunctionInactive;      
   }
   else if ((pAdjustLevelData->Previous != pAdjustLevelData->Current)
           && (LevelAdjustmentAction_UpShortMovement == pAdjustLevelData->Current))
   {
      pLvlRqst->FrontAxle_RE   = LevelChangeRequest_VehicleBodyUpMinimumMovementLifting;
      *pTimers_AdjustFront     = CONST_AdjustLevelTimeout;
   }
   else if ((pAdjustLevelData->Previous != pAdjustLevelData->Current)
           && (LevelAdjustmentAction_DownBasic == pAdjustLevelData->Current))
   {
      pLvlRqst->FrontAxle_RE   = LevelChangeRequest_VehicleBodyDownLowering;
      *pTimers_AdjustFront     = CONST_TimerFunctionInactive;     
   }
   else if ((pAdjustLevelData->Previous != pAdjustLevelData->Current)
           && (LevelAdjustmentAction_DownShortMovement == pAdjustLevelData->Current))
   {
      pLvlRqst->FrontAxle_RE   = LevelChangeRequest_VehicleBodyDownMinimumMovementLowering;
      *pTimers_AdjustFront     = CONST_AdjustLevelTimeout;
   }
   else if (((pAdjustLevelData->Previous != pAdjustLevelData->Current)
           && ((LevelAdjustmentAction_Reserved == pAdjustLevelData->Current)
           || (LevelAdjustmentAction_Reserved_01 == pAdjustLevelData->Current)
           || (LevelAdjustmentAction_Reserved_02 == pAdjustLevelData->Current)
           || (LevelAdjustmentAction_Reserved_03 == pAdjustLevelData->Current)
           || (LevelAdjustmentAction_Reserved_04 == pAdjustLevelData->Current)
           || (LevelAdjustmentAction_Reserved_05 == pAdjustLevelData->Current)
           || (LevelAdjustmentAction_Reserved_06 == pAdjustLevelData->Current)
           || (LevelAdjustmentAction_GotoDriveLevel == pAdjustLevelData->Current)
           || (LevelAdjustmentAction_Ferry == pAdjustLevelData->Current)
           || (LevelAdjustmentAction_Idle == pAdjustLevelData->Current)
           || (LevelAdjustmentAction_Error == pAdjustLevelData->Current)
           || (LevelAdjustmentAction_NotAvailable == pAdjustLevelData->Current)))
           && (CONST_TimerFunctionInactive == *pTimers_AdjustFront))
   {
      pLvlRqst->FrontAxle_RE = LevelChangeRequest_TakeNoAction;    
   }
   else
   {
      //! #### Check for time elapsed value of AjustFront timer
      if (CONST_TimerFunctionElapsed == *pTimers_AdjustFront)
      {
         pLvlRqst->FrontAxle_RE  = LevelChangeRequest_TakeNoAction; 
         *pTimers_AdjustFront    = CONST_TimerFunctionInactive;
      }    
      else
      {
         // Do nothing : keep previous value
      }
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the AdjustLevelRear
//!
//! \param   *pAdjustLvlData      Provides the status of LevelAdjustmentActionType 
//! \param   *pLevelRqst          Updating the status as per conditions
//! \param   *pTimers_AdjustRear  To check current timer value and update 
//!                  
//!======================================================================================
static void AdjustLevelRear(const LevelAdjustmentActionType  *pAdjustLvlData,
                                  LevelRequest_T         *pLevelRqst,
                                  uint16                 *pTimers_AdjustRear)
{
   //! ##### Process the conditions to update 'LevelRequest.RearAxle_RE' output port based on 'LevelAdjustmentAction'
   //! #### Check for the LevelAdjustmentAction status
   if ((pAdjustLvlData->Previous != pAdjustLvlData->Current)
      && (LevelAdjustmentAction_UpBasic == pAdjustLvlData->Current))
   {
      pLevelRqst->RearAxle_RE = LevelChangeRequest_VehicleBodyUpLifting;
      *pTimers_AdjustRear     = CONST_TimerFunctionInactive;     
   }
   else if ((pAdjustLvlData->Previous != pAdjustLvlData->Current)
           && (LevelAdjustmentAction_UpShortMovement == pAdjustLvlData->Current))
   {
      pLevelRqst->RearAxle_RE = LevelChangeRequest_VehicleBodyUpMinimumMovementLifting;
      *pTimers_AdjustRear     = CONST_AdjustLevelTimeout;
   }
   else if ((pAdjustLvlData->Previous != pAdjustLvlData->Current)
           && (LevelAdjustmentAction_DownBasic == pAdjustLvlData->Current))
   {
      pLevelRqst->RearAxle_RE = LevelChangeRequest_VehicleBodyDownLowering;
      *pTimers_AdjustRear     = CONST_TimerFunctionInactive;     
   }
   else if ((pAdjustLvlData->Previous != pAdjustLvlData->Current)
           && (LevelAdjustmentAction_DownShortMovement == pAdjustLvlData->Current))
   {
      pLevelRqst->RearAxle_RE = LevelChangeRequest_VehicleBodyDownMinimumMovementLowering;
      *pTimers_AdjustRear     = CONST_AdjustLevelTimeout;
   }
   else if (((pAdjustLvlData->Previous != pAdjustLvlData->Current)
           && ((LevelAdjustmentAction_Reserved == pAdjustLvlData->Current)
           || (LevelAdjustmentAction_Reserved_01 == pAdjustLvlData->Current)
           || (LevelAdjustmentAction_Reserved_02 == pAdjustLvlData->Current)
           || (LevelAdjustmentAction_Reserved_03 == pAdjustLvlData->Current)
           || (LevelAdjustmentAction_Reserved_04 == pAdjustLvlData->Current)
           || (LevelAdjustmentAction_Reserved_05 == pAdjustLvlData->Current)
           || (LevelAdjustmentAction_Reserved_06 == pAdjustLvlData->Current)
           || (LevelAdjustmentAction_GotoDriveLevel == pAdjustLvlData->Current)
           || (LevelAdjustmentAction_Ferry == pAdjustLvlData->Current)
           || (LevelAdjustmentAction_Error == pAdjustLvlData->Current)
           || (LevelAdjustmentAction_NotAvailable == pAdjustLvlData->Current)
           || (LevelAdjustmentAction_Idle == pAdjustLvlData->Current)))
           && (CONST_TimerFunctionInactive == *pTimers_AdjustRear))
   {
      pLevelRqst->RearAxle_RE = LevelChangeRequest_TakeNoAction;  
   }
   //! #### Check for timer elapsed value 
   else
   {
      if (CONST_TimerFunctionElapsed == *pTimers_AdjustRear)
      {
         pLevelRqst->RearAxle_RE = LevelChangeRequest_TakeNoAction; 
         *pTimers_AdjustRear     = CONST_TimerFunctionInactive;        
      }
      else
      {
         // Do nothing : keep previous value
      }
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the ControlBoxLogicForBackToDriveLevelRequest
//!
//! \param   *pLvlAdjustAction        Provides the status of LevelAdjustmentAction 
//! \param   *pBackToDriveReqstACK    Specifies the acknowledgement of BackToDriveRequest
//! \param   *pBackToDrive            Reinitialize the state machine to init state
//! \param   *pBackToDriveReqst       Provides and updates the present status based on conditions
//! \param   *pTimers_B2D             To check current timer value and update
//!
//!======================================================================================
static void ControlBoxLogicForBackToDriveLevelRequest(const LevelAdjustmentActionType  *pLvlAdjustAction,
                                                      const BackToDriveReqACK_T        *pBackToDriveReqstACK,
                                                            uint8                      *pBackToDrive,
                                                            BackToDriveReq_T           *pBackToDriveReqst,
                                                            uint16                     *pTimers_B2D)
{
   static Lvlcntrl_SM_States is_BackToDrive = SM_BackToDrive_Idle;
   if (CONST_NotInitialize == *pBackToDrive)
   {
      *pBackToDrive      = CONST_Initialize;
      is_BackToDrive     = SM_BackToDrive_Idle;
      *pBackToDriveReqst = BackToDriveReq_Idle;
      *pTimers_B2D       = CONST_TimerFunctionInactive;
   }
   else
   {
      //! ###### Select the state based on 'B2DData.is_BackToDrive'
      switch (is_BackToDrive)
      {
         //! ##### Check the conditions for state change from 'BacktoDrive B2D' state
         case SM_BackToDrive_B2D:
            //! #### Check for the state change to 'BackToDrive_Idle'
            //! #### Check for 'BackToDriveRequestAck' status
            if (BackToDriveReqACK_ChangeAcknowledged == *pBackToDriveReqstACK)
            {
               *pTimers_B2D       = CONST_TimerFunctionInactive;
               is_BackToDrive     = SM_BackToDrive_Idle;
               *pBackToDriveReqst = BackToDriveReq_Idle;
            }
            //! #### Check for the state change to 'BackToDrive_Idle'
            else
            {
               //! #### Check for time elapse of B2D timer
               if (CONST_TimerFunctionElapsed == *pTimers_B2D)
               {
                  is_BackToDrive     = SM_BackToDrive_Idle;
                  *pBackToDriveReqst = BackToDriveReq_Idle;
               }
               else
               {
                  // Do Nothing : Keep previous value
               }
            }
         break;
       //! ##### Check the conditions for state change from 'BacktoDrive IDLE' state
         default: //case SM_BackToDrive_Idle:
			 //! #### Check for the state change to 'BackToDrive_B2D'
			 //! #### Check for 'LevelAdjustmentAction' and 'BackToDriveRequest' status
			 if (((pLvlAdjustAction->Current == pLvlAdjustAction->Previous)
				|| (LevelAdjustmentAction_NotAvailable != pLvlAdjustAction->Previous))
				&& ((pLvlAdjustAction->Current != pLvlAdjustAction->Previous)
			    && (LevelAdjustmentAction_GotoDriveLevel == pLvlAdjustAction->Current))
				&& (BackToDriveReqACK_ChangeAcknowledged != *pBackToDriveReqstACK))
			 {
				 *pTimers_B2D       = CONST_B2DSwitchStatus_Timeout;
				 is_BackToDrive     = SM_BackToDrive_B2D;
				 *pBackToDriveReqst = BackToDriveReq_B2DRequested;
			 }
			 else
			 {
				 // Do nothing : keep previous value
			 }
         break;
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the ControlBoxLogicForStopRequest logic
//!
//! \param   *pAirSuspensionStopRequest   Provides the AirSuspensionStopRequest for  True or False 
//! \param   *pStopLvlChangeAck           Specifies the acknowledgement of StopLevelChange
//! \param   *pStopRequest                Reinitialize the state machine to init state
//! \param   *pStopLvlChangeRequest       Updating the status as per conditions 
//! \param   *pLvlReq                     Updating the output signals to the ports of LevelRequest structure
//! \param   *pTimers_StopReq             To check current timer value and update
//!
//! \return Lvlcntrl_SM_States                 Returns current state
//!
//!======================================================================================
static Lvlcntrl_SM_States ControlBoxLogicForStopRequest(      FalseTrue               *pAirSuspensionStopRequest,
                                                        const StopLevelChangeStatus_T *pStopLvlChangeAck,
                                                              uint8                   *pStopRequest,
                                                              Request_T               *pStopLvlChangeRequest,
                                                              LevelRequest_T          *pLvlReq,
                                                              uint16                  *pTimers_StopReq)
{
   static Lvlcntrl_SM_States is_StopReq = SM_StopRequest_NoAction;
   if (CONST_NotInitialize == *pStopRequest)
   {
      pAirSuspensionStopRequest->Previous = pAirSuspensionStopRequest->Current;
      *pStopRequest                       = CONST_Initialize;
      *pStopLvlChangeRequest              = Request_NotRequested;
      is_StopReq                          = SM_StopRequest_NoAction;
      *pTimers_StopReq                    = CONST_TimerFunctionInactive;
   }
   else
   {
      //! ###### Select the state based on 'StopReqData.is_StopReq'
      switch (is_StopReq)
      {
         //! ##### Check the conditions for state change from 'StopRequest Stopaction'
         case SM_StopRequest_StopAction:
            //! #### Check for the state change to 'StopRequest_NoAction' State 
            //! #### Check for 'StopLevelChangeAck' status
            if (StopLevelChangeStatus_LevelChangedStopped == *pStopLvlChangeAck)
            {
               *pTimers_StopReq       = CONST_TimerFunctionInactive;
               *pStopLvlChangeRequest = Request_NotRequested;
                is_StopReq            = SM_StopRequest_NoAction;
            }
            //! #### Check for the state change to StopRequest_NoAction
            else
            {
               //! #### Check for time elapse of StopRequest timer
               if (CONST_TimerFunctionElapsed == *pTimers_StopReq)
               {
                  *pStopLvlChangeRequest = Request_NotRequested;
                   is_StopReq            = SM_StopRequest_NoAction;
               }
               else
               {
                  // No event, keep the state until timeout occurs
               }
            }
         break;
         //! ##### Check the conditions for state change from 'StopRequest Noaction'
         default: //case SM_StopRequest_NoAction:
			 //! #### Check for the state change to 'StopRequest_StopAction'
			 //! #### Check for 'AirSuspensionStopRequest' status
			 if ((pAirSuspensionStopRequest->Current != pAirSuspensionStopRequest->Previous)
				&& (FalseTrue_False == pAirSuspensionStopRequest->Previous)
				&& (FalseTrue_True == pAirSuspensionStopRequest->Current))
			 {
				 *pTimers_StopReq        = CONST_StopRequestTimeout;
				 pLvlReq->FrontAxle_RE   = LevelChangeRequest_TakeNoAction;
				 pLvlReq->RearAxle_RE    = LevelChangeRequest_TakeNoAction;
				 *pStopLvlChangeRequest  = Request_RequestActive;
				 is_StopReq              = SM_StopRequest_StopAction;
			 }
			 else
			 {
				 // Do nothing : keep previous value
			 }
         break;
      }
   }
   // returns the current StopRequest state
   return is_StopReq;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the LevelControl_HMICtrl_Loc_Init
//!
//! \param   *pWiredAirSuspensionStopReq   Provides the status of AirSuspensionStopRequest for wired
//! \param   *pWRCAirSuspensionStopReq     Provides the status of AirSuspensionStopRequest for WRC
//! \param   *pLevelCntrlOutData           Updates the default values to the output structure
//! \param   *pState_Type                  Updates all Controlbox related FSM's to Init state
//! \param   *pStateMachine_Type           Updates all switch related FSM's to Init state
//! \param   Timer_init                    Initialize all timer values
//!
//!======================================================================================
static void LevelControl_HMICtrl_Loc_Init(const FalseTrue                     *pWiredAirSuspensionStopReq,
                                          const FalseTrue                     *pWRCAirSuspensionStopReq,
                                                LevelControl_Out_StructType_T *pLevelCntrlOutData,
                                                state_StructType_T            *pState_Type,
                                                StateMachine_StrctType_T      *pStateMachine_Type,
                                                uint16                        Timer_init[CONST_NbOfTimers])
{
   //! ###### Processing Initialization logic
   if  (((FalseTrue_Error == pWiredAirSuspensionStopReq->Current)
       || (FalseTrue_NotAvaiable == pWiredAirSuspensionStopReq->Current))
       || ((FalseTrue_Error == pWRCAirSuspensionStopReq->Current)
       || (FalseTrue_NotAvaiable == pWRCAirSuspensionStopReq->Current)))
   { 
      pLevelCntrlOutData->StopLevelChangeRequest   = Request_NotAvailable;
   }
   //! ##### Intiliaze the output ports with default values 
   pLevelCntrlOutData->RampLevelStorageRequest     = RampLevelRequest_TakeNoAction;
   pLevelCntrlOutData->RampLevelRequest            = RampLevelRequest_TakeNoAction;
   pLevelCntrlOutData->FerryFunctionRequest        = Request_NotRequested;
   pLevelCntrlOutData->BackToDriveReq              = BackToDriveReq_Idle;
   pLevelCntrlOutData->RideHeightStorageRequest    = RideHeightStorageRequest_TakeNoAction;
   pLevelCntrlOutData->RideHeightFunctionRequest   = RideHeightFunction_NotAvailable;
   pLevelCntrlOutData->LevelRequest.FrontAxle_RE   = LevelChangeRequest_TakeNoAction;
   pLevelCntrlOutData->LevelRequest.RearAxle_RE    = LevelChangeRequest_TakeNoAction;
   pLevelCntrlOutData->LevelRequest.RollRequest_RE = RollRequest_Idle;
   pState_Type->is_active_StoreRamplevel           = CONST_NotInitialize;
   pState_Type->is_active_RecallRampLevel          = CONST_NotInitialize;
   pState_Type->is_active_StoreRestoreStandard     = CONST_NotInitialize;
   pState_Type->is_active_StopRequestLevel         = CONST_NotInitialize;
   pState_Type->is_active_BackToDriveLevelReq      = CONST_NotInitialize;
   pStateMachine_Type->is_active_ECS               = CONST_NotInitialize;
   pStateMachine_Type->is_active_FerrySwitch       = CONST_NotInitialize;
   pStateMachine_Type->is_active_KneelingSwitch    = CONST_NotInitialize;
   pStateMachine_Type->is_active_Ramplevel         = CONST_NotInitialize;
   pStateMachine_Type->is_active_ADLS              = CONST_NotInitialize;
   pStateMachine_Type->is_active_FPBR              = CONST_NotInitialize;
   //! ##### Check for P1ALT_ECS_PartialAirSystem or P1ALU_ECS_FullAirSystem status
   if ((TRUE == PCODE_ECS_PartialAirSystem)
      || (TRUE == PCODE_ECS_FullAirSystem))
   {
      Timer_init[CONST_AdjustStroke] = CONST_LvlStrokeReqInitTimeout;
      Timer_init[CONST_AdjustFront]  = CONST_TimerFunctionInactive;
      Timer_init[CONST_AdjustRear]   = CONST_TimerFunctionInactive;
      Timer_init[CONST_FerryTimer]   = CONST_TimerFunctionInactive;
   }
   //! ##### Check for P1B9X_WirelessRC_Enable status
   else if (TRUE == PCODE_WirelessRC_Enable)
   {
      Timer_init[CONST_AdjustStroke] = CONST_LvlStrokeReqInitTimeout;
      Timer_init[CONST_AdjustFront]  = CONST_TimerFunctionInactive;
      Timer_init[CONST_AdjustRear]   = CONST_TimerFunctionInactive;
      Timer_init[CONST_FerryTimer]   = CONST_TimerFunctionInactive;
   }  
   else
   {
      // Do nothing: ignore value, keep the previous status
   }
}

#define LevelControl_HMICtrl_STOP_SEC_CODE
#include "LevelControl_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
