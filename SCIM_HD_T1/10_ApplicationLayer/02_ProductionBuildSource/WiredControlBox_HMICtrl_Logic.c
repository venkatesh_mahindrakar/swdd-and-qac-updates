/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file WiredControlBox_HMICtrl_Logic.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_VehicleDynamics_Suspension 
//! @{
//! @addtogroup WiredControlBox
//! @{
//!
//! \brief
//! WiredControlBox_HMICtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the WiredControlBox_HMICtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 /*********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
// Includes
#include "Rte_WiredControlBox_HMICtrl.h"
#include "WiredControlBox_HMICtrl_If.h"
#include "WiredControlBox_HMICtrl_Logic.h"
#include "WiredControlBox_HMICtrl_Logic_If.h"
#include "FuncLibrary_ScimStd_If.h"
#include "FuncLibrary_Timer_If.h"

#define PCODE_ECSStopButtonHoldTimeout    ((uint16)(Rte_Prm_P1DWI_ECSStopButtonHoldTimeout_v() * (0.2) * CONST_sec2msec) / CONST_TimeBase)
#define PCODE_RCECS_HoldCircuitTimer      (((uint16)(Rte_Prm_P1IUS_RCECS_HoldCircuitTimer_v()) * 20U) / CONST_TimeBase)
#define PCODE_RCECSButtonStucked          (((uint16)(Rte_Prm_P1DWJ_RCECSButtonStucked_v()) * 10U *CONST_sec2msec) / CONST_TimeBase)
#define PCODE_RCECSUpDownStucked          (((uint16)(Rte_Prm_P1DWK_RCECSUpDownStucked_v()) * 10U *CONST_sec2msec) / CONST_TimeBase)
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Recall_OutputRecalllevel
//!
//! \param   *pWCB_HMICtrl_input_Buttondata   Providing the input button status 
//! \param   *pWiredLevelUserMemAction        Update the output value based on conditions
//! \param   *pECS_MemSwTimings               Provides min and max switch press time values
//! \param   *pTimers                         To check current timer value and update
//!
//!======================================================================================
void Recall_OutputRecalllevel(const WiredControlBox_HMICtrl_in_ButtonStructType  *pWCB_HMICtrl_input_Buttondata,
                                    LevelUserMemoryAction_T                      *pWiredLevelUserMemAction,
                              const SEWS_ECS_MemSwTimings_P1BWF_s_T              *pECS_MemSwTimings,
                              const uint16                                       *pTimers)
{
   //! ##### Check for MemoryButton status
   if ((PushButtonStatus_Neutral == pWCB_HMICtrl_input_Buttondata->MemButtonStatus.previousValue)
      && (PushButtonStatus_Pushed == pWCB_HMICtrl_input_Buttondata->MemButtonStatus.currentvalue))
   {
      *pWiredLevelUserMemAction = LevelUserMemoryAction_Inactive;
   }
   else
   {
      // Do nothing: keep the previous status
   }
   //! ##### Check for StopButton status
   if ((PushButtonStatus_Neutral == pWCB_HMICtrl_input_Buttondata->StopButtonStatus.previousValue)
      && (PushButtonStatus_Pushed == pWCB_HMICtrl_input_Buttondata->StopButtonStatus.currentvalue))
   {
      *pWiredLevelUserMemAction = LevelUserMemoryAction_Inactive;
   }
   else
   {
      // Do nothing: keep the previous status
   }
   //! ##### Check for AdjustButton status
   if ((PushButtonStatus_Neutral == pWCB_HMICtrl_input_Buttondata->AdjustButtonStatus.previousValue)
      && (PushButtonStatus_Pushed == pWCB_HMICtrl_input_Buttondata->AdjustButtonStatus.currentvalue))
   {
      *pWiredLevelUserMemAction = LevelUserMemoryAction_Inactive;
   }
   else
   {
      // Do nothing: keep the previous status
   }
   //! ##### Check for BackButton status
   if ((PushButtonStatus_Neutral == pWCB_HMICtrl_input_Buttondata->BackButtonStatus.previousValue)
      && (PushButtonStatus_Pushed == pWCB_HMICtrl_input_Buttondata->BackButtonStatus.currentvalue))
   {
      *pWiredLevelUserMemAction = LevelUserMemoryAction_Inactive;
   }
   else
   {
      // Do nothing: keep the previous status
   }
   //! ##### Check for WRUpButton status 
   if ((EvalButtonRequest_Neutral == pWCB_HMICtrl_input_Buttondata->WRUpButtonStatus.previousValue)
      && ((EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_input_Buttondata->WRUpButtonStatus.currentvalue)
      || (EvalButtonRequest_ShortPush == pWCB_HMICtrl_input_Buttondata->WRUpButtonStatus.currentvalue)
      || (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_input_Buttondata->WRUpButtonStatus.currentvalue)))
   {
      *pWiredLevelUserMemAction = LevelUserMemoryAction_Inactive;
   }
   else
   {
      // Do nothing: keep the previous status
   }
   //! ##### Check for WRDownButton status
   if ((EvalButtonRequest_Neutral == pWCB_HMICtrl_input_Buttondata->WRDownButtonStatus.previousValue)
      && ((EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_input_Buttondata->WRDownButtonStatus.currentvalue)
      || (EvalButtonRequest_ShortPush == pWCB_HMICtrl_input_Buttondata->WRDownButtonStatus.currentvalue)
      || (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_input_Buttondata->WRDownButtonStatus.currentvalue)))
   {
      *pWiredLevelUserMemAction = LevelUserMemoryAction_Inactive;
   }
   else
   {
      // Do nothing: keep the previous status
   }
   if (PushButtonStatus_Pushed == pWCB_HMICtrl_input_Buttondata->MemButtonStatus.currentvalue)
   {
      // Do nothing:maintain previous value
   }
   //! ##### Check for memory button status and timers value
   else if ((*pTimers > (uint16)(PCODE_RCECSButtonStucked - (((uint16)(pECS_MemSwTimings->Memory_Recall_Max_Press)* 100U)/ CONST_TimeBase)))
           && (*pTimers < (uint16)(PCODE_RCECSButtonStucked - (((uint16)(pECS_MemSwTimings->Memory_Recall_Min_Press)* 100U)/ CONST_TimeBase))))
   {
      *pWiredLevelUserMemAction = LevelUserMemoryAction_Recall;
   }
   else
   {
      // Do nothing
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Storelevel_OutputStorelevel
//!
//! \param   *pWCB_in_Buttondata       Providing the input button status 
//! \param   *pWiredLevelUserMemActn   Updating the LevelUserMemoryAction for wired varient
//! \param   *pECS_MemSwTiming         Provides min and max switch press time values
//! \param   *pSM_States               Providing current state value
//! \param   *pStore_Timers            To check current timer value and update
//!
//!======================================================================================
void Storelevel_OutputStorelevel(const WiredControlBox_HMICtrl_in_ButtonStructType  *pWCB_in_Buttondata,
                                       LevelUserMemoryAction_T                      *pWiredLevelUserMemActn,
                                 const SEWS_ECS_MemSwTimings_P1BWF_s_T              *pECS_MemSwTiming,
                                 const Wcb_LevelAdjustment_FSM                      *pSM_States,
                                       uint16                                       *pStore_Timers)
{
   static uint8  Flag       = CONST_Resetflag;
   static uint16 minPressTiming    = 0U;

   if (CONST_Resetflag == Flag)
   {
      *pStore_Timers = CONST_TimerFunctionInactive;
      Flag           = CONST_Setflag;
   }
   else
   {
      // Do nothing
   }
   //! ##### Check for memory button status 
   if ((PushButtonStatus_Neutral == pWCB_in_Buttondata->MemButtonStatus.previousValue)
      && (PushButtonStatus_Pushed == pWCB_in_Buttondata->MemButtonStatus.currentvalue))
   {
      *pWiredLevelUserMemActn = LevelUserMemoryAction_Inactive;
   }
   else
   {
      // Do nothing: keep the previous status
   }
   //! ##### Check for StopButton status
   if ((PushButtonStatus_Neutral == pWCB_in_Buttondata->StopButtonStatus.previousValue)
      && (PushButtonStatus_Pushed == pWCB_in_Buttondata->StopButtonStatus.currentvalue))
   {
      *pWiredLevelUserMemActn = LevelUserMemoryAction_Inactive;
   }
   else
   {
      // Do nothing: keep the previous status
   }
   //! ##### Check for AdjustButton status
   if ((PushButtonStatus_Neutral == pWCB_in_Buttondata->AdjustButtonStatus.previousValue)
      && (PushButtonStatus_Pushed == pWCB_in_Buttondata->AdjustButtonStatus.currentvalue))
   {
      *pWiredLevelUserMemActn = LevelUserMemoryAction_Inactive;
   }
   else
   {
      // Do nothing: keep the previous status
   }
   //! ##### Check for BackButton status
   if ((PushButtonStatus_Neutral == pWCB_in_Buttondata->BackButtonStatus.previousValue)
      && (PushButtonStatus_Pushed == pWCB_in_Buttondata->BackButtonStatus.currentvalue))
   {
      *pWiredLevelUserMemActn = LevelUserMemoryAction_Inactive;
   }
   else
   {
      // Do nothing: keep the previous status
   }
   //! ##### Check for WRUpButton status
   if ((EvalButtonRequest_Neutral == pWCB_in_Buttondata->WRUpButtonStatus.previousValue)
      && ((EvalButtonRequest_EvaluatingPush == pWCB_in_Buttondata->WRUpButtonStatus.currentvalue)
      || (EvalButtonRequest_ShortPush == pWCB_in_Buttondata->WRUpButtonStatus.currentvalue)
      || (EvalButtonRequest_ContinuouslyPushed == pWCB_in_Buttondata->WRUpButtonStatus.currentvalue)))
   {
      *pWiredLevelUserMemActn = LevelUserMemoryAction_Inactive;
   }
   else
   {
      // Do nothing: keep the previous status
   }
   //! ##### Check for WRDownButton status
   if ((EvalButtonRequest_Neutral == pWCB_in_Buttondata->WRDownButtonStatus.previousValue) 
      && ((EvalButtonRequest_EvaluatingPush == pWCB_in_Buttondata->WRDownButtonStatus.currentvalue)
      || (EvalButtonRequest_ShortPush == pWCB_in_Buttondata->WRDownButtonStatus.currentvalue)
      || (EvalButtonRequest_ContinuouslyPushed == pWCB_in_Buttondata->WRDownButtonStatus.currentvalue)))
   {
      *pWiredLevelUserMemActn = LevelUserMemoryAction_Inactive;
   }
   else
   {
      // Do nothing: keep the previous status
   }
   //! ##### Check for MemoryButton status
   if (PushButtonStatus_Error == pWCB_in_Buttondata->MemButtonStatus.currentvalue)
   {
      *pWiredLevelUserMemActn   = LevelUserMemoryAction_Error;
      *pStore_Timers            = CONST_TimerFunctionInactive;
   }
   else
   {
      // Do nothing: keep the previous status
   } 
   if (PushButtonStatus_Pushed == pWCB_in_Buttondata->MemButtonStatus.currentvalue)
   {
      if ((((uint16)(pECS_MemSwTiming->Memory_Recall_Max_Press) * 100U) / CONST_TimeBase) < (((uint16)(pECS_MemSwTiming->Level_Memorization_Min_Press)* 100U) / CONST_TimeBase))
      {
         minPressTiming = (((uint16)(pECS_MemSwTiming->Memory_Recall_Max_Press) * 100U) / CONST_TimeBase);
      }
      else
      {
         minPressTiming = (((uint16)(pECS_MemSwTiming->Level_Memorization_Min_Press) * 100U) / CONST_TimeBase);
      }
   }
   else
   {
      //Do Nothing:keep previous value
   }
   //! ##### Check for memory button, SM_states, store_timers values
   if ((*pStore_Timers < (PCODE_RCECSButtonStucked - minPressTiming))
      && ((SM_WCB_LA_Up_Basic != pSM_States->newValue)
      && (SM_WCB_LA_Down_Basic != pSM_States->newValue))
      && (PushButtonStatus_Pushed != pWCB_in_Buttondata->MemButtonStatus.currentvalue))
   {
      *pWiredLevelUserMemActn   = LevelUserMemoryAction_Store;
   }
   else
   {
      // Do nothing: keep the previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the WiredControlBox_HMICtrl_Fallback_modes
//!
//! \param   *pWCB_HMICtrl_input_Button           Examine and update the input signals based on RTE failure events
//! \param   *pWiredLvlAdjustAction               Update the output based on conditions
//! \param   *pWiredLvlAdjustmentAxles            Update the output based on conditions
//!
//!======================================================================================
void WiredControlBox_HMICtrl_Fallback_modes(const PushButtonStatus                             *pSelectButtonStatus,
											      WiredControlBox_HMICtrl_in_ButtonStructType  *pWCB_HMICtrl_input_Button,
                                                  LevelAdjustmentAction_T                      *pWiredLvlAdjustAction,
                                                  LevelAdjustmentAxles_T                       *pWiredLvlAdjustmentAxles)
{
   //! ###### Processing the LIN communication faulty logic
   //! ##### Check SelectButton status
   if (PushButtonStatus_Error == pSelectButtonStatus->currentvalue)
   {
      *pWiredLvlAdjustmentAxles = LevelAdjustmentAxles_Error;
   }
   else
   {
      // Do nothing: keep the previous status
   }
   //! ##### Check for BackButton status
   if (PushButtonStatus_Error == pWCB_HMICtrl_input_Button->BackButtonStatus.currentvalue)
   {
      *pWiredLvlAdjustAction = LevelAdjustmentAction_Error;
   }
   else
   {
      // Do nothing: keep the previous status
   }
   //! ##### Check for WRUpButton status
   if (EvalButtonRequest_Error == pWCB_HMICtrl_input_Button->WRUpButtonStatus.currentvalue)
   {
      pWCB_HMICtrl_input_Button->WRUpButtonStatus.currentvalue = EvalButtonRequest_Neutral;
   }
   else
   {
      // Do nothing: keep the previous status
   }
   //! ##### Check for WRDownButton status
   if (EvalButtonRequest_Error == pWCB_HMICtrl_input_Button->WRDownButtonStatus.currentvalue)
   {
      pWCB_HMICtrl_input_Button->WRDownButtonStatus.currentvalue = EvalButtonRequest_Neutral;
   }
   else
   {
      // Do nothing: keep the previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Fullaircontrolboxselectionlogic
//!
//! \param   *pWCB_in_ButtonLogic                 Providing the input button status
//! \param   *pFullair_SelectButtonStatus         Providing select button status
//! \param   *pFullAirControl_States              Update the current state based on conditions
//! \param   *pstateInitialize                    Provide and update the initialization status
//! \param   *pWiredControlBox_HMICtrl_out_data   Update the output signals to ports of output structure
//!
//!======================================================================================
void Fullaircontrolboxselectionlogic(const WiredControlBox_HMICtrl_in_ButtonStructType  *pWCB_in_ButtonLogic,
                                     const PushButtonStatus                             *pFullair_SelectButtonStatus,
                                           Wcb_Fullaircontrolbox_logic                  *pFullAirControl_States,
                                           StateInitializationType                      *pstateInitialize,
                                           WiredControlBox_HMICtrl_out_StructType       *pWiredControlBox_HMICtrl_out_data)
{
   //! ###### Processing state transitions
   pFullAirControl_States->currentvalue = pFullAirControl_States->newValue;
   if (CONST_Noninitialized == pstateInitialize->Fullaircontrolbox_Initialization)
   {
      pstateInitialize->Fullaircontrolbox_Initialization           = CONST_Initialized;
      pFullAirControl_States->newValue                             = SM_WCB_FullAirControlBox_Off;
      pWiredControlBox_HMICtrl_out_data->M1_DeviceIndication       = DeviceIndication_Off;
      pWiredControlBox_HMICtrl_out_data->M2_DeviceIndication       = DeviceIndication_Off;
      pWiredControlBox_HMICtrl_out_data->M3_DeviceIndication       = DeviceIndication_Off;
      pWiredControlBox_HMICtrl_out_data->WiredLevelUserMemory      = WiredLevelUserMemory_MemOff;
      pWiredControlBox_HMICtrl_out_data->WiredLevelAdjustmentAxles = LevelAdjustmentAxles_NotAvailable;
   }
   else
   {
      switch (pFullAirControl_States->newValue)
      {
         //! ##### Conditions for the state change from 'FrontorM1'
         case SM_WCB_FullAirControlBox_FrontorM1:
            //! #### Check back button status for state changes to 'Off'
            if ((PushButtonStatus_Neutral == pWCB_in_ButtonLogic->BackButtonStatus.previousValue)
               && (PushButtonStatus_Pushed == pWCB_in_ButtonLogic->BackButtonStatus.currentvalue))
            {
               pFullAirControl_States->newValue = SM_WCB_FullAirControlBox_Off;
            }
            //! #### Check select button status for state changes to 'ParallelorM2'
            else if ((PushButtonStatus_Neutral == pFullair_SelectButtonStatus->previousValue)
                    && (PushButtonStatus_Pushed == pFullair_SelectButtonStatus->currentvalue))
            {
               pFullAirControl_States->newValue = SM_WCB_FullAirControlBox_ParallelorM2;
            }
            else
            {
               // Do nothing: keep previous value
            }
         break;
         //! ##### Conditions for the state change from 'ParallelorM2'
         case SM_WCB_FullAirControlBox_ParallelorM2:
            //! #### Check back button status for state changes to 'Off'
            if ((PushButtonStatus_Neutral == pWCB_in_ButtonLogic->BackButtonStatus.previousValue)
               && (PushButtonStatus_Pushed == pWCB_in_ButtonLogic->BackButtonStatus.currentvalue))
            {
               pFullAirControl_States->newValue = SM_WCB_FullAirControlBox_Off;
            }
            //! #### Check select button status for state changes to 'RearorM3'
            else if ((PushButtonStatus_Neutral == pFullair_SelectButtonStatus->previousValue)
                    && (PushButtonStatus_Pushed == pFullair_SelectButtonStatus->currentvalue))
            {
               pFullAirControl_States->newValue = SM_WCB_FullAirControlBox_RearorM3;
            }
            else
            {
               // Do nothing: keep previous value
            }
         break;
         //! ##### Check for the state change from 'RearorM3'
         case SM_WCB_FullAirControlBox_RearorM3:
            //! #### Check back button status for state changes to 'Off'
            if ((PushButtonStatus_Neutral == pWCB_in_ButtonLogic->BackButtonStatus.previousValue)
               && (PushButtonStatus_Pushed == pWCB_in_ButtonLogic->BackButtonStatus.currentvalue))
            {
               pFullAirControl_States->newValue = SM_WCB_FullAirControlBox_Off;
            }
            //! #### Check select button status for state changes to 'FrontorM1'
            else if ((PushButtonStatus_Neutral == pFullair_SelectButtonStatus->previousValue)
                    && (PushButtonStatus_Pushed == pFullair_SelectButtonStatus->currentvalue))
            {
               pFullAirControl_States->newValue = SM_WCB_FullAirControlBox_FrontorM1;
            }
            else
            {
               // Do  nothing Keep Previous Value
            }
         break;
         //! ##### Conditions for the state change from 'default'
         default:
            //! #### Check AdjustButton, MemButton, StopButton status for state changes to 'ParallelorM2'
            if (((PushButtonStatus_Neutral == pWCB_in_ButtonLogic->AdjustButtonStatus.previousValue)
               && (PushButtonStatus_Pushed == pWCB_in_ButtonLogic->AdjustButtonStatus.currentvalue))
               || ((PushButtonStatus_Neutral == pWCB_in_ButtonLogic->MemButtonStatus.previousValue)
               && (PushButtonStatus_Pushed == pWCB_in_ButtonLogic->MemButtonStatus.currentvalue))
               || ((PushButtonStatus_Neutral == pWCB_in_ButtonLogic->StopButtonStatus.previousValue)
               && (PushButtonStatus_Pushed == pWCB_in_ButtonLogic->StopButtonStatus.currentvalue)))
            {
               pFullAirControl_States->newValue = SM_WCB_FullAirControlBox_ParallelorM2;
            }
            //! #### Check WRUpButton, WRDownButtonStatus for state changes to 'ParallelorM2'
            else if (((EvalButtonRequest_Neutral == pWCB_in_ButtonLogic->WRUpButtonStatus.previousValue)
                    && ((EvalButtonRequest_EvaluatingPush == pWCB_in_ButtonLogic->WRUpButtonStatus.currentvalue)
                    || (EvalButtonRequest_ContinuouslyPushed == pWCB_in_ButtonLogic->WRUpButtonStatus.currentvalue)
                    || (EvalButtonRequest_ShortPush == pWCB_in_ButtonLogic->WRUpButtonStatus.currentvalue)))
                    || ((EvalButtonRequest_Neutral == pWCB_in_ButtonLogic->WRDownButtonStatus.previousValue)
                    && ((EvalButtonRequest_EvaluatingPush == pWCB_in_ButtonLogic->WRDownButtonStatus.currentvalue)
                    || (EvalButtonRequest_ContinuouslyPushed == pWCB_in_ButtonLogic->WRDownButtonStatus.currentvalue)
                    || (EvalButtonRequest_ShortPush == pWCB_in_ButtonLogic->WRDownButtonStatus.currentvalue))))
            {
               pFullAirControl_States->newValue = SM_WCB_FullAirControlBox_ParallelorM2;
            }
            //! #### Check SelectButton status for state changes to 'ParallelorM2'
            else if ((PushButtonStatus_Neutral == pFullair_SelectButtonStatus->previousValue)
                    && (PushButtonStatus_Pushed == pFullair_SelectButtonStatus->currentvalue))
            {
               pFullAirControl_States->newValue = SM_WCB_FullAirControlBox_ParallelorM2;
            }
            else
            {
               // Do nothing keep previous value
            }
         break;
      }    
      //! ##### Process full air control box selection output actions logic : 'Fullaircontrolboxselection_Outputlogic()'
      Fullaircontrolboxselection_Outputlogic(&pFullAirControl_States->newValue,
                                             pWiredControlBox_HMICtrl_out_data);
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Fullaircontrolboxselection_Outputlogic
//!
//! \param   *pnewValue                           Providing the current state value
//! \param   *pWiredControlBox_HMICtrl_out_data   Updating the output signals based on current state
//!
//!======================================================================================
static void Fullaircontrolboxselection_Outputlogic(const Wcb_Fullaircontrolbox_logic_Type        *pnewValue, 
                                                         WiredControlBox_HMICtrl_out_StructType  *pWiredControlBox_HMICtrl_out_data)
{
   //! ###### Processing output actions for current state
   switch (*pnewValue)
   {
      //! ##### Output actions for 'FrontorM1' state 
      case SM_WCB_FullAirControlBox_FrontorM1:
         pWiredControlBox_HMICtrl_out_data->M1_DeviceIndication       = DeviceIndication_On;
         pWiredControlBox_HMICtrl_out_data->M2_DeviceIndication       = DeviceIndication_Off;
         pWiredControlBox_HMICtrl_out_data->M3_DeviceIndication       = DeviceIndication_Off;
         pWiredControlBox_HMICtrl_out_data->WiredLevelUserMemory      = WiredLevelUserMemory_M1;
         pWiredControlBox_HMICtrl_out_data->WiredLevelAdjustmentAxles = LevelAdjustmentAxles_Front;
      break;
      //! ##### Output actions for 'ParallelorM2' state 
      case SM_WCB_FullAirControlBox_ParallelorM2:
         pWiredControlBox_HMICtrl_out_data->M1_DeviceIndication       = DeviceIndication_Off;
         pWiredControlBox_HMICtrl_out_data->M2_DeviceIndication       = DeviceIndication_On;
         pWiredControlBox_HMICtrl_out_data->M3_DeviceIndication       = DeviceIndication_Off;
         pWiredControlBox_HMICtrl_out_data->WiredLevelUserMemory      = WiredLevelUserMemory_M2;
         pWiredControlBox_HMICtrl_out_data->WiredLevelAdjustmentAxles = LevelAdjustmentAxles_Parallel;
      break;
      //! ##### Output actions for 'RearorM3' state 
      case SM_WCB_FullAirControlBox_RearorM3:
         pWiredControlBox_HMICtrl_out_data->M1_DeviceIndication       = DeviceIndication_Off;
         pWiredControlBox_HMICtrl_out_data->M2_DeviceIndication       = DeviceIndication_Off;
         pWiredControlBox_HMICtrl_out_data->M3_DeviceIndication       = DeviceIndication_On;
         pWiredControlBox_HMICtrl_out_data->WiredLevelUserMemory      = WiredLevelUserMemory_M3;
         pWiredControlBox_HMICtrl_out_data->WiredLevelAdjustmentAxles = LevelAdjustmentAxles_Rear;
      break;
      //! ##### Output actions for 'Default' state 
      default: 
         pWiredControlBox_HMICtrl_out_data->M1_DeviceIndication       = DeviceIndication_Off;
         pWiredControlBox_HMICtrl_out_data->M2_DeviceIndication       = DeviceIndication_Off;
         pWiredControlBox_HMICtrl_out_data->M3_DeviceIndication       = DeviceIndication_Off;
         pWiredControlBox_HMICtrl_out_data->WiredLevelUserMemory      = WiredLevelUserMemory_MemOff;
         pWiredControlBox_HMICtrl_out_data->WiredLevelAdjustmentAxles = LevelAdjustmentAxles_NotAvailable;
      break;
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the ECSStandbyrequestactivation
//!
//! \param   *pWCB_HMICtrl_in_Buttondata   Provides the input button status
//! \param   buttonStuck_rtrn              Providing the button status
//! \param   *pSelectbutton                Providing the select button status
//! \param   *pStop_Timer                  To check current timers value and update
//! \param   *pTimerECS                    To check current timers value and update  
//! \param   *pECSStandByReqRCECS          Update the value based on conditions
//!
//!======================================================================================
void ECSStandbyrequestactivation(const WiredControlBox_HMICtrl_in_ButtonStructType  *pWCB_HMICtrl_in_Buttondata,
                                 const uint8                                        buttonStuck_rtrn,
                                 const PushButtonStatus                             *pSelectbutton,
                                 const uint16                                       *pStop_Timer,
                                       uint16                                       *pTimerECS,
                                       ECSStandByReq_T                              *pECSStandByReqRCECS)
{
   //! ###### Processing the ECS standby request activation logic
   static uint8 ECS_flag = CONST_Resetflag;

   //! ##### Check for ECS_flag status
   if (CONST_Resetflag == ECS_flag)
   {
      *pECSStandByReqRCECS = ECSStandByReq_Idle;
   }
   else
   {
      // Do nothing: keep previous value
   }
   //! ##### Check for adjust, back, memory and select button status
   if (((PushButtonStatus_Neutral == pWCB_HMICtrl_in_Buttondata->AdjustButtonStatus.previousValue)
      && (PushButtonStatus_Pushed == pWCB_HMICtrl_in_Buttondata->AdjustButtonStatus.currentvalue))
      || ((PushButtonStatus_Neutral == pWCB_HMICtrl_in_Buttondata->BackButtonStatus.previousValue)
      && (PushButtonStatus_Pushed == pWCB_HMICtrl_in_Buttondata->BackButtonStatus.currentvalue))
      || ((PushButtonStatus_Neutral == pWCB_HMICtrl_in_Buttondata->MemButtonStatus.previousValue)
      && (PushButtonStatus_Pushed == pWCB_HMICtrl_in_Buttondata->MemButtonStatus.currentvalue))
      || ((PushButtonStatus_Neutral == pSelectbutton->previousValue)
      && (PushButtonStatus_Pushed == pSelectbutton->currentvalue)))
   {
      *pECSStandByReqRCECS = ECSStandByReq_StandbyRequested;
      *pTimerECS           = PCODE_RCECS_HoldCircuitTimer;
      ECS_flag             = CONST_Setflag;
   }
   //! ##### Check for WRUp and WRDown button status
   else if (((EvalButtonRequest_Neutral == pWCB_HMICtrl_in_Buttondata->WRUpButtonStatus.previousValue)
           && ((EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_Buttondata->WRUpButtonStatus.currentvalue)
           || (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_Buttondata->WRUpButtonStatus.currentvalue)
           || (EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_Buttondata->WRUpButtonStatus.currentvalue)))
           || ((EvalButtonRequest_Neutral == pWCB_HMICtrl_in_Buttondata->WRDownButtonStatus.previousValue)
           && ((EvalButtonRequest_EvaluatingPush == pWCB_HMICtrl_in_Buttondata->WRDownButtonStatus.currentvalue)
           || (EvalButtonRequest_ContinuouslyPushed == pWCB_HMICtrl_in_Buttondata->WRDownButtonStatus.currentvalue)
           || (EvalButtonRequest_ShortPush == pWCB_HMICtrl_in_Buttondata->WRDownButtonStatus.currentvalue))))
   {
      *pECSStandByReqRCECS = ECSStandByReq_StandbyRequested;
      *pTimerECS           = PCODE_RCECS_HoldCircuitTimer;
      ECS_flag             = CONST_Setflag;
   }
   else
   {
      // Do nothing: keep previous value
   }
   //! ##### Check for StopButton status
   if ((PushButtonStatus_Neutral == pWCB_HMICtrl_in_Buttondata->StopButtonStatus.previousValue)
      && (PushButtonStatus_Pushed == pWCB_HMICtrl_in_Buttondata->StopButtonStatus.currentvalue))
   {
      ECS_flag = CONST_Setflag;
   }
   //! ##### Check for ECS flag and stop timer values
   else if ((CONST_Setflag == ECS_flag)
           && (*pStop_Timer < (uint16)(PCODE_RCECSButtonStucked - ((uint16)PCODE_ECSStopButtonHoldTimeout))))
   {
      *pECSStandByReqRCECS = ECSStandByReq_StopStandby;
      *pTimerECS           = PCODE_RCECS_HoldCircuitTimer;
      ECS_flag             = 4U;
   }
   else
   {
      // Do nothing: keep previous value
   }
   //! ##### Check for button stuck return value
   if (CONST_ButtonStucked == buttonStuck_rtrn)
   {
      *pECSStandByReqRCECS = ECSStandByReq_Error;
      *pTimerECS           = CONST_TimerFunctionInactive;
      ECS_flag             = 3U; 
   }
   //! ##### Check for any button status is equal to Error
   else if ((PushButtonStatus_Error == pWCB_HMICtrl_in_Buttondata->AdjustButtonStatus.currentvalue)
           || (PushButtonStatus_Error == pWCB_HMICtrl_in_Buttondata->BackButtonStatus.currentvalue)
           || (PushButtonStatus_Error == pWCB_HMICtrl_in_Buttondata->MemButtonStatus.currentvalue)
           || (EvalButtonRequest_Error == pWCB_HMICtrl_in_Buttondata->WRDownButtonStatus.currentvalue)
           || (EvalButtonRequest_Error == pWCB_HMICtrl_in_Buttondata->WRUpButtonStatus.currentvalue)
           || (PushButtonStatus_Error == pWCB_HMICtrl_in_Buttondata->StopButtonStatus.currentvalue)
           || (PushButtonStatus_Error == pSelectbutton->currentvalue))
   {
      *pECSStandByReqRCECS = ECSStandByReq_Error;
      *pTimerECS           = CONST_TimerFunctionInactive;
      ECS_flag             = CONST_Resetflag;
   }
   //! ##### Check for button status
   else if ((((pWCB_HMICtrl_in_Buttondata->WRUpButtonStatus.currentvalue != pWCB_HMICtrl_in_Buttondata->WRUpButtonStatus.previousValue)
           && (EvalButtonRequest_Neutral == pWCB_HMICtrl_in_Buttondata->WRUpButtonStatus.currentvalue))
           || ((pWCB_HMICtrl_in_Buttondata->WRDownButtonStatus.currentvalue != pWCB_HMICtrl_in_Buttondata->WRDownButtonStatus.previousValue)
           && (EvalButtonRequest_Neutral == pWCB_HMICtrl_in_Buttondata->WRDownButtonStatus.currentvalue))
           || ((pWCB_HMICtrl_in_Buttondata->AdjustButtonStatus.currentvalue != pWCB_HMICtrl_in_Buttondata->AdjustButtonStatus.previousValue)
           && (PushButtonStatus_Neutral == pWCB_HMICtrl_in_Buttondata->AdjustButtonStatus.currentvalue))
           || ((pWCB_HMICtrl_in_Buttondata->BackButtonStatus.currentvalue != pWCB_HMICtrl_in_Buttondata->BackButtonStatus.previousValue)
           && (PushButtonStatus_Neutral == pWCB_HMICtrl_in_Buttondata->BackButtonStatus.currentvalue))
           || ((pWCB_HMICtrl_in_Buttondata->MemButtonStatus.currentvalue != pWCB_HMICtrl_in_Buttondata->MemButtonStatus.previousValue)
           && (PushButtonStatus_Neutral == pWCB_HMICtrl_in_Buttondata->MemButtonStatus.currentvalue))
           || ((pWCB_HMICtrl_in_Buttondata->StopButtonStatus.currentvalue != pWCB_HMICtrl_in_Buttondata->StopButtonStatus.previousValue)
           && (PushButtonStatus_Neutral == pWCB_HMICtrl_in_Buttondata->StopButtonStatus.currentvalue))
           || ((pSelectbutton->currentvalue != pSelectbutton->previousValue)
           && (PushButtonStatus_Neutral == pSelectbutton->currentvalue)))
           && (3U == ECS_flag))
   {
      *pECSStandByReqRCECS = ECSStandByReq_Idle;
      ECS_flag             = CONST_Resetflag;
   }
   else
   {
      // Do nothing: keep previous value
   }
   //! ##### Check for ECS timer value
   if (CONST_TimerFunctionElapsed == *pTimerECS)
   {
      *pECSStandByReqRCECS = ECSStandByReq_Idle;
      ECS_flag             = CONST_Resetflag;
   }
   else
   {
      // Do nothing:keep previous value
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the FunctionButtonStucked
//!
//! \param   *pWCB_input_Buttondata            Providing the button status
//! \param   *pSelectButtonStatus              Providing the select button status
//! \param   pTimer                            To check current timers value and update
//! \param   *pWiredAirSuspensionStopRequest   Updating the stop request value
//! \param   *pWiredLevelAdjustmentStroke      Updating the adjustment stroke value
//! \param   *pWiredLevelAdjustmentAction      Updating the adjustment action value
//! \param   *pWiredLevelUserMemoryAction      Updating the user memory action value
//! \param   *pWiredLevelUserMemory            Updating the user memory value
//! \param   *pWiredLevelAdjustmentAxles       Updating the adjustment axles value
//!
//! \return   buttonStuck_rtrn                 Returns the buttonStuck return value
//!
//!======================================================================================
uint8 FunctionButtonStucked(const WiredControlBox_HMICtrl_in_ButtonStructType  *pWCB_input_Buttondata,
                            const PushButtonStatus                             *pSelectButtonStatus,
                                  uint16                                       pTimer[CONST_NbOfTimers],
                                  FalseTrue_T                                  *pWiredAirSuspensionStopRequest,
                                  LevelAdjustmentStroke_T                      *pWiredLevelAdjustmentStroke,
                                  LevelAdjustmentAction_T                      *pWiredLevelAdjustmentAction,
                                  LevelUserMemoryAction_T                      *pWiredLevelUserMemoryAction,
                                  WiredLevelUserMemory_T                       *pWiredLevelUserMemory,
                                  LevelAdjustmentAxles_T                       *pWiredLevelAdjustmentAxles)
{
   //! ###### Processing the button stucked logic
   uint8  buttonStuck_rtrn        = 0U;
   uint8  buttonstuckcheck        = 0U;
   uint8  isButtonStucked         = 0U;
   static ButtonSetReset stuck[7] = { Reset, Reset, Reset, Reset, Reset, Reset, Reset};
   
   //! ##### Check for stop button status
   ButtonStuckLogicForPushButtons(&pWCB_input_Buttondata->StopButtonStatus,
                                  &pTimer[CONST_StopButtonTimer],
                                  &stuck[CONST_StopButtonTimer]);
   if (Button_set == stuck[0])
   {
      *pWiredAirSuspensionStopRequest = FalseTrue_Error;
   }
   else
   {
      //Do:Nothing keep previous value
   }
   if ((Button_set == stuck[1])
	   && (PushButtonStatus_Neutral == pWCB_input_Buttondata->AdjustButtonStatus.currentvalue))
   {
	   *pWiredLevelAdjustmentStroke = LevelAdjustmentStroke_DockingStroke;
   }
   else
   {
      //Do:Nothing
   }
   //! ##### Check for the adjust button status
   ButtonStuckLogicForPushButtons(&pWCB_input_Buttondata->AdjustButtonStatus,
                                  &pTimer[CONST_AdjustButtonTimer],
                                  &stuck[CONST_AdjustButtonTimer]);
   if (Button_set == stuck[1])
   {
      *pWiredLevelAdjustmentStroke = LevelAdjustmentStroke_Error;
   }
   else
   {
      //Do:Nothing keep previous value
   }
   //! ##### Check for the back button status
   ButtonStuckLogicForPushButtons(&pWCB_input_Buttondata->BackButtonStatus,
                                  &pTimer[CONST_BackButtonTimer],
                                  &stuck[CONST_BackButtonTimer]);
   if (Button_set == stuck[2])
   {
      *pWiredLevelAdjustmentAction = LevelAdjustmentAction_Error;
   }
   else
   {
      //Do:Nothing keep previous value
   }
   if ((Button_set == stuck[4])
	   && (PushButtonStatus_Neutral == pWCB_input_Buttondata->MemButtonStatus.currentvalue))
   {
	  *pWiredLevelUserMemoryAction = LevelUserMemoryAction_Inactive;
   }
   else
   {
      //Do:Nothing
   }
   //! ##### Check for the memory button status
   ButtonStuckLogicForPushButtons(&pWCB_input_Buttondata->MemButtonStatus,
                                  &pTimer[CONST_MemButtonTimer],
                                  &stuck[CONST_MemButtonTimer]);
   if (Button_set == stuck[4])
   {
      *pWiredLevelUserMemoryAction = LevelUserMemoryAction_Error;
   }
   else
   {
      //Do:Nothing keep previous value
   }
   //! ##### Check for the WRUp button status
   ButtonStuckLogicForWrUpDownButton(&pWCB_input_Buttondata->WRUpButtonStatus,
                                     &pTimer[CONST_WRUpButtonTimer],
                                     &stuck[CONST_WRUpButtonTimer]);
   if (Button_set == stuck[5])
   {
      *pWiredLevelAdjustmentAction = LevelAdjustmentAction_Error;
   }
   else
   {
      //Do:Nothing keep previous value
   }
   //! ##### Check for the WRDown button status
   //! ##### Check for the WRUp button status
   ButtonStuckLogicForWrUpDownButton(&pWCB_input_Buttondata->WRDownButtonStatus,
                                     &pTimer[CONST_WRDownButtonTimer],
                                     &stuck[CONST_WRDownButtonTimer]);
   if (Button_set == stuck[6])
   {
      *pWiredLevelAdjustmentAction = LevelAdjustmentAction_Error;
   }
   else
   {
      //Do:Nothing keep previous value
   }
   if ((Button_set == stuck[3])
	   && (PushButtonStatus_Neutral == pSelectButtonStatus->currentvalue))
   {
	   *pWiredLevelUserMemory      = WiredLevelUserMemory_MemOff;
      *pWiredLevelAdjustmentAxles = LevelAdjustmentAxles_Parallel;
   }
   else
   {
      //Do:Nothing
   }
   //! ##### Check for the select button status
   ButtonStuckLogicForPushButtons(pSelectButtonStatus,
                                  &pTimer[CONST_SelectButtonTimer],
                                  &stuck[CONST_SelectButtonTimer]);
   if (Button_set == stuck[3])
   {
      *pWiredLevelUserMemory      = WiredLevelUserMemory_Error;
      *pWiredLevelAdjustmentAxles = LevelAdjustmentAxles_Error;
   }
   else
   {
      //Do:Nothing keep previous value
   }
   for (buttonstuckcheck = 0U;buttonstuckcheck <= 6U;buttonstuckcheck++)
   {
      if (stuck[buttonstuckcheck] == Button_set)
      {
         isButtonStucked  = 1U;
         buttonStuck_rtrn = CONST_ButtonStucked;
         (void)Rte_Call_Event_D1BOI_63_RCECS_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         // Do Nothing
      }
   }
   if (isButtonStucked == 0U)
   {
      (void)Rte_Call_Event_D1BOI_63_RCECS_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else
   {
      //Do:Nothing keep previous value
   }
   return buttonStuck_rtrn;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the ButtonStuckLogicForPushButtons,
//!
//! \param   *pinputButton       Providing the input button status to process functionality
//! \param   *pTimer             Activating Buttonstuck timer if any button changes to 'Pushed'
//! \param   *pStuck             Temporary variable to store data for buttonstuck
//!
//!======================================================================================
static void ButtonStuckLogicForPushButtons(const PushButtonStatus *pinputButton,
                                                 uint16           *pTimer,
                                                 ButtonSetReset   *pStuck)
{
   //! ##### Check for input button's status
   if (PushButtonStatus_Neutral == pinputButton->currentvalue)
   {
      *pStuck = Reset;
      *pTimer = CONST_TimerFunctionInactive;
   }
   else if (PushButtonStatus_Pushed == pinputButton->currentvalue)
   {
      if (pinputButton->previousValue != pinputButton->currentvalue)
      {
         *pTimer = PCODE_RCECSButtonStucked;
         *pStuck = set;
      } 
      else
      {
         // Check for the button stuck timer is elapsed
         if ((CONST_TimerFunctionInactive == *pTimer)
            && (*pStuck == set))
         {
            *pStuck = Button_set;
         }
         else
         {
            // Do nothing:keep previous value
         }
      }
   }
   else
   {
      *pTimer = CONST_TimerFunctionInactive;
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the ButtonStuckLogicForWrUpDownButton,
//!
//! \param   *pinputButton       Providing the input button status to process functionality
//! \param   *pTimer             Activating Buttonstuck timer if any button changes to 'Pushed'
//! \param   *pStuck             Temporary variable to store data for buttonstuck
//!
//!======================================================================================
static void ButtonStuckLogicForWrUpDownButton(const EvalButtonRequest *pinputButton,
                                                    uint16            *pTimer,
                                                    ButtonSetReset    *pStuck)
{
   //! ##### Check for the WRUp button status
   if (EvalButtonRequest_Neutral == pinputButton->currentvalue)
   {
      *pStuck = Reset;
      *pTimer = CONST_TimerFunctionInactive;
   }
   else if ((EvalButtonRequest_ShortPush == pinputButton->currentvalue)
           || (EvalButtonRequest_ContinuouslyPushed == pinputButton->currentvalue)
           || (EvalButtonRequest_EvaluatingPush == pinputButton->currentvalue))
   { 
      if (pinputButton->previousValue != pinputButton->currentvalue)
      {
         *pTimer = PCODE_RCECSUpDownStucked; 
         *pStuck = set;
      }
      else
      {
         // Check for the button stuck timer is elapsed
         if ((CONST_TimerFunctionInactive == *pTimer)
            && (set == *pStuck)) 
         {
            *pStuck = Button_set;
         }
         else
         {
            // Do nothing: keep previous value
         }
      }
   }
   else
   {
      *pTimer = CONST_TimerFunctionInactive;
   }
}
//! @}
//! @}
//! @}
