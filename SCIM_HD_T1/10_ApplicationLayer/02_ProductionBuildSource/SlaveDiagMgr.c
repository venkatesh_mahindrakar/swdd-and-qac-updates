/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  SlaveDiagMgr.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  SlaveDiagMgr
 *  Generation Time:  2020-10-07 15:25:42
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <SlaveDiagMgr>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Int8Bit_T
 *   255 Not Available
 *
 *********************************************************************************************************************/

#include "Rte_SlaveDiagMgr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "FuncLibrary_ScimStd_If.h"
#include "SlaveDiagMgr.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DiagInfo_T: Integer in interval [0...127]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * Int8Bit_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 * Record Types:
 * =============
 * SRS2_SNPN_T: Record with elements
 *   Byte0_RE of type Int8Bit_T
 *   Byte1_RE of type Int8Bit_T
 *   Byte2_RE of type Int8Bit_T
 *   Byte3_RE of type Int8Bit_T
 *   Byte4_RE of type Int8Bit_T
 * SRS2_SN_T: Record with elements
 *   Byte0_RE of type Int8Bit_T
 *   Byte1_RE of type Int8Bit_T
 *   Byte2_RE of type Int8Bit_T
 *   Byte3_RE of type Int8Bit_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1M7Q_EraGlonassUnitInstalled_v(void)
 *
 *********************************************************************************************************************/


#define SlaveDiagMgr_START_SEC_CODE
#include "SlaveDiagMgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_P1M7Q_EraGlonassUnitInstalled      (Rte_Prm_P1M7Q_EraGlonassUnitInstalled_v())
/**********************************************************************************************************************
 *
 * Runnable Entity Name: SlaveDiagMgr_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_DiagInfoERAU_DiagInfo(DiagInfo_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1D72_44_EraU_DataMemoryFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1D72_45_EraU_ProgramMemoryFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1D72_46_EraU_ParameterMemoryFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1D72_49_EraU_InternalElectronicFailure49_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1D72_4A_EraU_IncorrectComponentInstalled_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1D72_55_EraU_NotConfigured_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1D72_92_EraU_PerfIncorrectOperation_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1D72_96_EraU_ComponentInternalFailure96_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1D72_9A_EraU_CompOrSysOperatingCond_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1D73_01_EraU_GpsAntennaElecFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1D74_01_EraU_GsmAntennaElecFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1D75_01_EraU_MicroElecFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1D76_01_EraU_SpeakerElecFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E8B_01_EraU_BatteryFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SlaveDiagMgr_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SlaveDiagMgr_CODE) SlaveDiagMgr_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SlaveDiagMgr_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input data structures
   static SlaveDiagMgr_in_StructType  RteInData_Common; 
   Std_ReturnType retvalue = RTE_E_INVALID;   
   retvalue = Rte_Read_DiagInfoERAU_DiagInfo(&RteInData_Common.DiagInfoERAU);
   MACRO_StdRteRead_ExtRPort((retvalue), (RteInData_Common.DiagInfoERAU), (0U), (0U))
   retvalue = Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&RteInData_Common.SwcActivation_IgnitionOn);
   MACRO_StdRteRead_IntRPort((retvalue),(RteInData_Common.SwcActivation_IgnitionOn),(NonOperational))
   //! ##### Check for vehicle mode for IgnitionOn
   if (Operational == RteInData_Common.SwcActivation_IgnitionOn)
   {
       //! #### Select 'EraGlonassUnitInstalled' parameter and managing fault code information
      if (TRUE == PCODE_P1M7Q_EraGlonassUnitInstalled)
      {                    
         SlaveDiagMgr_FCI_Indication(&RteInData_Common.DiagInfoERAU);                      
      }
      else
      {         
         // ERA-GLONASSEmCallInstalled is not installed
      }
   }
   else
   {
      // Vehicle mode is in non-operational mode
   }

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define SlaveDiagMgr_STOP_SEC_CODE
#include "SlaveDiagMgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 //!======================================================================================
 //!
 //! \brief
 //! This function implements the logic for the 'SlaveDiagMgr_FCI_Indication'
 //!
 //! \param   *pDiagInfoERAU   Provides the DiagInfoERAU value
 //!
 //!======================================================================================
static void SlaveDiagMgr_FCI_Indication(const DiagInfo_T *pDiagInfoERAU)
{      
   //! ###### Check the FCI conditions and log or remove the faults
   if ((DiagInfo_T)0x51 == *pDiagInfoERAU)
   {
      //! ##### Set supply voltage too low fault event to fail
      Rte_Call_Event_D1E8B_01_EraU_BatteryFailure_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x11 == *pDiagInfoERAU)
   {
      //! ##### Set supply voltage too low fault event to pass
      Rte_Call_Event_D1E8B_01_EraU_BatteryFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x42 == *pDiagInfoERAU)
   {
      //! ##### Set supply voltage too high fault event to fail
      Rte_Call_Event_D1D72_44_EraU_DataMemoryFailure_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x02 == *pDiagInfoERAU)
   {
      //! ##### Set supply voltage too high fault event to pass
      Rte_Call_Event_D1D72_44_EraU_DataMemoryFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else if ((DiagInfo_T)0x43 == *pDiagInfoERAU)
   {
      //! ##### Set ROM CS error fault event to fail
      Rte_Call_Event_D1D72_45_EraU_ProgramMemoryFailure_SetEventStatus(DEM_EVENT_STATUS_FAILED);
   }
   else if ((DiagInfo_T)0x03 == *pDiagInfoERAU)
   {
      //! ##### Set ROM CS error fault event to pass
      Rte_Call_Event_D1D72_45_EraU_ProgramMemoryFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);       
   }
   else if ((DiagInfo_T)0x44 == *pDiagInfoERAU)
   {
      //! ##### Set RAM check fault event to fail
      Rte_Call_Event_D1D72_46_EraU_ParameterMemoryFailure_SetEventStatus(DEM_EVENT_STATUS_FAILED);       
   }
   else if ((DiagInfo_T)0x04 == *pDiagInfoERAU)
   {
      //! ##### Set RAM check fault event to pass
      Rte_Call_Event_D1D72_46_EraU_ParameterMemoryFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);       
   }
   else if ((DiagInfo_T)0x41 == *pDiagInfoERAU)
   {
      //! ##### Set watchdog error fault event to fail
      Rte_Call_Event_D1D72_49_EraU_InternalElectronicFailure49_SetEventStatus(DEM_EVENT_STATUS_FAILED);      
   }
   else if ((DiagInfo_T)0x01 == *pDiagInfoERAU)
   {
      //! ##### Set watchdog error fault event to pass
      Rte_Call_Event_D1D72_49_EraU_InternalElectronicFailure49_SetEventStatus(DEM_EVENT_STATUS_PASSED);       
   }
   else if ((DiagInfo_T)0x5B == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to fail
      Rte_Call_Event_D1D72_4A_EraU_IncorrectComponentInstalled_SetEventStatus(DEM_EVENT_STATUS_FAILED);       
   }
   else if ((DiagInfo_T)0x1B == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1D72_4A_EraU_IncorrectComponentInstalled_SetEventStatus(DEM_EVENT_STATUS_PASSED);       
   }    
   else if ((DiagInfo_T)0x50 == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to fail
      Rte_Call_Event_D1D72_55_EraU_NotConfigured_SetEventStatus(DEM_EVENT_STATUS_FAILED);        
   }
   else if ((DiagInfo_T)0x10 == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1D72_55_EraU_NotConfigured_SetEventStatus(DEM_EVENT_STATUS_PASSED);        
   }
   else if ((DiagInfo_T)0x53 == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to fail
      Rte_Call_Event_D1D72_92_EraU_PerfIncorrectOperation_SetEventStatus(DEM_EVENT_STATUS_FAILED);      
   }
   else if ((DiagInfo_T)0x13 == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1D72_92_EraU_PerfIncorrectOperation_SetEventStatus(DEM_EVENT_STATUS_PASSED);       
   }
   else if ((DiagInfo_T)0x52 == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to fail
      Rte_Call_Event_D1D72_96_EraU_ComponentInternalFailure96_SetEventStatus(DEM_EVENT_STATUS_FAILED);       
   }
   else if ((DiagInfo_T)0x12 == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1D72_96_EraU_ComponentInternalFailure96_SetEventStatus(DEM_EVENT_STATUS_PASSED);        
   }
   else if ((DiagInfo_T)0x55 == *pDiagInfoERAU)
   {
     //! ##### Set overrun fault event to fail
      Rte_Call_Event_D1D72_9A_EraU_CompOrSysOperatingCond_SetEventStatus(DEM_EVENT_STATUS_FAILED);      
   }
   else if ((DiagInfo_T)0x15 == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1D72_9A_EraU_CompOrSysOperatingCond_SetEventStatus(DEM_EVENT_STATUS_PASSED);      
   }
   else if ((DiagInfo_T)0x58 == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to fail
      Rte_Call_Event_D1D73_01_EraU_GpsAntennaElecFailure_SetEventStatus(DEM_EVENT_STATUS_FAILED);       
   }
   else if ((DiagInfo_T)0x18 == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1D73_01_EraU_GpsAntennaElecFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);       
   }
   else if ((DiagInfo_T)0x59 == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to fail
      Rte_Call_Event_D1D74_01_EraU_GsmAntennaElecFailure_SetEventStatus(DEM_EVENT_STATUS_FAILED);       
   }
   else if ((DiagInfo_T)0x19 == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1D74_01_EraU_GsmAntennaElecFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);       
   }
   else if ((DiagInfo_T)0x5C == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to fail
      Rte_Call_Event_D1D75_01_EraU_MicroElecFailure_SetEventStatus(DEM_EVENT_STATUS_FAILED);        
   }
   else if ((DiagInfo_T)0x1C == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1D75_01_EraU_MicroElecFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);    
   }
   else if ((DiagInfo_T)0x5E == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to fail
      Rte_Call_Event_D1D76_01_EraU_SpeakerElecFailure_SetEventStatus(DEM_EVENT_STATUS_FAILED);       
   }
   else if ((DiagInfo_T)0X1E == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1D76_01_EraU_SpeakerElecFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);    
   }  
   else if ((DiagInfo_T)0x00 == *pDiagInfoERAU)
   {
      //! ##### Set overrun fault event to pass
      Rte_Call_Event_D1E8B_01_EraU_BatteryFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1D72_44_EraU_DataMemoryFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1D72_45_EraU_ProgramMemoryFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1D72_46_EraU_ParameterMemoryFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1D72_49_EraU_InternalElectronicFailure49_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1D72_4A_EraU_IncorrectComponentInstalled_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1D72_55_EraU_NotConfigured_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1D72_92_EraU_PerfIncorrectOperation_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1D72_96_EraU_ComponentInternalFailure96_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1D72_9A_EraU_CompOrSysOperatingCond_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1D73_01_EraU_GpsAntennaElecFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1D74_01_EraU_GsmAntennaElecFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1D75_01_EraU_MicroElecFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      Rte_Call_Event_D1D76_01_EraU_SpeakerElecFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   else
   {
      // Do nothing, keep previous status
   }  
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
