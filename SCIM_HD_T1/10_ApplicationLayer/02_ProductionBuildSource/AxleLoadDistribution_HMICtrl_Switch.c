/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file AxleLoadDistribution_HMICtrl_Switch.c
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_VehicleDynamics_Suspension 
//! @{
//! @addtogroup AxleLoadDistribution_HMICtrl
//! @{
//!
//! \brief
//! AxleLoadDistribution_HMICtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the AxleLoadDistribution_HMICtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "Rte_AxleLoadDistribution_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "AxleLoadDistribution_HMICtrl_If.h"
#include "AxleLoadDistribution_HMICtrl_Switch.h"
#include "AxleLoadDistribution_HMICtrl_Switch_If.h"
#include "FuncLibrary_Timer_If.h"

#define PCODE_P1KN4_AxleLoad_CRideLEDlowerEnd          (Rte_Prm_P1KN4_AxleLoad_CRideLEDlowerEnd_v())
#define PCODE_P1KN5_AxleLoad_CRideLEDIndicationType    (Rte_Prm_P1KN5_AxleLoad_CRideLEDIndicationType_v())
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'LiftAxlePositionSwitchTransition'
//!
//! \param    *pLiftAxle1Switch2_Status   Provides the current switch status
//! \param    *pLiftAxle1UpRequestACK     Indicates the current status is no action or not
//! \param    *pTimer_LA1UpReqACK         To check current timer value and update
//! \param    *pTimer_LA1S2ButtonStuck    To check current timer value and update
//! \param    *pinitialize_LAPS           Providing and updating the LiftAxlePositionSwitch initialization status
//! \param    *pLAPSwitchSM               Providing and updating the present LiftAxlePositionSwitch state
//!
//!======================================================================================
void LiftAxlePositionSwitchTransition(      A3PosSwitchStatus      *pLiftAxle1Switch2_Status,
                                      const LiftAxleUpRequestACK_T *pLiftAxle1UpRequestACK,
                                            uint16                 *pTimer_LA1UpReqACK,
                                            uint16                 *pTimer_LA1S2ButtonStuck,
                                            uint8                  *pinitialize_LAPS,
                                            AldLAPS_States         *pLAPSwitchSM)
{
   //! ###### Processing lift axle position switch transition logic
   static uint8 liftAxlePosBS_Flag    = CONST_ResetFlag;
   uint8 current_state                = CONST_ResetFlag;
   pLAPSwitchSM->currentValue         = pLAPSwitchSM->newValue;
   
   if (CONST_NonInitialize == *pinitialize_LAPS)
   {
      pLiftAxle1Switch2_Status->previousValue = pLiftAxle1Switch2_Status->currentValue;
      *pTimer_LA1UpReqACK                     = CONST_TimerFunctionInactive;
      *pTimer_LA1S2ButtonStuck                = CONST_TimerFunctionInactive;
      liftAxlePosBS_Flag                      = CONST_ResetFlag;
      pLAPSwitchSM->newValue                  = SM_LAPS_Default;
      *pinitialize_LAPS                       = CONST_Initialize;
   }
   else
   {
      //! ##### Select the state based on 'pLAPSwitchSM->newValue'
      switch (pLAPSwitchSM->newValue)
      {
         //! ##### Processing conditions for state change from lift state
         case SM_LAPS_Lift:
            //! #### Check for LiftAxle1Switch2 and LiftAxle1UpRequestACK status
            //! #### Check for state change to 'LAPS_Down'(Down switch event)
            if ((A3PosSwitchStatus_Middle == pLiftAxle1Switch2_Status->previousValue)
               && (A3PosSwitchStatus_Lower == pLiftAxle1Switch2_Status->currentValue))
            {
               pLAPSwitchSM->newValue   = SM_LAPS_Down;
			   *pTimer_LA1S2ButtonStuck = CONST_ButtonStuckTimerPeriod;
			   liftAxlePosBS_Flag       = CONST_SetFlag;
            }
            //! #### Check for state change to 'LAPS_Default'(ChangeAcknowledge or NoAcknowledge event)
            else if ((LiftAxleUpRequestACK_NoAction != *pLiftAxle1UpRequestACK)
                    || (CONST_TimerFunctionElapsed == *pTimer_LA1UpReqACK))
            {
               // Cancel No Ack Timer
               *pTimer_LA1UpReqACK    = CONST_TimerFunctionInactive;
               pLAPSwitchSM->newValue = SM_LAPS_Default;
            }
            //! #### Check for state change to 'LAPS_Fallback'(Error detection event)
            else if ((pLiftAxle1Switch2_Status->previousValue != pLiftAxle1Switch2_Status->currentValue)
                    && ((A3PosSwitchStatus_Error == pLiftAxle1Switch2_Status->currentValue)
                    || (A3PosSwitchStatus_NotAvailable == pLiftAxle1Switch2_Status->currentValue)
                    || (A3PosSwitchStatus_Spare == pLiftAxle1Switch2_Status->currentValue)
                    || (A3PosSwitchStatus_Spare_01 == pLiftAxle1Switch2_Status->currentValue)
                    || (A3PosSwitchStatus_Spare_02 == pLiftAxle1Switch2_Status->currentValue)))
            {
               pLAPSwitchSM->newValue = SM_LAPS_Fallback;
            }
            else
            {
               // Do nothing: keep previous status until the timeout occurres
            }
         break;
         //! ##### Processing conditions for state change from Down state.
         case SM_LAPS_Down:
            //! #### Check for LiftAxle1Switch2 status
            //! #### Check for state change to 'LAPS_Lift'(Up switch event)
            if ((A3PosSwitchStatus_Middle == pLiftAxle1Switch2_Status->previousValue)
               && (A3PosSwitchStatus_Upper == pLiftAxle1Switch2_Status->currentValue))
            {
               // Start NoACK Timer
               *pTimer_LA1UpReqACK    = CONST_Axleload_Timeout;
               pLAPSwitchSM->newValue = SM_LAPS_Lift;
            }
            //! #### Check for state change to 'LAPS_Fallback'(Error detection event)
            else if ((pLiftAxle1Switch2_Status->previousValue != pLiftAxle1Switch2_Status->currentValue)
                    && ((A3PosSwitchStatus_Error == pLiftAxle1Switch2_Status->currentValue)
                    || (A3PosSwitchStatus_NotAvailable == pLiftAxle1Switch2_Status->currentValue)
                    || (A3PosSwitchStatus_Spare == pLiftAxle1Switch2_Status->currentValue)
                    || (A3PosSwitchStatus_Spare_01 == pLiftAxle1Switch2_Status->currentValue)
                    || (A3PosSwitchStatus_Spare_02 == pLiftAxle1Switch2_Status->currentValue)))
            {
               pLAPSwitchSM->newValue = SM_LAPS_Fallback;
            }
            else
            {
               // Do nothing: keep previous status until the timeout occurres
            }
            //! #### Process the A3_ButtonStuckEvent (Button stuck event): 'A3_ButtonStuckEvent()'
            current_state = A3_ButtonStuckEvent(pLiftAxle1Switch2_Status,
                                                pTimer_LA1S2ButtonStuck,
                                                &liftAxlePosBS_Flag);
         break;
            //! ##### Processing conditions for state change from Fallback state.
         case SM_LAPS_Fallback:
            //! #### Check for LiftAxle1Switch2 status 
            //! #### Check for state change to 'LAPS_Down'(Solve error event).
            if ((pLiftAxle1Switch2_Status->previousValue != pLiftAxle1Switch2_Status->currentValue)
               && (A3PosSwitchStatus_Error != pLiftAxle1Switch2_Status->currentValue)
               && (A3PosSwitchStatus_NotAvailable != pLiftAxle1Switch2_Status->currentValue)
               && (A3PosSwitchStatus_Spare != pLiftAxle1Switch2_Status->currentValue)
               && (A3PosSwitchStatus_Spare_01 != pLiftAxle1Switch2_Status->currentValue)
               && (A3PosSwitchStatus_Spare_02 != pLiftAxle1Switch2_Status->currentValue))
            {
               pLAPSwitchSM->newValue = SM_LAPS_Down;
               Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            }
            else
            {
               // Do nothing: keep previous status
            }
            //! #### Process the A3_ButtonStuckEvent (Button stuck event): 'A3_ButtonStuckEvent()'
            current_state = A3_ButtonStuckEvent(pLiftAxle1Switch2_Status,
                                                pTimer_LA1S2ButtonStuck,
                                                &liftAxlePosBS_Flag);
         break;
            //! ##### Processing conditions for state change from Default state
         default: // SM_LAPS_Default
            //! #### Check for LiftAxle1Switch2 Status 
            //! #### Check for state change to 'LAPS_Down'(Down Switch event)
            if ((A3PosSwitchStatus_Middle == pLiftAxle1Switch2_Status->previousValue)
               && (A3PosSwitchStatus_Lower == pLiftAxle1Switch2_Status->currentValue))
            {
               pLAPSwitchSM->newValue   = SM_LAPS_Down;
            }
            //! #### Check for state change to 'LAPS_Lift'(Up switch event)
            else if ((A3PosSwitchStatus_Middle == pLiftAxle1Switch2_Status->previousValue)
                    && (A3PosSwitchStatus_Upper == pLiftAxle1Switch2_Status->currentValue))
            {
               // Start NoACK Timer
               *pTimer_LA1UpReqACK      = CONST_Axleload_Timeout;
               pLAPSwitchSM->newValue   = SM_LAPS_Lift;
            }
            //! #### Check for state change to 'LAPS_Fallback'(Error detection event)
            else if ((pLiftAxle1Switch2_Status->previousValue != pLiftAxle1Switch2_Status->currentValue)
                    && ((A3PosSwitchStatus_Error == pLiftAxle1Switch2_Status->currentValue)
                    /*|| (A3PosSwitchStatus_NotAvailable == pLiftAxle1Switch2_Status->currentValue)*/ //currently we are not considering this condition due to issue in FSP need to discuss with customer
                    || (A3PosSwitchStatus_Spare == pLiftAxle1Switch2_Status->currentValue)
                    || (A3PosSwitchStatus_Spare_01 == pLiftAxle1Switch2_Status->currentValue)
                    || (A3PosSwitchStatus_Spare_02 == pLiftAxle1Switch2_Status->currentValue)))
            {
               pLAPSwitchSM->newValue   = SM_LAPS_Fallback;
            }
            else
            {
               //Do nothing, keep previous status
            }
            //! #### Process the A3_ButtonStuckEvent (Button stuck event): 'A3_ButtonStuckEvent()'
            current_state = A3_ButtonStuckEvent(pLiftAxle1Switch2_Status,
                                                pTimer_LA1S2ButtonStuck,
                                                &liftAxlePosBS_Flag);
         break;
      }
      if (CONST_SetFlag == current_state)
      {
         pLAPSwitchSM->newValue = SM_LAPS_Fallback;
      }
      else
      {
         // Do nothing: keep previous status
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'LiftAxlePositionSwitchOutputProcessing'
//!
//! \param   *pLAPSwitchSM                    Providing the current state value
//! \param   *pLiftAxle1Switch2_Status        Specifies the current Switch status
//! \param   *pLiftAxle1LiftPositionRequest   Updating the output signals based on current state
//!
//!======================================================================================
void LiftAxlePositionSwitchOutputProcessing(const AldLAPS_States                *pLAPSwitchSM,
                                            const A3PosSwitchStatus             *pLiftAxle1Switch2_Status,
                                                  LiftAxleLiftPositionRequest_T *pLiftAxle1LiftPositionRequest)
{
   //! ###### Processing LiftAxlePositionSwitch OutputProcessing logic.
   // Process output only if currentValue is not equal to newValue
   if (pLAPSwitchSM->currentValue != pLAPSwitchSM->newValue)
   {
      switch (pLAPSwitchSM->newValue)
      {
         //! ##### Processing output action for 'LAPS_Lift' state
         case SM_LAPS_Lift:
            *pLiftAxle1LiftPositionRequest = LiftAxleLiftPositionRequest_Up;
         break;
         //! ##### Processing output action for 'LAPS_Down' state
         case SM_LAPS_Down:
            *pLiftAxle1LiftPositionRequest = LiftAxleLiftPositionRequest_Down;
         break;
         //! ##### Processing output action for 'LAPS_Fallback' state
         case SM_LAPS_Fallback:
            if (A3PosSwitchStatus_NotAvailable == pLiftAxle1Switch2_Status->currentValue)
            {
               *pLiftAxle1LiftPositionRequest = LiftAxleLiftPositionRequest_NotAvailable;
            }
            else
            { 
               *pLiftAxle1LiftPositionRequest = LiftAxleLiftPositionRequest_Error;
            }
         break;
         //! ##### Processing output action for 'LAPS_Default' state
         default: // SM_LAPS_Default
            *pLiftAxle1LiftPositionRequest = LiftAxleLiftPositionRequest_Idle;
         break;
      }
   }
   else
   {
      // Do nothing: keep previous state
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'LiftAxlePositionLEDLogic'
//!
//! \param   *pLiftAxle1Switch2_Status        Provides the current switch status
//! \param   *pLiftAxle1PositionStatus        Specifies the current status
//! \param   *pBogieSwitch_DeviceIndication   Updating the Device indication based on conditions
//!
//!======================================================================================
void LiftAxlePositionLEDLogic(const A3PosSwitchStatus        *pLiftAxle1Switch2_Status,
                              const LiftAxlePositionStatus_T *pLiftAxle1PositionStatus,
                                    DualDeviceIndication_T   *pBogieSwitch_DeviceIndication)
{
   //! ###### Process lift axle position LED logic
   //! ##### Check for CRideLEDIndicationType parameter
   // CRideLEDIndicationType range [0-255]- NoIndication-0, RequestedPosition-1 & ActualPosition-2
   if (2U == PCODE_P1KN5_AxleLoad_CRideLEDIndicationType)
   {
      //! #### Check for CRideLEDlowerEnd parameter and LiftAxle1Position status
      if ((TRUE == PCODE_P1KN4_AxleLoad_CRideLEDlowerEnd)
         && ((LiftAxlePositionStatus_Lowering == *pLiftAxle1PositionStatus)
         || (LiftAxlePositionStatus_Lowered == *pLiftAxle1PositionStatus)))
      {
         *pBogieSwitch_DeviceIndication = DualDeviceIndication_UpperOffLowerOn;
      }
      else if ((LiftAxlePositionStatus_Lifting == *pLiftAxle1PositionStatus)
              || (LiftAxlePositionStatus_Lifted == *pLiftAxle1PositionStatus))
      {
         *pBogieSwitch_DeviceIndication = DualDeviceIndication_UpperOnLowerOff;
      }
      else
      {
         *pBogieSwitch_DeviceIndication = DualDeviceIndication_UpperOffLowerOff;
      }
   }
   //! #### Check for CRideLEDIndicationType parameter and LiftAxle1Switch2 status
   else if (1U == PCODE_P1KN5_AxleLoad_CRideLEDIndicationType)
   {
      if ((A3PosSwitchStatus_Middle == pLiftAxle1Switch2_Status->previousValue) 
         && (A3PosSwitchStatus_Upper == pLiftAxle1Switch2_Status->currentValue)
         && (FALSE == PCODE_P1KN4_AxleLoad_CRideLEDlowerEnd))
      {
         *pBogieSwitch_DeviceIndication = DualDeviceIndication_UpperOnLowerOff;
      }
      else if ((A3PosSwitchStatus_Middle == pLiftAxle1Switch2_Status->previousValue)
              && (A3PosSwitchStatus_Lower == pLiftAxle1Switch2_Status->currentValue)
              && (TRUE == PCODE_P1KN4_AxleLoad_CRideLEDlowerEnd))
      {
         *pBogieSwitch_DeviceIndication = DualDeviceIndication_UpperOffLowerOn;
      }
      else if (A3PosSwitchStatus_Middle == pLiftAxle1Switch2_Status->currentValue)
      {
         *pBogieSwitch_DeviceIndication = DualDeviceIndication_UpperOffLowerOff;
      }
      else
      {
         // Do nothing: keep previous status
      }
   }
   else
   {
      *pBogieSwitch_DeviceIndication = DualDeviceIndication_UpperOffLowerOff;
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'DualRatioSwitchStateTransitions'
//!
//! \param    *pDualRatioSwitchStatus          Provides previous and current switch status
//! \param    *pLoadDistributionSelected       Providing the current value
//! \param    *pLoadDistributionRequestedACK   Indicates the current status is NoAction or not
//! \param    *pTimer_DualRatioSwitch_BS       To check current Timer value and update
//! \param    *pTimer_LoadDistributionReqACK   To check current Timer value and update
//! \param    *pinitialize_DRS                 Providing and updating the DualRatioSwitch initialization status
//! \param    *pDualRatioSwitchSM              Provides and Updates the present state value
//!
//!======================================================================================
void DualRatioSwitchStateTransitions(      A3PosSwitchStatus              *pDualRatioSwitchStatus,
                                     const LoadDistributionSelect         *pLoadDistributionSelected,
                                     const LoadDistributionRequestedACK_T *pLoadDistributionRequestedACK,
                                           uint16                         *pTimer_DualRatioSwitch_BS,
                                           uint16                         *pTimer_LoadDistributionReqACK,
                                           uint8                          *pinitialize_DRS,
                                           DualRatioSwitch_States         *pDualRatioSwitchSM)
{
   //! ###### Processing dual ratio switch state transition logic
   static uint8 dualRatioSwitchBS_Flag = CONST_ResetFlag;
   uint8 current_state                 = CONST_ResetFlag;
   boolean isAck_Check                 = FALSE;
   pDualRatioSwitchSM->currentValue    = pDualRatioSwitchSM->newValue; 
   if (CONST_NonInitialize == *pinitialize_DRS)
   {
      pDualRatioSwitchStatus->previousValue = pDualRatioSwitchStatus->currentValue;
      *pTimer_DualRatioSwitch_BS            = CONST_TimerFunctionInactive;
      *pTimer_LoadDistributionReqACK        = CONST_TimerFunctionInactive;
      dualRatioSwitchBS_Flag                = CONST_ResetFlag;
      pDualRatioSwitchSM->newValue          = SM_DRS_NoActionRequested;
      *pinitialize_DRS                      = CONST_Initialize;
   }
   else 
   {
      //! ###### Select the state based on 'pDualRatioSwitchSM->newValue'
      switch (pDualRatioSwitchSM->newValue)
      {
         //! ##### Processing conditions for state change from 'DRS_DefaultLoadDistribution' state.
         case SM_DRS_DefaultLoadDistribution:
            //! #### Check for state change to 'DRS_NoActionRequested'(ChangeAcknowledge or NoAcknowledge event).
            Check_Acknowledge(pLoadDistributionRequestedACK,
                              pTimer_LoadDistributionReqACK,
                              pDualRatioSwitchSM,
                              &isAck_Check);
            //! #### Check for state change to 'DRS_Fallback'(Error detection event).
            if ((FALSE == isAck_Check)
               && (pDualRatioSwitchStatus->previousValue != pDualRatioSwitchStatus->currentValue)
               && ((A3PosSwitchStatus_Error == pDualRatioSwitchStatus->currentValue)
               || (A3PosSwitchStatus_NotAvailable == pDualRatioSwitchStatus->currentValue)))
            {
               pDualRatioSwitchSM->newValue = SM_DRS_Fallback;
            }
            else
            {
               // Do nothing: keep previous status until the timeout occurres
            }
         break;
         //! ##### Processing conditions for state change from 'Dual Ratio Switch MaxTraction 1'.
         case SM_DRS_MaxTraction_1:
             //! #### Check for state change to 'DRS_NoActionRequested'(ChangeAcknowledge or NoAcknowledge event).
             Check_Acknowledge(pLoadDistributionRequestedACK,
                                       pTimer_LoadDistributionReqACK,
                                       pDualRatioSwitchSM,
                                       &isAck_Check);
               //! #### Check for state change to 'DRS_MaxTraction_2'(MaxTraction1_Interrupt event).
            if ((FALSE == isAck_Check)
               && (A3PosSwitchStatus_Upper != pDualRatioSwitchStatus->previousValue)
               && (A3PosSwitchStatus_Upper == pDualRatioSwitchStatus->currentValue))
			   {
				   *pTimer_DualRatioSwitch_BS = CONST_ButtonStuckTimerPeriod;
				   dualRatioSwitchBS_Flag     = CONST_SetFlag;
                  // Start LoadDistributionReqNOACK Timer
				   *pTimer_LoadDistributionReqACK = CONST_LoadDistributionReqNOACK_TimerPeriod;
				   pDualRatioSwitchSM->newValue   = SM_DRS_MaxTraction_2;
			   }
            //! #### Check the conditions for state change to 'DRS_Fallback'(Error detection event).
            else if ((pDualRatioSwitchStatus->previousValue != pDualRatioSwitchStatus->currentValue)
                    && ((A3PosSwitchStatus_Error == pDualRatioSwitchStatus->currentValue)
                    || (A3PosSwitchStatus_NotAvailable == pDualRatioSwitchStatus->currentValue)))
            {
               pDualRatioSwitchSM->newValue = SM_DRS_Fallback;
            }
            else
            {
               // Do nothing: keep previous status until the timeout occurres
            }
         break;
         //! ##### Processing conditions for state change from 'Dual ratio switch MaxTraction 2'.
         case SM_DRS_MaxTraction_2:
            //! #### Check for state change to 'DRS_NoActionRequested'(ChangeAcknowledge or NoAcknowledge event).
            Check_Acknowledge(pLoadDistributionRequestedACK,
                              pTimer_LoadDistributionReqACK,
                              pDualRatioSwitchSM,
                              &isAck_Check);
            //! #### Check for state change to 'DRS_MaxTraction_1'(MaxTraction2_Interrupt event).
            if ((FALSE == isAck_Check)
               && (A3PosSwitchStatus_Lower != pDualRatioSwitchStatus->previousValue)
               && (A3PosSwitchStatus_Lower == pDualRatioSwitchStatus->currentValue))
			   {
				   *pTimer_DualRatioSwitch_BS = CONST_ButtonStuckTimerPeriod;
				   dualRatioSwitchBS_Flag     = CONST_SetFlag;
				   // Start LoadDistributionReqNOACK timer
				   *pTimer_LoadDistributionReqACK = CONST_LoadDistributionReqNOACK_TimerPeriod;
				   pDualRatioSwitchSM->newValue   = SM_DRS_MaxTraction_1;
			   }
            //! #### Check for state change to 'DRS_Fallback'(Error detection event).
            else if ((pDualRatioSwitchStatus->previousValue != pDualRatioSwitchStatus->currentValue)
                    && ((A3PosSwitchStatus_Error == pDualRatioSwitchStatus->currentValue)
                    || (A3PosSwitchStatus_NotAvailable == pDualRatioSwitchStatus->currentValue)))
            {
               pDualRatioSwitchSM->newValue = SM_DRS_Fallback;
            }
            else
            {
               // Do nothing: keep previous status until the timeout occurres
            }
         break;
            //! ##### Processing conditions for state change from 'Dual Ratio Switch Fallback'.
         case SM_DRS_Fallback:
            //! #### Check for DualRatioSwitch status
            //! #### Check for state change to 'DRS_NoActionRequested'(Solve error event).
            if ((pDualRatioSwitchStatus->previousValue != pDualRatioSwitchStatus->currentValue)
               && (A3PosSwitchStatus_Error != pDualRatioSwitchStatus->currentValue)
               && (A3PosSwitchStatus_NotAvailable != pDualRatioSwitchStatus->currentValue))
            {
               pDualRatioSwitchSM->newValue = SM_DRS_NoActionRequested;
               Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            }
            else
            {
               // Do nothing: keep previous status
            }
         break;
         default: // SM_DRS_NoActionRequested:
            //! ##### Processing conditions for state change from 'DRS_NoActionRequested'.
            //! #### Check for dual ratio switch status
            if ((A3PosSwitchStatus_Lower != pDualRatioSwitchStatus->previousValue)
               && (A3PosSwitchStatus_Lower == pDualRatioSwitchStatus->currentValue))
            {
               //! #### Check for state change to 'DRS_DefaultLoadDistribution'(DefaultLoad_RequestLower event)
               if (((LoadDistributionSelected_TractionHelp == pLoadDistributionSelected->currentValue)
                  || (LoadDistributionSelected_StartingHelp == pLoadDistributionSelected->currentValue)
                  || (LoadDistributionSelected_StartingHelpInterDiff == pLoadDistributionSelected->currentValue)
                  || (LoadDistributionSelected_TractionHelpInterDiff == pLoadDistributionSelected->currentValue))
                  && (LoadDistributionRequestedACK_NoAction == *pLoadDistributionRequestedACK))
               {
                  // Start LDReqACK Timer
                  *pTimer_LoadDistributionReqACK = CONST_LoadDistributionReqNOACK_TimerPeriod;
                  pDualRatioSwitchSM->newValue = SM_DRS_DefaultLoadDistribution;
               }
               //! #### Check for state change to 'DRS_MaxTraction_1'(MaxTraction1_Activation event).
               else if ((A3PosSwitchStatus_Middle == pDualRatioSwitchStatus->previousValue)
                       && ((LoadDistributionSelected_TractionHelp != pLoadDistributionSelected->currentValue)
                       || (LoadDistributionSelected_StartingHelp != pLoadDistributionSelected->currentValue)
                       || (LoadDistributionSelected_StartingHelpInterDiff != pLoadDistributionSelected->currentValue)
                       || (LoadDistributionSelected_TractionHelpInterDiff != pLoadDistributionSelected->currentValue))
                       && (LoadDistributionRequestedACK_NoAction == *pLoadDistributionRequestedACK))
               {
                  // Start LDReqACK Timer
                  *pTimer_LoadDistributionReqACK = CONST_LoadDistributionReqNOACK_TimerPeriod;
                  pDualRatioSwitchSM->newValue = SM_DRS_MaxTraction_1;
               }
               else
               {
                  // Do nothing: keep previous status
               }
            }
            //! #### Check for state change to 'DRS_DefaultLoadDistribution'(DefaultLoad_RequestUpper event).
            else if ((A3PosSwitchStatus_Upper != pDualRatioSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Upper == pDualRatioSwitchStatus->currentValue))
            {
               if (((LoadDistributionSelected_TractionHelp2 == pLoadDistributionSelected->currentValue)
                  || (LoadDistributionSelected_StartingHelp2 == pLoadDistributionSelected->currentValue))
                  && (LoadDistributionRequestedACK_NoAction == *pLoadDistributionRequestedACK))
               {
                  // Start LDReqACK Timer
                  *pTimer_LoadDistributionReqACK = CONST_LoadDistributionReqNOACK_TimerPeriod;
                  pDualRatioSwitchSM->newValue = SM_DRS_DefaultLoadDistribution;
               }
               //! #### Check for state change to 'DRS_MaxTraction_2'(MaxTraction2_Activation event).
               else if ((A3PosSwitchStatus_Middle == pDualRatioSwitchStatus->previousValue)
                       && ((LoadDistributionSelected_TractionHelp2 != pLoadDistributionSelected->currentValue)
                       || (LoadDistributionSelected_StartingHelp2 != pLoadDistributionSelected->currentValue))
                       && (LoadDistributionRequestedACK_NoAction == *pLoadDistributionRequestedACK))
               {
                  // Start LDReqACK Timer
                  *pTimer_LoadDistributionReqACK = CONST_LoadDistributionReqNOACK_TimerPeriod;
                  pDualRatioSwitchSM->newValue = SM_DRS_MaxTraction_2;
               }
               else
               {
                  // Do nothing: keep previous status
               }
            }
            //! #### Check for state change to 'DRS_Fallback' state(Error detection event)
            else if ((pDualRatioSwitchStatus->previousValue != pDualRatioSwitchStatus->currentValue)
                    && ((A3PosSwitchStatus_Error == pDualRatioSwitchStatus->currentValue)
                    /*|| (A3PosSwitchStatus_NotAvailable == pDualRatioSwitchStatus->currentValue)*/)) //currently we are not considering this condition due to issue in FSP need to discuss with customer
            {
               pDualRatioSwitchSM->newValue = SM_DRS_Fallback;
            }
            else
            {
               // Do nothing: keep previous status
            }
			//! ##### process the A3_ButtonStuckEvent (Button stuck event): 'A3_ButtonStuckEvent()'
         current_state = A3_ButtonStuckEvent(pDualRatioSwitchStatus,
                                             pTimer_DualRatioSwitch_BS,
                                             &dualRatioSwitchBS_Flag);
         break;
      }
      if (CONST_SetFlag == current_state)
      {
         pDualRatioSwitchSM->newValue = SM_DRS_Fallback;
      }
      else
      {
         // Do nothing: keep previous status
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'DualRatioSwitchOutputProcessing'
//!
//! \param   *pDualRatioSwitchSM               Providing the current state value
//! \param   *pDualRatioSwitchStatus           Specifies the switch status value
//! \param   *pDRS_LoadDistributionRequested   Updating the output signals based on current state
//!
//!======================================================================================
void DualRatioSwitchOutputProcessing(const DualRatioSwitch_States      *pDualRatioSwitchSM,
                                     const A3PosSwitchStatus           *pDualRatioSwitchStatus,
                                           LoadDistributionRequested_T *pDRS_LoadDistributionRequested)
{
   //! ###### Processing DualRatioSwitch OutputProcessing logic 
   // Process output only if currentValue is not equal to newValue
   if (pDualRatioSwitchSM->currentValue != pDualRatioSwitchSM->newValue)
   {
      //! ##### Select the state based on 'pDualRatioSwitchSM->newValue' value
      switch (pDualRatioSwitchSM->newValue)
      {
         //! #### Output action for 'DRS_DefaultLoadDistribution' state
         case SM_DRS_DefaultLoadDistribution:
            *pDRS_LoadDistributionRequested = LoadDistributionRequested_DefaultLoadDistribution;
         break;
         //! #### Output action for 'DRS_MaxTraction_1' state
         case SM_DRS_MaxTraction_1:
            *pDRS_LoadDistributionRequested = LoadDistributionRequested_MaximumTraction1;
         break;
         //! #### Output action for 'DRS_MaxTraction_2' state
         case SM_DRS_MaxTraction_2:
            *pDRS_LoadDistributionRequested = LoadDistributionRequested_MaximumTraction2;
         break;
         //! #### Output action for 'DRS_Fallback' state
         case SM_DRS_Fallback:
            if (A3PosSwitchStatus_NotAvailable == pDualRatioSwitchStatus->currentValue)
            {
               *pDRS_LoadDistributionRequested = LoadDistributionRequested_NotAvailable;
            }
            else
            {
               *pDRS_LoadDistributionRequested = LoadDistributionRequested_Error;
            }
         break;
         //! #### Output action for 'DRS_NoActionRequested' state
         default:
            *pDRS_LoadDistributionRequested = LoadDistributionRequested_NoAction;
         break;
      }
   }
   else
   {
      // Do nothing: keep previous state
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'DualRatioSwitchDeviceIndication'
//!
//! \param   *pLoadDistributionSelected     Provides the current status 
//! \param   *pDualRatio_DeviceIndication   Updating the Device indication based on conditions
//!
//!======================================================================================
void DualRatioSwitchDeviceIndication(const LoadDistributionSelect *pLoadDistributionSelected,
                                           DualDeviceIndication_T *pDualRatio_DeviceIndication)
{
   //! ###### Processing dual ratio switch device indication logic
   //! ##### Check for LoadDistributionSelected is 'TractionHelp' or 'TractionHelpInterDiff'
   if ((LoadDistributionSelected_TractionHelp == pLoadDistributionSelected->currentValue) 
      || (LoadDistributionSelected_TractionHelpInterDiff == pLoadDistributionSelected->currentValue))
   {
      *pDualRatio_DeviceIndication = DualDeviceIndication_UpperOffLowerOn;
   }
   //! ##### Check for LoadDistributionSelected is 'StartingHelp' or 'StartingHelpInterDiff'
   else if ((LoadDistributionSelected_StartingHelp == pLoadDistributionSelected->currentValue) 
           || (LoadDistributionSelected_StartingHelpInterDiff == pLoadDistributionSelected->currentValue))
   {
      *pDualRatio_DeviceIndication = DualDeviceIndication_UpperOffLowerBlink;
   }
   //! ##### Check for LoadDistributionSelected is 'TractionHelp2'. 
   else if (LoadDistributionSelected_TractionHelp2 == pLoadDistributionSelected->currentValue)
   {
      *pDualRatio_DeviceIndication = DualDeviceIndication_UpperOnLowerOff;
   }
   //! ##### Check for LoadDistributionSelected is 'StartingHelp2'. 
   else if (LoadDistributionSelected_StartingHelp2 == pLoadDistributionSelected->currentValue)
   {
      *pDualRatio_DeviceIndication = DualDeviceIndication_UpperBlinkLowerOff;
   } 
   else
   {
      *pDualRatio_DeviceIndication = DualDeviceIndication_UpperOffLowerOff;
   }
} 
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'ARideStateTransitions'
//!
//! \param    *pLiftAxle2MaxTractSwitchStatus   Provides the current switch status of LiftAxle2MaxTraction
//! \param    *pLiftAxle1UpRequestACK           Indicates the current status
//! \param    *pTimer_LiftAxle2MaxTract_BS      To check current timer value and update
//! \param    *pTimer_LiftAxle1UpReq            To check current timer value and update
//! \param    *pTimer_LiftAxle1UpReq_NoACK      To check current timer value and update
//! \param    *pinitialize_ARide                Providing and updating the current ARideState initialization status
//! \param    *pARideSM                         Provides and Updates the present state based on conditions
//!
//!======================================================================================
void ARideStateTransitions(A3PosSwitchStatus    *pLiftAxle2MaxTractSwitchStatus,
                           LiftAxleUpRequestACK *pLiftAxle1UpRequestACK,
                           uint16               *pTimer_LiftAxle2MaxTract_BS,
                           uint16               *pTimer_LiftAxle1UpReq,
                           uint16               *pTimer_LiftAxle1UpReq_NoACK,
                           uint8                *pinitialize_ARide,
                           AldARide_States      *pARideSM)
{
   //! ###### Processing ARide state transition logic.
   static uint8 shortLongPushFlag       = CONST_ResetFlag;
   static uint8 aRideBS_Flag            = CONST_ResetFlag;
   uint8 current_state                  = CONST_ResetFlag;
   pARideSM->currentValue               = pARideSM->newValue;
   if (CONST_NonInitialize == *pinitialize_ARide)
   {
      pLiftAxle2MaxTractSwitchStatus->previousValue = pLiftAxle2MaxTractSwitchStatus->currentValue;
      pLiftAxle1UpRequestACK->previousValue         = pLiftAxle1UpRequestACK->currentValue;
      *pTimer_LiftAxle2MaxTract_BS                  = CONST_TimerFunctionInactive;
      *pTimer_LiftAxle1UpReq                        = CONST_TimerFunctionInactive;
      *pTimer_LiftAxle1UpReq_NoACK                  = CONST_TimerFunctionInactive;
      aRideBS_Flag                                  = CONST_ResetFlag;
      pARideSM->newValue                            = SM_ALD_ARide_Default;
      *pinitialize_ARide                            = CONST_Initialize;
   }
   else
   {
      //! ###### Select the state based on 'pARideSM->newValue'.
      switch (pARideSM->newValue)
      {
         //! ##### Processing conditions for state change from 'ARide_Short LiftRequest'.
         case SM_ALD_ARide_Short_LiftRequest:
             //! #### Check for LiftAxle1UpRequestACK, LiftAxle1UpRequestACK and LiftAxle2MaxTractSwitch status
             //! #### Check for state change to 'ARide_Default'(ChangeAcknowledge or NoAcknowledge event).
            if ((LiftAxleUpRequestACK_ChangeAcknowledged == pLiftAxle1UpRequestACK->currentValue)
               || (LiftAxleUpRequestACK_LiftDeniedFrontOverload == pLiftAxle1UpRequestACK->currentValue)
               || (LiftAxleUpRequestACK_LiftDeniedRearOverload == pLiftAxle1UpRequestACK->currentValue)
               || (LiftAxleUpRequestACK_LiftDeniedPBrakeActive == pLiftAxle1UpRequestACK->currentValue)
               || (LiftAxleUpRequestACK_SystemDeniedVersatile == pLiftAxle1UpRequestACK->currentValue)
               || (CONST_TimerFunctionElapsed == *pTimer_LiftAxle1UpReq_NoACK))
            {
               // Cancel ACK timer
               *pTimer_LiftAxle1UpReq_NoACK = CONST_TimerFunctionInactive;
               // Cancel LiftAxle1UpReq timer
               *pTimer_LiftAxle1UpReq       = CONST_TimerFunctionInactive;
               pARideSM->newValue           = SM_ALD_ARide_Default;
            }
            //! #### Check for state change to 'ARide_Fallback'(Error detection event).
            else if ((A3PosSwitchStatus_Error != pLiftAxle2MaxTractSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Error == pLiftAxle2MaxTractSwitchStatus->currentValue))
            {
               pARideSM->newValue = SM_ALD_ARide_Fallback;
            }
            else
            {
               // Do nothing: keep previous status
            }
            if ((pLiftAxle1UpRequestACK->previousValue != pLiftAxle1UpRequestACK->currentValue)
               && ((LiftAxleUpRequestACK_NoAction == pLiftAxle1UpRequestACK->currentValue)
               || (LiftAxleUpRequestACK_Error == pLiftAxle1UpRequestACK->currentValue)
               || (LiftAxleUpRequestACK_NotAvailable == pLiftAxle1UpRequestACK->currentValue)))
            {
               // Cancel ACK timer
               *pTimer_LiftAxle1UpReq_NoACK = CONST_TimerFunctionInactive;
            }
            else
            { 
               // Do nothing: keep previous status
            }
         break;
         //! ##### Processing conditions for state change from 'ARide_Long_LiftRequest'.
         case SM_ALD_ARide_Long_LiftRequest:
            //! #### Check for LiftAxle2MaxTractSwitch status
            //! #### Check for state change to 'ARide_Default'(Push_Released event).
            if (A3PosSwitchStatus_Middle == pLiftAxle2MaxTractSwitchStatus->currentValue)
            {
               pARideSM->newValue = SM_ALD_ARide_Default;
            }
            //! #### Check for state change to 'ARide_Fallback'(Error detection event).
            else if ((A3PosSwitchStatus_Error != pLiftAxle2MaxTractSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Error == pLiftAxle2MaxTractSwitchStatus->currentValue))
            {
               pARideSM->newValue = SM_ALD_ARide_Fallback;
            }
            else
            {
               // Do nothing: keep previous status
            }
         break;
         //! ##### Processing conditions for state change from 'ARide_DownRequest'.
         case SM_ALD_ARide_DownRequest:
            //! #### Check for LiftAxle2MaxTractSwitch Status
            //! #### Check for state change to 'ARide_Default'(Push_Released event).
            if (A3PosSwitchStatus_Middle == pLiftAxle2MaxTractSwitchStatus->currentValue)
            {
               pARideSM->newValue = SM_ALD_ARide_Default;
            }
            //! #### Check for state change to 'ARide_Fallback'(Error detection event)
            else if ((A3PosSwitchStatus_Error != pLiftAxle2MaxTractSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Error == pLiftAxle2MaxTractSwitchStatus->currentValue))
            {
               pARideSM->newValue = SM_ALD_ARide_Fallback;
            }
            else
            {
                  // Do nothing: keep previous status
            }
         break;
         //! ##### Processing conditions for state change from 'ARide_Fallback'.
         case SM_ALD_ARide_Fallback:
            //! #### Check for LiftAxle2MaxTractSwitch Status
            //! #### Check for state change to 'ARide_Default'(Solve error event).
            if ((pLiftAxle2MaxTractSwitchStatus->currentValue != pLiftAxle2MaxTractSwitchStatus->previousValue)
               && (A3PosSwitchStatus_Error != pLiftAxle2MaxTractSwitchStatus->currentValue))
            {
               // Cancel LiftAxle1UpReq timer
               *pTimer_LiftAxle1UpReq       = CONST_TimerFunctionInactive;
               *pTimer_LiftAxle1UpReq_NoACK = CONST_TimerFunctionInactive;
               shortLongPushFlag            = CONST_ResetFlag;
               pARideSM->newValue           = SM_ALD_ARide_Default;
               Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            }
            else
            {
               // Do nothing: keep previous status
            }
         break;
         //! ##### Processing conditions for state change from 'ARide_Default'.
         default: //SM_ALD_ARide_Default
            //! #### Check for LiftAxle2MaxTractSwitch status 
            if ((A3PosSwitchStatus_Middle == pLiftAxle2MaxTractSwitchStatus->previousValue)
               && (A3PosSwitchStatus_Upper == pLiftAxle2MaxTractSwitchStatus->currentValue))
            {
               // Start LiftAxle1UpReq timer
               *pTimer_LiftAxle1UpReq = CONST_LiftAxle1UpRequestACK_TimerPeriod;
               shortLongPushFlag      = CONST_SetFlag;
            }
            //! #### Check for state change to 'ARide_Short_LiftRequest'(Short_PushUp event).
            else if ((CONST_SetFlag == shortLongPushFlag) 
                    && (*pTimer_LiftAxle1UpReq <= CONST_LiftAxle1UpRequestACK_TimerPeriod)
                    && (A3PosSwitchStatus_Middle == pLiftAxle2MaxTractSwitchStatus->currentValue))
            {
               shortLongPushFlag            = CONST_ResetFlag;
               *pTimer_LiftAxle1UpReq_NoACK = CONST_LiftAxle1UpRequestNOACK_TimerPeriod;
               pARideSM->newValue           = SM_ALD_ARide_Short_LiftRequest;
            }
            //! #### Check for state change to 'ARide_Long_LiftRequest'(Long_PushUp event).
            else if ((CONST_SetFlag == shortLongPushFlag) 
                    && (*pTimer_LiftAxle1UpReq > CONST_LiftAxle1UpRequestACK_TimerPeriod)
                    && (A3PosSwitchStatus_Upper == pLiftAxle2MaxTractSwitchStatus->currentValue))
            {
               // Cancel LiftAxle1UpReq timer
               *pTimer_LiftAxle1UpReq = CONST_TimerFunctionInactive;
               shortLongPushFlag      = CONST_ResetFlag;
               pARideSM->newValue     = SM_ALD_ARide_Long_LiftRequest;
            }
            //! #### Check for state change to 'ARide_DownRequest'(Push_Down event).
            else if ((A3PosSwitchStatus_Middle == pLiftAxle2MaxTractSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Lower == pLiftAxle2MaxTractSwitchStatus->currentValue))
            {
               pARideSM->newValue = SM_ALD_ARide_DownRequest;
            }
            //! #### Check for state change to 'ARide_Fallback'(Error detection event)
            else if ((A3PosSwitchStatus_Error != pLiftAxle2MaxTractSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Error == pLiftAxle2MaxTractSwitchStatus->currentValue))
            {
               pARideSM->newValue = SM_ALD_ARide_Fallback;
            }
            else
            {
                  // Do nothing: keep previous status
            }
         break;
      }
      //! #### process the A3_ButtonStuckEvent (Button stuck event): 'A3_ButtonStuckEvent()'
      current_state = A3_ButtonStuckEvent(pLiftAxle2MaxTractSwitchStatus,
                                          pTimer_LiftAxle2MaxTract_BS,
                                          &aRideBS_Flag);
      if (CONST_SetFlag == current_state)
      {
         pARideSM->newValue = SM_ALD_ARide_Fallback;
      }
      else
      {
         // Do nothing: keep previous status
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'ARideStateOutputProcessing'
//!
//! \param   *pARideSM                        Providing the current state value
//! \param   *pLiftAxle1LiftPositionRequest   Updating the output signal based on current state
//! \param   *pLiftAxle1DirectControl         Updating the output signal based on current state
//!
//!======================================================================================
void ARideStateOutputProcessing(const AldARide_States               *pARideSM,
                                      LiftAxleLiftPositionRequest_T *pLiftAxle1LiftPositionRequest,
                                      LiftAxleLiftPositionRequest_T *pLiftAxle1DirectControl)
{
   //! ###### Processing ARide state output processing logic
   // Process output only if currentValue is not equal to newValue
   if (pARideSM->currentValue != pARideSM->newValue)
   {
      //! ##### Select the state based on 'pARideSM->newValue'
      switch (pARideSM->newValue)
      {
         //! #### Output actions for 'ARide_Short_LiftRequest' state
         case SM_ALD_ARide_Short_LiftRequest:
            *pLiftAxle1LiftPositionRequest = LiftAxleLiftPositionRequest_Up;
            *pLiftAxle1DirectControl       = LiftAxleLiftPositionRequest_Idle;
         break;
         //! #### Output actions for 'ARide_Long_LiftRequest' state
         case SM_ALD_ARide_Long_LiftRequest:
            *pLiftAxle1LiftPositionRequest = LiftAxleLiftPositionRequest_Idle;
            *pLiftAxle1DirectControl       = LiftAxleLiftPositionRequest_Up;
         break;
         //! #### Output actions for 'ARide_DownRequest' state
         case SM_ALD_ARide_DownRequest:
            *pLiftAxle1LiftPositionRequest = LiftAxleLiftPositionRequest_Idle;
            *pLiftAxle1DirectControl       = LiftAxleLiftPositionRequest_Down;
         break;
         //! #### Output actions for 'ARide_Fallback' state
         case SM_ALD_ARide_Fallback:
            *pLiftAxle1LiftPositionRequest = LiftAxleLiftPositionRequest_Error;
            *pLiftAxle1DirectControl       = LiftAxleLiftPositionRequest_Error;
         break;
         //! #### Output actions for 'ARide_Default' state
         default: //SM_ALD_ARide_Default
            *pLiftAxle1LiftPositionRequest = LiftAxleLiftPositionRequest_Idle;
            *pLiftAxle1DirectControl       = LiftAxleLiftPositionRequest_Idle;
         break;
      }
   }
   else
   {
      // Do nothing: keep previous state
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'ARideSwitchLEDLogic'
//!
//! \param    *pLoadDistributionSelected    Provides the current status
//! \param    *pMaxTract_DeviceIndication   Updating the Device indication based on conditions
//!
//!======================================================================================
void ARideSwitchLEDLogic(const LoadDistributionSelect *pLoadDistributionSelected,
                               DeviceIndication_T     *pMaxTract_DeviceIndication)
{
   //! ###### Processing the ARideSwitch LED Logic
   //! ##### Updating device indication based on LoadDistributionSelected status
   if (LoadDistributionSelected_VariableAxleLoad == pLoadDistributionSelected->currentValue)
   {
      *pMaxTract_DeviceIndication = DeviceIndication_On;
   }
   else
   {
      *pMaxTract_DeviceIndication = DeviceIndication_Off;
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'ALDSwitchStateTransitions'
//!
//! \param    *pALDSwitchStatus                Provides the current switch status of ALDSwitch
//! \param    *pLoadDistributionALDChangeACK   Indicates the current status
//! \param    *pLoadDistributionSelected       Specifies the current status
//! \param    *pTimer_ALDSwitch_BS             To check current Timer value and update
//! \param    *pinitialize_ALD                 Specify and update the current value
//! \param    *pALD_StateSM                    Provides and Updates the present state based on conditions
//!
//!======================================================================================
void ALDSwitchStateTransitions(      A2PosSwitchStatus              *pALDSwitchStatus,
                               const LoadDistributionALDChangeACK_T *pLoadDistributionALDChangeACK,
                               const LoadDistributionSelect         *pLoadDistributionSelected,
                                     uint16                         *pTimer_ALDSwitch_BS,
                                     uint8                          *pinitialize_ALD,
                                     ALDSwitch_States               *pALD_StateSM)
{
   //! ###### Processing ALD switch state transition logic.
   static uint8 aldSwitcBS_Flag      = CONST_ResetFlag;
   uint8 current_state               = CONST_ResetFlag;
   pALD_StateSM->currentValue        = pALD_StateSM->newValue;
   if (CONST_NonInitialize == *pinitialize_ALD)
   {
      pALDSwitchStatus->previousValue = pALDSwitchStatus->currentValue;
      *pTimer_ALDSwitch_BS            = CONST_TimerFunctionInactive;
      aldSwitcBS_Flag                 = CONST_ResetFlag;
      pALD_StateSM->newValue          = SM_ALD_Switch_Default;
      *pinitialize_ALD                = CONST_Initialize;
   }
   else
   {
      //! ###### Select the state based on 'pALD_StateSM->newValue'
      switch (pALD_StateSM->newValue)
      {
         //! ##### Processing conditions for state change from ALD 'Switch LastFunc'
         case SM_ALD_Switch_LastFunc:
            //! #### Check for LoadDistributionSelected and ALDSwitch status
            //! #### Check for state change to 'ALD_Switch_StandBy'(AlternativeLoad_NotSelected event)
            if (LoadDistributionSelected_AlternativeLoad != pLoadDistributionSelected->currentValue)
            {
               pALD_StateSM->newValue = SM_ALD_Switch_StandBy;
            }
            //! #### Check for state change to 'ALD_Switch_ALD_Ack'(AlternativeLoad_Selected event)
            else
            {
               pALD_StateSM->newValue = SM_ALD_Switch_ALD_Ack;
            }
         break;
         //! ##### Processing conditions for state change from ALD 'Switch StandBy'.
         case SM_ALD_Switch_StandBy:
            //! #### Check for ALDSwitch status 
            //! #### Check for state change to 'ALD_Switch_ALD_On'(On_Pressed event).
            if ((A2PosSwitchStatus_Off == pALDSwitchStatus->previousValue) 
               && (A2PosSwitchStatus_On == pALDSwitchStatus->currentValue))
            {
               pALD_StateSM->newValue = SM_ALD_Switch_ALD_On;
            }
            //! #### Check for state change to 'ALD_Switch_Fallback'(Error detection event).
            else if ((pALDSwitchStatus->previousValue != pALDSwitchStatus->currentValue)
                    && ((A2PosSwitchStatus_Error == pALDSwitchStatus->currentValue)
                    || (A2PosSwitchStatus_NotAvailable == pALDSwitchStatus->currentValue)))
            {
               pALD_StateSM->newValue = SM_ALD_Switch_Fallback;
            }
            else
            {
               // Do nothing: keep previous status until the timeout occurres
            }
            //! #### process the A2_ButtonStuckEvent (Button stuck event): 'A2_ButtonStuckEvent()'
            current_state = A2_ButtonStuckEvent(pALDSwitchStatus,
                                                pTimer_ALDSwitch_BS,
                                                &aldSwitcBS_Flag);
         break;
            //! ##### Processing conditions for state change from ALD Switch 'ALD On'.
         case SM_ALD_Switch_ALD_On:
            //! #### Check for LoadDistributionSelected and ALDSwitch status 
            //! #### Check for state change to 'ALD_Switch_ALD_Ack'(ChangeAcknowledge event).
            if (LoadDistributionALDChangeACK_ChangeAcknowledged == *pLoadDistributionALDChangeACK)
            {
               pALD_StateSM->newValue = SM_ALD_Switch_ALD_Ack;
            }
            //! #### Check for state change to 'ALD_Switch_ALD_Off'(On_Pressed event).
            else if ((A2PosSwitchStatus_Off == pALDSwitchStatus->previousValue)
                    && (A2PosSwitchStatus_On == pALDSwitchStatus->currentValue))
            {
               pALD_StateSM->newValue = SM_ALD_Switch_ALD_Off;
            }
            //! #### Check for state change to 'ALD_Switch_ALD_Denied'(System_Denied event).
            else if ((LoadDistributionALDChangeACK_LoadShiftDeniedOverspeed == *pLoadDistributionALDChangeACK)
                    || (LoadDistributionALDChangeACK_SystemDeniedVersatile == *pLoadDistributionALDChangeACK))
            {
               pALD_StateSM->newValue = SM_ALD_Switch_ALD_Denied;
            }
            //! #### Check for state change to 'ALD_Switch_Fallback'(Error detection event).
            else if ((pALDSwitchStatus->previousValue != pALDSwitchStatus->currentValue)
                    && ((A2PosSwitchStatus_Error == pALDSwitchStatus->currentValue)
                    || (A2PosSwitchStatus_NotAvailable == pALDSwitchStatus->currentValue)))
            {
               pALD_StateSM->newValue = SM_ALD_Switch_Fallback;
            }
            //! #### Check for state change to 'ALD_RatioALD_Fallback'(Button stuck event)
            else if ((CONST_TimerFunctionInactive == *pTimer_ALDSwitch_BS)
                    && (CONST_SetFlag == aldSwitcBS_Flag))
            {
               // Cancel button stuck timer
               pALD_StateSM->newValue = SM_ALD_Switch_Fallback;
               Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_FAILED);
            }
            else
            {
               // Do nothing: keep previous status until the timeout occurres
            }
            if ((pALDSwitchStatus->previousValue != pALDSwitchStatus->currentValue)
               && (A2PosSwitchStatus_On == pALDSwitchStatus->currentValue))
            {
               // Start button stuck timer
               *pTimer_ALDSwitch_BS = CONST_ButtonStuckTimerPeriod;
               aldSwitcBS_Flag      = CONST_SetFlag;
            }
            else if ((pALDSwitchStatus->previousValue != pALDSwitchStatus->currentValue)
                    && (A2PosSwitchStatus_On != pALDSwitchStatus->currentValue))
            {
               // Cancel button stuck timer
               aldSwitcBS_Flag      = CONST_ResetFlag;
               *pTimer_ALDSwitch_BS = CONST_TimerFunctionInactive;
            }
            else
            {
               // Do nothing: keep previous status until the timeout occurres
            }
         break;
            //! ##### Processing conditions for state change from ALD Switch 'ALD Ack'.
         case SM_ALD_Switch_ALD_Ack:
            //! #### Check for LoadDistributionSelected and ALDSwitch status
            //! #### Check for state change to 'ALD_Switch_ALD_Off'(On_Pressed event).
            if ((A2PosSwitchStatus_Off == pALDSwitchStatus->previousValue)
               && (A2PosSwitchStatus_On == pALDSwitchStatus->currentValue))
            {
               pALD_StateSM->newValue = SM_ALD_Switch_ALD_Off;
            }
            //! #### Check for state change to 'ALD_Switch_ALD_Off'(AlternativeLoad_Changes event).
            else if ((LoadDistributionSelected_AlternativeLoad == pLoadDistributionSelected->previousValue)
                    && (LoadDistributionSelected_AlternativeLoad != pLoadDistributionSelected->currentValue))
            {
               pALD_StateSM->newValue = SM_ALD_Switch_ALD_Off;
            }
            //! #### Check for state change to 'ALD_Switch_Fallback'(Error detection event).
            else if ((pALDSwitchStatus->previousValue != pALDSwitchStatus->currentValue)
                    && ((A2PosSwitchStatus_Error == pALDSwitchStatus->currentValue)
                    || (A2PosSwitchStatus_NotAvailable == pALDSwitchStatus->currentValue)))
            {
               pALD_StateSM->newValue = SM_ALD_Switch_Fallback;
            }
            //! #### Check for state change to 'ALD_Switch_Fallback'(Button stuck event).
            else if ((CONST_TimerFunctionInactive == *pTimer_ALDSwitch_BS)
                    && (CONST_SetFlag == aldSwitcBS_Flag))
            {
               pALD_StateSM->newValue = SM_ALD_Switch_Fallback;
               Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_FAILED);
            }
            else
            {
               // Do nothing: keep previous status until the timeout occurres
            }
            if ((pALDSwitchStatus->previousValue != pALDSwitchStatus->currentValue)
               && (A2PosSwitchStatus_On == pALDSwitchStatus->currentValue))
            {
               // Start button stuck timer
               *pTimer_ALDSwitch_BS = CONST_ButtonStuckTimerPeriod;
               aldSwitcBS_Flag      = CONST_SetFlag;
            }
            else if ((pALDSwitchStatus->previousValue != pALDSwitchStatus->currentValue)
                    && (A2PosSwitchStatus_On != pALDSwitchStatus->currentValue))
            {
               // Cancel button stuck timer
               aldSwitcBS_Flag      = CONST_ResetFlag;
               *pTimer_ALDSwitch_BS = CONST_TimerFunctionInactive;
            }
            else
            {
               // Do nothing: keep previous status until the timeout occurres
            }
         break;
            //! ##### Processing conditions for state change from ALD Switch 'ALD Off'
         case SM_ALD_Switch_ALD_Off:
            //! #### Check for ALD switch status
            //! #### Unconditional transition for state change to the 'ALD_Switch_StandBy' state (Unconditional transition event).
            //! ##### Processing conditions for state change from ALD Switch 'Denied'
         case SM_ALD_Switch_ALD_Denied:
            pALD_StateSM->newValue  = SM_ALD_Switch_StandBy;
         break;
         //! ##### Processing conditions for state change from ALD Switch 'Fallback'.
         case SM_ALD_Switch_Fallback:
            //! #### Check for state change to 'ALD_Switch_Default'(Solve error event).
            if ((A2PosSwitchStatus_Error != pALDSwitchStatus->currentValue)
               && (A2PosSwitchStatus_NotAvailable != pALDSwitchStatus->currentValue)
               && (pALDSwitchStatus->previousValue != pALDSwitchStatus->currentValue))
            {
               pALD_StateSM->newValue = SM_ALD_Switch_Default;
               Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            }
            else
            {
               // Do nothing: keep previous status
            }
            //! #### process the A2_ButtonStuckEvent (Button stuck event): 'A2_ButtonStuckEvent()'
            current_state = A2_ButtonStuckEvent(pALDSwitchStatus,
                                                pTimer_ALDSwitch_BS,
                                                &aldSwitcBS_Flag);
         break;
         //! ##### Processing conditions for state change from ALD Switch 'Default'.
         default: //SM_ALD_Switch_Default
            //! #### Check for state change to 'ALD_Switch_LastFunc'(System_Ready event).
            if ((A2PosSwitchStatus_Off == pALDSwitchStatus->currentValue) 
               && (LoadDistributionALDChangeACK_NotAvailable != *pLoadDistributionALDChangeACK)
               && (LoadDistributionSelected_NotAvailable != pLoadDistributionSelected->currentValue))
            {
               pALD_StateSM->newValue = SM_ALD_Switch_LastFunc;
            }
            //! #### Check for state change to 'ALD_Switch_Fallback'(Error detection event).
            else if ((pALDSwitchStatus->previousValue != pALDSwitchStatus->currentValue)
                    && ((A2PosSwitchStatus_Error == pALDSwitchStatus->currentValue)
                    /*|| (A2PosSwitchStatus_NotAvailable == pALDSwitchStatus->currentValue)*/)) //currently we are not considering this condition due to issue in FSP need to discuss with customer
            {
               pALD_StateSM->newValue = SM_ALD_Switch_Fallback;
            }
            else
            {
               // Do nothing: keep previous status
            }
            //! #### process the A2_ButtonStuckEvent (Button stuck event): 'A2_ButtonStuckEvent()'
            current_state = A2_ButtonStuckEvent(pALDSwitchStatus,
                                                pTimer_ALDSwitch_BS,
                                                &aldSwitcBS_Flag);
         break;
      }
      if (CONST_SetFlag == current_state)
      {
         pALD_StateSM->newValue = SM_ALD_Switch_Fallback;
      }
      else
      {
         // Do nothing: keep previous status
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'ALDSwitchStatusOutputProcessing'
//!
//! \param   *pALD_StateSM                    Providing the current state value
//! \param   *pALDSwitchStatus                Provides the switch status for ALD_Switch
//! \param   *pALD_AltLoadDistribution_rqst   Updating the output signal based on current state
//!
//!======================================================================================
void ALDSwitchStatusOutputProcessing(const ALDSwitch_States            *pALD_StateSM,
                                     const A2PosSwitchStatus           *pALDSwitchStatus,
                                           AltLoadDistribution_rqst_T  *pALD_AltLoadDistribution_rqst)
{
   //! ###### Processing ALD switch status output processing logic
   // Process output only if currentValue is not equal to newValue
   if (pALD_StateSM->currentValue != pALD_StateSM->newValue)
   {
      //! ##### Select the state based on 'pALD_StateSM->newValue'
      switch (pALD_StateSM->newValue)
      {
         //! #### No output action for 'ALD_Switch_LastFunc' state.
         case SM_ALD_Switch_LastFunc:
            //No action
         //! #### No output action for 'ALD_Switch_StandBy' state.
         case SM_ALD_Switch_StandBy:
            //No action
            //Explicit break due to No Action
         break;
         //! #### Output action for 'ALD_Switch_On' state.
         case SM_ALD_Switch_ALD_On:
            *pALD_AltLoadDistribution_rqst = AltLoadDistribution_rqst_ALDOn;
         break;
         //! #### Output action for 'ALD_Switch_Ack' state.
         case SM_ALD_Switch_ALD_Ack:
            *pALD_AltLoadDistribution_rqst = AltLoadDistribution_rqst_Idle;
         break;
         //! #### Output action for 'ALD_Switch_Off' state.
         case SM_ALD_Switch_ALD_Off:
            *pALD_AltLoadDistribution_rqst = AltLoadDistribution_rqst_ALDOff;
         break;
         //! #### Output action for 'ALD_Switch_Denied' state.
         case SM_ALD_Switch_ALD_Denied:
            *pALD_AltLoadDistribution_rqst = AltLoadDistribution_rqst_Idle;
         break;
         //! #### Output action for 'ALD_Switch_Fallback' state.
         case SM_ALD_Switch_Fallback:
            if (A2PosSwitchStatus_NotAvailable == pALDSwitchStatus->currentValue)
            {
               *pALD_AltLoadDistribution_rqst = AltLoadDistribution_rqst_NotAvailable;
            }
            else 
            {
               *pALD_AltLoadDistribution_rqst = AltLoadDistribution_rqst_Error;
            }
         break;
         //! #### Output action for 'ALD_Switch_Default' state.
         default: //SM_ALD_Switch_Default
            *pALD_AltLoadDistribution_rqst = AltLoadDistribution_rqst_Idle;
         break;
      }
   }
   else
   {
      // Do nothing: keep previous state
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'ALDDeviceIndication'
//!
//! \param   *pLoadDistributionSelected   Provides the current status
//! \param   *pALD_DeviceIndication       Updating the Device indication based on conditions
//!
//!======================================================================================
void ALDDeviceIndication(const LoadDistributionSelect  *pLoadDistributionSelected,
                               DeviceIndication_T      *pALD_DeviceIndication)
{
   //! ###### Processing output for ALD device indication
   //! ##### Updating device indication based on LoadDistributionSelected status.
   if (LoadDistributionSelected_AlternativeLoad == pLoadDistributionSelected->currentValue)
   {
      *pALD_DeviceIndication = DeviceIndication_On;
   }
   else
   {
      *pALD_DeviceIndication = DeviceIndication_Off;
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'RatioALDSwitchTransitions'
//!
//! \param    *pRatioALDSwitchStatus           Provides the current switch status of ratio ALDSwitch
//! \param    *pLoadDistributionSelected       Specifies the current status
//! \param    *pLoadDistributionALDChangeACK   Indicates the current acknowledgement status
//! \param    *pLoadDistributionChangeACK      Provides the current acknowledgement status
//! \param    *pLoadDistributionFuncSelected   Specifies the current status
//! \param    *pTimer_RatioALDSwitchBS         To check current timer value and update
//! \param    *pTimer_RatioALDChangeACK        To check current timer value and update
//! \param    *pinitialize_RatioALD            Providing and updating the RatioALDSwitch initialization status
//! \param    *pRatioALD_SM                    Provides and Updates the present state based on condition
//!
//!======================================================================================
void RatioALDSwitchTransitions(      A3PosSwitchStatus               *pRatioALDSwitchStatus,
                               const LoadDistributionSelect          *pLoadDistributionSelected,
                               const LoadDistributionALDChangeACK_T  *pLoadDistributionALDChangeACK,
                               const LoadDistributionChangeACK       *pLoadDistributionChangeACK,
                               const LoadDistributionFuncSelected_T  *pLoadDistributionFuncSelected,
                                     uint16                          *pTimer_RatioALDSwitchBS,
                                     uint16                          *pTimer_RatioALDChangeACK,
                                     uint8                           *pinitialize_RatioALD,
                                     RatioALDSwitch_States           *pRatioALD_SM)
{
   //! ###### Process ratio ALD switch state machine transition logic.
   static uint8 ratioALDSwitchBS_Flag = CONST_ResetFlag;
   uint8 current_state                = CONST_ResetFlag;
   pRatioALD_SM->currentValue         = pRatioALD_SM->newValue;
   if (CONST_NonInitialize == *pinitialize_RatioALD)
   {
      pRatioALDSwitchStatus->previousValue = pRatioALDSwitchStatus->currentValue;
      *pTimer_RatioALDSwitchBS             = CONST_TimerFunctionInactive;
      *pTimer_RatioALDChangeACK            = CONST_TimerFunctionInactive;
      ratioALDSwitchBS_Flag                = CONST_ResetFlag;
      pRatioALD_SM->newValue               = SM_ALD_RatioALD_Default;
      *pinitialize_RatioALD                = CONST_Initialize;
   }
   else
   {
      //! ##### Select the state based on 'pRatioALD_SM->newValue'.
      switch (pRatioALD_SM->newValue)
      {
         //! ##### Processing conditions for state change from RatioALD 'LastFunc'.
         case SM_ALD_RatioALD_LastFunc:
            //! #### Check for LoadDistributionSelected and ratio ALD switch status 
            //! #### Check for state change to 'ALD_RatioALD_StandBy'(AlternativeLoad_NotSelected event).
            if (LoadDistributionSelected_AlternativeLoad != pLoadDistributionSelected->currentValue)
            {
               pRatioALD_SM->newValue = SM_ALD_RatioALD_StandBy;
            }
            //! #### Check for state change to 'ALD_RatioALD_ALD_Ack'(AlternativeLoad_Selected event).
            else
            {
               pRatioALD_SM->newValue = SM_ALD_RatioALD_ALD_Ack;
            }
         break;
         //! ##### Processing conditions for state change from RatioALD 'StandBy'.
         case SM_ALD_RatioALD_StandBy:
            //! #### Check for ratio ALD switch status 
            //! #### Check for state change to 'ALD_RatioALD_ALD_On'(Switch_Up event).
            if ((A3PosSwitchStatus_Upper != pRatioALDSwitchStatus->previousValue)
               && (A3PosSwitchStatus_Upper == pRatioALDSwitchStatus->currentValue))
            {
               // Start RatioALDChangeACK Timer
               *pTimer_RatioALDChangeACK = CONST_RatioALD_TimePeriod;
               pRatioALD_SM->newValue    = SM_ALD_RatioALD_ALD_On;
            }
            //! #### Check for state change to 'ALD_RatioALD_Change_LoadReq'(Switch_Down event).
            else if ((A3PosSwitchStatus_Lower != pRatioALDSwitchStatus->previousValue) 
                    && (A3PosSwitchStatus_Lower == pRatioALDSwitchStatus->currentValue))
            {
               // Start RatioALDChangeACK Timer
               *pTimer_RatioALDChangeACK = CONST_RatioALD_TimePeriod;
               pRatioALD_SM->newValue    = SM_ALD_RatioALD_Change_LoadReq;
            }
            //! #### Check for state change to 'ALD_RatioALD_Fallback'(Error detection event).
            else if ((A3PosSwitchStatus_Error != pRatioALDSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Error == pRatioALDSwitchStatus->currentValue))
            {
               pRatioALD_SM->newValue = SM_ALD_RatioALD_Fallback;
            }
            else
            {
               // Do nothing: keep previous status
            }
            //! #### Process the A3_ButtonStuckEvent (Button stuck event): 'A3_ButtonStuckEvent()'
            current_state = A3_ButtonStuckEvent(pRatioALDSwitchStatus,
                                                pTimer_RatioALDSwitchBS,
                                                &ratioALDSwitchBS_Flag);
         break;
         //! ##### Processing conditions for state change from 'RatioALD On'.
         case SM_ALD_RatioALD_ALD_On:
             //! #### Check for LoadDistributionALDChangeACK and RatioALDSwitch status
            //! #### Check for state change to 'ALD_RatioALD_Fallback'(ChangeAcknowledge event).
            if (LoadDistributionALDChangeACK_ChangeAcknowledged == *pLoadDistributionALDChangeACK)
            {
               pRatioALD_SM->newValue   = SM_ALD_RatioALD_ALD_Ack;
            }
            //! #### Check for state change to 'ALD_RatioALD_ALD_Off'(Switch_Up event).
            else if ((A3PosSwitchStatus_Upper != pRatioALDSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Upper == pRatioALDSwitchStatus->currentValue))
            {
               // Start RatioALDChangeACK Timer
               *pTimer_RatioALDChangeACK = CONST_RatioALD_TimePeriod;
               pRatioALD_SM->newValue    = SM_ALD_RatioALD_ALD_Off;
            }
            //! #### Check for state change to 'ALD_RatioALD_ALD_Denied'(System_Denied event).
            else if ((LoadDistributionALDChangeACK_LoadShiftDeniedOverspeed == *pLoadDistributionALDChangeACK)
                    || (LoadDistributionALDChangeACK_SystemDeniedVersatile == *pLoadDistributionALDChangeACK))
            {
               pRatioALD_SM->newValue   = SM_ALD_RatioALD_ALD_Denied;
            }
            //! #### Check for state change to 'ALD_RatioALD_Change_LoadReq'(Switch_Lower event).
            else if (A3PosSwitchStatus_Lower == pRatioALDSwitchStatus->currentValue)
            {
               // Start RatioALDChangeACK Timer
               *pTimer_RatioALDChangeACK = CONST_RatioALD_TimePeriod;
               pRatioALD_SM->newValue    = SM_ALD_RatioALD_Change_LoadReq;
            }
            //! #### Check for state change to 'ALD_RatioALD_Fallback'(Error detection event).
            else if ((A3PosSwitchStatus_Error != pRatioALDSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Error == pRatioALDSwitchStatus->currentValue))
            {
               pRatioALD_SM->newValue   = SM_ALD_RatioALD_Fallback;
            }
            //! #### Check for state change to 'ALD_RatioALD_Fallback'(Button stuck event)
            else if ((CONST_TimerFunctionInactive == *pTimer_RatioALDSwitchBS)
                    && (CONST_SetFlag == ratioALDSwitchBS_Flag))
            {
               // Cancel button stuck timer
               pRatioALD_SM->newValue = SM_ALD_RatioALD_Fallback;
               Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_FAILED);
            }
            else
            {
               // Do nothing: keep previous status until the timeout occurres
            }
            if ((pRatioALDSwitchStatus->previousValue != pRatioALDSwitchStatus->currentValue)
               && ((A3PosSwitchStatus_Upper == pRatioALDSwitchStatus->currentValue)
               || (A3PosSwitchStatus_Lower == pRatioALDSwitchStatus->currentValue)))
            {
               // Start button stuck timer
               *pTimer_RatioALDSwitchBS  = CONST_ButtonStuckTimerPeriod;
               ratioALDSwitchBS_Flag     = CONST_SetFlag;
            }
            else if ((pRatioALDSwitchStatus->previousValue != pRatioALDSwitchStatus->currentValue)
                    && (A3PosSwitchStatus_Upper != pRatioALDSwitchStatus->currentValue)
                    && (A3PosSwitchStatus_Lower != pRatioALDSwitchStatus->currentValue))
            {
               // Cancel button stuck timer
               ratioALDSwitchBS_Flag     = CONST_ResetFlag;
               *pTimer_RatioALDSwitchBS  = CONST_TimerFunctionInactive;
            }
            else
            {
               // Do nothing: keep previous status
            }
         break;
         //! ##### Processing conditions for state change from 'RatioALD Ack'.
         case SM_ALD_RatioALD_ALD_Ack:
            //! #### Check for ratio ALD switch status 
            //! #### Check for state change to 'ALD_RatioALD_ALD_Off'(Switch_Up event).
            if ((A3PosSwitchStatus_Upper != pRatioALDSwitchStatus->previousValue)
               && (A3PosSwitchStatus_Upper == pRatioALDSwitchStatus->currentValue))
            {
               // Start RatioALDChangeACK Timer
               *pTimer_RatioALDChangeACK = CONST_RatioALD_TimePeriod;
               pRatioALD_SM->newValue    = SM_ALD_RatioALD_ALD_Off;
            }
            //! #### Check for state change to 'ALD_RatioALD_Change_LoadReq'(Switch_Lower event).
            else if (A3PosSwitchStatus_Lower == pRatioALDSwitchStatus->currentValue)
            {
               // Start RatioALDChangeACK Timer
               *pTimer_RatioALDChangeACK = CONST_RatioALD_TimePeriod;
               pRatioALD_SM->newValue    = SM_ALD_RatioALD_Change_LoadReq;
            }
            //! #### Check for state change to 'ALD_RatioALD_Fallback'(Error detection event).
            else if ((A3PosSwitchStatus_Error != pRatioALDSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Error == pRatioALDSwitchStatus->currentValue))
            {
               pRatioALD_SM->newValue   = SM_ALD_RatioALD_Fallback;
            }
            else
            {
               // Do nothing: keep previous status until the timeout occurres
            }
            //! #### Process the A3_ButtonStuckEvent (Button stuck event): 'A3_ButtonStuckEvent()'
            current_state = A3_ButtonStuckEvent(pRatioALDSwitchStatus,
                                                pTimer_RatioALDSwitchBS,
                                                &ratioALDSwitchBS_Flag);
         break;
         //! ##### Processing conditions for state change from 'RatioALD Off'.
         case SM_ALD_RatioALD_ALD_Off:
            //! #### Check for ratio ALD switch status
            //! #### Unconditional transition for state change to the 'ALD_RatioALD_StandBy' (Unconditional transition event).
         //! ##### Processing conditions for state change from 'RatioALD Denied'.
         case SM_ALD_RatioALD_ALD_Denied:
            pRatioALD_SM->newValue = SM_ALD_RatioALD_StandBy;         
         break;
         //! ##### Processing conditions for state change from 'RatioALD Change LoadReq'.
         case SM_ALD_RatioALD_Change_LoadReq:
            //! #### Check for LoadDistributionChangeACK and RatioALDSwitch Status
            //! #### Check for state change to 'ALD_RatioALD_StandBy'(NoAcknowledge event).
            if (CONST_TimerFunctionElapsed == *pTimer_RatioALDChangeACK)
            {
               // Cancel ACK Timer
               *pTimer_RatioALDChangeACK = CONST_TimerFunctionInactive;
               pRatioALD_SM->newValue    = SM_ALD_RatioALD_StandBy;
            }
            //! #### Check for state change to 'ALD_RatioALD_ALD_On'(Switch_Upper event).
            else if (A3PosSwitchStatus_Upper == pRatioALDSwitchStatus->currentValue)
            {
               // Start RatioALDChangeACK Timer
               *pTimer_RatioALDChangeACK = CONST_RatioALD_TimePeriod;
			   *pTimer_RatioALDSwitchBS  = CONST_ButtonStuckTimerPeriod;
               pRatioALD_SM->newValue    = SM_ALD_RatioALD_ALD_On;
			   ratioALDSwitchBS_Flag     = CONST_SetFlag;
            }
            //! #### Check for state change to 'ALD_RatioALD_Change_Accepted'(Fdbk_Acknowledge event).
            else if ((LoadDistributionChangeACK_ChangeAcknowledged == pLoadDistributionChangeACK->currentValue)
                    || (LoadDistributionChangeACK_LoadShiftDeniedFALIMOverload == pLoadDistributionChangeACK->currentValue)
                    || (LoadDistributionChangeACK_SystemDeniedVersatile == pLoadDistributionChangeACK->currentValue))
            {
               // Start ACK Timer
               *pTimer_RatioALDChangeACK = CONST_RatioALD_TimePeriod;
               pRatioALD_SM->newValue    = SM_ALD_RatioALD_Change_Accepted;
            }
            //! #### Check for state change to 'ALD_RatioALD_Fallback'(Error detection event).
            else if ((A3PosSwitchStatus_Error != pRatioALDSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Error == pRatioALDSwitchStatus->currentValue))
            {
               pRatioALD_SM->newValue = SM_ALD_RatioALD_Fallback;
            }
            else
            {
               // Do nothing: keep previous status until the timeout occurres
            }
            if ((pLoadDistributionChangeACK->previousValue != pLoadDistributionChangeACK->currentValue)
               && (LoadDistributionChangeACK_ChangeAcknowledged != pLoadDistributionChangeACK->currentValue)
               && (LoadDistributionChangeACK_LoadShiftDeniedFALIMOverload != pLoadDistributionChangeACK->currentValue)
               && (LoadDistributionChangeACK_SystemDeniedVersatile != pLoadDistributionChangeACK->currentValue))
            {
               // Cancel ACK Timer
               *pTimer_RatioALDChangeACK = CONST_TimerFunctionInactive;
            }
            else
            {
               // Do nothing: keep previous status until the timeout occurres
            }
         break;
         //! ##### Processing conditions for state change from 'RatioALD Change_Accepted'.
         case SM_ALD_RatioALD_Change_Accepted:
            //! #### Check for LoadDistributionChangeACK and RatioALDSwitch Status 
            //! #### Check for state change to 'ALD_RatioALD_StandBy'(NoAcknowledge or NoActionAcknowledge event).
            if ((CONST_TimerFunctionElapsed == *pTimer_RatioALDChangeACK)
               || (LoadDistributionChangeACK_NoAction == pLoadDistributionChangeACK->currentValue))
            {
               // Cancel ACK Timer
               *pTimer_RatioALDChangeACK = CONST_TimerFunctionInactive;
               pRatioALD_SM->newValue    = SM_ALD_RatioALD_StandBy;
            }
            //! #### Check for state change to 'ALD_RatioALD_ALD_On'(Switch_Upper event).
            else if (A3PosSwitchStatus_Upper == pRatioALDSwitchStatus->currentValue)
            {
               // Start RatioALDChangeACK Timer
               *pTimer_RatioALDChangeACK = CONST_RatioALD_TimePeriod;
			   *pTimer_RatioALDSwitchBS  = CONST_ButtonStuckTimerPeriod;
               pRatioALD_SM->newValue    = SM_ALD_RatioALD_ALD_On;
			   ratioALDSwitchBS_Flag     = CONST_SetFlag;
            }
            //! #### Check for state change to 'ALD_RatioALD_Fallback'(Error detection event).
            else if ((A3PosSwitchStatus_Error != pRatioALDSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Error == pRatioALDSwitchStatus->currentValue))
            {
               pRatioALD_SM->newValue   = SM_ALD_RatioALD_Fallback;
            }
            else
            {
               // Do nothing: keep previous status until the timeout occurres
            }
            if (pLoadDistributionChangeACK->previousValue != pLoadDistributionChangeACK->currentValue)
            {
               // Cancel ACK Timer
               *pTimer_RatioALDChangeACK = CONST_TimerFunctionInactive;
            }
            else
            {
               // Do nothing: keep previous status until the timeout occurres
            }
         break;
         //! ##### Processing conditions for state change from 'RatioALD Fallback'.
         case SM_ALD_RatioALD_Fallback:
         //! #### Check for ratio ALD switch status
            //! #### Check for state change to 'ALD_RatioALD_Default'(Solve error event).
            if ((pRatioALDSwitchStatus->currentValue != pRatioALDSwitchStatus->previousValue)
               && (A3PosSwitchStatus_Error != pRatioALDSwitchStatus->currentValue))
            {
               pRatioALD_SM->newValue = SM_ALD_RatioALD_Default;
               Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            }
            else
            {
               // Do nothing: keep previous status
            }
            //! #### Process the A3_ButtonStuckEvent (Button stuck event): 'A3_ButtonStuckEvent()'
            current_state = A3_ButtonStuckEvent(pRatioALDSwitchStatus,
                                                pTimer_RatioALDSwitchBS,
                                                &ratioALDSwitchBS_Flag);
         break;
         //! ##### Processing conditions for state change from 'RatioALD Default'.
         default: //SM_ALD_RatioALD_Default
            //! #### Check for LoadDistributionChangeACK, LoadDistributionALDChangeACK, LoadDistributionFuncSelected and RatioALDSwitch Status 
            //! #### Check for state change to 'ALD_RatioALD_LastFunc'(System ready event)
            if ((A3PosSwitchStatus_Middle == pRatioALDSwitchStatus->currentValue)
               && (LoadDistributionChangeACK_NotAvailable != pLoadDistributionChangeACK->currentValue)
               && (LoadDistributionALDChangeACK_NotAvailable != *pLoadDistributionALDChangeACK)
               && (LoadDistributionFuncSelected_NotAvaiable != *pLoadDistributionFuncSelected))
            {
               pRatioALD_SM->newValue = SM_ALD_RatioALD_LastFunc;
            }
            //! #### Check for state change to 'ALD_RatioALD_Fallback'(Error detection event).
            else if ((A3PosSwitchStatus_Error != pRatioALDSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Error == pRatioALDSwitchStatus->currentValue))
            {
               pRatioALD_SM->newValue = SM_ALD_RatioALD_Fallback;
            }
            else
            {
               // Do nothing: keep previous status
            }
            //! #### Process the A3_ButtonStuckEvent (Button stuck event): 'A3_ButtonStuckEvent()'
            current_state = A3_ButtonStuckEvent(pRatioALDSwitchStatus,
                                                pTimer_RatioALDSwitchBS,
                                                &ratioALDSwitchBS_Flag);
         break;
      }
       if (CONST_SetFlag == current_state)
      {
         pRatioALD_SM->newValue = SM_ALD_RatioALD_Fallback;
      }
      else
      {
         // Do nothing: keep previous status
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'RatioALDSwitchOutputProcessing'
//!
//! \param   *pRatioALD_SM                              Providing the current state value
//! \param   *pRatioALD_LoadDistributionChangeRequest   Updating the output signal based on current state
//! \param   *pRatioALD_AltLoadDistribution_rqst        Updating the output signal based on current state
//!
//!======================================================================================
void RatioALDSwitchOutputProcessing(const RatioALDSwitch_States            *pRatioALD_SM,
                                          LoadDistributionChangeRequest_T  *pRatioALD_LoadDistributionChangeRequest,
                                          AltLoadDistribution_rqst_T       *pRatioALD_AltLoadDistribution_rqst)
{
   //! ###### Processing ratio ALD switch output processing logic
   // Process output only if current value is not equal to new value
   if (pRatioALD_SM->currentValue != pRatioALD_SM->newValue)
   {
      //! ##### Select the state based on 'pRatioALD_SM->newValue'
      switch (pRatioALD_SM->newValue)
      {
         //! #### No output action for 'RatioALD_LastFunc'
         case SM_ALD_RatioALD_LastFunc:
            //No Action
         break;
         //! #### Output action for 'RatioALD_StandBy' state
         case SM_ALD_RatioALD_StandBy:
            *pRatioALD_LoadDistributionChangeRequest = LoadDistributionChangeRequest_Idle;
         break;
         //! #### Output action for 'RatioALD_On' state
         case SM_ALD_RatioALD_ALD_On:
            *pRatioALD_AltLoadDistribution_rqst = AltLoadDistribution_rqst_ALDOn;
         break;
         //! #### Output action for 'RatioALD_Ack' state
         case SM_ALD_RatioALD_ALD_Ack:
            *pRatioALD_AltLoadDistribution_rqst = AltLoadDistribution_rqst_Idle;
         break;
         //! #### Output action for 'RatioALD_Off' state
         case SM_ALD_RatioALD_ALD_Off:
            *pRatioALD_AltLoadDistribution_rqst = AltLoadDistribution_rqst_ALDOff;
         break;
         //! #### Output action for 'RatioALD_Denied' state
         case SM_ALD_RatioALD_ALD_Denied:
            *pRatioALD_AltLoadDistribution_rqst = AltLoadDistribution_rqst_Idle;
         break;
         //! #### Output action for 'RatioALD_Change_LoadReq' state
         case SM_ALD_RatioALD_Change_LoadReq:
            *pRatioALD_LoadDistributionChangeRequest = LoadDistributionChangeRequest_ChangeLoadDistribution;
         break;
         //! #### Output action for 'RatioALD_Change_Accepted' state
         case SM_ALD_RatioALD_Change_Accepted:
            *pRatioALD_LoadDistributionChangeRequest = LoadDistributionChangeRequest_Idle;
         break;
         //! #### Output actions for 'RatioALD_Fallback' state
         case SM_ALD_RatioALD_Fallback:
            *pRatioALD_LoadDistributionChangeRequest = LoadDistributionChangeRequest_Error;
            *pRatioALD_AltLoadDistribution_rqst      = AltLoadDistribution_rqst_Error ;
         break;
         //! #### Output actions for 'RatioALD_Default' state
         default: //SM_ALD_RatioALD_Default
            *pRatioALD_LoadDistributionChangeRequest = LoadDistributionChangeRequest_Idle;
            *pRatioALD_AltLoadDistribution_rqst      = AltLoadDistribution_rqst_Idle;
         break;
      }
   }
   else
   {
      // Do nothing: keep previous state
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'RatioALD_LED_Logic'
//!
//! \param   *pLoadDistributionFuncSelected    Provides the current status
//! \param   *pRatioALD_DualDeviceIndication   Updating the Device indication status
//!
//!======================================================================================
void RatioALD_LED_Logic(const LoadDistributionFuncSelected_T  *pLoadDistributionFuncSelected,
                              DualDeviceIndication_T          *pRatioALD_DualDeviceIndication)
{
   //! ###### Processing RatioALD_LED Logic
   //! ##### Updating DualDeviceIndication based on 'LoadDistributionFuncSelected' status
   if (LoadDistributionFuncSelected_AlternativeLoad == *pLoadDistributionFuncSelected)
   {
      *pRatioALD_DualDeviceIndication = DualDeviceIndication_UpperOnLowerOff;
   }
   else if (LoadDistributionFuncSelected_MaximumTractionTractionHelp == *pLoadDistributionFuncSelected)
   {
      *pRatioALD_DualDeviceIndication = DualDeviceIndication_UpperOffLowerOn;
   }
   else if (LoadDistributionFuncSelected_MaximumTractionStartingHelp == *pLoadDistributionFuncSelected)
   {
      *pRatioALD_DualDeviceIndication = DualDeviceIndication_UpperOffLowerBlink;
   }
   else
   {
      *pRatioALD_DualDeviceIndication = DualDeviceIndication_UpperOffLowerOff;
   }
}

//!======================================================================================
//!
//! \brief
//! This function is processing the 'A3_ButtonStuckEvent'
//!
//! \param   *pA3Switch         Providing switch status value
//! \param   *pA3timer          To check current Timer value and update
//! \param   *pflag             To check current flag value and update
//!
//!======================================================================================
uint8 A3_ButtonStuckEvent (const A3PosSwitchStatus      *pA3Switch,
                                 uint16                 *pA3timer,
                                 uint8                  *pflag)
{
   uint8 state = CONST_ResetFlag;
   //! #### Check the A3 position switch status
   if ((A3PosSwitchStatus_Upper == pA3Switch->currentValue)
      || (A3PosSwitchStatus_Lower == pA3Switch->currentValue))
   {
      if (pA3Switch->previousValue != pA3Switch->currentValue)
      {
         // Start button stuck timer
         *pA3timer = CONST_ButtonStuckTimerPeriod;
         *pflag   = CONST_SetFlag;
      }
      //! #### Check for state change to 'Fallback'(Button stuck event)
      else if ((CONST_TimerFunctionInactive == *pA3timer)
              && (CONST_SetFlag == *pflag))
      {
         // Cancel button stuck timer
         state   = CONST_SetFlag;
         Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         // Do nothing: keep previous status
      }
   }
   else
   {
      // Cancel button stuck timer
      *pflag   = CONST_ResetFlag;
      *pA3timer = CONST_TimerFunctionInactive;
      Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   return state;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'A2_ButtonStuckEvent'
//!
//! \param   *pA2Switch        Providing switch status value
//! \param   *pA2timer           To check current Timer value and update
//! \param   *pflag            To check current flag value and update
//!
//!======================================================================================
uint8 A2_ButtonStuckEvent (const A2PosSwitchStatus      *pA2Switch,
                                 uint16                 *pA2timer,
                                 uint8                  *pflag)
{
   uint8 state = CONST_ResetFlag;
   //! #### Check the A2 position switch status
   if (A2PosSwitchStatus_On == pA2Switch->currentValue)
   {
      if (pA2Switch->previousValue != pA2Switch->currentValue)
      {
         // Start button stuck timer
         *pA2timer = CONST_ButtonStuckTimerPeriod;
         *pflag   = CONST_SetFlag;
      }
      //! #### Check for state change to 'Fallback'(Button stuck event)
      else if ((CONST_TimerFunctionInactive == *pA2timer)
              && (CONST_SetFlag == *pflag))
      {
         // Cancel button stuck timer
         state   = CONST_SetFlag;
         Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         // Do nothing: keep previous status
      }
   }
   else
   {
      // Cancel button stuck timer
      *pflag   = CONST_ResetFlag;
      *pA2timer = CONST_TimerFunctionInactive;
      Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
   }
   return state;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'Check_Acknowledge'
//!
//! \param   *pLoadDistributionRequestedACK    Providing load distribution request value
//! \param   *pTimer_LoadDistributionReqACK    To check current Timer value and update
//! \param   *pDualRatioSwitchSM               Updates the present state value
//! \param   *pisAck_Check                     To check current flag value and update
//!
//!======================================================================================
void Check_Acknowledge(const LoadDistributionRequestedACK_T  *pLoadDistributionRequestedACK,
                             uint16                          *pTimer_LoadDistributionReqACK,
                             DualRatioSwitch_States          *pDualRatioSwitchSM,
                             boolean                         *pisAck_Check)
{
   //! #### Check for DualRatioSwitch status and LoadDistributionRequestedACK status
   if (LoadDistributionRequestedACK_NoAction != *pLoadDistributionRequestedACK)
   {
      // Cancel LDReqACK Timer
      *pTimer_LoadDistributionReqACK = CONST_TimerFunctionInactive;
      pDualRatioSwitchSM->newValue = SM_DRS_NoActionRequested;
      *pisAck_Check = TRUE;
   }
   else if (CONST_TimerFunctionElapsed == *pTimer_LoadDistributionReqACK)
   {
      // Cancel LDReqACK Timer
      *pTimer_LoadDistributionReqACK = CONST_TimerFunctionInactive;
      pDualRatioSwitchSM->newValue = SM_DRS_NoActionRequested;
      *pisAck_Check = TRUE;
   }
   else 
   {
      // Do nothing
   }
}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/