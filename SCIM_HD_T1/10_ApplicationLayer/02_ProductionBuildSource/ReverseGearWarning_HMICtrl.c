/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  ReverseGearWarning_HMICtrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  ReverseGearWarning_HMICtrl
 *  Generated at:  Fri Jun 12 17:10:04 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <ReverseGearWarning_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file ReverseGearWarning_HMICtrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_BodyAndSecurity 
//! @{
//! @addtogroup ReverseGearWarning_HMICtrl
//! @{
//!
//! \brief
//! ReverseGearWarning_HMICtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the ReverseGearWarning_HMICtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_ReverseGearWarning_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
// Includes
#include "ReverseGearWarning_HMICtrl.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * ReverseWarning_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 *   ReverseWarning_rqst_SoundOff (0U)
 *   ReverseWarning_rqst_SoundOn (1U)
 *   ReverseWarning_rqst_Error (2U)
 *   ReverseWarning_rqst_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *   FALSE (0U)
 *   TRUE (1U)
 *
 * Array Types:
 * ============
 * StandardNVM_T: Array with 4 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1BXH_ReverseWarning_SwType_v(void)
 *   boolean Rte_Prm_P1AJJ_ReverseWarning_Act_v(void)
 *
 *********************************************************************************************************************/


#define ReverseGearWarning_HMICtrl_START_SEC_CODE
#include "ReverseGearWarning_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_ReverseWarning_Act    (Rte_Prm_P1AJJ_ReverseWarning_Act_v())
#define PCODE_ReverseWarning_SwType (Rte_Prm_P1BXH_ReverseWarning_SwType_v())

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ReverseGearWarning_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_RGW_HMICtrl_NVM_I_RGW_HMICtrl_NVM_I(uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Read_ReverseGearWarningBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_ReverseGearWarningSw_stat_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_RGW_HMICtrl_NVM_I_RGW_HMICtrl_NVM_I(const uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Write_ReverseWarningInd_cmd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_ReverseWarning_rqst_ReverseWarning_rqst(ReverseWarning_rqst_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ReverseGearWarning_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the ReverseGearWarning_HMICtrl_20ms_runnable
//!
//!==================================================================================================================== 

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ReverseGearWarning_HMICtrl_CODE) ReverseGearWarning_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ReverseGearWarning_HMICtrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   static ReverseGearWarning_HMICtrl_InStruct  RteInData_Common = {{PushButtonStatus_NotAvailable,
                                                                    PushButtonStatus_NotAvailable},
                                                                   A2PosSwitchStatus_NotAvailable,
																   NonOperational};
   static ReverseGearWarning_HMICtrl_OutStruct RteOutData_Common              = { ReverseWarning_rqst_NotAvailable,
                                                                                  DeviceIndication_SpareValue };
   static ReverseGearWarning_States            ReverseGearWarningSM_State     = SM_RGW_Request_SoundOff;
   static uint8  RGW_InitializeStates        = CONST_NonInitialize;
   static uint8  arDataStore[MemoryBlocks]   = {255U,255U,255U,255U};
   static uint8  Previous_State              = 255U;

   //! ###### Process RTE Read common logic: 'Get_RteInDataRead_Common()'
   Get_RteInDataRead_Common(&RteInData_Common);
   
   //! ###### Check for 'SwcActivation_IgnitionOn' port is operational entry
   if (OperationalEntry == RteInData_Common.SwcActivation_IgnitionOn)
   {
      (void)Rte_Read_RGW_HMICtrl_NVM_I_RGW_HMICtrl_NVM_I(arDataStore);
      //! ##### Check for the version of the NVRAM
      if (CONST_NvramReverseGearVersion == arDataStore[CONST_NvramVersionIndex])
      {
         // if version is in between 1-254, which means NVRAM data shall be initialized
         if (255U == arDataStore[CONST_NvramReverseGearValueIndex])
         {
            ReverseGearWarningSM_State = SM_RGW_Request_SoundOff;
         }
         else 
         {
            ReverseGearWarningSM_State = (ReverseGearWarning_States)arDataStore[CONST_NvramReverseGearValueIndex];
         }
      }
      else
      {
         //! ##### Write CONST_NvramReverseGearVersion to the NVRAM
         arDataStore[CONST_NvramVersionIndex] = CONST_NvramReverseGearVersion;
      }
   }
   //! ###### Check for 'SwcActivation_IgnitionOn' port is operational
   else if (Operational == RteInData_Common.SwcActivation_IgnitionOn)
   {
      //! ##### Select the 'ReverseWarning_Act' parameter 
      if (TRUE == PCODE_ReverseWarning_Act)
      {
         //! ##### Check for 'ReverseWarning_SwType' parameter status
         if (FALSE == PCODE_ReverseWarning_SwType)
         {
            //! #### Process ReverseGearWarning VT configuration logic : 'ReverseGearWarning_VT_Configuration()'
            ReverseGearWarning_VT_Configuration(&RteInData_Common.ReverseGearWarningSw_stat,
                                                &RteOutData_Common.ReverseWarning_rqst);
         }
         //! ##### Check for 'ReverseWarning_SwType' parameter status
         else if (TRUE == PCODE_ReverseWarning_SwType)
         {
            //! #### Process ReverseGearWarning state transition logic : 'ReverseGearWarning_RT_Configuration()'
            ReverseGearWarning_RT_Configuration(&RteInData_Common.ReverseGearWarningBtn_stat,
                                                &RGW_InitializeStates,
                                                &ReverseGearWarningSM_State);
            Previous_State = (uint8)ReverseGearWarningSM_State;
            //! #### Process ReverseGearWarning StateMachine output logic : 'ReverseGearWarningStateOutputProcessing()'
            ReverseGearWarningStateOutputProcessing(&ReverseGearWarningSM_State,
                                                    &RteOutData_Common);
         }
         else
         {
            // Do nothing, keep previous status
         }
      }
      //! ##### Processing ReverseGearWarning disabled logic
      else if (FALSE == PCODE_ReverseWarning_Act)
      {
         RteOutData_Common.ReverseWarning_rqst   = ReverseWarning_rqst_SoundOff;
         RteOutData_Common.ReverseWarningInd_cmd = DeviceIndication_Off;
      }
      else
      {
         // Do nothing, keep previous status
      }
   }
   //! ###### Check for 'SwcActivation_IgnitionOn' port is operational exit
   else if (OperationalExit == RteInData_Common.SwcActivation_IgnitionOn)
   {
      if (Previous_State != arDataStore[CONST_NvramReverseGearValueIndex])
      {
         arDataStore[CONST_NvramReverseGearValueIndex] = Previous_State;
         (void)Rte_Write_RGW_HMICtrl_NVM_I_RGW_HMICtrl_NVM_I(arDataStore);
      }
      else
      {
         // Do Nothing : wait for the state change to update in NVRAM
      }
   }
   else
   {
      //! ###### ReverseGearWarning_HMICtrl logic output value for other then operational mode
      RteOutData_Common.ReverseWarning_rqst    = ReverseWarning_rqst_NotAvailable;
      RteOutData_Common.ReverseWarningInd_cmd  = DeviceIndication_Off;
      RGW_InitializeStates                     = CONST_NonInitialize;
   }
   //! ###### Process RTE write common logic: 'RteOutDataWrite_Common()'
   RteOutDataWrite_Common(&RteOutData_Common);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define ReverseGearWarning_HMICtrl_STOP_SEC_CODE
#include "ReverseGearWarning_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief
//!  This function implements the logic for the 'Get_RteInDataRead_Common'
//!
//! \param   *pRteInData_Common   Examine and update the input signals based on RTE failure events
//!
//!======================================================================================
static void Get_RteInDataRead_Common(ReverseGearWarning_HMICtrl_InStruct *pRteInData_Common)
{
   //! ###### Read RTE ports and process RTE failure events for error indication mode
   Std_ReturnType retValue = RTE_E_INVALID;

   pRteInData_Common->ReverseGearWarningBtn_stat.prevValue = pRteInData_Common->ReverseGearWarningBtn_stat.newValue;
   //! ##### Read 'ReverseGearWarningBtn_stat' interface
   retValue = Rte_Read_ReverseGearWarningBtn_stat_PushButtonStatus(&pRteInData_Common->ReverseGearWarningBtn_stat.newValue);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->ReverseGearWarningBtn_stat.newValue), (PushButtonStatus_Error))
   //! ##### Read 'ReverseGearWarningSw_stat' interface
   retValue = Rte_Read_ReverseGearWarningSw_stat_A2PosSwitchStatus(&pRteInData_Common->ReverseGearWarningSw_stat);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->ReverseGearWarningSw_stat), (A2PosSwitchStatus_Error))
   //! ##### Read 'SwcActivation_IgnitionOn' interface
   retValue = Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&pRteInData_Common->SwcActivation_IgnitionOn);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->SwcActivation_IgnitionOn), (NonOperational))
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'RteOutDataWrite_Common'
//!
//! \param   *pRteOutData_common   Updating the output signals to the ports of output structure
//!
//!======================================================================================
static void RteOutDataWrite_Common(const ReverseGearWarning_HMICtrl_OutStruct *pRteOutData_common)
{
   //! ###### Processing RTE write common logic
   Rte_Write_ReverseWarningInd_cmd_DeviceIndication(pRteOutData_common->ReverseWarningInd_cmd);
   (void)Rte_Write_ReverseWarning_rqst_ReverseWarning_rqst(pRteOutData_common->ReverseWarning_rqst);
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'ReverseGearWarning_VT_Configuration'
//!
//! \param   *pReverseGearWarningSw_stat   Providing current switch status of input data
//! \param   *pReverseWarning_rqst         Update the ReverseWarning_rqst output value
//!
//!======================================================================================
static void ReverseGearWarning_VT_Configuration(const A2PosSwitchStatus_T   *pReverseGearWarningSw_stat,
                                                      ReverseWarning_rqst_T *pReverseWarning_rqst)
{
   //! ###### Processing ReverseGearWarning VT configuration logic
   //! ##### Check for ReverseGearWarning switch status
   //! #### Updating the output port value
   if (A2PosSwitchStatus_On == *pReverseGearWarningSw_stat)
   {
      *pReverseWarning_rqst = ReverseWarning_rqst_SoundOn;
   }
   else if (A2PosSwitchStatus_Off == *pReverseGearWarningSw_stat)
   { 
      *pReverseWarning_rqst = ReverseWarning_rqst_SoundOff;
   }
   else
   {
      *pReverseWarning_rqst = ReverseWarning_rqst_Error;
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'ReverseGearWarning_RT_Configuration'
//!
//! \param    *pReverseGearWarningBtn_stat   Providing the current button status of input data
//! \param    *pRGWInitializeStates          Indicating current values for input data
//! \param    *pReverseGearWarningSM         Updating the state's present status
//!
//!======================================================================================
static void ReverseGearWarning_RT_Configuration(PushButtonStatus           *pReverseGearWarningBtn_stat,
                                                uint8                      *pRGWInitializeStates,
                                                ReverseGearWarning_States  *pReverseGearWarningSM)
{
   if (CONST_NonInitialize == *pRGWInitializeStates)
   {
      pReverseGearWarningBtn_stat->prevValue = pReverseGearWarningBtn_stat->newValue;
      *pRGWInitializeStates                  = CONST_Initialize;
   }
   else
   {
      switch (*pReverseGearWarningSM)
      {
         //! ###### Check for ReverseGearWarning button status
         case SM_RGW_Request_SoundOn:
            //! ##### Check for state change from 'sound_on' to 'sound_off'
            if ((PushButtonStatus_Neutral == pReverseGearWarningBtn_stat->prevValue)
               &&(PushButtonStatus_Pushed == pReverseGearWarningBtn_stat->newValue))
            {
               *pReverseGearWarningSM = SM_RGW_Request_SoundOff;
            }  
            else
            {
               // Do nothing, Invalid status from button
            }
         break;
         case SM_RGW_Request_Error:
            //! ##### Check for state change from 'error' to 'sound_off'
            if (((PushButtonStatus_NotAvailable == pReverseGearWarningBtn_stat->prevValue)
               || (PushButtonStatus_Error == pReverseGearWarningBtn_stat->prevValue))
               && (PushButtonStatus_Neutral == pReverseGearWarningBtn_stat->newValue))
            {
               *pReverseGearWarningSM = SM_RGW_Request_SoundOff;
            }
            else
            {
               // Do nothing, wait for error to resolve
            }
         break;
         default: //case SM_RGW_Request_SoundOff:
            //! ##### Check for state change from 'sound_off' to 'sound_on'
            if ((PushButtonStatus_Neutral == pReverseGearWarningBtn_stat->prevValue)
               && (PushButtonStatus_Pushed == pReverseGearWarningBtn_stat->newValue))
            {
               *pReverseGearWarningSM = SM_RGW_Request_SoundOn;
            }
            else
            {
               // Do nothing, wait for error to resolve
            }
         break;
      }
      //! ##### Check for Fallback condition
      if ((PushButtonStatus_NotAvailable == pReverseGearWarningBtn_stat->newValue)
         || (PushButtonStatus_Error == pReverseGearWarningBtn_stat->newValue))
      {
         *pReverseGearWarningSM = SM_RGW_Request_Error;
      }
      else
      {
         // Do nothing, wait for error to resolve
      }
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'ReverseGearWarningStateOutputProcessing'
//!
//! \param    *pReverseGearWarningSM   Providing the current state value
//! \param    *pRGW_OutData            Update the output values to ReverseWarning_rqst and ReverseWarningInd_cmd
//!
//!======================================================================================
static void ReverseGearWarningStateOutputProcessing(const ReverseGearWarning_States            *pReverseGearWarningSM,
                                                          ReverseGearWarning_HMICtrl_OutStruct *pRGW_OutData)
{
   //! ###### Processing current state output actions
   switch (*pReverseGearWarningSM)
   {
      //! ##### Output actions for 'sound_on' state
      case SM_RGW_Request_SoundOn:
         pRGW_OutData->ReverseWarning_rqst   = ReverseWarning_rqst_SoundOn;
         pRGW_OutData->ReverseWarningInd_cmd = DeviceIndication_On;
      break;
      //! ##### Output actions for 'error' state
      case SM_RGW_Request_Error:
         pRGW_OutData->ReverseWarning_rqst   = ReverseWarning_rqst_Error;
         pRGW_OutData->ReverseWarningInd_cmd = DeviceIndication_Off;
      break;
         //! ##### Output actions for 'sound_off' state
      default: //case SM_RGW_Request_SoundOff:
         pRGW_OutData->ReverseWarning_rqst   = ReverseWarning_rqst_SoundOff;
         pRGW_OutData->ReverseWarningInd_cmd = DeviceIndication_Off;
      break;
   }
}

//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
