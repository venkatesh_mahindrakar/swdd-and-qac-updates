/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  ServiceBraking_HMICtrl.c
 *        Config:  D:/GitSCIM/scim_seoyon/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  ServiceBraking_HMICtrl
 *  Generated at:  Tue Oct 15 21:51:07 2019
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <ServiceBraking_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file ServiceBraking_HMICtrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_VehicleDynamics_Suspension 
//! @{
//! @addtogroup ServiceBraking_HMICtrl
//! @{
//!
//! \brief
//! ServiceBraking_HMICtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the ServiceBraking_HMICtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_ABS_Inhibit_SwType_P1SY6_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_ABS_Inhibit_SwType_P1SY6_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Speed16bit_T
 *   65024 - 65279 Error ; 65280 - 65535 Not available
 *
 *********************************************************************************************************************/

#include "Rte_ServiceBraking_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
// Includes 
#include "ServiceBraking_HMICtrl.h"
#include "FuncLibrary_Timer_If.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_ABS_Inhibit_SwType_P1SY6_T: Integer in interval [0...255]
 * SEWS_ABS_Inhibit_SwType_P1SY6_T: Integer in interval [0...255]
 * Speed16bit_T: Integer in interval [0...65535]
 *   Unit: [Km_per_h], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   InactiveActive_Inactive (0U)
 *   InactiveActive_Active (1U)
 *   InactiveActive_Error (2U)
 *   InactiveActive_NotAvailable (3U)
 * Inhibit_T: Enumeration of integer in interval [0...3] with enumerators
 *   Inhibit_NoInhibit (0U)
 *   Inhibit_InhibitActive (1U)
 *   Inhibit_Error (2U)
 *   Inhibit_NotAvailable (3U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PassiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   PassiveActive_Passive (0U)
 *   PassiveActive_Active (1U)
 *   PassiveActive_Error (2U)
 *   PassiveActive_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_ABS_Inhibit_SwType_P1SY6_T Rte_Prm_P1SY6_ABS_Inhibit_SwType_v(void)
 *   boolean Rte_Prm_P1A1R_HSA_Installed_v(void)
 *   boolean Rte_Prm_P1NTV_HSA_DefaultConfig_v(void)
 *   boolean Rte_Prm_P1SY4_ABS_Inhibit_Installed_v(void)
 *
 *********************************************************************************************************************/


#define ServiceBraking_HMICtrl_START_SEC_CODE
#include "ServiceBraking_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_ABS_Inhibit_SwType      (Rte_Prm_P1SY6_ABS_Inhibit_SwType_v())
#define PCODE_HSA_Installed           (Rte_Prm_P1A1R_HSA_Installed_v())
#define PCODE_HSA_DefaultConfig       (Rte_Prm_P1NTV_HSA_DefaultConfig_v())
#define PCODE_ABS_Inhibit_Installed   (Rte_Prm_P1SY4_ABS_Inhibit_Installed_v())

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SB_ABS_Inhibit_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ABSInhibitSwitchStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_ABSInhibitionStatus_ABSInhibitionStatus(Inhibit_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ABSInhibitionRequest_ABSInhibitionRequest(OffOn_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SB_ABS_Inhibit_20ms_runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the SB_ABS_Inhibit_20ms_runnable
//!
//!==================================================================================================================== 

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ServiceBraking_HMICtrl_CODE) SB_ABS_Inhibit_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SB_ABS_Inhibit_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   static ServiceBraking_HMICtrl_in_StructType  RteInData_ABSCommon;
   static SB_HMICtrl_ABSin_StructType           RteInData_ABS;
   static SB_HMICtrl_ABSout_StructType          RteOutData_ABS = { OffOn_NotAvailable };
          Std_ReturnType                        retValue       = RTE_E_INVALID;

   //! ##### Read ABSInhibitSwitchStatus interface
   retValue = Rte_Read_ABSInhibitSwitchStatus_PushButtonStatus(&RteInData_ABS.ABSInhibitSwitchStatus);
   MACRO_StdRteRead_IntRPort((retValue), (RteInData_ABS.ABSInhibitSwitchStatus), (PushButtonStatus_Error))
   //! ##### Read SwcActivation_IgnitionOn interface
   retValue = Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&RteInData_ABSCommon.SwcActivation_IgnitionOn);
   MACRO_StdRteRead_IntRPort((retValue), (RteInData_ABSCommon.SwcActivation_IgnitionOn), (NonOperational))
   //! ###### Check for SwcActivation_IgnitionOn port status
   if (Operational == RteInData_ABSCommon.SwcActivation_IgnitionOn)
   {
      //! ##### Check for 'ABS Inhibit installed' parameter
      if (TRUE == PCODE_ABS_Inhibit_Installed)
      {
         //! ##### Check for 'ABS Inhibit switchType' parameter
         if (1U == PCODE_ABS_Inhibit_SwType)
         {
            //! #### Processing ABS Inhibit logic : 'ABS_InhibitLogic()'
            ABS_InhibitLogic(&RteInData_ABS,
                             &RteOutData_ABS);
         }
         else
         {
            // Do nothing: keep the previous status
         }
      }
      else
      {
         //! ##### Processing ABS function disable logic
         RteOutData_ABS.ABSInhibitionRequest = OffOn_NotAvailable;
      }
   }
   else
   {
      // If invalid vehicle mode is given then default value
      RteOutData_ABS.ABSInhibitionRequest = OffOn_NotAvailable;
   }
   //! ###### Process RTE write port
   (void)Rte_Write_ABSInhibitionRequest_ABSInhibitionRequest(RteOutData_ABS.ABSInhibitionRequest);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SB_HSA_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ASRHillHolderSwitch_ASRHillHolderSwitch(PassiveActive_T *data)
 *   Std_ReturnType Rte_Read_HillStartAidButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_HSADriverRequest_HSADriverRequest(InactiveActive_T data)
 *   Std_ReturnType Rte_Write_HillStartAid_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SB_HSA_20ms_runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the SB_HSA_20ms_runnable
//!
//!==================================================================================================================== 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ServiceBraking_HMICtrl_CODE) SB_HSA_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SB_HSA_20ms_runnable
 *********************************************************************************************************************/
 //! ###### Definition of the SWC input and output data structures
   static ServiceBraking_HMICtrl_in_StructType RteInData_HSACommon;
   static SB_HMICtrl_HSAin_StructType          RteInData_HSA;
   static SB_HMICtrl_HSAout_StructType         RteOutData_HSA = { InactiveActive_NotAvailable,
                                                                  DeviceIndication_SpareValue };
   static ServiceBraking_HMICtrl_StructType    ServiceBraking_HMICtrl_Disable_data;
   static ServiceBraking_HMICtrl_StructType    ServiceBraking_HMICtrl_Enable_data;
   static ServiceBraking_State                 disableStates;
   static ServiceBraking_State                 enableStates;
          Std_ReturnType                       ASRretValue = RTE_E_INVALID;
          boolean                              HSAretValue = FALSE;
   static uint16  Timer[CONST_NbofTimers];

   //! ###### Read input RTE ports : 'Get_RteInDataRead_Common()'
   HSAretValue = Get_RteInDataRead_Common(&RteInData_HSACommon,
                                          &RteInData_HSA,
                                          &ASRretValue);
   //! ###### Check for SwcActivation_IgnitionOn is operational entry
   if (OperationalEntry == RteInData_HSACommon.SwcActivation_IgnitionOn)
   {
      ServiceBraking_HMICtrl_Disable_data.is_HSA_Operational = SM_HSAOff;
      ServiceBraking_HMICtrl_Enable_data.is_HSA_Operational  = SM_HSAOn;
   }
   //! ###### Check for SwcActivation_IgnitionOn is operational
   else if (Operational == RteInData_HSACommon.SwcActivation_IgnitionOn)
   {
      //! ##### Check for 'HSA Installed' parameter status
      if (TRUE == PCODE_HSA_Installed)
      {
         //! ##### Check for 'HSA Defaultconfig' parameter status
         if (FALSE == PCODE_HSA_DefaultConfig)
         {
            //! #### Process HSA disabled by defalut logic : 'SB_HSADisabledByDefault()'
            SB_HSADisabledByDefault(&RteInData_HSA,
                                    &ServiceBraking_HMICtrl_Disable_data,
                                    &ASRretValue,
                                    &disableStates,
                                    &Timer[CONST_SB_DisableTimer],
                                    &RteOutData_HSA);
            //! #### Process HSA disabled by default output processing logic : 'HSADisabledoutputprocessing()'
            HSADisabledoutputprocessing(&disableStates,
                                        &RteOutData_HSA);
         }
         else
         {
            //! #### Process HSA enabled by defalut logic : 'SB_HSAEnabledByDefault()'
            SB_HSAEnabledByDefault(&RteInData_HSA,
                                   &ServiceBraking_HMICtrl_Enable_data,
                                   HSAretValue,
                                   &ASRretValue,
                                   &enableStates,
                                   &Timer[CONST_SB_EnableTimer]);
            //! #### Process HSA enabled by default output processing logic : 'HSAEnableoutputprocessing()'
            HSAEnableoutputprocessing(&enableStates,
                                      &RteOutData_HSA);
         }
      }
      else
      {
         //! ##### Processing HSA function disable logic
         RteOutData_HSA.HillStartAid_DeviceIndication = DeviceIndication_Off;
         RteOutData_HSA.HSADriverRequest              = InactiveActive_NotAvailable;
      }
   }
   else
   {
      // If invalid vehicle mode is given then default values
      RteOutData_HSA.HSADriverRequest                          = InactiveActive_NotAvailable;
      RteOutData_HSA.HillStartAid_DeviceIndication             = DeviceIndication_Off;
      ServiceBraking_HMICtrl_Disable_data.is_active_ByDefault  = CONST_NonInitialize;
      ServiceBraking_HMICtrl_Enable_data.is_active_ByDefault   = CONST_NonInitialize;
      disableStates.newvalue                                   = SM_HSAOn;
      enableStates.newvalue                                    = SM_HSAOff;
   }
   //! ##### Timers decrement logic : 'TimerFunction_Tick()'
   TimerFunction_Tick(CONST_NbofTimers,
                      Timer);
   //! ###### Process the RTE write ports
   (void)Rte_Write_HSADriverRequest_HSADriverRequest(RteOutData_HSA.HSADriverRequest);
   Rte_Write_HillStartAid_DeviceIndication_DeviceIndication(RteOutData_HSA.HillStartAid_DeviceIndication);
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ServiceBraking_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ABSInhibitSwitchStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_ABSInhibitionStatus_ABSInhibitionStatus(Inhibit_T *data)
 *   Std_ReturnType Rte_Read_ASRHillHolderSwitch_ASRHillHolderSwitch(PassiveActive_T *data)
 *   Std_ReturnType Rte_Read_HillStartAidButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ABSInhibitionRequest_ABSInhibitionRequest(OffOn_T data)
 *   Std_ReturnType Rte_Write_HSADriverRequest_HSADriverRequest(InactiveActive_T data)
 *   Std_ReturnType Rte_Write_HillStartAid_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ServiceBraking_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the ServiceBraking_HMICtrl_20ms_runnable
//!
//!==================================================================================================================== 

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
FUNC(void, ServiceBraking_HMICtrl_CODE) ServiceBraking_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ServiceBraking_HMICtrl_20ms_runnable
 *********************************************************************************************************************/
 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ServiceBraking_HMICtrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ServiceBraking_HMICtrl_Init_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the ServiceBraking_HMICtrl_Init_runnable
//!
//!==================================================================================================================== 

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ServiceBraking_HMICtrl_CODE) ServiceBraking_HMICtrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ServiceBraking_HMICtrl_Init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

#define ServiceBraking_HMICtrl_STOP_SEC_CODE
#include "ServiceBraking_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

//!======================================================================================
//!
//! \brief
//! This function is processing the 'Get_RteInDataRead_Common'
//!
//! \param   *pServiceBraking_HMICtrl_HSAIn_data   Provides the switch activation ignition on status
//! \param   *pHSA_indata                          Providing the HSAButtonStatus and ASRHillholderSwitch value
//! \param   *pASRretValue                         Update ASRHillHolderSwitch RTE read error status
//!
//! \return   boolean                              Returns 'HSAButtonStatus' signal RTE read error status
//!
//!======================================================================================
static boolean Get_RteInDataRead_Common(ServiceBraking_HMICtrl_in_StructType  *pServiceBraking_HMICtrl_HSAIn_data,
                                        SB_HMICtrl_HSAin_StructType           *pHSA_indata,
                                        Std_ReturnType                        *pASRretValue)
{
   //! ###### Processing RTE failure events for error indication
   Std_ReturnType retValue = RTE_E_INVALID;

   //! ##### Read SwcActivation_IgnitionOn interface
   retValue = Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&pServiceBraking_HMICtrl_HSAIn_data->SwcActivation_IgnitionOn);
   MACRO_StdRteRead_IntRPort((retValue), (pServiceBraking_HMICtrl_HSAIn_data->SwcActivation_IgnitionOn), (NonOperational))
   //! ##### Read ASRHillHolderSwitch interface
   pHSA_indata->ASRHillHolderSwitch.previous = pHSA_indata->ASRHillHolderSwitch.current;
   *pASRretValue = Rte_Read_ASRHillHolderSwitch_ASRHillHolderSwitch(&pHSA_indata->ASRHillHolderSwitch.current);
   MACRO_StdRteRead_ExtRPort((*pASRretValue), (pHSA_indata->ASRHillHolderSwitch.current), (PassiveActive_NotAvailable),(PassiveActive_Error))
   //! ##### Read HillStartAidButtonStatus interface
   pHSA_indata->HillStartAidButtonStatus.previous = pHSA_indata->HillStartAidButtonStatus.current;
   retValue = Rte_Read_HillStartAidButtonStatus_PushButtonStatus(&pHSA_indata->HillStartAidButtonStatus.current);
   MACRO_StdRteRead_IntRPort((retValue), (pHSA_indata->HillStartAidButtonStatus.current), (PushButtonStatus_Error))
   return retValue;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'SB_HSADisabledByDefault'
//!
//! \param   *pHSADisable_indata                     Indicating current values for input data
//! \param   *pServiceBraking_HMICtrl_Disable_data   Updating the state for HSA_Operational and active_ByDefault
//! \param   *pASRreturnValue                        It indicates ASRHillHolderSwitch RTE error status
//! \param   *pDisableStates                         Providing the present HSA state
//! \param   *pTimers_SBDisable                      To check current timer value and update
//! \param   *pHSA_outputdata                        Update the HSADriverRequest and HillStartAid_DeviceIndication
//!
//!======================================================================================
static void SB_HSADisabledByDefault(      SB_HMICtrl_HSAin_StructType       *pHSADisable_indata,
                                          ServiceBraking_HMICtrl_StructType *pServiceBraking_HMICtrl_Disable_data,
                                    const Std_ReturnType                    *pASRreturnValue,
                                          ServiceBraking_State              *pDisableStates,
                                          uint16                            *pTimers_SBDisable,
                                          SB_HMICtrl_HSAout_StructType      *pHSA_outputdata)
{
   //! ###### Processing HSA disabled state transitions
   pDisableStates->currentvalue = pDisableStates->newvalue;
   if (CONST_NonInitialize == pServiceBraking_HMICtrl_Disable_data->is_active_ByDefault)
   {
      pHSADisable_indata->HillStartAidButtonStatus.previous     = pHSADisable_indata->HillStartAidButtonStatus.current;
      pServiceBraking_HMICtrl_Disable_data->is_active_ByDefault = CONST_Initialize;
      pServiceBraking_HMICtrl_Disable_data->is_HSA_Operational  = SM_HSAOff;
      pHSA_outputdata->HSADriverRequest                         = InactiveActive_Inactive;
      pHSA_outputdata->HillStartAid_DeviceIndication            = DeviceIndication_Off;
   }
   else
   {
      switch (pServiceBraking_HMICtrl_Disable_data->is_HSA_Operational)
      {
         //! ##### Check for HSAButton status
         //! #### State change from 'HSA On' to 'HSA Off' state
         case SM_HSAOn:
            if ((PassiveActive_Active == pHSADisable_indata->ASRHillHolderSwitch.current)
               && (*pTimers_SBDisable != CONST_TimerFunctionElapsed))
            {
               *pTimers_SBDisable = CONST_TimerFunctionInactive;
            }
            else
            {
               // Do nothing
            }
            if ((PushButtonStatus_Neutral == pHSADisable_indata->HillStartAidButtonStatus.previous)
               && (PushButtonStatus_Pushed == pHSADisable_indata->HillStartAidButtonStatus.current))
            {
               pServiceBraking_HMICtrl_Disable_data->is_HSA_Operational = SM_HSAOff;
               *pTimers_SBDisable                                       = CONST_TimerFunctionInactive;
            }
            else
            {
               //! #### Check ASRHillHolderSwitch status
               if ((PassiveActive_Error == pHSADisable_indata->ASRHillHolderSwitch.current)
                  || (RTE_E_INVALID == *pASRreturnValue)
                  || (RTE_E_MAX_AGE_EXCEEDED == *pASRreturnValue)
                  || (RTE_E_NEVER_RECEIVED == *pASRreturnValue))
               {
                  pServiceBraking_HMICtrl_Disable_data->is_HSA_Operational = SM_HSAOff;
                  *pTimers_SBDisable                                       = CONST_TimerFunctionInactive;
               }
               else if ((pHSADisable_indata->HillStartAidButtonStatus.previous != pHSADisable_indata->HillStartAidButtonStatus.current)
                       && (PushButtonStatus_Error == pHSADisable_indata->HillStartAidButtonStatus.current))
               {
                  pServiceBraking_HMICtrl_Disable_data->is_HSA_Operational = SM_HSAOff;
                  *pTimers_SBDisable                                       = CONST_TimerFunctionInactive;
               }
               else
               {
                  if (CONST_TimerFunctionElapsed == *pTimers_SBDisable)
                  {
                     pServiceBraking_HMICtrl_Disable_data->is_HSA_Operational = SM_HSAOff;
                     *pTimers_SBDisable                                       = CONST_TimerFunctionInactive;
                  }
                  else
                  {
                     // Do nothing : keep the previous status
                  }
               }
            }
         break;
		 //! #### State change from 'HSA Off' to 'HSA On' state
         default: //case SM_HSAOff:
			 if ((PushButtonStatus_Neutral == pHSADisable_indata->HillStartAidButtonStatus.previous)
				&& (PushButtonStatus_Pushed == pHSADisable_indata->HillStartAidButtonStatus.current))
			 {
				 pServiceBraking_HMICtrl_Disable_data->is_HSA_Operational = SM_HSAOn;
				 *pTimers_SBDisable                                       = CONST_SBR_InteractnHandlTmout;
			 }
			 else
			 {
				 // Do nothing : keep the previous status
			 }
         break;
      }
   }
   pDisableStates->newvalue = pServiceBraking_HMICtrl_Disable_data->is_HSA_Operational;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'HSADisabledoutputprocessing'
//!
//! \param   *pDisableState     Providing the current state value
//! \param   *pHSA_outputdata   Updating the output signals based on current state
//!
//!======================================================================================
static void HSADisabledoutputprocessing(const ServiceBraking_State          *pDisableState,
                                              SB_HMICtrl_HSAout_StructType  *pHSA_outputdata)
{
   //! ###### Processing state output actions based on current state
   if (pDisableState->currentvalue != pDisableState->newvalue)
   {
      switch (pDisableState->newvalue)
      { 
         //! #### Process output actions for HSA On state
         case SM_HSAOn:
            pHSA_outputdata->HSADriverRequest              = InactiveActive_Active;
            pHSA_outputdata->HillStartAid_DeviceIndication = DeviceIndication_On;
         break;
         //! #### Process output actions for HSA Off state 
         default: //case SM_HSAOff:
			 pHSA_outputdata->HSADriverRequest              = InactiveActive_Inactive;
			 pHSA_outputdata->HillStartAid_DeviceIndication = DeviceIndication_Off;
         break;
      }
   }
   else
   {
      // Do nothing : keep the previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'SB_HSAEnabledByDefault'
//!
//! \param   *pHSAEnable_indata                     Indicating current values for input data
//! \param   *pServiceBraking_HMICtrl_Enable_data   Updating the state for 'HSA_Operational' and 'active_ByDefault'
//! \param   HSArtrn                                It indicates True or False value
//! \param   *pASREnretValue                        It indicates ASRHillHolderSwitch RTE error status
//! \param   *pEnableStates                         Providing the present HSA state 
//! \param   *pTimers_SBEnable                      To check current timer value and update
//! 
//!======================================================================================
static void SB_HSAEnabledByDefault(      SB_HMICtrl_HSAin_StructType       *pHSAEnable_indata,
                                         ServiceBraking_HMICtrl_StructType *pServiceBraking_HMICtrl_Enable_data,
                                   const boolean                           HSArtrn,
                                   const Std_ReturnType                    *pASREnretValue,
                                         ServiceBraking_State              *pEnableStates,
                                         uint16                            *pTimers_SBEnable)
{ 
   //! ###### Processing HSA enabled state transitions
   pEnableStates->currentvalue = pEnableStates->newvalue;
   if (CONST_NonInitialize == pServiceBraking_HMICtrl_Enable_data->is_active_ByDefault)
   {
      pHSAEnable_indata->HillStartAidButtonStatus.previous     = pHSAEnable_indata->HillStartAidButtonStatus.current;
      pServiceBraking_HMICtrl_Enable_data->is_active_ByDefault = CONST_Initialize;
      pServiceBraking_HMICtrl_Enable_data->is_HSA_Operational  = SM_HSAOn;
      *pTimers_SBEnable                                        = CONST_SBR_InteractnHandlTmout;
   }
   else
   {
      switch (pServiceBraking_HMICtrl_Enable_data->is_HSA_Operational)
      {
         case SM_HSAOff:
            //! ##### Check HillStartAid button status
            //! #### Check for the state change from 'HSA OFF' to 'HSA OFF'
            if (((pHSAEnable_indata->HillStartAidButtonStatus.previous != pHSAEnable_indata->HillStartAidButtonStatus.current)
               && (PushButtonStatus_Error == pHSAEnable_indata->HillStartAidButtonStatus.current))
               || (RTE_E_OK != HSArtrn))
            {
               pServiceBraking_HMICtrl_Enable_data->is_HSA_Operational = SM_HSAOff;
            }
            //! #### Check for the state change from 'HSA OFF' to 'HSA ON' state
            else if ((PushButtonStatus_Neutral == pHSAEnable_indata->HillStartAidButtonStatus.previous)
                    && (PushButtonStatus_Pushed == pHSAEnable_indata->HillStartAidButtonStatus.current))
            {
               pServiceBraking_HMICtrl_Enable_data->is_HSA_Operational = SM_HSAOn;
               *pTimers_SBEnable                                       = CONST_SBR_InteractnHandlTmout;
            }
            else
            {
               // Do nothing: keep the previous status
            }
         break;
         //! #### Check for state change from 'HSA ON' to 'HSA OFF' state
         //! #### Check for ASRHillHolder switch status
		 default: //case SM_HSAOn:
            if ((PassiveActive_Active == pHSAEnable_indata->ASRHillHolderSwitch.current)
               && (*pTimers_SBEnable != CONST_TimerFunctionElapsed))
            {
               *pTimers_SBEnable = CONST_TimerFunctionInactive;
            }
            else
            {
               // Do nothing
            }
            //! #### Check for HillStartAid button status
            if ((PushButtonStatus_Neutral == pHSAEnable_indata->HillStartAidButtonStatus.previous)
               && (PushButtonStatus_Pushed == pHSAEnable_indata->HillStartAidButtonStatus.current))
            {
               pServiceBraking_HMICtrl_Enable_data->is_HSA_Operational = SM_HSAOff;
            }
            else
            {
               if ((PassiveActive_Error == pHSAEnable_indata->ASRHillHolderSwitch.current)
                  || (RTE_E_INVALID == *pASREnretValue)
                  || (RTE_E_MAX_AGE_EXCEEDED == *pASREnretValue)
                  || (RTE_E_NEVER_RECEIVED == *pASREnretValue))
               {
                  pServiceBraking_HMICtrl_Enable_data->is_HSA_Operational = SM_HSAOff;
               }
               //! #### Check for state change from 'HSA ON' to 'HSA ON' state
               // RTE check for HillStartAidButtonStatus interface i.e Event_ButtonError
               else if (((pHSAEnable_indata->HillStartAidButtonStatus.previous != pHSAEnable_indata->HillStartAidButtonStatus.current)
                       && (PushButtonStatus_Error == pHSAEnable_indata->HillStartAidButtonStatus.current))
                       || (TRUE == HSArtrn))
               {
                  pServiceBraking_HMICtrl_Enable_data->is_HSA_Operational = SM_HSAOn;
                  *pTimers_SBEnable                                       = CONST_SBR_InteractnHandlTmout;
               }
               // HSA ON to HSA OFF:condition for noAcknowledge i.e ASRHillHolderSwitch is not Active within one second
               else
               {
                  if (CONST_TimerFunctionElapsed == *pTimers_SBEnable)
                  {
                     pServiceBraking_HMICtrl_Enable_data->is_HSA_Operational = SM_HSAOff;
                     *pTimers_SBEnable                                       = CONST_TimerFunctionInactive;
                  }
                  else
                  {
                     // Do nothing : keep the previous status
                  }
               }
            }
         break;
      }
   }
   pEnableStates->newvalue = pServiceBraking_HMICtrl_Enable_data->is_HSA_Operational;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the HSAEnableoutputprocessing logic
//!
//! \param   *pEnableState   Providing the current state value
//! \param   *pHSA_outdata   Updating the output signals based on current state
//!
//!======================================================================================
static void HSAEnableoutputprocessing(const ServiceBraking_State         *pEnableState,
                                            SB_HMICtrl_HSAout_StructType *pHSA_outdata)
{
   //! ###### Processing state output actions based on current state
   if (pEnableState->currentvalue != pEnableState->newvalue)
   {
      switch (pEnableState->newvalue)
      {
         //! #### Process output actions for HSA Off state
         case SM_HSAOff:
            pHSA_outdata->HSADriverRequest              = InactiveActive_Inactive;
            pHSA_outdata->HillStartAid_DeviceIndication = DeviceIndication_On;
         break;
		 //! #### Process output actions for HSA On state
         default: //case SM_HSAOn: 
			 pHSA_outdata->HSADriverRequest              = InactiveActive_Active;
			 pHSA_outdata->HillStartAid_DeviceIndication = DeviceIndication_Off;
         break;
      }
   }
   else
   {
      // Do nothing : keep the previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the ABS_Inhibit logic
//!
//! \param   *pABS_indata    Indicates the ABS inhibit switchstatus signal
//! \param   *pABS_outdata   Update the output ABS inhibition request value
//!
//!======================================================================================
static void ABS_InhibitLogic(const SB_HMICtrl_ABSin_StructType  *pABS_indata,
                                   SB_HMICtrl_ABSout_StructType *pABS_outdata)
{
   //! ##### Check for ABS inhibit switch status
   //! #### Updating the ABS inhibition request port values
   // ABSInhibitSwitchStatus is read with value 'Pushed'
   if (PushButtonStatus_Pushed == pABS_indata->ABSInhibitSwitchStatus)
   {
      pABS_outdata->ABSInhibitionRequest = OffOn_On;
   }
   else
   {
      // Do nothing : keep the previous status
   }
   // ABSInhibitSwitchStatus is read with value neutral or error or not available
   if ((PushButtonStatus_Neutral == pABS_indata->ABSInhibitSwitchStatus)
      || (PushButtonStatus_Error == pABS_indata->ABSInhibitSwitchStatus)
      || (PushButtonStatus_NotAvailable == pABS_indata->ABSInhibitSwitchStatus))
   {
      pABS_outdata->ABSInhibitionRequest = OffOn_Off;
   }
   else
   {
      // Do nothing : keep the previous status
   }
}

//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
