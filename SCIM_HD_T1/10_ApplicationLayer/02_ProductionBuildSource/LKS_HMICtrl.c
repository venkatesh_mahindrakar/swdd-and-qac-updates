/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  LKS_HMICtrl.c
 *        Config:  C:/SCIM_integration/SCIM-BSP_B08_190722_fromVolvo/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  LKS_HMICtrl
 *  Generated at:  Tue Oct 15 21:51:05 2019
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <LKS_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file LKS_HMICtrl.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_VehicleControlAndActiveSafety 
//! @{
//! @addtogroup ActiveSafety
//! @{
//! @addtogroup LKS_HMICtrl
//! @{
//!
//! \brief
//! LKS_HMICtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the LKS_HMICtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_LKS_SwType_P1R5P_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_LKS_SwType_P1R5P_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_LKS_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
// Includes
#include "LKS_HMICtrl.h"
#include "FuncLibrary_Timer_If.h"
#include "FuncLibrary_ScimStd_If.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_LKS_SwType_P1R5P_T: Integer in interval [0...255]
 * SEWS_LKS_SwType_P1R5P_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DualDeviceIndication_T: Enumeration of integer in interval [0...15] with enumerators
 *   DualDeviceIndication_UpperOffLowerOff (0U)
 *   DualDeviceIndication_UpperOnLowerOff (1U)
 *   DualDeviceIndication_UpperBlinkLowerOff (2U)
 *   DualDeviceIndication_UpperDontCareLowerOff (3U)
 *   DualDeviceIndication_UpperOffLowerOn (4U)
 *   DualDeviceIndication_UpperOnLowerOn (5U)
 *   DualDeviceIndication_UpperBlinkLowerOn (6U)
 *   DualDeviceIndication_UpperDontCareLowerOn (7U)
 *   DualDeviceIndication_UpperOffLowerBlink (8U)
 *   DualDeviceIndication_UpperOnLowerBlink (9U)
 *   DualDeviceIndication_UpperBlinkLowerBlink (10U)
 *   DualDeviceIndication_UpperDontCareLowerBlink (11U)
 *   DualDeviceIndication_UpperOffLowerDontCare (12U)
 *   DualDeviceIndication_UpperOnLowerDontCare (13U)
 *   DualDeviceIndication_UpperBlinkLowerDontCare (14U)
 *   DualDeviceIndication_UpperDontCareLowerDontCare (15U)
 * LKSCorrectiveSteeringStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   LKSCorrectiveSteeringStatus_Off (0U)
 *   LKSCorrectiveSteeringStatus_Available (1U)
 *   LKSCorrectiveSteeringStatus_Steering (2U)
 *   LKSCorrectiveSteeringStatus_DriverTakeOverRqst (3U)
 *   LKSCorrectiveSteeringStatus_TempUnavailable (4U)
 *   LKSCorrectiveSteeringStatus_Spare2 (5U)
 *   LKSCorrectiveSteeringStatus_Error (6U)
 *   LKSCorrectiveSteeringStatus_NotAvailable (7U)
 * LKSStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   LKSStatus_OFF (0U)
 *   LKSStatus_INACTIVE (1U)
 *   LKSStatus_ACTIVE (2U)
 *   LKSStatus_Spare (3U)
 *   LKSStatus_Spare_01 (4U)
 *   LKSStatus_Spare_02 (5U)
 *   LKSStatus_Error (6U)
 *   LKSStatus_NotAvailable (7U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_LKS_SwType_P1R5P_T Rte_Prm_P1R5P_LKS_SwType_v(void)
 *   boolean Rte_Prm_P1BKI_LKS_Installed_v(void)
 *   boolean Rte_Prm_P1NQD_LKS_SwIndicationType_v(void)
 *
 *********************************************************************************************************************/


#define LKS_HMICtrl_START_SEC_CODE
#include "LKS_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_LKS_Installed            (Rte_Prm_P1BKI_LKS_Installed_v())
#define PCODE_LKS_SwType               (Rte_Prm_P1R5P_LKS_SwType_v())
#define PCODE_LKS_SwIndicationType     (Rte_Prm_P1NQD_LKS_SwIndicationType_v())
     

/**********************************************************************************************************************
 *
 * Runnable Entity Name: LKS_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_LKSApplicationStatus_LKSApplicationStatus(LKSStatus_T *data)
 *   Std_ReturnType Rte_Read_LKSCS_SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LKSCorrectiveSteeringStatus_LKSCorrectiveSteeringStatus(LKSCorrectiveSteeringStatus_T *data)
 *   Std_ReturnType Rte_Read_LKSPushButton_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LKS_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_LKSCSEnableSwitch_rqst_LKSEnableSwitch_rqst(OffOn_T data)
 *   Std_ReturnType Rte_Write_LKSCS_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LKSEnableSwitch_rqst_LKSEnableSwitch_rqst(OffOn_T data)
 *   Std_ReturnType Rte_Write_LKS_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: LKS_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the LKS_HMICtrl_20ms_runnable
//!
//!==================================================================================================================== 

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LKS_HMICtrl_CODE) LKS_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: LKS_HMICtrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   static LKS_HMICtrl_in_StructType  RteInData_Common;
   static LKS_HMICtrl_out_StructType RteOutData_Common = { OffOn_NotAvailable,
                                                           DualDeviceIndication_UpperOffLowerOff,
                                                           OffOn_NotAvailable,
                                                           DeviceIndication_SpareValue };
   static uint16                     Timer[CONST_TimersUsed];

   //! ###### Process the RTE read input data : 'Get_RteInData_Common()'
   Get_RteInData_Common(&RteInData_Common);

   //! ###### Check for 'SwcActivation_EngineRun' port is equal to operational 
   if (Operational == RteInData_Common.SwcActivation_EngineRun)
   {
      //! ##### Check for 'LKS_Installed' parameter status
      if (TRUE == PCODE_LKS_Installed)
      {
         //! ##### Check for 'LKS_SwitchType' parameter status
         if (0U == PCODE_LKS_SwType)
         {
            //! #### Process InputLogic WithoutSwitch : 'InputLogic_WithoutSwitch()'
            InputLogic_WithoutSwitch(&RteOutData_Common);
         }
         else if (1U == PCODE_LKS_SwType)
         {
            //! #### Process PushButton InputLogic : 'PushButtonInputLogic()'
            PushButtonInputLogic(&RteInData_Common.LKSPushButton,
                                 &RteOutData_Common.LKSEnableSwitch_rqst,
                                 &Timer[CONST_PushButtonTimer]);
            //! #### Process LED indication feedback logic : 'FeedbackLEDIndicationLogic()'
            FeedbackLEDIndicationLogic(&RteInData_Common.LKSApplicationStatus,
                                       &RteOutData_Common.LKS_DeviceIndication);
         }
         else if (2U == PCODE_LKS_SwType)
         {
            //! #### Process LKS on_off RockerSwitch logic : 'OnOffRockerSwitch()'
            // Input logic for mono-stable rocker switch PC24/D3: LSS-DW
            OnOffRockerSwitch(&RteInData_Common.LKS_SwitchStatus,
                              &RteOutData_Common.LKSEnableSwitch_rqst,
                              &Timer[CONST_OnOffRockerSwitchTimer]);
            //! #### Process LED indication feedback logic : 'FeedbackLEDIndicationLogic()'
            FeedbackLEDIndicationLogic(&RteInData_Common.LKSApplicationStatus,
                                       &RteOutData_Common.LKS_DeviceIndication);
         }          
         else if (3U == PCODE_LKS_SwType)
         {
            //! #### Process LKSCS switch logic : 'LKSCSSwitchLogic()'
            // Input logic for LKSCS switch PC24: LSS-DWC
            LKSCSSwitchLogic(&RteInData_Common.LKSCS_SwitchStatus,
                             &RteOutData_Common.LKSEnableSwitch_rqst,
                             &RteOutData_Common.LKSCSEnableSwitch_rqst,
                             &Timer[CONST_LKSSwitchTimer],
                             &Timer[CONST_LKSCSSwitchTimer]);
            //! #### Process LKSCS indication logic : 'LKSCSIndication()'
            LKSCSIndication(&RteInData_Common.LKSApplicationStatus,
                            &RteInData_Common.LKSCorrectiveSteeringStatus,
                            &RteOutData_Common.LKSCS_DeviceIndication);
         }
         else
         {
            // do nothing: Keep the previous LKS_Switch Type status
         }
      }
      //! ##### Processing LKS disable logic, based on 'LKS_Installed' parameter
      else
      {
         RteOutData_Common.LKS_DeviceIndication   = DeviceIndication_Off;
         RteOutData_Common.LKSCS_DeviceIndication = DualDeviceIndication_UpperOffLowerOff;
         RteOutData_Common.LKSEnableSwitch_rqst   = OffOn_NotAvailable;
         RteOutData_Common.LKSCSEnableSwitch_rqst = OffOn_NotAvailable;
      }
   }
   //! ###### Processing LKS disable logic, based on 'SwcActivation_EngineRun' port
   else
   {
      RteOutData_Common.LKS_DeviceIndication   = DeviceIndication_Off;
      RteOutData_Common.LKSCS_DeviceIndication = DualDeviceIndication_UpperOffLowerOff;
      RteOutData_Common.LKSEnableSwitch_rqst   = OffOn_NotAvailable;
      RteOutData_Common.LKSCSEnableSwitch_rqst = OffOn_NotAvailable;
   } 
   // Decrement Timers 
   TimerFunction_Tick(CONST_TimersUsed,
                      Timer);
   //! ###### Process RTE write input logic : 'RteOutDataWrite_Common()'
   RteOutDataWrite_Common(&RteOutData_Common);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

#define LKS_HMICtrl_STOP_SEC_CODE
#include "LKS_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief
//! This function is processing the Get_RteInData_Common Logic
//!
//! \param   *pLKS_HMICtrl_input_data   Examine and update the input signals based on RTE failure events
//! 
//!======================================================================================
static void Get_RteInData_Common(LKS_HMICtrl_in_StructType  *pLKS_HMICtrl_input_data)
{
   Std_ReturnType retValue = RTE_E_INVALID;

   //! ###### Read the RTE input ports and process RTE failure events for error indicating mode
   //! ##### Read LKSApplicationStatus interface
   retValue = Rte_Read_LKSApplicationStatus_LKSApplicationStatus(&pLKS_HMICtrl_input_data->LKSApplicationStatus);
   MACRO_StdRteRead_ExtRPort((retValue),(pLKS_HMICtrl_input_data->LKSApplicationStatus),(LKSStatus_NotAvailable),(LKSStatus_Error))
   //! ##### Read LKSCS_SwitchStatus interface
   retValue = Rte_Read_LKSCS_SwitchStatus_A3PosSwitchStatus(&pLKS_HMICtrl_input_data->LKSCS_SwitchStatus);
   MACRO_StdRteRead_IntRPort((retValue),(pLKS_HMICtrl_input_data->LKSCS_SwitchStatus),(A3PosSwitchStatus_Error))
   //! ##### Read LKSCorrectiveSteeringStatus interface
   retValue = Rte_Read_LKSCorrectiveSteeringStatus_LKSCorrectiveSteeringStatus(&pLKS_HMICtrl_input_data->LKSCorrectiveSteeringStatus);
   MACRO_StdRteRead_ExtRPort((retValue),(pLKS_HMICtrl_input_data->LKSCorrectiveSteeringStatus),(LKSCorrectiveSteeringStatus_NotAvailable),(LKSCorrectiveSteeringStatus_Error))
   //! ##### Read LKSPushButton interface
   retValue = Rte_Read_LKSPushButton_PushButtonStatus(&pLKS_HMICtrl_input_data->LKSPushButton);
   MACRO_StdRteRead_IntRPort((retValue),(pLKS_HMICtrl_input_data->LKSPushButton),(PushButtonStatus_Error))
   //! ##### Read LKS_SwitchStatus interface
   retValue = Rte_Read_LKS_SwitchStatus_A2PosSwitchStatus(&pLKS_HMICtrl_input_data->LKS_SwitchStatus);
   MACRO_StdRteRead_IntRPort((retValue),(pLKS_HMICtrl_input_data->LKS_SwitchStatus),(A2PosSwitchStatus_Error))
   //! ##### Read SwcActivation_EngineRun interface
   retValue = Rte_Read_SwcActivation_EngineRun_EngineRun(&pLKS_HMICtrl_input_data->SwcActivation_EngineRun);
   MACRO_StdRteRead_IntRPort((retValue),(pLKS_HMICtrl_input_data->SwcActivation_EngineRun),(NonOperational))
}
//!======================================================================================
//!
//! \brief
//! This function is processing the RteOutDataWrite_Common logic
//!
//! \param   *pRteOutData_common   Update the output signals to ports of output structure
//!
//!======================================================================================
static void RteOutDataWrite_Common(const LKS_HMICtrl_out_StructType *pRteOutData_common)
{
   //! ###### Processing RteInData write common logic
   Rte_Write_LKSCSEnableSwitch_rqst_LKSEnableSwitch_rqst(pRteOutData_common->LKSCSEnableSwitch_rqst);
   Rte_Write_LKSCS_DeviceIndication_DualDeviceIndication(pRteOutData_common->LKSCS_DeviceIndication);
   Rte_Write_LKSEnableSwitch_rqst_LKSEnableSwitch_rqst(pRteOutData_common->LKSEnableSwitch_rqst);
   Rte_Write_LKS_DeviceIndication_DeviceIndication(pRteOutData_common->LKS_DeviceIndication);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the PushButtonInputLogic
//!
//! \param   *pLKSPushButton          Providing LKSPushButton current status
//! \param   *pLKSEnable_Switchrqst   Update LKSEnableSwitchRequest value based on LKSPushButton status
//! \param   *pTimer_PushButton       To check current timer value and update
//! 
//!======================================================================================
static void PushButtonInputLogic(const PushButtonStatus_T *pLKSPushButton,
                                       OffOn_T            *pLKSEnable_Switchrqst,
                                       uint16             *pTimer_PushButton)
{
   //! ##### Check for PushButton timer status
   if (CONST_TimerFunctionElapsed == *pTimer_PushButton)
   {
      *pTimer_PushButton = CONST_TimerFunctionInactive;
   }
   else
   {
      // do nothing: keep previous status
   }
   if (CONST_TimerFunctionInactive == *pTimer_PushButton)
   {
      //! #### Check for LKSPushButton status
      if (PushButtonStatus_Pushed == *pLKSPushButton)
      {
         *pLKSEnable_Switchrqst = OffOn_On;
         *pTimer_PushButton     = CONST_PushButtonTimeout;
      }
      else if (PushButtonStatus_Neutral == *pLKSPushButton)
      {
         *pLKSEnable_Switchrqst = OffOn_Off;
      }
      else if (PushButtonStatus_Error == *pLKSPushButton)
      {
         *pLKSEnable_Switchrqst = OffOn_Error;
      }
      else if (PushButtonStatus_NotAvailable == *pLKSPushButton)
      {
         *pLKSEnable_Switchrqst = OffOn_NotAvailable;
      }
      else 
      {
         // Do nothing: keep previous status
      }
   }
   else
   {
      // Do nothing: keep previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the OnOffRockerSwitch Logic
//!
//! \param    *pLKS_SwitchStatus         Provides LKSSwitchStatus value
//! \param    *pLKSEnableSwrequest       Update LKSEnableSwitch value based on LKSPushButton status
//! \param    *pTimer_OnOffRockerSwitch  To check current timer value and update
//! 
//!======================================================================================
static void OnOffRockerSwitch(const A2PosSwitchStatus_T *pLKS_SwitchStatus,
                                    OffOn_T             *pLKSEnableSwrequest,
                                    uint16              *pTimer_OnOffRockerSwitch)
{
   //! ##### Check for OnOffRockerSwitch timer status
   if (CONST_TimerFunctionElapsed == *pTimer_OnOffRockerSwitch)
   {
      *pTimer_OnOffRockerSwitch = CONST_TimerFunctionInactive;
   }
   else
   {
      // Do nothing: keep previous status
   }
   if (CONST_TimerFunctionInactive == *pTimer_OnOffRockerSwitch)
   {
      //! #### Check for LKS_Switch status
      if (A2PosSwitchStatus_On == *pLKS_SwitchStatus)
      {
         *pLKSEnableSwrequest      = OffOn_On;
         *pTimer_OnOffRockerSwitch = CONST_OnOffRockerSwTimeout;
      }
      else if (A2PosSwitchStatus_Off == *pLKS_SwitchStatus)
      {
         *pLKSEnableSwrequest = OffOn_Off;
      }
      else if (A2PosSwitchStatus_Error == *pLKS_SwitchStatus)
      {
         *pLKSEnableSwrequest = OffOn_Error;
      }
      else if (A2PosSwitchStatus_NotAvailable == *pLKS_SwitchStatus)
      {
         *pLKSEnableSwrequest = OffOn_NotAvailable;
      }
      else
      {
         // Do nothing: keep previous status
      }
   }
   else
   {
      // Do nothing: keep previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the LKSCSSwitch Logic
//!
//! \param   *pLKSCSSwitchStatus       Provides current input switch status 
//! \param   *pLKSEnableSwrqst         Update LKSEnableSwitchRequest port value based on LKSCSSwitchStatus
//! \param   *pLKSCSEnableSwitchrqst   Update LKSCSEnableSwitchrequest port value based on LKSCSSwitchStatus
//! \param   *pTimer_LKSSwitch         To check current timer value and update
//! \param   *pTimer_LKSCSSwitch       To check current timer value and update
//!
//!======================================================================================
static void LKSCSSwitchLogic(const A3PosSwitchStatus_T *pLKSCSSwitchStatus,
                                   OffOn_T             *pLKSEnableSwrqst,
                                   OffOn_T             *pLKSCSEnableSwitchrqst,
                                   uint16              *pTimer_LKSSwitch,
                                   uint16              *pTimer_LKSCSSwitch)
{
   //! ##### Check for LKSSwitch timer status
   if (CONST_TimerFunctionElapsed == *pTimer_LKSSwitch)
   {
      *pTimer_LKSSwitch = CONST_TimerFunctionInactive;
   }
   else
   {
      // Do nothing: keep previous status
   }
   if (CONST_TimerFunctionInactive == *pTimer_LKSSwitch)
   {
      //! #### Check for LKSCSSwitch status
      if (A3PosSwitchStatus_Lower == *pLKSCSSwitchStatus)
      {
         *pLKSEnableSwrqst = OffOn_On;
         *pTimer_LKSSwitch = CONST_LKSCSSwitchTimeout;
      }
      else if (A3PosSwitchStatus_Upper == *pLKSCSSwitchStatus)
      {
         *pLKSEnableSwrqst = OffOn_Off;
      }
      else if (A3PosSwitchStatus_Middle == *pLKSCSSwitchStatus)
      {
         *pLKSEnableSwrqst = OffOn_Off;
      }
      else if (A3PosSwitchStatus_Error == *pLKSCSSwitchStatus)
      {
         *pLKSEnableSwrqst = OffOn_Error;
      }
      else if (A3PosSwitchStatus_NotAvailable == *pLKSCSSwitchStatus)
      {
         *pLKSEnableSwrqst = OffOn_NotAvailable;
      }
      else
      {
         // Do nothing: keep previous status
      }
   }
   else
   {
      // Do nothing: keep previous statusv
   }
   //! ##### Check for LKSCSSwitch timer status
   if (CONST_TimerFunctionElapsed == *pTimer_LKSCSSwitch)
   {
      *pTimer_LKSCSSwitch = CONST_TimerFunctionInactive;
   }
   else
   {
      // Do nothing: keep previous status
   }
   if (CONST_TimerFunctionInactive == *pTimer_LKSCSSwitch)
   {
      //! #### Check for LKSCSSwitch status
      if (A3PosSwitchStatus_Lower == *pLKSCSSwitchStatus)
      {
         *pLKSCSEnableSwitchrqst = OffOn_Off;
      }
      else if (A3PosSwitchStatus_Upper == *pLKSCSSwitchStatus)
      {
         *pLKSCSEnableSwitchrqst = OffOn_On;
         *pTimer_LKSCSSwitch     = CONST_LKSCSSwitchTimeout;
      }
      else if (A3PosSwitchStatus_Middle == *pLKSCSSwitchStatus)
      {
         *pLKSCSEnableSwitchrqst = OffOn_Off;
      }
      else if (A3PosSwitchStatus_Error == *pLKSCSSwitchStatus)
      {
         *pLKSCSEnableSwitchrqst = OffOn_Error;
      }
      else if (A3PosSwitchStatus_NotAvailable == *pLKSCSSwitchStatus)
      {
         *pLKSCSEnableSwitchrqst = OffOn_NotAvailable;
      }
      else
      {
         // Do nothing: keep previous status
      }
   }
   else
   {
      // Do nothing: keep previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the LKSCSIndication Logic
//!
//! \param   *pLKSApplicationSts             Indicates LKSApplicationStatus is Active or Inactive
//! \param   *pLKSCorrectiveSteeringStatus   Provides current LKSCorrectiveSteering status
//! \param   *pLKSCS_DvcIndication           Update device indication status based on LKSApplicationStatus and LKSCorrectiveSteeringstatus
//!
//!======================================================================================
static void LKSCSIndication(const LKSStatus_T                    *pLKSApplicationSts,
                            const LKSCorrectiveSteeringStatus_T  *pLKSCorrectiveSteeringStatus,
                                  DualDeviceIndication_T         *pLKSCS_DvcIndication)
{
   static DeviceIndication_T lkscs_DVCIND_Lower = CONST_Off;
   static DeviceIndication_T lkscs_DVCIND_Upper = CONST_Off;

   //! ##### Update LKSCSDeviceIndication_Lower based on 'LKSApplicationStatus'
   if ((LKSStatus_ACTIVE == *pLKSApplicationSts)
      || (LKSStatus_INACTIVE == *pLKSApplicationSts))
   {
      lkscs_DVCIND_Lower = CONST_On;
   }
   else
   {
      lkscs_DVCIND_Lower = CONST_Off;
   }
   //! ##### Update LKSCSDeviceIndication_Upper based on 'LKSCorrectiveSteeringStatus'
   if ((LKSCorrectiveSteeringStatus_Off == *pLKSCorrectiveSteeringStatus)
      || (LKSCorrectiveSteeringStatus_Error == *pLKSCorrectiveSteeringStatus)
      || (LKSCorrectiveSteeringStatus_NotAvailable == *pLKSCorrectiveSteeringStatus))
   {
      lkscs_DVCIND_Upper = CONST_Off;
   }
   else
   {
      lkscs_DVCIND_Upper = CONST_On;
   }
   //! ##### Updating device indication based on LKSCSDeviceIndication_Lower and LKSCSDeviceIndication_Upper
   if ((CONST_On == lkscs_DVCIND_Lower)
      && (CONST_Off == lkscs_DVCIND_Upper))
   {
      *pLKSCS_DvcIndication = DualDeviceIndication_UpperOffLowerOn;
   }
   else if ((CONST_On == lkscs_DVCIND_Lower)
           && (CONST_On == lkscs_DVCIND_Upper))
   {
      *pLKSCS_DvcIndication = DualDeviceIndication_UpperOnLowerOn;
   }
   else if ((CONST_Off == lkscs_DVCIND_Lower)
           && (CONST_On == lkscs_DVCIND_Upper))
   {
      *pLKSCS_DvcIndication = DualDeviceIndication_UpperOnLowerOff;
   }
   else
   {
      *pLKSCS_DvcIndication = DualDeviceIndication_UpperOffLowerOff;
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the FeedbackLEDIndication Logic
//!
//! \param   *pLKSApplicationStatus   Indicates LKSApplication status is Active or Inactive
//! \param   *pLKS_DeviceIndication   Update device indication status based on LKSApplication status
//!
//!======================================================================================
static void FeedbackLEDIndicationLogic(const LKSStatus_T         *pLKSApplicationStatus,
                                             DeviceIndication_T  *pLKS_DeviceIndication)
{
   //! ###### Check for 'LKS_SwIndicationType' parameter status
   if (TRUE == PCODE_LKS_SwIndicationType)
   {
      //! ##### Processing positive indication logic 
      //! #### Check for 'LKSApplicationStatus' is Active or Inactive
      if ((LKSStatus_ACTIVE == *pLKSApplicationStatus)
         || (LKSStatus_INACTIVE == *pLKSApplicationStatus))
      {
         *pLKS_DeviceIndication = DeviceIndication_On;
      }
      else
      {
         *pLKS_DeviceIndication = DeviceIndication_Off;
      }
   }
   else
   {
      //! ##### Processing negative indication logic
      //! #### Check for 'LKSApplicationStatus' is Active or Inactive
      // FeedbackLEDNegativeIndicationLogic : cycle 20ms 
      if ((LKSStatus_ACTIVE == *pLKSApplicationStatus)
         || (LKSStatus_INACTIVE == *pLKSApplicationStatus))
      {
         *pLKS_DeviceIndication = DeviceIndication_Off;
      }
      else
      {
         *pLKS_DeviceIndication = DeviceIndication_On;
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the InputLogic_WithoutSwitch
//!
//! \param   *pLKS_HMICtrl_out_data   Updating the output data to ports of output structure
//!
//!======================================================================================
static void InputLogic_WithoutSwitch(LKS_HMICtrl_out_StructType *pLKS_HMICtrl_out_data)
{
   //! ##### update all the output ports as per logic
   pLKS_HMICtrl_out_data->LKS_DeviceIndication   = DeviceIndication_Blink;
   pLKS_HMICtrl_out_data->LKSCS_DeviceIndication = DualDeviceIndication_UpperBlinkLowerBlink;
   pLKS_HMICtrl_out_data->LKSEnableSwitch_rqst   = OffOn_On;
   pLKS_HMICtrl_out_data->LKSCSEnableSwitch_rqst = OffOn_Off;
}

//! @}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
