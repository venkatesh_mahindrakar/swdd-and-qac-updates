#ifdef WIREDCONTROLBOX_HMICtrl_H
#error WIREDCONTROLBOX_HMICtrl_H unexpected multi-inclusion
#else
#define WIREDCONTROLBOX_HMICtrl_H

//=====================================================================================
// Included header files
//=====================================================================================

#include "WiredControlBox_HMICtrl_If.h"

//=====================================================================================
// Private macros
//=====================================================================================

//=====================================================================================
// Private constants and enum definitions
//=====================================================================================
#define CONST_RecallTimer                       (7U)
#define CONST_OutputStoreTimer                  (8U)
#define CONST_StopLevelTimer                    (9U)
#define CONST_LevelAdjTimer                     (10U)
#define RCECS_HoldCircuitId                     (11U)
#define CONST_StopLevelTimeout                  ((500U)/CONST_TimeBase)

#ifndef SHORTPULSEMAXLENGTH
#define SHORTPULSEMAXLENGTH                     (3U)
#endif 

//=====================================================================================
// Private type definitions
//=====================================================================================
typedef enum
{
   SM_WCB_ADLLFSM_Off       = 0,
   SM_WCB_ADLLFSM_On        = 1,
   SM_WCB_ADLLFSM_FALLBACK  = 2
} Wcb_AdjustDriveOrLoadingLevel_FSM_Type; // e_AdjustDriveOrLoadingLevel_FSM; 

typedef struct
{
   Wcb_AdjustDriveOrLoadingLevel_FSM_Type newValue;
   Wcb_AdjustDriveOrLoadingLevel_FSM_Type currentvalue;
} Wcb_AdjustDriveOrLoadingLevel_FSM;

/*temporary*/
typedef uint8 ECSStandByReq_T;
typedef uint8 VehicleModeEvt_T;

//=====================================================================================
// Private data declarations
//=====================================================================================

//=====================================================================================
// Private function prototypes
//=====================================================================================
static void Get_RteInDataRead_Common(WiredControlBox_HMICtrl_in_StructType       *pWiredControlBox_HMICtrl_input_data,
                                     WiredControlBox_HMICtrl_in_ButtonStructType *pWCB_HMIControl_in_Buttondata);
static void RteInDatawrite_Common(const WiredControlBox_HMICtrl_out_StructType *pWiredControlBox_output_data);
static void Simultaneous_button_pushes(const PushButtonStatus_T  *pbutton_StopButtonStatuscurrent,
                                             PushButtonStatus_T  *pbutton_AdjustButtonStatuscurrent,
                                             PushButtonStatus_T  *pbutton_BackButtonStatuscurrent,
                                             PushButtonStatus_T  *pbutton_MemButtonStatuscurrent,
                                             PushButtonStatus_T  *pbutton_SelectButtonStatuscurrent,
                                             EvalButtonRequest_T *pbutton_WRDownButtonStatuscurrent,
                                             EvalButtonRequest_T *pbutton_WRUpButtonStatuscurrent);
static void WiredControlBox_common_logic(      WiredControlBox_HMICtrl_in_StructType        *pRteInData_Common,
                                               WiredControlBox_HMICtrl_in_ButtonStructType  *pWCB_HMICtrl_in_Buttonlogic,
                                               WiredControlBox_HMICtrl_out_StructType       *pWiredControlBox_HMICtrl_output,
                                               Wcb_LevelAdjustment_FSM                      *pSM_commonStates,
                                               Wcb_AdjustDriveOrLoadingLevel_FSM            *pSMCommonStates_AdjustDrive,
                                               StateInitializationType                      *pstateInitialization,
                                         const SEWS_ECS_MemSwTimings_P1BWF_s_T              *pECS_MemSwTimings,
                                               uint16                                       *pTimers_stop,
                                               uint16                                       *pTimers_Adj);
static void WiredControlBox_HMICtrl_default_Startup_values(const PushButtonStatus_current               *pStopButtonStatuscurrent,
                                                                 WiredControlBox_HMICtrl_out_StructType *pWiredCntrlBox_HMICtrl_output_data);
static void WiredControlBox_HMICtrl_behavior_HeightAdjustmentAllowed(const PushButtonStatus_T                     *pbutton_StopButtonStatuscurrent,
                                                                     const FalseTrue                              *pHeightAdjustmentAllowed,
                                                                           StateInitializationType                *pInitializationState,
                                                                           WiredControlBox_HMICtrl_out_StructType *pWiredCntrlBox_output_data);
static void AdjustDriveOrLoadingLevel_FSM(const PushButtonStatus                  *pAdjustBtnStatus,
                                          const PushButtonStatus                  *pBackBtnStatus,
                                          const RampLevelRequest                  *pRampLevelRequest,
                                          const PushButtonStatus                  *pStopButtonStatus,
                                                Wcb_AdjustDriveOrLoadingLevel_FSM *pSMStates_AdjustDrive,
                                                StateInitializationType           *pstateInitialize_Adjust,
                                                DeviceIndication_T                *pAdjust_DeviceIndication,
                                                LevelAdjustmentStroke_T           *pWiredLevelAdjustmentStroke,
                                                LevelUserMemoryAction_T           *pFSM_WiredLevelUserMemAction);
static void Request_stop_level_change(const PushButtonStatus        *pStopBtnStatus,
                                            FalseTrue_T             *pWiredAirSuspensionStopRequest,
                                            LevelUserMemoryAction_T *pRequest_WiredLevelUserMemActn,
                                            uint16                  *pRequest_Timers);
static void LevelAdjustment_State_Machine(const WiredControlBox_HMICtrl_in_StructType        *pWiredCntrlBox_HMICtrl_in_data,
                                          const WiredControlBox_HMICtrl_in_ButtonStructType  *pWCB_HMICtrl_in_SMButtondata,
                                                Wcb_LevelAdjustment_FSM                      *pSMLevel_States,
                                          const Wcb_AdjustDriveOrLoadingLevel_FSM            *pSMLevelStates_AdjustDrive,
                                                StateInitializationType                      *pstateInitialize,
                                          const SEWS_ECS_MemSwTimings_P1BWF_s_T              *pECS_MemSwTimings,
                                                uint16                                       *pLevel_Timers,
                                                WiredControlBox_HMICtrl_out_StructType       *pWiredCntrlBox_out_data);
static void LevelAdjustment_FSM_Output(const Wcb_LevelAdjustment_FSM                *pLevelAdjustment_FSM,
                                             WiredControlBox_HMICtrl_out_StructType *pRteOutData_Common);
static boolean CheckForButtonPushed(const PushButtonStatus_current *pcurrentinput,
                                          uint8                    *pbuttonPushedCount);
static void AdjustDriveOrLoadingLevel_OutputProcessing(DeviceIndication_T                 *pAdjust_DeviceIndication,
                                                       LevelAdjustmentStroke_T            *pWiredLevelAdjustmentStroke,
                                                       LevelUserMemoryAction_T            *pFSM_WiredLevelUserMemAction,
													   Wcb_AdjustDriveOrLoadingLevel_FSM  *pSMStates_AdjustDrive);


//=====================================================================================
// End of file
//=====================================================================================
#endif


