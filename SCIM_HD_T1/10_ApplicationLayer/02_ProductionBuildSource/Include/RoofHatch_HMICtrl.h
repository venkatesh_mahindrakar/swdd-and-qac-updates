#ifdef ROOFHATCH_HMICTRL_H
#error ROOFHATCH_HMICTRL_H unexpected multi-inclusion
#else
#define ROOFHATCH_HMICTRL_H

//=====================================================================================
// Included header files
//=====================================================================================

//=======================================================================================
// Private constants and enum definitions
//=======================================================================================

//=======================================================================================
// Private macros
//=======================================================================================

//=======================================================================================
// Private type definitions
//=======================================================================================

// Structure for inputs 
typedef struct 
{
   PushButtonStatus_T        BunkH1RoofhatchCloseBtn_Stat;
   PushButtonStatus_T        BunkH1RoofhatchOpenBtn_Stat;
   PushButtonStatus_T        BunkH2RoofhatchCloseBtn_Stat;
   PushButtonStatus_T        BunkH2RoofhatchOpenBtn_Stat;
   A3PosSwitchStatus_T       RoofHatch_SwitchStatus;
   VehicleModeDistribution_T SwcActivation_Living;
} RoofHatch_HMICtrl_InStructType;

// Structure for outputs 
typedef struct 
{
   RoofHatch_HMI_rqst_T   RoofHatch_HMI_rqst;
} RoofHatch_HMICtrl_OutStructType;

//=======================================================================================
// Private data declarations
//=======================================================================================
// Note: MUST BE static
// Note: Shall not be used/avoided with AUTOSAR SWC.


//=======================================================================================
// Private function prototypes
//=======================================================================================
// Note : MUST BE static

// Static local functions
static void Get_RteInDataRead_Common(RoofHatch_HMICtrl_InStructType *pRteInData_Common);
static void RteOutDataWrite_Common(const RoofHatch_HMICtrl_OutStructType* pRteOutData_Common);
static void InputLogicLECMLIN(const RoofHatch_HMICtrl_InStructType  *pIn_Data_LIN,
                                    RoofHatch_HMICtrl_OutStructType *pOut_Data_LIN);
static void InputLogicLECMCAN(const RoofHatch_HMICtrl_InStructType  *pIn_Data_CAN,
                                    RoofHatch_HMICtrl_OutStructType *pOut_Data_CAN);
static void InputLogicRockerSwitch(const RoofHatch_HMICtrl_InStructType  *pRockerSwitch_in_Data,
                                         RoofHatch_HMICtrl_OutStructType *pRteOutData_Common);
static void RoofHatch_HMI_FallbackLogic(const RoofHatch_HMICtrl_InStructType  *pFallback_in_Data,
                                              RoofHatch_HMICtrl_OutStructType *pOut_Data);
static boolean isNormalStatusBunkH1RoofhatchCloseBtn(const PushButtonStatus_T *pBunkH1RoofhatchCloseBtn_Stat);
static boolean isNormalStatusBunkH1RoofhatchOpenBtn(const PushButtonStatus_T *pBunkH1RoofhatchOpenBtn_Stat);
static boolean isNormalStatusBunkH2RoofhatchCloseBtn(const PushButtonStatus_T *pBunkH2RoofhatchCloseBtn_Stat);
static boolean isNormalStatusBunkH2RoofhatchOpenBtn(const PushButtonStatus_T *pBunkH2RoofhatchOpenBtn_Stat);
static boolean isNormalStatusRoofHatchSwitchStatus(const A3PosSwitchStatus_T *pRoofHatch_SwitchStatus);


//=======================================================================================
// End of file
//=======================================================================================
#endif
