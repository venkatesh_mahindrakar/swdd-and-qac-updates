#ifdef AXLELOADDISTRIBUTION_HMICTRL_SWITCH_IF_H
#error AXLELOADDISTRIBUTION_HMICTRL_SWITCH_IF_H unexpected multi-inclusion
#else
#define AXLELOADDISTRIBUTION_HMICTRL_SWITCH_IF_H
//=====================================================================================
// Included header files
//=====================================================================================

//=======================================================================================
// Public type definitions
//=======================================================================================


//=======================================================================================
// Public constants and enums definitions
//=======================================================================================
typedef enum
{
   SM_ALD_Switch_Default    = 0,
   SM_ALD_Switch_LastFunc   = 1,
   SM_ALD_Switch_StandBy    = 2,
   SM_ALD_Switch_ALD_On     = 3,
   SM_ALD_Switch_ALD_Ack    = 4,
   SM_ALD_Switch_ALD_Off    = 5,
   SM_ALD_Switch_ALD_Denied = 6,
   SM_ALD_Switch_Fallback   = 7
} ALDSwitch_Enum;

typedef struct
{
   ALDSwitch_Enum newValue;
   ALDSwitch_Enum currentValue;
} ALDSwitch_States;

typedef enum
{
   SM_ALD_RatioALD_Default         = 0,
   SM_ALD_RatioALD_LastFunc        = 1,
   SM_ALD_RatioALD_StandBy         = 2,
   SM_ALD_RatioALD_ALD_On          = 3,
   SM_ALD_RatioALD_ALD_Ack         = 4,
   SM_ALD_RatioALD_ALD_Off         = 5,
   SM_ALD_RatioALD_ALD_Denied      = 6,
   SM_ALD_RatioALD_Change_LoadReq  = 7,
   SM_ALD_RatioALD_Change_Accepted = 8,
   SM_ALD_RatioALD_Fallback        = 9
} RatioALD_Enum;

typedef struct
{
   RatioALD_Enum newValue;
   RatioALD_Enum currentValue;
} RatioALDSwitch_States;

typedef enum
{
   SM_ALD_ARide_Default           = 0,
   SM_ALD_ARide_Short_LiftRequest = 1,
   SM_ALD_ARide_Long_LiftRequest  = 2,
   SM_ALD_ARide_DownRequest       = 3,
   SM_ALD_ARide_Fallback          = 4
} ARideState_Enum;
typedef struct
{
   ARideState_Enum newValue;
   ARideState_Enum currentValue;
} AldARide_States;

typedef enum
{
   SM_DRS_NoActionRequested       = 1,
   SM_DRS_DefaultLoadDistribution = 2,
   SM_DRS_MaxTraction_1           = 3,
   SM_DRS_MaxTraction_2           = 4,
   SM_DRS_Fallback                = 5
} DualRatioSwitchSM_enum;

typedef struct
{
   DualRatioSwitchSM_enum currentValue;
   DualRatioSwitchSM_enum newValue;
} DualRatioSwitch_States;

typedef enum
{
   SM_LAPS_Default  = 0,
   SM_LAPS_Lift     = 1,
   SM_LAPS_Down     = 2,
   SM_LAPS_Fallback = 3
} LAPSwitch_enum;

typedef struct
{
   LAPSwitch_enum newValue;
   LAPSwitch_enum currentValue;
} AldLAPS_States;

//=======================================================================================
// Public data declarations
//=======================================================================================
// Note: Shall not be used with AUTOSAR SWC.

//=======================================================================================
// Public macros
//=======================================================================================
// Note, coding convention: shall be avoided to support debug capability.

//=======================================================================================
// Public function prototypes
//=======================================================================================
void LiftAxlePositionSwitchTransition(       A3PosSwitchStatus      *pLiftAxle1Switch2_Status,
                                      const  LiftAxleUpRequestACK_T *pLiftAxle1UpRequestACK,
                                             uint16                 *pTimer_LA1UpReqACK,
                                             uint16                 *pTimer_LA1S2ButtonStuck,
                                             uint8                  *pinitialize_LAPS,
                                             AldLAPS_States         *pLAPSwitchSM);
void LiftAxlePositionSwitchOutputProcessing(const AldLAPS_States                *pLAPSwitchSM,
                                            const A3PosSwitchStatus             *pLiftAxle1Switch2_Status,
                                                  LiftAxleLiftPositionRequest_T *pLiftAxle1LiftPositionRequest);
void LiftAxlePositionLEDLogic(const A3PosSwitchStatus        *pLiftAxle1Switch2_Status,
                              const LiftAxlePositionStatus_T *pLiftAxle1PositionStatus,
                                    DualDeviceIndication_T   *pBogieSwitch_DeviceIndication);
void DualRatioSwitchStateTransitions(      A3PosSwitchStatus              *pDualRatioSwitchStatus,
                                     const LoadDistributionSelect         *pLoadDistributionSelected,
                                     const LoadDistributionRequestedACK_T *pLoadDistributionRequestedACK,
                                           uint16                         *pTimer_DualRatioSwitch_BS,
                                           uint16                         *pTimer_LoadDistributionReqACK,
                                           uint8                          *pinitialize_DRS,
                                           DualRatioSwitch_States         *pDualRatioSwitchSM);
void DualRatioSwitchOutputProcessing(const DualRatioSwitch_States      *pDualRatioSwitchSM,
                                     const A3PosSwitchStatus           *pDualRatioSwitchStatus,
                                           LoadDistributionRequested_T *pDRS_LoadDistributionRequested);
void DualRatioSwitchDeviceIndication(const LoadDistributionSelect *pLoadDistributionSelected,
                                           DualDeviceIndication_T *pDualRatio_DeviceIndication);
void ARideStateTransitions(A3PosSwitchStatus    *pLiftAxle2MaxTractSwitchStatus,
                           LiftAxleUpRequestACK *pLiftAxle1UpRequestACK,
                           uint16               *pTimer_LiftAxle2MaxTract_BS,
                           uint16               *pTimer_LiftAxle1UpReq,
                           uint16               *pTimer_LiftAxle1UpReq_NoACK,
                           uint8                *pinitialize_ARide,
                           AldARide_States      *pARideSM);
void ARideStateOutputProcessing(const AldARide_States               *pARideSM,
                                      LiftAxleLiftPositionRequest_T *pLiftAxle1LiftPositionRequest,
                                      LiftAxleLiftPositionRequest_T *pLiftAxle1DirectControl);
void ARideSwitchLEDLogic(const LoadDistributionSelect *pLoadDistributionSelected,
                               DeviceIndication_T     *pMaxTract_DeviceIndication);
void ALDSwitchStateTransitions(      A2PosSwitchStatus              *pALDSwitchStatus,
                               const LoadDistributionALDChangeACK_T *pLoadDistributionALDChangeACK,
                               const LoadDistributionSelect         *pLoadDistributionSelected,
                                     uint16                         *pTimer_ALDSwitch_BS,
                                     uint8                          *pinitialize_ALD,
                                     ALDSwitch_States               *pALD_StateSM);
void ALDSwitchStatusOutputProcessing(const ALDSwitch_States           *pALD_StateSM,
                                     const A2PosSwitchStatus          *pALDSwitchStatus,
                                           AltLoadDistribution_rqst_T *pALD_AltLoadDistribution_rqst);
void ALDDeviceIndication(const LoadDistributionSelect *pLoadDistributionSelected,
                               DeviceIndication_T     *pALD_DeviceIndication);
void RatioALDSwitchTransitions(      A3PosSwitchStatus              *pRatioALDSwitchStatus,
                               const LoadDistributionSelect         *pLoadDistributionSelected,
                               const LoadDistributionALDChangeACK_T *pLoadDistributionALDChangeACK,
                               const LoadDistributionChangeACK      *pLoadDistributionChangeACK,
                               const LoadDistributionFuncSelected_T *pLoadDistributionFuncSelected,
                                     uint16                         *pTimer_RatioALDSwitchBS,
                                     uint16                         *pTimer_RatioALDChangeACK,
                                     uint8                          *pinitialize_RatioALD,
                                     RatioALDSwitch_States          *pRatioALD_SM);
void RatioALDSwitchOutputProcessing(const RatioALDSwitch_States           *pRatioALD_SM,
                                          LoadDistributionChangeRequest_T *pRatioALD_LoadDistributionChangeRequest,
                                          AltLoadDistribution_rqst_T      *pRatioALD_AltLoadDistribution_rqst);
void RatioALD_LED_Logic(const LoadDistributionFuncSelected_T *pLoadDistributionFuncSelected,
                              DualDeviceIndication_T         *pRatioALD_DualDeviceIndication);
uint8 A3_ButtonStuckEvent(const A3PosSwitchStatus        *pA3Switch,
                                uint16                   *pA3timer,
                                uint8                    *pflag);
uint8 A2_ButtonStuckEvent(const A2PosSwitchStatus        *pA2Switch,
                                uint16                   *pA2timer,
                                uint8                    *pflag);
void Check_Acknowledge(const LoadDistributionRequestedACK_T  *pLoadDistributionRequestedACK,
                             uint16                          *pTimer_LoadDistributionReqACK,
                             DualRatioSwitch_States          *pDualRatioSwitchSM,
                             boolean                         *pisAck_Check);
//=======================================================================================
// End of file
//=======================================================================================
#endif
