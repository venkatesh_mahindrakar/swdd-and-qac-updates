#ifdef WIREDCONTROLBOX_HMICtrl_LOGIC_IF_H
#error WIREDCONTROLBOX_HMICtrl_LOGIC_IF_H unexpected multi-inclusion
#else
#define WIREDCONTROLBOX_HMICtrl_LOGIC_IF_H

//=======================================================================================
// Public type definitions
//=======================================================================================

void WiredControlBox_HMICtrl_Fallback_modes(const PushButtonStatus                             *pSelectButtonStatus,
												  WiredControlBox_HMICtrl_in_ButtonStructType  *pWCB_HMICtrl_input_Button,
                                            	  LevelAdjustmentAction_T                      *pWiredLvlAdjustAction,
                                            	  LevelAdjustmentAxles_T                       *pWiredLvlAdjustmentAxles);
void Storelevel_OutputStorelevel(const WiredControlBox_HMICtrl_in_ButtonStructType  *pWCB_in_Buttondata,
                                       LevelUserMemoryAction_T                      *pWiredLevelUserMemActn,
                                 const SEWS_ECS_MemSwTimings_P1BWF_s_T              *pECS_MemSwTiming,
                                 const Wcb_LevelAdjustment_FSM                      *pSM_States,
                                       uint16                                       *pStore_Timers);
void Recall_OutputRecalllevel(const WiredControlBox_HMICtrl_in_ButtonStructType  *pWCB_HMICtrl_input_Buttondata,
                                    LevelUserMemoryAction_T                      *pWiredLevelUserMemAction,
                              const SEWS_ECS_MemSwTimings_P1BWF_s_T              *pECS_MemSwTimings,
	                          const uint16                                       *pTimers);
uint8 FunctionButtonStucked(const WiredControlBox_HMICtrl_in_ButtonStructType  *pWCB_input_Buttondata,
                            const PushButtonStatus                             *pSelectButtonStatus,
                                  uint16                                       *pTimer,
                                  FalseTrue_T                                  *pWiredAirSuspensionStopRequest,
                                  LevelAdjustmentStroke_T                      *pWiredLevelAdjustmentStroke,
                                  LevelAdjustmentAction_T                      *pWiredLevelAdjustmentAction,
                                  LevelUserMemoryAction_T                      *pWiredLevelUserMemoryAction,
                                  WiredLevelUserMemory_T                       *pWiredLevelUserMemory,
                                  LevelAdjustmentAxles_T                       *pWiredLevelAdjustmentAxles);
void Fullaircontrolboxselectionlogic(const WiredControlBox_HMICtrl_in_ButtonStructType  *pWCB_in_ButtonLogic,
                                     const PushButtonStatus                             *pFullair_SelectButtonStatus,
                                           Wcb_Fullaircontrolbox_logic                  *pFullAirControl_States,
                                           StateInitializationType                      *pstateInitialize,
                                           WiredControlBox_HMICtrl_out_StructType       *pWiredControlBox_HMICtrl_out_data);
void ECSStandbyrequestactivation(const WiredControlBox_HMICtrl_in_ButtonStructType  *pWCB_HMICtrl_in_Buttondata,
                                 const uint8                                        buttonStuck_rtrn,
                                 const PushButtonStatus                             *pSelectbutton,
                                 const uint16                                       *pStop_Timer,
                                       uint16                                       *pTimerECS,
                                       ECSStandByReq_T                              *pECSStandByReqRCECS);
//=======================================================================================
// Public constants and enums definitions
//=======================================================================================

//=======================================================================================
// Public macros
//=======================================================================================


//=======================================================================================
// End of file
//=======================================================================================
#endif
