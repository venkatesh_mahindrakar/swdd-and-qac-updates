
#include "Rte_Type.h"

//=======================================================================================
// Private type definitions
//=======================================================================================

typedef struct {
    VehicleModeDistribution_T   Previous_state;
    VehicleModeDistribution_T   Current_state;
}SwcActivation_In_StructType;;

typedef struct {
    A3PosSwitchStatus_T     Previous_state;
    A3PosSwitchStatus_T     Current_state;
}A3PosSwitchStatus_Data;

typedef struct {
    A3PosSwitchStatus_Data  RoofHatch_SwitchStatus_1;
    A3PosSwitchStatus_Data  RoofHatch_SwitchStatus_2;
}DoubleSwitch_In_StructType;

typedef struct {
    A3PosSwitchStatus_T     SwitchStatus_combined;
}DoubleSwitch_Out_StructType;

//=======================================================================================
// Private constants and enum definitions
//=======================================================================================

// State Machine constants
enum DS_SM_Enum { SM_DS_Status_Middle,
                  SM_DS_Status_Upper,
                  SM_DS_Status_Lower,
                  SM_DS_Status_Failure};

//=======================================================================================
// Private macros
//=======================================================================================



//=======================================================================================
// Private data declarations
//=======================================================================================
// Note: MUST BE static
// Note: Shall not be used/avoided with AUTOSAR SWC.



//=======================================================================================
// Private function prototypes
//=======================================================================================
// Note : MUST BE static

void SwcActivation_DoubleSwitch(SwcActivation_In_StructType *pRteInData_swcactivation);
void ReadInData_DoubleSwitch(DoubleSwitch_In_StructType *pRteInData_DS);
A3PosSwitchStatus_T Ds_InputLogic(DoubleSwitch_In_StructType *pRteInData_DS, enum DS_SM_Enum *pDS_SM);


//=======================================================================================
// End of file
//=======================================================================================
