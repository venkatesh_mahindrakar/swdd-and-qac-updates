#ifdef LEVELCONTROL_HMICTRL_LOGIC_H
#error LEVELCONTROL_HMICTRL_LOGIC_H undefined multi-inclusion
#else 
#define LEVELCONTROL_HMICTRL_LOGIC_H
//=====================================================================================
// Included header files
//=====================================================================================

//=======================================================================================
// Private constants and enum definitions
//=======================================================================================

//=======================================================================================
// Private macros
//=======================================================================================

// Varient Parameters
#define CONST_ConversionFactor                    (1000U)
#define CONST_FerryFunc_NoAckTimeout              (((uint16)5000U) / CONST_RunnableTimeBase)
#define CONST_kneelFunc_NoAckTimeout              (((uint16)5000U) / CONST_RunnableTimeBase)
#define CONST_LoadingLevelAckTimeout              (((uint16)5000U) / CONST_RunnableTimeBase)
#define CONST_AnwEcsStandbyDeactTimeout           (((uint16)3000U) / CONST_RunnableTimeBase)

//=======================================================================================
// Private data declarations
//=======================================================================================

//=======================================================================================
// Private type definitions
//=======================================================================================

//=======================================================================================
// Private function prototypes
//=======================================================================================

static void Ferryfun_OutputProcessing(const SM_Ferryswitch      *pOutFerryswitch_SM,
                                            ChangeRequest2Bit_T *pFerryFunctionSwitchChangeReq,
                                            RampLevelRequest_T  *pFerryRampLevelRequest);
static void KneelingSwitch_Outprocessing(      KneelingChangeRequest_T *pKneelingChangeRequest,
                                         const SM_KneelingSwitch       *pStateMachine_KneelingSwitch);
static void RampLevel_OutputProcessing(const Ramplevel_Switch     *pOutSM_Ramplevel,
                                             RampLevelRequest_T   *pRampLevelRequest,
                                             LevelStrokeRequest_T *pRampLevelStrokeRequest);
static void AltDriveLevelSwitch_OutputProcessing(const SM_ADLS              *pOutTransitionState,
                                                       RideHeightFunction_T *pRideHeightFunctionRequest);
static void FPBR_Switch_OutputProcessing(const FPBRMMIStat_T       *pOutSM_FPBR_Switch,
                                         const FPBR_Switch         *pStateMachine_FPBR_Switch,
                                               DeviceIndication_T  *pFPBR_DeviceIndication,
                                               FPBRChangeReq_T     *pFPBRChangeReq);
static void ECS_StandbyFSM_Transitions(const VehicleMode_T    *pVehicleMode,
                                             ECS_StandbyFSM   *pSM_ECS_StandbyFSM,
                                       const ECSStandByReq    *pECSStandByReqRCECS_FSM,
                                       const ECSStandByReq    *pECSStandByReqWRC,
                                       const FalseTrue_T      *pECSStandbyAllowed,
	                                         uint8            *pECS_StandbyFSM_Type,
                                             uint32           *ptimers_ECSActive,
                                             uint16           *ptimers_ECSBlinkTime,
											 FalseTrue_T      *pBlinkECSWiredLEDs);
static void ECS_StandbyFSM_Outprocessing(const ECS_StandbyFSM      *pOutSM_ECS_StandbyFSM,
                                               ECSStandByRequest_T *pECSStandByRequest,
											   FalseTrue_T         *pBlinkECSWiredLEDs,
										 const ECSStandByReq       *pECSStandByReqRCECS);
static void ANW_ECSStandByActive(const FormalBoolean *pisActTriggerDetected,
                                 const FormalBoolean *pisDeActTriggerDetected);
static FormalBoolean ECS_ActivationTrigger(const ECS_StandbyFSM  *pECSANWControl_SM);
static FormalBoolean ECS_DeactivationTrigger(const ECS_StandbyFSM  *pECSANWCtrl_SM);
//=======================================================================================
// End of file
//=======================================================================================
#endif

