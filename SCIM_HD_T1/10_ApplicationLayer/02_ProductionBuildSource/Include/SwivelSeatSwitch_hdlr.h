#ifdef SWIVELSEATSWITCH_HDLR_H
#error SWIVELSEATSWITCH_HDLR_H unexpected multi-inclusion
#else
#define SWIVELSEATSWITCH_HDLR_H
//=====================================================================================
// Included header files
//=====================================================================================


//=======================================================================================
// Private constants and enum definitions
//=======================================================================================


//=======================================================================================
// Private macros
//=======================================================================================

#ifndef CONST_AdiPin8_SwivelSeat_Hdlr
#define CONST_AdiPin8_SwivelSeat_Hdlr              (8U)
#endif 


//=======================================================================================
// Private type definitions
//=======================================================================================


typedef struct
{
    SeatSwivelStatus_T   preValue;
    SeatSwivelStatus_T   newvalue;
}SeatSwivelStatus;


// Inputs structure 
typedef struct
{
    VehicleModeDistribution_T  SwcActivation_EngineRun;
    VGTT_EcuPinVoltage_0V2     AdiPinVoltage_SwivelSeatHdlr;
    VGTT_EcuPinVoltage_0V2     LivingPullUpVoltage_SwivelSeatHdlr;
   
} SwivelSeatSwitch_hdlr_in_StructType;

// Output structure 
typedef struct
{
    SeatSwivelStatus   SeatSwivelStatus;
} SwivelSeatSwitch_hdlr_out_StructType;

//=======================================================================================
// Private data declarations
//=======================================================================================
// Note: MUST BE static
// Note: Shall not be used/avoided with AUTOSAR SWC.


//=======================================================================================
// Private function prototypes
//=======================================================================================
// Note : MUST BE static

//=====================================================================================
// End of file
//=====================================================================================
#endif
 