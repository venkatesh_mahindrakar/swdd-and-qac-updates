#ifdef LKS_HMICtrl_H
#error LKS_HMICtrl_H unexpected multi-inclusion
#else
#define LKS_HMICtrl_H

//=====================================================================================
// Included header files
//=====================================================================================


//=======================================================================================
// Private constants and enum definitions
//=======================================================================================


//=======================================================================================
// Private macros
//=======================================================================================

// Defined parameters
#define CONST_TimeBase                  (20U)
#define CONST_TimersUsed                (4U)
#define CONST_PushButtonTimer           (0U)
#define CONST_OnOffRockerSwitchTimer    (1U)
#define CONST_LKSCSSwitchTimer          (2U)
#define CONST_LKSSwitchTimer            (3U)
#define CONST_PushButtonTimeout         ((200U) / CONST_TimeBase)
#define CONST_OnOffRockerSwTimeout      ((200U) / CONST_TimeBase)
#define CONST_LKSCSSwitchTimeout        ((200U) / CONST_TimeBase)

# ifndef CONST_Off
# define CONST_Off                      (0U)
# endif

# ifndef CONST_On
# define CONST_On                       (1U)
# endif

//=======================================================================================
// Private type definitions
//=======================================================================================

// structure for inputs 
typedef struct
{
   LKSStatus_T                   LKSApplicationStatus;
   A3PosSwitchStatus_T           LKSCS_SwitchStatus;
   LKSCorrectiveSteeringStatus_T LKSCorrectiveSteeringStatus;
   PushButtonStatus_T            LKSPushButton;
   A2PosSwitchStatus_T           LKS_SwitchStatus;
   VehicleModeDistribution_T     SwcActivation_EngineRun;
} LKS_HMICtrl_in_StructType;

// structure for outputs 
typedef struct
{
   OffOn_T                LKSCSEnableSwitch_rqst;
   DualDeviceIndication_T LKSCS_DeviceIndication;
   OffOn_T                LKSEnableSwitch_rqst;
   DeviceIndication_T     LKS_DeviceIndication;
} LKS_HMICtrl_out_StructType;

//=======================================================================================
// Private data declarations
//=======================================================================================
// Note: MUST BE static
// Note: Shall not be used/avoided with AUTOSAR SWC.


//=======================================================================================
// Private function prototypes
//=======================================================================================
// Note : MUST BE static

// function declarations 
static void InputLogic_WithoutSwitch(LKS_HMICtrl_out_StructType *pLKS_HMICtrl_out_data);
static void PushButtonInputLogic(const PushButtonStatus_T *pLKSPushButton,
                                       OffOn_T            *pLKSEnable_Switchrqst,
                                       uint16             *pTimer_PushButton);
static void OnOffRockerSwitch(const A2PosSwitchStatus_T *pLKS_SwitchStatus,
                                    OffOn_T             *pLKSEnableSwrequest,
                                    uint16              *pTimer_OnOffRockerSwitch);
static void FeedbackLEDIndicationLogic(const LKSStatus_T        *pLKSApplicationStatus,
                                             DeviceIndication_T *pLKS_DeviceIndication);
static void LKSCSSwitchLogic(const A3PosSwitchStatus_T *pLKSCSSwitchStatus,
                                   OffOn_T             *pLKSEnableSwrqst,
                                   OffOn_T             *pLKSCSEnableSwitchrqst,
                                   uint16              *pTimer_LKSSwitch,
                                   uint16              *pTimer_LKSCSSwitch);
static void LKSCSIndication(const LKSStatus_T                    *pLKSApplicationSts,
                            const LKSCorrectiveSteeringStatus_T  *pLKSCorrectiveSteeringStatus,
                                  DualDeviceIndication_T         *pLKSCS_DvcIndication);
static void Get_RteInData_Common(LKS_HMICtrl_in_StructType  *pLKS_HMICtrl_input_data);
static void RteOutDataWrite_Common(const LKS_HMICtrl_out_StructType *pRteOutData_common);


//=====================================================================================
// End of file
//=====================================================================================
#endif
