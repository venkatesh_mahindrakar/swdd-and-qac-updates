#ifdef LEVELCONTROL_HMICTRL_LOGIC_IF_H
#error LEVELCONTROL_HMICTRL_LOGIC_IF_H undefined multi-inclusion
#else 
#define LEVELCONTROL_HMICTRL_LOGIC_IF_H
//=====================================================================================
// Included header files
//=====================================================================================
#include "FuncLibrary_AnwStateMachine_If.h"

//=======================================================================================
// Public constants and enums definitions
//=======================================================================================

//=======================================================================================
// Public macros
//=======================================================================================

//=======================================================================================
// Public type definitions
//=======================================================================================
// States Start
typedef enum
{
   SM_ECSStandby_StandByDisabled         = 0,
   SM_ECSStandby_InitStandBy             = 1,
   SM_ECSStandby_StandByIdle             = 2,
   SM_ECSStandby_WaitForAllowed          = 3,
   SM_ECSStandby_StandByActive           = 4,
   SM_ECSStandby_ExtendedStandBy         = 5,
   SM_FPBRSwitch_Init                    = 6,
   SM_FPBRSwitch_Neutral                 = 7,
   SM_FPBRSwitch_Pushed                  = 8,
   SM_ADLS_StandardDrivePosition         = 9,
   SM_ADLS_AlternativeDrivePosition1     = 10,
   SM_ADLS_AlternativeDrivePosition2     = 11,
   SM_Ramplevel_NoAction                 = 12,
   SM_Ramplevel_M1                       = 13,
   SM_Ramplevel_M2                       = 14,
   SM_Ramplevel_Stuck                    = 15,
   SM_KneelingSwitch_Default             = 16,
   SM_KneelingSwitch_StandBy             = 17,
   SM_KneelingSwitch_SwitchOff           = 18,
   SM_KneelingSwitch_ChangeKneelFunction = 19,
   SM_KneelingSwitch_Error               = 20,
   SM_FerrySwitch_Default                = 21,
   SM_FerrySwitch_StandBy                = 22,
   SM_FerrySwitch_SwitchOff              = 23,
   SM_FerrySwitch_ChangeFerryFunction    = 24,
   SM_FerrySwitch_Stuck                  = 25
} switch_sm_states;

typedef struct
{
   switch_sm_states CurrentValue;
   switch_sm_states PreviousValue;
} ECS_StandbyFSM;

typedef struct
{
   switch_sm_states CurrentValue;
   switch_sm_states PreviousValue;
} FPBR_Switch;

typedef struct
{
   switch_sm_states PreviousValue;
   switch_sm_states CurrentValue;
} SM_ADLS;

typedef struct
{
   switch_sm_states CurrentValue;
   switch_sm_states PreviousValue;
} Ramplevel_Switch;

typedef struct
{
   switch_sm_states PreviousValue;
   switch_sm_states CurrentValue;
} SM_KneelingSwitch;

typedef struct
{
   switch_sm_states CurrentValue;
   switch_sm_states PreviousValue;
} SM_Ferryswitch;

typedef struct
{
   uint8    is_active_ECS;
   uint8    is_active_FPBR;
   uint8    is_active_FerrySwitch;
   uint8    is_active_ADLS;
   uint8    is_active_Ramplevel;
   uint8    is_active_KneelingSwitch;
} StateMachine_StrctType_T;

//=======================================================================================
// Public data declarations
//=======================================================================================

//=======================================================================================
// Public function prototypes
//=======================================================================================
// Note, coding convention: shall be avoided to support debug capability.
void Ferryswitch_StateTransitions(const A2PosSwitchStatus_T *pFerryFunctionSwitchStatus,
                                  const Ack2Bit_T           *pFerryFunctionSwitchChangeACK,
                                        SM_Ferryswitch      *pFerrySwitch_SM,
                                        uint8               *pFerrySwitch_type,
                                        uint16              *pFerryTimers,
                                        uint16              *pFerryTimers_ChangeAck,
										ChangeRequest2Bit_T *pFerryFunctionSwitchChangeReq,
                                        RampLevelRequest_T  *pFerryRampLevelRequest);
void Ferryfun_Indication(const FerryFunctionStatus_T *pFerryFunctionStatus,
                               DeviceIndication_T    *pFerryFunction_DeviceIndication);
void KneelingSwitch_StateTransitions(const A2PosSwitchStatus        *pKneelingSwitchStatus,
                                     const ChangeKneelACK_T         *pChangeKneelACK,
                                           SM_KneelingSwitch        *pSM_KneelingSwitch,
                                           uint8                    *pKneelingSwitch_Type,
                                           uint16                   *pKneelingSwitchTimers,
                                           uint16                   *pKneelingTimers_changeAck,
										   KneelingChangeRequest_T  *pKneelingChangeRequest);
void KneelingSwitch_DeviceIndication(const KneelingStatusHMI_T *pKneelingStatusHMI,
                                           DeviceIndication_T  *pKneelDeviceIndication);
switch_sm_states RampLevel_StateTransitions(      Ramplevel_Switch     *pSM_Ramplevel,
                                            const A3PosSwitchStatus    *pLoadingLevelSwitchStatus,
                                            const ChangeRequest2Bit_T  *pRampLevelRequestACK,
                                                  uint8                *pRamplevel_Type,
                                                  uint16               *pRampLeveltimers,
                                                  uint16               *ptimers_rampAck,
												  RampLevelRequest_T   *pRampLevelRequest,
                                                  LevelStrokeRequest_T *pRampLevelStrokeRequest);
void AltDriveLevelSwitch_Transition(const A3PosSwitchStatus     *pAlternativeDriveLevelSw_stat,
                                          SM_ADLS               *pTransitionState,
                                          uint8                 *pADLS_Type,
										  RideHeightFunction_T  *pRideHeightFunctionRequest);
void FPBR_Switch_StateTransition(      FPBR_Switch        *pSM_FPBR_Switch,
                                 const PushButtonStatus   *pFPBRSwitchStatus,
                                 const FPBRMMIStat_T      *pFPBRMMIStat,
                                       uint8              *pFPBR_Type,
                                       uint16             *pFPBRtimers,
									   DeviceIndication_T *pFPBR_DeviceIndication,
                                       FPBRChangeReq_T    *pFPBRChangeReq);
void FPBR_SwitchstuckProcessing(const PushButtonStatus *pFPBRSwitchStatus,
                                      uint16           *pFPBRtimers_Stuck);
uint8 LoadingLvlAdj_SwitchLogic(LevelStrokeRequest_T   *pLevelStrokeRequest,
                                LevelRequest_T         *pLevelRequest,
                                A3PosSwitchStatus      *pLoadingLevelAdjSwitchStatus,
                                uint16                 *pLoadingLvlAdjSwitchtimer_Stuck);
void Output_InhibitWRCECS(const A3PosSwitchStatus   *pLoadingLevelAdjSwitchStatus,
                          const A3PosSwitchStatus   *pLoadingLevelSwitchStatus,
                          const SM_KneelingSwitch   *pSM_KneelingSwitch,
                          const SM_Ferryswitch      *pFerrySwitch_SM,
                          const Ramplevel_Switch    *pRamplevelSwitch_SM,
                          const uint16              *pLoadingLvlAdjSwitch_Stucktimer,
                          const uint16              *pRamptimer,
                          const uint16              *pFerryimer,
                          const uint16              *pKneelingtimer,
                                InactiveActive_T    *pInhibitWRCECSMenuCmd);
void ECS_StandByLogic(const VehicleMode_T        *pVehicleMode,
						    ECS_StandbyFSM       *pSM_ECS_StandbyFSM,
					  const ECSStandByReq        *pECSStandByReqRCECS_FSM,
					  const ECSStandByReq        *pECSStandByReqWRC,
					  const FalseTrue_T          *pECSStandbyAllowed,
						    uint8                *pECS_StandbyFSM_Type,
						    uint32               *ptimers_ECSActive,
						    uint16               *ptimers_ECSBlinkTime,
						    ECSStandByRequest_T  *pECSStandByRequest,
                            FalseTrue_T          *pBlinkECSWiredLEDs);
                           
//=======================================================================================
// End of file
//=======================================================================================
#endif
 
