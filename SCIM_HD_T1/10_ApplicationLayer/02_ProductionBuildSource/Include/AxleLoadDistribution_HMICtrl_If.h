#ifdef AXLELOADDISTRIBUTION_HMICTRL_IF_H
#error AXLELOADDISTRIBUTION_HMICTRL_IF_H unexpected multi-inclusion
#else
#define AXLELOADDISTRIBUTION_HMICTRL_IF_H
//=====================================================================================
// Included header files
//=====================================================================================

//=======================================================================================
// Public type definitions
//=======================================================================================

typedef struct
{
   LoadDistributionSelected_T previousValue;
   LoadDistributionSelected_T currentValue;
} LoadDistributionSelect;

typedef struct
{
   A2PosSwitchStatus_T previousValue;
   A2PosSwitchStatus_T currentValue;
} A2PosSwitchStatus;

typedef struct
{
   A3PosSwitchStatus_T previousValue;
   A3PosSwitchStatus_T currentValue;
} A3PosSwitchStatus;

typedef struct
{
   LiftAxleUpRequestACK_T previousValue;
   LiftAxleUpRequestACK_T currentValue;
} LiftAxleUpRequestACK;

typedef struct
{
   LoadDistributionChangeACK_T previousValue;
   LoadDistributionChangeACK_T currentValue;
} LoadDistributionChangeACK;

//=======================================================================================
// Public constants and enums definitions
//=======================================================================================
#ifndef CONST_NonInitialize
#define CONST_NonInitialize          (0U)
#endif 

#ifndef CONST_Initialize
#define CONST_Initialize             (1U)
#endif

#ifndef CONST_ResetFlag
#define CONST_ResetFlag              (0U)
#endif 

#ifndef CONST_SetFlag
#define CONST_SetFlag                (1U)
#endif



//=======================================================================================
// Public data declarations
//=======================================================================================


//=======================================================================================
// Public macros
//=======================================================================================
#define CONST_RunnableTimeBase                          (20U)
#define CONST_Axleload_Timeout                          ((uint16)(5000U/CONST_RunnableTimeBase))
#define CONST_ButtonStuckTimerPeriod                    ((uint16)(90000U/CONST_RunnableTimeBase))

//=======================================================================================
// Public function prototypes
//=======================================================================================


//=======================================================================================
// End of file
//=======================================================================================
#endif
