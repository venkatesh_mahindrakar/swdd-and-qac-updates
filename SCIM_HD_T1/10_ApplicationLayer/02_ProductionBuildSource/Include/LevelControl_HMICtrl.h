#ifdef LEVELCONTROL_HMICTRL_H
#error LEVELCONTROL_HMICTRL_H undefined multi-inclusion
#else 
#define LEVELCONTROL_HMICTRL_H
//=====================================================================================
// Included header files
//=====================================================================================

//=======================================================================================
// Private constants and enum definitions
//=======================================================================================

//=======================================================================================
// Private macros
//=======================================================================================

//=======================================================================================
// Private type definitions
//=======================================================================================
// defined parameters
typedef enum
{
   CONST_FerryTimer             = 0,
   CONST_StoreRecallRampTimer   = 1,
   CONST_StoreRestoreStandard   = 2,
   CONST_AdjustFront            = 3,
   CONST_AdjustRear             = 4,
   CONST_B2D                    = 5,
   CONST_StopReq                = 6,
   CONST_AdjustStroke           = 7
} Wired_Timer;

typedef enum
{
   CONST_SwitchFerryTimer       = 8,
   CONST_FerryAck               = 9,
   CONST_KneelTimer             = 10,
   CONST_KneelACK               = 11,
   CONST_RampLevelTimer         = 12,
   CONST_RampLevelAck           = 13,
   CONST_FPBRAck                = 14,
   CONST_FPBRStuck              = 15,
   CONST_LoadingLevelStuckTimer = 16,
   CONST_ECSActive              = 17,
   CONST_ECSBlinkTime           = 18,
   CONST_OutECSActive           = 19
} Timer;

// local structure variables for StoreRamp 
typedef struct
{
   uint8   is_active_StoreRamplevel;
   uint8   is_active_RecallRampLevel;
   uint8   is_active_StoreRestoreStandard;
   uint8   is_active_BackToDriveLevelReq;
   uint8   is_active_StopRequestLevel;
} state_StructType_T;

typedef struct 
{
   LevelAdjustmentActionType     LevelAdjustmentAction;
   LevelUserMemoryActionType     LevelUserMemoryAction;
   LevelUserMemory_T             LevelUserMemory;
   LevelAdjustmentStrokeType     LevelAdjustmentStroke;
   FalseTrue                     AirSuspensionStopRequest;   
   LevelAdjustmentAxles_T        LevelAdjustmentAxles;   
} LevelControl_In_CntrlBox_StructType_T;

typedef struct
{
   StorageAck_T              RampLevelStorageAck;
   ChangeRequest2Bit_T       RampLevelRequestACK;
   BackToDriveReqACK_T       BackToDriveReqACK;
   StorageAck_T              RideHeightStorageAck;
   StopLevelChangeStatus_T   StopLevelChangeAck;
} LevelControl_CBCommon_InputStructType_T;
//=======================================================================================
// Private data declarations
//=======================================================================================

//=======================================================================================
// Private function prototypes
//=======================================================================================
// static Local Functions
static void LevelControl_HMICtrl_Loc_Init(const FalseTrue                       *pWiredAirSuspensionStopReq,
                                          const FalseTrue                       *pWRCAirSuspensionStopReq,
                                                LevelControl_Out_StructType_T   *pLevelCntrlOutData,
                                                state_StructType_T              *pState_Type,
                                                StateMachine_StrctType_T        *pStateMachine_Type,
                                                uint16                          Timer_init[CONST_NbOfTimers]);
static void ControlBoxLogicForFerryFunction(LevelAdjustmentActionType *pLvlAdjustmentAction,
                                            Request_T                 *pFerryFuncRequest,
                                            uint16                    *pTimers);
static void ControlBoxLogicForStoreRampLevel(      Lvlcntrl_SM_States         *pRampLevelState,
                                             const LevelUserMemoryActionType  *pLvlUserMemoryAction,
                                             const LevelAdjustmentStroke_T    *pLvlAdjustmentStroke_cur,
                                             const StorageAck_T               *pRampLvlStorageAck,
                                             const LevelUserMemory_T          *pLvlUserMemory,
                                                   uint8                      *pStoreRampData,
                                                   RampLevelRequest_T         *pRampLvlStorageRequest,
                                                   uint16                     *pTimers_StoreRamp);
static void ControlBoxLogicForRecallRampLevel(      Lvlcntrl_SM_States          *pRampLevelState,
                                              const LevelUserMemoryActionType   *pLevelUserMemoryAction,
                                              const LevelAdjustmentStroke_T     *pLevelAdjustStroke_cur,
                                              const LevelUserMemory_T           *pLevelUserMemory,
                                              const ChangeRequest2Bit_T         *pRampLvlRequestACK,
                                                    uint8                       *pRecallRampData,
                                                    RampLevelRequest_T          *pRampLvlRequest,
                                                    uint16                      *pTimers_RecallRamp);
static void ControlBoxLogicForStoreRestoreStandard(const LevelUserMemoryActionType  *pLevelUserMemoryAction,
                                                   const LevelAdjustmentStroke_T    *pLevelAdjustStroke_cur,
                                                   const StorageAck_T               *pRideHeightStoreAck,
                                                         Lvlcntrl_SM_States         *pRampLevelState,
                                                         uint8                      *pStoreRestoreRampData,
                                                         RideHeightStorageRequest_T *pRideHghtStoreRequest,
                                                         uint16                     *pTimers_StoreRestoreStandard);
static void ControlBoxLogicForAdjustStrokeRequest(const LevelAdjustmentStrokeType *pLevelAdjustmentStroke,
                                                        LevelStrokeRequest_T      *pLvlStrokeRequest,
                                                        uint16                    *pTimers_AdjustStroke);
static void ControlBoxLogicForBackToDriveLevelRequest(const LevelAdjustmentActionType  *pLvlAdjustAction,
                                                      const BackToDriveReqACK_T        *pBackToDriveReqstACK,
                                                            uint8                      *pBackToDrive,
                                                            BackToDriveReq_T           *pBackToDriveReqst,
                                                            uint16                     *pTimers_B2D);
static Lvlcntrl_SM_States ControlBoxLogicForStopRequest(      FalseTrue               *pAirSuspensionStopRequest,
                                                        const StopLevelChangeStatus_T *pStopLvlChangeAck,
                                                              uint8                   *pStopRequest,
                                                              Request_T               *pStopLvlChangeRequest,
                                                              LevelRequest_T          *pLvlReq,
                                                              uint16                  *pTimers_StopReq);
static void ControlBoxLogicForAdjustLevelMain(const LevelAdjustmentActionType   *pLevelAdjustmentAction,
                                              const LevelAdjustmentAxles_T      *pLevelAdjustmentAxles,
                                              const Lvlcntrl_SM_States          StopRequest_state,
                                                    LevelRequest_T              *pLvlRequest,
                                                    uint16                      *pTimers_AdjustFront,
                                                    uint16                      *pTimers_AdjustRear);
static void AdjustLevelRear(const LevelAdjustmentActionType  *pAdjustLvlData,
                                  LevelRequest_T             *pLevelRqst,
                                  uint16                     *pTimers_AdjustRear);
static void AdjustLevelFront(const LevelAdjustmentActionType   *pAdjustLevelData,
                                   LevelRequest_T              *pLvlRqst,
                                   uint16                      *pTimers_AdjustFront);
static void Get_RteInDataRead_Common(LevelControl_CommonInput_StructType_T     *pLevelControl_InDataCommon,
                                     LevelControl_CBCommon_InputStructType_T   *pRteInData_CBCommon,
									 LevelControl_Switches_StructType_T        *pRteInData_Switches);
static void Get_RteInDataRead_Wired(LevelControl_In_CntrlBox_StructType_T *pLevelControl_InDataWired);
static void Get_RteInDataRead_WRC(LevelControl_In_CntrlBox_StructType_T *pLevelControl_InDataWRC,
                                  RollRequest_T                         *pWRCRollRequest);
static void RteInDataWrite_Common(const LevelControl_Out_StructType_T  *pLevelControl_OutData);
static void ControlBoxCommonLogic(       LevelControl_In_CntrlBox_StructType_T     *pLevelControlCBInput,
                                   const LevelControl_CBCommon_InputStructType_T    *pRteInData_CBCommon,
								         state_StructType_T                        *pstate_Type,
								         uint16                                    pTimers[CONST_NbOfTimers],
								         LevelControl_Out_StructType_T             *pRteOutData_Common,
								   const switch_sm_states                          *pRecallRamp_switch,
								         Lvlcntrl_SM_States                        *pControlBoxRampLevel_State);
static void ControlLogicForSwitches(      LevelControl_Switches_StructType_T  *pRteInData_Switches,
                                    const ChangeRequest2Bit_T                 *pRampLevelRequestACK,
									      StateMachine_StrctType_T            *pStateMachine_Type,
									      LevelControl_Out_StructType_T       *pRteOutData_Common,
									      uint16                              pTimers[CONST_NbOfTimers],
									const Lvlcntrl_SM_States                  *pControlBoxRampLevel_State,
									      switch_sm_states                    *pRecallRamp_switch);

//=======================================================================================
// End of file
//=======================================================================================
#endif
