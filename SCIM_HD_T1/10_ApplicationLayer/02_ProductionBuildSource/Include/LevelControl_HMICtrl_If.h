#ifdef LEVELCONTROL_HMICTRL_IF_H
#error LEVELCONTROL_HMICTRL_IF_H undefined multi-inclusion
#else 
#define LEVELCONTROL_HMICTRL_IF_H

//=======================================================================================
// Public constants and enums definitions
//=======================================================================================

//=======================================================================================
// Public macros
//=======================================================================================

#define CONST_RunnableTimeBase                  (20U)
#define CONST_NbOfTimers                        (20U)
#define CONST_NbofTimersTT                      (1U)
#define CONST_ECSTimerTT                        (0U)

#ifndef CONST_NotInitialize
#define CONST_NotInitialize                     (0U)
#endif

#ifndef CONST_Initialize
#define CONST_Initialize                        (1U)
#endif
// Wired Varient Parameters
#define CONST_FerryFunctionReqTimeout           (((uint16)500U) / CONST_RunnableTimeBase)
#define CONST_StoreStandardTimeout              (((uint16)5000U) / CONST_RunnableTimeBase)
#define CONST_RampLevelStorageTimeout           (((uint16)5000U) / CONST_RunnableTimeBase)
#define CONST_RampLevelRequestTimeout           (((uint16)5000U) / CONST_RunnableTimeBase)
#define CONST_RestoreFactoryTimeout             (((uint16)5000U) / CONST_RunnableTimeBase)
#define CONST_LvlStrokeReqInitTimeout           (((uint16)100U) / CONST_RunnableTimeBase)
#define CONST_B2DSwitchStatus_Timeout           (((uint16)5000U) / CONST_RunnableTimeBase)
#define CONST_StopRequestTimeout                (((uint16)5000U) / CONST_RunnableTimeBase)
#define CONST_AdjustLevelTimeout                (((uint16)60U) / CONST_RunnableTimeBase)

#define PCODE_ECS_PartialAirSystem              (Rte_Prm_P1ALT_ECS_PartialAirSystem_v())
#define PCODE_ECS_FullAirSystem                 (Rte_Prm_P1ALU_ECS_FullAirSystem_v())
#define PCODE_WirelessRC_Enable                 (Rte_Prm_P1B9X_WirelessRC_Enable_v())

#ifndef CONST_ResetFlag
#define CONST_ResetFlag                         (1U)
#endif 

#ifndef CONST_SetFlag
#define CONST_SetFlag                           (2U)
#endif 

#ifndef CONST_SWITCHSTATUSUPPER
#define CONST_SWITCHSTATUSUPPER                 (2U)
#endif

#ifndef CONST_SWITCHSTATUSLOWER
#define CONST_SWITCHSTATUSLOWER                 (1U)
#endif
//=======================================================================================
// Public type definitions
//=======================================================================================

typedef enum
{
   SM_StoreRampLevel_StoreLvlReq = 0,
   SM_StoreRamp_StoreStandard    = 1,
   SM_Ramp_NoAction              = 2,
   SM_RecallRamp_RampLvlRequest  = 3,
   SM_RecallRamp_RestoreFactory  = 4,
   SM_BackToDrive_B2D            = 5,
   SM_BackToDrive_Idle           = 6,
   SM_StopRequest_NoAction       = 7,
   SM_StopRequest_StopAction     = 8
} Lvlcntrl_SM_States;

typedef struct
{
   LevelAdjustmentAction_T   Previous;
   LevelAdjustmentAction_T   Current;
} LevelAdjustmentActionType;

typedef struct
{
   LevelUserMemoryAction_T   Previous;
   LevelUserMemoryAction_T   Current;
} LevelUserMemoryActionType;

typedef struct
{
   FalseTrue_T   Previous;
   FalseTrue_T   Current;
} FalseTrue;

typedef struct
{
   LevelAdjustmentStroke_T   Previous;
   LevelAdjustmentStroke_T   Current;
} LevelAdjustmentStrokeType;

typedef struct
{
   ECSStandByReq_T PreviousValue;
   ECSStandByReq_T CurrentValue;
} ECSStandByReq;

typedef struct
{
   A3PosSwitchStatus_T PreviousValue;
   A3PosSwitchStatus_T CurrentValue;
} A3PosSwitchStatus;

typedef struct
{
   A2PosSwitchStatus_T PreviousValue;
   A2PosSwitchStatus_T CurrentValue;
} A2PosSwitchStatus;

typedef struct
{
   PushButtonStatus_T PreviousValue;
   PushButtonStatus_T CurrentValue;
} PushButtonStatus;

// Input structure for levelcontrol
typedef struct
{
   A2PosSwitchStatus_T       FerryFunctionSwitchStatus;
   Ack2Bit_T                 FerryFunctionSwitchChangeACK;
   FerryFunctionStatus_T     FerryFunctionStatus;
   A2PosSwitchStatus         KneelingSwitchStatus;
   ChangeKneelACK_T          ChangeKneelACK;
   KneelingStatusHMI_T       KneelingStatusHMI;
   A3PosSwitchStatus         LoadingLevelSwitchStatus;
   A3PosSwitchStatus         LoadingLevelAdjSwitchStatus;
   PushButtonStatus          FPBRSwitchStatus;
   A3PosSwitchStatus         AlternativeDriveLevelSw_stat;
   FPBRMMIStat_T             FPBRMMIStat;
} LevelControl_Switches_StructType_T;

typedef struct
{
	VehicleModeDistribution_T  SwcActivation_Living;
	FalseTrue                  HeightAdjustmentAllowed;
	VehicleMode_T             VehicleMode;
   ECSStandByReq             ECSStandByReqRCECS;
   ECSStandByReq             ECSStandByReqWRC;
   FalseTrue_T               ECSStandbyAllowed;
}LevelControl_CommonInput_StructType_T;

// Output structure for levelcontrol
typedef struct
{
   RampLevelRequest_T                RampLevelStorageRequest;
   RampLevelRequest_T                RampLevelRequest;
   Request_T                         FerryFunctionRequest;
   BackToDriveReq_T                  BackToDriveReq;
   RideHeightStorageRequest_T        RideHeightStorageRequest;
   LevelStrokeRequest_T              LevelStrokeRequest;
   Request_T                         StopLevelChangeRequest;
   RideHeightFunction_T              RideHeightFunctionRequest;
   ChangeRequest2Bit_T               FerryFunctionSwitchChangeReq;
   InactiveActive_T                  InhibitWRCECSMenuCmd;
   DeviceIndication_T                FerryFunction_DeviceIndication;
   KneelingChangeRequest_T           KneelingChangeRequest;
   DeviceIndication_T                KneelDeviceIndication;
   FPBRChangeReq_T                   FPBRChangeReq;
   DeviceIndication_T                FPBR_DeviceIndication;
   LevelRequest_T                    LevelRequest;
   ECSStandByRequest_T               ECSStandByRequest;
   FalseTrue_T                       BlinkECSWiredLEDs;
} LevelControl_Out_StructType_T;

//=======================================================================================
// Public data declarations
//=======================================================================================

//=======================================================================================
// Public function prototypes
//=======================================================================================                       

//=======================================================================================
// End of file
//=======================================================================================
#endif 

