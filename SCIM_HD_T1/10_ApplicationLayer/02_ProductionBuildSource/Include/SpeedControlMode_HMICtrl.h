#ifdef SPEEDCONTROLMODE_HMICtrl_H
#error SPEEDCONTROLMODE_HMICtrl_H unexpected multi-inclusion
#else
#define SPEEDCONTROLMODE_HMICtrl_H

//=====================================================================================
// Included header files
//=====================================================================================


//=======================================================================================
// Private constants and enum definitions
//=======================================================================================


//=======================================================================================
// Private macros
//=======================================================================================


#ifndef CONST_ResetFlag
#define CONST_ResetFlag                (0U)
#endif 

#ifndef CONST_SetFlag
#define CONST_SetFlag                  (1U)
#endif 
                              
#ifndef Maximum_Memoryblocks
#define Maximum_Memoryblocks           (16U)       
#endif

#ifndef DefaultDriverMemroryIndex
#define DefaultDriverMemroryIndex      (0U)       
#endif

#ifndef XRSLStates_SpareValues1
#define XRSLStates_SpareValues1        (7U)       
#endif

#ifndef XRSLStates_SpareValues2
#define XRSLStates_SpareValues2        (8U)       
#endif

#ifndef XRSLStates_SpareValues3
#define XRSLStates_SpareValues3        (9U)       
#endif

#ifndef XRSLStates_SpareValues4
#define XRSLStates_SpareValues4        (10U)       
#endif

#ifndef XRSLStates_SpareValues5
#define XRSLStates_SpareValues5        (11U)       
#endif

#ifndef XRSLStates_SpareValues6
#define XRSLStates_SpareValues6        (12U)       
#endif

#ifndef XRSLStates_SpareValues7
#define XRSLStates_SpareValues7        (13U)       
#endif

#define SM_SCM_Off                     (0U)
#define SM_SCM_ACC1                    (1U)
#define SM_SCM_ACC2                    (2U)
#define SM_SCM_ACC3                    (3U)
#define SM_SCM_CC                      (4U)
#define SM_SCM_ASL                     (5U)
#define SM_SCM_Invalid                 (255U)

#define CONST_RunnableTimeBase         (20U)
#define CONST_NbOfTimers               (1U)
#define CONST_EndStopEventTimer        (0U)
#define CONST_EndStopEventTimeout      ((100U)/CONST_RunnableTimeBase)

//=======================================================================================
// Private type definitions
//=======================================================================================

typedef enum
{
   CONST_COUNTERCLOCKWISE = 1,
   CONST_CLOCKWISE        = 2
} SCMTurnType_Enum;

typedef struct
{
	uint8 newValue;
	uint8 currentValue;
} SCM_SM_States;

typedef struct
{
   FreeWheel_Status_T  newValue;
   FreeWheel_Status_T  currentValue;
} FreeWheel_Status;

typedef struct
{
   PushButtonStatus_T  newValue;
   PushButtonStatus_T  currentValue;
} PushButtonType;

typedef struct
{
   DriverMemory_rqst_T newValue;
   DriverMemory_rqst_T currentValue;
} DriverMemoryReq;

 // structure for inputs
typedef struct
{
   CCStates_T                CCStates;
   DriverMemoryReq           DriverMemory_rqst;
   PushButtonType            SpeedControlModeButtonStatus;
   FreeWheel_Status          SpeedControlModeWheelStatus;
   XRSLStates_T              XRSLStates;
   VehicleModeDistribution_T SwcActivation_IgnitionOn;
} SpeedControlMode_HMICtrl_in_StructType;

 //structure for outputs
typedef struct
{
   DeviceIndication_T           ACCOrCCIndication;
   DeviceIndication_T           ASLIndication;
   CCIM_ACC_T                   FWSelectedACCMode;
   FWSelectedSpeedControlMode_T FWSelectedSpeedControlMode;
   OffOn_T                      FWSpeedControlEndStopEvent;
} SpeedControlMode_HMICtrl_out_StructType;

//=======================================================================================
// Private data declarations
//=======================================================================================
// Note: MUST BE static
// Note: Shall not be used/avoided with AUTOSAR SWC.


//=======================================================================================
// Private function prototypes
//=======================================================================================
// Note : MUST BE static
static void Get_RteInDataRead_Common(SpeedControlMode_HMICtrl_in_StructType *pRteInData_Common);
static void Get_RteInDataWrite_Common(const SpeedControlMode_HMICtrl_out_StructType    *pRteOutData_Common);
static uint8 FreeWheelOperation(      SCM_SM_States      *pSpeedControlModeState,
                                const SCMTurnType_Enum   turntype);
static void SpeedControlMode_HMICtrl_StateMachine(SpeedControlMode_HMICtrl_in_StructType *pSpeedControlMode_in_data,
                                                  SCM_SM_States                          *pSpeedModeState,
                                                  uint16                                 *pTimers,
                                                  OffOn_T                                *pFWSpeedControlEndStopEvent);
static void SpeedControlMode_Restore_and_Store_Operation(const SpeedControlMode_HMICtrl_in_StructType *pSpeedControlMode_input_data,
                                                               SCM_SM_States                          *pSpeedControlState,
                                                         const uint8                  parSpeedCntrlMode[Maximum_Memoryblocks]);
static void SpeedControlMode_Store_Operation_OnSpeedModeChange(const DriverMemoryReq  *pVarDriverMem_rqst,
                                                               const SCM_SM_States    *pSpeedControlModeSM,
	                                                                 uint8            pSpeedControlMode[Maximum_Memoryblocks]);
static void SCM_Store_Operation_XRSLState_OnDriverMemoryRequestChange(const SpeedControlMode_HMICtrl_in_StructType *pSpeedCntrlMode_in_data,
                                                                      const DriverMemoryReq                        *pVarDriverMem_request,
                                                                      const SCM_SM_States                          *pSpeedControlSM,
	                                                                        uint8                                  parSpeedControlMode[Maximum_Memoryblocks]);
static void SCM_Store_Operation_OnDriverMemoryRequestChange(const DriverMemoryReq  *pVarDriverMemory_rqst,
                                                            const SCM_SM_States    *pSpeedModeSM,
	                                                              uint8            pSpeedCntrlMode[Maximum_Memoryblocks]);
static void SpeedControlMode_HMICtrl_OutSignalProcByStateMachine(      SpeedControlMode_HMICtrl_out_StructType *pSpeedControlMode_output_data,
                                                                 const SCM_SM_States                           *pSpeedCntrlModeSM);
static void SpeedControlMode_HMICtrl_CoreLogic(SpeedControlMode_HMICtrl_in_StructType  *pRteInData_Common,
                                               SCM_SM_States                           *pSpeedCntrlModeState,
                                               SpeedControlMode_HMICtrl_out_StructType *pSpeedControlMode_out_data,
	                                           uint8                                   parSpeedControlMode[Maximum_Memoryblocks],
	                                           uint16                                  *pTimer);
static uint8 counterclockwiselogic(      SCM_SM_States           *pSpeedModeSM,
                                   const uint8   MaxCCW_SpeedControlModeValue);
static uint8 clockwiselogic(      SCM_SM_States           *pSpeedControlModeSM,
                            const uint8   MaxCCW_SpeedControlModeValue);
static void fallbacklogic(SpeedControlMode_HMICtrl_out_StructType *pSpeedControlModel_output_data);

//=======================================================================================
// End of file
//=======================================================================================
#endif



