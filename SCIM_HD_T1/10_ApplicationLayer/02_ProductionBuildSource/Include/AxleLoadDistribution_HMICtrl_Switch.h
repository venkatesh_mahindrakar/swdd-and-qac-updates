#ifdef AXLELOADDISTRIBUTION_HMICTRL_SWITCH_H
#error AXLELOADDISTRIBUTION_HMICTRL_SWITCH_H unexpected multi-inclusion
#else
#define AXLELOADDISTRIBUTION_HMICTRL_SWITCH_H
//=====================================================================================
// Included header files
//=====================================================================================

//=====================================================================================
// Private constants and enum definitions
//=====================================================================================

//=====================================================================================
// Private macros
//=====================================================================================
#define CONST_RunnableTimeBase                          (20U)
#define CONST_RatioALD_TimePeriod                       ((uint16)(5000U/CONST_RunnableTimeBase))
#define CONST_LiftAxle1UpRequestACK_TimerPeriod         ((uint16)(2000U/CONST_RunnableTimeBase))
#define CONST_LiftAxle1UpRequestNOACK_TimerPeriod       ((uint16)(5000U/CONST_RunnableTimeBase))
#define CONST_LoadDistributionReqNOACK_TimerPeriod      ((uint16)(5000U/CONST_RunnableTimeBase))

//=====================================================================================
// Private type definitions
//=====================================================================================

//=====================================================================================
// Private data declarations
//=====================================================================================

//=====================================================================================
// Private function prototypes
//=====================================================================================

//=====================================================================================
// End of file
//=====================================================================================
#endif

