#ifdef SERVICEBRAKING_HMICTRL_H
#error SERVICEBRAKING_HMICTRL_H unexpected multi-inclusion
#else
#define SERVICEBRAKING_HMICTRL_H

//=====================================================================================
// Included header files
//=====================================================================================


//=======================================================================================
// Private constants and enum definitions
//=======================================================================================


//=======================================================================================
// Private macros
//=======================================================================================

// Defined parameters
#define CONST_RunnableTimebase                  (20U)
#define CONST_NbofTimers                        (2U)
#define CONST_SB_DisableTimer                   (0U)
#define CONST_SB_EnableTimer                    (1U)
#define CONST_SBR_InteractnHandlTmout           ((1000U) / CONST_RunnableTimebase)

#ifndef CONST_NonInitialize
#define CONST_NonInitialize                     (0U)
#endif

#ifndef CONST_Initialize
#define CONST_Initialize                        (1U)
#endif

//=======================================================================================
// Private type definitions
//=======================================================================================

typedef enum
{
   SM_HSAOff  = 0,
   SM_HSAOn   = 1
} ServiceBraking_States;

// Variables for the 'HSAByDefault Logic'
typedef struct
{
   ServiceBraking_States  is_HSA_Operational;
   uint8                  is_active_ByDefault;
} ServiceBraking_HMICtrl_StructType;

typedef struct
{
   PushButtonStatus_T current;
   PushButtonStatus_T previous;
} PushButtonType;

typedef struct
{
   PassiveActive_T current;
   PassiveActive_T previous;
} PassiveActiveType;

// Structure for inputs
typedef struct
{
   VehicleModeDistribution_T  SwcActivation_IgnitionOn;
} ServiceBraking_HMICtrl_in_StructType;

typedef struct
{
	PassiveActiveType   ASRHillHolderSwitch;
	PushButtonType      HillStartAidButtonStatus;
} SB_HMICtrl_HSAin_StructType;

typedef struct
{
   PushButtonStatus_T   ABSInhibitSwitchStatus;
} SB_HMICtrl_ABSin_StructType;

// Structure for outputs 
typedef struct
{
   InactiveActive_T     HSADriverRequest;
   DeviceIndication_T   HillStartAid_DeviceIndication;
} SB_HMICtrl_HSAout_StructType;

typedef struct
{
   OffOn_T  ABSInhibitionRequest;
} SB_HMICtrl_ABSout_StructType;

typedef struct
{
   ServiceBraking_States currentvalue;
   ServiceBraking_States newvalue;
} ServiceBraking_State;

//=======================================================================================
// Private data declarations
//=======================================================================================
// Note: MUST BE static
// Note: Shall not be used/avoided with AUTOSAR SWC.


//=======================================================================================
// Private function prototypes
//=======================================================================================
// Note : MUST BE static

// static function declarations
static void SB_HSADisabledByDefault(      SB_HMICtrl_HSAin_StructType          *pHSADisable_indata,
                                          ServiceBraking_HMICtrl_StructType    *pServiceBraking_HMICtrl_Disable_data,
                                    const Std_ReturnType                       *pASRreturnValue,
                                          ServiceBraking_State                 *pDisableStates,
                                          uint16                               *pTimers_SBDisable,
                                          SB_HMICtrl_HSAout_StructType         *pHSA_outputdata);
static void HSADisabledoutputprocessing(const ServiceBraking_State          *pDisableState,
                                              SB_HMICtrl_HSAout_StructType  *pHSA_outputdata);
static void SB_HSAEnabledByDefault(      SB_HMICtrl_HSAin_StructType         *pHSAEnable_indata,
                                         ServiceBraking_HMICtrl_StructType   *pServiceBraking_HMICtrl_Enable_data,
                                   const boolean                             HSArtrn,
                                   const Std_ReturnType                      *pASREnretValue,
                                         ServiceBraking_State                *pEnableStates,
                                         uint16                              *pTimers_SBEnable);
static void HSAEnableoutputprocessing(const ServiceBraking_State         *pEnableState,
                                            SB_HMICtrl_HSAout_StructType *pHSA_outdata);
static void ABS_InhibitLogic(const SB_HMICtrl_ABSin_StructType  *pABS_indata,
                                   SB_HMICtrl_ABSout_StructType *pABS_outdata);
static boolean Get_RteInDataRead_Common(ServiceBraking_HMICtrl_in_StructType  *pServiceBraking_HMICtrl_HSAIn_data,
                                        SB_HMICtrl_HSAin_StructType           *pHSA_indata,
                                        Std_ReturnType                        *pASRretValue);

//=======================================================================================
// End of file
//=======================================================================================
#endif

