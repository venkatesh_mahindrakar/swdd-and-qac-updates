#ifdef SWIVELSEAT_CTRL_H
#error SWIVELSEAT_CTRL_H unexpected multi-inclusion
#else
#define SWIVELSEAT_CTRL_H
//=====================================================================================
// Included header files
//=====================================================================================


//=======================================================================================
// Private constants and enum definitions
//=======================================================================================


//=======================================================================================
// Private macros
//=======================================================================================

#define MAX_NOTAVAILABLE        (65535U)
#define MAX_ERRORINDICATOR      (65279U)

#define CONST_WheelSpeed_Resl   ((float32)(0.00390625))


//=======================================================================================
// Private type definitions
//=======================================================================================

// Inputs structure 
typedef struct
{
   SeatSwivelStatus_T         SeatSwivelStatus;
   VehicleModeDistribution_T  SwcActivation_EngineRun;
   Speed16bit_T               WheelBasedVehicleSpeed;
} SwivelSeatCtrl_in_StructType;


//=======================================================================================
// Private data declarations
//=======================================================================================
// Note: MUST BE static
// Note: Shall not be used/avoided with AUTOSAR SWC.


//=======================================================================================
// Private function prototypes
//=======================================================================================
// Note : MUST BE static
static void Get_RteDataRead_Common(SwivelSeatCtrl_in_StructType *pInData_Common);
//=====================================================================================
// End of file
//=====================================================================================
#endif

 