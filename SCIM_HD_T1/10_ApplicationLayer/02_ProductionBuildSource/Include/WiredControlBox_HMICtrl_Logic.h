#ifdef WIREDCONTROLBOX_HMICtrl_LOGIC_H
#error WIREDCONTROLBOX_HMICtrl_LOGIC_H unexpected multi-inclusion
#else
#define WIREDCONTROLBOX_HMICtrl_LOGIC_H


//=====================================================================================
// Included header files
//=====================================================================================


//=====================================================================================
// Private macros
//=====================================================================================

//=====================================================================================
// Private constants and enum definitions
//=====================================================================================

#define CONST_sec2msec                   (1000U)
#define CONST_ButtonStucked              (1U)
#define CONST_AdjustButtonTimer          (1U)
#define CONST_BackButtonTimer            (2U)
#define CONST_SelectButtonTimer          (3U)
#define CONST_WRUpButtonTimer            (5U)
#define CONST_WRDownButtonTimer          (6U)

#ifndef CONST_Set
#define CONST_Set                        (2U)
#endif 

//=====================================================================================
// Private type definitions
//=====================================================================================
typedef enum
{
   Reset      = 0,
   set        = 1,
   Button_set = 2
 } ButtonSetReset;


//=====================================================================================
// Private data declarations
//=====================================================================================

//=====================================================================================
// Private function prototypes
//=====================================================================================
 static void Fullaircontrolboxselection_Outputlogic(const Wcb_Fullaircontrolbox_logic_Type       *pnewValue,
                                                          WiredControlBox_HMICtrl_out_StructType *pWiredControlBox_HMICtrl_out_data);
 static void ButtonStuckLogicForPushButtons(const PushButtonStatus *pinputButton,
                                                  uint16           *pTimer,
                                                  ButtonSetReset   *pStuck);
 static void ButtonStuckLogicForWrUpDownButton(const EvalButtonRequest *pinputButton,
                                                     uint16           *pTimer,
                                                     ButtonSetReset   *pStuck);

//=====================================================================================
// End of file
//=====================================================================================
#endif

