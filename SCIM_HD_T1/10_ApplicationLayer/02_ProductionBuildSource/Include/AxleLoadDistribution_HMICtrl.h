#ifdef AXLELOADDISTRIBUTION_HMICTRL_H
#error AXLELOADDISTRIBUTION_HMICTRL_H unexpected multi-inclusion
#else
#define AXLELOADDISTRIBUTION_HMICTRL_H
//=====================================================================================
// Included header files
//=====================================================================================

//=====================================================================================
// Private constants and enum definitions
//=====================================================================================
#define CONST_NbOfTimers       (33U)

typedef enum
{
   CONST_R1S_ChangeLoadDisTimerID  = 0,
   CONST_Ratio1SwitchBS_TimerID    = 1,
   CONST_R2S_ChangeLoadDisTimerID  = 2,
   CONST_Ratio2SwitchBS_TimerID    = 3,
   CONST_R3S_ChangeLoadDisTimerID  = 4,
   CONST_Ratio3SwitchBS_TimerID    = 5,
   CONST_R6S_ChangeLoadDisTimerID  = 6,
   CONST_Ratio6SwitchBS_TimerID    = 7,
   CONST_ALDSwitchBS_TimerID       = 8,
   CONST_TridemALDSwitchBSTimerID  = 9,
   CONST_RatioALDSwitchBS_TimerID  = 10,
   CONST_RatioALD_ACK_TimerID      = 11,
   CONST_LiftAxle1S2BS_TimerID     = 12,
   CONST_LA1UpReqACK_TimerID       = 13,
   CONST_LA2MaxTractTimerID        = 14,
   CONST_LA1UpRequestTimerID       = 15,
   CONST_LA1UpReq_NoACK_TimerID    = 16,
   CONST_LA2SwitchBS_TimerID       = 17,
   CONST_LA2S_UpBtnPrsAckTimerID   = 18,
   CONST_LA1SwitchBS_TimerID       = 19,
   CONST_LA1S_UpBtnPrsAckTimerID   = 20,
   CONST_LA2MTSwitchBS_TimerID     = 21,
   CONST_LA2MTS_UpBtnPrsAckTimerID = 22,
   CONST_LA1MTSwitchBS_TimerID     = 23,
   CONST_LA1MTS_UpBtnPrsAckTimerID = 24,
   CONST_LA1TSwitchBS_TimerID      = 25,
   CONST_LA1TS_UpBtnPrsAck_TimerID = 26,
   CONST_LA2TSwitchBS_TimerID      = 27,
   CONST_LA2TS_UpBtnPrsAckTimerID  = 28,
   CONST_R4S_LoadDisReqACKTimerID  = 29,
   CONST_Ratio4SwitchBS_TimerID    = 30,
   CONST_R5S_LDisReqACKTimerID     = 31,
   CONST_Ratio5SwitchBS_TimerID    = 32
} AxleLoadDistribution_Timer;

//=====================================================================================
// Private macros
//=====================================================================================


//=====================================================================================
// Private type definitions
//=====================================================================================
// Ratio Switch
typedef enum
{
   SM_ALD_RS_Default         = 0,
   SM_ALD_RS_Stand_By        = 1,
   SM_ALD_RS_Change_Load_Req = 2,
   SM_ALD_RS_Change_Ack      = 3,
   SM_ALD_RS_Fallback        = 4
} AldRatioSwitch_Enum;

typedef struct
{
   AldRatioSwitch_Enum newValue;
   AldRatioSwitch_Enum currentValue;
} AldRS_States;

// Lift Axle Switch
typedef enum
{
   SM_ALD_OLA_Default  = 0,
   SM_ALD_OLA_AutoIdle = 1,
   SM_ALD_OLA_Lift     = 2,
   SM_ALD_OLA_Down     = 3,
   SM_ALD_OLA_Fallback = 4
} AldOneLiftAxle_Enum;

typedef struct
{
   AldOneLiftAxle_Enum newValue;
   AldOneLiftAxle_Enum currentValue;
} SM_OneLiftAxleStates;


// structure for inputs
typedef struct
{
   A3PosSwitchStatus        LiftAxle2SwitchStatus;
   A3PosSwitchStatus        LiftAxle1SwitchStatus;
   A3PosSwitchStatus        LiftAxle2MaxTractSwitchStatus;
   A3PosSwitchStatus        LiftAxle1MaxTractSwitchStatus;
   A3PosSwitchStatus        LiftAxle1TRIDEMSwitchStatus;
   A3PosSwitchStatus        LiftAxle2TRIDEMSwitchStatus;
   A3PosSwitchStatus        LiftAxle1Switch2_Status;
   LiftAxleUpRequestACK     LiftAxle1UpRequestACK;
   LiftAxleUpRequestACK_T   LiftAxle2UpRequestACK;
   LiftAxlePositionStatus_T LiftAxle1PositionStatus;
} AxleLoadDistribution_LiftAxleInStruct;

typedef struct
{
   A2PosSwitchStatus              Ratio1SwitchStatus;
   A2PosSwitchStatus              Ratio2SwitchStatus;
   A2PosSwitchStatus              Ratio3SwitchStatus;
   A3PosSwitchStatus              Ratio4SwitchStatus;
   A3PosSwitchStatus              Ratio5SwitchStatus;
   A2PosSwitchStatus              Ratio6SwitchStatus;
   LoadDistributionRequestedACK_T LoadDistributionRequestedACK;
} AxleLoadDistribution_RatioSwitchInStruct;

typedef struct
{
   A3PosSwitchStatus              RatioALDSwitchStatus;
   LoadDistributionALDChangeACK_T LoadDistributionALDChangeACK;
   A2PosSwitchStatus              TridemALDSwitchStatus;
   A2PosSwitchStatus              ALDSwitchStatus;
} AxleLoadDistribution_ALDSwitchInStruct;

typedef struct 
{
   VehicleModeDistribution_T      SwcActivation_Living;
   LoadDistributionChangeACK      LoadDistributionChangeACK; 
   LoadDistributionSelect         LoadDistributionSelected; 
   LoadDistributionFuncSelected_T LoadDistributionFuncSelected; 
   ECSStandByRequest_T            ECSStandByRequest; 
} AxleLoadDistribution_HMICtrl_InStruct;


// structure for outputs
typedef struct
{
   LiftAxleLiftPositionRequest_T LiftAxle1LiftPositionRequest;
   LiftAxleLiftPositionRequest_T LiftAxle2LiftPositionRequest;
   InactiveActive_T              LiftAxle1AutoLiftRequest;
   InactiveActive_T              LiftAxle2AutoLiftRequest;
   DualDeviceIndication_T        BogieSwitch_DeviceIndication;
   DeviceIndication_T            MaxTract_DeviceIndication;
   LiftAxleLiftPositionRequest_T LiftAxle1DirectControl;
} AxleLoadDistribution_LiftAxleOutStruct;

typedef struct
{
   DeviceIndication_T              Ratio1_DeviceIndication;
   DeviceIndication_T              Ratio2_DeviceIndication;
   DualDeviceIndication_T          Ratio4_DeviceIndication;
   DualDeviceIndication_T          Ratio5_DeviceIndication;
   DeviceIndication_T              Ratio6_DeviceIndication;
   LoadDistributionRequested_T     LoadDistributionRequested;
   LoadDistributionChangeRequest_T LoadDistributionChangeRequest;
} AxleLoadDistribution_RatioSwitchOutStruct;

typedef struct
{
   DeviceIndication_T         ALD_DeviceIndication;
   AltLoadDistribution_rqst_T AltLoadDistribution_rqst;
   DualDeviceIndication_T     RatioALD_DualDeviceIndication;
   DeviceIndication_T         TridemALD_DeviceIndication;
} AxleLoadDistribution_ALD_SwitchOutStruct;

typedef struct
{
   uint8 initialize_LA2S;
   uint8 initialize_LA1S;
   uint8 initialize_LA2MTS;
   uint8 initialize_LA1MTS;
   uint8 initialize_LA1TS;
   uint8 initialize_LA2TS;
   uint8 initialize_R3S;
   uint8 initialize_R1S;
   uint8 initialize_R2S;
   uint8 initialize_R6S;
   uint8 initialize_LAPS;
   uint8 initialize_R4S;
   uint8 initialize_R5S;
   uint8 initialize_ARide;
   uint8 initialize_ALDS;
   uint8 initialize_TridemALDS;
   uint8 initialize_RatioALDS;
} ALD_HMICtrl_InitializeStatesStruct;

typedef struct
{
   uint8 LA2SButtonStuckFlag;
   uint8 LA1SButtonStuckFlag;
   uint8 LA2MTSButtonStuckFlag;
   uint8 LA1MTSButtonStuckFlag;
   uint8 LA1TSButtonStuckFlag;
   uint8 LA2TSButtonStuckFlag;
} ALD_OLA_ButtonStuckFlagStruct;

//=====================================================================================
// Private data declarations
//=====================================================================================

//=====================================================================================
// Private function prototypes
//=====================================================================================
static void Get_RteInDataRead_LiftAxle(AxleLoadDistribution_LiftAxleInStruct *pRteInData_LiftAxle);
static void Get_RteInDataRead_RatioSwitch(AxleLoadDistribution_RatioSwitchInStruct *pRteInData_RatioSwitch);
static void Get_RteInDataRead_ALD_Switch(AxleLoadDistribution_ALDSwitchInStruct *pRteInData_ALD_Switch);
static void Get_RteInDataRead_Common(AxleLoadDistribution_HMICtrl_InStruct *pRteInData_Common);
static void RteInDataWrite_LiftAxle(const AxleLoadDistribution_LiftAxleOutStruct *pRteOutData_LiftAxle);
static void RteInDataWrite_RatioSwitch(const AxleLoadDistribution_RatioSwitchOutStruct *pRteOutData_RatioSwitch);
static void RteInDataWrite_ALD_Switch(const AxleLoadDistribution_ALD_SwitchOutStruct *pRteOutData_ALD_Switch);
static void OneLiftAxleStateTransitions(      A3PosSwitchStatus             *pLiftAxleSwitchStatus,
                                        const LiftAxleUpRequestACK_T        *pLiftAxle1UpRequestACK,
                                              uint16                        *pTimer_ButtonStuck,
                                              uint16                        *pTimer_UpButtonPrsAcknowledge,
                                              uint8                         *pTimer_ButtonStuckFlag,
                                              uint8                         *pinitialize_OLA,
                                              SM_OneLiftAxleStates          *pOneLiftAxleSM,
                                              InactiveActive_T              *pLiftAxleAutoLiftRequest,
                                              LiftAxleLiftPositionRequest_T *pLiftAxleLiftPositionRequest);
static void OneLiftAxleStateOutputProcessing(const SM_OneLiftAxleStates          *pOneLiftAxleSM,
                                                   InactiveActive_T              *pLiftAxleAutoLiftRequest,
                                                   LiftAxleLiftPositionRequest_T *pLiftAxleLiftPositionRequest);
static void LiftAxleSwitchStatusError(const A3PosSwitchStatus             *pLiftAxle1SwitchStatus, 
                                      const A3PosSwitchStatus             *pLiftAxle2SwitchStatus,
                                      const A3PosSwitchStatus             *pLiftAxle1TRIDEMSwitchStatus,
                                      const A3PosSwitchStatus             *pLiftAxle2TRIDEMSwitchStatus,
                                      const A3PosSwitchStatus             *pLiftAxle2MaxTractSwitchStatus,
                                      const A3PosSwitchStatus             *pLiftAxle1MaxTractSwitchStatus,
                                            LiftAxleLiftPositionRequest_T *pLiftAxle1LiftPositionReq,
                                            InactiveActive_T              *pLiftAxle1AutoLiftReq);
static void RatioSwitchStateTransitions(      A2PosSwitchStatus               *pRatioSwitchStatus,
                                        const LoadDistributionChangeACK_T     *pLoadDistributionChangeACK,
                                              uint16                          *pTimer_RatioSwitchButtonStuck,
                                              uint16                          *pTimer_ChangeLoadDistribution,
                                              uint8                           *pinitialize_RS,
                                              AldRS_States                    *pRatioSwitchSM,
                                              LoadDistributionChangeRequest_T *pLoadDistributionChangeRequest);
static void RatioSwitchStateOutputProcessing(const AldRS_States                    *pRatioSwitchSM,
                                                   LoadDistributionChangeRequest_T *pLoadDistributionChangeRequest);
static void DeviceIndication(const LoadDistributionFuncSelected_T *pLoadDistributionFuncSelected,
                                   DeviceIndication_T             *pDeviceIndication);
static void UD_DeviceIndication(const LoadDistributionFuncSelected_T *pUD_LoadDistributionFuncSelected,
                                      DeviceIndication_T             *pUD_DeviceIndication);
static uint8 RatioSwitchConfigurationErrorDetection(void);
static uint8 OneLiftAxleConfigurationErrorDetection(void);
static void OneLiftAxleStateMachine(      AxleLoadDistribution_LiftAxleInStruct  *pRteInData_LiftAxle,
                                    const LoadDistributionFuncSelected_T         *pLoadDistributionFuncSelected,
                                          uint16                                 OLA_Timers[CONST_NbOfTimers],
                                          ALD_OLA_ButtonStuckFlagStruct          *pOLA_ButtonStuckFlag,
                                          ALD_HMICtrl_InitializeStatesStruct     *pALD_InitializeStates,
                                          AxleLoadDistribution_LiftAxleOutStruct *pRteOutData_LiftAxle);
static void LiftAxlePositionswitchStateMachine(      A3PosSwitchStatus                       *pLiftAxle1Switch2_Status,
                                               const LiftAxleUpRequestACK                    *pLiftAxle1UpRequestACK,
                                               const LiftAxlePositionStatus_T                *pLiftAxle1PositionStatus,
                                                     uint16                                  LAP_Timers[CONST_NbOfTimers],
                                                     ALD_HMICtrl_InitializeStatesStruct      *pALD_InitializeStates,
                                                     LiftAxleLiftPositionRequest_T           *pLiftAxle1LiftPositionRequest,
                                                     DualDeviceIndication_T                  *pBogieSwitch_DeviceIndication);
static void DualRatioSwitchStateMachine(      A3PosSwitchStatus                     *pRatio4SwitchStatus,
                                              A3PosSwitchStatus                     *pRatio5SwitchStatus,
                                        const LoadDistributionRequestedACK_T        *pLoadDistributionRequestedACK,
                                        const LoadDistributionSelect                *pLoadDistributionSelected,
                                              uint16                                DRS_Timers[CONST_NbOfTimers],
                                              ALD_HMICtrl_InitializeStatesStruct    *pALD_InitializeStates,
                                              DualDeviceIndication_T                *pRatio4_DeviceIndication,
                                              DualDeviceIndication_T                *pRatio5_DeviceIndication,
                                              LoadDistributionRequested_T           *pLoadDistributionRequested);
static void RatioSwitchStateMachine(      AxleLoadDistribution_RatioSwitchInStruct  *pRteInData_RatioSwitch,
                                    const LoadDistributionChangeACK_T               *pLoadDistributionChangeACK,
                                    const LoadDistributionFuncSelected_T            *pLoadDistributionFuncSelected,
                                          uint16                                    RS_Timers[CONST_NbOfTimers],
                                          ALD_HMICtrl_InitializeStatesStruct        *pALD_InitializeStates,
                                          AxleLoadDistribution_RatioSwitchOutStruct *pRteOutData_RatioSwitch);
static void ARideStateMachine(      A3PosSwitchStatus                     *pLiftAxle2MaxTractSwitchStatus,
                                    LiftAxleUpRequestACK                  *pLiftAxle1UpRequestACK,
                              const LoadDistributionSelect                *pLoadDistributionSelected,
                                    uint16                                ARide_Timers[CONST_NbOfTimers],
                                    ALD_HMICtrl_InitializeStatesStruct    *pALD_InitializeStates,
                                    LiftAxleLiftPositionRequest_T         *pLiftAxle1LiftPositionRequest,
                                    LiftAxleLiftPositionRequest_T         *pLiftAxle1DirectControl,
                                    DeviceIndication_T                    *pMaxTract_DeviceIndication);
static void ALDSwitchStateMachine(      A2PosSwitchStatus                       *pALDSwitchStatus,
                                        A2PosSwitchStatus                       *pTridemALDSwitchStatus,
                                  const LoadDistributionALDChangeACK_T          *pLoadDistributionALDChangeACK,
                                  const LoadDistributionSelect                  *pLoadDistributionSelected,
                                        uint16                                  ALD_Timers[CONST_NbOfTimers],
                                        ALD_HMICtrl_InitializeStatesStruct      *pALD_InitializeStates,
                                        DeviceIndication_T                      *pALD_DeviceIndication,
                                        DeviceIndication_T                      *pTridemALD_DeviceIndication,
                                        AltLoadDistribution_rqst_T              *pAltLoadDistribution_rqst);
static void RatioALDSwitchStateMachine(      A3PosSwitchStatus                        *pRatioALDSwitchStatus,
                                       const LoadDistributionALDChangeACK_T           *pLoadDistributionALDChangeACK,
                                       const AxleLoadDistribution_HMICtrl_InStruct    *pRteInData_Common,
                                             uint16                                   RatioALD_Timers[CONST_NbOfTimers],
                                             ALD_HMICtrl_InitializeStatesStruct       *pALD_InitializeStates,
                                             LoadDistributionChangeRequest_T          *pLoadDistributionChangeRequest,
                                             AltLoadDistribution_rqst_T               *pAltLoadDistribution_rqst,
                                             DualDeviceIndication_T                   *pRatioALD_DualDeviceIndication);

//=====================================================================================
// End of file
//=====================================================================================
#endif

