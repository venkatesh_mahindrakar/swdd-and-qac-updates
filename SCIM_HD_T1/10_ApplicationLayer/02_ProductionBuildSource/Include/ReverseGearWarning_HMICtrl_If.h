//!======================================================================================
//! \file ReverseGearWarning_HMICtrl_If.h
//!
//! @addtogroup Library
//! @{
//! @addtogroup ReverseGearWarning_HMICtrl_If
//! @{
//!
//! \brief
//! ReverseGearWarning_HMICtrl_If SWC.
//! ASIL Level : QM.
//! This module implements the logic for ReverseGearWarning_HMICtrl_If.
//!======================================================================================

#ifndef ReverseGearWarning_HMICtrl_If_H
// To protect against multi-inclusion : VehicleAccess_Ctrl_If_H must be unique
#define ReverseGearWarning_HMICtrl_If_H
//=======================================================================================
// Public type definitions
//=======================================================================================

//=======================================================================================
// Public constants and enums definitions
//=======================================================================================

//=======================================================================================
// Public macros
//=======================================================================================
// Note, coding convention: shall be avoided to support debug capability.

# endif                            

//! @}
//! @}

//=======================================================================================
// End of file
//=======================================================================================