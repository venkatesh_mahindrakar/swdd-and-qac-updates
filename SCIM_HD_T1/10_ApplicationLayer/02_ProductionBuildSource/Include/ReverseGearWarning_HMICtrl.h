#ifdef REVERSEGEARWARNING_HMICTRL_H
#error REVERSEGEARWARNING_HMICTRL_H unexpected multi-inclusion
#else
#define REVERSEGEARWARNING_HMICTRL_H

//=====================================================================================
// Included header files
//=====================================================================================

//=======================================================================================
// Private constants and enum definitions
//=======================================================================================
typedef enum
{
   SM_RGW_Request_SoundOff = 0,
   SM_RGW_Request_SoundOn  = 1,
   SM_RGW_Request_Error    = 2
} ReverseGearWarning_States;

//=======================================================================================
// Private macros
//=======================================================================================

#ifndef CONST_NonInitialize
#define CONST_NonInitialize                 (0U)
#endif

#ifndef CONST_Initialize
#define CONST_Initialize                    (1U)
#endif

#define MemoryBlocks                        (4U)
#define CONST_NvramReverseGearValueIndex    (1U)
#define CONST_NvramReverseGearVersion       (1U)


//=======================================================================================
// Private type definitions
//=======================================================================================

typedef struct
{
   PushButtonStatus_T prevValue;
   PushButtonStatus_T newValue;
} PushButtonStatus;

// Structure for inputs
typedef struct 
{
   PushButtonStatus          ReverseGearWarningBtn_stat;
   A2PosSwitchStatus_T       ReverseGearWarningSw_stat;
   VehicleModeDistribution_T SwcActivation_IgnitionOn;
} ReverseGearWarning_HMICtrl_InStruct;

// Structure for outputs
typedef struct 
{
   ReverseWarning_rqst_T ReverseWarning_rqst;
   DeviceIndication_T    ReverseWarningInd_cmd;
} ReverseGearWarning_HMICtrl_OutStruct;

//=======================================================================================
// Private data declarations
//=======================================================================================
// Note: MUST BE static
// Note: Shall not be used/avoided with AUTOSAR SWC.

//=======================================================================================
// Private function prototypes
//=======================================================================================
// Note : MUST BE static

static void Get_RteInDataRead_Common(ReverseGearWarning_HMICtrl_InStruct *pRteInData_Common);
static void RteOutDataWrite_Common(const ReverseGearWarning_HMICtrl_OutStruct *pRteOutData_common);
static void ReverseGearWarning_VT_Configuration(const A2PosSwitchStatus_T   *pReverseGearWarningSw_stat,
                                                      ReverseWarning_rqst_T *pReverseWarning_rqst);
static void ReverseGearWarning_RT_Configuration(PushButtonStatus           *pReverseGearWarningBtn_stat,
                                                uint8                      *pRGWInitializeStates,
                                                ReverseGearWarning_States  *pReverseGearWarningSM);
static void ReverseGearWarningStateOutputProcessing(const ReverseGearWarning_States            *pReverseGearWarningSM,
                                                          ReverseGearWarning_HMICtrl_OutStruct *pRGW_OutData);

//=======================================================================================
// End of file
//=======================================================================================
#endif
