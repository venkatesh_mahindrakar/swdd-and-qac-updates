#ifdef WIREDCONTROLBOX_HMICtrl_IF_H
#error WIREDCONTROLBOX_HMICtrl_IF_H unexpected multi-inclusion
#else
#define WIREDCONTROLBOX_HMICtrl_IF_H

//=====================================================================================
// Included header files
//=====================================================================================

//=======================================================================================
// Public type definitions
//=======================================================================================

#define CONST_TimeBase                          (20U)
#define CONST_StopButtonTimer                   (0U)
#define CONST_MemButtonTimer                    (4U)
#define CONST_NbOfTimers                        (12U)

#ifndef CONST_Resetflag
#define CONST_Resetflag                         (0U)
#endif 

#ifndef CONST_Setflag
#define CONST_Setflag                           (1U)
#endif 

#ifndef CONST_Noninitialized
#define CONST_Noninitialized                    (0U)
#endif

#ifndef CONST_Initialized
#define CONST_Initialized                       (1U)
#endif

typedef uint8 PushButtonStatus_current;

typedef enum
{
   SM_WCB_FullAirControlBox_Off          = 0,
   SM_WCB_FullAirControlBox_FrontorM1    = 1,
   SM_WCB_FullAirControlBox_ParallelorM2 = 2,
   SM_WCB_FullAirControlBox_RearorM3     = 3
} Wcb_Fullaircontrolbox_logic_Type;

typedef struct
{
   Wcb_Fullaircontrolbox_logic_Type newValue;
   Wcb_Fullaircontrolbox_logic_Type currentvalue;
} Wcb_Fullaircontrolbox_logic;

typedef struct
{
   PushButtonStatus_T previousValue;
   PushButtonStatus_T currentvalue;
} PushButtonStatus;

typedef struct
{
   EvalButtonRequest_T previousValue;
   EvalButtonRequest_T currentvalue;
} EvalButtonRequest;

typedef struct
{
   FalseTrue_T previousValue;
   FalseTrue_T currentvalue;
} FalseTrue;

typedef struct
{
   RampLevelRequest_T previousValue;
   RampLevelRequest_T currentvalue;
} RampLevelRequest;

typedef struct
{
   VehicleMode_T previousValue;
   VehicleMode_T currentvalue;
} VehicleModeType;

typedef struct
{
   uint8 FSM_Initialization;
   uint8 Adjust_Initialization;
   uint8 Fullaircontrolbox_Initialization;
} StateInitializationType;
typedef struct
{
   PushButtonStatus          SelectButtonStatus;
   FalseTrue_T               BlinkECSWiredLEDs;
   FalseTrue                 HeightAdjustmentAllowed;
   LevelControlInformation_T LevelControlInformation;
   RampLevelRequest          RampLevelRequest;
   VehicleModeType           VehicleMode;
   VehicleModeDistribution_T SwcActivation_Living;
} WiredControlBox_HMICtrl_in_StructType;

typedef struct
{
   PushButtonStatus  AdjustButtonStatus;
   PushButtonStatus  BackButtonStatus;
   PushButtonStatus  MemButtonStatus;
   PushButtonStatus  StopButtonStatus;
   EvalButtonRequest WRDownButtonStatus;
   EvalButtonRequest WRUpButtonStatus;
} WiredControlBox_HMICtrl_in_ButtonStructType;

// structure for outputs 
typedef struct
{
   DeviceIndication_T       Adjust_DeviceIndication;
   DeviceIndication_T       Down_DeviceIndication;
   ECSStandByReq_T          ECSStandByReqRCECS;
   DeviceIndication_T       M1_DeviceIndication;
   DeviceIndication_T       M2_DeviceIndication;
   DeviceIndication_T       M3_DeviceIndication;
   ShortPulseMaxLength_T    ShortPulseMaxLength;
   DeviceIndication_T       Up_DeviceIndication;
   FalseTrue_T              WiredAirSuspensionStopRequest;
   LevelAdjustmentAction_T  WiredLevelAdjustmentAction;
   LevelAdjustmentAxles_T   WiredLevelAdjustmentAxles;
   LevelAdjustmentStroke_T  WiredLevelAdjustmentStroke;
   WiredLevelUserMemory_T   WiredLevelUserMemory;
   LevelUserMemoryAction_T  WiredLevelUserMemoryAction;
} WiredControlBox_HMICtrl_out_StructType;

// enum for levelAdjustment 
typedef enum
{
   SM_WCB_LA_Init                = 0,
   SM_WCB_LA_Idle                = 1,
   SM_WCB_LA_Up_Auto             = 2,
   SM_WCB_LA_Up_Basic            = 3,
   SM_WCB_LA_Evaluating_Up       = 4,
   SM_WCB_LA_Down_Auto           = 5,
   SM_WCB_LA_Down_Basic          = 6,
   SM_WCB_LA_Evaluating_Down     = 7,
   SM_WCB_LA_Up_Short_Movement   = 8,
   SM_WCB_LA_Down_Short_Movement = 9,
   SM_WCB_LA_GotoDrive           = 10,
   SM_WCB_LA_Ferry               = 11
} Wcb_LevelAdjustment_FSM_Type; // e_LevelAdjustment; 

typedef struct
{
   Wcb_LevelAdjustment_FSM_Type newValue;
   Wcb_LevelAdjustment_FSM_Type currentvalue;
} Wcb_LevelAdjustment_FSM;
//=======================================================================================
// Public constants and enums definitions
//=======================================================================================

//=======================================================================================
// Public macros
//=======================================================================================

//=======================================================================================
// End of file
//=======================================================================================
# endif

