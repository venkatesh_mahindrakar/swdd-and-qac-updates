/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  DoubleSwitch_HMICtrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  DoubleSwitch_HMICtrl
 *  Generated at:  Fri Jun 12 17:10:00 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <DoubleSwitch_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file DoubleSwitch_HMICtrl.c
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_BodyAndComfort 
//! @{
//! @addtogroup DoubleSwitch_HMICtrl
//! @{
//!
//! \brief
//! DoubleSwitch_HMICtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the DoubleSwitch_HMICtrl runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#define DISABLE_RTE_PTR2ARRAYBASETYPE_PASSING
#include "Rte_DoubleSwitch_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "DoubleSwitch_HMICtrl.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Enumeration Types:
 * ==================
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


#define DoubleSwitch_HMICtrl_START_SEC_CODE
#include "DoubleSwitch_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DoubleSwitch_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_RoofHatch_SwitchStatus_1_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_RoofHatch_SwitchStatus_2_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_SwitchStatus_combined_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DoubleSwitch_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DoubleSwitch_HMICtrl_CODE) DoubleSwitch_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DoubleSwitch_HMICtrl_20ms_runnable
 *********************************************************************************************************************/

    //! ###### Definition of the SWC In/Out data structures
    // Definition of the SwcActivation
    static SwcActivation_In_StructType SwcActivation_DS;
    // Definition of the input port
    static DoubleSwitch_In_StructType RteInData_DS;
    // Definition of the output port
    static DoubleSwitch_Out_StructType RteOutData_DS;
    // Definition of state machine
    static enum DS_SM_Enum StatusStatemachine_DS = SM_DS_Status_Middle;

    //! ###### Software component Activation
    //! ##### Read Swc Activation signal : SwcActivation_DoubleSwitch()
    SwcActivation_DoubleSwitch(&SwcActivation_DS);

    //! ###### Common input logic processing
    //! ##### Process logic in operational vehicle mode
    if (Operational == SwcActivation_DS.Current_state)
    {
        //! #### Read the RTE input ports: ReadInData_DoubleSwitch()
        ReadInData_DoubleSwitch(&RteInData_DS);
        //! #### Process input logic for DoubleSwitch : Ds_InputLogic()
        RteOutData_DS.SwitchStatus_combined = Ds_InputLogic(&RteInData_DS, &StatusStatemachine_DS);
    }
    //! ##### Process Disable logic if non active Vehicle Mode
    else
    {
        RteOutData_DS.SwitchStatus_combined = A3PosSwitchStatus_Error;
    }
    //! ###### Save previous values
    RteInData_DS.RoofHatch_SwitchStatus_1.Previous_state = RteInData_DS.RoofHatch_SwitchStatus_1.Current_state;
    RteInData_DS.RoofHatch_SwitchStatus_2.Previous_state = RteInData_DS.RoofHatch_SwitchStatus_2.Current_state;

    //! ###### Common outputs processing
    //! ##### Write the RTE output ports
    (void)Rte_Write_SwitchStatus_combined_A3PosSwitchStatus(RteOutData_DS.SwitchStatus_combined);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define DoubleSwitch_HMICtrl_STOP_SEC_CODE
#include "DoubleSwitch_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//!
//! \brief
//! This function read data RTE for the swc activation signal
//!
//! \param *pRteInData_swcactivation RTE input data for SwcActivation structure to fill with data
//!
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void SwcActivation_DoubleSwitch(SwcActivation_In_StructType *pRteInData_swcactivation)
{
    if (RTE_E_OK != Rte_Read_SwcActivation_Living_Living(&pRteInData_swcactivation->Current_state))
    {
        pRteInData_swcactivation->Current_state = NonOperational;
    }
    else
    {
        // RTE_E_OK : do nothing
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//!
//! \brief
//! This function read data RTE for input port of DoubleSwitch
//!
//! \param *pRteInData_DS RTE input data for data structure to fill with data
//!
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ReadInData_DoubleSwitch(DoubleSwitch_In_StructType *pRteInData_DS)
{
    if (RTE_E_OK != Rte_Read_RoofHatch_SwitchStatus_1_A3PosSwitchStatus(&pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state))
    {
        pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state = NonOperational;
    }
    else
    {
        // RTE_E_OK : do nothing
    }
    if (RTE_E_OK != Rte_Read_RoofHatch_SwitchStatus_2_A3PosSwitchStatus(&pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state))
    {
        pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state = NonOperational;
    }
    else
    {
        // RTE_E_OK : do nothing
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//!
//! \brief
//!  This function processing the input logic
//!
//! \param *pRteInData_DS   Check the values of the input ports
//! \param *pDS_SM          Used to select the state
//!
//! \return A3PosSwitchStatus_T
//!
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

A3PosSwitchStatus_T Ds_InputLogic(DoubleSwitch_In_StructType *pRteInData_DS, enum DS_SM_Enum *pDS_SM)
{
    A3PosSwitchStatus_T SwitchStatus_combined = A3PosSwitchStatus_Middle;
    // TRANSITION
    switch (*pDS_SM)
    {
    case SM_DS_Status_Upper :
       // ChangeTo_Middle
       if (((pRteInData_DS->RoofHatch_SwitchStatus_1.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state == A3PosSwitchStatus_Middle))
          ||
          ((pRteInData_DS->RoofHatch_SwitchStatus_2.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state == A3PosSwitchStatus_Middle)))
       {
          *pDS_SM = SM_DS_Status_Middle;
       }
       else
       {
          // do nothing
       }
        // ChangeTo_Lower
        if (((pRteInData_DS->RoofHatch_SwitchStatus_1.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state == A3PosSwitchStatus_Lower)) ||
            ((pRteInData_DS->RoofHatch_SwitchStatus_2.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state == A3PosSwitchStatus_Lower)))
        {
            *pDS_SM = SM_DS_Status_Middle;
        }
        else
        {
            // do nothing
        }
        // BothFaulty
        if (((pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state == A3PosSwitchStatus_Error) || (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state == A3PosSwitchStatus_NotAvailable)) &&
            ((pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state == A3PosSwitchStatus_Error) || (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state == A3PosSwitchStatus_NotAvailable)))
        {
            *pDS_SM = SM_DS_Status_Failure;
        }
        else
        {
            // do nothing
        }
    break;
    case SM_DS_Status_Lower :
       // ChangeTo_Middle
       if (((pRteInData_DS->RoofHatch_SwitchStatus_1.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state == A3PosSwitchStatus_Middle))
          ||
          ((pRteInData_DS->RoofHatch_SwitchStatus_2.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state == A3PosSwitchStatus_Middle)))
       {
          *pDS_SM = SM_DS_Status_Middle;
       }
       else
       {
          // do nothing
       }
        // ChangeTo_Upper
        if (((pRteInData_DS->RoofHatch_SwitchStatus_1.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state == A3PosSwitchStatus_Upper)) ||
            ((pRteInData_DS->RoofHatch_SwitchStatus_2.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state == A3PosSwitchStatus_Upper)))
        {
            *pDS_SM = SM_DS_Status_Middle;
        }
        else
        {
            // do nothing
        }
        // BothFaulty
        if (((pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state == A3PosSwitchStatus_Error) || (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state == A3PosSwitchStatus_NotAvailable)) &&
            ((pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state == A3PosSwitchStatus_Error) || (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state == A3PosSwitchStatus_NotAvailable)))
        {
            *pDS_SM = SM_DS_Status_Failure;
        }
        else
        {
            // do nothing
        }
    break;
    case SM_DS_Status_Failure :
        // ChangeTo_Middle
        if (((pRteInData_DS->RoofHatch_SwitchStatus_1.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state == A3PosSwitchStatus_Middle))
            ||
            ((pRteInData_DS->RoofHatch_SwitchStatus_2.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state == A3PosSwitchStatus_Middle)))
        {
            *pDS_SM = SM_DS_Status_Middle;
        }
        else
        {
            // do nothing
        }
        // ChangeTo_Upper
        if (((pRteInData_DS->RoofHatch_SwitchStatus_1.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state == A3PosSwitchStatus_Upper)) ||
            ((pRteInData_DS->RoofHatch_SwitchStatus_2.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state == A3PosSwitchStatus_Upper)))
        {
            *pDS_SM = SM_DS_Status_Upper;
        }
        else
        {
            // do nothing
        }
        // ChangeTo_Lower
        if (((pRteInData_DS->RoofHatch_SwitchStatus_1.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state == A3PosSwitchStatus_Lower)) ||
            ((pRteInData_DS->RoofHatch_SwitchStatus_2.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state == A3PosSwitchStatus_Lower)))
        {
            *pDS_SM = SM_DS_Status_Lower;
        }
        else
        {
            // do nothing
        }
    break;
    default: //case SM_DS_Status_Middle:
        // ChangeTo_Upper
       if (((pRteInData_DS->RoofHatch_SwitchStatus_1.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state == A3PosSwitchStatus_Upper)) ||
          ((pRteInData_DS->RoofHatch_SwitchStatus_2.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state == A3PosSwitchStatus_Upper)))
       {
          *pDS_SM = SM_DS_Status_Upper;
       }
       else
       {
          // do nothing
       }
       // ChangeTo_Lower
       if (((pRteInData_DS->RoofHatch_SwitchStatus_1.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state == A3PosSwitchStatus_Lower)) ||
          ((pRteInData_DS->RoofHatch_SwitchStatus_2.Previous_state) != (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state) && (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state == A3PosSwitchStatus_Lower)))
       {
          *pDS_SM = SM_DS_Status_Lower;
       }
       else
       {
          // do nothing
       }
       // BothFaulty
       if (((pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state == A3PosSwitchStatus_Error) || (pRteInData_DS->RoofHatch_SwitchStatus_1.Current_state == A3PosSwitchStatus_NotAvailable)) &&
          ((pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state == A3PosSwitchStatus_Error) || (pRteInData_DS->RoofHatch_SwitchStatus_2.Current_state == A3PosSwitchStatus_NotAvailable)))
       {
          *pDS_SM = SM_DS_Status_Failure;
       }
       else
       {
          // do nothing
       }
    break;
    }

    // ACTIONS
    switch (*pDS_SM)
    {
        case SM_DS_Status_Upper:
            SwitchStatus_combined = A3PosSwitchStatus_Upper;
        break;
        case SM_DS_Status_Lower:
            SwitchStatus_combined = A3PosSwitchStatus_Lower;
        break;
        case SM_DS_Status_Failure:
            SwitchStatus_combined = A3PosSwitchStatus_Error;
        break;
        default: //case SM_DS_Status_Middle:
           SwitchStatus_combined = A3PosSwitchStatus_Middle;
        break;
    }

    return SwitchStatus_combined;
}

//! @}
//! @}
//! @}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
