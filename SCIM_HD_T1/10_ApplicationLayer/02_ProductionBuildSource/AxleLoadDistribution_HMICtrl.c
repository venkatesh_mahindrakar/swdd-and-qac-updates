/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  AxleLoadDistribution_HMICtrl.c
 *           Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  AxleLoadDistribution_HMICtrl
 *  Generation Time:  2020-08-25 16:24:49
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <AxleLoadDistribution_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file AxleLoadDistribution_HMICtrl.c
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_VehicleDynamics_Suspension 
//! @{
//! @addtogroup AxleLoadDistribution_HMICtrl
//! @{
//!
//! \brief
//! AxleLoadDistribution_HMICtrl SWC.
//! ASIL Level : QM.
//! This module implements the logic for the AxleLoadDistribution_HMICtrl runnables.
//!======================================================================================


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_AxleLoadDistribution_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "AxleLoadDistribution_HMICtrl_If.h"
#include "AxleLoadDistribution_HMICtrl.h"
#include "FuncLibrary_Timer_If.h"
#include "AxleLoadDistribution_HMICtrl_Switch_If.h"
#include "FuncLibrary_ScimStd_If.h"
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * AltLoadDistribution_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   AltLoadDistribution_rqst_Idle (0U)
 *   AltLoadDistribution_rqst_ALDOn (1U)
 *   AltLoadDistribution_rqst_ALDOff (2U)
 *   AltLoadDistribution_rqst_Reserved_1 (3U)
 *   AltLoadDistribution_rqst_Reserved_2 (4U)
 *   AltLoadDistribution_rqst_Reserved_3 (5U)
 *   AltLoadDistribution_rqst_Error (6U)
 *   AltLoadDistribution_rqst_NotAvailable (7U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DualDeviceIndication_T: Enumeration of integer in interval [0...15] with enumerators
 *   DualDeviceIndication_UpperOffLowerOff (0U)
 *   DualDeviceIndication_UpperOnLowerOff (1U)
 *   DualDeviceIndication_UpperBlinkLowerOff (2U)
 *   DualDeviceIndication_UpperDontCareLowerOff (3U)
 *   DualDeviceIndication_UpperOffLowerOn (4U)
 *   DualDeviceIndication_UpperOnLowerOn (5U)
 *   DualDeviceIndication_UpperBlinkLowerOn (6U)
 *   DualDeviceIndication_UpperDontCareLowerOn (7U)
 *   DualDeviceIndication_UpperOffLowerBlink (8U)
 *   DualDeviceIndication_UpperOnLowerBlink (9U)
 *   DualDeviceIndication_UpperBlinkLowerBlink (10U)
 *   DualDeviceIndication_UpperDontCareLowerBlink (11U)
 *   DualDeviceIndication_UpperOffLowerDontCare (12U)
 *   DualDeviceIndication_UpperOnLowerDontCare (13U)
 *   DualDeviceIndication_UpperBlinkLowerDontCare (14U)
 *   DualDeviceIndication_UpperDontCareLowerDontCare (15U)
 * ECSStandByRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   ECSStandByRequest_NoRequest (0U)
 *   ECSStandByRequest_Initiate (1U)
 *   ECSStandByRequest_StandbyRequestedRCECS (2U)
 *   ECSStandByRequest_StandbyRequestedWRC (3U)
 *   ECSStandByRequest_Reserved (4U)
 *   ECSStandByRequest_Reserved_01 (5U)
 *   ECSStandByRequest_Error (6U)
 *   ECSStandByRequest_NotAvailable (7U)
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   InactiveActive_Inactive (0U)
 *   InactiveActive_Active (1U)
 *   InactiveActive_Error (2U)
 *   InactiveActive_NotAvailable (3U)
 * LiftAxleLiftPositionRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   LiftAxleLiftPositionRequest_Idle (0U)
 *   LiftAxleLiftPositionRequest_Down (1U)
 *   LiftAxleLiftPositionRequest_Up (2U)
 *   LiftAxleLiftPositionRequest_Reserved (3U)
 *   LiftAxleLiftPositionRequest_Reserved_01 (4U)
 *   LiftAxleLiftPositionRequest_Reserved_02 (5U)
 *   LiftAxleLiftPositionRequest_Error (6U)
 *   LiftAxleLiftPositionRequest_NotAvailable (7U)
 * LiftAxlePositionStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   LiftAxlePositionStatus_Lowered (0U)
 *   LiftAxlePositionStatus_Lifted (1U)
 *   LiftAxlePositionStatus_Lowering (2U)
 *   LiftAxlePositionStatus_Lifting (3U)
 *   LiftAxlePositionStatus_Reserved (4U)
 *   LiftAxlePositionStatus_Reserved_01 (5U)
 *   LiftAxlePositionStatus_Error (6U)
 *   LiftAxlePositionStatus_NotAvailable (7U)
 * LiftAxleUpRequestACK_T: Enumeration of integer in interval [0...7] with enumerators
 *   LiftAxleUpRequestACK_NoAction (0U)
 *   LiftAxleUpRequestACK_ChangeAcknowledged (1U)
 *   LiftAxleUpRequestACK_LiftDeniedFrontOverload (2U)
 *   LiftAxleUpRequestACK_LiftDeniedRearOverload (3U)
 *   LiftAxleUpRequestACK_LiftDeniedPBrakeActive (4U)
 *   LiftAxleUpRequestACK_SystemDeniedVersatile (5U)
 *   LiftAxleUpRequestACK_Error (6U)
 *   LiftAxleUpRequestACK_NotAvailable (7U)
 * LoadDistributionALDChangeACK_T: Enumeration of integer in interval [0...7] with enumerators
 *   LoadDistributionALDChangeACK_NoAction (0U)
 *   LoadDistributionALDChangeACK_ChangeAcknowledged (1U)
 *   LoadDistributionALDChangeACK_LoadShiftDeniedOverspeed (2U)
 *   LoadDistributionALDChangeACK_Spare (3U)
 *   LoadDistributionALDChangeACK_Spare_01 (4U)
 *   LoadDistributionALDChangeACK_SystemDeniedVersatile (5U)
 *   LoadDistributionALDChangeACK_Error (6U)
 *   LoadDistributionALDChangeACK_NotAvailable (7U)
 * LoadDistributionChangeACK_T: Enumeration of integer in interval [0...7] with enumerators
 *   LoadDistributionChangeACK_NoAction (0U)
 *   LoadDistributionChangeACK_ChangeAcknowledged (1U)
 *   LoadDistributionChangeACK_LoadShiftDeniedFALIMOverload (2U)
 *   LoadDistributionChangeACK_Reserved_1 (3U)
 *   LoadDistributionChangeACK_Reserved_2 (4U)
 *   LoadDistributionChangeACK_SystemDeniedVersatile (5U)
 *   LoadDistributionChangeACK_Error (6U)
 *   LoadDistributionChangeACK_NotAvailable (7U)
 * LoadDistributionChangeRequest_T: Enumeration of integer in interval [0...3] with enumerators
 *   LoadDistributionChangeRequest_Idle (0U)
 *   LoadDistributionChangeRequest_ChangeLoadDistribution (1U)
 *   LoadDistributionChangeRequest_Error (2U)
 *   LoadDistributionChangeRequest_NotAvailable (3U)
 * LoadDistributionFuncSelected_T: Enumeration of integer in interval [0...15] with enumerators
 *   LoadDistributionFuncSelected_NormalLoad (0U)
 *   LoadDistributionFuncSelected_OptimisedTraction (1U)
 *   LoadDistributionFuncSelected_MaximumTractionTractionHelp (2U)
 *   LoadDistributionFuncSelected_AlternativeLoad (3U)
 *   LoadDistributionFuncSelected_NoLoadDistribution (4U)
 *   LoadDistributionFuncSelected_VariableAxleLoad (5U)
 *   LoadDistributionFuncSelected_MaximumTractionStartingHelp (6U)
 *   LoadDistributionFuncSelected_SpareValue_02 (7U)
 *   LoadDistributionFuncSelected_SpareValue_03 (8U)
 *   LoadDistributionFuncSelected_SpareValue_04 (9U)
 *   LoadDistributionFuncSelected_SpareValue_05 (10U)
 *   LoadDistributionFuncSelected_SpareValue_06 (11U)
 *   LoadDistributionFuncSelected_SpareValue_07 (12U)
 *   LoadDistributionFuncSelected_SpareValue_08 (13U)
 *   LoadDistributionFuncSelected_ErrorIndicator (14U)
 *   LoadDistributionFuncSelected_NotAvaiable (15U)
 * LoadDistributionRequestedACK_T: Enumeration of integer in interval [0...7] with enumerators
 *   LoadDistributionRequestedACK_NoAction (0U)
 *   LoadDistributionRequestedACK_ChangeAcknowledged (1U)
 *   LoadDistributionRequestedACK_LoadShiftDeniedFALIMOverload (2U)
 *   LoadDistributionRequestedACK_Reserved1 (3U)
 *   LoadDistributionRequestedACK_Reserved2 (4U)
 *   LoadDistributionRequestedACK_SystemDeniedVersatile (5U)
 *   LoadDistributionRequestedACK_Error (6U)
 *   LoadDistributionRequestedACK_NotAvailable (7U)
 * LoadDistributionRequested_T: Enumeration of integer in interval [0...15] with enumerators
 *   LoadDistributionRequested_NoAction (0U)
 *   LoadDistributionRequested_DefaultLoadDistribution (1U)
 *   LoadDistributionRequested_NormalLoadDistribution (2U)
 *   LoadDistributionRequested_OptimizedTraction (3U)
 *   LoadDistributionRequested_MaximumTraction1 (4U)
 *   LoadDistributionRequested_MaximumTraction2 (5U)
 *   LoadDistributionRequested_AlternativeLoadDistribution (6U)
 *   LoadDistributionRequested_VariableLoadDistribution (7U)
 *   LoadDistributionRequested_DelpressLoadDistribution (8U)
 *   LoadDistributionRequested_Reserved1 (9U)
 *   LoadDistributionRequested_Reserved2 (10U)
 *   LoadDistributionRequested_Reserved3 (11U)
 *   LoadDistributionRequested_Reserved4 (12U)
 *   LoadDistributionRequested_Reserved5 (13U)
 *   LoadDistributionRequested_Error (14U)
 *   LoadDistributionRequested_NotAvailable (15U)
 * LoadDistributionSelected_T: Enumeration of integer in interval [0...15] with enumerators
 *   LoadDistributionSelected_NormalLoad (0U)
 *   LoadDistributionSelected_OptimisedTraction (1U)
 *   LoadDistributionSelected_StartingHelp (2U)
 *   LoadDistributionSelected_TractionHelp (3U)
 *   LoadDistributionSelected_AlternativeLoad (4U)
 *   LoadDistributionSelected_StartingHelpInterDiff (5U)
 *   LoadDistributionSelected_TractionHelpInterDiff (6U)
 *   LoadDistributionSelected_NoLoadDistribution (7U)
 *   LoadDistributionSelected_VariableAxleLoad (8U)
 *   LoadDistributionSelected_StartingHelp2 (9U)
 *   LoadDistributionSelected_TractionHelp2 (10U)
 *   LoadDistributionSelected_Reserved_03 (11U)
 *   LoadDistributionSelected_Reserved_04 (12U)
 *   LoadDistributionSelected_Reserved_05 (13U)
 *   LoadDistributionSelected_Error (14U)
 *   LoadDistributionSelected_NotAvailable (15U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T Rte_Prm_P1KN5_AxleLoad_CRideLEDIndicationType_v(void)
 *   boolean Rte_Prm_P1BOS_AxleLoad_AccessoryBoggieALD_v(void)
 *   boolean Rte_Prm_P1BOV_AxleLoad_RatioALD_v(void)
 *   boolean Rte_Prm_P1BOW_AxleLoad_ArideLiftAxle_v(void)
 *   boolean Rte_Prm_P1BOX_AxleLoad_TridemFirstAxleLift_v(void)
 *   boolean Rte_Prm_P1CZ0_AxleLoad_MaxTractionTag_v(void)
 *   boolean Rte_Prm_P1CZ1_AxleLoad_RatioTagOrLoadDistrib_v(void)
 *   boolean Rte_Prm_P1CZ2_AxleLoad_RatioPusherRocker_v(void)
 *   boolean Rte_Prm_P1CZ3_AxleLoad_RatioTagRocker_v(void)
 *   boolean Rte_Prm_P1CZW_AxleLoad_OneLiftPusher_v(void)
 *   boolean Rte_Prm_P1CZX_AxleLoad_OneLiftAxleMaxTraction_v(void)
 *   boolean Rte_Prm_P1CZY_AxleLoad_OneLiftTag_v(void)
 *   boolean Rte_Prm_P1CZZ_AxleLoad_MaxTractionPusher_v(void)
 *   boolean Rte_Prm_P1J6B_AxleLoad_AccessoryTridemALD_v(void)
 *   boolean Rte_Prm_P1J6C_AxleLoad_BoggieDualRatio_v(void)
 *   boolean Rte_Prm_P1J6D_AxleLoad_TridemDualRatio_v(void)
 *   boolean Rte_Prm_P1KN2_AxleLoad_CRideLiftAxle_v(void)
 *   boolean Rte_Prm_P1KN4_AxleLoad_CRideLEDlowerEnd_v(void)
 *   boolean Rte_Prm_P1M5B_AxleLoad_RatioRoadGripPusher_v(void)
 *   boolean Rte_Prm_P1BOY_AxleLoad_TridemSecondAxleLift_v(void)
 *
 *********************************************************************************************************************/


#define AxleLoadDistribution_HMICtrl_START_SEC_CODE
#include "AxleLoadDistribution_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define PCODE_P1BOS_AxleLoad_AccessoryBoggieALD      (Rte_Prm_P1BOS_AxleLoad_AccessoryBoggieALD_v())
#define PCODE_P1BOV_AxleLoad_RatioALD                (Rte_Prm_P1BOV_AxleLoad_RatioALD_v())
#define PCODE_P1BOW_AxleLoad_ArideLiftAxle           (Rte_Prm_P1BOW_AxleLoad_ArideLiftAxle_v())
#define PCODE_P1BOX_AxleLoad_TridemFirstAxleLift     (Rte_Prm_P1BOX_AxleLoad_TridemFirstAxleLift_v())
#define PCODE_P1BOY_AxleLoad_TridemSecondAxleLift    (Rte_Prm_P1BOY_AxleLoad_TridemSecondAxleLift_v())
#define PCODE_P1CZ0_AxleLoad_MaxTractionTag          (Rte_Prm_P1CZ0_AxleLoad_MaxTractionTag_v())
#define PCODE_P1CZ1_AxleLoad_RatioTagOrLoadDistrib   (Rte_Prm_P1CZ1_AxleLoad_RatioTagOrLoadDistrib_v())
#define PCODE_P1CZ2_AxleLoad_RatioPusherRocker       (Rte_Prm_P1CZ2_AxleLoad_RatioPusherRocker_v())
#define PCODE_P1CZ3_AxleLoad_RatioTagRocker          (Rte_Prm_P1CZ3_AxleLoad_RatioTagRocker_v())
#define PCODE_P1CZW_AxleLoad_OneLiftPusher           (Rte_Prm_P1CZW_AxleLoad_OneLiftPusher_v())
#define PCODE_P1CZX_AxleLoad_OneLiftAxleMaxTraction  (Rte_Prm_P1CZX_AxleLoad_OneLiftAxleMaxTraction_v())
#define PCODE_P1CZY_AxleLoad_OneLiftTag              (Rte_Prm_P1CZY_AxleLoad_OneLiftTag_v())
#define PCODE_P1CZZ_AxleLoad_MaxTractionPusher       (Rte_Prm_P1CZZ_AxleLoad_MaxTractionPusher_v())
#define PCODE_P1J6B_AxleLoad_AccessoryTridemALD      (Rte_Prm_P1J6B_AxleLoad_AccessoryTridemALD_v())
#define PCODE_P1J6C_AxleLoad_BoggieDualRatio         (Rte_Prm_P1J6C_AxleLoad_BoggieDualRatio_v())
#define PCODE_P1J6D_AxleLoad_TridemDualRatio         (Rte_Prm_P1J6D_AxleLoad_TridemDualRatio_v())
#define PCODE_P1KN2_AxleLoad_CRideLiftAxle           (Rte_Prm_P1KN2_AxleLoad_CRideLiftAxle_v())
#define PCODE_P1M5B_AxleLoad_RatioRoadGripPusher     (Rte_Prm_P1M5B_AxleLoad_RatioRoadGripPusher_v())


/**********************************************************************************************************************
 *
 * Runnable Entity Name: AxleLoadDistribution_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ALDSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_ECSStandByRequest_ECSStandByRequest(ECSStandByRequest_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1PositionStatus_LiftAxle1PositionStatus(LiftAxlePositionStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1Switch2_Status_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1UpRequestACK_LiftAxle1UpRequestACK(LiftAxleUpRequestACK_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle2SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle2UpRequestACK_LiftAxle2UpRequestACK(LiftAxleUpRequestACK_T *data)
 *   Std_ReturnType Rte_Read_LoadDistributionALDChangeACK_LoadDistributionALDChangeACK(LoadDistributionALDChangeACK_T *data)
 *   Std_ReturnType Rte_Read_LoadDistributionChangeACK_LoadDistributionChangeACK(LoadDistributionChangeACK_T *data)
 *   Std_ReturnType Rte_Read_LoadDistributionFuncSelected_LoadDistributionFuncSelected(LoadDistributionFuncSelected_T *data)
 *   Std_ReturnType Rte_Read_LoadDistributionRequestedACK_LoadDistributionRequestedACK(LoadDistributionRequestedACK_T *data)
 *   Std_ReturnType Rte_Read_LoadDistributionSelected_LoadDistributionSelected(LoadDistributionSelected_T *data)
 *   Std_ReturnType Rte_Read_Ratio1SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_Ratio2SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_Ratio3SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_Ratio4SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_Ratio5SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_Ratio6SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_RatioALDSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_TridemALDSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ALD_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_AltLoadDistribution_rqst_AltLoadDistribution_rqst(AltLoadDistribution_rqst_T data)
 *   Std_ReturnType Rte_Write_BogieSwitch_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1AutoLiftRequest_LiftAxle1AutoLiftRequest(InactiveActive_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1DirectControl_LiftAxle1DirectControl(LiftAxleLiftPositionRequest_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1LiftPositionRequest_LiftAxle1LiftPositionRequest(LiftAxleLiftPositionRequest_T data)
 *   Std_ReturnType Rte_Write_LiftAxle2AutoLiftRequest_LiftAxle2AutoLiftRequest(InactiveActive_T data)
 *   Std_ReturnType Rte_Write_LiftAxle2LiftPositionRequest_LiftAxle2LiftPositionRequest(LiftAxleLiftPositionRequest_T data)
 *   Std_ReturnType Rte_Write_LoadDistributionChangeRequest_LoadDistributionChangeRequest(LoadDistributionChangeRequest_T data)
 *   Std_ReturnType Rte_Write_LoadDistributionRequested_LoadDistributionRequested(LoadDistributionRequested_T data)
 *   Std_ReturnType Rte_Write_MaxTract_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Ratio1_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Ratio2_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Ratio4_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Ratio5_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Ratio6_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_RatioALD_DualDeviceIndication_DualDeviceIndication(DualDeviceIndication_T data)
 *   Std_ReturnType Rte_Write_TridemALD_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BOV_56_ALD_InvalidConfiguration_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AxleLoadDistribution_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the AxleLoadDistribution_HMICtrl_20ms_runnable
//! 
//!==================================================================================================================== 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
FUNC(void, AxleLoadDistribution_HMICtrl_CODE) AxleLoadDistribution_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AxleLoadDistribution_HMICtrl_20ms_runnable
 *********************************************************************************************************************/
   //! ###### Definition of the SWC input and output data structures
   // Define input RteInData Common
   static AxleLoadDistribution_HMICtrl_InStruct     RteInData_Common;
   // Define input RteInData LiftAxle 
   static AxleLoadDistribution_LiftAxleInStruct     RteInData_LiftAxle;
   // Define input RteInData RatioSwitch 
   static AxleLoadDistribution_RatioSwitchInStruct  RteInData_RatioSwitch; 
   // Define input RteInData ALD_Switch 
   static AxleLoadDistribution_ALDSwitchInStruct    RteInData_ALD_Switch;
   static ALD_HMICtrl_InitializeStatesStruct        ALD_InitializeStates;
   static ALD_OLA_ButtonStuckFlagStruct             OLA_ButtonStuckFlag;
   // Define input RteOutData LiftAxle 
   static AxleLoadDistribution_LiftAxleOutStruct RteOutData_LiftAxle       = { LiftAxleLiftPositionRequest_NotAvailable,
                                                                               LiftAxleLiftPositionRequest_NotAvailable,
                                                                               InactiveActive_NotAvailable,
                                                                               InactiveActive_NotAvailable,
                                                                               DualDeviceIndication_UpperDontCareLowerDontCare,
                                                                               DeviceIndication_Off,
                                                                               LiftAxleLiftPositionRequest_NotAvailable };  
   // Define input RteOutData RatioSwitch 
   static AxleLoadDistribution_RatioSwitchOutStruct RteOutData_RatioSwitch = { DeviceIndication_SpareValue,
                                                                               DeviceIndication_SpareValue,
                                                                               DualDeviceIndication_UpperDontCareLowerDontCare,
                                                                               DualDeviceIndication_UpperDontCareLowerDontCare,
                                                                               DeviceIndication_SpareValue,
                                                                               LoadDistributionRequested_NotAvailable,
                                                                               LoadDistributionChangeRequest_NotAvailable };
   // Define input RteOutData ALD Switch 
   static AxleLoadDistribution_ALD_SwitchOutStruct RteOutData_ALD_Switch   = { DeviceIndication_SpareValue,
                                                                               AltLoadDistribution_rqst_NotAvailable,
                                                                               DualDeviceIndication_UpperDontCareLowerDontCare,
                                                                               DeviceIndication_SpareValue }; 
   static uint16 Timers[CONST_NbOfTimers];
   uint8 oneLiftAxle_return = 0U;
   uint8 ratioSwitch_return = 0U;
   uint8 flag_DEM_Error     = CONST_ResetFlag;

   //! ###### Process RTE read ports for lift axle logic : 'Get_RteInDataRead_LiftAxle()'
   Get_RteInDataRead_LiftAxle(&RteInData_LiftAxle);
   //! ###### Process RTE read ports for ratio switch logic : 'Get_RteInDataRead_RatioSwitch()'
   Get_RteInDataRead_RatioSwitch(&RteInData_RatioSwitch);
   //! ###### Process RTE read ports for ALD switch logic : 'Get_RteInDataRead_ALD_Switch()'
   Get_RteInDataRead_ALD_Switch(&RteInData_ALD_Switch);
   //! ###### Process RTE read common ports logic : 'Get_RteInDataRead_Common()'
   Get_RteInDataRead_Common(&RteInData_Common);
   //! ###### Check for 'SwcActivation_Living' port status
   if (Operational == RteInData_Common.SwcActivation_Living)
   {
      //! ##### Check for one lift axle configuration error detection : 'OneLiftAxleConfigurationErrorDetection()'
      oneLiftAxle_return = OneLiftAxleConfigurationErrorDetection();
      if (oneLiftAxle_return >= 2U)
      {
         RteOutData_LiftAxle.LiftAxle1LiftPositionRequest = LiftAxleLiftPositionRequest_Error;
         RteOutData_LiftAxle.LiftAxle1AutoLiftRequest     = InactiveActive_Error;
         RteOutData_LiftAxle.LiftAxle2LiftPositionRequest = LiftAxleLiftPositionRequest_Error;
         RteOutData_LiftAxle.LiftAxle2AutoLiftRequest     = InactiveActive_Error;
         // DEM failed event
         flag_DEM_Error	                                  = CONST_SetFlag;
      }
      else
      {
         //! ##### Process one lift axle state machine logic : 'OneLiftAxleStateMachine()'
         OneLiftAxleStateMachine(&RteInData_LiftAxle,
                                 &RteInData_Common.LoadDistributionFuncSelected,
                                 Timers,
                                 &OLA_ButtonStuckFlag,
                                 &ALD_InitializeStates,
                                 &RteOutData_LiftAxle);
      }
      //! ##### Check for ratio switch configuration error detection : 'RatioSwitchConfigurationErrorDetection()'
      ratioSwitch_return = RatioSwitchConfigurationErrorDetection();
      if (ratioSwitch_return >= 2U)
      {
         RteOutData_RatioSwitch.LoadDistributionChangeRequest = LoadDistributionChangeRequest_Error;
		 // DEM failed event
         flag_DEM_Error	                                      = CONST_SetFlag;
      }
      else
      {
         //! ##### Process ratio switch state machine logic : 'RatioSwitchStateMachine()'
         RatioSwitchStateMachine(&RteInData_RatioSwitch,
                                 &RteInData_Common.LoadDistributionChangeACK.currentValue,
                                 &RteInData_Common.LoadDistributionFuncSelected,
                                 Timers,
                                 &ALD_InitializeStates,
                                 &RteOutData_RatioSwitch);
      }
	  if (CONST_SetFlag == flag_DEM_Error)
	  {
         Rte_Call_Event_D1BOV_56_ALD_InvalidConfiguration_SetEventStatus(DEM_EVENT_STATUS_FAILED);
	  }
      else
      {
      	 Rte_Call_Event_D1BOV_56_ALD_InvalidConfiguration_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      }
      //! ##### Select 'AxleLoad_CRideLiftAxle' parameter
      if (TRUE == PCODE_P1KN2_AxleLoad_CRideLiftAxle)
      {
         //! ##### Process lift axle position switch state machine logic : 'LiftAxlePositionswitchStateMachine()'
         LiftAxlePositionswitchStateMachine(&RteInData_LiftAxle.LiftAxle1Switch2_Status,
                                            &RteInData_LiftAxle.LiftAxle1UpRequestACK,
                                            &RteInData_LiftAxle.LiftAxle1PositionStatus,
                                            Timers,
                                            &ALD_InitializeStates,
                                            &RteOutData_LiftAxle.LiftAxle1LiftPositionRequest,
                                            &RteOutData_LiftAxle.BogieSwitch_DeviceIndication);
      }
      else
      {
         // Do nothing, keep previous status
      }
      //! ##### Process dual ratio switch state machine logic : 'DualRatioSwitchStateMachine()'
      DualRatioSwitchStateMachine(&RteInData_RatioSwitch.Ratio4SwitchStatus,
                                  &RteInData_RatioSwitch.Ratio5SwitchStatus,
                                  &RteInData_RatioSwitch.LoadDistributionRequestedACK,
                                  &RteInData_Common.LoadDistributionSelected,
                                  Timers,
                                  &ALD_InitializeStates,
                                  &RteOutData_RatioSwitch.Ratio4_DeviceIndication,
                                  &RteOutData_RatioSwitch.Ratio5_DeviceIndication,
                                  &RteOutData_RatioSwitch.LoadDistributionRequested);
      //! ##### Select 'AxleLoad_ArideLiftAxle' parameter
      if (TRUE == PCODE_P1BOW_AxleLoad_ArideLiftAxle)
      {  
         //! ##### Process ARide state machine logic : 'ARideStateMachine()'
         ARideStateMachine(&RteInData_LiftAxle.LiftAxle2MaxTractSwitchStatus,
                           &RteInData_LiftAxle.LiftAxle1UpRequestACK,
                           &RteInData_Common.LoadDistributionSelected,
                           Timers,
                           &ALD_InitializeStates,
                           &RteOutData_LiftAxle.LiftAxle1LiftPositionRequest,
                           &RteOutData_LiftAxle.LiftAxle1DirectControl,
                           &RteOutData_LiftAxle.MaxTract_DeviceIndication);
      }
      else
      {
         // Do nothing, keep previous status
      }
      //! ##### Select 'AxleLoad_RatioALD' parameter
      if (TRUE == PCODE_P1BOV_AxleLoad_RatioALD) 
      {
         //! ##### Process ratio ALD switch state machine logic : 'RatioALDSwitchStateMachine()'
         RatioALDSwitchStateMachine(&RteInData_ALD_Switch.RatioALDSwitchStatus,
                                    &RteInData_ALD_Switch.LoadDistributionALDChangeACK,
                                    &RteInData_Common,
                                    Timers,
                                    &ALD_InitializeStates,
                                    &RteOutData_RatioSwitch.LoadDistributionChangeRequest,
                                    &RteOutData_ALD_Switch.AltLoadDistribution_rqst,
                                    &RteOutData_ALD_Switch.RatioALD_DualDeviceIndication);
      }
      else
      {
         // Do nothing, keep previous status
      }
      //! ##### Process ALD switch state machine logic : 'ALDSwitchStateMachine()'
      ALDSwitchStateMachine(&RteInData_ALD_Switch.ALDSwitchStatus,
                            &RteInData_ALD_Switch.TridemALDSwitchStatus,
                            &RteInData_ALD_Switch.LoadDistributionALDChangeACK,
                            &RteInData_Common.LoadDistributionSelected,
                            Timers,
                            &ALD_InitializeStates,
                            &RteOutData_ALD_Switch.ALD_DeviceIndication,
                            &RteOutData_ALD_Switch.TridemALD_DeviceIndication,
                            &RteOutData_ALD_Switch.AltLoadDistribution_rqst);
   }
   else
   {
      //! ###### Processing logic for NonOperational, OperationalEntry or OperationalExit vehicle mode 
      RteOutData_ALD_Switch.AltLoadDistribution_rqst       = AltLoadDistribution_rqst_Idle;
      RteOutData_LiftAxle.LiftAxle1DirectControl           = LiftAxleLiftPositionRequest_Idle;
      RteOutData_LiftAxle.LiftAxle1LiftPositionRequest     = LiftAxleLiftPositionRequest_Idle;
      RteOutData_LiftAxle.LiftAxle2LiftPositionRequest     = LiftAxleLiftPositionRequest_Idle;
      RteOutData_RatioSwitch.LoadDistributionChangeRequest = LoadDistributionChangeRequest_Idle;
      RteOutData_LiftAxle.LiftAxle1AutoLiftRequest         = InactiveActive_Inactive;
      RteOutData_LiftAxle.LiftAxle2AutoLiftRequest         = InactiveActive_Inactive;
      RteOutData_RatioSwitch.LoadDistributionRequested     = LoadDistributionRequested_NoAction;
      RteOutData_LiftAxle.BogieSwitch_DeviceIndication     = DualDeviceIndication_UpperOffLowerOff;
      RteOutData_RatioSwitch.Ratio4_DeviceIndication       = DualDeviceIndication_UpperOffLowerOff;
      RteOutData_RatioSwitch.Ratio5_DeviceIndication       = DualDeviceIndication_UpperOffLowerOff;
      RteOutData_ALD_Switch.RatioALD_DualDeviceIndication  = DualDeviceIndication_UpperOffLowerOff;
      RteOutData_ALD_Switch.ALD_DeviceIndication           = DeviceIndication_Off;
      RteOutData_LiftAxle.MaxTract_DeviceIndication        = DeviceIndication_Off;
      RteOutData_RatioSwitch.Ratio1_DeviceIndication       = DeviceIndication_Off;
      RteOutData_RatioSwitch.Ratio2_DeviceIndication       = DeviceIndication_Off;
      RteOutData_RatioSwitch.Ratio6_DeviceIndication       = DeviceIndication_Off;
      RteOutData_ALD_Switch.TridemALD_DeviceIndication     = DeviceIndication_Off;
      ALD_InitializeStates.initialize_LA2S                 = CONST_NonInitialize;
      ALD_InitializeStates.initialize_LA1S                 = CONST_NonInitialize;
      ALD_InitializeStates.initialize_LA2MTS               = CONST_NonInitialize;
      ALD_InitializeStates.initialize_LA1MTS               = CONST_NonInitialize;
      ALD_InitializeStates.initialize_LA1TS                = CONST_NonInitialize;
      ALD_InitializeStates.initialize_LA2TS                = CONST_NonInitialize;
      ALD_InitializeStates.initialize_R3S                  = CONST_NonInitialize;
      ALD_InitializeStates.initialize_R1S                  = CONST_NonInitialize;
      ALD_InitializeStates.initialize_R2S                  = CONST_NonInitialize;
      ALD_InitializeStates.initialize_R6S                  = CONST_NonInitialize;
      ALD_InitializeStates.initialize_LAPS                 = CONST_NonInitialize;
      ALD_InitializeStates.initialize_R4S                  = CONST_NonInitialize;
      ALD_InitializeStates.initialize_R5S                  = CONST_NonInitialize;
      ALD_InitializeStates.initialize_ARide                = CONST_NonInitialize;
      ALD_InitializeStates.initialize_ALDS                 = CONST_NonInitialize;
      ALD_InitializeStates.initialize_TridemALDS           = CONST_NonInitialize;
      ALD_InitializeStates.initialize_RatioALDS            = CONST_NonInitialize;
   }
   //! ###### Process the decrement timer : 'TimerFunction_Tick()'
   TimerFunction_Tick(CONST_NbOfTimers,
                      Timers);
   //! ###### Process RTE write ports logic : 'RteInDataWrite_LiftAxle()'
   RteInDataWrite_LiftAxle(&RteOutData_LiftAxle);
   //! ###### Process RTE write ports logic : 'RteInDataWrite_RatioSwitch()'
   RteInDataWrite_RatioSwitch(&RteOutData_RatioSwitch);
   //! ###### Process RTE write ports logic : 'RteInDataWrite_ALD_Switch()'
   RteInDataWrite_ALD_Switch(&RteOutData_ALD_Switch);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AxleLoadDistribution_HMICtrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AxleLoadDistribution_HMICtrl_Init_doc
 *********************************************************************************************************************/
//!==================================================================================================================
//!
//! \brief
//! This function implements the cyclic execution runnable logic for the AxleLoadDistribution_HMICtrl_Init
//!
//!==================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, AxleLoadDistribution_HMICtrl_CODE) AxleLoadDistribution_HMICtrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AxleLoadDistribution_HMICtrl_Init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define AxleLoadDistribution_HMICtrl_STOP_SEC_CODE
#include "AxleLoadDistribution_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

//!======================================================================================
//!
//! \brief
//! This function is processing the 'Get_RteInDataRead_LiftAxle' logic
//!
//! \param   *pRteInData_LiftAxle   Update the input signals to ports of input structure
//!
//!======================================================================================
static void Get_RteInDataRead_LiftAxle(AxleLoadDistribution_LiftAxleInStruct *pRteInData_LiftAxle)
{
   Std_ReturnType liftAxleRetValue = RTE_E_INVALID;

   //! ###### Processing the RTE input data read 'Lift Axle' logic
   //! ##### Read LiftAxle1MaxTractSwitchStatus port interface
   pRteInData_LiftAxle->LiftAxle1MaxTractSwitchStatus.previousValue = pRteInData_LiftAxle->LiftAxle1MaxTractSwitchStatus.currentValue;
   liftAxleRetValue = Rte_Read_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus(&pRteInData_LiftAxle->LiftAxle1MaxTractSwitchStatus.currentValue);
   MACRO_StdRteRead_IntRPort((liftAxleRetValue),(pRteInData_LiftAxle->LiftAxle1MaxTractSwitchStatus.currentValue),(A3PosSwitchStatus_Error))

   //! ##### Read LiftAxle1SwitchStatus port interface
   pRteInData_LiftAxle->LiftAxle1SwitchStatus.previousValue = pRteInData_LiftAxle->LiftAxle1SwitchStatus.currentValue;
   liftAxleRetValue = Rte_Read_LiftAxle1SwitchStatus_A3PosSwitchStatus(&pRteInData_LiftAxle->LiftAxle1SwitchStatus.currentValue);
   MACRO_StdRteRead_IntRPort((liftAxleRetValue),(pRteInData_LiftAxle->LiftAxle1SwitchStatus.currentValue),(A3PosSwitchStatus_Error))

   //! ##### Read LiftAxle1TRIDEMSwitchStatus port interface
   pRteInData_LiftAxle->LiftAxle1TRIDEMSwitchStatus.previousValue = pRteInData_LiftAxle->LiftAxle1TRIDEMSwitchStatus.currentValue;
   liftAxleRetValue = Rte_Read_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus(&pRteInData_LiftAxle->LiftAxle1TRIDEMSwitchStatus.currentValue);
   MACRO_StdRteRead_IntRPort((liftAxleRetValue),(pRteInData_LiftAxle->LiftAxle1TRIDEMSwitchStatus.currentValue),(A3PosSwitchStatus_Error))

   //! ##### Read LiftAxle2MaxTractSwitchStatus port interface
   pRteInData_LiftAxle->LiftAxle2MaxTractSwitchStatus.previousValue = pRteInData_LiftAxle->LiftAxle2MaxTractSwitchStatus.currentValue;
   liftAxleRetValue = Rte_Read_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus(&pRteInData_LiftAxle->LiftAxle2MaxTractSwitchStatus.currentValue);
   MACRO_StdRteRead_IntRPort((liftAxleRetValue),(pRteInData_LiftAxle->LiftAxle2MaxTractSwitchStatus.currentValue),(A3PosSwitchStatus_Error))

   //! ##### Read LiftAxle2SwitchStatus port interface
   pRteInData_LiftAxle->LiftAxle2SwitchStatus.previousValue = pRteInData_LiftAxle->LiftAxle2SwitchStatus.currentValue;
   liftAxleRetValue = Rte_Read_LiftAxle2SwitchStatus_A3PosSwitchStatus(&pRteInData_LiftAxle->LiftAxle2SwitchStatus.currentValue);
   MACRO_StdRteRead_IntRPort((liftAxleRetValue),(pRteInData_LiftAxle->LiftAxle2SwitchStatus.currentValue),(A3PosSwitchStatus_Error))

   //! ##### Read LiftAxle2TRIDEMSwitchStatus port interface
   pRteInData_LiftAxle->LiftAxle2TRIDEMSwitchStatus.previousValue = pRteInData_LiftAxle->LiftAxle2TRIDEMSwitchStatus.currentValue;
   liftAxleRetValue = Rte_Read_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus(&pRteInData_LiftAxle->LiftAxle2TRIDEMSwitchStatus.currentValue);
   MACRO_StdRteRead_IntRPort((liftAxleRetValue),(pRteInData_LiftAxle->LiftAxle2TRIDEMSwitchStatus.currentValue),(A3PosSwitchStatus_Error))
    
   //! ##### Read LiftAxle1PositionStatus port interface
   liftAxleRetValue = Rte_Read_LiftAxle1PositionStatus_LiftAxle1PositionStatus(&pRteInData_LiftAxle->LiftAxle1PositionStatus);
   MACRO_StdRteRead_ExtRPort((liftAxleRetValue),(pRteInData_LiftAxle->LiftAxle1PositionStatus),(LiftAxlePositionStatus_NotAvailable),(LiftAxlePositionStatus_Error))

   //! ##### Read LiftAxle1Switch2_Status port interface
   pRteInData_LiftAxle->LiftAxle1Switch2_Status.previousValue = pRteInData_LiftAxle->LiftAxle1Switch2_Status.currentValue;
   liftAxleRetValue = Rte_Read_LiftAxle1Switch2_Status_A3PosSwitchStatus(&pRteInData_LiftAxle->LiftAxle1Switch2_Status.currentValue);
   MACRO_StdRteRead_IntRPort((liftAxleRetValue),(pRteInData_LiftAxle->LiftAxle1Switch2_Status.currentValue),(A3PosSwitchStatus_Error))

   //! ##### Read LiftAxle1UpRequestACK port interface
   pRteInData_LiftAxle->LiftAxle1UpRequestACK.previousValue = pRteInData_LiftAxle->LiftAxle1UpRequestACK.currentValue;
   liftAxleRetValue = Rte_Read_LiftAxle1UpRequestACK_LiftAxle1UpRequestACK(&pRteInData_LiftAxle->LiftAxle1UpRequestACK.currentValue);
   MACRO_StdRteRead_ExtRPort((liftAxleRetValue),(pRteInData_LiftAxle->LiftAxle1UpRequestACK.currentValue),(LiftAxleUpRequestACK_NotAvailable),(LiftAxleUpRequestACK_Error))
   
   //! ##### Read LiftAxle2UpRequestACK port interface
   liftAxleRetValue = Rte_Read_LiftAxle2UpRequestACK_LiftAxle2UpRequestACK(&pRteInData_LiftAxle->LiftAxle2UpRequestACK);
   MACRO_StdRteRead_ExtRPort((liftAxleRetValue),(pRteInData_LiftAxle->LiftAxle2UpRequestACK),(LiftAxleUpRequestACK_NotAvailable),(LiftAxleUpRequestACK_Error))
    
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'Get_RteInDataRead_RatioSwitch' logic
//!
//! \param   *pRteInData_RatioSwitch   Update the input signals to ports of input structure
//!
//!======================================================================================
static void Get_RteInDataRead_RatioSwitch(AxleLoadDistribution_RatioSwitchInStruct *pRteInData_RatioSwitch)
{
   //! ###### Processing the RTE input data dead 'Ratio Switch' logic
   Std_ReturnType ratioSwitchRetValue = RTE_E_INVALID;

   //! ##### Read LoadDistributionRequestedACK port interface
   ratioSwitchRetValue = Rte_Read_LoadDistributionRequestedACK_LoadDistributionRequestedACK(&pRteInData_RatioSwitch->LoadDistributionRequestedACK);
   MACRO_StdRteRead_ExtRPort((ratioSwitchRetValue),(pRteInData_RatioSwitch->LoadDistributionRequestedACK),(LoadDistributionRequestedACK_NotAvailable),(LoadDistributionRequestedACK_Error))

   //! ##### Read Ratio1SwitchStatus port interface
   pRteInData_RatioSwitch->Ratio1SwitchStatus.previousValue = pRteInData_RatioSwitch->Ratio1SwitchStatus.currentValue;
   ratioSwitchRetValue = Rte_Read_Ratio1SwitchStatus_A2PosSwitchStatus(&pRteInData_RatioSwitch->Ratio1SwitchStatus.currentValue);
   MACRO_StdRteRead_IntRPort((ratioSwitchRetValue),(pRteInData_RatioSwitch->Ratio1SwitchStatus.currentValue),(A2PosSwitchStatus_Error))

   //! ##### Read Ratio2SwitchStatus port interface
   pRteInData_RatioSwitch->Ratio2SwitchStatus.previousValue = pRteInData_RatioSwitch->Ratio2SwitchStatus.currentValue;
   ratioSwitchRetValue = Rte_Read_Ratio2SwitchStatus_A2PosSwitchStatus(&pRteInData_RatioSwitch->Ratio2SwitchStatus.currentValue);
   MACRO_StdRteRead_IntRPort((ratioSwitchRetValue),(pRteInData_RatioSwitch->Ratio2SwitchStatus.currentValue),(A2PosSwitchStatus_Error))

   //! ##### Read Ratio3SwitchStatus port interface 
   pRteInData_RatioSwitch->Ratio3SwitchStatus.previousValue = pRteInData_RatioSwitch->Ratio3SwitchStatus.currentValue;
   ratioSwitchRetValue = Rte_Read_Ratio3SwitchStatus_A2PosSwitchStatus(&pRteInData_RatioSwitch->Ratio3SwitchStatus.currentValue);
   MACRO_StdRteRead_IntRPort((ratioSwitchRetValue),(pRteInData_RatioSwitch->Ratio3SwitchStatus.currentValue),(A2PosSwitchStatus_Error))

   //! ##### Read Ratio4SwitchStatus port interface 
   pRteInData_RatioSwitch->Ratio4SwitchStatus.previousValue = pRteInData_RatioSwitch->Ratio4SwitchStatus.currentValue;
   ratioSwitchRetValue = Rte_Read_Ratio4SwitchStatus_A3PosSwitchStatus(&pRteInData_RatioSwitch->Ratio4SwitchStatus.currentValue);
   MACRO_StdRteRead_IntRPort((ratioSwitchRetValue),(pRteInData_RatioSwitch->Ratio4SwitchStatus.currentValue),(A3PosSwitchStatus_Error))

   //! ##### Read Ratio5SwitchStatus port interface 
   pRteInData_RatioSwitch->Ratio5SwitchStatus.previousValue = pRteInData_RatioSwitch->Ratio5SwitchStatus.currentValue;
   ratioSwitchRetValue = Rte_Read_Ratio5SwitchStatus_A3PosSwitchStatus(&pRteInData_RatioSwitch->Ratio5SwitchStatus.currentValue);
   MACRO_StdRteRead_IntRPort((ratioSwitchRetValue),(pRteInData_RatioSwitch->Ratio5SwitchStatus.currentValue),(A3PosSwitchStatus_Error))

   //! ##### Read Ratio6SwitchStatus port interface 
   pRteInData_RatioSwitch->Ratio6SwitchStatus.previousValue = pRteInData_RatioSwitch->Ratio6SwitchStatus.currentValue;
   ratioSwitchRetValue = Rte_Read_Ratio6SwitchStatus_A2PosSwitchStatus(&pRteInData_RatioSwitch->Ratio6SwitchStatus.currentValue);
   MACRO_StdRteRead_IntRPort((ratioSwitchRetValue),(pRteInData_RatioSwitch->Ratio6SwitchStatus.currentValue),(A2PosSwitchStatus_Error))
    
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'Get_RteInDataRead_ALD_Switch' logic
//!
//! \param  *pRteInData_ALD_Switch    Update the input signals to ports of input structure
//!
//!======================================================================================
static void Get_RteInDataRead_ALD_Switch(AxleLoadDistribution_ALDSwitchInStruct *pRteInData_ALD_Switch)
{
   //! ###### Processing the RTE input data read 'ALD Switch' logic
   Std_ReturnType ald_retValue = RTE_E_INVALID;

   //! ##### Read ALDSwitchStatus port interface
   pRteInData_ALD_Switch->ALDSwitchStatus.previousValue = pRteInData_ALD_Switch->ALDSwitchStatus.currentValue;
   ald_retValue = Rte_Read_ALDSwitchStatus_A2PosSwitchStatus(&pRteInData_ALD_Switch->ALDSwitchStatus.currentValue);
   MACRO_StdRteRead_IntRPort((ald_retValue),(pRteInData_ALD_Switch->ALDSwitchStatus.currentValue),(A2PosSwitchStatus_Error))
    
   //! ##### Read LoadDistributionALDChangeACK port interface
   ald_retValue = Rte_Read_LoadDistributionALDChangeACK_LoadDistributionALDChangeACK(&pRteInData_ALD_Switch->LoadDistributionALDChangeACK);
   MACRO_StdRteRead_ExtRPort((ald_retValue),(pRteInData_ALD_Switch->LoadDistributionALDChangeACK),(LoadDistributionALDChangeACK_NotAvailable),(LoadDistributionALDChangeACK_Error))

   //! ##### Read RatioALDSwitchStatus port interface
   pRteInData_ALD_Switch->RatioALDSwitchStatus.previousValue = pRteInData_ALD_Switch->RatioALDSwitchStatus.currentValue;
   ald_retValue = Rte_Read_RatioALDSwitchStatus_A3PosSwitchStatus(&pRteInData_ALD_Switch->RatioALDSwitchStatus.currentValue);
   MACRO_StdRteRead_IntRPort((ald_retValue),(pRteInData_ALD_Switch->RatioALDSwitchStatus.currentValue),(A3PosSwitchStatus_Error))

   //! ##### Read TridemALDSwitchStatus port interface 
   pRteInData_ALD_Switch->TridemALDSwitchStatus.previousValue = pRteInData_ALD_Switch->TridemALDSwitchStatus.currentValue;
   ald_retValue = Rte_Read_TridemALDSwitchStatus_A2PosSwitchStatus(&pRteInData_ALD_Switch->TridemALDSwitchStatus.currentValue);
   MACRO_StdRteRead_IntRPort((ald_retValue),(pRteInData_ALD_Switch->TridemALDSwitchStatus.currentValue),(A2PosSwitchStatus_Error))
    
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'Get_RteInDataRead_Common' logic
//!
//! \param   *pRteInData_Common   Update the input signals to ports of input structure
//!
//!======================================================================================
static void Get_RteInDataRead_Common(AxleLoadDistribution_HMICtrl_InStruct *pRteInData_Common)
{
   //! ###### Processing the RTE input data read common logic
   Std_ReturnType retValue = RTE_E_INVALID;

   //! ##### Read ECSStandByRequest interface
   retValue = Rte_Read_ECSStandByRequest_ECSStandByRequest(&pRteInData_Common->ECSStandByRequest);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->ECSStandByRequest), (ECSStandByRequest_Error))

   //! ##### Read LoadDistributionChangeACK port interface
   pRteInData_Common->LoadDistributionChangeACK.previousValue = pRteInData_Common->LoadDistributionChangeACK.currentValue;
   retValue = Rte_Read_LoadDistributionChangeACK_LoadDistributionChangeACK(&pRteInData_Common->LoadDistributionChangeACK.currentValue);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->LoadDistributionChangeACK.currentValue), (LoadDistributionChangeACK_NotAvailable), (LoadDistributionChangeACK_Error))
    
   //! ##### Read LoadDistributionFuncSelected port interface
   retValue = Rte_Read_LoadDistributionFuncSelected_LoadDistributionFuncSelected(&pRteInData_Common->LoadDistributionFuncSelected);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->LoadDistributionFuncSelected), (LoadDistributionFuncSelected_NotAvaiable), (LoadDistributionFuncSelected_ErrorIndicator))
    
   //! ##### Read SwcActivation_Living port interface
   retValue = Rte_Read_SwcActivation_Living_Living(&pRteInData_Common->SwcActivation_Living);
   MACRO_StdRteRead_IntRPort((retValue), (pRteInData_Common->SwcActivation_Living), (NonOperational))

   //! ##### Read LoadDistributionSelected port interface
   pRteInData_Common->LoadDistributionSelected.previousValue = pRteInData_Common->LoadDistributionSelected.currentValue;
   retValue = Rte_Read_LoadDistributionSelected_LoadDistributionSelected(&pRteInData_Common->LoadDistributionSelected.currentValue);
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->LoadDistributionSelected.currentValue), (LoadDistributionSelected_NotAvailable), (LoadDistributionSelected_Error))
    
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'RteInDataWrite_LiftAxle'
//!
//! \param   *pRteOutData_LiftAxle   Update the output signals to ports of output structure
//!
//!======================================================================================
static void RteInDataWrite_LiftAxle(const AxleLoadDistribution_LiftAxleOutStruct *pRteOutData_LiftAxle)
{
   //! ###### Processing RTE output data write 'Lift Axle' ports logic
   (void)Rte_Write_LiftAxle1AutoLiftRequest_LiftAxle1AutoLiftRequest(pRteOutData_LiftAxle->LiftAxle1AutoLiftRequest);
   (void)Rte_Write_LiftAxle1LiftPositionRequest_LiftAxle1LiftPositionRequest(pRteOutData_LiftAxle->LiftAxle1LiftPositionRequest);
   Rte_Write_BogieSwitch_DeviceIndication_DualDeviceIndication(pRteOutData_LiftAxle->BogieSwitch_DeviceIndication);
   (void)Rte_Write_LiftAxle1DirectControl_LiftAxle1DirectControl(pRteOutData_LiftAxle->LiftAxle1DirectControl);
   (void)Rte_Write_LiftAxle2AutoLiftRequest_LiftAxle2AutoLiftRequest(pRteOutData_LiftAxle->LiftAxle2AutoLiftRequest);
   (void)Rte_Write_LiftAxle2LiftPositionRequest_LiftAxle2LiftPositionRequest(pRteOutData_LiftAxle->LiftAxle2LiftPositionRequest);
   Rte_Write_MaxTract_DeviceIndication_DeviceIndication(pRteOutData_LiftAxle->MaxTract_DeviceIndication);
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'RteInDataWrite_RatioSwitch'
//!
//! \param   *pRteOutData_RatioSwitch   Update the output signals to ports of output structure
//!
//!======================================================================================
static void RteInDataWrite_RatioSwitch(const AxleLoadDistribution_RatioSwitchOutStruct *pRteOutData_RatioSwitch)
{
   //! ###### Processing RTE output data write 'Ratio Switch' ports logic
   (void)Rte_Write_LoadDistributionRequested_LoadDistributionRequested(pRteOutData_RatioSwitch->LoadDistributionRequested);
   Rte_Write_Ratio1_DeviceIndication_DeviceIndication(pRteOutData_RatioSwitch->Ratio1_DeviceIndication);
   Rte_Write_Ratio2_DeviceIndication_DeviceIndication(pRteOutData_RatioSwitch->Ratio2_DeviceIndication);
   Rte_Write_Ratio4_DeviceIndication_DualDeviceIndication(pRteOutData_RatioSwitch->Ratio4_DeviceIndication);
   Rte_Write_Ratio5_DeviceIndication_DualDeviceIndication(pRteOutData_RatioSwitch->Ratio5_DeviceIndication);
   Rte_Write_Ratio6_DeviceIndication_DeviceIndication(pRteOutData_RatioSwitch->Ratio6_DeviceIndication);
   (void)Rte_Write_LoadDistributionChangeRequest_LoadDistributionChangeRequest(pRteOutData_RatioSwitch->LoadDistributionChangeRequest);
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'RteInDataWrite_ALD_Switch'
//!
//! \param  *pRteOutData_ALD_Switch   Update the output signals to ports of output structure
//!
//!======================================================================================
static void RteInDataWrite_ALD_Switch(const AxleLoadDistribution_ALD_SwitchOutStruct *pRteOutData_ALD_Switch)
{
   //! ###### Processing RTE output data write 'ALD Switch' ports logic
   Rte_Write_ALD_DeviceIndication_DeviceIndication(pRteOutData_ALD_Switch->ALD_DeviceIndication);
   (void)Rte_Write_AltLoadDistribution_rqst_AltLoadDistribution_rqst(pRteOutData_ALD_Switch->AltLoadDistribution_rqst);
   Rte_Write_RatioALD_DualDeviceIndication_DualDeviceIndication(pRteOutData_ALD_Switch->RatioALD_DualDeviceIndication);
   Rte_Write_TridemALD_DeviceIndication_DeviceIndication(pRteOutData_ALD_Switch->TridemALD_DeviceIndication);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'OneLiftAxleStateMachine' logic
//!
//! \param   *pRteInData_LiftAxle                 Providing the data of input ports
//! \param   *pLoadDistributionFuncSelected       Indicating the value of LoadDistributionFuncSelected
//! \param   OLA_Timers                           Providing the timer values
//! \param   *pOLA_ButtonStuckFlag                Providing the flag status of button stuck
//! \param   *pALD_InitializeStates               Providing the current ARide state initialization status
//! \param   *pRteOutData_LiftAxle                Providing the output structure
//!
//!======================================================================================
static void OneLiftAxleStateMachine(      AxleLoadDistribution_LiftAxleInStruct  *pRteInData_LiftAxle,
                                    const LoadDistributionFuncSelected_T         *pLoadDistributionFuncSelected,
                                          uint16                                 OLA_Timers[CONST_NbOfTimers],
                                          ALD_OLA_ButtonStuckFlagStruct          *pOLA_ButtonStuckFlag,
                                          ALD_HMICtrl_InitializeStatesStruct     *pALD_InitializeStates,
                                          AxleLoadDistribution_LiftAxleOutStruct *pRteOutData_LiftAxle)
{
   //! ##### Initialize the one lift axle states
   static SM_OneLiftAxleStates LiftAxle2SwitchSM_State         = { SM_ALD_OLA_AutoIdle,
                                                                   SM_ALD_OLA_Default };
   static SM_OneLiftAxleStates LiftAxle1SwitchSM_State         = { SM_ALD_OLA_AutoIdle,
                                                                   SM_ALD_OLA_Default };
   static SM_OneLiftAxleStates LiftAxle2MaxTractSwitchSM_State = { SM_ALD_OLA_AutoIdle,
                                                                   SM_ALD_OLA_Default };
   static SM_OneLiftAxleStates LiftAxle1MaxTractSwitchSM_State = { SM_ALD_OLA_AutoIdle,
                                                                   SM_ALD_OLA_Default };
   static SM_OneLiftAxleStates LiftAxle1TRIDEMSwitchSM_State   = { SM_ALD_OLA_AutoIdle,
                                                                   SM_ALD_OLA_Default };
   static SM_OneLiftAxleStates LiftAxle2TRIDEMSwitchSM_State   = { SM_ALD_OLA_AutoIdle,
                                                                   SM_ALD_OLA_Default };

   //! ##### Select 'AxleLoad_OneLiftAxleMaxTraction' parameter
   // One lift Axle - Axle 2 lift rocker switch
   if (TRUE == PCODE_P1CZX_AxleLoad_OneLiftAxleMaxTraction)
   {
      //! #### Process one lift axle state machine logic : 'OneLiftAxleStateTransitions()'
      OneLiftAxleStateTransitions(&pRteInData_LiftAxle->LiftAxle2SwitchStatus,
                                  &pRteInData_LiftAxle->LiftAxle1UpRequestACK.currentValue,
                                  &OLA_Timers[CONST_LA2SwitchBS_TimerID],
                                  &OLA_Timers[CONST_LA2S_UpBtnPrsAckTimerID],
                                  &pOLA_ButtonStuckFlag->LA2SButtonStuckFlag,
                                  &pALD_InitializeStates->initialize_LA2S,
                                  &LiftAxle2SwitchSM_State,
                                  &pRteOutData_LiftAxle->LiftAxle1AutoLiftRequest,
                                  &pRteOutData_LiftAxle->LiftAxle1LiftPositionRequest);
   }
   //! ##### Select 'AxleLoad_OneLiftPusher' parameter
   // VT- One Lift Axle Switch VT Push
   else if (TRUE == PCODE_P1CZW_AxleLoad_OneLiftPusher)
   {
      //! #### Process one lift axle state machine logic : 'OneLiftAxleStateTransitions()'
      OneLiftAxleStateTransitions(&pRteInData_LiftAxle->LiftAxle1SwitchStatus,
                                  &pRteInData_LiftAxle->LiftAxle1UpRequestACK.currentValue,
                                  &OLA_Timers[CONST_LA1SwitchBS_TimerID],
                                  &OLA_Timers[CONST_LA1S_UpBtnPrsAckTimerID],
                                  &pOLA_ButtonStuckFlag->LA1SButtonStuckFlag,
                                  &pALD_InitializeStates->initialize_LA1S,
                                  &LiftAxle1SwitchSM_State, 
                                  &pRteOutData_LiftAxle->LiftAxle1AutoLiftRequest,
                                  &pRteOutData_LiftAxle->LiftAxle1LiftPositionRequest);
   }
   //! ##### Select 'AxleLoad_OneLiftTag' parameter
   // VT - One Lift Axle Switch VT tag
   else if (TRUE == PCODE_P1CZY_AxleLoad_OneLiftTag)
   {
      //! #### Process one lift axle state machine logic : 'OneLiftAxleStateTransitions()'
      OneLiftAxleStateTransitions(&pRteInData_LiftAxle->LiftAxle2SwitchStatus,
                                  &pRteInData_LiftAxle->LiftAxle1UpRequestACK.currentValue,
                                  &OLA_Timers[CONST_LA2SwitchBS_TimerID],
                                  &OLA_Timers[CONST_LA2S_UpBtnPrsAckTimerID],
                                  &pOLA_ButtonStuckFlag->LA2SButtonStuckFlag,
                                  &pALD_InitializeStates->initialize_LA2S,
                                  &LiftAxle2SwitchSM_State,
                                  &pRteOutData_LiftAxle->LiftAxle1AutoLiftRequest,
                                  &pRteOutData_LiftAxle->LiftAxle1LiftPositionRequest);
   }
   //! ##### Select 'AxleLoad_MaxTractionTag' parameter
   // VT- One Lift Axle and Max Traction Tag
   else if (TRUE == PCODE_P1CZ0_AxleLoad_MaxTractionTag)
   {
      //! #### Process one lift axle state machine logic : 'OneLiftAxleStateTransitions()'
      OneLiftAxleStateTransitions(&pRteInData_LiftAxle->LiftAxle2MaxTractSwitchStatus,
                                  &pRteInData_LiftAxle->LiftAxle1UpRequestACK.currentValue,
                                  &OLA_Timers[CONST_LA2MTSwitchBS_TimerID],
                                  &OLA_Timers[CONST_LA2MTS_UpBtnPrsAckTimerID],
                                  &pOLA_ButtonStuckFlag->LA2MTSButtonStuckFlag,
                                  &pALD_InitializeStates->initialize_LA2MTS,
                                  &LiftAxle2MaxTractSwitchSM_State,
                                  &pRteOutData_LiftAxle->LiftAxle1AutoLiftRequest,
                                  &pRteOutData_LiftAxle->LiftAxle1LiftPositionRequest);
      //! #### Process MaxTract_DeviceIndication logic : 'DeviceIndication()'
      DeviceIndication(pLoadDistributionFuncSelected,
                       &pRteOutData_LiftAxle->MaxTract_DeviceIndication);
   }
   //! ##### Select 'AxleLoad_MaxTractionPusher' parameter
   // One Lift Axle and Max Traction Push
   else if (TRUE == PCODE_P1CZZ_AxleLoad_MaxTractionPusher)
   {
      //! #### Process one lift axle state machine logic : 'OneLiftAxleStateTransitions()'
      OneLiftAxleStateTransitions(&pRteInData_LiftAxle->LiftAxle1MaxTractSwitchStatus,
                                  &pRteInData_LiftAxle->LiftAxle1UpRequestACK.currentValue,
                                  &OLA_Timers[CONST_LA1MTSwitchBS_TimerID],
                                  &OLA_Timers[CONST_LA1MTS_UpBtnPrsAckTimerID],
                                  &pOLA_ButtonStuckFlag->LA1MTSButtonStuckFlag,
                                  &pALD_InitializeStates->initialize_LA1MTS,
                                  &LiftAxle1MaxTractSwitchSM_State,
                                  &pRteOutData_LiftAxle->LiftAxle1AutoLiftRequest,
                                  &pRteOutData_LiftAxle->LiftAxle1LiftPositionRequest);
      //! #### Process MaxTract_DeviceIndication logic : 'DeviceIndication()'
      DeviceIndication(pLoadDistributionFuncSelected,
                       &pRteOutData_LiftAxle->MaxTract_DeviceIndication);
   }
   //! ##### Check for 'AxleLoad_TridemFirstAxleLift' is true and 'AxleLoad_TridemSecondAxleLift' is false
   // VT- One Lift Axle TRIDEM :: Push Configuration
   else if ((TRUE == PCODE_P1BOX_AxleLoad_TridemFirstAxleLift) 
           && (FALSE == PCODE_P1BOY_AxleLoad_TridemSecondAxleLift))
   {
      //! #### Process one lift axle state machine logic : 'OneLiftAxleStateTransitions()'
      OneLiftAxleStateTransitions(&pRteInData_LiftAxle->LiftAxle1TRIDEMSwitchStatus,
                                  &pRteInData_LiftAxle->LiftAxle1UpRequestACK.currentValue,
                                  &OLA_Timers[CONST_LA1TSwitchBS_TimerID],
                                  &OLA_Timers[CONST_LA1TS_UpBtnPrsAck_TimerID],
                                  &pOLA_ButtonStuckFlag->LA1TSButtonStuckFlag,
                                  &pALD_InitializeStates->initialize_LA1TS,
                                  &LiftAxle1TRIDEMSwitchSM_State,
                                  &pRteOutData_LiftAxle->LiftAxle1AutoLiftRequest,
                                  &pRteOutData_LiftAxle->LiftAxle1LiftPositionRequest);
   }
   //! ##### Select 'AxleLoad_TridemSecondAxleLift' parameter
   else if (TRUE == PCODE_P1BOY_AxleLoad_TridemSecondAxleLift)
   {
      //! ##### Select 'AxleLoad_TridemFirstAxleLift' parameter
      if (TRUE == PCODE_P1BOX_AxleLoad_TridemFirstAxleLift)
      {
         // First lift axle (pusher)
         //! #### Process one lift axle state machine logic : 'OneLiftAxleStateTransitions()'
         OneLiftAxleStateTransitions(&pRteInData_LiftAxle->LiftAxle1TRIDEMSwitchStatus,
                                     &pRteInData_LiftAxle->LiftAxle1UpRequestACK.currentValue,
                                     &OLA_Timers[CONST_LA1TSwitchBS_TimerID],
                                     &OLA_Timers[CONST_LA1TS_UpBtnPrsAck_TimerID],
                                     &pOLA_ButtonStuckFlag->LA1TSButtonStuckFlag,
                                     &pALD_InitializeStates->initialize_LA1TS,
                                     &LiftAxle1TRIDEMSwitchSM_State,
                                     &pRteOutData_LiftAxle->LiftAxle1AutoLiftRequest,
                                     &pRteOutData_LiftAxle->LiftAxle1LiftPositionRequest);
         // Second lift axle (tag)
         //! #### Process one lift axle state machine logic : 'OneLiftAxleStateTransitions()'
         OneLiftAxleStateTransitions(&pRteInData_LiftAxle->LiftAxle2TRIDEMSwitchStatus,
                                     &pRteInData_LiftAxle->LiftAxle2UpRequestACK,
                                     &OLA_Timers[CONST_LA2TSwitchBS_TimerID],
                                     &OLA_Timers[CONST_LA2TS_UpBtnPrsAckTimerID],
                                     &pOLA_ButtonStuckFlag->LA2TSButtonStuckFlag,
                                     &pALD_InitializeStates->initialize_LA2TS,
                                     &LiftAxle2TRIDEMSwitchSM_State,
                                     &pRteOutData_LiftAxle->LiftAxle2AutoLiftRequest,
                                     &pRteOutData_LiftAxle->LiftAxle2LiftPositionRequest);
      }
      else
      {
         //! #### Process one lift axle state machine logic : 'OneLiftAxleStateTransitions()'
         OneLiftAxleStateTransitions(&pRteInData_LiftAxle->LiftAxle2TRIDEMSwitchStatus,
                                     &pRteInData_LiftAxle->LiftAxle1UpRequestACK.currentValue,
                                     &OLA_Timers[CONST_LA2TSwitchBS_TimerID],
                                     &OLA_Timers[CONST_LA2TS_UpBtnPrsAckTimerID],
                                     &pOLA_ButtonStuckFlag->LA2TSButtonStuckFlag,
                                     &pALD_InitializeStates->initialize_LA2TS,
                                     &LiftAxle2TRIDEMSwitchSM_State,
                                     &pRteOutData_LiftAxle->LiftAxle1AutoLiftRequest,
                                     &pRteOutData_LiftAxle->LiftAxle1LiftPositionRequest);
      }
   }
   else
   {
      // Do nothing: keep previous value
   }
   //! ##### Processing lift axle switch status error logic : 'LiftAxleSwitchStatusError()'
   LiftAxleSwitchStatusError(&pRteInData_LiftAxle->LiftAxle1SwitchStatus,
                             &pRteInData_LiftAxle->LiftAxle2SwitchStatus,
                             &pRteInData_LiftAxle->LiftAxle1TRIDEMSwitchStatus,
                             &pRteInData_LiftAxle->LiftAxle2TRIDEMSwitchStatus,
                             &pRteInData_LiftAxle->LiftAxle2MaxTractSwitchStatus,
                             &pRteInData_LiftAxle->LiftAxle1MaxTractSwitchStatus,
                             &pRteOutData_LiftAxle->LiftAxle1LiftPositionRequest,
                             &pRteOutData_LiftAxle->LiftAxle1AutoLiftRequest);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'RatioSwitchStateMachine' logic
//!
//! \param   *pRteInData_RatioSwitch          Providing the data of input ports
//! \param   *pLoadDistributionChangeACK      Providing the input port value
//! \param   *pLoadDistributionFuncSelected   Providing the input port value
//! \param   RS_Timers                        Providing the timer values
//! \param   *pALD_InitializeStates           Providing the current ALD state initialization status
//! \param   *pRteOutData_RatioSwitch         Providing the output structure
//!
//!======================================================================================
static void RatioSwitchStateMachine(      AxleLoadDistribution_RatioSwitchInStruct  *pRteInData_RatioSwitch,
                                    const LoadDistributionChangeACK_T               *pLoadDistributionChangeACK,
                                    const LoadDistributionFuncSelected_T            *pLoadDistributionFuncSelected,
                                          uint16                                    RS_Timers[CONST_NbOfTimers],
                                          ALD_HMICtrl_InitializeStatesStruct        *pALD_InitializeStates,
                                          AxleLoadDistribution_RatioSwitchOutStruct *pRteOutData_RatioSwitch)
{  
   //! ##### Initialize the one ratio switch states
   static AldRS_States Ratio3SwitchSM_States = { SM_ALD_RS_Stand_By,
                                                 SM_ALD_RS_Default };
   static AldRS_States Ratio1SwitchSM_States = { SM_ALD_RS_Stand_By,
                                                 SM_ALD_RS_Default };
   static AldRS_States Ratio2SwitchSM_States = { SM_ALD_RS_Stand_By,
                                                 SM_ALD_RS_Default };
   static AldRS_States Ratio6SwitchSM_States = { SM_ALD_RS_Stand_By,
                                                 SM_ALD_RS_Default };
   //! ##### Select 'AxleLoad_RatioTagOrLoadDistrib' parameter
   // Ratio switch - Ratio Charge push switch
   if (TRUE == PCODE_P1CZ1_AxleLoad_RatioTagOrLoadDistrib)
   {
      //! #### Process ratio switch state machine logic : 'RatioSwitchStateTransitions()'
      RatioSwitchStateTransitions(&pRteInData_RatioSwitch->Ratio3SwitchStatus,
                                  pLoadDistributionChangeACK,
                                  &RS_Timers[CONST_Ratio3SwitchBS_TimerID],
                                  &RS_Timers[CONST_R3S_ChangeLoadDisTimerID],
                                  &pALD_InitializeStates->initialize_R3S,
                                  &Ratio3SwitchSM_States,
                                  &pRteOutData_RatioSwitch->LoadDistributionChangeRequest);
   }
   //! ##### Select 'AxleLoad_RatioPusherRocker' parameter
   // VT- Ratio Switch :: Push Configuration
   else if (TRUE == PCODE_P1CZ2_AxleLoad_RatioPusherRocker)
   {
      //! #### Process ratio switch state machine logic : 'RatioSwitchStateTransitions()'
      RatioSwitchStateTransitions(&pRteInData_RatioSwitch->Ratio1SwitchStatus,
                                  pLoadDistributionChangeACK,
                                  &RS_Timers[CONST_Ratio1SwitchBS_TimerID],
                                  &RS_Timers[CONST_R1S_ChangeLoadDisTimerID],
                                  &pALD_InitializeStates->initialize_R1S,
                                  &Ratio1SwitchSM_States,
                                  &pRteOutData_RatioSwitch->LoadDistributionChangeRequest);
      //! #### Process Ratio1_DeviceIndication logic : 'DeviceIndication()'
      DeviceIndication(pLoadDistributionFuncSelected,
                       &pRteOutData_RatioSwitch->Ratio1_DeviceIndication);
   }
   //! ##### Select 'AxleLoad_RatioTagRocker' parameter
   // VT- Ratio Switch : Tag Configuration
   else if (TRUE == PCODE_P1CZ3_AxleLoad_RatioTagRocker)
   {
      //! #### Process ratio switch state machine logic : 'RatioSwitchStateTransitions()'
      RatioSwitchStateTransitions(&pRteInData_RatioSwitch->Ratio2SwitchStatus,
                                  pLoadDistributionChangeACK,
                                  &RS_Timers[CONST_Ratio2SwitchBS_TimerID],
                                  &RS_Timers[CONST_R2S_ChangeLoadDisTimerID],
                                  &pALD_InitializeStates->initialize_R2S,
                                  &Ratio2SwitchSM_States,
                                  &pRteOutData_RatioSwitch->LoadDistributionChangeRequest);
      //! #### Process Ratio2_DeviceIndication logic : 'DeviceIndication()'
      DeviceIndication(pLoadDistributionFuncSelected,
                       &pRteOutData_RatioSwitch->Ratio2_DeviceIndication);
   }
   //! ##### Select 'AxleLoad_RatioRoadGripPusher' parameter
   // UD- Ratio Switch
   else if (TRUE == PCODE_P1M5B_AxleLoad_RatioRoadGripPusher)
   {
      //! #### Process ratio switch state machine logic : 'RatioSwitchStateTransitions()'
      RatioSwitchStateTransitions(&pRteInData_RatioSwitch->Ratio6SwitchStatus,
                                  pLoadDistributionChangeACK,
                                  &RS_Timers[CONST_Ratio6SwitchBS_TimerID],
                                  &RS_Timers[CONST_R6S_ChangeLoadDisTimerID],
                                  &pALD_InitializeStates->initialize_R6S,
                                  &Ratio6SwitchSM_States,
                                  &pRteOutData_RatioSwitch->LoadDistributionChangeRequest);
      //! #### Process Ratio6_DeviceIndication logic : 'UD_DeviceIndication()'
      UD_DeviceIndication(pLoadDistributionFuncSelected,
                          &pRteOutData_RatioSwitch->Ratio6_DeviceIndication);
   }
   else
   {
      // Fallback Logic: Transfer the input faults to outputs 
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'LiftAxlePositionswitchStateMachine' logic
//!
//! \param   *pLiftAxle1Switch2_Status            Providing the data of input port
//! \param   *pLiftAxle1UpRequestACK              Providing the data of input port
//! \param   *pLiftAxle1PositionStatus            Providing the data of input port
//! \param   LAP_Timers                           Providing the timer values
//! \param   *pALD_InitializeStates               Providing the current ALD state initialization status
//! \param   *pLiftAxle1LiftPositionRequest       Providing the output port value
//! \param   *pBogieSwitch_DeviceIndication       Providing the output port value
//!
//!======================================================================================
static void LiftAxlePositionswitchStateMachine(      A3PosSwitchStatus                       *pLiftAxle1Switch2_Status,
                                               const LiftAxleUpRequestACK                    *pLiftAxle1UpRequestACK,
                                               const LiftAxlePositionStatus_T                *pLiftAxle1PositionStatus,
                                                     uint16                                  LAP_Timers[CONST_NbOfTimers],
                                                     ALD_HMICtrl_InitializeStatesStruct      *pALD_InitializeStates, 
                                                     LiftAxleLiftPositionRequest_T           *pLiftAxle1LiftPositionRequest,
                                                     DualDeviceIndication_T                  *pBogieSwitch_DeviceIndication)
{
   //! ##### Initialize the ALD lift axle position switch states
   static AldLAPS_States LAPSwitchSM_State = { SM_LAPS_Lift,
                                               SM_LAPS_Default };
   //! #### Processing lift axle position switch transition logic : 'LiftAxlePositionSwitchTransition()'
   LiftAxlePositionSwitchTransition(pLiftAxle1Switch2_Status,
                                    &pLiftAxle1UpRequestACK->currentValue,
                                    &LAP_Timers[CONST_LA1UpReqACK_TimerID],
                                    &LAP_Timers[CONST_LiftAxle1S2BS_TimerID],
                                    &pALD_InitializeStates->initialize_LAPS,
                                    &LAPSwitchSM_State);
   //! #### Processing lift axle position switch output action logic : 'LiftAxlePositionSwitchOutputProcessing()'
   LiftAxlePositionSwitchOutputProcessing(&LAPSwitchSM_State,
                                          pLiftAxle1Switch2_Status,
                                          pLiftAxle1LiftPositionRequest);
   //! #### Process lift axle position LED logic : 'LiftAxlePositionLEDLogic()'
   LiftAxlePositionLEDLogic(pLiftAxle1Switch2_Status,
                            pLiftAxle1PositionStatus,
                            pBogieSwitch_DeviceIndication);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'DualRatioSwitchStateMachine' logic
//!
//! \param   *pRatio4SwitchStatus              Providing the data of input port
//! \param   *pRatio5SwitchStatus              Providing the data of input port
//! \param   *pLoadDistributionRequestedACK    Providing the input port value
//! \param   *pLoadDistributionSelected        Providing the input port value
//! \param   DRS_Timers                        Providing the timer values
//! \param   *pALD_InitializeStates            Providing the current ALD state initialization status
//! \param   *pRatio4_DeviceIndication         Providing the output port value
//! \param   *pRatio5_DeviceIndication         Providing the output port value
//! \param   *pLoadDistributionRequested       Providing the output port value
//!
//!======================================================================================
static void DualRatioSwitchStateMachine(      A3PosSwitchStatus                     *pRatio4SwitchStatus,
                                              A3PosSwitchStatus                     *pRatio5SwitchStatus,
                                        const LoadDistributionRequestedACK_T        *pLoadDistributionRequestedACK,
                                        const LoadDistributionSelect                *pLoadDistributionSelected,
                                              uint16                                DRS_Timers[CONST_NbOfTimers],
                                              ALD_HMICtrl_InitializeStatesStruct    *pALD_InitializeStates,
                                              DualDeviceIndication_T                *pRatio4_DeviceIndication,
                                              DualDeviceIndication_T                *pRatio5_DeviceIndication,
                                              LoadDistributionRequested_T           *pLoadDistributionRequested)
{
   //! ##### Initialize the dual ratio switch states
   static DualRatioSwitch_States Ratio4SwitchSM_States = { SM_DRS_DefaultLoadDistribution,
                                                           SM_DRS_NoActionRequested };
   static DualRatioSwitch_States Ratio5SwitchSM_States = { SM_DRS_DefaultLoadDistribution,
                                                           SM_DRS_NoActionRequested };
   //! ##### Conditions check to Implement 'Dual RatioSwitch State Transitions' logic
   //! ##### Select 'AxleLoad_BoggieDualRatio' parameter
   if (TRUE == PCODE_P1J6C_AxleLoad_BoggieDualRatio)
   {
      //! #### Process dual ratio switch state machine logic : 'DualRatioSwitchStateTransitions()'
      DualRatioSwitchStateTransitions(pRatio4SwitchStatus,
                                      pLoadDistributionSelected,
                                      pLoadDistributionRequestedACK,
                                      &DRS_Timers[CONST_Ratio4SwitchBS_TimerID],
                                      &DRS_Timers[CONST_R4S_LoadDisReqACKTimerID],
                                      &pALD_InitializeStates->initialize_R4S,
                                      &Ratio4SwitchSM_States);
      //! #### Processing dual ratio switch output action logic : 'DualRatioSwitchOutputProcessing()'
      DualRatioSwitchOutputProcessing(&Ratio4SwitchSM_States,
                                      pRatio4SwitchStatus,
                                      pLoadDistributionRequested);
      //! #### Process ratio4 device indication logic : 'DualRatioSwitchDeviceIndication()'
      DualRatioSwitchDeviceIndication(pLoadDistributionSelected,
                                      pRatio4_DeviceIndication);
   }
   //! ##### Select 'AxleLoad_TridemDualRatio' parameter
   else if (TRUE == PCODE_P1J6D_AxleLoad_TridemDualRatio)
   {
      //! #### Process dual ratio switch state machine logic : 'DualRatioSwitchStateTransitions()'
      DualRatioSwitchStateTransitions(pRatio5SwitchStatus,
                                      pLoadDistributionSelected,
                                      pLoadDistributionRequestedACK,
                                      &DRS_Timers[CONST_Ratio5SwitchBS_TimerID],
                                      &DRS_Timers[CONST_R5S_LDisReqACKTimerID],
                                      &pALD_InitializeStates->initialize_R5S,
                                      &Ratio5SwitchSM_States);
      //! #### Processing dual ratio switch output action logic : 'DualRatioSwitchOutputProcessing()'
      DualRatioSwitchOutputProcessing(&Ratio5SwitchSM_States,
                                      pRatio5SwitchStatus,
                                      pLoadDistributionRequested);
      //! #### Process ratio5 device indication logic : 'DualRatioSwitchDeviceIndication()'
      DualRatioSwitchDeviceIndication(pLoadDistributionSelected,
                                      pRatio5_DeviceIndication);
   }
   else
   {
      // Do nothing, keep previous status   
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'ARideStateMachine' logic
//!
//! \param   *pLiftAxle2MaxTractSwitchStatus        Providing the data of input port
//! \param   *pLiftAxle1UpRequestACK                Providing the data of input port
//! \param   *pLoadDistributionSelected             Providing the input port value
//! \param   ARide_Timers                           Providing the timer values
//! \param   *pALD_InitializeStates                 Providing the current ALD state initialization status
//! \param   *pLiftAxle1LiftPositionRequest         Providing the output port value
//! \param   *pLiftAxle1DirectControl               Providing the output port value
//! \param   *pMaxTract_DeviceIndication            Providing the output port value
//!
//!======================================================================================
static void ARideStateMachine(      A3PosSwitchStatus                     *pLiftAxle2MaxTractSwitchStatus,
                                    LiftAxleUpRequestACK                  *pLiftAxle1UpRequestACK,
                              const LoadDistributionSelect                *pLoadDistributionSelected,
                                    uint16                                ARide_Timers[CONST_NbOfTimers],
                                    ALD_HMICtrl_InitializeStatesStruct    *pALD_InitializeStates,
                                    LiftAxleLiftPositionRequest_T         *pLiftAxle1LiftPositionRequest,
                                    LiftAxleLiftPositionRequest_T         *pLiftAxle1DirectControl,
                                    DeviceIndication_T                    *pMaxTract_DeviceIndication)
{
   //! ##### Initialize the ARide states
   static AldARide_States ARideSM_States = { SM_ALD_ARide_Short_LiftRequest,
                                             SM_ALD_ARide_Default };
   //! #### Process ARide switch state machine logic : 'ARideStateTransitions()'
   ARideStateTransitions(pLiftAxle2MaxTractSwitchStatus,
                         pLiftAxle1UpRequestACK,
                         &ARide_Timers[CONST_LA2MaxTractTimerID],
                         &ARide_Timers[CONST_LA1UpRequestTimerID],
                         &ARide_Timers[CONST_LA1UpReq_NoACK_TimerID],
                         &pALD_InitializeStates->initialize_LA2MTS,
                         &ARideSM_States);
   //! #### Process ARide state machine output action logic : 'ARideStateOutputProcessing()'
   ARideStateOutputProcessing(&ARideSM_States,
                              pLiftAxle1LiftPositionRequest,
                              pLiftAxle1DirectControl);
   //! #### Process ARide switch LED logic : 'ARideSwitchLEDLogic()'
   ARideSwitchLEDLogic(pLoadDistributionSelected,
                       pMaxTract_DeviceIndication);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'ALDSwitchStateMachine' logic
//!
//! \param   *pALDSwitchStatus                  Providing the data of input port
//! \param   *pTridemALDSwitchStatus            Providing the data of input port
//! \param   *pLoadDistributionALDChangeACK     Providing the input port value
//! \param   *pLoadDistributionSelected         Providing the input port value
//! \param   ALD_Timers                         Providing the timer values
//! \param   *pALD_InitializeStates             Providing the current ALD state initialization status
//! \param   *pALD_DeviceIndication             Providing the output port value
//! \param   *pTridemALD_DeviceIndication       Providing the output port value
//! \param   *pAltLoadDistribution_rqst         Providing the output port value
//!
//!======================================================================================
static void ALDSwitchStateMachine(      A2PosSwitchStatus                       *pALDSwitchStatus,
                                        A2PosSwitchStatus                       *pTridemALDSwitchStatus,
                                  const LoadDistributionALDChangeACK_T          *pLoadDistributionALDChangeACK,
                                  const LoadDistributionSelect                  *pLoadDistributionSelected,
                                        uint16                                  ALD_Timers[CONST_NbOfTimers],
                                        ALD_HMICtrl_InitializeStatesStruct      *pALD_InitializeStates,
                                        DeviceIndication_T                      *pALD_DeviceIndication,
                                        DeviceIndication_T                      *pTridemALD_DeviceIndication,
                                        AltLoadDistribution_rqst_T              *pAltLoadDistribution_rqst)
{
   //! ##### Initialize the ALD switch states
   static ALDSwitch_States ALDSwitchSM_State       = { SM_ALD_Switch_LastFunc,
                                                       SM_ALD_Switch_Default };
   static ALDSwitch_States TridemALDSwitchSM_State = { SM_ALD_Switch_LastFunc,
                                                       SM_ALD_Switch_Default };

   //! ##### Check for 'AxleLoad_AccessoryBoggieALD' parameter status
   // VT- ALD Switch :: Boggie ALD Configuration
   if (TRUE == PCODE_P1BOS_AxleLoad_AccessoryBoggieALD)
   {
      //! #### Process ALD switch statemachine logic : 'ALDSwitchStateTransitions()'
      ALDSwitchStateTransitions(pALDSwitchStatus,
                                pLoadDistributionALDChangeACK,
                                pLoadDistributionSelected,
                                &ALD_Timers[CONST_ALDSwitchBS_TimerID],
                                &pALD_InitializeStates->initialize_ALDS,
                                &ALDSwitchSM_State);
      //! #### Process ALD switch state machine output action logic : 'ALDSwitchStatusOutputProcessing()'
      ALDSwitchStatusOutputProcessing(&ALDSwitchSM_State,
                                      pALDSwitchStatus,
                                      pAltLoadDistribution_rqst);
      //! #### Process ALD device indication logic : 'ALDDeviceIndication()'
      ALDDeviceIndication(pLoadDistributionSelected,
                          pALD_DeviceIndication);
   }
   //! ##### Check for 'AxleLoad_AccessoryTridemALD' parameter status
      // VT- ALD Switch :: Tridem ALD Configuration
   else if (TRUE == PCODE_P1J6B_AxleLoad_AccessoryTridemALD)
   {
      //! #### Process ALD switch statemachine logic : 'ALDSwitchStateTransitions()'
      ALDSwitchStateTransitions(pTridemALDSwitchStatus,
                                pLoadDistributionALDChangeACK,
                                pLoadDistributionSelected,
                                &ALD_Timers[CONST_TridemALDSwitchBSTimerID],
                                &pALD_InitializeStates->initialize_TridemALDS,
                                &TridemALDSwitchSM_State);
      //! #### Processing ALD switch state machine output action logic : 'ALDSwitchStatusOutputProcessing()'
      ALDSwitchStatusOutputProcessing(&TridemALDSwitchSM_State,
                                      pTridemALDSwitchStatus,
                                      pAltLoadDistribution_rqst);
      //! #### Process TridemALD device indication logic : 'ALDDeviceIndication()'
      ALDDeviceIndication(pLoadDistributionSelected,
                          pTridemALD_DeviceIndication);
   }
   else
   {
      // Fallback Logic: Transfer the input faults to outputs
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'RatioALDSwitchStateMachine' logic
//!
//! \param   *pRatioALDSwitchStatus                 Providing the data of input port
//! \param   *pLoadDistributionALDChangeACK         Providing the input port value
//! \param   *pRteInData_Common                     Providing the input port value
//! \param   RatioALD_Timers                        Providing the timer values
//! \param   *pALD_InitializeStates                 Providing the current ARide state initialization status
//! \param   *pLoadDistributionChangeRequest        Providing the output port value
//! \param   *pAltLoadDistribution_rqst             Providing the output port value
//! \param   *pRatioALD_DualDeviceIndication        Providing the output port value
//!
//!======================================================================================
static void RatioALDSwitchStateMachine(      A3PosSwitchStatus                          *pRatioALDSwitchStatus,
                                       const LoadDistributionALDChangeACK_T             *pLoadDistributionALDChangeACK,
                                       const AxleLoadDistribution_HMICtrl_InStruct      *pRteInData_Common,
                                             uint16                                     RatioALD_Timers[CONST_NbOfTimers],
                                             ALD_HMICtrl_InitializeStatesStruct         *pALD_InitializeStates,
                                             LoadDistributionChangeRequest_T            *pLoadDistributionChangeRequest,
                                             AltLoadDistribution_rqst_T                 *pAltLoadDistribution_rqst,
                                             DualDeviceIndication_T                     *pRatioALD_DualDeviceIndication)
{
   //! ##### Initialize the ratio ALD switch states
   static RatioALDSwitch_States RatioALDSwitchSM_State = { SM_ALD_RatioALD_LastFunc,
                                                           SM_ALD_RatioALD_Default };
   //! #### Process RatioALD switch state machine logic : 'RatioALDSwitchTransitions()'
   RatioALDSwitchTransitions(pRatioALDSwitchStatus,
                             &pRteInData_Common->LoadDistributionSelected,
                             pLoadDistributionALDChangeACK,
                             &pRteInData_Common->LoadDistributionChangeACK,
                             &pRteInData_Common->LoadDistributionFuncSelected,
                             &RatioALD_Timers[CONST_RatioALDSwitchBS_TimerID],
                             &RatioALD_Timers[CONST_RatioALD_ACK_TimerID],
                             &pALD_InitializeStates->initialize_RatioALDS,
                             &RatioALDSwitchSM_State);
   //! #### Process RatioALDSwitch state machine output action logic : 'RatioALDSwitchOutputProcessing()'
   RatioALDSwitchOutputProcessing(&RatioALDSwitchSM_State,
                                  pLoadDistributionChangeRequest,
                                  pAltLoadDistribution_rqst);
   //! #### Prcoess RatioALD LED logic : 'RatioALD_LED_Logic()'
   RatioALD_LED_Logic(&pRteInData_Common->LoadDistributionFuncSelected,
                      pRatioALD_DualDeviceIndication);
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'OneLiftAxleStateTransitions' logic
//!
//! \param   *pLiftAxleSwitchStatus           Indicating the current LiftAxleSwitch Status
//! \param   *pLiftAxle1UpRequestACK          Specify the current status
//! \param   *pTimer_ButtonStuck              To check current timer value and update
//! \param   *pTimer_UpButtonPrsAcknowledge   To check current timer value and update
//! \param   *pTimer_ButtonStuckFlag          Update the button stuck timer flag value
//! \param   *pinitialize_OLA                 Providing and updating one lift axle state initialization status
//! \param   *pOneLiftAxleSM                  Providing and updating the present OneLiftAxle State
//! \param   *pLiftAxleAutoLiftRequest        Providing value of LiftAxleAutoLiftRequest
//! \param   *pLiftAxleLiftPositionRequest    Providing value of LiftAxleLiftPositionRequest
//!
//!====================================================================================== 
static void OneLiftAxleStateTransitions(      A3PosSwitchStatus               *pLiftAxleSwitchStatus,
                                        const LiftAxleUpRequestACK_T          *pLiftAxle1UpRequestACK,
                                              uint16                          *pTimer_ButtonStuck,
                                              uint16                          *pTimer_UpButtonPrsAcknowledge,
                                              uint8                           *pTimer_ButtonStuckFlag,
                                              uint8                           *pinitialize_OLA,
                                              SM_OneLiftAxleStates            *pOneLiftAxleSM,
                                              InactiveActive_T                *pLiftAxleAutoLiftRequest,
                                              LiftAxleLiftPositionRequest_T   *pLiftAxleLiftPositionRequest)
{  
   //! ###### Processing one lift axle state transition logic
   uint8 current_state               = CONST_ResetFlag;
   pOneLiftAxleSM->currentValue      = pOneLiftAxleSM->newValue;
   if (CONST_NonInitialize == *pinitialize_OLA)
   {
      pLiftAxleSwitchStatus->previousValue = pLiftAxleSwitchStatus->currentValue;
      *pTimer_ButtonStuck                  = CONST_TimerFunctionInactive;
      *pTimer_UpButtonPrsAcknowledge       = CONST_TimerFunctionInactive;
      *pTimer_ButtonStuckFlag              = CONST_ResetFlag;
      pOneLiftAxleSM->newValue             = SM_ALD_OLA_Default;
      *pinitialize_OLA                     = CONST_Initialize;
   }
   else
   {
      //! ###### Select the state based on 'pOneLiftAxleSM->newValue'
      switch (pOneLiftAxleSM->newValue)
      {         
         //! ##### Processing conditions for state change from 'OLA_AutoIdle' state
         case SM_ALD_OLA_AutoIdle:
            //! #### Check the lift axle switch status and LiftAxle1UpRequestACK status
            //! #### Check for the state changes to 'OLA_Down'(Down Pressed event).
            if ((A3PosSwitchStatus_Lower != pLiftAxleSwitchStatus->previousValue) 
               && (A3PosSwitchStatus_Lower == pLiftAxleSwitchStatus->currentValue))
            {
               pOneLiftAxleSM->newValue = SM_ALD_OLA_Down;
            }
            //! #### Check for the state changes to 'OLA_Lift'(Up Pressed and System Ready event).
            else if (((A3PosSwitchStatus_Upper != pLiftAxleSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Upper == pLiftAxleSwitchStatus->currentValue))
                    && (LiftAxleUpRequestACK_NoAction == *pLiftAxle1UpRequestACK))
            {
               // Start UpButtonPressAcknowledge Timer
               *pTimer_UpButtonPrsAcknowledge = CONST_Axleload_Timeout;
               pOneLiftAxleSM->newValue       = SM_ALD_OLA_Lift;
            }
            //! #### Check for the state changes to 'OLA_Fallback'(Error Detection event).
            else if ((A3PosSwitchStatus_Error != pLiftAxleSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Error == pLiftAxleSwitchStatus->currentValue))
            {
               pOneLiftAxleSM->newValue = SM_ALD_OLA_Fallback;
            }
            else
            {
               // No event, keep the state until timeout occurs
            }
            //! #### Process the A3_ButtonStuckEvent (Button stuck event) : 'A3_ButtonStuckEvent()'
            current_state = A3_ButtonStuckEvent(pLiftAxleSwitchStatus,
                                                pTimer_ButtonStuck,
                                                pTimer_ButtonStuckFlag);
         break;
         //! ##### Processing conditions for state change from 'OLA_Lift' state.
         case SM_ALD_OLA_Lift:
            //! #### Check the lift axle switch status and LiftAxle1UpRequestACK status
            //! #### Check for the state changes to 'OLA_Down' (Down pressed event).
            if ((A3PosSwitchStatus_Lower != pLiftAxleSwitchStatus->previousValue)
               && (A3PosSwitchStatus_Lower == pLiftAxleSwitchStatus->currentValue))
            {
               pOneLiftAxleSM->newValue = SM_ALD_OLA_Down;
            }
            //! #### Check for the state changes to 'OLA_AutoIdle'(Acknowledge event).
            else if (((LiftAxleUpRequestACK_ChangeAcknowledged == *pLiftAxle1UpRequestACK)
                    || (LiftAxleUpRequestACK_LiftDeniedFrontOverload == *pLiftAxle1UpRequestACK)
                    || (LiftAxleUpRequestACK_LiftDeniedRearOverload == *pLiftAxle1UpRequestACK)
                    || (LiftAxleUpRequestACK_LiftDeniedPBrakeActive == *pLiftAxle1UpRequestACK)
                    || (LiftAxleUpRequestACK_SystemDeniedVersatile == *pLiftAxle1UpRequestACK))
                    && (CONST_TimerFunctionInactive != *pTimer_UpButtonPrsAcknowledge))
            {
               // Cancel UpButtonPressAcknowledge Timer
               *pTimer_UpButtonPrsAcknowledge = CONST_TimerFunctionInactive;
               pOneLiftAxleSM->newValue       = SM_ALD_OLA_AutoIdle;
            }
            else if ((A3PosSwitchStatus_Upper != pLiftAxleSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Upper == pLiftAxleSwitchStatus->currentValue)
                    && (CONST_TimerFunctionInactive == *pTimer_UpButtonPrsAcknowledge))
            {
               // Start UpButtonPressAcknowledge Timer
               *pTimer_UpButtonPrsAcknowledge = CONST_Axleload_Timeout;
            }
            //! #### Check for the state changes to 'OLA_Fallback'(Error detection event).
            else if ((A3PosSwitchStatus_Error != pLiftAxleSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Error == pLiftAxleSwitchStatus->currentValue))
            {
               pOneLiftAxleSM->newValue = SM_ALD_OLA_Fallback;
            }
            //! #### Check for the state changes to 'OLA_Fallback'(Button stuck event)
            else if ((CONST_TimerFunctionInactive == *pTimer_ButtonStuck)
                    && (CONST_SetFlag == *pTimer_ButtonStuckFlag))
            {
               pOneLiftAxleSM->newValue = SM_ALD_OLA_Fallback;
               Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_FAILED);
            }
            else
            {
               // No event, keep the state until timeout occurs
            }
            if ((pLiftAxleSwitchStatus->previousValue != pLiftAxleSwitchStatus->currentValue)
               && ((A3PosSwitchStatus_Upper == pLiftAxleSwitchStatus->currentValue)
               || (A3PosSwitchStatus_Lower == pLiftAxleSwitchStatus->currentValue)))
            {
               // Start button stuck timer
               *pTimer_ButtonStuck     = CONST_ButtonStuckTimerPeriod;
               *pTimer_ButtonStuckFlag = CONST_SetFlag;
            }
            else if ((pLiftAxleSwitchStatus->previousValue != pLiftAxleSwitchStatus->currentValue)
                     && ((A3PosSwitchStatus_Upper != pLiftAxleSwitchStatus->currentValue)
                     && (A3PosSwitchStatus_Lower != pLiftAxleSwitchStatus->currentValue)))
            {
               // Cancel button stuck timer
               *pTimer_ButtonStuck      = CONST_TimerFunctionInactive;
               *pTimer_ButtonStuckFlag  = CONST_ResetFlag;
            }
            else
            {
               //// Do nothing: keep previous value
            }
         break;
         //! ##### Processing conditions for state change from 'OLA_Down' state
         case SM_ALD_OLA_Down:
            //! #### Check the lift axle switch Status and LiftAxle1UpRequestACK status
            //! #### Check for the state changes to 'OLA_AutoIdle'(Switch to middle position Event)
            if ((A3PosSwitchStatus_Middle != pLiftAxleSwitchStatus->previousValue)
               && (A3PosSwitchStatus_Middle == pLiftAxleSwitchStatus->currentValue))
            {
               pOneLiftAxleSM->newValue = SM_ALD_OLA_AutoIdle;
            }
            else if ((A3PosSwitchStatus_Upper != pLiftAxleSwitchStatus->previousValue) 
                    && (A3PosSwitchStatus_Upper == pLiftAxleSwitchStatus->currentValue))
            {
               //! #### Check for the state changes to 'OLA_Lift'(Up pressed and system ready event).
               if (LiftAxleUpRequestACK_NoAction == *pLiftAxle1UpRequestACK)
               {
                  // Start UpButtonPressAcknowledge Timer
                  *pTimer_UpButtonPrsAcknowledge = CONST_Axleload_Timeout;
                  pOneLiftAxleSM->newValue       = SM_ALD_OLA_Lift;
               }
               //! #### Check for the state changes to 'OLA_AutoIdle'(Up pressed, system not ready event).
               else
               {
                  pOneLiftAxleSM->newValue = SM_ALD_OLA_AutoIdle;
               }
            }
            //! #### Check for the state changes to 'OLA_Fallback'(Error detection event).
            else if ((A3PosSwitchStatus_Error != pLiftAxleSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Error == pLiftAxleSwitchStatus->currentValue))
            {
               pOneLiftAxleSM->newValue = SM_ALD_OLA_Fallback;
            }
            else
            {
               // No event, keep the state until timeout occurs
            }
            //! #### Process the A3_ButtonStuckEvent (Button stuck event) : 'A3_ButtonStuckEvent()'
            current_state = A3_ButtonStuckEvent(pLiftAxleSwitchStatus,
                                                pTimer_ButtonStuck,
                                                pTimer_ButtonStuckFlag);
         break;
         //! ##### Processing conditions for state change from 'OLA_Fallback' state
         case SM_ALD_OLA_Fallback:
            //! #### Check for the state changes to 'OLA_Down'(Solve Error Event).
            if ((pLiftAxleSwitchStatus->currentValue != pLiftAxleSwitchStatus->previousValue)
               && (A3PosSwitchStatus_Error != pLiftAxleSwitchStatus->currentValue))
            {
               pOneLiftAxleSM->newValue = SM_ALD_OLA_Down;
               Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            }
            else
            {
               // Do nothing, keep previous status
            }
            //! #### Process the A3_ButtonStuckEvent (Button stuck event) : 'A3_ButtonStuckEvent()'
            current_state = A3_ButtonStuckEvent(pLiftAxleSwitchStatus,
                                                pTimer_ButtonStuck,
                                                pTimer_ButtonStuckFlag);
         break;
         //! ##### Processing conditions for state change from 'OLA_Default' state.
         default: // SM_ALD_OLA_Default
            //! #### Check for lift axle switch status
            //! #### Check for the state changes to 'OLA_Down'(Down position event).
            if (A3PosSwitchStatus_Lower == pLiftAxleSwitchStatus->currentValue)
            {
               // Start button stuck timer
               *pTimer_ButtonStuck       = CONST_ButtonStuckTimerPeriod;
               *pTimer_ButtonStuckFlag   = CONST_SetFlag;
               pOneLiftAxleSM->newValue  = SM_ALD_OLA_Down;
            }
            //! #### Check for the state changes to 'OLA_AutoIdle'(Middle position event).
            else if (A3PosSwitchStatus_Middle == pLiftAxleSwitchStatus->currentValue)
            {
               pOneLiftAxleSM->newValue = SM_ALD_OLA_AutoIdle;
               Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            }
            //! #### Check for the state changes to 'OLA_AutoIdle'(Up position check event).
            else if (A3PosSwitchStatus_Upper == pLiftAxleSwitchStatus->currentValue)
            {
               // Start button stuck timer
               *pTimer_ButtonStuck      = CONST_ButtonStuckTimerPeriod;
               *pTimer_ButtonStuckFlag  = CONST_SetFlag;
               pOneLiftAxleSM->newValue = SM_ALD_OLA_AutoIdle;
            }
            //! #### Check for the state changes to 'OLA_Fallback' (Error detection event).
            else if ((A3PosSwitchStatus_Error != pLiftAxleSwitchStatus->previousValue)
                    && (A3PosSwitchStatus_Error == pLiftAxleSwitchStatus->currentValue))
            {
               pOneLiftAxleSM->newValue = SM_ALD_OLA_Fallback;
            }
            else
            {
               // do nothing: ignore value, keep previous status
            }
         break;
      }
      if (CONST_SetFlag == current_state)
        {
           pOneLiftAxleSM->newValue = SM_ALD_OLA_Fallback;
        }
      else
      {
         // Do nothing: keep previous status
      }
   }
   // Process output only if currentValue is not equal to newValue
   if (pOneLiftAxleSM->currentValue != pOneLiftAxleSM->newValue)
   {
      //! ##### Processing one lift axle state machine output action logic : 'OneLiftAxleStateOutputProcessing()'
      OneLiftAxleStateOutputProcessing(pOneLiftAxleSM,
                                       pLiftAxleAutoLiftRequest,
                                       pLiftAxleLiftPositionRequest);
   }
   else
   {
      // Do nothing, keep previous status
   }
} 
//!======================================================================================
//!
//! \brief
//! This function is processing the 'OneLiftAxleStateOutputProcessing'
//!
//! \param   *pOneLiftAxleSM                 Providing the present OneLiftAxle state 
//! \param   *pLiftAxleAutoLiftRequest       Updating output AutoLiftRequest status
//! \param   *pLiftAxleLiftPositionRequest   Updating output LiftPositionRequest status
//!
//!======================================================================================
static void OneLiftAxleStateOutputProcessing(const SM_OneLiftAxleStates          *pOneLiftAxleSM,
                                                   InactiveActive_T              *pLiftAxleAutoLiftRequest,
                                                   LiftAxleLiftPositionRequest_T *pLiftAxleLiftPositionRequest)
{
   //! ###### Processing one lift axle state output processing logic
   //! ##### Select the state based on 'pOneLiftAxleSM->newValue'
   switch (pOneLiftAxleSM->newValue)
   {
      //! #### Output actions for 'OLA_Lift' state
      case SM_ALD_OLA_Lift:
         *pLiftAxleAutoLiftRequest     = InactiveActive_Active;
         *pLiftAxleLiftPositionRequest = LiftAxleLiftPositionRequest_Up;
      break;
      //! #### Output actions for 'OLA_Down' state
      case SM_ALD_OLA_Down:
         *pLiftAxleAutoLiftRequest     = InactiveActive_Inactive;
         *pLiftAxleLiftPositionRequest = LiftAxleLiftPositionRequest_Down;
      break;
         //! #### Output actions for 'OLA_Fallback' state
      case SM_ALD_OLA_Fallback:
         *pLiftAxleAutoLiftRequest     = InactiveActive_Error;
         *pLiftAxleLiftPositionRequest = LiftAxleLiftPositionRequest_Error;
      break;
         //! #### Output actions for 'OLA_AutoIdle' state
      case SM_ALD_OLA_AutoIdle:
         *pLiftAxleAutoLiftRequest     = InactiveActive_Active;
         *pLiftAxleLiftPositionRequest = LiftAxleLiftPositionRequest_Idle;
      break;
         //! #### No output action for 'OLA_Default' state
      default: //SM_ALD_OLA_Default
         // Do nothing, keep previous status
      break;
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'LiftAxleSwitchStatusError'
//!
//! \param  *pLiftAxle1SwitchStatus           Indicating current axle1 switch status
//! \param  *pLiftAxle2SwitchStatus           Specify the current axle1 switch status
//! \param  *pLiftAxle1TRIDEMSwitchStatus     Indicating current axle1 TRIDEM switch status
//! \param  *pLiftAxle2TRIDEMSwitchStatus     Specify the current axle1 TRIDEM switch status
//! \param  *pLiftAxle2MaxTractSwitchStatus   Indicating current axle1 MaxTract switch status
//! \param  *pLiftAxle1MaxTractSwitchStatus   Specify the current axle1 MaxTract switch status
//! \param  *pLiftAxle1LiftPositionReq        Updating output axle1 LiftPositionReq status
//! \param  *pLiftAxle1AutoLiftReq            Updating output axle1 AutoLiftReq status
//! 
//!=======================================================================================
static void LiftAxleSwitchStatusError(const A3PosSwitchStatus             *pLiftAxle1SwitchStatus,
                                      const A3PosSwitchStatus             *pLiftAxle2SwitchStatus,
                                      const A3PosSwitchStatus             *pLiftAxle1TRIDEMSwitchStatus,
                                      const A3PosSwitchStatus             *pLiftAxle2TRIDEMSwitchStatus,
                                      const A3PosSwitchStatus             *pLiftAxle2MaxTractSwitchStatus,
                                      const A3PosSwitchStatus             *pLiftAxle1MaxTractSwitchStatus,
                                            LiftAxleLiftPositionRequest_T *pLiftAxle1LiftPositionReq,
                                            InactiveActive_T              *pLiftAxle1AutoLiftReq)
{
   //! ###### Processing lift axle switch status input signal error logic
   //! ##### Check the switch status for outputs set to 'Error' value
   if (((TRUE == PCODE_P1CZW_AxleLoad_OneLiftPusher)
      && (A3PosSwitchStatus_Error == pLiftAxle1SwitchStatus->currentValue))
      || ((TRUE == PCODE_P1CZX_AxleLoad_OneLiftAxleMaxTraction)
      && (A3PosSwitchStatus_Error == pLiftAxle2SwitchStatus->currentValue))
      || ((TRUE == PCODE_P1CZY_AxleLoad_OneLiftTag)
      && (A3PosSwitchStatus_Error == pLiftAxle2SwitchStatus->currentValue))
      || ((TRUE == PCODE_P1BOX_AxleLoad_TridemFirstAxleLift)
      && (A3PosSwitchStatus_Error == pLiftAxle1TRIDEMSwitchStatus->currentValue))
      || ((TRUE == PCODE_P1BOY_AxleLoad_TridemSecondAxleLift)
      && (A3PosSwitchStatus_Error == pLiftAxle2TRIDEMSwitchStatus->currentValue))
      || ((TRUE == PCODE_P1CZ0_AxleLoad_MaxTractionTag)
      && (A3PosSwitchStatus_Error == pLiftAxle2MaxTractSwitchStatus->currentValue))
      || ((TRUE == PCODE_P1CZZ_AxleLoad_MaxTractionPusher)
      && (A3PosSwitchStatus_Error == pLiftAxle1MaxTractSwitchStatus->currentValue)))
   {
      //! #### If switch status is error then make output as error
      *pLiftAxle1LiftPositionReq = LiftAxleLiftPositionRequest_Error;
      *pLiftAxle1AutoLiftReq     = InactiveActive_Error;
   }
   else
   {
      // Do nothing, keep previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'OneLiftAxleConfigurationErrorDetection'
//!
//! \return uint8    Returns the 'count' value
//! 
//!=======================================================================================
static uint8 OneLiftAxleConfigurationErrorDetection(void)
{
   //! ###### Processing one lift axle configuration error detection logic
   uint8 count = 0U;
   //! ##### Check for parameters of OneLiftAxle is True then increase the count value
   //! ##### Check for 'AxleLoad_OneLiftPusher' is parameter 'True'
   if (TRUE == PCODE_P1CZW_AxleLoad_OneLiftPusher)
   {
      count = count + 1U;
   }
   else
   {
      // Do nothing, keep previous status
   } 
   //! ##### Check for 'AxleLoad_OneLiftAxleMaxTraction' parameter status
   if (TRUE == PCODE_P1CZX_AxleLoad_OneLiftAxleMaxTraction)
   {
      count = count + 1U;
   }
   else
   {
      // Do nothing, keep previous status
   }
   //! ##### Check for 'AxleLoad_OneLiftTag' is parameter status
   if (TRUE == PCODE_P1CZY_AxleLoad_OneLiftTag)
   {
      count = count + 1U;
   }
   else
   {
      // Do nothing, keep previous status
   }
   //! ##### Check for 'AxleLoad_MaxTractionPusher' parameter status
   if (TRUE == PCODE_P1CZZ_AxleLoad_MaxTractionPusher)
   {
      count = count + 1U;
   }
   else
   {
      // Do nothing, keep previous status
   }
   //! ##### Check for 'AxleLoad_MaxTractionTag' parameter status
   if (TRUE == PCODE_P1CZ0_AxleLoad_MaxTractionTag)
   {
      count = count + 1U;
   }  
   else
   {
      // Do nothing, keep previous status
   }
   //! ##### Check for 'AxleLoad_TridemFirstAxleLift' and 'AxleLoad_TridemSecondAxleLift' parameter status
   if ((TRUE == PCODE_P1BOX_AxleLoad_TridemFirstAxleLift)
      && (FALSE == PCODE_P1BOY_AxleLoad_TridemSecondAxleLift))
   {
      count = count + 1U;
   }
   else
   {
      // Do nothing, keep previous status
   }
   //! ##### Check for 'AxleLoad_TridemSecondAxleLift' status
   if (TRUE == PCODE_P1BOY_AxleLoad_TridemSecondAxleLift)
   {
      //! ##### Check for 'AxleLoad_TridemFirstAxleLift' status
      if (TRUE == PCODE_P1BOX_AxleLoad_TridemFirstAxleLift)
      {
         count = count + 1U;
      }
      else
      {
         count = count + 1U;
      }
   }
   return count;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'RatioSwitchConfigurationErrorDetection'
//!
//! \return uint8    Returns the 'count_RS' value
//! 
//!======================================================================================
static uint8 RatioSwitchConfigurationErrorDetection(void)
{
   //! ###### Processing ratio switch configuration error detection logic
   uint8 count_RS = 0U;
   //! ##### Check for parameters of RatioSwitch is True then increase the count value
   //! ##### Check for 'AxleLoad_RatioTagOrLoadDistrib' parameter status
   if (TRUE == PCODE_P1CZ1_AxleLoad_RatioTagOrLoadDistrib)
   {
      count_RS = count_RS + 1U;
   }
   else
   {
      // Do nothing, keep previous status
   }
   //! ##### Check for 'AxleLoad_RatioRoadGripPusher' parameter status
   if (TRUE == PCODE_P1M5B_AxleLoad_RatioRoadGripPusher)
   {
      count_RS = count_RS + 1U;
   }
   else
   {
      // Do nothing, keep previous status
   }
   //! ##### Check for 'AxleLoad_RatioPusherRocker' parameter status
   if (TRUE == PCODE_P1CZ2_AxleLoad_RatioPusherRocker)
   {
      count_RS = count_RS + 1U;
   }
   else
   {
      // Do nothing, keep previous status
   }
   //! ##### Check for 'AxleLoad_RatioTagRocker'  parameter status
   if (TRUE == PCODE_P1CZ3_AxleLoad_RatioTagRocker)
   {
      count_RS = count_RS + 1U;
   }
   else
   {
      // Do nothing, keep previous status
   }   
   return count_RS;
}
//!======================================================================================
//!
//! \brief
//! This function is processing the 'RatioSwitchStateTransitions'
//!
//! \param   *pRatioSwitchStatus              Providing current switch status value
//! \param   *pLoadDistributionChangeACK      Indicating Present value for LoadDistributionChangeACK
//! \param   *pTimer_RatioSwitchButtonStuck   To check current Timer value and update
//! \param   *pTimer_ChangeLoadDistribution   To check current Timer value and update
//! \param   *pinitialize_RS                  Providing and updating RatioSwitch initialization stauts
//! \param   *pRatioSwitchSM                  Providing and Updating present RatioSwitch State
//! \param   *pLoadDistributionChangeRequest  Providing present RatioSwitch State
//!
//!====================================================================================== 
static void RatioSwitchStateTransitions(      A2PosSwitchStatus               *pRatioSwitchStatus,
                                        const LoadDistributionChangeACK_T     *pLoadDistributionChangeACK,
                                              uint16                          *pTimer_RatioSwitchButtonStuck,
                                              uint16                          *pTimer_ChangeLoadDistribution,
                                              uint8                           *pinitialize_RS,
                                              AldRS_States                    *pRatioSwitchSM,
                                              LoadDistributionChangeRequest_T *pLoadDistributionChangeRequest)
{
   static uint8 ratioSwitchBS_Flag  = CONST_ResetFlag;
   uint8 current_state              = CONST_ResetFlag;
   pRatioSwitchSM->currentValue     = pRatioSwitchSM->newValue;
   if (CONST_NonInitialize == *pinitialize_RS)
   {
      pRatioSwitchStatus->previousValue = pRatioSwitchStatus->currentValue;
      *pTimer_RatioSwitchButtonStuck    = CONST_TimerFunctionInactive;
      *pTimer_ChangeLoadDistribution    = CONST_TimerFunctionInactive;
      ratioSwitchBS_Flag                = CONST_ResetFlag;
      pRatioSwitchSM->newValue          = SM_ALD_RS_Default;
      *pinitialize_RS                   = CONST_Initialize;
   }
   else
   {
      //! ###### Processing ratio switch state transition logic
      //! ##### Select the state based on 'pRatioSwitchSM->newValue'
      switch (pRatioSwitchSM->newValue)
      {
            //! ##### Processing conditions for state changes from 'RS_StandBy' state.
         case SM_ALD_RS_Stand_By:
            //! #### Check for ratio switch status
            //! #### Check for the state changes to 'RS_Change_Load_Req'(Button pushed event).
            if ((A2PosSwitchStatus_On != pRatioSwitchStatus->previousValue)
               && (A2PosSwitchStatus_On == pRatioSwitchStatus->currentValue))
            {
               // Start ChangeLoadDistribution timer
               *pTimer_ChangeLoadDistribution = CONST_Axleload_Timeout;
               pRatioSwitchSM->newValue       = SM_ALD_RS_Change_Load_Req;
            }
            //! #### Check for state change to 'RS_Fallback' (Error Detection event).
            else if ((A2PosSwitchStatus_Error != pRatioSwitchStatus->previousValue)
                    && (A2PosSwitchStatus_Error == pRatioSwitchStatus->currentValue))
            {
               pRatioSwitchSM->newValue = SM_ALD_RS_Fallback;
            }
            else
            {
               // Do nothing, keep previous status
            }
            //! #### Process the A2_ButtonStuckEvent (Button stuck event) : 'A2_ButtonStuckEvent()'
            current_state = A2_ButtonStuckEvent(pRatioSwitchStatus,
                                                pTimer_RatioSwitchButtonStuck,
                                                &ratioSwitchBS_Flag);
         break;
            //! ##### Processing conditions for state change from 'RS_Change_Load_Request' state.
         case SM_ALD_RS_Change_Load_Req:
            //! #### Check for LoadDistributionChangeAcknowledge value and RatioSwitch Status
            //! #### Check for the state changes to 'RS_Change_Ack'(Acknowledge event).
            if ((LoadDistributionChangeACK_ChangeAcknowledged == *pLoadDistributionChangeACK)
               || (LoadDistributionChangeACK_LoadShiftDeniedFALIMOverload == *pLoadDistributionChangeACK)
               || (LoadDistributionChangeACK_SystemDeniedVersatile == *pLoadDistributionChangeACK))
            {
               // Cancel ChangeLoadDistribution timer
               *pTimer_ChangeLoadDistribution = CONST_TimerFunctionInactive;
               pRatioSwitchSM->newValue       = SM_ALD_RS_Change_Ack;
            }
            //! #### Check for the state changes to 'RS_Fallback'(Error detection event).
            else if ((A2PosSwitchStatus_Error != pRatioSwitchStatus->previousValue)
                    && (A2PosSwitchStatus_Error == pRatioSwitchStatus->currentValue))
            {
               pRatioSwitchSM->newValue = SM_ALD_RS_Fallback;
            }
            //! #### Check for the state changes to 'RS_Stand_By'(No acknowledge event).
            else if (CONST_TimerFunctionElapsed == *pTimer_ChangeLoadDistribution)
            {             
               pRatioSwitchSM->newValue = SM_ALD_RS_Stand_By;
            }
            else
            {
               // Do nothing, keep previous status
            }
         break;
            //! ##### Processing conditions for state change from 'RS_Change_Ack' state.
         case SM_ALD_RS_Change_Ack:
            //! #### Check for  LoadDistributionChangeACK and ratio switch status
            //! #### Check for the state changes to 'RS_Stand_By' (No action event).
            if (LoadDistributionChangeACK_NoAction == *pLoadDistributionChangeACK)
            {
               // Cancel ChangeLoadDistribution timer
               *pTimer_ChangeLoadDistribution = CONST_TimerFunctionInactive;
               pRatioSwitchSM->newValue       = SM_ALD_RS_Stand_By;
            }
            //! #### Check for the state changes to 'RS_Fallback' (Error detection event).
            else if ((A2PosSwitchStatus_Error != pRatioSwitchStatus->previousValue)
                    && (A2PosSwitchStatus_Error == pRatioSwitchStatus->currentValue))
            {
               pRatioSwitchSM->newValue = SM_ALD_RS_Fallback;
            }
            else if ((CONST_TimerFunctionInactive == *pTimer_RatioSwitchButtonStuck)
                    && (CONST_SetFlag == ratioSwitchBS_Flag))
            {
               // Cancel button stuck timer
               pRatioSwitchSM->newValue = SM_ALD_RS_Fallback; 
               Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_FAILED);
            }
            else
            {
               // Do nothing: keep previous status
            }
            if ((pRatioSwitchStatus->previousValue != pRatioSwitchStatus->currentValue)
               && (A2PosSwitchStatus_On == pRatioSwitchStatus->currentValue))
            {
               // Start button stuck timer
               *pTimer_RatioSwitchButtonStuck = CONST_ButtonStuckTimerPeriod;
               ratioSwitchBS_Flag             = CONST_SetFlag;
            }
            else if ((pRatioSwitchStatus->previousValue != pRatioSwitchStatus->currentValue)
                     && (A2PosSwitchStatus_On != pRatioSwitchStatus->currentValue))
            {
               // Cancel button stuck timer
               ratioSwitchBS_Flag             = CONST_ResetFlag;
               *pTimer_RatioSwitchButtonStuck = CONST_TimerFunctionInactive;
            }
            else
            {
                // Do nothing: keep previous value
            }
         break;
            //! ##### Processing conditions for state change from 'RS_Fallback' state.
         case SM_ALD_RS_Fallback:
            //! #### Check for the ratio switch status
            //! #### Check for the state changes to 'RS_Default' (solve error event).
            if ((pRatioSwitchStatus->currentValue != pRatioSwitchStatus->previousValue)
               && (A2PosSwitchStatus_Error != pRatioSwitchStatus->currentValue))
            {
               // Cancel ButtonStuck Timer
               *pTimer_RatioSwitchButtonStuck = CONST_TimerFunctionInactive;
               ratioSwitchBS_Flag             = CONST_ResetFlag;
               pRatioSwitchSM->newValue       = SM_ALD_RS_Default;
               Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            }
            else
            {
               // Do nothing, keep previous status
            }
         break;
            //! ##### Processing conditions for state change from 'RS_Default' state.
         default: //SM_ALD_RS_Default
            //! #### Check for the ratio switch and LoadDistributionChangeACK status
            //! #### Check for the state changes to 'RS_Stand_By'(Button not pushed event).
            if ((A2PosSwitchStatus_Off == pRatioSwitchStatus->currentValue)
               && (LoadDistributionChangeACK_NotAvailable != *pLoadDistributionChangeACK))
            {
               // Cancel ButtonStuck Timer    
               *pTimer_RatioSwitchButtonStuck = CONST_TimerFunctionInactive;
               ratioSwitchBS_Flag             = CONST_ResetFlag;
               pRatioSwitchSM->newValue       = SM_ALD_RS_Stand_By;
               Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(DEM_EVENT_STATUS_PASSED);
            }
            //! #### Check for the state changes to 'RS_Fallback' (Error detection event).
            else if ((A2PosSwitchStatus_Error != pRatioSwitchStatus->previousValue)
                    && (A2PosSwitchStatus_Error == pRatioSwitchStatus->currentValue))
            {
               // Cancel ButtonStuck Timer
               *pTimer_RatioSwitchButtonStuck = CONST_TimerFunctionInactive;
               ratioSwitchBS_Flag             = CONST_ResetFlag;
               pRatioSwitchSM->newValue       = SM_ALD_RS_Fallback;
            }
            else
            {
               // Do nothing, keep previous status
            }
         break;
      }
      if (CONST_SetFlag == current_state)
      {
         pRatioSwitchSM->newValue = SM_ALD_RS_Fallback;
      }
      else
      {
         // Do nothing: keep previous status
      }
   }
   // Process output only if currentValue is not equal to newValue
   if (pRatioSwitchSM->currentValue != pRatioSwitchSM->newValue)
   {
      //! ##### Processing ratio switch state machine output action logic : 'RatioSwitchStateOutputProcessing()'
      RatioSwitchStateOutputProcessing(pRatioSwitchSM,
                                       pLoadDistributionChangeRequest);
   }
   else
   {
      // Do nothing, keep previous status
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'RatioSwitchStateOutputProcessing'
//!
//! \param   *pRatioSwitchSM                   Update 'Output Actions' based on 'pRatioSwitchSM' value as per conditions
//! \param   *pLoadDistributionChangeRequest   Update 'pLoadDistributionChangeRequest' value as per conditions
//!
//!=======================================================================================
static void RatioSwitchStateOutputProcessing(const AldRS_States                    *pRatioSwitchSM,
                                                   LoadDistributionChangeRequest_T *pLoadDistributionChangeRequest)
{
   //! ###### Process ratio switch state output processing 
   //! ##### Process output actions for particular state of ratio switch state machine
   //! ##### Process the output based on 'pRatioSwitchSM->newValue'
   switch (pRatioSwitchSM->newValue)
   {
      //! #### Output action for ratio switch 'Stand By' state.
      case SM_ALD_RS_Stand_By:
         *pLoadDistributionChangeRequest = LoadDistributionChangeRequest_Idle;
      break;
      //! #### Output action for ratio switch 'Change Load Req' state.
      case SM_ALD_RS_Change_Load_Req:
         *pLoadDistributionChangeRequest = LoadDistributionChangeRequest_ChangeLoadDistribution;
      break;
      //! #### Output action for ratio switch 'Change Ack' state.
      case SM_ALD_RS_Change_Ack:
         *pLoadDistributionChangeRequest = LoadDistributionChangeRequest_Idle;
      break;
      //! #### Output action for ratio switch 'Fallback' state.
      case SM_ALD_RS_Fallback:
         *pLoadDistributionChangeRequest = LoadDistributionChangeRequest_Error;
      break;
      //! #### Output action for ratio switch 'Default' state.
      default: //SM_ALD_RS_Default
         *pLoadDistributionChangeRequest = LoadDistributionChangeRequest_Idle;
      break;
   }
} 
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'DeviceIndication'
//!
//! \param   *pLoadDistributionFuncSelected   Indicating the current value for LoadDistributionFuncSelected
//! \param   *pDeviceIndication               Updating the Device indication based on conditions
//!
//!======================================================================================
static void DeviceIndication(const LoadDistributionFuncSelected_T *pLoadDistributionFuncSelected,
                                   DeviceIndication_T             *pDeviceIndication)
{
   //! ###### Processing the device indication logic for LoadDistribution function
   //! ##### Check for LoadDistributionFuncSelected status
   if (LoadDistributionFuncSelected_MaximumTractionTractionHelp == *pLoadDistributionFuncSelected)
   {
      *pDeviceIndication = DeviceIndication_On;
   }
   else if (LoadDistributionFuncSelected_MaximumTractionStartingHelp == *pLoadDistributionFuncSelected)
   {
      *pDeviceIndication = DeviceIndication_Blink;
   }
   else
   {
      *pDeviceIndication = DeviceIndication_Off;
   }  
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'UD_DeviceIndication'
//!
//! \param   *pUD_LoadDistributionFuncSelected   Indicating the current value for UD_LoadDistributionFuncSelected
//! \param   *pUD_DeviceIndication               Updating the Device indication based on conditions
//!
//!======================================================================================
static void UD_DeviceIndication(const LoadDistributionFuncSelected_T *pUD_LoadDistributionFuncSelected,
                                      DeviceIndication_T             *pUD_DeviceIndication)
{
   //! ###### Processing the UD DeviceIndication logic
   //! ##### Check for LoadDistributionFuncSelected status
   if ((LoadDistributionFuncSelected_OptimisedTraction == *pUD_LoadDistributionFuncSelected)
      || (LoadDistributionFuncSelected_MaximumTractionStartingHelp == *pUD_LoadDistributionFuncSelected))
   {
      *pUD_DeviceIndication = DeviceIndication_On;
   }
   else
   {
      *pUD_DeviceIndication = DeviceIndication_Off;
   }
}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
