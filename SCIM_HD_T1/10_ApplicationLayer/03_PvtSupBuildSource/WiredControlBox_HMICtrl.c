/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  WiredControlBox_HMICtrl.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  WiredControlBox_HMICtrl
 *  Generated at:  Fri Jun 12 17:10:07 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <WiredControlBox_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_ECSStopButtonHoldTimeout_P1DWI_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1BWF_Level_Memorization_Min_Press_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1BWF_Memory_Recall_Max_Press_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1BWF_Memory_Recall_Min_Press_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1JSY_DriveStroke_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1JSY_RampStroke_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1JSZ_DriveStroke_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1JSZ_RampStroke_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_RCECSButtonStucked_P1DWJ_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_RCECSUpDownStucked_P1DWK_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_RCECS_HoldCircuitTimer_P1IUS_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_WiredControlBox_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_ECSStopButtonHoldTimeout_P1DWI_T: Integer in interval [0...255]
 * SEWS_P1BWF_Level_Memorization_Min_Press_T: Integer in interval [0...255]
 * SEWS_P1BWF_Memory_Recall_Max_Press_T: Integer in interval [0...255]
 * SEWS_P1BWF_Memory_Recall_Min_Press_T: Integer in interval [0...255]
 * SEWS_P1JSY_DriveStroke_T: Integer in interval [0...255]
 * SEWS_P1JSY_RampStroke_T: Integer in interval [0...255]
 * SEWS_P1JSZ_DriveStroke_T: Integer in interval [0...255]
 * SEWS_P1JSZ_RampStroke_T: Integer in interval [0...255]
 * SEWS_RCECSButtonStucked_P1DWJ_T: Integer in interval [0...255]
 * SEWS_RCECSUpDownStucked_P1DWK_T: Integer in interval [0...255]
 * SEWS_RCECS_HoldCircuitTimer_P1IUS_T: Integer in interval [0...255]
 * ShortPulseMaxLength_T: Integer in interval [0...15]
 *   Unit: [ms], Factor: 1, Offset: 0
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * ECSStandByReq_T: Enumeration of integer in interval [0...7] with enumerators
 *   ECSStandByReq_Idle (0U)
 *   ECSStandByReq_StandbyRequested (1U)
 *   ECSStandByReq_StopStandby (2U)
 *   ECSStandByReq_Reserved (3U)
 *   ECSStandByReq_Reserved_01 (4U)
 *   ECSStandByReq_Reserved_02 (5U)
 *   ECSStandByReq_Error (6U)
 *   ECSStandByReq_NotAvailable (7U)
 * EvalButtonRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   EvalButtonRequest_Neutral (0U)
 *   EvalButtonRequest_EvaluatingPush (1U)
 *   EvalButtonRequest_ContinuouslyPushed (2U)
 *   EvalButtonRequest_ShortPush (3U)
 *   EvalButtonRequest_Spare1 (4U)
 *   EvalButtonRequest_Spare2 (5U)
 *   EvalButtonRequest_Error (6U)
 *   EvalButtonRequest_NotAvailable (7U)
 * FalseTrue_T: Enumeration of integer in interval [0...3] with enumerators
 *   FalseTrue_False (0U)
 *   FalseTrue_True (1U)
 *   FalseTrue_Error (2U)
 *   FalseTrue_NotAvaiable (3U)
 * LevelAdjustmentAction_T: Enumeration of integer in interval [0...15] with enumerators
 *   LevelAdjustmentAction_Idle (0U)
 *   LevelAdjustmentAction_UpBasic (1U)
 *   LevelAdjustmentAction_DownBasic (2U)
 *   LevelAdjustmentAction_UpShortMovement (3U)
 *   LevelAdjustmentAction_DownShortMovement (4U)
 *   LevelAdjustmentAction_Reserved (5U)
 *   LevelAdjustmentAction_GotoDriveLevel (6U)
 *   LevelAdjustmentAction_Reserved_01 (7U)
 *   LevelAdjustmentAction_Ferry (8U)
 *   LevelAdjustmentAction_Reserved_02 (9U)
 *   LevelAdjustmentAction_Reserved_03 (10U)
 *   LevelAdjustmentAction_Reserved_04 (11U)
 *   LevelAdjustmentAction_Reserved_05 (12U)
 *   LevelAdjustmentAction_Reserved_06 (13U)
 *   LevelAdjustmentAction_Error (14U)
 *   LevelAdjustmentAction_NotAvailable (15U)
 * LevelAdjustmentAxles_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelAdjustmentAxles_Rear (0U)
 *   LevelAdjustmentAxles_Front (1U)
 *   LevelAdjustmentAxles_Parallel (2U)
 *   LevelAdjustmentAxles_Reserved (3U)
 *   LevelAdjustmentAxles_Reserved_01 (4U)
 *   LevelAdjustmentAxles_Reserved_02 (5U)
 *   LevelAdjustmentAxles_Error (6U)
 *   LevelAdjustmentAxles_NotAvailable (7U)
 * LevelAdjustmentStroke_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelAdjustmentStroke_DriveStroke (0U)
 *   LevelAdjustmentStroke_DockingStroke (1U)
 *   LevelAdjustmentStroke_Reserved (2U)
 *   LevelAdjustmentStroke_Reserved_01 (3U)
 *   LevelAdjustmentStroke_Reserved_02 (4U)
 *   LevelAdjustmentStroke_Reserved_03 (5U)
 *   LevelAdjustmentStroke_Error (6U)
 *   LevelAdjustmentStroke_NotAvailable (7U)
 * LevelControlInformation_T: Enumeration of integer in interval [0...15] with enumerators
 *   LevelControlInformation_Prohibit (0U)
 *   LevelControlInformation_HeightAdjustment (1U)
 *   LevelControlInformation_DockingHeight (2U)
 *   LevelControlInformation_DriveHeight (3U)
 *   LevelControlInformation_KneelingHeight (4U)
 *   LevelControlInformation_FerryFunction (5U)
 *   LevelControlInformation_LimpHome (6U)
 *   LevelControlInformation_Reserved (7U)
 *   LevelControlInformation_Reserved_01 (8U)
 *   LevelControlInformation_Reserved_02 (9U)
 *   LevelControlInformation_Reserved_03 (10U)
 *   LevelControlInformation_Reserved_04 (11U)
 *   LevelControlInformation_Reserved_05 (12U)
 *   LevelControlInformation_Reserved_06 (13U)
 *   LevelControlInformation_Error (14U)
 *   LevelControlInformation_NotAvailable (15U)
 * LevelUserMemoryAction_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelUserMemoryAction_Inactive (0U)
 *   LevelUserMemoryAction_Recall (1U)
 *   LevelUserMemoryAction_Store (2U)
 *   LevelUserMemoryAction_Default (3U)
 *   LevelUserMemoryAction_Reserved (4U)
 *   LevelUserMemoryAction_Reserved_01 (5U)
 *   LevelUserMemoryAction_Error (6U)
 *   LevelUserMemoryAction_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * RampLevelRequest_T: Enumeration of integer in interval [0...15] with enumerators
 *   RampLevelRequest_TakeNoAction (0U)
 *   RampLevelRequest_RampLevelM1 (1U)
 *   RampLevelRequest_RampLevelM2 (2U)
 *   RampLevelRequest_RampLevelM3 (3U)
 *   RampLevelRequest_RampLevelM4 (4U)
 *   RampLevelRequest_RampLevelM5 (5U)
 *   RampLevelRequest_RampLevelM6 (6U)
 *   RampLevelRequest_RampLevelM7 (7U)
 *   RampLevelRequest_NotDefined_04 (8U)
 *   RampLevelRequest_NotDefined_05 (9U)
 *   RampLevelRequest_NotDefined_06 (10U)
 *   RampLevelRequest_NotDefined_07 (11U)
 *   RampLevelRequest_NotDefined_08 (12U)
 *   RampLevelRequest_NotDefined_09 (13U)
 *   RampLevelRequest_ErrorIndicator (14U)
 *   RampLevelRequest_NotAvailable (15U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 * WiredLevelUserMemory_T: Enumeration of integer in interval [0...15] with enumerators
 *   WiredLevelUserMemory_MemOff (0U)
 *   WiredLevelUserMemory_M1 (1U)
 *   WiredLevelUserMemory_M2 (2U)
 *   WiredLevelUserMemory_M3 (3U)
 *   WiredLevelUserMemory_M4 (4U)
 *   WiredLevelUserMemory_M5 (5U)
 *   WiredLevelUserMemory_M6 (6U)
 *   WiredLevelUserMemory_M7 (7U)
 *   WiredLevelUserMemory_Spare (8U)
 *   WiredLevelUserMemory_Spare01 (9U)
 *   WiredLevelUserMemory_Spare02 (10U)
 *   WiredLevelUserMemory_Spare03 (11U)
 *   WiredLevelUserMemory_Spare04 (12U)
 *   WiredLevelUserMemory_Spare05 (13U)
 *   WiredLevelUserMemory_Error (14U)
 *   WiredLevelUserMemory_NotAvailable (15U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *   FALSE (0U)
 *   TRUE (1U)
 *
 * Record Types:
 * =============
 * SEWS_ECS_MemSwTimings_P1BWF_s_T: Record with elements
 *   Level_Memorization_Min_Press of type SEWS_P1BWF_Level_Memorization_Min_Press_T
 *   Memory_Recall_Min_Press of type SEWS_P1BWF_Memory_Recall_Min_Press_T
 *   Memory_Recall_Max_Press of type SEWS_P1BWF_Memory_Recall_Max_Press_T
 * SEWS_ForcedPositionStatus_Front_P1JSY_s_T: Record with elements
 *   DriveStroke of type SEWS_P1JSY_DriveStroke_T
 *   RampStroke of type SEWS_P1JSY_RampStroke_T
 * SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T: Record with elements
 *   DriveStroke of type SEWS_P1JSZ_DriveStroke_T
 *   RampStroke of type SEWS_P1JSZ_RampStroke_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_ECSStopButtonHoldTimeout_P1DWI_T Rte_Prm_P1DWI_ECSStopButtonHoldTimeout_v(void)
 *   SEWS_RCECSButtonStucked_P1DWJ_T Rte_Prm_P1DWJ_RCECSButtonStucked_v(void)
 *   SEWS_RCECSUpDownStucked_P1DWK_T Rte_Prm_P1DWK_RCECSUpDownStucked_v(void)
 *   SEWS_RCECS_HoldCircuitTimer_P1IUS_T Rte_Prm_P1IUS_RCECS_HoldCircuitTimer_v(void)
 *   SEWS_ECS_MemSwTimings_P1BWF_s_T *Rte_Prm_P1BWF_ECS_MemSwTimings_v(void)
 *   SEWS_ForcedPositionStatus_Front_P1JSY_s_T *Rte_Prm_P1JSY_ForcedPositionStatus_Front_v(void)
 *   SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T *Rte_Prm_P1JSZ_ForcedPositionStatus_Rear_v(void)
 *   boolean Rte_Prm_P1ALT_ECS_PartialAirSystem_v(void)
 *   boolean Rte_Prm_P1ALU_ECS_FullAirSystem_v(void)
 *
 *********************************************************************************************************************/


#define WiredControlBox_HMICtrl_START_SEC_CODE
#include "WiredControlBox_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: WiredControlBox_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AdjustButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BackButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BlinkECSWiredLEDs_BlinkECSWiredLEDs(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_LevelControlInformation_LevelControlInformation(LevelControlInformation_T *data)
 *   Std_ReturnType Rte_Read_MemButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_RampLevelRequest_RampLevelRequest(RampLevelRequest_T *data)
 *   Std_ReturnType Rte_Read_SelectButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_StopButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *   Std_ReturnType Rte_Read_WRDownButtonStatus_EvalButtonRequest(EvalButtonRequest_T *data)
 *   Std_ReturnType Rte_Read_WRUpButtonStatus_EvalButtonRequest(EvalButtonRequest_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Adjust_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Down_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_ECSStandByReqRCECS_ECSStandByReqRCECS(ECSStandByReq_T data)
 *   Std_ReturnType Rte_Write_M1_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_M2_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_M3_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_ShortPulseMaxLength_ShortPulseMaxLength(ShortPulseMaxLength_T data)
 *   Std_ReturnType Rte_Write_Up_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_WiredAirSuspensionStopRequest_AirSuspensionStopRequest(FalseTrue_T data)
 *   Std_ReturnType Rte_Write_WiredLevelAdjustmentAction_LevelAdjustmentAction(LevelAdjustmentAction_T data)
 *   Std_ReturnType Rte_Write_WiredLevelAdjustmentAxles_LevelAdjustmentAxles(LevelAdjustmentAxles_T data)
 *   Std_ReturnType Rte_Write_WiredLevelAdjustmentStroke_LevelAdjustmentStroke(LevelAdjustmentStroke_T data)
 *   Std_ReturnType Rte_Write_WiredLevelUserMemory_WiredLevelUserMemory(WiredLevelUserMemory_T data)
 *   Std_ReturnType Rte_Write_WiredLevelUserMemoryAction_LevelUserMemoryAction(LevelUserMemoryAction_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BOI_63_RCECS_ButtonStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: WiredControlBox_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, WiredControlBox_HMICtrl_CODE) WiredControlBox_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: WiredControlBox_HMICtrl_20ms_runnable
 *********************************************************************************************************************/
PushButtonStatus_T StopButtonVar = PushButtonStatus_NotAvailable;
Rte_Read_StopButtonStatus_PushButtonStatus(&StopButtonVar);
Rte_Write_WiredAirSuspensionStopRequest_AirSuspensionStopRequest(StopButtonVar);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: WiredControlBox_HMICtrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: WiredControlBox_HMICtrl_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, WiredControlBox_HMICtrl_CODE) WiredControlBox_HMICtrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: WiredControlBox_HMICtrl_Init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define WiredControlBox_HMICtrl_STOP_SEC_CODE
#include "WiredControlBox_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
