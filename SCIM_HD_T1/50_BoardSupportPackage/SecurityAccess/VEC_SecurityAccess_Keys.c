/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2017 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  VEC_SecurityAccess_Keys.c
 *    Component:  Diag_AsrSwcSecurityAccess_Volvo_AB
 *       Module:  -
 *    Generator:  -
 *
 *  Description:  User keys of module Diag_AsrSwcSecurityAccess_Volvo_AB
 *  
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *********************************************************************************************************************/

#define VEC_SECURITYACCESS_KEYS_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#ifdef DemoSecuritykeys
	
#include "VEC_SecurityAccess_demoKeys.inc"
	
#else
	
#include "VEC_SecurityAcces_RSA_MODULO_01.inc"
#include "VEC_SecurityAccess_Keys.inc"
	
#endif


#include "VEC_SecurityAccess_Keys.h"

/**********************************************************************************************************************
 *  GLOBAL DATA
 **********************************************************************************************************************/

#define MSW_SecurityAccess_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_01 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_01_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_01
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_01_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_01
};

/* Key handle for service 01 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_01 =
{
  sizeof(VEC_SecurityAccess_Key_01_Modulo),
  VEC_SecurityAccess_Key_01_Modulo,
  sizeof(VEC_SecurityAccess_Key_01_Exponent),
  VEC_SecurityAccess_Key_01_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_03 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_03_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_03
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_03_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_03
};

/* Key handle for service 03 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_03 =
{
  sizeof(VEC_SecurityAccess_Key_03_Modulo),
  VEC_SecurityAccess_Key_03_Modulo,
  sizeof(VEC_SecurityAccess_Key_03_Exponent),
  VEC_SecurityAccess_Key_03_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_05 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_05_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_05
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_05_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_05
};

/* Key handle for service 05 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_05 =
{
  sizeof(VEC_SecurityAccess_Key_05_Modulo),
  VEC_SecurityAccess_Key_05_Modulo,
  sizeof(VEC_SecurityAccess_Key_05_Exponent),
  VEC_SecurityAccess_Key_05_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_07 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_07_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_07
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_07_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_07
};

/* Key handle for service 07 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_07 =
{
  sizeof(VEC_SecurityAccess_Key_07_Modulo),
  VEC_SecurityAccess_Key_07_Modulo,
  sizeof(VEC_SecurityAccess_Key_07_Exponent),
  VEC_SecurityAccess_Key_07_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_09 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_09_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_09
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_09_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_09
};

/* Key handle for service 09 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_09 =
{
  sizeof(VEC_SecurityAccess_Key_09_Modulo),
  VEC_SecurityAccess_Key_09_Modulo,
  sizeof(VEC_SecurityAccess_Key_09_Exponent),
  VEC_SecurityAccess_Key_09_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_0B )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_0B_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_0B
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_0B_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_0B
};

/* Key handle for service 0B */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_0B =
{
  sizeof(VEC_SecurityAccess_Key_0B_Modulo),
  VEC_SecurityAccess_Key_0B_Modulo,
  sizeof(VEC_SecurityAccess_Key_0B_Exponent),
  VEC_SecurityAccess_Key_0B_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_0D )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_0D_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_0D
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_0D_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_0D
};

/* Key handle for service 0D */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_0D =
{
  sizeof(VEC_SecurityAccess_Key_0D_Modulo),
  VEC_SecurityAccess_Key_0D_Modulo,
  sizeof(VEC_SecurityAccess_Key_0D_Exponent),
  VEC_SecurityAccess_Key_0D_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_0F )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_0F_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_0F
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_0F_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_0F
};

/* Key handle for service 0F */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_0F =
{
  sizeof(VEC_SecurityAccess_Key_0F_Modulo),
  VEC_SecurityAccess_Key_0F_Modulo,
  sizeof(VEC_SecurityAccess_Key_0F_Exponent),
  VEC_SecurityAccess_Key_0F_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_11 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_11_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_11
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_11_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_11
};

/* Key handle for service 11 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_11 =
{
  sizeof(VEC_SecurityAccess_Key_11_Modulo),
  VEC_SecurityAccess_Key_11_Modulo,
  sizeof(VEC_SecurityAccess_Key_11_Exponent),
  VEC_SecurityAccess_Key_11_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_13 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_13_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_13
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_13_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_13
};

/* Key handle for service 13 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_13 =
{
  sizeof(VEC_SecurityAccess_Key_13_Modulo),
  VEC_SecurityAccess_Key_13_Modulo,
  sizeof(VEC_SecurityAccess_Key_13_Exponent),
  VEC_SecurityAccess_Key_13_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_15 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_15_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_15
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_15_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_15
};

/* Key handle for service 15 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_15 =
{
  sizeof(VEC_SecurityAccess_Key_15_Modulo),
  VEC_SecurityAccess_Key_15_Modulo,
  sizeof(VEC_SecurityAccess_Key_15_Exponent),
  VEC_SecurityAccess_Key_15_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_17 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_17_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_17
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_17_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_17
};

/* Key handle for service 17 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_17 =
{
  sizeof(VEC_SecurityAccess_Key_17_Modulo),
  VEC_SecurityAccess_Key_17_Modulo,
  sizeof(VEC_SecurityAccess_Key_17_Exponent),
  VEC_SecurityAccess_Key_17_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_19 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_19_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_19
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_19_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_19
};

/* Key handle for service 19 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_19 =
{
  sizeof(VEC_SecurityAccess_Key_19_Modulo),
  VEC_SecurityAccess_Key_19_Modulo,
  sizeof(VEC_SecurityAccess_Key_19_Exponent),
  VEC_SecurityAccess_Key_19_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_1B )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_1B_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_1B
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_1B_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_1B
};

/* Key handle for service 1B */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_1B =
{
  sizeof(VEC_SecurityAccess_Key_1B_Modulo),
  VEC_SecurityAccess_Key_1B_Modulo,
  sizeof(VEC_SecurityAccess_Key_1B_Exponent),
  VEC_SecurityAccess_Key_1B_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_1D )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_1D_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_1D
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_1D_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_1D
};

/* Key handle for service 1D */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_1D =
{
  sizeof(VEC_SecurityAccess_Key_1D_Modulo),
  VEC_SecurityAccess_Key_1D_Modulo,
  sizeof(VEC_SecurityAccess_Key_1D_Exponent),
  VEC_SecurityAccess_Key_1D_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_1F )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_1F_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_1F
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_1F_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_1F
};

/* Key handle for service 1F */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_1F =
{
  sizeof(VEC_SecurityAccess_Key_1F_Modulo),
  VEC_SecurityAccess_Key_1F_Modulo,
  sizeof(VEC_SecurityAccess_Key_1F_Exponent),
  VEC_SecurityAccess_Key_1F_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_21 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_21_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_21
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_21_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_21
};

/* Key handle for service 21 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_21 =
{
  sizeof(VEC_SecurityAccess_Key_21_Modulo),
  VEC_SecurityAccess_Key_21_Modulo,
  sizeof(VEC_SecurityAccess_Key_21_Exponent),
  VEC_SecurityAccess_Key_21_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_23 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_23_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_23
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_23_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_23
};

/* Key handle for service 23 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_23 =
{
  sizeof(VEC_SecurityAccess_Key_23_Modulo),
  VEC_SecurityAccess_Key_23_Modulo,
  sizeof(VEC_SecurityAccess_Key_23_Exponent),
  VEC_SecurityAccess_Key_23_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_25 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_25_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_25
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_25_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_25
};

/* Key handle for service 25 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_25 =
{
  sizeof(VEC_SecurityAccess_Key_25_Modulo),
  VEC_SecurityAccess_Key_25_Modulo,
  sizeof(VEC_SecurityAccess_Key_25_Exponent),
  VEC_SecurityAccess_Key_25_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_27 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_27_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_27
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_27_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_27
};

/* Key handle for service 27 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_27 =
{
  sizeof(VEC_SecurityAccess_Key_27_Modulo),
  VEC_SecurityAccess_Key_27_Modulo,
  sizeof(VEC_SecurityAccess_Key_27_Exponent),
  VEC_SecurityAccess_Key_27_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_29 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_29_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_29
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_29_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_29
};

/* Key handle for service 29 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_29 =
{
  sizeof(VEC_SecurityAccess_Key_29_Modulo),
  VEC_SecurityAccess_Key_29_Modulo,
  sizeof(VEC_SecurityAccess_Key_29_Exponent),
  VEC_SecurityAccess_Key_29_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_2B )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_2B_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_2B
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_2B_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_2B
};

/* Key handle for service 2B */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_2B =
{
  sizeof(VEC_SecurityAccess_Key_2B_Modulo),
  VEC_SecurityAccess_Key_2B_Modulo,
  sizeof(VEC_SecurityAccess_Key_2B_Exponent),
  VEC_SecurityAccess_Key_2B_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_2D )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_2D_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_2D
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_2D_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_2D
};

/* Key handle for service 2D */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_2D =
{
  sizeof(VEC_SecurityAccess_Key_2D_Modulo),
  VEC_SecurityAccess_Key_2D_Modulo,
  sizeof(VEC_SecurityAccess_Key_2D_Exponent),
  VEC_SecurityAccess_Key_2D_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_2F )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_2F_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_2F
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_2F_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_2F
};

/* Key handle for service 2F */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_2F =
{
  sizeof(VEC_SecurityAccess_Key_2F_Modulo),
  VEC_SecurityAccess_Key_2F_Modulo,
  sizeof(VEC_SecurityAccess_Key_2F_Exponent),
  VEC_SecurityAccess_Key_2F_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_31 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_31_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_31
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_31_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_31
};

/* Key handle for service 31 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_31 =
{
  sizeof(VEC_SecurityAccess_Key_31_Modulo),
  VEC_SecurityAccess_Key_31_Modulo,
  sizeof(VEC_SecurityAccess_Key_31_Exponent),
  VEC_SecurityAccess_Key_31_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_33 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_33_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_33
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_33_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_33
};

/* Key handle for service 33 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_33 =
{
  sizeof(VEC_SecurityAccess_Key_33_Modulo),
  VEC_SecurityAccess_Key_33_Modulo,
  sizeof(VEC_SecurityAccess_Key_33_Exponent),
  VEC_SecurityAccess_Key_33_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_35 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_35_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_35
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_35_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_35
};

/* Key handle for service 35 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_35 =
{
  sizeof(VEC_SecurityAccess_Key_35_Modulo),
  VEC_SecurityAccess_Key_35_Modulo,
  sizeof(VEC_SecurityAccess_Key_35_Exponent),
  VEC_SecurityAccess_Key_35_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_37 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_37_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_37
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_37_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_37
};

/* Key handle for service 37 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_37 =
{
  sizeof(VEC_SecurityAccess_Key_37_Modulo),
  VEC_SecurityAccess_Key_37_Modulo,
  sizeof(VEC_SecurityAccess_Key_37_Exponent),
  VEC_SecurityAccess_Key_37_Exponent
};
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_39 )
/* Modulo */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_39_Modulo[] =
{ 
  VOLVO_SEWS_SECURITYACCESS_RSA_MODULO_39
};

/* Exponent */
CONST( uint8, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_39_Exponent[] =
{
  VOLVO_SEWS_SECURITYACCESS_RSA_PUBLIC_EXPONENT_39
};

/* Key handle for service 39 */
CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_39 =
{
  sizeof(VEC_SecurityAccess_Key_39_Modulo),
  VEC_SecurityAccess_Key_39_Modulo,
  sizeof(VEC_SecurityAccess_Key_39_Exponent),
  VEC_SecurityAccess_Key_39_Exponent
};
#endif

#define MSW_SecurityAccess_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *  END OF FILE: VEC_SECURITYACCESS_KEYS_SOURCE
 *********************************************************************************************************************/
