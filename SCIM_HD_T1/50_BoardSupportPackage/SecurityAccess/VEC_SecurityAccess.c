/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  VEC_SecurityAccess.c
 *        Config:  D:/GitSCIM/SCIM/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  VEC_SecurityAccess
 *  Generated at:  Tue Oct 15 21:51:07 2019
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <VEC_SecurityAccess>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * AsymDecryptDataBuffer
 *   Used as Buffer for service.
 *
 * AsymDecryptResultBuffer
 *   Used as Buffer for service.
 *
 * AsymPrivateKeyType
 *   This type is used for Csm key parameters of type AsymPrivateKey
 *
 * Csm_ReturnType
 *   The data type Csm_ReturnType indicates the result of a service request.
 *
 * RandomGenerateResultBuffer
 *   Used as Buffer for service.
 *
 * RandomSeedDataBuffer
 *   Used as Buffer for service.
 *
 *
 * Operation Prototypes:
 * =====================
 * AsymDecryptFinish of Port Interface CsmAsymDecrypt
 *   This interface shall be used to finish the asymmetrical decryption service.
 *
 * AsymDecryptStart of Port Interface CsmAsymDecrypt
 *   This interface shall be used to initialize the asymmetrical decryption service of the CSM module.
 *
 * AsymDecryptUpdate of Port Interface CsmAsymDecrypt
 *   This interface shall be used to feed the asymmetrical decryption service with the input data.
 *
 * RandomGenerate of Port Interface CsmRandomGenerate
 *   This interface shall be used to start the random number generation service of the CSM module.
 *
 * RandomSeedFinish of Port Interface CsmRandomSeed
 *   This interface shall be used to finish the random seed service.
 *
 * RandomSeedStart of Port Interface CsmRandomSeed
 *   This interface shall be used to initialize the random seed service of the CSM module.
 *
 * RandomSeedUpdate of Port Interface CsmRandomSeed
 *   This interface shall be used to feed the random seed service with the input data.
 *
 *********************************************************************************************************************/

#include "Rte_VEC_SecurityAccess.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "VEC_SecurityAccess.h"
#include "VEC_SecurityAccess_Keys.h"
#include "VEC_SecurityAccess_Cfg.h"


/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/

 /* Check the version of the header file */
#if (  (VEC_SECURITYACCESS_SW_MAJOR_VERSION != (0x04)) \
    || (VEC_SECURITYACCESS_SW_MINOR_VERSION != (0x00)) \
    || (VEC_SECURITYACCESS_SW_PATCH_VERSION != (0x06)) )
# error "Vendor specific version numbers of VEC_SecurityAccess.c and VEC_SecurityAccess.h are inconsistent"
#endif

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 **********************************************************************************************************************/
/* AR4-220: static keyword is no longer defined by compile abstraction */
#if defined( STATIC )
#else
# define STATIC static
#endif

/** Key size */
#define VEC_SECURITYACCESS_KEY_SIZE  (128u)
/** Seed size */
#define VEC_SECURITYACCESS_SEED_SIZE (116u)
/** Remapping of NRC */
#ifndef VEC_SECURITYACCESS_NRC_INVALIDKEY
# define VEC_SECURITYACCESS_NRC_INVALIDKEY DCM_E_INVALIDKEY
#endif
#ifndef VEC_SECURITYACCESS_NRC_GENERALREJECT
# define VEC_SECURITYACCESS_NRC_GENERALREJECT DCM_E_GENERALREJECT
#endif

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 **********************************************************************************************************************/

/** State of operation (CSM) */
typedef enum
{
  CSMSTATE_INITIAL = 0u,     /**< Initial state */
  CSMSTATE_START,            /**< Trigger start function */
  CSMSTATE_START_PENDING,    /**< Wait for start function result */
  CSMSTATE_UPDATE,           /**< Trigger update function */
  CSMSTATE_UPDATE_PENDING,   /**< Wait for update function result */
  CSMSTATE_FINISH,           /**< Trigger finish function */
  CSMSTATE_FINISH_PENDING,   /**< Wait for finish function result */
  CSMSTATE_IDLE              /**< Idle state */
} Sec_CsmStateType;

/** State of entropy */
typedef enum
{
  ENTROPYSTATE_INVALID = 0u, /**< No entropy feed to RNG yet */
  ENTROPYSTATE_VALID         /**< Entropy feed to RNG at least once */
} Sec_EntropyStateType;

/** State Type */
typedef enum
{
  STATE_INITIAL = 0u,        /**< Initial state */
  STATE_OPERATION_PENDING,   /**< Operation is pending */
  STATE_VALID,               /**< Successful */
  STATE_INVALID              /**< Failed */
} Sec_StateType;

/** Operation Type */
typedef enum
{
  OPERATION_DECRYPT = 0u,    /**< Decrypt operation */
  OPERATION_ENTROPY          /**< Entropy operation */
} Sec_OperationType;

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 **********************************************************************************************************************/
#define VEC_SecurityAccess_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "VEC_SecurityAccess_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/** State of entropy operation feed into random number generator (CSM) */
static VAR(Sec_CsmStateType, VEC_SecurityAccess_VAR_ZERO_INIT)     Sec_EntropyOp;

/** State of entropy */
static VAR(Sec_EntropyStateType, VEC_SecurityAccess_VAR_ZERO_INIT) Sec_EntropyState;

/** Buffer to hold entropy feed into random number generator (CSM) */
static VAR(RandomSeedDataBuffer, VEC_SecurityAccess_VAR_ZERO_INIT) Sec_EntropyBuffer;

/** Length of entropy feed into random number generator (CSM) */
static VAR(UInt32, VEC_SecurityAccess_VAR_ZERO_INIT)               Sec_EntropyLength;

/** State of seed generation */
static VAR(Sec_StateType, VEC_SecurityAccess_VAR_ZERO_INIT)        Sec_SeedState;

/** State of decryption operation (CSM) */
static VAR(Sec_CsmStateType, VEC_SecurityAccess_VAR_ZERO_INIT)     Sec_DecryptOp;

/** State of key verification */
static VAR(Sec_StateType, VEC_SecurityAccess_VAR_ZERO_INIT)        Sec_VerifyState;

#if ( VEC_SECURITYACCESS_RESET_CSM_STATE_BY_FINISH_FUNCTION == STD_ON )
/** Verify Update Failed State */
static VAR(UInt8, VEC_SecurityAccess_VAR_ZERO_INIT)                Sec_VerifyUpdateFailed;
#endif

/** Buffer for the received key from tester */
static VAR(UInt8, VEC_SecurityAccess_VAR_ZERO_INIT)                Sec_ReceivedKeyBuffer[VEC_SECURITYACCESS_KEY_SIZE];

/** Decrypted plain seed key value */
static VAR(UInt8, VEC_SecurityAccess_VAR_ZERO_INIT)                Sec_DecryptedPlainSeedKey[VEC_SECURITYACCESS_SEED_SIZE];

/** Decrypted plain seed key length */
static VAR(UInt32, VEC_SecurityAccess_VAR_ZERO_INIT)               Sec_DecryptedPlainSeedKeyLength;

/** Current seed value */
static VAR(UInt8, VEC_SecurityAccess_VAR_ZERO_INIT)                Sec_CurrentSeedValue[VEC_SECURITYACCESS_SEED_SIZE];

/** RSA key pointer */
static P2CONST(Cry_RsaKeyType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Sec_RsaKeyPtr;

#define VEC_SecurityAccess_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "VEC_SecurityAccess_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 **********************************************************************************************************************/

#define VEC_SecurityAccess_START_SEC_CODE
#include "VEC_SecurityAccess_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

STATIC FUNC(void, VEC_SecurityAccess_CODE) VEC_SecurityAccess_TriggerState(
  P2VAR(Sec_CsmStateType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) statePtr,
  Sec_OperationType operation,
  Csm_ReturnType retVal );

STATIC FUNC(void, VEC_SecurityAccess_CODE) VEC_SecurityAccess_TriggerProcessing(
  P2VAR(Sec_CsmStateType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) statePtr,
  Sec_OperationType operation );

STATIC FUNC(UInt8, VEC_SecurityAccess_CODE) VEC_SecurityAccess_FunctionStart(
  P2CONST(Sec_CsmStateType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) statePtr,
  Sec_OperationType operation );

STATIC FUNC(UInt8, VEC_SecurityAccess_CODE) VEC_SecurityAccess_FunctionUpdate(
  P2CONST(Sec_CsmStateType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) statePtr,
  Sec_OperationType operation );

STATIC FUNC(UInt8, VEC_SecurityAccess_CODE) VEC_SecurityAccess_FunctionFinish(
  P2CONST(Sec_CsmStateType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) statePtr,
  Sec_OperationType operation );

STATIC FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_GetSeed(
  Dcm_OpStatusType OpStatus,
  P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed,
  P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode );

#ifdef RTE_PTR2ARRAYBASETYPE_PASSING
STATIC FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_CompareKey(
  P2CONST(uint8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Key,
  Dcm_OpStatusType OpStatus,
  P2CONST(Cry_RsaKeyType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_CONST) RsaKeyPtr,
  P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) ErrorCode);
#else
STATIC FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_CompareKey(
  P2CONST(uint8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Key,
  Dcm_OpStatusType OpStatus,
  P2CONST(Cry_RsaKeyType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_CONST) RsaKeyPtr,
  P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) ErrorCode);
#endif

#define VEC_SecurityAccess_STOP_SEC_CODE
#include "VEC_SecurityAccess_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * UInt32: Integer in interval [0...4294967295]
 * UInt8: Integer in interval [0.0...0.0]
 *
 * Enumeration Types:
 * ==================
 * Csm_ReturnType: Enumeration of integer in interval [0...255] with enumerators
 *   CSM_E_OK (0U)
 *   CSM_E_NOT_OK (1U)
 *   CSM_E_BUSY (2U)
 *   CSM_E_SMALL_BUFFER (3U)
 *   CSM_E_ENTROPY_EXHAUSTION (4U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 *
 * Array Types:
 * ============
 * AsymDecryptDataBuffer: Array with 128 element(s) of type UInt8
 * AsymDecryptResultBuffer: Array with 128 element(s) of type UInt8
 * Dcm_Data116ByteType: Array with 116 element(s) of type UInt8
 * Dcm_Data128ByteType: Array with 128 element(s) of type UInt8
 * RandomGenerateResultBuffer: Array with 128 element(s) of type UInt8
 * RandomSeedDataBuffer: Array with 128 element(s) of type UInt8
 * Rte_DT_AsymPrivateKeyType_1: Array with 128 element(s) of type UInt8
 *
 * Record Types:
 * =============
 * AsymPrivateKeyType: Record with elements
 *   length of type UInt32
 *   data of type Rte_DT_AsymPrivateKeyType_1
 *
 *********************************************************************************************************************/


#define VEC_SecurityAccess_START_SEC_CODE
#include "VEC_SecurityAccess_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_CallbackNotificationAsymDecrypt
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <JobFinished> of PortPrototype <CsmCallbackAsymDecrypt>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_CallbackNotificationAsymDecrypt(Csm_ReturnType retVal)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmCallback_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_CallbackNotificationAsymDecrypt_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_CallbackNotificationAsymDecrypt(Csm_ReturnType retVal) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_CallbackNotificationAsymDecrypt (returns application error)
 *********************************************************************************************************************/

  VEC_SecurityAccess_TriggerState( &Sec_DecryptOp, OPERATION_DECRYPT, retVal );

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_CallbackNotificationRandomGenerate
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <JobFinished> of PortPrototype <CsmCallbackRandomGenerate>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_CallbackNotificationRandomGenerate(Csm_ReturnType retVal)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmCallback_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_CallbackNotificationRandomGenerate_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_CallbackNotificationRandomGenerate(Csm_ReturnType retVal) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_CallbackNotificationRandomGenerate (returns application error)
 *********************************************************************************************************************/

  /* Result of CSM operation */
  switch ( retVal )
  {
    case CSM_E_OK:
    {
      /* Operation successful */

      /* Check seed state */
      switch ( Sec_SeedState )
      {
        case STATE_OPERATION_PENDING:
        {
          /* Seed generation expected to be pending
             Value is now valid */
          Sec_SeedState = STATE_VALID;

          break;
        }
        default:
        {
          /* Notification is unexpected, seed value is invalid */
          Sec_SeedState = STATE_INVALID;

          break;
        }
      }

      break;
    }
    default:
    {
      /* Operation failed, seed value is invalid */
      Sec_SeedState = STATE_INVALID;

      break;
    }
  }

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_CallbackNotificationRandomSeed
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <JobFinished> of PortPrototype <CsmCallbackRandomSeed>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_CallbackNotificationRandomSeed(Csm_ReturnType retVal)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmCallback_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_CallbackNotificationRandomSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_CallbackNotificationRandomSeed(Csm_ReturnType retVal) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_CallbackNotificationRandomSeed (returns application error)
 *********************************************************************************************************************/

  VEC_SecurityAccess_TriggerState( &Sec_EntropyOp, OPERATION_ENTROPY, retVal );

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_SecurityAccess_CODE) VEC_SecurityAccess_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_Init
 *********************************************************************************************************************/

  /* Initialize states */
  Sec_EntropyOp     = CSMSTATE_INITIAL;
  Sec_EntropyState  = ENTROPYSTATE_INVALID;

  Sec_SeedState     = STATE_INITIAL;

  Sec_DecryptOp     = CSMSTATE_INITIAL;
  Sec_VerifyState   = STATE_INITIAL;

#if ( VEC_SECURITYACCESS_RESET_CSM_STATE_BY_FINISH_FUNCTION == STD_ON )
  Sec_VerifyUpdateFailed = FALSE;
#endif

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_MainFunction
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmRandomSeed_RandomSeedFinish(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomSeed_CSM_E_BUSY, RTE_E_CsmRandomSeed_CSM_E_NOT_OK, RTE_E_CsmRandomSeed_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmRandomSeed_RandomSeedStart(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomSeed_CSM_E_BUSY, RTE_E_CsmRandomSeed_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmRandomSeed_RandomSeedUpdate(const UInt8 *seedBuffer, UInt32 seedLength)
 *     Argument seedBuffer: UInt8* is of type RandomSeedDataBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomSeed_CSM_E_BUSY, RTE_E_CsmRandomSeed_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_MainFunction_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_SecurityAccess_CODE) VEC_SecurityAccess_MainFunction(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_MainFunction
 *********************************************************************************************************************/

  /* Check entropy and verification state */
  VEC_SecurityAccess_TriggerProcessing( &Sec_EntropyOp, OPERATION_ENTROPY );
  VEC_SecurityAccess_TriggerProcessing( &Sec_DecryptOp, OPERATION_DECRYPT );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_ProvideEntropy
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ProvideEntropy> of PortPrototype <VEC_SecurityAccess_ProvideEntropy>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_ProvideEntropy(const UInt8 *seedPtr, UInt32 seedLength)
 *     Argument seedPtr: UInt8* is of type RandomSeedDataBuffer
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_VEC_SecurityAccess_ProvideEntropy_E_BUSY
 *   RTE_E_VEC_SecurityAccess_ProvideEntropy_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_ProvideEntropy_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_ProvideEntropy(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) seedPtr, UInt32 seedLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_ProvideEntropy (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType result = RTE_E_VEC_SecurityAccess_ProvideEntropy_E_NOT_OK;
  uint16_least index;

  /* Provided entropy fits into temporary buffer? */
  if ( seedLength <= sizeof(Sec_EntropyBuffer) )
  {
    /* Check entropy state */
    switch ( Sec_EntropyOp )
    {
      case CSMSTATE_INITIAL:
      case CSMSTATE_IDLE:
      {
        /* Idle, feed entropy into RNG */
        /* Copy provided data into temporary buffer */
        for ( index = 0u; index < seedLength; index++ )
        {
          Sec_EntropyBuffer[index] = seedPtr[index];
        }

        /* Set seed length */
        Sec_EntropyLength = seedLength;
        /* Set state to start */
        Sec_EntropyOp     = CSMSTATE_START;

        /* Operation successful */
        result = RTE_E_OK;

        break;
      }
      default:
      {
        /* Operation in progress, try again */
        result = RTE_E_VEC_SecurityAccess_ProvideEntropy_E_BUSY;
      }
    }
  }

  return result;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_01 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_01_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_01>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_01_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_01_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_01_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_01_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_01, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_01_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_01>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_01_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_01_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_01_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_01_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_01 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_03 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_03_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_03>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_03_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_03_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_03_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_03_CompareKey (returns application error)
 *********************************************************************************************************************/

 return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_03, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_03_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_03>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_03_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_03_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_03_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_03_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_03 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_05 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_05_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_05>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_05_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_05_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_05_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_05_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_05, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_05_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_05>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_05_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_05_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_05_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_05_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_05 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_07 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_07_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_07>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_07_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_07_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_07_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_07_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_07, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_07_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_07>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_07_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_07_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_07_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_07_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_07 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_09 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_09_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_09>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_09_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_09_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_09_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_09_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_09, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_09_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_09>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_09_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_09_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_09_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_09_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_09 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_0B )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_0B_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_0B>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_0B_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0B_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0B_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0B_CompareKey (returns application error)
 *********************************************************************************************************************/

 return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_0B, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_0B_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_0B>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_0B_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0B_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0B_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_0B */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_0D )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_0D_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_0D>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_0D_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0D_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0D_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0D_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_0D, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_0D_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_0D>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_0D_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0D_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0D_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0D_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_0D */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_0F )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_0F_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_0F>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_0F_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0F_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0F_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0F_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_0F, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_0F_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_0F>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_0F_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0F_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0F_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0F_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_0F */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_11 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_11_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_11>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_11_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_11_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_11_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_11_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_11, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_11_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_11>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_11_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_11_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_11_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_11_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_11 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_13 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_13_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_13>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_13_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_13_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_13_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_13_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_13, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_13_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_13>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_13_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_13_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_13_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_13_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_13 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_15 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_15_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_15>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_15_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_15_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_15_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_15_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_15, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_15_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_15>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_15_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_15_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_15_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_15_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_15 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_17 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_17_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_17>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_17_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_17_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_17_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_17_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_17, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_17_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_17>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_17_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_17_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_17_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_17_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_17 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_19 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_19_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_19>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_19_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_19_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_19_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_19_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_19, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_19_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_19>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_19_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_19_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_19_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_19_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_19 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_1B )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_1B_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_1B>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_1B_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1B_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1B_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1B_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_1B, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_1B_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_1B>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_1B_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1B_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1B_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_1B */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_1D )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_1D_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_1D>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_1D_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1D_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1D_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1D_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_1D, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_1D_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_1D>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_1D_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1D_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1D_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1D_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_1D */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_1F )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_1F_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_1F>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_1F_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1F_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1F_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1F_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_1F, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_1F_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_1F>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_1F_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1F_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1F_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1F_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_1F */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_21 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_21_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_21>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_21_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_21_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_21_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_21_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_21, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_21_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_21>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_21_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_21_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_21_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_21_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_21 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_23 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_23_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_23>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_23_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_23_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_23_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_23_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_23, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_23_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_23>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_23_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_23_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_23_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_23_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_23 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_25 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_25_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_25>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_25_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_25_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_25_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_25_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_25, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_25_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_25>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_25_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_25_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_25_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_25_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_25 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_27 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_27_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_27>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_27_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_27_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_27_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_27_CompareKey (returns application error)
 *********************************************************************************************************************/

 return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_27, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_27_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_27>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_27_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_27_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_27_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_27_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_27 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_29 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_29_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_29>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_29_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_29_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_29_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_29_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_29, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_29_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_29>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_29_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_29_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_29_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_29_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_29 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_2B )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_2B_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_2B>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_2B_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2B_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2B_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2B_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_2B, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_2B_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_2B>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_2B_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2B_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2B_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_2B */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_2D )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_2D_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_2D>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_2D_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2D_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2D_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2D_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_2D, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_2D_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_2D>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_2D_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2D_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2D_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2D_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_2D */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_2F )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_2F_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_2F>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_2F_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2F_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2F_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2F_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_2F, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_2F_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_2F>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_2F_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2F_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2F_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2F_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_2F */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_31 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_31_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_31>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_31_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_31_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_31_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_31_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_31, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_31_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_31>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_31_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_31_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_31_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_31_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_31 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_33 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_33_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_33>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_33_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_33_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_33_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_33_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_33, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_33_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_33>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_33_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_33_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_33_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_33_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_33 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_35 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_35_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_35>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_35_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_35_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_35_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_35_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_35, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_35_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_35>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_35_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_35_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_35_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_35_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_35 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_37 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_37_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_37>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_37_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_37_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_37_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_37_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_37, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_37_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_37>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_37_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_37_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_37_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_37_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_37 */


#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_39 )
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_39_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_39>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_39_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_39_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_39_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_39_CompareKey (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_CompareKey( Key, OpStatus, &VEC_SecurityAccess_Key_39, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_39_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_39>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_39_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_39_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_39_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_39_GetSeed (returns application error)
 *********************************************************************************************************************/

  return VEC_SecurityAccess_GetSeed( OpStatus, Seed, ErrorCode );

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#endif /* VEC_SECURITYACCESS_ENABLE_SEED_39 */

#define VEC_SecurityAccess_STOP_SEC_CODE
#include "VEC_SecurityAccess_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 **********************************************************************************************************************/
#define VEC_SecurityAccess_START_SEC_CODE
#include "VEC_SecurityAccess_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/***********************************************************************************************************************
 *  VEC_SecurityAccess_TriggerState
 **********************************************************************************************************************/
STATIC FUNC(void, VEC_SecurityAccess_CODE) VEC_SecurityAccess_TriggerState(
  P2VAR(Sec_CsmStateType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) statePtr,
  Sec_OperationType operation,
  Csm_ReturnType retVal)
{
  uint8_least index;
  Sec_StateType verifyResult;

  /* Result of CSM operation */
  switch ( retVal )
  {
    case CSM_E_OK:
    {
      /* Operation successful */

      /* Switch state */
      switch ( *statePtr )
      {
        case CSMSTATE_START_PENDING:
        {
          /* Start function finished, continue with update function */
          *statePtr = CSMSTATE_UPDATE;

          break;
        }
        case CSMSTATE_UPDATE_PENDING:
        {
          /* Update function finished, continue with finish function */
          *statePtr = CSMSTATE_FINISH;

          break;
        }
        case CSMSTATE_FINISH_PENDING:
        {
          /* Finish function was successful */
          *statePtr = CSMSTATE_IDLE;

          if ( operation == OPERATION_DECRYPT )
          {
            /* Set state to valid */
            verifyResult = STATE_VALID;

            /* Validate plain text against seed value */
            for ( index = 0; index < VEC_SECURITYACCESS_SEED_SIZE ; index++ )
            {
              if ( Sec_DecryptedPlainSeedKey[index] != Sec_CurrentSeedValue[index] )
              {
                /* Verification failed */
                verifyResult = STATE_INVALID;
              }
            }

            /* Interpret verification result */
            if ( verifyResult == STATE_VALID )
            {
              Sec_VerifyState = STATE_VALID;
            }
            else
            {
              Sec_VerifyState = STATE_INVALID;
            }
          }
          else
          {
            /* Remember seed request success */
            Sec_EntropyState  = ENTROPYSTATE_VALID;
          }

          break;
        }
        default:
        {
          /* Notification is unexpected, reset state */
          *statePtr = CSMSTATE_INITIAL;
          Sec_VerifyState = STATE_INVALID;

          break;
        }
      }

      break;
    }
    default:
    {
      /* Operation failed, reset state */
      *statePtr = CSMSTATE_INITIAL;
#if ( VEC_SECURITYACCESS_RESET_CSM_STATE_BY_FINISH_FUNCTION == STD_ON )
      /* Marker for Verification Failure */
      if ( operation == OPERATION_DECRYPT )
      {
        Sec_VerifyUpdateFailed = TRUE;
      }
#endif
      Sec_VerifyState = STATE_INVALID;

      break;
    }
  }
}


/***********************************************************************************************************************
 *  VEC_SecurityAccess_TriggerProcessing
 **********************************************************************************************************************/
STATIC FUNC(void,  VEC_SecurityAccess_CODE) VEC_SecurityAccess_TriggerProcessing(
  P2VAR(Sec_CsmStateType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) statePtr,
  Sec_OperationType operation)
{

  /* Check state */
  switch ( *statePtr )
  {
    case CSMSTATE_START:
    {
      /* Start CSM function */
      switch ( VEC_SecurityAccess_FunctionStart( statePtr, operation ) )
      {
        case CSM_E_OK:
        {
#if defined( VEC_SECURITYACCESS_CSM_ASYNC_JOB_PROCESSING )
          /* Wait for result of CSM start function */
          *statePtr = CSMSTATE_START_PENDING;
#else
          /* Continue with the update function */
          *statePtr = CSMSTATE_UPDATE;
#endif /* VEC_SECURITYACCESS_CSM_ASYNC_JOB_PROCESSING */

          break;
        }
        case CSM_E_BUSY:
        {
          /* CSM busy, try again */
          break;
        }
        case CSM_E_NOT_OK:
        default:
        {
          /* Operation failed, reset state */
          *statePtr = CSMSTATE_INITIAL;

          /* Set verification state to invalid */
          Sec_VerifyState = STATE_INVALID;

          break;
        }
      }

      break;
    }
    case CSMSTATE_UPDATE:
    {
      /* Call the update function */
      switch ( VEC_SecurityAccess_FunctionUpdate( statePtr, operation ) )
      {
        case CSM_E_OK:
        {
#if defined( VEC_SECURITYACCESS_CSM_ASYNC_JOB_PROCESSING )
          /* Wait for result of the update function */
          *statePtr = CSMSTATE_UPDATE_PENDING;
#else
          /* Continue with the finish function */
          *statePtr = CSMSTATE_FINISH;
#endif /* VEC_SECURITYACCESS_CSM_ASYNC_JOB_PROCESSING */

          break;
        }
        case CSM_E_BUSY:
        {
          /* CSM busy, try again */
          break;
        }
        case CSM_E_NOT_OK:
        default:
        {
          /* Operation failed, reset state */
          *statePtr = CSMSTATE_INITIAL;

#if ( VEC_SECURITYACCESS_RESET_CSM_STATE_BY_FINISH_FUNCTION == STD_ON )
          /* Marker for Verification Failure */
          if ( operation == OPERATION_DECRYPT )
          {
            Sec_VerifyUpdateFailed = TRUE;
          }
#endif

          /* Set verification state to invalid */
          Sec_VerifyState = STATE_INVALID;

          break;
        }
      }

      break;
    }
    case CSMSTATE_FINISH:
    {
      /* Call finish function */
      switch ( VEC_SecurityAccess_FunctionFinish( statePtr, operation ) )
      {
        case CSM_E_OK:
        {
#if defined( VEC_SECURITYACCESS_CSM_ASYNC_JOB_PROCESSING )
          /* Wait for result of the finish function */
          *statePtr = CSMSTATE_FINISH_PENDING;
#else
          /* Set state temporary for processing in VEC_SecurityAccess_TriggerState */
          *statePtr = CSMSTATE_FINISH_PENDING;
          VEC_SecurityAccess_TriggerState( statePtr, operation, CSM_E_OK );
#endif /* VEC_SECURITYACCESS_CSM_ASYNC_JOB_PROCESSING */

          break;
        }
        case CSM_E_BUSY:
        {
          /* CSM busy, try again */
          break;
        }
        case CSM_E_NOT_OK:
        default:
        {
          /* Operation failed, reset state */
          *statePtr = CSMSTATE_INITIAL;

          /* Set verification state to invalid */
          Sec_VerifyState = STATE_INVALID;

          break;
        }
      }

      break;
    }
    default:
    {
      /* Nothing to do */
      break;
    }
  }
}

/***********************************************************************************************************************
 *  VEC_SecurityAccess_FunctionStart
 **********************************************************************************************************************/
STATIC FUNC(UInt8,  VEC_SecurityAccess_CODE) VEC_SecurityAccess_FunctionStart(
  P2CONST(Sec_CsmStateType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) statePtr,
  Sec_OperationType operation)
{
  UInt8 retVal;

  if ( operation == OPERATION_ENTROPY )
  {
    retVal = Rte_Call_CsmRandomSeed_RandomSeedStart();
  }
  else
  {
    retVal = Rte_Call_CsmAsymDecrypt_AsymDecryptStart( (P2VAR(AsymPrivateKeyType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA))Sec_RsaKeyPtr );
  }

  return retVal;
}

/***********************************************************************************************************************
 *  VEC_SecurityAccess_FunctionUpdate
 **********************************************************************************************************************/
STATIC FUNC(UInt8,  VEC_SecurityAccess_CODE) VEC_SecurityAccess_FunctionUpdate(
  P2CONST(Sec_CsmStateType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) statePtr,
  Sec_OperationType operation)
{
  UInt8 retVal;

  if ( operation == OPERATION_ENTROPY )
  {
    retVal = Rte_Call_CsmRandomSeed_RandomSeedUpdate( Sec_EntropyBuffer, Sec_EntropyLength );
  }
  else
  {
    retVal = Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate( Sec_ReceivedKeyBuffer, sizeof(Sec_ReceivedKeyBuffer), Sec_DecryptedPlainSeedKey, &Sec_DecryptedPlainSeedKeyLength );
  }

  return retVal;
}

/***********************************************************************************************************************
 *  VEC_SecurityAccess_FunctionFinish
 **********************************************************************************************************************/
STATIC FUNC(UInt8,  VEC_SecurityAccess_CODE) VEC_SecurityAccess_FunctionFinish(
  P2CONST(Sec_CsmStateType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) statePtr,
  Sec_OperationType operation)
{
  UInt8 retVal;

  if ( operation == OPERATION_ENTROPY )
  {
    retVal = Rte_Call_CsmRandomSeed_RandomSeedFinish();
  }
  else
  {
    retVal = Rte_Call_CsmAsymDecrypt_AsymDecryptFinish( Sec_DecryptedPlainSeedKey, &Sec_DecryptedPlainSeedKeyLength );
  }

  return retVal;
}


/************************************************************************************************************************
 *  VEC_SecurityAccess_CompareKey
 ***********************************************************************************************************************/
#ifdef RTE_PTR2ARRAYBASETYPE_PASSING
STATIC FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_CompareKey(
  P2CONST(uint8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Key,
  Dcm_OpStatusType OpStatus,
  P2CONST(Cry_RsaKeyType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_CONST) RsaKeyPtr,
  P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) ErrorCode)
#else
STATIC FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_CompareKey(
  P2CONST(uint8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Key,
  Dcm_OpStatusType OpStatus,
  P2CONST(Cry_RsaKeyType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_CONST) RsaKeyPtr,
  P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) ErrorCode)
#endif
{
  Std_ReturnType result = RTE_E_SecurityAccess_E_NOT_OK;
  UInt8 i;

  /* Avoid compiler warnings on unused function parameters */
  (void)OpStatus;  /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */

  /* Handle current verification state */
  switch ( Sec_VerifyState )
  {
    /* Start verify value */
    case STATE_INITIAL:
    {
      /* Store received key for verification */
      for ( i = 0; i < VEC_SECURITYACCESS_KEY_SIZE; i++ )
      {
        Sec_ReceivedKeyBuffer[i] = Key[i];
      }

      /* Store RSA key pointer */
      Sec_RsaKeyPtr = RsaKeyPtr;

      /* Prepare decryption */
      Sec_DecryptedPlainSeedKeyLength = sizeof(Sec_DecryptedPlainSeedKey);

      Sec_DecryptOp = CSMSTATE_START;
      Sec_VerifyState = STATE_OPERATION_PENDING;
      result = RTE_E_SecurityAccess_DCM_E_PENDING;

      break;
    }
    case STATE_OPERATION_PENDING:
    {
      /* Verification of received key not finished yet, try again */
      result = RTE_E_SecurityAccess_DCM_E_PENDING;

      break;
    }
    case STATE_VALID:
	//case STATE_INVALID:
    {
      /* Verification of received key was successful */
      result = RTE_E_OK;

      /* Set back states */
      Sec_VerifyState = STATE_INITIAL;
      Sec_SeedState = STATE_INITIAL;

      break;
    }
   case STATE_INVALID:
      result = RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED;
      /* Set NRC for invalid key */
      *ErrorCode = VEC_SECURITYACCESS_NRC_INVALIDKEY;
    default:
    {
      /* Set back states */
      Sec_VerifyState = STATE_INITIAL;
      Sec_SeedState = STATE_INITIAL;

#if ( VEC_SECURITYACCESS_RESET_CSM_STATE_BY_FINISH_FUNCTION == STD_ON )
      /* Reset CSM state */
      if ( Sec_VerifyUpdateFailed == TRUE )
      {
        (void)Rte_Call_CsmAsymDecrypt_AsymDecryptFinish( Sec_DecryptedPlainSeedKey, &Sec_DecryptedPlainSeedKeyLength );
        Sec_VerifyUpdateFailed = FALSE;
      }
#endif
	  
      break;
    }
  }

  return result;
}

/************************************************************************************************************************
 *  VEC_SecurityAccess_GetSeed
 ***********************************************************************************************************************/
STATIC FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_GetSeed( Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode )
{
  Std_ReturnType result = RTE_E_SecurityAccess_E_NOT_OK;
  /* Access seed value as byte array */
  P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) pSeedByte = (P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA))Seed;
  uint8_least index;

  /* Avoid compiler warnings on unused function parameters */
  (void)OpStatus;  /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */

#if defined( VEC_SECURITYACCESS_CSM_SYNC_JOB_PROCESSING )
  do
#endif /* VEC_SECURITYACCESS_CSM_SYNC_JOB_PROCESSING */
  {
    /* Handle current seed state */
    switch ( Sec_SeedState )
    {
      /* Start generating new seed value */
      case STATE_INITIAL:
      {
        /* Entropy feed into RNG at least once? */
        if ( Sec_EntropyState == ENTROPYSTATE_VALID )
        {
          /* Trigger random number generation */
          switch ( Rte_Call_CsmRandomGenerate_RandomGenerate( Sec_CurrentSeedValue, sizeof(Sec_CurrentSeedValue) ) )
          {
            case CSM_E_OK:
            {
              /* Generation pending (asynchronous CSM) or successful (synchronous CSM) */
              Sec_SeedState = STATE_OPERATION_PENDING;

              result = RTE_E_SecurityAccess_DCM_E_PENDING;

              break;
            }
            case CSM_E_BUSY:
            {
              /* CSM busy, try again */
              result = RTE_E_SecurityAccess_DCM_E_PENDING;

              break;
            }
            default:
            {
              /* Operation failed */
              result = RTE_E_SecurityAccess_E_NOT_OK;
              *ErrorCode = VEC_SECURITYACCESS_NRC_GENERALREJECT;
              break;
            }
          }
        }
        else
        {
          switch ( Sec_EntropyOp )
          {
            case CSMSTATE_INITIAL:
            case CSMSTATE_IDLE:
            {
              /* No entropy operation pending */
              result = RTE_E_SecurityAccess_E_NOT_OK;
              *ErrorCode = VEC_SECURITYACCESS_NRC_GENERALREJECT;
              break;
            }
            default:
            {
              /* Entropy operation pending, try again */
              result = RTE_E_SecurityAccess_DCM_E_PENDING;

              break;
            }
          }
        }

        break;
      }
      case STATE_OPERATION_PENDING:
#if defined( VEC_SECURITYACCESS_CSM_ASYNC_JOB_PROCESSING )
      {
        /* Random number generation not finished yet, try again */
        result = RTE_E_SecurityAccess_DCM_E_PENDING;

        break;
      }
#else
      /* Directly return generated value */
#endif /* VEC_SECURITYACCESS_CSM_ASYNC_JOB_PROCESSING */
      case STATE_VALID:
      {
        /* Valid key present or key generation finished */

        /* Remember seed value is valid, only necessary for synchronous CSM */
        Sec_SeedState = STATE_VALID;

        /* Copy current seed value to output */
        for ( index = 0u; index < sizeof(Sec_CurrentSeedValue); index++ )
        {
          pSeedByte[index] = Sec_CurrentSeedValue[index];
        }

        /* Operation successful */
        result = RTE_E_OK;

        break;
      }
      case STATE_INVALID:
      default:
      {
        /* Seed generation failed or invalid state */
        /* Allow next seed request */
        Sec_SeedState = STATE_INITIAL;
        *ErrorCode = VEC_SECURITYACCESS_NRC_GENERALREJECT;
        break;
      }
    }
  }
#if defined( VEC_SECURITYACCESS_CSM_SYNC_JOB_PROCESSING )
  /* Repeat switch to directly return generated value */
  while (STATE_OPERATION_PENDING == Sec_SeedState);
#endif /* VEC_SECURITYACCESS_CSM_SYNC_JOB_PROCESSING */

  return result;
}


#define VEC_SecurityAccess_STOP_SEC_CODE
#include "VEC_SecurityAccess_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0777:  MISRA rule: 5.1
     Reason:     The defined RTE naming convention may result in identifiers with more than 31 characters. The compliance to this rule is under user's control.
                 This is covered in the MISRA C compliance section of the Rte specification.
     Risk:       Ambiguous identifiers can lead to compiler errors / warnings.
     Prevention: Verified during compile time. If the compiler reports an error / warning. The user has to rename the objects to be unique within the significant characters.

   MD_Rte_0779:  MISRA rule: 5.1
     Reason:     The defined RTE naming convention may result in identifiers with more than 31 characters. The compliance to this rule is under user's control.
                 This is covered in the MISRA C compliance section of the Rte specification.
     Risk:       Ambiguous identifiers can lead to compiler errors / warnings.
     Prevention: Verified during compile time. If the compiler reports an error / warning. The user has to rename the objects to be unique within the significant characters.

*/
