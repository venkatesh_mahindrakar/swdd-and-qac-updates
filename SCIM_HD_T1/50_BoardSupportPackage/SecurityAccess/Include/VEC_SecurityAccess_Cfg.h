/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2017 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  VEC_SecurityAccess_Cfg.h
 *    Component:  Diag_AsrSwcSecurityAccess_Volvo_AB
 *       Module:  -
 *    Generator:  -
 *
 *  Description:  Configuration of module Diag_AsrSwcSecurityAccess_Volvo_AB
 *  
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *********************************************************************************************************************/
#if !defined (VEC_SECURITYACCESS_CFG_H)
#define VEC_SECURITYACCESS_CFG_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "Csm_Cfg.h"

/* Include Dcm.h for level defines */
#include "Dcm.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* Reset the CSM state by calling the Asym Decrypt Finish function */
#ifndef VEC_SECURITYACCESS_RESET_CSM_STATE_BY_FINISH_FUNCTION
# define VEC_SECURITYACCESS_RESET_CSM_STATE_BY_FINISH_FUNCTION STD_ON
#endif

/* Check operation mode of CSM */
#if ( CSM_USE_SYNC_JOB_PROCESSING == STD_OFF )
/** CSM operates in asynchronous mode */
# define VEC_SECURITYACCESS_CSM_ASYNC_JOB_PROCESSING
#else
/** CSM operates in synchronous mode */
# define VEC_SECURITYACCESS_CSM_SYNC_JOB_PROCESSING
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_01 )
# define VEC_SECURITYACCESS_ENABLE_SEED_01
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_03 )
# define VEC_SECURITYACCESS_ENABLE_SEED_03
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_05 )
# define VEC_SECURITYACCESS_ENABLE_SEED_05
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_07 )
# define VEC_SECURITYACCESS_ENABLE_SEED_07
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_09 )
# define VEC_SECURITYACCESS_ENABLE_SEED_09
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_0B )
# define VEC_SECURITYACCESS_ENABLE_SEED_0B
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_0D )
# define VEC_SECURITYACCESS_ENABLE_SEED_0D
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_0F )
# define VEC_SECURITYACCESS_ENABLE_SEED_0F
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_11 )
# define VEC_SECURITYACCESS_ENABLE_SEED_11
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_13 )
# define VEC_SECURITYACCESS_ENABLE_SEED_13
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_15 )
# define VEC_SECURITYACCESS_ENABLE_SEED_15
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_17 )
# define VEC_SECURITYACCESS_ENABLE_SEED_17
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_19 )
# define VEC_SECURITYACCESS_ENABLE_SEED_19
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_1B )
# define VEC_SECURITYACCESS_ENABLE_SEED_1B
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_1D )
# define VEC_SECURITYACCESS_ENABLE_SEED_1D
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_1F )
# define VEC_SECURITYACCESS_ENABLE_SEED_1F
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_21 )
# define VEC_SECURITYACCESS_ENABLE_SEED_21
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_23 )
# define VEC_SECURITYACCESS_ENABLE_SEED_23
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_25 )
# define VEC_SECURITYACCESS_ENABLE_SEED_25
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_27 )
# define VEC_SECURITYACCESS_ENABLE_SEED_27
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_29 )
# define VEC_SECURITYACCESS_ENABLE_SEED_29
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_2B )
# define VEC_SECURITYACCESS_ENABLE_SEED_2B
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_2D )
# define VEC_SECURITYACCESS_ENABLE_SEED_2D
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_2F )
# define VEC_SECURITYACCESS_ENABLE_SEED_2F
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_31 )
# define VEC_SECURITYACCESS_ENABLE_SEED_31
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_33 )
# define VEC_SECURITYACCESS_ENABLE_SEED_33
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_35 )
# define VEC_SECURITYACCESS_ENABLE_SEED_35
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_37 )
# define VEC_SECURITYACCESS_ENABLE_SEED_37
#endif

#if defined ( DcmConf_DcmDspSecurityRow_SA_Seed_39 )
# define VEC_SECURITYACCESS_ENABLE_SEED_39
#endif

#endif /* VEC_SECURITYACCESS_KEYS_H */
/**********************************************************************************************************************
 *  END OF FILE: VEC_SECURITYACCESS_KEYS.H
 *********************************************************************************************************************/
