/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2017 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  VEC_SecurityAccess_Keys.h
 *    Component:  Diag_AsrSwcSecurityAccess_Volvo_AB
 *       Module:  -
 *    Generator:  -
 *
 *  Description:  User keys of module Diag_AsrSwcSecurityAccess_Volvo_AB
 *  
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *********************************************************************************************************************/
#if !defined (VEC_SECURITYACCESS_KEYS_H)
#define VEC_SECURITYACCESS_KEYS_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/

#include "Std_Types.h"
#include "Cry_Key_Types.h"
#include "VEC_SecurityAccess_Cfg.h"

/**********************************************************************************************************************
 *  GLOBAL DATA
 **********************************************************************************************************************/

#define MSW_SecurityAccess_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_01 )
/* Key handle for service 01 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_01;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_03 )
/* Key handle for service 03 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_03;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_05 )
/* Key handle for service 05 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_05;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_07 )
/* Key handle for service 07 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_07;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_09 )
/* Key handle for service 09 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_09;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_0B )
/* Key handle for service 0B */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_0B;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_0D )
/* Key handle for service 0D */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_0D;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_0F )
/* Key handle for service 0F */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_0F;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_11 )
/* Key handle for service 11 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_11;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_13 )
/* Key handle for service 13 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_13;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_15 )
/* Key handle for service 15 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_15;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_17 )
/* Key handle for service 17 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_17;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_19 )
/* Key handle for service 19 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_19;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_1B )
/* Key handle for service 1B */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_1B;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_1D )
/* Key handle for service 1D */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_1D;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_1F )
/* Key handle for service 1F */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_1F;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_21 )
/* Key handle for service 21 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_21;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_23 )
/* Key handle for service 23 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_23;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_25 )
/* Key handle for service 25 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_25;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_27 )
/* Key handle for service 27 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_27;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_29 )
/* Key handle for service 29 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_29;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_2B )
/* Key handle for service 2B */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_2B;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_2D )
/* Key handle for service 2D */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_2D;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_2F )
/* Key handle for service 2F */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_2F;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_31 )
/* Key handle for service 31 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_31;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_33 )
/* Key handle for service 33 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_33;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_35 )
/* Key handle for service 35 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_35;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_37 )
/* Key handle for service 37 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_37;
#endif

#if defined ( VEC_SECURITYACCESS_ENABLE_SEED_39 )
/* Key handle for service 39 */
extern CONST( Cry_RsaKeyType, VEC_SecurityAccess_CONST ) VEC_SecurityAccess_Key_39;
#endif

#define MSW_SecurityAccess_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#endif /* VEC_SECURITYACCESS_KEYS_H */
/**********************************************************************************************************************
 *  END OF FILE: VEC_SECURITYACCESS_KEYS.H
 *********************************************************************************************************************/
