/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2017 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  VEC_SecurityAccess.h
 *    Component:  Diag_AsrSwcSecurityAccess_Volvo_AB
 *       Module:  -
 *    Generator:  -
 *
 *  Description:  Header of the Volvo Security Access
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  -------------------------------------------------------------------------------------------------------------------
 *  Daniel Leutloff               DLf           Vector Informatik GmbH
 *  Joern Herwig                  JHg           Vector Informatik GmbH
 *  Markus Schneider              Mss           Vector Informatik GmbH
 *  Anant Gupta                   Gut           Vector Informatik GmbH
 *  -------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  01.00.00  2009-06-30  JHg     -             Initial creation
 *  01.01.00  2009-07-21  DLf     ESCAN00036586 Renamed key compare runnables to
 *                                               Appl_Dcm_SecurityAccess_SA_Seed_<LEVEL>_CompareKey
 *                        JHg     ESCAN00036587 Encapsulated runnables for specific security level with
 *                                               DCM_SEC_LEV_L<LEVEL>
 *                        JHg     ESCAN00036588 Place callouts in VEC_SECURITYACCESS_APPL_CODE MemMap section
 *  01.02.00  2011-11-18  JHg     ESCAN00042425 Added void parameter to VEC_SecurityAccess_TimerValue declaration
 *                                ESCAN00042426 Replaced RTE_VEC_SECURITYACCESS_APPL_CONST with  VEC_SECURITYACCESS_APPL_CONST
 *                                ESCAN00054960 Added additional security levels
 *                                ESCAN00054962 Reduced default size of RSA workspace
 *  01.03.00  2011-12-02  JHg     ESCAN00055287 Added additional security levels (marked as reserved)
 *  02.00.00  2014-11-11  JHg     ESCAN00078844 Support for updated security module API (VAP SLP2)
 *  03.00.00  2015-05-26  Mss     ESCAN00083027 Redesign to support the usage of CSM
 *  03.00.01  2015-06-10  Mss     ESCAN00083323 Added compatibility defines for CSwC variant
 *  03.00.02  2016-01-26  Mss     ESCAN00087787 Fixed seed comparison for branch 03.xx.xx
 *  04.00.00  2016-01-19  Mss     ESCAN00087442 Support of AUTOSAR 4.2.x DCM
 *  04.00.01  2016-01-26  Mss     ESCAN00087787 Fixed seed comparison for branch 04.xx.xx
 *  04.00.02  2016-03-04  Mss     ESCAN00087870 Fixed CSM error code handling
 *                                ESCAN00088298 VEC_SecurityAccess_Keys.c is no longer a template file; adapted the
 *                                               names of the key defines
 *  04.00.03  2016-12-14  Mss     ESCAN00093039 Option to reset the CSM state by calling the explicit finish function
 *  04.00.04  2017-02-21  Mss     ESCAN00093736 Fixed CSM reset handling for asynchronous use case
 *  04.00.05  2017-03-27  Gut     ESCAN00094335 VEC_SecurityAccess_SA_Seed_<level>_CompareKey does not set an ErrorCode in case of returning E_NOT_OK
 *  04.00.06  2017-12-08  Mss     ESCAN00095442 Fixed providing NRC for invalid key
 *********************************************************************************************************************/

#if !defined (VEC_SECURITYACCESS_H)
#define VEC_SECURITYACCESS_H

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* ----- Component version information (decimal version of ALM implementation package) ----- */
#define VEC_SECURITYACCESS_SW_MAJOR_VERSION       (4u)
#define VEC_SECURITYACCESS_SW_MINOR_VERSION       (0u)
#define VEC_SECURITYACCESS_SW_PATCH_VERSION       (6u)

#endif  /* VEC_SECURITYACCESS_H */
/**********************************************************************************************************************
 *  END OF FILE: VEC_SecurityAccess.h
 *********************************************************************************************************************/
