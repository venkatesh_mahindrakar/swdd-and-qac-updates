#ifndef FBL_APPLVECTOR_H
#define FBL_APPLVECTOR_H

#define APPLVECT_FROM_APPL 0xAA000000UL /* Set this label for the user              */

#define FBL_JSR(x)           (((void(*)(void))(x))())   /**< Jump to subroutine */


#pragma section applvect "applvect"
#pragma use_section applvect ApplIntJmpTable

extern unsigned long const ApplIntJmpTable[];

#pragma section CODE



#endif /* FBL_APPLVECTOR_H */

