#include "fbl_applvector.h"


extern void _start(void);

#define resetentrypoint (unsigned long) _start

#define FBLJUMP  0xF9001Cul


//#pragma section applvect "applvect"
//#pragma use_section applvect ApplIntJmpTable

unsigned long const ApplIntJmpTable[3] = {APPLVECT_FROM_APPL, resetentrypoint, FBLJUMP};

#pragma section CODE




