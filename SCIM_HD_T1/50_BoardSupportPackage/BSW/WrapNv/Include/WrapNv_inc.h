/***********************************************************************************************************************
 *  FILE DESCRIPTION
 *  ------------------------------------------------------------------------------------------------------------------*/
/** \file         WrapNv_inc.h
 *  \brief        Compacts the include files for the NV-Wrapper
 *
 *  --------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT
 *  --------------------------------------------------------------------------------------------------------------------
 *  \par Copyright
 *  \verbatim
 *  Copyright (c) 2018 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 */
/**********************************************************************************************************************/

/***********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  --------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  --------------------------------------------------------------------------------------------------------------------
 *  Marco Riedl                   Rie           Vector Informatik GmbH
 *  --------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  --------------------------------------------------------------------------------------------------------------------
 *  Version    Date        Author  Change Id        Description
 *  --------------------------------------------------------------------------------------------------------------------
 *  02.00.00   2016-02-22  Rie     ESCAN00088726    Added support for Fee/NvM
 *  02.01.00   2016-03-09  Rie     ESCAN00088817    No changes
 *                                 ESCAN00088819    No changes
 *  02.02.00   2016-07-05  Rie     ESCAN00090830    No changes
 *  02.02.01   2016-09-06  Rie     ESCAN00091757    No changes
 *  02.02.02   2016-10-17  Rie     ESCAN00092351    No changes
 *  02.03.00   2017-09-19  Rie     ESCAN00094172    No changes
 *                                 ESCAN00095574    No changes
 *  02.04.00   2017-11-29  Rie     ESCAN00096851    No changes
 *                                 ESCAN00097590    No changes
 *                                 ESCAN00097591    No changes
 *  02.04.01   2018-01-11  Rie     ESCAN00097944    No changes
 *                                 ESCAN00097953    No changes
 *  02.05.00   2018-02-08  Rie     ESCAN00097770    No changes
 **********************************************************************************************************************/

#ifndef __WRAPNV_INC_H__
#define __WRAPNV_INC_H__

/***********************************************************************************************************************
 *  INCLUDES
 **********************************************************************************************************************/

#include "WrapNv.h"

#endif   /* __WRAPNV_INC_H__ */

/***********************************************************************************************************************
 *  END OF FILE: WRAPNV_INC.H
 **********************************************************************************************************************/
