/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/** \file
 *  \brief        OEM-specific wrapper for FEE access
 *  -------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \par Copyright
 *  \verbatim
 *  Copyright (c) 2018 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 */
/*********************************************************************************************************************/

/**********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  -------------------------------------------------------------------------------------------------------------------
 *  Marco Riedl                   Rie           Vector Informatik GmbH
 *  -------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version    Date        Author  Change Id        Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  02.00.00   2016-02-05  Rie     -                Initial version
 *  02.01.00   2016-03-11  Rie     -                Fixed macros/defines for EepM use-case
 *                                 -                Update to new interface
 *  02.02.00   2016-07-05  Rie     ESCAN00090830    Added support for no NV driver use-case
 *  02.02.01   2016-09-06  Rie     ESCAN00091757    Fixed MISRA findings
 *  02.02.02   2016-10-17  Rie     ESCAN00092351    Configure used FEE functions
 *  02.03.00   2017-09-19  Rie     ESCAN00094172    No changes
 *                                 ESCAN00095574    Added support for EA
 *  02.04.00   2017-11-29  Rie     ESCAN00096851    No changes
 *                                 ESCAN00097590    Added constants for Record ID check
 *                                 ESCAN00097591    No changes
 *  02.04.01   2018-01-11  Rie     ESCAN00097944    No changes
 *                                 ESCAN00097953    No changes
 *  02.05.00   2018-02-08  Rie     ESCAN00097770    Specify call cycle for main functions
 *********************************************************************************************************************/

#ifndef __WRAPNV_CFG_H__
#define __WRAPNV_CFG_H__

/***********************************************************************************************************************
 *  CONFIGURATION SWITCHES
 **********************************************************************************************************************/

#if defined( WRAPNV_USECASE_FEE )    || \
    defined( WRAPNV_USECASE_EA )    || \
    defined( WRAPNV_USECASE_NVM )
#else
//#  define WRAPNV_USECASE_FEE     
#  define WRAPNV_USECASE_NVM  /*Added by SeoyonElec*/
#endif

#if defined( WRAPNV_USECASE_NVM ) || \
    defined( WRAPNV_USECASE_FEE ) || \
    defined( WRAPNV_USECASE_EA )
/***********************************************************************************************************************
 *  INCLUDES
 **********************************************************************************************************************/

# include "Std_Types.h"
# include "MemIf_Types.h"
# if defined( WRAPNV_USECASE_FEE )
#  include "Fls.h"
#  include "Fee.h"
# elif defined( WRAPNV_USECASE_EA )
#  include "Eep.h"
#  include "Ea.h"
# endif /* WRAPNV_USECASE_* */

/**********************************************************************************************************************
 *  CONFIGURATION DEFINES
 *********************************************************************************************************************/

# if defined( WRAPNV_USECASE_NVM )
/** Prefix of a NV block */
#  define WRAPNV_BLOCK_PREFIX(value)         NvMConf_NvMBlockDescriptor_ ## value /* PRQA S 0342 */ /* MD_WrapNv_0342 */
/** Function called in synchronous API while waiting for the job to be finished. Also used to get a millisecond cycle. */
#  define WRAPNV_TRIGGER_FCT()               (TRUE)
/** Configure whether the Fee/Fls should be initialized during WrapNv initialization */
#  define WRAPNV_DISABLE_MEM_INITIALIZATION
# elif defined( WRAPNV_USECASE_FEE )
/** Prefix of a NV block */
#  define WRAPNV_BLOCK_PREFIX(value)         FeeConf_FeeBlockConfiguration_Fee ## value /* PRQA S 0342 */ /* MD_WrapNv_0342 */
/** Function called in synchronous API while waiting for the job to be finished. Also used to get a millisecond cycle. */
#  define WRAPNV_TRIGGER_FCT()               ((FblRealTimeSupport() & FBL_TM_TRIGGERED) == FBL_TM_TRIGGERED)
/** Configure whether the Fee/Fls should be initialized during WrapNv initialization */
#  define WRAPNV_ENABLE_MEM_INITIALIZATION
# elif defined( WRAPNV_USECASE_EA )
/** Prefix of a NV block */
#  define WRAPNV_BLOCK_PREFIX(value)         EaConf_EaBlockConfiguration_Ea ## value /* PRQA S 0342 */ /* MD_WrapNv_0342 */
/** Function called in synchronous API while waiting for the job to be finished. Also used to get a millisecond cycle. */
#  define WRAPNV_TRIGGER_FCT()               ((FblRealTimeSupport() & FBL_TM_TRIGGERED) == FBL_TM_TRIGGERED)
/** Configure whether the Fee/Fls should be initialized during WrapNv initialization */
#  define WRAPNV_ENABLE_MEM_INITIALIZATION
# endif /* WRAPNV_USECASE_* */

/** Maximum size of all NV blocks */
# define WRAPNV_MAX_BLOCK_LENGTH             0x300u

/** Main function call cycle
 *   0 = Polling
 *  >0 = Millisecond cycle call
 */
# if defined( WRAPNV_USECASE_NVM )
#  define WRAPNV_NVM_CYCLE_MAINFUNCTION      0u       /**< Call cylce for NVM */
# endif /* WRAPNV_USECASE_NVM */
# if defined( WRAPNV_USECASE_FEE ) || \
     defined( WRAPNV_USECASE_EA )
#  define WRAPNV_HWDRV_CYCLE_MAINFUNCTION    0u       /**< Call cylce for HWDRV (Fls/Eep) */
#  define WRAPNV_MEMDRV_CYCLE_MAINFUNCTION   0u       /**< Call cylce for MEMDRV (Fee/Ea) */
# endif

# if defined( WRAPNV_USECASE_FEE )
/** FLS function abstraction */
#  define WRAPNV_HWDRV_INIT                  Fls_Init
#  define WRAPNV_HWDRV_MAINFUNCTION          Fls_MainFunction
/** FLS configuration set used by NV-Wrapper */
#  define WRAPNV_HWDRV_INIT_PARAMETER        FlsConfigSet

/** FEE function abstraction */
#  define WRAPNV_MEMDRV_INIT                 Fee_Init
#  define WRAPNV_MEMDRV_GETSTATUS            Fee_GetStatus
#  define WRAPNV_MEMDRV_GETJOBRESULT         Fee_GetJobResult
#  define WRAPNV_MEMDRV_MAINFUNCTION         Fee_MainFunction
#  define WRAPNV_MEMDRV_READ                 Fee_Read
#  define WRAPNV_MEMDRV_WRITE                Fee_Write
#  define WRAPNV_MEMDRV_INVALIDATEBLOCK      Fee_InvalidateBlock
#  define WRAPNV_MEMDRV_CANCEL               Fee_Cancel

# elif defined( WRAPNV_USECASE_EA )
/** EEP function abstraction */
#  define WRAPNV_HWDRV_INIT                  Eep_Init
#  define WRAPNV_HWDRV_MAINFUNCTION          Eep_MainFunction
/** EEP configuration set used by NV-Wrapper */
#  define WRAPNV_HWDRV_INIT_PARAMETER        V_NULL

/** EA function abstraction */
#  define WRAPNV_MEMDRV_INIT                 Ea_Init
#  define WRAPNV_MEMDRV_GETSTATUS            Ea_GetStatus
#  define WRAPNV_MEMDRV_GETJOBRESULT         Ea_GetJobResult
#  define WRAPNV_MEMDRV_MAINFUNCTION         Ea_MainFunction
#  define WRAPNV_MEMDRV_READ                 Ea_Read
#  define WRAPNV_MEMDRV_WRITE                Ea_Write
#  define WRAPNV_MEMDRV_INVALIDATEBLOCK      Ea_InvalidateBlock
#  define WRAPNV_MEMDRV_CANCEL               Ea_Cancel
# endif
#endif

/***********************************************************************************************************************
 *  COMPATIBILITY DEFINES
 **********************************************************************************************************************/

#if defined( FBL_MTAB_NO_OF_BLOCKS )
# define WRAPNV_COUNT_LOGICAL_BLOCKS   FBL_MTAB_NO_OF_BLOCKS
#else
/* NOTE: define has to be adapted */
# define WRAPNV_COUNT_LOGICAL_BLOCKS   3u
#endif
#if defined( SWM_DATA_MAX_NOAR )
# define WRAPNV_COUNT_SEGMENTS         SWM_DATA_MAX_NOAR
#else
/* NOTE: define has to be adapted */
# define WRAPNV_COUNT_SEGMENTS         16u
#endif
#if defined( FBL_FINGERPRINT_HISTORY_SIZE )
# define WRAPNV_COUNT_FINGERPRINT      FBL_FINGERPRINT_HISTORY_SIZE
#else
/* NOTE: define has to be adapted */
# define WRAPNV_COUNT_FINGERPRINT      5u
#endif

#if defined( WRAPNV_USECASE_NVM ) || \
      defined( WRAPNV_USECASE_FEE ) || \
      defined( WRAPNV_USECASE_EA )
/**********************************************************************************************************************
 *  DEFINES
 *********************************************************************************************************************/

/* Compatibility defines - only used in application */
#if defined( VGEN_ENABLE_CANFBL )
#else
# if defined( RESET_RESPONSE_NOT_REQUIRED )
# else
#  define RESET_RESPONSE_NOT_REQUIRED 0x00
# endif
# if defined( RESET_RESPONSE_SDS_REQUIRED )
# else
#  define RESET_RESPONSE_SDS_REQUIRED 0x01
# endif
# if defined( RESET_RESPONSE_ECURESET_REQUIRED )
# else
#  define RESET_RESPONSE_ECURESET_REQUIRED 0x02
# endif
# if defined( RESET_RESPONSE_KEYOFFON_REQUIRED )
# else
#  define RESET_RESPONSE_KEYOFFON_REQUIRED 0x03
# endif

# if defined( kEepFblReprogram )
# else
#  define kEepFblReprogram              0xB5u
# endif
# if defined( kEepFblActive )
# else
#  define kEepFblActive                 0xA4u
# endif
# if defined( kEepFblVpm )
# else
#  define kEepFblVpm                    0xC6u
# endif
# if defined( kEepFblLinkControl )
# else
#  define kEepFblLinkControl            0x93u
# endif

# if defined( kEepApplReprogrammed )
# else
#  define kEepApplReprogrammed          0xD2u
# endif
# if defined( kEepApplNotReprogrammed )
# else
#  define kEepApplNotReprogrammed       0xFFu
# endif
#endif

/*  Memory Layout
*   |
*   +-- ResetFlags
*   |   |
*   |   +-- ProgReqFlag                                           Reprogramming request flag
*   |   |
*   |   +-- ResetResponseFlag                                     Reset response flag
*   |   |
*   |   +-- BaudrateParam                                         Baudrate parameter
*   |   |
*   |   +-- TesterConnection                                      Tester communication connection
*   |   |
*   |   +-- ProgMarker                                            Application updated flag
*   |
*   +-- ValidityFlags                                             Validation bitfield
*   |
*   +-- FingerprintNumber                                         Number of fingerprints
*   |   |
*   |   +-- SwFingerprintNumber                                   Number of software fingerprints
*   |   |
*   |   +-- DataFingerprintNumber                                 Number of data fingerprints
*   |   |
*   |   +-- BootFingerprintNumber                                 Number of boot fingerprints
*   |
*   +-- SwFingerprint                                             Software fingerprint
*   |
*   +-- DataFingerprint                                           Data fingerprint
*   |
*   +-- BootFingerprint                                           Boot fingerprint
*   |
*   +-- SigInfo                                                   Signature information (for each logical block)
*   |   |
*   |   +-- SigType                                               Signature type
*   |   |
*   |   +-- SigStart                                              Signature start address
*   |   |
*   |   +-- SigLength                                             Signature length
*   |   |
*   |   +-- SigSegmentNumber                                      Number of segments
*   |
*   +-- SigValue                                                  Signature value (for each logical block)
*   |
*   +-- SigSegments                                               Segment information (for each segment of each logical block)
*   |   |
*   |   +-- SigSegmentStart                                       Start address of segment
*   |   |
*   |   +-- SigSegmentLength                                      Length of segment
*   |
*   +-- VpmStates                                                 VPM operation states
*   |   |
*   |   +-- VpmProcessedBlock                                     Index of the processed block
*   |   |
*   |   +-- VpmBlockStatus                                        Status of the currently processed block
*   |   |
*   |   +-- VpmActive                                             Active status
*   |
*   +-- VpmRequest                                                VPM request
*   |   |
*   |   +-- VpmPatchAddress                                       Patch address
*   |   |
*   |   +-- VpmPatchLength                                        Patch length (limited by FEE record)
*   |   |
*   |   +-- VpmPatchData                                          Patch data
*   |
*   +-- VpmValidityFlagsCopy                                      Copy of validity flags
*/

/* Reset flags */
#define kEepSizeProgReqFlag                  0x01u
#define kEepSizeResetResponseFlag            0x01u
#define kEepSizeBaudrateParam                0x01u
#define kEepSizeTesterConnection             0x01u
#define kEepSizeProgMarker                   0x01u
#define kEepSizeResetFlags                   (  kEepSizeProgReqFlag              \
                                              + kEepSizeResetResponseFlag        \
                                              + kEepSizeBaudrateParam            \
                                              + kEepSizeTesterConnection         \
                                              + kEepSizeProgMarker)

/* Validity flags */
#define kEepSizeValidityFlags                0x01u

/* Number of fingerprints */
#define kEepSizeSwFingerprintNumber          0x01u
#define kEepSizeDataFingerprintNumber        0x01u
#define kEepSizeBootFingerprintNumber        0x01u
#define kEepSizeFingerprintNumber            (  kEepSizeSwFingerprintNumber      \
                                              + kEepSizeDataFingerprintNumber    \
                                              + kEepSizeBootFingerprintNumber)

/* Software fingerprint */
#define kEepSizeSwFingerprint                0x11u

/* Data fingerprint */
#define kEepSizeDataFingerprint              0x11u

/* Boot fingerprint */
#define kEepSizeBootFingerprint              0x11u

/* Signature info */
#define kEepSizeSigType                      0x01u
#define kEepSizeSigStart                     0x04u
#define kEepSizeSigLength                    0x04u
#define kEepSizeSigSegmentNumber             0x01u
#define kEepSizeSigInfo                      (  kEepSizeSigType                  \
                                              + kEepSizeSigStart                 \
                                              + kEepSizeSigLength                \
                                              + kEepSizeSigSegmentNumber)

/* Signature value */
#define kEepSizeSigValue                     0x80u

/* Signature segments */
#define kEepSizeSigSegmentStart              0x04u
#define kEepSizeSigSegmentLength             0x04u
#define kEepSizeSigSegments                  (  kEepSizeSigSegmentStart          \
                                              + kEepSizeSigSegmentLength)

/* VPM operation states */
#define kEepSizeVpmPatchCount                0x02u
#define kEepSizeVpmActive                    0x01u
#define kEepSizeVpmProcessedBlock            0x01u
#define kEepSizeVpmBlockStatus               0x01u
#define kEepSizeVpmState                     (  kEepSizeVpmPatchCount            \
                                              + kEepSizeVpmActive                \
                                              + kEepSizeVpmProcessedBlock        \
                                              + kEepSizeVpmBlockStatus)

/* VPM request */
#define kEepSizeVpmPatchAddress              0x04u
#define kEepSizeVpmPatchLength               0x02u
#define kEepSizeVpmPatchData                 0x1Au
#define kEepSizeVpmRequest                   (  kEepSizeVpmPatchAddress          \
                                              + kEepSizeVpmPatchLength           \
                                              + kEepSizeVpmPatchData)

/* VPM copy of validity flags */
#define kEepSizeVpmValidityFlagsCopy         0x01u

/* Reset flags */
#define WRAPNV_DATASET_RESETFLAGS                    1u
#define WRAPNV_RECORD_ID_PROGREQFLAG                 0u
#define WRAPNV_RECORD_ID_RESETRESPONSEFLAG           1u
#define WRAPNV_RECORD_ID_BAUDRATEPARAM               2u
#define WRAPNV_RECORD_ID_TESTERCONNECTION            3u
#define WRAPNV_RECORD_ID_PROGMARKER                  4u

/* Validity flags */
#define WRAPNV_DATASET_VALIDITYFLAGS                 1u
#define WRAPNV_RECORD_ID_VALIDITYFLAGS               5u

/* Number of fingerprints */
#define WRAPNV_DATASET_NUMBEROFFINGERPRINTS          1u
#define WRAPNV_RECORD_ID_SWFINGERPRINTNUMBER         6u
#define WRAPNV_RECORD_ID_DATAFINGERPRINTNUMBER       7u
#define WRAPNV_RECORD_ID_BOOTFINGERPRINTNUMBER       8u

/* Software Fingerprint */
#define WRAPNV_DATASET_SWFINGERPRINT                 WRAPNV_COUNT_FINGERPRINT
#define WRAPNV_RECORD_ID_SWFINGERPRINT               9u

/* Data Fingerprint */
#define WRAPNV_DATASET_DATAFINGERPRINT               WRAPNV_COUNT_FINGERPRINT
#define WRAPNV_RECORD_ID_DATAFINGERPRINT             10u

/* Boot Fingerprint */
#define WRAPNV_DATASET_BOOTFINGERPRINT               WRAPNV_COUNT_FINGERPRINT
#define WRAPNV_RECORD_ID_BOOTFINGERPRINT             11u

/* Signature information */
#define WRAPNV_DATASET_SIGINFO                       WRAPNV_COUNT_LOGICAL_BLOCKS
#define WRAPNV_RECORD_ID_SIGTYPE                     12u
#define WRAPNV_RECORD_ID_SIGSTART                    13u
#define WRAPNV_RECORD_ID_SIGLENGTH                   14u
#define WRAPNV_RECORD_ID_SIGSEGMENTNUMBER            15u

/* Signature value */
#define WRAPNV_DATASET_SIGVALUE                      WRAPNV_COUNT_LOGICAL_BLOCKS
#define WRAPNV_RECORD_ID_SIGVALUE                    16u

/* Signature segments */
#define WRAPNV_DATASET_SIGSEGMENTS                   (WRAPNV_COUNT_LOGICAL_BLOCKS * WRAPNV_COUNT_SEGMENTS)
#define WRAPNV_RECORD_ID_SIGSEGMENTSTART             17u
#define WRAPNV_RECORD_ID_SIGSEGMENTLENGTH            18u

/* VPM operation states */
#define WRAPNV_DATASET_VPMSTATES                     1u
#define WRAPNV_RECORD_ID_VPMPATCHCOUNT               19u
#define WRAPNV_RECORD_ID_VPMACTIVE                   20u
#define WRAPNV_RECORD_ID_VPMPROCESSEDBLOCK           21u
#define WRAPNV_RECORD_ID_VPMBLOCKSTATUS              22u

/* VPM request */
#define WRAPNV_DATASET_VPMREQUEST                    8u
#define WRAPNV_RECORD_ID_VPMPATCH                    23u
#define WRAPNV_RECORD_ID_VPMPATCHADDRESS             24u
#define WRAPNV_RECORD_ID_VPMPATCHLENGTH              25u
#define WRAPNV_RECORD_ID_VPMPATCHDATA                26u

/* VPM copy of validity flags */
#define WRAPNV_DATASET_VPMVALIDITYFLAGSCOPY          1u
#define WRAPNV_RECORD_ID_VPMVALIDITYFLAGSCOPY        27u

#define WRAPNV_RECORD_ID_MAX                         (WRAPNV_RECORD_ID_VPMVALIDITYFLAGSCOPY)

/***********************************************************************************************************************
 *  TYPEDEFS
 **********************************************************************************************************************/

/** Specifies how the NV block is configured */
typedef enum
{
   WRAPNV_RECORDACCESS_STRUCTURED = 0u,      /**< NV block consists of several elements */
   WRAPNV_RECORDACCESS_SINGLE                /**< NV block consists of a single element */
} tWrapNvRecordAccess;

/** NV block configuration strucutre */
typedef struct
{
   tWrapNvRecordAccess blockDataAccess;      /**< Access variat to the NV block */
   uint16 blockNumber;                       /**< Number of NV block */
   uint16 blockLength;                       /**< Length of NV block */
   uint16 blockMaxDatasets;                  /**< Maximum datasets alloweed for the NV block */
   P2CONST(uint8, AUTOMATIC, WRAPNV_CONST) blockDefaultValue;           /**< Default value for NV block */
} tWrapNvBlockTbl;

/** NV record configuration structure */
typedef struct
{
   uint16 recordDataOffset;                  /**< Offset in NV block to the data */
   uint16 recordDataLength;                  /**< Length of data inside the NV block */
   P2CONST(tWrapNvBlockTbl, AUTOMATIC, WRAPNV_CONST) recordBlockPtr;    /**< Pointer to the NV block */
} tWrapNvRecordTbl;

/***********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 **********************************************************************************************************************/

extern CONST(tWrapNvRecordTbl, WRAPNV_CONST) kWrapNvRecordTbl[];
extern CONST(uint16, WRAPNV_CONST) kWrapNvNrOfBlock;
extern CONST(uint16, WRAPNV_CONST) kWrapNvNrOfRecord;

/***********************************************************************************************************************
 *  ACCESS MACROS
 **********************************************************************************************************************/

/* Reset flags */
#define ApplFblNvReadProgReqFlag(idx, buf)                        WrapNv_ReadSync(WRAPNV_RECORD_ID_PROGREQFLAG, (idx), (buf))
#define ApplFblNvWriteProgReqFlag(idx, buf)                       WrapNv_WriteSync(WRAPNV_RECORD_ID_PROGREQFLAG, (idx), (buf))
#define ApplFblNvReadAsyncProgReqFlag(idx, buf, op)               WrapNv_ReadAsync(WRAPNV_RECORD_ID_PROGREQFLAG, (idx), (buf), (op))
#define ApplFblNvWriteAsyncProgReqFlag(idx, buf, op)              WrapNv_WriteAsync(WRAPNV_RECORD_ID_PROGREQFLAG, (idx), (buf), (op))

#define ApplFblNvReadResetResponseFlag(idx, buf)                  WrapNv_ReadSync(WRAPNV_RECORD_ID_RESETRESPONSEFLAG, (idx), (buf))
#define ApplFblNvWriteResetResponseFlag(idx, buf)                 WrapNv_WriteSync(WRAPNV_RECORD_ID_RESETRESPONSEFLAG, (idx), (buf))
#define ApplFblNvReadAsyncResetResponseFlag(idx, buf, op)         WrapNv_ReadAsync(WRAPNV_RECORD_ID_RESETRESPONSEFLAG, (idx), (buf), (op))
#define ApplFblNvWriteAsyncResetResponseFlag(idx, buf, op)        WrapNv_WriteAsync(WRAPNV_RECORD_ID_RESETRESPONSEFLAG, (idx), (buf), (op))

#define ApplFblNvReadBaudrateParam(idx, buf)                      WrapNv_ReadSync(WRAPNV_RECORD_ID_BAUDRATEPARAM, (idx), (buf))
#define ApplFblNvWriteBaudrateParam(idx, buf)                     WrapNv_WriteSync(WRAPNV_RECORD_ID_BAUDRATEPARAM, (idx), (buf))
#define ApplFblNvReadAsyncBaudrateParam(idx, buf, op)             WrapNv_ReadAsync(WRAPNV_RECORD_ID_BAUDRATEPARAM, (idx), (buf), (op))
#define ApplFblNvWriteAsyncBaudrateParam(idx, buf, op)            WrapNv_WriteAsync(WRAPNV_RECORD_ID_BAUDRATEPARAM, (idx), (buf), (op))

#define ApplFblNvReadTesterConnection(idx, buf)                   WrapNv_ReadSync(WRAPNV_RECORD_ID_TESTERCONNECTION, (idx), (buf))
#define ApplFblNvWriteTesterConnection(idx, buf)                  WrapNv_WriteSync(WRAPNV_RECORD_ID_TESTERCONNECTION, (idx), (buf))
#define ApplFblNvReadAsyncTesterConnection(idx, buf, op)          WrapNv_ReadAsync(WRAPNV_RECORD_ID_TESTERCONNECTION, (idx), (buf), (op))
#define ApplFblNvWriteAsyncTesterConnection(idx, buf, op)         WrapNv_WriteAsync(WRAPNV_RECORD_ID_TESTERCONNECTION, (idx), (buf), (op))

#define ApplFblNvReadProgMarker(idx, buf)                         WrapNv_ReadSync(WRAPNV_RECORD_ID_PROGMARKER, (idx), (buf))
#define ApplFblNvWriteProgMarker(idx, buf)                        WrapNv_WriteSync(WRAPNV_RECORD_ID_PROGMARKER, (idx), (buf))
#define ApplFblNvReadAsyncProgMarker(idx, buf, op)                WrapNv_ReadAsync(WRAPNV_RECORD_ID_PROGMARKER, (idx), (buf), (op))
#define ApplFblNvWriteAsyncProgMarker(idx, buf, op)               WrapNv_WriteAsync(WRAPNV_RECORD_ID_PROGMARKER, (idx), (buf), (op))

/* Validity flags */
#define ApplFblNvReadValidityFlags(idx, buf)                      WrapNv_ReadSync(WRAPNV_RECORD_ID_VALIDITYFLAGS, (idx), (buf))
#define ApplFblNvWriteValidityFlags(idx, buf)                     WrapNv_WriteSync(WRAPNV_RECORD_ID_VALIDITYFLAGS, (idx), (buf))
#define ApplFblNvReadAsyncValidityFlags(idx, buf, op)             WrapNv_ReadAsync(WRAPNV_RECORD_ID_VALIDITYFLAGS, (idx), (buf), (op))
#define ApplFblNvWriteAsyncValidityFlags(idx, buf, op)            WrapNv_WriteAsync(WRAPNV_RECORD_ID_VALIDITYFLAGS, (idx), (buf), (op))

/* Number of fingerprints */
#define ApplFblNvReadSwFingerprintNumber(idx, buf)                WrapNv_ReadSync(WRAPNV_RECORD_ID_SWFINGERPRINTNUMBER, (idx), (buf))
#define ApplFblNvWriteSwFingerprintNumber(idx, buf)               WrapNv_WriteSync(WRAPNV_RECORD_ID_SWFINGERPRINTNUMBER, (idx), (buf))
#define ApplFblNvReadAsyncSwFingerprintNumber(idx, buf, op)       WrapNv_ReadAsync(WRAPNV_RECORD_ID_SWFINGERPRINTNUMBER, (idx), (buf), (op))
#define ApplFblNvWriteAsyncSwFingerprintNumber(idx, buf, op)      WrapNv_WriteAsync(WRAPNV_RECORD_ID_SWFINGERPRINTNUMBER, (idx), (buf), (op))

#define ApplFblNvReadDataFingerprintNumber(idx, buf)              WrapNv_ReadSync(WRAPNV_RECORD_ID_DATAFINGERPRINTNUMBER, (idx), (buf))
#define ApplFblNvWriteDataFingerprintNumber(idx, buf)             WrapNv_WriteSync(WRAPNV_RECORD_ID_DATAFINGERPRINTNUMBER, (idx), (buf))
#define ApplFblNvReadAsyncDataFingerprintNumber(idx, buf, op)     WrapNv_ReadAsync(WRAPNV_RECORD_ID_DATAFINGERPRINTNUMBER, (idx), (buf), (op))
#define ApplFblNvWriteAsyncDataFingerprintNumber(idx, buf, op)    WrapNv_WriteAsync(WRAPNV_RECORD_ID_DATAFINGERPRINTNUMBER, (idx), (buf), (op))

#define ApplFblNvReadBootFingerprintNumber(idx, buf)              WrapNv_ReadSync(WRAPNV_RECORD_ID_BOOTFINGERPRINTNUMBER, (idx), (buf))
#define ApplFblNvWriteBootFingerprintNumber(idx, buf)             WrapNv_WriteSync(WRAPNV_RECORD_ID_BOOTFINGERPRINTNUMBER, (idx), (buf))
#define ApplFblNvReadAsyncBootFingerprintNumber(idx, buf, op)     WrapNv_ReadAsync(WRAPNV_RECORD_ID_BOOTFINGERPRINTNUMBER, (idx), (buf), (op))
#define ApplFblNvWriteAsyncBootFingerprintNumber(idx, buf, op)    WrapNv_WriteAsync(WRAPNV_RECORD_ID_BOOTFINGERPRINTNUMBER, (idx), (buf), (op))

/* Software fingerprints */
#define ApplFblNvReadSwFingerprint(idx, buf)                      WrapNv_ReadSync(WRAPNV_RECORD_ID_SWFINGERPRINT, (idx), (buf))
#define ApplFblNvWriteSwFingerprint(idx, buf)                     WrapNv_WriteSync(WRAPNV_RECORD_ID_SWFINGERPRINT, (idx), (buf))
#define ApplFblNvReadAsyncSwFingerprint(idx, buf, op)             WrapNv_ReadAsync(WRAPNV_RECORD_ID_SWFINGERPRINT, (idx), (buf), (op))
#define ApplFblNvWriteAsyncSwFingerprint(idx, buf, op)            WrapNv_WriteAsync(WRAPNV_RECORD_ID_SWFINGERPRINT, (idx), (buf), (op))

/* Data fingerprints */
#define ApplFblNvReadDataFingerprint(idx, buf)                    WrapNv_ReadSync(WRAPNV_RECORD_ID_DATAFINGERPRINT, (idx), (buf))
#define ApplFblNvWriteDataFingerprint(idx, buf)                   WrapNv_WriteSync(WRAPNV_RECORD_ID_DATAFINGERPRINT, (idx), (buf))
#define ApplFblNvReadAsyncDataFingerprint(idx, buf, op)           WrapNv_ReadAsync(WRAPNV_RECORD_ID_DATAFINGERPRINT, (idx), (buf), (op))
#define ApplFblNvWriteAsyncDataFingerprint(idx, buf, op)          WrapNv_WriteAsync(WRAPNV_RECORD_ID_DATAFINGERPRINT, (idx), (buf), (op))

/* Boot fingerprints */
#define ApplFblNvReadBootFingerprint(idx, buf)                    WrapNv_ReadSync(WRAPNV_RECORD_ID_BOOTFINGERPRINT, (idx), (buf))
#define ApplFblNvWriteBootFingerprint(idx, buf)                   WrapNv_WriteSync(WRAPNV_RECORD_ID_BOOTFINGERPRINT, (idx), (buf))
#define ApplFblNvReadAsyncBootFingerprint(idx, buf, op)           WrapNv_ReadAsync(WRAPNV_RECORD_ID_BOOTFINGERPRINT, (idx), (buf), (op))
#define ApplFblNvWriteAsyncBootFingerprint(idx, buf, op)          WrapNv_WriteAsync(WRAPNV_RECORD_ID_BOOTFINGERPRINT, (idx), (buf), (op))

/* Signature information (for each logical block) */
#define ApplFblNvReadSigType(idx, buf)                            WrapNv_ReadSync(WRAPNV_RECORD_ID_SIGTYPE, (idx), (buf))
#define ApplFblNvWriteSigType(idx, buf)                           WrapNv_WriteSync(WRAPNV_RECORD_ID_SIGTYPE, (idx), (buf))
#define ApplFblNvReadAsyncSigType(idx, buf, op)                   WrapNv_ReadAsync(WRAPNV_RECORD_ID_SIGTYPE, (idx), (buf), (op))
#define ApplFblNvWriteAsyncSigType(idx, buf, op)                  WrapNv_WriteAsync(WRAPNV_RECORD_ID_SIGTYPE, (idx), (buf), (op))

#define ApplFblNvReadSigStart(idx, buf)                           WrapNv_ReadSync(WRAPNV_RECORD_ID_SIGSTART, (idx), (buf))
#define ApplFblNvWriteSigStart(idx, buf)                          WrapNv_WriteSync(WRAPNV_RECORD_ID_SIGSTART, (idx), (buf))
#define ApplFblNvReadAsyncSigStart(idx, buf, op)                  WrapNv_ReadAsync(WRAPNV_RECORD_ID_SIGSTART, (idx), (buf), (op))
#define ApplFblNvWriteAsyncSigStart(idx, buf, op)                 WrapNv_WriteAsync(WRAPNV_RECORD_ID_SIGSTART, (idx), (buf), (op))

#define ApplFblNvReadSigLength(idx, buf)                          WrapNv_ReadSync(WRAPNV_RECORD_ID_SIGLENGTH, (idx), (buf))
#define ApplFblNvWriteSigLength(idx, buf)                         WrapNv_WriteSync(WRAPNV_RECORD_ID_SIGLENGTH, (idx), (buf))
#define ApplFblNvReadAsyncSigLength(idx, buf, op)                 WrapNv_ReadAsync(WRAPNV_RECORD_ID_SIGLENGTH, (idx), (buf), (op))
#define ApplFblNvWriteAsyncSigLength(idx, buf, op)                WrapNv_WriteAsync(WRAPNV_RECORD_ID_SIGLENGTH, (idx), (buf), (op))

#define ApplFblNvReadSigSegmentNumber(idx, buf)                   WrapNv_ReadSync(WRAPNV_RECORD_ID_SIGSEGMENTNUMBER, (idx), (buf))
#define ApplFblNvWriteSigSegmentNumber(idx, buf)                  WrapNv_WriteSync(WRAPNV_RECORD_ID_SIGSEGMENTNUMBER, (idx), (buf))
#define ApplFblNvReadAsyncSigSegmentNumber(idx, buf, op)          WrapNv_ReadAsync(WRAPNV_RECORD_ID_SIGSEGMENTNUMBER, (idx), (buf), (op))
#define ApplFblNvWriteAsyncSigSegmentNumber(idx, buf, op)         WrapNv_WriteAsync(WRAPNV_RECORD_ID_SIGSEGMENTNUMBER, (idx), (buf), (op))

/* Signature value */
#define ApplFblNvReadSigValue(idx, buf)                           WrapNv_ReadSync(WRAPNV_RECORD_ID_SIGVALUE, (idx), (buf))
#define ApplFblNvWriteSigValue(idx, buf)                          WrapNv_WriteSync(WRAPNV_RECORD_ID_SIGVALUE, (idx), (buf))
#define ApplFblNvReadAsyncSigValue(idx, buf, op)                  WrapNv_ReadAsync(WRAPNV_RECORD_ID_SIGVALUE, (idx), (buf), (op))
#define ApplFblNvWriteAsyncSigValue(idx, buf, op)                 WrapNv_WriteAsync(WRAPNV_RECORD_ID_SIGVALUE, (idx), (buf), (op))

/* Segment information (for each segment of each logical block) */
#define ApplFblNvReadSigSegmentStart(idx, buf)                    WrapNv_ReadSync(WRAPNV_RECORD_ID_SIGSEGMENTSTART, (idx), (buf))
#define ApplFblNvWriteSigSegmentStart(idx, buf)                   WrapNv_WriteSync(WRAPNV_RECORD_ID_SIGSEGMENTSTART, (idx), (buf))
#define ApplFblNvReadAsyncSigSegmentStart(idx, buf, op)           WrapNv_ReadAsync(WRAPNV_RECORD_ID_SIGSEGMENTSTART, (idx), (buf), (op))
#define ApplFblNvWriteAsyncSigSegmentStart(idx, buf, op)          WrapNv_WriteAsync(WRAPNV_RECORD_ID_SIGSEGMENTSTART, (idx), (buf), (op))

#define ApplFblNvReadSigSegmentLength(idx, buf)                   WrapNv_ReadSync(WRAPNV_RECORD_ID_SIGSEGMENTLENGTH, (idx), (buf))
#define ApplFblNvWriteSigSegmentLength(idx, buf)                  WrapNv_WriteSync(WRAPNV_RECORD_ID_SIGSEGMENTLENGTH, (idx), (buf))
#define ApplFblNvReadAsyncSigSegmentLength(idx, buf, op)          WrapNv_ReadAsync(WRAPNV_RECORD_ID_SIGSEGMENTLENGTH, (idx), (buf), (op))
#define ApplFblNvWriteAsyncSigSegmentLength(idx, buf, op)         WrapNv_WriteAsync(WRAPNV_RECORD_ID_SIGSEGMENTLENGTH, (idx), (buf), (op))

/* VPM state */
#define ApplFblNvReadVpmPatchCount(idx, buf)                      WrapNv_ReadSync(WRAPNV_RECORD_ID_VPMPATCHCOUNT, (idx), (buf))
#define ApplFblNvWriteVpmPatchCount(idx, buf)                     WrapNv_WriteSync(WRAPNV_RECORD_ID_VPMPATCHCOUNT, (idx), (buf))
#define ApplFblNvReadAsyncVpmPatchCount(idx, buf, op)             WrapNv_ReadAsync(WRAPNV_RECORD_ID_VPMPATCHCOUNT, (idx), (buf), (op))
#define ApplFblNvWriteAsyncVpmPatchCount(idx, buf, op)            WrapNv_WriteAsync(WRAPNV_RECORD_ID_VPMPATCHCOUNT, (idx), (buf), (op))

#define ApplFblNvReadVpmActive(idx, buf)                          WrapNv_ReadSync(WRAPNV_RECORD_ID_VPMACTIVE, (idx), (buf))
#define ApplFblNvWriteVpmActive(idx, buf)                         WrapNv_WriteSync(WRAPNV_RECORD_ID_VPMACTIVE, (idx), (buf))
#define ApplFblNvReadAsyncVpmActive(idx, buf, op)                 WrapNv_ReadAsync(WRAPNV_RECORD_ID_VPMACTIVE, (idx), (buf), (op))
#define ApplFblNvWriteAsyncVpmActive(idx, buf, op)                WrapNv_WriteAsync(WRAPNV_RECORD_ID_VPMACTIVE, (idx), (buf), (op))

#define ApplFblNvReadVpmProcessedBlock(idx, buf)                  WrapNv_ReadSync(WRAPNV_RECORD_ID_VPMPROCESSEDBLOCK, (idx), (buf))
#define ApplFblNvWriteVpmProcessedBlock(idx, buf)                 WrapNv_WriteSync(WRAPNV_RECORD_ID_VPMPROCESSEDBLOCK, (idx), (buf))
#define ApplFblNvReadAsyncVpmProcessedBlock(idx, buf, op)         WrapNv_ReadAsync(WRAPNV_RECORD_ID_VPMPROCESSEDBLOCK, (idx), (buf), (op))
#define ApplFblNvWriteAsyncVpmProcessedBlock(idx, buf, op)        WrapNv_WriteAsync(WRAPNV_RECORD_ID_VPMPROCESSEDBLOCK, (idx), (buf), (op))

#define ApplFblNvReadVpmBlockStatus(idx, buf)                     WrapNv_ReadSync(WRAPNV_RECORD_ID_VPMBLOCKSTATUS, (idx), (buf))
#define ApplFblNvWriteVpmBlockStatus(idx, buf)                    WrapNv_WriteSync(WRAPNV_RECORD_ID_VPMBLOCKSTATUS, (idx), (buf))
#define ApplFblNvReadAsyncVpmBlockStatus(idx, buf, op)            WrapNv_ReadAsync(WRAPNV_RECORD_ID_VPMBLOCKSTATUS, (idx), (buf), (op))
#define ApplFblNvWriteAsyncVpmBlockStatus(idx, buf, op)           WrapNv_WriteAsync(WRAPNV_RECORD_ID_VPMBLOCKSTATUS, (idx), (buf), (op))

/* VPM request */
#define ApplFblNvReadVpmPatch(idx, buf)                           WrapNv_ReadSync(WRAPNV_RECORD_ID_VPMPATCH, (idx), (buf))
#define ApplFblNvWriteVpmPatch(idx, buf)                          WrapNv_WriteSync(WRAPNV_RECORD_ID_VPMPATCH, (idx), (buf))
#define ApplFblNvReadAsyncVpmPatch(idx, buf, op)                  WrapNv_ReadAsync(WRAPNV_RECORD_ID_VPMPATCH, (idx), (buf), (op))
#define ApplFblNvWriteAsyncVpmPatch(idx, buf, op)                 WrapNv_WriteAsync(WRAPNV_RECORD_ID_VPMPATCH, (idx), (buf), (op))
#define ApplFblNvDeleteVpmPatch(idx)                              WrapNv_DeleteSync(WRAPNV_RECORD_ID_VPMPATCH, (idx))
#define ApplFblNvDeleteAsyncVpmPatch(idx, op)                     WrapNv_DeleteAsync(WRAPNV_RECORD_ID_VPMPATCH, (idx), (op))

#define ApplFblNvReadVpmPatchAddress(idx, buf)                    WrapNv_ReadSync(WRAPNV_RECORD_ID_VPMPATCHADDRESS, (idx), (buf))
#define ApplFblNvWriteVpmPatchAddress(idx, buf)                   WrapNv_WriteSync(WRAPNV_RECORD_ID_VPMPATCHADDRESS, (idx), (buf))
#define ApplFblNvReadAsyncVpmPatchAddress(idx, buf, op)           WrapNv_ReadAsync(WRAPNV_RECORD_ID_VPMPATCHADDRESS, (idx), (buf), (op))
#define ApplFblNvWriteAsyncVpmPatchAddress(idx, buf, op)          WrapNv_WriteAsync(WRAPNV_RECORD_ID_VPMPATCHADDRESS, (idx), (buf), (op))

#define ApplFblNvReadVpmPatchLength(idx, buf)                     WrapNv_ReadSync(WRAPNV_RECORD_ID_VPMPATCHLENGTH, (idx), (buf))
#define ApplFblNvWriteVpmPatchLength(idx, buf)                    WrapNv_WriteSync(WRAPNV_RECORD_ID_VPMPATCHLENGTH, (idx), (buf))
#define ApplFblNvReadAsyncVpmPatchLength(idx, buf, op)            WrapNv_ReadAsync(WRAPNV_RECORD_ID_VPMPATCHLENGTH, (idx), (buf), (op))
#define ApplFblNvWriteAsyncVpmPatchLength(idx, buf, op)           WrapNv_WriteAsync(WRAPNV_RECORD_ID_VPMPATCHLENGTH, (idx), (buf), (op))

#define ApplFblNvReadVpmPatchData(idx, buf)                       WrapNv_ReadSync(WRAPNV_RECORD_ID_VPMPATCHDATA, (idx), (buf))
#define ApplFblNvWriteVpmPatchData(idx, buf)                      WrapNv_WriteSync(WRAPNV_RECORD_ID_VPMPATCHDATA, (idx), (buf))
#define ApplFblNvReadAsyncVpmPatchData(idx, buf, op)              WrapNv_ReadAsync(WRAPNV_RECORD_ID_VPMPATCHDATA, (idx), (buf), (op))
#define ApplFblNvWriteAsyncVpmPatchData(idx, buf, op)             WrapNv_WriteAsync(WRAPNV_RECORD_ID_VPMPATCHDATA, (idx), (buf), (op))
#define ApplFblNvReadPartialVpmPatchData(idx, buf, offset, length) WrapNv_ReadPartialSync(WRAPNV_RECORD_ID_VPMPATCHDATA, (idx), (buf), (offset), (length))
#define ApplFblNvReadPartialAsyncVpmPatchData(idx, buf, offset, length, op) WrapNv_ReadPartialAsync(WRAPNV_RECORD_ID_VPMPATCHDATA, (idx), (buf), (offset), (length), (op))

/* VPM copy of validity flags */
#define ApplFblNvReadVpmValidityFlagsCopy(idx, buf)               WrapNv_ReadSync(WRAPNV_RECORD_ID_VPMVALIDITYFLAGSCOPY, (idx), (buf))
#define ApplFblNvWriteVpmValidityFlagsCopy(idx, buf)              WrapNv_WriteSync(WRAPNV_RECORD_ID_VPMVALIDITYFLAGSCOPY, (idx), (buf))
#define ApplFblNvReadAsyncVpmValidityFlagsCopy(idx, buf, op)      WrapNv_ReadAsync(WRAPNV_RECORD_ID_VPMVALIDITYFLAGSCOPY, (idx), (buf), (op))
#define ApplFblNvWriteAsyncVpmValidityFlagsCopy(idx, buf, op)     WrapNv_WriteAsync(WRAPNV_RECORD_ID_VPMVALIDITYFLAGSCOPY, (idx), (buf), (op))

#endif /* WRAPNV_USECASE_NONE */

#endif /* __WRAPNV_CFG_H__ */

/***********************************************************************************************************************
 *  END OF FILE: WRAPNV_CFG.H
 **********************************************************************************************************************/
