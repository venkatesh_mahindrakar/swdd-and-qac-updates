/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/** \file
 *  \brief        OEM-specific wrapper for FEE access
 *  -------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \par Copyright
 *  \verbatim
 *  Copyright (c) 2018 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 */
/*********************************************************************************************************************/

/**********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  -------------------------------------------------------------------------------------------------------------------
 *  Marco Riedl                   Rie           Vector Informatik GmbH
 *  -------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version    Date        Author  Change Id        Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  02.00.00   2016-02-05  Rie     -                Initial version
 *  02.01.00   2016-03-11  Rie     -                Update to new interface
 *  02.02.00   2016-07-05  Rie     ESCAN00090830    No changes
 *  02.02.01   2016-09-06  Rie     ESCAN00091757    No changes
 *  02.02.02   2016-10-17  Rie     ESCAN00092351    No changes
 *  02.03.00   2017-09-19  Rie     ESCAN00094172    No changes
 *                                 ESCAN00095574    No changes
 *  02.04.00   2017-11-29  Rie     ESCAN00096851    No changes
 *                                 ESCAN00097590    Added constants for Record ID check
 *                                 ESCAN00097591    No changes
 *  02.04.01   2018-01-11  Rie     ESCAN00097944    No changes
 *                                 ESCAN00097953    No changes
 *  02.05.00   2018-02-08  Rie     ESCAN00097770    No changes
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

#include "WrapNv.h"
#if defined( WRAPNV_USECASE_NVM )
# include "NvM.h"
#endif /* WRAPNV_USECASE_NVM */

/***********************************************************************************************************************
 *  GLOBAL DATA
 **********************************************************************************************************************/

# define WRAPNV_START_SEC_CONST_UNSPECIFIED
# include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */

CONST(uint8, WRAPNV_CONST) kWrapNvDefaultValueResetFlags[kEepSizeResetFlags] =
{
   0x00u, 0x00u, 0x00u, 0x00u, 0x00u
};

CONST(uint8, WRAPNV_CONST) kWrapNvDefaultValueValidityFlags[kEepSizeValidityFlags] =
{
   0xFFu
};

CONST(uint8, WRAPNV_CONST) kWrapNvDefaultValueFingerprintNumber[kEepSizeFingerprintNumber] =
{
   0xFFu, 0xFFu, 0xFFu
};

CONST(uint8, WRAPNV_CONST) kWrapNvDefaultValueVpmState[kEepSizeVpmState] =
{
   0x00u, 0x00u, 0x00u, 0x00u, 0x00u
};

CONST(uint8, WRAPNV_CONST) kWrapNvDefaultValueVpmValidityFlagsCopy[kEepSizeVpmValidityFlagsCopy] =
{
   0xFFu
};

CONST(uint8, WRAPNV_CONST) kWrapNvDefaultValueFingerprint[kEepSizeSwFingerprint] =
{
   0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u
};

CONST(uint8, WRAPNV_CONST) kWrapNvDefaultValueSigInfo[kEepSizeSigInfo] =
{
   0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u
};

CONST(uint8, WRAPNV_CONST) kWrapNvDefaultValueSigSegments[kEepSizeSigSegments] =
{
   0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u
};

/** Lookup table for each configured NV block */
CONST(tWrapNvBlockTbl, WRAPNV_CONST) kWrapNvBlockTbl[] =
{
   /* Reset flags */
   {
      WRAPNV_RECORDACCESS_STRUCTURED,
      WRAPNV_BLOCK_PREFIX(FblResetFlags),
      kEepSizeResetFlags,
      WRAPNV_DATASET_RESETFLAGS,
      kWrapNvDefaultValueResetFlags
   },

   /* Validity flags */
   {
      WRAPNV_RECORDACCESS_SINGLE,
      WRAPNV_BLOCK_PREFIX(FblValidityFlags),
      kEepSizeValidityFlags,
      WRAPNV_DATASET_VALIDITYFLAGS,
      kWrapNvDefaultValueValidityFlags
   },

   /* Number of fingerprints */
   {
      WRAPNV_RECORDACCESS_STRUCTURED,
      WRAPNV_BLOCK_PREFIX(FblFingerprintNumber),
      kEepSizeFingerprintNumber,
      WRAPNV_DATASET_NUMBEROFFINGERPRINTS,
      kWrapNvDefaultValueFingerprintNumber
   },

   /* Software fingerprint */
   {
      WRAPNV_RECORDACCESS_SINGLE,
      WRAPNV_BLOCK_PREFIX(FblSwFingerprint),
      kEepSizeSwFingerprint,
      WRAPNV_DATASET_SWFINGERPRINT,
      kWrapNvDefaultValueFingerprint
   },

   /* Data fingerprint */
   {
      WRAPNV_RECORDACCESS_SINGLE,
      WRAPNV_BLOCK_PREFIX(FblDataFingerprint),
      kEepSizeDataFingerprint,
      WRAPNV_DATASET_DATAFINGERPRINT,
      kWrapNvDefaultValueFingerprint
   },

   /* Boot fingerprint */
   {
      WRAPNV_RECORDACCESS_SINGLE,
      WRAPNV_BLOCK_PREFIX(FblBootFingerprint),
      kEepSizeBootFingerprint,
      WRAPNV_DATASET_BOOTFINGERPRINT,
      kWrapNvDefaultValueFingerprint
   },

   /* Signature information (for each logical block) */
   {
      WRAPNV_RECORDACCESS_STRUCTURED,
      WRAPNV_BLOCK_PREFIX(FblSigInfo),
      kEepSizeSigInfo,
      WRAPNV_DATASET_SIGINFO,
      kWrapNvDefaultValueSigInfo
   },

   /* Signature value (Meta data for each logical block) */
   {
      WRAPNV_RECORDACCESS_SINGLE,
      WRAPNV_BLOCK_PREFIX(FblSigValue),
      kEepSizeSigValue,
      WRAPNV_DATASET_SIGVALUE,
      (P2CONST(uint8, AUTOMATIC, WRAPNV_CONST)) NULL_PTR
   },

   /* Segment information (for each segment of each logical block) */
   {
      WRAPNV_RECORDACCESS_STRUCTURED,
      WRAPNV_BLOCK_PREFIX(FblSigSegments),
      kEepSizeSigSegments,
      WRAPNV_DATASET_SIGSEGMENTS,
      kWrapNvDefaultValueSigSegments
   },

   /* VPM operation states */
   {
      WRAPNV_RECORDACCESS_STRUCTURED,
      WRAPNV_BLOCK_PREFIX(FblVpmState),
      kEepSizeVpmState,
      WRAPNV_DATASET_VPMSTATES,
      kWrapNvDefaultValueVpmState
   },

   /* VPM request */
   {
      WRAPNV_RECORDACCESS_SINGLE,
      WRAPNV_BLOCK_PREFIX(FblVpmPatch),
      kEepSizeVpmRequest,
      WRAPNV_DATASET_VPMREQUEST,
      (P2CONST(uint8, AUTOMATIC, WRAPNV_CONST)) NULL_PTR
   },

   /* VPM copy of validity flags */
   {
      WRAPNV_RECORDACCESS_SINGLE,
      WRAPNV_BLOCK_PREFIX(FblVpmValidityFlagsCopy),
      kEepSizeVpmValidityFlagsCopy,
      WRAPNV_DATASET_VPMVALIDITYFLAGSCOPY,
      kWrapNvDefaultValueVpmValidityFlagsCopy
   }
};

/** Lookup table for each configured NV element */
CONST(tWrapNvRecordTbl, WRAPNV_CONST) kWrapNvRecordTbl[] =
{
   /* Reset flags */
   {  /* Reprogramming flag */
      0u,
      kEepSizeProgReqFlag,
      &kWrapNvBlockTbl[0u]
   },

   {  /* Reset response flag */
      1u,
      kEepSizeResetResponseFlag,
      &kWrapNvBlockTbl[0u]
   },

   {  /* Baudrate parameter */
      2u,
      kEepSizeBaudrateParam,
      &kWrapNvBlockTbl[0u]
   },

   {  /* Tester communication connection */
      3u,
      kEepSizeTesterConnection,
      &kWrapNvBlockTbl[0u]
   },

   {  /* Application updated flag */
      4u,
      kEepSizeProgMarker,
      &kWrapNvBlockTbl[0u]
   },

   /* Validity flags */
   {
      0u,
      kEepSizeValidityFlags,
      &kWrapNvBlockTbl[1u]
   },

   /* Number of fingerprints */
   {  /* Number of software fingerprints */
      0u,
      kEepSizeSwFingerprintNumber,
      &kWrapNvBlockTbl[2u]
   },

   {  /* Number of data fingerprints */
      1u,
      kEepSizeDataFingerprintNumber,
      &kWrapNvBlockTbl[2u]
   },

   {  /* Number of Boot fingerprints */
      2u,
      kEepSizeBootFingerprintNumber,
      &kWrapNvBlockTbl[2u]
   },

   /* Software fingerprint */
   {
      0u,
      kEepSizeSwFingerprint,
      &kWrapNvBlockTbl[3u]
   },

   /* Data fingerprint */
   {
      0u,
      kEepSizeDataFingerprint,
      &kWrapNvBlockTbl[4u]
   },

   /* Boot fingerprint */
   {
      0u,
      kEepSizeBootFingerprint,
      &kWrapNvBlockTbl[5u]
   },

   /* Signature information (for each logical block) */
   {  /* Signature type */
      0u,
      kEepSizeSigType,
      &kWrapNvBlockTbl[6u]
   },

   {  /* Signature start */
      1u,
      kEepSizeSigStart,
      &kWrapNvBlockTbl[6u]
   },

   {  /* Signature length */
      5u,
      kEepSizeSigLength,
      &kWrapNvBlockTbl[6u]
   },

   {  /* Signature segment number */
      9u,
      kEepSizeSigSegmentNumber,
      &kWrapNvBlockTbl[6u]
   },

   /* Signature value (Meta data for each logical block) */
   {
      0u,
      kEepSizeSigValue,
      &kWrapNvBlockTbl[7u]
   },

   /* Segment information (for each segment of each logical block) */
   {  /* Start address of segment */
      0u,
      kEepSizeSigSegmentStart,
      &kWrapNvBlockTbl[8u]
   },

   {  /* Length of segment */
      4u,
      kEepSizeSigSegmentLength,
      &kWrapNvBlockTbl[8u]
   },

   /* VPM operation states */
   {  /* Number of pending VPM patch requests */
      0u,
      kEepSizeVpmPatchCount,
      &kWrapNvBlockTbl[9u]
   },

   {  /* Index of the processed block */
      2u,
      kEepSizeVpmActive,
      &kWrapNvBlockTbl[9u]
   },

   {  /* Status of the currently processed block */
      3u,
      kEepSizeVpmProcessedBlock,
      &kWrapNvBlockTbl[9u]
   },

   {  /* Active status */
      4u,
      kEepSizeVpmBlockStatus,
      &kWrapNvBlockTbl[9u]
   },

   /* VPM request */
   {  /* Patch request */
      0u,
      kEepSizeVpmRequest,
      &kWrapNvBlockTbl[10u]
   },

   {  /* Patch address */
      0u,
      kEepSizeVpmPatchAddress,
      &kWrapNvBlockTbl[10u]
   },

   {  /* Patch length (limited by FEE record) */
      4u,
      kEepSizeVpmPatchLength,
      &kWrapNvBlockTbl[10u]
   },

   {  /* Patch data */
      6u,
      kEepSizeVpmPatchData,
      &kWrapNvBlockTbl[10u]
   },

   /* VPM copy of validity flags */
   {
      0u,
      kEepSizeVpmValidityFlagsCopy,
      &kWrapNvBlockTbl[11u]
   }
};

CONST(uint16, WRAPNV_CONST) kWrapNvNrOfBlock = sizeof(kWrapNvBlockTbl) / sizeof(kWrapNvBlockTbl[0]);
CONST(uint16, WRAPNV_CONST) kWrapNvNrOfRecord = sizeof(kWrapNvRecordTbl) / sizeof(kWrapNvRecordTbl[0]);

# define WRAPNV_STOP_SEC_CONST_UNSPECIFIED
# include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */

/***********************************************************************************************************************
 *  END OF FILE: WRAPNV_CFG.C
 **********************************************************************************************************************/
