/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2016 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Vpm.c
 *        \brief  Variant Parameter Manager - Client side
 *
 *      \details  Store WriteMemoryByAddress requests in a shadow buffer
 *                Buffer will be written to the actual memory locations before or at the next startup
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/

/***********************************************************************************************************************
 *  INCLUDES
 **********************************************************************************************************************/

#include "Std_Types.h"
#include "Vpm.h"

# include "Vpm_Cfg.h"
# include "WrapNv_inc.h"

/***********************************************************************************************************************
 *   VERSION
 **********************************************************************************************************************/

#if ( IF_VXASRVPM_VOLVOAB_VERSION != 0x0100 ) || \
    ( IF_VXASRVPM_VOLVOAB_RELEASE_VERSION != 0x00 )
# error "Error in VPM.C: Source and header file are inconsistent!"
#endif

/***********************************************************************************************************************
 *  DEFINES
 **********************************************************************************************************************/

/** Size of serialized patch address */
# define VPM_PATCH_SIZE_ADDRESS       4u
/** Size of serialized patch length */
# define VPM_PATCH_SIZE_LENGTH        2u
/** Total size of serialized meta data */
# define VPM_PATCH_SIZE_METADATA      (VPM_PATCH_SIZE_ADDRESS + VPM_PATCH_SIZE_LENGTH)
/** Size of serialized patch count */
# define VPM_PATCH_SIZE_COUNT         2u

/** Offset of patch address in serialized buffer */
# define VPM_PATCH_OFFSET_ADDRESS     0u
/** Offset of patch length in serialized buffer */
# define VPM_PATCH_OFFSET_LENGTH      (VPM_PATCH_OFFSET_ADDRESS + VPM_PATCH_SIZE_ADDRESS)
/** Offset of patch data in serialized buffer */
# define VPM_PATCH_OFFSET_DATA        (VPM_PATCH_OFFSET_LENGTH + VPM_PATCH_SIZE_LENGTH)

/** Maximum allowed length of a single patch (larger requests are split into multiple patches) */
# define VPM_MAX_PATCH_LENGTH         (VPM_NV_BLOCK_SIZE - VPM_PATCH_SIZE_METADATA)

/***********************************************************************************************************************
 *  TYPEDEFS
 **********************************************************************************************************************/

/** State of VPM operation */
typedef enum
{
  VPM_STATE_IDLE,       /**< Idle */
  VPM_STATE_PATCH,      /**< Write patch data */
  VPM_STATE_COUNT,      /**< Update patch count */
  VPM_STATE_INVALIDATE  /**< Invalidate patches of current (failed) request */
} VpmStateType;

/***********************************************************************************************************************
 *  GLOBAL DATA
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *  LOCAL DATA
 **********************************************************************************************************************/

#define VPM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h"

/* Currently processed VPM request */
/** Patch start address */
static VAR(uint32, VPM_VAR_NOINIT)                  vpmAddress;
/** Patch size */
static VAR(uint32, VPM_VAR_NOINIT)                  vpmDataSize;
/** Pointer to patch data */
static P2VAR(uint8, VPM_VAR_NOINIT, VPM_APPL_DATA)  vpmDataPtr;

/* Temporary storage of serialized VPM patch */
/** Serialization buffer for single VPM patch */
static VAR(uint8, VPM_VAR_NOINIT)                   vpmPatchBuffer[VPM_NV_BLOCK_SIZE];
/** Length of serialized patch */
static VAR(uint32, VPM_VAR_NOINIT)                  vpmPatchLength;

/** Index of first patch stored for current VPM request */
static VAR(uint16, VPM_VAR_NOINIT)                  vpmFirstIndex;
/** Index used for current/next patch */
static VAR(uint16, VPM_VAR_NOINIT)                  vpmCurrentIndex;
/** State of VPM operation */
static VAR(VpmStateType, VPM_VAR_NOINIT)            vpmState;

#define VPM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h"

/***********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 **********************************************************************************************************************/

#define VPM_START_SEC_CODE
#include "MemMap.h"

static FUNC(void, VPM_CODE) Vpm_SerializeInteger( uint32 count, uint32 input, P2VAR(uint8, AUTOMATIC, VPM_APPL_DATA) output );
static FUNC(uint32, VPM_CODE) Vpm_DeserializeInteger( uint32 count, P2CONST(uint8, AUTOMATIC, VPM_APPL_DATA) input );
static FUNC(VpmResultType, VPM_CODE) Vpm_WriteData(void);

/***********************************************************************************************************************
 *   LOCAL FUNCTIONS
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *  Vpm_SerializeInteger
 **********************************************************************************************************************/
/*! \brief      Convert given integer value to big-endian byte array
 *  \param[in]  count Number of relevant bytes
 *  \param[in]  input Input value
 *  \param[out] output Pointer to output buffer
 **********************************************************************************************************************/
static FUNC(void, VPM_CODE) Vpm_SerializeInteger( uint32 count, uint32 input, P2VAR(uint8, AUTOMATIC, VPM_APPL_DATA) output )
{
   /* Loop relevant bytes */
   while (count > 0u)
   {
      count--;
      /* Store most significant byte first */
      output[count] = (uint8)(input & 0xFFu);
      /* Shift in next byte */
      input >>= 8u;
   }
}

/***********************************************************************************************************************
 *  Vpm_SerializeInteger
 **********************************************************************************************************************/
/*! \brief      Convert given big-endian byte array to integer value
 *  \param[in]  count Number of relevant bytes
 *  \param[in]  input Pointer to input buffer
 *  \return     Integer value
 **********************************************************************************************************************/
static FUNC(uint32, VPM_CODE) Vpm_DeserializeInteger( uint32 count, P2CONST(uint8, AUTOMATIC, VPM_APPL_DATA) input )
{
   uint32      output;
   uint8_least idx;

   output = 0u;
   idx    = 0u;

   /* Loop relevant bytes */
   while (count > 0u)
   {
      /* Most significant byte first */
      output <<= 8u;
      /* Add current byte */
      output |= (uint32)input[idx];

      idx++;
      count--;
   }

   return output;
}

/***********************************************************************************************************************
 *  Vpm_WriteData
 **********************************************************************************************************************/
/*! \brief       Write data to non-volatile memory (shadow buffer)
 *  \details     If parameter data is larger than max patch size it will be split into several entries
 *               In case the writing of one entry fails, the previous entries will be invalidated.
 *  \pre         Vpm_Init executed before
 *  \return      Status of write request
 *               kVpmOk - Operation finished successfully
 *               kVpmFailed - Writing to shadow buffer failed
 *               kVpmBusy - Operation in progress
 **********************************************************************************************************************/
static FUNC(VpmResultType, VPM_CODE) Vpm_WriteData( void )
{
  VpmResultType   rvalStatus;
  VpmResultType   loop;

  uint32          index;
  uint8           patchCount[VPM_PATCH_SIZE_COUNT];
  tWrapNvOpStatus nvOpStatus;

  P2VAR(uint8, AUTOMATIC, VPM_APPL_DATA) dataPtr;

  rvalStatus  = kVpmFailed;

  /* Initially enter state machine loop*/
  loop        = kVpmOk;
  /* Assume NV-operation from last cycle is pending */
  nvOpStatus  = WRAPNV_OPSTATUS_PENDING;

  /* Allow looping of state machine
     Removes delay because of additional task cycles when NV-operation is finished */
  while (kVpmOk == loop)
  {
    /* Leave loop per default */
    loop = kVpmFailed;

    /* Handle operation state */
    switch (vpmState)
    {
      /* Currently idle: setup first/next patch */
      case VPM_STATE_IDLE:
      {
        /* Check if something has to be done */
        if (0u == vpmDataSize)
        {
          /* No more data to be written */
          rvalStatus = kVpmOk;
        }
        /* Verify patch won't exceed available non-volatile memory */
        else if (vpmCurrentIndex >= VPM_PATCH_COUNT)
        {
          /* Immediatelly begin invalidation of already written patches */
          vpmState    = VPM_STATE_INVALIDATE;
          nvOpStatus  = WRAPNV_OPSTATUS_INIT;
          loop        = kVpmOk;
        }
        else
        {
          /* First call to write operation in this cycle
              Setup request buffer */

          /* Set patch length */
          vpmPatchLength = vpmDataSize;

          if (vpmPatchLength > VPM_MAX_PATCH_LENGTH)
          {
            /* Request does not fit into a single patch, split it */
            vpmPatchLength = VPM_MAX_PATCH_LENGTH;
          }

          /* Serialize patch meta-data */
          Vpm_SerializeInteger(VPM_PATCH_SIZE_ADDRESS, vpmAddress, &vpmPatchBuffer[VPM_PATCH_OFFSET_ADDRESS]);
          Vpm_SerializeInteger(VPM_PATCH_SIZE_LENGTH, vpmPatchLength, &vpmPatchBuffer[VPM_PATCH_OFFSET_LENGTH]);

          /* Add patch data */
          dataPtr = &vpmPatchBuffer[VPM_PATCH_OFFSET_DATA];
          for (index = 0u; index < vpmPatchLength; index++)
          {
            dataPtr[index] = vpmDataPtr[index];
          }

          /* Immediatelly begin writing of patch */
          vpmState    = VPM_STATE_PATCH;
          nvOpStatus  = WRAPNV_OPSTATUS_INIT;
          loop        = kVpmOk;
        }

        break;
      }
      /* Write patch record */
      case VPM_STATE_PATCH:
      {
        /* Start writing or evaluate current status */
        switch (ApplFblNvWriteAsyncVpmPatch(vpmCurrentIndex, vpmPatchBuffer, nvOpStatus))
        {
          /* NV-operation successfully finished */
          case WRAPNV_E_OK:
          {
            /* Update request information */
            vpmAddress  += vpmPatchLength;
            vpmDataSize -= vpmPatchLength;
            vpmDataPtr  = &vpmDataPtr[vpmPatchLength];

            vpmCurrentIndex++;

            /* Request not completely processed yet? */
            if (vpmDataSize > 0u)
            {
              /* Immediatelly trigger next write operation */
              vpmState = VPM_STATE_IDLE;
            }
            else
            {
              /* Immediatelly update patch count */
              vpmState = VPM_STATE_COUNT;
            }

            /* Setup initial NV-operation and immediatelly trigger it */
            nvOpStatus  = WRAPNV_OPSTATUS_INIT;
            loop        = kVpmOk;

            break;
          }
          /* NV-operation pending */
          case WRAPNV_E_PENDING:
          {
            /* Continue on next cycle */
            rvalStatus = kVpmBusy;

            break;
          }
          /* NV-operation failed */
          case WRAPNV_E_NOT_OK:
          default:
          {
            /* Immediatelly begin invalidation of already written patches */
            vpmState    = VPM_STATE_INVALIDATE;
            nvOpStatus  = WRAPNV_OPSTATUS_INIT;
            loop        = kVpmOk;

            break;
          }
        }

        break;
      }
      /* Update patch count */
      case VPM_STATE_COUNT:
      {
        /* Serialize patch count */
        Vpm_SerializeInteger(VPM_PATCH_SIZE_COUNT, vpmCurrentIndex, patchCount);

        /* Start writing or evaluate current status */
        switch (ApplFblNvWriteAsyncVpmPatchCount(0u, patchCount, nvOpStatus))
        {
          /* NV-operation successfully finished */
          case WRAPNV_E_OK:
          {
            /* VPM request successfully stored */
            vpmState    = VPM_STATE_IDLE;
            rvalStatus  = kVpmOk;

            break;
          }
          /* NV-operation pending */
          case WRAPNV_E_PENDING:
          {
            /* Continue on next cycle */
            rvalStatus = kVpmBusy;

            break;
          }
          /* NV-operation failed */
          case WRAPNV_E_NOT_OK:
          default:
          {
            /* Immediatelly begin invalidation of already written patches */
            vpmState    = VPM_STATE_INVALIDATE;
            nvOpStatus  = WRAPNV_OPSTATUS_INIT;
            loop        = kVpmOk;

            break;
          }
        }

        break;
      }
      /* Invalidate patches of current (failed) request */
      case VPM_STATE_INVALIDATE:
      {
        /* Start erasing or evaluate current status */
        switch (ApplFblNvDeleteAsyncVpmPatch(vpmCurrentIndex, nvOpStatus))
        {
          /* NV-operation successfully finished */
          case WRAPNV_E_OK:
          {
            /* Initial patch of current request reached? */
            if (vpmCurrentIndex == vpmFirstIndex)
            {
              /* All patches invalidated */
              vpmState = VPM_STATE_IDLE;
            }
            else
            {
              /* Immediatelly begin invalidation of next patch */
              vpmCurrentIndex--;
              nvOpStatus  = WRAPNV_OPSTATUS_INIT;
              loop        = kVpmOk;
            }

            break;
          }
          /* NV-operation pending */
          case WRAPNV_E_PENDING:
          {
            /* Continue on next cycle */
            rvalStatus = kVpmBusy;

            break;
          }
          /* NV-operation failed */
          case WRAPNV_E_NOT_OK:
          default:
          {
            /* Reset index for next patch and abort */
            vpmCurrentIndex = vpmFirstIndex;
            vpmState        = VPM_STATE_IDLE;

            break;
          }
        }

        break;
      }
    }
  }

  return rvalStatus;
}

/***********************************************************************************************************************
 *   GLOBAL FUNCTIONS
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *  Vpm_Init
 **********************************************************************************************************************/
/*! \brief       Initialize VPM operation
 **********************************************************************************************************************/
FUNC(void, VPM_CODE) Vpm_Init( void )
{
  uint8 patchCount[VPM_PATCH_SIZE_COUNT];

  /* Setup state */
  vpmDataSize = 0u;
  vpmState    = VPM_STATE_IDLE;

  /* Get current number of stored patches (synchronous operation) */
  if (WRAPNV_E_OK == ApplFblNvReadVpmPatchCount(0u, patchCount))
  {
    /* Deserialize read data */
    vpmCurrentIndex = (uint16)Vpm_DeserializeInteger(VPM_PATCH_SIZE_COUNT, patchCount);

    /* Check for unitialized value, indicating entry doesn't exist yet */
    if (VPM_PATCH_COUNT_UNINIT == vpmCurrentIndex)
    {
       /* Assume no patches present */
       vpmCurrentIndex = 0u;
    }
  }
  else
  {
    /* Assume no patches present */
    vpmCurrentIndex = 0u;
  }
}

/***********************************************************************************************************************
 *  Vpm_GetStatus
 **********************************************************************************************************************/
/*! \brief       Get status of pending operation and continue writing
 *  \pre         Vpm_DcmWriteData executed before
 *  \return      Status of active write request
 *               kVpmOk - No open request or operation finished successfully
 *               kVpmFailed - Writing to shadow buffer failed
 *               kVpmBusy - Operation in progress, conclude by repeatedly
 *               calling function until other result is returned
 **********************************************************************************************************************/
FUNC(VpmResultType, VPM_CODE) Vpm_GetStatus( void )
{
  return Vpm_WriteData();
}

/***********************************************************************************************************************
 *  Vpm_DcmWriteData
 **********************************************************************************************************************/
/*! \brief       Store WriteMemoryByAddress request in shadow buffer
 *  \pre         Vpm_Init executed before
 *  \param[in]   address - Memory address of parameter
 *  \param[in]   length - Length of data
 *  \param[in]   data - Data buffer
 *  \return      Status of write request
 *               kVpmOk - Operation finished successfully
 *               kVpmFailed - Writing to shadow buffer failed
 *               kVpmBusy - Previous operation still in progress.
 *                Call Vpm_GetStatus until exit status other than kVpmBusy is returned. Retry Vpm_DcmWriteData afterwards
 **********************************************************************************************************************/
FUNC(VpmResultType, VPM_CODE) Vpm_DcmWriteData( uint32 address, uint32 length, P2VAR(uint8, AUTOMATIC, VPM_APPL_DATA) data )
{
  VpmResultType rvalStatus;

  /* Initialize failed result */
  rvalStatus = kVpmFailed;

  /* Check if a job is pending */
  if (VPM_STATE_IDLE != vpmState)
  {
    rvalStatus = kVpmBusy;
  }
  else
  {
    /* Only accept a write request when enough unused patch entries are available in non-volatile memory */
    if ((((length - 1u) / VPM_MAX_PATCH_LENGTH) + 1u) <= (VPM_PATCH_COUNT - vpmCurrentIndex))
    {
      /* Remember index of first patch */
      vpmFirstIndex = vpmCurrentIndex;

      /* Store request information */
      vpmDataSize = length;
      vpmDataPtr  = data;
      vpmAddress  = address;

      /* Initiate storage of request in non-volatile memory */
      rvalStatus = Vpm_WriteData();
    }
  }

  return rvalStatus;
}

#define VPM_STOP_SEC_CODE
#include "MemMap.h"

/***********************************************************************************************************************
 *  END OF FILE: VPM.C
 **********************************************************************************************************************/

