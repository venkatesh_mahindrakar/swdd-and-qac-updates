/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2016 by Vector Informatik GmbH.                                              All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Vpm.h
 *        \brief  Variant Parameter Manager - Client side
 *
 *      \details  Store WriteMemoryByAddress requests in a shadow buffer
 *                Buffer will be written to the actual memory locations before or at the next startup
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  -------------------------------------------------------------------------------------------------------------------
 *  Joern Herwig                  visjhg        Vector Informatik GmbH
 *  -------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  01.00.00  2016-03-03  visjhg  ESCAN00087567 Initial version based on If_VxFblVpm_Volvo_Ab 1.03.00
 *                                              Use Fee for data exchange in non-volatile memory
 *                                              Store explicit patch count
 *********************************************************************************************************************/

#ifndef __VPM_H__
#define __VPM_H__

/***********************************************************************************************************************
 *   VERSION
 **********************************************************************************************************************/

/* ##V_CFG_MANAGEMENT ##CQProject : If_VxAsrVpm_VolvoAb CQComponent : Implementation */
#define IF_VXASRVPM_VOLVOAB_VERSION           0x0100u
#define IF_VXASRVPM_VOLVOAB_RELEASE_VERSION   0x00u

/***********************************************************************************************************************
 *  DEFINES
 **********************************************************************************************************************/

/* Return codes */
#define kVpmOk          0u  /**< Operation finished successfully */
#define kVpmFailed      1u  /**< Operation failed */
#define kVpmBusy        2u  /**< Operation in progress */

/***********************************************************************************************************************
 *  TYPEDEFS
 **********************************************************************************************************************/

/** Result type */
typedef uint8 VpmResultType;

/***********************************************************************************************************************
 *  PROTOTYPES
 **********************************************************************************************************************/

#define VPM_START_SEC_CODE
#include "MemMap.h"

extern FUNC(void, VPM_CODE) Vpm_Init( void );
extern FUNC(VpmResultType, VPM_CODE) Vpm_DcmWriteData(uint32 address, uint32 length, P2VAR(uint8, AUTOMATIC, VPM_APPL_DATA) data);
extern FUNC(VpmResultType, VPM_CODE) Vpm_GetStatus( void );

#define VPM_STOP_SEC_CODE
#include "MemMap.h"

#endif /* __VPM_H__ */

/***********************************************************************************************************************
 *  END OF FILE: VPM.H
 **********************************************************************************************************************/

