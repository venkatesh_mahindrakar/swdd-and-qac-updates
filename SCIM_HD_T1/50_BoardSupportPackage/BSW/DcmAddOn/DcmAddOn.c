/* ********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                             All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  DcmAddOn.c
 *      Project:  MICROSAR Complex Device Driver (CDD)
 *       Module:  -
 *    Generator:  -
 *
 *  Description:  Implementation of the complex device driver DcmAddOn. Extends the Dcm with additional functionality.
 *********************************************************************************************************************/

#define DCMADDON_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "DcmAddOn.h"
#include "Vpm.h"
#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif
#include "Rte_Dcm.h"
#include "WrapNv_inc.h"

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/
/* Check the version of Msn header file */
#if (  (DCMADDON_SW_MAJOR_VERSION != (0x04U)) \
    || (DCMADDON_SW_MINOR_VERSION != (0x01U)) \
    || (DCMADDON_SW_PATCH_VERSION != (0x01U)) )
# error "Vendor specific version numbers of DcmAddOn.c and DcmAddOn.h are inconsistent"
#endif

/* Check the version of the configuration header file */
#if (  (DCMADDON_CFG_MAJOR_VERSION != (0x04U)) \
    || (DCMADDON_CFG_MINOR_VERSION != (0x01U)) )
# error "Version numbers of DcmAddOn.c and DcmAddOn_Cfg.h are inconsistent!"
#endif

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 **********************************************************************************************************************/
#if(DCMADDON_DEV_ERROR_REPORT == STD_ON) && \
   (DCMADDON_DEV_ERROR_DETECT == STD_OFF)
# error "Invalid configuration! Development error reporting requires development error detection!"
#endif

/* Restrictions for the size of the fingerprint numbers */
#if(kEepSizeSwFingerprintNumber == kEepSizeDataFingerprintNumber)
# if defined (kEepSizeBootFingerprintNumber)
#  if(kEepSizeSwFingerprintNumber == kEepSizeBootFingerprintNumber)
#  else
#   error "All fingerprint numbers must have the same size!"
#  endif
# endif
# define DCMADDON_SIZE_FINGERPRINT_NUMBER                            kEepSizeSwFingerprintNumber
#else
# error "All fingerprint numbers must have the same size!"
#endif

#if(DCMADDON_SIZE_FINGERPRINT_NUMBER != 1)
# error "DcmAddOn currently only supports one-byte fingerprint numbers!"
#endif

/* Restrictions for the size of the fingerprints */
#if(kEepSizeSwFingerprint == kEepSizeDataFingerprint)
# if defined (kEepSizeBootFingerprint)
#  if(kEepSizeSwFingerprint == kEepSizeBootFingerprint)
#  else
#   error "All fingerprints must have the same size!"
#  endif
# endif
# define DCMADDON_SIZE_FINGERPRINT                                   kEepSizeSwFingerprint
#else
# error "All fingerprint must have the same size!"
#endif

#if defined (kEepSizeBootFingerprintNumber) && \
    defined (kEepSizeBootFingerprint)
# define DCMADDON_SUPPORT_BOOT_SW_FP                                 STD_ON
#else
# define DCMADDON_SUPPORT_BOOT_SW_FP                                 STD_OFF
#endif

#if (DCMADDON_SUPPORT_FINGERPRINTPROTECTION == STD_ON)
# if (DCM_STATE_SECURITY_NOTIFICATION_ENABLED == STD_OFF)
#  error "The notification function for security level changes has to be configured in the DCM!"
# endif
#endif

#define DCMADDON_SETPROGCOND_PROGRESS_DECIDE_CONTEXT                 ((DcmAddOn_SetProgCondProgressType)0)
#define DCMADDON_SETPROGCOND_PROGRESS_PROG_REQ_FLAG                  ((DcmAddOn_SetProgCondProgressType)1)
#define DCMADDON_SETPROGCOND_PROGRESS_TESTER_SRC_ADDR                ((DcmAddOn_SetProgCondProgressType)2)
#define DCMADDON_SETPROGCOND_PROGRESS_RST_RESP_FLAG                  ((DcmAddOn_SetProgCondProgressType)3)
#define DCMADDON_SETPROGCOND_PROGRESS_APPL_UPTD_FLAG                 ((DcmAddOn_SetProgCondProgressType)4)

#define DCMADDON_RD_FINGERPRINT_NUM                                  ((DcmAddOn_ReadStateType)0)
#define DCMADDON_RD_FINGERPRINT_DATA                                 ((DcmAddOn_ReadStateType)1)

#define DCMADDON_WR_FINGERPRINT_IDLE                                 ((DcmAddOn_WriteStateType)0)
#define DCMADDON_WR_FINGERPRINT_NUM_RD                               ((DcmAddOn_WriteStateType)1)
#define DCMADDON_WR_FINGERPRINT_DATA                                 ((DcmAddOn_WriteStateType)2)
#define DCMADDON_WR_FINGERPRINT_NUM_CALC                             ((DcmAddOn_WriteStateType)3)
#define DCMADDON_WR_FINGERPRINT_NUM_WR                               ((DcmAddOn_WriteStateType)4)

#define DCMADDON_WR_PROTECT_PASS_THROUGH                             ((DcmAddOn_WriteProtectStateType)0)
#define DCMADDON_WR_PROTECT_CHECK_SERVICES                           ((DcmAddOn_WriteProtectStateType)1)

#define DCMADDON_SVC_SESSIONCNTRL                                    ((DcmAddOn_ServiceIdType)0x10U)
#define DCMADDON_SVC_ECURESET                                        ((DcmAddOn_ServiceIdType)0x11U)
#define DCMADDON_SVC_WRITEDATABYID                                   ((DcmAddOn_ServiceIdType)0x2EU)
#define DCMADDON_SVC_WRITEDATABYADDR                                 ((DcmAddOn_ServiceIdType)0x3DU)
#define DCMADDON_SVC_LINKCTRL                                        ((DcmAddOn_ServiceIdType)0x87U)

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 **********************************************************************************************************************/
#define EVER                                                         (;;)                                                                             /* PRQA S 3412 */ /* MD_MSR_19.4 */
/* Mapping of error reporting macro */
#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
# define DcmAddOn_CallDetReportError(apiId, errId)                   ((void)Det_ReportError((uint16)(DCMADDON_MODULE_ID), (uint8)0, (uint8)(apiId), (uint8)(errId))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#else
# define DcmAddOn_CallDetReportError(apiId, errId)                   /* Not used */
#endif

#if(DCMADDON_DEBUG_ERROR_DETECT == STD_ON)
# define DcmAddOn_AssertAlways(apiId, errId)                         { DcmAddOn_CallDetReportError(apiId, errId); }                                   /* PRQA S 3458 */ /* MD_MSR_19.4 */
# define DcmAddOn_Assert(cond, apiId, errId)                         if(!(cond)) DcmAddOn_AssertAlways((apiId),(errId))                               /* PRQA S 3412 */ /* MD_MSR_19.4 */
#else
# define DcmAddOn_AssertAlways(apiId, errId)                         /* Not used */
# define DcmAddOn_Assert(cond, apiId, errId)                         /* Not used */
#endif

#define DcmAddOn_RepeaterResetOpStatus()                             (dcmAddOn_RepeaterContext.opStatus = DCM_INITIAL)

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 **********************************************************************************************************************/
typedef uint8 DcmAddOn_ServiceIdType;

typedef uint8 DcmAddOn_SetProgCondProgressType;
typedef uint8 DcmAddOn_ReadStateType;
typedef uint8 DcmAddOn_WriteStateType;
typedef uint8 DcmAddOn_WriteProtectStateType;

typedef uint8 DcmAddOn_ProgReqFlagValueType;
typedef uint8 DcmAddOn_TesterSrcAddrValueType;

typedef P2FUNC(Std_ReturnType, DCMADDON_CODE, DcmAddOn_RepeaterProxyFuncType) (Dcm_OpStatusType, Dcm_MsgContextPtrType);

typedef struct DCMADDON_SETPROGCONDCONTEXT_TAG
{
  Dcm_ProgConditionsType              progConditions;
  DcmAddOn_SetProgCondProgressType    progress;
  Dcm_OpStatusType                    opStatus;
}DcmAddOn_SetProgCondContext;

typedef struct DCMADDON_GETPROGCONDCONTEXT_TAG
{
  Dcm_EcuStartModeType                ecuStartMode;
  uint8                               rstRespFlagVal;
  uint8                               progMarkerValue;
  DcmAddOn_TesterSrcAddrValueType     testerSourceAddr;
}DcmAddOn_GetProgCondContext;

typedef struct DCMADDON_REPEATERCONTEXTTYPE_TAG
{
  DcmAddOn_RepeaterProxyFuncType      callee;
  Dcm_OpStatusType                    opStatus;
}DcmAddOn_RepeaterContextType;

typedef struct DCMADDON_READFINGERPRINTCONTEXTTYPE_TAG
{
  uint8                               index;
  uint8                               count;
  Dcm_OpStatusType                    opStatus;
  DcmAddOn_ReadStateType              readState;
}DcmAddOn_ReadFingerprintContext;

typedef struct DCMADDON_READSINGLEFPCONTEXTTYPE_TAG
{
  uint16                              counter;
  Dcm_OpStatusType                    opStatus;
  DcmAddOn_ReadStateType              readState;
}DcmAddOn_ReadSingleFpContext;

typedef struct DCMADDON_WRITEFINGERPRINTCONTEXTTYPE_TAG
{
  DcmAddOn_FingerprintType            fpType;
  uint8                               fpNumber;
  Dcm_OpStatusType                    opStatus;
  DcmAddOn_WriteStateType             writeState;
  DcmAddOn_WriteProtectStateType      writeProtectState;
}DcmAddOn_WriteFingerprintContext;

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 **********************************************************************************************************************/
#define DCMADDON_START_SEC_VAR_INIT_UNSPECIFIED
#include "MemMap.h"                                                                                                                                   /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#if(DCMADDON_DEV_ERROR_DETECT == STD_ON)
static VAR(boolean, DCMADDON_VAR_INIT)                               dcmAddOn_Initialized = FALSE;
#endif

#define DCMADDON_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "MemMap.h"                                                                                                                                   /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

#define DCMADDON_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h"                                                                                                                                   /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

static VAR(DcmAddOn_SetProgCondContext, DCMADDON_VAR_NOINIT)         dcmAddOn_SetProgCondContext;
static VAR(DcmAddOn_GetProgCondContext, DCMADDON_VAR_NOINIT)         dcmAddOn_GetProgCondContext;
static VAR(DcmAddOn_RepeaterContextType, DCMADDON_VAR_NOINIT)        dcmAddOn_RepeaterContext;
static VAR(DcmAddOn_ReadFingerprintContext, DCMADDON_VAR_NOINIT)     dcmAddOn_ReadFingerprintContext;                                                 /* PRQA S 3218 */ /* MD_DcmAddOn_CodingRule_3218 */
static VAR(DcmAddOn_ReadSingleFpContext, DCMADDON_VAR_NOINIT)        dcmAddOn_ReadSingleFpContext;                                                    /* PRQA S 3218 */ /* MD_DcmAddOn_CodingRule_3218 */
static VAR(DcmAddOn_WriteFingerprintContext, DCMADDON_VAR_NOINIT)    dcmAddOn_WriteFingerprintContext;

#define DCMADDON_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h"                                                                                                                                   /* PRQA S 5087 1 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *  GLOBAL DATA
 **********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 **********************************************************************************************************************/
#define DCMADDON_START_SEC_CODE
#include "MemMap.h"                                                                                                                                   /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Functions of the repeater mechanism */
/**********************************************************************************************************************
 *  DcmAddOn_RepeaterExecute()
 *********************************************************************************************************************/
/*! \brief          Starts polling a repeater proxy.
 *  \details        Allows to split the service processing into different/multiple repeater proxies.
 *  \param[in]      OpStatus       The operation status
 *  \param[in,out]  pMsgContext    The current request context
 *  \return         Operation result
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_RepeaterExecute(Dcm_OpStatusType OpStatus,
                                                                    Dcm_MsgContextPtrType pMsgContext);

/**********************************************************************************************************************
 *  DcmAddOn_RepeaterSetCallee()
 *********************************************************************************************************************/
/*! \brief          Starts polling a repeater proxy.
 *  \details        -
 *  \param[in]      pProxy    The proxy callee
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(void, DCMADDON_CODE) DcmAddOn_RepeaterSetCallee(DcmAddOn_RepeaterProxyFuncType pProxy);

/* Repeater proxy functions for service 0x11 */
/**********************************************************************************************************************
 *  DcmAddOn_Svc11_RepProxy_SetProgConditions()
 *********************************************************************************************************************/
/*! \brief          Repeater proxy for setting the programming conditions for KeyOffOnReset.
 *  \details        -
 *  \param[in]      OpStatus       The operation status
 *  \param[in,out]  pMsgContext    The current request context
 *  \return         Operation result
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Svc11_RepProxy_SetProgConditions(Dcm_OpStatusType OpStatus,
                                                                                     Dcm_MsgContextPtrType pMsgContext);

/**********************************************************************************************************************
 *  DcmAddOn_Svc11_RepProxy_TriggerReset()
 *********************************************************************************************************************/
/*! \brief          Repeater proxy for service 0x11 initiate reset and set next repeater proxy.
 *  \details        -
 *  \param[in]      OpStatus       The operation status
 *  \param[in,out]  pMsgContext    The current request context
 *  \return         Operation result
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Svc11_RepProxy_TriggerReset(Dcm_OpStatusType OpStatus,
                                                                                Dcm_MsgContextPtrType pMsgContext);

/**********************************************************************************************************************
 *  DcmAddOn_Svc11_RepProxy_WaitForResetAck()
 *********************************************************************************************************************/
/*! \brief          Repeater proxy for triggering a SwitchAck.
 *  \details        -
 *  \param[in]      OpStatus       The operation status
 *  \param[in,out]  pMsgContext    The current request context
 *  \return         Operation result
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Svc11_RepProxy_WaitForResetAck(Dcm_OpStatusType OpStatus,
                                                                             Dcm_MsgContextPtrType pMsgContext);

/* Repeater proxy functions for service 0x87 */
/**********************************************************************************************************************
 *  DcmAddOn_Svc87_RepProxy_RequestEvaluation()
 *********************************************************************************************************************/
/*! \brief          Repeater proxy for the request evaluation of service 0x87.
 *  \details        -
 *  \param[in]      OpStatus       The operation status
 *  \param[in,out]  pMsgContext    The current request context
 *  \return         Operation result
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Svc87_RepProxy_RequestEvaluation(Dcm_OpStatusType OpStatus,
                                                                                     Dcm_MsgContextPtrType pMsgContext);

/**********************************************************************************************************************
 *  DcmAddOn_Svc87_RepProxy_WriteBaudRate()
 *********************************************************************************************************************/
/*! \brief          Repeater proxy for writing the baud rate to non-volatile memory.
 *  \details        -
 *  \param[in]      OpStatus       The operation status
 *  \param[in,out]  pMsgContext    The current request context
 *  \return         Operation result
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Svc87_RepProxy_WriteBaudRate(Dcm_OpStatusType OpStatus,
                                                                                 Dcm_MsgContextPtrType pMsgContext);

/**********************************************************************************************************************
 *  DcmAddOn_Svc87_RepProxy_SetProgConditions()
 *********************************************************************************************************************/
/*! \brief          Repeater proxy for setting the programming conditions for service LinkControl.
 *  \details        -
 *  \param[in]      OpStatus       The operation status
 *  \param[in,out]  pMsgContext    The current request context
 *  \return         Operation result
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Svc87_RepProxy_SetProgConditions(Dcm_OpStatusType OpStatus,
                                                                                     Dcm_MsgContextPtrType pMsgContext);

/**********************************************************************************************************************
 *  DcmAddOn_Svc87_RepProxy_TriggerReset()
 *********************************************************************************************************************/
/*! \brief          Repeater proxy for service 0x87 initiate reset and set next repeater proxy.
 *  \details        -
 *  \param[in]      OpStatus       The operation status
 *  \param[in,out]  pMsgContext    The current request context
 *  \return         Operation result
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Svc87_RepProxy_TriggerReset(Dcm_OpStatusType OpStatus,
                                                                                Dcm_MsgContextPtrType pMsgContext);

/**********************************************************************************************************************
 *  DcmAddOn_Svc87_RepProxy_WaitForResetAck()
 *********************************************************************************************************************/
/*! \brief          Repeater proxy for triggering SwitchAck and SwitchEcuReset.
 *  \details        -
 *  \param[in]      OpStatus       The operation status
 *  \param[in,out]  pMsgContext    The current request context
 *  \return         Operation result
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Svc87_RepProxy_WaitForResetAck(Dcm_OpStatusType OpStatus,
                                                                             Dcm_MsgContextPtrType pMsgContext);

/* Service unspecific Repeater proxy functions */
/* Fingerprint functions */
/**********************************************************************************************************************
 *  DcmAddOn_ReadNumOfWrittenFingerprints()
 *********************************************************************************************************************/
/*! \brief          Function to read the number of fingerprints written for a specific fingerprint type.
 *  \details        -
 *  \param[in]      fpType         The fingerprint type
 *  \param[in]      OpStatus       The operation status
 *  \param[out]     result         The number of written fingerprints
 *  \return         Operation result
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_ReadNumOfWrittenFingerprints(DcmAddOn_FingerprintType fpType,
                                                                                 Dcm_OpStatusType OpStatus,
                                                                                 P2VAR(uint8, AUTOMATIC, DCMADDON_APPL_DATA) result);

/**********************************************************************************************************************
 *  DcmAddOn_ReadNumOfFingerprintsInBuffer()
 *********************************************************************************************************************/
/*! \brief          Function to read the number of fingerprints currently in the buffer.
 *  \details        -
 *  \param[in]      fpType         The fingerprint type
 *  \param[in]      OpStatus       The operation status
 *  \param[out]     result         The number of fingerprints in buffer
 *  \return         Operation result
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_ReadNumOfFingerprintsInBuffer(DcmAddOn_FingerprintType fpType,
                                                                                  Dcm_OpStatusType OpStatus,
                                                                                  P2VAR(uint8, AUTOMATIC, DCMADDON_APPL_DATA) result);

/**********************************************************************************************************************
 *  DcmAddOn_ReadSingleFingerprintEntry()
 *********************************************************************************************************************/
/*! \brief          Function to read one specific fingerprint entry.
 *  \details        -
 *  \param[in]      fpType         The fingerprint type
 *  \param[in]      number         The fingerprint number
 *  \param[in]      OpStatus       The operation status
 *  \param[out]     data           The fingerprint data
 *  \return         Operation result
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_ReadSingleFingerprintEntry(DcmAddOn_FingerprintType fpType,
                                                                               uint8 number,
                                                                               Dcm_OpStatusType OpStatus,
                                                                               P2VAR(uint8, AUTOMATIC, DCMADDON_APPL_DATA) data);

/**********************************************************************************************************************
 *  DcmAddOn_ReadFingerprint()
 *********************************************************************************************************************/
/*! \brief          Function to read the data of a fingerprint from non-volatile memory.
 *  \details        -
 *  \param[in]      fpType         The fingerprint type
 *  \param[in]      OpStatus       The operation status
 *  \param[out]     Data           The fingerprint data
 *  \return         Operation result
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_ReadFingerprint(DcmAddOn_FingerprintType fpType,
                                                                    Dcm_OpStatusType OpStatus,
                                                                    P2VAR(uint8, AUTOMATIC, DCMADDON_APPL_DATA) Data);

/**********************************************************************************************************************
 *  DcmAddOn_ReadFingerprintDataLength()
 *********************************************************************************************************************/
/*! \brief          Function to read the number of stored fingerprints and calculate the response data length.
 *  \details        -
 *  \param[in]      fpType         The fingerprint type
 *  \param[in]      OpStatus       The operation status
 *  \param[out]     DataLength     The fingerprint data length
 *  \return         Operation result
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_ReadFingerprintDataLength(DcmAddOn_FingerprintType fpType,
                                                                              Dcm_OpStatusType OpStatus,
                                                                              P2VAR(uint16, AUTOMATIC, DCM_VAR_NOINIT) DataLength);

/**********************************************************************************************************************
 *  DcmAddOn_WriteFingerprint()
 *********************************************************************************************************************/
/*! \brief          Function to write one fingerprint entry to non-volatile memory.
 *  \details        -
 *  \param[in]      fpType         The fingerprint type
 *  \param[in]      Data           The fingerprint data
 *  \param[in]      DataLength     The fingerprint data length
 *  \param[in]      OpStatus       The operation status
 *  \param[out]     ErrorCode      The NRC
 *  \return         Operation result
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_WriteFingerprint(DcmAddOn_FingerprintType fpType,
                                                                     P2CONST(uint8, AUTOMATIC, DCM_VAR_NOINIT) Data,
                                                                     uint16 DataLength,
                                                                     Dcm_OpStatusType OpStatus,
                                                                     P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR_NOINIT) ErrorCode);

/* Other internal functions */
/**********************************************************************************************************************
 *  DcmAddOn_SetProgConditions()
 *********************************************************************************************************************/
/*! \brief          Implementation of the Dcm callout function to write parameters before jumping into the
 *                  flash boot loader.
 *  \details        This function is called by Dcm_SetProgConditions.
 *  \param[in,out]  progConditions    Conditions on which the jump to boot loader has been requested
 *  \return         E_OK              Conditions have correctly been set
 *  \return         E_NOT_OK          Conditions cannot be set
 *  \return         DCM_E_PENDING     Conditions set is in progress, a further call to this API is needed to end the
 *                                    setting
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_SetProgConditions(Dcm_ProgConditionsPtrType progConditions);

/**********************************************************************************************************************
 *  DcmAddOn_GetProgConditions()
 *********************************************************************************************************************/
/*! \brief          Implementation of the Dcm callout function to read parameters before jumping into the
 *                  flash boot loader.
 *  \details        This function is called by Dcm_GetProgConditions.
 *  \param[in,out]  progConditions    Conditions on which the jump to boot loader has been requested
 *  \return         DCM_COLD_START    The ECU starts normally
 *  \return         DCM_WARM_START    The ECU starts from a boot-loader jump
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Dcm_EcuStartModeType, DCMADDON_CODE) DcmAddOn_GetProgConditions(Dcm_ProgConditionsPtrType progConditions);

/**********************************************************************************************************************
 *  DcmAddOn_SetProgCond_GetEepValue()
 *********************************************************************************************************************/
/*! \brief          Function to calculate the eep-value needed for setting the programming conditions.
 *  \details        -
 *  \param[in]      progConditions    Conditions on which the jump to boot loader has been requested
 *  \param[in]      eepValue          The calculated eep-value
 *  \return         DCM_E_OK          The Eep-value has been successfully derived
 *  \return         DCM_E_NOT_OK      The progConditions are invalid
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_SetProgCond_GetEepValue(Dcm_ProgConditionsPtrType progConditions,
                                                                            P2VAR(DcmAddOn_ProgReqFlagValueType, AUTOMATIC, AUTOMATIC) eepValue);

/**********************************************************************************************************************
 *  DcmAddOn_SetProgCond_CheckForNextState()
 *********************************************************************************************************************/
/*! \brief          Function to check and set the current progress for setting the programming conditions.
 *  \details        -
 *  \param[in]      retVal         The return value of the last called API
 *  \param[in]      nextState      The required state
 *  \return         Operation result
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_SetProgCond_CheckForNextState(WrapNv_ReturnType retVal,
                                                                                  DcmAddOn_SetProgCondProgressType nextState);

/**********************************************************************************************************************
 *  DcmAddOn_ConvertOpStatus()
 *********************************************************************************************************************/
/*! \brief          Function to convert a Dcm_OpStatusType type to tWrapNvOpStatus type.
 *  \details        -
 *  \param[in]      OpStatus       The operation status
 *  \return         The converted operation status
 *  \context        TASK
 *  \reentrant      FALSE
 *  \pre            -
 *********************************************************************************************************************/
static FUNC(tWrapNvOpStatus, DCMADDON_CODE) DcmAddOn_ConvertOpStatus(Dcm_OpStatusType OpStatus);

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 **********************************************************************************************************************/
/**********************************************************************************************************************
 *  DcmAddOn_RepeaterExecute()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_RepeaterExecute(Dcm_OpStatusType OpStatus, Dcm_MsgContextPtrType pMsgContext)
{
  Std_ReturnType stdReturn;

  dcmAddOn_RepeaterContext.opStatus = OpStatus;

  for EVER
  {
    stdReturn = dcmAddOn_RepeaterContext.callee(dcmAddOn_RepeaterContext.opStatus,
                                                pMsgContext);

#if (DCMADDON_SUPPORT_CANCELLATION == STD_ON)
    if(DCM_CANCEL == OpStatus)
    {
      Dcm_ExternalSetNegResponse(pMsgContext, DCM_DIAG_CANCEL_OP_NRC);
      stdReturn = DCM_E_PROCESSINGDONE;
    }
#endif
    if(DCM_E_LOOP != stdReturn)
    {
      break;
    } /* else - just loop again */

    DcmAddOn_RepeaterResetOpStatus();
  }
  return stdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_RepeaterSetCallee()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(void, DCMADDON_CODE) DcmAddOn_RepeaterSetCallee(DcmAddOn_RepeaterProxyFuncType pProxy)
{
  DcmAddOn_Assert((NULL_PTR != pProxy), DCMADDON_DETSID_INTERNAL, DCM_E_PARAM_POINTER)

  dcmAddOn_RepeaterContext.callee = pProxy;
  DcmAddOn_RepeaterResetOpStatus();
}                                                                                                                                                     /* PRQA S 2006 */ /* MD_DcmAddOn_Protect_2006 */

/**********************************************************************************************************************
 *  DcmAddOn_Svc11_RepProxy_SetProgConditions()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Svc11_RepProxy_SetProgConditions(Dcm_OpStatusType OpStatus, Dcm_MsgContextPtrType pMsgContext)    /* PRQA S 3673 */ /* MD_DcmAddOn_Design_3673 */
{
  Std_ReturnType stdReturn;

  DCMADDON_IGNORE_UNREF_PARAM(OpStatus);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */

#if (DCMADDON_SUPPORT_CANCELLATION == STD_ON)
  if(DCM_CANCEL == OpStatus)
  {
    stdReturn = DCM_E_NOT_OK;
  }
  else
#endif
  {
    if(DCM_INITIAL == OpStatus)
    {
      uint16 testerSrcAddr;

      /* Prepare other parameters for Fbl */
      dcmAddOn_SetProgCondContext.progConditions.Sid                  = DCMADDON_SVC_ECURESET;
      dcmAddOn_SetProgCondContext.progConditions.SubFuncId            = 0x02U;
      dcmAddOn_SetProgCondContext.progConditions.ReprogrammingRequest = TRUE;
      dcmAddOn_SetProgCondContext.progConditions.ResponseRequired     = FALSE;

      /* Retrieve the tester source address from the RxPduId */
      if(DCM_E_OK != Dcm_GetTesterSourceAddress(pMsgContext->rxPduId, &testerSrcAddr))
      {
        DcmAddOn_AssertAlways(DCMADDON_DETSID_INTERNAL, DCMADDON_E_INTERFACE_RETURN_VALUE)
        Dcm_ExternalSetNegResponse(pMsgContext, DCM_E_PANIC_NRC);
        return DCM_E_PROCESSINGDONE;
      }

      dcmAddOn_SetProgCondContext.progConditions.TesterSourceAddr     = testerSrcAddr;
      dcmAddOn_SetProgCondContext.progress                            = DCMADDON_SETPROGCOND_PROGRESS_DECIDE_CONTEXT;
    }

    stdReturn = DcmAddOn_SetProgConditions(&dcmAddOn_SetProgCondContext.progConditions);
    switch(stdReturn)
    {
      case DCM_E_OK:
        /* Go to next repeater proxy */
        DcmAddOn_RepeaterSetCallee(DcmAddOn_Svc11_RepProxy_TriggerReset);
        stdReturn = DCM_E_LOOP;
        break;
      case DCM_E_PENDING:
        stdReturn = DCM_E_PENDING; /* Wait for finished job first */
        break;
      default: /* DCM_E_NOT_OK */
        Dcm_ExternalSetNegResponse(pMsgContext, DCM_E_CONDITIONSNOTCORRECT);
        stdReturn = DCM_E_PROCESSINGDONE;
        break;
    }
  }
  return stdReturn;
}                                                                                                                                                     /* PRQA S 2006 */ /* MD_DcmAddOn_Protect_2006 */

/**********************************************************************************************************************
 *  DcmAddOn_Svc11_RepProxy_TriggerReset()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Svc11_RepProxy_TriggerReset(Dcm_OpStatusType OpStatus, Dcm_MsgContextPtrType pMsgContext)         /* PRQA S 3673 */ /* MD_DcmAddOn_Design_3673 */
{
  Std_ReturnType stdReturn;
  DCMADDON_IGNORE_UNREF_PARAM(pMsgContext);                                                                                                           /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
  DCMADDON_IGNORE_UNREF_PARAM(OpStatus);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */

  /* Initiate Reset and go to the next repeater proxy */
  if(DCM_E_OK == Rte_Switch_DcmEcuReset_DcmEcuReset(RTE_MODE_DcmEcuReset_KEYONOFF))
  {
    DcmAddOn_RepeaterSetCallee(DcmAddOn_Svc11_RepProxy_WaitForResetAck);
    stdReturn = DCM_E_LOOP;
  }
  else
  {
    /* For some reason, no mode swtich is possible - exit with NRC */
    Dcm_ExternalSetNegResponse(pMsgContext, DCM_E_CONDITIONSNOTCORRECT);
    stdReturn = DCM_E_PROCESSINGDONE;
  }

  return stdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_Svc87_RepProxy_RequestEvaluation()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Svc87_RepProxy_RequestEvaluation(Dcm_OpStatusType OpStatus, Dcm_MsgContextPtrType pMsgContext)
{
  Std_ReturnType stdReturn;
  Dcm_NegativeResponseCodeType nrc = DCM_E_POSITIVERESPONSE;

  DCMADDON_IGNORE_UNREF_PARAM(OpStatus);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */

  DcmAddOn_Assert((DCM_INITIAL == OpStatus), DCMADDON_DETSID_INTERNAL, DCM_E_PARAM)

  /* Do the request verification and send negative response if necessary */
  /* Minimum length check -> Due to callout on SID level not done by Dcm */
  if(0 < pMsgContext->reqDataLen)
  {
    /* Dispatching the sub-function */
    switch(pMsgContext->reqData[0])
    {
      case 0x01U:
        /* Check for session restriction not necessary, because the session restriction is on service level */
        /* Total length check */
        if(2 == pMsgContext->reqDataLen)
        {
          /* Check the baud rate parameter */
          switch(pMsgContext->reqData[1])
          {
            case 0x11U:
            case 0x12U: /* Fall through */
            case 0x13U: /* Fall through */
              /* Valid request */
              break;
            default:
              nrc = DCM_E_REQUESTOUTOFRANGE;
              break;
          }
        }
        else
        {
          nrc = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
        }
        break;
      case 0x03U:
        /* Check for session restriction not necessary, because the session restriction is on service level */
        /* Minimum length check */
        if(1 == pMsgContext->reqDataLen)
        {
          nrc = DCM_E_REQUESTSEQUENCEERROR;
        }
        else
        {
          nrc = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
        }
        break;
      default: /* Sub-function 0x02 and all other non-standard sub-functions */
        nrc = DCM_E_SUBFUNCTIONNOTSUPPORTED;
        break;
    }
  }
  else
  {
    nrc = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
  }

  if(DCM_E_POSITIVERESPONSE != nrc)
  {
    /* Request is not valid, set the NRC and end the service processing */
    Dcm_ExternalSetNegResponse(pMsgContext, nrc);
    stdReturn = DCM_E_PROCESSINGDONE;
  }
  else
  {
    /* valid request -> go to the next repeater proxy */
    DcmAddOn_RepeaterSetCallee(DcmAddOn_Svc87_RepProxy_WriteBaudRate);
    /* Sub-function already in the buffer, just add it to the response data */
    pMsgContext->resDataLen++;
    stdReturn = DCM_E_LOOP;
  }

  return stdReturn;
}                                                                                                                                                     /* PRQA S 6030 */ /* MD_MSR_STCYC */

/**********************************************************************************************************************
 *  DcmAddOn_Svc87_RepProxy_WriteBaudRate()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Svc87_RepProxy_WriteBaudRate(Dcm_OpStatusType OpStatus, Dcm_MsgContextPtrType pMsgContext)        /* PRQA S 3673 */ /* MD_DcmAddOn_Design_3673 */
{
  Std_ReturnType stdReturn = DCM_E_PROCESSINGDONE;
  WrapNv_ReturnType retVal;

  DCMADDON_IGNORE_UNREF_PARAM(OpStatus);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */

  /* Write to non-volatile memory */
  retVal = ApplFblNvWriteAsyncBaudrateParam(0, &(pMsgContext->reqData[1]), DcmAddOn_ConvertOpStatus(OpStatus));
  if(WRAPNV_E_OK == retVal)
  {
    /* Go to next repeater proxy */
    DcmAddOn_RepeaterSetCallee(DcmAddOn_Svc87_RepProxy_SetProgConditions);
    stdReturn = DCM_E_LOOP;
  }
  else if(WRAPNV_E_PENDING == retVal)
  {
    stdReturn = DCM_E_PENDING;
  }
  else /* WRAPNV_E_NOT_OK */
  {
#if !defined (FBL_ENABLE_EEPMGR)
    if(WRAPNV_E_NOT_OK != retVal)
    {
      DcmAddOn_AssertAlways(DCMADDON_DETSID_INTERNAL, DCMADDON_E_INTERFACE_RETURN_VALUE)
      Dcm_ExternalSetNegResponse(pMsgContext, DCM_E_PANIC_NRC);
    }
#endif
    Dcm_ExternalSetNegResponse(pMsgContext, DCM_E_CONDITIONSNOTCORRECT);
    /* Return value already set to DCM_E_PROCESSINGDONE */
  }
  return stdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_Svc87_RepProxy_SetProgConditions()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Svc87_RepProxy_SetProgConditions(Dcm_OpStatusType OpStatus, Dcm_MsgContextPtrType pMsgContext)    /* PRQA S 3673 */ /* MD_DcmAddOn_Design_3673 */
{
  Std_ReturnType stdReturn;

  DCMADDON_IGNORE_UNREF_PARAM(OpStatus);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */

#if (DCMADDON_SUPPORT_CANCELLATION == STD_ON)
  if(DCM_CANCEL == OpStatus)
  {
    stdReturn = DCM_E_NOT_OK;
  }
  else
#endif
  {
    if(DCM_INITIAL == OpStatus)
    {
      uint16 testerSrcAddr;

      /* Prepare other parameters for Fbl */
      dcmAddOn_SetProgCondContext.progConditions.Sid                  = DCMADDON_SVC_LINKCTRL;
      dcmAddOn_SetProgCondContext.progConditions.SubFuncId            = 0x01U;
      dcmAddOn_SetProgCondContext.progConditions.ReprogrammingRequest = TRUE;
      dcmAddOn_SetProgCondContext.progConditions.ResponseRequired     = TRUE;

      /* Retrieve the tester source address from the RxPduId */
      if(DCM_E_OK != Dcm_GetTesterSourceAddress(pMsgContext->rxPduId, &testerSrcAddr))
      {
        DcmAddOn_AssertAlways(DCMADDON_DETSID_INTERNAL, DCMADDON_E_INTERFACE_RETURN_VALUE)
        Dcm_ExternalSetNegResponse(pMsgContext, DCM_E_PANIC_NRC);
        return DCM_E_PROCESSINGDONE;
      }

      dcmAddOn_SetProgCondContext.progConditions.TesterSourceAddr     = testerSrcAddr;
      dcmAddOn_SetProgCondContext.progress                            = DCMADDON_SETPROGCOND_PROGRESS_DECIDE_CONTEXT;
    }

    stdReturn = DcmAddOn_SetProgConditions(&dcmAddOn_SetProgCondContext.progConditions);
    switch(stdReturn)
    {
      case DCM_E_OK:
        /* Go to next repeater proxy */
        DcmAddOn_RepeaterSetCallee(DcmAddOn_Svc87_RepProxy_TriggerReset);
        stdReturn = DCM_E_LOOP;
        break;
      case DCM_E_PENDING:
        stdReturn = DCM_E_PENDING; /* Wait for finished job first */
        break;
      default: /* DCM_E_NOT_OK */
        Dcm_ExternalSetNegResponse(pMsgContext, DCM_E_CONDITIONSNOTCORRECT);
        stdReturn = DCM_E_PROCESSINGDONE;
        break;
    }
  }
  return stdReturn;
}                                                                                                                                                     /* PRQA S 2006 */ /* MD_DcmAddOn_Protect_2006 */

/**********************************************************************************************************************
 *  DcmAddOn_Svc87_RepProxy_TriggerReset()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Svc87_RepProxy_TriggerReset(Dcm_OpStatusType OpStatus
                                                                               ,Dcm_MsgContextPtrType pMsgContext)                                    /* PRQA S 3673 */ /* MD_DcmAddOn_Design_3673 */
{
  Std_ReturnType stdReturn;
  DCMADDON_IGNORE_UNREF_PARAM(pMsgContext);                                                                                                           /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
  switch(OpStatus)
  {
    case DCM_INITIAL:
      /* Before triggering the reset, a response pending should be sent first */
      stdReturn = DCM_E_FORCE_RCRRP;
      break;
    case DCM_FORCE_RCRRP_NOT_OK:
    case DCM_FORCE_RCRRP_OK:
      /* Response pending sent -> Initiate Reset and go to the next repeater proxy */
      if(DCM_E_OK == Rte_Switch_DcmEcuReset_DcmEcuReset(RTE_MODE_DcmEcuReset_JUMPTOBOOTLOADER))
      {
        /* Determine entry point - with or without RCR-RP */
        DcmAddOn_RepeaterSetCallee(DcmAddOn_Svc87_RepProxy_WaitForResetAck);
        stdReturn = DCM_E_LOOP;
      }
      else
      {
        /* For some reason, no mode swtich is possible - exit with NRC */
        Dcm_ExternalSetNegResponse(pMsgContext, DCM_E_CONDITIONSNOTCORRECT);
        stdReturn = DCM_E_PROCESSINGDONE;
      }
      break;
#if (DCMADDON_SUPPORT_CANCELLATION == STD_ON)
    case DCM_CANCEL:
      stdReturn = DCM_E_OK; /* Return value will be discarded */
      break;
#endif
    default:
      DcmAddOn_AssertAlways(DCMADDON_DETSID_INTERNAL, DCMADDON_E_INVALID_STATE)
      Dcm_ExternalSetNegResponse(pMsgContext, DCM_E_PANIC_NRC);
      stdReturn = DCM_E_PROCESSINGDONE;
      break;
  }
  return stdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_Svc11_RepProxy_WaitForResetAck()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Svc11_RepProxy_WaitForResetAck(Dcm_OpStatusType OpStatus
                                                                            ,Dcm_MsgContextPtrType pMsgContext)                                       /* PRQA S 3673 */ /* MD_DcmAddOn_Design_3673 */
{
  Std_ReturnType lStdReturn;

  DCMADDON_IGNORE_UNREF_PARAM(OpStatus);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
  DCMADDON_IGNORE_UNREF_PARAM(pMsgContext);                                                                                                           /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */

  /* Trigger a mode switch to a bootloader type */
  lStdReturn = Rte_SwitchAck_DcmEcuReset_DcmEcuReset();
  switch(lStdReturn)
  {
    case RTE_E_TRANSMIT_ACK:
      /*Processing done here, Dcm sends postive response and execute reset on post handler*/
      lStdReturn = DCM_E_PROCESSINGDONE;
      break;
    case RTE_E_NO_DATA:
      lStdReturn = DCM_E_PENDING; /* Wait for ack */
      break;
    default:
      Dcm_ExternalSetNegResponse(pMsgContext, DCM_E_CONDITIONSNOTCORRECT);
      lStdReturn = DCM_E_PROCESSINGDONE;
      break;
  }
  return lStdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_Svc87_RepProxy_WaitForResetAck()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Svc87_RepProxy_WaitForResetAck(Dcm_OpStatusType OpStatus
                                                                            ,Dcm_MsgContextPtrType pMsgContext)                                       /* PRQA S 3673 */ /* MD_DcmAddOn_Design_3673 */
{
  Std_ReturnType lStdReturn;

  DCMADDON_IGNORE_UNREF_PARAM(OpStatus);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
  DCMADDON_IGNORE_UNREF_PARAM(pMsgContext);                                                                                                           /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */

  /* Trigger a mode switch to a bootloader type */
  lStdReturn = Rte_SwitchAck_DcmEcuReset_DcmEcuReset();
  switch(lStdReturn)
  {
    case RTE_E_TRANSMIT_ACK:
      /* go on with reset */
      lStdReturn = Rte_Switch_DcmEcuReset_DcmEcuReset(RTE_MODE_DcmEcuReset_EXECUTE); /* Fire and forget */

      if(RTE_E_OK == lStdReturn)
      {
        lStdReturn = DCM_E_OK; /* just wait for reset */
      }
      else
      {
        DcmAddOn_AssertAlways(DCMADDON_DETSID_INTERNAL, DCM_E_ILLEGAL_STATE)

        Dcm_ExternalSetNegResponse(pMsgContext, DCM_E_PANIC_NRC);
        lStdReturn = DCM_E_PROCESSINGDONE;
      }
      break;
    case RTE_E_NO_DATA:
      lStdReturn = DCM_E_PENDING; /* Wait for ack */
      break;
    default:
      Dcm_ExternalSetNegResponse(pMsgContext, DCM_E_CONDITIONSNOTCORRECT);
      lStdReturn = DCM_E_PROCESSINGDONE;
      break;
  }
  return lStdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_ReadNumOfWrittenFingerprints()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_ReadNumOfWrittenFingerprints(DcmAddOn_FingerprintType fpType,
                                                                                 Dcm_OpStatusType OpStatus,
                                                                                 P2VAR(uint8, AUTOMATIC, DCMADDON_APPL_DATA) result)
{
  Std_ReturnType stdReturn;
  WrapNv_ReturnType retValue;
  uint8 lFpNumber = 0xFFU;

#if (DCMADDON_FINGERPRINT_END == 3)
  /* Everything fine*/
#else
# error "Changes in the enumeration requires adaption of the code! Remove this error if adaptions were made."
#endif

  switch(fpType)
  {
    case DCMADDON_FP_APPLICATION_SOFTWARE:
      retValue = ApplFblNvReadAsyncSwFingerprintNumber(0, &lFpNumber, DcmAddOn_ConvertOpStatus(OpStatus));
      break;
    case DCMADDON_FP_APPLICATION_DATA:
      retValue = ApplFblNvReadAsyncDataFingerprintNumber(0, &lFpNumber, DcmAddOn_ConvertOpStatus(OpStatus));
      break;
#if (DCMADDON_SUPPORT_BOOT_SW_FP == STD_ON)
    case DCMADDON_FP_BOOT_SOFTWARE:
      retValue = ApplFblNvReadAsyncBootFingerprintNumber(0, &lFpNumber, DcmAddOn_ConvertOpStatus(OpStatus));
      break;
#endif
    default:
      retValue = WRAPNV_E_NOT_OK;
      break;
  }

#if (DCMADDON_SUPPORT_CANCELLATION == STD_ON)
  if(DCM_CANCEL == OpStatus)
  {
    retValue = WRAPNV_E_NOT_OK;
  }
#endif

  switch(retValue)
  {
    case WRAPNV_E_OK:
      /* The number of written fingerprints is stored negated */
      *result = (uint8)(~lFpNumber);                                                                                                                  /* PRQA S 0291 */ /* MD_DcmAddOn_BitNegation_0291 */
      stdReturn = DCM_E_OK;
      break;
    case WRAPNV_E_PENDING:                                                                                                                            /* PRQA S 3201 */ /* MD_DcmAddOn_UnreachableStmt_3201 */
      stdReturn = DCM_E_PENDING;
      break;
    default: /* WRAPNV_E_NOT_OK */
      DcmAddOn_Assert((WRAPNV_E_NOT_OK == retValue), DCMADDON_DETSID_INTERNAL, DCMADDON_E_INTERFACE_RETURN_VALUE)                                     /* PRQA S 3201, 3355, 3356, 3359 */ /* MD_DcmAddOn_UnreachableStmt_3201, MD_DcmAddOn_ConstantCondition_3335 */
      stdReturn = DCM_E_NOT_OK;
      break;
  }
  return stdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_ReadNumOfFingerprintsInBuffer()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_ReadNumOfFingerprintsInBuffer(DcmAddOn_FingerprintType fpType,
                                                                                  Dcm_OpStatusType OpStatus,
                                                                                  P2VAR(uint8, AUTOMATIC, DCMADDON_APPL_DATA) result)
{
  Std_ReturnType stdReturn;

  stdReturn = DcmAddOn_ReadNumOfWrittenFingerprints(fpType, OpStatus, result);
  if(DCM_E_OK == stdReturn)
  {
    /* When more fingerprints than the history size have been written, only the last x fingerprints are stored */
    if (*result > DCMADDON_FINGERPRINT_HISTORY_SIZE)
    {
      *result = (uint8)DCMADDON_FINGERPRINT_HISTORY_SIZE;
    }
  }

  return stdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_ReadSingleFingerprintEntry()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_ReadSingleFingerprintEntry(DcmAddOn_FingerprintType fpType,
                                                                               uint8 number,
                                                                               Dcm_OpStatusType OpStatus,
                                                                               P2VAR(uint8, AUTOMATIC, DCMADDON_APPL_DATA) data)
{
  Std_ReturnType stdReturn = DCM_E_OK;
  uint16 lCounter;
  uint8 lFpCount;

  if(DCM_INITIAL == OpStatus)
  {
    dcmAddOn_ReadSingleFpContext.readState = DCMADDON_RD_FINGERPRINT_NUM;
  }

  if(DCMADDON_RD_FINGERPRINT_NUM == dcmAddOn_ReadSingleFpContext.readState)
  {
    stdReturn = DcmAddOn_ReadNumOfWrittenFingerprints(fpType, OpStatus, &lFpCount);

    if(DCM_E_OK == stdReturn)
    {
      if(lFpCount >= DCMADDON_FINGERPRINT_HISTORY_SIZE)
      {
        /* Wraparound in the ring-buffer already happened */
        lCounter = (uint16)lFpCount + (uint16)number;
      }
      else
      {
        lCounter = (uint16)number;
      }

      /* Wrap around due to ring-buffer organization */
      lCounter %= DCMADDON_FINGERPRINT_HISTORY_SIZE;

      dcmAddOn_ReadSingleFpContext.counter   = lCounter;
      dcmAddOn_ReadSingleFpContext.opStatus  = DCM_INITIAL;
      dcmAddOn_ReadSingleFpContext.readState = DCMADDON_RD_FINGERPRINT_DATA;
    } /* else - reading failed or pending */
  }

  if(DCMADDON_RD_FINGERPRINT_DATA == dcmAddOn_ReadSingleFpContext.readState)
  {
    WrapNv_ReturnType retValue = WRAPNV_E_NOT_OK;

#if (DCMADDON_SUPPORT_CANCELLATION == STD_ON)
    if(DCM_CANCEL == OpStatus)
    {
      dcmAddOn_ReadSingleFpContext.opStatus = DCM_CANCEL;
    }
#endif

#if (DCMADDON_FINGERPRINT_END == 3)
    /* Everything fine*/
#else
# error "Changes in the enumeration requires adaption of the code! Remove this error if adaptions were made."
#endif

    switch(fpType)
    {
      case DCMADDON_FP_APPLICATION_SOFTWARE:
        retValue = ApplFblNvReadAsyncSwFingerprint(dcmAddOn_ReadSingleFpContext.counter, data, DcmAddOn_ConvertOpStatus(dcmAddOn_ReadSingleFpContext.opStatus));
        break;
      case DCMADDON_FP_APPLICATION_DATA:
        retValue = ApplFblNvReadAsyncDataFingerprint(dcmAddOn_ReadSingleFpContext.counter, data, DcmAddOn_ConvertOpStatus(dcmAddOn_ReadSingleFpContext.opStatus));
        break;
#if (DCMADDON_SUPPORT_BOOT_SW_FP == STD_ON)
      case DCMADDON_FP_BOOT_SOFTWARE:
        retValue = ApplFblNvReadAsyncBootFingerprint(dcmAddOn_ReadSingleFpContext.counter, data, DcmAddOn_ConvertOpStatus(dcmAddOn_ReadSingleFpContext.opStatus));
        break;
#endif
      default:
        DcmAddOn_AssertAlways(DCMADDON_DETSID_INTERNAL, DCMADDON_E_UNSUPPORTED_FINGERPRINT)
        break;
    }

#if (DCMADDON_SUPPORT_CANCELLATION == STD_ON)
    if(DCM_CANCEL == OpStatus)
    {
      retValue = WRAPNV_E_NOT_OK;
    }
#endif

    switch(retValue)
    {
      case WRAPNV_E_OK:
        stdReturn = DCM_E_OK;
        break;
      case WRAPNV_E_PENDING:                                                                                                                          /* PRQA S 3201 */ /* MD_DcmAddOn_UnreachableStmt_3201 */
        dcmAddOn_ReadSingleFpContext.opStatus = DCM_PENDING;
        stdReturn = DCM_E_PENDING;
        break;
      default: /* WRAPNV_E_NOT_OK */
        DcmAddOn_Assert((WRAPNV_E_NOT_OK == retValue), DCMADDON_DETSID_INTERNAL, DCMADDON_E_INTERFACE_RETURN_VALUE)                                   /* PRQA S 3201, 3355, 3356, 3359 */ /* MD_DcmAddOn_UnreachableStmt_3201, MD_DcmAddOn_ConstantCondition_3335 */
        stdReturn = DCM_E_NOT_OK;
        break;
    }
  }

  return stdReturn;
}                                                                                                                                                     /* PRQA S 6010, 6030 */ /* MD_MSR_STPTH, MD_MSR_STCYC */

/**********************************************************************************************************************
 *  DcmAddOn_ReadFingerprint()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_ReadFingerprint(DcmAddOn_FingerprintType fpType,
                                                                    Dcm_OpStatusType OpStatus,
                                                                    P2VAR(uint8, AUTOMATIC, DCMADDON_APPL_DATA) Data)
{
  Std_ReturnType stdReturn = DCM_E_OK;

  if(DCM_INITIAL == OpStatus)
  {
    dcmAddOn_ReadFingerprintContext.readState = DCMADDON_RD_FINGERPRINT_NUM;
  }

  if(DCMADDON_RD_FINGERPRINT_NUM == dcmAddOn_ReadFingerprintContext.readState)
  {
    /* Get the number of stored fingerprints */
    stdReturn = DcmAddOn_ReadNumOfFingerprintsInBuffer(fpType, OpStatus, &dcmAddOn_ReadFingerprintContext.count);

    if(DCM_E_OK == stdReturn)
    {
      dcmAddOn_ReadFingerprintContext.index     = (uint8)0;
      dcmAddOn_ReadFingerprintContext.opStatus  = DCM_INITIAL;
      dcmAddOn_ReadFingerprintContext.readState = DCMADDON_RD_FINGERPRINT_DATA;
    } /* else - reading failed or pending */
  }

  if(DCMADDON_RD_FINGERPRINT_DATA == dcmAddOn_ReadFingerprintContext.readState)
  {
    Std_ReturnType retVal = DCM_E_OK;

#if (DCMADDON_SUPPORT_CANCELLATION == STD_ON)
    if(DCM_CANCEL == OpStatus)
    {
      dcmAddOn_ReadFingerprintContext.opStatus = DCM_CANCEL;
    }
#endif

    while( (dcmAddOn_ReadFingerprintContext.index < dcmAddOn_ReadFingerprintContext.count)
         &&(DCM_E_OK == retVal) )
    {
      /* Read a single fingerprint entry */
      retVal = DcmAddOn_ReadSingleFingerprintEntry(fpType,
                                                   dcmAddOn_ReadFingerprintContext.index,
                                                   dcmAddOn_ReadFingerprintContext.opStatus,
                                                   &Data[1 + (dcmAddOn_ReadFingerprintContext.index * DCMADDON_SIZE_FINGERPRINT)]);

      if(DCM_E_OK == retVal)
      {
        ++dcmAddOn_ReadFingerprintContext.index;
      }

      if(DCM_E_PENDING == retVal)
      {
        dcmAddOn_ReadFingerprintContext.opStatus = DCM_PENDING;
      }
      else
      {
        dcmAddOn_ReadFingerprintContext.opStatus = DCM_INITIAL;
      }
      stdReturn = retVal;
    }

    /* Set number of read fingerprints */
    Data[0] = dcmAddOn_ReadFingerprintContext.index;
  }

  return stdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_ReadFingerprintDataLength()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_ReadFingerprintDataLength(DcmAddOn_FingerprintType fpType,
                                                                              Dcm_OpStatusType OpStatus,
                                                                              P2VAR(uint16, AUTOMATIC, DCM_VAR_NOINIT) DataLength)
{
  Std_ReturnType stdReturn;
  uint8 lCount;

  /* Get the number of stored fingerprints */
  stdReturn = DcmAddOn_ReadNumOfFingerprintsInBuffer(fpType, OpStatus, &lCount);

  if(DCM_E_OK == stdReturn)
  {
    /* Calculate length including the fingerprint count */
    *DataLength = (uint16)(1 + (lCount * DCMADDON_SIZE_FINGERPRINT));
  } /* else - reading failed or pending */

  return stdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_WriteFingerprint()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_WriteFingerprint(DcmAddOn_FingerprintType fpType,
                                                                     P2CONST(uint8, AUTOMATIC, DCM_VAR_NOINIT) Data,
                                                                     uint16 DataLength,
                                                                     Dcm_OpStatusType OpStatus,
                                                                     P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR_NOINIT) ErrorCode)
{
  Std_ReturnType stdReturn = DCM_E_NOT_OK;

  if(DCM_INITIAL == OpStatus)
  {
    dcmAddOn_WriteFingerprintContext.writeState = DCMADDON_WR_FINGERPRINT_IDLE;

    /* Total length check */
    if((uint16)(1 + DCMADDON_SIZE_FINGERPRINT) == DataLength)
    {
      /* Check for number of blocks to be only 1 */
      if((Data[0] == 0x01U))
      {
        dcmAddOn_WriteFingerprintContext.writeState = DCMADDON_WR_FINGERPRINT_NUM_RD;
      }
      else
      {
        *ErrorCode = DCM_E_REQUESTOUTOFRANGE;
      }
    }
    else
    {
      *ErrorCode = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
    }
  }

  if(DCMADDON_WR_FINGERPRINT_NUM_RD == dcmAddOn_WriteFingerprintContext.writeState)
  {
    uint8 lFpNumber;
    stdReturn = DcmAddOn_ReadNumOfWrittenFingerprints(fpType, OpStatus, &lFpNumber);

    if(DCM_E_OK == stdReturn)
    {
      dcmAddOn_WriteFingerprintContext.fpNumber   = lFpNumber;
      dcmAddOn_WriteFingerprintContext.opStatus   = DCM_INITIAL;
      dcmAddOn_WriteFingerprintContext.writeState = DCMADDON_WR_FINGERPRINT_DATA;
    }
    else if(DCM_E_NOT_OK == stdReturn)
    {
      *ErrorCode = DCM_E_GENERALPROGRAMMINGFAILURE;
    }
    else
    {
      /* Pending */
    }
  }

  if(DCMADDON_WR_FINGERPRINT_DATA == dcmAddOn_WriteFingerprintContext.writeState)
  {
    WrapNv_ReturnType retValue                = WRAPNV_E_NOT_OK;

#if (DCMADDON_SUPPORT_CANCELLATION == STD_ON)
    if(DCM_CANCEL == OpStatus)
    {
      dcmAddOn_WriteFingerprintContext.opStatus = DCM_CANCEL;
    }
#endif

#if (DCMADDON_FINGERPRINT_END == 3)
    /* Everything fine*/
#else
# error "Changes in the enumeration requires adaption of the code! Remove this error if adaptions were made."
#endif

    /*
     * Store fingerprint in ring buffer (starting at offset of fingerprint type)
     * In case of overflow the oldest entry will be overwritten.
     */
    switch(fpType)
    {
      case DCMADDON_FP_APPLICATION_SOFTWARE:
        retValue = ApplFblNvWriteAsyncSwFingerprint((dcmAddOn_WriteFingerprintContext.fpNumber % DCMADDON_FINGERPRINT_HISTORY_SIZE),       /* PRQA S 0311 */ /* MD_DcmAddOn_Design_0311 */
                                                    (uint8*)&Data[1], /* Point to the record, cast introduced to match 
                                                    function signature with EepM legacy case, override const qualifier*/
                                                    DcmAddOn_ConvertOpStatus(dcmAddOn_WriteFingerprintContext.opStatus));
        break;
      case DCMADDON_FP_APPLICATION_DATA:
        retValue = ApplFblNvWriteAsyncDataFingerprint((dcmAddOn_WriteFingerprintContext.fpNumber % DCMADDON_FINGERPRINT_HISTORY_SIZE),       /* PRQA S 0311 */ /* MD_DcmAddOn_Design_0311 */
                                                      (uint8*)&Data[1], /* Point to the record, cast introduced to match 
                                                                        function signature with EepM legacy case, override const qualifier*/
                                                      DcmAddOn_ConvertOpStatus(dcmAddOn_WriteFingerprintContext.opStatus));
        break;
#if (DCMADDON_SUPPORT_BOOT_SW_FP == STD_ON)
      case DCMADDON_FP_BOOT_SOFTWARE:
        retValue = ApplFblNvWriteAsyncBootFingerprint((dcmAddOn_WriteFingerprintContext.fpNumber % DCMADDON_FINGERPRINT_HISTORY_SIZE),       /* PRQA S 0311 */ /* MD_DcmAddOn_Design_0311 */
                                                      (uint8*)&Data[1], /* Point to the record, cast introduced to match 
                                                                        function signature with EepM legacy case, override const qualifier*/
                                                      DcmAddOn_ConvertOpStatus(dcmAddOn_WriteFingerprintContext.opStatus));
        break;
#endif
      default:
        DcmAddOn_AssertAlways(DCMADDON_DETSID_INTERNAL, DCMADDON_E_UNSUPPORTED_FINGERPRINT)
        break;
    }

#if (DCMADDON_SUPPORT_CANCELLATION == STD_ON)
    if(DCM_CANCEL == OpStatus)
    {
      retValue = WRAPNV_E_NOT_OK;
    }
#endif

    if(WRAPNV_E_OK == retValue)
    {
      dcmAddOn_WriteFingerprintContext.opStatus = DCM_INITIAL;
      dcmAddOn_WriteFingerprintContext.writeState = DCMADDON_WR_FINGERPRINT_NUM_CALC;
    }
    else if(WRAPNV_E_PENDING == retValue)
    {
      dcmAddOn_WriteFingerprintContext.opStatus = DCM_PENDING;
      stdReturn = DCM_E_PENDING;
    }
    else /* WRAPNV_E_NOT_OK */
    {
      DcmAddOn_Assert((WRAPNV_E_NOT_OK == retValue), DCMADDON_DETSID_INTERNAL, DCMADDON_E_INTERFACE_RETURN_VALUE)
      *ErrorCode = DCM_E_GENERALPROGRAMMINGFAILURE;
      stdReturn = DCM_E_NOT_OK;
    }
  }

  if(DCMADDON_WR_FINGERPRINT_NUM_CALC == dcmAddOn_WriteFingerprintContext.writeState)
  {
    uint16 lCounter = (uint16)dcmAddOn_WriteFingerprintContext.fpNumber;

    /* Increase number of stored fingerprints */
    lCounter++;

    /* Counter value has to fit in 8 bit range */
    if (lCounter > (uint16)0xFFU)
    {
      /*
       * Calculate modulo, add fingerprint number because all fingerprints are filled >= DCMADDON_FINGERPRINT_HISTORY_SIZE
       * is used as marker for full buffer.
       */
      lCounter = (lCounter % DCMADDON_FINGERPRINT_HISTORY_SIZE) + DCMADDON_FINGERPRINT_HISTORY_SIZE;
    }

    /* Convert to byte value */
    dcmAddOn_WriteFingerprintContext.fpNumber = (uint8)lCounter;

    /* Fingerprint number is stored inverted */
    dcmAddOn_WriteFingerprintContext.fpNumber = (uint8)(~dcmAddOn_WriteFingerprintContext.fpNumber);                                                  /* PRQA S 0291 */ /* MD_DcmAddOn_BitNegation_0291 */

    dcmAddOn_WriteFingerprintContext.writeState = DCMADDON_WR_FINGERPRINT_NUM_WR;
  }

  if(DCMADDON_WR_FINGERPRINT_NUM_WR == dcmAddOn_WriteFingerprintContext.writeState)
  {
    WrapNv_ReturnType retValue = WRAPNV_E_NOT_OK;

#if (DCMADDON_SUPPORT_CANCELLATION == STD_ON)
    if(DCM_CANCEL == OpStatus)
    {
      dcmAddOn_WriteFingerprintContext.opStatus = DCM_CANCEL;
    }
#endif

    /* Update number of fingerprints */
#if (DCMADDON_FINGERPRINT_END == 3)
/* Everything fine*/
#else
# error "Changes in the enumeration requires adaption of the code! Remove this error if adaptions were made."
#endif
    /* Write fingerprint number using offset ID */
    switch(fpType)
    {
      case DCMADDON_FP_APPLICATION_SOFTWARE:
        retValue = ApplFblNvWriteAsyncSwFingerprintNumber(0,
                                                          &dcmAddOn_WriteFingerprintContext.fpNumber,
                                                          DcmAddOn_ConvertOpStatus(dcmAddOn_WriteFingerprintContext.opStatus));
        break;
      case DCMADDON_FP_APPLICATION_DATA:
        retValue = ApplFblNvWriteAsyncDataFingerprintNumber(0,
                                                            &dcmAddOn_WriteFingerprintContext.fpNumber,
                                                            DcmAddOn_ConvertOpStatus(dcmAddOn_WriteFingerprintContext.opStatus));
        break;
#if (DCMADDON_SUPPORT_BOOT_SW_FP == STD_ON)
      case DCMADDON_FP_BOOT_SOFTWARE:
        retValue = ApplFblNvWriteAsyncBootFingerprintNumber(0,
                                                            &dcmAddOn_WriteFingerprintContext.fpNumber,
                                                            DcmAddOn_ConvertOpStatus(dcmAddOn_WriteFingerprintContext.opStatus));
        break;
#endif
      default:
        DcmAddOn_AssertAlways(DCMADDON_DETSID_INTERNAL, DCMADDON_E_UNSUPPORTED_FINGERPRINT)
        break;
    }

#if (DCMADDON_SUPPORT_CANCELLATION == STD_ON)
    if(DCM_CANCEL == OpStatus)
    {
      retValue = WRAPNV_E_NOT_OK;
    }
#endif

    if(WRAPNV_E_OK == retValue)
    {
      dcmAddOn_WriteFingerprintContext.fpType = fpType; /* store fingerprint type for write protection confirmation function */
      stdReturn = DCM_E_OK;
    }
    else if(WRAPNV_E_PENDING == retValue)
    {
      dcmAddOn_WriteFingerprintContext.opStatus = DCM_PENDING;
      stdReturn = DCM_E_PENDING;
    }
    else /* WRAPNV_E_NOT_OK */
    {
      *ErrorCode = DCM_E_GENERALPROGRAMMINGFAILURE;
      stdReturn = DCM_E_NOT_OK;
    }
  }

  return stdReturn;
}                                                                                                                                                     /* PRQA S 6010, 6030 */ /* MD_MSR_STPTH, MD_MSR_STCYC */

/**********************************************************************************************************************
 *  DcmAddOn_SetProgConditions()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *
 *
 *
 *
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_SetProgConditions(Dcm_ProgConditionsPtrType progConditions)
{
  Std_ReturnType stdReturn = DCM_E_NOT_OK;
  WrapNv_ReturnType retVal;

  if(DCMADDON_SETPROGCOND_PROGRESS_DECIDE_CONTEXT == dcmAddOn_SetProgCondContext.progress)
  {
    dcmAddOn_SetProgCondContext.opStatus = DCM_INITIAL;

    /* Decide whether the function is called for reprogramming or for clearing flags after returning from FBL */
    if( (TRUE == progConditions->ReprogrammingRequest)
      ||(TRUE == progConditions->ResponseRequired) )
    {
      dcmAddOn_SetProgCondContext.progress = DCMADDON_SETPROGCOND_PROGRESS_PROG_REQ_FLAG;
    }
    else
    {
      dcmAddOn_SetProgCondContext.progress = DCMADDON_SETPROGCOND_PROGRESS_RST_RESP_FLAG;
    }
  }

  if(DCMADDON_SETPROGCOND_PROGRESS_PROG_REQ_FLAG == dcmAddOn_SetProgCondContext.progress)
  {
    /* Reprogramming request states begin */
    /* Mapping of the Dcm parameter values to the Fbl parameter value */
    DcmAddOn_ProgReqFlagValueType eepValue;

    if(DCM_E_OK == DcmAddOn_SetProgCond_GetEepValue(progConditions, &eepValue))
    {
      /* Write to non-volatile memory */
      retVal = ApplFblNvWriteAsyncProgReqFlag(0, &eepValue, DcmAddOn_ConvertOpStatus(dcmAddOn_SetProgCondContext.opStatus));

      stdReturn = DcmAddOn_SetProgCond_CheckForNextState(retVal, DCMADDON_SETPROGCOND_PROGRESS_TESTER_SRC_ADDR);
    }
    else
    {
      /* Reset state machine */
      stdReturn = DcmAddOn_SetProgCond_CheckForNextState(E_NOT_OK, DCMADDON_SETPROGCOND_PROGRESS_DECIDE_CONTEXT);
    }
  }

  if(DCMADDON_SETPROGCOND_PROGRESS_TESTER_SRC_ADDR == dcmAddOn_SetProgCondContext.progress)
  {
    /* No mapping required, write directly -> configuration of TesterSourceAddr has to be correct */
    DcmAddOn_TesterSrcAddrValueType testerSrcAddr = (DcmAddOn_TesterSrcAddrValueType)(progConditions->TesterSourceAddr);
    retVal = ApplFblNvWriteAsyncTesterConnection(0, &testerSrcAddr, DcmAddOn_ConvertOpStatus(dcmAddOn_SetProgCondContext.opStatus));

    stdReturn = DcmAddOn_SetProgCond_CheckForNextState(retVal, DCMADDON_SETPROGCOND_PROGRESS_DECIDE_CONTEXT);
    /* Reprogramming request states end */
  }

  if(DCMADDON_SETPROGCOND_PROGRESS_RST_RESP_FLAG == dcmAddOn_SetProgCondContext.progress)
  {
    /* Clear flags states begin */
    uint8 temp = RESET_RESPONSE_NOT_REQUIRED;
    retVal = ApplFblNvWriteAsyncResetResponseFlag(0, &temp, DcmAddOn_ConvertOpStatus(dcmAddOn_SetProgCondContext.opStatus));

    stdReturn = DcmAddOn_SetProgCond_CheckForNextState(retVal, DCMADDON_SETPROGCOND_PROGRESS_APPL_UPTD_FLAG);
  }

  if(DCMADDON_SETPROGCOND_PROGRESS_APPL_UPTD_FLAG == dcmAddOn_SetProgCondContext.progress)
  {
    uint8 temp = kEepApplNotReprogrammed;
    retVal = ApplFblNvWriteAsyncProgMarker(0, &temp, DcmAddOn_ConvertOpStatus(dcmAddOn_SetProgCondContext.opStatus));

    stdReturn = DcmAddOn_SetProgCond_CheckForNextState(retVal, DCMADDON_SETPROGCOND_PROGRESS_DECIDE_CONTEXT);
    /* Clear flag states end */
  }

#if (DCMADDON_TRIGGER_APPL_ON_REQUEST == STD_ON)
  if(stdReturn == DCM_E_OK)
  {
    DcmAddOn_Appl_SetProgConditionsRequested(progConditions);
  }
#endif

  return stdReturn;
}                                                                                                                                                     /* PRQA S 6010 */ /* MD_MSR_STPTH */

/**********************************************************************************************************************
 *  DcmAddOn_GetProgConditions()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(Dcm_EcuStartModeType, DCMADDON_CODE) DcmAddOn_GetProgConditions(Dcm_ProgConditionsPtrType progConditions)
{
  if(DCM_WARM_START == dcmAddOn_GetProgCondContext.ecuStartMode)
  {
    /* -------------------------*/
    /* Read Reset Response Flag */
    /* -------------------------*/
    switch(dcmAddOn_GetProgCondContext.rstRespFlagVal)
    {
      case RESET_RESPONSE_SDS_REQUIRED:
        progConditions->Sid = DCMADDON_SVC_SESSIONCNTRL;
        progConditions->SubFuncId = 0x01U;
        progConditions->ResponseRequired = TRUE;
        break;
      case RESET_RESPONSE_ECURESET_REQUIRED:
        progConditions->Sid = DCMADDON_SVC_ECURESET;
        progConditions->SubFuncId = 0x01U;
        progConditions->ResponseRequired = FALSE;
        break;
      case RESET_RESPONSE_KEYOFFON_REQUIRED:
        progConditions->Sid = DCMADDON_SVC_ECURESET;
        progConditions->SubFuncId = 0x02U;
        progConditions->ResponseRequired = FALSE;
        break;
      default:
        DcmAddOn_Assert((RESET_RESPONSE_NOT_REQUIRED == dcmAddOn_GetProgCondContext.rstRespFlagVal), DCMADDON_DETSID_GETPROGCOND, DCMADDON_E_INTERFACE_RETURN_VALUE)
        progConditions->ResponseRequired = FALSE;
        dcmAddOn_GetProgCondContext.ecuStartMode = DCM_COLD_START;
        break;
    }
  }

  if(DCM_WARM_START == dcmAddOn_GetProgCondContext.ecuStartMode)
  {
    /* --------------------------------------------*/
    /* Read Prog Marker / Application Updated Flag */
    /* --------------------------------------------*/
    if(kEepApplReprogrammed == dcmAddOn_GetProgCondContext.progMarkerValue)
    {
      progConditions->ApplUpdated = TRUE;
    }
    else
    {
      DcmAddOn_Assert((kEepApplNotReprogrammed == dcmAddOn_GetProgCondContext.progMarkerValue), DCMADDON_DETSID_GETPROGCOND, DCMADDON_E_INTERFACE_RETURN_VALUE)
      progConditions->ApplUpdated = FALSE;
    }

    /* ---------------------------*/
    /* Read Tester source address */
    /* ---------------------------*/
    progConditions->TesterSourceAddr = dcmAddOn_GetProgCondContext.testerSourceAddr;
  }

  return dcmAddOn_GetProgCondContext.ecuStartMode;
}

/**********************************************************************************************************************
 *  DcmAddOn_SetProgCond_GetEepValue()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_SetProgCond_GetEepValue(Dcm_ProgConditionsPtrType progConditions,                                 /* PRQA S 3673 */ /* MD_DcmAddOn_Design_3673 */
                                                                            P2VAR(DcmAddOn_ProgReqFlagValueType, AUTOMATIC, AUTOMATIC) eepValue)
{
  Std_ReturnType stdReturn = DCM_E_OK;

  if(   (DCMADDON_SVC_SESSIONCNTRL == progConditions->Sid)
      && (0x02U == progConditions->SubFuncId)
      && (TRUE == progConditions->ReprogrammingRequest)
      && (TRUE == progConditions->ResponseRequired))
  {
    *eepValue = (DcmAddOn_ProgReqFlagValueType)kEepFblReprogram;
  }
  else if(   (DCMADDON_SVC_ECURESET == progConditions->Sid)
          && (0x02U == progConditions->SubFuncId))
  {
    *eepValue = (DcmAddOn_ProgReqFlagValueType)kEepFblVpm;
  }
  else if(   (DCMADDON_SVC_LINKCTRL == progConditions->Sid)
          && (0x01U == progConditions->SubFuncId)
          && (TRUE == progConditions->ReprogrammingRequest)
          && (TRUE == progConditions->ResponseRequired))
  {
    *eepValue = (DcmAddOn_ProgReqFlagValueType)kEepFblLinkControl;
  }
  else
  {
    DcmAddOn_AssertAlways(DCMADDON_DETSID_SETPROGCOND, DCMADDON_E_PARAM_VALUE)
    stdReturn = E_NOT_OK;
  }

  return stdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_SetProgCond_CheckForNextState()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_SetProgCond_CheckForNextState(WrapNv_ReturnType retVal,
                                                                                  DcmAddOn_SetProgCondProgressType nextState)
{
  Std_ReturnType stdReturn;

  if(WRAPNV_E_OK == retVal)
  {
    /* Go to next state */
    dcmAddOn_SetProgCondContext.progress = nextState;
    dcmAddOn_SetProgCondContext.opStatus = DCM_INITIAL;
    stdReturn = DCM_E_OK;
  }
  else if(WRAPNV_E_PENDING == retVal)
  {
    /* Retry later */
    dcmAddOn_SetProgCondContext.opStatus = DCM_PENDING;
    stdReturn = DCM_E_PENDING;
  }
  else
  {
#if !defined (FBL_ENABLE_EEPMGR)
    DcmAddOn_Assert((WRAPNV_E_NOT_OK == retVal), DCMADDON_DETSID_SETPROGCOND, DCMADDON_E_INTERFACE_RETURN_VALUE)
#endif
    /* Something went wrong -> reset state machine */
    dcmAddOn_SetProgCondContext.progress = DCMADDON_SETPROGCOND_PROGRESS_DECIDE_CONTEXT;
    dcmAddOn_SetProgCondContext.opStatus = DCM_INITIAL;
    stdReturn = E_NOT_OK;
  }

  return stdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_ConvertOpStatus()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
static FUNC(tWrapNvOpStatus, DCMADDON_CODE) DcmAddOn_ConvertOpStatus(Dcm_OpStatusType OpStatus)                                                       /* PRQA S 3219 */ /* MD_MSR_14.1 */
{
  tWrapNvOpStatus retVal;

  switch(OpStatus)
  {
    case DCM_INITIAL:
      retVal = WRAPNV_OPSTATUS_INIT;
      break;
    case DCM_PENDING:
      retVal = WRAPNV_OPSTATUS_PENDING;
      break;
    default: /* DCM_CANCEL, DCM_FORCE_RCRRP_OK or DCM_FORCE_RCRRP_NOT_OK */
      /* DCM_FORCE_RCRRP_OK and DCM_FORCE_RCRRP_NOT_OK are invalid since no WrapNv representations exist. */
      DcmAddOn_Assert((OpStatus == DCM_CANCEL), DCMADDON_DETSID_INTERNAL, DCMADDON_E_PARAM_VALUE)
      retVal = WRAPNV_OPSTATUS_CANCEL;
      break;
  }
  return retVal;
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 **********************************************************************************************************************/
/**********************************************************************************************************************
 *  DcmAddOn_InitMemory()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(void, DCMADDON_CODE) DcmAddOn_InitMemory(void)
{
#if(DCMADDON_DEV_ERROR_DETECT == STD_ON)
  dcmAddOn_Initialized = FALSE;
#endif
}

/**********************************************************************************************************************
 *  DcmAddOn_Init()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(void, DCMADDON_CODE) DcmAddOn_Init(void)
{
  dcmAddOn_SetProgCondContext.progress = DCMADDON_SETPROGCOND_PROGRESS_DECIDE_CONTEXT;
  dcmAddOn_SetProgCondContext.opStatus = DCM_INITIAL;

  Vpm_Init();

  dcmAddOn_GetProgCondContext.ecuStartMode = DCM_COLD_START;
  if(WRAPNV_E_OK == ApplFblNvReadResetResponseFlag(0, &dcmAddOn_GetProgCondContext.rstRespFlagVal))
  {
    if(RESET_RESPONSE_NOT_REQUIRED != dcmAddOn_GetProgCondContext.rstRespFlagVal)
    {
      if(WRAPNV_E_OK == ApplFblNvReadProgMarker(0, &dcmAddOn_GetProgCondContext.progMarkerValue))
      {
        if(WRAPNV_E_OK == ApplFblNvReadTesterConnection(0, &dcmAddOn_GetProgCondContext.testerSourceAddr))
        {
          dcmAddOn_GetProgCondContext.ecuStartMode = DCM_WARM_START;
        }
      }
    }
  }

  dcmAddOn_WriteFingerprintContext.writeProtectState = DCMADDON_WR_PROTECT_PASS_THROUGH;
  dcmAddOn_WriteFingerprintContext.fpType = DCMADDON_FP_INVALID;

#if(DCMADDON_DEV_ERROR_DETECT == STD_ON)
  dcmAddOn_Initialized = TRUE;
#endif
}

#if (DCMADDON_VERSION_INFO_API == STD_ON)
/**********************************************************************************************************************
 *  DcmAddOn_GetVersionInfo()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(void, DCMADDON_CODE) DcmAddOn_GetVersionInfo(P2VAR(Std_VersionInfoType, AUTOMATIC, DCMADDON_APPL_DATA) versioninfo)
{
  uint8 lErrorId = DCMADDON_E_NO_ERROR;

# if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  if(NULL_PTR == versioninfo)
  {
    lErrorId = DCMADDON_E_PARAM_POINTER;
  }
  else
# endif
  {
    versioninfo->vendorID         = DCMADDON_VENDOR_ID;
    versioninfo->moduleID         = DCMADDON_MODULE_ID;
    versioninfo->sw_major_version = DCMADDON_SW_MAJOR_VERSION;
    versioninfo->sw_minor_version = DCMADDON_SW_MINOR_VERSION;
    versioninfo->sw_patch_version = DCMADDON_SW_PATCH_VERSION;
  }

# if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_GETVERSIONINFO, lErrorId);
  }
# else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
# endif
}                                                                                                                                                     /* PRQA S 2006 */ /* MD_DcmAddOn_Protect_2006 */
#endif /* (DCMADDON_VERSION_INFO_API == STD_ON) */

/**********************************************************************************************************************
 *  Dcm_WriteMemory()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Dcm_ReturnWriteMemoryType, DCMADDON_CODE) Dcm_WriteMemory(Dcm_OpStatusType OpStatus,
                                                               uint8  MemoryIdentifier,
                                                               uint32 MemoryAddress,
                                                               uint32 MemorySize,
                                                               Dcm_MsgType MemoryData)
{
  Dcm_ReturnWriteMemoryType lRetVal;
  uint8 lErrorId = DCMADDON_E_NO_ERROR;

#if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lRetVal = DCM_WRITE_FAILED;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId = DCMADDON_E_UNINIT;
  }
  else if(NULL_PTR == MemoryData)
  {
    lErrorId = DCMADDON_E_PARAM_POINTER;
  }
  else
#endif
  {
    lRetVal = DCM_WRITE_OK;
    /* Dispatch the MID */
    if(MemoryIdentifier == 0x01U)
    {
      VpmResultType vpmResult = kVpmFailed;
      switch(OpStatus)
      {
        case DCM_INITIAL:
          /* First call -> start the write */
          vpmResult = Vpm_DcmWriteData(MemoryAddress,
                                    MemorySize,
                                    MemoryData);
          break;
        case DCM_PENDING:
          /* Further calls -> poll status */
          vpmResult = Vpm_GetStatus();
          break;
        default: /* DCM_CANCEL, DCM_FORCE_RCRRP_OK or DCM_FORCE_RCRRP_NOT_OK */
          /* DCM_FORCE_RCRRP_OK and DCM_FORCE_RCRRP_NOT_OK are invalid values because forced RCRRP is never requested */
#if (DCMADDON_SUPPORT_CANCELLATION == STD_ON)
          /* DCM_CANCEL -> Abort by Dcm -> do nothing */
          DcmAddOn_Assert((DCM_CANCEL == OpStatus),DCMADDON_DETSID_MEMORYWRITE, DCMADDON_E_PARAM_VALUE)
#else
          DcmAddOn_AssertAlways(DCMADDON_DETSID_MEMORYWRITE, DCMADDON_E_PARAM_VALUE);
#endif
          lRetVal = DCM_WRITE_FAILED;
          break;
      }

      /* Has Dcm aborted the processing? */
      if(DCM_WRITE_OK == lRetVal)
      {
        switch(vpmResult)
        {
          case kVpmOk:
            /* Just let the Dcm send the positive response */
#if (DCMADDON_TRIGGER_APPL_ON_REQUEST == STD_ON)
            DcmAddOn_Appl_WriteMemoryRequested(MemoryIdentifier, MemoryAddress, MemorySize, MemoryData);
#endif
            break;
          case kVpmBusy:
            /* Let the Dcm call this function again for polling the status */
            lRetVal = DCM_WRITE_PENDING;
            break;
          default: /* kVpmFailed */
            DcmAddOn_Assert((kVpmFailed == vpmResult), DCMADDON_DETSID_MEMORYWRITE, DCMADDON_E_INTERFACE_RETURN_VALUE)
            lRetVal = DCM_WRITE_FAILED;
            break;
        }
      } /* else - DCM_WRITE_FAILED -> Dcm has aborted processing */
    }
    else /* MID != 0x01 */
    {
      /* Forward to application */
      lRetVal = DcmAddOn_WriteMemory(OpStatus,
                                     MemoryIdentifier,
                                     MemoryAddress,
                                     MemorySize,
                                     MemoryData);
    }
  }

#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_MEMORYWRITE, lErrorId);
  }
#else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
#endif


  return lRetVal;
}                                                                                                                                                     /* PRQA S 6080, 6030 */ /* MD_MSR_STMIF, MD_MSR_STCYC */

/**********************************************************************************************************************
 *  Dcm_ReadMemory()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Dcm_ReturnReadMemoryType, DCMADDON_CODE) Dcm_ReadMemory(Dcm_OpStatusType OpStatus,
                                                             uint8  MemoryIdentifier,
                                                             uint32 MemoryAddress,
                                                             uint32 MemorySize,
                                                             Dcm_MsgType MemoryData)
{
  Std_ReturnType lStdReturn;
  uint8          lErrorId = DCMADDON_E_NO_ERROR;

#if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lStdReturn = DCM_READ_FAILED;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId = DCMADDON_E_UNINIT;
  }
  else
#endif
  {
    lStdReturn = DcmAddOn_ReadMemory(OpStatus, MemoryIdentifier, MemoryAddress, MemorySize, MemoryData);
  }

#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_MEMORYREAD, lErrorId);
  }
#else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
#endif

  return lStdReturn;
}

/**********************************************************************************************************************
 *  Dcm_SetProgConditions()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Std_ReturnType, DCMADDON_CODE) Dcm_SetProgConditions(Dcm_ProgConditionsPtrType progConditions)
{
  Std_ReturnType lStdReturn;
  uint8          lErrorId = DCMADDON_E_NO_ERROR;

#if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lStdReturn = DCM_E_NOT_OK;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId = DCMADDON_E_UNINIT;
  }
  else if(NULL_PTR == progConditions)
  {
    lErrorId = DCMADDON_E_PARAM_POINTER;
  }
  else if(255 <= progConditions->TesterSourceAddr)
  {
     /* Fbl uses only 8 bit value for tester source address */
    lErrorId = DCMADDON_E_PARAM_VALUE;
  }
  else
#endif
  {
    lStdReturn = DcmAddOn_SetProgConditions(progConditions);
  }

#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_SETPROGCOND, lErrorId);
  }
#else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
#endif

  return lStdReturn;
}

/**********************************************************************************************************************
 *  Dcm_GetProgConditions()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Dcm_EcuStartModeType, DCMADDON_CODE) Dcm_GetProgConditions(Dcm_ProgConditionsPtrType progConditions)
{
  Std_ReturnType lStdReturn;
  uint8          lErrorId = DCMADDON_E_NO_ERROR;

#if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lStdReturn = DCM_E_NOT_OK;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId = DCMADDON_E_UNINIT;
  }
  else if(NULL_PTR == progConditions)
  {
    lErrorId = DCMADDON_E_PARAM_POINTER;
  }
  else
#endif
  {
    lStdReturn = DcmAddOn_GetProgConditions(progConditions);
  }

#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_GETPROGCOND, lErrorId);
  }
#else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
#endif

  return lStdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_Service11_KeyOffOnReset()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Service11_KeyOffOnReset(Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext)
{
  Std_ReturnType lStdReturn;
  uint8          lErrorId = DCMADDON_E_NO_ERROR;

#if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lStdReturn = DCM_E_PROCESSINGDONE;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId = DCMADDON_E_UNINIT;
  }
  else if(pMsgContext == NULL_PTR)
  {
    lErrorId = DCMADDON_E_PARAM_POINTER;
  }
  else if( (opStatus != DCM_INITIAL)
         &&(opStatus != DCM_PENDING)
# if (DCMADDON_SUPPORT_CANCELLATION == STD_ON)
         &&(opStatus != DCM_CANCEL)
# endif
      )
  {
    lErrorId = DCMADDON_E_PARAM_VALUE;
  }
  else
#endif
  {
    if(opStatus == DCM_INITIAL)
    {
      DcmAddOn_RepeaterSetCallee(DcmAddOn_Svc11_RepProxy_SetProgConditions);
    }

    lStdReturn = DcmAddOn_RepeaterExecute(opStatus, pMsgContext);
  }

#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_SVC11PROCESSOR, lErrorId);
  }
#else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
#endif

  return lStdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_Service87_Processor()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Service87_Processor(Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext)
{
  Std_ReturnType lStdReturn;
  uint8          lErrorId = DCMADDON_E_NO_ERROR;

#if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lStdReturn = DCM_E_NOT_OK;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId = DCMADDON_E_UNINIT;
  }
  else if(pMsgContext == NULL_PTR)
  {
    lErrorId = DCMADDON_E_PARAM_POINTER;
  }
  else if( (opStatus != DCM_INITIAL)
         &&(opStatus != DCM_PENDING)
# if (DCMADDON_SUPPORT_CANCELLATION == STD_ON)
         &&(opStatus != DCM_CANCEL)
# endif
         &&(opStatus != DCM_FORCE_RCRRP_OK)
         &&(opStatus != DCM_FORCE_RCRRP_NOT_OK) )
  {
    lErrorId = DCMADDON_E_PARAM_VALUE;
  }
  else
#endif
  {
    if(opStatus == DCM_INITIAL)
    {
      DcmAddOn_RepeaterSetCallee(DcmAddOn_Svc87_RepProxy_RequestEvaluation);
    }

    lStdReturn = DcmAddOn_RepeaterExecute(opStatus, pMsgContext);
  }

#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_SVC87PROCESSOR, lErrorId);
  }
#else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
#endif

  return lStdReturn;
}

#if (DCMADDON_SUPPORT_FINGERPRINTPROTECTION == STD_ON)
/**********************************************************************************************************************
 *  DcmAddOn_WriteProtection_Indication()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_WriteProtection_Indication(uint8 SID,
                                                                        P2CONST(uint8, AUTOMATIC, DCM_VAR_NOINIT) RequestData,
                                                                        uint16 DataSize,
                                                                        uint8 ReqType,
                                                                        uint16 SourceAddress,
                                                                        P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR_NOINIT) ErrorCode)
{
  Std_ReturnType lStdReturn;
  uint8          lErrorId = DCMADDON_E_NO_ERROR;

#if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lStdReturn = DCM_E_NOT_OK;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId = DCMADDON_E_UNINIT;
  }
  else
#endif
  {
    lStdReturn = DCM_E_OK;

    DCMADDON_IGNORE_UNREF_PARAM(ReqType);                                                                                                             /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
    DCMADDON_IGNORE_UNREF_PARAM(SourceAddress);                                                                                                       /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */

    if(DCMADDON_WR_PROTECT_CHECK_SERVICES == dcmAddOn_WriteFingerprintContext.writeProtectState)
    {
      if(DCMADDON_SVC_WRITEDATABYID == SID)
      {
        if( (2 <= DataSize)
          &&(0xF185U != Dcm_UtiMake16Bit(RequestData[0], RequestData[1])) )
        {
          *ErrorCode = DCM_E_SECURITYACCESSDENIED;
          lStdReturn = DCM_E_NOT_OK;
        }
      }
      else if(DCMADDON_SVC_WRITEDATABYADDR == SID)
      {
        *ErrorCode = DCM_E_SECURITYACCESSDENIED;
        lStdReturn = DCM_E_NOT_OK;
      }
      else
      {
        /* Any other service is ok */
      }
    } /* else - either fingerprint already written or no security unlocked */
  }

#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_WRITEPROTECT_IND, lErrorId);
  }
#else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
#endif

  return lStdReturn;
}                                                                                                                                                     /* PRQA S 6060 */ /* MD_MSR_STPAR */
#endif /* DCMADDON_SUPPORT_FINGERPRINTPROTECTION == STD_ON */

#if (DCMADDON_SUPPORT_FINGERPRINTPROTECTION == STD_ON)
/**********************************************************************************************************************
 *  DcmAddOn_WriteProtection_Confirmation()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_WriteProtection_Confirmation(uint8 SID,
                                                                          uint8 ReqType,
                                                                          uint16 SourceAddress,
                                                                          Dcm_ConfirmationStatusType ConfirmationStatus)
{
  Std_ReturnType lStdReturn;
  uint8          lErrorId = DCMADDON_E_NO_ERROR;

#if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lStdReturn = DCM_E_NOT_OK;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId = DCMADDON_E_UNINIT;
  }
  else
#endif
  {
    DCMADDON_IGNORE_UNREF_PARAM(ReqType);                                                                                                             /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
    DCMADDON_IGNORE_UNREF_PARAM(SourceAddress);                                                                                                       /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */

    if(DCM_RES_POS_OK == ConfirmationStatus)
    {
      if((DCMADDON_SVC_WRITEDATABYID == SID)
          && (DCMADDON_FP_APPLICATION_DATA == dcmAddOn_WriteFingerprintContext.fpType))
      {
        dcmAddOn_WriteFingerprintContext.writeProtectState = DCMADDON_WR_PROTECT_PASS_THROUGH;
      } /* else - Any other service request or write DID request except for the application data fingerprint shouldn't have any impact */
    }
    lStdReturn = DCM_E_OK;
  }

#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_WRITEPROTECT_CONF, lErrorId);
  }
#else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
#endif

  return lStdReturn;
}
#endif /* DCMADDON_SUPPORT_FINGERPRINTPROTECTION == STD_ON */

#if (DCMADDON_SUPPORT_FINGERPRINTPROTECTION == STD_ON)
/**********************************************************************************************************************
 *  DcmAddOn_OnSecurityLevelChange()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(void, DCMADDON_CODE) DcmAddOn_OnSecurityLevelChange(Dcm_SecLevelType formerState, Dcm_SecLevelType newState)
{
  uint8 lErrorId = DCMADDON_E_NO_ERROR;

#if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId = DCMADDON_E_UNINIT;
  }
  else
#endif
  {
    DCMADDON_IGNORE_UNREF_PARAM(formerState);                                                                                                         /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
    if(DCM_SEC_LEV_LOCKED != newState)
    {
      dcmAddOn_WriteFingerprintContext.writeProtectState = DCMADDON_WR_PROTECT_CHECK_SERVICES;
      dcmAddOn_WriteFingerprintContext.fpType = DCMADDON_FP_INVALID; /* When writing the fingerprint is required, reset the FP type */
    }
    else
    {
      dcmAddOn_WriteFingerprintContext.writeProtectState = DCMADDON_WR_PROTECT_PASS_THROUGH; /* If locked -> allow writing unprotected data */
    }
  }

#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_ONSECURITYCHANGE, lErrorId);
  }
#else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
#endif
}
#endif /* DCMADDON_SUPPORT_FINGERPRINTPROTECTION == STD_ON */

/**********************************************************************************************************************
 *  DcmAddOn_Data_Application_Software_Fingerprint_ReadDataLength()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Data_Application_Software_Fingerprint_ReadDataLength(Dcm_OpStatusType OpStatus,
                                                                                                  P2VAR(uint16, AUTOMATIC, DCM_VAR_NOINIT) DataLength)
{
  Std_ReturnType lStdReturn;
  uint8          lErrorId = DCMADDON_E_NO_ERROR;

#if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lStdReturn = DCM_E_NOT_OK;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId = DCMADDON_E_UNINIT;
  }
  else
#endif
  {
    lStdReturn = DcmAddOn_ReadFingerprintDataLength(DCMADDON_FP_APPLICATION_SOFTWARE, OpStatus, DataLength);
  }

#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_FP_READDATALENGTH, lErrorId);
  }
#else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
#endif

  return lStdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_Data_Application_Software_Fingerprint_ReadData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Data_Application_Software_Fingerprint_ReadData(Dcm_OpStatusType OpStatus,
                                                                                            P2VAR(uint8, AUTOMATIC, DCM_VAR_NOINIT) Data)
{
  Std_ReturnType lStdReturn;
  uint8          lErrorId = DCMADDON_E_NO_ERROR;

#if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lStdReturn = DCM_E_NOT_OK;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId = DCMADDON_E_UNINIT;
  }
  else
#endif
  {
    lStdReturn = DcmAddOn_ReadFingerprint(DCMADDON_FP_APPLICATION_SOFTWARE, OpStatus, Data);
  }

#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_FP_READDATA, lErrorId);
  }
#else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
#endif

  return lStdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_Data_Application_Software_Fingerprint_WriteData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Data_Application_Software_Fingerprint_WriteData(P2CONST(uint8, AUTOMATIC, DCM_VAR_NOINIT) Data,
                                                                                             uint16 DataLength,
                                                                                             Dcm_OpStatusType OpStatus,
                                                                                             P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR_NOINIT) ErrorCode)
{
  Std_ReturnType lStdReturn;
  uint8          lErrorId = DCMADDON_E_NO_ERROR;

#if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lStdReturn = DCM_E_NOT_OK;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId   = DCMADDON_E_UNINIT;
    *ErrorCode = DCM_E_PANIC_NRC;
  }
  else
#endif
  {
    lStdReturn = DCM_E_OK;
    //lStdReturn = DcmAddOn_WriteFingerprint(DCMADDON_FP_APPLICATION_SOFTWARE, Data, DataLength, OpStatus, ErrorCode);
  }

#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_FP_WRITEDATA, lErrorId);
  }
#else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
#endif

  return lStdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_Data_Application_Data_Fingerprint_ReadDataLength()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Data_Application_Data_Fingerprint_ReadDataLength(Dcm_OpStatusType OpStatus,
                                                                                              P2VAR(uint16, AUTOMATIC, DCM_VAR_NOINIT) DataLength)
{
  Std_ReturnType lStdReturn;
  uint8          lErrorId = DCMADDON_E_NO_ERROR;

#if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lStdReturn = DCM_E_NOT_OK;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId = DCMADDON_E_UNINIT;
  }
  else
#endif
  {
    lStdReturn = DcmAddOn_ReadFingerprintDataLength(DCMADDON_FP_APPLICATION_DATA, OpStatus, DataLength);
  }

#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_FP_READDATALENGTH, lErrorId);
  }
#else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
#endif

  return lStdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_Data_Application_Data_Fingerprint_ReadData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Data_Application_Data_Fingerprint_ReadData(Dcm_OpStatusType OpStatus,
                                                                                        P2VAR(uint8, AUTOMATIC, DCM_VAR_NOINIT) Data)
{
  Std_ReturnType lStdReturn;
  uint8          lErrorId = DCMADDON_E_NO_ERROR;

#if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lStdReturn = DCM_E_NOT_OK;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId = DCMADDON_E_UNINIT;
  }
  else
#endif
  {
    lStdReturn = DcmAddOn_ReadFingerprint(DCMADDON_FP_APPLICATION_DATA, OpStatus, Data);
  }

#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_FP_READDATA, lErrorId);
  }
#else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
#endif

  return lStdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_Data_Application_Data_Fingerprint_WriteData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Data_Application_Data_Fingerprint_WriteData(P2CONST(uint8, AUTOMATIC, DCM_VAR_NOINIT) Data,
                                                                                         uint16 DataLength,
                                                                                         Dcm_OpStatusType OpStatus,
                                                                                         P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR_NOINIT) ErrorCode)
{
  Std_ReturnType lStdReturn;
  uint8          lErrorId = DCMADDON_E_NO_ERROR;

#if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lStdReturn = DCM_E_NOT_OK;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId   = DCMADDON_E_UNINIT;
    *ErrorCode = DCM_E_PANIC_NRC;
  }
  else
#endif
  {
    lStdReturn = DCM_E_OK;
    //lStdReturn = DcmAddOn_WriteFingerprint(DCMADDON_FP_APPLICATION_DATA, Data, DataLength, OpStatus, ErrorCode);
  }

#if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_FP_WRITEDATA, lErrorId);
  }
#else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
#endif

  return lStdReturn;
}

#if (DCMADDON_SUPPORT_BOOT_SW_FP == STD_ON)
/**********************************************************************************************************************
 *  DcmAddOn_Data_Boot_Software_Fingerprint_ReadDataLength()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Data_Boot_Software_Fingerprint_ReadDataLength(Dcm_OpStatusType OpStatus,
                                                                                           P2VAR(uint16, AUTOMATIC, DCM_VAR_NOINIT) DataLength)
{
  Std_ReturnType lStdReturn;
  uint8          lErrorId = DCMADDON_E_NO_ERROR;

# if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lStdReturn = DCM_E_NOT_OK;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId = DCMADDON_E_UNINIT;
  }
  else
# endif
  {
    lStdReturn = DcmAddOn_ReadFingerprintDataLength(DCMADDON_FP_BOOT_SOFTWARE, OpStatus, DataLength);
  }

# if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_FP_READDATALENGTH, lErrorId);
  }
# else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
# endif

  return lStdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_Data_Boot_Software_Fingerprint_ReadData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Data_Boot_Software_Fingerprint_ReadData(Dcm_OpStatusType OpStatus,
                                                                                     P2VAR(uint8, AUTOMATIC, DCM_VAR_NOINIT) Data)
{
  Std_ReturnType lStdReturn;
  uint8          lErrorId = DCMADDON_E_NO_ERROR;

# if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lStdReturn = DCM_E_NOT_OK;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId = DCMADDON_E_UNINIT;
  }
  else
# endif
  {
    lStdReturn = DcmAddOn_ReadFingerprint(DCMADDON_FP_BOOT_SOFTWARE, OpStatus, Data);
  }

# if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_FP_READDATA, lErrorId);
  }
# else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
# endif

  return lStdReturn;
}

/**********************************************************************************************************************
 *  DcmAddOn_Data_Boot_Software_Fingerprint_WriteData()
 *********************************************************************************************************************/
/*!
 * Internal comment removed.
 *
 *
 *********************************************************************************************************************/
FUNC(Std_ReturnType, DCMADDON_CODE) DcmAddOn_Data_Boot_Software_Fingerprint_WriteData(P2CONST(uint8, AUTOMATIC, DCM_VAR_NOINIT) Data,
                                                                                      uint16 DataLength,
                                                                                      Dcm_OpStatusType OpStatus,
                                                                                      P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, DCM_VAR_NOINIT) ErrorCode)
{
  Std_ReturnType lStdReturn;
  uint8          lErrorId = DCMADDON_E_NO_ERROR;

# if (DCMADDON_DEV_ERROR_DETECT == STD_ON)
  lStdReturn = DCM_E_NOT_OK;

  if(FALSE == dcmAddOn_Initialized)
  {
    lErrorId   = DCMADDON_E_UNINIT;
    *ErrorCode = DCM_E_PANIC_NRC;
  }
  else
# endif
  {
    lStdReturn = DCM_E_OK;
    //lStdReturn = DcmAddOn_WriteFingerprint(DCMADDON_FP_BOOT_SOFTWARE, Data, DataLength, OpStatus, ErrorCode);
  }

# if (DCMADDON_DEV_ERROR_REPORT == STD_ON)
  if (lErrorId != DCMADDON_E_NO_ERROR)
  {
    DcmAddOn_CallDetReportError(DCMADDON_DETSID_FP_WRITEDATA, lErrorId);
  }
# else
  DCMADDON_IGNORE_UNREF_PARAM(lErrorId);                                                                                                              /* PRQA S 3112 */ /* MD_DcmAddOn_3112 */
# endif

  return lStdReturn;
}
#endif

#define DCMADDON_STOP_SEC_CODE
#include "MemMap.h"                                                                                                                                   /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*
  MD_DcmAddOn_BitNegation_0291:
     Description: Rule 21.1
                  Minimisation of run-time failures shall be ensured by the use of at least one of (a) static analysis
                  tools/techniques; (b) dynamic analysis tools/techniques; (c) explicit coding of checks to handle
                  run-time faults.
     Reason:      False positive due to insufficient data flow analysis. Despite thorough review no defect could be
                  recognized.
     Risk:        None.
     Prevention:  None.

   MD_DcmAddOn_Protect_2006:
     Description: Rule 14.7
                  A function shall have a single point of exit at the end of the function.
     Reason:      For safety reason failed assertions leave the function immediately. Disabled assertions shall
                  not affect the code execution, so functions are not modified to take control flow into account that
                  originates from assertions.
     Risk:        None.
     Prevention:  None.

  MD_DcmAddOn_3112:
     Description: Rule 14.2
                  All non-null statements shall either (i) have at least one side-effect however executed, or
                  (ii) cause control flow to change.
     Reason:      This statement is used to avoid warnings caused by unused parameters. Parameters are defined by
                  standardized API requirements, and not needed in case a feature is disabled by configuration.
                  It is either impossible due to API standardization or necessary code duplication (severe maintenance
                  increase) to provide feature dependent APIs with different signatures.
     Risk:        Unavoidable compiler warning or error because of either unused statements or unused parameter.
     Prevention:  None.

  MD_DcmAddOn_UnreachableStmt_3201:
     Description: Rule 14.1
                  There shall be no unreachable code.
     Reason:      Since the actual usage of the software component by the customer is not known some allowed return
                  values of provided interfaces may not be used.
     Risk:        Unavoidable compiler warning or error because of either unused statements or unused parameter.
     Prevention:  None.

  MD_DcmAddOn_CodingRule_3218:
     Description: Rule 8.7
                  Objects shall be defined at block scope if they are only accessed from within a single function.
     Reason:      Vector style guide prevents usage of static variables/constant objects in function scope.
     Risk:        None.
     Prevention:  None.

  MD_DcmAddOn_ConstantCondition_3335:
     Description: Rule 14.1
                  The result of this logical operation is always constant.
     Reason:      Since the actual usage of the software component by the customer is not known some allowed return
                  values of provided interfaces may not be used which result into constant conditional expressions.
     Risk:        Unavoidable compiler warning or error because of either unused statements or unused parameter.
     Prevention:  None.

  MD_DcmAddOn_Design_3673:
     Description: Rule 16.7
                  A pointer parameter in a function prototype should be declared as pointer to const if the pointer is
                  not used to modify the addressed object.
     Reason:      The API has to be compatible to a common prototype, defined by module's design to serve generic purposes.
     Risk:        None.
     Prevention:  None.

  MD_DcmAddOn_Design_0311:
     Description: Rule 11.5
                  A cast shall not be performed that removes any const or volatile qualification from 
                  the type addressed by a pointer. 
     Reason:      A function call has to be compatible to a common prototype, defined by someelse's module design.
     Risk:        The called API can change the data addressed by the pointer.
     Prevention:  None.
*/

/**********************************************************************************************************************
 *  END OF FILE: DcmAddOn.c
 *********************************************************************************************************************/
