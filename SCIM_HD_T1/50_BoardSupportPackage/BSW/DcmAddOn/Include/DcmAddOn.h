/* ********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                             All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  DcmAddOn.h
 *      Project:  MICROSAR Complex Device Driver (CDD)
 *       Module:  -
 *    Generator:  -
 *
 *  Description:  Header file of the complex device driver component DcmAddOn.
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  -------------------------------------------------------------------------------------------------------------------
 *  Patrick Rieder                visrpk        Vector Informatik GmbH
 *  Vitalij Krieger               visvkr        Vector Informatik GmbH
 *  Erik Jeglorz                  visejz        Vector Informatik GmbH
 *  Amr Hegazi                    visahe        Vector Informatik GmbH
 *  -------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  01.00.00  2014-08-26  visrpk  -             Initial creation
 *  02.00.00  2015-01-08  visrpk  ESCAN00080244 FEAT-473: Release of Dcm and Dem for OEM VAB
 *  02.01.00  2016-02-22  visvkr  ESCAN00087565 FEAT-1808: Adaption of VPM, RomTest and DcmAddOn to support Fee in addition to EepM
 *                        visvkr  ESCAN00082332 Compiler error: UInt8 is an unknown identifier
 *  03.00.00  2016-09-07  visvkr  FEATC-284     FEAT-2132: DCM VAB Addon: Change from Template to Beta Status
 *                        visvkr  ESCAN00091785 Service 0x87: NRC 0x31 instead of NRC 0x13
 *                        visvkr  ESCAN00091784 Service 0x2E: Random NRC responded
 *                        visvkr  ESCAN00091786 DET report error: Wrong API-ID reported for Dcm_GetProgConditions()
 *                        visvkr  ESCAN00091788 Service 0x22: Any subsequent non-volatile memory related service request is delayed
 *                        visvkr  ESCAN00091789 Service 0x2E: Any subsequent non-volatile memory related service request is delayed
 *                        visvkr  ESCAN00091793 Service 0x11: ECU can not perform the requested reset type
 *                        visvkr  ESCAN00091795 Service 0x87: ECU can not perform the requested reset type
 *  04.00.00  2017-09-22  visejz  STORYC-147    Update of DcmAddOn on current process/quality level
 *  04.01.00  2018-08-06  visejz  STORYC-5732   Rework DcmAddOn for "Make Writing fingerprints optional"
 *  04.01.01  2019-02-07  visahe  ESCAN00101840 Compiler error: Unknown identifier Dcm_OptimizedSetNegResponse
 *********************************************************************************************************************/

#if !defined (DCMADDON_H)
# define DCMADDON_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
# include "Std_Types.h"
# include "Dcm.h"
# include "DcmAddOn_Cfg.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
/* ##V_CFG_MANAGEMENT ##CQProject : Cdd_Asr4DcmAddOn CQComponent : Implementation */
/* Version - BCD coded version number - main- and sub-version - release-version */
# define CDD_ASR4DCMADDON_VERSION                                    (0x0401U)
# define CDD_ASR4DCMADDON_RELEASE_VERSION                            (0x01U)

/* Vendor and module identification - decimal encoding */
# define DCMADDON_VENDOR_ID                                          (30U)
# define DCMADDON_MODULE_ID                                          (255U)

/* AUTOSAR Software specification version information - decimal encoding */
# define DCMADDON_AR_RELEASE_MAJOR_VERSION                           (4U)
# define DCMADDON_AR_RELEASE_MINOR_VERSION                           (0U)
# define DCMADDON_AR_RELEASE_REVISION_VERSION                        (3U)

/* Component version information */
# define DCMADDON_SW_MAJOR_VERSION                                   (CDD_ASR4DCMADDON_VERSION >> (0x08U))
# define DCMADDON_SW_MINOR_VERSION                                   (CDD_ASR4DCMADDON_VERSION & (0x00FFU))
# define DCMADDON_SW_PATCH_VERSION                                   (CDD_ASR4DCMADDON_RELEASE_VERSION)

/* API IDs */
# define DCMADDON_DETSID_INIT                                        (0x00U)
# define DCMADDON_DETSID_GETVERSIONINFO                              (0x01U)
# define DCMADDON_DETSID_MEMORYWRITE                                 (0x20U)
# define DCMADDON_DETSID_MEMORYREAD                                  (0x21U)
# define DCMADDON_DETSID_SETPROGCOND                                 (0x30U)
# define DCMADDON_DETSID_GETPROGCOND                                 (0x31U)
# define DCMADDON_DETSID_SVC87PROCESSOR                              (0x40U)
# define DCMADDON_DETSID_SVC11PROCESSOR                              (0x41U)
# define DCMADDON_DETSID_FP_CONDCHECKREAD                            (0x42U)
# define DCMADDON_DETSID_FP_READDATA                                 (0x43U)
# define DCMADDON_DETSID_FP_READDATALENGTH                           (0x44U)
# define DCMADDON_DETSID_FP_WRITEDATA                                (0x45U)
# define DCMADDON_DETSID_WRITEPROTECT_IND                            (0x60U)
# define DCMADDON_DETSID_WRITEPROTECT_CONF                           (0x61U)
# define DCMADDON_DETSID_ONSECURITYCHANGE                            (0x62U)
# define DCMADDON_DETSID_INTERNAL                                    (0xF0U)

/* Error codes */
# define DCMADDON_E_NO_ERROR                                         (0x00U)
# define DCMADDON_E_UNINIT                                           (0x01U)
# define DCMADDON_E_PARAM_POINTER                                    (0x02U)
# define DCMADDON_E_PARAM_VALUE                                      (0x03U)
# define DCMADDON_E_INTERFACE_RETURN_VALUE                           (0x04U)
# define DCMADDON_E_INVALID_STATE                                    (0x05U)
# define DCMADDON_E_UNSUPPORTED_FINGERPRINT                          (0x06U)


/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/
# define DCMADDON_IGNORE_UNREF_PARAM(param)                          ((void)(param))                                                                  /* PRQA S 3453 */ /* MD_MSR_19.7 */

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
/* Fingerprint types */
# define DCMADDON_FINGERPRINT_END                                    3
typedef enum
{
  DCMADDON_FP_BOOT_SOFTWARE = 0,
  DCMADDON_FP_APPLICATION_SOFTWARE = 1,
  DCMADDON_FP_APPLICATION_DATA = 2,
  DCMADDON_FP_INVALID = DCMADDON_FINGERPRINT_END
} DcmAddOn_FingerprintType;

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define DCMADDON_START_SEC_CODE
# include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Service functions */
/**********************************************************************************************************************
 *  DcmAddOn_Init()
 *********************************************************************************************************************/
/*! \brief          Initialization function
 *  \details        Function to initialize the component and the VPM
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \pre            Global interrupts shall be disabled.
 *********************************************************************************************************************/
FUNC(void, DCMADDON_CODE) DcmAddOn_Init(void);

/**********************************************************************************************************************
 *  DcmAddOn_InitMemory()
 *********************************************************************************************************************/
/*! \brief          Initialization function
 *  \details        Module's memory initialization
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \pre            Global interrupts shall be disabled. If used, then always prior calling DcmAddOn_Init.
 *********************************************************************************************************************/
FUNC(void, DCMADDON_CODE) DcmAddOn_InitMemory(void);

# if (DCMADDON_VERSION_INFO_API == STD_ON)
/**********************************************************************************************************************
 *  DcmAddOn_GetVersionInfo()
 *********************************************************************************************************************/
/*! \brief          Reports component's version information.
 *  \details        Returns the version information of the used DcmAddOn implementation.
 *  \param[out]     versioninfo    pointer to the application structure
 *  \context        ISR1|ISR2|TASK
 *  \reentrant      TRUE
 *  \synchronous    TRUE
 *  \config         This function is only available if DCMADDON_VERSION_INFO_API = STD_ON.
 *  \pre            -
 *********************************************************************************************************************/
FUNC(void, DCMADDON_CODE) DcmAddOn_GetVersionInfo(P2VAR(Std_VersionInfoType, AUTOMATIC, DCMADDON_APPL_DATA) versioninfo);
# endif

# define DCMADDON_STOP_SEC_CODE
# include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_19.1 */

# define DCMADDON_START_SEC_CALLOUT_CODE
# include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_19.1 */
/* Callback function declarations */
/**********************************************************************************************************************
  DcmAddOn_WriteMemory()
 **********************************************************************************************************************/
/*! \brief          Call-out to write to memory.
 *  \details        The DcmAddOn_WriteMemory() is used to write memory to a specific memory location.
 *                  This function is called after dispatching of the MemoryIdentifier.
 *  \param[in]      OpStatus                Operation status
 *  \param[in]      MemoryIdentifier        Identifier of the memory block. The value will be always unequal to one
 *                                          because write requests with memory identifier equal to one are handled
 *                                          internally.
 *  \param[in]      MemoryAddress           Starting address of the memory the data should be written to
 *  \param[in]      MemorySize              The amount of data to be written
 *  \param[in]      MemoryData              Pointer to the data which should be written
 *  \return         DCM_WRITE_OK            Write was successful
 *  \return         DCM_WRITE_FAILED        Write was not successful
 *  \return         DCM_WRITE_PENDING       Write is not yet finished
 *  \return         DCM_WRITE_FORCE_RCRRP   Enforce RCR-RP transmission (vendor extension)
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    FALSE
 *  \pre            -
 *********************************************************************************************************************/
FUNC(Dcm_ReturnWriteMemoryType, DCMADDON_CALLOUT_CODE) DcmAddOn_WriteMemory(Dcm_OpStatusType OpStatus,
                                                                            uint8  MemoryIdentifier,
                                                                            uint32 MemoryAddress,
                                                                            uint32 MemorySize,
                                                                            Dcm_MsgType MemoryData);

/**********************************************************************************************************************
 *  DcmAddOn_ReadMemory()
 *********************************************************************************************************************/
/*! \brief          Call-out to read from memory.
 *  \details        The DcmAddOn_ReadMemory() is used to read data from a specific memory location.
 *                  The DcmAddOn module only forwards the call of the DCM without any functionality.
 *  \param[in]      OpStatus                Operation status
 *  \param[in]      MemoryIdentifier        Identifier of the Memory Block
 *  \param[in]      MemoryAddress           Starting address of the memory the data should be read from
 *  \param[in]      MemorySize              The amount of data to be read
 *  \param[in]      MemoryData              Pointer to a buffer to store the read information
 *  \return         DCM_READ_OK             Read was successful
 *  \return         DCM_READ_FAILED         Read was not successful
 *  \return         DCM_READ_PENDING        Read is not yet finished
 *  \return         DCM_READ_FORCE_RCRRP    Enforce RCR-RP transmission (vendor extension)
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    FALSE
 *  \pre            -
 *********************************************************************************************************************/
FUNC(Dcm_ReturnReadMemoryType, DCMADDON_CALLOUT_CODE) DcmAddOn_ReadMemory(Dcm_OpStatusType OpStatus,
                                                                          uint8  MemoryIdentifier,
                                                                          uint32 MemoryAddress,
                                                                          uint32 MemorySize,
                                                                          Dcm_MsgType MemoryData);

#if (DCMADDON_TRIGGER_APPL_ON_REQUEST == STD_ON)
/**********************************************************************************************************************
 *  DcmAddOn_Appl_WriteMemoryRequested()
 *********************************************************************************************************************/
/*! \brief          Call-out to inform an application that a write to memory request has been executed successfully.
 *  \details        If write to memory request with Memory identifier 0x01 is finished the request is forwarded 
 *                  to the application.
 *  \param[in]      MemoryIdentifier        Identifier of the Memory Block, value is always 0x01
 *  \param[in]      MemoryAddress           Starting address of the memory the data should be read from
 *  \param[in]      MemorySize              The amount of data to be read
 *  \param[in]      MemoryData              Pointer to a buffer to store the read information
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \config         DCMADDON_TRIGGER_APPL_ON_REQUEST == STD_ON
 *  \pre            -
 *********************************************************************************************************************/
FUNC(void, DCMADDON_CALLOUT_CODE) DcmAddOn_Appl_WriteMemoryRequested(uint8  MemoryIdentifier,
                                                                    uint32 MemoryAddress,
                                                                    uint32 MemorySize,
                                                                    Dcm_ReadOnlyMsgType MemoryData);
#endif

#if (DCMADDON_TRIGGER_APPL_ON_REQUEST == STD_ON)
/**********************************************************************************************************************
 *  DcmAddOn_Appl_SetProgConditionsRequested()
 *********************************************************************************************************************/
/*! \brief          Call-out to inform an application that a set programming condition request has been 
 *                  executed successfully.
 *  \details        If the set programming condition request is finished the request is forwarded to the application.
 *  \param[in]      progConditions    Conditions on which the jump to boot loader has been requested
 *  \context        TASK
 *  \reentrant      FALSE
 *  \synchronous    TRUE
 *  \config         DCMADDON_TRIGGER_APPL_ON_REQUEST == STD_ON
 *  \pre            -
 *********************************************************************************************************************/
FUNC(void, DCMADDON_CALLOUT_CODE) DcmAddOn_Appl_SetProgConditionsRequested(
                                           P2CONST(Dcm_ProgConditionsType, AUTOMATIC, DCM_VAR_NOINIT) progConditions);

#endif

# define DCMADDON_STOP_SEC_CALLOUT_CODE
# include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_19.1 */


#endif /* DCMADDON_H */

/**********************************************************************************************************************
 *  END OF FILE: DcmAddOn.h
 *********************************************************************************************************************/
