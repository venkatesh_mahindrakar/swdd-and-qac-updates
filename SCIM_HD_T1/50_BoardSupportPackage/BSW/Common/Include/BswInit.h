/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                            All rights reserved.
 *              This software is copyright protected and proprietary to Vector Informatik GmbH.
 *              Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *              All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  EXAMPLE CODE ONLY
 *  -------------------------------------------------------------------------------------------------------------------
 *              This Example Code is only intended for illustrating an example of a possible BSW integration and BSW
 *              configuration. The Example Code has not passed any quality control measures and may be incomplete. The
 *              Example Code is neither intended nor qualified for use in series production. The Example Code as well
 *              as any of its modifications and/or implementations must be tested with diligent care and must comply
 *              with all quality requirements which are necessary according to the state of the art before their use.
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: BswInit.h
 *       Description: BswInit API
 *********************************************************************************************************************/

#ifdef BSWINIT_H
#error BSWINIT_H unexpected multi-inclusion
#else
#define BSWINIT_H
/* PRQA S 0777 EOF */ /* MD_MSR_5.1_777 */

//=====================================================================================
// Included header files
//=====================================================================================
#include"CanTp.h"
#include"J1939Tp.h"
#include"PduR.h"
#include"CanSM.h"
#include"CanNM.h"
#include"LpuRAMCode.h"


//=======================================================================================
// Public function prototypes
//=======================================================================================
int maincore1(void);


//=======================================================================================
// GLOBAL FUNCTION PROTOTYPES
//=======================================================================================


void BswInit_NvMReadAll(void);

//=====================================================================================
// End of file
//=====================================================================================
#endif /* __BSWINIT_H__ */

