/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2016 by Vector Informatik GmbH.                                              All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  Vpm.h
 *        \brief  Variant Parameter Manager - Client side
 *
 *      \details  Configuration file for Variant Parameter Manager
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *********************************************************************************************************************/

#ifndef __VPM_CFG_H__
#define __VPM_CFG_H__

/***********************************************************************************************************************
 *  DEFINES
 **********************************************************************************************************************/

/** Size of non-volatile memory block used to store VPM patches (including address and length meta data - 6 bytes total)
    A single VPM request may be split into multiple patches when it doesn't fit into a single NV-block */
#define VPM_NV_BLOCK_SIZE         30u
/** Maximum number of VPM patches which can be stored in non-volatile memory */
#define VPM_PATCH_COUNT           8u
/** Value reported by non-volatile memory read function when patch count entry doesn't exist yet */
#define VPM_PATCH_COUNT_UNINIT    0xFFFFu

#endif /* __VPM_CFG_H__ */

/***********************************************************************************************************************
 *  END OF FILE: VPM_CFG.H
 **********************************************************************************************************************/

