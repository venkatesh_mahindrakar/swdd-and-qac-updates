/* ********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                             All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  DcmAddOn_Cfg.h
 *      Project:  MICROSAR Complex Device Driver (CDD)
 *       Module:  -
 *    Generator:  -
 *
 *  Description:  Contains the configuration of the component DcmAddOn.
 *********************************************************************************************************************/

#if !defined (DCMADDON_CFG_H)
# define DCMADDON_CFG_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Configuration NOT allowed to be changed by the user
 **********************************************************************************************************************/
/* AUTOSAR Software Specification Version Information of BSW-IMPLEMENTATION */
# define DCMADDON_CFG_MAJOR_VERSION                                  (0x04U)
# define DCMADDON_CFG_MINOR_VERSION                                  (0x01U)


/**********************************************************************************************************************
 *  Configuration allowed to be changed by the user
 **********************************************************************************************************************/
/* Switches - Value Range: STD_ON or STD_OFF */
/* Access to version information */
# define DCMADDON_VERSION_INFO_API                                   STD_ON
/* Development error detection */
# define DCMADDON_DEV_ERROR_DETECT                                   STD_ON
/* Development error detection and reporting */
# define DCMADDON_DEV_ERROR_REPORT                                   STD_ON
/* Additional internal checks for debug purposes */
# define DCMADDON_DEBUG_ERROR_DETECT                                 STD_OFF

/* Support cancellation from Dcm */
# define DCMADDON_SUPPORT_CANCELLATION                               STD_ON
/* Callouts to inform application on VPM operations */
# define DCMADDON_TRIGGER_APPL_ON_REQUEST                            STD_OFF
/* Require Fingerprint before accessing UDS Service 0x3D und 0x2E for secured data */
# define DCMADDON_SUPPORT_FINGERPRINTPROTECTION                      STD_OFF

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  Configuration allowed to be changed by the user
 **********************************************************************************************************************/
# define DCMADDON_FINGERPRINT_HISTORY_SIZE                           5


/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

#endif /* DCMADDON_CFG_H */
/**********************************************************************************************************************
 *  END OF FILE: DcmAddOn_Cfg.h
 *********************************************************************************************************************/
