/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  EXAMPLE CODE ONLY
 *  -------------------------------------------------------------------------------------------------------------------
 *              This Example Code is only intended for illustrating an example of a possible BSW integration and BSW
 *              configuration. The Example Code has not passed any quality control measures and may be incomplete. The
 *              Example Code is neither intended nor qualified for use in series production. The Example Code as well
 *              as any of its modifications and/or implementations must be tested with diligent care and must comply
 *              with all quality requirements which are necessary according to the state of the art before their use.
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: BswInit.c
 *       Description: Provides the implementation of the main function and unimplemented tasks
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "BswInit.h"
#include "EcuM.h"
#include "Mpc5746c.h"
#include "BrsHw.h"
#include "BswInit_Callout_stubs.h"
#include "NvM.h"
#include "Dio.h"



/**********************************************************************************************************************
 *  ADDITIONAL USER INCLUDES
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK User Includes>
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK User Includes end block>
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  VERSION CHECK
 **********************************************************************************************************************/
#if ((BSWINIT_CALLOUT_STUBS_MAJOR_VERSION != 1U) || (BSWINIT_CALLOUT_STUBS_MINOR_VERSION != 0U))
# error "Vendor specific version numbers of BswInit_Callout_Stubs.h and BswInit.c are inconsistent"
#endif

/**********************************************************************************************************************
 *  FUNCTIONS
 **********************************************************************************************************************/

extern  unsigned long __MY_RAM_SIZE;
extern  unsigned long __RTE_VAR_MEM;
extern  unsigned long __RTE_VAR_SIZE;
extern  unsigned long __RTECOPY_START;
extern  unsigned long __RTECOPY_END;



/***********************************************************************************************************************
 *  main
 **********************************************************************************************************************/
extern  uint32 __LPU_RAMCOPY_CODE_;

const  uint32 *const LPU_RAMCOPY_CODE=(uint32 *)&__LPU_RAMCOPY_CODE_;
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the main
//!
//! \return int returns the integer value
//!
//!======================================================================================
int main(void)
{
	#if 1
   //! ##### Initialization of MSCR voltages 
   SIUL2.MSCR[GPIO_12V_DCDC_EN].R      = 0x02000000;  // 12V DCDC Enable
   SIUL2.MSCR[GPIO_12V_LIVING_OUT].R   = 0x02000000;  // Living ctrl
   SIUL2.MSCR[GPIO_12V_PARKED_OUT].R   = 0x02000000;  // Parked ctrl

   //! ##### Initialization of GPDO voltages 
   SIUL2.GPDO[GPIO_12V_DCDC_EN].B.PDO_4n     = GpioCtrl[DCDC_EN];  // 12V DCDC Enable
   SIUL2.GPDO[GPIO_12V_LIVING_OUT].B.PDO_4n  = GpioCtrl[LIVING_OUT]; // Living Ctrl
   SIUL2.GPDO[GPIO_12V_PARKED_OUT].B.PDO_4n  = GpioCtrl[PARKED_OUT]; // Parked Ctrl
    PMCDIG.RDCR.B.PAD_KEEP_EN = 0x0;
	#endif
	//! ##### Processing to disable the internal watchdog: 'DisableInternalWatchdog()'
   DisableInternalWatchdog();
   Rte_MemCpy(0x40000000 , LPU_RAMCOPY_CODE, &__MY_RAM_SIZE); // actual size of LPU code for SRAM0
   //Rte_MemCpy(0x40000000 , 0x011FC000, 0x2000); // actual size of LPU code for SRAM0
   //Rte_MemCpy(0x40000000 , 0x00FA4000, &__MY_RAM_SIZE); // will be used later(after C2 truck - 20/Jun/2020)
   //! ##### Process the initializtion functions after start up 
	//! ##### Process BswInit_PreInitPowerOn :'BswInit_PreInitPowerOn()'
   BswInit_PreInitPowerOn();
	//! ##### Process BswInit_InitializeOne :'BswInit_InitializeOne()'
   BswInit_InitializeOne();
	//! ##### Process BswInit_InitializeTwo :'BswInit_InitializeTwo()'
   BswInit_InitializeTwo();
	//! ##### Process EcuM_Init :'EcuM_Init()'
   EcuM_Init(); /* never returns */

   return 0;
}

/***********************************************************************************************************************
 *  IdleTask_OsCore0
 **********************************************************************************************************************/
TASK(IdleTask_OsCore0)
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK IdleTask_OsCore0 begin block>
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>
 *********************************************************************************************************************/

  for(;;)
  {
    (void)Schedule();
  }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK IdleTask_OsCore0 end block>
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>
 *********************************************************************************************************************/

}
/***********************************************************************************************************************
 *  Default_Init_Task
 **********************************************************************************************************************/
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the inital Task
//!
//!======================================================================================
TASK(Init_Task)
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK Default_Init_Task begin block>
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>
 *********************************************************************************************************************/
  //! ##### Process EcuM_StartupTwo :'EcuM_StartupTwo()'
  EcuM_StartupTwo();
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK Default_Init_Task end block>
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>
 *********************************************************************************************************************/
	//! ##### Process TerminateTask :'TerminateTask()'
  (void)TerminateTask();
}
/***********************************************************************************************************************
 *  BswInit_NvMReadAll
 **********************************************************************************************************************/

//!======================================================================================
//!
//! \brief
//! This function implements the logic for the BswInit_NvMReadAll
//!
//!======================================================================================
void BswInit_NvMReadAll(void)
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK BswInit_NvMReadAll begin block>
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>
 *********************************************************************************************************************/
   NvM_RequestResultType Int_NvMultiBlockStatus;
	//! ##### Process NvM_ReadAll :'NvM_ReadAll()'
   NvM_ReadAll();
	//! ##### Initialise the functions while the Int_NvMultiBlockStatus Request is pending 
   do
   {
      //! #### Process the Fls main function : 'Fls_MainFunction()'
      Fls_MainFunction();
		//! #### Process the Fee main function : 'Fee_MainFunction()'
      Fee_MainFunction();
		//! #### Process the NvM main function : 'NvM_MainFunction()'
      NvM_MainFunction();
		//! #### Process the NvM_GetErrorStatus for Multi block request and status : 'NvM_GetErrorStatus()'
      NvM_GetErrorStatus(NvMConf___MultiBlockRequest, &Int_NvMultiBlockStatus);
   }
   while (Int_NvMultiBlockStatus == NVM_REQ_PENDING);
   //! ##### Process the Dio_FlipChannel logic : 'Dio_FlipChannel()'
   Dio_FlipChannel(DioConf_DioChannel_ExWdg_WDI);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK BswInit_NvMReadAll end block>
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>
 *********************************************************************************************************************/

}
uint32 corecounter;
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the maincore1 
//!
//!======================================================================================
int maincore1(void)
{
   //! ##### Incrementing core counter
   uint32 i,j = 0U;
   //SIUL2.MSCR[2].R = 0x02000000;	 OBE set 
   //asm("e_bl _Core1start");

   while(1)
   {
      corecounter++;

      for(i = 0; i < 400000; i++)
      {

      }

      j=!j;
      //SIUL2.MSCR[5].R = 0x02000000;

      /* toggle with LED1 to indicate wakeup to SRAM */
      //SIUL2.GPDO[5].R = j;  LED1 ON 
   }
}

/**********************************************************************************************************************
 *  END OF FILE: BswInit.c
 *********************************************************************************************************************/