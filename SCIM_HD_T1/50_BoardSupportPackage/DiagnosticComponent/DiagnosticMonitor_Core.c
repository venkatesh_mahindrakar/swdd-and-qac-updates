/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  DiagnosticMonitor_Core.c
 *        Config:  D:/SCIM_GIT/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  DiagnosticMonitor_Core
 *  Generated at:  Thu Jul  9 14:07:26 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <DiagnosticMonitor_Core>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
//!======================================================================================
//! \file DiagnosticMonitor_Core.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup Platform 
//! @{
//! @addtogroup Platform_SCIM 
//! @{
//! @addtogroup DiagnosticComponent
//! @{
//! @addtogroup DiagnosticMonitor_Core
//! @{
//!
//! \brief
//! DiagnosticMonitor_Core SWC.
//! ASIL Level : QM.
//! This module implements the logic for the DiagnosticMonitor_Core runnables.
//!======================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_UdsStatusByteType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_DiagnosticMonitor_Core.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "Mcu.h"
//#include "mpc5746c.h"
#include "StdRegMacros.h"
#include "CoreTests_If.h"
#include "ResetProcess_If.h"
#include "DiagnosticMonitor_Core.h"
static uint8                 SCIM_RamTestStart      = 0U;
static RamTst_TestResultType SCIM_RamTestResults    = RAMTST_RESULT_NOT_TESTED;
static RtmCpuLoadDataType    SCIM_CpuLoad;
static CoreStackUsage_T      SCIM_StackUsage;
static uint8                 SCIM_StackUsage_start  = 0U;
static ResetCausetypes       IrvResetCause          = ResetCause_None;
static uint8                 InternalEcuFailurFlag  = 0;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Rte_DT_EngTraceHWData_T_0: Integer in interval [0...255]
 * Rte_DT_EngTraceHWData_T_10: Integer in interval [0...255]
 * Rte_DT_EngTraceHWData_T_11: Integer in interval [0...255]
 * Rte_DT_EngTraceHWData_T_2: Integer in interval [0...255]
 * Rte_DT_EngTraceHWData_T_3: Integer in interval [0...255]
 * Rte_DT_EngTraceHWData_T_4: Integer in interval [0...255]
 * Rte_DT_EngTraceHWData_T_8: Integer in interval [0...65535]
 * Rte_DT_EngTraceHWData_T_9: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * Dem_UdsStatusByteType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_UDS_STATUS_TF (1U)
 *   DEM_UDS_STATUS_TF_BflMask 1U (0b00000001)
 *   DEM_UDS_STATUS_TF_BflPn 0
 *   DEM_UDS_STATUS_TF_BflLn 1
 *   DEM_UDS_STATUS_TFTOC (2U)
 *   DEM_UDS_STATUS_TFTOC_BflMask 2U (0b00000010)
 *   DEM_UDS_STATUS_TFTOC_BflPn 1
 *   DEM_UDS_STATUS_TFTOC_BflLn 1
 *   DEM_UDS_STATUS_PDTC (4U)
 *   DEM_UDS_STATUS_PDTC_BflMask 4U (0b00000100)
 *   DEM_UDS_STATUS_PDTC_BflPn 2
 *   DEM_UDS_STATUS_PDTC_BflLn 1
 *   DEM_UDS_STATUS_CDTC (8U)
 *   DEM_UDS_STATUS_CDTC_BflMask 8U (0b00001000)
 *   DEM_UDS_STATUS_CDTC_BflPn 3
 *   DEM_UDS_STATUS_CDTC_BflLn 1
 *   DEM_UDS_STATUS_TNCSLC (16U)
 *   DEM_UDS_STATUS_TNCSLC_BflMask 16U (0b00010000)
 *   DEM_UDS_STATUS_TNCSLC_BflPn 4
 *   DEM_UDS_STATUS_TNCSLC_BflLn 1
 *   DEM_UDS_STATUS_TFSLC (32U)
 *   DEM_UDS_STATUS_TFSLC_BflMask 32U (0b00100000)
 *   DEM_UDS_STATUS_TFSLC_BflPn 5
 *   DEM_UDS_STATUS_TFSLC_BflLn 1
 *   DEM_UDS_STATUS_TNCTOC (64U)
 *   DEM_UDS_STATUS_TNCTOC_BflMask 64U (0b01000000)
 *   DEM_UDS_STATUS_TNCTOC_BflPn 6
 *   DEM_UDS_STATUS_TNCTOC_BflLn 1
 *   DEM_UDS_STATUS_WIR (128U)
 *   DEM_UDS_STATUS_WIR_BflMask 128U (0b10000000)
 *   DEM_UDS_STATUS_WIR_BflPn 7
 *   DEM_UDS_STATUS_WIR_BflLn 1
 *
 * Array Types:
 * ============
 * Dcm_Data126ByteType: Array with 126 element(s) of type uint8
 * Dcm_Data130ByteType: Array with 130 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 * Dcm_Data60ByteType: Array with 60 element(s) of type uint8
 * Dcm_Data80ByteType: Array with 80 element(s) of type uint8
 * EngTraceHWArray: Array with 10 element(s) of type EngTraceHWData_T
 *
 * Record Types:
 * =============
 * EngTraceHWData_T: Record with elements
 *   Reset_Type of type Rte_DT_EngTraceHWData_T_0
 *   Timelog of type uint32
 *   TaskID of type Rte_DT_EngTraceHWData_T_2
 *   ServiceID of type Rte_DT_EngTraceHWData_T_3
 *   RunnableID of type Rte_DT_EngTraceHWData_T_4
 *   MemoryAddress of type uint32
 *   MC_RGM_FES_Reg of type uint32
 *   MC_RGM_DES_Reg of type uint32
 *   ModuleID of type Rte_DT_EngTraceHWData_T_8
 *   InstanceID of type Rte_DT_EngTraceHWData_T_9
 *   APIID of type Rte_DT_EngTraceHWData_T_10
 *   ErrorID of type Rte_DT_EngTraceHWData_T_11
 *
 *********************************************************************************************************************/


#define DiagnosticMonitor_Core_START_SEC_CODE
#include "DiagnosticMonitor_Core_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1QXM_Data_P1QXM_ECUMemoryFailure_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1QXM_Data_P1QXM_ECUMemoryFailure>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1QXM_Data_P1QXM_ECUMemoryFailure_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1QXM_Data_P1QXM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1QXM_Data_P1QXM_ECUMemoryFailure_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1QXM_Data_P1QXM_ECUMemoryFailure_ReadData
//! 
//! \param  *Data          
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_P1QXM_Data_P1QXM_ECUMemoryFailure_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1QXM_Data_P1QXM_ECUMemoryFailure_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1QXP_Data_P1QXP_ECUSupervisionData_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1QXP_Data_P1QXP_ECUSupervisionData>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1QXP_Data_P1QXP_ECUSupervisionData_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1QXP_Data_P1QXP_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1QXP_Data_P1QXP_ECUSupervisionData_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1QXP_Data_P1QXP_ECUSupervisionData_ReadData
//! 
//! \param  *Data          
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_P1QXP_Data_P1QXP_ECUSupervisionData_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1QXP_Data_P1QXP_ECUSupervisionData_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1QXR_Data_P1QXR_ECUInternalFailureExtendedData_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1QXR_Data_P1QXR_ECUInternalFailureExtendedData>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(EngTraceHWData_T *data)
 *     Argument data: EngTraceHWData_T* is of type EngTraceHWArray
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1QXR_Data_P1QXR_ECUInternalFailureExtendedData_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1QXR_Data_P1QXR_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1QXR_Data_P1QXR_ECUInternalFailureExtendedData_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1QXR_Data_P1QXR_ECUInternalFailureExtendedData_ReadData
//! 
//! \param  *Data          
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_P1QXR_Data_P1QXR_ECUInternalFailureExtendedData_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1QXR_Data_P1QXR_ECUInternalFailureExtendedData_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(EngTraceHWData_T *data)
 *     Argument data: EngTraceHWData_T* is of type EngTraceHWArray
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data80ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1C12_Data_X1C12_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_ReadData
//! 
//! \param  *Data          To fill in data of Engineering trace 
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8           i                    = 0;
   uint8           Index                = 0;
   EngTraceHWArray EngTraceHWDataBuffer = {{0,0,0,0,0,0,0,0,0,0,0,0},
                                          {0,0,0,0,0,0,0,0,0,0,0,0},
                                          {0,0,0,0,0,0,0,0,0,0,0,0},
                                          {0,0,0,0,0,0,0,0,0,0,0,0},
                                          {0,0,0,0,0,0,0,0,0,0,0,0},
                                          {0,0,0,0,0,0,0,0,0,0,0,0},
                                          {0,0,0,0,0,0,0,0,0,0,0,0},
                                          {0,0,0,0,0,0,0,0,0,0,0,0},
                                          {0,0,0,0,0,0,0,0,0,0,0,0},
                                          {0,0,0,0,0,0,0,0,0,0,0,0}};
   //! ###### Processing the Data services for Monitoring Reset log 
   (void)Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(&EngTraceHWDataBuffer[0]);
   //! ##### Reading data from Engineering trace Hardware buffer
   for(i=0;i<10;i++)
   {
      Index       = i*8;  //13 bytes of data structure
      Data[Index] = i+1;
      Index++;
      Data[Index] = EngTraceHWDataBuffer[i].TaskID ;   
      Index++;
      Data[Index] = EngTraceHWDataBuffer[i].ServiceID;
      Index++;
      Data[Index] = EngTraceHWDataBuffer[i].RunnableID; 
      Index++;
      Data[Index] = (uint8)(EngTraceHWDataBuffer[i].MemoryAddress &(0x000000FFU));
      Index++;
      Data[Index] = (uint8)((EngTraceHWDataBuffer[i].MemoryAddress &(0xFF000000U))>>24U);    
      Index++;
      Data[Index] = (uint8)((EngTraceHWDataBuffer[i].MemoryAddress &(0x00FF0000U))>>16);
      Index++;
      Data[Index] = (uint8)((EngTraceHWDataBuffer[i].MemoryAddress &(0x0000FF00U))>>8);     
      Index++;   
   } 
   return RTE_E_OK;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_WriteData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <WriteData> of PortPrototype <DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_EngTraceHW_NvM_I_EngTraceHW_NvM(const EngTraceHWData_T *data)
 *     Argument data: EngTraceHWData_T* is of type EngTraceHWArray
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_WriteData(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data80ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1C12_Data_X1C12_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_WriteData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_WriteData
//! 
//! \param  *Data          
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_WriteData(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_WriteData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(EngTraceHWData_T *data)
 *     Argument data: EngTraceHWData_T* is of type EngTraceHWArray
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data130ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1C13_Data_X1C13_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_ReadData
//! 
//! \param  *Data          
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8           i                    = 0;
   uint8           Index                = 0;
   EngTraceHWArray EngTraceHWDataBuffer = {{0,0,0,0,0,0,0,0,0,0,0,0},
                                           {0,0,0,0,0,0,0,0,0,0,0,0},
                                           {0,0,0,0,0,0,0,0,0,0,0,0},
                                           {0,0,0,0,0,0,0,0,0,0,0,0},
                                           {0,0,0,0,0,0,0,0,0,0,0,0},
                                           {0,0,0,0,0,0,0,0,0,0,0,0},
                                           {0,0,0,0,0,0,0,0,0,0,0,0},
                                           {0,0,0,0,0,0,0,0,0,0,0,0},
                                           {0,0,0,0,0,0,0,0,0,0,0,0},
                                           {0,0,0,0,0,0,0,0,0,0,0,0}};
   //! ###### Processing the Data services for Monitoring Read data of Reset log
   (void)Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(&EngTraceHWDataBuffer[0]);
   //! ##### Reading data from Engineering trace Hardware buffer
   for (i=0;i<10;i++)
   {
      Index       = i*13;  //13 bytes of data structure
      Data[Index] = EngTraceHWDataBuffer[i].Reset_Type&(0x0f0U);
      Index++;
      Data[Index] = (uint8)((EngTraceHWDataBuffer[i].MC_RGM_DES_Reg &(0xFF000000U))>>24U);   
      Index++;
      Data[Index] = (uint8)((EngTraceHWDataBuffer[i].MC_RGM_DES_Reg &(0x00FF0000U))>>16);
      Index++;
      Data[Index] = (uint8)((EngTraceHWDataBuffer[i].MC_RGM_DES_Reg &(0x0000FF00U))>>8); 
      Index++;
      Data[Index] = (uint8)(EngTraceHWDataBuffer[i].MC_RGM_DES_Reg &(0x000000FFU));
      Index++;
      Data[Index] = (uint8)((EngTraceHWDataBuffer[i].MC_RGM_FES_Reg &(0xFF000000U))>>24U);    
      Index++;
      Data[Index] = (uint8)((EngTraceHWDataBuffer[i].MC_RGM_FES_Reg &(0x00FF0000U))>>16);
      Index++;
      Data[Index] = (uint8)((EngTraceHWDataBuffer[i].MC_RGM_FES_Reg &(0x0000FF00U))>>8); 
      Index++;
      Data[Index] = (uint8)(EngTraceHWDataBuffer[i].MC_RGM_FES_Reg &(0x000000FFU));
      Index++;
   } 
   return RTE_E_OK;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_WriteData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <WriteData> of PortPrototype <DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_EngTraceHW_NvM_I_EngTraceHW_NvM(const EngTraceHWData_T *data)
 *     Argument data: EngTraceHWData_T* is of type EngTraceHWArray
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_WriteData(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data130ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1C13_Data_X1C13_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_WriteData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_WriteData
//! 
//! \param  *Data          
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_WriteData(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_WriteData (returns application error)
 *********************************************************************************************************************/
	uint8 i=0;
	uint8 Index=0;
	EngTraceHWArray EngTraceHWDataBuffer={{0,0,0,0,0,0,0,0,0,0,0,0},
										 {0,0,0,0,0,0,0,0,0,0,0,0},
										 {0,0,0,0,0,0,0,0,0,0,0,0},
										 {0,0,0,0,0,0,0,0,0,0,0,0},
										 {0,0,0,0,0,0,0,0,0,0,0,0},
										 {0,0,0,0,0,0,0,0,0,0,0,0},
										 {0,0,0,0,0,0,0,0,0,0,0,0},
										 {0,0,0,0,0,0,0,0,0,0,0,0},
										 {0,0,0,0,0,0,0,0,0,0,0,0},
										 {0,0,0,0,0,0,0,0,0,0,0,0}};

  //! ###### Processing the Data services for Monitoring write data of Reset log
  //! ##### Writing data into Engineering trace Hardware buffer
  for(i=0;i<10;i++)
  {
      Index = i*13;  //13 bytes of data structure
      EngTraceHWDataBuffer[i].Reset_Type=Data[Index];
      Index++;
      EngTraceHWDataBuffer[i].MC_RGM_DES_Reg = (uint32)Data[Index];
      Index += 4;
      EngTraceHWDataBuffer[i].MC_RGM_FES_Reg = (uint32)Data[Index]; 	

  } 
  (void)Rte_Write_EngTraceHW_NvM_I_EngTraceHW_NvM(&EngTraceHWDataBuffer[0]);

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data126ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1C1U_Data_X1C1U_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_ReadData
//! 
//! \param  *Data          
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_WriteData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <WriteData> of PortPrototype <DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_WriteData(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data126ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1C1U_Data_X1C1U_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_WriteData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_WriteData
//! 
//! \param  *Data   
//! \param  *ErrorCode                 
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_WriteData(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_WriteData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1C1Z_Data_X1C1Z_CoreDiag_ResetLog_BSW_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_X1C1Z_Data_X1C1Z_CoreDiag_ResetLog_BSW>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(EngTraceHWData_T *data)
 *     Argument data: EngTraceHWData_T* is of type EngTraceHWArray
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1C1Z_Data_X1C1Z_CoreDiag_ResetLog_BSW_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data60ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1C1Z_Data_X1C1Z_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1C1Z_Data_X1C1Z_CoreDiag_ResetLog_BSW_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_X1C1Z_Data_X1C1Z_CoreDiag_ResetLog_BSW_ReadData
//! 
//! \param  *Data          
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C1Z_Data_X1C1Z_CoreDiag_ResetLog_BSW_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1C1Z_Data_X1C1Z_CoreDiag_ResetLog_BSW_ReadData (returns application error)
 *********************************************************************************************************************/
   uint8           i                     = 0;
   uint8           Index                 = 0;
   EngTraceHWArray EngTraceHWDataBuffer  = {{0,0,0,0,0,0,0,0,0,0,0,0},
                                            {0,0,0,0,0,0,0,0,0,0,0,0},
                                            {0,0,0,0,0,0,0,0,0,0,0,0},
                                            {0,0,0,0,0,0,0,0,0,0,0,0},
                                            {0,0,0,0,0,0,0,0,0,0,0,0},
                                            {0,0,0,0,0,0,0,0,0,0,0,0},
                                            {0,0,0,0,0,0,0,0,0,0,0,0},
                                            {0,0,0,0,0,0,0,0,0,0,0,0},
                                            {0,0,0,0,0,0,0,0,0,0,0,0},
                                            {0,0,0,0,0,0,0,0,0,0,0,0}};
   //! ###### Processing the Data services for Read data of BSW Reset log
   (void)Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(&EngTraceHWDataBuffer[0]);
   //! ##### Reading data from Engineering trace Hardware buffer
   for(i=0;i<10;i++)
   {
      Index       = i*8;  //13 bytes of data structure
      Data[Index] = i+1;
      Index++;
      Data[Index] = (uint8)((EngTraceHWDataBuffer[i].ModuleID&0xFF00U)>>8);
      Index++;
      Data[Index] = (uint8)((EngTraceHWDataBuffer[i].ModuleID&0x00FFU));
      Index++;
      Data[Index] = EngTraceHWDataBuffer[i].InstanceID;
      Index++;
      Data[Index] = EngTraceHWDataBuffer[i].ServiceID; 
      Index++;
      Data[Index] = EngTraceHWDataBuffer[i].APIID;
      Index++;
      Data[Index] = EngTraceHWDataBuffer[i].ErrorID;    
      Index++;   
   }  
   return RTE_E_OK;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1CYG_Data_X1CYG_SwExecProfile_FirstFaults_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * This runnable is never executed by the RTE.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CYG_Data_X1CYG_SwExecProfile_FirstFaults_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_X1CYG_Data_X1CYG_SwExecProfile_FirstFaults_ReadData
//!        
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DiagnosticMonitor_Core_CODE) DataServices_X1CYG_Data_X1CYG_SwExecProfile_FirstFaults_ReadData(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CYG_Data_X1CYG_SwExecProfile_FirstFaults_ReadData
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1CYG_Data_X1CYG_SwExecProfile_FirstFaults_WriteData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * This runnable is never executed by the RTE.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CYG_Data_X1CYG_SwExecProfile_FirstFaults_WriteData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_X1CYG_Data_X1CYG_SwExecProfile_FirstFaults_WriteData
//!        
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DiagnosticMonitor_Core_CODE) DataServices_X1CYG_Data_X1CYG_SwExecProfile_FirstFaults_WriteData(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CYG_Data_X1CYG_SwExecProfile_FirstFaults_WriteData
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1CYH_Data_X1CYH_SwExecProfile_LastFaults_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * This runnable is never executed by the RTE.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CYH_Data_X1CYH_SwExecProfile_LastFaults_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_X1CYH_Data_X1CYH_SwExecProfile_LastFaults_ReadData
//! 
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DiagnosticMonitor_Core_CODE) DataServices_X1CYH_Data_X1CYH_SwExecProfile_LastFaults_ReadData(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CYH_Data_X1CYH_SwExecProfile_LastFaults_ReadData
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1CYH_Data_X1CYH_SwExecProfile_LastFaults_WriteData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * This runnable is never executed by the RTE.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CYH_Data_X1CYH_SwExecProfile_LastFaults_WriteData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_X1CYH_Data_X1CYH_SwExecProfile_LastFaults_WriteData
//!         
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DiagnosticMonitor_Core_CODE) DataServices_X1CYH_Data_X1CYH_SwExecProfile_LastFaults_WriteData(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CYH_Data_X1CYH_SwExecProfile_LastFaults_WriteData
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DiagnosticMonitor_Core_10ms_Runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1AD0_41_Core_GeneralChecksumFailure_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_41_Core_GeneralChecksumFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_41_Core_GeneralChecksumFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_44_Core_RamFailure_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_44_Core_RamFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_44_Core_RamFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_45_Core_ProgramMemoryFailure_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_45_Core_ProgramMemoryFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_45_Core_ProgramMemoryFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_46_Core_NvramFailure_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_46_Core_NvramFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_46_Core_NvramFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_47_Core_WatchdogFailure_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_47_Core_WatchdogFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_47_Core_WatchdogFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_48_Core_SupervisionFailure_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_48_Core_SupervisionFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_48_Core_SupervisionFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_94_Core_UnexpectedOperation_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_94_Core_UnexpectedOperation_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_94_Core_UnexpectedOperation_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DiagnosticMonitor_Core_10ms_Runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution of runnable logic for the DiagnosticMonitor_Core_10ms_Runnable
//! 
//! \param  *Data          
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DiagnosticMonitor_Core_CODE) DiagnosticMonitor_Core_10ms_Runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DiagnosticMonitor_Core_10ms_Runnable
 *********************************************************************************************************************/
   boolean InternalEcuFailuerStatus = 0;
   //! ##### Checking the reset cause and accordingly setting the DEM_Event to failed or passed
   if (0U == InternalEcuFailurFlag)
   {
		//! ##### Checking for the reset cause
      if ((ResetCause_VoltReg_LVD==IrvResetCause)
         ||(ResetCause_VoltReg_HVD==IrvResetCause)
         ||(ResetCause_Clock_Failure==IrvResetCause))
      { 
			//! #### Calling for the failed event:'Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_GetEventFailed()' 
         Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_GetEventFailed(&InternalEcuFailuerStatus);
			//! #### Checking the type of failure and accordingly setting the DEM event staus
         if (1 == InternalEcuFailuerStatus)
         {
            IrvResetCause         = ResetCause_None;
            InternalEcuFailurFlag = 1;
         }
         else
         {
            Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_SetEventStatus(DEM_EVENT_STATUS_FAILED);
         }
      }
      else
      {
         Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_GetEventFailed(&InternalEcuFailuerStatus);
         if (0 == InternalEcuFailuerStatus)
         {
            InternalEcuFailurFlag = 2;
         }
         else
         {
            Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_SetEventStatus(DEM_EVENT_STATUS_PASSED);
         }
      }
   }
   else
   {
      // Do nothing
   }   
   //! ##### Processing to read CPU Load  :'CoreTest_GetCpuLoad()'
   (void)CoreTest_GetCpuLoad(&SCIM_CpuLoad);   
   //! ##### Processing to Read Stack Usage :'CoreTest_GetStackUsage()'
   if (SCIM_StackUsage_start == 1U)
   {
      CoreTest_GetStackUsage(&SCIM_StackUsage);
   }
   else
   {
      // Do nothing
   }
   //! ##### Checking if RAM Test started
   if(SCIM_RamTestStart == 1u)
   {
      SCIM_RamTestStart = 0;
      //! #### Processing Ram Test :'CoreTest_RAMTestStart()'
      (void)CoreTest_RAMTestStart();
      //! #### Processing Ram Test results :'CoreTest_GetRAMTestResults()'
      (void)CoreTest_GetRAMTestResults(&SCIM_RamTestResults);
   }
   else
   {
      // Do nothing
   }  
   //! ##### Processing CPU Load measurement initialisation :'CoreTest_CpuLoadInit()'
   //! ##### Memory Test Period Runnable
   (void)MemoryFaultHandler_10ms_Runnable();  
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DiagnosticMonitor_Core_Init_Runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(EngTraceHWData_T *data)
 *     Argument data: EngTraceHWData_T* is of type EngTraceHWArray
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_EngTraceHW_NvM_I_EngTraceHW_NvM(const EngTraceHWData_T *data)
 *     Argument data: EngTraceHWData_T* is of type EngTraceHWArray
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DiagnosticMonitor_Core_Init_Runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the runnable logic for the DiagnosticMonitor_Core_Init_Runnable
//!    
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DiagnosticMonitor_Core_CODE) DiagnosticMonitor_Core_Init_Runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DiagnosticMonitor_Core_Init_Runnable
 *********************************************************************************************************************/
	InternalEcuFailurFlag=0;
   //! ##### Processing MemoryFaultHandler initialisation :'MemoryFaultHandler_Init()'
   (void)MemoryFaultHandler_Init();
	IrvResetCause = ResetProcess_Init();
   //! ##### Processing CPU Load measurement initialisation :'CoreTest_CpuLoadInit()'
	CoreTest_CpuLoadInit();	

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define DiagnosticMonitor_Core_STOP_SEC_CODE
#include "DiagnosticMonitor_Core_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DiagnosticMonitor_Core_MCU_ErrorNotification
//! 
//! \param  uint8 u8ErrorCode          
//! 
//!====================================================================================================================
void DiagnosticMonitor_Core_MCU_ErrorNotification(uint8 u8ErrorCode)
{
   //! ##### If Mcu_Isr_PLL clock fails then trigger reset
   if (MCU_E_ISR_PLL_LOCK_FAILURE == u8ErrorCode)
   {
		//! #### Processing Trigger reset in Reset process :'ResetProcess_TriggerReset()'
      ResetProcess_TriggerReset(ResetCause_PLL_Error);
   }
   else
   {
      // Do nothing
   }
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
