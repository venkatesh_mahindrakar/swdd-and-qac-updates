/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  DiagnosticComponent.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  DiagnosticComponent
 *  Generated at:  Fri Jun 12 17:30:27 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <DiagnosticComponent>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
//!======================================================================================
//! \file DiagnosticComponent.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup Platform 
//! @{
//! @addtogroup Platform_SCIM 
//! @{
//! @addtogroup DiagnosticComponent
//! @{
//! @addtogroup DiagnosticComponent
//! @{
//!
//! \brief
//! DiagnosticComponent SWC.
//! ASIL Level : QM.
//! This module implements the logic for the DiagnosticComponent runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Days8bit_Fact025_T
 *   254 Error ; 255 Not available
 *
 * Dcm_ConfirmationStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_OperationCycleStateType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Hours8bit_T
 *   254 Error ; 255 Not available
 *
 * Minutes8bit_T
 *   254 Error ; 255 Not available
 *
 * Months8bit_T
 *   254 Error ; 255 Not available
 *
 * Seconds8bitFact025_T
 *   254 Error ; 255 Not available
 *
 * Years8bit_T
 *   254 Error ; 255 Not available
 *
 *********************************************************************************************************************/

#include "Rte_DiagnosticComponent.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "FuncLibrary_ScimStd_If.h"
#include "DiagnosticComponent.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Days8bit_Fact025_T: Integer in interval [0...255]
 *   Unit: [Days], Factor: 1, Offset: 0
 * Hours8bit_T: Integer in interval [0...255]
 *   Unit: [h], Factor: 1, Offset: 0
 * Minutes8bit_T: Integer in interval [0...255]
 *   Unit: [min], Factor: 1, Offset: 0
 * Months8bit_T: Integer in interval [0...255]
 *   Unit: [Months], Factor: 1, Offset: 0
 * Seconds8bitFact025_T: Integer in interval [0...255]
 *   Unit: [s], Factor: 1, Offset: 0
 * Years8bit_T: Integer in interval [0...255]
 *   Unit: [Years], Factor: 1, Offset: 1985
 * uint16: Integer in interval [0...65535] (standard type)
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_ConfirmationStatusType: Enumeration of integer in interval [0...3] with enumerators
 *   DCM_RES_POS_OK (0U)
 *   DCM_RES_POS_NOT_OK (1U)
 *   DCM_RES_NEG_OK (2U)
 *   DCM_RES_NEG_NOT_OK (3U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dem_OperationCycleStateType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_CYCLE_STATE_START (0U)
 *   DEM_CYCLE_STATE_END (1U)
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * DataArrayType_uint8_1: Array with 1 element(s) of type uint8
 * Dcm_Data2043ByteType: Array with 2043 element(s) of type uint8
 *
 *********************************************************************************************************************/


#define DiagnosticComponent_START_SEC_CODE
#include "DiagnosticComponent_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBReadData_UTCTimeStamp_First_Day_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <CBReadData_UTCTimeStamp_First_Day>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   Days8bit_Fact025_T Rte_IrvRead_CBReadData_UTCTimeStamp_First_Day_ReadData_IrvDayUTC(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBReadData_UTCTimeStamp_First_Day_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CSDataServices_UTCTimeStamp_First_Day_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_First_Day_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the Read UTCTimestamp FirstDay Data runnable logic for the 'CBReadData_UTCTimeStamp_First_Day_ReadData'
//!
//!
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Day_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_First_Day_ReadData (returns application error)
 *********************************************************************************************************************/   
   Days8bit_Fact025_T TStampFirstDay;
   Std_ReturnType     Ret            = RTE_E_OK;
   
   //! ###### Reading the first day value
   TStampFirstDay = Rte_IrvRead_CBReadData_UTCTimeStamp_First_Day_ReadData_IrvDayUTC();
   //! ##### Check if day values are within range then read otherwise report error
   if (MAXLIMIT_DAYS_UTC > TStampFirstDay)
   {
     Data[0] = (uint8) TStampFirstDay;
   }
   else
   {
     Data[0] = 0U;
     Ret     = RTE_E_CSDataServices_UTCTimeStamp_First_Day_E_NOT_OK;
   }   
   return Ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBReadData_UTCTimeStamp_First_Hour_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <CBReadData_UTCTimeStamp_First_Hour>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   Hours8bit_T Rte_IrvRead_CBReadData_UTCTimeStamp_First_Hour_ReadData_IrvHoursUTC(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBReadData_UTCTimeStamp_First_Hour_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CSDataServices_UTCTimeStamp_First_Hour_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_First_Hour_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the Read UTCTimestamp FirstDay Data runnable logic for the 'CBReadData_UTCTimeStamp_First_Day_ReadData' 
//!
//!
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Hour_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_First_Hour_ReadData (returns application error)
 *********************************************************************************************************************/      
   Hours8bit_T    TStampFirstHour;
   Std_ReturnType Ret             = RTE_E_OK;

   //! ###### Reading the first hour value
   TStampFirstHour = Rte_IrvRead_CBReadData_UTCTimeStamp_First_Hour_ReadData_IrvHoursUTC();   
    //! ##### Check if hour values are within range then read otherwise report error
   if (MAXLIMIT_HOURS_UTC > TStampFirstHour)
   {
      Data[0] =(uint8) TStampFirstHour;
   }
   else
   {
      Data[0] = 0U;
      Ret     = RTE_E_CSDataServices_UTCTimeStamp_First_Hour_E_NOT_OK;
   }  
   return Ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBReadData_UTCTimeStamp_First_Minutes_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <CBReadData_UTCTimeStamp_First_Minutes>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   Minutes8bit_T Rte_IrvRead_CBReadData_UTCTimeStamp_First_Minutes_ReadData_IrvMinutesUTC(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBReadData_UTCTimeStamp_First_Minutes_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CSDataServices_UTCTimeStamp_First_Minutes_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_First_Minutes_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//!
//! \brief
//! This function implements the Read UTCTimestamp FirstMinutes Data runnable logic for the 'CBReadData_UTCTimeStamp_First_Minutes_ReadData'
//!
//!
//!==================================================================================================================== 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Minutes_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_First_Minutes_ReadData (returns application error)
 *********************************************************************************************************************/
   Minutes8bit_T  TStampFirstMinutes;
   Std_ReturnType Ret                = RTE_E_OK;
   
   //!###### Reading the first minute value
   TStampFirstMinutes = Rte_IrvRead_CBReadData_UTCTimeStamp_First_Minutes_ReadData_IrvMinutesUTC();   
    //! ##### Check if minutes values are within range then read otherwise report error
   if (MAXLIMIT_MINUTES_UTC > TStampFirstMinutes)
   {
     Data[0] = (uint8) TStampFirstMinutes;
   }
   else
   {
     Data[0] = 0U;
     Ret     = RTE_E_CSDataServices_UTCTimeStamp_First_Minutes_E_NOT_OK;
   }
   return Ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBReadData_UTCTimeStamp_First_Month_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <CBReadData_UTCTimeStamp_First_Month>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   Months8bit_T Rte_IrvRead_CBReadData_UTCTimeStamp_First_Month_ReadData_IrvMonthUTC(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBReadData_UTCTimeStamp_First_Month_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CSDataServices_UTCTimeStamp_First_Month_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_First_Month_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the Read UTCTimestamp FirstMonth Data runnable logic for  the 'CBReadData_UTCTimeStamp_First_Month_ReadData'
//!
//!
//!==================================================================================================================== 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Month_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_First_Month_ReadData (returns application error)
 *********************************************************************************************************************/
   Months8bit_T   TStampFirstMonth;
   Std_ReturnType Ret              = RTE_E_OK;

   //! ###### Reading the first month value
   TStampFirstMonth   = Rte_IrvRead_CBReadData_UTCTimeStamp_First_Month_ReadData_IrvMonthUTC();
   //! ##### Check if month values are within range then read otherwise report error
   if (MAXLIMIT_MONTHS_UTC > TStampFirstMonth)
   {
     Data[0] = (uint8) TStampFirstMonth;
   }
   else
   {
     Data[0] = 0U;
     Ret     = RTE_E_CSDataServices_UTCTimeStamp_First_Month_E_NOT_OK;
   }
   return Ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBReadData_UTCTimeStamp_First_Seconds_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <CBReadData_UTCTimeStamp_First_Seconds>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   Seconds8bitFact025_T Rte_IrvRead_CBReadData_UTCTimeStamp_First_Seconds_ReadData_IrvSecondsUTC(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBReadData_UTCTimeStamp_First_Seconds_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CSDataServices_UTCTimeStamp_First_Seconds_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_First_Seconds_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the Read UTCTimestamp FirstSeconds Data runnable logic for the 'CBReadData_UTCTimeStamp_First_Seconds_ReadData' 
//!'
//!
//!==================================================================================================================== 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Seconds_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_First_Seconds_ReadData (returns application error)
 *********************************************************************************************************************/   
    Seconds8bitFact025_T TStampFirstSecond;
    Std_ReturnType       Ret                = RTE_E_OK;

   //! ###### Reading the first second value
   TStampFirstSecond  = Rte_IrvRead_CBReadData_UTCTimeStamp_First_Seconds_ReadData_IrvSecondsUTC();   
    //! ##### Check if second value is within range then read otherwise report error   
   if (MAXLIMIT_SECONDS_UTC > TStampFirstSecond)
   {
     Data[0] = (uint8) TStampFirstSecond;
   }
   else
   {
     Data[0] = 0U;
     Ret     = RTE_E_CSDataServices_UTCTimeStamp_First_Seconds_E_NOT_OK;
   }
   return Ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBReadData_UTCTimeStamp_First_Year_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <CBReadData_UTCTimeStamp_First_Year>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   Years8bit_T Rte_IrvRead_CBReadData_UTCTimeStamp_First_Year_ReadData_IrvYearUTC(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBReadData_UTCTimeStamp_First_Year_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CSDataServices_UTCTimeStamp_First_Year_E_NOT_OK
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_First_Year_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the Read UTCTimestamp FirstYear Data runnable logic for  the 'CBReadData_UTCTimeStamp_First_Year_ReadData'
//!
//!
//!==================================================================================================================== 

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Year_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_First_Year_ReadData (returns application error)
 *********************************************************************************************************************/
   Years8bit_T     TStampFirstYear;
   Std_ReturnType  Ret              = RTE_E_OK;
   
   //! ###### Reading the first year value
   TStampFirstYear = Rte_IrvRead_CBReadData_UTCTimeStamp_First_Year_ReadData_IrvYearUTC();   
   //! ##### Check if year value is within range then read otherwise report error
   if (MAXLIMIT_YEARS_UTC > TStampFirstYear)
   {
      Data[0] = (uint8) TStampFirstYear;
   }
   else
   {
      Data[0] = 0U;
      Ret     = RTE_E_CSDataServices_UTCTimeStamp_First_Year_E_NOT_OK;
   }
   return Ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBReadData_UTCTimeStamp_Latest_Day_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <CBReadData_UTCTimeStamp_Latest_Day>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   Days8bit_Fact025_T Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Day_ReadData_IrvDayUTC(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBReadData_UTCTimeStamp_Latest_Day_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CSDataServices_UTCTimeStamp_Latest_Day_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_Latest_Day_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the Read UTCTimestamp LatestDay Data runnable logic for  the 'CBReadData_UTCTimeStamp_Latest_Day_ReadData'
//!
//!
//!
//!==================================================================================================================== 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Day_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_Latest_Day_ReadData (returns application error)
 *********************************************************************************************************************/
   Days8bit_Fact025_T TStampLatestDay;
   Std_ReturnType     Ret             = RTE_E_OK;
   
   //! ###### Reading the latest day value
   TStampLatestDay = Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Day_ReadData_IrvDayUTC();   
   //! ##### Check if day value is within range then read otherwise report error
   if (MAXLIMIT_DAYS_UTC > TStampLatestDay)
   {
      Data[0] =(uint8) TStampLatestDay;
   }
   else
   {
      Data[0] = 0U;
      Ret     = RTE_E_CSDataServices_UTCTimeStamp_Latest_Day_E_NOT_OK;
   }
   return Ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBReadData_UTCTimeStamp_Latest_Hour_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <CBReadData_UTCTimeStamp_Latest_Hour>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   Hours8bit_T Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Hour_ReadData_IrvHoursUTC(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBReadData_UTCTimeStamp_Latest_Hour_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CSDataServices_UTCTimeStamp_Latest_Hour_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_Latest_Hour_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the Read UTCTimestamp LatestHour Data runnable logic for the 'CBReadData_UTCTimeStamp_Latest_Hour_ReadData'
//!
//!
//!==================================================================================================================== 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Hour_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_Latest_Hour_ReadData (returns application error)
 *********************************************************************************************************************/
   Hours8bit_T       TStampLatestHour;
   Std_ReturnType    Ret              = RTE_E_OK;
   
   //! ###### Reading the latest hour value
   TStampLatestHour = Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Hour_ReadData_IrvHoursUTC();
   //! ##### Check if hour value is within range then read otherwise report error
   if (MAXLIMIT_HOURS_UTC > TStampLatestHour)
   {
     Data[0] =(uint8) TStampLatestHour;
   }
   else
   {
     Data[0] = 0U;
     Ret     = RTE_E_CSDataServices_UTCTimeStamp_Latest_Hour_E_NOT_OK;
   }
   return Ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBReadData_UTCTimeStamp_Latest_Minutes_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <CBReadData_UTCTimeStamp_Latest_Minutes>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   Minutes8bit_T Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Minutes_ReadData_IrvMinutesUTC(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBReadData_UTCTimeStamp_Latest_Minutes_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CSDataServices_UTCTimeStamp_Latest_Minutes_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_Latest_Minutes_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the Read UTCTimestamp LatestMinutes Data runnable logic for the 'CBReadData_UTCTimeStamp_Latest_Minutes_ReadData' 
//!
//!
//!==================================================================================================================== 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Minutes_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_Latest_Minutes_ReadData (returns application error)
 *********************************************************************************************************************/
   Minutes8bit_T TStampLatestMinutes;
   Std_ReturnType Ret = RTE_E_OK;
   
   //! ###### Reading the latest minutes value
   TStampLatestMinutes = Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Minutes_ReadData_IrvMinutesUTC();
   //! ##### Check if minutes value is within range then read otherwise report error   
   if (MAXLIMIT_MINUTES_UTC > TStampLatestMinutes)
   {
     Data[0] =(uint8) TStampLatestMinutes;
   }
   else
   {
     Data[0] = 0U;
     Ret      = RTE_E_CSDataServices_UTCTimeStamp_Latest_Minutes_E_NOT_OK;
   }   
   return Ret;   

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBReadData_UTCTimeStamp_Latest_Month_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <CBReadData_UTCTimeStamp_Latest_Month>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   Months8bit_T Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Month_ReadData_IrvMonthUTC(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBReadData_UTCTimeStamp_Latest_Month_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CSDataServices_UTCTimeStamp_Latest_Month_E_NOT_OK
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_Latest_Month_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the Read UTCTimestamp LatestMonth Data runnable logic for the 'CBReadData_UTCTimeStamp_Latest_Month_ReadData'
//!
//!
//!==================================================================================================================== 

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Month_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_Latest_Month_ReadData (returns application error)
 *********************************************************************************************************************/
   Months8bit_T   TStampLatestMonth;
   Std_ReturnType Ret               = RTE_E_OK;

   //! ###### Reading the latest month value
   TStampLatestMonth = Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Month_ReadData_IrvMonthUTC();   
   //! ##### Check if month value is within range then read otherwise report error   
   if (MAXLIMIT_MONTHS_UTC > TStampLatestMonth)
   {
     Data[0] =(uint8) TStampLatestMonth;
   }
   else
   {
     Data[0] = 0U;
     Ret      = RTE_E_CSDataServices_UTCTimeStamp_Latest_Month_E_NOT_OK;
   }
   return Ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBReadData_UTCTimeStamp_Latest_Seconds_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <CBReadData_UTCTimeStamp_Latest_Seconds>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   Seconds8bitFact025_T Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Seconds_ReadData_IrvSecondsUTC(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBReadData_UTCTimeStamp_Latest_Seconds_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CSDataServices_UTCTimeStamp_Latest_Seconds_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_Latest_Seconds_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the Read UTCTimestamp LatestSeconds Data runnable logic for the 'CBReadData_UTCTimeStamp_Latest_Seconds_ReadData'
//!
//!
//!==================================================================================================================== 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Seconds_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_Latest_Seconds_ReadData (returns application error)
 *********************************************************************************************************************/
   Seconds8bitFact025_T TStampLatestSeconds;
   Std_ReturnType       Ret                 = RTE_E_OK;
   
   //! ###### Reading the latest second value
   TStampLatestSeconds = Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Seconds_ReadData_IrvSecondsUTC();
   //! ##### Check if second value is within range then read otherwise report error
   if (MAXLIMIT_SECONDS_UTC > TStampLatestSeconds)
   {
     Data[0] = (uint8) TStampLatestSeconds;
   }
   else
   {
     Data[0] = 0U;
     Ret      = RTE_E_CSDataServices_UTCTimeStamp_Latest_Seconds_E_NOT_OK;
   }
   return Ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBReadData_UTCTimeStamp_Latest_Year_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <CBReadData_UTCTimeStamp_Latest_Year>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   Years8bit_T Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Year_ReadData_IrvYearUTC(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBReadData_UTCTimeStamp_Latest_Year_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type DataArrayType_uint8_1
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CSDataServices_UTCTimeStamp_Latest_Year_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_Latest_Year_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the Read UTCTimestamp LatestYear Data runnable logic for the 'CBReadData_UTCTimeStamp_Latest_Year_ReadData'
//!
//!
//!==================================================================================================================== 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Year_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBReadData_UTCTimeStamp_Latest_Year_ReadData (returns application error)
 *********************************************************************************************************************/
   Years8bit_T    TStampLatestYear;
   Std_ReturnType Ret              = RTE_E_OK;
   
   //! ###### Reading the latest year value
   TStampLatestYear = Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Year_ReadData_IrvYearUTC();
   //! ##### Check if year value is within range then read otherwise report error
   if (MAXLIMIT_YEARS_UTC > TStampLatestYear)
   {
      Data[0] = (uint8) TStampLatestYear;
   }
   else
   {
      Data[0] = 0U;
      Ret     = RTE_E_CSDataServices_UTCTimeStamp_Latest_Year_E_NOT_OK;
   }
   return Ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DcmRequestManufacturerNotification_Confirmation
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Confirmation> of PortPrototype <DcmRequestManufacturerNotification>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DcmRequestManufacturerNotification_Confirmation(uint8 SID, uint8 ReqType, uint16 SourceAddress, Dcm_ConfirmationStatusType ConfirmationStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_ServiceRequestNotification_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DcmRequestManufacturerNotification_Confirmation_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements Manufacturer Level DCM Confirmation Notification runnable logic for the 'DcmRequestManufacturerNotification_Confirmation'
//!
//!
//!==================================================================================================================== 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticComponent_CODE) DcmRequestManufacturerNotification_Confirmation(uint8 SID, uint8 ReqType, uint16 SourceAddress, Dcm_ConfirmationStatusType ConfirmationStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DcmRequestManufacturerNotification_Confirmation (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DcmRequestManufacturerNotification_Indication
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Indication> of PortPrototype <DcmRequestManufacturerNotification>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DcmRequestManufacturerNotification_Indication(uint8 SID, const uint8 *RequestData, uint16 DataSize, uint8 ReqType, uint16 SourceAddress, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument RequestData: uint8* is of type Dcm_Data2043ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_ServiceRequestNotification_E_NOT_OK
 *   RTE_E_ServiceRequestNotification_E_REQUEST_NOT_ACCEPTED
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DcmRequestManufacturerNotification_Indication_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements Manufacturer Level DCM Indication Notification runnable logic for the 'DcmRequestManufacturerNotification_Indication''
//!
//!
//!==================================================================================================================== 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticComponent_CODE) DcmRequestManufacturerNotification_Indication(uint8 SID, P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_DATA) RequestData, uint16 DataSize, uint8 ReqType, uint16 SourceAddress, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DcmRequestManufacturerNotification_Indication (returns application error)
 *********************************************************************************************************************/
   VehicleMode_T   VehicleModeInternal = VehicleMode_NotAvailable;
   Std_ReturnType  retval              = RTE_E_OK;
   Std_ReturnType  Vmretval            = RTE_E_OK;
  
   //! ###### Reading the vehicle mode 
   Vmretval = Rte_Read_VehicleModeInternal_VehicleMode(&VehicleModeInternal);  
   //! ##### Check the vehicle mode if it is not RTE_E_OK then report it as error 
   if (RTE_E_OK != Vmretval)
   {
      VehicleModeInternal = VehicleMode_Error;
   }
   else
   {
      // Do nothing
   }  
   //! ##### Check the DcmRequest Conditions 
   if ((VehicleMode_Running == VehicleModeInternal)
      &&((DCMCMP_SVC_ECURESET==SID)
      ||(DCMCMP_SVC_COMCTRL==SID)
      ||(DCMCMP_SVC_LINKCTRL==SID)
      ||((DCMCMP_SVC_SESSIONCNTRL==SID)
      &&(DCMCMP_SVCSUB_PROGRAMSESSION==RequestData[0]))))
   {
      *ErrorCode = DCM_E_CONDITIONSNOTCORRECT;
      retval     = RTE_E_ServiceRequestNotification_E_NOT_OK;
   }
   else
   {
      // Do nothing
   }
   return retval;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DiagnosticComponent_Dcm_ActivateIss
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <EXTENDED_SESSION> of ModeDeclarationGroupPrototype <DcmDiagnosticSessionControl> of PortPrototype <DcmDiagnosticSessionControl>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DiagActiveState_isDiagActive(DiagActiveState_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_UR_ANW_Dcm_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DiagnosticComponent_Dcm_ActivateIss_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements DCM Extended Session Entry Notification runnable logic for the 'DiagnosticComponent_Dcm_ActivateIss' 
//!
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DiagnosticComponent_CODE) DiagnosticComponent_Dcm_ActivateIss(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DiagnosticComponent_Dcm_ActivateIss
 *********************************************************************************************************************/
   //! ###### Rte call for activation is given and then diagnostic state is set to active
   Rte_Call_UR_ANW_Dcm_ActivateIss();
   Rte_Write_DiagActiveState_isDiagActive(Diag_Active_TRUE);
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DiagnosticComponent_Dcm_DeactivateIss
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on exiting of Mode <EXTENDED_SESSION> of ModeDeclarationGroupPrototype <DcmDiagnosticSessionControl> of PortPrototype <DcmDiagnosticSessionControl>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DiagActiveState_isDiagActive(DiagActiveState_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_UR_ANW_Dcm_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DiagnosticComponent_Dcm_DeactivateIss_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements DCM Extended Session Exit Notification runnable logic for the 'DiagnosticComponent_Dcm_ActivateIss'
//!
//! 
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DiagnosticComponent_CODE) DiagnosticComponent_Dcm_DeactivateIss(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DiagnosticComponent_Dcm_DeactivateIss
 *********************************************************************************************************************/
   //! ###### Rte call for deactivation is given and then diagnostic state is set to inactive 
   Rte_Call_UR_ANW_Dcm_DeactivateIss();
   Rte_Write_DiagActiveState_isDiagActive(Diag_Active_FALSE);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DiagnosticCycleCtrl_100ms_Runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_OpCycle_PowerCycle_SetOperationCycleState(Dem_OperationCycleStateType CycleState)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_OperationCycle_E_NOT_OK, RTE_E_OperationCycle_E_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DiagnosticCycleCtrl_100ms_Runnable_doc
 *********************************************************************************************************************/
 //!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for 'DiagnosticCycleCtrl_100ms_Runnable'
//! 
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
FUNC(void, DiagnosticComponent_CODE) DiagnosticCycleCtrl_100ms_Runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DiagnosticCycleCtrl_100ms_Runnable
 *********************************************************************************************************************/
   static VehicleMode_Type Vehicle_Mode = {VehicleMode_NotAvailable,
                                           VehicleMode_NotAvailable};
          uint8            retValue     = RTE_E_OK;
   
   //! ###### Processing the control of Diagnostic cycle
   //! ##### Read VehicleMode interface
   Vehicle_Mode.Previous = Vehicle_Mode.Current;
   retValue = Rte_Read_VehicleModeInternal_VehicleMode(&Vehicle_Mode.Current);
   MACRO_StdRteRead_IntRPort((retValue),(Vehicle_Mode.Current),(VehicleMode_Error))   
   //! ##### If the previous state of vehicle mode is not same as current sate 
   if (Vehicle_Mode.Current != Vehicle_Mode.Previous)
   {
      //! #### Then if the vehicle mode current state is in Hibernate then set the Diagnostic Cycle to END
      if (VehicleMode_Hibernate == Vehicle_Mode.Current)
      {
         Rte_Call_OpCycle_PowerCycle_SetOperationCycleState(DEM_CYCLE_STATE_END);
      }
      //! #### If the vehicle mode current state is in running then set Diagnostic cycle to Start
      else if (VehicleMode_Running == Vehicle_Mode.Current)     
      {
         Rte_Call_OpCycle_PowerCycle_SetOperationCycleState(DEM_CYCLE_STATE_START);
      }
      else
      {
         // do nothing: keep previous cycle run
      }
   }
   else
   {
      // do nothing: keep previous cycle run
   }   
   //! ##### If the previous state of Diagnostic cycle is in Hibernate state 
   //! #### Check the vehicle mode and accordingly set then Diagnostic Cycle to START
   if ((VehicleMode_NotAvailable == Vehicle_Mode.Previous)
      && ((VehicleMode_Parked < Vehicle_Mode.Current)
      && (VehicleMode_Running >= Vehicle_Mode.Current)))
   {
      Rte_Call_OpCycle_PowerCycle_SetOperationCycleState(DEM_CYCLE_STATE_START);
   }
   else
   {
      // Do Nothing
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
/**********************************************************************************************************************
 *
 * Runnable Entity Name: LocalTimeDistribution_200ms_Runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 200ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_DayUTC_DayUTC(Days8bit_Fact025_T *data)
 *   Std_ReturnType Rte_Read_HoursUTC_HoursUTC(Hours8bit_T *data)
 *   Std_ReturnType Rte_Read_LpModeRunTime_LpModeRunTime(uint32 *data)
 *   Std_ReturnType Rte_Read_MinutesUTC_MinutesUTC(Minutes8bit_T *data)
 *   Std_ReturnType Rte_Read_MonthUTC_MonthUTC(Months8bit_T *data)
 *   Std_ReturnType Rte_Read_SecondsUTC_SecondsUTC(Seconds8bitFact025_T *data)
 *   Std_ReturnType Rte_Read_YearUTC_YearUTC(Years8bit_T *data)
 *   boolean Rte_IsUpdated_SecondsUTC_SecondsUTC(void)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_LpModeRunTime_LpModeRunTime(uint32 data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   Days8bit_Fact025_T Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvDayUTC(void)
 *   Hours8bit_T Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvHoursUTC(void)
 *   Minutes8bit_T Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvMinutesUTC(void)
 *   Months8bit_T Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvMonthUTC(void)
 *   Seconds8bitFact025_T Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvSecondsUTC(void)
 *   Years8bit_T Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvYearUTC(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvDayUTC(Days8bit_Fact025_T data)
 *   void Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvHoursUTC(Hours8bit_T data)
 *   void Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvMinutesUTC(Minutes8bit_T data)
 *   void Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvMonthUTC(Months8bit_T data)
 *   void Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvSecondsUTC(Seconds8bitFact025_T data)
 *   void Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvYearUTC(Years8bit_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: LocalTimeDistribution_200ms_Runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//!
//! \brief
//! This function implements the cyclic execution runnable logic for the 'LocalTimeDistribution_200ms_Runnable'
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DiagnosticComponent_CODE) LocalTimeDistribution_200ms_Runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: LocalTimeDistribution_200ms_Runnable
 *********************************************************************************************************************/
   static LocalUTC_Type  LocalUtcTime            = {0U,0U,0U,0U,0U,0U,0U};
   static LocalUTC_Type  PreviousLocalUtc        = {0U,0U,0U,0U,0U,0U,0U};
   static Std_ReturnType retValue_MinuteUTC_New  = RTE_E_INVALID;
   static Std_ReturnType retValue_MinuteUTC_Old  = RTE_E_INVALID;

   
          LPU_Type       LpuUtcTime              = {0U,0U,0U,0U,0U,0U,0U};
          uint32         LpuTime                 = 0U;
          uint32         OffsetTime              = 0U;
          boolean        IsConverted             = 0U;
          PhysicalUTC    PhysicalTime            = {0U,0U,0U,0U,0U,0U,0U};
          Std_ReturnType retValue                = RTE_E_INVALID;     
   //! ###### Processing the Local time for Diagnostics   
   //! ##### Check if the value Seconds has updated 
   //! ##### If it is updated then read the values
   retValue_MinuteUTC_Old = retValue_MinuteUTC_New;
   retValue_MinuteUTC_New = Rte_Read_MinutesUTC_MinutesUTC(&(LocalUtcTime.MinutesUTC));
   MACRO_StdRteRead_ExtRPort((retValue_MinuteUTC_New), (LocalUtcTime.MinutesUTC),(255U), (PreviousLocalUtc.MinutesUTC))
   //! ##### Condition to check the return value status for MinuteUTC
   if ((retValue_MinuteUTC_New != RTE_E_COM_STOPPED)
      &&(retValue_MinuteUTC_New != RTE_E_NEVER_RECEIVED)
      &&((retValue_MinuteUTC_New & RTE_E_MAX_AGE_EXCEEDED) != RTE_E_MAX_AGE_EXCEEDED))
   {        
      RteDataRead_Common(&LocalUtcTime,&PreviousLocalUtc);
      LocalUtcTime.Milliseconds = 0U;      
   }
   else
   {
      //! #### If the Minutes value is not received (COM_STOPPED or TimeOut) then run the internal counter
      retValue = Rte_Read_LpModeRunTime_LpModeRunTime(&LpuTime);
      MACRO_StdRteRead_IntRPort((retValue), (LpuTime), 0U) // Error value as Zero      
      if((RTE_E_OK==retValue_MinuteUTC_Old)
        &&(RTE_E_MAX_AGE_EXCEEDED==retValue_MinuteUTC_New))
      {
         OffsetTime = UTC_MINUTE_SIGNAL_TIMEOUT_VALUE;
      }
      else
      {
         // Do Nothing
      }
      LocalUtcTime.Milliseconds += UTC_TIME_INCREAMENT_TIME;         
      LocalUtcTime.DayUTC       = Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvDayUTC();
      LocalUtcTime.HoursUTC     = Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvHoursUTC();
      LocalUtcTime.MinutesUTC   = Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvMinutesUTC();
      LocalUtcTime.MonthUTC     = Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvMonthUTC();
      LocalUtcTime.SecondsUTC   = Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvSecondsUTC();
      LocalUtcTime.YearUTC      = Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvYearUTC();
      //! #### Read raw values are converted to physical
      ConvertRawToPhysical(&LocalUtcTime,&PhysicalTime);
      LpuTime     = LpuTime+OffsetTime;      
      IsConverted = ConvertLpuTime2UtcFormat(LpuTime,&LpuUtcTime);      
      //! #### If Lputime is converted to Utc format then add the lpu time 
      //! #### Process Add LPU time logic: 'AddLpuTime()'      
      if (1U == IsConverted)
      {
         AddLpuTime(&LpuUtcTime,&PhysicalTime); 
         Rte_Write_LpModeRunTime_LpModeRunTime(0);
      }
      else
      {
         // Do Nothing
      }      
      //! #### update the local time 
      //! #### Process update LPU time logic: 'UpdateLocalUtcTime()'
      UpdateLocalUtcTime(&PhysicalTime);
      //! #### Convert physical values into raw values
      //! #### Process Convert Physical to raw logic: 'CovertPhysicalToRaw()'
      CovertPhysicalToRaw(&PhysicalTime,
                          &LocalUtcTime);
   }   
   //! ##### Check if the values obtained are in range and then write the values
   //! #### Process check UTc values logic: 'CheckUtcValues()'
   CheckUtcValues(&PreviousLocalUtc,
                  &LocalUtcTime);
   //! ##### Process to write the status of LocalUtcTime on to the RTE interface
   Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvDayUTC(LocalUtcTime.DayUTC);
   Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvHoursUTC(LocalUtcTime.HoursUTC);
   Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvMinutesUTC(LocalUtcTime.MinutesUTC);
   Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvMonthUTC(LocalUtcTime.MonthUTC);
   Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvSecondsUTC(LocalUtcTime.SecondsUTC);
   Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvYearUTC(LocalUtcTime.YearUTC);
   
   PreviousLocalUtc.DayUTC     = LocalUtcTime.DayUTC;
   PreviousLocalUtc.HoursUTC   = LocalUtcTime.HoursUTC;
   PreviousLocalUtc.MinutesUTC = LocalUtcTime.MinutesUTC;
   PreviousLocalUtc.MonthUTC   = LocalUtcTime.MonthUTC;
   PreviousLocalUtc.SecondsUTC = LocalUtcTime.SecondsUTC;
   PreviousLocalUtc.YearUTC    = LocalUtcTime.YearUTC;        
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#define DiagnosticComponent_STOP_SEC_CODE
#include "DiagnosticComponent_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the 'RteDataRead_Common'
//!
//! \param   *pRteInData_Common     Examine and update the input signals  
//! \param   *pPreviousData         Provides previous status of the interface    
//!
//!====================================================================================================================
static void RteDataRead_Common(      LocalUTC_Type *pRteInData_Common,
                               const LocalUTC_Type *pPreviousData)
{
    //! ###### Processing RTE data read common logic
   Std_ReturnType retValue = RTE_E_INVALID;
   //! ##### Read 'DayUTC' interface
   retValue = Rte_Read_DayUTC_DayUTC(&(pRteInData_Common->DayUTC));
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->DayUTC),(255U), (pPreviousData->DayUTC))
   //! ##### Read 'HoursUTC' interface
   retValue = Rte_Read_HoursUTC_HoursUTC(&(pRteInData_Common->HoursUTC));
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->HoursUTC),(255U), (pPreviousData->HoursUTC))
   //! ##### Read 'SecondsUTC' interface
   retValue = Rte_Read_SecondsUTC_SecondsUTC(&(pRteInData_Common->SecondsUTC));
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->SecondsUTC),(255U), (pPreviousData->SecondsUTC))
   //! ##### Read 'YearUTC' interface
   retValue = Rte_Read_YearUTC_YearUTC(&(pRteInData_Common->YearUTC));
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->YearUTC),(255U), (pPreviousData->YearUTC))
   //! ##### Read 'MonthUTC' interface
   retValue = Rte_Read_MonthUTC_MonthUTC(&(pRteInData_Common->MonthUTC));
   MACRO_StdRteRead_ExtRPort((retValue), (pRteInData_Common->MonthUTC),(255U), (pPreviousData->MonthUTC))
}
//!====================================================================================================================
//! 
//! \brief
//! This function implements the Check the  UTC Time logic for the 'CheckUtcValues'
//!
//! \param   *LocalUtcTime         Update the check the values 
//! \param   *PreviousLocalUtc     Providing the previous UTC values
//!
//!====================================================================================================================
static void CheckUtcValues(const LocalUTC_Type *PreviousLocalUtc,
                                 LocalUTC_Type *LocalUtcTime)
{
   //! ###### Checking if the UTC values are in range 
   //! ##### If the signal MonthUTC is out of range, then use last received value as replacement value.
   if ((LocalUtcTime->MonthUTC >= MAXLIMIT_MONTHS_UTC)
      &&((LocalUtcTime->MonthUTC < MINLIMIT_MONTHS_UTC)))
   {
     LocalUtcTime->MonthUTC = PreviousLocalUtc->MonthUTC;
   }
   else
   {
      // Do nothing
   }
   //! ##### If the signal DayUTC is out of range, then use last received value as replacement value.
   if (LocalUtcTime->DayUTC >= MAXLIMIT_DAYS_UTC)
   {
      LocalUtcTime->DayUTC = PreviousLocalUtc->DayUTC;
   }
   else
   {
      // Do nothing
   }
   //! ##### If the signal HoursUTC is out of range, then use last received value as replacement value.
   if (LocalUtcTime->HoursUTC >= MAXLIMIT_HOURS_UTC)
   {
      LocalUtcTime->HoursUTC = PreviousLocalUtc->HoursUTC;
   }
   else
   {
      // Do nothing
   }
   //! ##### If the signal MinutesUTC is out of range, then use last received value as replacement value.
   if (LocalUtcTime->MinutesUTC >= MAXLIMIT_MINUTES_UTC)
   {
     LocalUtcTime->MinutesUTC = PreviousLocalUtc->MinutesUTC;
   }
   else
   {
      // Do nothing
   }
   //! ##### If the signal SecondsUTC is out of range, then use last received value as replacement value.
   if (LocalUtcTime->SecondsUTC >= MAXLIMIT_SECONDS_UTC)
   {
      LocalUtcTime->SecondsUTC = PreviousLocalUtc->SecondsUTC;
   }
   else
   {
      // Do nothing
   }
   //! ##### If the signal YearUTC is out of range, then use last received value as replacement value.
   if (LocalUtcTime->YearUTC >= MAXLIMIT_YEARS_UTC)
   {
      LocalUtcTime->YearUTC = PreviousLocalUtc->YearUTC;
   } 
   else
   {
      // Do nothing
   }
}
//!====================================================================================================================
//! 
//! \brief
//! This function implements logic to Convert the Input Raw Time to Physical value in the 'ConvertRawToPhysical'
//!
//! \param   *timeStruct        Input structure to provide UTC values
//! \param   *PhysicalStruct    Output structure to take Updated values 
//!
//!====================================================================================================================
static void ConvertRawToPhysical(const LocalUTC_Type *timeStruct,
                                       PhysicalUTC   *PhysicalStruct)
{
   //! ##### Conversion of received raw values to physical for calculation
   PhysicalStruct->DayUTC       = (timeStruct->DayUTC) / 4U;
   PhysicalStruct->YearUTC      = (timeStruct->YearUTC) + MINLIMIT_YEARS_UTC;
   PhysicalStruct->HoursUTC     = (timeStruct->HoursUTC);
   PhysicalStruct->Milliseconds = (timeStruct->Milliseconds);
   PhysicalStruct->MinutesUTC   = (timeStruct->MinutesUTC);
   PhysicalStruct->MonthUTC     = (timeStruct->MonthUTC);
   PhysicalStruct->SecondsUTC   = (timeStruct->SecondsUTC) / 4U;   
}
//!====================================================================================================================
//! 
//! \brief
//! This function implements logic to Convert the Input Physical value to Raw Time in the 'CovertPhysicalToRaw'
//!
//! \param   *PhysicalStruct        Input structure to provide UTC values
//! \param   *timeStruct            Output structure to take Updated values 
//!
//!====================================================================================================================
static void CovertPhysicalToRaw(const PhysicalUTC   *PhysicalStruct,
                                      LocalUTC_Type *timeStruct)
{
   //! ##### Conversion of calculated physical values to raw
   timeStruct->DayUTC       = (PhysicalStruct->DayUTC) * 4;
   timeStruct->YearUTC      = (Years8bit_T)((PhysicalStruct->YearUTC) - MINLIMIT_YEARS_UTC);
   timeStruct->HoursUTC     = PhysicalStruct->HoursUTC;
   timeStruct->Milliseconds = PhysicalStruct->Milliseconds;
   timeStruct->MinutesUTC   = PhysicalStruct->MinutesUTC;
   timeStruct->MonthUTC     = PhysicalStruct->MonthUTC;
   timeStruct->SecondsUTC   = (PhysicalStruct->SecondsUTC) * 4;
}
//!====================================================================================================================
//! 
//! \brief
//! This function implements logic to convert the LPUTime to UTC format in the 'ConvertLpuTime2UtcFormat'
//!
//! \param   LpuTime      Input Lpu time
//! \param   *LpuStruct   Output structure to take Updated values 
//!
//! \return  retval       returns boolean type retval value 
//!
//!====================================================================================================================
static boolean ConvertLpuTime2UtcFormat(const uint32   LpuTime,
                                              LPU_Type *LpuStruct)
{
   //! ###### Processing the recieved Lputime into UTC format 
   boolean retval = 0U;
   uint32  LocalSeconds = 0U;
   uint32  LocalMinutes = 0U;
   uint32  LocalHours = 0U;
   //! ##### Check if Lpu time is not zero and process it further
   if (0U != LpuTime)
   {
      LocalSeconds   = LpuTime / Const_MaxMilliSeconds;
      LpuStruct->Milliseconds = (uint16)(LpuTime % Const_MaxMilliSeconds);
      //! #### Checking if seconds are in range and updating Lpu structure
      if (LocalSeconds >= Const_MaxSeconds)
      {
         LocalMinutes          = LocalSeconds/Const_MaxSeconds;
         LpuStruct->SecondsUTC = (Seconds8bitFact025_T)(LocalSeconds % Const_MaxSeconds);
      }
      else
      {
         LpuStruct->SecondsUTC = (Seconds8bitFact025_T) LocalSeconds;
      }
      //! #### Checking if minutes are in range and update Lpu structure
      if (LocalMinutes >= Const_MaxMinutes)
      {
         LocalHours            = LocalMinutes / Const_MaxMinutes;
         LpuStruct->MinutesUTC = (Minutes8bit_T)(LocalMinutes % Const_MaxMinutes);
      }
      else
      {
         LpuStruct->MinutesUTC = (Minutes8bit_T)LocalMinutes;
      }
      //! #### Checking if hours are in range and update Lpu structure
      if (LocalHours >= Const_MaxHours)
      {
         LpuStruct->DayUTC   = (Days8bit_Fact025_T)(LocalHours / Const_MaxHours);
         LpuStruct->HoursUTC = (Hours8bit_T) (LocalHours % Const_MaxHours);
      }
      else
      {
         LpuStruct->HoursUTC = (Hours8bit_T)LocalHours;
      }
   retval = 1U;
   }
   else
   {
      retval = 0U;
   }
   return retval;
}
//!====================================================================================================================
//! 
//! \brief
//! This function implements Logic to Added the LPU Utc Time to Present UTC time in the 'AddLpuTime'
//!
//! \param   *LpuStruct             Input structure to provide UTC values
//! \param   *timeStruct            Output structure to take Updated values 
//!
//!====================================================================================================================
static void AddLpuTime(const LPU_Type    *LpuStruct,
                             PhysicalUTC *timeStruct)
{
   static uint8    DaysOfMonths[13U] = {0U, 31U, 28U, 31U, 30U, 31U, 30U,                     
                                        31U, 31U, 30U, 31U, 30U, 31U };   
          LPU_Type TempStruct        = {0U,0U,0U,0U,0U,0U,0U};
   //! ###### Processing the addition of Lputime and physical time
   //! ##### Checking if the year is leap or not
   isLeap(timeStruct->YearUTC,&DaysOfMonths[2U]);
   TempStruct.Milliseconds = (timeStruct->Milliseconds)+(LpuStruct->Milliseconds);
   
   //! ##### Checking if milliseconds are in range and converting to seconds 
   if (TempStruct.Milliseconds >= Const_MaxMilliSeconds)
   {
      TempStruct.SecondsUTC    = (Seconds8bitFact025_T)(TempStruct.Milliseconds / Const_MaxMilliSeconds);
      timeStruct->Milliseconds = TempStruct.Milliseconds % Const_MaxMilliSeconds;
   } 
   else
   {
      timeStruct->Milliseconds = TempStruct.Milliseconds;
   }
   TempStruct.SecondsUTC = (TempStruct.SecondsUTC) + (timeStruct->SecondsUTC) + (LpuStruct->SecondsUTC);
   //! ##### Checking if seconds are in range and converting to minutes
   if (TempStruct.SecondsUTC >= Const_MaxSeconds)
   {
      TempStruct.MinutesUTC  = TempStruct.SecondsUTC / Const_MaxSeconds;
      timeStruct->SecondsUTC = TempStruct.SecondsUTC % Const_MaxSeconds;
   }
   else
   {
      timeStruct->SecondsUTC = TempStruct.SecondsUTC;
   }

   TempStruct.MinutesUTC = (timeStruct->MinutesUTC) + (LpuStruct->MinutesUTC) + (TempStruct.MinutesUTC);
   //! ##### Checking if minutes are in range and converting to hours
   if (TempStruct.MinutesUTC >= Const_MaxMinutes)
   {
      TempStruct.HoursUTC    = TempStruct.MinutesUTC / Const_MaxMinutes;
      timeStruct->MinutesUTC = TempStruct.MinutesUTC % Const_MaxMinutes;      
   }
   else
   {
      timeStruct->MinutesUTC = TempStruct.MinutesUTC;
   }
   TempStruct.HoursUTC = (timeStruct->HoursUTC) + (LpuStruct->HoursUTC) + (TempStruct.HoursUTC);
    //! ##### Checking if hours are in range and converting to days
   if (TempStruct.HoursUTC >= Const_MaxHours)
   {
      TempStruct.DayUTC    = TempStruct.HoursUTC / Const_MaxHours;
      timeStruct->HoursUTC = TempStruct.HoursUTC % Const_MaxHours;
   }
   else
   {
      timeStruct->HoursUTC = (TempStruct.HoursUTC);
   }
   TempStruct.DayUTC = (timeStruct->DayUTC) + (LpuStruct->DayUTC) + (TempStruct.DayUTC);
   //! ##### Checking if days are in range and converting to months
   if (timeStruct->MonthUTC < MAXLIMIT_MONTHS_UTC)
   {
      if (TempStruct.DayUTC > DaysOfMonths[timeStruct->MonthUTC] )
      {
         do
         {
            TempStruct.DayUTC  = (TempStruct.DayUTC) - DaysOfMonths[timeStruct->MonthUTC];     
            (timeStruct->MonthUTC)++;            
            //! ##### Checking if months are in range and converting to year
            if (timeStruct->MonthUTC > Const_MaxMonths)
            {
               timeStruct->MonthUTC = 1U;
               (timeStruct->YearUTC)++;
            }
            else
            {
               // Do Nothing
            } 
             
         }while(TempStruct.DayUTC > DaysOfMonths[timeStruct->MonthUTC]);           
         timeStruct->DayUTC = TempStruct.DayUTC;   
      }
      else
      {
         timeStruct->DayUTC = (TempStruct.DayUTC);
      }
   } 
   else
   {
      timeStruct->MonthUTC = RTE_E_INVALID;
   }
}
//!====================================================================================================================
//! 
//! \brief
//! This function implements Logic to Find & Update the Leap year Days(February) in the 'isLeap'
//!
//! \param   Years             Input year
//! \param   *DaysOfMonths     Check and Update the values 
//!
//!====================================================================================================================
static void isLeap(const uint16      Years,
                          uint8      *DaysOfMonths)
{
   //! ##### If year is leap then Feb should have 29 days
   if ((((Years%100U) != 0U) 
      && ((Years%4U) == 0U)) 
      || ((Years%400U) == 0U))   
   {      
      DaysOfMonths[0U]=29U;   
   }    
   else    
   {      
      DaysOfMonths[0U]=28U;     
   }
} 
//!====================================================================================================================
//! 
//! \brief
//! This function implements logic to increament the Days in 'updateDays'
//!
//! \param   *DaysOfMonths    Check and Update the values
//! \param   *Days            Updated Days value
//! \param   *Months          Input Month is provided
//!
//!====================================================================================================================
static void updateDays(const uint8              *DaysOfMonths,
                             Days8bit_Fact025_T *Days,
                             Months8bit_T       *Months)
{  
   //! ##### Updating days and month based on the standard values
   Months8bit_T cur_Month = 0U;
   
   cur_Month = *Months; 
   if (cur_Month < MAXLIMIT_MONTHS_UTC)
   {
      if(*Days < DaysOfMonths[cur_Month])         
      {            
         (*Days)++; 
      }   
      else          
      {            
         (*Days) = 1U;            
         (*Months)++;         
      }
   }   
}
//!====================================================================================================================
//! 
//! \brief
//! This function implements logic to update Local Utc Time in the 'UpdateLocalUtcTime'
//!
//! \param   *timeStruct          Check and Update the values
//!
//!====================================================================================================================
static void UpdateLocalUtcTime(PhysicalUTC *timeStruct)
{   

static uint8 DaysOfMonths[13]={ 0U, 31U, 28U, 31U, 30U, 31U, 30U,                          
                         31U, 31U, 30U, 31U, 30U, 31U };

   if (timeStruct->Milliseconds >= Const_MaxMilliSeconds)  
   {
      timeStruct->Milliseconds = 0U;      
      //! ##### If seconds are not 60(i.e max limit) then increment
      if (timeStruct->SecondsUTC < (Const_MaxSeconds-1))      
      {         
         (timeStruct->SecondsUTC)++;      
      }      
      else      
      {         
         timeStruct->SecondsUTC = 0U;         
         //! #### If minutes are not 59(i.e max limit) then increment
         if (timeStruct->MinutesUTC < (Const_MaxMinutes-1))         
         {         
            (timeStruct->MinutesUTC)++;         
         }         
         else         
         {           
            timeStruct->MinutesUTC = 0U;            
            //! ### If hours are not 24(i.e max limit) then increment
            if(timeStruct->HoursUTC < Const_MaxHours)            
            {               
               (timeStruct->HoursUTC)++;            
            }            
            else            
            {               
               timeStruct->HoursUTC = 0U;               
               isLeap(timeStruct->YearUTC, 
                      &DaysOfMonths[2U]);            
               
               updateDays(&DaysOfMonths[0U],
                          &(timeStruct->DayUTC),
                          &(timeStruct->MonthUTC));                
               if (timeStruct->MonthUTC >= Const_MaxMonths)               
               {                  
                  (timeStruct->YearUTC)++;                  
                  timeStruct->MonthUTC = 1U;               
               }
               else
               {
                   // Do Nothing
               }
            }         
         }               
      }     
   }
   else
   {
      // Do Nothing
   }
}   
//! @}
//! @}
//! @}
//! @}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 
