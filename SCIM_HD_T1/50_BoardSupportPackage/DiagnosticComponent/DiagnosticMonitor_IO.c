/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  DiagnosticMonitor_IO.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  DiagnosticMonitor_IO
 *  Generated at:  Thu Jul 23 01:47:29 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <DiagnosticMonitor_IO>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file DiagnosticMonitor_IO.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup Platform_SCIM 
//! @{
//! @addtogroup DiagnosticComponent 
//! @{
//! @addtogroup DiagnosticMonitor_IO
//! @{
//!
//! \brief
//! DiagnosticMonitor_IO SWC.
//! ASIL Level : QM.
//! This module implements the logic for the DiagnosticMonitor_IO runnables.
//!======================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * IOHWAB_BOOL
 *   boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * IOHWAB_SINT8
 *   sint8 represents integers with a minimum value of -128 and a maximum value of 127.
 *      The order-relation on sint8 is: x < y if y - x is positive.
 *      sint8 has a lexical representation consisting of an optional sign followed 
 *      by a finite-length sequence of decimal digits (#x30-#x39). If the sign is 
 *      omitted, "+" is assumed. 
 *      
 *      For example: -1, 0, 12678, +10000.
 *
 * IOHWAB_UINT16
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * IOHWAB_UINT8
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_Diag_Act_DOWHS01_P1V6O_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_Diag_Act_DOWHS02_P1V6P_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_Diag_Act_DOWLS02_P1V7E_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_Diag_Act_DOWLS03_P1V7F_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_HwToleranceThreshold_X1C04_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V79_P1Interface_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V79_P2Interface_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V79_P3Interface_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V79_P4Interface_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V79_PiInterface_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinFaultStatus
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinVoltage_0V2
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPwmDutycycle
 *   sint8 represents integers with a minimum value of -128 and a maximum value of 127.
 *      The order-relation on sint8 is: x < y if y - x is positive.
 *      sint8 has a lexical representation consisting of an optional sign followed 
 *      by a finite-length sequence of decimal digits (#x30-#x39). If the sign is 
 *      omitted, "+" is assumed. 
 *      
 *      For example: -1, 0, 12678, +10000.
 *
 * VGTT_EcuPwmPeriod
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 *********************************************************************************************************************/

#include "Rte_DiagnosticMonitor_IO.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#define PCODE_OnState_FaultDetected_PWM 36U
#include "DiagnosticMonitor_IO.h"


static uint8 IrvDowls2_0Activation    = 0U;
static uint8 IrvDowls3_0Activation    = 0U;
static uint8 IrvDowhs1_0Activation    = 0U;
static uint8 IrvDowhs2_0Activation    = 0U;
uint8 IrvDowls2_Diag0Requested = 0U;
uint8 IrvDowls3_Diag0Requested = 0U;
uint8 IrvDowhs1_Diag0Requested = 0U;
uint8 IrvDowhs2_Diag0Requested = 0U;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IOHWAB_BOOL: Boolean
 * IOHWAB_SINT8: Integer in interval [-128...127]
 * IOHWAB_UINT16: Integer in interval [0...65535]
 * IOHWAB_UINT8: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWHS01_P1V6O_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWHS02_P1V6P_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWLS02_P1V7E_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWLS03_P1V7F_T: Integer in interval [0...255]
 * SEWS_HwToleranceThreshold_X1C04_T: Integer in interval [0...255]
 * SEWS_P1V79_P1Interface_T: Integer in interval [0...255]
 * SEWS_P1V79_P2Interface_T: Integer in interval [0...255]
 * SEWS_P1V79_P3Interface_T: Integer in interval [0...255]
 * SEWS_P1V79_P4Interface_T: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * VGTT_EcuPwmDutycycle: Integer in interval [-128...127]
 * VGTT_EcuPwmPeriod: Integer in interval [0...65535]
 * boolean: Boolean (standard type)
 * sint8: Integer in interval [-128...127] (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * IOCtrlReq_T: Enumeration of integer in interval [0...255] with enumerators
 *   IOCtrl_AppRequest (0U)
 *   IOCtrl_DiagReturnCtrlToApp (1U)
 *   IOCtrl_DiagShortTermAdjust (2U)
 * SEWS_P1V79_PiInterface_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_P1V79_PiInterface_T_Deactivation (0U)
 *   SEWS_P1V79_PiInterface_T_Activation (1U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 *
 * Array Types:
 * ============
 * DataArrayType_uint8_5: Array with 5 element(s) of type uint8
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 *
 * Record Types:
 * =============
 * SEWS_Diag_Act_LF_P_P1V79_s_T: Record with elements
 *   PiInterface of type SEWS_P1V79_PiInterface_T
 *   P1Interface of type SEWS_P1V79_P1Interface_T
 *   P2Interface of type SEWS_P1V79_P2Interface_T
 *   P3Interface of type SEWS_P1V79_P3Interface_T
 *   P4Interface of type SEWS_P1V79_P4Interface_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_HwToleranceThreshold_X1C04_T Rte_Prm_X1C04_HwToleranceThreshold_v(void)
 *   boolean Rte_Prm_X1C05_DtcActivationVbatEnable_v(void)
 *   SEWS_Diag_Act_DOWHS01_P1V6O_T Rte_Prm_P1V6O_Diag_Act_DOWHS01_v(void)
 *   SEWS_Diag_Act_DOWHS02_P1V6P_T Rte_Prm_P1V6P_Diag_Act_DOWHS02_v(void)
 *   SEWS_Diag_Act_DOWLS02_P1V7E_T Rte_Prm_P1V7E_Diag_Act_DOWLS02_v(void)
 *   SEWS_Diag_Act_DOWLS03_P1V7F_T Rte_Prm_P1V7F_Diag_Act_DOWLS03_v(void)
 *   boolean Rte_Prm_P1V6I_Diag_Act_AO12_P_v(void)
 *   boolean Rte_Prm_P1V6K_Diag_Act_AO12_L_v(void)
 *   boolean Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v(void)
 *   boolean Rte_Prm_P1V6R_Diag_Act_DOBHS02_v(void)
 *   boolean Rte_Prm_P1V6S_Diag_Act_DOBHS03_v(void)
 *   boolean Rte_Prm_P1V6T_Diag_Act_DOBHS04_v(void)
 *   boolean Rte_Prm_P1V7D_Diag_Act_DOBLS01_v(void)
 *   boolean Rte_Prm_P1V7G_Diag_Act_DAI01_v(void)
 *   boolean Rte_Prm_P1V7H_Diag_Act_DAI02_v(void)
 *   boolean Rte_Prm_P1V8E_Diag_Act_12VDCDC_v(void)
 *   SEWS_Diag_Act_LF_P_P1V79_s_T *Rte_Prm_P1V79_Diag_Act_LF_P_v(void)
 *
 *********************************************************************************************************************/


#define DiagnosticMonitor_IO_START_SEC_CODE
#include "DiagnosticMonitor_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1QXI_Data_P1QXI_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the logic for the DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData
//! 
//! \param Data    For Reading Battery Voltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements readby id service for P1QXI PWR 24v
   IOHWAB_UINT8           SelectParkedOrLivingPin      = 0U;
   IOHWAB_BOOL            IsDo12VActivated             = 0U;
   VGTT_EcuPinVoltage_0V2 Do12VPinVoltage              = 0U;
   VGTT_EcuPinVoltage_0V2 BatteryVoltage               = 0U;
   VGTT_EcuPinVoltage_0V2 DcdcRefVoltage               = 0U;
   VGTT_EcuPinFaultStatus DiagStatus                   = 0U;
   Std_ReturnType         retVal                       = RTE_E_INVALID;
   Std_ReturnType         returnVal                    = RTE_E_INVALID;
   //! ##### Processing to getDo12pinsState: 'Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS()'
   retVal = Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(SelectParkedOrLivingPin,
                                                           &IsDo12VActivated,
                                                           &Do12VPinVoltage,
                                                           &BatteryVoltage,
                                                           &DiagStatus);
   //! ##### If returned value is RTE_E_OK then fill data buffer with BatteryVoltage    
   if (RTE_E_OK == retVal)
   {
       Data[0]   = (uint8)BatteryVoltage;
       returnVal = RTE_E_OK;
   }
	//! #### else return RTE_E_DataServices_P1QXI_Data_P1QXI_E_NOT_OK
   else
   {
      returnVal = RTE_E_DataServices_P1QXI_Data_P1QXI_E_NOT_OK;
   }  
   return returnVal;


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ1_Data_P1VQ1_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the logic for the DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData
//! 
//! \param Data    For Reading Do12VPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements readby id service for P1VQ1 Ao12V_L
   IOHWAB_UINT8            SelectParkedOrLivingPin   = 1U; // Living
   IOHWAB_BOOL             IsDo12VActivated          = 0U;
   VGTT_EcuPinVoltage_0V2  Do12VPinVoltage           = 0U;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage            = 0U;
   VGTT_EcuPinVoltage_0V2  DcdcRefVoltage            = 0U;
   VGTT_EcuPinFaultStatus  DiagStatus                = 0U;
   Std_ReturnType          retVal                    = RTE_E_INVALID;
   Std_ReturnType          returnVal                 = RTE_E_INVALID;
   //! ##### Processing to getDo12pinsState: 'Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS()'
   retVal = Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(SelectParkedOrLivingPin,
                                                           &IsDo12VActivated,
                                                           &Do12VPinVoltage,
                                                           &BatteryVoltage,
                                                           &DiagStatus);
   //! ##### If returned value is RTE_E_OK then fill data buffer with BatteryVoltage    
   if (RTE_E_OK == retVal)
   {
      Data[0]   = (uint8)Do12VPinVoltage;
      returnVal = RTE_E_OK;
   }
	//! #### Else return RTE_E_DataServices_P1VQ1_Data_P1VQ1_E_NOT_OK
   else
   {
      returnVal = RTE_E_DataServices_P1VQ1_Data_P1VQ1_E_NOT_OK;
   }
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ2_Data_P1VQ2_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the logic for the DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData
//! 
//! \param    Data    For Reading Do12VPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData (returns application error)
 *********************************************************************************************************************/
    //! ###### This function implements readby id service for P1VQ1 Ao12V_p
    IOHWAB_UINT8             SelectParkedOrLivingPin    = 0U; //Parked
    IOHWAB_BOOL              IsDo12VActivated           = 0U;
    VGTT_EcuPinVoltage_0V2   Do12VPinVoltage            = 0U;
    VGTT_EcuPinVoltage_0V2   BatteryVoltage             = 0U;
    VGTT_EcuPinVoltage_0V2   DcdcRefVoltage             = 0U;
    VGTT_EcuPinFaultStatus   DiagStatus                 = 0U;
    Std_ReturnType           retVal                     = RTE_E_INVALID;
    Std_ReturnType           returnVal                  = RTE_E_INVALID;
    //! ##### Rtecall to read Do12VPinVoltage
    retVal = Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(SelectParkedOrLivingPin,
                                                            &IsDo12VActivated,
                                                            &Do12VPinVoltage,
                                                            &BatteryVoltage,
                                                            &DiagStatus);
    //! ##### If returned value is RTE_E_OK then fill data buffer with BatteryVoltage    
    if (RTE_E_OK == retVal)
    {
       Data[0]   = (uint8)Do12VPinVoltage;
       returnVal = RTE_E_OK;
    }
	 //! #### Else return RTE_E_DataServices_P1VQ2_Data_P1VQ2_E_NOT_OK
    else
    {
       returnVal = RTE_E_DataServices_P1VQ2_Data_P1VQ2_E_NOT_OK;
    }
    return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ3_Data_P1VQ3_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData
//! 
//! \param  Data[4]   For reading Period,Dutycucle and Dopinvol
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the ReadData by id logic for DOWHS01 
   IOHWAB_UINT8             DoPinRef      = 1U;   
   IOHWAB_BOOL              IsDoAct       = 0U;
   VGTT_EcuPinVoltage_0V2   DoPinVolt     = 0U; 
   VGTT_EcuPinVoltage_0V2   BatteryVolt   = 0U; 
   VGTT_EcuPwmDutycycle     DutyCycle     = (VGTT_EcuPwmDutycycle)0U; 
   VGTT_EcuPwmPeriod        Period        = 0U; 
   VGTT_EcuPinFaultStatus   DiagStatus    = 0U;
   Std_ReturnType           retVal        = RTE_E_INVALID;
   Std_ReturnType           returnVal     = RTE_E_INVALID;
   //! ##### Rte_call to Get DutyCycle , Period and DiagStatus
   retVal = Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(DoPinRef,
                                                          &IsDoAct,
                                                          &DoPinVolt,
                                                          &BatteryVolt,
                                                          &DutyCycle,
                                                          &Period,
                                                          &DiagStatus);
   //! ##### if rte call returns with success then fill the data buffer with Activation , period and DutyCycle
   if (RTE_E_OK == retVal)
   {
      Data[0] = (uint8)DoPinVolt; 
      Data[1] = (uint8)(((uint16)Period>>8) & (uint16)0x00FF);
      Data[2] = (uint8)((uint16)Period & (uint16)0x00FF);
      Data[3] = (uint8)DutyCycle;
      returnVal = RTE_E_OK;
   }
	//! #### Else return Error NOT_OK
   else
   {
      returnVal = RTE_E_DataServices_P1VQ3_Data_P1VQ3_E_NOT_OK;
   }
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ4_Data_P1VQ4_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData
//! 
//! \param  Data[4]    For reading Period,Dutycucle and Dopinvol
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the ReadData by id logic for DOWHS02 
   IOHWAB_UINT8           DoPinRef       = 2U;   
   IOHWAB_BOOL            IsDoAct        = 0U;
   VGTT_EcuPinVoltage_0V2 DoPinVolt      = 0U; 
   VGTT_EcuPinVoltage_0V2 BatteryVolt    = 0U; 
   VGTT_EcuPwmDutycycle   DutyCycle      = (VGTT_EcuPwmDutycycle)0U; 
   VGTT_EcuPwmPeriod      Period         = 0U; 
   VGTT_EcuPinFaultStatus DiagStatus     = 0U;
   Std_ReturnType         retVal         = RTE_E_INVALID;
   Std_ReturnType         returnVal      = RTE_E_INVALID;
   //! ##### Rte_call to Get DutyCycle , Period and DiagStatus
   retVal = Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(DoPinRef,
                                                          &IsDoAct,
                                                          &DoPinVolt,
                                                          &BatteryVolt,
                                                          &DutyCycle,
                                                          &Period,
                                                          &DiagStatus);
   //! ##### if rte call returns with success then fill the data buffer with Activation , period and DutyCycle
   if (RTE_E_OK == retVal)
   {
      Data[0]   = (uint8)DoPinVolt; 
      Data[1]   = (uint8)(((uint16)Period>>8) & (uint16)0x00FF);
      Data[2]   = (uint8)((uint16)Period & (uint16)0x00FF);
      Data[3]   = (uint8)DutyCycle;
      returnVal = RTE_E_OK;
   }
	//! #### Else return Error NOT_OK
   else
   {
      returnVal = RTE_E_DataServices_P1VQ4_Data_P1VQ4_E_NOT_OK;
   }
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ5_Data_P1VQ5_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData
//! 
//! \param  Data[0]  For reading Dopinvoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the ReadData by id logic for DOBHS01 
   IOHWAB_BOOL            IsDoAct       = 0U;
   VGTT_EcuPinVoltage_0V2 DoPinVolt     = 0U; 
   VGTT_EcuPinVoltage_0V2 BatteryVolt   = 0U; 
   VGTT_EcuPinFaultStatus DiagStatus    = 0U;
   Std_ReturnType         retVal        = RTE_E_INVALID;
   Std_ReturnType         returnVal     = RTE_E_INVALID;
   //! ##### Rte call to read DoPinVolt
   retVal = Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(1U,
                                                              &IsDoAct,
                                                              &DoPinVolt,
                                                              &BatteryVolt,
                                                              &DiagStatus);
   //! ##### if rte call return RTE_E_ok then write DoPinVolt to data buffer
   if (RTE_E_OK == retVal)
   {
      Data[0]   = (uint8)DoPinVolt;
      returnVal = RTE_E_OK;
   }
	//! #### Else return Error NOT_OK
   else
   {
      returnVal = RTE_E_DataServices_P1VQ5_Data_P1VQ5_E_NOT_OK;
   }    
   return returnVal;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ6_Data_P1VQ6_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData'
//! 
//! \param  Data[0]  For reading Dopinvoltage
//! 
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the ReadData by id logic for DOBHS02
   IOHWAB_BOOL            IsDoAct      = 0U;
   VGTT_EcuPinVoltage_0V2 DoPinVolt    = 0U; 
   VGTT_EcuPinVoltage_0V2 BatteryVolt  = 0U; 
   VGTT_EcuPinFaultStatus DiagStatus   = 0U;
   Std_ReturnType         retVal       = RTE_E_INVALID;
   Std_ReturnType         returnVal    = RTE_E_INVALID;
   //! ##### Rte call to read DoPinVolt
   retVal =  Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(2U,
                                                               &IsDoAct,
                                                               &DoPinVolt,
                                                               &BatteryVolt,
                                                               &DiagStatus);
   //! ##### if rte call return RTE_E_ok then write DoPinVolt to data buffer
   if (RTE_E_OK == retVal)
   {
      Data[0]   = DoPinVolt;
      returnVal = RTE_E_OK;
   }
	//! #### Else return Error NOT_OK
   else
   {
      returnVal = RTE_E_DataServices_P1VQ6_Data_P1VQ6_E_NOT_OK;
   }
   return returnVal;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ7_Data_P1VQ7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData
//! 
//! \param  Data[0]  For reading Dopinvoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the ReadData by id logic for DOBHS03
   IOHWAB_BOOL            IsDoAct      = 0U;
   VGTT_EcuPinVoltage_0V2 DoPinVolt    = 0U; 
   VGTT_EcuPinVoltage_0V2 BatteryVolt  = 0U; 
   VGTT_EcuPinFaultStatus DiagStatus   = 0U;
   Std_ReturnType         retVal       = RTE_E_INVALID;
   Std_ReturnType         returnVal    = RTE_E_INVALID;
   //! ##### Rte call to read DoPinVolt  
   retVal = Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(3U,
                                                              &IsDoAct,
                                                              &DoPinVolt,
                                                              &BatteryVolt,
                                                              &DiagStatus);
   //! ##### if rte call return RTE_E_ok then write DoPinVolt to data buffer
   if (RTE_E_OK == retVal)
   {
      Data[0]   = DoPinVolt;
      returnVal = RTE_E_OK;
   }
	//! #### Else return Error NOT_OK
   else
   {
      returnVal = RTE_E_DataServices_P1VQ7_Data_P1VQ7_E_NOT_OK;
   }
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ8_Data_P1VQ8_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData
//! 
//! \param  Data[0]  For reading Dopinvoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the ReadData by id logic for DOBHS04 
   IOHWAB_BOOL IsDoAct                 = 0U;
   VGTT_EcuPinVoltage_0V2 DoPinVolt    = 0U; 
   VGTT_EcuPinVoltage_0V2 BatteryVolt  = 0U; 
   VGTT_EcuPinFaultStatus DiagStatus   = 0U;
   Std_ReturnType         retVal       = RTE_E_INVALID;
   Std_ReturnType         returnVal    = RTE_E_INVALID;
   //! ##### Rte call to read DoPinVolt
   retVal = Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(4U,
                                                              &IsDoAct,
                                                              &DoPinVolt,
                                                              &BatteryVolt,
                                                              &DiagStatus);
   //! ##### if rte call return RTE_E_ok then write DoPinVolt to data buffer
   if (RTE_E_OK == retVal)
   {
      Data[0]   = DoPinVolt;
      returnVal = RTE_E_OK;
   }
	//! #### Else return Error NOT_OK
   else
   {
      returnVal = RTE_E_DataServices_P1VQ8_Data_P1VQ8_E_NOT_OK;
   }
   return returnVal;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ9_Data_P1VQ9_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData
//! 
//! \param  Data[0]  For reading AdiPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the ReadData by id logic for ADI01
   IOHWAB_UINT8           AdiPinRef           = 1U;
   VGTT_EcuPinVoltage_0V2 AdiPinVoltage       = 0U;
   VGTT_EcuPinVoltage_0V2 BatteryVoltage      = 0U;
   VGTT_EcuPinFaultStatus FaultStatus         = 0U;
   Std_ReturnType         retVal              = RTE_E_INVALID;
   Std_ReturnType         returnVal           = RTE_E_INVALID;
   //! ##### Rte call to read AdiPinVoltage
   retVal = Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);   
   //! ##### if rte call return RTE_E_ok then write AdiPinVoltage to data buffer
   if (RTE_E_OK == retVal)
   {
      Data[0]   = (uint8)AdiPinVoltage;
      returnVal = RTE_E_OK;
   }
	//! #### Else return Error NOT_OK
   else
   {
      returnVal = RTE_E_DataServices_P1VQ9_Data_P1VQ9_E_NOT_OK;
   }
   return returnVal;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_FreezeCurrentState_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the for the DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_FreezeCurrentState
//! 
//! \param  ErrorCode[1]   For reading error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/   
   Std_ReturnType retval = RTE_E_OK;
   //! ##### Filling data buffer with  Freeze current state for DOWHS01
   if (ErrorCode != NULL_PTR)
   {
      *ErrorCode = DCM_E_SUBFUNCTIONNOTSUPPORTED;
      retval     = RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK;
   }
	//! #### Else return Error NOT_OK
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData
//! 
//! \param  Data[4]   For reading Period,Dutycycle and Activation status
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL_ReadData for DOWHS01
   IOHWAB_UINT8           DoPinRef        = 1U;   
   IOHWAB_BOOL            IsDoAct         = 0U;
   VGTT_EcuPinVoltage_0V2 DoPinVolt       = 0U; 
   VGTT_EcuPinVoltage_0V2 BatteryVolt     = 0U; 
   VGTT_EcuPwmDutycycle   DutyCycle       = (VGTT_EcuPwmDutycycle)0U; 
   VGTT_EcuPwmPeriod      Period          = 0U; 
   VGTT_EcuPinFaultStatus DiagStatus      = 0U;
   Std_ReturnType         retVal          = RTE_E_INVALID;
   Std_ReturnType         returnVal       = RTE_E_INVALID;
   //! ##### Rtecall to read Period, Dutycycle and DiagStatus
   retVal = Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(DoPinRef,
                                                          &IsDoAct,
                                                          &DoPinVolt,
                                                          &BatteryVolt,
                                                          &DutyCycle,
                                                          &Period,
                                                          &DiagStatus);
   //! ##### if rte call returns successfully then start processing Period, Dutycycle
   if (RTE_E_OK == retVal)
   {
      if (IrvDowhs1_Diag0Requested == 1U)
      {
         if (IrvDowhs1_0Activation == 0U)
         {
            Data[0] = 0U; //Activation -- 1 Byte
         }
         else
         {
            Data[0] = 1U;
         }
         Data[1] = 0U;
         Data[2] = 0U;
      }
      else
      {
         Data[0] = 0x01; //Activation -- 1 Byte
         Data[1] = (uint8)(((uint16)Period>>8) & (uint16)0x00FF);
         Data[2] = (uint8)((uint16)Period & (uint16)0x00FF);
      }
      Data[3]   = (uint8)DutyCycle;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK;
   }


   return returnVal;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReturnControlToECU_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReturnControlToECU
//! 
//! \param  ErrorCode    For reading error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL ReturnControlToECU for Dowhs01
   IOCtrlReq_T           IOCtrlReqType   = IOCtrl_DiagReturnCtrlToApp;
   IOHWAB_UINT8          OutputId        = 1U; // for DOWHS1
   VGTT_EcuPwmPeriod     Period          = 0U;
   VGTT_EcuPwmDutycycle  DutyCycle       = (VGTT_EcuPwmDutycycle)0U;
   Std_ReturnType        retVal          = RTE_E_INVALID;
   Std_ReturnType        returnVal       = RTE_E_INVALID;
   //! ##### Processing the RTE call for Dowhs interface:'Rte_Call_DowhsInterface_P_SetDowActive_CS()'
   retVal = Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrlReqType,
                                                      OutputId,
                                                      Period,
                                                      DutyCycle,
                                                      0U);
   //! ##### if rte call returns successfully then fill the Data  buffer with Period Dutycycle                                                    0U);   
   if (RTE_E_OK == retVal)
   {
      IrvDowhs1_Diag0Requested = 0U;
      returnVal                = RTE_E_OK;
   }
	//! #### Else return Error NOT_OK
   else
   {
      returnVal = RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK;
   }

   return returnVal;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment
//! 
//! \param  Data[4]     For Writing shortTermAdjustment Data 
//! \param  ErrorCode   For reading error code 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL ShortTermAdjustment for DOWHS01
   IOCtrlReq_T           IOCtrlReqType    = IOCtrl_DiagShortTermAdjust;
   IOHWAB_UINT8          OutputId         = 1U; // for DOWHS1  
   VGTT_EcuPwmPeriod     Period           = 0U; 
   VGTT_EcuPwmDutycycle  DutyCycle        = (VGTT_EcuPwmDutycycle)0U;
   IOHWAB_UINT8          Activation       = 0U;
   Std_ReturnType        retVal           = RTE_E_INVALID;
   Std_ReturnType        returnVal        = RTE_E_INVALID;
   //! ##### reading Period and dutycycle from data buffer from Const DataPtr argument   
   Activation = Data[0];
   if (Activation == 1U)
   {
      if ((0U != Data[1]) 
         || (0U != Data[2]))
      {
         Period                   = (uint16) ((((uint16)Data[1]<<8)& (uint16)0xFF00)|(( (uint16)Data[2])& (uint16)0x00FF));
         IrvDowhs1_Diag0Requested = 0U;
         IrvDowhs1_0Activation    = 0U;
      }
      else
      {
         IrvDowhs1_Diag0Requested = 1U;
         IrvDowhs1_0Activation    = 1U;
      }
      DutyCycle  = (VGTT_EcuPwmDutycycle)Data[3];       
   }
   else
   {
       DutyCycle                = (VGTT_EcuPwmDutycycle)0U;
       IrvDowhs1_Diag0Requested = 1U;
   }
   //! ##### Rte call to setDowActive and provide Period and Dutycycle value
   retVal = Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrlReqType,
                                                      OutputId,
                                                      Period,
                                                      DutyCycle,
                                                      Activation);
   if (RTE_E_OK == retVal)
   {
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK;
   }   
   return returnVal;


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_FreezeCurrentState_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_FreezeCurrentState
//! 
//! \param  ErrorCode[1]    For reading error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/
   Std_ReturnType retval = RTE_E_OK;
   //! ##### Filling data buffer with  Freeze current state for DOWHS02
   if (ErrorCode != NULL_PTR)
   {
      *ErrorCode = DCM_E_SUBFUNCTIONNOTSUPPORTED;
      retval     = RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK;
   }
   else
   {
      retval = RTE_E_INVALID;  
   }
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData
//! 
//! \param  Data[4]  For Writing short term adjustment data
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL ReadData for DOWHS02
   IOHWAB_UINT8           DoPinRef         = 2U;
   IOHWAB_BOOL            IsDoAct          = 0U;
   VGTT_EcuPinVoltage_0V2 DoPinVolt        = 0U; 
   VGTT_EcuPinVoltage_0V2 BatteryVolt      = 0U; 
   VGTT_EcuPwmDutycycle   DutyCycle        = (VGTT_EcuPwmDutycycle)0U; 
   VGTT_EcuPwmPeriod      Period           = 0U; 
   VGTT_EcuPinFaultStatus DiagStatus       = 0U;
   Std_ReturnType         retVal           = RTE_E_INVALID;
   Std_ReturnType         returnVal        = RTE_E_INVALID;   
   //! ##### rte call to read Period , Dutycycle and DiagStatus
   retVal = Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(DoPinRef,
                                                          &IsDoAct,
                                                          &DoPinVolt,
                                                          &BatteryVolt,
                                                          &DutyCycle,
                                                          &Period,
                                                          &DiagStatus);
   //! ##### if Rte call returns successfully then write Period and Data cycle to Buffer
   if (RTE_E_OK == retVal)
   {   
      if ( IrvDowhs2_Diag0Requested == 1U)
      {
         if (IrvDowhs2_0Activation == 0U)
         {
            Data[0] = 0U; //Activation -- 1 Byte
         }
         else
         {
            Data[0] = 1U;
         }
         Data[1] = 0U;
         Data[2] = 0U;
      }
      else
      {
         Data[0] = 0x01; //Activation -- 1 Byte
         Data[1] = (uint8)(((uint16)Period>>8) & (uint16)0x00FF);
         Data[2] = (uint8)((uint16)Period & (uint16)0x00FF);
      }
      Data[3]   = (uint8)DutyCycle;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK;
   }    
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReturnControlToECU_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReturnControlToECU
//! 
//! \param  ErrorCode[1]    For reading Error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL ReturnControlToECU for DOWHS02
   IOCtrlReq_T          IOCtrlReqType      = IOCtrl_DiagReturnCtrlToApp;
   IOHWAB_UINT8         OutputId           = 2U; // for DOWHS2
   VGTT_EcuPwmPeriod    Period             = 0U;
   VGTT_EcuPwmDutycycle DutyCycle          = (VGTT_EcuPwmDutycycle)0U;
   Std_ReturnType       retVal             = RTE_E_INVALID;
   Std_ReturnType       returnVal          = RTE_E_INVALID;
   //! ##### Giving control back to ecu and setting Default Period and Dutycycle
   retVal = Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrlReqType,
                                                      OutputId,
                                                      Period,
                                                      DutyCycle,
                                                      0U);
   if (RTE_E_OK == retVal)
   {
      IrvDowhs2_Diag0Requested = 0U;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment
//! 
//! \param  Data[4]       For writing shortTermAdjustment data
//! \param  ErrorCode     For reading error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL ShortTermAdjustment DOWHS02
   IOCtrlReq_T          IOCtrlReqType  = IOCtrl_DiagShortTermAdjust;
   IOHWAB_UINT8         OutputId       = 2U; // for DOWHS2  
   IOHWAB_UINT8         Activation     = 0U;
   VGTT_EcuPwmPeriod    Period         = 0U; 
   VGTT_EcuPwmDutycycle DutyCycle      = (VGTT_EcuPwmDutycycle)0U;
   Std_ReturnType       retVal         = RTE_E_INVALID;
   Std_ReturnType       returnVal      = RTE_E_INVALID;
   //! ##### Reading Period and DutyCycle values from Const DataPtr argument  
   Activation = Data[0];
   if (Activation == 1U)
   {
      if ((Data[1] != 0U) 
         || (Data[2] != 0U))
      {
         Period                   = (VGTT_EcuPwmPeriod) (((uint16)((uint16)Data[1]<<8)& (uint16) 0xFF00)|( (uint16)(Data[2])& (uint16)0x00FF));
         IrvDowhs2_Diag0Requested = 0U;
         IrvDowhs2_0Activation    = 0U;
      }
      else
      {
         IrvDowhs2_Diag0Requested = 1U;
         IrvDowhs2_0Activation    = 1U;
      }
      DutyCycle  = (uint8)  Data[3];
   }
   else
   {
      DutyCycle                = (VGTT_EcuPwmDutycycle)0U;
      IrvDowhs2_Diag0Requested = 1U;
   }
   //! ##### Rte call to set Period , Dutycycle and changing IOCtrlReqType
   retVal = Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrlReqType,
                                                      OutputId,
                                                      Period,
                                                      DutyCycle, 
                                                      Activation);
   if (RTE_E_OK == retVal)
   { 
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK;
   }  
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_FreezeCurrentState_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_FreezeCurrentState
//! 
//! \param  ErrorCode[1]    For reading Error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL FreezeCurrentState for DOBHS01
   *ErrorCode = DCM_E_SUBFUNCTIONNOTSUPPORTED;
   return RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData
//! 
//! \param  Data[1]   For reading IsDoActivated
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL  ReadData for DOBHS01
   IOHWAB_UINT8           DoPinRef       = 1U; 
   IOHWAB_BOOL            IsDoActivated  = 0U;
   VGTT_EcuPinVoltage_0V2 DoPinVoltage   = 0U;
   VGTT_EcuPinVoltage_0V2 BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus DiagStatus     = 0U;
   Std_ReturnType         retVal         = RTE_E_INVALID;
   Std_ReturnType         returnVal      = RTE_E_INVALID;
   //! ##### Rte call to read DoPinVoltage   
   retVal = Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(DoPinRef,
                                                              &IsDoActivated,
                                                              &DoPinVoltage,
                                                              &BatteryVoltage,
                                                              &DiagStatus);
   //! ##### if rte call returns successfully then fill databuffer with DoPinVoltage    
   if (RTE_E_OK == retVal)
   {       
      Data[0]   = (uint8)IsDoActivated;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK;
   }   
   return returnVal;


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReturnControlToECU_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReturnControlToECU
//! 
//! \param  ErrorCode[1]   For reading Error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL ReturnControlToECU for DOBHS01
   IOCtrlReq_T    IOCtrlReqType  = IOCtrl_DiagReturnCtrlToApp;
   IOHWAB_BOOL    Activation     = 0U;
   Std_ReturnType retVal         = RTE_E_INVALID;
   Std_ReturnType returnVal      = RTE_E_INVALID;
   //! ##### rte call to set IOCtrl_DiagReturnCtrlToApp request and Activation
   retVal = Rte_Call_DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReqType,
                                                              Activation);    
   if (RTE_E_OK == retVal)
   { 
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment
//! 
//! \param  Data[1]     For writing Activation data
//! \param  ErrorCode   For reading error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL ShortTermAdjustment for DOBHS01
   IOCtrlReq_T     IOCtrlReqType   = IOCtrl_DiagShortTermAdjust;
   IOHWAB_BOOL     Activation      = Data[0];
   Std_ReturnType  retVal          = RTE_E_INVALID;
   Std_ReturnType  returnVal       = RTE_E_INVALID;
   //! ##### rte call to set IOCtrl_DiagShortTermAdjust request and Activation
   retVal = Rte_Call_DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReqType,
                                                              Activation);
   if (RTE_E_OK == retVal)
   {
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_FreezeCurrentState_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_FreezeCurrentState
//! 
//! \param  ErrorCode[1]   For reading Error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL FreezeCurrentState for DOBHS02
   *ErrorCode = DCM_E_SUBFUNCTIONNOTSUPPORTED;
   return RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData
//! 
//! \param  Data[1]    For reading IsDoActivated status
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL ReadData for DOBHS02
   IOHWAB_UINT8           DoPinRef        = 2U; 
   IOHWAB_BOOL            IsDoActivated   = 0U;
   VGTT_EcuPinVoltage_0V2 DoPinVoltage    = 0U;
   VGTT_EcuPinVoltage_0V2 BatteryVoltage  = 0U;
   VGTT_EcuPinFaultStatus DiagStatus      = 0U;
   Std_ReturnType         retVal          = RTE_E_INVALID;
   Std_ReturnType         returnVal       = RTE_E_INVALID;
   //! ##### Rte call to read IsDoActivated   
   retVal = Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(DoPinRef,
                                                              &IsDoActivated,
                                                              &DoPinVoltage,
                                                              &BatteryVoltage,
                                                              &DiagStatus);
   //! ##### filling databuffer with DoPinVoltage     
   if (RTE_E_OK == retVal)
   {       
      Data[0]   = (uint8)IsDoActivated;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReturnControlToECU_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReturnControlToECU
//! 
//! \param  ErrorCode[1]   For reading Error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL ReturnControlToECU for DOBHS02
   IOCtrlReq_T    IOCtrlReqType = IOCtrl_DiagReturnCtrlToApp; 
   IOHWAB_BOOL    Activation    = 0U;
   Std_ReturnType retVal        = RTE_E_INVALID;
   Std_ReturnType returnVal     = RTE_E_INVALID;
   //! ##### Rte call to set IOCtrl_DiagReturnCtrlToApp request and Activation
   retVal = Rte_Call_DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReqType,
                                                              Activation);
   if (RTE_E_OK == retVal)
   {   
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment
//! 
//! \param  Data[0]      For shortTermAdjustment Activation request
//! \param  ErrorCode    For reading Error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements logic for DataServices Diag_IOCTL ShortTermAdjustment for DOBHS02
   IOCtrlReq_T     IOCtrlReqType = IOCtrl_DiagShortTermAdjust;
   IOHWAB_BOOL     Activation    = Data[0];
   Std_ReturnType  retVal        = RTE_E_INVALID;
   Std_ReturnType  returnVal     = RTE_E_INVALID;
   //! ##### Rte call to set IOCtrlReqType and activation
   retVal = Rte_Call_DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReqType,
                                                              Activation);
   if (RTE_E_OK == retVal)
   {
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_FreezeCurrentState_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_FreezeCurrentState
//! 
//! \param  ErrorCode[1]    For reading Error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/   
   Std_ReturnType retval = RTE_E_OK;
   //! ##### Filling data buffer with  Freeze current state for DOBHS03
   if (ErrorCode != NULL_PTR)
   {
      *ErrorCode = DCM_E_SUBFUNCTIONNOTSUPPORTED;
      retval     = RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK;
   }
   else
   {
      retval = RTE_E_INVALID;         
   }
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData
//! 
//! \param  Data[1]    For reading IsDoActivated
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL ReadData for DOBHS03
   IOHWAB_UINT8            DoPinRef       = 3U; 
   IOHWAB_BOOL             IsDoActivated  = 0U;
   VGTT_EcuPinVoltage_0V2  DoPinVoltage   = 0U;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus  DiagStatus     = 0U;
   Std_ReturnType          retVal         = RTE_E_INVALID;
   Std_ReturnType          returnVal      = RTE_E_INVALID;
   //! ##### Rte call to read DoPinVoltage
   retVal = Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(DoPinRef,
                                                              &IsDoActivated,
                                                              &DoPinVoltage,
                                                              &BatteryVoltage,
                                                              &DiagStatus);
   //! ##### if rte call returns successfully then fill the Databuffer with DoPinVoltage
   if (RTE_E_OK == retVal)
   {
      Data[0]   = (uint8)IsDoActivated;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReturnControlToECU_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReturnControlToECU
//! 
//! \param  ErrorCode[1]    For reading error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL ReturnControlToECU for DOBHS03
   IOCtrlReq_T      IOCtrlReqType = IOCtrl_DiagReturnCtrlToApp;
   IOHWAB_BOOL      Activation    = 0U;
   Std_ReturnType   retVal        = RTE_E_INVALID;
   Std_ReturnType   returnVal     = RTE_E_INVALID;
   //! ##### Rte call to set IOCtrl_DiagReturnCtrlToApp request and Activation
   retVal =  Rte_Call_DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReqType,
                                                               Activation);     
   if (RTE_E_OK == retVal)
   {
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment
//! 
//! \param  Data[1]      For ShortTermAdjustment of Activation
//! \param  ErrorCode    For reading error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for DataServices Diag_IOCTL ShortTermAdjustment for DOBHS03
   IOCtrlReq_T     IOCtrlReqType = IOCtrl_DiagShortTermAdjust;
   IOHWAB_BOOL     Activation    = Data[0];
   Std_ReturnType  retVal        = RTE_E_INVALID;
   Std_ReturnType  returnVal     = RTE_E_INVALID;
   //! ##### Rte call to set IOCtrlReqType request and Activation
   retVal = Rte_Call_DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReqType,
                                                              Activation);     
   if (RTE_E_OK == retVal)
   {
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK;
   }   
   return returnVal;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_FreezeCurrentState_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_FreezeCurrentState
//! 
//! \param  ErrorCode[1]   For reading error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/  
   Std_ReturnType retval = RTE_E_OK;
   //! ##### Filling data buffer with  Freeze current state for DOBHS04
   if (ErrorCode != NULL_PTR)
   {
      *ErrorCode = DCM_E_SUBFUNCTIONNOTSUPPORTED;
      retval     = RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK;
   }
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData
//! 
//! \param  Data[1]    For reading IsDoActivated
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL ReadData for DOBHS04
   IOHWAB_UINT8            DoPinRef       = 4U; 
   IOHWAB_BOOL             IsDoActivated  = 0U;
   VGTT_EcuPinVoltage_0V2  DoPinVoltage   = 0U;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus  DiagStatus     = 0U;
   Std_ReturnType          retVal         = RTE_E_INVALID;
   Std_ReturnType          returnVal      = RTE_E_INVALID;
   //! ##### Rte call to read DoPinVoltage
   retVal = Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(DoPinRef,
                                                              &IsDoActivated,
                                                              &DoPinVoltage,
                                                              &BatteryVoltage,
                                                              &DiagStatus);
   //! ##### if rte call returns successfully then fill the Databuffer with DoPinVoltage  
   if (RTE_E_OK == retVal)
   {
      Data[0]   = (uint8)IsDoActivated;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReturnControlToECU_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReturnControlToECU
//! 
//! \param  ErrorCode[1]   For reading error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL ReturnControlToECU for DOBHS04
   IOCtrlReq_T     IOCtrlReqType = IOCtrl_DiagReturnCtrlToApp;
   IOHWAB_BOOL     Activation    = 0U;
   Std_ReturnType  retVal        = RTE_E_INVALID;
   Std_ReturnType  returnVal     = RTE_E_INVALID;
   //! ##### Rte call to set IOCtrl_DiagReturnCtrlToApp request and Activation
   retVal = Rte_Call_DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReqType,
                                                              Activation);
   if (RTE_E_OK == retVal)
   {
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment
//! 
//! \param  Data[1]     For Short term adjustment of Activation
//! \param  ErrorCode   For reading error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL ShortTermAdjustment for DOBHS04
   IOCtrlReq_T     IOCtrlReqType = IOCtrl_DiagShortTermAdjust;
   IOHWAB_BOOL     Activation    = Data[0];
   Std_ReturnType  retVal        = RTE_E_INVALID;
   Std_ReturnType  returnVal     = RTE_E_INVALID;
   //! ##### Rte call to set IOCtrlReqType and activation
   retVal =   Rte_Call_DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReqType,
                                                                Activation);
   if (RTE_E_OK == retVal)
   {  
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRA_Data_P1VRA_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData
//! 
//! \param  Data[1]    for Reading AdiPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadById ADI02
   IOHWAB_UINT8            AdiPinRef      = 2U;
   VGTT_EcuPinVoltage_0V2  AdiPinVoltage  = 0U ;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus  FaultStatus    = 0U;
   Std_ReturnType          retVal         = RTE_E_INVALID;
   Std_ReturnType          returnVal      = RTE_E_INVALID;
   //! ##### Rte call to read AdiPinVoltage
   retVal = Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   //! ##### if rte call returns successfully then fill the Data  buffer with AdiPinVoltage
   if (RTE_E_OK == retVal)
   {    
      Data[0]   = (uint8)AdiPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRA_Data_P1VRA_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRB_Data_P1VRB_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the  DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData
//! 
//! \param  Data[1]    To read AdiPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadById ADI03
   IOHWAB_UINT8             AdiPinRef      = 3U;
   VGTT_EcuPinVoltage_0V2   AdiPinVoltage  = 0U;
   VGTT_EcuPinVoltage_0V2   BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus   FaultStatus    = 0U;
   Std_ReturnType           retVal         = RTE_E_INVALID;
   Std_ReturnType           returnVal      = RTE_E_INVALID;
   //! ##### Rte  call to read AdiPinVoltage
   retVal = Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   //! ##### if rte call returns successfully then fill the Data  buffer with AdiPinVoltage
   if (RTE_E_OK == retVal)
   {
      Data[0]   = (uint8)AdiPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRB_Data_P1VRB_E_NOT_OK;
   }
   return returnVal;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRC_Data_P1VRC_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData
//! 
//! \param  Data[1]     To read AdiPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadById ADI04
   IOHWAB_UINT8            AdiPinRef      = 4U;
   VGTT_EcuPinVoltage_0V2  AdiPinVoltage  = 0U ;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus  FaultStatus    = 0U;
   Std_ReturnType          retVal         = RTE_E_INVALID;
   Std_ReturnType          returnVal      = RTE_E_INVALID;
   //! ##### Rte call to read AdiPinVoltage
   retVal = Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   //! ##### if rte call returns successfully then fill the Data  buffer with AdiPinVoltage 
   if (RTE_E_OK == retVal)
   {
      Data[0]   = (uint8)AdiPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal =RTE_E_DataServices_P1VRC_Data_P1VRC_E_NOT_OK;
   }   
   return returnVal;  
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRD_Data_P1VRD_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData
//! 
//! \param  Data[1]    To read AdiPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadById ADI05
   IOHWAB_UINT8            AdiPinRef      = 5U;
   VGTT_EcuPinVoltage_0V2  AdiPinVoltage  = 0U ;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus  FaultStatus    = 0U;
   Std_ReturnType          retVal         = RTE_E_INVALID;
   Std_ReturnType          returnVal      = RTE_E_INVALID;
   //! ##### Rte  call to read AdiPinVoltage
   retVal = Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   //! ##### if rte call returns successfully then fill the Data  buffer with AdiPinVoltage    
   if (RTE_E_OK == retVal)
   {
      Data[0] = (uint8)AdiPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRD_Data_P1VRD_E_NOT_OK;
   }   
   return returnVal;  
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRE_Data_P1VRE_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData
//! 
//! \param  Data[1]    To read AdiPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadById ADI06
   IOHWAB_UINT8            AdiPinRef      = 6U;
   VGTT_EcuPinVoltage_0V2  AdiPinVoltage  = 0U ;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus  FaultStatus    = 0U;
   Std_ReturnType          retVal         = RTE_E_INVALID;
   Std_ReturnType          returnVal      = RTE_E_INVALID;
   //! ##### Rte  call to read AdiPinVoltage
   retVal = Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   //! ##### if rte call returns successfully then fill the Data  buffer with AdiPinVoltage
   if (RTE_E_OK == retVal)
   {   
      Data[0]   = (uint8)AdiPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRE_Data_P1VRE_E_NOT_OK;
   }   
   return returnVal;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRF_Data_P1VRF_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData
//! 
//! \param  Data[1]   To read AdiPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadById ADI07
   IOHWAB_UINT8            AdiPinRef      = 7U;
   VGTT_EcuPinVoltage_0V2  AdiPinVoltage  = 0U ;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus  FaultStatus    = 0U;
   Std_ReturnType          retVal         = RTE_E_INVALID;
   Std_ReturnType          returnVal      = RTE_E_INVALID;
   //! ##### Rte  call to read AdiPinVoltage
   retVal = Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   //! ##### if rte call returns successfully then fill the Data  buffer with AdiPinVoltage 
   if (RTE_E_OK == retVal)
   {    
      Data[0]   = (uint8)AdiPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRF_Data_P1VRF_E_NOT_OK;
   }
   return returnVal;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRG_Data_P1VRG_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData
//! 
//! \param  Data[1]     To read AdiPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadById ADI08
   IOHWAB_UINT8            AdiPinRef      = 8U;
   VGTT_EcuPinVoltage_0V2  AdiPinVoltage  = 0U ;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus  FaultStatus    = 0U;
   Std_ReturnType          retVal         = RTE_E_INVALID;
   Std_ReturnType          returnVal      = RTE_E_INVALID;
   //! ##### Rte  call to read AdiPinVoltage
   retVal = Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus); 
   //! ##### if rte call returns successfully then fill the Data  buffer with AdiPinVoltage
   if (RTE_E_OK == retVal)
   {
      Data[0]   = (uint8)AdiPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRG_Data_P1VRG_E_NOT_OK;
   }
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRH_Data_P1VRH_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData
//! 
//! \param  Data[0]   To read AdiPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadById ADI09
   IOHWAB_UINT8            AdiPinRef      = 9U;           
   VGTT_EcuPinVoltage_0V2  AdiPinVoltage  = 0U ;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus  FaultStatus    = 0U;
   Std_ReturnType          retVal         = RTE_E_INVALID;
   Std_ReturnType          returnVal      = RTE_E_INVALID;
   //! ##### Rte  call to read AdiPinVoltage
   retVal = Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus); 
   //! ##### if rte call returns successfully then fill the Data  buffer with AdiPinVoltage
   if (RTE_E_OK == retVal)
   {   
      Data[0]   = (uint8)AdiPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRH_Data_P1VRH_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRI_Data_P1VRI_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData
//! 
//! \param  Data[1]   To read AdiPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadById ADI10
   IOHWAB_UINT8            AdiPinRef      = 10U;
   VGTT_EcuPinVoltage_0V2  AdiPinVoltage  = 0U ;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus  FaultStatus    = 0U;
   Std_ReturnType          retVal         = RTE_E_INVALID;
   Std_ReturnType          returnVal      = RTE_E_INVALID;
   //! ##### Rte  call to read AdiPinVoltage
   retVal = Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);    
   //! ##### if rte call returns successfully then fill the Data  buffer with AdiPinVoltage
   if (RTE_E_OK == retVal)
   {   
      Data[0]   = (uint8)AdiPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRI_Data_P1VRI_E_NOT_OK;
   }
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRJ_Data_P1VRJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData
//! 
//! \param  Data[1]   To read AdiPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadById ADI11
   IOHWAB_UINT8            AdiPinRef      = 11U;
   VGTT_EcuPinVoltage_0V2  AdiPinVoltage  = 0U ;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus  FaultStatus    = 0U;
   Std_ReturnType          retVal         = RTE_E_INVALID;
   Std_ReturnType          returnVal      = RTE_E_INVALID;
   //! ##### Rte  call to read AdiPinVoltage
   retVal = Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   //! ##### if rte call returns successfully then fill the Data  buffer with AdiPinVoltage    
   if (RTE_E_OK == retVal)
   {
      Data[0]   = (uint8)AdiPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRJ_Data_P1VRJ_E_NOT_OK;
   }
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRK_Data_P1VRK_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData
//! 
//! \param  Data[1]   To read AdiPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadById ADI12
   IOHWAB_UINT8            AdiPinRef      = 12U;
   VGTT_EcuPinVoltage_0V2  AdiPinVoltage  = 0U ;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus  FaultStatus    = 0U;
   Std_ReturnType          retVal         = RTE_E_INVALID;
   Std_ReturnType          returnVal      = RTE_E_INVALID;
   //! ##### Rte  call to read AdiPinVoltage
   retVal = Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   //! ##### if rte call returns successfully then fill the Data  buffer with AdiPinVoltage
   if (RTE_E_OK == retVal)
   {   
      Data[0]   = (uint8)AdiPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRK_Data_P1VRK_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRL_Data_P1VRL_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData
//! 
//! \param  Data[1]    To read AdiPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadById ADI13
   IOHWAB_UINT8            AdiPinRef      = 13U;
   VGTT_EcuPinVoltage_0V2  AdiPinVoltage  = 0U ;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus  FaultStatus    = 0U;
   Std_ReturnType          retVal         = RTE_E_INVALID;
   Std_ReturnType          returnVal      = RTE_E_INVALID;
   //! ##### Rte  call to read AdiPinVoltage
   retVal = Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   //! ##### if rte call returns successfully then fill the Data  buffer with AdiPinVoltage
   if (RTE_E_OK == retVal)
   {   
      Data[0]   = (uint8)AdiPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRL_Data_P1VRL_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRM_Data_P1VRM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData
//! 
//! \param  Data[1]   To read AdiPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadById ADI14
   IOHWAB_UINT8            AdiPinRef      = 14U;
   VGTT_EcuPinVoltage_0V2  AdiPinVoltage  = 0U ;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus  FaultStatus    = 0U;
   Std_ReturnType          retVal         = RTE_E_INVALID;
   Std_ReturnType          returnVal      = RTE_E_INVALID;
   //! ##### Rte  call to read AdiPinVoltage
   retVal = Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   //! ##### if rte call returns successfully then fill the Data  buffer with AdiPinVoltage    
   if (RTE_E_OK == retVal)
   {   
      Data[0]   = (uint8)AdiPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRM_Data_P1VRM_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRN_Data_P1VRN_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData
//! 
//! \param  Data[1]   To read AdiPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadById ADI15
   IOHWAB_UINT8           AdiPinRef      = 15U;
   VGTT_EcuPinVoltage_0V2 AdiPinVoltage  = 0U ;
   VGTT_EcuPinVoltage_0V2 BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus FaultStatus    = 0U;
   Std_ReturnType         retVal         = RTE_E_INVALID;
   Std_ReturnType         returnVal      = RTE_E_INVALID;
   //! ##### Rte  call to read AdiPinVoltage
   retVal = Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   //! ##### if rte call returns successfully then fill the Data  buffer with AdiPinVoltage    
   if (RTE_E_OK == retVal)
   {
      Data[0]   = (uint8)AdiPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRN_Data_P1VRN_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRO_Data_P1VRO_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData
//! 
//! \param  Data[1]   To read AdiPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadById ADI16
   IOHWAB_UINT8            AdiPinRef      = 16U;
   VGTT_EcuPinVoltage_0V2  AdiPinVoltage  = 0U ;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus  FaultStatus    = 0U;
   Std_ReturnType          retVal         = RTE_E_INVALID;
   Std_ReturnType          returnVal      = RTE_E_INVALID;
   //! ##### Rte  call to read AdiPinVoltage
   retVal = Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   //! ##### if rte call returns successfully then fill the Data  buffer with AdiPinVoltage    
   if (RTE_E_OK == retVal)
   {   
      Data[0]   = (uint8)AdiPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRO_Data_P1VRO_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRS_Data_P1VRS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData
//! 
//! \param  Data[4]   To read DoPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for DataServices ReadById ReadData for DOWLS1
   IOHWAB_BOOL            IsDoActivated   = 0U;
   VGTT_EcuPinVoltage_0V2 DoPinVoltage    = 0U;
   VGTT_EcuPinVoltage_0V2 BatteryVoltage  = 0U;
   VGTT_EcuPinFaultStatus DiagStatus      = 0U;
   Std_ReturnType         retVal          = RTE_E_INVALID;
   Std_ReturnType         returnVal       = RTE_E_INVALID;
   //! ##### Rte call for reading Period,Dutycycle and DiagStatus    
   retVal = Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(&IsDoActivated,
                                                              &DoPinVoltage,
                                                              &BatteryVoltage,
                                                              &DiagStatus);
   //! ##### if the rte call returns successfully then fill the databuffer with DoPinVoltage
   if (RTE_E_OK == retVal)
   {
      Data[0]   = (uint8)DoPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRS_Data_P1VRS_E_NOT_OK;
   }   
   return returnVal;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRT_Data_P1VRT_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData
//! 
//! \param  Data[4]   for reading Period,Dutycycle,DoPinvoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for DataServices ReadById ReadData for DOWLS2
   IOHWAB_UINT8            DoPinRef      = 2U; // for DOWHS2
   IOHWAB_BOOL             IsDoAct       = 0U;
   VGTT_EcuPinVoltage_0V2  DoPinVolt     = 0U; 
   VGTT_EcuPinVoltage_0V2  BatteryVolt   = 0U; 
   VGTT_EcuPwmDutycycle    DutyCycle     = (VGTT_EcuPwmDutycycle)0U; 
   VGTT_EcuPwmPeriod       Period        = 0U; 
   VGTT_EcuPinFaultStatus  DiagStatus    = 0U;
   Std_ReturnType          retVal        = RTE_E_INVALID;
   Std_ReturnType          returnVal     = RTE_E_INVALID;
   //! ##### Rte call for reading Period,Dutycycle and DiagStatus 
   retVal = Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(DoPinRef,
                                                          &IsDoAct,
                                                          &DoPinVolt,
                                                          &BatteryVolt,
                                                          &DutyCycle,
                                                          &Period,
                                                          &DiagStatus);
    //! ##### if the rte call returns successfully then fill the databuffer with period and dutycycle value    
   if (RTE_E_OK == retVal)
   {    
      Data[0]   = DoPinVolt;
      Data[1]   = (uint8)(((uint16)Period>>8) & (uint16)0x00FF);
      Data[2]   = (uint8)((uint16)Period & (uint16)0x00FF);
      Data[3]   = (uint8) DutyCycle;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRT_Data_P1VRT_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRU_Data_P1VRU_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData
//! 
//! \param  Data[4]  For reading Period,Dutycycle and DoPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for DataServices ReadById ReadData for DOWLS3
   IOHWAB_UINT8            DoPinRef       = 3U;
   IOHWAB_BOOL             IsDoAct        = 0U;
   VGTT_EcuPinVoltage_0V2  DoPinVolt      = 0U; 
   VGTT_EcuPinVoltage_0V2  BatteryVolt    = 0U; 
   VGTT_EcuPwmDutycycle    DutyCycle      = (VGTT_EcuPwmDutycycle)0U; 
   VGTT_EcuPwmPeriod       Period         = 0U; 
   VGTT_EcuPinFaultStatus  DiagStatus     = 0U;
   Std_ReturnType          retVal         = RTE_E_INVALID;
   Std_ReturnType          returnVal      = RTE_E_INVALID;
   //! ##### Rte call for reading Period,Dutycycle and DiagStatus
   retVal = Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(DoPinRef,
                                                          &IsDoAct,
                                                          &DoPinVolt,
                                                          &BatteryVolt,
                                                          &DutyCycle,
                                                          &Period,
                                                          &DiagStatus);
   //! ##### if the rte call returns successfully then fill the databuffer with period and dutycycle value    
   if (RTE_E_OK == retVal)
   {    
      Data[0]   = DoPinVolt;
      Data[1]   = (uint8)(((uint16)Period>>8) & (uint16)0x00FF);
      Data[2]   = (uint8)((uint16)Period & (uint16)0x00FF);
      Data[3]   = (uint8) DutyCycle;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRU_Data_P1VRU_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetPullUpState_CS(IOHWAB_BOOL *isPullUpActive_Strong, IOHWAB_BOOL *isPullUpActive_Weak, IOHWAB_BOOL *isPullUpActive_DAI)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRV_Data_P1VRV_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData
//! 
//! \param  Data[1]   For reading PullupVoltageDai
//! 
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadById ReadData for DAI01
   Boolean        PullUpVoltage_Living = 0U;
   Boolean        PullUpVoltage_Parked = 0U;
   Boolean        PullUpVoltage_DAI    = 0U;
   Std_ReturnType retVal               = RTE_E_INVALID;
   Std_ReturnType returnVal            = RTE_E_INVALID;
   //! ##### Rte call to read PullUpVoltage_DAI
   retVal = Rte_Call_AdiInterface_P_GetPullUpState_CS(&PullUpVoltage_Living,
                                                      &PullUpVoltage_Parked,
                                                      &PullUpVoltage_DAI);
   //! ##### if rte call returns successfully then fill databuffer with PullUpVoltage_DAI  
   if (RTE_E_OK == retVal)
   {    
      Data[0]   = (uint8) PullUpVoltage_DAI;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRV_Data_P1VRV_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetPullUpState_CS(IOHWAB_BOOL *isPullUpActive_Strong, IOHWAB_BOOL *isPullUpActive_Weak, IOHWAB_BOOL *isPullUpActive_DAI)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRW_Data_P1VRW_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData
//! 
//! \param  Data[1]   For reading PullUpVoltage_DAI
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for the DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData
   Boolean        PullUpVoltage_Living = 0U;
   Boolean        PullUpVoltage_Parked = 0U;
   Boolean        PullUpVoltage_DAI    = 0U;
   Std_ReturnType retVal               = RTE_E_INVALID;
   Std_ReturnType returnVal            = RTE_E_INVALID;
   //! ##### Rte call to read PullUpVoltage_DAI
   retVal = Rte_Call_AdiInterface_P_GetPullUpState_CS(&PullUpVoltage_Living,
                                                      &PullUpVoltage_Parked,
                                                      &PullUpVoltage_DAI);
   //! ##### if rte call returns successfully then fill databuffer with PullUpVoltage_DAI  
   if (RTE_E_OK == retVal)
   {    
      Data[0]   = (uint8) PullUpVoltage_DAI;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRW_Data_P1VRW_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_FreezeCurrentState_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_FreezeCurrentState
//! 
//! \param  ErrorCode[1]   For reading Errorcode
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for FreezeCurrentState for AO12_P
   *ErrorCode = DCM_E_SUBFUNCTIONNOTSUPPORTED;
   return RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData
//! 
//! \param  Data[0]   For reading IsDoActivated
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for the DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData
   IOHWAB_UINT8           SelectParkedOrLivingPin    = 0U; // Parked
   IOHWAB_BOOL            IsDo12VActivated           = 0U;
   VGTT_EcuPinVoltage_0V2 Do12VPinVoltage            = 0U;
   VGTT_EcuPinVoltage_0V2 BatteryVoltage             = 0U;
   VGTT_EcuPinVoltage_0V2 DcdcRefVoltage             = 0U;
   VGTT_EcuPinFaultStatus DiagStatus                 = 0U;
   Std_ReturnType         retVal                     = RTE_E_INVALID;
   Std_ReturnType         returnVal                  = RTE_E_INVALID;
   //! ##### Rte call to read IsDo12VActivated status
   retVal = Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(SelectParkedOrLivingPin,
                                                           &IsDo12VActivated,
                                                           &Do12VPinVoltage,
                                                           &BatteryVoltage,
                                                           &DiagStatus);
   //! ##### if the rte call returns successfully then fill databuffer with IsDo12VActivated        
   if (RTE_E_OK == retVal)
   {
      Data[0]   = (uint8)IsDo12VActivated;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK;
   }
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU
//! 
//! \param  ErrorCode  For reading Error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for the DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU
   IOCtrlReq_T    IOCtrlReqType = IOCtrl_DiagReturnCtrlToApp;
   IOHWAB_BOOL    Activation    = 0U; 
   Std_ReturnType retVal        = RTE_E_INVALID;
   Std_ReturnType returnVal     = RTE_E_INVALID;
   //! ##### Rte call to return control to ecu
   retVal = Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReqType,
                                                              Activation);   
   if (RTE_E_OK == retVal)
   {
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment
//! 
//! \param  ErrorCode   For reading error code
//! 
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for the DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment
   IOCtrlReq_T    IOCtrlReqType = IOCtrl_DiagShortTermAdjust;
   IOHWAB_BOOL    Activation    = Data[0];
   Std_ReturnType retVal        = RTE_E_INVALID;
   Std_ReturnType returnVal     = RTE_E_INVALID;
   //! ##### Rte call to set Activation 
   retVal = Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReqType,
                                                              Activation);
   if (RTE_E_OK == retVal)
   {
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_FreezeCurrentState_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_FreezeCurrentState
//! 
//! \param  ErrorCode[1]   For reading error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/  
   Std_ReturnType retval = RTE_E_OK;
   //! ##### Filling data buffer with  Freeze current state for DOWLS01
   if (ErrorCode != NULL_PTR)
   {
      *ErrorCode = DCM_E_SUBFUNCTIONNOTSUPPORTED;
      retval     = RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK;
   }
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData
//! 
//! \param  Data[4]   For reading isDoActivated
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData (returns application error)
 *********************************************************************************************************************/
    //! ###### This function implements DataService for P1VSB
    IOHWAB_BOOL             IsDoActivated  = 0U;
    VGTT_EcuPinVoltage_0V2  DoPinVoltage   = 0U;
    VGTT_EcuPinVoltage_0V2  BatteryVoltage = 0U;
    VGTT_EcuPinFaultStatus  DiagStatus     = 0U;
    Std_ReturnType          retVal         = RTE_E_INVALID;
    Std_ReturnType          returnVal      = RTE_E_INVALID;
    //! ##### Rte call for reading IsDoActivated    
    retVal = Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(&IsDoActivated,
                                                               &DoPinVoltage,
                                                               &BatteryVoltage,
                                                               &DiagStatus);
    //! ##### If rte call returns successfully then fill data buff with IsDoActivated value       
    if (RTE_E_OK == retVal)
    {         
       Data[0]   = (uint8)IsDoActivated;
       returnVal = RTE_E_OK;
    }
    else
    {
       returnVal = RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK;
    }    
    return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReturnControlToECU_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReturnControlToECU
//! 
//! \param  ErrorCode   For reading Errorcode
//! 
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements ReturnControlToEcu service for P1VSB
   IOCtrlReq_T     IOCtrlReqType = IOCtrl_DiagReturnCtrlToApp; 
   IOHWAB_BOOL     Activation    = 0U;
   Std_ReturnType  retVal        = RTE_E_INVALID;
   Std_ReturnType  returnVal     = RTE_E_INVALID;   
   //! ##### Rte call to set activation
   retVal =  Rte_Call_DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReqType,
                                                             Activation);
   if (RTE_E_OK == retVal)
   {
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK;
   }    
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment
//! 
//! \param  Data        For getting activation request
//! \param  ErrorCode   For reading Errorcode
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/
    //! ###### This function implements the logic for Dataservice Diag_IOCTL ShortTermAdjustment for DOWLS1
   IOCtrlReq_T    IOCtrlReqType  = IOCtrl_DiagShortTermAdjust;
   IOHWAB_BOOL    Activation     = Data[0];
   Std_ReturnType retVal         = RTE_E_INVALID;
   Std_ReturnType returnVal      = RTE_E_INVALID;
   //! ##### Rte call for ShortTermAdjustment
   retVal =  Rte_Call_DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReqType,
                                                             Activation);       
   if (RTE_E_OK == retVal)
   {    
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK;
   }    
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_FreezeCurrentState_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the  logic for the DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_FreezeCurrentState
//! 
//! \param  ErrorCode[1]   For writing error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/
   //! ###### writing the Error code DCM_E_SUBFUNCTIONNOTSUPPORTED
   Std_ReturnType retval = RTE_E_OK;
   //! ##### Filling data buffer with  Freeze current state for DOWLS02
   if (ErrorCode != NULL_PTR)
   {
      *ErrorCode = DCM_E_SUBFUNCTIONNOTSUPPORTED;
      retval     = RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK;
   }
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements logic for the DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData
//! 
//! \param  Data[4]   For reading Period ,Dutycycle and Activation request
//! 
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice ReadData Dowls02
   IOHWAB_UINT8            DoPinRef    = 2U; 
   IOHWAB_BOOL             IsDoAct     = 0U;
   VGTT_EcuPinVoltage_0V2  DoPinVolt   = 0U; 
   VGTT_EcuPinVoltage_0V2  BatteryVolt = 0U; 
   VGTT_EcuPwmDutycycle DutyCycle     = (VGTT_EcuPwmDutycycle)0U; 
   VGTT_EcuPwmPeriod       Period      = 0U; 
   VGTT_EcuPinFaultStatus  DiagStatus  = 0U;
   Std_ReturnType          retVal      = RTE_E_INVALID;
   Std_ReturnType          returnVal   = RTE_E_INVALID;
   //! ##### Rte call for reading Period,Dutycycle and DiagStatus
   retVal = Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(DoPinRef,
                                                          &IsDoAct,
                                                          &DoPinVolt,
                                                          &BatteryVolt,
                                                          &DutyCycle,
                                                          &Period,
                                                          &DiagStatus);
   //! ##### if the rte call returns successfully then fill the databuffer with period and dutycycle value  
   if (RTE_E_OK == retVal)
   {      
      if (IrvDowls2_Diag0Requested == 1U)
      {
         if (IrvDowls2_0Activation == 0U)
         {
            Data[0] = 0U; //Activation -- 1 Byte
         }
         else
         {
            Data[0] = 1U;
         }
         Data[1] = 0U;
         Data[2] = 0U;
      }
      else
      {
         Data[0] = 0x01; //Activation -- 1 Byte
         Data[1] = (uint8)((Period>>8) & (uint16)0x00FF);
         Data[2] = (uint8)(Period & (uint16)0x00FF);
      }
      Data[3]   = (uint8) DutyCycle;
      returnVal = RTE_E_OK;
   }
   else
   {
       returnVal = RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReturnControlToECU_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS02_ReturnControlToECU
//! 
//! \param  ErrorCode[1]   For reading errorcode
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL ReturnControlToECU for DOWLS02
   IOCtrlReq_T           IOCtrlReqType    = IOCtrl_DiagReturnCtrlToApp;
   IOHWAB_UINT8          OutputId         = 2U; // for DOWHS2
   VGTT_EcuPwmPeriod     Period           = 0U;
   VGTT_EcuPwmDutycycle  DutyCycle        = (VGTT_EcuPwmDutycycle)0U;
   Std_ReturnType        retVal           = RTE_E_INVALID;
   Std_ReturnType        returnVal        = RTE_E_INVALID;
   //! ##### writing Period ,Dutycycle for returnControlToEcu
   IrvDowls2_Diag0Requested = 0U;
   IrvDowls2_0Activation    = 0U;
   retVal                   = Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrlReqType,
                                                                        OutputId,
                                                                        Period,
                                                                        DutyCycle,
                                                                        0U);
   if (RTE_E_OK == retVal)
   {
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS02_ShortTermAdjustment
//! 
//! \param  Data[4]     For writing ShortTermAdjustment Data
//! \param  ErrorCode   For reading Error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements logic for DataServices Diag_IOCTL ShortTermAdjustment for DOWLS02
   IOCtrlReq_T           IOCtrlReqType      = IOCtrl_DiagShortTermAdjust;
   IOHWAB_UINT8          OutputId           = 2U; // for DOWLS2
   IOHWAB_UINT8          Activation         = 0U;
   VGTT_EcuPwmPeriod     Period             = 0U;
   VGTT_EcuPwmDutycycle  DutyCycle          = (VGTT_EcuPwmDutycycle)0U;
   Std_ReturnType        retVal             = RTE_E_INVALID;
   Std_ReturnType        returnVal          = RTE_E_INVALID;
   //! ##### Reading Period and DutyCycle values from Const DataPtr argument
   Activation = Data[0];
   if(Activation == 1)
   {
      if ((Data[1] != 0) 
         || (Data[2] != 0))
      {
         Period                   =  ((((uint16)Data[1]<<8U)&0xFF00U)|(((uint16)Data[2])&0x00FFU));
         IrvDowls2_Diag0Requested = 0U;
         IrvDowls2_0Activation    = 0U;
      }
      else
      {
         IrvDowls2_Diag0Requested = 1U;
         IrvDowls2_0Activation    = 1U;
      }
      DutyCycle  = (uint8)  Data[3];       
   }
   else
   {
      DutyCycle                = (VGTT_EcuPwmDutycycle)0U;
      IrvDowls2_Diag0Requested = 1U;
   }
   //! ##### rte call for setting Dowls Active
   retVal = Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrlReqType,
                                                       OutputId,
                                                       Period,
                                                       DutyCycle, 
                                                       Activation);
   if (RTE_E_OK == retVal)
   {
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_FreezeCurrentState_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS03_FreezeCurrentState
//! 
//! \param  ErrorCode[1]   For reading Error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/   
   Std_ReturnType retval = RTE_E_OK;
   //! ##### Filling data buffer with  Freeze current state for DOWLS03
   if (ErrorCode != NULL_PTR)
   {
      *ErrorCode = DCM_E_SUBFUNCTIONNOTSUPPORTED;
      retval     = RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK;
   }
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;   
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS03_ReadData'
//! 
//! \param  Data[4]   For reading Period ,dutycycle and activation data
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData (returns application error)
 *********************************************************************************************************************/   
   IOHWAB_UINT8            DoPinRef      = 3U; 
   IOHWAB_BOOL             IsDoAct       = 0U;
   VGTT_EcuPinVoltage_0V2  DoPinVolt     = 0U; 
   VGTT_EcuPinVoltage_0V2  BatteryVolt   = 0U; 
   VGTT_EcuPwmDutycycle    DutyCycle     = (VGTT_EcuPwmDutycycle)0U; 
   VGTT_EcuPwmPeriod       Period        = 0U; 
   VGTT_EcuPinFaultStatus  DiagStatus    = 0U;
   Std_ReturnType          retVal        = RTE_E_INVALID;
   Std_ReturnType          returnVal     = RTE_E_INVALID;
   //! ##### Rte call to read Period , Dutycycle and Diagstatus
   retVal =   Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(DoPinRef,
                                                            &IsDoAct,
                                                            &DoPinVolt,
                                                            &BatteryVolt,
                                                            &DutyCycle,
                                                            &Period,
                                                            &DiagStatus);
   //! ##### if rte call return successfully then filling databuffer with  period and dutycycle   
   if (RTE_E_OK == retVal)
   {       
      if (IrvDowls3_Diag0Requested == 1U)
      {
         if (IrvDowls3_0Activation == 0U)
         {
            Data[0] = 0U; //Activation -- 1 Byte
         }
         else
         {
            Data[0] = 1U;
         }
         Data[1] = 0U;
         Data[2] = 0U;
      }
      else
      {
         Data[0] = 0x01; //Activation -- 1 Byte
         Data[1] = (uint8)((Period>>8) & (VGTT_EcuPwmPeriod)0x00FF);
         Data[2] = (uint8)(Period & (VGTT_EcuPwmPeriod)0x00FF);
      }
      Data[3]   = (uint8) DutyCycle;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK;
   }    
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReturnControlToECU_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReturnControlToECU
//! 
//! \param  ErrorCode[1]   For reading Errorcode
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/  
   IOCtrlReq_T           IOCtrlReqType   = IOCtrl_DiagReturnCtrlToApp;
   IOHWAB_UINT8          OutputId        = 3U; // for DOWHS3
   VGTT_EcuPwmPeriod     Period          = 0U;
   VGTT_EcuPwmDutycycle  DutyCycle       = (VGTT_EcuPwmDutycycle)0U;
   Std_ReturnType        retVal          = RTE_E_INVALID;
   Std_ReturnType        returnVal       = RTE_E_INVALID;
   //! ##### Rte call for returning control to ECU
   IrvDowls3_Diag0Requested = 0U;
   IrvDowls3_0Activation    = 0U;
   retVal                   = Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrlReqType,
                                                                        OutputId,
                                                                        Period,
                                                                        DutyCycle, 
                                                                        0U);
   if (RTE_E_OK == retVal)
   {
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment
//! 
//! \param  Data[4]      For reading the shortTermAdjustment data
//! \param  ErrorCode    For reading error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/   
   IOCtrlReq_T          IOCtrlReqType = IOCtrl_DiagShortTermAdjust;
   IOHWAB_UINT8         OutputId      = 3U; // for DOWLS3
   IOHWAB_UINT8         Activation    = 0U;
   VGTT_EcuPwmPeriod    Period        = 0U;
   VGTT_EcuPwmDutycycle DutyCycle     = (VGTT_EcuPwmDutycycle)0U;
   Std_ReturnType       retVal        = RTE_E_INVALID;
   Std_ReturnType       returnVal     = RTE_E_INVALID;
   //! ##### Reading Period and Dutycycle values from Const DataPtr argument
   Activation = Data[0];
   if(Activation == 1)
   {
      if ((Data[1] != 0) 
         || (Data[2] != 0))
      {
          Period                   = ((((uint16)Data[1]<<8U)&0xFF00U)|(((uint16)Data[2])&0x00FFU));
          IrvDowls3_Diag0Requested = 0U;
          IrvDowls3_0Activation    = 0U;
      }
      else
      {
         IrvDowls3_Diag0Requested = 1U;
         IrvDowls3_0Activation    = 1U;
      }
      DutyCycle  = (uint8)  Data[3];       
   }
   else
   {
      DutyCycle                = (VGTT_EcuPwmDutycycle)0U;
      IrvDowls3_Diag0Requested = 1U;
   }
   //! ##### Rte call for shorttermadjustment
   retVal = Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrlReqType,
                                                     OutputId,
                                                     Period,
                                                     DutyCycle,
                                                     Activation);
   if (RTE_E_OK == retVal)
   {
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK;
   }   
   return returnVal;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_FreezeCurrentState_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_FreezeCurrentState
//! 
//! \param  ErrorCode[1]  For writing Error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/ 
   Std_ReturnType retval = RTE_E_OK;
   //! ##### Filling the data buffer with Freezecurrent state for AO12_L
   if (ErrorCode != NULL_PTR)
   {
      *ErrorCode = DCM_E_SUBFUNCTIONNOTSUPPORTED;
      retval     = RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK;
   }
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;  
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData
//! 
//! \param  *Data    For reading IsDo12VActivated status
//! 
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ######  This function implements the logic for the DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_P_ReadData
   IOHWAB_UINT8           SelectParkedOrLivingPin    = 1U; // Living
   IOHWAB_BOOL            IsDo12VActivated           = 0U;
   VGTT_EcuPinVoltage_0V2 Do12VPinVoltage            = 0U;
   VGTT_EcuPinVoltage_0V2 BatteryVoltage             = 0U;
   VGTT_EcuPinFaultStatus DiagStatus                 = 0U;
   Std_ReturnType         retVal                     = RTE_E_INVALID;
   Std_ReturnType         returnVal                  = RTE_E_INVALID;
   //! ##### Rte call to read IsDo12VActivated
   retVal = Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(SelectParkedOrLivingPin,
                                                           &IsDo12VActivated,
                                                           &Do12VPinVoltage,
                                                           &BatteryVoltage,
                                                           &DiagStatus);
   //! ##### If rte call returns successfully then fill the data buffer with IsDo12VActivated       
   if (RTE_E_OK == retVal)
   {
      Data[0] = (uint8)IsDo12VActivated;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK;
   }
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU
//! 
//! \param  ErrorCode[1]
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for the DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU
   IOCtrlReq_T     IOCtrlReqType  = IOCtrl_DiagReturnCtrlToApp;
   IOHWAB_BOOL     Activation     = 0U; 
   Std_ReturnType  retVal         = RTE_E_INVALID;
   Std_ReturnType  returnVal      = RTE_E_INVALID;
   //! ##### Rte call for returning control to ecu
   retVal = Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReqType,
                                                              Activation);   
   if (RTE_E_OK == retVal)
   {    
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment
//! 
//! \param  ErrorCode[1]    For reading errorcode
//! 
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/  
   IOCtrlReq_T    IOCtrlReqType = IOCtrl_DiagShortTermAdjust;
   IOHWAB_BOOL    Activation    = 0U;
   Std_ReturnType retVal        = RTE_E_INVALID;
   Std_ReturnType returnVal     = RTE_E_INVALID;
   //! ##### Rte call for short term adjustment
   Activation = Data[0];   
   retVal     = Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReqType,
                                                                  Activation);
   if (RTE_E_OK == retVal)
   {       
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(VGTT_EcuPinVoltage_0V2 *DcDc12vRefVoltage, IOHWAB_BOOL *IsDcDc12vActivated, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1CV5_Data_X1CV5_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData
//! 
//! \param  Data[0]   For reading Do12VPinVoltage
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ######  This function implements the logic for the DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData
   IOHWAB_BOOL            IsDo12VActivated    = 0U;
   VGTT_EcuPinVoltage_0V2 Do12VPinVoltage     = 0U;
   VGTT_EcuPinFaultStatus DiagStatus          = 0U;
   Std_ReturnType         retVal              = RTE_E_INVALID;
   Std_ReturnType        returnVal            = RTE_E_INVALID;
   //! ##### Rte call to read Do12VPinVoltage
   retVal = Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(&Do12VPinVoltage,
                                                         &IsDo12VActivated,
                                                         &DiagStatus);
   //! ##### if rte call returns successfully then fill data buffer with Do12VPinVoltage value     
   if (RTE_E_OK == retVal)
   {
      Data[0]   = (uint8) Do12VPinVoltage;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_X1CV5_Data_X1CV5_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_FreezeCurrentState_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_FreezeCurrentState
//! 
//! \param  ErrorCode[1]   For reading Error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/ 
   Std_ReturnType retval = RTE_E_OK;
   //! ##### Filling the Databuffer with FreezeCurrentState   for DCDC12
   if (ErrorCode != NULL_PTR)
   {
      *ErrorCode = DCM_E_SUBFUNCTIONNOTSUPPORTED;
      retval     = RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK;
   }
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;   
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(VGTT_EcuPinVoltage_0V2 *DcDc12vRefVoltage, IOHWAB_BOOL *IsDcDc12vActivated, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData
//! 
//! \param  Data[1]    For reading IsDo12VActivated
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for DataServices Diag_IOCTL ReadData DCDC12
   IOHWAB_BOOL            IsDo12VActivated  = 0U;
   VGTT_EcuPinVoltage_0V2 Do12VPinVoltage   = 0U;
   VGTT_EcuPinFaultStatus DiagStatus        = 0U;
   Std_ReturnType         retVal            = RTE_E_INVALID;
   Std_ReturnType         returnVal         = RTE_E_INVALID;
   //! ##### Rte call to read Do12VPinVoltage
   retVal = Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(&Do12VPinVoltage,
                                                         &IsDo12VActivated,
                                                         &DiagStatus);
   //! ##### if rte call returns successfully then filling the Databuffer with Do12VPinVoltage 
   if (RTE_E_OK == retVal)
   {
      Data[0] = (uint8) IsDo12VActivated;
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReturnControlToECU_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReturnControlToECU
//! 
//! \param  ErrorCode[1]   For reading error
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/
   //! ###### This function implements the logic for Dataservice Diag_IOCTL ReturnControlToECU for DCDC12
   IOCtrlReq_T    IOCtrlReqType = IOCtrl_DiagReturnCtrlToApp;
   IOHWAB_BOOL    Activation    = 0U; 
   Std_ReturnType retVal        = RTE_E_INVALID;
   Std_ReturnType returnVal     = RTE_E_INVALID;
   //! ##### Rte call to set IOCtrl_DiagReturnCtrlToApp request and Activation
   retVal = Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReqType,
                                                          Activation);
   if (RTE_E_OK == retVal)
   {   
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment
//! 
//! \param  Data[1]     For ShortTermAdjustment of Activation request
//! \param  ErrorCode   For reading error code
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/
   //! ###### this function implements the logic for ShortTermAdjustment for DCDC12
   IOCtrlReq_T    IOCtrlReqType = IOCtrl_DiagShortTermAdjust;
   IOHWAB_BOOL    Activation    = 0U;
   Std_ReturnType retVal        = RTE_E_INVALID;
   Std_ReturnType returnVal     = RTE_E_INVALID;
   //! ##### Rte call for DCDC12v  control
   Activation = Data[0];   
   retVal     = Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReqType,
                                                              Activation);
   if (RTE_E_OK == retVal)
   {
      returnVal = RTE_E_OK;
   }
   else
   {
      returnVal = RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK;
   }   
   return returnVal;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DiagMonitor_IO_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetPullUpState_CS(IOHWAB_BOOL *isPullUpActive_Strong, IOHWAB_BOOL *isPullUpActive_Weak, IOHWAB_BOOL *isPullUpActive_DAI)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(VGTT_EcuPinVoltage_0V2 *DcDc12vRefVoltage, IOHWAB_BOOL *IsDcDc12vActivated, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_LFAntennaDiagnostic_P_GetDiagnosticResult(uint8 *ValidFlag, uint8 *STGStatus, uint8 *STBStatus, uint8 *OCStatus, uint8 *OTStatus)
 *     Argument STGStatus: uint8* is of type DataArrayType_uint8_5
 *     Argument STBStatus: uint8* is of type DataArrayType_uint8_5
 *     Argument OCStatus: uint8* is of type DataArrayType_uint8_5
 *     Argument OTStatus: uint8* is of type DataArrayType_uint8_5
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_VbatInterface_I_GetVbatVoltage_CS(VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_VbatInterface_I_AdcInFailure, RTE_E_VbatInterface_I_IoHwAbApplicationError
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1AD0_1C_DcDc12v_VOR_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_29_ECU_SignalInvalid_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_2F_ECU_SignalErratic_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD9_16_PWR24V_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD9_17_PWR24V_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD9_1C_PWR24V_VOR_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E0K_11_DOWHS1_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E0K_12_DOWHS1_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E0K_13_DOWHS1_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E0K_16_DOWHS1_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E0K_17_DOWHS1_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E0K_38_DOWHS1_PWM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E10_11_ADI4_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E10_13_ADI4_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E11_11_ADI5_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E11_13_ADI5_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E12_11_ADI6_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E12_13_ADI6_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E13_11_ADI7_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E13_13_ADI7_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E14_11_ADI8_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E14_13_ADI8_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E15_11_ADI9_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E15_13_ADI9_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E16_11_ADI10_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E16_13_ADI10_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E17_11_ADI11_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E17_13_ADI11_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E18_11_ADI12_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E18_13_ADI12_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E19_11_ADI13_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E19_13_ADI13_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Q_11_AO12L_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Q_12_AO12L_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Q_16_AO12L_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Q_17_AO12L_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Q_19_AO12L_CAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1R_11_AO12P_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1R_12_AO12P_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1R_16_AO12P_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1R_17_AO12P_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1R_19_AO12P_CAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1S_11_DOWHS2_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1S_12_DOWHS2_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1S_13_DOWHS2_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1S_16_DOWHS2_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1S_17_DOWHS2_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1S_38_DOWHS2_PWM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1T_11_DOBHS1_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1T_12_DOBHS1_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1T_13_DOBHS1_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1T_16_DOBHS1_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1T_17_DOBHS1_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1U_11_DOBHS2_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1U_12_DOBHS2_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1U_13_DOBHS2_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1U_16_DOBHS2_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1U_17_DOBHS2_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1V_11_DOBHS3_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1V_12_DOBHS3_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1V_13_DOBHS3_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1V_16_DOBHS3_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1V_17_DOBHS3_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1W_11_DOBHS4_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1W_12_DOBHS4_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1W_13_DOBHS4_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1W_16_DOBHS4_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1W_17_DOBHS4_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1X_11_ADI1_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1X_13_ADI1_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Y_11_ADI2_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Y_13_ADI2_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Z_11_ADI3_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Z_13_ADI3_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2A_11_ADI14_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2A_13_ADI14_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2B_11_ADI15_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2B_13_ADI15_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2C_11_ADI16_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2C_13_ADI16_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2G_12_DOBLS1_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2G_14_DOBLS1_STGOC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2G_16_DOBLS1_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2G_17_DOBLS1_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2H_12_DOWLS2_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2H_14_DOWLS2_STGOC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2H_16_DOWLS2_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2H_17_DOWLS2_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2H_38_DOWLS2_PWM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2I_12_DOWLS3_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2I_14_DOWLS3_STGOC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2I_16_DOWLS3_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2I_17_DOWLS3_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2I_38_DOWLS3_PWM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2L_29_DAI1_SI_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2M_29_DAI2_SI_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F1A_11_ADI17_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F1A_13_ADI17_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F1B_11_ADI18_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F1B_13_ADI18_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F1C_11_ADI19_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F1C_13_ADI19_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM3_11_LFPi_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM3_12_LFPi_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM3_13_LFPi_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM3_98_LFPi_OverTemperature_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM4_11_LFP4_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM4_12_LFP4_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM4_13_LFP4_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM5_11_LFP1_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM5_12_LFP1_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM5_13_LFP1_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM6_11_LFP2_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM6_12_LFP2_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM6_13_LFP2_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM7_11_LFP3_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM7_12_LFP3_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM7_13_LFP3_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DiagMonitor_IO_10ms_runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the DiagMonitor_IO_10ms_runnable
//!  
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DiagnosticMonitor_IO_CODE) DiagMonitor_IO_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DiagMonitor_IO_10ms_runnable
 *********************************************************************************************************************/
   //! ######    This function implements runnable logic for DiagMonitor_IO_10ms_runnable
   VGTT_EcuPinFaultStatus FaultStatusVbat         = 0U;
   //IOHWAB_UINT8         SelectParkedOrLivingPin = 0U;   
   VGTT_EcuPinVoltage_0V2 BatteryVoltage          = 0U;
   
   //! ##### rte call to read battery voltage and Faultstatus Vbat
   (void)Rte_Call_VbatInterface_I_GetVbatVoltage_CS(&BatteryVoltage,
                                                  &FaultStatusVbat);   
   
   if (Rte_Prm_X1C05_DtcActivationVbatEnable_v() == TRUE)
   {
        //! ##### Handling of DTC's for Battery voltage
        DiagMonitor_IO_24V_DTC_Handler(BatteryVoltage,
                                       FaultStatusVbat);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### if FaultStatusVbat is equal to OnState_NoFaultDetected then invoke handlers
   if (FaultStatusVbat == OnState_NoFaultDetected)
   {
      //! #### invoking 12V DTC handler : 'DiagMonitor_IO_12V_DTC_Handler()'
      DiagMonitor_IO_12V_DTC_Handler();
      //! #### invoking Dowhs DTC handler    : 'DiagMonitor_IO_Dowhs_DTC_Handler()'
      DiagMonitor_IO_Dowhs_DTC_Handler();
      //! #### invoking Dowls DTC handler    : 'DiagMonitor_IO_Dowls_DTC_Handler()'
      DiagMonitor_IO_Dowls_DTC_Handler();
      //! #### invoking Dobhs DTC handler : 'DiagMonitor_IO_Dobhs_DTC_Handler()'
      DiagMonitor_IO_Dobhs_DTC_Handler();
      //! #### invoking LF Antenna DTC handler : 'DiagMonitor_IO_LFAnt_DTC_Handler()'
      DiagMonitor_IO_LFAnt_DTC_Handler();
		 //! #### invoking ADI DTC handler : 'DiagMonitor_IO_ADI_DTC_Handler()'
		DiagMonitor_IO_ADI_DTC_Handler();
   }
   else
   {
      // do nothing : MISRA
   }    
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#define DiagnosticMonitor_IO_STOP_SEC_CODE
#include "DiagnosticMonitor_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_24V_DTC_Handler'
//!
//! \param    BatteryVoltage             Battery voltage for VGTT Ecu pin
//! \param    FaultStatusVbat            Provided Fault Status for Vbat
//!   
//!======================================================================================
static void DiagMonitor_IO_24V_DTC_Handler(VGTT_EcuPinVoltage_0V2 BatteryVoltage,
                                           VGTT_EcuPinFaultStatus FaultStatusVbat)
{
   //! ###### if BatteryVoltage is valid then  check for FaultStatusVbat                                          
   if (BatteryVoltage != (VGTT_EcuPinVoltage_0V2)0xFE)
   {
      //! ##### according to FaultStatusVbat state setting the DEM_EVENT_STATUS  for D1AD9_16_PWR24V_VBT
      if (FaultStatusVbat == OnState_FaultDetected_VBT)
      {
         Rte_Call_Event_D1AD9_16_PWR24V_VBT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if ((FaultStatusVbat != OnState_FaultDetected_VBT) 
              ||( FaultStatusVbat != OnState_FaultDetected_VOR))
      {
         Rte_Call_Event_D1AD9_16_PWR24V_VBT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
      //! ##### according to FaultStatusVbat state setting the DEM_EVENT_STATUS  for D1AD9_17_PWR24V_VAT
      if (FaultStatusVbat == OnState_FaultDetected_VAT)
      {
         Rte_Call_Event_D1AD9_17_PWR24V_VAT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (FaultStatusVbat != OnState_FaultDetected_VAT)
      {
         Rte_Call_Event_D1AD9_17_PWR24V_VAT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
      //! ##### according to FaultStatusVbat state setting the DEM_EVENT_STATUS  for D1AD9_1C_PWR24V_VOR
      if (FaultStatusVbat == OnState_FaultDetected_VOR)
      {
         Rte_Call_Event_D1AD9_1C_PWR24V_VOR_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (FaultStatusVbat != OnState_FaultDetected_VOR)
      {
         Rte_Call_Event_D1AD9_1C_PWR24V_VOR_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
      Rte_Call_Event_D1AD0_2F_ECU_SignalErratic_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   //! ###### battery voltage value pass the DEM_EVENT_STATUS to Rte call for D1AD0_2F_ECU_SignalErratic 
   else
   {
      Rte_Call_Event_D1AD0_2F_ECU_SignalErratic_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_12V_DTC_Handler'      
//!   
//!======================================================================================
static void DiagMonitor_IO_12V_DTC_Handler(void)
{
   //! ###### This function implements the logic for 12V DTC handler
   VGTT_EcuPinVoltage_0V2 DcDc12vRefVoltage  = 0U;
   IOHWAB_BOOL            IsDcDc12vActivated = 0U;
   IOHWAB_BOOL            IsDo12VActivated   = 0U;
   VGTT_EcuPinVoltage_0V2 BatteryVoltage     = 0U;
   VGTT_EcuPinFaultStatus FaultStatusDcDc    = 0U;
   VGTT_EcuPinFaultStatus FaultStatusL       = 0U;
   VGTT_EcuPinFaultStatus FaultStatusP       = 0U;
   VGTT_EcuPinVoltage_0V2 Do12VPinVoltage    = 0U;
   if (Rte_Prm_P1V8E_Diag_Act_12VDCDC_v() == TRUE)
   {
      //! ##### rte call to read DcDc12vRefVoltage,IsDcDc12vActivated,FaultStatusDcDc
      (void)Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(&DcDc12vRefVoltage, 
                                                         &IsDcDc12vActivated,
                                                         &FaultStatusDcDc);
      //! ##### if FaultStatusDcDc is equal to OnState_NoFaultDetected
      if ((FaultStatusDcDc == OnState_FaultDetected_VOR) 
         && (IsDcDc12vActivated == TRUE))
      {
         Rte_Call_Event_D1AD0_1C_DcDc12v_VOR_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (FaultStatusDcDc == OnState_NoFaultDetected)
      {
         Rte_Call_Event_D1AD0_1C_DcDc12v_VOR_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing
      }
   }
   else
   {
      // do nothing
   }
   if(Rte_Prm_P1V6I_Diag_Act_AO12_P_v() == TRUE)
   {
      (void)Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(0,
                                                           &IsDo12VActivated, 
                                                           &Do12VPinVoltage, 
                                                           &BatteryVoltage, 
                                                           &FaultStatusP);
       //! ##### Handling of DTC's for P
       DiagMonitor_IO_12VParked_DTC_Handler(FaultStatusP);      
   }
   else
   {
      // do nothing : MISRA
   }
   if(Rte_Prm_P1V6K_Diag_Act_AO12_L_v() == TRUE)
   {
      //! ##### Rte call to read IsDo12VActivated,DoPinVoltage,BatteryVoltage,FaultStatusL
      (void)Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(1, 
                                                           &IsDo12VActivated, 
                                                           &Do12VPinVoltage, 
                                                           &BatteryVoltage, 
                                                           &FaultStatusL);
       //! ##### Handling of DTC's for L
       DiagMonitor_IO_12VLiving_DTC_Handler(FaultStatusL);
   }
   else
   {
      // do nothing : MISRA
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_12VParked_DTC_Handler'   
//!
//! \param  FaultStatusP   Provides FaultStatus value for FaultStatusP
//!   
//!======================================================================================
static void DiagMonitor_IO_12VParked_DTC_Handler(VGTT_EcuPinFaultStatus FaultStatusP)
{
   //! ##### according to FaultStatusP value pass the DEM_EVENT_STATUS to D1E1R_11_AO12P_STG rte call      
   if (FaultStatusP == OnState_FaultDetected_STG)
   {
      Rte_Call_Event_D1E1R_11_AO12P_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusP != OnState_FaultDetected_STG)
   {
      Rte_Call_Event_D1E1R_11_AO12P_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing
   }
   //! ##### according to FaultStatusP value pass the DEM_EVENT_STATUS to D1E1R_16_AO12P_VBT rte call
   if (FaultStatusP == OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E1R_16_AO12P_VBT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusP != OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E1R_16_AO12P_VBT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing
   }
  //! ##### according to FaultStatusP value pass the DEM_EVENT_STATUS to D1E1R_17_AO12P_VAT rte call
   if (FaultStatusP == OnState_FaultDetected_VAT)
   {
      Rte_Call_Event_D1E1R_17_AO12P_VAT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusP != OnState_FaultDetected_VAT)
   {
      Rte_Call_Event_D1E1R_17_AO12P_VAT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing
   }
   //! ##### according to FaultStatusP value pass the DEM_EVENT_STATUS to D1E1R_12_AO12P_STB rte call
   if ((FaultStatusP == OnState_FaultDetected_STB) 
      || (FaultStatusP == OffState_FaultDetected_STB))
   {
      Rte_Call_Event_D1E1R_12_AO12P_STB_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if ((FaultStatusP != OnState_FaultDetected_STB) 
           || (FaultStatusP != OffState_FaultDetected_STB))
   {
      Rte_Call_Event_D1E1R_12_AO12P_STB_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing
   }
   //! ##### according to FaultStatusP value pass the DEM_EVENT_STATUS to D1E1R_19_AO12P_CAT rte call
   if (FaultStatusP == OnState_FaultDetected_CAT)
   {
      Rte_Call_Event_D1E1R_19_AO12P_CAT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusP != OnState_FaultDetected_CAT)
   {
      Rte_Call_Event_D1E1R_19_AO12P_CAT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_12VLiving_DTC_Handler'   
//!   
//! \param  FaultStatusL   Provides FaultStatus value for FaultStatusL
//!
//!======================================================================================
static void DiagMonitor_IO_12VLiving_DTC_Handler(VGTT_EcuPinFaultStatus FaultStatusL)
{
    //! ##### according to FaultStatusL value pass the DEM_EVENT_STATUS to D1E1Q_11_AO12L_STG rte call
    if (FaultStatusL == OnState_FaultDetected_STG)
    {
       Rte_Call_Event_D1E1Q_11_AO12L_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
    }
    else if (FaultStatusL != OnState_FaultDetected_STG)
    {
       Rte_Call_Event_D1E1Q_11_AO12L_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
    }
    else
    {
        // do nothing : MISRA
    }
    //! ##### according to FaultStatusL value pass the DEM_EVENT_STATUS to D1E1Q_16_AO12L_VBT rte call
    if (FaultStatusL == OnState_FaultDetected_VBT)
    {
       Rte_Call_Event_D1E1Q_16_AO12L_VBT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
    }
    else if (FaultStatusL != OnState_FaultDetected_VBT)
    {
       Rte_Call_Event_D1E1Q_16_AO12L_VBT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
    }
    else
    {
        // do nothing : MISRA
    }
    //! ##### according to FaultStatusL value pass the DEM_EVENT_STATUS to D1E1Q_17_AO12L_VAT rte call
    if (FaultStatusL == OnState_FaultDetected_VAT)
    {
       Rte_Call_Event_D1E1Q_17_AO12L_VAT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
    }
    else if (FaultStatusL != OnState_FaultDetected_VAT)
    {
       Rte_Call_Event_D1E1Q_17_AO12L_VAT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
    }
    else
    {
        // do nothing : MISRA
    }
    //! ##### according to FaultStatusL value pass the DEM_EVENT_STATUS to D1E1Q_12_AO12L_STB rte call
    if ((FaultStatusL == OnState_FaultDetected_STB) 
       ||(FaultStatusL == OffState_FaultDetected_STB))
    {
        Rte_Call_Event_D1E1Q_12_AO12L_STB_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
    }
    else if ((FaultStatusL != OnState_FaultDetected_STB) 
            || (FaultStatusL != OffState_FaultDetected_STB))
    {
       Rte_Call_Event_D1E1Q_12_AO12L_STB_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
    }
    else
    {
        // do nothing : MISRA
    }
    //! ##### according to FaultStatusL value pass the DEM_EVENT_STATUS to D1E1Q_19_AO12L_CAT rte call
    if (FaultStatusL == OnState_FaultDetected_CAT)
    {
       Rte_Call_Event_D1E1Q_19_AO12L_CAT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
    }
    else if (FaultStatusL != OnState_FaultDetected_CAT)
    {
       Rte_Call_Event_D1E1Q_19_AO12L_CAT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
    }
    else
    {
        // do nothing : MISRA
    }
}

//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_Dowhs_DTC_Handler'
//!        
//!   
//!======================================================================================
static void DiagMonitor_IO_Dowhs_DTC_Handler(void)
{
   IOHWAB_BOOL              IsDoActivated     = 0U;
   VGTT_EcuPinVoltage_0V2   DoPinVoltage      = 0U;
   VGTT_EcuPinVoltage_0V2   BatteryVoltage    = 0U;
   VGTT_EcuPwmDutycycle     DutyCycle         = 0;
   VGTT_EcuPwmPeriod        Period            = 0U;
   VGTT_EcuPinFaultStatus   FaultStatusDowhs1 = 0U;
   VGTT_EcuPinFaultStatus   FaultStatusDowhs2 = 0U;
   if(Rte_Prm_P1V6O_Diag_Act_DOWHS01_v() == TRUE)
   {
      //! ##### Rte call to read IsDoActivated,DoPinVoltage,BatteryVoltage,DutyCycle,Period,FaultStatusDowhs1 values
      (void)Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(1U,
                                                          &IsDoActivated,
                                                          &DoPinVoltage,
                                                          &BatteryVoltage,
                                                          &DutyCycle,
                                                          &Period,
                                                          &FaultStatusDowhs1);
      //! ##### Handling of DTC's for Dowhs1
      DiagMonitor_IO_Dowhs1_DTC_Handler(FaultStatusDowhs1);                                          
   }
   else
   {
      //do nothing: MISRA
   }
   if(Rte_Prm_P1V6P_Diag_Act_DOWHS02_v() == TRUE)
   {
      //! ##### Rte call to read isDioActivated, DoPinVoltage, BatteryVoltage, DutyCycle, Period, FaultStatusDowhs2
      (void)Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(2U,
                                                          &IsDoActivated, 
                                                          &DoPinVoltage,
                                                          &BatteryVoltage,
                                                          &DutyCycle, 
                                                          &Period, 
                                                          &FaultStatusDowhs2);
      //! ##### Handling of DTC's for Dowhs1
      DiagMonitor_IO_Dowhs2_DTC_Handler(FaultStatusDowhs2);
   }
   else
   {
      //do nothing: MISRA
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_Dowhs1_DTC_Handler'
//! 
//! \param  FaultStatusDowhs1   Provides FaultStatus value for Dowhs1
//!   
//!======================================================================================
static void DiagMonitor_IO_Dowhs1_DTC_Handler(VGTT_EcuPinFaultStatus FaultStatusDowhs1)
{
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E0K_11_DOWHS1_STG rte call
   if (FaultStatusDowhs1 == OnState_FaultDetected_STG)
   {
      Rte_Call_Event_D1E0K_11_DOWHS1_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDowhs1 != OnState_FaultDetected_STG)
   {
      Rte_Call_Event_D1E0K_11_DOWHS1_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      //do nothing
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E0K_12_DOWHS1_STB rte call
   if (FaultStatusDowhs1 == OnState_FaultDetected_STB)
   {
      Rte_Call_Event_D1E0K_12_DOWHS1_STB_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDowhs1 != OnState_FaultDetected_STB)
   {
      Rte_Call_Event_D1E0K_12_DOWHS1_STB_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      //do nothing: MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E0K_13_DOWHS1_OC rte call
   if (FaultStatusDowhs1 == OnState_FaultDetected_OC)
   {
      Rte_Call_Event_D1E0K_13_DOWHS1_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDowhs1 != OnState_FaultDetected_OC)
   {
      Rte_Call_Event_D1E0K_13_DOWHS1_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      //do nothing: MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E0K_16_DOWHS1_VBT rte call
   if (FaultStatusDowhs1 == OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E0K_16_DOWHS1_VBT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDowhs1 != OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E0K_16_DOWHS1_VBT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      //do nothing: MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E0K_17_DOWHS1_VAT rte call
   if (FaultStatusDowhs1 == OnState_FaultDetected_VAT)
   {
      Rte_Call_Event_D1E0K_17_DOWHS1_VAT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDowhs1 != OnState_FaultDetected_VAT)
   {
      Rte_Call_Event_D1E0K_17_DOWHS1_VAT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      //Do nothing: MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E0K_38_DOWHS1_PWM rte call
   if(FaultStatusDowhs1 == PCODE_OnState_FaultDetected_PWM)
   {
      Rte_Call_Event_D1E0K_38_DOWHS1_PWM_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if(FaultStatusDowhs1 != PCODE_OnState_FaultDetected_PWM)
   {
      Rte_Call_Event_D1E0K_38_DOWHS1_PWM_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      //Do nothing: MISRA
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_Dowhs2_DTC_Handler'
//!
//! \param  FaultStatusDowhs2   Provides FaultStatus value for Dowhs2
//!   
//!======================================================================================
static void DiagMonitor_IO_Dowhs2_DTC_Handler(VGTT_EcuPinFaultStatus FaultStatusDowhs2)
{
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1S_11_DOWHS2_STG rte call
   if (FaultStatusDowhs2 == OnState_FaultDetected_STG)
   {
      Rte_Call_Event_D1E1S_11_DOWHS2_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDowhs2 != OnState_FaultDetected_STG)
   {
      Rte_Call_Event_D1E1S_11_DOWHS2_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      //do nothing: MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1S_12_DOWHS2_STB rte call
   if (FaultStatusDowhs2 == OnState_FaultDetected_STB)
   {
      Rte_Call_Event_D1E1S_12_DOWHS2_STB_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDowhs2 != OnState_FaultDetected_STB)
   {
      Rte_Call_Event_D1E1S_12_DOWHS2_STB_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      //do nothing: MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1S_13_DOWHS2_OC rte call
   if (FaultStatusDowhs2 == OnState_FaultDetected_OC)
   {
      Rte_Call_Event_D1E1S_13_DOWHS2_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDowhs2 != OnState_FaultDetected_OC)
   {
      Rte_Call_Event_D1E1S_13_DOWHS2_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      //do nothing: MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1S_16_DOWHS2_VBT rte call
   if (FaultStatusDowhs2 == OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E1S_16_DOWHS2_VBT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDowhs2 != OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E1S_16_DOWHS2_VBT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      //do nothing: MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1S_17_DOWHS2_VAT rte call
   if (FaultStatusDowhs2 == OnState_FaultDetected_VAT)
   {
      Rte_Call_Event_D1E1S_17_DOWHS2_VAT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDowhs2 != OnState_FaultDetected_VAT)
   {
      Rte_Call_Event_D1E1S_17_DOWHS2_VAT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      //Do nothing: MISRA
   } 
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1S_38_DOWHS2_PWM rte call
   if(FaultStatusDowhs2 == PCODE_OnState_FaultDetected_PWM)
   {
      Rte_Call_Event_D1E1S_38_DOWHS2_PWM_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if(FaultStatusDowhs2 != PCODE_OnState_FaultDetected_PWM)
   {
      Rte_Call_Event_D1E1S_38_DOWHS2_PWM_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      //Do nothing: MISRA
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_Dowls_DTC_Handler'
//!        
//!   
//!======================================================================================
static void DiagMonitor_IO_Dowls_DTC_Handler(void)
{
   //! ###### This function implements logic for Dowls_Dtc handler
   IOHWAB_BOOL             IsDoActivated     = 0U;
   VGTT_EcuPinVoltage_0V2  DoPinVoltage      = 0U;
   VGTT_EcuPinVoltage_0V2  BatteryVoltage    = 0U;
   VGTT_EcuPwmDutycycle    DutyCycle         = 0;
   VGTT_EcuPwmPeriod       Period            = 0U;
   VGTT_EcuPinFaultStatus  FaultStatusDOBLS1 = 0U; 
   VGTT_EcuPinFaultStatus  FaultStatusDowls2 = 0U;
   VGTT_EcuPinFaultStatus  FaultStatusDowls3 = 0U;
   if (Rte_Prm_P1V7D_Diag_Act_DOBLS01_v() == TRUE)
   {
      //! ##### rte call to read IsDoActivated,DoPinVoltage,BatteryVoltage,FaultStatusDOBLS1
      (void)Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(&IsDoActivated, 
                                                              &DoPinVoltage,
                                                              &BatteryVoltage, 
                                                              &FaultStatusDOBLS1);
      //! ##### Handling DTC's for Dobls1
      DiagMonitor_IO_Dobls1_DTC_Handler(FaultStatusDOBLS1);
   }
   else
   {
      // do nothing : MISRA
	   if (Rte_Prm_P1V7E_Diag_Act_DOWLS02_v() == TRUE)
	   {
	      //! ##### rte call to read IsDoActivated,DoPinVoltage,BatteryVoltage,DutyCycle,Period,FaultStatusDowls2
	      (void)Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(2U, 
	                                                          &IsDoActivated,
	                                                          &DoPinVoltage,
	                                                          &BatteryVoltage,
	                                                          &DutyCycle, 
	                                                          &Period,
	                                                          &FaultStatusDowls2);
	       //! ##### Handling DTC's for Dowls2
	       DiagMonitor_IO_Dowls2_DTC_Handler(FaultStatusDowls2);      
	   }
	   else
	   {
	      // do nothing : MISRA
	   }
	   if (Rte_Prm_P1V7F_Diag_Act_DOWLS03_v() == TRUE)
	   {
	      //! ##### rte call to read IsDoActivated,DoPinVoltage,BatteryVoltage,DutyCycle,Period,FaultStatusDowls3
	      (void)Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(3U, 
	                                                          &IsDoActivated, 
	                                                          &DoPinVoltage,
	                                                          &BatteryVoltage,
	                                                          &DutyCycle, 
	                                                          &Period, 
	                                                          &FaultStatusDowls3);
	      //! ##### Handling DTC's for Dowls3
	      DiagMonitor_IO_Dowls3_DTC_Handler(FaultStatusDowls3);
	   }
	   else
	   {
	      // do nothing : MISRA
	   }
	}
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_Dobls1_DTC_Handler'
//! 
//! \param  FaultStatusDOBLS1   Provides FaultStatus value for Dobls1
//!   
//!======================================================================================
static void DiagMonitor_IO_Dobls1_DTC_Handler(VGTT_EcuPinFaultStatus FaultStatusDOBLS1)
{
    //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E2G_14_DOBLS1_STGOC rte call
   if (FaultStatusDOBLS1 == OnState_FaultDetected_STG)
   {
      Rte_Call_Event_D1E2G_14_DOBLS1_STGOC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDOBLS1 != OnState_FaultDetected_STG)
   {
      Rte_Call_Event_D1E2G_14_DOBLS1_STGOC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E2G_12_DOBLS1_STB rte call
   if (FaultStatusDOBLS1 == OnState_FaultDetected_STB)
   {
      Rte_Call_Event_D1E2G_12_DOBLS1_STB_SetEventStatus(DEM_EVENT_STATUS_PREFAILED );
   }
   else if (FaultStatusDOBLS1 != OnState_FaultDetected_STB)
   {
      Rte_Call_Event_D1E2G_12_DOBLS1_STB_SetEventStatus(DEM_EVENT_STATUS_PREPASSED );
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E2G_14_DOBLS1_STGOC rte call
   if (FaultStatusDOBLS1 == OnState_FaultDetected_OC)
   {
      Rte_Call_Event_D1E2G_14_DOBLS1_STGOC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED );
   }
   else if (FaultStatusDOBLS1 != OnState_FaultDetected_OC)
   {
      Rte_Call_Event_D1E2G_14_DOBLS1_STGOC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED );
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E2G_16_DOBLS1_VBT rte call
   if (FaultStatusDOBLS1 == OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E2G_16_DOBLS1_VBT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED );
   }
   else if (FaultStatusDOBLS1 != OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E2G_16_DOBLS1_VBT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED );
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E2G_17_DOBLS1_VAT rte call
   if (FaultStatusDOBLS1 == OnState_FaultDetected_VAT)
   {
      Rte_Call_Event_D1E2G_17_DOBLS1_VAT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED );
   }
   else if (FaultStatusDOBLS1 != OnState_FaultDetected_VAT)
   {
      Rte_Call_Event_D1E2G_17_DOBLS1_VAT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED );
   }
   else
   {
      // do nothing : MISRA
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_Dowls2_DTC_Handler'
//! 
//! \param  FaultStatusDowls2   Provides FaultStatus value for Dowls2 
//!   
//!======================================================================================
static void DiagMonitor_IO_Dowls2_DTC_Handler(VGTT_EcuPinFaultStatus FaultStatusDowls2)
{
    //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E2H_14_DOWLS2_STGOC rte call
   if (FaultStatusDowls2 == OnState_FaultDetected_STG)
   {
      Rte_Call_Event_D1E2H_14_DOWLS2_STGOC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDowls2 != OnState_FaultDetected_STG)
   {
      Rte_Call_Event_D1E2H_14_DOWLS2_STGOC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E2H_12_DOWLS2_STB rte call
   if (FaultStatusDowls2 == OnState_FaultDetected_STB)
   {
      Rte_Call_Event_D1E2H_12_DOWLS2_STB_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDowls2 != OnState_FaultDetected_STB)
   {
      Rte_Call_Event_D1E2H_12_DOWLS2_STB_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E2H_14_DOWLS2_STGOC rte call
   if (FaultStatusDowls2 == OnState_FaultDetected_OC)
   {
      Rte_Call_Event_D1E2H_14_DOWLS2_STGOC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDowls2 != OnState_FaultDetected_OC)
   {
      Rte_Call_Event_D1E2H_14_DOWLS2_STGOC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E2H_16_DOWLS2_VBT rte call
   if (FaultStatusDowls2 == OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E2H_16_DOWLS2_VBT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDowls2 != OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E2H_16_DOWLS2_VBT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E2H_17_DOWLS2_VAT rte 
   if (FaultStatusDowls2 == OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E2H_17_DOWLS2_VAT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDowls2 != OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E2H_17_DOWLS2_VAT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### If Pwm fault is detected accordingly DEM event is set to fail or pass
   if (FaultStatusDowls2 == PCODE_OnState_FaultDetected_PWM)
   {
      Rte_Call_Event_D1E2H_38_DOWLS2_PWM_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if(FaultStatusDowls2 != PCODE_OnState_FaultDetected_PWM)
   {
      Rte_Call_Event_D1E2H_38_DOWLS2_PWM_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_Dowls3_DTC_Handler'
//!        
//! \param  FaultStatusDowls3   Provides FaultStatus value for Dowls3  
//!   
//!======================================================================================
static void DiagMonitor_IO_Dowls3_DTC_Handler(VGTT_EcuPinFaultStatus FaultStatusDowls3)
{
    //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E2I_14_DOWLS3_STGOC rte 
   if (FaultStatusDowls3 == OnState_FaultDetected_STG)
   {
      Rte_Call_Event_D1E2I_14_DOWLS3_STGOC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED );
   }
   else if (FaultStatusDowls3 != OnState_FaultDetected_STG)
   {
      Rte_Call_Event_D1E2I_14_DOWLS3_STGOC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED );
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E2I_12_DOWLS3_STB rte 
   if (FaultStatusDowls3 == OnState_FaultDetected_STB)
   {
      Rte_Call_Event_D1E2I_12_DOWLS3_STB_SetEventStatus(DEM_EVENT_STATUS_PREFAILED );
   }
   else if (FaultStatusDowls3 != OnState_FaultDetected_STB)
   {
      Rte_Call_Event_D1E2I_12_DOWLS3_STB_SetEventStatus(DEM_EVENT_STATUS_PREPASSED );
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E2I_14_DOWLS3_STGOC rte 
   if (FaultStatusDowls3 == OnState_FaultDetected_OC)
   {
      Rte_Call_Event_D1E2I_14_DOWLS3_STGOC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED );
   }
   else if (FaultStatusDowls3 != OnState_FaultDetected_OC)
   {
      Rte_Call_Event_D1E2I_14_DOWLS3_STGOC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED );
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E2I_16_DOWLS3_VBT rte 
   if (FaultStatusDowls3 == OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E2I_16_DOWLS3_VBT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED );
   }
   else if (FaultStatusDowls3 != OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E2I_16_DOWLS3_VBT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED );
   }
   else
   {
      // do nothing : MISRA
   }    
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E2I_17_DOWLS3_VAT rte 
   if (FaultStatusDowls3 == OnState_FaultDetected_VAT)
   {
      Rte_Call_Event_D1E2I_17_DOWLS3_VAT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED );
   }
   else if (FaultStatusDowls3 != OnState_FaultDetected_VAT)
   {
      Rte_Call_Event_D1E2I_17_DOWLS3_VAT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED );
   }
   else
   {
      // do nothing : MISRA
   }   
   if(FaultStatusDowls3 == PCODE_OnState_FaultDetected_PWM)
   {
      Rte_Call_Event_D1E2I_38_DOWLS3_PWM_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if(FaultStatusDowls3 != PCODE_OnState_FaultDetected_PWM)
   {
      Rte_Call_Event_D1E2I_38_DOWLS3_PWM_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
}

//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_Dobhs1_DTC_Handler'
//!        
//! \param  FaultStatusDobhs   Provides FaultStatus value for Dobhs1   
//!   
//!======================================================================================
static void DiagMonitor_IO_Dobhs1_DTC_Handler(VGTT_EcuPinFaultStatus FaultStatusDobhs)
{
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1T_11_DOBHS1_STG rte
   if (FaultStatusDobhs == OnState_FaultDetected_STG)
   {
       Rte_Call_Event_D1E1T_11_DOBHS1_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OnState_FaultDetected_STG)
   {
      Rte_Call_Event_D1E1T_11_DOBHS1_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
       // do nothing : MISRA
   }   
   if (FaultStatusDobhs == OnState_FaultDetected_OC)
   {
      Rte_Call_Event_D1E1T_13_DOBHS1_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OnState_FaultDetected_OC)
   {
       Rte_Call_Event_D1E1T_13_DOBHS1_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1T_16_DOBHS1_VBT rte
   if (FaultStatusDobhs == OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E1T_16_DOBHS1_VBT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs!= OnState_FaultDetected_VBT)
   {
       Rte_Call_Event_D1E1T_16_DOBHS1_VBT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1T_17_DOBHS1_VAT rte            
   if (FaultStatusDobhs == OffState_FaultDetected_VAT)
   {
       Rte_Call_Event_D1E1T_17_DOBHS1_VAT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs!= OffState_FaultDetected_VAT)
   {
       Rte_Call_Event_D1E1T_17_DOBHS1_VAT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
       // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1T_12_DOBHS1_STB rte    
   if (FaultStatusDobhs == OffState_FaultDetected_STB)
   {
      Rte_Call_Event_D1E1T_12_DOBHS1_STB_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs!= OffState_FaultDetected_STB)
   {
      Rte_Call_Event_D1E1T_12_DOBHS1_STB_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_Dobhs2_DTC_Handler'
//!
//! \param  FaultStatusDobhs   Provides FaultStatus value for Dobhs2   
//!   
//!======================================================================================
static void DiagMonitor_IO_Dobhs2_DTC_Handler(VGTT_EcuPinFaultStatus FaultStatusDobhs)
{
   if (FaultStatusDobhs == OnState_FaultDetected_STG)
   {
      Rte_Call_Event_D1E1U_11_DOBHS2_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OnState_FaultDetected_STG)
   {
      Rte_Call_Event_D1E1U_11_DOBHS2_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to OC fault if detected  DEM_EVENT_STATUS is set to prefailed or prepassed
   if (FaultStatusDobhs == OnState_FaultDetected_OC)
   {
      Rte_Call_Event_D1E1U_13_DOBHS2_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OnState_FaultDetected_OC)
   {
      Rte_Call_Event_D1E1U_13_DOBHS2_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1U_16_DOBHS2_VBT rte
   if (FaultStatusDobhs == OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E1U_16_DOBHS2_VBT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OnState_FaultDetected_VBT)
   {
       Rte_Call_Event_D1E1U_16_DOBHS2_VBT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }   
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1U_17_DOBHS2_VAT rte
   if (FaultStatusDobhs == OffState_FaultDetected_VAT)
   {
       Rte_Call_Event_D1E1U_17_DOBHS2_VAT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OffState_FaultDetected_VAT)
   {
      Rte_Call_Event_D1E1U_17_DOBHS2_VAT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1U_12_DOBHS2_STB rte    
   if (FaultStatusDobhs == OffState_FaultDetected_STB)
   {
       Rte_Call_Event_D1E1U_12_DOBHS2_STB_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OffState_FaultDetected_STB)
   {
       Rte_Call_Event_D1E1U_12_DOBHS2_STB_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // Do nothing
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_Dobhs3_DTC_Handler'
//!        
//! \param  FaultStatusDobhs   Provides FaultStatus value for Dobhs3   
//!   
//!======================================================================================
static void DiagMonitor_IO_Dobhs3_DTC_Handler(VGTT_EcuPinFaultStatus FaultStatusDobhs)
{
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1V_11_DOBHS3_STG rte     
   if (FaultStatusDobhs == OnState_FaultDetected_STG)
   {
       Rte_Call_Event_D1E1V_11_DOBHS3_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OnState_FaultDetected_STG)
   {
       Rte_Call_Event_D1E1V_11_DOBHS3_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to OC fault if detected  DEM_EVENT_STATUS is set to prefailed or prepassed
   if (FaultStatusDobhs == OnState_FaultDetected_OC)
   {
       Rte_Call_Event_D1E1V_13_DOBHS3_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OnState_FaultDetected_OC)
   {
      Rte_Call_Event_D1E1V_13_DOBHS3_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1V_16_DOBHS3_VBT rte
   if (FaultStatusDobhs == OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E1V_16_DOBHS3_VBT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E1V_16_DOBHS3_VBT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1V_17_DOBHS3_VAT rte                
   if (FaultStatusDobhs == OffState_FaultDetected_VAT)
   {
      Rte_Call_Event_D1E1V_17_DOBHS3_VAT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OffState_FaultDetected_VAT)
   {
       Rte_Call_Event_D1E1V_17_DOBHS3_VAT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1V_12_DOBHS3_STB rte        
   if (FaultStatusDobhs == OffState_FaultDetected_STB)
   {
      Rte_Call_Event_D1E1V_12_DOBHS3_STB_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OffState_FaultDetected_STB)
   {
      Rte_Call_Event_D1E1V_12_DOBHS3_STB_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_Dobhs4_DTC_Handler'
//!
//! \param  FaultStatusDobhs   Provides FaultStatus value for Dobhs4       
//!   
//!======================================================================================
static void DiagMonitor_IO_Dobhs4_DTC_Handler(VGTT_EcuPinFaultStatus FaultStatusDobhs)
{
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1W_11_DOBHS4_STG rte    
   if (FaultStatusDobhs == OnState_FaultDetected_STG)
   {
       Rte_Call_Event_D1E1W_11_DOBHS4_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OnState_FaultDetected_STG)
   {
       Rte_Call_Event_D1E1W_11_DOBHS4_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to OC fault if detected  DEM_EVENT_STATUS is set to prefailed or prepassed
   if (FaultStatusDobhs == OnState_FaultDetected_OC)
   {
       Rte_Call_Event_D1E1W_13_DOBHS4_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OnState_FaultDetected_OC)
   {
        Rte_Call_Event_D1E1W_13_DOBHS4_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1W_16_DOBHS4_VBT rte
   if (FaultStatusDobhs == OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E1W_16_DOBHS4_VBT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OnState_FaultDetected_VBT)
   {
      Rte_Call_Event_D1E1W_16_DOBHS4_VBT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1W_17_DOBHS4_VAT rte            
   if (FaultStatusDobhs == OffState_FaultDetected_VAT)
   {
      Rte_Call_Event_D1E1W_17_DOBHS4_VAT_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OffState_FaultDetected_VAT)
   {
      Rte_Call_Event_D1E1W_17_DOBHS4_VAT_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### according to FaultStatus value pass the DEM_EVENT_STATUS to D1E1W_12_DOBHS4_STB rte    
   if (FaultStatusDobhs == OffState_FaultDetected_STB)
   {
      Rte_Call_Event_D1E1W_12_DOBHS4_STB_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
   }
   else if (FaultStatusDobhs != OffState_FaultDetected_STB)
   {
      Rte_Call_Event_D1E1W_12_DOBHS4_STB_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
   }
   else
   {
      // do nothing : MISRA
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_Dobhs_DTC_Handler'
//!        
//!   
//!======================================================================================
static void DiagMonitor_IO_Dobhs_DTC_Handler(void)
{
   VGTT_EcuPinFaultStatus FaultStatusDobhs = 0U;
   IOHWAB_BOOL            isDioActivated   = 0U;
   VGTT_EcuPinVoltage_0V2 DoPinVoltage     = 0U;
   VGTT_EcuPinVoltage_0V2 BatteryVoltage   = 0U;
   if (Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v() == TRUE)
   {
      //! ##### Rte call to read isDioActivated,DoPinVoltage,BatteryVoltage,FaultStatusDobhs
      (void)Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(1U, 
                                                              &isDioActivated,
                                                              &DoPinVoltage , 
                                                              &BatteryVoltage, 
                                                              &FaultStatusDobhs);
      //! ##### Handling of DTC's for DObhs1
      (void)DiagMonitor_IO_Dobhs1_DTC_Handler(FaultStatusDobhs);
   }
   else
   {
      // do nothing : MISRA
   }
   if(Rte_Prm_P1V6R_Diag_Act_DOBHS02_v()==TRUE)
   {
      //! ##### Rte call to read isDioActivated,DoPinVoltage,BatteryVoltage,FaultStatusDobhs
      (void)Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(2U, 
                                                              &isDioActivated , 
                                                              &DoPinVoltage , 
                                                              &BatteryVoltage, 
                                                              &FaultStatusDobhs);
      //! ##### Handling of DTC's for DObhs2
      (void)DiagMonitor_IO_Dobhs2_DTC_Handler(FaultStatusDobhs);
   }
   else
   {
      // do nothing : MISRA
   }
   if(Rte_Prm_P1V6S_Diag_Act_DOBHS03_v()==TRUE)
   {
      //! ##### Rte call to read isDioActivated,DoPinVoltage,BatteryVoltage,FaultStatusDobhs
      (void)Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(3U,
                                                              &isDioActivated , 
                                                              &DoPinVoltage , 
                                                              &BatteryVoltage, 
                                                              &FaultStatusDobhs);
      //! ##### Handling of DTC's for DObhs3
      (void)DiagMonitor_IO_Dobhs3_DTC_Handler(FaultStatusDobhs);
   }
   else
   {
      // do nothing : MISRA
   }
   if (Rte_Prm_P1V6T_Diag_Act_DOBHS04_v() == TRUE)
   {
      //! ##### Rte call to read isDioActivated,DoPinVoltage,BatteryVoltage,FaultStatusDobhs
      (void)Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(4U,
                                                              &isDioActivated , 
                                                              &DoPinVoltage , 
                                                              &BatteryVoltage, 
                                                              &FaultStatusDobhs);
      //! ##### Handling of DTC's for DObhs4
      (void)DiagMonitor_IO_Dobhs4_DTC_Handler(FaultStatusDobhs);
   }
   else
   {
      // do nothing : MISRA
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_ADI_DTC_Handler'       
//!   
//!======================================================================================
static void DiagMonitor_IO_ADI_DTC_Handler(void)
{
   //! ###### Processing DTC Handler for ADI01,ADI04,ADI07,ADI10 and ADI13
   ADI01_03_DTC_Handler();
   ADI04_06_DTC_Handler();
   ADI07_09_DTC_Handler();
   ADI10_12_DTC_Handler();
   ADI13_16_DTC_Handler();
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'DiagMonitor_IO_LFAnt_DTC_Handler'
//!        
//!   
//!======================================================================================
static void DiagMonitor_IO_LFAnt_DTC_Handler(void)
{
   uint8                        LFDiagResultValid  = 0u;
   uint8                        LFAntSTG[5]        = {0u,0u,0u,0u,0u};
   uint8                        LFAntSTB[5]        = {0u,0u,0u,0u,0u};
   uint8                        LFAntOC[5]         = {0u,0u,0u,0u,0u};
   uint8                        LFAntOT[5]         = {0u,0u,0u,0u,0u};
   SEWS_Diag_Act_LF_P_P1V79_s_T *DiagActLFAnt;
   VGTT_EcuPinVoltage_0V2       DcDc12vRefVoltage  = 0U;
   IOHWAB_BOOL                  IsDcDc12vActivated = 0U;
   VGTT_EcuPinFaultStatus       FaultStatusDcDc    = 0U;
   //! ###### Processing DTC Handler for Left antenna
   //! ##### Read LF Antenna status
   (void)Rte_Call_LFAntennaDiagnostic_P_GetDiagnosticResult(&LFDiagResultValid,
                                                            LFAntSTG,
                                                            LFAntSTB, 
                                                            LFAntOC, 
                                                            LFAntOT);
   //! ##### Read 12V Antenna status
   (void)Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(&DcDc12vRefVoltage,
                                                      &IsDcDc12vActivated, 
                                                      &FaultStatusDcDc);
   //! ##### Get Parameter value for left antenna Diagnostic
   DiagActLFAnt = (SEWS_Diag_Act_LF_P_P1V79_s_T*) Rte_Prm_P1V79_Diag_Act_LF_P_v(); // Pi, P1, P2, P3, P4
   if ((TRUE == IsDcDc12vActivated) 
      && (1u == LFDiagResultValid))
   {
      //! #### Processing for Short to Ground
      if ((1u == LFAntSTG[0]) 
         && (1u == DiagActLFAnt->PiInterface)) // Pi
      {
         (void)Rte_Call_Event_D1FM3_11_LFPi_STG_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         (void)Rte_Call_Event_D1FM3_11_LFPi_STG_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      }
      if ((1u == LFAntSTG[1]) 
         && (1u == DiagActLFAnt->P1Interface)) // P1
      {
         (void)Rte_Call_Event_D1FM5_11_LFP1_STG_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         (void)Rte_Call_Event_D1FM5_11_LFP1_STG_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      }
      if ((1u == LFAntSTG[2]) 
         && (1u == DiagActLFAnt->P2Interface)) // P2
      {
         (void)Rte_Call_Event_D1FM6_11_LFP2_STG_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         (void)Rte_Call_Event_D1FM6_11_LFP2_STG_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      }
      if ((1u == LFAntSTG[3]) 
         && (1u == DiagActLFAnt->P3Interface)) // P3
      {
         (void)Rte_Call_Event_D1FM7_11_LFP3_STG_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         (void)Rte_Call_Event_D1FM7_11_LFP3_STG_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      }
      if ((1u == LFAntSTG[4]) 
         && (1u == DiagActLFAnt->P4Interface)) // P4
      {
         (void)Rte_Call_Event_D1FM4_11_LFP4_STG_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         (void)Rte_Call_Event_D1FM4_11_LFP4_STG_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      }
      //! #### Processing for Short to Battery
      if ((1u == LFAntSTB[0]) 
         && (1u == DiagActLFAnt->PiInterface)) // Pi
      {
         (void)Rte_Call_Event_D1FM3_12_LFPi_STB_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         (void)Rte_Call_Event_D1FM3_12_LFPi_STB_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      }
      if ((1u == LFAntSTB[1]) 
         && (1u == DiagActLFAnt->P1Interface)) // P1
      {
         (void)Rte_Call_Event_D1FM5_12_LFP1_STB_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         (void)Rte_Call_Event_D1FM5_12_LFP1_STB_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      }
      if ((1u == LFAntSTB[2]) 
         && (1u == DiagActLFAnt->P2Interface)) // P2
      {
         (void)Rte_Call_Event_D1FM6_12_LFP2_STB_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         (void)Rte_Call_Event_D1FM6_12_LFP2_STB_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      }
      if ((1u == LFAntSTB[3]) 
         && (1u == DiagActLFAnt->P3Interface)) // P3
      {
         (void)Rte_Call_Event_D1FM7_12_LFP3_STB_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         (void)Rte_Call_Event_D1FM7_12_LFP3_STB_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      }
      if ((1u == LFAntSTB[4]) 
         && (1u == DiagActLFAnt->P4Interface)) // P4
      {
         (void)Rte_Call_Event_D1FM4_12_LFP4_STB_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         (void)Rte_Call_Event_D1FM4_12_LFP4_STB_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      }
      
      //! #### Processing for Open circuit
      if ((1u == LFAntOC[0]) 
         && (1u == DiagActLFAnt->PiInterface)) // Pi
      {
         (void)Rte_Call_Event_D1FM3_13_LFPi_OC_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         (void)Rte_Call_Event_D1FM3_13_LFPi_OC_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      }
      
      if ((1u == LFAntOC[4]) 
         && (1u == DiagActLFAnt->P4Interface)) // P4
      {
         (void)Rte_Call_Event_D1FM4_13_LFP4_OC_SetEventStatus(DEM_EVENT_STATUS_FAILED);
      }
      else
      {
         (void)Rte_Call_Event_D1FM4_13_LFP4_OC_SetEventStatus(DEM_EVENT_STATUS_PASSED);
      }
   }
   else
   {
      // Do nothing
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'ADI01_03_DTC_Handler'
//!
//!
//!======================================================================================
static void __inline__ ADI01_03_DTC_Handler(void)
{
   Std_ReturnType         retval          = RTE_E_OK;
   VGTT_EcuPinVoltage_0V2 AdiPinVoltage   = 0U;
   VGTT_EcuPinVoltage_0V2 BatteryVoltage  = 0U;
   VGTT_EcuPinFaultStatus FaultStatus     = 0U;
   //! ##### Rte call to read  AdiPinVoltage,BatteryVoltage,FaultStatus of ADI1
   retval = Rte_Call_AdiInterface_P_GetAdiPinState_CS(1U,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   if (RTE_E_OK == retval)
   {
      //! #### STG Fault detection For ADI1
      if (OnState_FaultDetected_STG == FaultStatus)
      {
         Rte_Call_Event_D1E1X_11_ADI1_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STG != FaultStatus)
      {
         Rte_Call_Event_D1E1X_11_ADI1_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }

      
      //! ##### STB Fault detection For ADI1
      if (OnState_FaultDetected_STB == FaultStatus)
      {
         Rte_Call_Event_D1E1X_13_ADI1_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STB != FaultStatus)
      {
         Rte_Call_Event_D1E1X_13_ADI1_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### Rte call to read  AdiPinVoltage,BatteryVoltage,FaultStatus of ADI2
   retval = Rte_Call_AdiInterface_P_GetAdiPinState_CS(2U,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   if (RTE_E_OK == retval)
   {
      //! #### STG Fault detection For ADI2
      if (OnState_FaultDetected_STG == FaultStatus)
      {
         Rte_Call_Event_D1E1Y_11_ADI2_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STG != FaultStatus)
      {
         Rte_Call_Event_D1E1Y_11_ADI2_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
      //! #### STB Fault detection For ADI2
      if (OnState_FaultDetected_STB == FaultStatus)
      {
         Rte_Call_Event_D1E1Y_13_ADI2_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STB != FaultStatus)
      {
         Rte_Call_Event_D1E1Y_13_ADI2_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### rte call to read  AdiPinVoltage,BatteryVoltage,FaultStatus of ADI3
   retval = Rte_Call_AdiInterface_P_GetAdiPinState_CS(3U,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   if (RTE_E_OK == retval)
   {
      //! #### STG Fault detection For ADI3      
      if (OnState_FaultDetected_STG == FaultStatus)
      {
         Rte_Call_Event_D1E1Z_11_ADI3_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STG != FaultStatus)
      {
         Rte_Call_Event_D1E1Z_11_ADI3_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }         
      //! #### STB Fault detection For ADI3
      if (OnState_FaultDetected_STB == FaultStatus)
      {
         Rte_Call_Event_D1E1Z_13_ADI3_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STB != FaultStatus)
      {
         Rte_Call_Event_D1E1Z_13_ADI3_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
   }
   else
   {
      // do nothing : MISRA
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'ADI04_06_DTC_Handler'
//!
//!
//!======================================================================================
static void __inline__ ADI04_06_DTC_Handler(void)
{
   Std_ReturnType         retval         = RTE_E_OK;
   VGTT_EcuPinVoltage_0V2 AdiPinVoltage  = 0U;
   VGTT_EcuPinVoltage_0V2 BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus FaultStatus    = 0U;
   //! ##### rte call to read  AdiPinVoltage,BatteryVoltage,FaultStatus of ADI4
   retval = Rte_Call_AdiInterface_P_GetAdiPinState_CS(4U,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   if (RTE_E_OK == retval)
   {
      //! #### STG Fault detection For ADI4
      if (OnState_FaultDetected_STG == FaultStatus)
      {
         Rte_Call_Event_D1E10_11_ADI4_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STG != FaultStatus)
      {
         Rte_Call_Event_D1E10_11_ADI4_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
          // Do nothing
      }         
      //! #### STB Fault detection For ADI4
      if (OnState_FaultDetected_STB == FaultStatus)
      {
         Rte_Call_Event_D1E10_13_ADI4_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STB != FaultStatus)
      {
         Rte_Call_Event_D1E10_13_ADI4_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### rte call to read  AdiPinVoltage,BatteryVoltage,FaultStatus of ADI5
   retval = Rte_Call_AdiInterface_P_GetAdiPinState_CS(5U,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   if (RTE_E_OK == retval)
   {
      //! #### STG Fault detection For ADI5
      if (OnState_FaultDetected_STG == FaultStatus)
      {
         Rte_Call_Event_D1E11_11_ADI5_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STG != FaultStatus)
      {
         Rte_Call_Event_D1E11_11_ADI5_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // Do nothing      
      }
      //! #### STB Fault detection For ADI5    
      if (OnState_FaultDetected_STB == FaultStatus)
      {
         Rte_Call_Event_D1E11_13_ADI5_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STB != FaultStatus)
      {
         Rte_Call_Event_D1E11_13_ADI5_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### rte call to read  AdiPinVoltage,BatteryVoltage,FaultStatus of ADI6
   retval = Rte_Call_AdiInterface_P_GetAdiPinState_CS(6U,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   if (RTE_E_OK == retval)
   {      
      //! #### STG Fault detection For ADI6
      if (OnState_FaultDetected_STG == FaultStatus)
      {
          Rte_Call_Event_D1E12_11_ADI6_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STG != FaultStatus)
      {
          Rte_Call_Event_D1E12_11_ADI6_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }         
      //! #### STB Fault detection For ADI6
      if (OnState_FaultDetected_STB == FaultStatus)
      {
         Rte_Call_Event_D1E12_13_ADI6_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STB != FaultStatus)
      {
         Rte_Call_Event_D1E12_13_ADI6_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
   }
   else
   {
      // do nothing : MISRA
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'ADI07_09_DTC_Handler'
//!
//!
//!======================================================================================
static void __inline__ ADI07_09_DTC_Handler(void)
{
   Std_ReturnType         retval         = RTE_E_OK;
   VGTT_EcuPinVoltage_0V2 AdiPinVoltage  = 0U;
   VGTT_EcuPinVoltage_0V2 BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus FaultStatus    = 0U;
   //! ##### rte call to read  AdiPinVoltage,BatteryVoltage,FaultStatus of ADI7 
   retval = Rte_Call_AdiInterface_P_GetAdiPinState_CS(7U,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   if (RTE_E_OK == retval)
   {
      //! #### STG Fault detection For ADI7
      if (OnState_FaultDetected_STG == FaultStatus)
      {
         Rte_Call_Event_D1E13_11_ADI7_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STG != FaultStatus)
      {
          Rte_Call_Event_D1E13_11_ADI7_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // Do nothing
      }      
      //! #### STB Fault detection For ADI7
      if (OnState_FaultDetected_STB == FaultStatus)
      {
         Rte_Call_Event_D1E13_13_ADI7_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STB != FaultStatus)
      {
         Rte_Call_Event_D1E13_13_ADI7_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### rte call to read  AdiPinVoltage,BatteryVoltage,FaultStatus of ADI8
   retval = Rte_Call_AdiInterface_P_GetAdiPinState_CS(8U,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   if (RTE_E_OK == retval)
   {
      //! #### STG Fault detection For ADI8
      if (OnState_FaultDetected_STG == FaultStatus)
      {
          Rte_Call_Event_D1E14_11_ADI8_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STG != FaultStatus)
      {
          Rte_Call_Event_D1E14_11_ADI8_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }      
      //! #### STB Fault detection For ADI8
      if (OnState_FaultDetected_STB == FaultStatus)
      {
         Rte_Call_Event_D1E14_13_ADI8_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STB != FaultStatus)
      {
         Rte_Call_Event_D1E14_13_ADI8_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### rte call to read  AdiPinVoltage,BatteryVoltage,FaultStatus of ADI9
   retval = Rte_Call_AdiInterface_P_GetAdiPinState_CS(9U,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   if (RTE_E_OK == retval)
   {
      //! #### STG Fault detection For ADI9
      if (OnState_FaultDetected_STG == FaultStatus)
      {
         Rte_Call_Event_D1E15_11_ADI9_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STG != FaultStatus)
      {
         Rte_Call_Event_D1E15_11_ADI9_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }      
      //! #### STB Fault detection For ADI9
      if (OnState_FaultDetected_STB == FaultStatus)
      {
         Rte_Call_Event_D1E15_13_ADI9_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STB != FaultStatus)
      {
         Rte_Call_Event_D1E15_13_ADI9_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
   }
   else
   {
      // do nothing : MISRA
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'ADI10_12_DTC_Handler'
//!
//!
//!======================================================================================
static void __inline__ ADI10_12_DTC_Handler(void)
{
   Std_ReturnType         retval         = RTE_E_OK;
   VGTT_EcuPinVoltage_0V2 AdiPinVoltage  = 0U;
   VGTT_EcuPinVoltage_0V2 BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus FaultStatus    = 0U;
   //! ##### Rte call to read  AdiPinVoltage,BatteryVoltage,FaultStatus of ADI10
   retval = Rte_Call_AdiInterface_P_GetAdiPinState_CS(10U,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   if (RTE_E_OK == retval)
   {
      //! #### STG Fault detection For ADI10
      if (OnState_FaultDetected_STG == FaultStatus)
      {
         Rte_Call_Event_D1E16_11_ADI10_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STG != FaultStatus)
      {
         Rte_Call_Event_D1E16_11_ADI10_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }      
      //! #### STB Fault detection For ADI10
      if (OnState_FaultDetected_STB == FaultStatus)
      {
         Rte_Call_Event_D1E16_13_ADI10_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STB != FaultStatus)
      {
         Rte_Call_Event_D1E16_13_ADI10_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### rte call to read  AdiPinVoltage,BatteryVoltage,FaultStatus of ADI11
   retval = Rte_Call_AdiInterface_P_GetAdiPinState_CS(11U,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   if (RTE_E_OK == retval)
   {
      //! #### STG Fault detection For ADI11   
      if (OnState_FaultDetected_STG == FaultStatus)
      {
         Rte_Call_Event_D1E17_11_ADI11_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STG != FaultStatus)
      {
         Rte_Call_Event_D1E17_11_ADI11_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
      //! #### STB Fault detection For ADI10   
      if (OnState_FaultDetected_STB == FaultStatus)
      {
         Rte_Call_Event_D1E17_13_ADI11_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STB != FaultStatus)
      {
         Rte_Call_Event_D1E17_13_ADI11_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### rte call to read  AdiPinVoltage,BatteryVoltage,FaultStatus of ADI12
   retval = Rte_Call_AdiInterface_P_GetAdiPinState_CS(12U,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   if (RTE_E_OK == retval)
   {
      //! #### STG Fault detection For ADI12
      if (OnState_FaultDetected_STG == FaultStatus)
      {
         Rte_Call_Event_D1E18_11_ADI12_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STG != FaultStatus)
      {
         Rte_Call_Event_D1E18_11_ADI12_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
      //! #### STB Fault detection For ADI12
      if (OnState_FaultDetected_STB == FaultStatus)
      {
         Rte_Call_Event_D1E18_13_ADI12_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STB != FaultStatus)
      {
         Rte_Call_Event_D1E18_13_ADI12_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
   }
   else
   {
      // do nothing : MISRA
   }
}
//!======================================================================================
//!
//! \brief 
//!  This function implements the logic for the 'ADI13_16_DTC_Handler'
//!
//!
//!======================================================================================
static void __inline__ ADI13_16_DTC_Handler(void)
{
   Std_ReturnType retval                 = RTE_E_OK;
   VGTT_EcuPinVoltage_0V2 AdiPinVoltage  = 0U;
   VGTT_EcuPinVoltage_0V2 BatteryVoltage = 0U;
   VGTT_EcuPinFaultStatus FaultStatus    = 0U; 
   //! ##### Rte call to read  AdiPinVoltage,BatteryVoltage,FaultStatus of ADI13
   retval = Rte_Call_AdiInterface_P_GetAdiPinState_CS(13U,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   if (RTE_E_OK == retval)
   {
      //! #### STG Fault detection For ADI13
      if (OnState_FaultDetected_STG == FaultStatus)
      {
         Rte_Call_Event_D1E19_11_ADI13_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STG != FaultStatus)
      {
         Rte_Call_Event_D1E19_11_ADI13_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // Do Nothing
      }
      //! #### STB Fault detection For ADI13
      if (OnState_FaultDetected_STB == FaultStatus)
      {
         Rte_Call_Event_D1E19_13_ADI13_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STB != FaultStatus)
      {
         Rte_Call_Event_D1E19_13_ADI13_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
          // Do nothing
      }
   }
   else
   {
      // Do nothing
   }
   //! ##### Rte call to read  AdiPinVoltage,BatteryVoltage,FaultStatus of ADI14
   retval = Rte_Call_AdiInterface_P_GetAdiPinState_CS(14U,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   if (RTE_E_OK == retval)
   {   
      //! #### STG Fault detection For ADI14   
      if (OnState_FaultDetected_STG == FaultStatus)
      {
         Rte_Call_Event_D1E2A_11_ADI14_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STG != FaultStatus)
      {
         Rte_Call_Event_D1E2A_11_ADI14_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }

      
      //! ##### STB Fault detection For ADI14
      if (OnState_FaultDetected_STB == FaultStatus)
      {
         Rte_Call_Event_D1E2A_13_ADI14_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STB != FaultStatus)
      {
         Rte_Call_Event_D1E2A_13_ADI14_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### Rte call to read  AdiPinVoltage,BatteryVoltage,FaultStatus of ADI15
   retval = Rte_Call_AdiInterface_P_GetAdiPinState_CS(15U,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   if (RTE_E_OK == retval)
   {
      //! #### STG Fault detection For ADI15   
      if (OnState_FaultDetected_STG == FaultStatus)
      {
         Rte_Call_Event_D1E2B_11_ADI15_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STG != FaultStatus)
      {
         Rte_Call_Event_D1E2B_11_ADI15_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
      //! #### STB Fault detection For ADI15
      if (OnState_FaultDetected_STB == FaultStatus)
      {
         Rte_Call_Event_D1E2B_13_ADI15_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STB != FaultStatus)
      {
         Rte_Call_Event_D1E2B_13_ADI15_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### Rte call to read  AdiPinVoltage,BatteryVoltage,FaultStatus of ADI16
   retval = Rte_Call_AdiInterface_P_GetAdiPinState_CS(16U,
                                                      &AdiPinVoltage,
                                                      &BatteryVoltage,
                                                      &FaultStatus);
   if (RTE_E_OK == retval)
   {
      //! #### STG Fault detection For ADI16
      if (OnState_FaultDetected_STG == FaultStatus)
      {
         (void)Rte_Call_Event_D1E2C_11_ADI16_STG_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STG != FaultStatus)
      {
         (void)Rte_Call_Event_D1E2C_11_ADI16_STG_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }      
      //! #### STB Fault detection For ADI16
      if (OnState_FaultDetected_STB == FaultStatus)
      {
         (void)Rte_Call_Event_D1E2C_13_ADI16_OC_SetEventStatus(DEM_EVENT_STATUS_PREFAILED);
      }
      else if (OnState_FaultDetected_STB != FaultStatus)
      {
         (void)Rte_Call_Event_D1E2C_13_ADI16_OC_SetEventStatus(DEM_EVENT_STATUS_PREPASSED);
      }
      else
      {
         // do nothing : MISRA
      }
   }
   else
   {
      // do nothing : MISRA
   }
}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
