/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  DiagnosticMonitor_COM.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  DiagnosticMonitor_COM
 *  Generated at:  Tue Jul 21 15:22:20 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <DiagnosticMonitor_COM>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
//!======================================================================================
//! \file DiagnosticMonitor_COM.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup Platform 
//! @{
//! @addtogroup Platform_SCIM 
//! @{
//! @addtogroup DiagnosticComponent
//! @{
//! @addtogroup DiagnosticMonitor_COM
//! @{
//!
//! \brief
//! DiagnosticMonitor_COM SWC.
//! ASIL Level : QM.
//! This module implements the logic for the DiagnosticMonitor_COM runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_DiagnosticMonitor_COM.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "DiagnosticMonitor_COM.h"


#define PCODE_P1DXX_FMSgateway_Act          (Rte_Prm_P1DXX_FMSgateway_Act_v())
#define PCODE_P1V8I_BB2_active              (Rte_Prm_P1V8I_BB2_active_v())
#define PCODE_P1V8J_BB1_active              (Rte_Prm_P1V8J_BB1_active_v())
#define PCODE_P1V8K_CabSubnet_active        (Rte_Prm_P1V8K_CabSubnet_active_v())
#define PCODE_P1V8L_SecuritySubnet_active   (Rte_Prm_P1V8L_SecuritySubnet_active_v())
#define PCODE_P1V8M_CAN6_active             (Rte_Prm_P1V8M_CAN6_active_v())

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1DXX_FMSgateway_Act_v(void)
 *   boolean Rte_Prm_P1V8I_BB2_active_v(void)
 *   boolean Rte_Prm_P1V8J_BB1_active_v(void)
 *   boolean Rte_Prm_P1V8K_CabSubnet_active_v(void)
 *   boolean Rte_Prm_P1V8L_SecuritySubnet_active_v(void)
 *   boolean Rte_Prm_P1V8M_CAN6_active_v(void)
 *
 *********************************************************************************************************************/


#define DiagnosticMonitor_COM_START_SEC_CODE
#include "DiagnosticMonitor_COM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DiagnosticMonitor_COM_10ms_Runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Mode Interfaces:
 * ================
 *   uint8 Rte_Mode_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void)
 *   Modes of Rte_ModeType_BswMRteMDG_CanBusOff:
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom
 *   - RTE_TRANSITION_BswMRteMDG_CanBusOff
 *   uint8 Rte_Mode_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void)
 *   Modes of Rte_ModeType_BswMRteMDG_CanBusOff:
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom
 *   - RTE_TRANSITION_BswMRteMDG_CanBusOff
 *   uint8 Rte_Mode_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void)
 *   Modes of Rte_ModeType_BswMRteMDG_CanBusOff:
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom
 *   - RTE_TRANSITION_BswMRteMDG_CanBusOff
 *   uint8 Rte_Mode_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void)
 *   Modes of Rte_ModeType_BswMRteMDG_CanBusOff:
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom
 *   - RTE_TRANSITION_BswMRteMDG_CanBusOff
 *   uint8 Rte_Mode_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void)
 *   Modes of Rte_ModeType_BswMRteMDG_CanBusOff:
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom
 *   - RTE_TRANSITION_BswMRteMDG_CanBusOff
 *   uint8 Rte_Mode_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void)
 *   Modes of Rte_ModeType_BswMRteMDG_CanBusOff:
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom
 *   - RTE_TRANSITION_BswMRteMDG_CanBusOff
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1A6D_88_BB2Net_ComFail_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1A6E_88_BB1Net_ComFail_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1A6F_88_CabNet_ComFail_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1A6H_88_SecurityNet_ComFail_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1A6L_88_FMSNet_ComFail_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E4Q_88_CAN6_ComFail_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DiagnosticMonitor_COM_10ms_Runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//!
//! \brief
//! This function implements the cyclic execution runnable logic for the DiagnosticMonitor_COM_10ms_Runnable
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DiagnosticMonitor_COM_CODE) DiagnosticMonitor_COM_10ms_Runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DiagnosticMonitor_COM_10ms_Runnable
 *********************************************************************************************************************/

   //! ###### Processing the monitoring of COM for Diagnostics    
   
	//! ##### Checking if CanBus  BB1 is active
   if(1U==PCODE_P1V8J_BB1_active)
   {
   	//! ##### FaultMonitoring of BB1 bus
		CanBus_BB1_StatusCheck();
   }
   else
   {
      // Do Nothing
   }
	//! ##### Checking if CanBus BB2 is active
   if(1U==PCODE_P1V8I_BB2_active)
   {
   	//! ##### FaultMonitoring of BB2 bus
		CANBus_BB2_StatusCheck();	
   }
   else
   {
      // Do Nothing
   }

	//! ##### Checking if CanBus CabSubnet is active
   if(1U==PCODE_P1V8K_CabSubnet_active)
   {     
      //! ##### FaultMonitoring of CabSubnet bus
		CANBus_CabSubNet_StatusCheck();
   }
   else
   {
      // Do Nothing
   } 
   
	//! ##### Checking if CanBus SecuritySubnet is active
   if(1U==PCODE_P1V8L_SecuritySubnet_active)
   {
      //! ##### FaultMonitoring of SecuritySubnet bus
		CANBus_SecSubNet_StatusCheck();
   }
   else
   {
      // Do Nothing
   }    
   
	//! ##### Checking if CanBus FMSgateway is active
   if(1U==PCODE_P1DXX_FMSgateway_Act)
   {     
      //! ##### FaultMonitoring of FMSgateway bus 
		 CANBus_FMSnet_StatusCheck();        
   }
   else
   {
      // Do Nothing
   }
   
	//! ##### Checking if CanBus CAN6 is active
   if(1U==PCODE_P1V8M_CAN6_active)
   {      
      //! ##### FaultMonitoring of CAN6 bus
		CANBus_CAN6_StatusCheck();    
   }
   else
   {
      // Do Nothing
   }
   

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

//! @}
//! @}
//! @}

#define DiagnosticMonitor_COM_STOP_SEC_CODE
#include "DiagnosticMonitor_COM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution logic for the CanBus_BB1_StatusCheck
//!
//!====================================================================================================================
static void CanBus_BB1_StatusCheck(void)
{
   uint8 CANBusStatus_BB1;  
   
   //! ##### Reading BB1 bus status
   CANBusStatus_BB1 = Rte_Mode_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff();

	//! #### If the bus status is CanBus of then set the Dem event status to failed
	if(RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff==CANBusStatus_BB1)
	{  
		Rte_Call_Event_D1A6E_88_BB1Net_ComFail_SetEventStatus(DEM_EVENT_STATUS_FAILED);  
	}
	//! #### If the bus status is normal then set the Dem event status to passed
	else if(RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom==CANBusStatus_BB1)
	{
		Rte_Call_Event_D1A6E_88_BB1Net_ComFail_SetEventStatus(DEM_EVENT_STATUS_PASSED);  
	}
	else
	{

	}
}
//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution logic for the CANBus_BB2_StatusCheck
//!
//!====================================================================================================================
static void CANBus_BB2_StatusCheck(void)
{
   uint8 CANBusStatus_BB2;
  
   //! ##### Reading BB2 bus status
   CANBusStatus_BB2       = Rte_Mode_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff();
		  
	//! #### If the bus status is CanBus off then set the Dem event status to failed
	if(RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff==CANBusStatus_BB2)
	{  
		Rte_Call_Event_D1A6D_88_BB2Net_ComFail_SetEventStatus(DEM_EVENT_STATUS_FAILED);  
	}
	//! #### If the bus status is normal then set the Dem event status to passed
	else if(RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom==CANBusStatus_BB2)
	{
		Rte_Call_Event_D1A6D_88_BB2Net_ComFail_SetEventStatus(DEM_EVENT_STATUS_PASSED); 
	}
	else
	{
	 
	} 
}



//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution logic for the CANBus_CabSubNet_StatusCheck
//!
//!====================================================================================================================

static void CANBus_CabSubNet_StatusCheck(void)
{
   uint8 CANBusStatus_CabSubNet; 
 
   //! ##### Reading CabSubnet bus status
   CANBusStatus_CabSubNet = Rte_Mode_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff();
		
   //! #### If the bus status is CanBus off then set the Dem event status to failed
	if(RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff==CANBusStatus_CabSubNet)
	{
		Rte_Call_Event_D1A6F_88_CabNet_ComFail_SetEventStatus(DEM_EVENT_STATUS_FAILED); 
	}
	//! #### If the bus status is normal then set the Dem event status to passed
	else if(RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom==CANBusStatus_CabSubNet)
	{
		Rte_Call_Event_D1A6F_88_CabNet_ComFail_SetEventStatus(DEM_EVENT_STATUS_PASSED); 
	}
	else
	{
	 
	}
}


//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution logic for the CANBus_SecSubNet_StatusCheck
//!
//!====================================================================================================================

static void CANBus_SecSubNet_StatusCheck(void)
{	
   uint8 CANBusStatus_SecSubNet;

   //! ##### Reading SecuritySubnet bus status   
   CANBusStatus_SecSubNet = Rte_Mode_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff();
			
	//! #### If the bus status is CanBus off then set the Dem event status to failed
	if(RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff==CANBusStatus_SecSubNet)
	{
		Rte_Call_Event_D1A6H_88_SecurityNet_ComFail_SetEventStatus(DEM_EVENT_STATUS_FAILED); 
	}
	//! #### If the bus status is normal then set the Dem event status to passed
	else if(RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom==CANBusStatus_SecSubNet)
	{
		Rte_Call_Event_D1A6H_88_SecurityNet_ComFail_SetEventStatus(DEM_EVENT_STATUS_PASSED); 
	}
	else
	{
	 
	}
}



//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution logic for the CANBus_FMSnet_StatusCheck
//!
//!====================================================================================================================

static void CANBus_FMSnet_StatusCheck(void)
{	
   uint8 CANBusStatus_FMSnet;
   
   //! ##### Reading FMSgateway bus status
   CANBusStatus_FMSnet    = Rte_Mode_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff();
			
	//! #### If the bus status is CanBus off then set the Dem event status to failed
	if(RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff==CANBusStatus_FMSnet)
	{  
		Rte_Call_Event_D1A6L_88_FMSNet_ComFail_SetEventStatus(DEM_EVENT_STATUS_FAILED); 
	}
	//! #### If the bus status is normal then set the Dem event status to passed
	else if(RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom==CANBusStatus_FMSnet)
	{
		Rte_Call_Event_D1A6L_88_FMSNet_ComFail_SetEventStatus(DEM_EVENT_STATUS_PASSED); 
	}
	else
	{
	 
	}
}


//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution logic for the CANBus_CAN6_StatusCheck
//!
//!====================================================================================================================
static void CANBus_CAN6_StatusCheck(void)
{		
   uint8 CANBusStatus_CAN6;

   //! ##### Reading CAN6 bus status
   CANBusStatus_CAN6 = Rte_Mode_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff();
		
	//! #### If the bus status is CanBus off then set the Dem event status to failed
	if(RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff==CANBusStatus_CAN6)
	{   
		Rte_Call_Event_D1E4Q_88_CAN6_ComFail_SetEventStatus(DEM_EVENT_STATUS_FAILED); 
	}
	//! #### If the bus status is normal then set the Dem event status to passed
	else if(RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom==CANBusStatus_CAN6)
	{
		Rte_Call_Event_D1E4Q_88_CAN6_ComFail_SetEventStatus(DEM_EVENT_STATUS_PASSED); 
	} 
	else
	{
	 
	} 
}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
