/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  VOL_DIDServer.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  VOL_DIDServer
 *  Generation Time:  2020-09-21 13:44:24
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <VOL_DIDServer>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_OpStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Distance32bit_T
 *   4261412864 - 4278190079 Error ; 4278190080 - 4294967295 Not available
 *
 * Temperature16bit_T
 *   65024 - 65279 Error ; 65280 - 65535 Not available
 *
 *********************************************************************************************************************/

#include "Rte_VOL_DIDServer.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "Rte_Dcm_Type.h"
#include "BuildId.inc"

#define PART_NUMBER_SIZE           8u
#define REVISION_NUMBER_SIZE       3u
#define SERIAL_NUMBER_SIZE         8u


#define NUMBER_OF_DATA_MODULES   2u
#define NUMBER_OF_SW_MODULES     2u


#define ModuleTypeHW          0x01u
#define ModuleTypeSubHW       0x02u
#define ModuleTypeBoot        0x03u    /* Used to identify updateable PBL */
#define ModuleTypeMSW         0x05u
#define ModuleTypeCSW         0x06u
#define ModuleTypeData        0x07u
#define ModuleTypeDataPatch   0x08u



typedef struct
{
 uint8 HwPartNumber[PART_NUMBER_SIZE];
 uint8 HwRevision[REVISION_NUMBER_SIZE];
 uint8 SerialNumber[SERIAL_NUMBER_SIZE];
 uint8 SystemNameOrEngine[PART_NUMBER_SIZE];
}FblProdLogType;

typedef struct
{
 uint8 PartNumber[PART_NUMBER_SIZE];
 uint8 Revision[REVISION_NUMBER_SIZE];
}FblPBLPNType;



#pragma section DstVarPartNumber ".DstVarPartNumber"  /* Create a named section. Required by postbuild process */
#pragma use_section DstVarPartNumber DstVar_PartNumber  /* Place variable in named section. Required by postbuild process */
#pragma use_section DstVarPartNumber DstVar_RevNumber /* Place variable in named section. Required by postbuild process */

#pragma section PostBuildPartNumber ".PostBuildPartNumber" /* Create a named section. Required by postbuild process */
#pragma use_section PostBuildPartNumber PostBuild_PartNumber  /* Place variable in named section. Required by postbuild process */
#pragma use_section PostBuildPartNumber PostBuild_RevNumber  /* Place variable in named section. Required by postbuild process */

#pragma section MSWPartNumber ".MSWPartNumber" /* Create a named section. Required by postbuild process */
#pragma use_section MSWPartNumber MSW_PartNumber  /* Place variable in named section. Required by postbuild process */
#pragma use_section MSWPartNumber MSW_RevNumber  /* Place variable in named section. Required by postbuild process */

#pragma section HWPartNumber "HWPartNumber" /* Create a named section. Required by postbuild process */
#pragma use_section HWPartNumber fblAp_ProdLog  /* Place variable in named section. Required by postbuild process */

#pragma section PBLPartNumber "PBLPartNumber" /* Create a named section. Required by postbuild process */
#pragma use_section PBLPartNumber fblAp_PblPN  /* Place variable in named section. Required by postbuild process */

const signed char MSW_PartNumber[PART_NUMBER_SIZE]          = {'2','7','7','7','0','0','7','0'};
const signed char MSW_RevNumber[REVISION_NUMBER_SIZE]       = {'B','0','1'};

const signed char PostBuild_PartNumber[PART_NUMBER_SIZE]    = {'1','2','3','4','5','6','7','8'};
const signed char PostBuild_RevNumber[REVISION_NUMBER_SIZE] = {'C','0','1'};

const signed char DstVar_PartNumber[PART_NUMBER_SIZE]          = {'1','2','3','4','5','6','7','8'};
const signed char DstVar_RevNumber[REVISION_NUMBER_SIZE]       = {'C','0','1'};

/* PBL Production log (HW_PN,HW_REV,HW_SN & SystemNameOrEngineInfo) */

const FblProdLogType  fblAp_ProdLog  = {{ '2', '3', '2', '8', '3', '3', '6', '1' },
										{ 'A', '1', '2' },
										{ '1', '2', '3', '4', '5', '6', '7', '8' },
										{0,11,0,3,0,0,0,0}};


/* PBL PartNumber and Revision Info*/
const FblPBLPNType  fblAp_PblPN  = {{ '2', '3', '9', '4', '5', '6', '4', '7' },
                                    { 'A', '1', '2' }};

const signed char BuildTagReport[64] = BuildTag;

static uint8 started_U8 = (uint8)0;
static uint8  nofLinSlaves_U8;
static uint8 linDiagRespBuffer_pU8[450];
static LinDiagServiceStatus  linStatus_U8 = LinDiagService_None;


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Distance32bit_T: Integer in interval [0...4294967295]
 *   Unit: [m], Factor: 1, Offset: 0
 * Temperature16bit_T: Integer in interval [0...65535]
 *   Unit: [DegreeC], Factor: 1, Offset: -273
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_OpStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * LinDiagBusInfo: Enumeration of integer in interval [0...255] with enumerators
 *   None (0U)
 *   LinDiag_BUS1 (1U)
 *   LinDiag_BUS2 (2U)
 *   LinDiag_BUS3 (3U)
 *   LinDiag_BUS4 (4U)
 *   LinDiag_BUS5 (5U)
 *   LinDiag_SpareBUS1 (6U)
 *   LinDiag_SpareBUS2 (7U)
 *   LinDiag_ALL_BUSSES (8U)
 * LinDiagServiceStatus: Enumeration of integer in interval [0...255] with enumerators
 *   LinDiagService_None (0U)
 *   LinDiagService_Pending (1U)
 *   LinDiagService_Completed (2U)
 *   LinDiagService_Error (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Dcm_Data16ByteType: Array with 16 element(s) of type uint8
 * Dcm_Data17ByteType: Array with 17 element(s) of type uint8
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data221ByteType: Array with 221 element(s) of type uint8
 * Dcm_Data241ByteType: Array with 241 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 * Dcm_Data406ByteType: Array with 406 element(s) of type uint8
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 * Dcm_Data64ByteType: Array with 64 element(s) of type uint8
 * Dcm_Data8ByteType: Array with 8 element(s) of type uint8
 * SEWS_ChassisId_CHANO_T: Array with 16 element(s) of type uint8
 * SEWS_VIN_VINNO_T: Array with 17 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v(void)
 *   boolean Rte_Prm_P1C54_FactoryModeActive_v(void)
 *   uint8 *Rte_Prm_CHANO_ChassisId_v(void)
 *     Returnvalue: uint8* is of type SEWS_ChassisId_CHANO_T
 *   uint8 *Rte_Prm_VINNO_VIN_v(void)
 *     Returnvalue: uint8* is of type SEWS_VIN_VINNO_T
 *
 *********************************************************************************************************************/


#define VOL_DIDServer_START_SEC_CODE
#include "VOL_DIDServer_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_CHANO_Data_CHANO_ChassisId_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_CHANO_Data_CHANO_ChassisId>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_CHANO_Data_CHANO_ChassisId_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data16ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_CHANO_Data_CHANO_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_CHANO_Data_CHANO_ChassisId_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_CHANO_Data_CHANO_ChassisId_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_CHANO_Data_CHANO_ChassisId_ReadData (returns application error)
 *********************************************************************************************************************/

  const SEWS_ChassisId_CHANO_T * chassiNo = (const SEWS_ChassisId_CHANO_T *)Rte_Prm_CHANO_ChassisId_v(); /*lint !e545 ANSA 100629, PC-LINT 545. Suspicious use of & disabled*/

   Data[0]  = (uint8)(*chassiNo)[0];
   Data[1]  = (uint8)(*chassiNo)[1];
   Data[2]  = (uint8)(*chassiNo)[2];
   Data[3]  = (uint8)(*chassiNo)[3];
   Data[4]  = (uint8)(*chassiNo)[4];
   Data[5]  = (uint8)(*chassiNo)[5];
   Data[6]  = (uint8)(*chassiNo)[6];
   Data[7]  = (uint8)(*chassiNo)[7];
   Data[8]  = (uint8)(*chassiNo)[8];
   Data[9]  = (uint8)(*chassiNo)[9];
   Data[10] = (uint8)(*chassiNo)[10];
   Data[11] = (uint8)(*chassiNo)[11];
   Data[12] = (uint8)(*chassiNo)[12];
   Data[13] = (uint8)(*chassiNo)[13];
   Data[14] = (uint8)(*chassiNo)[14];
   Data[15] = (uint8)(*chassiNo)[15];
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1AFR_Data_P1AFR_OutdoorTemperature>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AmbientAirTemperature_AmbientAirTemperature(Temperature16bit_T *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1AFR_Data_P1AFR_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData (returns application error)
 *********************************************************************************************************************/
  Temperature16bit_T Temperature;

  Rte_Read_AmbientAirTemperature_AmbientAirTemperature(&Temperature);

   /* PR#825: PR Tracker: T2P_I-009000 CIOM: Reversed byte order for Odometer value in P1AFS */
   Data[0] = (uint8)(((UInt16)0xff00u & (UInt16)Temperature)>>8);
   Data[1] = (uint8)(((UInt16)0x00ffu & (UInt16)Temperature));

   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1AFS_Data_P1AFS_Odometer_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1AFS_Data_P1AFS_Odometer>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes(Distance32bit_T *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1AFS_Data_P1AFS_Odometer_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1AFS_Data_P1AFS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1AFS_Data_P1AFS_Odometer_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1AFS_Data_P1AFS_Odometer_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1AFS_Data_P1AFS_Odometer_ReadData (returns application error)
 *********************************************************************************************************************/
   Distance32bit_T Distance;

    Rte_Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes(&Distance);

   /* PR#825: PR Tracker: T2P_I-009000 CIOM: Reversed byte order for Odometer value in P1AFS */
    Data[0] = (uint8)(((UInt32)0xff000000u & (UInt32)Distance)>>24);
    Data[1] = (uint8)(((UInt32)0x00ff0000u & (UInt32)Distance)>>16);
    Data[2] = (uint8)(((UInt32)0x0000ff00u & (UInt32)Distance)>>8);
    Data[3] = (uint8)(((UInt32)0x000000ffu & (UInt32)Distance));


   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1AFT_Data_P1AFT_VehicleMode>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1AFT_Data_P1AFT_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData (returns application error)
 *********************************************************************************************************************/

   VehicleMode_T VehicleMode;

   Rte_Read_VehicleModeInternal_VehicleMode(&VehicleMode);

   Data[0] = (uint8)VehicleMode;


   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data406ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1ALA_Data_P1ALA_DCM_E_PENDING
 *   RTE_E_DataServices_P1ALA_Data_P1ALA_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData_doc
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData (returns application error)
 *********************************************************************************************************************/

   const UInt8* bootHWPartNumber = fblAp_ProdLog.HwPartNumber; /*lint !e928 ANSA 100629, MISRA 2004 Rule 11.4 , Pointers into partnumberblock */
   const UInt8* bootHWRevision = fblAp_ProdLog.HwRevision; /*lint !e928 ANSA 100629, MISRA 2004 Rule 11.4 , Pointers into partnumberblock */
   const UInt8* bootSerial = fblAp_ProdLog.SerialNumber; /*lint !e928 ANSA 100629, MISRA 2004 Rule 11.4 , Pointers into partnumberblock */
   UInt16 index = 0u;

   uint8 LoopVar1;
   uint8 LoopVar2;
   uint16 BufferIndexVar1;

   /* MAKE */
    Data[index++] = (UInt8)'T';
    Data[index++] = (UInt8)'E';
    Data[index++] = (UInt8)'A';
    Data[index++] = (UInt8)'2';
    Data[index++] = (UInt8)'+';

   /* Number of modules */
    Data[index++] = (uint8)(1u + nofLinSlaves_U8);

   /* Module type */
    Data[index++] = 0x01u; /* HW */

   /* HW part number */
    Data[index++] = (UInt8)bootHWPartNumber[0];
    Data[index++] = (UInt8)bootHWPartNumber[1];
    Data[index++] = (UInt8)bootHWPartNumber[2];
    Data[index++] = (UInt8)bootHWPartNumber[3];
    Data[index++] = (UInt8)bootHWPartNumber[4];
    Data[index++] = (UInt8)bootHWPartNumber[5];
    Data[index++] = (UInt8)bootHWPartNumber[6];
    Data[index++] = (UInt8)bootHWPartNumber[7];

   /* HW revision level */
    Data[index++] = (UInt8)bootHWRevision[0];
    Data[index++] = (UInt8)bootHWRevision[1];
    Data[index++] = (UInt8)bootHWRevision[2];

   /* Serial number */
    Data[index++] = bootSerial[0];
    Data[index++] = bootSerial[1];
    Data[index++] = bootSerial[2];
    Data[index++] = bootSerial[3];
    Data[index++] = bootSerial[4];
    Data[index++] = bootSerial[5];
    Data[index++] = bootSerial[6];
    Data[index++] = bootSerial[7];

   BufferIndexVar1 = (uint16)0;
   for(LoopVar1=(uint8)0; LoopVar1<nofLinSlaves_U8; LoopVar1++)
   {
	   for(LoopVar2=(uint8)0; LoopVar2<(uint8)20; LoopVar2++)
	   {
		   Data[index++] = linDiagRespBuffer_pU8[BufferIndexVar1];
		   BufferIndexVar1++;
	   }
   }

   linStatus_U8 = (uint8)0;
   started_U8 = (uint8)0;

   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadDataLength
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadDataLength> of PortPrototype <DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_CddLinDiagServices_SlaveNodePnSnReq(LinDiagBusInfo LinBusInfo)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_CddLinDiagServices_SlaveNodePnSnResp(LinDiagServiceStatus *DiagServiceStatus, uint8 *NoOfLinSlaves, uint8 *LinDiagRespPNSN)
 *     Synchronous Server Invocation. Timeout: None
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadDataLength(Dcm_OpStatusType OpStatus, uint16 *DataLength)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1ALA_Data_P1ALA_DCM_E_PENDING
 *   RTE_E_DataServices_P1ALA_Data_P1ALA_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadDataLength_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadDataLength(Dcm_OpStatusType OpStatus, P2VAR(uint16, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) DataLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadDataLength (returns application error)
 *********************************************************************************************************************/
   
   const LinDiagBusInfo linChannels_U8 = LinDiag_ALL_BUSSES; // 0x1F = All LIN busses.
   Std_ReturnType retValue;

	if(started_U8 == (uint8)0)
	{
		 started_U8 = (uint8)1;
		 nofLinSlaves_U8 = (uint8)0;
		 linStatus_U8 = (uint8)0;
		 
		 Rte_Call_CddLinDiagServices_SlaveNodePnSnReq(linChannels_U8);
		 
		 retValue = RTE_E_DataServices_P1ALA_Data_P1ALA_DCM_E_PENDING;
	}
    else
    {
       Rte_Call_CddLinDiagServices_SlaveNodePnSnResp(&linStatus_U8, &nofLinSlaves_U8, &linDiagRespBuffer_pU8[0]);
       
       if(linStatus_U8 == LinDiagService_Pending)
       {
		  /* Wait. */
		  retValue = RTE_E_DataServices_P1ALA_Data_P1ALA_DCM_E_PENDING;
	   }
	   else
	   {
         *DataLength = (UInt16)((UInt16)6 + 20 + ((UInt16)nofLinSlaves_U8)*(UInt16)20);
		 linStatus_U8 = LinDiagService_None;
		 retValue = RTE_E_OK;
	   }

	}

  return retValue;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1ALB_Data_P1ALB_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData (returns application error)
 *********************************************************************************************************************/

   const uint8* bootHWSystemNameEngineType = &fblAp_ProdLog.SystemNameOrEngine[0];    /*lint !e928 OTRI 120126, MISRA 2004 Rule 11.4 , Pointers into partnumberblock */

   /* System name or engine type */
    Data[0] = (uint8)bootHWSystemNameEngineType[0];
    Data[1] = (uint8)bootHWSystemNameEngineType[1];
    Data[2] = (uint8)bootHWSystemNameEngineType[2];
    Data[3] = (uint8)bootHWSystemNameEngineType[3];
    Data[4] = (uint8)bootHWSystemNameEngineType[4];
    Data[5] = (uint8)bootHWSystemNameEngineType[5];
    Data[6] = (uint8)bootHWSystemNameEngineType[6];
    Data[7] = (uint8)bootHWSystemNameEngineType[7];
   

   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1ALP_Data_P1ALP_ApplicationDataId>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data241ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1ALP_Data_P1ALP_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 index = 0u;

    Data[index++] = 2;//NUMBER_OF_DATA_MODULES; /*Number of modules*/
  
     /*V3P variability record*/
    Data[index++] = ModuleTypeData; /*Module type = Data*/
    Data[index++] = (UInt8)DstVar_PartNumber[0u];
    Data[index++] = (UInt8)DstVar_PartNumber[1u];
    Data[index++] = (UInt8)DstVar_PartNumber[2u];
    Data[index++] = (UInt8)DstVar_PartNumber[3u];
    Data[index++] = (UInt8)DstVar_PartNumber[4u];
    Data[index++] = (UInt8)DstVar_PartNumber[5u];
    Data[index++] = (UInt8)DstVar_PartNumber[6u];
    Data[index++] = (UInt8)DstVar_PartNumber[7u];
    Data[index++] = (UInt8)DstVar_RevNumber[0u];
    Data[index++] = (UInt8)DstVar_RevNumber[1u];
    Data[index++] = (UInt8)DstVar_RevNumber[2u];
  

   /*V3P postbuild record*/
    Data[index++] = ModuleTypeData; /*Module type = Data*/
    Data[index++] = (UInt8)PostBuild_PartNumber[0u];
    Data[index++] = (UInt8)PostBuild_PartNumber[1u];
    Data[index++] = (UInt8)PostBuild_PartNumber[2u];
    Data[index++] = (UInt8)PostBuild_PartNumber[3u];
    Data[index++] = (UInt8)PostBuild_PartNumber[4u];
    Data[index++] = (UInt8)PostBuild_PartNumber[5u];
    Data[index++] = (UInt8)PostBuild_PartNumber[6u];
    Data[index++] = (UInt8)PostBuild_PartNumber[7u];
    Data[index++] = (UInt8)PostBuild_RevNumber[0u];
    Data[index++] = (UInt8)PostBuild_RevNumber[1u];
    Data[index++] = (UInt8)PostBuild_RevNumber[2u];

   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadDataLength
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadDataLength> of PortPrototype <DataServices_P1ALP_Data_P1ALP_ApplicationDataId>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadDataLength(uint16 *DataLength)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1ALP_Data_P1ALP_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadDataLength_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadDataLength(P2VAR(uint16, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) DataLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadDataLength (returns application error)
 *********************************************************************************************************************/

   *DataLength = 1u + (12u * 2u);//NUMBER_OF_DATA_MODULES);
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data241ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1ALQ_Data_P1ALQ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData (returns application error)
 *********************************************************************************************************************/

   uint8 index = 0u;

    Data[index++] = 2u; /*Number of modules*/
    
    Data[index++] = ModuleTypeMSW; /*Module type = Main SW*/
    Data[index++] = (UInt8)MSW_PartNumber[0u];
    Data[index++] = (UInt8)MSW_PartNumber[1u];
    Data[index++] = (UInt8)MSW_PartNumber[2u];
    Data[index++] = (UInt8)MSW_PartNumber[3u];
    Data[index++] = (UInt8)MSW_PartNumber[4u];
    Data[index++] = (UInt8)MSW_PartNumber[5u];
    Data[index++] = (UInt8)MSW_PartNumber[6u];
    Data[index++] = (UInt8)MSW_PartNumber[7u];
    Data[index++] = (UInt8)MSW_RevNumber[0u];
    Data[index++] = (UInt8)MSW_RevNumber[1u];
    Data[index++] = (UInt8)MSW_RevNumber[2u];

	Data[index++] = ModuleTypeBoot; /*Module type = PBL SW*/
	Data[index++] = (UInt8)fblAp_PblPN.PartNumber[0u];
	Data[index++] = (UInt8)fblAp_PblPN.PartNumber[1u];
	Data[index++] = (UInt8)fblAp_PblPN.PartNumber[2u];
	Data[index++] = (UInt8)fblAp_PblPN.PartNumber[3u];
	Data[index++] = (UInt8)fblAp_PblPN.PartNumber[4u];
	Data[index++] = (UInt8)fblAp_PblPN.PartNumber[5u];
	Data[index++] = (UInt8)fblAp_PblPN.PartNumber[6u];
	Data[index++] = (UInt8)fblAp_PblPN.PartNumber[7u];
	Data[index++] = (UInt8)fblAp_PblPN.Revision[0u];
	Data[index++] = (UInt8)fblAp_PblPN.Revision[1u];
	Data[index++] = (UInt8)fblAp_PblPN.Revision[2u];
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadDataLength
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadDataLength> of PortPrototype <DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadDataLength(uint16 *DataLength)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1ALQ_Data_P1ALQ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadDataLength_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadDataLength(P2VAR(uint16, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) DataLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadDataLength (returns application error)
 *********************************************************************************************************************/
 *DataLength = (1u + (12u*2u));
  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1B1O_Data_P1B1O_BootSWIdentifier>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data221ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1B1O_Data_P1B1O_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData (returns application error)
 *********************************************************************************************************************/
 uint8 index=0;

	Data[index++] = 1u; /*Number of modules*/

	Data[index++] = ModuleTypeBoot; /*Module type = Main SW*/

	/* Part number */
	Data[index++] = fblAp_PblPN.PartNumber[0];
	Data[index++] = fblAp_PblPN.PartNumber[1];
	Data[index++] = fblAp_PblPN.PartNumber[2];
	Data[index++] = fblAp_PblPN.PartNumber[3];
	Data[index++] = fblAp_PblPN.PartNumber[4];
	Data[index++] = fblAp_PblPN.PartNumber[5];
	Data[index++] = fblAp_PblPN.PartNumber[6];
	Data[index++] = fblAp_PblPN.PartNumber[7];

	/* Revision level */
	Data[index++] = fblAp_PblPN.Revision[0];
	Data[index++] = fblAp_PblPN.Revision[1];
	Data[index++] = fblAp_PblPN.Revision[2];


  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId>
 *
 **********************************************************************************************************************
 *
 * Mode Interfaces:
 * ================
 *   uint8 Rte_Mode_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl(void)
 *   Modes of Rte_ModeType_DcmDiagnosticSessionControl:
 *   - RTE_MODE_DcmDiagnosticSessionControl_DEFAULT_SESSION
 *   - RTE_MODE_DcmDiagnosticSessionControl_EXTENDED_SESSION
 *   - RTE_MODE_DcmDiagnosticSessionControl_PROGRAMMING_SESSION
 *   - RTE_TRANSITION_DcmDiagnosticSessionControl
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1DIH_Data_P1DIH_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData (returns application error)
 *********************************************************************************************************************/
  Std_ReturnType retval = RTE_E_OK;
  uint8 DiagSessionMode = RTE_MODE_DcmDiagnosticSessionControl_DEFAULT_SESSION;

  DiagSessionMode = Rte_Mode_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl();

  if(DiagSessionMode==RTE_TRANSITION_DcmDiagnosticSessionControl)
  {
   retval = RTE_E_DataServices_P1DIH_Data_P1DIH_E_NOT_OK;
  }
  else
  {
   Data[0] = DiagSessionMode;
   retval = RTE_E_OK;
  }

  return retval;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1OLT_Data_P1OLT_BuildVersionInfo>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data64ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1OLT_Data_P1OLT_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData (returns application error)
 *********************************************************************************************************************/
   
   uint8 i;

   for (i = 0U; i < sizeof(BuildTagReport); i++)
   {
      Data[i] = BuildTagReport[i];
   }
   
   return RTE_E_OK;


  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data64ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1Q82_Data_P1Q82_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_VINNO_Data_VINNO_VIN_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_VINNO_Data_VINNO_VIN>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_VINNO_Data_VINNO_VIN_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data17ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_VINNO_Data_VINNO_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_VINNO_Data_VINNO_VIN_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_VINNO_Data_VINNO_VIN_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_VINNO_Data_VINNO_VIN_ReadData (returns application error)
 *********************************************************************************************************************/
   const SEWS_VIN_VINNO_T* vinNo = (const SEWS_VIN_VINNO_T*)Rte_Prm_VINNO_VIN_v(); /*lint !e545 ANSA 100629, PC-LINT 545. Suspicious use of & disabled*/

   Data[0]  = (uint8)(*vinNo)[0];
   Data[1]  = (uint8)(*vinNo)[1];
   Data[2]  = (uint8)(*vinNo)[2];
   Data[3]  = (uint8)(*vinNo)[3];
   Data[4]  = (uint8)(*vinNo)[4];
   Data[5]  = (uint8)(*vinNo)[5];
   Data[6]  = (uint8)(*vinNo)[6];
   Data[7]  = (uint8)(*vinNo)[7];
   Data[8]  = (uint8)(*vinNo)[8];
   Data[9]  = (uint8)(*vinNo)[9];
   Data[10] = (uint8)(*vinNo)[10];
   Data[11] = (uint8)(*vinNo)[11];
   Data[12] = (uint8)(*vinNo)[12];
   Data[13] = (uint8)(*vinNo)[13];
   Data[14] = (uint8)(*vinNo)[14];
   Data[15] = (uint8)(*vinNo)[15];
   Data[16] = (uint8)(*vinNo)[16];
   return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define VOL_DIDServer_STOP_SEC_CODE
#include "VOL_DIDServer_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
