/*
 ***************************************************************************************
 ***************************************************************************************
 ***
 ***     File: IoHwAb_QM_IO.h
 ***
 ***     Project: SCIM
 ***
 ***     
 ***
 ***************************************************************************************
 ***************************************************************************************
 */


#ifdef DIAGNOSTICMONITOR_IO_H
#error DIAGNOSTICMONITOR_IO_H unexpected multi-inclusion
#else
#define DIAGNOSTICMONITOR_IO_H

/*
 **=====================================================================================
 ** Included header files
 **=====================================================================================
 */
 
 
 
 /*
 **=====================================================================================
 ** Public definitions
 **=====================================================================================
 */
 
 
 
/*
 **=====================================================================================
 ** Public data declarations
 **=====================================================================================
 */


//=======================================================================================
// Public function prototypes
//=======================================================================================
static void DiagMonitor_IO_Dowhs_DTC_Handler(void);
static void DiagMonitor_IO_Dowls_DTC_Handler(void);
static void DiagMonitor_IO_12V_DTC_Handler(void);
static void DiagMonitor_IO_Dobhs_DTC_Handler(void);
static void DiagMonitor_IO_ADI_DTC_Handler(void);
static void DiagMonitor_IO_LFAnt_DTC_Handler(void);

static void DiagMonitor_IO_24V_DTC_Handler      (VGTT_EcuPinVoltage_0V2 BatteryVoltage,VGTT_EcuPinFaultStatus FaultStatusVbat);
static void DiagMonitor_IO_12VParked_DTC_Handler(VGTT_EcuPinFaultStatus FaultStatusP);
static void DiagMonitor_IO_12VLiving_DTC_Handler(VGTT_EcuPinFaultStatus FaultStatusL);
static void DiagMonitor_IO_Dowhs1_DTC_Handler   (VGTT_EcuPinFaultStatus FaultStatusDowhs1);
static void DiagMonitor_IO_Dowhs2_DTC_Handler   (VGTT_EcuPinFaultStatus FaultStatusDowhs2);
static void DiagMonitor_IO_Dobls1_DTC_Handler   (VGTT_EcuPinFaultStatus FaultStatusDOBLS1);
static void DiagMonitor_IO_Dowls2_DTC_Handler   (VGTT_EcuPinFaultStatus FaultStatusDowls2);
static void DiagMonitor_IO_Dowls3_DTC_Handler   (VGTT_EcuPinFaultStatus FaultStatusDowls3);
static void DiagMonitor_IO_Dobhs1_DTC_Handler   (VGTT_EcuPinFaultStatus FaultStatusDobhs);
static void DiagMonitor_IO_Dobhs2_DTC_Handler   (VGTT_EcuPinFaultStatus FaultStatusDobhs);
static void DiagMonitor_IO_Dobhs3_DTC_Handler   (VGTT_EcuPinFaultStatus FaultStatusDobhs);
static void DiagMonitor_IO_Dobhs4_DTC_Handler   (VGTT_EcuPinFaultStatus FaultStatusDobhs);
static void __inline__                          ADI01_03_DTC_Handler(void);
static void __inline__                          ADI04_06_DTC_Handler(void);
static void __inline__                          ADI07_09_DTC_Handler(void);
static void __inline__                          ADI10_12_DTC_Handler(void);
static void __inline__                          ADI13_16_DTC_Handler(void);
//=======================================================================================
// End of file
//=======================================================================================
#endif