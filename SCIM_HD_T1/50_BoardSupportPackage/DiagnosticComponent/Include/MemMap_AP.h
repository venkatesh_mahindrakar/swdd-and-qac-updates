/*******************************************************************************
 * COPYRIGHT (c) Volvo Corporation 2016-2017
 *
 * The copyright of the computer program(s) herein is the property of Volvo
 * Corporation, Sweden. The programs may be used and copied only with the
 * written permission from Volvo Corporation, or in accordance with the terms
 * and conditions stipulated in the agreement contract under which the
 * program(s) have been supplied.
 *
 *
 *********************************************************************************************************************/
#ifdef RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#define START_SEC_VAR_NOINIT_UNSPECIFIED
#undef RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
  	#pragma section DATA  ".Rte_Var" ".Rte_Var" 
#endif	
#ifdef RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
 #define START_SEC_VAR_ZERO_INIT_UNSPECIFIED
 #undef RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
 	#pragma section DATA  ".Rte_Var" ".Rte_Var"	
#endif

#ifdef RTE_START_SEC_VAR_ZERO_INIT_8BIT
#undef RTE_START_SEC_VAR_ZERO_INIT_8BIT
 #define START_SEC_VAR_ZERO_INIT_8BIT
 	#pragma section DATA  ".Rte_Var" ".Rte_Var"
#endif

#ifdef RTE_START_SEC_VAR_INIT_UNSPECIFIED
#undef RTE_START_SEC_VAR_INIT_UNSPECIFIED
#define START_SEC_VAR_INIT_UNSPECIFIED
 	#pragma section DATA  ".Rte_Var" ".Rte_Var"
#endif

 
#ifdef RTE_START_SEC_CONST_UNSPECIFIED
#undef RTE_START_SEC_CONST_UNSPECIFIED
#define START_SEC_CONST_UNSPECIFIED
 	#pragma section DATA  ".Rte_Var" ".Rte_Var"
#endif


#ifdef RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#undef RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#define STOP_SEC_VAR
 	#pragma section DATA  
#endif	 
#ifdef RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#undef RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#define STOP_SEC_VAR
 	#pragma section  DATA	
#endif 
#ifdef RTE_STOP_SEC_VAR_ZERO_INIT_8BIT
#undef RTE_STOP_SEC_VAR_ZERO_INIT_8BIT
#define STOP_SEC_VAR
 	#pragma section  DATA
#endif 
#ifdef RTE_STOP_SEC_VAR_INIT_UNSPECIFIED
#undef RTE_STOP_SEC_VAR_INIT_UNSPECIFIED
#define STOP_SEC_VAR
 	#pragma section  DATA
#endif	

#ifdef RTE_STOP_SEC_CONST_UNSPECIFIED
#undef RTE_STOP_SEC_CONST_UNSPECIFIED
#define STOP_SEC_CONST
 	#pragma section DATA  
#endif
 
//MSW Keys mapping

 
#ifdef MSW_SecurityAccess_START_SEC_CONST_UNSPECIFIED
# undef MSW_SecurityAccess_START_SEC_CONST_UNSPECIFIED
# define START_SEC_CONST_UNSPECIFIED
#pragma section CONST ".MswSecurityKeys" far-absolute
#endif
 
#ifdef MSW_SecurityAccess_STOP_SEC_CONST_UNSPECIFIED
# undef MSW_SecurityAccess_STOP_SEC_CONST_UNSPECIFIED
# define STOP_SEC_CONST
#pragma section CONST
#endif


 //Addresss Parameter mapping
 
#ifdef RTE_START_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#pragma section CONST ".security_level_29" far-absolute
//#pragma use_section CONST security_level_29
#endif

#ifdef RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#pragma section CONST ".security_level_2B" far-absolute
//#pragma use_section CONST security_level_2B
#endif

#ifdef RTE_START_SEC_CONST_SA_lvl_0x2D_UNSPECIFIED
#pragma section CONST ".security_level_2D" far-absolute
//#pragma use_section CONST security_level_2D
#endif

#ifdef RTE_START_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#pragma section CONST ".security_level_2F" far-absolute
//#pragma use_section CONST security_level_2F
#endif

#ifdef RTE_START_SEC_CONST_SA_lvl_0x29_and_0x37_UNSPECIFIED
#pragma section CONST ".security_level_29_37" far-absolute
//#pragma use_section CONST security_level_29_37  
#endif

#ifdef RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#pragma section CONST ".security_level_2B_37" far-absolute
//#pragma use_section CONST security_level_2B_37  
#endif

#ifdef RTE_START_SEC_CONST_SA_lvl_0x2D_and_0x37_UNSPECIFIED
#pragma section CONST ".security_level_2D_37" far-absolute
//#pragma use_section CONST security_level_2D_37  
#endif

 #ifdef RTE_START_SEC_CONST_dataset_allocated_UNSPECIFIED
#pragma section CONST ".dataset_allocated" ".dataset_allocated" 
#endif 

/* close sections */

#ifdef RTE_STOP_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#pragma section CONST 
#endif

#ifdef RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#pragma section CONST 
#endif

#ifdef RTE_STOP_SEC_CONST_SA_lvl_0x2D_UNSPECIFIED
#pragma section CONST 
#endif

#ifdef RTE_STOP_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#pragma section CONST 
#endif

#ifdef RTE_STOP_SEC_CONST_SA_lvl_0x29_and_0x37_UNSPECIFIED
#pragma section CONST 
#endif

#ifdef RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#pragma section CONST 
#endif

#ifdef RTE_STOP_SEC_CONST_SA_lvl_0x2D_and_0x37_UNSPECIFIED
#pragma section CONST 
#endif

#ifdef RTE_STOP_SEC_CONST_dataset_allocated_UNSPECIFIED
#pragma section CONST 
#endif

//GTP-290: Address parameters must be set as volatile

