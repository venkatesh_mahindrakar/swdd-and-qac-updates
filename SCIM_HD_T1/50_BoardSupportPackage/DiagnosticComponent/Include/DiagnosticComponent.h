/*
 ***************************************************************************************
 ***************************************************************************************
 ***
 ***     File: DiagnosticComponent.h
 ***
 ***     Project: SCIM
 ***
 ***     
 ***
 ***************************************************************************************
 ***************************************************************************************
 */

#ifdef DIAGNOSTICCOMPONENT_H
#error DIAGNOSTICCOMPONENT_H unexpected multi-inclusion
#else
#define DIAGNOSTICCOMPONENT_H

//=====================================================================================
// Included header files
//=====================================================================================
#include "Rte_Type.h" 

//=======================================================================================
// Public macros
//=======================================================================================

#define Const_MaxMilliSeconds              1000U
#define Const_MaxSeconds                   60U
#define Const_MaxMinutes                   60U
#define Const_MaxHours                     24U
#define Const_MaxMonths                    12U
#define MINLIMIT_MONTHS_UTC                1U
#define MAXLIMIT_DAYS_UTC                  128U  
#define MAXLIMIT_HOURS_UTC                 24U  
#define MAXLIMIT_MINUTES_UTC               60U  
#define MAXLIMIT_MONTHS_UTC                13U  
#define MAXLIMIT_SECONDS_UTC               240U  
#define MAXLIMIT_YEARS_UTC                 251U  
#define MINLIMIT_YEARS_UTC                 1985U
#define DCMCMP_SVC_SESSIONCNTRL            ((uint8)0x10U)
#define DCMCMP_SVC_ECURESET                ((uint8)0x11U)
#define DCMCMP_SVC_COMCTRL                 ((uint8)0x28U)
#define DCMCMP_SVC_LINKCTRL                ((uint8)0x87U)
#define DCMCMP_SVCSUB_PROGRAMSESSION       ((uint8)0x02U)
#define UTC_MINUTE_SIGNAL_TIMEOUT_VALUE    2000U 
#define UTC_TIME_INCREAMENT_TIME           200U 

//=======================================================================================
// Public data declarations
//=======================================================================================
typedef struct
{
   VehicleMode_T Current;
   VehicleMode_T Previous; 
}VehicleMode_Type;

typedef struct
{
   uint8 Current;
   uint8 Previous; 
}EcumMode_Type;

typedef struct
{
   uint16               Milliseconds; 
   Days8bit_Fact025_T   DayUTC;
   Hours8bit_T          HoursUTC;
   Minutes8bit_T        MinutesUTC;
   Months8bit_T         MonthUTC;
   Seconds8bitFact025_T SecondsUTC;
   Years8bit_T          YearUTC;
}LocalUTC_Type;

typedef struct
{   
   uint16               Milliseconds;
   Days8bit_Fact025_T   DayUTC;
   Hours8bit_T          HoursUTC;
   Minutes8bit_T        MinutesUTC;
   Months8bit_T         MonthUTC;
   Seconds8bitFact025_T SecondsUTC;
   Years8bit_T          YearUTC;
}LPU_Type;

typedef struct
{
   uint16               Milliseconds; 
   Days8bit_Fact025_T   DayUTC;
   Hours8bit_T          HoursUTC;
   Minutes8bit_T        MinutesUTC;
   Months8bit_T         MonthUTC;
   Seconds8bitFact025_T SecondsUTC;
   uint16               YearUTC;
}PhysicalUTC;

//=======================================================================================
// Public function prototypes
//=======================================================================================
static void ConvertRawToPhysical(const LocalUTC_Type *timeStruct,
                                       PhysicalUTC   *PhysicalStruct);
static void CovertPhysicalToRaw(const PhysicalUTC   *PhysicalStruct,
                                      LocalUTC_Type *timeStruct);
static boolean ConvertLpuTime2UtcFormat(const uint32   LpuTime,
                                              LPU_Type *LpuStruct);
static void isLeap(const uint16 Years,
                         uint8  *DaysOfMonths);
static void updateDays(const uint8              *DaysOfMonths,
                             Days8bit_Fact025_T *Days, 
                             Months8bit_T       *Months);
static void UpdateLocalUtcTime(PhysicalUTC *timeStruct);
static void AddLpuTime(const LPU_Type    *LpuStruct,
                             PhysicalUTC *timeStruct);
static void CheckUtcValues(const LocalUTC_Type *PreviousLocalUtc, 
                                 LocalUTC_Type *LocalUtcTime);
static void RteDataRead_Common(      LocalUTC_Type *pRteInData_Common,
                               const LocalUTC_Type *pPreviousData);

//=======================================================================================
// End of file
//=======================================================================================
#endif