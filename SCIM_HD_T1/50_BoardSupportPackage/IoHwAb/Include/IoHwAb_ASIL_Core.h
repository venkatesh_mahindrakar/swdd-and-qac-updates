/*
 ***************************************************************************************
 ***************************************************************************************
 ***
 ***     File: IoHwAb_ASIL_Core.h
 ***
 ***     Project: SCIM
 ***
 ***     
 ***
 ***************************************************************************************
 ***************************************************************************************
 */

/*!
 ***************************************************************************************
 * \file
 *
 * \ingroup application_swc
 *
 * \subject
 * Definition file for the IoHwAb_ASIL_Core runnables.
 *
 * \version
 * 
 *
 * \{
 ***************************************************************************************
 */
#ifndef SRE_IOHWAB_ASIL_CORE_H
#define SRE_IOHWAB_ASIL_CORE_H

/*
 **=====================================================================================
 ** Included header files
 **=====================================================================================
 */
#include "Rte_Type.h"
#include "Port.h"
#include "IoHwAb_Adc.h"

/*
 **=====================================================================================
 ** Public definitions
 **=====================================================================================
 */
 /*defined parameters*/

# ifndef CONST_Populated_CapacitiveInterface
#define CONST_NotPopulated                  (0U)
#define CONST_Populated_PullUpCircuit       (1U)
#define CONST_Populated_CapacitiveInterface (2U)
# endif

// AdcGroup_Adc0
#define ADC0_ADI_1                0x0U 
#define ADC0_ADI_2                0x1U 
#define ADC0_ADI_3                0x2U 
#define ADC0_ADI_4                0x3U 
#define ADC0_ADI_5                0x4U 
#define ADC0_ADI_6                0x5U 
#define ADC0_ADI_7                0x6U 
#define ADC0_VBAT_LOAD_MON        0x7U 
#define ADC0_DOWHS2_MON           0x8U 
#define ADC0_DOWHS_IPS_CS         0x9U 
#define ADC0_DOBHS_IPS1_CS        0xAU 
#define ADC0_DOBHS_IPS2_CS        0xBU 

#define ADCGROUP_ADC0_MAX        (ADC0_DOBHS_IPS2_CS+1U)

// AdcGroup_Adc1
#define ADC1_ADI_8                0x0U 
#define ADC1_ADI_9                0x1U 
#define ADC1_ADI_10               0x2U 
#define ADC1_ADI_11               0x3U 
#define ADC1_ADI_12               0x4U 
#define ADC1_CAN1_H2              0x5U 
#define ADC1_CAN1_H1              0x6U 
#define ADC1_CAN1_L2              0x7U 
#define ADC1_CAN1_L1              0x8U
#define ADC1_DC2DC_MON            0x9U 
#define ADC1_PARKED_MON           0xAU 
#define ADC1_LIVING_MON           0xBU 
#define ADC1_DOWHS1_MON           0xCU 
#define ADC1_DOBLS1_MON           0xDU 
#define ADC1_DOWLS3_MON           0xEU 
#define ADC1_DOWLS2_MON           0xFU 
#define ADC1_DO12_IPS_MULTI       0x10U 
#define ADC1_DOBHS1_MON           0x11U
#define ADC1_DOBHS2_MON           0x12U
#define ADC1_DOBHS3_MON           0x13U
#define ADC1_DOBHS4_MON           0x14U
#define ADC1_DAI1                 0x15U
#define ADC1_DAI2                 0x16U


#define ADCGROUP_ADC1_MAX         (ADC1_DAI2+1U)

#define TotalAdcChannels          (ADCGROUP_ADC0_MAX+ADCGROUP_ADC1_MAX)
#define MaxVoltageArray            40U


#define CONST_TimeBase            (10U)


#ifndef FSC_PowerReduceModeFilteTimelimit
#define FSC_PowerReduceModeFilteTimelimit        12U  // 120ms
#endif




// Battery preset value for ADC0 //(ADC1)
# define BATVOLTAGE_ADC_RAW_HYSTERESIS          (10U) //(40U)
# define BATVOLTAGE_ADC_RAW_8V                  (160U * ADC0_CONVFACTOR) //(642U)
# define BATVOLTAGE_ADC_RAW_16V                 (321U * ADC0_CONVFACTOR) //(1284U)
# define BATVOLTAGE_ADC_RAW_32V                 (642U * ADC0_CONVFACTOR) //(2568U)
# define BATVOLTAGE_ADC_RAW_36V                 (722U * ADC0_CONVFACTOR) //(2889U)
# define BATVOLTAGE_ADC_RAW_48V                 (963U * ADC0_CONVFACTOR) //(3852U)




/*
 **=====================================================================================
 ** Public macros
 **=====================================================================================
 */

/*
 **=====================================================================================
 ** Public type definitions
 **=====================================================================================
 */

 // declare temporary
typedef enum
{
   FSCState_ShutdownReady = 0,
   FSCState_Reduced,
   FSCState_Operating,
   FSCState_Protecting,
   FSCState_WithStand
} FSCMode_States;


/*
 **=====================================================================================
 ** Public data declarations
 **=====================================================================================
 */

typedef struct
{
	Port_PinType Pin;
	Port_PinModeType Mode;
}IoAsilCore_PortMode;

void  IohwAb_Adc1Group_EndOfNotification(void);
void  IohwAb_Adc0Group_EndOfNotification(void);
static void             FSCOperationalModeLogic(const IOHWAB_UINT8 *pBatVoltageRawADCValue, IOHWAB_UINT8 *pFscMode);
static void __inline__  ADC0_3_PcbPopulatedInfo(uint8 *IrvEcuVoltageValues,const PcbPopulatedInfo_T *IrvIsAdcPinPopulated);
static void __inline__  ADC4_6_PcbPopulatedInfo(uint8 *IrvEcuVoltageValues,const PcbPopulatedInfo_T *IrvIsAdcPinPopulated);
static void __inline__  ADC7_9_PcbPopulatedInfo(uint8 *IrvEcuVoltageValues,const PcbPopulatedInfo_T *IrvIsAdcPinPopulated);
static void __inline__  ADC10_12_PcbPopulatedInfo(uint8 *IrvEcuVoltageValues,const PcbPopulatedInfo_T *IrvIsAdcPinPopulated);
static void __inline__  ADC13_16_PcbPopulatedInfo(uint8 *IrvEcuVoltageValues,const PcbPopulatedInfo_T *IrvIsAdcPinPopulated);
static void __inline__  Dio_PcbPopulatedInfo(uint8 *IrvEcuVoltageValues,const PcbPopulatedInfo_T *IrvIsAdcPinPopulated);
static void __inline__  Dio_Dobhs_PcbPopulatedInfo(uint8 *IrvEcuVoltageValues,const PcbPopulatedInfo_T *IrvIsAdcPinPopulated);
static void __inline__  Dio_Dowhs_PcbPopulatedInfo(uint8 *IrvEcuVoltageValues,const PcbPopulatedInfo_T *IrvIsAdcPinPopulated);
static void __inline__  EnablingAnlogInputPath(void);
static void __inline__  PcbPopulated_StatusUpdation( PcbPopulatedInfo_T *IrvIsAdcPinPopulated,const SEWS_PcbConfig_DOWHS_X1CXY_T *IoQmHsPwmPcbConfig,const SEWS_PcbConfig_DOBHS_X1CXX_T *IoAsilDobhsPcbConfig);
static void __inline__  FSCModeCheck(FSCMode_States FSCModeState,IOHWAB_UINT8 *pFscMode);
static void __inline__  PcbPopulatedChecking(uint8 *IrvEcuVoltageValues,const PcbPopulatedInfo_T *IrvIsAdcPinPopulated);
/*
 **=====================================================================================
 ** Public function declarations
 **=====================================================================================
*/

#endif /* SRE_IOHWAB_ASIL_CORE_H */

/*
 **=====================================================================================
 ** End of file
 **=====================================================================================
 */
/*!
 * \}
 ************************************ End of file **************************************
 */

