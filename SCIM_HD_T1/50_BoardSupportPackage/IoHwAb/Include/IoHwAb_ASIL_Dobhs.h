/*
 ***************************************************************************************
 ***************************************************************************************
 ***
 ***     File: IoHwAb_ASIL_Dobhs.h
 ***
 ***     Project: SCIM
 ***
 ***     
 ***
 ***************************************************************************************
 ***************************************************************************************
 */

/*!
 ***************************************************************************************
 * \file
 *
 * \ingroup application_swc
 *
 * \subject
 * Definition file for the IoHwAb_ASIL_Dobhs runnables.
 *
 * \version
 * 
 *
 * \{
 ***************************************************************************************
 */

#ifndef SRE_IoHwAb_ASIL_Dobhs_H
#define SRE_IoHwAb_ASIL_Dobhs_H

/*
 **=====================================================================================
 ** Included header files
 **=====================================================================================
 */
#include "Rte_Type.h"
#include "IoHwAb_Adc.h"
/*
 **=====================================================================================
 ** Public definitions
 **=====================================================================================
 */
 /*defined parameters*/
 
 /*
 **=====================================================================================
 ** Public macros
 **=====================================================================================
 */
 


// Battery preset value for ADC0 //(ADC1)

#define BATVOLTAGE_ADC_RAW_0_01875V            (0.01875*20U * ADC0_CURCONVFACTOR_DOBHS)
#define BATVOLTAGE_ADC_RAW_1V                  (20U * ADC0_CONVFACTOR)
#define BATVOLTAGE_ADC_RAW_2V                  (40U * ADC0_CONVFACTOR) 
#define BATVOLTAGE_ADC_RAW_5V                  (100U * ADC0_CONVFACTOR) 
#define BATVOLTAGE_ADC_RAW_3_7V                (74U * ADC0_CONVFACTOR) 


#define DCDCVOLTAGE_ADC_RAW_1V					 (81U * ADC1_CONVFACTOR)
#define DCDCVOLTAGE_ADC_RAW_3_7V					 (301U * ADC1_CONVFACTOR)

#define DOBHSIPS_ADC_RAW_3_86V					(314U * ADC0_CURCONVFACTOR_DOBHS)
#define DOBHSIPS_ADC_RAW_4_5V					   (335U * ADC0_CURCONVFACTOR_DOBHS)
#define DOBHSIPS_ADC_RAW_5V						(405U * ADC0_CURCONVFACTOR_DOBHS)					


#define UltraLowVoltageConfig_UltraLowVoltage          3U // BATVOLTAGE_ADC_RAW_0_01875V

   
# ifndef True
# define True                                (1U)
# endif
   
# ifndef False
# define False                               (0U)
# endif
   

/*
 **=====================================================================================
 ** Public type definitions
 **=====================================================================================
 */

/*!
 * Contains Dio control data
 */
/* structure for Dio control */


/*!
 * Contains Pwm control data
 */

 
 /*
 **=====================================================================================
 ** Public data declarations
 **=====================================================================================
 */

/*
 **=====================================================================================
 ** Public function declarations
 **=====================================================================================
*/
/* function declarations */
static void      FaultConditionsS01S03_Check(boolean                      DobhsRead, 
                                  uint8                        DobhsPin, 
                                  uint8                        Dobhs_IPSvalue,
                                  boolean                      DobhsFaultChannel,
                                  Rte_DT_EcuHwFaultValues_T_0  *EcuDobhsASILFaultStatus);                                        
static void      FaultConditionsS02S04_Check(boolean                      DobhsRead, 
                                   uint8                        DobhsPin, 
                                  uint8                        Dobhs_IPSvalue,
                                  boolean                      DobhsFaultChannel,
                                  Rte_DT_EcuHwFaultValues_T_0  *EcuDobhsASILFaultStatus);                                        
static void      DobhsControl(uint8                         DobhsPin,
                              Rte_DT_EcuHwDioCtrlArray_T_0 *DobhsCtrlArray);                                        
static void      IoHwAb_ASILDobhs_DioHandling(void);
static void      IoHwAb_ASILDobhs_FaultDetection(void);
static void      IoHwAb_ASILDobhs_CurrentMonitoring(void);




#endif /* SRE_IoHwAb_ASIL_Dobhs_H */


/*
 **=====================================================================================
 ** End of file
 **=====================================================================================
 */
/*!
 * \}
 ************************************ End of file **************************************
 */

