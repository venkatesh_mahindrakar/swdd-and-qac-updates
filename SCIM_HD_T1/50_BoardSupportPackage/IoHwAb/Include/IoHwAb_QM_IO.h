/*
 ***************************************************************************************
 ***************************************************************************************
 ***
 ***     File: IoHwAb_QM_IO.h
 ***
 ***     Project: SCIM
 ***
 ***     
 ***
 ***************************************************************************************
 ***************************************************************************************
 */

/*!
 ***************************************************************************************
 * \file
 *
 * \ingroup application_swc
 *
 * \subject
 * Definition file for the IoHwAb_QM_IO runnables.
 *
 * \version
 * 
 *
 * \{
 ***************************************************************************************
 */
#ifndef SRE_IOHWAB_QM_IO_H
#define SRE_IOHWAB_QM_IO_H

/*
 **=====================================================================================
 ** Included header files
 **=====================================================================================
 */
#include "Rte_Type.h"
#include "IoHwAb_Adc.h"
#include "Icu.h"

/*
 **=====================================================================================
 ** Public definitions
 **=====================================================================================
 */
 /*defined parameters*/
#define CONST_TimeBase                       (10U)


# ifndef True
# define True                                (1U)
# endif

# ifndef False
# define False                               (0U)
# endif

# ifndef PWM_INPUT_FREQUENCY
# define PWM_INPUT_FREQUENCY                 (80000U)
# endif

# ifndef CONST_Populated_CapacitiveInterface
#define CONST_NotPopulated                  (0U)
#define CONST_Populated_PullUpCircuit       (1U)
#define CONST_Populated_CapacitiveInterface (2U)
# endif

 //! #####  Battery preset value for ADC0 //(ADC1)
#define BATVOLTAGE_ADC_RAW_HYSTERESIS               (10U) //(40U)
#define BATVOLTAGE_ADC_RAW_8V                       (160U * ADC0_CONVFACTOR) 
#define BATVOLTAGE_ADC_RAW_16V                      (321U * ADC0_CONVFACTOR)
#define BATVOLTAGE_ADC_RAW_24V					    (487U * ADC0_CONVFACTOR)
#define BATVOLTAGE_ADC_RAW_32V                      (642U * ADC0_CONVFACTOR)
#define BATVOLTAGE_ADC_RAW_36V                      (722U * ADC0_CONVFACTOR)
#define BATVOLTAGE_ADC_RAW_48V                      (963U * ADC0_CONVFACTOR)
#define BATVOLTAGE_ADC_RAW_9V						(183U * ADC0_CONVFACTOR)
 //! #####  DCDC preset value for ADC
#define DCDCVOLTAGE_ADC_RAW_8V                      BATVOLTAGE_ADC_RAW_8V
#define DCDCVOLTAGE_ADC_RAW_11_6V					(942U * ADC1_CONVFACTOR)
#define DCDCVOLTAGE_ADC_RAW_13V						(1055U * ADC1_CONVFACTOR)
#define DCDCVOLTAGE_ADC_RAW_18V						(1461U * ADC1_CONVFACTOR)
#define DCDCVOLTAGE_ADC_RAW_24V						(1948U * ADC1_CONVFACTOR)
#define DCDCVOLTAGE_ADC_RAW_22V						(1785U * ADC1_CONVFACTOR)
#define DCDCVOLTAGE_ADC_RAW_1V						(81U * ADC1_CONVFACTOR)
#define DCDCVOLTAGE_ADC_RAW_2V						(162U * ADC1_CONVFACTOR)
#define DCDCVOLTAGE_ADC_RAW_3_7V					(301U * ADC1_CONVFACTOR)
#define DCDCVOLTAGE_ADC_RAW_5V						(405U * ADC1_CONVFACTOR)
#define DCDCIPS_ADC_RAW_3_86V					    (314U * ADC1_CURCONVFACTOR_DCDC)
#define DCDCIPS_ADC_RAW_4_5V						(335U * ADC1_CURCONVFACTOR_DCDC)				
 //! #####  Battery preset value for ADC_RAW
#define BATVOLTAGE_ADC_RAW_0_01875V                 (0.01875*20 * ADC0_CURCONVFACTOR_DOWHS)

//! #####  MACRO definition for DOWHS1,DOWHS2, DOWLS1,DOWLS2 and DOWLS3
#define DOWHS1 0U
#define DOWHS2 1U
#define DOWLS1 0U
#define DOWLS2 1U
#define DOWLS3 2U
//! #####   MACRO definition for parameters
#define PCODE_DcdcStabilityLimit					  BATVOLTAGE_ADC_RAW_9V	
#define PCODE_ADIPullUpStabilizationTime              (0x04)  // 40ms ADI PullUp Stabilization time
#define PCODE_DAIPullUpStabilizationTime              (0x04)  // 40ms DAI PullUp Stabilization time
#define PCODE_DCDCStabilizationTime                   (0x04)  // 40ms DCDC Stabilization time


#define BUFSIZE_TIMESTAMP              11U // for both_edge 5 samples
#define NOTIFICATION_INDEX_TIMESTAMP   BUFSIZE_TIMESTAMP
#define TIMESTAMP_MEASURE_INTERVAL     100U // 1 second
#define TIMESTAMP_MEASURE_TIMEOUT      100U // 1 second
#define DO12V_FAULT_THRESHOLD                         (20U)

/*
 **=====================================================================================
 ** Public macros
 **=====================================================================================
 */

/*
 **=====================================================================================
 ** Public type definitions
 **=====================================================================================
 */

typedef enum
{
   PWMFDMode_WaitInterval,
   PWMFDMode_WaitResult,
   PWMFDMode_CheckResult
} PWMFaultDetectionMode_Enum;

typedef struct
{
   uint32 Period;
   uint8 Duty;
} DowxsMeasuredValue;

/*!
 * Contains Dio control data
 */
 //! ##### Structure for Dio control 

typedef struct
{
	uint8 FuncReq;
	uint8 DiagReq;
	uint16 FuncPeriod;
	uint16 DiagPeriod;
	uint16 FuncDuty;
	uint16 DiagDuty;
}IO_DOWXS_TYPE;
/*
 **=====================================================================================
 ** Public data declarations
 **=====================================================================================
 */
extern uint8 IrvDowhs1_Diag0Requested;
extern uint8 IrvDowhs2_Diag0Requested;
extern uint8 IrvDOBLS1_Diag0Requested;
extern uint8 IrvDowls2_Diag0Requested;
extern uint8 IrvDowls3_Diag0Requested;
/*
 **=====================================================================================
 ** Public function declarations
 **=====================================================================================
*/
//! ##### Function declarations 


void Dowhs1_OverflowNotification(void);
void Dowhs2_OverflowNotification(void);
void Dowls2_OverflowNotification(void);
void Dowls3_OverflowNotification(void);
void Dowls2_Notification(void);

static void IoHwAb_QM_IO_VoltageStabilizer(void);
static void IoHwAb_QM_IO_FaultDetection(void);
static void IoHwAb_QM_IO_12V_Activation(void);
static void IoHwAb_QM_IO_DCDC_Activation(Fsc_OperationalMode_T FSCModeCurr,Rte_DT_EcuHwDioCtrlArray_T_0* DioCtrlArray);
static void IoHwAb_QM_IO_12VLiving_Activation(Fsc_OperationalMode_T FSCModeCurr,Rte_DT_EcuHwDioCtrlArray_T_0* DioCtrlArray);
static void IoHwAb_QM_IO_12VParked_Activation(Fsc_OperationalMode_T FSCModeCurr,Rte_DT_EcuHwDioCtrlArray_T_0* DioCtrlArray);

static void IoHwAb_QM_IO_CurrentMonitoring(void);
static void IoHwAb_QM_IO_Dowhs_Activation(void);
static void IoHwAb_QM_IO_Dowls_Activation(void);
static void IoHwAb_QM_IO_ADI_Activation(void);
static void IoHwAb_QM_IO_DAI_Activation(void);
static uint8 Dowls_PwmActivationRequest(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle);
static uint8 Dowhs_PwmActivationRequest(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle);
static uint8 Dowhs_DioActivationRequest(IOCtrlReq_T IOCtrlReqType,IOHWAB_UINT8 OutputId, IOHWAB_BOOL Activation);
static uint8 Dowls_DioActivationRequest(IOCtrlReq_T IOCtrlReqType,IOHWAB_UINT8 OutputId, IOHWAB_BOOL Activation);

static void IoHwAb_QM_IO_Dowhs1_FaultDectection(uint8 *IrvEcuIoQmFaultStatus, uint8 FaultPinStatus, uint8 Dowhs_SelStatus);
static void IoHwAb_QM_IO_Dowhs2_FaultDectection(uint8 *IrvEcuIoQmFaultStatus, uint8 FaultPinStatus, uint8 Dowhs_SelStatus);
static void IoHwAb_QM_IO_Dobls1_FaultDectection(uint8 *IrvEcuIoQmFaultStatus);
static void IoHwAb_QM_IO_Dowls2_FaultDectection(uint8 *IrvEcuIoQmFaultStatus);
static void IoHwAb_QM_IO_Dowls3_FaultDectection(uint8 *IrvEcuIoQmFaultStatus);
static void IoHwAb_QM_IO_12VLivingFaultDetection(uint8* IrvEcuIoQmFaultStatus, uint8 D012V_LivingStatus, uint8 D012V_SenStatus, uint8 D012V_SelectOut0Status);
static void IoHwAb_QM_IO_12VParkedFaultDetection(uint8* IrvEcuIoQmFaultStatus, uint8 D012V_ParkedStatus, uint8 D012V_SenStatus, uint8 D012V_SelectOut0Status);
static void IoHwAb_QM_IO_DcDcFaultDetection(uint8* IrvEcuIoQmFaultStatus, uint8 D012V_DCDCStatus, uint8 D012V_LivingStatus, uint8 D012V_ParkedStatus);


static void IoHwAb_QM_IO_Pullup_FaultDetection(uint8* IrvEcuIoQmFaultStatus);
static void IoHwAb_QM_IO_ADI_FaultDetection(uint8* IrvEcuIoQmFaultStatus);
static void IoHwAb_QM_IO_Dobls1_Activation(uint8* DioCtrlArray);
static void IoHwAb_QM_IO_Dowhs1_Activation(uint8* DioCtrlArray);
static void IoHwAb_QM_IO_Dowhs2_Activation(uint8* DioCtrlArray);

static void IoHwAb_QM_IO_Dowls3_Activation(uint8* DioCtrlArray);
static void IoHwAb_QM_IO_Dowls2_Activation(uint8* DioCtrlArray);

static void IoHwAb_QM_IO_Dowhs1_Pwm_FaultDetection(uint8* IrvEcuIoQmFaultStatus, uint8 FaultPinStatus);
static void IoHwAb_QM_IO_Dowhs1_IO_FaultDetection(uint8* IrvEcuIoQmFaultStatus, uint8 FaultPinStatus, uint8 Dowhs_SelStatus);
static void IoHwAb_QM_IO_Dowhs2_IO_FaultDetection(uint8* IrvEcuIoQmFaultStatus, uint8 FaultPinStatus, uint8 Dowhs_SelStatus);
static void IoHwAb_QM_IO_Dowhs2_Pwm_FaultDetection(uint8* IrvEcuIoQmFaultStatus, uint8 FaultPinStatus);
static void  IoHwAb_QM_IO_Dowls3_Pwm_FaultDetection(uint8 *IrvEcuIoQmFaultStatus);
static void  IoHwAb_QM_IO_Dowls2_Pwm_FaultDetection(uint8 *IrvEcuIoQmFaultStatus);
static void  IoHwAb_QM_IO_Dowls3_IO_FaultDetection(uint8 *IrvEcuIoQmFaultStatus);
static void  IoHwAb_QM_IO_Dowls2_IO_FaultDetection(uint8 *IrvEcuIoQmFaultStatus);
static Std_ReturnType AdiInterface_P_SetPullupDAI_Activate(SEWS_PcbConfig_DoorAccessIf_X1CX3_T PcbConfig_DaiInterfaces , IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL ActivateDAIPullUp);
static Std_ReturnType AdiInterface_P_SetPullupParked_Activate(const SEWS_PcbConfig_AdiPullUp_X1CX5_s_T *PcbConfig_AdiPullup , IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL ActivateWeakPullUp);
static Std_ReturnType AdiInterface_P_SetPullupLiving_Activate(const SEWS_PcbConfig_AdiPullUp_X1CX5_s_T *PcbConfig_AdiPullup , IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL ActivateStrongPullUp);
static void InitTimestampBuffer(uint32 *timestampBuf, uint8 bufSize);
static void GetPeriodAndDutyFromTimestamp(Icu_ChannelType ch, uint16 refPeriod, uint8 refDuty, uint32 *timestampBuf, uint8 bufSize, DowxsMeasuredValue *MeasuredValue);

#endif /* SRE_IOHWAB_QM_IO_H */

/*
 **=====================================================================================
 ** End of file
 **=====================================================================================
 */
/*!
 * \}
 ************************************ End of file **************************************
 */

