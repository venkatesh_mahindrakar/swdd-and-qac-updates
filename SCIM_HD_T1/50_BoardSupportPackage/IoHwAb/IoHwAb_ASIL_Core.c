/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  IoHwAb_ASIL_Core.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  IoHwAb_ASIL_Core
 *  Generated at:  Mon Jun 29 18:01:33 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <IoHwAb_ASIL_Core>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
//!======================================================================================
//! \file IoHwAb_ASIL_Core.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup Platform_SCIM_IoHwAb
//! @{
//! @addtogroup IoHwAb_ASIL
//! @{
//! @addtogroup IoHwAb_ASIL_Core
//! @{
//!
//! \brief
//! IoHwAb_ASIL_Core SWC.
//! ASIL Level : QM.
//! This module implements the logic for the IoHwAb_ASIL_Core runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * IOHWAB_UINT8
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * PcbPopulatedInfo_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Rte_DT_SEWS_PcbConfig_AdiPullUp_X1CX5_s_T_0
 *   boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * Rte_DT_SEWS_PcbConfig_AdiPullUp_X1CX5_s_T_1
 *   boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * SEWS_FSC_TimeoutThreshold_X1CZR_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1QR6_Threshold_VAT_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1QR6_Threshold_VBT_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1QR6_Threshold_VOR_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_Adi_X1CXW_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_DOBHS_X1CXX_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_DOWHS_X1CXY_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_DOWLS_X1CXZ_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_DoorAccessIf_X1CX3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CZQ_Operating_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CZQ_Protecting_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CZQ_Reduced_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CZQ_ShutdownReady_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CZQ_Withstand_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinFaultStatus
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinVoltage_0V2
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_IoHwAb_ASIL_Core.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "IoHwAb_ASIL_Core.h"
#include "dio.h"
#include "adc.h"
#include "Mpc5746c.h"


#define IoHwAb_ASIL_Core_START_SEC_CONST_UNSPECIFIED
#include "IoHwAb_ASIL_Core_MemMap.h"

static const IoAsilCore_PortMode IoAsilPortModeCfg[TotalAdcChannels]={{PortConfigSet_PortContainer_0_PortPin_PF6_BAT_VOL_MON,PORT86_ADC_0_ADC0_S_14},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PB9_ADI_1,PORT25_ADC_0_ADC0_S_1  },
                                                                      {PortConfigSet_PortContainer_0_PortPin_PF1_ADI_2,PORT81_ADC_0_ADC0_S_9  },
                                                                      {PortConfigSet_PortContainer_0_PortPin_PF3_ADI_3,PORT83_ADC_0_ADC0_S_11 },
                                                                      {PortConfigSet_PortContainer_0_PortPin_PF5_ADI_4,PORT85_ADC_0_ADC0_S_13 },
                                                                      {PortConfigSet_PortContainer_0_PortPin_PB8_ADI_5,PORT24_ADC_0_ADC0_S_0  },
                                                                      {PortConfigSet_PortContainer_0_PortPin_PF2_ADI_6,PORT82_ADC_0_ADC0_S_10 },
                                                                      {PortConfigSet_PortContainer_0_PortPin_PF4_ADI_7,PORT84_ADC_0_ADC0_S_12 },
                                                                      {PortConfigSet_PortContainer_0_PortPin_PD2_ADI_8,PORT50_ADC_1_ADC1_P_6  },
                                                                      {PortConfigSet_PortContainer_0_PortPin_PD4_ADI_9,PORT52_ADC_1_ADC1_P_8  },
                                                                      {PortConfigSet_PortContainer_0_PortPin_PD3_ADI_10,PORT51_ADC_1_ADC1_P_7 },
                                                                      {PortConfigSet_PortContainer_0_PortPin_PD6_ADI_11,PORT54_ADC_1_ADC1_P_10},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PD5_ADI_12,PORT53_ADC_1_ADC1_P_9 },
                                                                      {PortConfigSet_PortContainer_0_PortPin_PF14_CAN1_TERM_H2,PORT94_ADC_1_ADC1_X_2},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PE13_CAN1_TERM_H1,PORT77_ADC_1_ADC1_X_3},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PG0_CAN1_TERM_L2,PORT96_ADC_1_ADC1_X_0},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PF15_CAN1_TERM_L1,PORT95_ADC_1_ADC1_X_1},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PA10_DAI_INPUT1,PORT10_ADC_1_ADC1_S_11},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PA11_DAI_INPUT2,PORT11_ADC_1_ADC1_S_12},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PB4_DC2DC_MON,PORT20_ADC_1_ADC1_P_0},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PD8_PS_LIVING_MON ,PORT56_ADC_1_ADC1_P_12},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PD7_PS_PARKED_MON ,PORT55_ADC_1_ADC1_P_11},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PA3_DOWHS1_MON,PORT3_ADC_1_ADC1_S_0},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PB13_DOWHS2_MON,PORT29_ADC_0_ADC0_X_1},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PA7_DOBLS1_ADCMON,PORT7_ADC_1_ADC1_S_8},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PA9_LS_PWM2_ADCMON,PORT9_ADC_1_ADC1_S_10},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PA8_LS_PWM3_ADCMON,PORT8_ADC_1_ADC1_S_9},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PD9_DOBHS1_MON,PORT57_ADC_1_ADC1_P_13},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PB6_DOBHS2_MON,PORT22_ADC_1_ADC1_P_2 },
                                                                      {PortConfigSet_PortContainer_0_PortPin_PB5_DOBHS3_MON,PORT21_ADC_1_ADC1_P_1 },
                                                                      {PortConfigSet_PortContainer_0_PortPin_PB7_DOBHS4_MON,PORT23_ADC_1_ADC1_P_3 },
                                                                      {PortConfigSet_PortContainer_0_PortPin_PI11_DOBHS_IPS1_CS,PORT139_ADC_0_ADC0_S_19},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PI8_DOBHS_IPS2_CS ,PORT136_ADC_0_ADC0_S_16},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PI12_DOWHS_IPS_CS ,PORT140_ADC_0_ADC0_S_20},
                                                                      {PortConfigSet_PortContainer_0_PortPin_PD10_IPS_MULTI,PORT58_ADC_1_ADC1_P_14}};                                           
#define IoHwAb_ASIL_Core_STOP_SEC_CONST_UNSPECIFIED
#include "IoHwAb_ASIL_Core_MemMap.h"                                           
                                                         
#define IoHwAb_ASIL_Core_START_SEC_VAR_INIT_UNSPECIFIED
#include "IoHwAb_ASIL_Core_MemMap.h"
   
static uint16 IoAdc0Input[ADCGROUP_ADC0_MAX] = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,0U};
/*Adc_ValueGroupType*/
static uint16 IoAdc1Input[ADCGROUP_ADC1_MAX] = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,};
#define IoHwAb_ASIL_Core_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "IoHwAb_ASIL_Core_MemMap.h"
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IOHWAB_UINT8: Integer in interval [0...255]
 * SEWS_FSC_TimeoutThreshold_X1CZR_T: Integer in interval [0...255]
 * SEWS_P1QR6_Threshold_VAT_T: Integer in interval [0...255]
 * SEWS_P1QR6_Threshold_VBT_T: Integer in interval [0...255]
 * SEWS_P1QR6_Threshold_VOR_T: Integer in interval [0...255]
 * SEWS_X1CZQ_Operating_T: Integer in interval [0...255]
 * SEWS_X1CZQ_Protecting_T: Integer in interval [0...255]
 * SEWS_X1CZQ_Reduced_T: Integer in interval [0...255]
 * SEWS_X1CZQ_ShutdownReady_T: Integer in interval [0...255]
 * SEWS_X1CZQ_Withstand_T: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Fsc_OperationalMode_T: Enumeration of integer in interval [0...255] with enumerators
 *   FSC_ShutdownReady (0U)
 *   FSC_Reduced_12vDcDcLimit (1U)
 *   FSC_Reduced (2U)
 *   FSC_Operating (3U)
 *   FSC_Protecting (4U)
 *   FSC_Withstand (5U)
 *   FSC_NotAvailable (6U)
 * PcbPopulatedInfo_T: Enumeration of integer in interval [0...255] with enumerators
 *   NotPopulated (0U)
 *   Populated (1U)
 * Rte_DT_SEWS_PcbConfig_AdiPullUp_X1CX5_s_T_0: Enumeration of integer in interval [0...1] with enumerators
 *   NotPopulated (0U)
 *   Populated (1U)
 * Rte_DT_SEWS_PcbConfig_AdiPullUp_X1CX5_s_T_1: Enumeration of integer in interval [0...1] with enumerators
 *   NotPopulated (0U)
 *   Populated (1U)
 * SEWS_PcbConfig_Adi_X1CXW_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_Adi_X1CXW_T_NotPopulated (0U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration (1U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration (2U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration (3U)
 * SEWS_PcbConfig_DOBHS_X1CXX_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOBHS_X1CXX_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOBHS_X1CXX_T_Populated (1U)
 * SEWS_PcbConfig_DOWHS_X1CXY_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOWHS_X1CXY_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOWHS_X1CXY_T_Populated (1U)
 * SEWS_PcbConfig_DOWLS_X1CXZ_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T_Populated (1U)
 * SEWS_PcbConfig_DoorAccessIf_X1CX3_T: Enumeration of integer in interval [0...255] with enumerators
 *   NotPopulated (0U)
 *   Populated_PullUpCircuit (1U)
 *   Populated_CapacitiveInterface (2U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 *
 * Array Types:
 * ============
 * EcuHwVoltageValues_T: Array with 40 element(s) of type VGTT_EcuPinVoltage_0V2
 * IoAsilCorePcbConfig_T: Array with 40 element(s) of type PcbPopulatedInfo_T
 * SEWS_PcbConfig_Adi_X1CXW_a_T: Array with 19 element(s) of type SEWS_PcbConfig_Adi_X1CXW_T
 * SEWS_PcbConfig_DOBHS_X1CXX_a_T: Array with 4 element(s) of type SEWS_PcbConfig_DOBHS_X1CXX_T
 * SEWS_PcbConfig_DOWHS_X1CXY_a_T: Array with 2 element(s) of type SEWS_PcbConfig_DOWHS_X1CXY_T
 * SEWS_PcbConfig_DOWLS_X1CXZ_a_T: Array with 3 element(s) of type SEWS_PcbConfig_DOWLS_X1CXZ_T
 *
 * Record Types:
 * =============
 * SEWS_FSC_VoltageThreshold_X1CZQ_s_T: Record with elements
 *   ShutdownReady of type SEWS_X1CZQ_ShutdownReady_T
 *   Reduced of type SEWS_X1CZQ_Reduced_T
 *   Operating of type SEWS_X1CZQ_Operating_T
 *   Protecting of type SEWS_X1CZQ_Protecting_T
 *   Withstand of type SEWS_X1CZQ_Withstand_T
 * SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T: Record with elements
 *   Threshold_VBT of type SEWS_P1QR6_Threshold_VBT_T
 *   Threshold_VAT of type SEWS_P1QR6_Threshold_VAT_T
 *   Threshold_VOR of type SEWS_P1QR6_Threshold_VOR_T
 * SEWS_PcbConfig_AdiPullUp_X1CX5_s_T: Record with elements
 *   AdiPullupLiving of type Rte_DT_SEWS_PcbConfig_AdiPullUp_X1CX5_s_T_0
 *   AdiPullupParked of type Rte_DT_SEWS_PcbConfig_AdiPullUp_X1CX5_s_T_1
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_PcbConfig_DoorAccessIf_X1CX3_T Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v(void)
 *   SEWS_FSC_TimeoutThreshold_X1CZR_T Rte_Prm_X1CZR_FSC_TimeoutThreshold_v(void)
 *   SEWS_PcbConfig_Adi_X1CXW_T *Rte_Prm_X1CXW_PcbConfig_Adi_v(void)
 *     Returnvalue: SEWS_PcbConfig_Adi_X1CXW_T* is of type SEWS_PcbConfig_Adi_X1CXW_a_T
 *   SEWS_PcbConfig_DOBHS_X1CXX_T *Rte_Prm_X1CXX_PcbConfig_DOBHS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOBHS_X1CXX_T* is of type SEWS_PcbConfig_DOBHS_X1CXX_a_T
 *   SEWS_PcbConfig_DOWHS_X1CXY_T *Rte_Prm_X1CXY_PcbConfig_DOWHS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOWHS_X1CXY_T* is of type SEWS_PcbConfig_DOWHS_X1CXY_a_T
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T *Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOWLS_X1CXZ_T* is of type SEWS_PcbConfig_DOWLS_X1CXZ_a_T
 *   SEWS_PcbConfig_AdiPullUp_X1CX5_s_T *Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v(void)
 *   SEWS_FSC_VoltageThreshold_X1CZQ_s_T *Rte_Prm_X1CZQ_FSC_VoltageThreshold_v(void)
 *   SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T *Rte_Prm_P1QR6_HWIO_CfgFault_PWR24V_v(void)
 *
 *********************************************************************************************************************/
#define IoHwAb_ASIL_Core_START_SEC_CODE
#include "IoHwAb_ASIL_Core_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
/**********************************************************************************************************************
 *
 * Runnable Entity Name: CoreHW_ASIL_AdcCtrl_10ms_Runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvIsAdcPinPopulated(PcbPopulatedInfo_T *data)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvEcuVoltageValues(const VGTT_EcuPinVoltage_0V2 *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CoreHW_ASIL_AdcCtrl_10ms_Runnable_doc
 *********************************************************************************************************************/
//!==================================================================================================================== 
//! 
//! \brief
//! 
//! This function implements the logic for the 'CoreHW_ASIL_AdcCtrl_10ms_Runnable'
//! 
//!==================================================================================================================== 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
FUNC(void, IoHwAb_ASIL_Core_CODE) CoreHW_ASIL_AdcCtrl_10ms_Runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CoreHW_ASIL_AdcCtrl_10ms_Runnable
 *********************************************************************************************************************/
   //! ##### This function implements the logic for Adc Control
   uint8              IrvEcuVoltageValues[MaxVoltageArray]  = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                               0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                               0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                               0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,};
   Adc_StatusType     adcGroup0Status                       = ADC_IDLE;
   Adc_StatusType     adcGroup1Status                       = ADC_IDLE;
   PcbPopulatedInfo_T IrvIsAdcPinPopulated[MaxVoltageArray] = { NotPopulated,NotPopulated,NotPopulated,NotPopulated,
                                                                NotPopulated,NotPopulated,NotPopulated,NotPopulated,
                                                                NotPopulated,NotPopulated,NotPopulated,NotPopulated,
                                                                NotPopulated,NotPopulated,NotPopulated,NotPopulated,
                                                                NotPopulated,NotPopulated,NotPopulated,NotPopulated,
                                                                NotPopulated,NotPopulated,NotPopulated,NotPopulated,
                                                                NotPopulated,NotPopulated,NotPopulated,NotPopulated,
                                                                NotPopulated,NotPopulated,NotPopulated,NotPopulated,
                                                                NotPopulated,NotPopulated,NotPopulated,NotPopulated,
                                                                NotPopulated,NotPopulated,NotPopulated,NotPopulated };
   uint8              index                                 = 0U;
   //! ###### Process the ADC0 & ADC1 conversion data
   //! ##### Perform the conversion of Raw Adc values to ECUVoltage values for Adi interface: 'AdcToVoltageAdi()'
   //! ##### Perform the conversion of Raw Adc values to ECUVoltage values for Io Pin types: 'AdcToVoltageIoPin()'
   //! ##### Perform the conversion of Raw Adc values to ECUVoltage values for 12v driver CS interface: 'AdcToVoltage12vCs()'
   //! ##### Process fallback mode, in case ADC0 & ADC1 conversion is not completed
   //! ##### Store the coverted values/error status to IrvEcuVoltage buffer    
   (void)Adc_ReadGroup(AdcConf_AdcGroup_AdcGroup_Adc0, IoAdc0Input);
   (void)Adc_ReadGroup(AdcConf_AdcGroup_AdcGroup_Adc1, IoAdc1Input);
   adcGroup0Status = Adc_GetGroupStatus(AdcConf_AdcGroup_AdcGroup_Adc0);
   adcGroup1Status = Adc_GetGroupStatus(AdcConf_AdcGroup_AdcGroup_Adc1);
   //! ##### Read the IrvPcbPopulatedInfo from Rte Irv interface
   Rte_IrvRead_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvIsAdcPinPopulated(&IrvIsAdcPinPopulated[0]);       
   if ((ADC_BUSY != adcGroup0Status)
      && (ADC_BUSY != adcGroup1Status))
   {
         PcbPopulatedChecking(&IrvEcuVoltageValues[0],
                              &IrvIsAdcPinPopulated[0]);                  
         //! #### Start the new ADC conversion cycle on ADC0 & ADC1         
         Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_Adc0);
         Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_Adc1);
   }
   else
   {
      //! #### Else report fault
      for (index = 0U;index< (uint8)MaxVoltageArray;index++)
         {
            IrvEcuVoltageValues[index] = IO_AdcValue_AdcFailure;
         }
   }
      Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvEcuVoltageValues(&IrvEcuVoltageValues[0]);      
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
/**********************************************************************************************************************
 *
 * Runnable Entity Name: CoreHW_ASIL_AdcCtrl_Init_Runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Fsc_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvEcuVoltageValues(const VGTT_EcuPinVoltage_0V2 *data)
 *   void Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvIsAdcPinPopulated(const PcbPopulatedInfo_T *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CoreHW_ASIL_AdcCtrl_Init_Runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the CoreHW_ASIL_AdcCtrl_Init_Runnable
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
FUNC(void, IoHwAb_ASIL_Core_CODE) CoreHW_ASIL_AdcCtrl_Init_Runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CoreHW_ASIL_AdcCtrl_Init_Runnable
 *********************************************************************************************************************/
   //! ##### Initialization of ADC
   //! ##### Initialize the IrvAdcBuffer     
   //! ##### Start the first ADC0/ADC1 conversions  
   //! ##### Initialization of FSC mode  
   //Std_ReturnType ret = RTE_E_INVALID;
   //Std_ReturnType ret1 = RTE_E_INVALID;
	const SEWS_PcbConfig_Adi_X1CXW_T         *IoQmAdiPcbConfig         = Rte_Prm_X1CXW_PcbConfig_Adi_v();
	const SEWS_PcbConfig_DOBHS_X1CXX_T       *IoAsilDobhsPcbConfig     = Rte_Prm_X1CXX_PcbConfig_DOBHS_v();
	const SEWS_PcbConfig_DOWHS_X1CXY_T       *IoQmHsPwmPcbConfig       = Rte_Prm_X1CXY_PcbConfig_DOWHS_v();
	const SEWS_PcbConfig_DOWLS_X1CXZ_T       *IoQmLsPwmPcbConfig       = Rte_Prm_X1CXZ_PcbConfig_DOWLS_v();
	const SEWS_PcbConfig_AdiPullUp_X1CX5_s_T *IoQmAdiPullUpPcbConfig   = Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v(); //Misra -unused variable
			Adc_StatusType                     adcGroup0Status           = ADC_IDLE;
			Adc_StatusType                     adcGroup1Status           = ADC_IDLE;
			PcbPopulatedInfo_T                 IrvIsAdcPinPopulated[40]  = {NotPopulated,NotPopulated,NotPopulated,NotPopulated,
																							  NotPopulated,NotPopulated,NotPopulated,NotPopulated,
																							  NotPopulated,NotPopulated,NotPopulated,NotPopulated,
																							  NotPopulated,NotPopulated,NotPopulated,NotPopulated,
																							  NotPopulated,NotPopulated,NotPopulated,NotPopulated,
																							  NotPopulated,NotPopulated,NotPopulated,NotPopulated,
																							  NotPopulated,NotPopulated,NotPopulated,NotPopulated,
																							  NotPopulated,NotPopulated,NotPopulated,NotPopulated,
																							  NotPopulated,NotPopulated,NotPopulated,NotPopulated,
																							  NotPopulated,NotPopulated,NotPopulated,NotPopulated};     
   
         uint8                              i                          = 0U;        
   //! ##### Copy Pcb Populated info to local IrvPcbPopulatedInfo IRV      
   //! ##### Default Populated Hardware Interfaces for 'IrvIsAdcPinPopulated'       
   IrvIsAdcPinPopulated[PwrSupply]       = Populated;  
   IrvIsAdcPinPopulated[Do12VDCDC]       = Populated;
   IrvIsAdcPinPopulated[Do12VLiving]     = Populated;
   IrvIsAdcPinPopulated[Do12VParked]     = Populated;
   IrvIsAdcPinPopulated[Do12V_IPS]       = Populated;    
   IrvIsAdcPinPopulated[ADI_01]          = IoQmAdiPcbConfig[0U];
   IrvIsAdcPinPopulated[ADI_02]          = IoQmAdiPcbConfig[1U];
   IrvIsAdcPinPopulated[ADI_03]          = IoQmAdiPcbConfig[2U];
   IrvIsAdcPinPopulated[ADI_04]          = IoQmAdiPcbConfig[3U];
   IrvIsAdcPinPopulated[ADI_05]          = IoQmAdiPcbConfig[4U];
   IrvIsAdcPinPopulated[ADI_06]          = IoQmAdiPcbConfig[5U];
   IrvIsAdcPinPopulated[ADI_07]          = IoQmAdiPcbConfig[6U];
   IrvIsAdcPinPopulated[ADI_08]          = IoQmAdiPcbConfig[7U];
   IrvIsAdcPinPopulated[ADI_09]          = IoQmAdiPcbConfig[8U];
   IrvIsAdcPinPopulated[ADI_10]          = IoQmAdiPcbConfig[9U];
   IrvIsAdcPinPopulated[ADI_11]          = IoQmAdiPcbConfig[10U];
   IrvIsAdcPinPopulated[ADI_12]          = IoQmAdiPcbConfig[11U];
   IrvIsAdcPinPopulated[ADI_13]          = IoQmAdiPcbConfig[12U];
   IrvIsAdcPinPopulated[ADI_14]          = IoQmAdiPcbConfig[13U];
   IrvIsAdcPinPopulated[ADI_15]          = IoQmAdiPcbConfig[14U];
   IrvIsAdcPinPopulated[ADI_16]          = IoQmAdiPcbConfig[15U];    
   IrvIsAdcPinPopulated[Dowhs_01]        = IoQmHsPwmPcbConfig[0U];
   IrvIsAdcPinPopulated[Dowhs_02]        = IoQmHsPwmPcbConfig[1U];   
   IrvIsAdcPinPopulated[Dobls_01]        = IoQmLsPwmPcbConfig[0U];
   IrvIsAdcPinPopulated[Dowls_02]        = IoQmLsPwmPcbConfig[1U];
   IrvIsAdcPinPopulated[Dowls_03]        = IoQmLsPwmPcbConfig[2U];   
   IrvIsAdcPinPopulated[Dobhs_01]        = IoAsilDobhsPcbConfig[0U];
   IrvIsAdcPinPopulated[Dobhs_02]        = IoAsilDobhsPcbConfig[1U];
   IrvIsAdcPinPopulated[Dobhs_03]        = IoAsilDobhsPcbConfig[2U];
   IrvIsAdcPinPopulated[Dobhs_04]        = IoAsilDobhsPcbConfig[3U];
   PcbPopulated_StatusUpdation(&IrvIsAdcPinPopulated[0],
                               IoQmHsPwmPcbConfig,
                               IoAsilDobhsPcbConfig);        
   //! ##### Process to provide some delay between PortMode change and Adc Conversion Start    
   for (i = 0U;i<=(uint8)Do12V_IPS;i++)
   {
		//! #### Checking conditions to enable analog inputs
      if(SEWS_PcbConfig_Adi_X1CXW_T_NotPopulated==IrvIsAdcPinPopulated[i])
      {
         Port_SetPinMode(IoAsilPortModeCfg[i].Pin,IoAsilPortModeCfg[i].Mode);
      }
      else
      {
         // Do Nothing
      }
   }  
	//! #### Process the enabling of anlog input:'EnablingAnlogInputPath()'
	EnablingAnlogInputPath();
	
   //! ##### check group status for starting new conversion :'Adc_GetGroupStatus()'    
   adcGroup0Status = Adc_GetGroupStatus(AdcConf_AdcGroup_AdcGroup_Adc0);
   adcGroup1Status = Adc_GetGroupStatus(AdcConf_AdcGroup_AdcGroup_Adc1);
   //! ##### if adc group status is not busy then start new adc conversion:'Adc_StartGroupConversion()'
   if ((ADC_BUSY != adcGroup0Status)
      && (ADC_BUSY != adcGroup1Status))
   {
      Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_Adc0);
      Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_Adc1);
   }
   else
   {
      // ADC Failure Fault notify
      for(i=0U;i<ADCGROUP_ADC0_MAX;i++)
      {
         IoAdc0Input[i] = IO_AdcValue_AdcFailure;
      }   
      for(i=0U;i<ADCGROUP_ADC0_MAX;i++)
      {
         IoAdc0Input[i] = IO_AdcValue_AdcFailure;
      }
   }
   Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvIsAdcPinPopulated(&IrvIsAdcPinPopulated[0U]);   
   Rte_Write_Fsc_OperationalMode_P_Fsc_OperationalMode((Fsc_OperationalMode_T)FSC_ShutdownReady);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
/**********************************************************************************************************************
 *
 * Runnable Entity Name: CoreHW_ASIL_VbatProcess_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Fsc_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_CoreHW_ASIL_VbatProcess_10ms_runnable_IrvEcuVoltageValues(VGTT_EcuPinVoltage_0V2 *data)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_CoreHW_ASIL_VbatProcess_10ms_runnable_FSCMode(IOHWAB_UINT8 data)
 *   void Rte_IrvWrite_CoreHW_ASIL_VbatProcess_10ms_runnable_IrvBatteryFaultStatus(VGTT_EcuPinFaultStatus data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CoreHW_ASIL_VbatProcess_10ms_runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'CoreHW_ASIL_VbatProcess_10ms_runnable'
//! 
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
FUNC(void, IoHwAb_ASIL_Core_CODE) CoreHW_ASIL_VbatProcess_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CoreHW_ASIL_VbatProcess_10ms_runnable
 *********************************************************************************************************************/
   //! ###### Power supply voltage processing
   //! #### Peform the power supply fault detection logic by checking against the threshold ranges    
   //! ###### FSC processing
   //! ##### Set FSC operational mode based on the power supply voltage value compared against the threshold values  
   //! ##### Write Fsc_OperationalMode_P
   uint8                              IrvEcuFaultStatus_PwrSupply = 0U;
   SEWS_HWIO_CfgDiag_PWR24V_P1QR6_s_T *IoHwAb24VThr;    
   VGTT_EcuPinVoltage_0V2             voltage[40]                 = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                     0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                     0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                     0U,0U,0U,0U,0U,0U,0U,0U,0U,0U};
   IOHWAB_UINT8                       FSCmode                     = FSC_NotAvailable;
   uint8                              ADValueHwPrecision          = 3U; // 0.6V (cannot set 0.5V for Hysteresis)
   IoHwAb24VThr = (SEWS_HWIO_CfgDiag_PWR24V_P1QR6_s_T *)Rte_Prm_P1QR6_HWIO_CfgFault_PWR24V_v();
   Rte_IrvRead_CoreHW_ASIL_VbatProcess_10ms_runnable_IrvEcuVoltageValues(&voltage[0]);
   //! ##### Peform the power supply fault detection logic by checking against the threshold ranges
   if ((IoHwAb24VThr->Threshold_VOR !=0)
      && (IoHwAb24VThr->Threshold_VBT!=0)
      && (IoHwAb24VThr->Threshold_VAT!=0))
   {
      if(voltage[PwrSupply] > (IoHwAb24VThr->Threshold_VAT + ADValueHwPrecision))
      {
         IrvEcuFaultStatus_PwrSupply = OnState_FaultDetected_VAT;
      }
      else if(voltage[PwrSupply] < (IoHwAb24VThr->Threshold_VOR - ADValueHwPrecision))
      {
         IrvEcuFaultStatus_PwrSupply = OnState_FaultDetected_VOR;
      }
      else if(voltage[PwrSupply] < (IoHwAb24VThr->Threshold_VBT - ADValueHwPrecision))
      {
         IrvEcuFaultStatus_PwrSupply = OnState_FaultDetected_VBT;
      }
      else
      {
         IrvEcuFaultStatus_PwrSupply = OnState_NoFaultDetected;
      }
   }
   else
   {
      // Do nothing
   }           
   Rte_IrvWrite_CoreHW_ASIL_VbatProcess_10ms_runnable_IrvBatteryFaultStatus(IrvEcuFaultStatus_PwrSupply);    
   //! ##### FSC processing: 'FSPOperationalModeLogic()'
   //! ##### Set FSC operational mode based on the power supply voltage value compared against the threshold values
   FSCOperationalModeLogic(&voltage[PwrSupply],
                           &FSCmode);   
   Rte_IrvWrite_CoreHW_ASIL_VbatProcess_10ms_runnable_FSCMode(FSCmode);
   //! ##### Write Fsc_OperationalMode_P:'Rte_Write_Fsc_OperationalMode_P_Fsc_OperationalMode()'
   Rte_Write_Fsc_OperationalMode_P_Fsc_OperationalMode(FSCmode);   
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
/**********************************************************************************************************************
 *
 * Runnable Entity Name: EcuHwState_P_GetEcuVoltages_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetEcuVoltages_CS> of PortPrototype <EcuHwState_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_EcuHwState_P_GetEcuVoltages_CS_IrvEcuVoltageValues(VGTT_EcuPinVoltage_0V2 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType EcuHwState_P_GetEcuVoltages_CS(VGTT_EcuPinVoltage_0V2 *EcuVoltageValues)
 *     Argument EcuVoltageValues: VGTT_EcuPinVoltage_0V2* is of type EcuHwVoltageValues_T
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_EcuHwState_I_AdcInFailure
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: EcuHwState_P_GetEcuVoltages_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'EcuHwState_P_GetEcuVoltages_CS'
//! \param EcuVoltageValues   For reading ecu voltages
//! 
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
FUNC(Std_ReturnType, IoHwAb_ASIL_Core_CODE) EcuHwState_P_GetEcuVoltages_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) EcuVoltageValues) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: EcuHwState_P_GetEcuVoltages_CS (returns application error)
 *********************************************************************************************************************/
   //! ###### Get the EcuVoltageValues IrvEcuVoltages buffer and report the values
   //! ##### If ADC failure is detected then provide the return value as RTE_E_EcuHwState_I_AdcInFailure
   Std_ReturnType retval;
   Rte_IrvRead_EcuHwState_P_GetEcuVoltages_CS_IrvEcuVoltageValues(EcuVoltageValues);      
   return RTE_E_OK;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VbatInterface_P_GetVbatVoltage_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetVbatVoltage_CS> of PortPrototype <VbatInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   VGTT_EcuPinFaultStatus Rte_IrvRead_VbatInterface_P_GetVbatVoltage_CS_IrvBatteryFaultStatus(void)
 *   void Rte_IrvRead_VbatInterface_P_GetVbatVoltage_CS_IrvEcuVoltageValues(VGTT_EcuPinVoltage_0V2 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VbatInterface_P_GetVbatVoltage_CS(VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_VbatInterface_I_AdcInFailure
 *   RTE_E_VbatInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VbatInterface_P_GetVbatVoltage_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'VbatInterface_P_GetVbatVoltage_CS'
//! \param BatteryVoltage   For reading BatteryVoltage
//! \param FaultStatus      For reading ecu faults 
//! 
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
FUNC(Std_ReturnType, IoHwAb_ASIL_Core_CODE) VbatInterface_P_GetVbatVoltage_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VbatInterface_P_GetVbatVoltage_CS (returns application error)
 *********************************************************************************************************************/
   //! ##### Get the value of Vbat fault status using IRvFaultStatus buffer and report it in FaultStatus
   VGTT_EcuPinVoltage_0V2 Voltage[40] = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                         0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                         0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                         0U,0U,0U,0U,0U,0U,0U,0U,0U,0U};
   *FaultStatus = Rte_IrvRead_VbatInterface_P_GetVbatVoltage_CS_IrvBatteryFaultStatus();
   //! ##### Get the value of Vbat voltage value using IrvEcuVoltage buffer report it to BatteryVoltage
   Rte_IrvRead_VbatInterface_P_GetVbatVoltage_CS_IrvEcuVoltageValues(&Voltage[0]);
   //! ##### If ADC failure is detected then provide the return value as RTE_E_EcuHwState_I_AdcInFailure
   *BatteryVoltage = Voltage[PwrSupply];
   return RTE_E_OK;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
/**********************************************************************************************************************
 *
 * Runnable Entity Name: Watchdog_ASIL_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Watchdog_ASIL_10ms_runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'Watchdog_ASIL_10ms_runnable'
//! 
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
FUNC(void, IoHwAb_ASIL_Core_CODE) Watchdog_ASIL_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Watchdog_ASIL_10ms_runnable
 *********************************************************************************************************************/
   //! ###### Processing of Watchdog 10ms Runnable
   //! ##### trigger the watchdog using Dio_WriteChannel for ExternalWDI with value of '1' for 10ms and then swithc to '0' for 90ms
   static uint8 ExternalWatchdogTimer = 0U;
   if(ExternalWatchdogTimer == 0U)
   {
      Dio_WriteChannel(DioConf_DioChannel_ExWdg_WDI,
                       STD_HIGH);
      ExternalWatchdogTimer = 9U;
   }
   else
   {
      ExternalWatchdogTimer--;
      Dio_WriteChannel(DioConf_DioChannel_ExWdg_WDI,
                       STD_LOW);
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
#define IoHwAb_ASIL_Core_STOP_SEC_CODE
#include "IoHwAb_ASIL_Core_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'PcbPopulated_StatusUpdation'
//!
//! \param  *IrvIsAdcPinPopulated    Update AdcPin status value for Irv
//! \param  *IoQmHsPwmPcbConfig      Checks IoQmHsPwmPcbConfig status 
//! \param  *IoAsilDobhsPcbConfig    Checks IoAsilDobhsPcbConfig status 
//!
//!====================================================================================================================
static void __inline__  PcbPopulated_StatusUpdation(      PcbPopulatedInfo_T           *IrvIsAdcPinPopulated,
                                                    const SEWS_PcbConfig_DOWHS_X1CXY_T *IoQmHsPwmPcbConfig,
                                                    const SEWS_PcbConfig_DOBHS_X1CXX_T *IoAsilDobhsPcbConfig)
{
   const SEWS_PcbConfig_DoorAccessIf_X1CX3_T IoQmDaiPcbConfig = Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
   //! ##### Set the IrvIsAdcPinPopulated pin to Populated if PCB config is of Populated_CapacitiveInterface or Populated_PullUpCircuit
   if ((CONST_Populated_CapacitiveInterface  ==  IoQmDaiPcbConfig)
      || (CONST_Populated_PullUpCircuit  ==  IoQmDaiPcbConfig))
   {
      IrvIsAdcPinPopulated[DAI_01] = Populated;
      IrvIsAdcPinPopulated[DAI_02] = Populated;
   }
   else
   {
      IrvIsAdcPinPopulated[DAI_01] = NotPopulated;
      IrvIsAdcPinPopulated[DAI_02] = NotPopulated;
   }
   //! ##### Set the IrvIsAdcPinPopulated Dowhs_IPS pin to Populated if PCB config is of SEWS_PcbConfig_DOWHS_X1CXY_T_Populated 
   if ((SEWS_PcbConfig_DOWHS_X1CXY_T_Populated == IoQmHsPwmPcbConfig[0U])
      || (SEWS_PcbConfig_DOWHS_X1CXY_T_Populated == IoQmHsPwmPcbConfig[1U]))
   {
      IrvIsAdcPinPopulated[Dowhs_IPS]  = Populated;  
   }
   else
   {
      IrvIsAdcPinPopulated[Dowhs_IPS]  = NotPopulated;
   }
   //! ##### Set the IrvIsAdcPinPopulated Dobhs_IPS1 pin to Populated if PCB config is of SEWS_PcbConfig_DOBHS_X1CXX_T_Populated 
   if ((SEWS_PcbConfig_DOBHS_X1CXX_T_Populated == IoAsilDobhsPcbConfig[0U])
      || (SEWS_PcbConfig_DOBHS_X1CXX_T_Populated == IoAsilDobhsPcbConfig[3U]))
   {
      IrvIsAdcPinPopulated[Dobhs_IPS1]  = Populated;  
   }
   else
   {
      IrvIsAdcPinPopulated[Dobhs_IPS1]  = NotPopulated;
   }   
   //! ##### Set the IrvIsAdcPinPopulated Dobhs_IPS2 pin to Populated if PCB config is of SEWS_PcbConfig_DOBHS_X1CXX_T_Populated 
   if ((SEWS_PcbConfig_DOBHS_X1CXX_T_Populated == IoAsilDobhsPcbConfig[1U])
      || (SEWS_PcbConfig_DOBHS_X1CXX_T_Populated == IoAsilDobhsPcbConfig[2U]))
   {
      IrvIsAdcPinPopulated[Dobhs_IPS2]  = Populated;  
   }
   else
   {
      IrvIsAdcPinPopulated[Dobhs_IPS2]  = NotPopulated;
   }     
}   
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'FSPOperationalModeLogic' 
//!
//! \param  pBatVoltageRawADCValue   For reading raw adc value
//! \param  pFscMode                 For reading fscmode
//!
//!====================================================================================================================
static void FSCOperationalModeLogic (const IOHWAB_UINT8  *pBatVoltageRawADCValue,
                                           IOHWAB_UINT8  *pFscMode)
{
   //! ###### This function implements logic of FSCOperationalMode handling
   static FSCMode_States                       FSCModeState          = FSCState_Operating; //FSCState_ShutdownReady;
   static IOHWAB_UINT8                         FSC_PowerReduceTimer  = 0U;
          SEWS_FSC_VoltageThreshold_X1CZQ_s_T  *FSC_VoltageThreshold;
          SEWS_FSC_TimeoutThreshold_X1CZR_T    FSC_TimeoutThreshold;   
          uint8                                ADValueHwPrecision    = 5U; // 1V (cannot set 0.5V for Hysteresis)
   
   FSC_TimeoutThreshold = Rte_Prm_X1CZR_FSC_TimeoutThreshold_v();
   FSC_VoltageThreshold = (SEWS_FSC_VoltageThreshold_X1CZQ_s_T *)Rte_Prm_X1CZQ_FSC_VoltageThreshold_v();
   //! ##### If battery voltage is less than FSC_VoltageThreshold->ShutdownReady change fscmode to FSCState_ShutdownReady   
   if ((IO_AdcValue_NotAvialble == *pBatVoltageRawADCValue) 
      || (FSC_VoltageThreshold->ShutdownReady > *pBatVoltageRawADCValue))
   {
      if (FSCState_ShutdownReady != FSCModeState) 
      {
         FSC_PowerReduceTimer++;
         if (FSC_TimeoutThreshold < FSC_PowerReduceTimer)
         {
            FSCModeState        = FSCState_ShutdownReady;
            FSC_PowerReduceTimer=0;
         }
         else
         {
             // Do nothing
         }
      }
      else
      {
          // Do nothing
      }
   }
   //! ##### If battery voltage is less than FSC_VoltageThreshold->Reduced and grater than equal to FSC_VoltageThreshold->ShutdownReady then change fscmode to FSCState_Reduced
   else if (((FSC_VoltageThreshold->ShutdownReady + ADValueHwPrecision) <= *pBatVoltageRawADCValue) 
           && ((FSC_VoltageThreshold->Reduced - ADValueHwPrecision) > *pBatVoltageRawADCValue))
   {
      if (FSCState_Reduced != FSCModeState) 
      {
         FSC_PowerReduceTimer++;
         if (FSC_TimeoutThreshold < FSC_PowerReduceTimer)
         {
            FSCModeState         = FSCState_Reduced;
            FSC_PowerReduceTimer = 0;
         }
         else
         {
             // Do nothing
         }
      }
      else
      {
          // Do nothing
      }
   }
   //! ##### If battery voltage is less than FSC_VoltageThreshold->Operating and grater than equal to FSC_VoltageThreshold->Reduced then change fscmode to FSCState_Operating
   else if ((FSC_VoltageThreshold->Reduced <= *pBatVoltageRawADCValue) 
           && (FSC_VoltageThreshold->Operating >= *pBatVoltageRawADCValue))
   {
      if (FSCState_Operating != FSCModeState) 
      {
         FSCModeState = FSCState_Operating;
      }
      else
      {
          // Do nothing
      }
   }
   else if (((FSC_VoltageThreshold->Operating + ADValueHwPrecision) < *pBatVoltageRawADCValue) 
           && (FSC_VoltageThreshold->Protecting > *pBatVoltageRawADCValue))
   {
      if (FSCState_Protecting != FSCModeState) 
      {
         FSCModeState = FSCState_Protecting;
      }
      else
      {
         // Do nothing
      }
   }
   else if ((FSC_VoltageThreshold->Protecting + ADValueHwPrecision) < *pBatVoltageRawADCValue)
   {
      if (FSCState_WithStand != FSCModeState) 
      {
         FSCModeState = FSCState_WithStand;
      }
      else 
      {
         // Do nothing
      }
   }
   else
   {
      //!#### Stay in previous mode
      FSC_PowerReduceTimer=0;
   }
   FSCModeCheck(FSCModeState,
                pFscMode);
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'PcbPopulatedChecking'
//!
//! \param   *IrvEcuVoltageValues      Provides ECU voltage value for Irv
//! \param   *IrvIsAdcPinPopulated     Checks AdcPin status value for Irv
//!
//!====================================================================================================================
static void __inline__ PcbPopulatedChecking(      uint8              *IrvEcuVoltageValues,
                                            const PcbPopulatedInfo_T *IrvIsAdcPinPopulated)
{
   //! ##### Process to update ADC0_3_PcbPopulatedInfo.
   ADC0_3_PcbPopulatedInfo(&IrvEcuVoltageValues[0],
                           &IrvIsAdcPinPopulated[0]);
   //! ##### Process to update ADC4_6_PcbPopulatedInfo.
   ADC4_6_PcbPopulatedInfo(&IrvEcuVoltageValues[0],
                           &IrvIsAdcPinPopulated[0]);
   //! ##### Process to update ADC7_9_PcbPopulatedInfo.
   ADC7_9_PcbPopulatedInfo(&IrvEcuVoltageValues[0],
                           &IrvIsAdcPinPopulated[0]);
   //! ##### Process to update ADC10_12_PcbPopulatedInfo.
   ADC10_12_PcbPopulatedInfo(&IrvEcuVoltageValues[0],
                             &IrvIsAdcPinPopulated[0]);
   //! ##### Process to update ADC13_16_PcbPopulatedInfo.
   ADC13_16_PcbPopulatedInfo(&IrvEcuVoltageValues[0],
                             &IrvIsAdcPinPopulated[0]);
   //! ##### Process to update Dio_PcbPopulatedInfo.
   Dio_PcbPopulatedInfo(&IrvEcuVoltageValues[0],
                        &IrvIsAdcPinPopulated[0]);
   //! ##### Process to update Dio_Dobhs_PcbPopulatedInfo.
   Dio_Dobhs_PcbPopulatedInfo(&IrvEcuVoltageValues[0],
                              &IrvIsAdcPinPopulated[0]);
   //! ##### Process to update Dio_Dowhs_PcbPopulatedInfo.
   Dio_Dowhs_PcbPopulatedInfo(&IrvEcuVoltageValues[0],
                              &IrvIsAdcPinPopulated[0]);
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'FSCModeCheck' 
//!
//! \param   FSCModeState   Provides FSC Mode state
//! \param   *pFscMode      For reading fscmode
//!
//!====================================================================================================================
static void __inline__ FSCModeCheck(FSCMode_States FSCModeState,
                                    IOHWAB_UINT8   *pFscMode)
{  
   //! ##### Check the FSC state and update the Mode
   switch(FSCModeState)
   {
      case FSCState_ShutdownReady:
         *pFscMode = FSC_ShutdownReady;
      break;
      case FSCState_Reduced:
         *pFscMode = FSC_Reduced;
      break;
      case FSCState_Operating:
         *pFscMode = FSC_Operating;
      break;
      case FSCState_Protecting:
         *pFscMode = FSC_Protecting;
      break;
      case FSCState_WithStand:
         *pFscMode = FSC_Withstand;
      break;
      default:
         *pFscMode = FSC_NotAvailable;
      break;
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'ADC0_3_PcbPopulatedInfo'
//!
//! \param   *IrvEcuVoltageValues      Provides ECU voltage value for Irv
//! \param   *IrvIsAdcPinPopulated     Checks AdcPin status value for Irv
//!
//!====================================================================================================================
static void __inline__  ADC0_3_PcbPopulatedInfo(      uint8              *IrvEcuVoltageValues,
                                                const PcbPopulatedInfo_T *IrvIsAdcPinPopulated)
{
   //! ##### Condition for checking 'ADI_01' pin is Populated and updating the Ecu voltage values accordingly
   if (SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration == IrvIsAdcPinPopulated[ADI_01])
   {
      IrvEcuVoltageValues[ADI_01] = (uint8)((float64)IoAdc0Input[ADC0_ADI_1] * ADC0_CONVFACTOR_ADI_PULLDOWN);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration == IrvIsAdcPinPopulated[ADI_01])
   {
    IrvEcuVoltageValues[ADI_01] = (uint8)((float64)IoAdc0Input[ADC0_ADI_1] * ADC0_CONVFACTOR_ADI_PULLUP);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration == IrvIsAdcPinPopulated[ADI_01])
   {
      IrvEcuVoltageValues[ADI_01] = (uint8)((float64)IoAdc0Input[ADC0_ADI_1] * ADC0_CONVFACTOR_ADI_PULLUP);
   }
   else
   {
      IrvEcuVoltageValues[ADI_01] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'ADI_02' pin is Populated and updating the Ecu voltage values accordingly
   if(SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration == IrvIsAdcPinPopulated[ADI_02])
   {
      IrvEcuVoltageValues[ADI_02] = (uint8)((float64)IoAdc0Input[ADC0_ADI_2] * ADC0_CONVFACTOR_ADI_PULLDOWN);
   }
   else if(SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration == IrvIsAdcPinPopulated[ADI_02])
   {
      IrvEcuVoltageValues[ADI_02] = (uint8)((float64)IoAdc0Input[ADC0_ADI_2] * ADC0_CONVFACTOR_ADI_PULLUP);
   }
   else if(SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration == IrvIsAdcPinPopulated[ADI_02])
   {
      IrvEcuVoltageValues[ADI_02] = (uint8)((float64)IoAdc0Input[ADC0_ADI_2] * ADC0_CONVFACTOR_ADI_PULLUP);
   }
   else
   {
      IrvEcuVoltageValues[ADI_02] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'ADI_03' pin is Populated and updating the Ecu voltage values accordingly
   if (SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration==IrvIsAdcPinPopulated[ADI_03])
   {
      IrvEcuVoltageValues[ADI_03] = (uint8)((float64)IoAdc0Input[ADC0_ADI_3] * ADC0_CONVFACTOR_ADI_PULLDOWN);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration==IrvIsAdcPinPopulated[ADI_03])
   {
      IrvEcuVoltageValues[ADI_03] = (uint8)((float64)IoAdc0Input[ADC0_ADI_3] * ADC0_CONVFACTOR_ADI_PULLUP);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration==IrvIsAdcPinPopulated[ADI_03])
   {
      IrvEcuVoltageValues[ADI_03] = (uint8)((float64)IoAdc0Input[ADC0_ADI_3] * ADC0_CONVFACTOR_ADI_PULLUP);
   }
   else
   {
      IrvEcuVoltageValues[ADI_03] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'ADC4_6_PcbPopulatedInfo' 
//!
//! \param   *IrvEcuVoltageValues      Provides ECU voltage value for Irv
//! \param   *IrvIsAdcPinPopulated     Checks AdcPin status value for Irv
//!
//!====================================================================================================================
static void __inline__  ADC4_6_PcbPopulatedInfo(      uint8              *IrvEcuVoltageValues,
                                                const PcbPopulatedInfo_T *IrvIsAdcPinPopulated)
{
   //! ##### Condition for checking 'ADI_04' pin is Populated
   if (SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration == IrvIsAdcPinPopulated[ADI_04])
   {
      IrvEcuVoltageValues[ADI_04] = (uint8)((float64)IoAdc0Input[ADC0_ADI_4] * ADC0_CONVFACTOR_ADI_PULLDOWN);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration == IrvIsAdcPinPopulated[ADI_04])
   {
      IrvEcuVoltageValues[ADI_04] = (uint8)((float64)IoAdc0Input[ADC0_ADI_4] * ADC0_CONVFACTOR_ADI_PULLUP);
   }
   else if(SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration == IrvIsAdcPinPopulated[ADI_04])
   {
      IrvEcuVoltageValues[ADI_04] = (uint8)((float64)IoAdc0Input[ADC0_ADI_4] * ADC0_CONVFACTOR_ADI_PULLUP);
   }
   else
   {
    IrvEcuVoltageValues[ADI_04] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'ADI_05' pin is Populated
   if (SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration == IrvIsAdcPinPopulated[ADI_05])
   {
      IrvEcuVoltageValues[ADI_05] = (uint8)((float64)IoAdc0Input[ADC0_ADI_5] * ADC0_CONVFACTOR_ADI_PULLDOWN);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration == IrvIsAdcPinPopulated[ADI_05])
   {
      IrvEcuVoltageValues[ADI_05] = (uint8)((float64)IoAdc0Input[ADC0_ADI_5] * ADC0_CONVFACTOR_ADI_PULLUP);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration == IrvIsAdcPinPopulated[ADI_05])
   {
      IrvEcuVoltageValues[ADI_05] = (uint8)((float64)IoAdc0Input[ADC0_ADI_5] * ADC0_CONVFACTOR_ADI_PULLUP);
   }
   else
   {
      IrvEcuVoltageValues[ADI_05] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'ADI_06' pin is Populated
   if (SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration == IrvIsAdcPinPopulated[ADI_06])
   {
      IrvEcuVoltageValues[ADI_06] = (uint8)((float64)IoAdc0Input[ADC0_ADI_6] * ADC0_CONVFACTOR_ADI_PULLDOWN);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration == IrvIsAdcPinPopulated[ADI_06])
   {
      IrvEcuVoltageValues[ADI_06] = (uint8)((float64)IoAdc0Input[ADC0_ADI_6] * ADC0_CONVFACTOR_ADI_PULLUP);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration == IrvIsAdcPinPopulated[ADI_06])
   {
      IrvEcuVoltageValues[ADI_06] = (uint8)((float64)IoAdc0Input[ADC0_ADI_6] * ADC0_CONVFACTOR_ADI_PULLUP);
   }
   else
   {
      IrvEcuVoltageValues[ADI_06] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'ADC7_10_PcbPopulatedInfo' 
//!
//! \param   *IrvEcuVoltageValues      Provides ECU voltage value for Irv
//! \param   *IrvIsAdcPinPopulated     Checks AdcPin status value for Irv
//!
//!====================================================================================================================
static void __inline__  ADC7_9_PcbPopulatedInfo(      uint8              *IrvEcuVoltageValues,
                                                const PcbPopulatedInfo_T *IrvIsAdcPinPopulated)
{
   //! ##### Condition for checking 'ADI_07' pin is Populated
   if (SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration == IrvIsAdcPinPopulated[ADI_07])
   {
      IrvEcuVoltageValues[ADI_07] = (uint8)((float64)IoAdc0Input[ADC0_ADI_7] * ADC0_CONVFACTOR_ADI_PULLDOWN);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration == IrvIsAdcPinPopulated[ADI_07])
   {
      IrvEcuVoltageValues[ADI_07] = (uint8)((float64)IoAdc0Input[ADC0_ADI_7] * ADC0_CONVFACTOR_ADI_PULLUP);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration == IrvIsAdcPinPopulated[ADI_07])
   {
      IrvEcuVoltageValues[ADI_07] = (uint8)((float64)IoAdc0Input[ADC0_ADI_7] * ADC0_CONVFACTOR_ADI_PULLUP);
   }
   else
   {
      IrvEcuVoltageValues[ADI_07] = (uint8)IO_AdcValue_PcbNotPopulated;
   }  
   //! ##### Condition for checking 'ADI_08' pin is Populated
   if (SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration == IrvIsAdcPinPopulated[ADI_08])
   {
      IrvEcuVoltageValues[ADI_08] = (uint8)((float64)IoAdc1Input[ADC1_ADI_8] * ADC1_CONVFACTOR_ADI_PULLDOWN);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration == IrvIsAdcPinPopulated[ADI_08])
   {
      IrvEcuVoltageValues[ADI_08] = (uint8)((float64)IoAdc1Input[ADC1_ADI_8] * ADC1_CONVFACTOR_ADI_PULLUP);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration == IrvIsAdcPinPopulated[ADI_08])
   {
      IrvEcuVoltageValues[ADI_08] = (uint8)((float64)IoAdc1Input[ADC1_ADI_8] * ADC1_CONVFACTOR_ADI_PULLUP);
   }
   else
   {
      IrvEcuVoltageValues[ADI_08] = (uint8)IO_AdcValue_PcbNotPopulated;
   }   
   //! ##### Condition for checking 'ADI_09' pin is Populated
   if (SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration == IrvIsAdcPinPopulated[ADI_09])
   {
      IrvEcuVoltageValues[ADI_09] = (uint8)((float64)IoAdc1Input[ADC1_ADI_9] * ADC1_CONVFACTOR_ADI_PULLDOWN);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration == IrvIsAdcPinPopulated[ADI_09])
   {
      IrvEcuVoltageValues[ADI_09] = (uint8)((float64)IoAdc1Input[ADC1_ADI_9] * ADC1_CONVFACTOR_ADI_PULLUP);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration == IrvIsAdcPinPopulated[ADI_09])
   {
      IrvEcuVoltageValues[ADI_09] = (uint8)((float64)IoAdc1Input[ADC1_ADI_9] * ADC1_CONVFACTOR_ADI_PULLUP);
   }
   else
   {
      IrvEcuVoltageValues[ADI_09] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the ADC10_12_PcbPopulatedInfo 
//!
//! \param   *IrvEcuVoltageValues      Provides ECU voltage value for Irv
//! \param   *IrvIsAdcPinPopulated     Checks AdcPin status value for Irv
//!
//!====================================================================================================================
static void __inline__  ADC10_12_PcbPopulatedInfo(      uint8              *IrvEcuVoltageValues,
                                                  const PcbPopulatedInfo_T *IrvIsAdcPinPopulated)
{
   //! ##### Condition for checking 'ADI_10' pin is Populated
   if (SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration == IrvIsAdcPinPopulated[ADI_10])
   {
      IrvEcuVoltageValues[ADI_10] = (uint8)((float64)IoAdc1Input[ADC1_ADI_10] * ADC1_CONVFACTOR_ADI_PULLDOWN);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration == IrvIsAdcPinPopulated[ADI_10])
   {
      IrvEcuVoltageValues[ADI_10] = (uint8)((float64)IoAdc1Input[ADC1_ADI_10] * ADC1_CONVFACTOR_ADI_PULLUP);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration == IrvIsAdcPinPopulated[ADI_10])
   {
      IrvEcuVoltageValues[ADI_10] = (uint8)((float64)IoAdc1Input[ADC1_ADI_10] * ADC1_CONVFACTOR_ADI_PULLUP);
   }
   else
   {
      IrvEcuVoltageValues[ADI_10] = (uint8)IO_AdcValue_PcbNotPopulated;
   } 
   //! ##### Condition for checking 'ADI_11' pin is Populated   
   if (SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration == IrvIsAdcPinPopulated[ADI_11])
   {
      IrvEcuVoltageValues[ADI_11] = (uint8)((float64)IoAdc1Input[ADC1_ADI_11] * ADC1_CONVFACTOR_ADI_PULLDOWN);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration == IrvIsAdcPinPopulated[ADI_11])
   {
      IrvEcuVoltageValues[ADI_11] = (uint8)((float64)IoAdc1Input[ADC1_ADI_11] * ADC1_CONVFACTOR_ADI_PULLUP);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration == IrvIsAdcPinPopulated[ADI_11])
   {
      IrvEcuVoltageValues[ADI_11] = (uint8)((float64)IoAdc1Input[ADC1_ADI_11] * ADC1_CONVFACTOR_ADI_PULLUP);
   }
   else
   {
      IrvEcuVoltageValues[ADI_11] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'ADI_12' pin is Populated
   if (SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration == IrvIsAdcPinPopulated[ADI_12])
   {
      IrvEcuVoltageValues[ADI_12] = (uint8)((float64)IoAdc1Input[ADC1_ADI_12] * ADC1_CONVFACTOR_ADI_PULLDOWN);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration == IrvIsAdcPinPopulated[ADI_12])
   {
      IrvEcuVoltageValues[ADI_12] = (uint8)((float64)IoAdc1Input[ADC1_ADI_12] * ADC1_CONVFACTOR_ADI_PULLUP);
   }
   else if (SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration == IrvIsAdcPinPopulated[ADI_12])
   {
      IrvEcuVoltageValues[ADI_12] = (uint8)((float64)IoAdc1Input[ADC1_ADI_12] * ADC1_CONVFACTOR_ADI_PULLUP);
   }
   else
   {
      IrvEcuVoltageValues[ADI_12] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'ADC13_16_PcbPopulatedInfo' 
//!
//! \param   *IrvEcuVoltageValues      Provides ECU voltage value for Irv
//! \param   *IrvIsAdcPinPopulated     Checks AdcPin status value for Irv
//!
//!====================================================================================================================
static void __inline__  ADC13_16_PcbPopulatedInfo(      uint8              *IrvEcuVoltageValues,
                                                  const PcbPopulatedInfo_T *IrvIsAdcPinPopulated)
{
   //! ##### Condition for checking 'ADI_13' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[ADI_13])
   {
      IrvEcuVoltageValues[ADI_13] = (uint8)((float64)IoAdc1Input[ADC1_CAN1_H2] * ADC1_CONVFACTOR_ADI_PULLUP);
   }
   else
   {
      IrvEcuVoltageValues[ADI_13] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //!##### Condition for checking 'ADI_14' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[ADI_14])
   {
      IrvEcuVoltageValues[ADI_14] = (uint8)((float64)IoAdc1Input[ADC1_CAN1_H1] * ADC1_CONVFACTOR_ADI_PULLUP);
   }
   else
   {
      IrvEcuVoltageValues[ADI_14] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'ADI_15' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[ADI_15])
   {
      IrvEcuVoltageValues[ADI_15] = (uint8)((float64)IoAdc1Input[ADC1_CAN1_L2] * ADC1_CONVFACTOR_ADI_PULLUP);
   }
   else
   {
      IrvEcuVoltageValues[ADI_15] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'ADI_16' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[ADI_16])
   {
      IrvEcuVoltageValues[ADI_16] = (uint8)((float64)IoAdc1Input[ADC1_CAN1_L1] * ADC1_CONVFACTOR_ADI_PULLUP);
   }
   else
   {
      IrvEcuVoltageValues[ADI_16] = (uint8)IO_AdcValue_PcbNotPopulated;
   }   
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'Dio_PcbPopulatedInfo' 
//!
//! \param   *IrvEcuVoltageValues      Provides ECU voltage value for Irv
//! \param   *IrvIsAdcPinPopulated     Checks AdcPin status value for Irv
//!
//!====================================================================================================================
static void __inline__  Dio_PcbPopulatedInfo(      uint8              *IrvEcuVoltageValues,
                                             const PcbPopulatedInfo_T *IrvIsAdcPinPopulated)
{
   //! ##### Condition for checking 'DAI_01' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[DAI_01])
   {
     IrvEcuVoltageValues[DAI_01] = (uint8)((float64)IoAdc1Input[ADC1_DAI1] * ADC1_CONVFACTOR_DAI);
   }
	//! #### If not true then assigning not populated 
   else
   {
     IrvEcuVoltageValues[DAI_01] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'DAI_02' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[DAI_02])
   {
     IrvEcuVoltageValues[DAI_02] = (uint8)((float64)IoAdc1Input[ADC1_DAI2] * ADC1_CONVFACTOR_DAI);
   }
	//! #### If not true then assigning not populated 
   else
   {
     IrvEcuVoltageValues[DAI_02] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'Dowhs_01' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[Dowhs_01])
   {
     IrvEcuVoltageValues[Dowhs_01] = (uint8)((float64)IoAdc1Input[ADC1_DOWHS1_MON] * ADC1_CONVFACTOR);
   }
	//! #### If not true then assigning not populated 
   else
   {
     IrvEcuVoltageValues[Dowhs_01] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'Dowhs_02' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[Dowhs_02])
   {
    IrvEcuVoltageValues[Dowhs_02] = (uint8)((float64)IoAdc0Input[ADC0_DOWHS2_MON] * ADC0_CONVFACTOR);
   }
	//! #### If not true then assigning not populated 
   else
   {
    IrvEcuVoltageValues[Dowhs_02] = (uint8)IO_AdcValue_PcbNotPopulated;
   } 
   //! ##### Condition for checking 'Dobls_01' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[Dobls_01])
   {
     IrvEcuVoltageValues[Dobls_01] = (uint8)((float64)IoAdc1Input[ADC1_DOBLS1_MON] * ADC1_CONVFACTOR);
   }
	//! #### If not true then assigning not populated 
   else
   {
     IrvEcuVoltageValues[Dobls_01] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'Dowls_02' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[Dowls_02])
   {
    IrvEcuVoltageValues[Dowls_02] = (uint8)((float64)IoAdc1Input[ADC1_DOWLS2_MON] * ADC1_CONVFACTOR);
   }
	//! #### If not true then assigning not populated 
   else
   {
    IrvEcuVoltageValues[Dowls_02] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'Dowls_03' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[Dowls_03])
   {
     IrvEcuVoltageValues[Dowls_03] = (uint8)((float64)IoAdc1Input[ADC1_DOWLS3_MON] * ADC1_CONVFACTOR);
   }
	//! #### If not true then assigning not populated 
   else
   {
     IrvEcuVoltageValues[Dowls_03] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'Dio_Dobhs_PcbPopulatedInfo' 
//!
//! \param   *IrvEcuVoltageValues      Provides ECU voltage value for Irv
//! \param   *IrvIsAdcPinPopulated     Checks AdcPin status value for Irv
//!
//!====================================================================================================================
static void __inline__  Dio_Dobhs_PcbPopulatedInfo(      uint8              *IrvEcuVoltageValues,
                                                   const PcbPopulatedInfo_T *IrvIsAdcPinPopulated)
{
   //! ##### Condition for checking 'Dobhs_01' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[Dobhs_01])
   {
     IrvEcuVoltageValues[Dobhs_01] = (uint8)((float64)IoAdc1Input[ADC1_DOBHS1_MON] * ADC1_CONVFACTOR);
   }
	//! #### If not true then assigning not populated 
   else
   {
     IrvEcuVoltageValues[Dobhs_01] = (uint8)IO_AdcValue_PcbNotPopulated;
   } 
   //! ##### Condition for checking 'Dobhs_02' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[Dobhs_02])
   {
      IrvEcuVoltageValues[Dobhs_02] = (uint8)((float64)IoAdc1Input[ADC1_DOBHS2_MON] * ADC1_CONVFACTOR);
   }
   else
   {
      IrvEcuVoltageValues[Dobhs_02] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'Dobhs_03' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[Dobhs_03])
   {
     IrvEcuVoltageValues[Dobhs_03] = (uint8)((float64)IoAdc1Input[ADC1_DOBHS3_MON] * ADC1_CONVFACTOR);
   }
	//! #### If not true then assigning not populated 
   else
   {
     IrvEcuVoltageValues[Dobhs_03] = (uint8)IO_AdcValue_PcbNotPopulated;
   } 
   //! ##### Condition for checking 'Dobhs_04' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[Dobhs_04])
   {
      IrvEcuVoltageValues[Dobhs_04] = (uint8)((float64)IoAdc1Input[ADC1_DOBHS4_MON] * ADC1_CONVFACTOR);
   }
	//! #### If not true then assigning not populated 
   else
   {
      IrvEcuVoltageValues[Dobhs_04] = (uint8)IO_AdcValue_PcbNotPopulated;
   } 
   //! ##### Condition for checking 'Dobhs_IPS1' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[Dobhs_IPS1])
   {
     IrvEcuVoltageValues[Dobhs_IPS1] = (uint8)((float64)IoAdc0Input[ADC0_DOBHS_IPS1_CS] * ADC0_CURCONVFACTOR_DOBHS);
   }
	//! #### If not true then assigning not populated 
   else
   {
     IrvEcuVoltageValues[Dobhs_IPS1] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'Dobhs_IPS2' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[Dobhs_IPS2])
   {
      IrvEcuVoltageValues[Dobhs_IPS2] = (uint8)((float64)IoAdc0Input[ADC0_DOBHS_IPS2_CS] * ADC0_CURCONVFACTOR_DOBHS);
   }
	//! #### If not true then assigning not populated 
   else
   {
      IrvEcuVoltageValues[Dobhs_IPS2] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'Dio_Dowhs_PcbPopulatedInfo' 
//!
//! \param   *IrvEcuVoltageValues        Provides Ecu voltage value for Irv
//! \param   *IrvIsAdcPinPopulated       Checks AdcPin status value for Irv
//!
//!====================================================================================================================
static void __inline__  Dio_Dowhs_PcbPopulatedInfo(      uint8              *IrvEcuVoltageValues,
                                                   const PcbPopulatedInfo_T *IrvIsAdcPinPopulated)
{
   //! ##### Condition for checking 'Dowhs_IPS' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[Dowhs_IPS])
   {
     IrvEcuVoltageValues[Dowhs_IPS] = (uint8)((float64)IoAdc0Input[ADC0_DOWHS_IPS_CS] * ADC0_CURCONVFACTOR_DOWHS);
   }
	//! #### If not true then assigning not populated 
   else
   {
     IrvEcuVoltageValues[Dowhs_IPS] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'PwrSupply' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[PwrSupply])
   {
     IrvEcuVoltageValues[PwrSupply]= (uint8)((float64)IoAdc0Input[ADC0_VBAT_LOAD_MON] * ADC0_CONVFACTOR); 
   }
	//! #### If not true then assigning not populated 
   else
   {
     IrvEcuVoltageValues[PwrSupply] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'Do12VDCDC' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[Do12VDCDC])
   {
     IrvEcuVoltageValues[Do12VDCDC]= (uint8)((float64)IoAdc1Input[ADC1_DC2DC_MON] * ADC1_CONVFACTOR);
   }
	//! #### If not true then assigning not populated 
   else
   {
     IrvEcuVoltageValues[Do12VDCDC] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'Do12VLiving' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[Do12VLiving])
   {
     IrvEcuVoltageValues[Do12VLiving]= (uint8)((float64)IoAdc1Input[ADC1_LIVING_MON] * ADC1_CONVFACTOR);
   }
	//! #### If not true then assigning not populated 
   else
   {
     IrvEcuVoltageValues[Do12VLiving] = (uint8)IO_AdcValue_PcbNotPopulated;
   }
   //! ##### Condition for checking 'Do12VParked' pin is Populated
   if (Populated == IrvIsAdcPinPopulated[Do12VParked])
   {
     IrvEcuVoltageValues[Do12VParked] = (uint8)(IoAdc1Input[ADC1_PARKED_MON] * ADC1_CONVFACTOR);
   }
	//! #### If not true then assigning not populated 
   else
   {
     IrvEcuVoltageValues[Do12VParked] = (uint8)IO_AdcValue_PcbNotPopulated;
   } 
   //! ##### Condition for checking 'Do12V_IPS' PinPopulated pin is Populated
   if (Populated == IrvIsAdcPinPopulated[Do12V_IPS])
   {
     IrvEcuVoltageValues[Do12V_IPS] = (uint8)(IoAdc1Input[ADC1_DO12_IPS_MULTI] * ADC1_CURCONVFACTOR_DCDC);
   }
	//! #### If not true then assigning not populated 
   else
   {
     IrvEcuVoltageValues[Do12V_IPS] = (uint8)IO_AdcValue_PcbNotPopulated;
   }   
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'EnablingAnlogInputPath' 
//!
//!====================================================================================================================
static void __inline__ EnablingAnlogInputPath(void)
{
   //! ##### Process for checking and enabling DOWHS01 Analog input path 
   if(Rte_Prm_P1V6O_Diag_Act_DOWHS01_v() != 1U)
	{
		SIUL2.MSCR[3].R = 0x00400000;	//DOWHS1 Analog input path enabled
		SIUL2.MSCR[31].R = 0x02080000; //DOWHS1 DIgital mode control
	}
   else
   {
      // Do nothing
   }
   //! ##### Process for checking and enabling DOWHS02 Analog input path 
   if(Rte_Prm_P1V6P_Diag_Act_DOWHS02_v() != 1U)
	{
		SIUL2.MSCR[29].R = 0x00400000;	//DOWHS2 Analog input path enabled
		SIUL2.MSCR[30].R = 0x02080000;	//DOWHS2 DIgital mode control
	}
   else
   {
      // Do nothing
   } 
   //! ##### Process for checking and enabling DOWLS02 Analog input path
   if(Rte_Prm_P1V7E_Diag_Act_DOWLS02_v() != 1U)
	{
		SIUL2.MSCR[9].R = 0x00400000;	//DOWLS2 Analog input path enabled
		Pwm_SetOutputToIdle(2);
		SIUL2.MSCR[45].R = 0x02080000; //DOWLS2 DIgital mode control
	}
   else
   {
      // Do nothing
   }
   //! ##### Process for checking and enabling DOWLS03 Analog input path
   if(Rte_Prm_P1V7F_Diag_Act_DOWLS03_v() != 1U)
   {
      SIUL2.MSCR[8].R = 0x00400000;   //DOWLS3 Analog input path enabled
		SIUL2.MSCR[70].R = 0x02080000;
   }
   else
   {
      // Do nothing
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IohwAb_Adc0Group_EndOfNotification' 
//!
//!====================================================================================================================
void IohwAb_Adc0Group_EndOfNotification(void)
{
	//To be implemented
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IohwAb_Adc1Group_EndOfNotification'
//!
//!====================================================================================================================
void IohwAb_Adc1Group_EndOfNotification(void)
{
	//To be implemented
}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
