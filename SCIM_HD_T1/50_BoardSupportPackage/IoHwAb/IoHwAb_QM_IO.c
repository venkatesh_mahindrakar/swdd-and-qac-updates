/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  IoHwAb_QM_IO.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  IoHwAb_QM_IO
 *  Generated at:  Mon Jun 29 18:01:33 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <IoHwAb_QM_IO>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
//!======================================================================================
//! \file IoHwAb_QM_IO.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup Platform_SCIM_IoHwAb
//! @{
//! @addtogroup IoHwAb_QM
//! @{
//! @addtogroup IoHwAb_QM_IO
//! @{
//!
//! \brief
//! IoHwAb_QM_IO SWC.
//! ASIL Level : QM.
//! This module implements the logic for the IoHwAb_QM_IO runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * IOHWAB_BOOL
 *   boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * IOHWAB_SINT8
 *   sint8 represents integers with a minimum value of -128 and a maximum value of 127.
 *      The order-relation on sint8 is: x < y if y - x is positive.
 *      sint8 has a lexical representation consisting of an optional sign followed 
 *      by a finite-length sequence of decimal digits (#x30-#x39). If the sign is 
 *      omitted, "+" is assumed. 
 *      
 *      For example: -1, 0, 12678, +10000.
 *
 * IOHWAB_UINT16
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * IOHWAB_UINT8
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Rte_DT_EcuHwDioCtrlArray_T_0
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Rte_DT_EcuHwFaultValues_T_0
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_Diag_Act_DOWHS01_P1V6O_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_Diag_Act_DOWHS02_P1V6P_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_Diag_Act_DOWLS02_P1V7E_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_Diag_Act_DOWLS03_P1V7F_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_HwToleranceThreshold_X1C04_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V60_ContactClosed_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V60_ContactOpen_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V61_ContactClosed_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V61_ContactOpen_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V62_ContactClosed_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V62_ContactOpen_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V63_ContactClosed_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V63_ContactOpen_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V64_ContactClosed_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V64_ContactOpen_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V65_ContactClosed_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V65_ContactOpen_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V66_ContactClosed_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V66_ContactOpen_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V67_ContactClosed_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V67_ContactOpen_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V68_ContactClosed_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V68_ContactOpen_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V69_ContactClosed_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V69_ContactOpen_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V6U_Threshold_OC_STB_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V6U_Threshold_STG_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V6V_Threshold_OC_STB_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V6V_Threshold_STG_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V6W_ContactClosed_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V6W_ContactOpen_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V6X_ContactClosed_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V6X_ContactOpen_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V6Y_ContactClosed_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V6Y_ContactOpen_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V6Z_ContactClosed_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V6Z_ContactOpen_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V8F_Threshold_VAT_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1V8F_Threshold_VBT_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1WMD_ThresholdHigh_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1WMD_ThresholdLow_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_Adi_X1CXW_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_CanInterfaces_X1CX2_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_DOWHS_X1CXY_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_DOWLS_X1CXZ_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_DoorAccessIf_X1CX3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_LinInterfaces_X1CX0_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CX4_P1Interface_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CX4_P2Interface_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CX4_P3Interface_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CX4_P4Interface_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CX4_PiInterface_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CY1_DigitalBiLevelHigh_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CY1_DigitalBiLevelLow_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinFaultStatus
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinVoltage_0V2
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPwmDutycycle
 *   sint8 represents integers with a minimum value of -128 and a maximum value of 127.
 *      The order-relation on sint8 is: x < y if y - x is positive.
 *      sint8 has a lexical representation consisting of an optional sign followed 
 *      by a finite-length sequence of decimal digits (#x30-#x39). If the sign is 
 *      omitted, "+" is assumed. 
 *      
 *      For example: -1, 0, 12678, +10000.
 *
 * VGTT_EcuPwmPeriod
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 *********************************************************************************************************************/

#include "Rte_IoHwAb_QM_IO.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "IoHwAb_QM_IO.h"
#include "Dio.h"
#include "Pwm.h"
#include "Icu.h"
#include "LpuRAMCode.h"


#define IoHwAb_QM_IO_START_SEC_VAR_INIT_UNSPECIFIED
#include "IoHwAb_QM_IO_MemMap.h"
static Rte_DT_EcuHwDioCtrlArray_T_0 EcuHwDioFuncCtrlArray[40] = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                 0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                 0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                 0U,0U,0U,0U,0U,0U,0U,0U,0U,0U};
static Rte_DT_EcuHwDioCtrlArray_T_0 EcuHwDioDiagCtrlArray[40] = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                 0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                 0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                 0U,0U,0U,0U,0U,0U,0U,0U,0U,0U};
static uint8 EcuVoltageValues[40] = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                     0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                     0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                     0U,0U,0U,0U,0U,0U,0U,0U,0U,0U};
static uint8 vAdiVoltageValues[20] = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                      0U,0U,0U,0U,0U,0U,0U,0U,0U,0U};
static uint8 Dowls1_Faultdetected,Dowls2_Faultdetected, Dowls3_Faultdetected;

IO_DOWXS_TYPE Io_Dowhs[2];
IO_DOWXS_TYPE Io_Dowls[3];


#define IoHwAb_QM_IO_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "IoHwAb_QM_IO_MemMap.h"

#define PCODE_DOWXSDISABLED 0U
#define PCODE_ENABLEPWM 1U
#define PCODE_ENABLEDIGITAL 2U
#define PCODE_OnState_FaultDetected_PWM 36U

static DowxsMeasuredValue Dowhs1MeasuredPWM;
static DowxsMeasuredValue Dowhs2MeasuredPWM;
static DowxsMeasuredValue Dowls2MeasuredPWM;
static DowxsMeasuredValue Dowls3MeasuredPWM;
// Notification flag for timestamp measurement done
uint8 Dowhs1TimestampDone=0U;
uint8 Dowhs2TimestampDone=0U;
uint8 Dowls2TimestampDone=0U;
uint8 Dowls3TimestampDone=0U;
// Buffer for timestamp measurement
uint32 Dowhs1TimestampBuf[BUFSIZE_TIMESTAMP];
uint32 Dowhs2TimestampBuf[BUFSIZE_TIMESTAMP];
uint32 Dowls2TimestampBuf[BUFSIZE_TIMESTAMP];
uint32 Dowls3TimestampBuf[BUFSIZE_TIMESTAMP];
static uint16 Dowhs1_Period=0;
static uint16 Dowhs1_PrevPeriod=0;
static uint16 Dowhs1_Duty= 0;
static uint16 Dowhs1_PrevDuty= 0;

static uint16 Dowhs2_Period=0;
static uint16 Dowhs2_PrevPeriod=0;
static uint16 Dowhs2_Duty=0;
static uint16 Dowhs2_PrevDuty=0;

static uint16 Dowls2_Period=0;
static uint16 Dowls2_PrevPeriod=0;
static uint16 Dowls2_Duty=0;
static uint16 Dowls2_PrevDuty=0;
static uint16 Dowls3_Period=0;
static uint16 Dowls3_PrevPeriod=0;
static uint16 Dowls3_Duty= 0;
static uint16 Dowls3_PrevDuty= 0;

static uint8 Dowhs1_OverFlowDetected=0;
static uint8 Dowhs2_OverFlowDetected=0;
static uint8 Dowls2_OverFlowDetected=0;
static uint8 Dowls3_OverFlowDetected=0;

static uint8 Dowhs1_PeriodFaultCnt=0;
static uint8 Dowhs2_PeriodFaultCnt=0;
static uint8 Dowls2_PeriodFaultCnt=0;
static uint8 Dowls3_PeriodFaultCnt=0;


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IOHWAB_BOOL: Boolean
 * IOHWAB_SINT8: Integer in interval [-128...127]
 * IOHWAB_UINT16: Integer in interval [0...65535]
 * IOHWAB_UINT8: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWHS01_P1V6O_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWHS02_P1V6P_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWLS02_P1V7E_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWLS03_P1V7F_T: Integer in interval [0...255]
 * SEWS_HwToleranceThreshold_X1C04_T: Integer in interval [0...255]
 * SEWS_P1V60_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V60_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V61_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V61_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V62_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V62_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V63_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V63_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V64_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V64_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V65_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V65_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V66_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V66_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V67_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V67_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V68_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V68_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V69_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V69_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V6U_Threshold_OC_STB_T: Integer in interval [0...255]
 * SEWS_P1V6U_Threshold_STG_T: Integer in interval [0...255]
 * SEWS_P1V6V_Threshold_OC_STB_T: Integer in interval [0...255]
 * SEWS_P1V6V_Threshold_STG_T: Integer in interval [0...255]
 * SEWS_P1V6W_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V6W_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V6X_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V6X_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V6Y_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V6Y_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V6Z_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V6Z_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V8F_Threshold_VAT_T: Integer in interval [0...255]
 * SEWS_P1V8F_Threshold_VBT_T: Integer in interval [0...255]
 * SEWS_P1WMD_ThresholdHigh_T: Integer in interval [0...255]
 * SEWS_P1WMD_ThresholdLow_T: Integer in interval [0...255]
 * SEWS_PcbConfig_DoorAccessIf_X1CX3_T: Integer in interval [0...255]
 * SEWS_X1CX4_P1Interface_T: Integer in interval [0...255]
 * SEWS_X1CX4_P2Interface_T: Integer in interval [0...255]
 * SEWS_X1CX4_P3Interface_T: Integer in interval [0...255]
 * SEWS_X1CX4_P4Interface_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelHigh_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelLow_T: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * VGTT_EcuPwmDutycycle: Integer in interval [-128...127]
 * VGTT_EcuPwmPeriod: Integer in interval [0...65535]
 * boolean: Boolean (standard type)
 * sint8: Integer in interval [-128...127] (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * Fsc_OperationalMode_T: Enumeration of integer in interval [0...255] with enumerators
 *   FSC_ShutdownReady (0U)
 *   FSC_Reduced_12vDcDcLimit (1U)
 *   FSC_Reduced (2U)
 *   FSC_Operating (3U)
 *   FSC_Protecting (4U)
 *   FSC_Withstand (5U)
 *   FSC_NotAvailable (6U)
 * IOCtrlReq_T: Enumeration of integer in interval [0...255] with enumerators
 *   IOCtrl_AppRequest (0U)
 *   IOCtrl_DiagReturnCtrlToApp (1U)
 *   IOCtrl_DiagShortTermAdjust (2U)
 * Rte_DT_EcuHwDioCtrlArray_T_0: Enumeration of integer in interval [0...255] with enumerators
 *   STD_LOW (0U)
 *   STD_HIGH (1U)
 *   Inactive (255U)
 * Rte_DT_EcuHwFaultValues_T_0: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 * SEWS_PcbConfig_Adi_X1CXW_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_Adi_X1CXW_T_NotPopulated (0U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration (1U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration (2U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration (3U)
 * SEWS_PcbConfig_CanInterfaces_X1CX2_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_CanInterfaces_X1CX2_T_NotPopulated (0U)
 *   SEWS_PcbConfig_CanInterfaces_X1CX2_T_Populated (1U)
 * SEWS_PcbConfig_DOWHS_X1CXY_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOWHS_X1CXY_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOWHS_X1CXY_T_Populated (1U)
 * SEWS_PcbConfig_DOWLS_X1CXZ_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T_Populated (1U)
 * SEWS_PcbConfig_LinInterfaces_X1CX0_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T_NotPopulated (0U)
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated (1U)
 * SEWS_X1CX4_PiInterface_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_X1CX4_PiInterface_T_NotPopulated (0U)
 *   SEWS_X1CX4_PiInterface_T_Populated (1U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * EcuHwDioCtrlArray_T: Array with 40 element(s) of type Rte_DT_EcuHwDioCtrlArray_T_0
 * EcuHwFaultValues_T: Array with 40 element(s) of type Rte_DT_EcuHwFaultValues_T_0
 * EcuHwVoltageValues_T: Array with 40 element(s) of type VGTT_EcuPinVoltage_0V2
 * SEWS_AdiWakeUpConfig_P1WMD_a_T: Array with 16 element(s) of type SEWS_AdiWakeUpConfig_P1WMD_s_T
 * SEWS_PcbConfig_Adi_X1CXW_a_T: Array with 19 element(s) of type SEWS_PcbConfig_Adi_X1CXW_T
 * SEWS_PcbConfig_CanInterfaces_X1CX2_a_T: Array with 6 element(s) of type SEWS_PcbConfig_CanInterfaces_X1CX2_T
 * SEWS_PcbConfig_DOWHS_X1CXY_a_T: Array with 2 element(s) of type SEWS_PcbConfig_DOWHS_X1CXY_T
 * SEWS_PcbConfig_DOWLS_X1CXZ_a_T: Array with 3 element(s) of type SEWS_PcbConfig_DOWLS_X1CXZ_T
 * SEWS_PcbConfig_LinInterfaces_X1CX0_a_T: Array with 7 element(s) of type SEWS_PcbConfig_LinInterfaces_X1CX0_T
 *
 * Record Types:
 * =============
 * SEWS_AdiWakeUpConfig_P1WMD_s_T: Record with elements
 *   ThresholdHigh of type SEWS_P1WMD_ThresholdHigh_T
 *   ThresholdLow of type SEWS_P1WMD_ThresholdLow_T
 *   isActiveInLiving of type boolean
 *   isActiveInParked of type boolean
 * SEWS_DAI_Installed_P1WMP_s_T: Record with elements
 *   LeftDoor of type boolean
 *   RightDoor of type boolean
 * SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T: Record with elements
 *   DigitalBiLevelLow of type SEWS_X1CY1_DigitalBiLevelLow_T
 *   DigitalBiLevelHigh of type SEWS_X1CY1_DigitalBiLevelHigh_T
 * SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T: Record with elements
 *   Threshold_VBT of type SEWS_P1V8F_Threshold_VBT_T
 *   Threshold_VAT of type SEWS_P1V8F_Threshold_VAT_T
 * SEWS_Fault_Config_ADI01_P1V6U_s_T: Record with elements
 *   Threshold_OC_STB of type SEWS_P1V6U_Threshold_OC_STB_T
 *   Threshold_STG of type SEWS_P1V6U_Threshold_STG_T
 * SEWS_Fault_Config_ADI02_P1V6V_s_T: Record with elements
 *   Threshold_OC_STB of type SEWS_P1V6V_Threshold_OC_STB_T
 *   Threshold_STG of type SEWS_P1V6V_Threshold_STG_T
 * SEWS_Fault_Config_ADI03_P1V6W_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V6W_ContactOpen_T
 *   ContactClosed of type SEWS_P1V6W_ContactClosed_T
 * SEWS_Fault_Config_ADI04_P1V6X_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V6X_ContactOpen_T
 *   ContactClosed of type SEWS_P1V6X_ContactClosed_T
 * SEWS_Fault_Config_ADI05_P1V6Y_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V6Y_ContactOpen_T
 *   ContactClosed of type SEWS_P1V6Y_ContactClosed_T
 * SEWS_Fault_Config_ADI06_P1V6Z_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V6Z_ContactOpen_T
 *   ContactClosed of type SEWS_P1V6Z_ContactClosed_T
 * SEWS_Fault_Config_ADI07_P1V60_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V60_ContactOpen_T
 *   ContactClosed of type SEWS_P1V60_ContactClosed_T
 * SEWS_Fault_Config_ADI08_P1V61_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V61_ContactOpen_T
 *   ContactClosed of type SEWS_P1V61_ContactClosed_T
 * SEWS_Fault_Config_ADI09_P1V62_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V62_ContactOpen_T
 *   ContactClosed of type SEWS_P1V62_ContactClosed_T
 * SEWS_Fault_Config_ADI10_P1V63_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V63_ContactOpen_T
 *   ContactClosed of type SEWS_P1V63_ContactClosed_T
 * SEWS_Fault_Config_ADI11_P1V64_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V64_ContactOpen_T
 *   ContactClosed of type SEWS_P1V64_ContactClosed_T
 * SEWS_Fault_Config_ADI12_P1V65_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V65_ContactOpen_T
 *   ContactClosed of type SEWS_P1V65_ContactClosed_T
 * SEWS_Fault_Config_ADI13_P1V66_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V66_ContactOpen_T
 *   ContactClosed of type SEWS_P1V66_ContactClosed_T
 * SEWS_Fault_Config_ADI14_P1V67_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V67_ContactOpen_T
 *   ContactClosed of type SEWS_P1V67_ContactClosed_T
 * SEWS_Fault_Config_ADI15_P1V68_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V68_ContactOpen_T
 *   ContactClosed of type SEWS_P1V68_ContactClosed_T
 * SEWS_Fault_Config_ADI16_P1V69_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V69_ContactOpen_T
 *   ContactClosed of type SEWS_P1V69_ContactClosed_T
 * SEWS_PcbConfig_AdiPullUp_X1CX5_s_T: Record with elements
 *   AdiPullupLiving of type boolean
 *   AdiPullupParked of type boolean
 * SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T: Record with elements
 *   PiInterface of type SEWS_X1CX4_PiInterface_T
 *   P1Interface of type SEWS_X1CX4_P1Interface_T
 *   P2Interface of type SEWS_X1CX4_P2Interface_T
 *   P3Interface of type SEWS_X1CX4_P3Interface_T
 *   P4Interface of type SEWS_X1CX4_P4Interface_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_HwToleranceThreshold_X1C04_T Rte_Prm_X1C04_HwToleranceThreshold_v(void)
 *   SEWS_PcbConfig_DoorAccessIf_X1CX3_T Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v(void)
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T *Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(void)
 *     Returnvalue: SEWS_PcbConfig_LinInterfaces_X1CX0_T* is of type SEWS_PcbConfig_LinInterfaces_X1CX0_a_T
 *   SEWS_PcbConfig_CanInterfaces_X1CX2_T *Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(void)
 *     Returnvalue: SEWS_PcbConfig_CanInterfaces_X1CX2_T* is of type SEWS_PcbConfig_CanInterfaces_X1CX2_a_T
 *   SEWS_PcbConfig_Adi_X1CXW_T *Rte_Prm_X1CXW_PcbConfig_Adi_v(void)
 *     Returnvalue: SEWS_PcbConfig_Adi_X1CXW_T* is of type SEWS_PcbConfig_Adi_X1CXW_a_T
 *   SEWS_PcbConfig_DOWHS_X1CXY_T *Rte_Prm_X1CXY_PcbConfig_DOWHS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOWHS_X1CXY_T* is of type SEWS_PcbConfig_DOWHS_X1CXY_a_T
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T *Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOWLS_X1CXZ_T* is of type SEWS_PcbConfig_DOWLS_X1CXZ_a_T
 *   SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T *Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v(void)
 *   SEWS_PcbConfig_AdiPullUp_X1CX5_s_T *Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v(void)
 *   SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void)
 *   SEWS_Diag_Act_DOWHS01_P1V6O_T Rte_Prm_P1V6O_Diag_Act_DOWHS01_v(void)
 *   SEWS_Diag_Act_DOWHS02_P1V6P_T Rte_Prm_P1V6P_Diag_Act_DOWHS02_v(void)
 *   SEWS_Diag_Act_DOWLS02_P1V7E_T Rte_Prm_P1V7E_Diag_Act_DOWLS02_v(void)
 *   SEWS_Diag_Act_DOWLS03_P1V7F_T Rte_Prm_P1V7F_Diag_Act_DOWLS03_v(void)
 *   boolean Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v(void)
 *   boolean Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v(void)
 *   boolean Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v(void)
 *   boolean Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v(void)
 *   SEWS_AdiWakeUpConfig_P1WMD_s_T *Rte_Prm_P1WMD_AdiWakeUpConfig_v(void)
 *     Returnvalue: SEWS_AdiWakeUpConfig_P1WMD_s_T* is of type SEWS_AdiWakeUpConfig_P1WMD_a_T
 *   SEWS_Fault_Config_ADI07_P1V60_s_T *Rte_Prm_P1V60_Fault_Config_ADI07_v(void)
 *   SEWS_Fault_Config_ADI08_P1V61_s_T *Rte_Prm_P1V61_Fault_Config_ADI08_v(void)
 *   SEWS_Fault_Config_ADI09_P1V62_s_T *Rte_Prm_P1V62_Fault_Config_ADI09_v(void)
 *   SEWS_Fault_Config_ADI10_P1V63_s_T *Rte_Prm_P1V63_Fault_Config_ADI10_v(void)
 *   SEWS_Fault_Config_ADI11_P1V64_s_T *Rte_Prm_P1V64_Fault_Config_ADI11_v(void)
 *   SEWS_Fault_Config_ADI12_P1V65_s_T *Rte_Prm_P1V65_Fault_Config_ADI12_v(void)
 *   SEWS_Fault_Config_ADI13_P1V66_s_T *Rte_Prm_P1V66_Fault_Config_ADI13_v(void)
 *   SEWS_Fault_Config_ADI14_P1V67_s_T *Rte_Prm_P1V67_Fault_Config_ADI14_v(void)
 *   SEWS_Fault_Config_ADI15_P1V68_s_T *Rte_Prm_P1V68_Fault_Config_ADI15_v(void)
 *   SEWS_Fault_Config_ADI16_P1V69_s_T *Rte_Prm_P1V69_Fault_Config_ADI16_v(void)
 *   SEWS_Fault_Config_ADI01_P1V6U_s_T *Rte_Prm_P1V6U_Fault_Config_ADI01_v(void)
 *   SEWS_Fault_Config_ADI02_P1V6V_s_T *Rte_Prm_P1V6V_Fault_Config_ADI02_v(void)
 *   SEWS_Fault_Config_ADI03_P1V6W_s_T *Rte_Prm_P1V6W_Fault_Config_ADI03_v(void)
 *   SEWS_Fault_Config_ADI04_P1V6X_s_T *Rte_Prm_P1V6X_Fault_Config_ADI04_v(void)
 *   SEWS_Fault_Config_ADI05_P1V6Y_s_T *Rte_Prm_P1V6Y_Fault_Config_ADI05_v(void)
 *   SEWS_Fault_Config_ADI06_P1V6Z_s_T *Rte_Prm_P1V6Z_Fault_Config_ADI06_v(void)
 *   SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T *Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v(void)
 *   SEWS_DAI_Installed_P1WMP_s_T *Rte_Prm_P1WMP_DAI_Installed_v(void)
 *   boolean Rte_Prm_P1WPP_isSecurityLinActive_v(void)
 *
 *********************************************************************************************************************/


#define IoHwAb_QM_IO_START_SEC_CODE
#include "IoHwAb_QM_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AdiInterface_P_GetAdiPinState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetAdiPinState_CS> of PortPrototype <AdiInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_AdiInterface_P_GetAdiPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *   void Rte_IrvRead_AdiInterface_P_GetAdiPinState_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AdiInterface_P_GetAdiPinState_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the AdiInterface_P_GetAdiPinState_CS
//! 
//! \param  *AdiPinRef          Pin reference for the ADI pin
//! \param  *AdiPinVoltage      Voltage at the ADI pin
//! \param  *BatteryVoltage     For reading Current Battery voltage  
//! \param  *FaultStatus        In case of fault occured , that will be stored
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) AdiPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AdiInterface_P_GetAdiPinState_CS (returns application error)
 *********************************************************************************************************************/
   Std_ReturnType              retval          = RTE_E_OK;
   Rte_DT_EcuHwFaultValues_T_0 FaultValues[40] = {0,0,0,0,0,0,0,0,0,0,
                                                  0,0,0,0,0,0,0,0,0,0,
                                                  0,0,0,0,0,0,0,0,0,0,
                                                  0,0,0,0,0,0,0,0,0,0,}; 
   //! ##### Condition to status for AdiPin interface 
   if ((AdiPinRef >= (IOHWAB_UINT8)ADI_01)
      &&(AdiPinRef <= (IOHWAB_UINT8)DAI_02)
      && (AdiPinVoltage != NULL_PTR)
      && (BatteryVoltage != NULL_PTR) 
      && (FaultStatus != NULL_PTR))
   {
      //! #### Reading the FaultValues using a IRV call
      Rte_IrvRead_AdiInterface_P_GetAdiPinState_CS_IrvEcuIoQmFaultStatus(&FaultValues[0]);
      //! #### Updating the AdiPinVoltage and BatteryVoltage values      
      *AdiPinVoltage  = vAdiVoltageValues[AdiPinRef];   
      *BatteryVoltage = EcuVoltageValues[PwrSupply];
      //! #### Updating the Fault Status
      *FaultStatus    = FaultValues[AdiPinRef];
   }
   else
   {
      retval = RTE_E_INVALID;
   }    
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AdiInterface_P_GetPullUpState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetPullUpState_CS> of PortPrototype <AdiInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_AdiInterface_P_GetPullUpState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *   void Rte_IrvRead_AdiInterface_P_GetPullUpState_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType AdiInterface_P_GetPullUpState_CS(IOHWAB_BOOL *isPullUpActive_Strong, IOHWAB_BOOL *isPullUpActive_Weak, IOHWAB_BOOL *isPullUpActive_DAI)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AdiInterface_P_GetPullUpState_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the AdiInterface_P_GetPullUpState_CS
//! 
//! \param  *isPullUpActive_Strong    for reading the Dio control values
//! \param  *isPullUpActive_Weak      for reading the Dio control values
//! \param  *isPullUpActive_DAI       for reading the Dio control values
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) AdiInterface_P_GetPullUpState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) isPullUpActive_Strong, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) isPullUpActive_Weak, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) isPullUpActive_DAI) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AdiInterface_P_GetPullUpState_CS (returns application error)
 *********************************************************************************************************************/
   Std_ReturnType               retval            = RTE_E_OK;
   Rte_DT_EcuHwDioCtrlArray_T_0 DioCtrlValues[40] = {0,0,0,0,0,0,0,0,0,0,
                                                     0,0,0,0,0,0,0,0,0,0,
                                                     0,0,0,0,0,0,0,0,0,0,
                                                     0,0,0,0,0,0,0,0,0,0,};
   //! ##### Condition to check pullupActive pin status 
   if ((isPullUpActive_Strong != NULL_PTR)
      && (isPullUpActive_Weak != NULL_PTR)
      && (isPullUpActive_DAI != NULL_PTR))
   {
      //! #### Reading DioCtrlValues using Irv Call :'Rte_IrvRead_AdiInterface_P_GetPullUpState_CS_IrvEcuHwDioCtrlArray()'
      Rte_IrvRead_AdiInterface_P_GetPullUpState_CS_IrvEcuHwDioCtrlArray(&DioCtrlValues[0]);
      //! #### Assigning values to isPullUpActive_Strong,isPullUpActive_Weak &isPullUpActive_DAI
      *isPullUpActive_Strong = (IOHWAB_BOOL)DioCtrlValues[LivingPullup];
      *isPullUpActive_Weak   = (IOHWAB_BOOL)DioCtrlValues[ParkedPullup];
      *isPullUpActive_DAI    = (IOHWAB_BOOL)DioCtrlValues[DaiActivation];
   }
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AdiInterface_P_SetPullUp_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetPullUp_CS> of PortPrototype <AdiInterface_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType AdiInterface_P_SetPullUp_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL ActivateStrongPullUp, IOHWAB_BOOL ActivateWeakPullUp, IOHWAB_BOOL ActivateDAIPullUp)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AdiInterface_P_SetPullUp_CS_doc
 *********************************************************************************************************************/
   
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the AdiInterface_P_SetPullUp_CS
//! 
//! \param  IOCtrlReqType           For checking IOCTRL request 
//! \param  ActivateStrongPullUp    Getting Strong Pullup request
//! \param  ActivateWeakPullUp      Getting weak Pullup request
//! \param  ActivateDAIPullUp       Getting DAI Pullup request
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) AdiInterface_P_SetPullUp_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL ActivateStrongPullUp, IOHWAB_BOOL ActivateWeakPullUp, IOHWAB_BOOL ActivateDAIPullUp) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AdiInterface_P_SetPullUp_CS (returns application error)
 *********************************************************************************************************************/
         Std_ReturnType                     retval                   = RTE_E_OK;
   const SEWS_PcbConfig_AdiPullUp_X1CX5_s_T *PcbConfig_AdiPullup     = Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
         SEWS_PcbConfig_DoorAccessIf_X1CX3_T PcbConfig_DaiInterfaces = 0U;
   //! ##### Reading PcbConfig for DAI interfaces :'PCODE_X1CX3_PcbConfig_DoorAccessIf_v'
   PcbConfig_DaiInterfaces = (SEWS_PcbConfig_DoorAccessIf_X1CX3_T) Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v(); 
   retval                  = AdiInterface_P_SetPullupLiving_Activate(PcbConfig_AdiPullup, 
                                                                     IOCtrlReqType, 
                                                                     ActivateStrongPullUp);
   retval                  = AdiInterface_P_SetPullupParked_Activate(PcbConfig_AdiPullup, 
                                                                     IOCtrlReqType,
                                                                     ActivateStrongPullUp);
   retval                  = AdiInterface_P_SetPullupDAI_Activate(PcbConfig_DaiInterfaces,
                                                                  IOCtrlReqType,
                                                                  ActivateDAIPullUp);
   //! ##### Condition to check  ActivateDAIPullUp, ActivateWeakPullUp, ActivateStrongPullUp status                                                              ActivateDAIPullUp);   
   if ((0xffU == ActivateDAIPullUp)
      && (0xffU == ActivateWeakPullUp)
      && (0xffU == ActivateStrongPullUp))
   { 
      retval = RTE_E_INVALID;
   }
   else
   {
      // do nothing
   }            
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Do12VInterface_P_GetDcdc12VState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDcdc12VState_CS> of PortPrototype <Do12VInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_Do12VInterface_P_GetDcdc12VState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *   void Rte_IrvRead_Do12VInterface_P_GetDcdc12VState_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Do12VInterface_P_GetDcdc12VState_CS(VGTT_EcuPinVoltage_0V2 *DcDc12vRefVoltage, IOHWAB_BOOL *IsDcDc12vActivated, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_GetDcdc12VState_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the Do12VInterface_P_GetDcdc12VState_CS
//! 
//! \param  *DcDc12vRefVoltage       For reading DCDC12v referance voltage
//! \param  *IsDcDc12vActivated      For checking DCDC12v Activation status
//! \param  *FaultStatus             For checking DCDC12v Fault Status
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) Do12VInterface_P_GetDcdc12VState_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DcDc12vRefVoltage, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDcDc12vActivated, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_GetDcdc12VState_CS (returns application error)
 *********************************************************************************************************************/
   Std_ReturnType               retval           = RTE_E_OK;
   Rte_DT_EcuHwFaultValues_T_0  FaultValues[40]  = {0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,};
   Rte_DT_EcuHwDioCtrlArray_T_0 DioCtrlArray[40] = {0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,};
   //! ###### Provide the DCDC readback voltage and faultstatus value
   if ((DcDc12vRefVoltage != NULL_PTR)
      && (IsDcDc12vActivated != NULL_PTR) 
      && (FaultStatus != NULL_PTR))
   {
      Rte_IrvRead_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuHwDioCtrlArray(&DioCtrlArray[0]);
      Rte_IrvRead_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuIoQmFaultStatus(&FaultValues[0]);
      *DcDc12vRefVoltage  = EcuVoltageValues[Do12VDCDC];
      *IsDcDc12vActivated = DioCtrlArray[Do12VDCDC];
      *FaultStatus        = FaultValues[Do12VDCDC];
   }
   else 
   {
      retval = RTE_E_INVALID;
   }      
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Do12VInterface_P_GetDo12VPinsState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDo12VPinsState_CS> of PortPrototype <Do12VInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *   void Rte_IrvRead_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_GetDo12VPinsState_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the Do12VInterface_P_GetDo12VPinsState_CS
//! 
//! \param  SelectParkedOrLivingPin     Selection pin for Parked or Living
//! \param  *IsDo12VActivated           For checking DO12V Activation Status
//! \param  *Do12VPinVoltage            For checking Do12 pin Voltage
//! \param  *BatteryVoltage             For reading Current Battery voltage       
//! \param  *FaultStatus                For reading Faults
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDo12VActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Do12VPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_GetDo12VPinsState_CS (returns application error)
 *********************************************************************************************************************/
   Std_ReturnType               retval           = RTE_E_OK;
   Rte_DT_EcuHwDioCtrlArray_T_0 DioCtrlArray[40] = {0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,};
   Rte_DT_EcuHwFaultValues_T_0  FaultValues[40]  = {0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,};
   //! ##### Provide the Battery, 12VLinving/Parked readback voltages and faultstatus value   
   Rte_IrvRead_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuHwDioCtrlArray(&DioCtrlArray[0]);
   Rte_IrvRead_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuIoQmFaultStatus(&FaultValues[0]);
   //! ##### Report the voltage values and faultstatus based on the Selected Parked/Living Pin  
   if ((IsDo12VActivated != NULL_PTR) 
      && (BatteryVoltage != NULL_PTR) 
      && (Do12VPinVoltage != NULL_PTR) 
      && (FaultStatus != NULL_PTR))
   {   
      if (0U == SelectParkedOrLivingPin)
      {
         *BatteryVoltage   = EcuVoltageValues[PwrSupply];
         *Do12VPinVoltage  = EcuVoltageValues[Do12VParked];
         if (*Do12VPinVoltage > 45)
         {
            *IsDo12VActivated = 1U;
         }
         else
         {
           *IsDo12VActivated = 0U;
         }
         *FaultStatus = FaultValues[Do12VParked];         
      }
      else if (1U == SelectParkedOrLivingPin)
      {
         *BatteryVoltage   = EcuVoltageValues[PwrSupply];
         *Do12VPinVoltage  = EcuVoltageValues[Do12VLiving];
         if (*Do12VPinVoltage > 45)
         {
            *IsDo12VActivated = 1U;
         }
         else
         {
            *IsDo12VActivated = 0U;
         }
         *FaultStatus = FaultValues[Do12VLiving];
      }
      else
      {
         // do nothing
      }
   }
   else
   {
      retval = RTE_E_INVALID;
   }      
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Do12VInterface_P_SetDcdc12VActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDcdc12VActive_CS> of PortPrototype <Do12VInterface_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_SetDcdc12VActive_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the Do12VInterface_P_SetDcdc12VActive_CS
//! 
//! \param  IOCtrlReqType    For checking IOCTRL request  
//! \param  Activation       For checking activation deactivation 
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_SetDcdc12VActive_CS (returns application error)
 *********************************************************************************************************************/
   //! ##### Based on the activation request update the EcuHwDioFuncCtrlArray buffer    
   if (IOCtrlReqType == IOCtrl_AppRequest)
   {
      EcuHwDioFuncCtrlArray[Do12VDCDC] = Activation;      
   }
	//! ##### If short term adjustment is requested activate the Do12VDCDC pin
   else if (IOCtrlReqType == IOCtrl_DiagShortTermAdjust)
   {
      EcuHwDioDiagCtrlArray[Do12VDCDC] = Activation;      
   }
	//! ##### If return control to application is requested then fill with 0xff value
   else if (IOCtrlReqType == IOCtrl_DiagReturnCtrlToApp)
   {
      EcuHwDioDiagCtrlArray[Do12VDCDC] = (Rte_DT_EcuHwDioCtrlArray_T_0)0xff;
   }
   else
   {
      //No activation   
   }          
   return RTE_E_OK;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Do12VInterface_P_SetDo12VLivingActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDo12VLivingActive_CS> of PortPrototype <Do12VInterface_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_SetDo12VLivingActive_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the Do12VInterface_P_SetDo12VLivingActive_CS
//! 
//! \param  IOCtrlReqType    For checking IOCTRL request
//! \param  Activation       For checking activation deactivation
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_SetDo12VLivingActive_CS (returns application error)
 *********************************************************************************************************************/
   //! ##### Based on the activation request update the EcuHwDioFuncCtrlArray buffer
   const SEWS_PcbConfig_LinInterfaces_X1CX0_T *PcbConfig_LinInterfaces = Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v();
   //! ##### Condition to check PcbConfig_LinInterfaces    
   if ((PcbConfig_LinInterfaces[0] == SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated) 
      || (PcbConfig_LinInterfaces[1] == SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated)
      || (PcbConfig_LinInterfaces[2] == SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated)
      || (PcbConfig_LinInterfaces[3] == SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated)
      || (PcbConfig_LinInterfaces[4] == SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated)
      || (PcbConfig_LinInterfaces[5] == SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated)
      || (PcbConfig_LinInterfaces[6] == SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated))
   {
      //! ####Check whether the request is from application or diagnostic and update the 12Vliving pin      
      if (IOCtrlReqType == IOCtrl_AppRequest)
      {
         EcuHwDioFuncCtrlArray[Do12VLiving] = Activation;       
      }
		//! ##### If short term adjustment is requested activate the 12VLiving pin
      else if (IOCtrlReqType == IOCtrl_DiagShortTermAdjust)
      {
         EcuHwDioDiagCtrlArray[Do12VLiving] = Activation;       
      }
		//! ##### If return control to application is requested then fill with 0xff value
      else if (IOCtrlReqType == IOCtrl_DiagReturnCtrlToApp)
      {
         EcuHwDioDiagCtrlArray[Do12VLiving] = (Rte_DT_EcuHwDioCtrlArray_T_0)0xff;       
      }
      else
      {
         //Do nothing 
      }
   }
   else
   {
      // Do nothing  
   }
   return RTE_E_OK;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Do12VInterface_P_SetDo12VParkedActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDo12VParkedActive_CS> of PortPrototype <Do12VInterface_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_SetDo12VParkedActive_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the Do12VInterface_P_SetDo12VParkedActive_CS
//! 
//! \param  IOCtrlReqType    For checking IOCTRL request
//! \param  Activation       For checking activation deactivation
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_SetDo12VParkedActive_CS (returns application error)
 *********************************************************************************************************************/
   Boolean isSecurityLinActive;
   //! ##### Update the activation request for 12VParked control to the corresponding EcuHwDioFuncCtrlArray location
   //! ##### Based on the activation request update the Irv buffer  
   isSecurityLinActive = Rte_Prm_P1WPP_isSecurityLinActive_v();
   if (isSecurityLinActive == 1)
   {
      //! #### Check whether the request is from application or diagnostic and update the Do12VParked pin     
      if (IOCtrlReqType == IOCtrl_AppRequest)
      {
        EcuHwDioFuncCtrlArray[Do12VParked] = Activation;        
      }
		//! ##### If short term adjustment is requested activate the Do12VParked pin
      else if (IOCtrlReqType == IOCtrl_DiagShortTermAdjust)
      {
        EcuHwDioDiagCtrlArray[Do12VParked] = Activation;        
      }
		//! ##### If return control to application is requested then fill with 0xff value
      else if (IOCtrlReqType == IOCtrl_DiagReturnCtrlToApp)
      {
        EcuHwDioDiagCtrlArray[Do12VParked] = (Rte_DT_EcuHwDioCtrlArray_T_0)0xff;        
      }
      else
      {
         // Do nothing  
      }
   } 
   else
   {
       // Do nothing  
   }
   return RTE_E_OK;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DoblsCtrlInterface_P_GetDoblsPinState_CS
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDoblsPinState_CS> of PortPrototype <DoblsCtrlInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *   void Rte_IrvRead_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DoblsCtrlInterface_P_GetDoblsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DoblsCtrlInterface_P_GetDoblsPinState_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DoblsCtrlInterface_P_GetDoblsPinState_CS
//! 
//! \param  *IsDoActivated    For reading DioValue for DOBLS01 Value  
//! \param  *DoPinVoltage     For reading ECUVoltages value for DOBLS01
//! \param  *BatteryVoltage   For reading ECUVoltageValues of powersupply
//! \param  *FaultStatus      For checking Fault status for DOBLS01
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DoblsCtrlInterface_P_GetDoblsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DoblsCtrlInterface_P_GetDoblsPinState_CS (returns application error)
 *********************************************************************************************************************/
   Std_ReturnType               retval           = RTE_E_OK;
   Rte_DT_EcuHwFaultValues_T_0  FaultStat[40]    = {0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,};
   Rte_DT_EcuHwDioCtrlArray_T_0 DioCtrlArray[40] = {0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,
                                                    0,0,0,0,0,0,0,0,0,0,};
   //! ###### Provide the DCDC readback voltage and faultstatus value
   if ((DoPinVoltage != NULL_PTR)
      && (BatteryVoltage != NULL_PTR) 
      && (FaultStatus != NULL_PTR))
   {
      //! ##### Rte call for reading Fault status : 'Rte_IrvRead_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus()'
      Rte_IrvRead_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(&FaultStat[0]);
      //! ##### Rte call for reading DioCtrlArray :'Rte_IrvRead_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuHwDioCtrlArray()'
      Rte_IrvRead_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuHwDioCtrlArray(&DioCtrlArray[0]);
      *DoPinVoltage   = EcuVoltageValues[Dobls_01];
      *BatteryVoltage = EcuVoltageValues[PwrSupply];
      *IsDoActivated  = DioCtrlArray[Dobls_01];
      *FaultStatus    = FaultStat[Dobls_01];
   }
   else 
   {
     retval = RTE_E_INVALID;
   }     
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DoblsCtrlInterface_P_SetDoblsActive_CS
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDoblsActive_CS> of PortPrototype <DoblsCtrlInterface_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DoblsCtrlInterface_P_SetDoblsActive_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DoblsCtrlInterface_P_SetDoblsActive_CS
//! 
//! \param  IOCtrlReqType
//! \param  Activation
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DoblsCtrlInterface_P_SetDoblsActive_CS (returns application error)
 *********************************************************************************************************************/
   const SEWS_PcbConfig_DOWHS_X1CXZ_T *PcbConfig_DowlsInterfaces =   Rte_Prm_X1CXZ_PcbConfig_DOWLS_v();;
   //! ###### This function implements the logic for activaition and deactivation  of DObls module
   //! ##### Reading PCB config
   //! ##### If PCB Configuration is Populated then perform the IOCTL requests
   if (PcbConfig_DowlsInterfaces[0U] == SEWS_PcbConfig_DOWLS_X1CXZ_T_Populated)
   {
      //! #### if IOctrl request us Apprequest then write Activation in EcuHwDioFuncCtrlArray
      if (IOCtrlReqType == IOCtrl_AppRequest)
      {
         EcuHwDioFuncCtrlArray[Dobls_01] = Activation;       
      }
      //! #### if IOctrl request is IOCtrl_DiagShortTermAdjust then write Activation in EcuHwDioDiagCtrlArray
      else if (IOCtrlReqType == IOCtrl_DiagShortTermAdjust)
      {
         EcuHwDioDiagCtrlArray[Dobls_01] = Activation;       
      }
      //! #### if IOctrl request is IOCtrl_DiagReturnCtrlToApp then write 0xff in EcuHwDioDiagCtrlArray
      else if (IOCtrlReqType == IOCtrl_DiagReturnCtrlToApp)
      {
         EcuHwDioDiagCtrlArray[Dobls_01] = (Rte_DT_EcuHwDioCtrlArray_T_0)0xff;       
      }
      else
      {
         //Do nothing 
      }
   }
   else
   {
       //Do nothing 
   }
   return RTE_E_OK;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DowhsInterface_P_GetDoPinStateOne_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDoPinStateOne_CS> of PortPrototype <DowhsInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *   void Rte_IrvRead_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DowhsInterface_P_GetDoPinStateOne_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DowhsInterface_P_GetDoPinStateOne_CS
//! 
//! \param   DoPinRef         Pin reference for DOWHS channel
//! \param  *IsDoActivated    For activaiton status 
//! \param  *DoPinVoltage     For reading  voltage at PWM pin
//! \param  *BatteryVoltage   For reading Current Battery voltage
//! \param  *DutyCycle        For reading the current DutyCycle  
//! \param  *Period           For reading the current Period          
//! \param  *DiagStatus       For checking Diagnostic Status
//!
//! \return  retval           Returns Std_ReturnType 'retVal' value  
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPwmDutycycle, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DutyCycle, P2VAR(VGTT_EcuPwmPeriod, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Period, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DiagStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DowhsInterface_P_GetDoPinStateOne_CS (returns application error)
 *********************************************************************************************************************/
   //! ###### Processing the Dowhs interface pin state
   Std_ReturnType         retval        = RTE_E_OK;
   VGTT_EcuPinFaultStatus FaultStat[40] = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                           0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                           0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                           0U,0U,0U,0U,0U,0U,0U,0U,0U,0U};
   //! ##### Checking the DoPinRef and accordingly updating DoPinVoltage,BatteryVoltage,Period,DutyCycle
   Rte_IrvRead_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(&FaultStat[0]);
   if (1U == DoPinRef)
   {
      *DoPinVoltage   = EcuVoltageValues[Dowhs_01];
      *BatteryVoltage = EcuVoltageValues[PwrSupply];
      //! #### Condition to process Period 
      if(IrvDowhs1_Diag0Requested != 1U)
      {
         *Period = (VGTT_EcuPwmPeriod)Dowhs1MeasuredPWM.Period;
      }
      else
      {
         *Period = 0U;
      }
      //! #### Condition to process DutyCycle
      if (0U == *Period)
      {
         *DutyCycle  = 0U;
      }
      else
      {
         *DutyCycle  = Dowhs1MeasuredPWM.Duty;
      }
      *DiagStatus = FaultStat[Dowhs_01];
   }
   else if (2U == DoPinRef)
   {
      *DoPinVoltage = EcuVoltageValues[Dowhs_02];
      *BatteryVoltage = EcuVoltageValues[PwrSupply];
      //! #### Condition to process Period
      if(IrvDowhs2_Diag0Requested != 1U)
      {
         *Period = (VGTT_EcuPwmPeriod)Dowhs2MeasuredPWM.Period;
      }
      else
      {
         *Period = 0U;
      }
      //! #### Condition to process DutyCycle
      if (0U == *Period)
      {
         *DutyCycle  = 0U;
      }
      else
      {
         *DutyCycle  = Dowhs2MeasuredPWM.Duty;
      }
      *DiagStatus = FaultStat[Dowhs_02];
   }
   else 
   {   
      retval = RTE_E_INVALID;
   }
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DowhsInterface_P_SetDowActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDowActive_CS> of PortPrototype <DowhsInterface_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DowhsInterface_P_SetDowActive_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DowhsInterface_P_SetDowActive_CS
//! 
//! \param  IOCtrlReqType    For checking IOCTRL request
//! \param  OutputId         For PWM pin ID  
//! \param  Period           For setting PWM period     
//! \param  DutyCycle        For setting PWM Dutycycle  
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DowhsInterface_P_SetDowActive_CS (returns application error)
 *********************************************************************************************************************/
   Std_ReturnType retval = RTE_E_OK;
   const SEWS_PcbConfig_DOWHS_X1CXY_T *PcbConfig_DowhsInterfaces =  Rte_Prm_X1CXY_PcbConfig_DOWHS_v();
   //! ###### Processing for the Dowhs  Activation       
   if ((OutputId > 0U) 
      && (OutputId < 3U))
   {
		
      if (PcbConfig_DowhsInterfaces[OutputId-1U] == SEWS_PcbConfig_DOWHS_X1CXY_T_Populated)
      {
			//! ##### Checking Dowhs01 conditions  and sending the PWM activation request
         if (Rte_Prm_P1V6O_Diag_Act_DOWHS01_v() == PCODE_ENABLEPWM)
         {
            retval = Dowhs_PwmActivationRequest(IOCtrlReqType,
																OutputId,
																Period,
																DutyCycle);
         }
			//! #### Else Dio activation request
         else
         {
            retval = Dowhs_DioActivationRequest(IOCtrlReqType,
																OutputId,
																Activation);
         }
			//! ##### Checking Dowhs02 conditions  and sending the activation request
         if (Rte_Prm_P1V6P_Diag_Act_DOWHS02_v() == PCODE_ENABLEPWM)
         {
            retval = Dowhs_PwmActivationRequest(IOCtrlReqType,
																OutputId,
																Period,
																DutyCycle);
         }
			//! #### Else Dio activation request
         else
         {
            retval = Dowhs_DioActivationRequest(IOCtrlReqType,
																OutputId,
																Activation);
         }
      }
      else
      {
         retval = RTE_E_INVALID;
      }
   }
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DowlsInterface_P_GetDoPinStateOne_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDoPinStateOne_CS> of PortPrototype <DowlsInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *   void Rte_IrvRead_DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DowlsInterface_P_GetDoPinStateOne_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DowlsInterface_P_GetDoPinStateOne_CS
//! 
//! \param  DoPinRef          For Dowls Pin reference
//! \param  *IsDoActivated    For activaiton status 
//! \param  *DoPinVoltage     For reading  voltage at PWM pin
//! \param  *BatteryVoltage   For reading Current Battery voltage
//! \param  *DutyCycle        For reading the current DutyCycle 
//! \param  *Period           For reading the current Period
//! \param  *DiagStatus       For reading the Diag status 
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPwmDutycycle, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DutyCycle, P2VAR(VGTT_EcuPwmPeriod, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Period, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DiagStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DowlsInterface_P_GetDoPinStateOne_CS (returns application error)
 *********************************************************************************************************************/
   //! ###### Processing the Dowls interface pin state
   Std_ReturnType retval = RTE_E_OK;
   VGTT_EcuPinFaultStatus FaultStat[40] = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                           0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                           0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                           0U,0U,0U,0U,0U,0U,0U,0U,0U,0U};
   //! ##### Checking the DoPinRef and accordingly updating DoPinVoltage,BatteryVoltage,Period,DutyCycle
   Rte_IrvRead_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(&FaultStat[0]);  
   if (2U == DoPinRef)
   {
      *DoPinVoltage   = EcuVoltageValues[Dowls_02];
      *BatteryVoltage = EcuVoltageValues[PwrSupply];
	  //! #### Condition to process Period 
      if (IrvDowls2_Diag0Requested != 1U)
      {
         *Period = (VGTT_EcuPwmPeriod)Dowls2MeasuredPWM.Period;
      }
      else
      {
         *Period = 0U;
      }
		//! #### Condition to process DutyCycle 
      if (0U == *Period)
      {
         *DutyCycle  = 0U;
      }
      else
      {
         *DutyCycle  = Dowls2MeasuredPWM.Duty;
      }
      *DiagStatus = FaultStat[Dowls_02];
   }
	//! ##### Checking the DoPinRef and accordingly updating DoPinVoltage,BatteryVoltage,Period,DutyCycle
   else if (3U == DoPinRef)
   {
      *DoPinVoltage   = EcuVoltageValues[Dowls_03];
      *BatteryVoltage = EcuVoltageValues[PwrSupply];
		//! #### Condition to process Period 
      if(IrvDowls3_Diag0Requested != 1U)
      {
         *Period = (VGTT_EcuPwmPeriod)Dowls3MeasuredPWM.Period;
      }
      else
      {
         *Period = 0U;
      }
		//! #### Condition to process DutyCycle
      if (0U == *Period)
      {
         *DutyCycle  = 0U;
      }
      else
      {
         *DutyCycle  = Dowls3MeasuredPWM.Duty;
      }
      *DiagStatus = FaultStat[Dowls_03];
   }
   else 
   {   
      retval = RTE_E_INVALID;
   }
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DowlsInterface_P_SetDowActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDowActive_CS> of PortPrototype <DowlsInterface_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DowlsInterface_P_SetDowActive_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DowlsInterface_P_SetDowActive_CS
//! 
//! \param  IOCtrlReqType    For checking IOCTRL request
//! \param  OutputId         For PWM pin ID
//! \param  Period           For setting PWM period
//! \param  DutyCycle        For setting PWM Dutycycle 
//! \param  Activation       For processing Activation    
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DowlsInterface_P_SetDowActive_CS (returns application error)
 ********************************************************************************************************************/
   Std_ReturnType retval = RTE_E_OK;
   const SEWS_PcbConfig_DOWHS_X1CXZ_T *PcbConfig_DowlsInterfaces =  Rte_Prm_X1CXZ_PcbConfig_DOWLS_v();
   //! ###### Processing for the Dowls Activation
   if ((OutputId > 1U) 
      && (OutputId < 4U))
   {
      if (PcbConfig_DowlsInterfaces[OutputId-1U] == SEWS_PcbConfig_DOWLS_X1CXZ_T_Populated)
      {
			//! ##### Checking Dowls02 conditions  and sending the PWM activation request
         if (Rte_Prm_P1V7E_Diag_Act_DOWLS02_v() == PCODE_ENABLEPWM)
         {
            retval = Dowls_PwmActivationRequest(IOCtrlReqType,
                                                OutputId,
                                                Period,
                                                DutyCycle);
         }
			//! #### Else Dio activation request
         else
         {
            retval = Dowls_DioActivationRequest(IOCtrlReqType,
                                                OutputId, 
                                                Activation);
         }
			//! ##### Checking Dowls03 conditions  and sending the PWM activation request
         if (Rte_Prm_P1V7F_Diag_Act_DOWLS03_v() == PCODE_ENABLEPWM)
         {
            retval = Dowls_PwmActivationRequest(IOCtrlReqType,
                                                OutputId, 
                                                Period,
                                                DutyCycle);
         }
			//! #### Else Dio activation request
         else
         {
            retval = Dowls_DioActivationRequest(IOCtrlReqType,
                                                OutputId,
                                                Activation);
         }
      }
      else
      {
         retval = RTE_E_INVALID;
      }         
   }
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: IoHwAb_QM_IO_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_DiagActiveState_P_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T *data)
 *   Std_ReturnType Rte_Read_ScimPvtControl_P_Status(uint8 *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(const Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *   void Rte_IrvWrite_IoHwAb_QM_IO_10ms_runnable_IrvEcuIoQmFaultStatus(const Rte_DT_EcuHwFaultValues_T_0 *data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_EcuHwState_P_GetEcuVoltages_CS(VGTT_EcuPinVoltage_0V2 *EcuVoltageValues)
 *     Argument EcuVoltageValues: VGTT_EcuPinVoltage_0V2* is of type EcuHwVoltageValues_T
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_EcuHwState_I_AdcInFailure
 *   Std_ReturnType Rte_Call_VbatInterface_P_GetVbatVoltage_CS(VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_VbatInterface_I_AdcInFailure, RTE_E_VbatInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: IoHwAb_QM_IO_10ms_runnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the IoHwAb_QM_IO_10ms_runnable
//! 
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, IoHwAb_QM_IO_CODE) IoHwAb_QM_IO_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: IoHwAb_QM_IO_10ms_runnable
 *********************************************************************************************************************/
   //! ##### Function call to : 'IoHwAb_QM_IO_VoltageStabilizer()'
   IoHwAb_QM_IO_VoltageStabilizer();
   //! ##### Function call to : 'IoHwAb_QM_IO_FaultDetection()'
   IoHwAb_QM_IO_FaultDetection();
   //! ##### Function call to : 'IoHwAb_QM_IO_12V_Activation()'
   IoHwAb_QM_IO_12V_Activation();
   //! ##### Function call to : 'IoHwAb_QM_IO_Dowhs_Activation()'
   //IoHwAb_QM_IO_Dowhs_Activation();
   //! ##### Function call to : 'IoHwAb_QM_IO_Dowls_Activation()'
   IoHwAb_QM_IO_Dowls_Activation();
   //! ##### Function call to : 'IoHwAb_QM_IO_ADI_Activation()'
   IoHwAb_QM_IO_ADI_Activation();
   //! ##### Function call to : 'IoHwAb_QM_IO_DAI_Activation()'
   IoHwAb_QM_IO_DAI_Activation();
   //! ##### Function call to : 'IoHwAb_QM_IO_CurrentMonitoring()'
   IoHwAb_QM_IO_CurrentMonitoring();
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define IoHwAb_QM_IO_STOP_SEC_CODE
#include "IoHwAb_QM_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'Dowhs_DioActivationRequest'
//!
//! \param  IOCtrlReqType    For checking IOCTRL request
//! \param  OutputId         For PWM pin ID
//! \param  Activation       For processing Activation 
//!
//! \return  retval          returns uint8 type 'retVal' value
//! 
//!====================================================================================================================
static uint8 Dowhs_DioActivationRequest(IOCtrlReq_T   IOCtrlReqType,
                                        IOHWAB_UINT8  OutputId,
                                        IOHWAB_BOOL   Activation)
{
   uint8 retval    = 0U;
   uint8 ChannelId = 0;
   //! ###### Processing of ChannelId for EcuHwDioFuncCtrlArray as per IOCtrlReqType
   ChannelId = ((uint8)Dowhs_01 + (OutputId-1U));
	//! #### if IOctrl request is Apprequest then write Activation in EcuHwDioFuncCtrlArray
   if (IOCtrlReqType == IOCtrl_AppRequest)
   {
      EcuHwDioFuncCtrlArray[ChannelId] = Activation;    
   }
	//! #### if IOctrl request is IOCtrl_DiagShortTermAdjust then write Activation in EcuHwDioDiagCtrlArray
   else if (IOCtrlReqType == IOCtrl_DiagShortTermAdjust)
   {
      EcuHwDioDiagCtrlArray[ChannelId] = Activation;    
   }
	//! #### if IOctrl request is IOCtrl_DiagReturnCtrlToApp then write 0xff in EcuHwDioDiagCtrlArray
   else if (IOCtrlReqType == IOCtrl_DiagReturnCtrlToApp)
   {
     EcuHwDioDiagCtrlArray[ChannelId] = 0xff;    
   }
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'Dowhs_DioActivationRequest'
//!
//! \param  IOCtrlReqType    For checking IOCTRL request
//! \param  OutputId         For PWM pin ID
//! \param  Activation       For processing Activation 
//!
//! \return  retval          returns uint8 type 'retVal' value
//! 
//!====================================================================================================================
static uint8 Dowls_DioActivationRequest(IOCtrlReq_T  IOCtrlReqType,
                                        IOHWAB_UINT8 OutputId,
                                        IOHWAB_BOOL  Activation)
{
   uint8 retval    = 0U;
   uint8 ChannelId = 0;
   //! ###### Processing of ChannelId for EcuHwDioFuncCtrlArray as per IOCtrlReqType
   ChannelId = ((uint8)Dowls_02 + (OutputId-2U));
	//! #### if IOctrl request is Apprequest then write Activation in EcuHwDioFuncCtrlArray
   if (IOCtrlReqType == IOCtrl_AppRequest)
   {
     EcuHwDioFuncCtrlArray[ChannelId] = Activation;
    
   }
	//! #### if IOctrl request is IOCtrl_DiagShortTermAdjust then write Activation in EcuHwDioDiagCtrlArray
   else if (IOCtrlReqType == IOCtrl_DiagShortTermAdjust)
   {
     EcuHwDioDiagCtrlArray[ChannelId] = Activation;
    
   }
	//! #### if IOctrl request is IOCtrl_DiagReturnCtrlToApp then write 0xff in EcuHwDioDiagCtrlArray
   else if (IOCtrlReqType == IOCtrl_DiagReturnCtrlToApp)
   {
     EcuHwDioDiagCtrlArray[ChannelId] = 0xff;
    
   }
   else
   {
     retval = RTE_E_INVALID;
   }
   return retval;
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'Dowls_PwmActivationRequest'
//!
//! \param  IOCtrlReqType    For checking IOCTRL request
//! \param  OutputId         For PWM pin ID
//! \param  Period           For processing Funstional period
//! \param  DutyCycle        To provide duty cycle 
//!
//! \return  retval          returns uint8 type 'retVal' value
//! 
//!====================================================================================================================
static uint8 Dowls_PwmActivationRequest(IOCtrlReq_T          IOCtrlReqType,
                                        IOHWAB_UINT8         OutputId, 
                                        VGTT_EcuPwmPeriod    Period, 
                                        VGTT_EcuPwmDutycycle DutyCycle)
{
   uint8 retval = 0U;
   
   //! ###### Processing of FuncPeriod for Io_Dowhs as per IOCtrlReqType
   if ((DutyCycle <= (VGTT_EcuPwmDutycycle)0x64U) && (Period <= 2000U))  
   {
      if (IOCtrlReqType == IOCtrl_AppRequest)
      {
         Io_Dowls[OutputId-1].FuncReq = 1U;
         if (Period != 0U)
         {
            Io_Dowls[OutputId-1].FuncPeriod = (uint16)((uint32)PWM_INPUT_FREQUENCY/(uint32)Period);
         }
         else
         {
            Io_Dowls[OutputId-1].FuncPeriod = 0U;
         }
         Io_Dowls[OutputId-1].FuncDuty = (uint16)(((uint32)DutyCycle*(uint32)0x8000U)/(uint32)100U);
      }
      else if (IOCtrlReqType == IOCtrl_DiagShortTermAdjust)
      {
         Io_Dowls[OutputId-1].DiagReq = 1U;
         if (Period != 0U)
         {
            Io_Dowls[OutputId-1].DiagPeriod = (uint16)((uint32)PWM_INPUT_FREQUENCY/(uint32)Period);
         }
         else
         {
            Io_Dowls[OutputId-1].DiagPeriod = 0U;
         }
         Io_Dowls[OutputId-1].DiagDuty = (uint16)(((uint32)DutyCycle*(uint32)0x8000U)/(uint32)100U);
      }
      else if (IOCtrlReqType == IOCtrl_DiagReturnCtrlToApp)
      {
         Io_Dowls[OutputId-1].DiagReq = 0xff;
      }
      else
      {
         retval = RTE_E_INVALID;
      }
   } 
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'Dowhs_PwmActivationRequest'
//!
//! \param  IOCtrlReqType    For checking IOCTRL request
//! \param  OutputId         For PWM pin ID
//! \param  Period           For processing Funstional period
//! \param  DutyCycle        To provide duty cycle 
//!
//! \return  retval          returns uint8 type 'retVal' value
//! 
//!====================================================================================================================
static uint8 Dowhs_PwmActivationRequest(IOCtrlReq_T          IOCtrlReqType, 
                                        IOHWAB_UINT8         OutputId,
                                        VGTT_EcuPwmPeriod    Period,
                                        VGTT_EcuPwmDutycycle DutyCycle)
{
   uint8 retval = 0U;
   //! ###### Processing of FuncPeriod for Io_Dowhs as per IOCtrlReqType
   if ((DutyCycle <= (VGTT_EcuPwmDutycycle)0x64U) && (Period <= 2000U))   
   {
		//! ##### Check whether the request is from application or diagnostic
		//! #### Update the Period, Duty values to the corresponding buffer based on OutputId
      if(IOCtrlReqType == IOCtrl_AppRequest)
      {
         Io_Dowhs[OutputId - 1U].FuncReq = 1U;
         if(Period != 0U)
         {
				Io_Dowhs[OutputId - 1U].FuncPeriod = (uint16)((uint32)PWM_INPUT_FREQUENCY/(uint32)Period);
         }
         else
         {
            Io_Dowhs[OutputId - 1U].FuncPeriod = 0U;
         }
			Io_Dowhs[OutputId - 1U].FuncDuty = (uint16)(((uint32)DutyCycle*(uint32)0x8000U)/(uint32)100U);
      }
      else if(IOCtrlReqType == IOCtrl_DiagShortTermAdjust)
      {
         Io_Dowhs[OutputId - 1U].DiagReq = 1U;
         if(Period != 0U)
         {
				Io_Dowhs[OutputId - 1U].DiagPeriod = (uint16)((uint32)PWM_INPUT_FREQUENCY/(uint32)Period);
         }
         else
         {
            Io_Dowhs[OutputId - 1U].DiagPeriod = 0U;
         }
			Io_Dowhs[OutputId - 1U].DiagDuty = (uint16)(((uint32)DutyCycle*(uint32)0x8000U)/(uint32)100U);
      }
      else if(IOCtrlReqType == IOCtrl_DiagReturnCtrlToApp)
      {
        Io_Dowhs[OutputId - 1U].DiagReq = 0xff;
      }
      else
      {
        retval = RTE_E_INVALID;
      }
   }
   else
   {
      retval = RTE_E_INVALID;
   }   
   return retval;
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_VoltageStabilizer'
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_VoltageStabilizer(void)
{
   static uint8                               ADIPullUpStatusPrev    = 0U;
   static uint8                               ADIPullUpStatus        = 0U;
   static uint8                               DAIPullUpStatusPrev    = 0U;
   static uint8                               DAIPullUpStatus        = 0U;
   static uint8                               ADIPullUpStabilizeTime = 0U;
   static uint8                               DAIPullUpStabilizeTime = 0U;
   static uint8                               DcDcStabilizationTime  = PCODE_DCDCStabilizationTime;
          uint8                               Index                   = 0U;
          SEWS_PcbConfig_DoorAccessIf_X1CX3_T PcbConfig_DaiInterfaces = 0U;
   //! ##### reading PcbConfig_DaiInterfaces  
   PcbConfig_DaiInterfaces = (SEWS_PcbConfig_DoorAccessIf_X1CX3_T) Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
   ADIPullUpStatusPrev     = ADIPullUpStatus;
   DAIPullUpStatusPrev     = DAIPullUpStatus;
   //! ##### reading ADIPullUpStatus and DAIPullUpStatus
   ADIPullUpStatus = Dio_ReadChannel(DioConf_DioChannel_Pullup_CurrentCtrlWetting);
   DAIPullUpStatus = Dio_ReadChannel(DioConf_DioChannel_DAI_PullupCtrl);
   (void)Rte_Call_EcuHwState_P_GetEcuVoltages_CS(&EcuVoltageValues[0]);  
   //! ##### checking for ADIPullUpStabilizeTime to pass
   if (ADIPullUpStabilizeTime > 0U)
   {
      ADIPullUpStabilizeTime--;
   }
   else
   {
      // Do nothing
   }
   if ((ADIPullUpStatus == STD_HIGH) 
      && (ADIPullUpStatusPrev == STD_LOW))
   {
      ADIPullUpStabilizeTime = PCODE_ADIPullUpStabilizationTime;
   }
   else
   {
      // Do nothing
   }
   //! ##### if ADIPullUpStabilizeTime is not passed then assigning vAdiVoltageValues as IO_AdcValue_NotAvialble
   //! ##### if stablization time is passed then assign EcuVoltageValues
   if (ADIPullUpStabilizeTime != 0U)
   {
      for (Index = (uint8)ADI_01;Index <= (uint8)ADI_16;Index++)
      {
         vAdiVoltageValues[Index] = IO_AdcValue_NotAvialble;
      }
   }
   else
   {
      for(Index=(uint8)ADI_01;Index<=(uint8)ADI_16;Index++)
      {
         vAdiVoltageValues[Index] = EcuVoltageValues[Index];
      }
   }
   //DAI Voltage Stability conditions
   //! ##### if DAIPullUpStabilizeTime is not passed then assigning vAdiVoltageValues as IO_AdcValue_NotAvialble
   //! ##### if stablization time is passed then assign EcuVoltageValues
   if (DAIPullUpStabilizeTime > 0U)
   {
      DAIPullUpStabilizeTime--;
   }
   else
   {
      // Do nothing
   }
   if ((DAIPullUpStatus == STD_HIGH) 
      && (DAIPullUpStatusPrev == STD_LOW))
   {
      DAIPullUpStabilizeTime = PCODE_DAIPullUpStabilizationTime;
   }
   else
   {
      // Do nothing
   }
   //! ##### checking PcbConfig_DaiInterfaces is populated with Populated_CapacitiveInterface
   if (CONST_Populated_CapacitiveInterface == CONST_Populated_CapacitiveInterface)
   {
      if ((float64)EcuVoltageValues[Do12VDCDC] > BATVOLTAGE_ADC_RAW_8V)
      {
         if (DcDcStabilizationTime>0)
         {
            DcDcStabilizationTime--;
         } 
         else
         {
            // Do nothing
         }
      }
      else
      {
         DcDcStabilizationTime = PCODE_DCDCStabilizationTime;
      }
      if ((DAIPullUpStabilizeTime != 0U) 
         && (DcDcStabilizationTime != 0U))
      {
         vAdiVoltageValues[DAI_01] = IO_AdcValue_NotAvialble;
         vAdiVoltageValues[DAI_02] = IO_AdcValue_NotAvialble;
      }
      else
      {
         vAdiVoltageValues[DAI_01] = EcuVoltageValues[DAI_01];
         vAdiVoltageValues[DAI_02] = EcuVoltageValues[DAI_02];
      }
   }
   //! ##### checking PcbConfig_DaiInterfaces is populated with Populated_PullUpCircuit
   else if (CONST_Populated_PullUpCircuit == PcbConfig_DaiInterfaces)
   {
      if (ADIPullUpStabilizeTime != 0U)
      {      
         vAdiVoltageValues[DAI_01] = IO_AdcValue_NotAvialble;
         vAdiVoltageValues[DAI_02] = IO_AdcValue_NotAvialble;
      }
      else
      {
         vAdiVoltageValues[DAI_01] = EcuVoltageValues[DAI_01];
         vAdiVoltageValues[DAI_02] = EcuVoltageValues[DAI_02];
      }
   }
   else
   {
      vAdiVoltageValues[DAI_01] = IO_AdcValue_PcbNotPopulated;
      vAdiVoltageValues[DAI_02] = IO_AdcValue_PcbNotPopulated;
   }
} 
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the IoHwAb_QM_IO_FaultDetection
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_FaultDetection(void)
{
   static uint8                                      D012V_ParkedStatus        = 0U;
   static uint8                                      D012V_LivingStatus        = 0U;   
          uint8                                      D012V_DCDCStatus          = 0U;
          uint8                                      D012V_SenStatus           = 0U;
          uint8                                      D012V_SelectOut0Status    = 0U;
          Rte_DT_EcuHwFaultValues_T_0                IrvEcuIoQmFaultStatus[40] = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                                  0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                                  0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                                  0U,0U,0U,0U,0U,0U,0U,0U,0U,0U};
          uint8                                       DowhsFaultStatus         = 0U;    
          uint8                                       Dowhs_SelStatus          = 0U;
          Rte_DT_EcuHwDioCtrlArray_T_0                DioCtrlArray[40]         = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                                  0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                                  0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                                  0U,0U,0U,0U,0U,0U,0U,0U,0U,0U};
          SEWS_HwToleranceThreshold_X1C04_T           HwToleranceThreshold;
  
  
   HwToleranceThreshold   = Rte_Prm_X1C04_HwToleranceThreshold_v();
   D012V_LivingStatus     = Dio_ReadChannel(DioConf_DioChannel_Do12V_OutputLiving);
   D012V_ParkedStatus     = Dio_ReadChannel(DioConf_DioChannel_Do12V_OutputParked);
   D012V_DCDCStatus       = Dio_ReadChannel(DioConf_DioChannel_DCDC12V_Enable);
   DowhsFaultStatus       = Dio_ReadChannel(DioConf_DioChannel_Dowhs_FaultInCh1Ch2);
   D012V_SenStatus        = Dio_ReadChannel(DioConf_DioChannel_Do12V_SEN);
   D012V_SelectOut0Status = Dio_ReadChannel(DioConf_DioChannel_Do12V_SelectOut0);
   Dowhs_SelStatus        = Dio_ReadChannel(DioConf_DioChannel_Dobhs_Select);      
   Rte_IrvRead_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(&DioCtrlArray[0]);            
   
   //! ###### Processing of fault detection for DCDC
   IoHwAb_QM_IO_DcDcFaultDetection(&IrvEcuIoQmFaultStatus[0],
                                  D012V_DCDCStatus,
                                  D012V_LivingStatus,
                                  D012V_ParkedStatus);
   //! ###### Processing of fault detection for 12VLiving Output
   IoHwAb_QM_IO_12VLivingFaultDetection(&IrvEcuIoQmFaultStatus[0],
                                       D012V_LivingStatus,
                                       D012V_SenStatus,
                                       D012V_SelectOut0Status);
   //! ###### Processing of fault detection for 12VParked Output
   IoHwAb_QM_IO_12VParkedFaultDetection(&IrvEcuIoQmFaultStatus[0],
                                       D012V_ParkedStatus,
                                       D012V_SenStatus,
                                       D012V_SelectOut0Status);   
   //! ###### Processing of fault detection for ADI
   IoHwAb_QM_IO_ADI_FaultDetection(&IrvEcuIoQmFaultStatus[0]);
   //! ###### Processing of fault detection for PULLUP
   IoHwAb_QM_IO_Pullup_FaultDetection(&IrvEcuIoQmFaultStatus[0]);
   //! ###### Processing of fault detection for DOBLS1
   IoHwAb_QM_IO_Dobls1_FaultDectection(&IrvEcuIoQmFaultStatus[0]);
   #if 0
   //! ###### Processing of fault detection for DOWHS
   IoHwAb_QM_IO_Dowhs1_FaultDectection(&IrvEcuIoQmFaultStatus[0],
                                       DowhsFaultStatus, 
                                       Dowhs_SelStatus);
   IoHwAb_QM_IO_Dowhs2_FaultDectection(&IrvEcuIoQmFaultStatus[0], 
                                       DowhsFaultStatus, 
                                       Dowhs_SelStatus);
   //! ###### Processing of fault detection for DOWLS2
   IoHwAb_QM_IO_Dowls2_FaultDectection(&IrvEcuIoQmFaultStatus[0]);
   //! ###### Processing of fault detection for DOWLS3
   IoHwAb_QM_IO_Dowls3_FaultDectection(&IrvEcuIoQmFaultStatus[0]);
   #endif
   //Write back the Fault status
   Rte_IrvWrite_IoHwAb_QM_IO_10ms_runnable_IrvEcuIoQmFaultStatus(&IrvEcuIoQmFaultStatus[0]);
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Pullup_FaultDetection'
//!
//! \param  IrvEcuIoQmFaultStatus    For checking Fault request for ECU Irv
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Pullup_FaultDetection(uint8 *IrvEcuIoQmFaultStatus)
{
   uint8 LivingPullupStatus = 0U;
   uint8 ParkedPullupStatus = 0U;
   SEWS_HwToleranceThreshold_X1C04_T HwToleranceThreshold;
   const SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *IoHwAbQmIo_DigiLev=Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();
   //! ###### Processing of fault detection for Living and parked pulllup control
   HwToleranceThreshold = Rte_Prm_X1C04_HwToleranceThreshold_v();     
   if (LivingPullupStatus == STD_HIGH)
   {
      if ((EcuVoltageValues[LivingPullup] > IoHwAbQmIo_DigiLev->DigitalBiLevelLow) 
         && ((float64)EcuVoltageValues[LivingPullup]  < (DCDCVOLTAGE_ADC_RAW_8V - (float64)HwToleranceThreshold)))
      {
         IrvEcuIoQmFaultStatus[LivingPullup] = OnState_FaultDetected_VBT;
      }
      else if (((float64)EcuVoltageValues[LivingPullup] > (DCDCVOLTAGE_ADC_RAW_18V + (float64)HwToleranceThreshold))   
              && (EcuVoltageValues[LivingPullup] < (EcuVoltageValues[PwrSupply] - HwToleranceThreshold)))
      {
         IrvEcuIoQmFaultStatus[LivingPullup] = OnState_FaultDetected_VAT;
      }
      else
      {
         IrvEcuIoQmFaultStatus[LivingPullup] = OnState_NoFaultDetected;
      }
   }
   else
   {
      // do nothing : MISRA
   }
   if (ParkedPullupStatus == STD_HIGH)
   {
      if ((EcuVoltageValues[ParkedPullup] > IoHwAbQmIo_DigiLev->DigitalBiLevelLow) 
         && ((float64)EcuVoltageValues[ParkedPullup]  < (DCDCVOLTAGE_ADC_RAW_8V - (float64)HwToleranceThreshold)))
      {
         IrvEcuIoQmFaultStatus[ParkedPullup] = OnState_FaultDetected_VBT;
      }
      else if (((float64)EcuVoltageValues[ParkedPullup] > (DCDCVOLTAGE_ADC_RAW_18V + (float64)HwToleranceThreshold)) 
              && (EcuVoltageValues[ParkedPullup] < (EcuVoltageValues[PwrSupply] - HwToleranceThreshold)))
      {
         IrvEcuIoQmFaultStatus[ParkedPullup] = OnState_FaultDetected_VAT;
      }
      else
      {
         IrvEcuIoQmFaultStatus[ParkedPullup] = OnState_NoFaultDetected;
      }
   }
   else
   {
      // do nothing : MISRA
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'Dowhs_DioActivationRequest'
//!
//! \param  IrvEcuIoQmFaultStatus    For checking Fault request for ECU Irv
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_ADI_FaultDetection(uint8 *IrvEcuIoQmFaultStatus)
{
   const SEWS_Fault_Config_ADI01_P1V6U_s_T *IoHwAbQmIoThr_ADI01 = Rte_Prm_P1V6U_Fault_Config_ADI01_v();
   const SEWS_Fault_Config_ADI02_P1V6V_s_T *IoHwAbQmIoThr_ADI02 = Rte_Prm_P1V6V_Fault_Config_ADI02_v();
   const SEWS_Fault_Config_ADI03_P1V6W_s_T *IoHwAbQmIoThr_ADI03 = Rte_Prm_P1V6W_Fault_Config_ADI03_v();
   const SEWS_Fault_Config_ADI04_P1V6X_s_T *IoHwAbQmIoThr_ADI04 = Rte_Prm_P1V6X_Fault_Config_ADI04_v();
   const SEWS_Fault_Config_ADI05_P1V6Y_s_T *IoHwAbQmIoThr_ADI05 = Rte_Prm_P1V6Y_Fault_Config_ADI05_v();
   const SEWS_Fault_Config_ADI06_P1V6Z_s_T *IoHwAbQmIoThr_ADI06 = Rte_Prm_P1V6Z_Fault_Config_ADI06_v();
   const SEWS_Fault_Config_ADI07_P1V60_s_T *IoHwAbQmIoThr_ADI07 = Rte_Prm_P1V60_Fault_Config_ADI07_v();
   const SEWS_Fault_Config_ADI08_P1V61_s_T *IoHwAbQmIoThr_ADI08 = Rte_Prm_P1V61_Fault_Config_ADI08_v();
   const SEWS_Fault_Config_ADI09_P1V62_s_T *IoHwAbQmIoThr_ADI09 = Rte_Prm_P1V62_Fault_Config_ADI09_v();
   const SEWS_Fault_Config_ADI10_P1V63_s_T *IoHwAbQmIoThr_ADI10 = Rte_Prm_P1V63_Fault_Config_ADI10_v();
   const SEWS_Fault_Config_ADI11_P1V64_s_T *IoHwAbQmIoThr_ADI11 = Rte_Prm_P1V64_Fault_Config_ADI11_v();
   const SEWS_Fault_Config_ADI12_P1V65_s_T *IoHwAbQmIoThr_ADI12 = Rte_Prm_P1V65_Fault_Config_ADI12_v();
   const SEWS_Fault_Config_ADI13_P1V66_s_T *IoHwAbQmIoThr_ADI13 = Rte_Prm_P1V66_Fault_Config_ADI13_v();
   const SEWS_Fault_Config_ADI14_P1V67_s_T *IoHwAbQmIoThr_ADI14 = Rte_Prm_P1V67_Fault_Config_ADI14_v();
   const SEWS_Fault_Config_ADI15_P1V68_s_T *IoHwAbQmIoThr_ADI15 = Rte_Prm_P1V68_Fault_Config_ADI15_v();
   const SEWS_Fault_Config_ADI16_P1V69_s_T *IoHwAbQmIoThr_ADI16 = Rte_Prm_P1V69_Fault_Config_ADI16_v();
   uint8 Index                                                  = 0U;
   uint8 IoHwAbQmIoAdithr_STB[19]                               = {255U,255U,255U,255U,255U,255U,255U,255U,255U,255U,
                                                                   255U,255U,255U,255U,255U,255U,255U,255U,255U};
   uint8 IoHwAbQmIoAdithr_STG[19]                               = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                   0U,0U,0U,0U,0U,0U,0U,0U,0U};
   //! ##### ADI Fault Detection for STB
   IoHwAbQmIoAdithr_STB[ADI_01] = IoHwAbQmIoThr_ADI01->Threshold_OC_STB; 
   IoHwAbQmIoAdithr_STB[ADI_02] = IoHwAbQmIoThr_ADI02->Threshold_OC_STB;
   IoHwAbQmIoAdithr_STB[ADI_03] = IoHwAbQmIoThr_ADI03->ContactOpen;
   IoHwAbQmIoAdithr_STB[ADI_04] = IoHwAbQmIoThr_ADI04->ContactOpen;
   IoHwAbQmIoAdithr_STB[ADI_05] = IoHwAbQmIoThr_ADI05->ContactOpen;
   IoHwAbQmIoAdithr_STB[ADI_06] = IoHwAbQmIoThr_ADI06->ContactOpen;
   IoHwAbQmIoAdithr_STB[ADI_07] = IoHwAbQmIoThr_ADI07->ContactOpen;
   IoHwAbQmIoAdithr_STB[ADI_08] = IoHwAbQmIoThr_ADI08->ContactOpen;
   IoHwAbQmIoAdithr_STB[ADI_09] = IoHwAbQmIoThr_ADI09->ContactOpen;
   IoHwAbQmIoAdithr_STB[ADI_10] = IoHwAbQmIoThr_ADI10->ContactOpen;
   IoHwAbQmIoAdithr_STB[ADI_11] = IoHwAbQmIoThr_ADI11->ContactOpen;
   IoHwAbQmIoAdithr_STB[ADI_12] = IoHwAbQmIoThr_ADI12->ContactOpen;
   IoHwAbQmIoAdithr_STB[ADI_13] = IoHwAbQmIoThr_ADI13->ContactOpen;
   IoHwAbQmIoAdithr_STB[ADI_14] = IoHwAbQmIoThr_ADI14->ContactOpen;
   IoHwAbQmIoAdithr_STB[ADI_15] = IoHwAbQmIoThr_ADI15->ContactOpen;
   IoHwAbQmIoAdithr_STB[ADI_16] = IoHwAbQmIoThr_ADI16->ContactOpen;
   //! ##### ADI Fault Detection for STG
   IoHwAbQmIoAdithr_STG[ADI_01] = IoHwAbQmIoThr_ADI01->Threshold_STG;
   IoHwAbQmIoAdithr_STG[ADI_02] = IoHwAbQmIoThr_ADI02->Threshold_STG;
   IoHwAbQmIoAdithr_STG[ADI_03] = IoHwAbQmIoThr_ADI03->ContactClosed;
   IoHwAbQmIoAdithr_STG[ADI_04] = IoHwAbQmIoThr_ADI04->ContactClosed;
   IoHwAbQmIoAdithr_STG[ADI_05] = IoHwAbQmIoThr_ADI05->ContactClosed;
   IoHwAbQmIoAdithr_STG[ADI_06] = IoHwAbQmIoThr_ADI06->ContactClosed;
   IoHwAbQmIoAdithr_STG[ADI_07] = IoHwAbQmIoThr_ADI07->ContactClosed;
   IoHwAbQmIoAdithr_STG[ADI_08] = IoHwAbQmIoThr_ADI08->ContactClosed;
   IoHwAbQmIoAdithr_STG[ADI_09] = IoHwAbQmIoThr_ADI09->ContactClosed;
   IoHwAbQmIoAdithr_STG[ADI_10] = IoHwAbQmIoThr_ADI10->ContactClosed;
   IoHwAbQmIoAdithr_STG[ADI_11] = IoHwAbQmIoThr_ADI11->ContactClosed;
   IoHwAbQmIoAdithr_STG[ADI_12] = IoHwAbQmIoThr_ADI12->ContactClosed;
   IoHwAbQmIoAdithr_STG[ADI_13] = IoHwAbQmIoThr_ADI13->ContactClosed;
   IoHwAbQmIoAdithr_STG[ADI_14] = IoHwAbQmIoThr_ADI14->ContactClosed;
   IoHwAbQmIoAdithr_STG[ADI_15] = IoHwAbQmIoThr_ADI15->ContactClosed;
   IoHwAbQmIoAdithr_STG[ADI_16] = IoHwAbQmIoThr_ADI16->ContactClosed;  
   //! ##### Process to update fault status in IrvEcuIoQmFaultStatus buffer
   for (Index=(uint8)ADI_01;Index<=(uint8)ADI_16;Index++)
   {
      if ((((255U == IoHwAbQmIoAdithr_STB[Index])
         || (vAdiVoltageValues[Index]<IoHwAbQmIoAdithr_STB[Index]))
         && ((0U == IoHwAbQmIoAdithr_STG[Index])
         || (vAdiVoltageValues[Index]>IoHwAbQmIoAdithr_STG[Index])))
         || (IO_AdcValue_PcbNotPopulated == vAdiVoltageValues[Index])
         || (IO_AdcValue_NotAvialble == vAdiVoltageValues[Index])
         || (IO_AdcValue_AdcFailure == vAdiVoltageValues[Index]))
      {
         IrvEcuIoQmFaultStatus[Index] = OnState_NoFaultDetected;
      }
      else
      {
         if (vAdiVoltageValues[Index] >= IoHwAbQmIoAdithr_STB[Index])
         {
            IrvEcuIoQmFaultStatus[Index] = OnState_FaultDetected_STB;
         }
         else if (vAdiVoltageValues[Index] <= IoHwAbQmIoAdithr_STG[Index])
         {
            IrvEcuIoQmFaultStatus[Index] = OnState_FaultDetected_STG;
         }
         else
         {
            // do nothing : MISRA
         }
      }
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_DcDcFaultDetection'
//!
//! \param  *IrvEcuIoQmFaultStatus     For checking Fault request for ECU Irv
//! \param  D012V_ParkedStatus         For parked status of 12V
//! \param  D012V_LivingStatus         For Living status of 12V 
//! \param  D012V_DCDCStatus           For DCDC status of 12V
//! 
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_DcDcFaultDetection(uint8  *IrvEcuIoQmFaultStatus,
                                            uint8  D012V_DCDCStatus,
                                            uint8  D012V_LivingStatus,
                                            uint8  D012V_ParkedStatus)
{
   static uint8                          DcDCFaultThresholdPeriod = 0U;
   static uint8                          IrvPrevDCDCFaultstatus   = TestNotRun;
	const SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T *IoHwAbQmIoThr_12V       = Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v();
   
   //! ###### processing of DcDc Fault detection for output driver, Living and parked outputs   
   if ((D012V_DCDCStatus == STD_HIGH) 
      && (D012V_LivingStatus == STD_LOW) 
      && (D012V_ParkedStatus == STD_LOW))
   {
		//! ##### Checking for the conditions and updating the fault status of the respective
      if ((DcDCFaultThresholdPeriod > 0U)
         && ((EcuVoltageValues[Do12VDCDC] < IoHwAbQmIoThr_12V->Threshold_VBT) 
         || (EcuVoltageValues[Do12VDCDC] > IoHwAbQmIoThr_12V->Threshold_VAT)))
      {
         DcDCFaultThresholdPeriod         = 0U;
         IrvEcuIoQmFaultStatus[Do12VDCDC] = OnState_FaultDetected_VOR;
      }
      else if ((D012V_DCDCStatus == STD_HIGH) 
             && ((EcuVoltageValues[Do12VDCDC] > IoHwAbQmIoThr_12V->Threshold_VBT) 
             && (EcuVoltageValues[Do12VDCDC] < IoHwAbQmIoThr_12V->Threshold_VAT)))
      {
         IrvEcuIoQmFaultStatus[Do12VDCDC] = OnState_NoFaultDetected;
      }
      else
      {
         DcDCFaultThresholdPeriod++;
       IrvEcuIoQmFaultStatus[Do12VDCDC] = IrvPrevDCDCFaultstatus;
      }
   }
   else if ((D012V_DCDCStatus == STD_HIGH) 
           && ((EcuVoltageValues[Do12VDCDC] > IoHwAbQmIoThr_12V->Threshold_VBT) 
           && (EcuVoltageValues[Do12VDCDC] < IoHwAbQmIoThr_12V->Threshold_VAT)))
   {
      IrvEcuIoQmFaultStatus[Do12VDCDC] = OnState_NoFaultDetected;
   }
   else
   {
      IrvEcuIoQmFaultStatus[Do12VDCDC] = IrvPrevDCDCFaultstatus;
   }   
   IrvPrevDCDCFaultstatus = IrvEcuIoQmFaultStatus[Do12VDCDC];
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_12VParkedFaultDetection'
//!
//! \param  *IrvEcuIoQmFaultStatus     For checking Fault request for ECU Irv
//! \param  D012V_ParkedStatus         For parked status of 12V
//! \param  D012V_SenStatus            For Sen status of 12V 
//! \param  D012V_SelectOut0Status     For SelectOut0Status of 12V
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_12VParkedFaultDetection(uint8 *IrvEcuIoQmFaultStatus,
                                                 uint8 D012V_ParkedStatus,
                                                 uint8 D012V_SenStatus,
                                                 uint8 D012V_SelectOut0Status)
{
	//! ###### Processing fault detetction for 12v parked
   static uint8                                      D012V_ParkedStatusPrev      = 0U;
   static uint8                                      D012V_ParkedFaultStatPrev   = 0U;
   static uint8                                      Fallback_STG_Parked         = 0U;
   static uint8                                      Fallback_STB_Parked         = 0U;
   static uint8                                      Fallback_CAT_Parked         = 0U;
   static uint8                                      VBT_ParkedThreshold         = 0U;
   static uint8                                      VAT_ParkedThreshold         = 0U;   
   static uint8                                      FaultResetParkedActivated   = 0U;
   const  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *IoHwAbQmIo_DigiLev         = Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();
   static VehicleMode_T                               VehicleModePrev            = 0U;      
   static VehicleMode_T                               VehicleModeCurr            = 0U;
          SEWS_HwToleranceThreshold_X1C04_T           HwToleranceThreshold;
   HwToleranceThreshold   = Rte_Prm_X1C04_HwToleranceThreshold_v();
   D012V_ParkedStatusPrev = D012V_ParkedStatus;
   VehicleModePrev        = VehicleModeCurr;
   Rte_Read_VehicleModeInternal_VehicleMode(&VehicleModeCurr);
   //! ##### checking for the conditions and logging the faults respectively
   // perform transition filtering period logic on status change
   if (D012V_ParkedStatusPrev != D012V_ParkedStatus)
   {
      IrvEcuIoQmFaultStatus[Do12VParked] = D012V_ParkedFaultStatPrev;
   }
   else
   {
      if ((STD_HIGH == D012V_ParkedStatus) 
          && (0U == FaultResetParkedActivated))
      {               
         if(EcuVoltageValues[Do12VParked] < IoHwAbQmIo_DigiLev->DigitalBiLevelLow)
         {
            if (255U > Fallback_STG_Parked)
            {
               Fallback_STG_Parked++;   
            }
            else
            {
                // Do nothing
            }
            VBT_ParkedThreshold = 0U;
         }
			//! #### Checking OnState VBT fault conditions and logging in the fault for D012V_Parked
         else if ((EcuVoltageValues[Do12VParked] > IoHwAbQmIo_DigiLev->DigitalBiLevelLow)
                 && ((float64)EcuVoltageValues[Do12VParked]  < (DCDCVOLTAGE_ADC_RAW_8V - (float64)HwToleranceThreshold)))
         {
            if (VBT_ParkedThreshold > 20)
            {            
               VBT_ParkedThreshold = 0U;
               IrvEcuIoQmFaultStatus[Do12VParked] = OnState_FaultDetected_VBT;
            }
            else
            {
               VBT_ParkedThreshold++;
            }            
         }
			//! #### Checking OnState VAT fault conditions and logging in the fault for D012V_Parked
         else if (((float64)EcuVoltageValues[Do12VParked] > (DCDCVOLTAGE_ADC_RAW_18V + (float64)HwToleranceThreshold))
                 && ((float64)EcuVoltageValues[Do12VParked] <= DCDCVOLTAGE_ADC_RAW_22V))
         {
            if (VAT_ParkedThreshold > 20)
            {            
               VAT_ParkedThreshold = 0U;
               IrvEcuIoQmFaultStatus[Do12VParked] = OnState_FaultDetected_VAT;
            }
            else
            {
               VAT_ParkedThreshold++;
            }            
         }
			//! #### Checking OnState STB fault conditions and logging in the fault for D012V_Parked
         else if ((float64)EcuVoltageValues[Do12VParked] > DCDCVOLTAGE_ADC_RAW_22V)
         {
            IrvEcuIoQmFaultStatus[Do12VParked] = OnState_FaultDetected_STB;
            Fallback_STB_Parked = 1U;
            VAT_ParkedThreshold = 0U;
         }
			//! #### Checking No fault conditions and logging in for D012V_Parked
         else if (((float64)EcuVoltageValues[Do12VParked] > (DCDCVOLTAGE_ADC_RAW_8V + (float64)HwToleranceThreshold))
                 && ((float64)EcuVoltageValues[Do12VParked] < (DCDCVOLTAGE_ADC_RAW_18V - (float64)HwToleranceThreshold)))
         {
            IrvEcuIoQmFaultStatus[Do12VParked] = OnState_NoFaultDetected;
            VBT_ParkedThreshold                = 0U;
            VAT_ParkedThreshold                = 0U;
            Fallback_STB_Parked                = 0U;
            Fallback_STG_Parked                = 0U;
         }
         else
         {
            // do nothing : MISRA
         }
         if (((float64)EcuVoltageValues[Do12VParked] < (DCDCVOLTAGE_ADC_RAW_18V - (float64)HwToleranceThreshold))
            && (D012V_SenStatus == STD_HIGH) 
            && (D012V_SelectOut0Status == STD_HIGH) 
            && ((float64)EcuVoltageValues[Do12V_IPS] > DCDCIPS_ADC_RAW_3_86V))
         {
            IrvEcuIoQmFaultStatus[Do12VParked] = OnState_FaultDetected_CAT;
            Fallback_CAT_Parked++;
         }
         else 
         {
            // do nothing : MISRA
         }
      }
      else
      {
         if ((EcuVoltageValues[Do12VParked] > IoHwAbQmIo_DigiLev->DigitalBiLevelHigh))
         {
            IrvEcuIoQmFaultStatus[Do12VParked] = OffState_FaultDetected_STB;
            Fallback_STB_Parked                = 1U;
         }
         else if (EcuVoltageValues[Do12VParked] < IoHwAbQmIo_DigiLev->DigitalBiLevelLow)
         {
            IrvEcuIoQmFaultStatus[Do12VParked] = OffState_NoFaultDetected;
            Fallback_STB_Parked                = 0U;
         }
         else 
         {
            // do nothing : MISRA
         }
      }
   }   
   //! ###### Processing of fallback mode
   //! ##### If STB, STG and COT faults has been detected perform fallback logic   
   if (1U == FaultResetParkedActivated)
   {
      Dio_WriteChannel( DioConf_DioChannel_Do12V_FaultResetOut, STD_HIGH);
      FaultResetParkedActivated = 0U;
   }
   else
   {
      // do nothing : MISRA
   }
   if (Fallback_STB_Parked > DO12V_FAULT_THRESHOLD) 
   {
      EcuHwDioFuncCtrlArray[Do12VParked] = 0U;
   }
   else
   {
      // do nothing : MISRA
   }
   if ((Fallback_STG_Parked > DO12V_FAULT_THRESHOLD)
      || (Fallback_CAT_Parked > DO12V_FAULT_THRESHOLD))
   {
      if ((VehicleModePrev != VehicleModeCurr)
         &&(VehicleModeCurr == VehicleMode_Living))
      {
         // DoNot change Ctrl Array
         Fallback_CAT_Parked       = 0U;
         Fallback_STG_Parked       = 0U;
         Dio_WriteChannel( DioConf_DioChannel_Do12V_FaultResetOut,
                           STD_LOW);
         FaultResetParkedActivated = 1U;
      }
      else
      {
         if (Fallback_STG_Parked > DO12V_FAULT_THRESHOLD)
         {
            IrvEcuIoQmFaultStatus[Do12VParked] = OnState_FaultDetected_STG;
         }
         else if (Fallback_CAT_Parked > DO12V_FAULT_THRESHOLD)
         {
            IrvEcuIoQmFaultStatus[Do12VParked] = OnState_FaultDetected_CAT;
         }
         else
         {
          // do nothing : MISRA
         }
      }
      EcuHwDioFuncCtrlArray[Do12VParked] = 0U;
      EcuHwDioDiagCtrlArray[Do12VParked] = 255U;
   }
   else
   {
      // do nothing : MISRA
   }
   D012V_ParkedFaultStatPrev = IrvEcuIoQmFaultStatus[Do12VParked];
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_12VLivingFaultDetection'
//!
//! \param  *IrvEcuIoQmFaultStatus     For checking Fault status for ECU Irv
//! \param  D012V_LivingStatus         For Living status of 12V
//! \param  D012V_SenStatus            For Sen status of 12V 
//! \param  D012V_SelectOut0Status     For SelectOut0Status of 12V
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_12VLivingFaultDetection(uint8  *IrvEcuIoQmFaultStatus,
                                                 uint8  D012V_LivingStatus,
                                                 uint8  D012V_SenStatus,
                                                 uint8  D012V_SelectOut0Status)
{ 
   static uint8                                      D012V_LivingStatusPrev           = 0U;
   static uint8                                      D012V_LivingFaultStatPrev        = 0U;
   static uint8                                      Fallback_STG_Living              = 0U;
   static uint8                                      Fallback_STB_Living              = 0U;
   static uint8                                      Fallback_CAT_Living              = 0U;
   static uint8                                      VBT_LivingThreshold              = 0U;
   static uint8                                      VAT_LivingThreshold              = 0U;
   static uint8                                      FaultResetLivingActivated        = 0U;
   const  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *IoHwAbQmIo_DigiLev              = Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();
   static VehicleMode_T                              VehicleModePrev                  = 0U;      
   static VehicleMode_T                              VehicleModeCurr                  = 0U;
          SEWS_HwToleranceThreshold_X1C04_T          HwToleranceThreshold;
   HwToleranceThreshold    = Rte_Prm_X1C04_HwToleranceThreshold_v();
   D012V_LivingStatusPrev  = D012V_LivingStatus;
   VehicleModePrev = VehicleModeCurr;
   Rte_Read_VehicleModeInternal_VehicleMode(&VehicleModeCurr);
   //! ##### Living control fault monitoring logic   
   if (D012V_LivingStatusPrev != D012V_LivingStatus)
   {
      IrvEcuIoQmFaultStatus[Do12VLiving] = D012V_LivingFaultStatPrev;
   }
   else
   {
      if ((STD_HIGH == D012V_LivingStatus) && (0U == FaultResetLivingActivated))
      {
         if(EcuVoltageValues[Do12VLiving] < IoHwAbQmIo_DigiLev->DigitalBiLevelLow)
         {
            if (255U > Fallback_STG_Living)
            {
               Fallback_STG_Living++;
            }
            else
            {
                // Do nothing
            }
            VBT_LivingThreshold = 0U;
         }
         else if ((EcuVoltageValues[Do12VLiving] > IoHwAbQmIo_DigiLev->DigitalBiLevelLow) 
                 && ((float64)EcuVoltageValues[Do12VLiving]  < (DCDCVOLTAGE_ADC_RAW_8V - (float64)HwToleranceThreshold)))
                 
         {
            if (VBT_LivingThreshold > 20)
            {
               VBT_LivingThreshold = 0U;
               IrvEcuIoQmFaultStatus[Do12VLiving] = OnState_FaultDetected_VBT;
            }
            else
            {
               VBT_LivingThreshold++;    
            }            
         }
         else if (((float64)EcuVoltageValues[Do12VLiving] > (DCDCVOLTAGE_ADC_RAW_18V + (float64)HwToleranceThreshold)) 
                 && ((float64)EcuVoltageValues[Do12VLiving] <= DCDCVOLTAGE_ADC_RAW_22V))
         {
            if (VAT_LivingThreshold > 20)
            {
               IrvEcuIoQmFaultStatus[Do12VLiving] = OnState_FaultDetected_VAT;
            }
            else
            {
               VAT_LivingThreshold++;
            }            
         }
         else if ((float64)EcuVoltageValues[Do12VLiving] > DCDCVOLTAGE_ADC_RAW_22V)
         {
            IrvEcuIoQmFaultStatus[Do12VLiving] = OnState_FaultDetected_STB;
            Fallback_STB_Living                = 1U;
            VBT_LivingThreshold                = 0U;
         }
         else if (((float64)EcuVoltageValues[Do12VLiving] > (DCDCVOLTAGE_ADC_RAW_8V + (float64)HwToleranceThreshold))  
                 && ((float64)EcuVoltageValues[Do12VLiving] < (DCDCVOLTAGE_ADC_RAW_18V - (float64)HwToleranceThreshold)))
         {
            IrvEcuIoQmFaultStatus[Do12VLiving] = OnState_NoFaultDetected;
            VBT_LivingThreshold                = 0U;
            VAT_LivingThreshold                = 0U;
            Fallback_STB_Living                = 0U;
            Fallback_STG_Living                = 0U;
         }
         else
         {
            // do nothing : MISRA
         }
         if (((float64)EcuVoltageValues[Do12VLiving] < (DCDCVOLTAGE_ADC_RAW_18V - (float64)HwToleranceThreshold))
            && (D012V_SenStatus == STD_HIGH) 
            && (D012V_SelectOut0Status == STD_HIGH) 
            && ((float64)EcuVoltageValues[Do12V_IPS] > DCDCIPS_ADC_RAW_3_86V))
         {
            IrvEcuIoQmFaultStatus[Do12VLiving] = OnState_FaultDetected_CAT;
            Fallback_CAT_Living++;
         }
         else
         {
            // do nothing : MISRA
         }         
      }
      else
      {
         if ((EcuVoltageValues[Do12VLiving] > IoHwAbQmIo_DigiLev->DigitalBiLevelHigh))
         {
            IrvEcuIoQmFaultStatus[Do12VLiving] = OffState_FaultDetected_STB;
            Fallback_STB_Living = 1U;
         }
         else if(EcuVoltageValues[Do12VLiving] < IoHwAbQmIo_DigiLev->DigitalBiLevelLow)
         {
            IrvEcuIoQmFaultStatus[Do12VLiving] = OffState_NoFaultDetected;
            Fallback_STB_Living = 0U;
         }
         else 
         {
            // do nothing : MISRA
         }
      }
   }
   //! ###### Processing of fallback mode
   //! ##### If STB, STG and COT faults has been detected perform fallback logic 
   if(1U == FaultResetLivingActivated)
   {
      Dio_WriteChannel( DioConf_DioChannel_Do12V_FaultResetOut,
                        STD_HIGH);
      FaultResetLivingActivated = 0U;
   }
   else
   {
      // do nothing : MISRA
   }
   if(Fallback_STB_Living > DO12V_FAULT_THRESHOLD) 
   {
      EcuHwDioFuncCtrlArray[Do12VLiving] = 0U;
   }
   else
   {
      // do nothing : MISRA
   }
   if ((Fallback_STG_Living > DO12V_FAULT_THRESHOLD)
      || (Fallback_CAT_Living > DO12V_FAULT_THRESHOLD))
   {
      if ((VehicleModePrev != VehicleModeCurr)
         &&(VehicleModeCurr == VehicleMode_Living))
      {
         // DoNot change Ctrl Array
         Fallback_STG_Living       = 0U;
         Fallback_CAT_Living       = 0U;
         Dio_WriteChannel( DioConf_DioChannel_Do12V_FaultResetOut,
                           STD_LOW);
         FaultResetLivingActivated = 1U;
      }
      else
      {
         if (Fallback_STG_Living > DO12V_FAULT_THRESHOLD)
         {
            IrvEcuIoQmFaultStatus[Do12VLiving] = OnState_FaultDetected_STG;
         }
         else if (Fallback_CAT_Living > DO12V_FAULT_THRESHOLD)
         {
            IrvEcuIoQmFaultStatus[Do12VLiving] = OnState_FaultDetected_CAT;
         }
         else
         {
            // do nothing : MISRA
         }
      }
      EcuHwDioFuncCtrlArray[Do12VLiving] = 0U;   
      EcuHwDioDiagCtrlArray[Do12VLiving] = 255U;
   }
   else
   {
      // do nothing : MISRA
   }
   D012V_LivingFaultStatPrev = IrvEcuIoQmFaultStatus[Do12VLiving];
}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_12V_Activation'
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_12V_Activation(void)
{
   Fsc_OperationalMode_T        FSCModeCurr      = 0U;
   Rte_DT_EcuHwDioCtrlArray_T_0 DioCtrlArray[40] = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                    0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                    0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                    0U,0U,0U,0U,0U,0U,0U,0U,0U,0U};
   DiagActiveState_T            DiagState        = 0U;
   uint8                        PvtCtrl;
   //! ###### This function implements the logic for the IoHwAb_QM_IO_12V_Activation
   Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(&FSCModeCurr);
   //Rte_IrvRead_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(&DioCtrlArray[0]);
   Rte_Read_DiagActiveState_P_isDiagActive(&DiagState);
   Rte_Read_ScimPvtControl_P_Status(&PvtCtrl);
   Rte_IrvRead_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(&DioCtrlArray[0]);           
   if ((DiagState != TRUE) 
      && (PvtCtrl != 1U))
   {
      EcuHwDioDiagCtrlArray[Do12VLiving] = (Rte_DT_EcuHwDioCtrlArray_T_0)0xff;
      EcuHwDioDiagCtrlArray[Do12VParked] = (Rte_DT_EcuHwDioCtrlArray_T_0)0xff;   
      EcuHwDioDiagCtrlArray[Do12VDCDC]   = (Rte_DT_EcuHwDioCtrlArray_T_0)0xff;         
   }
   else
   {
      // Do nothing
   }
	//! ##### Processing DcDc activation:'IoHwAb_QM_IO_DCDC_Activation()'
   IoHwAb_QM_IO_DCDC_Activation(FSCModeCurr, 
                               &DioCtrlArray[0]);
	//! ##### Processing 12VLiving activation:'IoHwAb_QM_IO_12VLiving_Activation()'
   IoHwAb_QM_IO_12VLiving_Activation(FSCModeCurr,
                                     &DioCtrlArray[0]);  
	//! ##### Processing 12VParked activation:'IoHwAb_QM_IO_12VParked_Activation()'
   IoHwAb_QM_IO_12VParked_Activation(FSCModeCurr,
                                     &DioCtrlArray[0]);      
   //! ##### controlling DIO pins by Dio_WriteChannel:'Dio_WriteChannel()'
   Dio_WriteChannel( DioConf_DioChannel_DCDC12V_Enable ,
                     DioCtrlArray[Do12VDCDC]);
   Dio_WriteChannel( DioConf_DioChannel_Do12V_OutputLiving ,
                     DioCtrlArray[Do12VLiving]);
   Dio_WriteChannel( DioConf_DioChannel_Do12V_OutputParked ,
                   DioCtrlArray[Do12VParked]);      
   //! ##### Write back the DioCtrl array status
   Rte_IrvWrite_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(&DioCtrlArray[0]);
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_DCDC_Activation'
//!
//! \param  FSCModeCurr         FSC operational current value
//! \param  *DioCtrlArray       Read Diag Ctrl pins for ECU hardware 
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_DCDC_Activation(Fsc_OperationalMode_T        FSCModeCurr,
                                         Rte_DT_EcuHwDioCtrlArray_T_0 *DioCtrlArray)
{
   //! ###### Processing of DCDC Activations
   //! ##### Activate the DCDC output based on FSC mode, Battery voltage value and Parked/Living activation status
   if (((FSCModeCurr != FSC_ShutdownReady) 
      && (FSCModeCurr != FSC_Withstand)) 
      && ((float64)EcuVoltageValues[PwrSupply] > PCODE_DcdcStabilityLimit)
      && ((EcuHwDioFuncCtrlArray[Do12VLiving] == 1U) 
      || (EcuHwDioDiagCtrlArray[Do12VLiving] == 1U) 
      || (EcuHwDioFuncCtrlArray[Do12VParked] == 1U) 
      || (EcuHwDioDiagCtrlArray[Do12VParked] == 1U) 
      || (EcuHwDioFuncCtrlArray[DaiActivation] == 1U)
      || (EcuHwDioDiagCtrlArray[DaiActivation] == 1U) 
      || (EcuHwDioFuncCtrlArray[LfChipActivation] == 1U))||(WakeupTimerFunc==WAKEUPTOFLASH))
   {
      EcuHwDioFuncCtrlArray[Do12VDCDC] = 1U;
   }
   else 
   {
      EcuHwDioFuncCtrlArray[Do12VDCDC] = 0U;
   }   
   if (((EcuHwDioDiagCtrlArray[Do12VDCDC] == (uint8)0xff) 
      && (EcuHwDioFuncCtrlArray[Do12VDCDC] == 1U)) 
      || (EcuHwDioDiagCtrlArray[Do12VDCDC] == 1U))
   {
      DioCtrlArray[Do12VDCDC] = 1U;
   }
   else
   {
      
   }
   if (((EcuHwDioDiagCtrlArray[Do12VDCDC] == (uint8)0xff) 
      && (EcuHwDioFuncCtrlArray[Do12VDCDC] == 0U))
      || (EcuHwDioDiagCtrlArray[Do12VDCDC] == 0U))
   {
      DioCtrlArray[Do12VDCDC] = 0U;
   }
   else
   {
      // do nothing : MISRA  
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_12VLiving_Activation'
//!  
//! \param  FSCModeCurr         FSC operational current value
//! \param  *DioCtrlArray       Read Diag Ctrl pins for ECU hardware 
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_12VLiving_Activation(Fsc_OperationalMode_T        FSCModeCurr,
                                              Rte_DT_EcuHwDioCtrlArray_T_0 *DioCtrlArray)
{
   static uint8 DcDcMonitorPeriod_Living = 0U;
   //! ###### Processing of 12V living activation 
   //! ##### Activate the 12V living output based on the FSC mode, ECU voltage values and activation requests.
   if (((FSCModeCurr != FSC_ShutdownReady) 
      && (FSCModeCurr != FSC_Withstand) 
      && ((float64)EcuVoltageValues[Do12VDCDC] > BATVOLTAGE_ADC_RAW_8V) 
      && (EcuHwDioFuncCtrlArray[Do12VLiving] == 1U))
      ||((EcuHwDioFuncCtrlArray[Do12VLiving] == 1U)&&(WakeupTimerFunc==WAKEUPTOFLASH)))
   {
      if (EcuVoltageValues[Do12VDCDC] > EcuVoltageValues[Do12VLiving])         
      {
         DcDcMonitorPeriod_Living++;
         if (DcDcMonitorPeriod_Living > 4U)
         {
            DcDcMonitorPeriod_Living           = 0U;
            EcuHwDioFuncCtrlArray[Do12VLiving] = 1U;
         }
         else
         {
            // Do nothing  
         }
      }
      else
      {
         //No action since threshold not reached
      }
   }
   else 
   {
      EcuHwDioFuncCtrlArray[Do12VLiving] = 0U;
   }      
   if (((EcuHwDioDiagCtrlArray[Do12VLiving] == (uint8)0xff) 
      && (EcuHwDioFuncCtrlArray[Do12VLiving] == 1U)) 
      || ((EcuHwDioDiagCtrlArray[Do12VLiving] == 1U) 
      && ((float64)EcuVoltageValues[Do12VDCDC] > BATVOLTAGE_ADC_RAW_8V)))
   {
      DioCtrlArray[Do12VLiving] = 1U;
   }
   else
   {
      // Do nothing
   }
   if (((EcuHwDioDiagCtrlArray[Do12VLiving] == (uint8)0xff) 
      && (EcuHwDioFuncCtrlArray[Do12VLiving] == 0U)) 
      || ((EcuHwDioDiagCtrlArray[Do12VLiving] == 0U) 
      && ((float64)EcuVoltageValues[Do12VDCDC] > BATVOLTAGE_ADC_RAW_8V)))
   {
      DioCtrlArray[Do12VLiving] = 0U;
   }
   else
   {
      // Do nothing
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_12VParked_Activation'
//!
//! \param  FSCModeCurr         FSC operational current value
//! \param  *DioCtrlArray       Read Diag Ctrl pins for ECU hardware   
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_12VParked_Activation(Fsc_OperationalMode_T         FSCModeCurr,
                                              Rte_DT_EcuHwDioCtrlArray_T_0  *DioCtrlArray)
{
   static uint8 DcDcMonitorPeriod_Parked = 0U;
   //! ###### Processing of 12V Parked activation
   //! ##### Activate the 12V Parked output based on the FSC mode, ECU voltage valels and activation requests.
   if (((FSCModeCurr != FSC_ShutdownReady) 
      && (FSCModeCurr != FSC_Withstand) 
      && ((float64)EcuVoltageValues[Do12VDCDC] > BATVOLTAGE_ADC_RAW_8V) 
      && (EcuHwDioFuncCtrlArray[Do12VParked] == 1U))
      ||((EcuHwDioFuncCtrlArray[Do12VParked] == 1U)&&(WakeupTimerFunc==WAKEUPTOFLASH)))
   {      
      if (EcuVoltageValues[Do12VDCDC] > EcuVoltageValues[Do12VParked])
      {
         ++DcDcMonitorPeriod_Parked ;
         if (DcDcMonitorPeriod_Parked > 4U)
         {
            DcDcMonitorPeriod_Parked           = 0U;
            EcuHwDioFuncCtrlArray[Do12VParked] = 1U;
         }
         else
         {
            // Do nothing
         }
      }
      else
      {   
         //No action since threhold not reached
      }
   }
   else 
   {
      EcuHwDioFuncCtrlArray[Do12VParked] = 0U;
   }
   if (((EcuHwDioDiagCtrlArray[Do12VParked] == (uint8)0xff) 
      && (EcuHwDioFuncCtrlArray[Do12VParked] == 1U))
      || ((EcuHwDioDiagCtrlArray[Do12VParked] == 1U) 
      && ((float64)EcuVoltageValues[Do12VDCDC] > BATVOLTAGE_ADC_RAW_8V)))
   {
      DioCtrlArray[Do12VParked] = 1U;
   }
   else
   {
      // do nothing : MISRA  
   }
   if (((EcuHwDioDiagCtrlArray[Do12VParked] == (uint8)0xff) 
      && (EcuHwDioFuncCtrlArray[Do12VParked] == 0U))
      || ((EcuHwDioDiagCtrlArray[Do12VParked] == 0U) 
      && ((float64)EcuVoltageValues[Do12VDCDC] > BATVOLTAGE_ADC_RAW_8V)))
   {
      DioCtrlArray[Do12VParked] = 0U;
   }
   else
   {
      // do nothing : MISRA
   }
		WakeupTimerFunc=NONE;
}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_CurrentMonitoring'
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_CurrentMonitoring(void)
{
   uint8 D012V_SelectOut0Status = 0U;
   
   //! ###### Perform current monitoring control logic 
   //! ##### reading DIO pins by Dio_WriteChannel:'Dio_WriteChannel()'
   D012V_SelectOut0Status = Dio_ReadChannel(DioConf_DioChannel_Do12V_SelectOut0);
   //! ##### controlling DIO pins by Dio_WriteChannel:'Dio_WriteChannel()'
   Dio_WriteChannel(DioConf_DioChannel_Do12V_SEN, 
                    STD_HIGH);
   //! #### control the DioConf_DioChannel_Do12V_SelectOut0 status according to D012V_SelectOut0Status      
   if (D012V_SelectOut0Status == STD_LOW)
   {
      Dio_WriteChannel(DioConf_DioChannel_Do12V_SelectOut0,
                       STD_HIGH);
   }
   else
   {
      Dio_WriteChannel(DioConf_DioChannel_Do12V_SelectOut0,
                       STD_LOW);
   }         
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dowhs_Activation'
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowhs_Activation(void)
{
   static Fsc_OperationalMode_T         FSCModeCurr       = FSC_NotAvailable;
   static Fsc_OperationalMode_T         FSCModePrev       = FSC_NotAvailable;
          DiagActiveState_T             DiagState         = 0U;
          uint8                         PvtCtrl;
          Rte_DT_EcuHwDioCtrlArray_T_0  DioCtrlArray[40]  = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                             0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                             0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                             0U,0U,0U,0U,0U,0U,0U,0U,0U,0U};
   //! ##### This function implements the logic for the IoHwAb_QM_IO_Dowhs_Activation
   FSCModePrev = FSCModeCurr;
   //! #### Read the DiagState by Rte_Read_DiagActiveState_P_isDiagActive:'Rte_Read_DiagActiveState_P_isDiagActive()'
   Rte_Read_DiagActiveState_P_isDiagActive(&DiagState);
   //! #### Read the Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode :'Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode()'
   Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(&FSCModeCurr);
   //! #### Read the Rte_Read_ScimPvtControl_P_Status :'Rte_Read_ScimPvtControl_P_Status()'
   Rte_Read_ScimPvtControl_P_Status(&PvtCtrl);
   Rte_IrvRead_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(&DioCtrlArray[0]);            
   if ((DiagState != TRUE) 
      && (PvtCtrl != 1U))
   {
      Io_Dowhs[DOWHS1].DiagReq = (uint8)0xff;
      Io_Dowhs[DOWHS2].DiagReq = (uint8)0xff;         
   } 
   else
   {
      // Do nothing
   }
   //! ##### Activate the DOWHS output based on FSC mode and Func/Diag activation request  
   if (FSCModePrev != FSC_NotAvailable)
   {
      if ((FSCModeCurr != FSC_ShutdownReady) 
         && (FSCModeCurr != FSC_Withstand))
      {
         IoHwAb_QM_IO_Dowhs1_Activation(&DioCtrlArray[0]);
         IoHwAb_QM_IO_Dowhs2_Activation(&DioCtrlArray[0]);
      }
      //! #### Disable the DOWHS ouputs if FSC mode doesnt meet requirement
      else
      {
         if (Rte_Prm_P1V6O_Diag_Act_DOWHS01_v() == PCODE_ENABLEPWM)
         {
            Pwm_SetPeriodAndDuty(PwmChannel_DowhsCh1,
                                 0U,
                                 0U);
            Dowhs1_Period = 0U;
            Dowhs1_Duty   = 0U;
         }
         else
         {
            DioCtrlArray[Dowhs_01] = 0;
            Dio_WriteChannel(DioConf_DioChannel_Dowhs_OutputCh1,
                             DioCtrlArray[Dowhs_01]);                           
         }
         if (Rte_Prm_P1V6P_Diag_Act_DOWHS02_v() == PCODE_ENABLEPWM)
         {
            Pwm_SetPeriodAndDuty(PwmChannel_DowhsCh2,
                                 0U,
                                 0U);
            Dowhs2_Period = 0U;
            Dowhs2_Duty   = 0U;
         }
         else
         {
            DioCtrlArray[Dowhs_02] = 0;
            Dio_WriteChannel(DioConf_DioChannel_Dowhs_OutputCh2, 
                             DioCtrlArray[Dowhs_02]);
         }
      }
   }
   else
   {
      //No activation of DOWHS1 and DOWHS2 since FSC mode is not stabilized
   }
   //! ##### Write back the DioCtrl array status
   Rte_IrvWrite_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(&DioCtrlArray[0]);      
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'Dowhs_DioActivationRequest'
//!
//! \param  DioCtrlArray    Read Diag Ctrl pins for ECU hardware 
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowhs1_Activation(uint8 *DioCtrlArray)
{
   //! ###### Address parameter is PWM mode
   if (Rte_Prm_P1V6O_Diag_Act_DOWHS01_v() == PCODE_ENABLEPWM)
   {
      if (Io_Dowhs[DOWHS1].DiagReq == 1U)
      {
         Dowhs1_Period = Io_Dowhs[DOWHS1].DiagPeriod;
         Dowhs1_Duty   = Io_Dowhs[DOWHS1].DiagDuty;
      }
      else if ((Io_Dowhs[DOWHS1].DiagReq == 0xffU) 
           && (Io_Dowhs[DOWHS1].FuncReq == 1U))
      {
         Dowhs1_Period = Io_Dowhs[DOWHS1].FuncPeriod;
         Dowhs1_Duty   = Io_Dowhs[DOWHS1].FuncDuty;
      }            
      else
      {
         //! ##### update pwm signal with default period(2000hz) and duty (10%)
         Dowhs1_Period = 40;
         Dowhs1_Duty   = 3300;
               Io_Dowhs[DOWHS1].FuncPeriod = 2000;
               Io_Dowhs[DOWHS1].FuncDuty = 10;
      }
            if((Dowhs1_PrevPeriod != Dowhs1_Period) || (Dowhs1_PrevDuty != Dowhs1_Duty))
            {
               Pwm_SetPeriodAndDuty(PwmChannel_DowhsCh1, Dowhs1_Period, Dowhs1_Duty);
               Dowhs1_PrevPeriod = Dowhs1_Period;
               Dowhs1_PrevDuty = Dowhs1_Duty;
            }
         }
   //! ###### Address parameter is Continuous mode
   else if (Rte_Prm_P1V6O_Diag_Act_DOWHS01_v() == PCODE_ENABLEDIGITAL)
   {
      if (((EcuHwDioDiagCtrlArray[Dowhs_01] == 0xffU)
         && (EcuHwDioFuncCtrlArray[Dowhs_01] == 1U))
         || (EcuHwDioDiagCtrlArray[Dowhs_01] == 1U))
      {
         DioCtrlArray[Dowhs_01] = 1U;
      }
      if (((EcuHwDioDiagCtrlArray[Dowhs_01] == 0xffU)
         && (EcuHwDioFuncCtrlArray[Dowhs_01] == 0U))
         || (EcuHwDioDiagCtrlArray[Dowhs_01] == 0U))
      {
         DioCtrlArray[Dowhs_01] = 0U;
      }         
      Dio_WriteChannel(DioConf_DioChannel_Dowhs_OutputCh1, 
                       DioCtrlArray[Dowhs_01]);
   }
   else
   {
      //Outputs are deactivated
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dowhs2_Activation'
//!
//! \param  DioCtrlArray    Read Diag Ctrl pins for ECU hardware 
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowhs2_Activation(uint8 *DioCtrlArray)
{
   //! ###### Address parameter is PWM mode
   if (Rte_Prm_P1V6P_Diag_Act_DOWHS02_v() == PCODE_ENABLEPWM)
   {
      if (Io_Dowhs[DOWHS2].DiagReq == 1)
      {
         Dowhs2_Period = Io_Dowhs[DOWHS2].DiagPeriod;
         Dowhs2_Duty   = Io_Dowhs[DOWHS2].DiagDuty;
      }
      else if ((Io_Dowhs[DOWHS2].DiagReq == 0xffU)
              && (Io_Dowhs[DOWHS2].FuncReq == 1U))
      {
         Dowhs2_Period = Io_Dowhs[DOWHS2].FuncPeriod;
         Dowhs2_Duty   = Io_Dowhs[DOWHS2].FuncDuty;
      }
      else
      {
         //! ##### update pwm signal with default period(2000hz) and duty (10%)
		 Dowhs2_Period = 40;
		 Dowhs2_Duty   = 3300;
               Io_Dowhs[DOWHS2].FuncPeriod = 2000;
               Io_Dowhs[DOWHS2].FuncDuty = 10;
      }
            if((Dowhs2_PrevPeriod != Dowhs2_Period) || (Dowhs2_PrevDuty != Dowhs2_Duty))
            {
               Pwm_SetPeriodAndDuty(PwmChannel_DowhsCh2, Dowhs2_Period, Dowhs2_Duty);
               Dowhs2_PrevPeriod = Dowhs2_Period;
               Dowhs2_PrevDuty = Dowhs2_Duty;
            }
         }
   //! ###### Address parameter is Continuous mode
   else if (Rte_Prm_P1V6P_Diag_Act_DOWHS02_v() == PCODE_ENABLEDIGITAL)
   {
      if (((EcuHwDioDiagCtrlArray[Dowhs_02] == 0xffU) 
         && (EcuHwDioFuncCtrlArray[Dowhs_02] == 1U)) 
         || (EcuHwDioDiagCtrlArray[Dowhs_02] == 1U))
      {
         DioCtrlArray[Dowhs_02] = 1U;
      }
      else
      {
         // Do nothing
      }
      if (((EcuHwDioDiagCtrlArray[Dowhs_02] == 0xffU) 
         && (EcuHwDioFuncCtrlArray[Dowhs_02] == 0U)) 
         || (EcuHwDioDiagCtrlArray[Dowhs_02] == 0U))
      {
            DioCtrlArray[Dowhs_02] = 0U;
      }
      else
      {
         // Do nothing
      }
      Dio_WriteChannel(DioConf_DioChannel_Dowhs_OutputCh2,
                       DioCtrlArray[Dowhs_02]);
   }
   else
   {
      //Outputs are deactivated
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_ADI_Activation'
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_ADI_Activation(void)
{
   Fsc_OperationalMode_T                FSCMode                    = FSC_NotAvailable;
   Rte_DT_EcuHwDioCtrlArray_T_0         DioCtrlArray[40]           = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                      0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                      0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                      0U,0U,0U,0U,0U,0U,0U,0U,0U,0U};
   const SEWS_PcbConfig_AdiPullUp_X1CX5_s_T  *PcbConfig_AdiPullup  = Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
   //! ##### Reading Fsc mode Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode:'Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode()'
   Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(&FSCMode);
   //! ##### Reading Dioctrol array:'Rte_IrvRead_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray()'
   Rte_IrvRead_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(&DioCtrlArray[0]);      
   //! ##### Processing of ADI activation 
   //! ##### Activate the ADI pullup control output based on the FSC mode, ECU voltage values and activation requests.
   //if (((Populated == PcbConfig_AdiPullup->AdiPullupParked)
   //  ||(Populated == PcbConfig_AdiPullup->AdiPullupLiving))
   //&&((FSCMode == FSC_Operating)||(FSCMode == FSC_Protecting))) 
   if ((CONST_Populated_PullUpCircuit == PcbConfig_AdiPullup->AdiPullupLiving)
      && ((FSCMode == FSC_Operating)
      || (FSCMode == FSC_Protecting)))
   {
      DioCtrlArray[LivingPullup] = 1U;
   }
   else
   {
      DioCtrlArray[LivingPullup] = 0U;
   }
   //!##### Deactivate the Parked pullup control
   //DioCtrlArray[ParkedPullup] = 0;
   if ((CONST_Populated_PullUpCircuit == PcbConfig_AdiPullup->AdiPullupParked)
      &&((FSCMode == FSC_Operating)||(FSCMode == FSC_Protecting)))
   {
      DioCtrlArray[ParkedPullup] = 1;
   }
   else
   {
      DioCtrlArray[ParkedPullup] = 0;
   }
   //!##### writing Dio pins Dio_WriteChannel :'Dio_WriteChannel()'
   Dio_WriteChannel(DioConf_DioChannel_Pullup_CurrentCtrlWetting ,
                    DioCtrlArray[LivingPullup]);
   Dio_WriteChannel(DioConf_DioChannel_Pullup_CurrentCtrlLowPower,
                    DioCtrlArray[ParkedPullup]);
   //! ##### Write back the DioCtrl array status
   Rte_IrvWrite_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(&DioCtrlArray[0]);
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_DAI_Activation'
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_DAI_Activation(void)
{
   Fsc_OperationalMode_T               FSCMode                  = FSC_NotAvailable;
   Rte_DT_EcuHwDioCtrlArray_T_0        DioCtrlArray[40]         = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                   0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                   0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                                   0U,0U,0U,0U,0U,0U,0U,0U,0U,0U};
   SEWS_PcbConfig_DoorAccessIf_X1CX3_T  PcbConfig_DaiInterfaces =  0U;
   
   PcbConfig_DaiInterfaces = (SEWS_PcbConfig_DoorAccessIf_X1CX3_T) Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
   Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(&FSCMode);
   Rte_IrvRead_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(&DioCtrlArray[0]);   
   //! ###### Processing of DAI activation 
   //! ##### Activate the DAI control output based on the FSC mode, ECU voltage values and activation requests.
   if ((CONST_Populated_CapacitiveInterface == PcbConfig_DaiInterfaces)
      &&((FSCMode == FSC_Operating)||(FSCMode == FSC_Protecting)))
   {
      DioCtrlArray[DaiActivation] = 1U;
   }
   else
   {
      DioCtrlArray[DaiActivation] = 0U;
   }
   //!##### writing Dio pins Dio_WriteChannel :'Dio_WriteChannel()'
   Dio_WriteChannel(DioConf_DioChannel_DAI_PullupCtrl,
                    DioCtrlArray[DaiActivation]);   
   //! ##### Write back the DioCtrl array status
   Rte_IrvWrite_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(&DioCtrlArray[0]);
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dowhs1_FaultDectection'
//! 
//! \param  *IrvEcuIoQmFaultStatus    For registering fault
//! \param   FaultPinStatus           For Fault Pin Status
//! \param   Dowhs_SelStatus          For getting channel select status
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowhs1_FaultDectection(uint8 *IrvEcuIoQmFaultStatus,
                                                uint8 FaultPinStatus, 
                                                uint8 Dowhs_SelStatus)
{
   //! ###### Address parameter is PWM mode
   if (Rte_Prm_P1V6O_Diag_Act_DOWHS01_v() == PCODE_ENABLEPWM)
   {
      IoHwAb_QM_IO_Dowhs1_Pwm_FaultDetection(&IrvEcuIoQmFaultStatus[0],
                                             FaultPinStatus);
   }
   //! ###### Address parameter is Continuous mode
   else if (Rte_Prm_P1V6O_Diag_Act_DOWHS01_v() == PCODE_ENABLEDIGITAL)
   {   
      IoHwAb_QM_IO_Dowhs1_IO_FaultDetection(&IrvEcuIoQmFaultStatus[0],
                                            FaultPinStatus, 
                                            Dowhs_SelStatus);
   }
   else
   {
      //No fault detection
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dowhs1_Pwm_FaultDectection'
//! 
//! \param  *IrvEcuIoQmFaultStatus    For registering fault
//! \param   FaultPinStatus           For Fault Pin Status
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowhs1_Pwm_FaultDetection(uint8 *IrvEcuIoQmFaultStatus,
                                                   uint8 FaultPinStatus)
{
   static PWMFaultDetectionMode_Enum Dowhs1_FDMode = PWMFDMode_WaitInterval;
   static uint8 Dowhs1_MeasureInterval = TIMESTAMP_MEASURE_INTERVAL;
   static uint8 Dowhs1_MeasureTimeout = 0U;
   static uint16  Dowhs1_PeriodPrev     = 0;
   static uint8   Dowhs1_FaultStatPrev  = 0U;
          uint8   FallbackActivate      = 0U;
		  uint16 lDowhs1_Period, lDowhs1_Duty;
   uint32 periodLow, periodHigh, periodDeviation;
   //! ###### Checking Dowls2_Period value to set lDowls2_Period 
	
	// Convert period & dutycycle values
	lDowhs1_Period = Dowhs1_Period;
	if (0U != lDowhs1_Period)
	{
		lDowhs1_Period = (uint16)((uint32)PWM_INPUT_FREQUENCY/(uint32)lDowhs1_Period); // 0 ~ 2000(Hz)
	}
	lDowhs1_Duty = (uint16)(((uint32)Dowhs1_Duty*(uint32)100U)/(uint32)0x8000U); // 0 ~ 100(%)

	// State machine for pwm fault detection
	switch(Dowhs1_FDMode)
	{
	case PWMFDMode_WaitInterval:
		if ((0U != lDowhs1_Period) 
			&& ((0U < lDowhs1_Duty) && (100U > lDowhs1_Duty))
			&& ((Io_Dowhs[DOWHS1].DiagReq==1U) || (Io_Dowhs[DOWHS1].FuncReq==1U)))
		{
			if (Dowhs1_MeasureInterval > 0U) 
			{
				Dowhs1_MeasureInterval--;
			}

			if (0U == Dowhs1_MeasureInterval)
			{
				InitTimestampBuffer(Dowhs1TimestampBuf, BUFSIZE_TIMESTAMP);
				Icu_StartTimestamp(IcuChannel_DOWHS1, Dowhs1TimestampBuf, BUFSIZE_TIMESTAMP, NOTIFICATION_INDEX_TIMESTAMP);
				Icu_EnableNotification(IcuChannel_DOWHS1);

				Dowhs1_MeasureTimeout = TIMESTAMP_MEASURE_TIMEOUT;

				Dowhs1_FDMode = PWMFDMode_WaitResult;
			}
			
			IrvEcuIoQmFaultStatus[Dowhs_01] = Dowhs1_FaultStatPrev;
		}
		else
		{
			Dowhs1MeasuredPWM.Period = 0U;
			Dowhs1MeasuredPWM.Duty = 0U;
			Dowhs1_MeasureInterval = TIMESTAMP_MEASURE_INTERVAL;
			IrvEcuIoQmFaultStatus[Dowhs_01] = OnState_NoFaultDetected;
		}
		break;

	case PWMFDMode_WaitResult:
		// Wait for done or timeout
		if (1U == Dowhs1TimestampDone)
		{
			GetPeriodAndDutyFromTimestamp(IcuChannel_DOWHS1, 
													lDowhs1_Period, 
													(uint8)lDowhs1_Duty, 
													Dowhs1TimestampBuf, 
													BUFSIZE_TIMESTAMP, 
													&Dowhs1MeasuredPWM);

			Dowhs1TimestampDone = 0U;
			Dowhs1_FDMode = PWMFDMode_CheckResult;
		}
		else
		{
			if (Dowhs1_MeasureTimeout > 0U)
			{
				Dowhs1_MeasureTimeout--;
			}
			
			if (0U == Dowhs1_MeasureTimeout) // time-out occurred
			{
				Icu_DisableNotification(IcuChannel_DOWHS1);
				Icu_StopTimestamp(IcuChannel_DOWHS1);
				Dowhs1MeasuredPWM.Period = 0U;
				Dowhs1MeasuredPWM.Duty = 0U;

				Dowhs1TimestampDone = 0U;
				Dowhs1_FDMode = PWMFDMode_CheckResult;
			}
		}

		IrvEcuIoQmFaultStatus[Dowhs_01] = Dowhs1_FaultStatPrev;
		break;

	case PWMFDMode_CheckResult:
		// Check result of PWM measurement
		periodDeviation = (uint32)(((uint32)lDowhs1_Period * (uint32)5U)/(uint32)100U); // 5% deviation
		if (lDowhs1_Period >= periodDeviation)
		{
			periodLow = lDowhs1_Period - periodDeviation;
		}
		else
		{
			periodLow = 0U;
		}
		periodHigh = lDowhs1_Period + periodDeviation;
		
		if ((periodLow <= Dowhs1MeasuredPWM.Period) && (periodHigh >= Dowhs1MeasuredPWM.Period)
			&& (0U != Dowhs1_MeasureTimeout))
		{
			IrvEcuIoQmFaultStatus[Dowhs_01] = OnState_NoFaultDetected;
		}
		else
		{
			IrvEcuIoQmFaultStatus[Dowhs_01] = PCODE_OnState_FaultDetected_PWM; // PWM error
			//FallbackActivate = ON;
		}

		// Go to next measurement
		Dowhs1_MeasureInterval = TIMESTAMP_MEASURE_INTERVAL;
		Dowhs1_FDMode = PWMFDMode_WaitInterval;
		break;

	default:
		// set to initial state
		IrvEcuIoQmFaultStatus[Dowhs_01] = Dowhs1_FaultStatPrev;
		Dowhs1_FDMode = PWMFDMode_WaitInterval;
		break;
	}

	//fallback mode logic to deactivate output when STB,STG fault is detected
	#if 0
	// Temporary removed, requirement needs to be clarified for "Once the fault is removed normal operation shall be resumed"
	if ((FaultPinStatus == STD_LOW) && (FallbackActivate == STD_ON))
	{
		Io_Dowhs[DOWHS1].DiagPeriod = 0U;
		Io_Dowhs[DOWHS1].FuncPeriod = 0U;
	}
	else
	{
		//perform normal operation
	}
	#endif

	Dowhs1_FaultStatPrev = IrvEcuIoQmFaultStatus[Dowhs_01];
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dowhs1_IO_FaultDectection'
//! 
//! \param  *IrvEcuIoQmFaultStatus    For registering fault
//! \param   FaultPinStatus           For Fault Pin Status
//! \param   Dowhs_SelStatus          For getting channel select status
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowhs1_IO_FaultDetection(uint8  *IrvEcuIoQmFaultStatus,
                                                  uint8  FaultPinStatus,
                                                  uint8  Dowhs_SelStatus)
{
   static uint8                                      Dowhs1_StatusPrev      = 0U;
   static uint8                                      Dowhs1_Status          = 0U;
   static uint8                                      Dowhs1_FaultStatPrev   = 0U; 
   static uint8 												  Dowhs1_FaultStat = 0U;	
	static uint8 Dowhs_PrevSelStatus = 0U;
          SEWS_HwToleranceThreshold_X1C04_T          HwToleranceThreshold;
   const  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *IoHwAbQmIo_DigiLev    = Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();
   
   //! ##### reading the hardware tolerance treshold 
	HwToleranceThreshold = Rte_Prm_X1C04_HwToleranceThreshold_v();
   IoHwAbQmIo_DigiLev = (SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *)Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();
	Dowhs1_StatusPrev = Dowhs1_Status;
	Dowhs1_Status = Dio_ReadChannel(DioConf_DioChannel_Dowhs_OutputCh1);
	
	if((Dowhs1_StatusPrev == Dowhs1_Status) 
		&& (Dowhs_SelStatus == Dowhs_PrevSelStatus)
		&& (Dowhs_SelStatus == STD_LOW))
	{
		if((Dowhs1_FaultStat == OnState_FaultDetected_STB) || (Dowhs1_FaultStat == OnState_FaultDetected_STG))
		{
			if((EcuVoltageValues[Dowhs_01] >= DCDCVOLTAGE_ADC_RAW_22V)
				&& (FaultPinStatus == STD_LOW))
			{
				Dowhs1_FaultStat = OnState_FaultDetected_STB;
				EcuHwDioDiagCtrlArray[Dowhs_01] = 0U;
				EcuHwDioFuncCtrlArray[Dowhs_01] = 0U;
			}
			else if((EcuVoltageValues[Dowhs_01] <= IoHwAbQmIo_DigiLev->DigitalBiLevelLow)
				&& (FaultPinStatus == STD_LOW))
			{
				Dowhs1_FaultStat = OnState_FaultDetected_STG;
				EcuHwDioDiagCtrlArray[Dowhs_01] = 0U;
				EcuHwDioFuncCtrlArray[Dowhs_01] = 0U;
			}
			else
			{
				//MISRA C
			}
		}
		else
		{
			//MISRA C
		}
	
		if(Dowhs1_Status == STD_LOW)
		{
			if((FaultPinStatus == STD_LOW) 
				&& (EcuVoltageValues[Dowhs_01] >= DCDCVOLTAGE_ADC_RAW_22V))
			{
				Dowhs1_FaultStat = OnState_FaultDetected_STB;
			}
			else if((EcuVoltageValues[Dowhs_01] > (DCDCVOLTAGE_ADC_RAW_5V + HwToleranceThreshold)))
			{
				Dowhs1_FaultStat = OnState_FaultDetected_VAT;
			}
			else
			{
				Dowhs1_FaultStat = OnState_NoFaultDetected;
			}
		}
		else
		{
			if((EcuVoltageValues[Dowhs_01] <= IoHwAbQmIo_DigiLev->DigitalBiLevelLow) 
				&& (FaultPinStatus == STD_LOW))
			{
				Dowhs1_FaultStat = OnState_FaultDetected_STG;
			}
			else if ((EcuVoltageValues[Dowhs_01] > IoHwAbQmIo_DigiLev->DigitalBiLevelLow) 
					&& (EcuVoltageValues[Dowhs_01]  < (EcuVoltageValues[PwrSupply] - DCDCVOLTAGE_ADC_RAW_2V - HwToleranceThreshold)))
			{
				Dowhs1_FaultStat = OnState_FaultDetected_VBT;
			}
			else if ((EcuVoltageValues[Dowhs_IPS] <= 0)
				&& (Dowhs_SelStatus == STD_LOW))
			{
				Dowhs1_FaultStat = OnState_FaultDetected_OC;
			}
			else
			{
				Dowhs1_FaultStat = OnState_NoFaultDetected;
			}
		}
		Dowhs1_FaultStatPrev = Dowhs1_FaultStat;
		
	}

	Dowhs_PrevSelStatus = Dowhs_SelStatus;
	IrvEcuIoQmFaultStatus[Dowhs_01] = Dowhs1_FaultStat;
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dowhs2_FaultDectection'
//! 
//! \param   *IrvEcuIoQmFaultStatus    For registering fault
//! \param   FaultPinStatus            For Fault Pin Status
//! \param   Dowhs_SelStatus           For getting channel select status
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowhs2_FaultDectection(uint8 *IrvEcuIoQmFaultStatus,
                                                uint8 FaultPinStatus, 
                                                uint8 Dowhs_SelStatus)
{
   //! ###### Address parameter is PWM mode
   if (Rte_Prm_P1V6P_Diag_Act_DOWHS02_v() == PCODE_ENABLEPWM)
   {
      IoHwAb_QM_IO_Dowhs2_Pwm_FaultDetection(&IrvEcuIoQmFaultStatus[0],
                                             FaultPinStatus);
   }
   //! ###### Address parameter is Continuous mode
   else if (Rte_Prm_P1V6P_Diag_Act_DOWHS02_v() == PCODE_ENABLEDIGITAL)
   {
      IoHwAb_QM_IO_Dowhs2_IO_FaultDetection(&IrvEcuIoQmFaultStatus[0],
                                            FaultPinStatus,
                                            Dowhs_SelStatus);
   }
   else
   {
      //No fault detection
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dowhs2_Pwm_FaultDectection'
//! 
//! \param  *IrvEcuIoQmFaultStatus    For registering fault
//! \param   FaultPinStatus           For Fault Pin Status
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowhs2_Pwm_FaultDetection(uint8 *IrvEcuIoQmFaultStatus,
                                                   uint8 FaultPinStatus)
{
	static PWMFaultDetectionMode_Enum Dowhs2_FDMode = PWMFDMode_WaitInterval;
   static uint8 Dowhs2_MeasureInterval = TIMESTAMP_MEASURE_INTERVAL;
   static uint8 Dowhs2_MeasureTimeout = 0U;
   static uint16 Dowhs2_PeriodPrev      = 0U;
   static uint8  Dowhs2_FaultStatPrev   = 0U;
   uint8  FallbackActivate       = 0U;
	uint16 lDowhs2_Period, lDowhs2_Duty;
   uint32 periodLow, periodHigh, periodDeviation;
   //! ###### Updating the Dowhs2_Period value  
   // Convert period & dutycycle values
      lDowhs2_Period = Dowhs2_Period;
      if (0U != lDowhs2_Period)
      {
         lDowhs2_Period = (uint16)((uint32)PWM_INPUT_FREQUENCY/(uint32)lDowhs2_Period); // 0 ~ 2000(Hz)
      }
      lDowhs2_Duty = (uint16)(((uint32)Dowhs2_Duty*(uint32)100U)/(uint32)0x8000U); // 0 ~ 100(%)

      // State machine for pwm fault detection
      switch(Dowhs2_FDMode)
      {
      case PWMFDMode_WaitInterval:
         if ((0U != lDowhs2_Period) 
            && ((0U < lDowhs2_Duty) && (100U > lDowhs2_Duty))
            && ((Io_Dowhs[DOWHS2].DiagReq==1U) || (Io_Dowhs[DOWHS2].FuncReq==1U)))
         {
            if (Dowhs2_MeasureInterval > 0U) 
            {
               Dowhs2_MeasureInterval--;
            }

            if (0U == Dowhs2_MeasureInterval)
            {
               InitTimestampBuffer(Dowhs2TimestampBuf, BUFSIZE_TIMESTAMP);
               Icu_StartTimestamp(IcuChannel_DOWHS2, Dowhs2TimestampBuf, BUFSIZE_TIMESTAMP, NOTIFICATION_INDEX_TIMESTAMP);
               Icu_EnableNotification(IcuChannel_DOWHS2);
               Dowhs2_MeasureTimeout = TIMESTAMP_MEASURE_TIMEOUT;

               Dowhs2_FDMode = PWMFDMode_WaitResult;
            }
            
            IrvEcuIoQmFaultStatus[Dowhs_02] = Dowhs2_FaultStatPrev;
         }
         else
         {
            Dowhs2MeasuredPWM.Period = 0U;
            Dowhs2MeasuredPWM.Duty = 0U;
            Dowhs2_MeasureInterval = TIMESTAMP_MEASURE_INTERVAL;
            IrvEcuIoQmFaultStatus[Dowhs_02] = OnState_NoFaultDetected;
         }
         break;

      case PWMFDMode_WaitResult:
         // Wait for done or timeout
         if (1U == Dowhs2TimestampDone)
         {
            GetPeriodAndDutyFromTimestamp(IcuChannel_DOWHS2, 
                                          lDowhs2_Period, 
                                          (uint8)lDowhs2_Duty, 
                                          Dowhs2TimestampBuf, 
                                          BUFSIZE_TIMESTAMP, 
                                          &Dowhs2MeasuredPWM);
            
            Dowhs2TimestampDone = 0U;
            Dowhs2_FDMode = PWMFDMode_CheckResult;
         }
         else
         {
            if (Dowhs2_MeasureTimeout > 0U)
            {
               Dowhs2_MeasureTimeout--;
            }
            
            if (0U == Dowhs2_MeasureTimeout) // time-out occurred
            {
               Icu_DisableNotification(IcuChannel_DOWHS2);
               Icu_StopTimestamp(IcuChannel_DOWHS2);
               Dowhs2MeasuredPWM.Period = 0U;
               Dowhs2MeasuredPWM.Duty = 0U;

               Dowhs2TimestampDone = 0U;
               Dowhs2_FDMode = PWMFDMode_CheckResult;
            }
         }

         IrvEcuIoQmFaultStatus[Dowhs_02] = Dowhs2_FaultStatPrev;
         break;

      case PWMFDMode_CheckResult:
         // Check result of PWM measurement
         periodDeviation = (uint32)(((uint32)lDowhs2_Period * (uint32)5U)/(uint32)100U); // 5% deviation
         if (lDowhs2_Period >= periodDeviation)
         {
            periodLow = lDowhs2_Period - periodDeviation;
         }
         else
         {
            periodLow = 0U;
         }
         periodHigh = lDowhs2_Period + periodDeviation;
         
         if ((periodLow <= Dowhs2MeasuredPWM.Period) && (periodHigh >= Dowhs2MeasuredPWM.Period)
            && (0U != Dowhs2_MeasureTimeout))
         {
            IrvEcuIoQmFaultStatus[Dowhs_02] = OnState_NoFaultDetected;
         }
         else
         {
            IrvEcuIoQmFaultStatus[Dowhs_02] = PCODE_OnState_FaultDetected_PWM; // PWM error
            //FallbackActivate = STD_ON;
         }

         // Go to next measurement
         Dowhs2_MeasureInterval = TIMESTAMP_MEASURE_INTERVAL;
         Dowhs2_FDMode = PWMFDMode_WaitInterval;
         break;

      default:
         // set to initial state
         IrvEcuIoQmFaultStatus[Dowhs_02] = Dowhs2_FaultStatPrev;
         Dowhs2_FDMode = PWMFDMode_WaitInterval;
         break;
      }

      //fallback mode logic to deactivate output when STB,STG fault is detected
#if 0
      // Temporary removed, requirement needs to be clarified for "Once the fault is removed normal operation shall be resumed"
      if ((FaultPinStatus == STD_LOW) && (FallbackActivate == STD_ON))
      {
         Io_Dowhs[DOWHS2].DiagPeriod = 0U;
         Io_Dowhs[DOWHS2].FuncPeriod = 0U;
      }
      else
      {
         //perform normal operation
      }
#endif

      Dowhs2_FaultStatPrev = IrvEcuIoQmFaultStatus[Dowhs_02];
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dowhs2_IO_FaultDectection'
//! 
//! \param  *IrvEcuIoQmFaultStatus    For registering fault
//! \param  FaultPinStatus            For Fault Pin Status
//! \param  Dowhs_SelStatus           For getting channel select status
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowhs2_IO_FaultDetection(uint8 *IrvEcuIoQmFaultStatus,
                                                  uint8 FaultPinStatus,
                                                  uint8 Dowhs_SelStatus)
{
	static uint8 Dowhs2_StatusPrev = 0U;
   static uint8 Dowhs2_Status = 0U;
	static uint8 Dowhs_PrevSelStatus = 0U;
	static uint8 Dowhs2_FaultStat = 0U;
	static uint8 Dowhs2_FaultStatPrev = 0U;
	SEWS_HwToleranceThreshold_X1C04_T HwToleranceThreshold;
   SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *IoHwAbQmIo_DigiLev;
	
   HwToleranceThreshold = Rte_Prm_X1C04_HwToleranceThreshold_v();
   IoHwAbQmIo_DigiLev = (SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *)Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

	Dowhs2_StatusPrev = Dowhs2_Status;
	Dowhs2_Status = Dio_ReadChannel(DioConf_DioChannel_Dowhs_OutputCh2);

	if((Dowhs2_StatusPrev == Dowhs2_Status) 
		&& (Dowhs_SelStatus == Dowhs_PrevSelStatus)
		&& (Dowhs_SelStatus == STD_HIGH))
	{
		if((Dowhs2_FaultStat == OnState_FaultDetected_STB) || (Dowhs2_FaultStat == OnState_FaultDetected_STG))
		{
			if((EcuVoltageValues[Dowhs_02] >= DCDCVOLTAGE_ADC_RAW_22V)
				&& (FaultPinStatus == STD_LOW))
			{
				Dowhs2_FaultStat = OnState_FaultDetected_STB;
				EcuHwDioDiagCtrlArray[Dowhs_02] = 0U;
				EcuHwDioFuncCtrlArray[Dowhs_02] = 0U;
			}
			else if((EcuVoltageValues[Dowhs_02] <= IoHwAbQmIo_DigiLev->DigitalBiLevelLow)
				&& (FaultPinStatus == STD_LOW))
			{
				Dowhs2_FaultStat = OnState_FaultDetected_STG;
				EcuHwDioDiagCtrlArray[Dowhs_02] = 0U;
				EcuHwDioFuncCtrlArray[Dowhs_02] = 0U;
			}
			else
			{
				//MISRA C
			}
		}
		else
		{
			//MISRA C
		}
		
		if(Dowhs2_Status == STD_LOW)
		{
			if((FaultPinStatus == STD_LOW) 
				&& (EcuVoltageValues[Dowhs_02] >= DCDCVOLTAGE_ADC_RAW_22V))
			{
				Dowhs2_FaultStat = OnState_FaultDetected_STB;
			}
			else if((EcuVoltageValues[Dowhs_02] > (DCDCVOLTAGE_ADC_RAW_5V + HwToleranceThreshold)))
			{
				Dowhs2_FaultStat = OnState_FaultDetected_VAT;
			}
			else
			{
				Dowhs2_FaultStat = OnState_NoFaultDetected;
			}
		}
		else
		{
			if((EcuVoltageValues[Dowhs_02] <= IoHwAbQmIo_DigiLev->DigitalBiLevelLow) 
				&& (FaultPinStatus == STD_LOW))
			{
				Dowhs2_FaultStat = OnState_FaultDetected_STG;
			}
			else if ((EcuVoltageValues[Dowhs_02] > IoHwAbQmIo_DigiLev->DigitalBiLevelLow) 
					&& (EcuVoltageValues[Dowhs_02]  < (EcuVoltageValues[PwrSupply] - DCDCVOLTAGE_ADC_RAW_2V - HwToleranceThreshold)))
			{
				Dowhs2_FaultStat = OnState_FaultDetected_VBT;
			}
			else if ((EcuVoltageValues[Dowhs_IPS] <= 0)
				&& (Dowhs_SelStatus == STD_HIGH))
			{
				Dowhs2_FaultStat = OnState_FaultDetected_OC;
			}
			else
			{
				Dowhs2_FaultStat = OnState_NoFaultDetected;
			}
			Dowhs2_FaultStatPrev = Dowhs2_FaultStat;
		}
	}
	Dowhs_PrevSelStatus = Dowhs_SelStatus;
	IrvEcuIoQmFaultStatus[Dowhs_02] = Dowhs2_FaultStat;
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dowls2_FaultDectection'
//! 
//! \param  *IrvEcuIoQmFaultStatus    For registering fault
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowls2_FaultDectection(uint8 *IrvEcuIoQmFaultStatus)
{
    //! ##### Address parameter is PWM mode
   if (Rte_Prm_P1V7E_Diag_Act_DOWLS02_v() == PCODE_ENABLEPWM)
   {
      IoHwAb_QM_IO_Dowls2_Pwm_FaultDetection(&IrvEcuIoQmFaultStatus[0]);
   }
   //! ##### Address parameter is Continuous mode
   else if (Rte_Prm_P1V7E_Diag_Act_DOWLS02_v() == PCODE_ENABLEDIGITAL)
   {
      IoHwAb_QM_IO_Dowls2_IO_FaultDetection(&IrvEcuIoQmFaultStatus[0]);
   }
   else
   {
      //No fault detection
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dowls2_Pwm_FaultDetection'
//! 
//! \param  *IrvEcuIoQmFaultStatus    For registering fault
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowls2_Pwm_FaultDetection(uint8* IrvEcuIoQmFaultStatus)
{
	static PWMFaultDetectionMode_Enum Dowls2_FDMode = PWMFDMode_WaitInterval;
   static uint8 Dowls2_MeasureInterval = TIMESTAMP_MEASURE_INTERVAL;
   static uint8 Dowls2_MeasureTimeout = 0U;
   static uint8   Dowls2_FaultStatPrev  = 0U;
   uint16  lDowls2_Period        = 0U;
   uint32 periodLow, periodHigh, periodDeviation;
   uint8   lDowls2_Duty          = 0U;
   
   // Convert period & dutycycle values
	lDowls2_Period = Dowls2_Period;
	if (0U != lDowls2_Period)
	{
		lDowls2_Period = (uint16)((uint32)PWM_INPUT_FREQUENCY/(uint32)lDowls2_Period); // 0 ~ 2000(Hz)
	}
	lDowls2_Duty = (uint16)(((uint32)Dowls2_Duty*(uint32)100U)/(uint32)0x8000U); // 0 ~ 100(%)

	// State machine for pwm fault detection
	switch(Dowls2_FDMode)
	{
	case PWMFDMode_WaitInterval:
		if ((0U != lDowls2_Period)
			&& ((0U < lDowls2_Duty) && (100U > lDowls2_Duty))
			&& ((Io_Dowls[DOWLS2].DiagReq==1U) || (Io_Dowls[DOWLS2].FuncReq==1U)))
		{
			if (Dowls2_MeasureInterval > 0U) 
			{
				Dowls2_MeasureInterval--;
			}

			if (0U == Dowls2_MeasureInterval)
			{
				InitTimestampBuffer(Dowls2TimestampBuf, BUFSIZE_TIMESTAMP);
				Icu_StartTimestamp(IcuChannel_DOWLS2, Dowls2TimestampBuf, BUFSIZE_TIMESTAMP, NOTIFICATION_INDEX_TIMESTAMP);
				Icu_EnableNotification(IcuChannel_DOWLS2);

				Dowls2_MeasureTimeout = TIMESTAMP_MEASURE_TIMEOUT;

				Dowls2_FDMode = PWMFDMode_WaitResult;
			}
			
			IrvEcuIoQmFaultStatus[Dowls_02] = Dowls2_FaultStatPrev;
		}
		else
		{
			Dowls2MeasuredPWM.Period = 0U;
			Dowls2MeasuredPWM.Duty = 0U;
			Dowls2_MeasureInterval = TIMESTAMP_MEASURE_INTERVAL;
			IrvEcuIoQmFaultStatus[Dowls_02] = OnState_NoFaultDetected;
		}
		break;

	case PWMFDMode_WaitResult:
		// Wait for done or timeout
		if (1U == Dowls2TimestampDone)
		{
			GetPeriodAndDutyFromTimestamp(IcuChannel_DOWLS2, 
													lDowls2_Period, 
													(uint8)lDowls2_Duty, 
													Dowls2TimestampBuf, 
													BUFSIZE_TIMESTAMP, 
													&Dowls2MeasuredPWM);

			Dowls2TimestampDone = 0U;
			Dowls2_FDMode = PWMFDMode_CheckResult;
		}
		else
		{
			if (Dowls2_MeasureTimeout > 0U)
			{
				Dowls2_MeasureTimeout--;
			}
			
			if (0U == Dowls2_MeasureTimeout) // time-out occurred
			{
				Icu_DisableNotification(IcuChannel_DOWLS2);
				Icu_StopTimestamp(IcuChannel_DOWLS2);
				Dowls2MeasuredPWM.Period = 0U;
				Dowls2MeasuredPWM.Duty = 0U;

				Dowls2TimestampDone = 0U;
				Dowls2_FDMode = PWMFDMode_CheckResult;
			}
		}

		IrvEcuIoQmFaultStatus[Dowls_02] = Dowls2_FaultStatPrev;
		break;

	case PWMFDMode_CheckResult:
		// Check result of PWM measurement
		periodDeviation = (uint32)(((uint32)lDowls2_Period * (uint32)5U)/(uint32)100U); // 5% deviation
		if (lDowls2_Period >= periodDeviation)
		{
			periodLow = lDowls2_Period - periodDeviation;
		}
		else
		{
			periodLow = 0U;
		}
		periodHigh = lDowls2_Period + periodDeviation;
		
		if ((periodLow <= Dowls2MeasuredPWM.Period) && (periodHigh >= Dowls2MeasuredPWM.Period)
			&& (0U != Dowls2_MeasureTimeout))
		{
			IrvEcuIoQmFaultStatus[Dowls_02] = OnState_NoFaultDetected;
		}
		else
		{
			IrvEcuIoQmFaultStatus[Dowls_02] = PCODE_OnState_FaultDetected_PWM; // PWM error
		}

		// Go to next measurement
		Dowls2_MeasureInterval = TIMESTAMP_MEASURE_INTERVAL;
		Dowls2_FDMode = PWMFDMode_WaitInterval;
		break;

	default:
		// set to initial state
		IrvEcuIoQmFaultStatus[Dowls_02] = Dowls2_FaultStatPrev;
		Dowls2_FDMode = PWMFDMode_WaitInterval;
		break;
	}

	Dowls2_FaultStatPrev = IrvEcuIoQmFaultStatus[Dowls_02];
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dowls2_IO_FaultDetection'
//! 
//! \param  *IrvEcuIoQmFaultStatus    For registering fault
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowls2_IO_FaultDetection(uint8 *IrvEcuIoQmFaultStatus)
{
	static uint8 Dowls2_StatusPrev = 0U;
   static uint8 Dowls2_Status = 0U;
	static uint8   Dowls2_FaultStatPrev  = 0U;
	SEWS_HwToleranceThreshold_X1C04_T HwToleranceThreshold;
   SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *IoHwAbQmIo_DigiLev;
	
   HwToleranceThreshold = Rte_Prm_X1C04_HwToleranceThreshold_v();
	IoHwAbQmIo_DigiLev = (SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *)Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

	Dowls2_StatusPrev = Dowls2_Status;
	Dowls2_Status = Dio_ReadChannel(DioConf_DioChannel_Dowls_OutputCh2);
	if((IrvEcuIoQmFaultStatus[Dowls_02] == OnState_FaultDetected_STB) 
		|| (IrvEcuIoQmFaultStatus[Dowls_02] == OnState_FaultDetected_STG))
	{
		if(EcuVoltageValues[Dowls_02] >= DCDCVOLTAGE_ADC_RAW_22V)
		{
			IrvEcuIoQmFaultStatus[Dowls_02] = OnState_FaultDetected_STB;
			EcuHwDioDiagCtrlArray[Dowls_02] = 0U;
			EcuHwDioFuncCtrlArray[Dowls_02] = 0U;
		}
		else if(EcuVoltageValues[Dowls_02] <= IoHwAbQmIo_DigiLev->DigitalBiLevelLow)
		{
			IrvEcuIoQmFaultStatus[Dowls_02] = OnState_FaultDetected_STG;
			EcuHwDioDiagCtrlArray[Dowls_02] = 0U;
			EcuHwDioFuncCtrlArray[Dowls_02] = 0U;
		}
		else
		{
			IrvEcuIoQmFaultStatus[Dowls_02] = OnState_NoFaultDetected;
		}
	}
	else if(Dowls2_StatusPrev != Dowls2_Status)
	{
		IrvEcuIoQmFaultStatus[Dowls_02] = Dowls2_FaultStatPrev;
	}
	else
	{
		if(Dowls2_Status == STD_HIGH)
		{
			if(EcuVoltageValues[Dowls_02] >= DCDCVOLTAGE_ADC_RAW_22V)
			{
				IrvEcuIoQmFaultStatus[Dowls_02] = OnState_FaultDetected_STB;
			}
			else if((EcuVoltageValues[Dowls_02] > (DCDCVOLTAGE_ADC_RAW_5V + HwToleranceThreshold)))
			{
				IrvEcuIoQmFaultStatus[Dowls_02] = OnState_FaultDetected_VAT;
			}
			else
			{
				IrvEcuIoQmFaultStatus[Dowls_02] = OnState_NoFaultDetected;
			}
		}
		else
		{
			if(EcuVoltageValues[Dowls_02] <= IoHwAbQmIo_DigiLev->DigitalBiLevelLow)
			{
				IrvEcuIoQmFaultStatus[Dowls_02] = OnState_FaultDetected_STG;
			}
			else if ((EcuVoltageValues[Dowls_02] > IoHwAbQmIo_DigiLev->DigitalBiLevelLow) 
					&& (EcuVoltageValues[Dowls_02]  < (EcuVoltageValues[PwrSupply] - DCDCVOLTAGE_ADC_RAW_2V - HwToleranceThreshold)))
			{
				IrvEcuIoQmFaultStatus[Dowls_02] = OnState_FaultDetected_VBT;
			}
			else
			{
				IrvEcuIoQmFaultStatus[Dowls_02] = OnState_NoFaultDetected;
			}
		}
		Dowls2_FaultStatPrev = IrvEcuIoQmFaultStatus[Dowls_02];
	}
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dowls3_FaultDectection'
//! 
//! \param  *IrvEcuIoQmFaultStatus    For registering fault
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowls3_FaultDectection(uint8 *IrvEcuIoQmFaultStatus)
{   
   //! ###### Address parameter is PWM mode
   if (Rte_Prm_P1V7F_Diag_Act_DOWLS03_v() == PCODE_ENABLEPWM)
   {
      IoHwAb_QM_IO_Dowls3_Pwm_FaultDetection(&IrvEcuIoQmFaultStatus[0]);
   }   
   //! ###### Address parameter is Continuous mode
   else if (Rte_Prm_P1V7F_Diag_Act_DOWLS03_v() == PCODE_ENABLEDIGITAL)
   {
      IoHwAb_QM_IO_Dowls3_IO_FaultDetection(&IrvEcuIoQmFaultStatus[0]);
   }
   else
   {
      //No fault detection
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dowls3_Pwm_FaultDetection'
//! 
//! \param  *IrvEcuIoQmFaultStatus    For registering fault
//! 
//!====================================================================================================================
static void  IoHwAb_QM_IO_Dowls3_Pwm_FaultDetection(uint8 *IrvEcuIoQmFaultStatus)
{
   static uint8   Dowls3_FaultStatPrev  = 0U;
	static PWMFaultDetectionMode_Enum Dowls3_FDMode = PWMFDMode_WaitInterval;
   static uint8 Dowls3_MeasureInterval = TIMESTAMP_MEASURE_INTERVAL;
   static uint8 Dowls3_MeasureTimeout = 0U;
	uint16 lDowls3_Period, lDowls3_Duty;
	uint32 periodLow, periodHigh, periodDeviation;
   //! ###### Logic to set 'Dowls3_Period' value
   // Convert period & dutycycle values
	lDowls3_Period = Dowls3_Period;
	if (0U != lDowls3_Period)
	{
		lDowls3_Period = (uint16)((uint32)PWM_INPUT_FREQUENCY/(uint32)lDowls3_Period); // 0 ~ 2000(Hz)
	}
	lDowls3_Duty = (uint16)(((uint32)Dowls3_Duty*(uint32)100U)/(uint32)0x8000U); // 0 ~ 100(%)

	// State machine for pwm fault detection
	switch(Dowls3_FDMode)
	{
	case PWMFDMode_WaitInterval:
		if ((0U != lDowls3_Period) 
			&& ((0U < lDowls3_Duty) && (100U > lDowls3_Duty))
			&& ((Io_Dowls[DOWLS3].DiagReq==1U) || (Io_Dowls[DOWLS3].FuncReq==1U)))
		{
			if (Dowls3_MeasureInterval > 0U) 
			{
				Dowls3_MeasureInterval--;
			}

			if (0U == Dowls3_MeasureInterval)
			{
				InitTimestampBuffer(Dowls3TimestampBuf, BUFSIZE_TIMESTAMP);
				Icu_StartTimestamp(IcuChannel_DOWLS3, Dowls3TimestampBuf, BUFSIZE_TIMESTAMP, NOTIFICATION_INDEX_TIMESTAMP);
				Icu_EnableNotification(IcuChannel_DOWLS3);

				Dowls3_MeasureTimeout = TIMESTAMP_MEASURE_TIMEOUT;

				Dowls3_FDMode = PWMFDMode_WaitResult;
			}
			
			IrvEcuIoQmFaultStatus[Dowls_03] = Dowls3_FaultStatPrev;
		}
		else
		{
			Dowls3MeasuredPWM.Period = 0U;
			Dowls3MeasuredPWM.Duty = 0U;
			Dowls3_MeasureInterval = TIMESTAMP_MEASURE_INTERVAL;
			IrvEcuIoQmFaultStatus[Dowls_03] = OnState_NoFaultDetected;
		}
		break;

	case PWMFDMode_WaitResult:
		// Wait for done or timeout
		if (1U == Dowls3TimestampDone)
		{
			GetPeriodAndDutyFromTimestamp(IcuChannel_DOWLS3, 
													lDowls3_Period, 
													(uint8)lDowls3_Duty, 
													Dowls3TimestampBuf, 
													BUFSIZE_TIMESTAMP, 
													&Dowls3MeasuredPWM);

			Dowls3TimestampDone = 0U;
			Dowls3_FDMode = PWMFDMode_CheckResult;
		}
		else
		{
			if (Dowls3_MeasureTimeout > 0U)
			{
				Dowls3_MeasureTimeout--;
			}
			
			if (0U == Dowls3_MeasureTimeout) // time-out occurred
			{
				Icu_DisableNotification(IcuChannel_DOWLS3);
				Icu_StopTimestamp(IcuChannel_DOWLS3);
				Dowls3MeasuredPWM.Period = 0U;
				Dowls3MeasuredPWM.Duty = 0U;

				Dowls3TimestampDone = 0U;
				Dowls3_FDMode = PWMFDMode_CheckResult;
			}
		}

		IrvEcuIoQmFaultStatus[Dowls_03] = Dowls3_FaultStatPrev;
		break;

	case PWMFDMode_CheckResult:
		// Check result of PWM measurement
		periodDeviation = (uint32)(((uint32)lDowls3_Period * (uint32)5U)/(uint32)100U); // 5% deviation
		if (lDowls3_Period >= periodDeviation)
		{
			periodLow = lDowls3_Period - periodDeviation;
		}
		else
		{
			periodLow = 0U;
		}
		periodHigh = lDowls3_Period + periodDeviation;
		
		if ((periodLow <= Dowls3MeasuredPWM.Period) && (periodHigh >= Dowls3MeasuredPWM.Period)
			&& (0U != Dowls3_MeasureTimeout))
		{
			IrvEcuIoQmFaultStatus[Dowls_03] = OnState_NoFaultDetected;
		}
		else
		{
			IrvEcuIoQmFaultStatus[Dowls_03] = PCODE_OnState_FaultDetected_PWM; // PWM error
		}

		// Go to next measurement
		Dowls3_MeasureInterval = TIMESTAMP_MEASURE_INTERVAL;
		Dowls3_FDMode = PWMFDMode_WaitInterval;
		break;

	default:
		// set to initial state
		IrvEcuIoQmFaultStatus[Dowls_03] = Dowls3_FaultStatPrev;
		Dowls3_FDMode = PWMFDMode_WaitInterval;
		break;
	}

	Dowls3_FaultStatPrev = IrvEcuIoQmFaultStatus[Dowls_03];
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dowls3_IO_FaultDetection'
//! 
//! \param  *IrvEcuIoQmFaultStatus    For registering fault
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowls3_IO_FaultDetection(uint8 *IrvEcuIoQmFaultStatus)
{
	static uint8 Dowls3_StatusPrev = 0U;
   static uint8 Dowls3_Status = 0U;
	static uint8   Dowls3_FaultStatPrev  = 0U;
	SEWS_HwToleranceThreshold_X1C04_T HwToleranceThreshold;
   SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *IoHwAbQmIo_DigiLev;
   HwToleranceThreshold = Rte_Prm_X1C04_HwToleranceThreshold_v();
	
	IoHwAbQmIo_DigiLev = (SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *)Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

	Dowls3_StatusPrev = Dowls3_Status;
	Dowls3_Status = Dio_ReadChannel(DioConf_DioChannel_Dowls_OutputCh3);

	if((IrvEcuIoQmFaultStatus[Dowls_03] == OnState_FaultDetected_STB) 
		|| (IrvEcuIoQmFaultStatus[Dowls_03] == OnState_FaultDetected_STG))
	{
		if(EcuVoltageValues[Dowls_03] >= DCDCVOLTAGE_ADC_RAW_22V)
		{
			IrvEcuIoQmFaultStatus[Dowls_03] = OnState_FaultDetected_STB;
			EcuHwDioDiagCtrlArray[Dowls_03] = 0U;
			EcuHwDioFuncCtrlArray[Dowls_03] = 0U;
		}
		else if(EcuVoltageValues[Dowls_03] <= IoHwAbQmIo_DigiLev->DigitalBiLevelLow)
		{
			IrvEcuIoQmFaultStatus[Dowls_03] = OnState_FaultDetected_STG;
			EcuHwDioDiagCtrlArray[Dowls_03] = 0U;
			EcuHwDioFuncCtrlArray[Dowls_03] = 0U;
		}
		else
		{
			IrvEcuIoQmFaultStatus[Dowls_03] = OnState_NoFaultDetected;
		}
	}
	else if(Dowls3_StatusPrev != Dowls3_Status)
	{
		IrvEcuIoQmFaultStatus[Dowls_03] = Dowls3_FaultStatPrev;
	}
	else
	{
		if(Dowls3_Status == STD_HIGH)
		{
			if(EcuVoltageValues[Dowls_03] >= DCDCVOLTAGE_ADC_RAW_22V)
			{
				IrvEcuIoQmFaultStatus[Dowls_03] = OnState_FaultDetected_STB;
			}
			else if((EcuVoltageValues[Dowls_03] > (DCDCVOLTAGE_ADC_RAW_5V + HwToleranceThreshold)))
			{
				IrvEcuIoQmFaultStatus[Dowls_03] = OnState_FaultDetected_VAT;
			}
			else
			{
				IrvEcuIoQmFaultStatus[Dowls_03] = OnState_NoFaultDetected;
			}
		}
		else
		{
			if(EcuVoltageValues[Dowls_03] <= IoHwAbQmIo_DigiLev->DigitalBiLevelLow)
			{
				IrvEcuIoQmFaultStatus[Dowls_03] = OnState_FaultDetected_STG;
			}
			else if ((EcuVoltageValues[Dowls_03] > IoHwAbQmIo_DigiLev->DigitalBiLevelLow) 
					&& (EcuVoltageValues[Dowls_03]  < (EcuVoltageValues[PwrSupply] - DCDCVOLTAGE_ADC_RAW_2V - HwToleranceThreshold)))
			{
				IrvEcuIoQmFaultStatus[Dowls_03] = OnState_FaultDetected_VBT;
			}
			else
			{
				IrvEcuIoQmFaultStatus[Dowls_03] = OnState_NoFaultDetected;
			}
		}
		Dowls3_FaultStatPrev = IrvEcuIoQmFaultStatus[Dowls_03];
	}
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dobls1_FaultDectection'
//! 
//! \param  *IrvEcuIoQmFaultStatus    For registering fault
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dobls1_FaultDectection(uint8 *IrvEcuIoQmFaultStatus)
{
   static uint8                                      Dobls1_StatusPrev      = 0U;
   static uint8                                      Dobls1_Status          = 0U;
   static uint8                                      Dobls1_FaultStatPrev   = 0U; 
          SEWS_HwToleranceThreshold_X1C04_T          HwToleranceThreshold;
   const  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *IoHwAbQmIo_DigiLev    = Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();
   
   //! ##### reading the hardware tolerance treshold 
   HwToleranceThreshold = Rte_Prm_X1C04_HwToleranceThreshold_v();   
   Dobls1_StatusPrev    = Dobls1_Status;
   //! ##### Reading Dobls1 pin status :'Dio_ReadChannel()'
   Dobls1_Status        = Dio_ReadChannel(DioConf_DioChannel_Dobls_OutputCh1);
   //! ###### Checking if fault detected
   if ((IrvEcuIoQmFaultStatus[Dobls_01] == OnState_FaultDetected_STB) 
      || (IrvEcuIoQmFaultStatus[Dobls_01] == OnState_FaultDetected_STG))
   {
      //! ##### Figuring out fault detected us STG / STB
      if ((float64)EcuVoltageValues[Dobls_01] >= DCDCVOLTAGE_ADC_RAW_22V)
      {
         IrvEcuIoQmFaultStatus[Dobls_01] = OnState_FaultDetected_STB;
         EcuHwDioDiagCtrlArray[Dobls_01] = 0U;
         EcuHwDioFuncCtrlArray[Dobls_01] = 0U;
      }
      else if (EcuVoltageValues[Dobls_01] <= IoHwAbQmIo_DigiLev->DigitalBiLevelLow)
      {
         IrvEcuIoQmFaultStatus[Dobls_01] = OnState_FaultDetected_STG;
         EcuHwDioDiagCtrlArray[Dobls_01] = 0U;
         EcuHwDioFuncCtrlArray[Dobls_01] = 0U;
      }
      else
      {
         // Do nothing
      }
   }
   //! ###### if previous Dobls fault status and current dobls fault status is not same
   //! ##### writing Dobls1_FaultStatPrev to IrvEcuIoQmFaultStatus array
   else if (Dobls1_StatusPrev != Dobls1_Status)
   {
      IrvEcuIoQmFaultStatus[Dobls_01] = Dobls1_FaultStatPrev;
   }
   else
   {
      //! ##### If Dobls1 status is HIGH then finding STB fault is there or VAT fault is there 
      if (Dobls1_Status == STD_HIGH)
      {
         //! #### if EcuVoltageValues are greater than equal to DCDCVOLTAGE_ADC_RAW_22V then STB fault occured
         if ((float64)EcuVoltageValues[Dobls_01] >= DCDCVOLTAGE_ADC_RAW_22V)
         {
            IrvEcuIoQmFaultStatus[Dobls_01] = OnState_FaultDetected_STB;
         }
         //! #### if EcuVoltageValues are greater than DCDCVOLTAGE_ADC_RAW_5V + HwToleranceThreshold then VAT fault occured
         else if (((float64)EcuVoltageValues[Dobls_01] > (DCDCVOLTAGE_ADC_RAW_5V + (float64)HwToleranceThreshold)))
         {
            IrvEcuIoQmFaultStatus[Dobls_01] = OnState_FaultDetected_VAT;
         }
         else
         {
            IrvEcuIoQmFaultStatus[Dobls_01] = OnState_NoFaultDetected;
         }
      }
      //! ##### if EcuVoltageValues Dobls01 is STD_LOW
      else
      {
         //! #### EcuVoltageValues[Dobls_01] is less than equal to DigitalBiLevelLow then STG fault occured
         if (EcuVoltageValues[Dobls_01] <= IoHwAbQmIo_DigiLev->DigitalBiLevelLow)
         {
            IrvEcuIoQmFaultStatus[Dobls_01] = OnState_FaultDetected_STG;
         }
         //! #### EcuVoltageValues[Dobls_01] is grater than DigitalBiLevelLow and less than
         //! #### DCDCVOLTAGE_ADC_RAW_24V - DCDCVOLTAGE_ADC_RAW_5V - HwToleranceThreshold then STG fault occured
         else if ((EcuVoltageValues[Dobls_01] > IoHwAbQmIo_DigiLev->DigitalBiLevelLow)
                 && ((float64)EcuVoltageValues[Dobls_01]  < (DCDCVOLTAGE_ADC_RAW_24V - DCDCVOLTAGE_ADC_RAW_5V - (float64)HwToleranceThreshold)))
         {
            IrvEcuIoQmFaultStatus[Dobls_01] = OnState_FaultDetected_VBT;
         }
         else
         {
            IrvEcuIoQmFaultStatus[Dobls_01] = OnState_NoFaultDetected;
         }
      }
      Dobls1_FaultStatPrev = IrvEcuIoQmFaultStatus[Dobls_01];
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dowls_Activation'
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowls_Activation(void)
{
   static Fsc_OperationalMode_T        FSCModeCurr      = FSC_NotAvailable;
   static Fsc_OperationalMode_T        FSCModePrev      = FSC_NotAvailable;
          DiagActiveState_T            DiagState        = 0U;
          Rte_DT_EcuHwDioCtrlArray_T_0 DioCtrlArray[40] = {0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                           0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                           0U,0U,0U,0U,0U,0U,0U,0U,0U,0U,
                                                           0U,0U,0U,0U,0U,0U,0U,0U,0U,0U};
          uint8                        PvtCtrl;
   FSCModePrev = FSCModeCurr;
   //! ##### Activate the DOWHS output based on FSC mode and Func/Diag activation request
   // Check the FSC mode and perform (func/diag)activation filtering 
   Rte_Read_DiagActiveState_P_isDiagActive(&DiagState);
   Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(&FSCModeCurr);
   Rte_Read_ScimPvtControl_P_Status(&PvtCtrl);
   Rte_IrvRead_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(&DioCtrlArray[0]);   
   //! #####  Checking DiagState state and Pvt Ctrl to process Diag request
   if ((DiagState != TRUE)
      && (PvtCtrl != 1U))
   {
      Io_Dowls[DOWLS1].DiagReq = 0xffu;
      Io_Dowls[DOWLS2].DiagReq = 0xffU;
      Io_Dowls[DOWLS3].DiagReq = 0xffU;   
   }
   else
   {
      //do nothing: MISRA
   }
   //! ##### Checking FSCMode status    
   if (FSCModePrev != FSC_NotAvailable)
   {
      if ((FSCModeCurr != FSC_ShutdownReady) 
         && (FSCModeCurr != FSC_Withstand))
      {
         IoHwAb_QM_IO_Dobls1_Activation(&DioCtrlArray[0]);
         IoHwAb_QM_IO_Dowls2_Activation(&DioCtrlArray[0]);
         IoHwAb_QM_IO_Dowls3_Activation(&DioCtrlArray[0]);
      }
      //! #### Disable the DOWLS ouputs if FSC mode doesnt meet requirement
      else
      {
         Dio_WriteChannel(  DioConf_DioChannel_Dobls_OutputCh1 , STD_LOW);
         #if 0
         if (Rte_Prm_P1V7E_Diag_Act_DOWLS02_v() == PCODE_ENABLEPWM)
         {
            Pwm_SetPeriodAndDuty(PwmChannel_DowlsCh2, 0, 0);
         }
         else if (Rte_Prm_P1V7E_Diag_Act_DOWLS02_v() == PCODE_ENABLEDIGITAL)
         {
            Dio_WriteChannel(DioConf_DioChannel_Dowls_OutputCh2, STD_LOW);
         }
         else
         {
            //outpus deactivated
         }
         if (Rte_Prm_P1V7F_Diag_Act_DOWLS03_v() == PCODE_ENABLEPWM)
         {
            Pwm_SetPeriodAndDuty(PwmChannel_DowlsCh3, 0, 0);
         }
         else if (Rte_Prm_P1V7F_Diag_Act_DOWLS03_v() == PCODE_ENABLEDIGITAL)
         {
            Dio_WriteChannel(DioConf_DioChannel_Dowls_OutputCh3, STD_LOW);
         }
         else
         {
            //outputs deactivated
         }
         #endif
      }
   }
   else
   {
      //No activation of DOWLS2 and DOWLS3 since FSC mode is not stabilized
   }
   //! ##### Write back the DioCtrl array status
   Rte_IrvWrite_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(&DioCtrlArray[0]);
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dobls1_Activation'
//!
//! \param  *DioCtrlArray    Read Diag Ctrl pins for ECU hardware 
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dobls1_Activation(uint8* DioCtrlArray)
{
   //! ##### Checking  Dobls_01 pin for EcuHwDioDiagCtrlArray and updating
   if (((EcuHwDioDiagCtrlArray[Dobls_01] == (Rte_DT_EcuHwDioCtrlArray_T_0)0xff) 
      && (EcuHwDioFuncCtrlArray[Dobls_01] == (Rte_DT_EcuHwDioCtrlArray_T_0)1U)) 
      || (EcuHwDioDiagCtrlArray[Dobls_01] == (Rte_DT_EcuHwDioCtrlArray_T_0)1U))
   {
      DioCtrlArray[Dobls_01] = 1U;
   }
   else
   {
      // do nothing : MISRA
   }
   if (((EcuHwDioDiagCtrlArray[Dobls_01] == 0xffU) 
      && (EcuHwDioFuncCtrlArray[Dobls_01] == 0U)) 
      || (EcuHwDioDiagCtrlArray[Dobls_01] == 0U))
   {
      DioCtrlArray[Dobls_01] = 0U;
   }
   else
   {
      // do nothing : MISRA
   }
   //! ##### Updating Diognostic channel with DioConf_DioChannel_Dobls_OutputCh1 and Dobls_o1 values
   Dio_WriteChannel(DioConf_DioChannel_Dobls_OutputCh1,
                    DioCtrlArray[Dobls_01]);
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the IoHwAb_QM_IO_Dowls3_Activation
//!
//! \param  *DioCtrlArray    Read Diag Ctrl pins for ECU hardware 
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowls3_Activation(uint8 *DioCtrlArray)
{
   //! ##### Process to update Diag period and Diag duty for 'DOWLS3' as per Diag request without any fault
   if (Rte_Prm_P1V7F_Diag_Act_DOWLS03_v() == PCODE_ENABLEPWM)
   {   
      if ((Io_Dowls[DOWLS3].DiagReq == 1U)
          && (Dowls3_Faultdetected == 0U))
      {
         Dowls3_Period = Io_Dowls[DOWLS3].DiagPeriod;
         Dowls3_Duty   = Io_Dowls[DOWLS3].DiagDuty;
      }
      else if ((Io_Dowls[DOWLS3].DiagReq == (uint8)0xff) 
              && (Io_Dowls[DOWLS3].FuncReq == 1U)
              && (Dowls3_Faultdetected == 0U))
      {
         Dowls3_Period = Io_Dowls[DOWLS3].FuncPeriod;
         Dowls3_Duty   = Io_Dowls[DOWLS3].FuncDuty;
      }
      else
      {
         //! #### if no function or diagnostic request are there then 
         //! #### update pwm signal with default period(2000hz) and duty (10%)
         // update pwm signal with default period(2000hz) and duty (10%)
         Dowls3_Period               = 40;
         Dowls3_Duty                 = 3300;
               Io_Dowls[DOWLS3].FuncPeriod = 2000;
               Io_Dowls[DOWLS3].FuncDuty = 10;
      }
            if((Dowls3_PrevPeriod != Dowls3_Period) || (Dowls3_PrevDuty != Dowls3_Duty))
            {
               Pwm_SetPeriodAndDuty(PwmChannel_DowlsCh3, Dowls3_Period, Dowls3_Duty);
               Dowls3_PrevPeriod = Dowls3_Period;
               Dowls3_PrevDuty = Dowls3_Duty;
            }
         }
   else if (Rte_Prm_P1V7F_Diag_Act_DOWLS03_v() == PCODE_ENABLEDIGITAL)
   {
      if (((EcuHwDioDiagCtrlArray[Dowls_03] == 0xffU)
         && (EcuHwDioFuncCtrlArray[Dowls_03] == 1U))  
         || (EcuHwDioDiagCtrlArray[Dowls_03] == 1U))
      {
         DioCtrlArray[Dowls_03] = 1U;
      }
      else
      {
         // Do nothing
      }
      if (((EcuHwDioDiagCtrlArray[Dowls_03] == 0xffU)
         && (EcuHwDioFuncCtrlArray[Dowls_03] == 0U)) 
         || (EcuHwDioDiagCtrlArray[Dowls_03] == 0U))
      {
         DioCtrlArray[Dowls_03] = 0U;
      }
      else
      {
         // Do noting
      }
      Dio_WriteChannel(DioConf_DioChannel_Dowls_OutputCh3,
                       DioCtrlArray[Dowls_03]);
   }
   else
   {
      //Outputs are deactivated
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_QM_IO_Dowls2_Activation'
//!
//! \param  *DioCtrlArray    Read Diag Ctrl pins for ECU hardware 
//! 
//!====================================================================================================================
static void IoHwAb_QM_IO_Dowls2_Activation(uint8 *DioCtrlArray)
{
   //! ###### Process to check for Dowls2 Activation
	//update Diag period and Diag duty  for 'DOWLS2' as per Diag request without any fault
	//! ##### Checking if the parameter has request to enable PWM
   if (Rte_Prm_P1V7E_Diag_Act_DOWLS02_v() == PCODE_ENABLEPWM)
   {
		//! #### Check for the Diagnostic request
      if (Io_Dowls[DOWLS2].DiagReq == 1)
		{
			Dowls2_Period = Io_Dowls[DOWLS2].DiagPeriod;
			Dowls2_Duty = Io_Dowls[DOWLS2].DiagDuty;
		}
		else if ((Io_Dowls[DOWLS2].DiagReq == 0xff) && (Io_Dowls[DOWLS2].FuncReq == 1))
		{
			Dowls2_Period = Io_Dowls[DOWLS2].FuncPeriod;
			Dowls2_Duty = Io_Dowls[DOWLS2].FuncDuty;
		}
		else
		{
			// update pwm signal with default period(2000hz) and duty (10%)
			Dowls2_Period = 40;
			Dowls2_Duty = 3300;
			Io_Dowls[DOWLS2].FuncPeriod = 2000;
			Io_Dowls[DOWLS2].FuncDuty = 10;
		}

		if((Dowls2_PrevPeriod != Dowls2_Period) || (Dowls2_PrevDuty != Dowls2_Duty))
		{
			Pwm_SetPeriodAndDuty(PwmChannel_DowlsCh2, Dowls2_Period, Dowls2_Duty);
			Dowls2_PrevPeriod = Dowls2_Period;
			Dowls2_PrevDuty = Dowls2_Duty;
		}
   }
	//! ##### Else if the parameter has request to enable Digital
   else if (Rte_Prm_P1V7E_Diag_Act_DOWLS02_v() == PCODE_ENABLEDIGITAL)
   {
		//! #### If it is in either functional or diagnostic state then set DIO buffer for Dowls2 
      if(((EcuHwDioDiagCtrlArray[Dowls_02] == 0xffU)
        && (EcuHwDioFuncCtrlArray[Dowls_02] == 1U)) 
        ||(EcuHwDioDiagCtrlArray[Dowls_02] == 1U))
      {
         DioCtrlArray[Dowls_02] = 1U;
      }
      else
      {
         // Do nothing
      }
		//! #### //! #### If it is neither in functional or diagnostic state then clear DIO buffer for Dowls2
      if(((EcuHwDioDiagCtrlArray[Dowls_02] == 0xffU) 
        && (EcuHwDioFuncCtrlArray[Dowls_02] == 0U))
        ||(EcuHwDioDiagCtrlArray[Dowls_02] == 0U))
      {
         DioCtrlArray[Dowls_02] = 0U;
      }
      else 
      {
         // Do nothing
      }
      Dio_WriteChannel(DioConf_DioChannel_Dowls_OutputCh2,
                       DioCtrlArray[Dowls_02]);
   }
   else
   {
      //Outputs are deactivated
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'AdiInterface_P_SetPullupLiving_Activate'
//!
//! \param   *PcbConfig_AdiPullup       For checking  PcbConfig_AdiPullup interface  is populated or not 
//! \param   IOCtrlReqType              For checking IOCTRL request
//! \param   ActivateStrongPullUp       Getting Strong Pullup request 
//!
//! \return  retval                     Std_ReturnType 'retval' value 
//! 
//!====================================================================================================================
static Std_ReturnType AdiInterface_P_SetPullupLiving_Activate(const SEWS_PcbConfig_AdiPullUp_X1CX5_s_T *PcbConfig_AdiPullup,
                                                                    IOCtrlReq_T                        IOCtrlReqType,
                                                                    IOHWAB_BOOL                        ActivateStrongPullUp)
{
   Std_ReturnType retval = RTE_E_OK;
   
   //! ##### If adi pullup living is populated and  ActivateStrongPullUp is not equal to 0xffU
   //! #### update EcuHwDioFuncCtrlArray according to IoCtrl request
   if ((PcbConfig_AdiPullup->AdiPullupLiving == CONST_Populated_PullUpCircuit)
      &&(0xffU!=ActivateStrongPullUp))
   {
      if (IOCtrlReqType == IOCtrl_AppRequest)
      {
       //  EcuHwDioFuncCtrlArray[LivingPullup] = ActivateStrongPullUp;
      }
      else if (IOCtrlReqType == IOCtrl_DiagShortTermAdjust)
      {
       //  EcuHwDioDiagCtrlArray[LivingPullup] = ActivateStrongPullUp;
      }
      else if (IOCtrlReqType == IOCtrl_DiagReturnCtrlToApp)
      {
       //  EcuHwDioDiagCtrlArray[LivingPullup] = 0xff;
      }
      else
      {
         retval = RTE_E_INVALID;   
      }
   }
   //! ##### AdiPullupLiving NotPopulated then return RTE_E_INVALID
   else if (PcbConfig_AdiPullup->AdiPullupLiving == CONST_NotPopulated)
   {
      retval = RTE_E_INVALID;   
   }
   else
   {
      // do nothing
   }
   return retval;
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'AdiInterface_P_SetPullupParked_Activate'
//!
//! \param   *PcbConfig_AdiPullup       For checking  PcbConfig_AdiPullup interface  is populated or not 
//! \param   IOCtrlReqType              For checking IOCTRL request
//! \param   ActivateWeakPullUp         Getting Weak Pullup request 
//!
//! \return  retval                     Std_ReturnType 'retval' value 
//! 
//!====================================================================================================================
static Std_ReturnType AdiInterface_P_SetPullupParked_Activate(const SEWS_PcbConfig_AdiPullUp_X1CX5_s_T *PcbConfig_AdiPullup,
                                                                    IOCtrlReq_T                        IOCtrlReqType,
                                                                    IOHWAB_BOOL                        ActivateWeakPullUp)
{
   Std_ReturnType retval = RTE_E_OK;
   //! ##### If adi pullup living is populated and  ActivateWeakPullUp is not equal to 0xffU
   //! #### update EcuHwDioFuncCtrlArray according to IoCtrl request
   if ((PcbConfig_AdiPullup->AdiPullupParked == CONST_Populated_PullUpCircuit)
      && (0xffU != ActivateWeakPullUp))
   {
      if (IOCtrlReqType == IOCtrl_AppRequest)
      {
         // EcuHwDioFuncCtrlArray[ParkedPullup] = ActivateWeakPullUp;
      }
      else if (IOCtrlReqType == IOCtrl_DiagShortTermAdjust)
      {
         // EcuHwDioDiagCtrlArray[ParkedPullup] = ActivateWeakPullUp;
      }
      else if (IOCtrlReqType == IOCtrl_DiagReturnCtrlToApp)
      {
         // EcuHwDioDiagCtrlArray[ParkedPullup] = 0xff;
      }
      else
      {
         retval = RTE_E_INVALID;   
      }
   }
   //! ##### AdiPullupParked NotPopulated then return RTE_E_INVALID
   else if (PcbConfig_AdiPullup->AdiPullupParked == CONST_NotPopulated)
   {
      retval = RTE_E_INVALID;   
   }
   else
   {
      // do nothing
   }
   return retval;
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'AdiInterface_P_SetPullupDAI_Activate'
//!
//! \param   PcbConfig_DaiInterfaces    For checking  PcbConfig_DaiInterfaces is populated or not 
//! \param   IOCtrlReqType              For checking IOCTRL request
//! \param   ActivateDAIPullUp          Getting DAI Pullup request 
//!
//! \return  retval                     Std_ReturnType 'retval' value 
//! 
//!====================================================================================================================
static Std_ReturnType AdiInterface_P_SetPullupDAI_Activate(SEWS_PcbConfig_DoorAccessIf_X1CX3_T PcbConfig_DaiInterfaces,
                                                           IOCtrlReq_T                         IOCtrlReqType,
                                                           IOHWAB_BOOL                         ActivateDAIPullUp)
{
   Std_ReturnType retval = RTE_E_OK;
   
   //! ##### If PcbConfig_DaiInterfaces is Populated_PullUpCircuit or Populated_CapacitiveInterface and  ActivateDAIPullUp is not equal to 0xffU
   //! #### update EcuHwDioFuncCtrlArray according to IoCtrl request
   if (((PcbConfig_DaiInterfaces == CONST_Populated_PullUpCircuit)
      || (PcbConfig_DaiInterfaces == CONST_Populated_CapacitiveInterface))
      && (0xffU != ActivateDAIPullUp))
   {
      if (IOCtrlReqType == IOCtrl_AppRequest)
      {
         // EcuHwDioFuncCtrlArray[DaiActivation] = ActivateDAIPullUp;    
      }
      else if (IOCtrlReqType == IOCtrl_DiagShortTermAdjust)
      { 
         // EcuHwDioDiagCtrlArray[DaiActivation] = ActivateDAIPullUp;
      }
      else if (IOCtrlReqType == IOCtrl_DiagReturnCtrlToApp)
      {
         // EcuHwDioDiagCtrlArray[DaiActivation] = 0xff;
      }
      else
      {
         retval = RTE_E_INVALID;   
      }
   }
   //! ##### PcbConfig_DaiInterfaces NotPopulated then return RTE_E_INVALID
   else if (PcbConfig_DaiInterfaces == CONST_NotPopulated)
   {
      retval = RTE_E_INVALID;   
   }
   else
   {
      // do nothing
   }
   return retval;
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'Dowhs1_OverflowNotification'
//! 
//!====================================================================================================================
void Dowhs1_OverflowNotification(void)
{
   Dowhs1_OverFlowDetected = 1;
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'Dowhs2_OverflowNotification'
//! 
//!====================================================================================================================
void Dowhs2_OverflowNotification(void)
{
   Dowhs2_OverFlowDetected = 1;
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'Dowls2_OverflowNotification'
//! 
//!====================================================================================================================
void Dowls2_OverflowNotification(void)
{
   Dowls2_OverFlowDetected = 1;
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'Dowls3_OverflowNotification'
//! 
//!====================================================================================================================
void Dowls3_OverflowNotification(void)
{
   Dowls3_OverFlowDetected = 1;
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'Dowls2_Notification'
//! 
//!====================================================================================================================
void Dowls2_Notification(void)
{
   //Pwm_SetPeriodAndDuty(PwmChannel_DowlsCh2,Dowls2_Period, Dowls2_Duty);
}

void IoHwAb_QM_IO_IcuNotification_Dowhs1(void)
{
   Dowhs1TimestampDone = 1U;
}

void IoHwAb_QM_IO_IcuNotification_Dowhs2(void)
{
   Dowhs2TimestampDone = 1U;
}

void IoHwAb_QM_IO_IcuNotification_Dowls2(void)
{
   Dowls2TimestampDone = 1U;
}

void IoHwAb_QM_IO_IcuNotification_Dowls3(void)
{
   Dowls3TimestampDone = 1U;
}

static void InitTimestampBuffer(uint32 *timestampBuf, uint8 bufSize)
{
   uint8 i;

   for (i=0U; i<bufSize; i++)
   {
      timestampBuf[i] = 0U;
   }
}

static void GetPeriodAndDutyFromTimestamp(Icu_ChannelType ch, uint16 refPeriod, uint8 refDuty, uint32 *timestampBuf, uint8 bufSize, DowxsMeasuredValue *MeasuredValue)
{
   uint8 i;
   uint8 divCnt = 0U;
   uint32 diff1, diff2, sum;
   uint32 sumPeriod, sumDuty;
   sint16 dutyDiff1, dutyDiff2;

   // overflow handling
   for (i=1U; i<bufSize; i++)
   {
      if (timestampBuf[i-1] > timestampBuf[i]) // in case of overflow
      {
         timestampBuf[i] = 0xFFFF + timestampBuf[i];
      }
   }

   // get period and duty
   sumPeriod = 0U;
   sumDuty = 0U;
   i = 0U;
   for (;;)
   {
      diff1 = timestampBuf[i+1] - timestampBuf[i];
      diff2 = timestampBuf[i+2] - timestampBuf[i+1];
      sum = diff1 + diff2;

      // calculate and sum period & duty
      if ((sum > 0U) && (sum <= 80000))
      {
         sumDuty = sumDuty + ((diff1*100U) / sum);
         sumPeriod = sumPeriod + sum;
         divCnt++;
      }
      else
      {
         sumDuty = sumDuty + 0U;
      }

      i = i + 2;
      if (i >= (bufSize-1))
      {
         break;
      }
   }

   if (divCnt > 0U)
   {
      MeasuredValue->Period = (uint32)(sumPeriod / divCnt);
      if (0U == MeasuredValue->Period)
      {
         MeasuredValue->Duty = 0U;
      }
      else
      {
         MeasuredValue->Period = (uint32)((uint32)PWM_INPUT_FREQUENCY/(uint32)MeasuredValue->Period); // Convert to real value
         if (refPeriod == 2000U) // 2000 Hz
         {
            if (MeasuredValue->Period > refPeriod)
            {
               MeasuredValue->Period = refPeriod;
            }
         }
         MeasuredValue->Duty = (uint32)(sumDuty / divCnt);

         // check abs differences
         dutyDiff1 = (sint16)((sint16)refDuty - (sint16)MeasuredValue->Duty);
         dutyDiff2 = (sint16)((sint16)refDuty - (sint16)(100U - MeasuredValue->Duty));
         if (abs(dutyDiff2) < abs(dutyDiff1))
         {
            MeasuredValue->Duty = 100U - MeasuredValue->Duty;
         }
         else if (abs(dutyDiff2) == abs(dutyDiff1))
         {
            if ((ch == IcuChannel_DOWHS1) || (ch == IcuChannel_DOWHS2))
            {
               if (MeasuredValue->Duty > refDuty)
               {
                  MeasuredValue->Duty = 100U - MeasuredValue->Duty;
               }
            }
            else if ((ch == IcuChannel_DOWLS2) || (ch == IcuChannel_DOWLS3))
            {
               if (MeasuredValue->Duty < refDuty)
               {
                  MeasuredValue->Duty = 100U - MeasuredValue->Duty;
               }
            }
            else
            {
               // do nothing
            }
         }
      }
   }
   else
   {
      MeasuredValue->Period = 0U;
      MeasuredValue->Duty = 0U;
   }
}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
