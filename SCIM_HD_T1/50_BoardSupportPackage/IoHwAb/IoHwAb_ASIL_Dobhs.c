/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  IoHwAb_ASIL_Dobhs.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  IoHwAb_ASIL_Dobhs
 *  Generated at:  Thu Jul 16 13:46:54 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <IoHwAb_ASIL_Dobhs>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
//!======================================================================================
//! \file IoHwAb_ASIL_Dobhs.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup Platform 
//! @{
//! @addtogroup Platform_SCIM_IoHwAb 
//! @{
//! @addtogroup IoHwAb_ASIL_Dobhs
//! @{
//!
//! \brief
//! IoHwAb_ASIL_Dobhs SWC.
//! ASIL Level : QM.
//! This module implements the logic for the IoHwAb_ASIL_Dobhs runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * IOHWAB_BOOL
 *   boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * IOHWAB_UINT8
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Rte_DT_EcuHwDioCtrlArray_T_0
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Rte_DT_EcuHwFaultValues_T_0
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_HwToleranceThreshold_X1C04_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_DOBHS_X1CXX_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CY1_DigitalBiLevelHigh_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CY1_DigitalBiLevelLow_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinFaultStatus
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinVoltage_0V2
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_IoHwAb_ASIL_Dobhs.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Dio.h"
#include "IoHwAb_ASIL_Dobhs.h"

#define IoHwAb_ASIL_Dobhs_START_SEC_VAR_INIT_UNSPECIFIED
#include "IoHwAb_ASIL_Dobhs_MemMap.h"

static           Rte_DT_EcuHwDioCtrlArray_T_0 EcuHwDobhsFuncCtrlArray[40] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                                             0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
static           Rte_DT_EcuHwDioCtrlArray_T_0 EcuHwDobhsDiagCtrlArray[40] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                                             0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
static           uint8                        EcuVoltageValues[40]        = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                                             0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
#define IoHwAb_ASIL_Dobhs_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "IoHwAb_ASIL_Dobhs_MemMap.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IOHWAB_BOOL: Boolean
 * IOHWAB_UINT8: Integer in interval [0...255]
 * SEWS_HwToleranceThreshold_X1C04_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelHigh_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelLow_T: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * Fsc_OperationalMode_T: Enumeration of integer in interval [0...255] with enumerators
 *   FSC_ShutdownReady (0U)
 *   FSC_Reduced_12vDcDcLimit (1U)
 *   FSC_Reduced (2U)
 *   FSC_Operating (3U)
 *   FSC_Protecting (4U)
 *   FSC_Withstand (5U)
 *   FSC_NotAvailable (6U)
 * IOCtrlReq_T: Enumeration of integer in interval [0...255] with enumerators
 *   IOCtrl_AppRequest (0U)
 *   IOCtrl_DiagReturnCtrlToApp (1U)
 *   IOCtrl_DiagShortTermAdjust (2U)
 * Rte_DT_EcuHwDioCtrlArray_T_0: Enumeration of integer in interval [0...255] with enumerators
 *   STD_LOW (0U)
 *   STD_HIGH (1U)
 *   Inactive (255U)
 * Rte_DT_EcuHwFaultValues_T_0: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 * SEWS_PcbConfig_DOBHS_X1CXX_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOBHS_X1CXX_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOBHS_X1CXX_T_Populated (1U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 *
 * Array Types:
 * ============
 * EcuHwDioCtrlArray_T: Array with 40 element(s) of type Rte_DT_EcuHwDioCtrlArray_T_0
 * EcuHwFaultValues_T: Array with 40 element(s) of type Rte_DT_EcuHwFaultValues_T_0
 * EcuHwVoltageValues_T: Array with 40 element(s) of type VGTT_EcuPinVoltage_0V2
 * SEWS_PcbConfig_DOBHS_X1CXX_a_T: Array with 4 element(s) of type SEWS_PcbConfig_DOBHS_X1CXX_T
 *
 * Record Types:
 * =============
 * SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T: Record with elements
 *   DigitalBiLevelLow of type SEWS_X1CY1_DigitalBiLevelLow_T
 *   DigitalBiLevelHigh of type SEWS_X1CY1_DigitalBiLevelHigh_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_HwToleranceThreshold_X1C04_T Rte_Prm_X1C04_HwToleranceThreshold_v(void)
 *   SEWS_PcbConfig_DOBHS_X1CXX_T *Rte_Prm_X1CXX_PcbConfig_DOBHS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOBHS_X1CXX_T* is of type SEWS_PcbConfig_DOBHS_X1CXX_a_T
 *   SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void)
 *
 *********************************************************************************************************************/


#define IoHwAb_ASIL_Dobhs_START_SEC_CODE
#include "IoHwAb_ASIL_Dobhs_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsCtrlInterface_P_1_GetDobhsPinState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDobhsPinState_CS> of PortPrototype <DobhsCtrlInterface_P_1>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *   void Rte_IrvRead_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsCtrlInterface_P_1_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_1_GetDobhsPinState_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the  logic for the DobhsCtrlInterface_P_1_GetDobhsPinState_CS
//!
//! \param   *IsDoActivated     Update and check if it has ben activated
//! \param   *DoPinVoltage      Update and check the Pin voltage
//! \param   *BatteryVoltage    Update and Check the Battery volage
//! \param   *FaultStatus       Update and check the Fault status
//!
//! \return   Std_ReturnType   Returns 'RTE_E_INVALID' if the input pointer are NULL
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_1_GetDobhsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_1_GetDobhsPinState_CS (returns application error)
 *********************************************************************************************************************/
   Std_ReturnType               retval                      = RTE_E_OK;
   Rte_DT_EcuHwDioCtrlArray_T_0 DobhsCtrlArray[40]          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                               0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
   Rte_DT_EcuHwFaultValues_T_0  EcuDobhsASILFaultStatus[40] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                               0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};  

   //! ##### Check for the valid pointers and read the parameters DoPinVoltage,IsDoActivated,BatteryVoltage,FaultStatus
   if ((NULL_PTR != IsDoActivated) 
      && (NULL_PTR != BatteryVoltage) 
      && (NULL_PTR != DoPinVoltage ) 
      && (NULL_PTR != FaultStatus))
   {
      Rte_IrvRead_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuFaultStatus(&EcuDobhsASILFaultStatus[0]);
      Rte_IrvRead_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(&DobhsCtrlArray[0]);
      *IsDoActivated   = DobhsCtrlArray[Dobhs_01];
      *DoPinVoltage    = EcuVoltageValues[Dobhs_01];
      *BatteryVoltage  = EcuVoltageValues[PwrSupply];
      *FaultStatus     = EcuDobhsASILFaultStatus[Dobhs_01];
   } 
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsCtrlInterface_P_1_SetDobhsActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDobhsActive_CS> of PortPrototype <DobhsCtrlInterface_P_1>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_1_SetDobhsActive_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the logic for the DobhsCtrlInterface_P_1_SetDobhsActive_CS
//!
//! \param   IOCtrlReqType     Update and check the Request type
//! \param   Activation        Update and check the Activation of Dobhs pin
//!
//! \return   Std_ReturnType   Returns 'RTE_E_INVALID' if PcbConfig_DOBHS_X1CXX_T is not populated
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_1_SetDobhsActive_CS (returns application error)
 *********************************************************************************************************************/
   Std_ReturnType               retval                     = RTE_E_OK;   
   SEWS_PcbConfig_DOBHS_X1CXX_T *PcbConfig_DobhsInterfaces = {NULL_PTR,};

   PcbConfig_DobhsInterfaces  = (SEWS_PcbConfig_DOBHS_X1CXX_T *)Rte_Prm_X1CXX_PcbConfig_DOBHS_v();
   //! ###### Check the status for Dobhs interface  for Pcb configuration 
   if (PcbConfig_DobhsInterfaces[0] == SEWS_PcbConfig_DOBHS_X1CXX_T_Populated)
   {
      //! ##### If IOCtrlReqType is AppRequest then DOBHS value in functional array is updated with activation value 
      if (IOCtrl_AppRequest == IOCtrlReqType)
      {
          EcuHwDobhsFuncCtrlArray[Dobhs_01] = Activation;    
      }
      //! ##### If IOCtrlReqType is ShortTermAdjust then DOBHS value in Diag array is updated with activation value 
      else if (IOCtrl_DiagShortTermAdjust == IOCtrlReqType)
      {
          EcuHwDobhsDiagCtrlArray[Dobhs_01] = Activation;    
      }
      //! ##### If IOCtrlReqType is Return Control to appilcation then DOBHS value in Diag array is updated with 0xFF
      else if (IOCtrl_DiagReturnCtrlToApp == IOCtrlReqType)
      {
          EcuHwDobhsDiagCtrlArray[Dobhs_01] = 0xff;    
      }
      else
      {
          retval = RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError;
      }   
   }
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;    
   
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsCtrlInterface_P_2_GetDobhsPinState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDobhsPinState_CS> of PortPrototype <DobhsCtrlInterface_P_2>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *   void Rte_IrvRead_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsCtrlInterface_P_2_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_2_GetDobhsPinState_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the logic for the DobhsCtrlInterface_P_2_GetDobhsPinState_CS
//!
//! \param   *IsDoActivated     Update and check if it has ben activated
//! \param   *DoPinVoltage      Update and check the Pin voltage
//! \param   *BatteryVoltage    Update and Check the Battery volage
//! \param   *FaultStatus       Update and check the Fault status
//!
//! \return   Std_ReturnType   Returns 'RTE_E_INVALID' if the input pointers are NULL
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_2_GetDobhsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_2_GetDobhsPinState_CS (returns application error)
 *********************************************************************************************************************/
   Std_ReturnType               retval                      = RTE_E_OK;
   Rte_DT_EcuHwDioCtrlArray_T_0 DobhsCtrlArray[40]          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                               0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
   Rte_DT_EcuHwFaultValues_T_0  EcuDobhsASILFaultStatus[40] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                               0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

   //! ###### check for valid pointers and read DoPinVoltage,IsDoActivated,BatteryVoltage,FaultStatus
   if ((NULL_PTR != IsDoActivated) 
      && (NULL_PTR != BatteryVoltage) 
      && (NULL_PTR != DoPinVoltage ) 
      && (NULL_PTR != FaultStatus))
   {
      Rte_IrvRead_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuFaultStatus(&EcuDobhsASILFaultStatus[0]);
      Rte_IrvRead_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(&DobhsCtrlArray[0]);
      *IsDoActivated  = DobhsCtrlArray[Dobhs_02];
      *DoPinVoltage   = EcuVoltageValues[Dobhs_02];
      *BatteryVoltage = EcuVoltageValues[PwrSupply];
      *FaultStatus    = EcuDobhsASILFaultStatus[Dobhs_02];
   } 
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsCtrlInterface_P_2_SetDobhsActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDobhsActive_CS> of PortPrototype <DobhsCtrlInterface_P_2>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_2_SetDobhsActive_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the logic for the DobhsCtrlInterface_P_2_SetDobhsActive_CS
//!
//! \param   IOCtrlReqType     Update and check the Request type
//! \param   Activation        Update and check the Activation of Dobhs pin
//!
//! \return   Std_ReturnType   Returns 'RTE_E_INVALID' if PcbConfig_DOBHS_X1CXX_T is not populated
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_2_SetDobhsActive_CS (returns application error)
 *********************************************************************************************************************/  
   SEWS_PcbConfig_DOBHS_X1CXX_T *PcbConfig_DobhsInterfaces = {NULL_PTR,};
   Std_ReturnType               retval                     = RTE_E_OK;
   
   PcbConfig_DobhsInterfaces = (SEWS_PcbConfig_DOBHS_X1CXX_T *)Rte_Prm_X1CXX_PcbConfig_DOBHS_v();   
   //! ###### check for PcbConfig_DobhsInterfaces status
   if (PcbConfig_DobhsInterfaces[1] == SEWS_PcbConfig_DOBHS_X1CXX_T_Populated)
   {
      //! ##### If IOCtrlReqType is AppRequest then DOBHS value in functional array is updated with activation value    
      if (IOCtrl_AppRequest == IOCtrlReqType)
      {
         EcuHwDobhsFuncCtrlArray[Dobhs_02] = Activation;
      }
      //! ##### If IOCtrlReqType is ShortTermAdjust then DOBHS value in Diag array is updated with activation value 
      else if (IOCtrl_DiagShortTermAdjust == IOCtrlReqType)
      {
         EcuHwDobhsDiagCtrlArray[Dobhs_02] = Activation;
      }     
      //! ##### If IOCtrlReqType is Return Control to appilcation then DOBHS value in Diag array is updated with 0xFF
      else if (IOCtrl_DiagReturnCtrlToApp == IOCtrlReqType)
      {
         EcuHwDobhsDiagCtrlArray[Dobhs_02] = 0xff;
      }
      else
      {
         retval = RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError;
      }
   }   
   else
   {
      retval = RTE_E_INVALID;
   }   
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsCtrlInterface_P_3_GetDobhsPinState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDobhsPinState_CS> of PortPrototype <DobhsCtrlInterface_P_3>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *   void Rte_IrvRead_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsCtrlInterface_P_3_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_3_GetDobhsPinState_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the logic for the DobhsCtrlInterface_P_3_GetDobhsPinState_CS
//!
//! \param   *IsDoActivated     Update and check if it has ben activated
//! \param   *DoPinVoltage      Update and check the Pin voltage
//! \param   *BatteryVoltage    Update and Check the Battery volage
//! \param   *FaultStatus       Update and check the Fault status
//!
//! \return   Std_ReturnType   Returns 'RTE_E_INVALID' if the input pointer are NULL
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_3_GetDobhsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_3_GetDobhsPinState_CS (returns application error)
 *********************************************************************************************************************/
   Std_ReturnType                  retval                      = RTE_E_OK;
   Rte_DT_EcuHwDioCtrlArray_T_0    DobhsCtrlArray[40]          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                                  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
   Rte_DT_EcuHwFaultValues_T_0     EcuDobhsASILFaultStatus[40] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                                  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  
   //! ###### check for valid pointers and read DoPinVoltage,IsDoActivated,BatteryVoltage,FaultStatus  
   if ((NULL_PTR != IsDoActivated) 
      && (NULL_PTR != BatteryVoltage) 
      && (NULL_PTR != DoPinVoltage ) 
      && (NULL_PTR != FaultStatus))
   {
      Rte_IrvRead_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuFaultStatus(&EcuDobhsASILFaultStatus[0]);
      Rte_IrvRead_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(&DobhsCtrlArray[0]);
      *IsDoActivated  = DobhsCtrlArray[Dobhs_03];
      *DoPinVoltage   = EcuVoltageValues[Dobhs_03];
      *BatteryVoltage = EcuVoltageValues[PwrSupply];
      *FaultStatus    = EcuDobhsASILFaultStatus[Dobhs_03];
   } 
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsCtrlInterface_P_3_SetDobhsActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDobhsActive_CS> of PortPrototype <DobhsCtrlInterface_P_3>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_3_SetDobhsActive_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the logic for the DobhsCtrlInterface_P_3_SetDobhsActive_CS
//!
//! \param   IOCtrlReqType     Update and check the Request type
//! \param   Activation        Update and check the Activation of Dobhs pin
//!
//! \return   Std_ReturnType   Returns 'RTE_E_INVALID' if PcbConfig_DOBHS_X1CXX_T is not populated
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_3_SetDobhsActive_CS (returns application error)
 *********************************************************************************************************************/ 
   Std_ReturnType retval = RTE_E_OK;
   
   const SEWS_PcbConfig_DOBHS_X1CXX_T *PcbConfig_DobhsInterfaces = (SEWS_PcbConfig_DOBHS_X1CXX_T *)Rte_Prm_X1CXX_PcbConfig_DOBHS_v();  
   //! ###### check for PcbConfig_DobhsInterfaces status
   if (PcbConfig_DobhsInterfaces[2] == SEWS_PcbConfig_DOBHS_X1CXX_T_Populated)
   {
      //! ##### If IOCtrlReqType is AppRequest then DOBHS value in functional array is updated with activation value
      if (IOCtrl_AppRequest == IOCtrlReqType)
      {
         EcuHwDobhsFuncCtrlArray[Dobhs_03] = Activation;
      }
      //! ##### If IOCtrlReqType is ShortTermAdjust then DOBHS value in Diag array is updated with activation value
      else if (IOCtrl_DiagShortTermAdjust == IOCtrlReqType)
      {
         EcuHwDobhsDiagCtrlArray[Dobhs_03] = Activation;
      }
      //! ##### If IOCtrlReqType is Return Control to appilcation then DOBHS value in Diag array is updated with 0xFF
      else if (IOCtrl_DiagReturnCtrlToApp == IOCtrlReqType)
      {
         EcuHwDobhsDiagCtrlArray[Dobhs_03] = 0xff;
      }
      else
      {
         retval = RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError;
      }
   }
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsCtrlInterface_P_4_GetDobhsPinState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDobhsPinState_CS> of PortPrototype <DobhsCtrlInterface_P_4>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *   void Rte_IrvRead_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsCtrlInterface_P_4_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_4_GetDobhsPinState_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the logic for the DobhsCtrlInterface_P_4_GetDobhsPinState_CS
//!
//! \param   *IsDoActivated     Update and check if it has ben activated
//! \param   *DoPinVoltage      Update and check the Pin voltage
//! \param   *BatteryVoltage    Update and Check the Battery volage
//! \param   *FaultStatus       Update and check the Fault status
//!
//! \return   Std_ReturnType   Returns 'RTE_E_INVALID' if the input pointer are NULL
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_4_GetDobhsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_4_GetDobhsPinState_CS (returns application error)
 *********************************************************************************************************************/
   Std_ReturnType               retval                      = RTE_E_OK;
   Rte_DT_EcuHwDioCtrlArray_T_0 DobhsCtrlArray[40]          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                               0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
   Rte_DT_EcuHwFaultValues_T_0  EcuDobhsASILFaultStatus[40] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                               0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

   //! ###### check for valid pointers and read DoPinVoltage,IsDoActivated,BatteryVoltage,FaultStatus
   if ((NULL_PTR != IsDoActivated) 
      && (NULL_PTR != BatteryVoltage) 
      && (NULL_PTR != DoPinVoltage ) 
      && (NULL_PTR != FaultStatus))
   {
      Rte_IrvRead_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuFaultStatus(&EcuDobhsASILFaultStatus[0]);
      Rte_IrvRead_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(&DobhsCtrlArray[0]);
      *IsDoActivated  = DobhsCtrlArray[Dobhs_04];
      *DoPinVoltage   = EcuVoltageValues[Dobhs_04];
      *BatteryVoltage = EcuVoltageValues[PwrSupply];
      *FaultStatus    = EcuDobhsASILFaultStatus[Dobhs_04];
   } 
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsCtrlInterface_P_4_SetDobhsActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDobhsActive_CS> of PortPrototype <DobhsCtrlInterface_P_4>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_4_SetDobhsActive_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the logic for the DobhsCtrlInterface_P_4_SetDobhsActive_CS
//!
//! \param   IOCtrlReqType     Update and check the Request type
//! \param   Activation        Update and check the Activation of Dobhs pin
//!
//! \return   Std_ReturnType   Returns 'RTE_E_INVALID' if PcbConfig_DOBHS_X1CXX_T is not populated
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_4_SetDobhsActive_CS (returns application error)
 *********************************************************************************************************************/    
   Std_ReturnType               retval                     = RTE_E_OK;
   SEWS_PcbConfig_DOBHS_X1CXX_T *PcbConfig_DobhsInterfaces = {NULL_PTR,};
   
   PcbConfig_DobhsInterfaces = (SEWS_PcbConfig_DOBHS_X1CXX_T *)Rte_Prm_X1CXX_PcbConfig_DOBHS_v();
   //! ###### check for PcbConfig_DobhsInterfaces status
   if (PcbConfig_DobhsInterfaces[3] == SEWS_PcbConfig_DOBHS_X1CXX_T_Populated)
   {
      //! ##### If IOCtrlReqType is AppRequest then DOBHS value in functional array is updated with activation value
      if (IOCtrl_AppRequest == IOCtrlReqType)
      {
         EcuHwDobhsFuncCtrlArray[Dobhs_04] = Activation;
      }
      //! ##### If DiagReqType is ShortTermAdjust then DOBHS value in Diag array is updated with activation value
      else if (IOCtrl_DiagShortTermAdjust == IOCtrlReqType)
      {
         EcuHwDobhsDiagCtrlArray[Dobhs_04] = Activation;
      }
      //! ##### If IOCtrlReqType is Return Control to appilcation then DOBHS value in Diag array is updated with 0xFF
      else if (IOCtrl_DiagReturnCtrlToApp == IOCtrlReqType)
      {
         EcuHwDobhsDiagCtrlArray[Dobhs_04] = 0xff;
      }
      else
      {
         retval = RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError;
      }
   }
   else
   {
      retval = RTE_E_INVALID;
   }
   return retval;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsDiagInterface_P_GetDobhsPinState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDobhsPinState_CS> of PortPrototype <DobhsDiagInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *   void Rte_IrvRead_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsDiagInterface_P_GetDobhsPinState_CS_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! This function implements the logic for the DobhsCtrlInterface_P_4_GetDobhsPinState_CS
//!
//! \param    DoPinRef          Update and check the Pin in use
//! \param   *isDioActivated    Update and check if it has ben activated
//! \param   *DoPinVoltage      Update and check the Pin voltage
//! \param   *BatteryVoltage    Update and Check the Battery volage
//! \param   *FaultStatus       Update and check the Fault status
//!
//!====================================================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) isDioActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsDiagInterface_P_GetDobhsPinState_CS (returns application error)
 *********************************************************************************************************************/
   Std_ReturnType               retval                      = RTE_E_OK;
   Rte_DT_EcuHwDioCtrlArray_T_0 DobhsCtrlArray[40]          = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                               0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
   Rte_DT_EcuHwFaultValues_T_0  EcuDobhsASILFaultStatus[40] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                               0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
   //! ###### Processing the Dobhs Pin state
   //! ##### Check if the input pointers are valid 
   if ((NULL_PTR != isDioActivated) 
      &&(NULL_PTR != BatteryVoltage)
      &&(NULL_PTR != DoPinVoltage) 
      &&(NULL_PTR != FaultStatus))
   {
      //! #### Read the Fault status and DioControl
      Rte_IrvRead_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuFaultStatus(&EcuDobhsASILFaultStatus[0]);
      Rte_IrvRead_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(&DobhsCtrlArray[0]);
      //! #### Check the Pin and accordingly assign the values
      if (DoPinRef == 1U)
      {
         *isDioActivated = DobhsCtrlArray[Dobhs_01];
         *DoPinVoltage   = EcuVoltageValues[Dobhs_01];
         *BatteryVoltage = EcuVoltageValues[PwrSupply];
         *FaultStatus    = EcuDobhsASILFaultStatus[Dobhs_01];
      }
      else if (DoPinRef == 2U)
      {
         *isDioActivated = DobhsCtrlArray[Dobhs_02];
         *DoPinVoltage   = EcuVoltageValues[Dobhs_02];
         *BatteryVoltage = EcuVoltageValues[PwrSupply];
         *FaultStatus    = EcuDobhsASILFaultStatus[Dobhs_02];
      }
      else if (DoPinRef == 3U)
      {
         *isDioActivated = DobhsCtrlArray[Dobhs_03];
         *DoPinVoltage   = EcuVoltageValues[Dobhs_03];
         *BatteryVoltage = EcuVoltageValues[PwrSupply];
         *FaultStatus    = EcuDobhsASILFaultStatus[Dobhs_03];
      }
      else if (DoPinRef == 4U)
      {
         *isDioActivated = DobhsCtrlArray[Dobhs_04];
         *DoPinVoltage   = EcuVoltageValues[Dobhs_04];
         *BatteryVoltage = EcuVoltageValues[PwrSupply];
         *FaultStatus    = EcuDobhsASILFaultStatus[Dobhs_04];
      }
      else
      {
          retval = RTE_E_INVALID;
      }
   } 
   //! ##### If the input pointers are not valid then return invalid
   else
   {
     retval = RTE_E_INVALID;
   }
   return retval;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: IoHwAb_ASIL_Dobhs_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_DiagActiveState_P_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T *data)
 *   Std_ReturnType Rte_Read_ScimPvtControl_P_Status(uint8 *data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *   void Rte_IrvRead_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(const Rte_DT_EcuHwFaultValues_T_0 *data)
 *   void Rte_IrvWrite_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(const Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_EcuHwState_P_GetEcuVoltages_CS(VGTT_EcuPinVoltage_0V2 *EcuVoltageValues)
 *     Argument EcuVoltageValues: VGTT_EcuPinVoltage_0V2* is of type EcuHwVoltageValues_T
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_EcuHwState_I_AdcInFailure
 *   Std_ReturnType Rte_Call_VbatInterface_P_GetVbatVoltage_CS(VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_VbatInterface_I_AdcInFailure, RTE_E_VbatInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: IoHwAb_ASIL_Dobhs_10ms_runnable_doc
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the cyclic execution runnable logic for the IoHwAb_ASIL_Dobhs_10ms_runnable
//!
//!==================================================================================================================== 

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, IoHwAb_ASIL_Dobhs_CODE) IoHwAb_ASIL_Dobhs_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: IoHwAb_ASIL_Dobhs_10ms_runnable
 *********************************************************************************************************************/
   //! ###### Processing IoHwAb_ASIL_Dobhs_10ms_runnable
   //! ##### Process the IoHwAb_ASILDobhs_FaultDetection logic :IoHwAb_ASILDobhs_FaultDetection()
   IoHwAb_ASILDobhs_FaultDetection();    
   //! ##### Process the IoHwAb_ASILDobhs_DioHandling logic :IoHwAb_ASILDobhs_DioHandling()
   IoHwAb_ASILDobhs_DioHandling(); 
   IoHwAb_ASILDobhs_CurrentMonitoring();



/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}



#define IoHwAb_ASIL_Dobhs_STOP_SEC_CODE
#include "IoHwAb_ASIL_Dobhs_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the IoHwAb_ASILDobhs_DioHandling
//! 
//!
//!====================================================================================================================


static void IoHwAb_ASILDobhs_DioHandling(void)
{
   //! ###### Processing the logic for IoHwAb_ASILDobhs_DioHandling
   uint8                         PvtCtrl            = 0U;
   Fsc_OperationalMode_T         FSCModeCurr        = 0U;
   DiagActiveState_T             DiagState          = 0U;
   Rte_DT_EcuHwDioCtrlArray_T_0  DobhsCtrlArray[40] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};   
     
   //! ##### check for the type of request (Diagnostic / functional)   
   Rte_Read_ScimPvtControl_P_Status(&PvtCtrl);
   Rte_Read_DiagActiveState_P_isDiagActive(&DiagState);
   Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(&FSCModeCurr);
   //! #### Check if current FSCMode is not shutdown ready and withstand and Dobhs in Functional or Diagnostics is STD_HIGH then write high to DOBHS output channel
   if ((FSC_ShutdownReady == FSCModeCurr)
      || (FSC_Withstand == FSCModeCurr))
   {
      EcuHwDobhsDiagCtrlArray[Dobhs_01] = 0U;
      EcuHwDobhsDiagCtrlArray[Dobhs_02] = 0U;
      EcuHwDobhsDiagCtrlArray[Dobhs_03] = 0U;
      EcuHwDobhsDiagCtrlArray[Dobhs_04] = 0U;      
      EcuHwDobhsFuncCtrlArray[Dobhs_01] = 0U;
      EcuHwDobhsFuncCtrlArray[Dobhs_02] = 0U;
      EcuHwDobhsFuncCtrlArray[Dobhs_03] = 0U;
      EcuHwDobhsFuncCtrlArray[Dobhs_04] = 0U;
   }
   else
   {
       // Do nothing
   }   
   if ((DiagState != TRUE)
      && (PvtCtrl != 1U))
   {
      EcuHwDobhsDiagCtrlArray[Dobhs_01] = 0xffU;
      EcuHwDobhsDiagCtrlArray[Dobhs_02] = 0xffU;
      EcuHwDobhsDiagCtrlArray[Dobhs_03] = 0xffU;
      EcuHwDobhsDiagCtrlArray[Dobhs_04] = 0xffU;      
   }
   else
   {
       // Do nothing
   }    
   //! #### Checking Diag or functional request for DOBHS_01
   DobhsControl((uint8)Dobhs_01,&DobhsCtrlArray[Dobhs_01]);
   //! #### Checking Diag or functional request for DOBHS_02
   DobhsControl((uint8)Dobhs_02,&DobhsCtrlArray[Dobhs_02]);
   //! #### Checking Diag or functional request for DOBHS_03
   DobhsControl((uint8)Dobhs_03,&DobhsCtrlArray[Dobhs_03]);
   //! #### Checking Diag or functional request for DOBHS_04
   DobhsControl((uint8)Dobhs_04,&DobhsCtrlArray[Dobhs_04]);
   //! ##### Writing all the update values to the respective DOBHS Dio channels
   Dio_WriteChannel( DioConf_DioChannel_Dobhs_OutputCh1 ,DobhsCtrlArray[Dobhs_01]);
   Dio_WriteChannel( DioConf_DioChannel_Dobhs_OutputCh2 ,DobhsCtrlArray[Dobhs_02]);
   Dio_WriteChannel( DioConf_DioChannel_Dobhs_OutputCh3 ,DobhsCtrlArray[Dobhs_03]);
   Dio_WriteChannel( DioConf_DioChannel_Dobhs_OutputCh4 ,DobhsCtrlArray[Dobhs_04]);
   Rte_IrvWrite_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(&DobhsCtrlArray[0]);
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'DobhsControl'
//! 
//!
//!====================================================================================================================
static void DobhsControl(uint8                        DobhsPin,
                         Rte_DT_EcuHwDioCtrlArray_T_0 *DobhsCtrlArray)
{
   if (((EcuHwDobhsDiagCtrlArray[DobhsPin] == 0xffU) 
      && (EcuHwDobhsFuncCtrlArray[DobhsPin] == 1U)) 
      || (EcuHwDobhsDiagCtrlArray[DobhsPin] == 1U))
   {
      DobhsCtrlArray[0] = 1U;
   }
   else
   {
       // Do nothing
   }
   if (((EcuHwDobhsDiagCtrlArray[DobhsPin] == 0xffU) 
      && (EcuHwDobhsFuncCtrlArray[DobhsPin] == 0U)) 
      || (EcuHwDobhsDiagCtrlArray[DobhsPin] == 0U))
   {
      DobhsCtrlArray[0] = 0U;
   }
   else
   {
       // Do nothing
   }
}


//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the IoHwAb_ASILDobhs_FaultDetection
//! 
//!
//!====================================================================================================================
static void FaultConditionsS01S03_Check(boolean                      DobhsRead, 
                                        uint8                        DobhsPin, 
                                        uint8                        Dobhs_IPSvalue,
                                        boolean                      DobhsFaultChannel,
                                        Rte_DT_EcuHwFaultValues_T_0 *EcuDobhsASILFaultStatus)
{
   boolean DobhsDiagEn;
   boolean DobhsSelect;
   SEWS_HwToleranceThreshold_X1C04_T          HWToleranceThreshold   = Rte_Prm_X1C04_HwToleranceThreshold_v();
   SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *DigitalBilevelVoltage = (SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *)Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

   (void)Rte_Call_EcuHwState_P_GetEcuVoltages_CS(&EcuVoltageValues[0]);
   DobhsDiagEn          = Dio_ReadChannel(DioConf_DioChannel_Dobhs_DignEnable);
   DobhsSelect          = Dio_ReadChannel(DioConf_DioChannel_Dobhs_Select);
   HWToleranceThreshold = Rte_Prm_X1C04_HwToleranceThreshold_v();
   //! ###### check for DobhsRead status
   if( DobhsRead == STD_HIGH)
   {      
      //! ##### Checking the EcuVoltageValues of Dobhs_01 and respectively assigning the fault status
      if ((EcuVoltageValues[DobhsPin] < DigitalBilevelVoltage->DigitalBiLevelLow) 
         &&(DobhsFaultChannel == STD_LOW)
         &&(DobhsDiagEn == STD_HIGH)
         &&(DobhsSelect == STD_LOW))
      {
         EcuDobhsASILFaultStatus[DobhsPin] = OnState_FaultDetected_STG;         
         EcuHwDobhsFuncCtrlArray[DobhsPin] = 0U;
         EcuHwDobhsDiagCtrlArray[DobhsPin] = 0U;
         //Fallback_STG = 1;
         //STG_Fault++;
      }
      else if ((EcuVoltageValues[DobhsPin] > DigitalBilevelVoltage->DigitalBiLevelLow) 
               &&(EcuVoltageValues[DobhsPin]  < (EcuVoltageValues[PwrSupply] - (uint8)BATVOLTAGE_ADC_RAW_2V - HWToleranceThreshold)))
      {
         EcuDobhsASILFaultStatus[DobhsPin] = OnState_FaultDetected_VBT;
      }      
      else if ((EcuVoltageValues[Dobhs_IPSvalue] < UltraLowVoltageConfig_UltraLowVoltage) 
              &&(DobhsDiagEn == STD_HIGH)
              &&(DobhsSelect == STD_LOW))
      {
         EcuDobhsASILFaultStatus[DobhsPin] = OnState_FaultDetected_OC;
      }   
      else if ((EcuVoltageValues[DobhsPin] > (EcuVoltageValues[PwrSupply] - (uint8)BATVOLTAGE_ADC_RAW_2V - HWToleranceThreshold)) 
              &&(EcuVoltageValues[DobhsPin] < (EcuVoltageValues[PwrSupply] + (uint8)BATVOLTAGE_ADC_RAW_5V   + HWToleranceThreshold))
              &&(DobhsFaultChannel == STD_HIGH)
              &&(DobhsDiagEn == STD_HIGH)
              &&(DobhsSelect == STD_LOW))
      {
         EcuDobhsASILFaultStatus[DobhsPin] = OnState_NoFaultDetected;
      }
      else
      {
         // Do nothing
      }
   }
   else if (DobhsRead == STD_LOW)
   {
      if ((EcuVoltageValues[DobhsPin] > DigitalBilevelVoltage->DigitalBiLevelLow) 
         &&(DobhsFaultChannel == STD_LOW)
         &&(DobhsDiagEn == STD_HIGH)
         &&(DobhsSelect == STD_LOW))      
      {
         EcuDobhsASILFaultStatus[DobhsPin] = OffState_FaultDetected_STB;
         //Fallback_STB = 1;
         EcuHwDobhsFuncCtrlArray[DobhsPin] = 0U;
         EcuHwDobhsDiagCtrlArray[DobhsPin] = 0U;
      }
      else if ((EcuVoltageValues[DobhsPin] > ( (uint8)BATVOLTAGE_ADC_RAW_5V + HWToleranceThreshold))
              &&(DobhsFaultChannel == STD_HIGH)
              &&(DobhsDiagEn == STD_HIGH)
              &&(DobhsSelect == STD_LOW))
      {
         EcuDobhsASILFaultStatus[DobhsPin] = OffState_FaultDetected_VAT;
      }   
      else if ((EcuVoltageValues[DobhsPin] < DigitalBilevelVoltage->DigitalBiLevelLow)
              &&(DobhsFaultChannel == STD_HIGH)
              &&(DobhsDiagEn == STD_HIGH)
              &&(DobhsSelect == STD_LOW))
      {
         EcuDobhsASILFaultStatus[DobhsPin] = OffState_NoFaultDetected;
      }
      else
      {
         // Do nothing
      }
   }
   //! #### If no fault detected then TestNotRun is assigned to DobhsPin Fault status
   else
   {
      EcuDobhsASILFaultStatus[DobhsPin] = TestNotRun;
   }
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'FaultConditionsS02S04_Check'
//! 
//!
//!====================================================================================================================

static void FaultConditionsS02S04_Check(boolean                      DobhsRead, 
                                        uint8                        DobhsPin, 
                                        uint8                        Dobhs_IPSvalue,
                                        boolean                      DobhsFaultChannel,
                                        Rte_DT_EcuHwFaultValues_T_0 *EcuDobhsASILFaultStatus)
{
   boolean DobhsDiagEn;
   boolean DobhsSelect;
         SEWS_HwToleranceThreshold_X1C04_T          HWToleranceThreshold   = Rte_Prm_X1C04_HwToleranceThreshold_v();
   const SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *DigitalBilevelVoltage = (SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *)Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();   

   (void)Rte_Call_EcuHwState_P_GetEcuVoltages_CS(&EcuVoltageValues[0]);
   DobhsDiagEn          = Dio_ReadChannel(DioConf_DioChannel_Dobhs_DignEnable);
   DobhsSelect          = Dio_ReadChannel(DioConf_DioChannel_Dobhs_Select);
   HWToleranceThreshold = Rte_Prm_X1C04_HwToleranceThreshold_v();
   //! ###### check for DobhsRead status
   if (DobhsRead == STD_HIGH)
   {
      //! #### Checking the EcuVoltageValues of Dobhs_02 and respectively assigning the fault status
      if ((EcuVoltageValues[DobhsPin] < DigitalBilevelVoltage->DigitalBiLevelLow) 
         &&(DobhsFaultChannel == STD_LOW)
         &&(DobhsDiagEn == STD_HIGH)
         &&(DobhsSelect == STD_HIGH))      
      {
         EcuDobhsASILFaultStatus[DobhsPin] = OnState_FaultDetected_STG;
         EcuHwDobhsFuncCtrlArray[DobhsPin] = 0;
         EcuHwDobhsDiagCtrlArray[DobhsPin] = 0;
      }
      else if ((EcuVoltageValues[DobhsPin] > DigitalBilevelVoltage->DigitalBiLevelLow)
              &&(EcuVoltageValues[DobhsPin]  < (EcuVoltageValues[PwrSupply] - (uint8)BATVOLTAGE_ADC_RAW_2V - HWToleranceThreshold)))
      {
         EcuDobhsASILFaultStatus[DobhsPin] = OnState_FaultDetected_VBT;
      }
      else if ((EcuVoltageValues[Dobhs_IPSvalue] < UltraLowVoltageConfig_UltraLowVoltage) 
              &&(DobhsDiagEn == STD_HIGH)
              &&(DobhsSelect == STD_HIGH))
      {
            EcuDobhsASILFaultStatus[DobhsPin] = OnState_FaultDetected_OC;
      
      }      
      else if  ((EcuVoltageValues[DobhsPin] > (EcuVoltageValues[PwrSupply] - (uint8)BATVOLTAGE_ADC_RAW_2V - HWToleranceThreshold))
               &&(EcuVoltageValues[DobhsPin] < (EcuVoltageValues[PwrSupply] + (uint8)BATVOLTAGE_ADC_RAW_5V  + HWToleranceThreshold))
               &&(DobhsFaultChannel == STD_HIGH)
               &&(DobhsDiagEn == STD_HIGH)
               &&(DobhsSelect == STD_HIGH))
      {
         EcuDobhsASILFaultStatus[DobhsPin] = OnState_NoFaultDetected;
      }
      else
      {
         // do nothing
      }      
   }
   else if (DobhsRead == STD_LOW)
   {
      if ((EcuVoltageValues[DobhsPin] > DigitalBilevelVoltage->DigitalBiLevelLow) 
         &&(DobhsFaultChannel == STD_LOW)
         &&(DobhsDiagEn == STD_HIGH)
         &&(DobhsSelect == STD_HIGH))      
      {
         EcuDobhsASILFaultStatus[DobhsPin] = OffState_FaultDetected_STB;
         EcuHwDobhsFuncCtrlArray[DobhsPin] = 0;
         EcuHwDobhsDiagCtrlArray[DobhsPin] = 0;
      }
      else if ((EcuVoltageValues[DobhsPin] > ( (uint8)BATVOLTAGE_ADC_RAW_5V + HWToleranceThreshold))
              &&(DobhsFaultChannel == STD_HIGH)
              &&(DobhsDiagEn == STD_HIGH)
              &&(DobhsSelect == STD_HIGH))
      {
         EcuDobhsASILFaultStatus[DobhsPin] = OffState_FaultDetected_VAT;
      }   
      else if ((EcuVoltageValues[DobhsPin] < DigitalBilevelVoltage->DigitalBiLevelLow)
               &&(DobhsFaultChannel == STD_HIGH)
               &&(DobhsDiagEn == STD_HIGH)
               &&(DobhsSelect == STD_HIGH))      
      {
         EcuDobhsASILFaultStatus[DobhsPin] = OffState_NoFaultDetected;
      }
      else
      {
         // Do nothing
      }
   }
   //! #### If no fault detected then TestNotRun is assigned to Dobhs_02 Fault status
   else
   {
      EcuDobhsASILFaultStatus[DobhsPin]= TestNotRun;
   }   
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the 'IoHwAb_ASILDobhs_FaultDetection'
//! 
//!
//!====================================================================================================================
static void IoHwAb_ASILDobhs_FaultDetection(void)
{
   uint8   DobhsPin;
   uint8   Dobhs_IPSvalue;
   boolean DobhsFaultChannel;
   boolean DobhsRead;
   //! ###### Processing the logic for IoHwAb_ASILDobhs_FaultDetection
   boolean Dobhs1Read;
   boolean Dobhs2Read;
   boolean Dobhs3Read;
   boolean Dobhs4Read;
   boolean DobhsFaultInCh3Ch2;
   boolean DobhsFaultInCh1Ch4;
   boolean DobhsSelect;

   static Rte_DT_EcuHwFaultValues_T_0 EcuDobhsASILFaultStatus[40] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};   
   static uint8                       PrevDobhsCh[4]              = {0,0,0,0};
   static uint8                       PrevDobhsSEL                = 0;
   //! ##### Reading the values of ECU voltage and Dio channels respectively
   Dobhs1Read         = Dio_ReadChannel(DioConf_DioChannel_Dobhs_OutputCh1);
   Dobhs2Read         = Dio_ReadChannel(DioConf_DioChannel_Dobhs_OutputCh2);
   Dobhs3Read         = Dio_ReadChannel(DioConf_DioChannel_Dobhs_OutputCh3);
   Dobhs4Read         = Dio_ReadChannel(DioConf_DioChannel_Dobhs_OutputCh4);
   DobhsFaultInCh3Ch2 = Dio_ReadChannel(DioConf_DioChannel_Dobhs_FaultInCh3Ch2);
   DobhsFaultInCh1Ch4 = Dio_ReadChannel(DioConf_DioChannel_Dobhs_FaultInCh1Ch4);
   DobhsSelect        = Dio_ReadChannel(DioConf_DioChannel_Dobhs_Select);

   //! ##### Reading Dobhs_Dio pins value and checking its state
   if (PrevDobhsSEL==DobhsSelect)
   {
      //! #### Processing DOBHS_01 fault detection 
      //! #### Reading Dobhs_01 Dio pin value and checking its state
      if (PrevDobhsCh[0]==Dobhs1Read)
      {
         DobhsRead         = Dobhs1Read;
         DobhsPin          = (uint8)Dobhs_01;
         Dobhs_IPSvalue    = (uint8)Dobhs_IPS1;
         DobhsFaultChannel = DobhsFaultInCh1Ch4;
         FaultConditionsS01S03_Check(DobhsRead,
                                     DobhsPin,
                                     Dobhs_IPSvalue,
                                     DobhsFaultChannel,
                                     &EcuDobhsASILFaultStatus[0]);
      }
      else
      {
         // Do nothing
      }   
      //! #### Processing DOBHS_02 fault detection 
      //! #### Reading Dobhs_02 Dio pin value and checking its state
      if (PrevDobhsCh[1] == Dobhs2Read)
      {
         DobhsRead         = Dobhs2Read;
         DobhsPin          = (uint8)Dobhs_02;
         Dobhs_IPSvalue    = (uint8)Dobhs_IPS2;
         DobhsFaultChannel = DobhsFaultInCh3Ch2;
         FaultConditionsS02S04_Check(DobhsRead,
                                     DobhsPin,
                                     Dobhs_IPSvalue,
                                     DobhsFaultChannel,
                                     &EcuDobhsASILFaultStatus[0]);
      }
      else
      {
         // Do nothing
      }
      //! #### Processing DOBHS_03 fault detection
      //! #### Reading Dobhs_03 Dio pin value and checking its state
      if (PrevDobhsCh[2]==Dobhs3Read)
      {
         DobhsRead         = Dobhs3Read;
         DobhsPin          = (uint8)Dobhs_03;
         Dobhs_IPSvalue    = (uint8)Dobhs_IPS2;
         DobhsFaultChannel = DobhsFaultInCh3Ch2;
         FaultConditionsS01S03_Check(DobhsRead,
                                     DobhsPin,
                                     Dobhs_IPSvalue,
                                     DobhsFaultChannel,
                                     &EcuDobhsASILFaultStatus[0]);
      }
      else
      {
         // Do nothing
      }   
      //! #### Processing DOBHS_04 fault detection
      //! #### Reading Dobhs_04 Dio pin value and checking its state
      if (PrevDobhsCh[3]==Dobhs4Read)
      {
         DobhsRead         = Dobhs4Read;
         DobhsPin          = (uint8)Dobhs_04;
         Dobhs_IPSvalue    = (uint8)Dobhs_IPS1;
         DobhsFaultChannel = DobhsFaultInCh1Ch4;
         FaultConditionsS02S04_Check(DobhsRead,
                                     DobhsPin,
                                     Dobhs_IPSvalue,
                                     DobhsFaultChannel,
                                     &EcuDobhsASILFaultStatus[0]);
      }
      else
      {
         // Do nothing
      }
   }
   else
   {
      // Do nothing
   }
   //! ##### If no fault detected then TestNotRun is assigned to Dobhs_04 Fault status
   PrevDobhsSEL=DobhsSelect;
   PrevDobhsCh[0]=Dobhs1Read;      
   PrevDobhsCh[1]=Dobhs2Read;    
   PrevDobhsCh[2]=Dobhs3Read;          
   PrevDobhsCh[3]=Dobhs4Read;    
   Rte_IrvWrite_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(&EcuDobhsASILFaultStatus[0]);
}
//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution logic for the 'IoHwAb_ASILDobhs_CurrentMonitoring'
//!
//!====================================================================================================================
static void IoHwAb_ASILDobhs_CurrentMonitoring(void)
{
   static uint8 flag               = 0;
          uint8 Dobhs_SelectStatus = 0;     
   //static uint8 dobhstest=1;

   //! ##### Based on the current D012V_SelectOut0Status status swap it to other channel
   if (flag == 0)
   {
      Dobhs_SelectStatus = Dio_ReadChannel(DioConf_DioChannel_Dobhs_Select);
      Dio_WriteChannel(DioConf_DioChannel_Dobhs_DignEnable, 
                       STD_HIGH);
      if(Dobhs_SelectStatus == STD_LOW)
      {
         Dio_WriteChannel(DioConf_DioChannel_Dobhs_Select, 
                          STD_HIGH);
      }
      else
      {
         Dio_WriteChannel(DioConf_DioChannel_Dobhs_Select,
                          STD_LOW);
      }
      flag = 1;
   }
   else
   {
      flag = 0;
   }         
}
//! @}
//! @}
//! @}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
