/*
 * Stubs.c
 *
 *  Created on: 18.05.2018
 *      Author: vissmd
 */
 
#include "Dcm.h"
#include "Os.h"
#include "CanTrcv_30_GenericCan.h"
#include "Icu.h"
#include "Dio.h"
#include "Mcu.h"
#include "Gpt.h"
#include "Port.h"
#include "Pwm.h"


#ifndef FBL_IOS
# define FBL_IOS(type, base, offset) (*((volatile type *)((base) + (offset))))
#endif

#define FBL_SIUL2_BASE     0xFFFC0000ul      /**< SIU lite base address */

# define FBL_SIUL2_GPDO(x)    FBL_IOS(uint8,  FBL_SIUL2_BASE, 0x1300ul + (x)) 


void Fls_AceessCodeCbkFunc(void)
{

 Dio_FlipChannel(DioConf_DioChannel_ExWdg_WDI);

}

void Wdg_Cbk_GptNotification0(void)
{

}

void GptChannel_RTC_notification(void)
{
   FBL_SIUL2_GPDO(13) ^= 0x01u;
}

FUNC(Std_ReturnType, DCM_CALLOUT_CODE) DcmDsdSidTabFnc_LinkControl(Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext)
{
   
}

void Os_Task_UntrustedAppl_Task(void)
{
   (void)TerminateTask();
}

void Os_Task_UntrustedAppl_Init_Task(void)
{
   (void)TerminateTask();
}

void CanTrcvWakeUpNotification_CAN2(void)
{
   //CanTrcv_30_GenericCan_CheckWakeup(CanTrcv_30_GenericCan_CanTrcvChannel_CAN2STB);
   Icu_DisableEdgeDetection(IcuConf_IcuChannel_IcuChannel_CAN2RX);
   Icu_DisableNotification(IcuConf_IcuChannel_IcuChannel_CAN2RX);
   EcuM_SetWakeupEvent(ECUM_WKSOURCE_CN_Backbone2_78967e2c);
}

void CanTrcvWakeUpNotification_CAN3(void)
{
   //CanTrcv_30_GenericCan_CheckWakeup(CanTrcv_30_GenericCan_CanTrcvChannel_CAN3STB);
   Icu_DisableEdgeDetection(IcuConf_IcuChannel_IcuChannel_CAN3RX);
   Icu_DisableNotification(IcuConf_IcuChannel_IcuChannel_CAN3RX);
   EcuM_SetWakeupEvent( ECUM_WKSOURCE_CN_CabSubnet_9ea693f1);
}

void CanTrcvWakeUpNotification_CAN4(void)
{
   //CanTrcv_30_GenericCan_CheckWakeup(CanTrcv_30_GenericCan_CanTrcvChannel_CAN4STB);
   Icu_DisableEdgeDetection(IcuConf_IcuChannel_IcuChannel_CAN4RX);
   Icu_DisableNotification(IcuConf_IcuChannel_IcuChannel_CAN4RX);
   EcuM_SetWakeupEvent(ECUM_WKSOURCE_CN_SecuritySubnet_e7a0ee54);
}

void CanTrcvWakeUpNotification_CAN5(void)
{
   //CanTrcv_30_GenericCan_CheckWakeup(CanTrcv_30_GenericCan_CanTrcvChannel_CAN5STB);
   //Icu_DisableEdgeDetection(IcuConf_IcuChannel_IcuChannel_CAN5RX);
   //Icu_DisableNotification(IcuConf_IcuChannel_IcuChannel_CAN5RX);
   //EcuM_SetWakeupEvent( ECUM_WKSOURCE_CN_FMSNet_fce1aae5 );
}

void LINWakeUpNotification_LIN1(void)
{
   Icu_DisableEdgeDetection(IcuChannel_LIN1_RX);	
   Icu_DisableNotification(IcuChannel_LIN1_RX);
   EcuM_SetWakeupEvent(ECUM_WKSOURCE_CN_LIN00_2cd9a7df);
}

void LINWakeUpNotification_LIN2(void)
{
   Icu_DisableEdgeDetection(IcuChannel_LIN2_RX);
   Icu_DisableNotification(IcuChannel_LIN2_RX);
   EcuM_SetWakeupEvent(ECUM_WKSOURCE_CN_LIN01_5bde9749);
}

void LINWakeUpNotification_LIN3(void)
{
   Icu_DisableEdgeDetection(IcuChannel_LIN3_RX);
   Icu_DisableNotification(IcuChannel_LIN3_RX);
   EcuM_SetWakeupEvent(ECUM_WKSOURCE_CN_LIN02_c2d7c6f3); 
}

void LINWakeUpNotification_LIN4(void)
{
   Icu_DisableEdgeDetection(IcuChannel_LIN4_RX);
   Icu_DisableNotification(IcuChannel_LIN4_RX);
   EcuM_SetWakeupEvent(ECUM_WKSOURCE_CN_LIN03_b5d0f665);
}

void LINWakeUpNotification_LIN5(void)
{
   Icu_DisableEdgeDetection(IcuChannel_LIN5_RX);
   Icu_DisableNotification(IcuChannel_LIN5_RX); 
   EcuM_SetWakeupEvent(ECUM_WKSOURCE_CN_LIN04_2bb463c6);
}

void IoHwAb_Init(void)
{

}

/* end of file */

