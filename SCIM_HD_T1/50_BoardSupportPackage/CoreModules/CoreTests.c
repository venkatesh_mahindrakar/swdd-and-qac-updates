/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  DiagnosticMonitor_Core.c
 *        Config:  C:/Personal/GIT/Prj/volvoscim/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  DiagnosticMonitor_Core
 *  Generated at:  Mon Apr 13 15:29:55 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <DiagnosticMonitor_Core>
 *********************************************************************************************************************/
 /**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "CoreTests.h"
#include "CoreTests_If.h"
#include "Os.h"
#include "Appl_RamTst.h"
#include "Rtm_Cbk.h"
#include "mpc5746c.h"
#include "StdRegMacros.h"

static uint8 CoreRAMTestFlag =0u;

#define DiagnosticMonitor_Core_START_SEC_CODE
#include "DiagnosticMonitor_Core_MemMap.h" 

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the CoreTest_CpuLoadInit
//! 
//! 
//!====================================================================================================================
void CoreTest_CpuLoadInit(void)
{ 
   //! ##### Processing CPU Load timer :'CoreTest_StartCpuLoadTimer()'
   CoreTest_StartCpuLoadTimer();
   //! ##### Processing CPU Load measurement :'Rtm_Start_CpuLoadMeasurement()'
   Rtm_Start_CpuLoadMeasurement(); 
   //! ##### Processing Task Activation:'ActivateTask()'
   (void)ActivateTask(CpuLoadIdleTask);
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the CoreTest_CpuLoadDeInit
//! 
//!====================================================================================================================
void CoreTest_CpuLoadDeInit(void)
{
   //! ##### Processing to stop CPU Load timer:'CoreTest_StopCpuLoadTimer()'
   CoreTest_StopCpuLoadTimer();
   //! ##### Processing to stop CPU Load measurement :'Rtm_Stop_CpuLoadMeasurement()'
   Rtm_Stop_CpuLoadMeasurement(); 
}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the CoreTest_GetCpuLoad
//! 
//! \param    *CpuLoad   To provide data
//! 
//!====================================================================================================================

uint8 CoreTest_GetCpuLoad(RtmCpuLoadDataType *CpuLoad)
{ 
   uint8 retval = RTE_E_INVALID;
   //! ###### Processing the cpu load
   //! ##### Checking the Cpu load measurement parameters
   if(CpuLoad != NULL_PTR)
   {
     (void)Rtm_GetMeasurementItem(RTM_CPU_LOAD_MEASUREMENT_ID,RTM_ITEM_CPU_LOAD_AVERAGE,&CpuLoad->Average);
     (void)Rtm_GetMeasurementItem(RTM_CPU_LOAD_MEASUREMENT_ID,RTM_ITEM_CPU_LOAD_CURRENT,&CpuLoad->Current);
     (void)Rtm_GetMeasurementItem(RTM_CPU_LOAD_MEASUREMENT_ID,RTM_ITEM_MIN,&CpuLoad->Min);
     (void)Rtm_GetMeasurementItem(RTM_CPU_LOAD_MEASUREMENT_ID,RTM_ITEM_MAX,&CpuLoad->Max);
      //! ####Based on the conditions Restarting measurement
      if(CpuLoad->Max>=100)
      {
          //! ##### Processing to stop CPU Load measurement :'Rtm_Stop_CpuLoadMeasurement()'
         Rtm_Stop_CpuLoadMeasurement();
          //! ##### Processing to start CPU Load measurement :'Rtm_Start_CpuLoadMeasurement()'
         Rtm_Start_CpuLoadMeasurement();
      }
     
      retval = RTE_E_OK;
   }
  else
  {
      retval = RTE_E_INVALID;
  }

 return retval;
}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the CoreTest_RAMTestStart
//! 
//! 
//!====================================================================================================================
uint8 CoreTest_RAMTestStart(void)
{

  uint8 retval = RTE_E_INVALID;
   //! ###### Processing to start the Ram test
  if(0U == CoreRAMTestFlag)
  {
      //! ##### Processing to Run Full RamTest :'RamTst_RunFullTest()'
      RamTst_RunFullTest();
      CoreRAMTestFlag = 1u;
      retval = RTE_E_OK;
  }
  else
  {
     retval = RTE_E_INVALID;
  }

 return retval;
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the CoreTest_GetRAMTestResults
//! 
//! \param  ErrorCode[1]    For reading Error code
//! 
//!====================================================================================================================

uint8 CoreTest_GetRAMTestResults(RamTst_TestResultType *Status)
{

  RamTst_ExecutionStatusType CoreRAMTestExecStatus = RAMTST_EXECUTION_UNINIT;
  uint8 retval = RTE_E_INVALID;
   //! ###### Processing to get results of RAM test
   //! ##### Check if the RamTst execution has stopped then get the result
   if(1U == CoreRAMTestFlag)
   {
      CoreRAMTestExecStatus = RamTst_GetExecutionStatus();

      if(CoreRAMTestExecStatus == RAMTST_EXECUTION_STOPPED)
      {
         *Status = RamTst_GetTestResult();
         CoreRAMTestFlag = 0U;
         retval = RTE_E_OK;
      }
   }
   else
   {
      retval = RTE_E_INVALID;
   }

 return retval;

}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the CoreTest_GetStackUsage
//! 
//! \param  *StackUasage    For reading data
//! 
//!====================================================================================================================
void CoreTest_GetStackUsage(CoreStackUsage_T *StackUasage)
{
  TaskType TaskId;
  ISRType  IsrId;
  CoreIdType CoreId = OS_CORE_ID_MASTER;
  //! ##### Based on TaskID checking for Stack usage
  for(TaskId=0U;TaskId<OS_TASKID_COUNT;TaskId++)
  {
    StackUasage->TaskStack[TaskId] = Os_GetTaskStackUsage(TaskId);
  }
  
  for(IsrId=0U;IsrId<OS_ISRID_COUNT;IsrId++)
  {
    //StackUasage->IsrStack[IsrId] = Os_GetISRStackUsage(IsrId);
  }

   //! ##### Reading the Stack usage based on active Os hooks 
  StackUasage->KernalStack         = Os_GetKernelStackUsage(CoreId);

  #if(OS_CFG_STARTUPHOOKS == STD_ON)
  StackUasage->StartUpHookStack    = Os_GetStartupHookStackUsage(CoreId);
  #else
  StackUasage->StartUpHookStack    = 0U;
  #endif

  #if(OS_CFG_SHUTDOWNHOOKS == STD_ON)
  StackUasage->ShutdownHookStack   = Os_GetShutdownHookStackUsage(CoreId);
  #else
  StackUasage->ShutdownHookStack   = 0U;
  #endif
  
  #if(OS_CFG_ERRORHOOKS == STD_ON)
  StackUasage->ErrorHookStack      = Os_GetErrorHookStackUsage(CoreId);
  #else
  StackUasage->ErrorHookStack      = 0U;
  #endif
  
  #if(OS_CFG_PROTECTIONHOOK == STD_ON)
  StackUasage->ProtectionHookStack = Os_GetProtectionHookStackUsage(CoreId);
  #else
  StackUasage->ProtectionHookStack = 0U;
  #endif

}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the CoreTest_StartCpuLoadTimer
//! 
//! 
//!====================================================================================================================
static void CoreTest_StartCpuLoadTimer(void)
{
   //! #####Setting the registers to start the timer for CpuLoad
	STM_1.CR.B.FRZ = 1;
	STM_1.CR.B.TEN = 0;
	STM_1.CR.B.CSL = 1;  
	STM_1.CR.B.CPS = 3; 
	STM_1.CR.B.TEN = 1;
}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the CoreTest_StopCpuLoadTimer
//! 
//! 
//!====================================================================================================================

static void CoreTest_StopCpuLoadTimer(void)
{
   //! #####Setting the registers to stop the timer for CpuLoad
	STM_1.CR.B.FRZ = 1;
	STM_1.CR.B.TEN = 0;
}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the Rtm_GetTimeMeasurement
//! 
//! \param  ErrorCode[1]    For reading Error code
//! 
//!====================================================================================================================

// RTM Module Callout Functions
Rtm_TimestampType Rtm_GetTimeMeasurement(void)
{
	Rtm_TimestampType retval;
	uint32 u32CounterValue;
   //! ##### Reading counter value
	u32CounterValue = STM_1.CNT.R;

	retval = (Rtm_TimestampType)(0xFFFFFFFFU & u32CounterValue);

	return retval;
}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This Task performs calling of Rtm_CpuLoadMeasurementFunction
//! 
//! 
//!====================================================================================================================

TASK(CpuLoadIdleTask)
{
	for(;;)
	{
		Rtm_CpuLoadMeasurementFunction();
	}
}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#define DiagnosticMonitor_Core_STOP_SEC_CODE
#include "DiagnosticMonitor_Core_MemMap.h"
