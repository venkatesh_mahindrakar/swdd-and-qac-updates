   .file        "IvorAsmHandler.asm"

    .extern IvorHandler
    .global Ivor1AsmHandler
    .global Ivor2AsmHandler
    .global UnhandledException
    .global RamExceptionHandler

    .text  ; code section
    .align 2


; Exception handler. Initialize the RAM 128-bits block that caused the exception
; r3 = data address that failed
RamExceptionHandler:
	se_li		r4, 0xF
	se_andc		r3, r4						; 128-bit align
	se_mr		r4, r28						; save r28
	se_mr		r5, r29						; save r29
	se_mr		r6, r30						; save r30
	se_mr		r7, r31						; save r31
	e_li		r28, 0						; r28 = 0
	e_li		r29, 0						; r29 = 0
	e_li		r30, 0						; r30 = 0
	e_li		r31, 0						; r31 = 0
	e_stmw      r28,0(r3)					; write r28 .. r31 to RAM block
	se_mr		r28, r4						; restore r28
	se_mr		r29, r5						; restore r29
	se_mr		r30, r6						; restore r30
	se_mr		r31, r7						; restore r31
	se_blr

UnhandledException:
; This is temporary during development
_loop_forever:
   e_b       _loop_forever








Ivor1AsmHandler:
    stwu sp, -64(sp)     ; Create EABI compliant stack frame and store old stack pointer - preserve 16-Byte alignment
                         ; note that sp+4 must remain unused here - it gets LR if a called function calls another function.
    stw   r0, 8(sp)      ; Save r0
    mflr  r0             ; save lr
    stw   r0, 60(sp)     ; since we don't have an actual caller (its an exception!) we have to save it in our own stack frame.

    mfxer r0
    stw   r0, 56(sp)
    mfctr r0
    stw   r0, 52(sp)

    stw r3, 12(sp)       ; save GPRs r3-r12, as they are not preserved by C Functions
    stw r4, 16(sp)
    stw r5, 20(sp)
    stw r6, 24(sp)
    stw r7, 28(sp)
    stw r8, 32(sp)
    stw r9, 36(sp)
    stw r10, 40(sp)
    stw r11, 44(sp)
    stw r12, 48(sp)

    ; if more values need to be saved on stack, increase stack frame an put them above the registers


    ; prepare function call - set parameters for Ivor1CHandler(instrAddress, dataAddress, syndrome)
    mfmcsr   r5       ; get MCSR contents
    mtmcsr   r5       ; clear MCSR
    mfmcar   r4       ; get data address
    mfmcsrr0 r3       ; get causing instruction address

    bl IvorHandler  ; call our high-level handler

    mtmcsrr0 r3       ; set the new return adress - we got it from the handler (return value).

    ; restore GPRs (we do not need to reverse the order as usual, because we do not use typical stack operations)
    lwz r3, 12(sp)
    lwz r4, 16(sp)
    lwz r5, 20(sp)
    lwz r6, 24(sp)
    lwz r7, 28(sp)
    lwz r8, 32(sp)
    lwz r9, 36(sp)
    lwz r10, 40(sp)
    lwz r11, 44(sp)
    lwz r12, 48(sp)

    lwz r0, 52(sp)
    mtctr r0
    lwz r0, 56(sp)
    mtxer r0

    lwz r0, 60(sp)   ; get lr value from stack
    mtlr r0          ; restore lr

    lwz r0, 8(sp)    ; restore r0

    addi sp, sp, 64  ; clean-up local stack-frame -> restore original sp
                     ; we do not use the sp value stored on stack

    rfmci            ; Return from machine check interrupt

;Ivor2AsmHandler:
;    stwu sp, -64(sp)     ; Create EABI compliant stack frame and store old stack pointer - preserve 16-Byte alignment
;                         ; note that sp+4 must remain unused here - it gets LR if a called function calls another function.
;    stw   r0, 8(sp)      ; Save r0
;    mflr  r0             ; save lr
;    stw   r0, 60(sp)     ; since we don't have an actual caller (its an exception!) we have to save it in our own stack frame.
;
;    mfxer r0
;    stw   r0, 56(sp)
;    mfctr r0
;    stw   r0, 52(sp)
;
;    stw r3, 12(sp)       ; save GPRs r3-r12, as they are not preserved by C Functions
;    stw r4, 16(sp)
;    stw r5, 20(sp)
;    stw r6, 24(sp)
;    stw r7, 28(sp)
;    stw r8, 32(sp)
;    stw r9, 36(sp)
;    stw r10, 40(sp)
;    stw r11, 44(sp)
;    stw r12, 48(sp)
;
;    ; if more values need to be saved on stack, increase stack frame an put them above the registers
;
;
;    ; prepare function call - set parameters for Ivor1CHandler(instrAddress, dataAddress, syndrome)
;    mfesr   r5       ; get MCSR contents
;    mfdear  r4       ; get data address
;    mfsrr0  r3       ; get causing instruction address
;
;    bl IvorHandler  ; call our high-level handler
;
;    mtsrr0 r3         ; set the new return adress - we got it from the handler (return value).
;
;    ; restore GPRs (we do not need to reverse the order as usual, because we do not use typical stack operations)
;    lwz r3, 12(sp)
;    lwz r4, 16(sp)
;    lwz r5, 20(sp)
;    lwz r6, 24(sp)
;    lwz r7, 28(sp)
;    lwz r8, 32(sp)
;    lwz r9, 36(sp)
;    lwz r10, 40(sp)
;    lwz r11, 44(sp)
;    lwz r12, 48(sp)
;
;    lwz r0, 52(sp)
;    mtctr r0
;    lwz r0, 56(sp)
;    mtxer r0
;
;    lwz r0, 60(sp)   ; get lr value from stack
;    mtlr r0          ; restore lr
;
;    lwz r0, 8(sp)    ; restore r0
;
;    addi sp, sp, 64  ; clean-up local stack-frame -> restore original sp
;                     ; we do not use the sp value stored on stack
;
;    rfi              ; Return from interrupt


