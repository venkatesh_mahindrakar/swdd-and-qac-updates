/******************************************************************************
 * COPYRIGHT (c) Volvo Corporation 2017
 *
 * The copyright of the computer program(s) herein is the property of Volvo
 * Corporation, Sweden. The programs may be used and copied only with the
 * written permission from Volvo Corporation, or in accordance with the terms
 * and conditions stipulated in the agreement contract under which the
 * program(s) have been supplied.
 *
 *****************************************************************************/
 
 /**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//////////////////////////////////////////////////////////////////////////////
// INCLUDES
//////////////////////////////////////////////////////////////////////////////


#include "IvorHandler.h"

extern void UnhandledException(void);
extern void RamExceptionHandler(uint32 failedAddress);

//////////////////////////////////////////////////////////////////////////////
// CONSTANTS AND DATA TYPES
//////////////////////////////////////////////////////////////////////////////
#define INTERNAL_RAM_START_ADDRESS     0x40000000
#define INTERNAL_RAM_END_ADDRESS       0x4007FFFF

//////////////////////////////////////////////////////////////////////////////
// VARIABLES
//////////////////////////////////////////////////////////////////////////////



extern uint8 SCIM_FaultInjectionFlag;




/****************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the IvorHandler
//! 
//! \param  *instructionAddr     To read data
//! \param  dataAddress          To provide data address
//! \param  syndrome             To provid data
//! 
//!====================================================================================================================

Fls_InstructionAddressType IvorHandler(Fls_InstructionAddressType instructionAddr,
                                       uint32                     dataAddress,
                                       uint32                     syndrome)
{
   Fls_CompHandlerReturnType handled;
   Fls_ExceptionDetailsType  excDetails;
   //! ##### Checking if the data address is in range and processing accordingly
   if ((dataAddress >= INTERNAL_RAM_START_ADDRESS)
       &&(dataAddress < INTERNAL_RAM_END_ADDRESS))
   {
      RamExceptionHandler(dataAddress);
      MemoryExceptionHandler();
      ResetProcess_TriggerReset(ResetCause_SRAM_ECC);
   }
   else if ((dataAddress==Flash_ECC_TestAddr)
            &&(1U==SCIM_FaultInjectionFlag))
   {
      SCIM_FaultInjectionFlag   = 0U;
      excDetails.instruction_pt = instructionAddr;
      excDetails.data_pt        = (Fls_DataAddressType)dataAddress;
      excDetails.syndrome_u32   = syndrome;

     
      instructionAddr = getNextInstructionAddress(&excDetails);
      MemoryExceptionHandler();
	  ResetProcess_TriggerReset(ResetCause_FLASH_ECC);
   }
   else
   {
      excDetails.instruction_pt = instructionAddr;
      excDetails.data_pt        = (Fls_DataAddressType)dataAddress;
      excDetails.syndrome_u32   = syndrome;

      handled = Fls_DsiHandler(&excDetails);

      if (handled == FLS_HANDLED_SKIP)
      {
         instructionAddr = getNextInstructionAddress(&excDetails);
         MemoryExceptionHandler();
      }
      else
      {
         MemoryExceptionHandler();
         ResetProcess_TriggerReset(ResetCause_FLASH_ECC);
      }
   }

   return instructionAddr;
}


//////////////////////////////////////////////////////////////////////////////
// PRIVATE FUNCTIONS
//////////////////////////////////////////////////////////////////////////////



/****************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the getNextInstructionAddress
//! 
//! \param  *excDetails     To read data
//! 
//!====================================================================================================================

static Fls_InstructionAddressType getNextInstructionAddress(Fls_ExceptionDetailsType  *excDetails)
{
   //  Note that in case of jump instructions the returned address is NOT address of the jump target!
   //! #####   This function determines the address of the instruction following the causing instruction.
   Fls_InstructionAddressType instrAddress = excDetails->instruction_pt;
   const uint8_least opCode = (*instrAddress) & 0x90u;  /* use the first byte of the instruction */
   Fls_InstructionAddressType nextaddress =NULL_PTR;

   //! ##### Assume VLE section and check if the opCode is a 32 bits VLE
   if (opCode == 0x10)
   {
      // most significant 4 Bits have a value of 1,3,5,7
      // instruction was 32 bit
      nextaddress = &instrAddress[4];
   }
   else
   {
      // most significant 4 Bits have a value of 0,2,4,6,8,9,A,B,C,D,E (and F, which is reserved)
      // instruction was 16 bit
      nextaddress = &instrAddress[2];
   }

   return nextaddress;
}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/