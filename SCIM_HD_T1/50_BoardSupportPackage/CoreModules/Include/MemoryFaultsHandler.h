//!======================================================================================
//!
//! \DiagnosticComponent.h
//!
//!======================================================================================
 
#include "Rte_Type.h" 

//=======================================================================================
// Public macros
//=======================================================================================

#define Inject_1Bit_EccError     0x1
#define Inject_2Bit_EccError     0x2

#define MAX_ECCERROR_FLAG   10U


//=======================================================================================
// Public data declarations
//=======================================================================================

typedef enum
{
 MemoryType_None = 0,
 MemoryType_SystemRAM,
 MemoryType_Flash
}Memory_T;

typedef enum
{
 MemoryFault_None = 0,
 MemoryFault_Inject_1b_ECCFault,
 MemoryFault_Inject_2b_ECCFault
}MemoryFault_T;

typedef struct
{
 Memory_T Memory;
 MemoryFault_T FaultInjection;
 uint8 StartFlag;
}MemoryFaultInject_T;




//=======================================================================================
// Public function prototypes
//=======================================================================================


static void FlashUnCorrected_FaultHandling(void);
static void FlashCorrected_FaultHandling(void);
static void SystemRamUnCorrected_FaultHandling(void);
static void SystemRamCorrected_FaultHandling(void);
static void MEMU_ExceptionHandler(void);
static void MEMU_PeriodicHandler(void);
static uint8 MemoryFaultInject(Memory_T MemoryType, MemoryFault_T FaultInjectionType);
static void MemoryFaultInjectTest(void);



//=======================================================================================
// End of file
//=======================================================================================
