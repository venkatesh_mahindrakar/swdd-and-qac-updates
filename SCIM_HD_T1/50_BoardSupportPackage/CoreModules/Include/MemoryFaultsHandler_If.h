//!======================================================================================
//!
//! \DiagnosticComponent.h
//!
//!======================================================================================
 
#include "Rte_Type.h" 

//=======================================================================================
// Public macros
//=======================================================================================


//=======================================================================================
// Public data declarations
//=======================================================================================

typedef struct
{
uint32 ErrFlag;
uint32 Address;
}EccFaultInfo_T;


typedef struct
{
EccFaultInfo_T Sram;
EccFaultInfo_T Fls;
}UnRecoveredFaultInfo_T;

typedef struct
{
EccFaultInfo_T Sram;
EccFaultInfo_T Fls;
}RecoveredFaultInfo_T;

//=======================================================================================
// Public function prototypes
//=======================================================================================

void  MemoryFaultHandler_Init(void);
void MemoryExceptionHandler(void);
void MemoryFaultHandler_10ms_Runnable(void);
void MemoryFaultHandler_GetStatus(UnRecoveredFaultInfo_T *UnRecvFault,RecoveredFaultInfo_T *RecvFault);

//=======================================================================================
// End of file
//=======================================================================================