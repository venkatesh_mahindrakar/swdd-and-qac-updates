//!======================================================================================
//!
//! \DiagnosticComponent.h
//!
//!======================================================================================
 
#include "Rte_Type.h" 
#include "Rtm.h"
#include "RamTst.h"
#include "Os.h"

//=======================================================================================
// Public macros
//=======================================================================================




//=======================================================================================
// Public data declarations
//=======================================================================================

typedef struct
{
 uint32 Average;
 uint32 Current;
 uint32 Min;
 uint32 Max;
}RtmCpuLoadDataType;


typedef struct
{
	uint32 TaskStack[OS_TASKID_COUNT];
	uint32 IsrStack[OS_ISRID_COUNT];
	uint32 KernalStack;
	uint32 StartUpHookStack;
	uint32 ErrorHookStack;
	uint32 ShutdownHookStack;
	uint32 ProtectionHookStack;
}CoreStackUsage_T;



//=======================================================================================
// Public function prototypes
//=======================================================================================

void CoreTest_CpuLoadInit(void);
void CoreTest_CpuLoadDeInit(void);
uint8 CoreTest_GetCpuLoad(RtmCpuLoadDataType *CpuLoad);
uint8 CoreTest_RAMTestStart(void);
uint8 CoreTest_GetRAMTestResults(RamTst_TestResultType *Status);
void CoreTest_GetStackUsage(CoreStackUsage_T *StackUasage);

//=======================================================================================
// End of file
//=======================================================================================