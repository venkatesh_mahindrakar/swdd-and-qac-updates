//!======================================================================================
//!
//! \ResetProcess_If.h
//!
//!======================================================================================
 
#include "Rte_Type.h" 

//=======================================================================================
// Public macros
//=======================================================================================
typedef struct {
	uint16 ModuleID;
	uint8  ErrorID;
}ModuleInfo_Type;

typedef struct
{
  uint16 mModuleId;
  uint8 mInstanceId;
  uint8 mApiId;
  uint8 mErrorId;
} Det_InfoType;

typedef enum{
   ResetCause_None=0,			   // value 0
   ResetCause_PowerOnReset,	       // value 1
   ResetCause_VoltReg_LVD,	       // value 2
   ResetCause_VoltReg_HVD,	       // value 3
   ResetCause_Clock_Failure,	   // value 4
   ResetCause_Diagnostic_Reset,	   // value 5
   ResetCause_External_Reset,	   // value 6
   ResetCause_Det_Error,		   // value 7
   ResetCause_Protection_Hook,	   // value 8
   ResetCause_WDGM_Error,		   // value 9
   ResetCause_SRAM_ECC,		       // value 10
   ResetCause_FLASH_ECC,		   // value 11
   ResetCause_PLL_Error,		   // value 12
   ResetCause_Others			   // value 13
}ResetCausetypes;

//=======================================================================================
// Public data declarations
//=======================================================================================



//=======================================================================================
// Public function prototypes
//=======================================================================================
ResetCausetypes ResetProcess_Init(void);
void ResetProcess_TriggerReset(ResetCausetypes Reset_Type);
Std_ReturnType Det_report_ForReset( uint16 ModuleId, uint8 InstanceId, uint8 ApiId, uint8 ErrorId );

//=======================================================================================
// End of file
//=======================================================================================