/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  DiagnosticMonitor_Core.c
 *        Config:  C:/Personal/GIT/Prj/volvoscim/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  DiagnosticMonitor_Core
 *  Generated at:  Mon Apr 13 15:29:55 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <DiagnosticMonitor_Core>
 *********************************************************************************************************************/
 
 
 /**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "MemoryTests.h"
#include "MemoryTests_If.h"
#include "Rtm_Cbk.h"
#include "mpc5746c.h"
#include "MPC5746C_MEMU_macros.h"
#include "StdRegMacros.h"
#include "Dio.h"


static uint32 ExtWdgTrigCnt = 0u;
static uint32 ExtWdgTrigThr = 100u;

static volatile uint32 FlashTest2 = 0U;
static volatile uint32 FlashTest1 = 0U;
static volatile uint32 SRAMTest2 = 0U;
static volatile uint32 SRAMTest1 = 0U;


#define DiagnosticMonitor_Core_START_SEC_CODE
#include "DiagnosticMonitor_Core_MemMap.h" 

#define EnableIntr asm(" wrteei 1")

//Macro for Assembly code  asm(" wrteei 1");

#define DisableIntr() asm(" wrteei 0")
//Macro for assembly code asm(" wrteei 0");
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the Inject_RAM_EccError
//! 
//! \param  Bits     To read data
//! 
//!====================================================================================================================

void Inject_RAM_EccError(uint8 Bits)
{
   register uint32 test_read;	
   //! ##### Based on the bits injecting error
   if(Bits == Inject_1Bit_EccError)
   {
      test_read = 0;
      //asm(" wrteei 0"); ---Disable external interrupts 
      asm("e_lis r4, 0x0000");
      asm("mtdcr 511, r4");
      asm("e_add16i r4,r4,0x1001");
      asm("mtdcr 511, r4");
      //asm(" wrteei 1"); --- Enable external interrupts 
   }
   else if(Bits == Inject_2Bit_EccError)
   {
      test_read = 0;	
      //asm(" wrteei 0"); ---Disable external interrupts 
      asm("e_lis r4, 0x0000");
      asm("mtdcr 511, r4");
      asm("e_add16i r4,r4,0x1003");
      asm("mtdcr 511, r4");
      // asm(" wrteei 1"); ---- Enable external interrupts 
   }
   else
   {


   }
   SRAMTest1 = SRAMTest2;
   test_read = SRAMTest1;

   asm("e_lis r4, 0x0000");
   asm("mtdcr 511, r4");
  
}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the Inject_noncorrectable_FLASH_ECC_error
//! 
//! 
//!====================================================================================================================

void Inject_noncorrectable_FLASH_ECC_error(void)
{
   register uint32 test_read = 0;

   //! ##### Processing for noncorrectable Flash error
   /* Generate flash ECC error */
   DisableIntr(); /* Disable external interrupts */
   // step1. unlock all blocks (for simplicity)
   C55FMC.LOCK0.R = 0;
   C55FMC.LOCK1.R = 0;
   C55FMC.LOCK2.R = 0;
   C55FMC.LOCK3.R = 0;

   // step2. erase the large block 9 (0x01240000 - 0x012FFFFF)
   C55FMC.MCR.B.ERS = 1;
   C55FMC.SEL2.R = 0x00000200; // select the large block 9
   *(unsigned int*)Flash_ECC_TestAddr = 0xFFFFFFFFU;    //interlock write
   C55FMC.MCR.B.EHV = 1;
   //! #### Processing waiting for flash :'WaitForFlashOperation()'
   WaitForFlashOperation();
   C55FMC.MCR.B.EHV = 0;
   C55FMC.MCR.B.ERS = 0;

   // step3. program data
   C55FMC.MCR.B.PGM = 1;
   *(unsigned int*)Flash_ECC_TestAddr = 0x00450000;    //interlock write
   *(unsigned int*)Flash_ECC_TestNextAddr = 0x00000000;

   C55FMC.MCR.B.EHV = 1;
   //! #### Processing waiting for flash :'WaitForFlashOperation()'
   WaitForFlashOperation();
   C55FMC.MCR.B.EHV = 0;
   C55FMC.MCR.B.PGM = 0;

   // step4. over-program data - this generates ECC error
   C55FMC.MCR.B.PGM = 1;
   *(unsigned int*)Flash_ECC_TestAddr = 0x00580000;    //interlock write
   *(unsigned int*)Flash_ECC_TestNextAddr = 0x00000000;   
   C55FMC.MCR.B.EHV = 1;
   //! #### Processing waiting for flash :'WaitForFlashOperation()'
   WaitForFlashOperation();



   C55FMC.MCR.B.EHV = 0;
   C55FMC.MCR.B.PGM = 0;
   EnableIntr; /* Enable external interrupts */
   // step 5. now here the ECC is checked and non-correctable error found
   test_read = *(unsigned int*)Flash_ECC_TestAddr;
   FlashTest1 = test_read;
   FlashTest2 = FlashTest1;
   //test_read = *(unsigned int*)0x011C0004;
    
}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the Inject_1bit_FLASH_ECC_error
//! 
//! 
//!====================================================================================================================

void Inject_1bit_FLASH_ECC_error(void)
{
   register uint32 test_read = 0;
   //! ##### Processing for 1 bit flash error
   /* Enable single bit ECC error reporting in flash controller */
   DisableIntr(); /* Disable external interrupts */
   // Enable UTest mode
   C55FMC.UT0.R = 0xF9F99999U;
   // Enable single bit error correction
   C55FMC.UT0.B.SBCE = 1;
   // Finish the UTest mode by writing UT0[UTE] with 0.
   C55FMC.UT0.B.UTE = 0;

   /* Now generate flash ECC error */

   // step1. unlock all blocks (for simplicity)
   C55FMC.LOCK0.R = 0;
   C55FMC.LOCK1.R = 0;
   C55FMC.LOCK2.R = 0;
   C55FMC.LOCK3.R = 0;

   // step2. erase the large block  9 (0x01240000 - 0x012FFFFF)
   C55FMC.MCR.B.ERS = 1;
   C55FMC.SEL2.R = 0x00000200; // select the large block 9
   *(unsigned int*)Flash_ECC_TestAddr = 0xFFFFFFFFU;    //interlock write
   C55FMC.MCR.B.EHV = 1;
   WaitForFlashOperation();
   C55FMC.MCR.B.EHV = 0;
   C55FMC.MCR.B.ERS = 0;

   // step3. program data
   C55FMC.MCR.B.PGM = 1;
   *(unsigned int*)Flash_ECC_TestAddr     = 0xFFFFFFFFU;    //interlock write
   *(unsigned int*)Flash_ECC_TestNextAddr = 0x00000000;

   C55FMC.MCR.B.EHV = 1;
   WaitForFlashOperation();
   C55FMC.MCR.B.EHV = 0;
   C55FMC.MCR.B.PGM = 0;

   // step4. over-program data - this generates ECC error
   C55FMC.MCR.B.PGM = 1;
   *(unsigned int*)Flash_ECC_TestAddr     = 0xFFFFFFFFU;    //interlock write
   *(unsigned int*)Flash_ECC_TestNextAddr = 0x00000001;   
   C55FMC.MCR.B.EHV = 1;
   WaitForFlashOperation();
   C55FMC.MCR.B.EHV = 0;
   C55FMC.MCR.B.PGM = 0;
   EnableIntr; /* Enable external interrupts */
   // step 5. now here the ECC is checked and non-correctable error found
   test_read = *(unsigned int*)Flash_ECC_TestAddr;
   FlashTest2 = test_read;
   FlashTest1 = FlashTest2;
   //test_read = *(unsigned int*)0x011C0004;

}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the Fix_2bit_error_FLASH_data
//! 
//! 
//!====================================================================================================================

void Fix_2bit_error_FLASH_data(void)
{
   //  erase block as example of data correction (application specific)
   //! #### Processing erase block :'Erase_L7_block()'
   Erase_L7_block();
}


//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the Erase_L7_block
//! 
//! 
//!====================================================================================================================
static void Erase_L7_block(void)
{
   //! ##### Processing for erasing a block
   //asm(" wrteei 0"); -- Disable external interrupts 
   // step1. unlock all blocks (for simplicity)
   C55FMC.LOCK0.R = 0;
   C55FMC.LOCK1.R = 0;
   C55FMC.LOCK2.R = 0;
   C55FMC.LOCK3.R = 0;

   // step2. erase the large block 9 (0x01240000 - 0x012FFFFF)
   C55FMC.MCR.B.ERS = 1;
   C55FMC.SEL2.R = 0x00000200; // select the large block 9
   *(unsigned int*)Flash_ECC_TestAddr = 0xFFFFFFFFU;    //interlock write
   C55FMC.MCR.B.EHV = 1;
   WaitForFlashOperation();
   C55FMC.MCR.B.EHV = 0;
   C55FMC.MCR.B.ERS = 0;
   //asm(" wrteei 1"); ---Enable external interrupts 
}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the WaitForFlashOperation
//! 
//! 
//!====================================================================================================================
static void WaitForFlashOperation(void)
{
   //! Proessing wait for Flash operation
	while(C55FMC.MCR.B.DONE == 0)
	{
	   ExtWdgTrigCnt++;
	   if (ExtWdgTrigCnt >= ExtWdgTrigThr)
	   {
	     (void)Dio_FlipChannel(DioConf_DioChannel_ExWdg_WDI);//External Watchdog Trigger
	     ExtWdgTrigCnt =0;
	   }	 
	}
}

#define DiagnosticMonitor_Core_STOP_SEC_CODE
#include "DiagnosticMonitor_Core_MemMap.h"
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/