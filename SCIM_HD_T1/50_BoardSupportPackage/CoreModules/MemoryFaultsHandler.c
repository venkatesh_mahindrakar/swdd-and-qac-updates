/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  DiagnosticMonitor_Core.c
 *        Config:  C:/Personal/GIT/Prj/volvoscim/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  DiagnosticMonitor_Core
 *  Generated at:  Mon Apr 13 15:29:55 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <DiagnosticMonitor_Core>
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "MemoryFaultsHandler.h"
#include "MemoryFaultsHandler_If.h" 
#include "MemoryTests_If.h"
#include "Rtm_Cbk.h"
#include "mpc5746c.h"
#include "MPC5746C_MEMU_macros.h"
#include "StdRegMacros.h"
#include "Dio.h"


static uint32 SCIM_MEMU_ERR_FLAG = 0U;
static MemoryFaultInject_T SCIM_MemoryFaultInject;
static UnRecoveredFaultInfo_T SCIM_UnRcvrECCFaultInfo = {{0u,0u},{0u,0u}};
static RecoveredFaultInfo_T   SCIM_RcvrECCFaultInfo[MAX_ECCERROR_FLAG];
 

uint8 SCIM_FaultInjectionFlag=0U;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#define DiagnosticMonitor_Core_START_SEC_CODE
#include "DiagnosticMonitor_Core_MemMap.h" 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the FlashUnCorrected_FaultHandling
//!         
//! 
//!====================================================================================================================
static void FlashUnCorrected_FaultHandling(void)
{
   //! ##### Based on conditions checking for uncorrect flash
	if ((SCIM_MEMU_ERR_FLAG & (uint32)MEMU_ERR_FLAG_F_UCE) 
      ||((uint32)(SCIM_MEMU_ERR_FLAG & (uint32)MEMU_ERR_FLAG_F_UCO))) 
	{
		if (1 == MEMU.FLASH_UNCERR_STS.B.VLD)
		{
			SCIM_UnRcvrECCFaultInfo.Fls.Address = MEMU.FLASH_UNCERR_ADDR.R;
			SCIM_UnRcvrECCFaultInfo.Fls.ErrFlag = SCIM_MEMU_ERR_FLAG;
		   
		    // invalidate the entryss
		    MEMU.FLASH_UNCERR_STS.B.VLD = 0x0;
		    // remove the reson of ECC error (application specific)
		    Fix_2bit_error_FLASH_data();
		} 
		MEMU.ERR_FLAG.R = (uint32)MEMU_ERR_FLAG_F_UCE|(uint32)MEMU_ERR_FLAG_F_UCO; // clear Error flags
	}

}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the FlashCorrected_FaultHandling
//!      
//! 
//!====================================================================================================================
static void FlashCorrected_FaultHandling(void)
{
   uint8 Index;
   //! ##### Based on conditions checking for correct flash and clearing Error flags
	if((SCIM_MEMU_ERR_FLAG & (uint32)MEMU_ERR_FLAG_F_CE)
	||(SCIM_MEMU_ERR_FLAG & (uint32)MEMU_ERR_FLAG_F_CEO))
	{

		for(Index=0;Index<MAX_ECCERROR_FLAG;Index++)
		{
			if (1 == MEMU.FLASH_CERR[Index].STS.B.VLD)
			{
				SCIM_RcvrECCFaultInfo[Index].Fls.Address = MEMU.FLASH_CERR[Index].ADDR.R;
				SCIM_RcvrECCFaultInfo[Index].Fls.ErrFlag = SCIM_MEMU_ERR_FLAG;
				
				// invalidate the entry
				MEMU.FLASH_CERR[Index].STS.B.VLD = 0x0;
			}
		}
		MEMU.ERR_FLAG.R = (uint32)MEMU_ERR_FLAG_F_CE|(uint32)MEMU_ERR_FLAG_F_CEO; // clear Error flags

	}
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the SystemRamUnCorrected_FaultHandling
//!           
//! 
//!====================================================================================================================
static void SystemRamUnCorrected_FaultHandling(void)
{
   //! ##### Based on conditions checking for Uncorrect SystemRam
	if((SCIM_MEMU_ERR_FLAG & (uint32)MEMU_ERR_FLAG_SR_UCE)
	  ||(SCIM_MEMU_ERR_FLAG & (uint32)MEMU_ERR_FLAG_SR_UCO))
	{
		if (1 == MEMU.SYS_RAM_UNCERR_STS.B.VLD)
		{
			SCIM_UnRcvrECCFaultInfo.Sram.Address = MEMU.SYS_RAM_UNCERR_ADDR.R;
			SCIM_UnRcvrECCFaultInfo.Sram.ErrFlag = SCIM_MEMU_ERR_FLAG;

			// invalidate the entry
			MEMU.SYS_RAM_UNCERR_STS.B.VLD = 0x0;
		}	
		MEMU.ERR_FLAG.R = (uint32)MEMU_ERR_FLAG_SR_UCE|(uint32)MEMU_ERR_FLAG_SR_UCO; // clear Error flags
	}
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the SystemRamCorrected_FaultHandling
//!        
//! 
//!====================================================================================================================
static void SystemRamCorrected_FaultHandling(void)
{
   uint8 Index=0;
   //! ##### Based on conditions checking for correct SystemRam and clearing Error flags
	if((SCIM_MEMU_ERR_FLAG & (uint32)MEMU_ERR_FLAG_SR_CE) 
	 ||(SCIM_MEMU_ERR_FLAG & (uint32)MEMU_ERR_FLAG_SR_CEO))
	{
      for(Index=0;Index<MAX_ECCERROR_FLAG;Index++)
      {
		  if (1 == MEMU.SYS_RAM_CERR[Index].STS.B.VLD)
		  {
			  SCIM_RcvrECCFaultInfo[Index].Sram.Address = MEMU.SYS_RAM_CERR[Index].ADDR.R;
			  SCIM_RcvrECCFaultInfo[Index].Sram.ErrFlag = SCIM_MEMU_ERR_FLAG;
			  // invalidate the entry
			  MEMU.SYS_RAM_CERR[Index].STS.B.VLD = 0x0;
		  }
		  
		  MEMU.ERR_FLAG.R = ((uint32)MEMU_ERR_FLAG_SR_CE|(uint32)MEMU_ERR_FLAG_SR_CEO); // clear Error flags
	  }

	}

}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the MEMU_PeriodicHandler
//!          
//! 
//!====================================================================================================================

static void MEMU_PeriodicHandler(void)
{  

	SCIM_MEMU_ERR_FLAG = MEMU.ERR_FLAG.R;

   //! ##### Based on conditions checking and calling for correct SystemRam and Flash
	if (SCIM_MEMU_ERR_FLAG & (uint32)MEMU_ERR_FLAG_SYSRAM_all)	  
	{
	  SystemRamCorrected_FaultHandling();
	}


	if (SCIM_MEMU_ERR_FLAG & (uint32)MEMU_ERR_FLAG_FLASH_all)	
	{
		FlashCorrected_FaultHandling();
	}

	/* check other possible sources */
	if (SCIM_MEMU_ERR_FLAG & (uint32)MEMU_ERR_FLAG_PERRAM_all)	  
	{
		
	}

	//MEMU.CTRL.B.SWR = 0x1;
	/* clear all MEMU error flag flags to stop FCCU fault indication */
	//MEMU.ERR_FLAG.R = 0xFFFFFFFF;

}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the MEMU_ExceptionHandler
//!          
//! 
//!====================================================================================================================
static void MEMU_ExceptionHandler(void)
{  

   SCIM_MEMU_ERR_FLAG = MEMU.ERR_FLAG.R;


   if (SCIM_MEMU_ERR_FLAG & (uint32)MEMU_ERR_FLAG_SYSRAM_all)    
   {
      SystemRamUnCorrected_FaultHandling();
      SystemRamCorrected_FaultHandling();
   }


   if (SCIM_MEMU_ERR_FLAG & (uint32)MEMU_ERR_FLAG_FLASH_all)   
   {
      FlashUnCorrected_FaultHandling();
      FlashCorrected_FaultHandling();
   }

   /* check other possible sources */
   if (SCIM_MEMU_ERR_FLAG & (uint32)MEMU_ERR_FLAG_PERRAM_all)    
   {
     
   }

   // MEMU.CTRL.B.SWR = 0x1;
    /* clear all MEMU error flag flags to stop FCCU fault indication */
   // MEMU.ERR_FLAG.R = 0xFFFFFFFF;
	
}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the DiagnosticMonitor_Core_MCU_ErrorNotification
//! 
//! \param  MemoryType          To read data  
//! \param  FaultInjectionType  To read data      
//! 
//!====================================================================================================================

static uint8 MemoryFaultInject(Memory_T MemoryType, MemoryFault_T FaultInjectionType)
{ 
   uint8 retval = RTE_E_OK;
   //! ##### Based on Memory type and conditions accordingly faults are injected
   switch(MemoryType)
   {
    case MemoryType_SystemRAM:
      
      if(MemoryFault_Inject_1b_ECCFault == FaultInjectionType)
      {
         Inject_RAM_EccError(Inject_1Bit_EccError); 
      }  
      else if(MemoryFault_Inject_2b_ECCFault == FaultInjectionType)
      {
         Inject_RAM_EccError(Inject_2Bit_EccError);  
      }    
      else
      {

      }  

   break;

   case MemoryType_Flash:

      if(MemoryFault_Inject_1b_ECCFault == FaultInjectionType)
      {
          SCIM_FaultInjectionFlag = 1U;
         Inject_1bit_FLASH_ECC_error();   
      }  
      else if(MemoryFault_Inject_2b_ECCFault == FaultInjectionType)
      {
          SCIM_FaultInjectionFlag = 1U;
         Inject_noncorrectable_FLASH_ECC_error();     
      }    
      else
      {

      }    

   break;

   default:
      //Do nothing
    break;
  }

 return retval;
}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the MemoryFaultInjectTest
//!           
//! 
//!====================================================================================================================

static void MemoryFaultInjectTest(void)
{
	//! ##### Memory ECC Fault Injection based on conditions
	if(SCIM_MemoryFaultInject.StartFlag == 1u)
	{
	  SCIM_MemoryFaultInject.StartFlag = 0u;
	  MemoryFaultInject(SCIM_MemoryFaultInject.Memory,SCIM_MemoryFaultInject.FaultInjection);
	}
}

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the MemoryFaultHandler_Init
//!           
//! 
//!====================================================================================================================

void MemoryFaultHandler_Init(void)
{ 
   //! ##### Processing MEMU fault handling initialisation :'MEMU_ExceptionHandler()'
   MEMU_ExceptionHandler();
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the MemoryExceptionHandler
//!           
//! 
//!====================================================================================================================
void MemoryExceptionHandler(void)
{ 
   //! ##### Processing Memory exception handling  :'MEMU_ExceptionHandler()'
   MEMU_ExceptionHandler();
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the MemoryFaultHandler_10ms_Runnable
//!           
//! 
//!====================================================================================================================
void MemoryFaultHandler_10ms_Runnable(void)
{  
   //! ##### Processing MEMU periodicHandler  :'MEMU_PeriodicHandler()'
   MEMU_PeriodicHandler();
   //! ##### Processing Memory Fault injecting  :'MemoryFaultInjectTest()'
   MemoryFaultInjectTest();
}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the MemoryFaultHandler_GetStatus
//! 
//! \param  uint8 u8ErrorCode          
//! 
//!====================================================================================================================
void MemoryFaultHandler_GetStatus(UnRecoveredFaultInfo_T *UnRecvFault,RecoveredFaultInfo_T *RecvFault)
{  
   //! ##### Reading the fault status of Memory
   UnRecvFault->Fls.Address  = SCIM_UnRcvrECCFaultInfo.Fls.Address;
   UnRecvFault->Fls.ErrFlag  = SCIM_UnRcvrECCFaultInfo.Fls.ErrFlag;
   UnRecvFault->Sram.Address = SCIM_UnRcvrECCFaultInfo.Sram.Address;
   UnRecvFault->Sram.ErrFlag = SCIM_UnRcvrECCFaultInfo.Sram.ErrFlag;


   RecvFault->Fls.Address  = SCIM_RcvrECCFaultInfo[0].Fls.Address;
   RecvFault->Fls.ErrFlag  = SCIM_RcvrECCFaultInfo[0].Fls.ErrFlag;
   RecvFault->Sram.Address = SCIM_RcvrECCFaultInfo[0].Sram.Address;
   RecvFault->Sram.ErrFlag = SCIM_RcvrECCFaultInfo[0].Sram.ErrFlag;

}



#define DiagnosticMonitor_Core_STOP_SEC_CODE
#include "DiagnosticMonitor_Core_MemMap.h"
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/