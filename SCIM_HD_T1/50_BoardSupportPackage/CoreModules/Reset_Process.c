
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "mpc5746c.h"
#include "Rte_DiagnosticMonitor_Core.h" 
#include"BswM.h"
#include"EcuM.h"
#include"Dem.h"
#include"Dcm.h"
#include"ComM.h"
#include"Com.h"
#include"CanNM.h"
#include"LINSM.h"
#include"CANSM.h"
#include"Csm.h"
#include"Mcu.h"
#include"Bswinit.h"
#include"ResetProcess_If.h"
#include"MemoryFaultsHandler_If.h"

ModuleInfo_Type BSWModuleInfo[10]={
												{BSWM_MODULE_ID,BSWM_E_NO_INIT},
												{ECUM_MODULE_ID,ECUM_E_UNINIT},
												{DEM_MODULE_ID,DEM_E_UNINIT},
												{NVM_MODULE_ID,NVM_E_NOT_INITIALIZED},
												{DCM_MODULE_ID,DCM_E_UNINIT},
												{COMM_MODULE_ID,COMM_E_UNINIT},
												{CANSM_MODULE_ID,CANSM_E_UNINIT},
												{LINSM_MODULE_ID,LINSM_E_UNINIT},
												{CANNM_MODULE_ID,CANNM_E_NO_INIT},
												{CSM_MODULE_ID,CSM_E_UNINIT}
											 };

Det_InfoType Det_Info={0,0,0,0};   
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the MemoryFaultHandler_Init
//!           
//! 
//!====================================================================================================================

ResetCausetypes ResetProcess_Init()
{

  	EngTraceHWArray EngTraceHWDataBuffer= {{0,0,0,0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0,0,0},
                                          {0,0,0,0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0,0,0},
                                          {0,0,0,0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0,0,0},
                                          {0,0,0,0,0,0,0,0,0,0,0,0}};
	uint32 PMCDIG_SGSRStatus = 0U;
	uint32 MC_RGM_FESStatus = 0U;
	uint32 MC_RGM_DESStatus=0U;
	uint8 Reset_Cause=(uint8)ResetCause_None;		
	uint8 i=0;		
	uint8 ResetLog=0;		
	uint8 Int_NvBlockStatus=0;
   //! ##### Initialising reset
	//! ##### Checking MCU Voltage Regulator status
		
	PMCDIG_SGSRStatus = (uint32)PMCDIG_SGSR;
	MC_RGM_FESStatus  = (uint32)MC_RGM_FES;
	MC_RGM_DESStatus  = (uint32)MC_RGM_DES;
	
   Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(&EngTraceHWDataBuffer[0]);

   ResetLog = EngTraceHWDataBuffer[5].Reset_Type&(0x0F0U);
   //! ##### Checking and reading the condition for the cause of reset
   if ((ResetLog!=0x10)&&(WakeupTimerFunc!=WAKEUPTOFLASH))
   {
      Reset_Cause=(uint8)ResetCause_None;

      if(((PMCDIG_SGSRStatus&PMCDIG_SGSR_LVD_IO_A_HI_MASK32)==PMCDIG_SGSR_LVD_IO_A_HI_MASK32)
      &&((MC_RGM_FESStatus&MC_RGM_FESS_SS_LVD_IO_A_HI_MASK32)==MC_RGM_FESS_SS_LVD_IO_A_HI_MASK32))  
      {
         Reset_Cause=(uint8)ResetCause_VoltReg_LVD;
      } 
      else if((MC_RGM_FESStatus&MC_RGM_FESS_SS_HVD_LV_COLD_MASK32)==MC_RGM_FESS_SS_HVD_LV_COLD_MASK32)
      {
         Reset_Cause=(uint8)ResetCause_VoltReg_HVD;
      } 
      else
      {
      }

      if((MC_RGM_FESStatus&MG_RGM_FESS_SS_CMU_OLR_MASK32)==MG_RGM_FESS_SS_CMU_OLR_MASK32)
      {
         Reset_Cause=(uint8)ResetCause_Clock_Failure;
      } 
      else
      {

      }
      //otherthan criical faults
      if(Reset_Cause==(uint8)ResetCause_None)
      {
         if(MC_RGM.DES.B.F_POR ==1)
         {
            Reset_Cause=(uint8)ResetCause_PowerOnReset;
         }
         else if(MC_RGM.FES.B.F_EXR==1)
         {
            Reset_Cause=(uint8)ResetCause_External_Reset;
         }
         else
         {
            Reset_Cause=(uint8)ResetCause_Others;
         }
      }


      for(i=9;i>5;i--)
      {
         Rte_MemCpy(&EngTraceHWDataBuffer[i],&EngTraceHWDataBuffer[i-1],sizeof(EngTraceHWData_T));
      }


      Rte_MemClr(&EngTraceHWDataBuffer[5],sizeof(EngTraceHWData_T));

      EngTraceHWDataBuffer[5].Reset_Type=Reset_Cause;
      EngTraceHWDataBuffer[5].MC_RGM_FES_Reg = (uint32)MC_RGM_FESStatus;
      EngTraceHWDataBuffer[5].MC_RGM_DES_Reg=(uint32)MC_RGM_DESStatus;

      if(((EngTraceHWDataBuffer[4].Reset_Type)&(0x0F0U))!=0x10U)
      {
         for(i=4;i>0;i--)
         {
            Rte_MemCpy(&EngTraceHWDataBuffer[i],&EngTraceHWDataBuffer[i-1],sizeof(EngTraceHWData_T));
         }
         Rte_MemClr(&EngTraceHWDataBuffer[0],sizeof(EngTraceHWData_T));

         EngTraceHWDataBuffer[0].Reset_Type=Reset_Cause|(uint8)0x10;
         EngTraceHWDataBuffer[0].MC_RGM_FES_Reg = (uint32)MC_RGM_FESStatus;
         EngTraceHWDataBuffer[0].MC_RGM_DES_Reg=(uint32)MC_RGM_DESStatus;
      }

   }
   else
   {

      EngTraceHWDataBuffer[5].Reset_Type&=(0x0F);
   }
   Rte_Write_EngTraceHW_NvM_I_EngTraceHW_NvM(&EngTraceHWDataBuffer[0]);

   MC_RGM_DES		 = 0xFFFFFFFFU; // Clear POR Flag
   MC_RGM_FES		 = 0XFFFFFFFFU; // Clear EXR Flag
   PMCDIG_SGSR 	 = 0XFFFFFFFFU; //Clear Low voltage drop reset
         
   return Reset_Cause;      

}
//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the MemoryFaultHandler_Init
//! \param  Reset_Type   To read data            
//! 
//!====================================================================================================================
void ResetProcess_TriggerReset(ResetCausetypes Reset_Type)
{

	EngTraceHWArray EngTraceHWDataBuffer= {{0,0,0,0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0,0,0},
                                          {0,0,0,0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0,0,0},
                                          {0,0,0,0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0,0,0},
                                          {0,0,0,0,0,0,0,0,0,0,0,0}};
	uint8 Int_NvBlockStatus=0,i=0;
	uint8 Reset_Cause = (uint8)Reset_Type;
	TaskType currentTask;
	Os_ErrorInformationType CurrentError;
	OSServiceIdType serviceId;
	UnRecoveredFaultInfo_T UnRecvFault;
	RecoveredFaultInfo_T   RecvFault;	


				
   Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(&EngTraceHWDataBuffer[0]);
   //! ##### Storing last Resets from 5-9
   for(i=9;i>5;i--)
   {
      Rte_MemCpy(&EngTraceHWDataBuffer[i],&EngTraceHWDataBuffer[i-1],sizeof(EngTraceHWData_T));
   }
   Rte_MemClr(&EngTraceHWDataBuffer[5],sizeof(EngTraceHWData_T));
   EngTraceHWDataBuffer[5].Reset_Type=Reset_Cause|(uint8)0x10;
   
   //! ##### Storing First 5 resets from 0-4
   if(((EngTraceHWDataBuffer[4].Reset_Type)&(0x0F0U))!=0x10U)
   {
      for(i=4;i>0;i--)
      {
         Rte_MemCpy(&EngTraceHWDataBuffer[i],&EngTraceHWDataBuffer[i-1],sizeof(EngTraceHWData_T));
      }
      Rte_MemClr(&EngTraceHWDataBuffer[0],sizeof(EngTraceHWData_T));
      EngTraceHWDataBuffer[0].Reset_Type=Reset_Cause|(uint8)0x10;
   }
   //! ##### Based on the reset cause reading the data accordingly
	switch(Reset_Cause)
	{

		case ResetCause_Det_Error:
				EngTraceHWDataBuffer[5].Reset_Type=(Reset_Cause|(uint8)0x10);
				EngTraceHWDataBuffer[5].ModuleID=Det_Info.mModuleId;
				EngTraceHWDataBuffer[5].InstanceID =Det_Info.mInstanceId;
				EngTraceHWDataBuffer[5].APIID=Det_Info.mApiId;
				EngTraceHWDataBuffer[5].ErrorID=Det_Info.mErrorId;
				EngTraceHWDataBuffer[5].Timelog=0x0;
				break;
		case ResetCause_Diagnostic_Reset:						
			
				EngTraceHWDataBuffer[5].Reset_Type=(Reset_Cause|(uint8)0x10);
				break;
		case ResetCause_Protection_Hook:
   			(void)Os_GetDetailedError(&CurrentError);
				serviceId = OSErrorGetServiceId();			
				GetTaskID(&currentTask);
				EngTraceHWDataBuffer[5].Reset_Type=Reset_Cause|(uint8)0x10;
				EngTraceHWDataBuffer[5].ServiceID= serviceId; 
				EngTraceHWDataBuffer[5].TaskID= currentTask; 
				EngTraceHWDataBuffer[5].Timelog= 0x0;
				EngTraceHWDataBuffer[5].MemoryAddress= 0x0;
				EngTraceHWDataBuffer[5].ErrorID= CurrentError.Error;
				break;			
		case ResetCause_SRAM_ECC:
			   MemoryFaultHandler_GetStatus(&UnRecvFault,&RecvFault);

				EngTraceHWDataBuffer[5].Reset_Type=Reset_Cause|(uint8)0x10;				
				EngTraceHWDataBuffer[5].MemoryAddress=UnRecvFault.Sram.Address;

				break;
		case ResetCause_FLASH_ECC:
			   MemoryFaultHandler_GetStatus(&UnRecvFault,&RecvFault);

				EngTraceHWDataBuffer[5].Reset_Type=Reset_Cause|(uint8)0x10;				
				EngTraceHWDataBuffer[5].MemoryAddress=UnRecvFault.Fls.Address;

				break;		
		case ResetCause_WDGM_Error:
		case ResetCause_PLL_Error:				
            EngTraceHWDataBuffer[5].Reset_Type=Reset_Cause|(uint8)0x10;
            break;
		default:
            //Do nothing
            break;
			
	}

	
	Rte_MemCpy(&EngTraceHWDataBuffer[0],&EngTraceHWDataBuffer[5],sizeof(EngTraceHWData_T));

	

	#if 1
	do
	  {
		  //! ##### Process the main functions
		  Fls_MainFunction();
		  Fee_MainFunction();
		  NvM_MainFunction();
	
		  NvM_GetErrorStatus(NvMConf_NvMBlockDescriptor_Application_Data_NVM_EngTraceHW_NvM, &Int_NvBlockStatus);
	  }
	  while (Int_NvBlockStatus == NVM_REQ_PENDING);

	NvM_WriteBlock(NvMConf_NvMBlockDescriptor_Application_Data_NVM_EngTraceHW_NvM, &EngTraceHWDataBuffer[0]);
	
	do
	  {
		  //! ##### Process the main functions
		  Fls_MainFunction();
		  Fee_MainFunction();
		  NvM_MainFunction();
	
		  NvM_GetErrorStatus(NvMConf_NvMBlockDescriptor_Application_Data_NVM_EngTraceHW_NvM, &Int_NvBlockStatus);
	  }
	  while (Int_NvBlockStatus == NVM_REQ_PENDING);
	#endif
   if(Reset_Cause==(uint8)ResetCause_Diagnostic_Reset)
   {
      Mcu_PerformReset();
   }

   while(1) // External Watchdog
   {
      //dO nothing
   }

}


//!====================================================================================================================
//! 
//! \brief
//! 
//! This function implements the logic for the Det_report_ForReset
//! \param   ModuleId     To provide id for the module 
//! \param   InstanceId   To provide id for the instance
//! \param   ApiId        To provide id for the particular Api
//! \param   ErrorId      To provide id for error
//! 
//!====================================================================================================================
Std_ReturnType Det_report_ForReset( uint16 ModuleId, uint8 InstanceId, uint8 ApiId, uint8 ErrorId )
{

   uint8 i;
   //! Reading the Id's for reporting to DET
   for(i=0;i<10;i++)
   {

    if((ModuleId==BSWModuleInfo[i].ModuleID)&&(ErrorId==BSWModuleInfo[i].ErrorID))
      {	
         Det_Info.mModuleId=ModuleId;
         Det_Info.mInstanceId=InstanceId;
         Det_Info.mApiId=ApiId;
         Det_Info.mErrorId=ErrorId;
         ResetProcess_TriggerReset(ResetCause_Det_Error);
      }
      else
      {}
   }

   return 1;
}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
