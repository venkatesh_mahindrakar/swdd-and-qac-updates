/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2016 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  VEC_RomTest.h
 *    Component:  Diag_AsrSwcRomTest_Volvo_AB
 *       Module:  -
 *    Generator:  -
 *
 *  Description:  Background ROM check
 *                Cyclic low priority background task constantly computes checksums and signatures of all logical
 *                block defined by the FBL configuration. Computed values are compared with the original
 *                checksum/signature stored in nonvolatile memory.
 *                Application will be informed in case of read or signature verification errors.
 *  
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  -------------------------------------------------------------------------------------------------------------------
 *  Oliver Garnatz                Gz            Vector Informatik GmbH
 *  Daniel Leutloff               DLf           Vector Informatik GmbH
 *  Joern Herwig                  JHg           Vector Informatik GmbH
 *  Markus Schneider              Mss           Vector Informatik GmbH
 *  -------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Version   Date        Author  Change Id     Description
 *  -------------------------------------------------------------------------------------------------------------------
 *  01.00.00  2009-08-03  Gz      -             Initial creation of template
 *                        DLf     -             Added VEC_ROMTest_MainFunction to template
 *                        JHg     -             Added implementation
 *  01.01.00  2009-08-03  DLf/JHg ESCAN00040529 Renamed block status type and enumeration value names
 *  01.01.01  2010-08-11  JHg     ESCAN00042876 Removed unused variable
 *                        JHg     ESCAN00043122 Replaced erroneous define for byte size of segment address
 *  02.00.00  2014-11-11  JHg     ESCAN00078829 Support for updated security module API (VAP SLP2)
 *                                              Remove compatibility mode
 *                        JHg     ESCAN00054725 Encapsulate NV accesses in exclusive area
 *  03.00.00  2015-05-29  Mss     ESCAN00083205 Added CSM and CRC support
 *  03.00.01  2015-06-12  Mss     ESCAN00083374 Added compatibility defines for CSwC variant
 *  04.00.00  2016-03-31  JHg     ESCAN00088398 Added support for asynchronous NV-operations
 *  04.00.01  2016-04-13  JHg     ESCAN00089468 Resolve compiler warning
 *                        JHg     ESCAN00089469 Resolve compiler warning
 *********************************************************************************************************************/

#if !defined (VEC_ROMTEST_H)
#define VEC_ROMTEST_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "Rte_VEC_RomTest.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* ----- Component version information (decimal version of ALM implementation package) ----- */
#define VEC_ROMTEST_SW_MAJOR_VERSION       (4u)
#define VEC_ROMTEST_SW_MINOR_VERSION       (0u)
#define VEC_ROMTEST_SW_PATCH_VERSION       (1u)

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/** Result type */
typedef enum
{
  kVecRomTestResultOk = 0,    /**< Operation successful */
  kVecRomTestResultFailed,    /**< Operation failed */
  kVecRomTestResultBusy       /**< Operation pending */
} tVecRomTestResult;

/** Result of memory read */
typedef enum
{
  kVecRomTestReadResultOk = 0,      /**<  Data successfully read */
  kVecRomTestReadResultFailed,      /**< Read error occurred */
  kVecRomTestReadResultBusy,        /**< Memory device busy */
  kVecRomTestReadResultUnavailable  /**< Memory region currently unavailable */
} tVecRomTestReadResult;

/* State of logical block (software structure component) */
typedef enum
{
  kVecRomTestBlockStateUndefined = 0,     /**< State undefined */
  kVecRomTestBlockStateValid,             /**< Block valid */
  kVecRomTestBlockStateReadFailure,       /**< Read failure occured */
  kVecRomTestBlockStateSignatureFailure,  /**< Signature verification failed */
  kVecRomTestBlockStateNotPresent         /**< Block not present */
} tVecRomTestBlockState;

/** Checksum type */
typedef enum
{
  kVecRomTestSignatureTypeChecksum = 0,     /**< Checksum (CRC-32) */
  kVecRomTestSignatureTypeSignature,        /**< RSA signature */
  kVecRomTestSignatureTypeInvalid = 0xFFu   /**< Invalid type */
} tVecRomTestSignatureType;

/** Error codes for fatal error condition */
typedef enum
{
  kVecRomTestFatalErrorNone = 0,                    /**< No error */
  kVecRomTestFatalErrorInsufficientBlockStateSize,  /**< Configuration error: insufficient size of block state array */
  kVecRomTestFatalErrorInvalidSignatureType,        /**< Invalid signature type */
  kVecRomTestFatalErrorSecurityRoutineFailed,       /**< Security routine reported failed operation */
  kVecRomTestFatalErrorKeySizeExceedsBufferSize,    /**< RSA key exceeds size of temporary buffer */
  kVecRomTestFatalErrorNvAccessFailed               /**< Non-volatile memory operation failed */
} tVecRomTestFatalError;

typedef uint32 tVecRomTestAddress;

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define VEC_RomTest_START_SEC_CODE
#include "VEC_RomTest_MemMap.h"


/* Non-volatile memory access routines */
FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_GetBlockValidity( uint8 blockIdx,
  P2VAR(tVecRomTestResult, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) pValidity );
FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_ReadSignatureType( uint8 blockIdx,
  P2VAR(tVecRomTestSignatureType, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) pType );
FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_ReadSignatureValue( uint8 blockIdx,
  P2VAR(uint8, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) pValue, uint32 length );
FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_ReadSegmentNumber( uint8 blockIdx,
  P2VAR(uint8, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) pNumber );
FUNC(tVecRomTestAddress, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_ReadSegmentStart( uint8 blockIdx, uint8 segment,
  P2VAR(tVecRomTestAddress, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) pAddress );
FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_ReadSegmentLength( uint8 blockIdx, uint8 segment,
  P2VAR(uint32, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) pLength );
/* Other callouts */
FUNC(void, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_InitCallouts( void );
FUNC(uint16, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_GetModulusSize( void );
FUNC(uint16, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_GetExponentSize( void );
FUNC(P2CONST(uint8, AUTOMATIC, RTE_VEC_ROMTEST_APPL_CONST), RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_GetModulus( void );
FUNC(P2CONST(uint8, AUTOMATIC, RTE_VEC_ROMTEST_APPL_CONST), RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_GetExponent( void );
FUNC(uint8, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_GetBlockCount( void );
FUNC(tVecRomTestReadResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_ReadMemory( tVecRomTestAddress address,
  P2VAR(uint32, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) length, P2VAR(uint8, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) buffer );
FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_FailureNotification( uint8 blockIdx, tVecRomTestBlockState blockState );
FUNC(void, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_FatalError( tVecRomTestFatalError errorCode );

/* Fixed routines */
FUNC(uint32, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_GetInteger( uint8 count, P2VAR(uint8, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) buffer );
FUNC(void, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_SetInteger( uint8 count, uint32 input, P2VAR(uint8, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) buffer );

#define VEC_RomTest_STOP_SEC_CODE
#include "VEC_RomTest_MemMap.h"

#endif  /* VEC_ROMTEST_H */

/**********************************************************************************************************************
 *  END OF FILE: VEC_RomTest.h
 *********************************************************************************************************************/
