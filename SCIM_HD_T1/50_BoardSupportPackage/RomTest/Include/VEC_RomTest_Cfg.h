/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2016 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  VEC_RomTest_Cfg.h
 *    Component:  Diag_AsrSwcRomTest_Volvo_AB
 *       Module:  -
 *    Generator:  -
 *
 *  Description: Configuration header
 *  
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *********************************************************************************************************************/

#if !defined (VEC_ROMTEST_CFG_H)
#define VEC_ROMTEST_CFG_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "Csm_Cfg.h"

#include "fbl_def.h"
#include "Fbl_Cfg.h"
#include "comdat.h"
#include "Fbl_Lbt.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
/** Number of bytes to verify per cycle */
#define VEC_ROMTEST_VERIFY_BYTES 64u

/** Maximum size of signature buffer */
#define VEC_ROMTEST_SIGNATURE_MAX_LENGTH_BUFFER   128u

/** Maximum size of modulus */
#define VEC_ROMTEST_SIGNATURE_MAX_LENGTH_MODULUS  128u

/** Maximum size of exponent */
#define VEC_ROMTEST_SIGNATURE_MAX_LENGTH_EXPONENT 128u

/** Maximum number of blocks (software components) */
#define VEC_ROMTEST_MAX_BLOCK_COUNT               FBL_MTAB_NO_OF_BLOCKS

/* Check operation mode of CSM */
#if ( CSM_USE_SYNC_JOB_PROCESSING == STD_OFF )
/** CSM operates in asynchronous mode */
# define VEC_ROMTEST_CSM_ASYNC_JOB_PROCESSING
#else
/** CSM operates in synchronous mode */
# define VEC_ROMTEST_CSM_SYNC_JOB_PROCESSING
#endif

#endif /* VEC_ROMTEST_KEYS_H */
/**********************************************************************************************************************
 *  END OF FILE: VEC_ROMTEST_KEYS.H
 *********************************************************************************************************************/
