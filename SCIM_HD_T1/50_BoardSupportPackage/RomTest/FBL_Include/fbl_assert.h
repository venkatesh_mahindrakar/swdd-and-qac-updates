/***********************************************************************************************************************
 *  FILE DESCRIPTION
 *  ------------------------------------------------------------------------------------------------------------------*/
/** \file
 *  \brief        System specific assertion defines
 *
 *  --------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT
 *  --------------------------------------------------------------------------------------------------------------------
 *  \par Copyright
 *  \verbatim
 *  Copyright (c) 2017 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 */
/**********************************************************************************************************************/

/***********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  --------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  --------------------------------------------------------------------------------------------------------------------
 *  Andreas Wenckebach            AWh           Vector Informatik GmbH
 *  Joern Herwig                  JHg           Vector Informatik GmbH
 *  Marcel Viole                  MVi           Vector Informatik GmbH
 *  Marco Riedl                   Rie           Vector Informatik GmbH
 *  Robert Schaeffner             Rr            Vector Informatik GmbH
 *  Thomas Bezold                 TBe           Vector Informatik GmbH
 *  Alexander Starke              ASe           Vector Informatik GmbH
 *  Andre Caspari                 Ci            Vector Informatik GmbH
 *  Jason Learst                  JLe           Vector CANtech, Inc.
 *  Torben Stoessel               TnS           Vector Informatik GmbH
 *  Fadie Ghraib                  FGh           Vector CANtech, Inc.
 *  Ralf Haegenlaeuer             HRf           Vector Informatik GmbH
 *  Sebastian Loos                Shs           Vector Informatik GmbH
 *  Torben Stoessel               TnS           Vector Informatik GmbH
 *  Junwei Ye                     Jy            Vector Informatik GmbH
 *  Johannes Krimmel              KJs           Vector Informatik GmbH
 *  Achim Strobelt                Ach           Vector Informatik GmbH
 *  --------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  --------------------------------------------------------------------------------------------------------------------
 *  Version    Date        Author  Change Id        Description
 *  --------------------------------------------------------------------------------------------------------------------
 *  03.00.00   2015-07-23  AWh     ESCAN00083431    General rework based on 2.49.00
 *                         JHg     ESCAN00076594    No changes
 *  03.01.00   2015-09-29  MVi     ESCAN00085254    No changes
 *                         Rie     ESCAN00085358    No changes
 *  03.01.01   2015-11-02  Rr      ESCAN00086192    No changes
 *  03.02.00   2015-12-16  Ci      ESCAN00086473    No changes
 *                         AWh     ESCAN00086840    No changes
 *                         TBe     ESCAN00087175    No changes
 *  03.03.00   2016-01-11  ASe     ESCAN00087377    No changes
 *  03.04.00   2016-01-13  Ci      ESCAN00087439    No changes
 *  03.04.01   2016-02-25  JLe     ESCAN00088188    No changes
 *                         TnS     ESCAN00088433    Moved assertion specific code to fbl_assert.h
 *  03.04.02   2016-03-09  FGh     ESCAN00088723    No changes
 *                         HRf     ESCAN00088811    No changes
 *                                 ESCAN00088783    No changes
 *  03.05.00   2016-03-17  Shs     ESCAN00088931    No changes
 *  03.06.00   2016-06-30  Ci      ESCAN00090448    No changes
 *  03.07.00   2016-10-21  TnS     ESCAN00092475    No changes
 *  03.08.00   2016-10-28  Shs     ESCAN00092626    No changes
 *  03.08.01   2016-11-23  HRf     ESCAN00092964    No changes
 *  03.08.02   2017-01-26  Jy      ESCAN00093738    No changes
 *  03.09.00   2017-02-09  Rie     ESCAN00093922    No changes
 *                         KJs     ESCAN00093947    No changes
 *  03.09.01   2017-05-09  JHg     ESCAN00095090    No changes
 *  03.09.02   2017-05-19  Rie     ESCAN00095204    Added assertion defines for DET
 *                         Ach     ESCAN00095245    No changes
 *  03.10.00   2017-07-05  JHg     ESCAN00095765    No changes
 **********************************************************************************************************************/

#ifndef __FBL_ASSERT_H__
#define __FBL_ASSERT_H__

/***********************************************************************************************************************
 *  DEFINITION OF ASSERTION CODES (SYSTEM)
 **********************************************************************************************************************/

#if defined( FBL_ENABLE_ASSERTION )
/* Note: System assertions are only allowed in the range from 0x00-0x7F. Others are reserved. */

/* 0x00-0x1F: General FBL assertions */
#define kFblSysAssertMissingForcedRCRRP               0x00u

#define kFblSysAssertInitializationFailed             0x10u

/* 0x20-0x3F: Internal assertions */
#define kFblSysAssertServiceReqBeforeResp             0x20u
#define kFblSysAssertServiceMainHandlerInvalid        0x21u
#define kFblSysAssertDet                              0x22u

/* 0x40-0x4F: Generated data assertions */
#define kFblSysAssertWdBufferOverflow                 0x40u

/* 0x50-0x6F: User parameter assertions */
#define kFblSysAssertParameterOutOfRange              0x50u

#define kFblSysAssertRepeatedCallOnConcludedService   0x60u

/* 0x70-0x7F: Communication wrapper assertions */
#define kFblSysAssertStateNotIdle                     0x70u
#define kFblSysAssertNoBufferAvailable                0x71u

/* 0x80-0xFF: OEM specific */
#include "fbl_assert_oem.h"

# if defined( __LINE__ ) && defined( __FILE__ ) && defined( FBL_ENABLE_ASSERTION_EXTENDED_INFO )
#  define FBL_DECL_ASSERT_EXTENDED_INFO(n)       const vuint8* module, vuint16 line, n
#  define FBL_USED_ASSERT_EXTENDED_INFO(n)       __FILE__, __LINE__,(n)
# else
#  define FBL_DECL_ASSERT_EXTENDED_INFO(n)        n   /* PRQA S 3410 */ /* MD_FblAssert_3410 */
#  define FBL_USED_ASSERT_EXTENDED_INFO(n)       (n)  /* PRQA S 3453 */ /* MD_MSR_19.7 */
# endif
/* No statement should be added to below if clauses, in order to prevent errors suggested by Misra rule 14.9 */
/* PRQA S 3412 4 */ /* MD_FblAssert_Assertion */
# define assertFbl(p, e)         if (!(p)) ApplFblFatalError(FBL_USED_ASSERT_EXTENDED_INFO(e)) /* PRQA S 2214 */ /* MD_FblAssert_Assertion */
# define assertFblUser(p, e)     if (!(p)) ApplFblFatalError(FBL_USED_ASSERT_EXTENDED_INFO(e)) /* PRQA S 2214 */ /* MD_FblAssert_Assertion */
# define assertFblGen(p, e)      if (!(p)) ApplFblFatalError(FBL_USED_ASSERT_EXTENDED_INFO(e)) /* PRQA S 2214 */ /* MD_FblAssert_Assertion */
# define assertFblInternal(p, e) if (!(p)) ApplFblFatalError(FBL_USED_ASSERT_EXTENDED_INFO(e)) /* PRQA S 2214 */ /* MD_FblAssert_Assertion */
#else
# define assertFbl(p, e)
# define assertFblUser(p, e)
# define assertFblGen(p, e)
# define assertFblInternal(p, e)
#endif /* FBL_ENABLE_ASSERTION */

/* Module specific MISRA deviations:

   MD_FblAssert_Assertion:
      Reason:     This is an assertion macro used only for debugging in development (FBL_ENABLE_ASSERTION). There should
                  be no need to extend the amount of statements, so the current usage is accepted.
      Risk:       Unintended debugging code behavior could result, through adding further statements,
      Prevention: A comment is in place to avoid that further statements are added to the if condition.

   MD_FblAssert_3410:
      Reason:     This macro serves as function parameter type qualifier and cannot have parentheses, because this would
                  result in an invalid function parameter list.
      Risk:       Usage at not intended places and unintended code behavior due to the missing parentheses.
      Prevention: Reviews must ensure that this macro is only used in function parameter lists.
*/

#endif /* __FBL_ASSERT_H__ */

/***********************************************************************************************************************
 *  END OF FILE: FBL_ASSERT.H
 **********************************************************************************************************************/
