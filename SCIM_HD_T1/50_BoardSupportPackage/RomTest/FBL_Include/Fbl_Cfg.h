/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Fbl
 *           Program: FBL_VolvoAb_SLP2
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: MPC5746C
 *    License Scope : The usage is restricted to CBD1800195_D01
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Fbl_Cfg.h
 *   Generation Time: 2020-05-28 18:58:03
 *           Project: SCIM_HD_T1_FBL - Version 1.0
 *          Delivery: CBD1800195_D01
 *      Tool Version: DaVinci Configurator  5.16.41 SP3
 *
 *
 *********************************************************************************************************************/


#if !defined(__FBL_CFG_H__)
#define __FBL_CFG_H__

/* Global Constant Macros ********************************************************************************************/
#ifndef FBL_USE_DUMMY_FUNCTIONS
#define FBL_USE_DUMMY_FUNCTIONS STD_OFF /* /MICROSAR/EcuC/EcucGeneral/DummyFunction */
#endif
#ifndef FBL_USE_DUMMY_STATEMENT
#define FBL_USE_DUMMY_STATEMENT STD_ON /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef FBL_DUMMY_STATEMENT
#define FBL_DUMMY_STATEMENT(v) (v)=(v) /* PRQA S 3453 */ /* MD_MSR_19.7 */  /* /MICROSAR/EcuC/EcucGeneral/DummyStatementKind */
#endif
#ifndef FBL_DUMMY_STATEMENT_CONST
#define FBL_DUMMY_STATEMENT_CONST(v) (void)(v) /* PRQA S 3453 */ /* MD_MSR_19.7 */  /* /MICROSAR/EcuC/EcucGeneral/DummyStatementKind */
#endif
#ifndef FBL_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define FBL_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef FBL_ATOMIC_VARIABLE_ACCESS
#define FBL_ATOMIC_VARIABLE_ACCESS 32U /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef FBL_PROCESSOR_MPC5746C
#define FBL_PROCESSOR_MPC5746C
#endif
#ifndef FBL_COMP_DIAB
#define FBL_COMP_DIAB
#endif
#ifndef FBL_GEN_GENERATOR_MSR
#define FBL_GEN_GENERATOR_MSR
#endif
#ifndef FBL_CPUTYPE_BITORDER_MSB2LSB
#define FBL_CPUTYPE_BITORDER_MSB2LSB /* /MICROSAR/EcuC/EcucGeneral/BitOrder */
#endif
#ifndef FBL_CONFIGURATION_VARIANT_PRECOMPILE
#define FBL_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef FBL_CONFIGURATION_VARIANT_LINKTIME
#define FBL_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef FBL_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define FBL_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef FBL_CONFIGURATION_VARIANT
#define FBL_CONFIGURATION_VARIANT FBL_CONFIGURATION_VARIANT_LINKTIME
#endif
#ifndef FBL_POSTBUILD_VARIANT_SUPPORT
#define FBL_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif

/* FblOem__base ******************************************************************************************************/
#define FBL_DISABLE_STAY_IN_BOOT 
#define FBL_USE_OWN_MEMCPY 
#define FBL_WATCHDOG_ON 
#define FBL_WATCHDOG_TIME                       (40 / FBL_REPEAT_CALL_CYCLE) 
#define FBL_HEADER_ADDRESS                      0x00F90600UL 
#define FBL_ENABLE_APPL_TIMER_TASK 
#define FBL_ENABLE_APPL_TASK 
#define FBL_DISABLE_APPL_STATE_TASK 
#define SWM_DATA_MAX_NOAR                       16 
#define FBL_DIAG_BUFFER_LENGTH                  4095 
#define FBL_DIAG_TIME_P2MAX                     (25 / FBL_REPEAT_CALL_CYCLE) 
#define FBL_DIAG_TIME_P3MAX                     (5000 / FBL_REPEAT_CALL_CYCLE) 
#define FBL_DISABLE_SLEEPMODE 
#define FBL_SLEEP_TIME                          0 
#define FBL_DISABLE_GAP_FILL 
#define kFillChar                               0x00U 
#define FBL_DISABLE_GATEWAY_SUPPORT 
#define FBL_DISABLE_PRESENCE_PATTERN 
#define FBL_DISABLE_FBL_START 
#define FBL_ENABLE_COMMON_DATA 
#define FBL_ENABLE_RESPONSE_AFTER_RESET 
#define FBL_DISABLE_USERSUBFUNCTION 
#define FBL_DISABLE_USERSERVICE 
#define FBL_DISABLE_USERROUTINE 
#define FBL_DIAG_TIME_S3EXT                     (0 / FBL_REPEAT_CALL_CYCLE) 
#define FBL_DIAG_TIME_S3PRG                     (5000 / FBL_REPEAT_CALL_CYCLE) 
#define FBL_DIAG_ENABLE_FLASHDRV_DOWNLOAD 
#define FBL_DIAG_DISABLE_FLASHDRV_ROM 
#define FBL_ENABLE_DATA_PROCESSING 
#define FBL_DISABLE_ENCRYPTION_MODE 
#define FBL_ENABLE_COMPRESSION_MODE 
#define FBL_INTEGRATION                         2 
#define FBL_PRODUCTION                          1 
#define FBL_PROJECT_STATE                       FBL_INTEGRATION 
#define FBL_ENABLE_SYSTEM_CHECK 
#define FBL_ENABLE_DEBUG_STATUS 
#define FBL_ENABLE_ASSERTION 
#define FBL_MEM_PROC_BUFFER_SIZE                256 
#define FBL_MEM_DISABLE_VERIFY_INPUT 
#define FBL_MEM_DISABLE_VERIFY_PROCESSED 
#define FBL_MEM_DISABLE_VERIFY_PIPELINED 
#define FBL_MEM_ENABLE_VERIFY_OUTPUT 
#define FBL_MEM_VERIFY_SEGMENTATION             64 
#define FBL_ENABLE_ADAPTIVE_DATA_TRANSFER_RCRRP 
#define FBL_DISABLE_PIPELINED_PROGRAMMING 
#define FBL_DISABLE_SUSPEND_PROGRAMMING 
#define FBL_MEM_WRITE_SEGMENTATION              256 
#define FBL_ENABLE_UNALIGNED_DATA_TRANSFER 

/* FblHal ************************************************************************************************************/
#include "FblHal_Cfg.h"

/* FblOem_VolvoAb ****************************************************************************************************/
#define FBL_DTC_NUMBER                          0x0000U 
#define FBL_DIAG_ENABLE_DID_BOOT_IDENTIFICATION 
#define FBL_DIAG_ENABLE_DID_BOOT_FINGERPRINT 
#define FBL_ENABLE_VPM 

/* User Config File **************************************************************************************************/
/* User Config File Start */
/* Memory stack  ********************************************************************************************* */

/* Configurable value(s) >>> */
/* Enable stream output - e.g. required for delta download */
#define FBL_MEM_ENABLE_STREAM_OUTPUT
/* Allocate pipelined verification jobs for all download segments */
#define FBL_MEM_VERIFY_PIPELINED_JOB_COUNT   SWM_DATA_MAX_NOAR

#if defined( FBL_MEM_ENABLE_STREAM_OUTPUT )
/* Enable fuse library */
# define FBL_APPL_ENABLE_REDBEND_FUSE
/* Disable background processing (pipelined programming) for stream output */
# define FBL_MEM_ENABLE_SELECTIVE_PIPELINED_PROGRAMMING
#endif /* FBL_MEM_ENABLE_STREAM_OUTPUT */
/* <<< Configurable value(s) */

/* Enable use of NV-wrapper */
#define FBL_ENABLE_WRAPPER_NV
#define FBL_APPL_ENABLE_FEE

/* Communication stack  ************************************************************************************** */

#if defined( FBL_ENABLE_PIPELINED_PROGRAMMING )
/* Required for parallel programming and communication */
# define FBL_CW_ENABLE_TASK_CODE_IN_RAM
#else
/* Required for RCR-RP handling with MSR Can, CanIf and CanTp */
# define FBL_CW_ENABLE_TASK_CODE_IN_RAM
#endif /* FBL_ENABLE_PIPELINED_PROGRAMMING */

/* Manufacturer specific part ******************************************************************************** */

/* Configurable value(s) >>> */
/* Enable PBL update approval */
#define FBL_DIAG_DISABLE_PBL_UPDATE_APPROVAL
//#define FBL_DIAG_ENABLE_PBL_UPDATE_APPROVAL // not exist fbl_bm.h file : 2019.01.22

/* Logical block number where FBL updater is located */
#define FBL_LBT_NR_OF_UPDATER                FBL_MTAB_APPLICATION_BLOCK_NUMBER
/* <<< Configurable value(s) */

/* Number of entries in fingerprint history */
#define FBL_FINGERPRINT_HISTORY_SIZE         5u


/* User Config File End */

/* User Section ******************************************************************************************************/

#endif /* __FBL_CFG_H__ */

