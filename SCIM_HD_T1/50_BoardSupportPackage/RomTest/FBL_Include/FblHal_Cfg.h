/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: FblHal
 *           Program: FBL_VolvoAb_SLP2
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: MPC5746C
 *    License Scope : The usage is restricted to CBD1800195_D01
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: FblHal_Cfg.h
 *   Generation Time: 2020-05-28 18:57:14
 *           Project: SCIM_HD_T1_FBL - Version 1.0
 *          Delivery: CBD1800195_D01
 *      Tool Version: DaVinci Configurator  5.16.41 SP3
 *
 *
 *********************************************************************************************************************/


#if !defined(__FBL_HAL_H__)
#define __FBL_HAL_H__

/* FblHal__base ******************************************************************************************************/
#define FBL_FLASH_ENABLE_ECC_SAFE_READ 
#define FLASH_SIZE                     1024 
#define FBL_TIMER_RELOAD_VALUE         39999 
#define FBL_TIMER_PRESCALER_VALUE      1 

/* Derivative Specific Defines */
#define FBL_SWT_BASE       0xFC050000ul      /**< Software watchdog timer base address */
#define FBL_PIT_BASE       0xFFF84000ul      /**< PIT base address */
#define FBL_PLL_BASE       0xFFFB0080ul      /**< Dual PLL base register */
#define FBL_ME_BASE        0xFFFB8000ul      /**< Mode entry module base register */
#define FBL_CMU_BASE       0xC3FE0100ul      /**< Clock monitoring unit base register */
#define FBL_CMU_PLL_BASE   0xFFFB0200ul      /**< CMU base register for PLL */
#define FBL_CGM_BASE       0xFFFB0000ul      /**< Clock generation module */
#define FBL_RGM_BASE       0xFFFA8000ul      /**< Reset generation module base register */
#define FBL_SIUL2_BASE     0xFFFC0000ul      /**< SIU lite base address */
#define FBL_STM_BASE       0xFC068000ul      /**< System timer base address */
#define FBL_EDMA_BASE      0xFC0A0000ul      /**< eDMA0 base address */
#define FBL_SFR_BASE_ADRESSES_PRECONFIGURED
#define FBL_SFR_SIU_TYPE   2u
#define FBL_FLASH_REG_0    0xFFFE0000u       /**< Flash module base register */
#define V_CPU_MPC5700                        /**< Compatibility define for platform */
#define V_COMP_DIABDATA_MPC5700              /**< Compatibility define for platform */
#define C_CPUTYPE_32BIT                      /**< Compatibility define for platform */
#define V_SUPPRESS_EXTENDED_VERSION_CHECK    /**< Compatibility define for platform */
//#define VGEN_ENABLE_CANFBL                   /**< Compatibility define for platform */
#define V_PROCESSOR_MPC5746C                 /**< Compatibility define for platform */
#define V_ENABLE_CAN_ASR_ABSTRACTION         /**< Compatibility define for platform */
#define FLASH_XD1_USER_PROTECTION_2 { 0x00000003ul, 0x00000000ul, 0x00000000ul, 0x00000000ul }

/* FblHal_Mpc ********************************************************************************************************/
#define FLASH_SETUP 2 


#endif /* __FBL_HAL_H__ */

