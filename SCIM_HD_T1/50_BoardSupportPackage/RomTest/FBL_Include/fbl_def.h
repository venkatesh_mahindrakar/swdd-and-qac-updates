/***********************************************************************************************************************
 *  FILE DESCRIPTION
 *  ------------------------------------------------------------------------------------------------------------------*/
/** \file
 *  \brief        Main definitions for the Flash Boot Loader
 *
 *  --------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT
 *  --------------------------------------------------------------------------------------------------------------------
 *  \par Copyright
 *  \verbatim
 *  Copyright (c) 2017 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 */
/**********************************************************************************************************************/

/***********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  --------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  --------------------------------------------------------------------------------------------------------------------
 *  Andreas Wenckebach            AWh           Vector Informatik GmbH
 *  Joern Herwig                  JHg           Vector Informatik GmbH
 *  Marcel Viole                  MVi           Vector Informatik GmbH
 *  Marco Riedl                   Rie           Vector Informatik GmbH
 *  Robert Schaeffner             Rr            Vector Informatik GmbH
 *  Thomas Bezold                 TBe           Vector Informatik GmbH
 *  Alexander Starke              ASe           Vector Informatik GmbH
 *  Andre Caspari                 Ci            Vector Informatik GmbH
 *  Jason Learst                  JLe           Vector CANtech, Inc.
 *  Torben Stoessel               TnS           Vector Informatik GmbH
 *  Fadie Ghraib                  FGh           Vector CANtech, Inc.
 *  Ralf Haegenlaeuer             HRf           Vector Informatik GmbH
 *  Sebastian Loos                Shs           Vector Informatik GmbH
 *  Torben Stoessel               TnS           Vector Informatik GmbH
 *  Junwei Ye                     Jy            Vector Informatik GmbH
 *  Johannes Krimmel              KJs           Vector Informatik GmbH
 *  Achim Strobelt                Ach           Vector Informatik GmbH
 *  --------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  --------------------------------------------------------------------------------------------------------------------
 *  Version    Date        Author  Change Id        Description
 *  --------------------------------------------------------------------------------------------------------------------
 *  03.00.00   2015-07-23  AWh     ESCAN00083431    General rework based on 2.49.00
 *                         JHg     ESCAN00076594    Added support for Volvo AB SLP2, removed support for Volvo AB SLP1
 *  03.01.00   2015-09-29  MVi     ESCAN00085254    Rework for SWCP filtering
 *                         Rie     ESCAN00085358    Added support for MMC SLP8
 *  03.01.01   2015-11-02  Rr      ESCAN00086192    Removed compiler warning for redefined macro
 *  03.02.00   2015-12-16  Ci      ESCAN00086473    Adapted configuration switches
 *                         AWh     ESCAN00086840    Gm: Allow passing Sbat to Fbl via CanInitTable explicitly
 *                         TBe     ESCAN00087175    Adapted configuration switches
 *  03.03.00   2016-01-11  ASe     ESCAN00087377    Corrected implementation of FblInvert...Bit() macros
 *  03.04.00   2016-01-13  Ci      ESCAN00087439    Removed legacy code
 *  03.04.01   2016-02-25  JLe     ESCAN00088188    Adapted switches for S12X hardware in CanInitTable
 *                         TnS     ESCAN00088433    Moved assertion specific code to fbl_assert.h
 *  03.04.02   2016-03-09  FGh     ESCAN00088723    Added support for RH850/IAR use case
 *                         HRf     ESCAN00088811    Added CanInitTable bit timing entry for Traveo
 *                                 ESCAN00088783    Adapted configuration switches for V850 hardware in CanInitTable
 *  03.05.00   2016-03-17  Shs     ESCAN00088931    Added support for PATAC SLP2
 *  03.06.00   2016-06-30  Ci      ESCAN00090448    Removed key export configuration and adapted FblHeader
 *  03.07.00   2016-10-21  TnS     ESCAN00092475    Added support for Toyota SLP3
 *  03.08.00   2016-10-28  Shs     ESCAN00092626    Moved Elements from FblHeader to Common Data
 *  03.08.01   2016-11-23  HRf     ESCAN00092964    Change C_COMP_KEIL_FM3_CCAN_COMMENT to C_COMP_KEIL_FM3_COMMENT
 *  03.08.02   2017-01-26  Jy      ESCAN00093738    Adapted switches for S32K hardware with IAR Compiler in CanInitTable
 *  03.09.00   2017-02-09  Rie     ESCAN00093922    Removed deprecated code
 *                         KJs     ESCAN00093947     Added CanInitTable bit timing entry for MN103
 *  03.09.01   2017-05-09  JHg     ESCAN00095090    Include upper-case Fbl_Cfg.h for DaVinci Configurator use-case
 *  03.09.02   2017-05-19  Rie     ESCAN00095204    No changes
 *                         Ach     ESCAN00095245    S12Z is not filtered correctly
 *  03.10.00   2017-07-05  JHg     ESCAN00095765    Enable additional programming request codes for Volvo AB SLP2
 **********************************************************************************************************************/

#ifndef __FBL_DEF_H__
#define __FBL_DEF_H__

/***********************************************************************************************************************
 *  INCLUDES
 **********************************************************************************************************************/

/* Basic configurations */
#include "v_cfg.h"

#if defined( VGEN_GENY ) && !defined( VGEN_ENABLE_CANFBL )
/* this file was obviously not included in FBL, so it's used in user application
   check if MAGIC_NUMBER was generated. In this case we have to remove the check for the
   following include because FBL generation use different number than application */
# if defined( MAGIC_NUMBER )
#  undef MAGIC_NUMBER
# endif
#endif
/* Configuration file for flash boot loader */
#if defined( V_GEN_GENERATOR_MSR )
# include "Fbl_Cfg.h"
#else /* VGEN_GENY */
# include "fbl_cfg.h"
#endif /* VGEN_GENY */
#if defined( VGEN_GENY ) && !defined( VGEN_ENABLE_CANFBL )
/* the last include redefine MAGIC_NUMBER, which is not relevant for application so
   invalidate it */
# if defined( MAGIC_NUMBER )
#  undef MAGIC_NUMBER
# endif
#endif

/* Basic type definitions */
#include "v_def.h"


/* In application either remove FBL_ENABLE_ASSERTION switch or provide fbl_assert.h */
# include "fbl_assert.h"

/* PRQA S 3453 EOF */ /* MD_MSR_19.7 */
/* PRQA S 3458 EOF */ /* MD_MSR_19.4 */

/***********************************************************************************************************************
 *  VERSION
 **********************************************************************************************************************/

/* ##V_CFG_MANAGEMENT ##CQProject : FblDef CQComponent : Implementation */
#define FBLDEF_VERSION          0x0310u
#define FBLDEF_RELEASE_VERSION  0x00u

/***********************************************************************************************************************
 *  DEFINES
 **********************************************************************************************************************/

#ifndef NULL
# define NULL ((void *)0)
#endif

#ifndef SWM_DATA_ALIGN
# define SWM_DATA_ALIGN 0
#endif

/* CanTransmit return values */
# ifndef kCanTxOk
#  define kCanTxOk             0/* Msg transmitted                        */
# endif
# ifndef kCanTxFailed
#  define kCanTxFailed         1/* Tx path switched off                   */
# endif
# define kCanTxInProcess       2

/* Define return code of several functions   */
#define kFblOk                0
#define kFblFailed            1

/* Parameters for ApplFblStartup() */
#define kStartupPreInit       0
#define kStartupPostInit      1
#define kStartupStayInBoot    2

/* Programming request flag */
#define kProgRequest         (tFblProgStatus)0x01u
#define kNoProgRequest       (tFblProgStatus)FblInvert8Bit(kProgRequest)

# define kProgRequestWithResponse          0x02  /* External programming request with response required */
# define kProgRequestNoResponse            0x03  /* No response required to programming request */
/* Application validation  */
# define kApplValid            1 /* Application is fully programmed */
# define kApplInvalid          0 /* Operational software is missing */

/* Memory status (flash erased detection) */
# define kFlashErased          1
# define kFlashNotErased       0


/* Define to access the FBL header structure */
# define FblHeaderTable ((V_MEMROM1_FAR tFblHeader V_MEMROM2_FAR V_MEMROM3 *)(FBL_HEADER_ADDRESS))
# define FblHeaderLocal ((V_MEMROM1_FAR tFblHeader V_MEMROM2_FAR V_MEMROM3 *)(&FblHeader))

#if !defined( FBL_REPEAT_CALL_CYCLE )
/* Set default to 1ms for repeat time of main loop */
# define FBL_REPEAT_CALL_CYCLE 1
#endif

#if defined( FBL_ENABLE_BANKING )
#else
# define FBL_CALL_TYPE
#endif /* FBL_ENABLE_BANKING */

#ifndef V_CALLBACK_NEAR
# define V_CALLBACK_NEAR
#endif
#ifndef V_API_NEAR
# define V_API_NEAR
#endif


#if defined( FBL_ENABLE_FBL_START )
/* Define pattern for magic flags used for reprogramming indication */
# define kFblStartMagicByte0   0x50u /* 'P' */
# define kFblStartMagicByte1   0x72u /* 'r' */
# define kFblStartMagicByte2   0x6Fu /* 'o' */
# define kFblStartMagicByte3   0x67u /* 'g' */
# define kFblStartMagicByte4   0x53u /* 'S' */
# define kFblStartMagicByte5   0x69u /* 'i' */
# define kFblStartMagicByte6   0x67u /* 'g' */
# define kFblStartMagicByte7   0x6Eu /* 'n' */
# define kFblNoOfMagicBytes    8u

# define FblSetFblStartMagicFlag() \
{ \
   fblStartMagicFlag[0] = kFblStartMagicByte0; \
   fblStartMagicFlag[1] = kFblStartMagicByte1; \
   fblStartMagicFlag[2] = kFblStartMagicByte2; \
   fblStartMagicFlag[3] = kFblStartMagicByte3; \
   fblStartMagicFlag[4] = kFblStartMagicByte4; \
   fblStartMagicFlag[5] = kFblStartMagicByte5; \
   fblStartMagicFlag[6] = kFblStartMagicByte6; \
   fblStartMagicFlag[7] = kFblStartMagicByte7; \
}

# define FblChkFblStartMagicFlag() \
   ((    (fblStartMagicFlag[0] == kFblStartMagicByte0) \
      && (fblStartMagicFlag[1] == kFblStartMagicByte1) \
      && (fblStartMagicFlag[2] == kFblStartMagicByte2) \
      && (fblStartMagicFlag[3] == kFblStartMagicByte3) \
      && (fblStartMagicFlag[4] == kFblStartMagicByte4) \
      && (fblStartMagicFlag[5] == kFblStartMagicByte5) \
      && (fblStartMagicFlag[6] == kFblStartMagicByte6) \
      && (fblStartMagicFlag[7] == kFblStartMagicByte7)) ? 1u : 0u)

# define FblClrFblStartMagicFlag() \
{ \
   vuint8 byteIndex; \
   for (byteIndex = 0; byteIndex < kFblNoOfMagicBytes; byteIndex++) \
   { \
      fblStartMagicFlag[byteIndex] = 0x00u; \
   } \
}
#endif /* #if defined( FBL_ENABLE_FBL_START ) */

/* Defines to convert BigEndian bytes into short or long values ********************/
# if defined( C_CPUTYPE_BIGENDIAN )
#  if defined( C_CPUTYPE_32BIT )
#   define FblBytesToShort(hi,lo)            (((vuint16)(hi) << 8) | ((vuint16)(lo) ))
#  else
#   define FblBytesToShort(hi,lo)            (vuint16)*(V_MEMRAM1_FAR vuint16 V_MEMRAM2_FAR *)(&(hi))
#  endif
# endif
# if defined( C_CPUTYPE_LITTLEENDIAN )
#  define FblBytesToShort(hi,lo)              (((vuint16)(hi) << 8) | ((vuint16)(lo) ))
# endif

# if defined( C_CPUTYPE_BIGENDIAN )
#  if defined( C_CPUTYPE_32BIT )
#   define FblBytesToLong(hiWrd_hiByt,hiWrd_loByt,loWrd_hiByt,loWrd_loByt)  \
                                         (((vuint32)(hiWrd_hiByt) << 24) |  \
                                          ((vuint32)(hiWrd_loByt) << 16) |  \
                                          ((vuint32)(loWrd_hiByt) <<  8) |  \
                                          ((vuint32)(loWrd_loByt)      )  )
#  else
#   define FblBytesToLong(hiWrd_hiByt,hiWrd_loByt,loWrd_hiByt, loWrd_loByt)  \
            (vuint32)*(V_MEMRAM1_FAR vuint32 V_MEMRAM2_FAR *)(&(hiWrd_hiByt))
#  endif
# endif
# if defined( C_CPUTYPE_LITTLEENDIAN )
#  define FblBytesToLong(hiWrd_hiByt,hiWrd_loByt,loWrd_hiByt,loWrd_loByt)  \
                                        (((vuint32)(hiWrd_hiByt) << 24) |  \
                                         ((vuint32)(hiWrd_loByt) << 16) |  \
                                         ((vuint32)(loWrd_hiByt) <<  8) |  \
                                         ((vuint32)(loWrd_loByt)      )  )
# endif

#define FBL_BIT0   0x01u
#define FBL_BIT1   0x02u
#define FBL_BIT2   0x04u
#define FBL_BIT3   0x08u
#define FBL_BIT4   0x10u
#define FBL_BIT5   0x20u
#define FBL_BIT6   0x40u
#define FBL_BIT7   0x80u

/* Macros and values for fblMode */
#define START_FROM_APPL          FBL_BIT0
#define START_FROM_RESET         FBL_BIT1
#define APPL_CORRUPT             FBL_BIT2
#define STAY_IN_FLASHER          FBL_BIT3
#define FBL_RESET_REQUEST        FBL_BIT4
#define WAIT_FOR_PING            FBL_BIT5
#define FBL_START_WITH_RESP      FBL_BIT6
#define FBL_START_WITH_PING      FBL_BIT7
#define SetFblMode(state)        (fblMode = (vuint8)((fblMode & 0xF0u) | (state)))
#define GetFblMode()             ((vuint8)(fblMode & 0x0Fu))
#define FblSetShutdownRequest()  SetFblMode(FBL_RESET_REQUEST)

/* Defines for response after reset */
#  define RESET_RESPONSE_NOT_REQUIRED         ((vuint8)0x00)
#  define RESET_RESPONSE_SDS_REQUIRED         ((vuint8)0x01)
#  define RESET_RESPONSE_ECURESET_REQUIRED    ((vuint8)0x02)
#  define RESET_RESPONSE_KEYOFFON_REQUIRED    ((vuint8)0x03)

#  define GetFblBuildVersion()   (FblHeaderTable->FblGenyVersion7)

/* Access macros for FblHeader elements for application */
# define GetFblMainVersion()     (FblHeaderTable->kFblMainVersion)
# define GetFblSubVersion()      (FblHeaderTable->kFblSubVersion)
# define GetFblReleaseVersion()  (FblHeaderTable->kFblBugFixVersion)
# if defined( FBL_ENABLE_COMMON_DATA )
#  define GetFblCommonDataPtr()  (FblHeaderTable->pFblCommonData)
# endif


#if defined( FBL_ENABLE_FBL_START )
#  define FBL_START_PARAM       ((void *)0)
#   define CallFblStart(pParam)  (FblHeaderTable->FblStartFct)((pParam))
#endif /* FBL_ENABLE_FBL_START */

/* Macros to harmonize type casts for inverting bits */
/* FblInvertBits(x,type) is deprecated and should not be used anymore. */
#define FblInvertBits(x,type)    ((type)~((type)(x))) /* PRQA S 0277 */ /* MD_FblDef_Invert */
#define FblInvert8Bit(x)         ((vuint8) ((x) ^ ((vuint8)0xFFu)))
#define FblInvert16Bit(x)        ((vuint16)((x) ^ ((vuint16)0xFFFFu)))
#define FblInvert32Bit(x)        ((vuint32)((x) ^ ((vuint32)0xFFFFFFFFul)))




# define kFblDiagTimeP2             FBL_DIAG_TIME_P2MAX
# define kFblDiagTimeP2Star         FBL_DIAG_TIME_P3MAX






#  define kEepFblReprogram       0xB5u
#  define kEepFblActive          0xA4u
#  define kEepFblVpm             0xC6u
#  define kEepFblLinkControl     0x93u

# define kEepWriteData           1u
# define kEepReadData            2u

/* Error code defines for ApplFblErrorNotification */
# define kFblErrTypeAppl         0x10u
# define kFblErrTypeFlash        0x20u
# define kFblErrTypeEeprom       0x30u
# define kFblErrTypeSec          0x40u
# define kFblErrTypeVectorTPMC   0x50u




/***********************************************************************************************************************
 *  TYPEDEFS (basic types)
 **********************************************************************************************************************/
typedef vuint8  tFblErrorType;
typedef vuint16 tFblErrorCode;

typedef vuint8 tFblResult;                    /**< FBL result codes */
typedef vuint8 tFblProgStatus;                /**< Status of reprogramming flag */
typedef vuint8 tApplStatus;                   /**< Application valid status */
typedef vuint8 tMagicFlag;                    /**< Application valid flag */
typedef vuint8 tFlashStatus;                  /**< Flash erased status flag */

/* The below bootloader address types always define logical addresses.              */
/* This is important for those platforms which provide a paged physical memory      */
/* address space. For those platforms with linear physical address space, logical   */
/* and physical addresses are identical.                                            */
#if defined( C_CPUTYPE_8BIT ) && !defined( FBL_PROCESSOR_BANKED )
typedef vuint16 FBL_ADDR_TYPE;
typedef vuint16 FBL_MEMSIZE_TYPE;
# define MEMSIZE_OK
#else
typedef vuint32 FBL_ADDR_TYPE;
typedef vuint32 FBL_MEMSIZE_TYPE;
# define MEMSIZE_OK
#endif
#if defined( MEMSIZE_OK )
#else
# error "Error in FBL_DEF.H: C_CPUTYPE_ not defined."
#endif

typedef vuint8 FBL_MEMID_TYPE;

typedef FBL_ADDR_TYPE       tFblAddress;
typedef FBL_MEMSIZE_TYPE    tFblLength;

typedef FBL_ADDR_TYPE       tMtabAddress;
typedef FBL_MEMSIZE_TYPE    tMtabLength;
typedef FBL_MEMID_TYPE      tMtabMemId;

typedef vuint16 tChecksum;

/***********************************************************************************************************************
 *  TYPEDEFS (used by below function pointers)
 **********************************************************************************************************************/

typedef void tCanInitTable;

/***********************************************************************************************************************
 *  TYPEDEFS (function pointer, used by below structs)
 **********************************************************************************************************************/

/* Function pointer for FBL-exported functions */
#if defined( FBL_ENABLE_BANKING )
typedef FBL_CALL_TYPE void (*FBL_CALL_TYPE tExportFct)(void); /* PRQA S 0313 */ /* MD_FblDef_Export */
#else
typedef MEMORY_FAR void (*tExportFct)(void); /* PRQA S 0313 */ /* MD_FblDef_Export */
#endif /* FBL_ENABLE_BANKING */

typedef FBL_CALL_TYPE vuint8 (* tFblRealtimeFct)(void);

/** Function pointer to read memory */
typedef FBL_CALL_TYPE tFblLength (* tReadMemoryFct)(tFblAddress address, V_MEMRAM1 vuint8 V_MEMRAM2 V_MEMRAM3 * buffer, tFblLength length);

typedef MEMORY_HUGE MEMORY_FAR void (* tFblStrtFct)(V_MEMRAM1 tCanInitTable V_MEMRAM2 V_MEMRAM3 *);

/***********************************************************************************************************************
 *  TYPEDEFS (other)
 **********************************************************************************************************************/

#if defined( FBL_ENABLE_DATA_PROCESSING )
typedef struct tagProcParam
{
   V_MEMRAM1 vuint8 V_MEMRAM2 V_MEMRAM3 * dataBuffer;
   vuint16        dataLength;
   V_MEMRAM1 vuint8 V_MEMRAM2 V_MEMRAM3 * dataOutBuffer;
   vuint16        dataOutLength;
   vuint16        dataOutMaxLength;
   vuint8         (* wdTriggerFct)(void);
   vuint8         mode;
} tProcParam;
#endif

/* Far pointer types to store pointer addresses in FblHeader to either RAM or ROM data */
#if defined( FBL_ENABLE_COMMON_DATA )
/* Pointer to shared ROM constants */
typedef V_MEMROM1_FAR void V_MEMROM2_FAR V_MEMROM3 * tFblCommonDataPtr;
#endif
typedef V_MEMRAM1_FAR tCanInitTable V_MEMRAM2_FAR V_MEMROM3 * tFblCanInitDataPtr;
typedef V_MEMROM1_FAR vuint8 V_MEMROM2_FAR V_MEMROM3 * tFblHeaderRomArrayPtr;
typedef V_MEMRAM1_FAR vuint16 V_MEMRAM2_FAR V_MEMROM3 * tFblHeaderMagicWordPtr;
typedef V_MEMROM1_FAR struct tLogicalBlockTableTag V_MEMROM2_FAR V_MEMROM3 * tFblHeaderLogicalBlockTablePtr;



typedef vuint8 *tCRCValue;
typedef vuint8 tFblData;

/** Structure for address and length information of segments */
typedef struct tagSegmentInfo
{
   tFblAddress targetAddress;
   tFblLength  length;
} tSegmentInfo;

/** Segment data structure */
typedef struct tagSegmentList
{
   vuint8         nrOfSegments;
   tSegmentInfo   segmentInfo[SWM_DATA_MAX_NOAR];
} tSegmentList;



/* Header of the FBL */
typedef struct tagFblHeader
{
   vuint8    kFblMainVersion;
   vuint8    kFblSubVersion;
   vuint8    kFblBugFixVersion;
   vuint8    FblHeaderAlign1;            /**< Alignment to even addresses */
   tFblHeaderLogicalBlockTablePtr kLbtAddress;
# if defined( FBL_ENABLE_COMMON_DATA )
   tFblCommonDataPtr pFblCommonData;
# endif
# if defined( FBL_ENABLE_FBL_START )
   tFblStrtFct FblStartFct;            /**< Pointer to FblStart-function */
# endif
} tFblHeader;



/***********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 **********************************************************************************************************************/

#if defined( FBL_ENABLE_FBL_START )
void V_CALLBACK_NEAR FblStart( V_MEMRAM1 tCanInitTable V_MEMRAM2 V_MEMRAM3 * pCanInitTable );
#endif /* FBL_ENABLE_FBL_START */


/***********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 **********************************************************************************************************************/

V_MEMRAM0 extern V_MEMRAM1_NEAR vuint8 V_MEMRAM2_NEAR fblMode;

#define FBLHEADER_START_SEC_CONST
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
V_MEMROM0 extern V_MEMROM1 tFblHeader V_MEMROM2 FblHeader;
#define FBLHEADER_STOP_SEC_CONST
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


#if defined( FBL_ENABLE_FBL_START )
# define MAGICFLAG_START_SEC_DATA
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
/* Entering bootloader with 8 byte special ('FBLSTARTMAGIC') values */
V_MEMRAM0 extern volatile V_MEMRAM1 vuint8 V_MEMRAM2 fblStartMagicFlag[kFblNoOfMagicBytes];
# define MAGICFLAG_STOP_SEC_DATA
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#endif /* FBL_ENABLE_FBL_START */

#endif /* __FBL_DEF_H__ */

/* Module specific MISRA deviations:

   MD_FblDef_Export:
     Reason: Verify interface functions are stored as void pointers as actual type is not known at configuration time.
             The software that uses these functions has to take care that a cast to the correct function type is being done before usage
     Risk: The software that uses this elements does not correctly cast before usage.
     Prevention: Review of implementations and testing of the functionality.

   MD_FblDef_Invert:
     Reason: Usage of FblInvert.. macros implies usage of ~ operator and appropriate casts. This includes a cast from
             negative integer to unsigned value. The integer is only an intermediate type that occurs because of
             integer promotion while using ~ operator, though only a bit mask is operated on.
     Risk: No risk.
     Prevention: No prevention defined.

*/

/***********************************************************************************************************************
 *  END OF FILE: FBL_DEF.H
 **********************************************************************************************************************/

