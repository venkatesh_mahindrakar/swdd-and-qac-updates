/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Fbl
 *           Program: FBL_VolvoAb_SLP2
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: MPC5746C
 *    License Scope : The usage is restricted to CBD1800195_D01
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Fbl_Lbt.h
 *   Generation Time: 2020-05-29 18:47:34
 *           Project: SCIM_HD_T1_FBL - Version 1.0
 *          Delivery: CBD1800195_D01
 *      Tool Version: DaVinci Configurator  5.16.41 SP3
 *
 *
 *********************************************************************************************************************/


#if !defined(__FBL_MTAB_H__)
#define __FBL_MTAB_H__

/* Defines ***********************************************************************************************************/
#define FBL_MTAB_APPLICATION_BLOCK_NUMBER  0 
#define FBL_MTAB_DST_VAR_BLOCK_NUMBER      1 
#define FBL_MTAB_POSTBUILDDST_BLOCK_NUMBER 2 
#define kBlockTypeCode                     0x0000009AUL 
#define kBlockTypeData                     0x0000009BUL 
#define FBL_MTAB_NO_OF_BLOCKS              3 
#define kNrOfValidationBytes               1 

/* Macro to determine if the block table is valid */
#define FBL_MTAB_MAGIC_FLAG               0xB5A4C3D2UL
#define IsLogicalBlockTableValid()        (FblLogicalBlockTable.magicFlag == FBL_MTAB_MAGIC_FLAG)

/* Typedefs **********************************************************************************************************/

/* Entry type of logical block table */
typedef struct tBlockDescriptorTag {
  vuint8 blockNr; /*  Number of logical block  */ 
  vuint32 blockType;
  tFblAddress blockStartAddress; /*  Start address of current block  */ 
  tFblLength blockLength; /*  Block length in bytes  */ 
  tExportFct verifyPipelined; /*  Pipelined Verification  */ 
  tExportFct verifyOutput; /*  Output Verification  */ 
} tBlockDescriptor;

/* The logical block table describes the memory layout of logical blocks */
typedef struct tLogicalBlockTableTag {
  vuint32 magicFlag; /*  Value of FBL_MTAB_MAGIC_FLAG - Indicates the existence of the table  */ 
  vuint8 assignFlags[kNrOfValidationBytes]; /*  Disposability bit field (mandatory/optional)  */ 
  vuint8 noOfBlocks; /*  Number of configured logical blocks  */ 
  tBlockDescriptor logicalBlock[FBL_MTAB_NO_OF_BLOCKS];
} tLogicalBlockTable;

/* Global data *******************************************************************************************************/
V_MEMROM0 extern  V_MEMROM1 tLogicalBlockTable V_MEMROM2 FblLogicalBlockTable;

#endif /* __FBL_MTAB_H__ */

