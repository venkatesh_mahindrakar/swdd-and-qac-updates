/***********************************************************************************************************************
 *  FILE DESCRIPTION
 *  ------------------------------------------------------------------------------------------------------------------*/
/** \file
 *  \brief         Definition of common data structures which are shared between
 *                 bootloader and application software
 *
 *  --------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT
 *  --------------------------------------------------------------------------------------------------------------------
 *  \par Copyright
 *  \verbatim
 *  Copyright (c) 2015 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *
 *  \par Note
 *  \verbatim
 *  Please note, that this file contains a collection of callback functions to be used with the Flash Bootloader. 
 *  These functions may influence the behaviour of the bootloader in principle. 
 *  Therefore, great care must be taken to verify the correctness of the implementation.
 * 
 *  The contents of the originally delivered files are only examples resp. implementation proposals. 
 *  With regard to the fact that these functions are meant for demonstration purposes only, Vector Informatik�s 
 *  liability shall be expressly excluded in cases of ordinary negligence, to the extent admissible by law or statute. 
 *  \endverbatim  
 */
/**********************************************************************************************************************/

/***********************************************************************************************************************
 *  AUTHOR IDENTITY
 *  --------------------------------------------------------------------------------------------------------------------
 *  Name                          Initials      Company
 *  --------------------------------------------------------------------------------------------------------------------
 *  Achim Strobelt                Ach           Vector Informatik GmbH
 *  --------------------------------------------------------------------------------------------------------------------
 *  REVISION HISTORY
 *  --------------------------------------------------------------------------------------------------------------------
 *  Version    Date        Author  Change Id        Description
 *  --------------------------------------------------------------------------------------------------------------------
 *  01.00.00   2014-09-11  JHg     -                Initial version
 *  01.01.00   2015-07-24  JHg     -                Release for series production
 **********************************************************************************************************************/

#ifndef __COMDAT_H__
#define __COMDAT_H__

/* Switch to identify bootloader context, disable for application context */
//#define VGEN_ENABLE_CANFBL

/***********************************************************************************************************************
 *  DEFINES
 **********************************************************************************************************************/

/* Function access macros for security module function */
/** Security access: generate seed */
#define ApplCommonSecGenerateSeed(a)            ((pSecGenerateSeedFct)fblCommonData->pSecGenerateSeedFct))(a)
/** Security access: compare key */
#define ApplCommonSecCompareKey(a, b)           ((pSecCompareKeyFct)fblCommonData->pSecCompareKeyFct))(a, b)

/** Verification: compute CRC-32 */
#define ApplCommonSecComputeCRC(a)              ((pSecComputeCRCFct)fblCommonData->pSecComputeCRCFct))(a)
/** Verification: signature verification (stream API) */
#define ApplCommonSecVerifySignatureFct(a)      ((pSecVerifySignatureFct)fblCommonData->pSecVerifySignatureFct))(a)
/** Verification: signature verification (initialize monolithic API) */
#define ApplCommonSecInitVerificationFct(a)     ((pSecInitVerificationFct)fblCommonData->pSecInitVerificationFct))(a)
/** Verification: signature verification (monolithic API) */
#define ApplCommonSecVerificationFct(a)         ((pSecVerificationFct)fblCommonData->pSecVerificationFct))(a)
/** Verification: signature verification (de-initialize monolithic API) */
#define ApplCommonSecDeinitVerificationFct(a)   ((pSecDeinitVerificationFct)fblCommonData->pSecDeinitVerificationFct))(a)

#if defined( VGEN_ENABLE_CANFBL )
#else
/** Common data access macro for usage in the application software */
# define fblCommonData   (*((V_MEMROM1_FAR tFblCommonData V_MEMROM2_FAR *)GetFblCommonDataPtr()))
#endif

/***********************************************************************************************************************
 *  TYPEDEFS
 **********************************************************************************************************************/

/** Generic pointer used for exported functions */
typedef void (*tFblCommonExportFct)(void);

/** Security key description */
typedef struct
{
   V_MEMROM1_FAR vuint8 V_MEMROM2_FAR V_MEMROM3 * pData;    /**< Pointer to key data */
   vuint16 size;                                            /**< Size of key data */
} tFblCommonSecKey;

/** RSA key pair description */
typedef struct
{
   tFblCommonSecKey  modulus;       /**< RSA key modulus */
   tFblCommonSecKey  exponent;      /**< RSA key exponent */
} tFblCommonSecRsaKey;

/** Description of memory driver stored in ROM array */
typedef struct
{
   V_MEMROM1_FAR vuint8 V_MEMROM2_FAR V_MEMROM3 * pData;    /**< Pointer to ROM array containing (encrypted) memory driver */
   vuint32  address;                                        /**< Execution address of memory driver in RAM */
   vuint32  size;                                           /**< Size of memory driver */
   vuint8   decryptConstant;                                /**< Constant for decryption of ROM array (XOR) */
} tFblCommonMemDrv;

/** Common data structure exported in FBL header */
typedef struct
{
   tFblCommonExportFct  pSecGenerateSeedFct;             /**< Security access: generate seed */
   tFblCommonExportFct  pSecCompareKeyFct;               /**< Security access: compare key */

   tFblCommonExportFct  pSecComputeCrcFct;               /**< Verification: compute CRC-32 */
   tFblCommonExportFct  pSecVerifySignatureFct;          /**< Verification: signature verification (stream API) */
   tFblCommonExportFct  pSecInitVerificationFct;         /**< Verification: signature verification (initialize monolithic API) */
   tFblCommonExportFct  pSecVerificationFct;             /**< Verification: signature verification (monolithic API) */
   tFblCommonExportFct  pSecDeinitVerificationFct;       /**< Verification: signature verification (de-initialize monolithic API) */

   tFblCommonSecRsaKey  secSigRsaKey;                    /**< RSA key pair description used for signature verification */
   tFblCommonSecRsaKey  secSeedKeyRsaKey;                /**< RSA key pair description used for security access */

#if defined( FBL_ENABLE_EEPMGR )
   tFblCommonMemDrv     memoryDriver;                    /**< Memory driver used by EepM */
#endif /* FBL_ENABLE_EEPMGR */
} tFblCommonData;

#endif /* __COMDAT_H__ */

/***********************************************************************************************************************
 *  END OF FILE: _COMDAT.H
 **********************************************************************************************************************/
