/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2016 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  VEC_RomTest.c
 *      Project:  Diag_AsrSwcRomTest_Volvo_AB
 *       Module:  -
 *    Generator:  -
 *
 *  Description:  Background ROM check
 *                Cyclic low priority background task constantly computes checksums and signatures of all logical
 *                block defined by the FBL configuration. Computed values are compared with the original
 *                checksum/signature stored in nonvolatile memory.
 *                Application will be informed in case of read or signature verification errors.
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_Rte_0777, MD_Rte_0779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************

 Data Types:
 ===========
  AsymPublicKeyType
    This type is used for Csm key parameters of type AsymPublicKey

  Csm_ReturnType
    The data type Csm_ReturnType indicates the result of a service request.

  Csm_VerifyResultType
    The  data  type  Csm_VerifyResultType  indicates  the  result  of  a  verification operation (used by services MacVerify and SignatureVerify).

  SignatureVerifyDataBuffer
    Used as Buffer for service.


 Operation Prototypes:
 =====================
  SignatureVerifyFinish of Port Interface CsmSignatureVerify
    This interface shall be used to finish the signature verification service.

  SignatureVerifyStart of Port Interface CsmSignatureVerify
    This interface shall be used to initialize the signature verification service of the CSM module.

  SignatureVerifyUpdate of Port Interface CsmSignatureVerify
    This interface shall be used to feed the signature verification service with the input data.

 *********************************************************************************************************************/

#include "Rte_VEC_RomTest.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "VEC_RomTest.h"
#include "VEC_RomTest_Cfg.h"

#include "Cry_Key_Types.h"
#include "Crc.h"

//#define VEC_ROMTEST_ENABLE


/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/
 /* Check the version of the header file */
#if (  (VEC_ROMTEST_SW_MAJOR_VERSION != (4u)) \
    || (VEC_ROMTEST_SW_MINOR_VERSION != (0u)) \
    || (VEC_ROMTEST_SW_PATCH_VERSION != (1u)) )
# error "Vendor specific version numbers of VEC_RomTest.c and VEC_RomTest.h are inconsistent"
#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 **********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 **********************************************************************************************************************/

/** States of state machine */
typedef enum
{
  kVecRomTestStateSetupType = 0,    /**< Initial setup: read signature types */
  kVecRomTestStateSetupValidity,    /**< Initial setup: evaluate block validity */
  kVecRomTestStateSetupBlock,       /**< Initial setup: loop over all blocks */
  kVecRomTestStateIdle,             /**< Start processing of next block */
  kVecRomTestStateInit,             /**< Initialize signature calculation */
  kVecRomTestStateInitPending,      /**< Wait for pending signature calculation initialization (CSM) */
  kVecRomTestStateSegmentCount,     /**< Read number of segments */
  kVecRomTestStateSegmentAddress,   /**< Read start address of current segment */
  kVecRomTestStateSegmentLength,    /**< Read length of current segment */
  kVecRomTestStateComputeMeta,      /**< Process meta information (update hash with address and length information) */
  kVecRomTestStateReadData,         /**< Read data from memory */
  kVecRomTestStateCompute,          /**< Process data (update hash) */
  kVecRomTestStateComputePending,   /**< Wait for pending data processing (CSM) */
  kVecRomTestStateSignature,        /**< Read signature */
  kVecRomTestStateCompare,          /**< Compare/verify signature */
  kVecRomTestStateComparePending,   /**< Wait for pending verification (CSM) */
  kVecRomTestStateNotify,           /**< Notify application about changed block state */
  kVecRomTestStateError             /**< Error state */
} tVecRomTestState;

/** Task state */
typedef enum
{
  kVecRomTestTaskStateIdle = 0,     /**< Task idle */
  kVecRomTestTaskStateBusy          /**< Task busy */
} tVecRomTestTaskState;

/** Parameters for CRC calculation */
typedef struct
{
  Crc_DataRefType crcDataPtr;       /**< Pointer to input data */
  uint32          crcLength;        /**< Length of input data */
  uint32          crcStartValue32;  /**< (Temporary) CRC value */
  boolean         crcIsFirstCall;   /**< Remember first call to CRC function */
} tVecRomTestCrcParam;

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 **********************************************************************************************************************/
#define VEC_RomTest_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "VEC_RomTest_MemMap.h"

/** Key buffer modulus */
static VAR(uint8, VEC_RomTest_VAR_NOINIT)                     vecRomTestKeyModulus[VEC_ROMTEST_SIGNATURE_MAX_LENGTH_MODULUS];

/** Key buffer exponent */
static VAR(uint8, VEC_RomTest_VAR_NOINIT)                     vecRomTestKeyExponent[VEC_ROMTEST_SIGNATURE_MAX_LENGTH_EXPONENT];

/** Key handle for CSM/CRY */
static VAR(Cry_RsaKeyType, VEC_RomTest_VAR_NOINIT)            vecRomTestKey;

/** Verification result */
static VAR(Csm_VerifyResultType, VEC_RomTest_VAR_NOINIT)      vecRomTestVerifyResult;

/** State of state machine */
static VAR(tVecRomTestState, VEC_RomTest_VAR_NOINIT)          vecRomTestState;

/** Task busy state */
static VAR(tVecRomTestTaskState, VEC_RomTest_VAR_NOINIT)      vecRomTestTaskState;

/** Parameters for CRC calculation */
static VAR(tVecRomTestCrcParam, VEC_RomTest_VAR_NOINIT)       vecRomTestCrcParam;

/* Global variables used by cyclic task */
/** Index of processed block */
static VAR(uint8, VEC_RomTest_VAR_NOINIT)                     vecRomTestBlockIdx;
/** Changed block state */
static VAR(tVecRomTestBlockState, VEC_RomTest_VAR_NOINIT)     vecRomTestNotifyBlockState;
/** Number of blocks (software components) */
static VAR(uint8_least, VEC_RomTest_VAR_NOINIT)               vecRomTestBlockCount;
/** Cached states of all blocks */
static VAR(tVecRomTestBlockState, VEC_RomTest_VAR_NOINIT)     vecRomTestBlockState[VEC_ROMTEST_MAX_BLOCK_COUNT];
/** Cached signature types of all blocks */
static VAR(tVecRomTestSignatureType, VEC_RomTest_VAR_NOINIT)  vecRomTestSigTypes[VEC_ROMTEST_MAX_BLOCK_COUNT];
/** Current segment */
static VAR(uint8, VEC_RomTest_VAR_NOINIT)                     vecRomTestSegmentIdx;
/** Segment count */
static VAR(uint8, VEC_RomTest_VAR_NOINIT)                     vecRomTestSegmentCount;
/** Memory address */
static VAR(tVecRomTestAddress, VEC_RomTest_VAR_NOINIT)        vecRomTestAddress;
/** Remaining segment length */
static VAR(uint32, VEC_RomTest_VAR_NOINIT)                    vecRomTestLength;
/** Type of stored signature */
static VAR(tVecRomTestSignatureType, VEC_RomTest_VAR_NOINIT)  vecRomTestSigType;
/** Buffer for signature */
static VAR(uint8, VEC_RomTest_VAR_NOINIT)                     vecRomTestSigBuffer[VEC_ROMTEST_SIGNATURE_MAX_LENGTH_BUFFER];

/** Temporary RAM buffer for input data */
static VAR(uint8, VEC_RomTest_VAR_NOINIT)                     vecRomTestDataBuffer[VEC_ROMTEST_VERIFY_BYTES];
/** Current length of input data */
static VAR(uint32, VEC_RomTest_VAR_NOINIT)                    vecRomTestReadLength;

#define VEC_RomTest_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "VEC_RomTest_MemMap.h"

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 **********************************************************************************************************************/
#define VEC_RomTest_START_SEC_CODE
#include "VEC_RomTest_MemMap.h"

static FUNC(void, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_ChangeBlockState( uint8 blockIdx, tVecRomTestBlockState blockState );
static FUNC(void, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_CheckNextBlock( void );
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_TriggerState( Csm_ReturnType csmResult );
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_HandleCsmResult( Csm_ReturnType csmResult, tVecRomTestState prevState );

static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateSetupType( void );
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateSetupValidity( void );
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateSetupBlock( void );
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateIdle( void );
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateInit( void );
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateSegmentCount( void );
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateSegmentAddress( void );
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateSegmentLength( void );
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateComputeMeta( void );
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateReadData( void );
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateCompute( void );
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateSignature( void );
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateCompare( void );
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateNotify( void );

#define VEC_RomTest_STOP_SEC_CODE
#include "VEC_RomTest_MemMap.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * UInt32: Integer in interval [0...4294967295]
 * UInt8: Integer in interval [0...255]
 *
 * Enumeration Types:
 * ==================
 * Csm_ReturnType: Enumeration of integer in interval [0...255] with enumerators
 *   CSM_E_OK (0U)
 *   CSM_E_NOT_OK (1U)
 *   CSM_E_BUSY (2U)
 *   CSM_E_SMALL_BUFFER (3U)
 *   CSM_E_ENTROPY_EXHAUSTION (4U)
 * Csm_VerifyResultType: Enumeration of integer in interval [0...255] with enumerators
 *   CSM_E_VER_OK (0U)
 *   CSM_E_VER_NOT_OK (1U)
 * tVecRomTestBlockStatus: Enumeration of integer in interval [1...3] with enumerators
 *   kVecRomTestValid (1U)
 *   kVecRomTestReadFailure (2U)
 *   kVecRomTestSignatureFailure (3U)
 *
 * Array Types:
 * ============
 * Rte_DT_AsymPublicKeyType_1: Array with 128 element(s) of type UInt8
 * SignatureVerifyDataBuffer: Array with 128 element(s) of type UInt8
 *
 * Record Types:
 * =============
 * AsymPublicKeyType: Record with elements
 *   length of type UInt32
 *   data of type Rte_DT_AsymPublicKeyType_1
 *
 *********************************************************************************************************************/


#define VEC_RomTest_START_SEC_CODE
#include "VEC_RomTest_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CallbackNotification
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <JobFinished> of PortPrototype <CsmCallbackSignatureVerify>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_CallbackNotification(Csm_ReturnType retVal)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmCallback_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_RomTest_CODE) VEC_CallbackNotification(Csm_ReturnType retVal) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CallbackNotification (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType result = RTE_E_CsmCallback_CSM_E_NOT_OK;

  /* State handling */
  if (kVecRomTestResultOk == VEC_RomTest_TriggerState(retVal))
  {
    result = RTE_E_OK;
  }
  else
  {
    /* Operation failed, continue with next block */
    VEC_RomTest_CheckNextBlock();
  }

  return result;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_GetBlockStatus
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetStatus> of PortPrototype <VEC_BlockStatus>
 *
 **********************************************************************************************************************
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_VEC_ExclusiveArea(void)
 *   void Rte_Exit_VEC_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_GetBlockStatus(UInt8 blockNr, tVecRomTestBlockStatus *status)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_VEC_BlockStatus_E_NOT_OK
 *
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_RomTest_CODE) VEC_GetBlockStatus(UInt8 blockIdx, P2VAR(tVecRomTestBlockStatus, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) status) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_GetBlockStatus (returns application error)
 *********************************************************************************************************************/
  Std_ReturnType result;
  tVecRomTestBlockState blockState;

  result = RTE_E_VEC_RomTestStatus_RTE_E_VEC_BlockStatus_E_NOT_OK;

  /* Check for valid block number */
  if (blockIdx < vecRomTestBlockCount)
  {
    result = RTE_E_OK;

    Rte_Enter_VEC_ExclusiveArea();
      /* Local copy of block state */
      blockState = vecRomTestBlockState[blockIdx];
    Rte_Exit_VEC_ExclusiveArea();

    switch (blockState)
    {
      case kVecRomTestBlockStateValid:
      case kVecRomTestBlockStateNotPresent:
      {
        /* Block valid or not present */
        *status = kVecRomTestValid;

        break;
      }
      case kVecRomTestBlockStateReadFailure:
      {
        /* Read failure occurred */
        *status = kVecRomTestReadFailure;

        break;
      }
      case kVecRomTestBlockStateSignatureFailure:
      {
        /* Signature verification failed */
        *status = kVecRomTestSignatureFailure;

        break;
      }
      default:
      {
        /* Invalid block state */
        result = RTE_E_VEC_RomTestStatus_RTE_E_VEC_BlockStatus_E_NOT_OK;

        break;
      }
    }
  }

  return result;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_RomTest_Init
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 **********************************************************************************************************************
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_VEC_ExclusiveArea(void)
 *   void Rte_Exit_VEC_ExclusiveArea(void)
 *
 *********************************************************************************************************************/

FUNC(void, VEC_RomTest_CODE) VEC_RomTest_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_RomTest_Init
 *********************************************************************************************************************/
#ifdef VEC_ROMTEST_ENABLE

  uint16 i;
  uint16 modulusSize;
  uint16 exponentSize;

  P2CONST(uint8, AUTOMATIC, RTE_VEC_ROMTEST_APPL_CONST) keyPointer;

  Rte_Enter_VEC_ExclusiveArea();
    vecRomTestTaskState = kVecRomTestTaskStateBusy;
  Rte_Exit_VEC_ExclusiveArea();

  vecRomTestBlockIdx = 0u;
  vecRomTestState = kVecRomTestStateSetupType;

  modulusSize = VEC_RomTest_GetModulusSize();
  exponentSize = VEC_RomTest_GetExponentSize();

  /* Check if the key fit into the provided buffers */
  if ( ( exponentSize <= VEC_ROMTEST_SIGNATURE_MAX_LENGTH_EXPONENT )
    && ( modulusSize <= VEC_ROMTEST_SIGNATURE_MAX_LENGTH_MODULUS ) )
  {
    /* Set local key pointer to key data location */
    keyPointer = VEC_RomTest_GetExponent();

    /* Copy exponent key data */
    for ( i = 0u; i < exponentSize; i++ )
    {
      vecRomTestKeyExponent[i] = keyPointer[i];
    }

    /* Set key for exponent */
    vecRomTestKey.keyExponent = vecRomTestKeyExponent;
    vecRomTestKey.keyExponentLength = exponentSize;

    /* Set local key pointer to key data location */
    keyPointer = VEC_RomTest_GetModulus();

    /* Copy modulus key data */
    for ( i = 0u; i < modulusSize; i++ )
    {
      vecRomTestKeyModulus[i] = keyPointer[i];
    }

    /* Set key for modulus */
    vecRomTestKey.keyModule = vecRomTestKeyModulus;
    vecRomTestKey.keyModuleLength = modulusSize;
  }
  else
  {
    /* Disable background task */
    vecRomTestState = kVecRomTestStateError;

    /* Notify application */
    VEC_RomTest_FatalError(kVecRomTestFatalErrorKeySizeExceedsBufferSize);
  }

  vecRomTestBlockCount = VEC_RomTest_GetBlockCount();

  /* Check static size of block state array */
  if (vecRomTestBlockCount > VEC_ROMTEST_MAX_BLOCK_COUNT)
  {
    /* Size of block state array insufficient! */

    /* Disable background task */
    vecRomTestState = kVecRomTestStateError;

    /* Notify application */
    VEC_RomTest_FatalError(kVecRomTestFatalErrorInsufficientBlockStateSize);
  }
  else
  {
    Rte_Enter_VEC_ExclusiveArea();
      /* Initialize block states */
      for (i = 0u; i < vecRomTestBlockCount; i++)
      {
          vecRomTestBlockState[i] = kVecRomTestBlockStateUndefined;
      }
    Rte_Exit_VEC_ExclusiveArea();
  }

  /* Initialize callout functions */
  VEC_RomTest_InitCallouts();

  Rte_Enter_VEC_ExclusiveArea();
    vecRomTestTaskState = kVecRomTestTaskStateIdle;
  Rte_Exit_VEC_ExclusiveArea();
#endif
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_RomTest_MainFunction
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmSignatureVerify_SignatureVerifyFinish(const UInt8 *signatureBuffer, UInt32 signatureLength, Csm_VerifyResultType *resultBuffer)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSignatureVerify_CSM_E_BUSY, RTE_E_CsmSignatureVerify_CSM_E_NOT_OK, RTE_E_CsmSignatureVerify_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmSignatureVerify_SignatureVerifyStart(const AsymPublicKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSignatureVerify_CSM_E_BUSY, RTE_E_CsmSignatureVerify_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmSignatureVerify_SignatureVerifyUpdate(const UInt8 *dataBuffer, UInt32 dataLength)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSignatureVerify_CSM_E_BUSY, RTE_E_CsmSignatureVerify_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/

FUNC(void, VEC_RomTest_CODE) VEC_RomTest_MainFunction(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_RomTest_MainFunction
 *********************************************************************************************************************/
#ifdef VEC_ROMTEST_ENABLE

  tVecRomTestResult result;
  tVecRomTestTaskState taskState;

  result = kVecRomTestResultOk;

  Rte_Enter_VEC_ExclusiveArea();
    /* Get current state of task */
    taskState = vecRomTestTaskState;
    /* Task will be busy regardless of previous state */
    vecRomTestTaskState = kVecRomTestTaskStateBusy;
  Rte_Exit_VEC_ExclusiveArea();

  /* Prevent multiple concurrent task executions */
  if (kVecRomTestTaskStateIdle == taskState)
  {
    /* State machine */
    switch (vecRomTestState)
    {
      /* Initial setup: read signature types */
      case kVecRomTestStateSetupType:
      {
        result = VEC_RomTest_StateSetupType();

        break;
      }
      /* Initial setup: evaluate block validity */
      case kVecRomTestStateSetupValidity:
      {
        result = VEC_RomTest_StateSetupValidity();

        break;
      }
      /* Initial setup: loop over all blocks */
      case kVecRomTestStateSetupBlock:
      {
        result = VEC_RomTest_StateSetupBlock();

        break;
      }
      /* Start processing of next block */
      case kVecRomTestStateIdle:
      {
        result = VEC_RomTest_StateIdle();

        break;
      }
      /* Initialize signature calculation */
      case kVecRomTestStateInit:
      {
        result = VEC_RomTest_StateInit();

        break;
      }
      /* Read number of segments */
      case kVecRomTestStateSegmentCount:
      {
        result = VEC_RomTest_StateSegmentCount();

        break;
      }
      /* Read start address of current segment */
      case kVecRomTestStateSegmentAddress:
      {
        result = VEC_RomTest_StateSegmentAddress();

        break;
      }
      /* Read length of current segment */
      case kVecRomTestStateSegmentLength:
      {
        result = VEC_RomTest_StateSegmentLength();

        break;
      }
      /* Process meta information (update hash with address and length information) */
      case kVecRomTestStateComputeMeta:
      {
        result = VEC_RomTest_StateComputeMeta();

        break;
      }
      /* Read data from memory */
      case kVecRomTestStateReadData:
      {
        result = VEC_RomTest_StateReadData();

        break;
      }
      /* Process data (update hash) */
      case kVecRomTestStateCompute:
      {
        result = VEC_RomTest_StateCompute();

        break;
      }
      /* Read signature */
      case kVecRomTestStateSignature:
      {
        result = VEC_RomTest_StateSignature();

        break;
      }
      /* Compare/verify signature */
      case kVecRomTestStateCompare:
      {
        result = VEC_RomTest_StateCompare();

        break;
      }
      /* Notify application about changed block state */
      case kVecRomTestStateNotify:
      {
        result = VEC_RomTest_StateNotify();

        break;
      }
      /* Pending CSM operations */
      case kVecRomTestStateInitPending:
      case kVecRomTestStateComputePending:
      case kVecRomTestStateComparePending:
      /* Error state */
      case kVecRomTestStateError:
      /* Invalid state */
      default:
      {
        /* Do nothing */
        break;
      }
    }

    if (kVecRomTestResultFailed == result)
    {
      /* Switch to next block */
      VEC_RomTest_CheckNextBlock();
    }

    Rte_Enter_VEC_ExclusiveArea();
      /* Leave busy state of task */
      vecRomTestTaskState = kVecRomTestTaskStateIdle;
    Rte_Exit_VEC_ExclusiveArea();
  }
#endif
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

#define VEC_RomTest_STOP_SEC_CODE
#include "VEC_RomTest_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#define VEC_RomTest_START_SEC_CODE
#include "VEC_RomTest_MemMap.h"

/* ***********************************************************************************************************************
*  VEC_RomTest_TriggerState
***********************************************************************************************************************/
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_TriggerState( Csm_ReturnType csmResult )
{
  tVecRomTestResult result = kVecRomTestResultOk;

  /* Security routine successful? */
  if (CSM_E_OK == csmResult)
  {
    /* Advance state */
    switch (vecRomTestState)
    {
      case kVecRomTestStateInitPending:
      {
        /* Start evalutation of block segments */
        vecRomTestState = kVecRomTestStateSegmentCount;

        break;
      }
      case kVecRomTestStateComputePending:
      {
        /* Continue with next chunk of data */
        vecRomTestState = kVecRomTestStateReadData;

        break;
      }
      case kVecRomTestStateComparePending:
      {
        if (CSM_E_VER_OK != vecRomTestVerifyResult)
        {
          /* Comparison failed, update block state and notify application */
          VEC_RomTest_ChangeBlockState(vecRomTestBlockIdx, kVecRomTestBlockStateSignatureFailure);
        }
        else
        {
          /* Continue with next block */
          result = kVecRomTestResultFailed;
        }

        break;
      }
      default:
      {
        break;
      }
    }
  }
  else
  {
    /* Security routine failed, continue with next block */
    result = kVecRomTestResultFailed;

    /* Notify application */
    VEC_RomTest_FatalError(kVecRomTestFatalErrorSecurityRoutineFailed);
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_HandleCsmResult
***********************************************************************************************************************/
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_HandleCsmResult( Csm_ReturnType csmResult, tVecRomTestState prevState )
{
  tVecRomTestResult result;

#if defined( VEC_ROMTEST_CSM_ASYNC_JOB_PROCESSING )
  result = kVecRomTestResultOk;

  /* Evaluate result of CSM operation */
  switch (csmResult)
  {
    case CSM_E_OK:
    {
      /* Operation successful, wait for confirmation */
      break;
    }
    case CSM_E_BUSY:
    {
      /* CSM busy, retry operation by restoring previous state */
      vecRomTestState = prevState;

      break;
    }
    case CSM_E_NOT_OK:
    default:
    {
      /* Operation failed, advance state machine */
      result = VEC_RomTest_TriggerState(csmResult);

      break;
    }
  }
#else
  (void)prevState;

  /* Advance state machine depending on CSM result */
  result = VEC_RomTest_TriggerState(csmResult);
#endif /* VEC_ROMTEST_CSM_ASYNC_JOB_PROCESSING */

  return result;
}


/* ***********************************************************************************************************************
*  VEC_RomTest_ChangeBlockState
***********************************************************************************************************************/
static FUNC(void, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_ChangeBlockState( uint8 blockIdx, tVecRomTestBlockState blockState )
{
  /* Check for changed state */
  if (blockState != vecRomTestBlockState[blockIdx])
  {
    /* Remember state */
    vecRomTestNotifyBlockState = blockState;
    /* Trigger notification */
    vecRomTestState = kVecRomTestStateNotify;
  }
}

/* ***********************************************************************************************************************
*  VEC_RomTest_CheckNextBlock
***********************************************************************************************************************/
static FUNC(void, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_CheckNextBlock( void )
{
  /* Increase block index */
  vecRomTestBlockIdx++;
  /* Go back to "Idle" state */
  vecRomTestState = kVecRomTestStateIdle;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_StateSetupType
***********************************************************************************************************************/
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateSetupType( void )
{
  tVecRomTestResult result;
  tVecRomTestSignatureType sigType;

  result = kVecRomTestResultOk;

  /* Read signature type */
  switch (VEC_RomTest_ReadSignatureType(vecRomTestBlockIdx, &sigType))
  {
    case kVecRomTestResultOk:
    {
      /* Remember signature type for later use */
      vecRomTestSigTypes[vecRomTestBlockIdx] = sigType;

      if (kVecRomTestSignatureTypeInvalid == sigType)
      {
        Rte_Enter_VEC_ExclusiveArea();
          /* No valid signature present */
          vecRomTestBlockState[vecRomTestBlockIdx] = kVecRomTestBlockStateNotPresent;
        Rte_Exit_VEC_ExclusiveArea();

        vecRomTestState = kVecRomTestStateSetupBlock;
      }
      else
      {
        vecRomTestState = kVecRomTestStateSetupValidity;
      }

      break;
    }
    case kVecRomTestResultBusy:
    {
      /* Busy, retry on next cycle */
      break;
    }
    case kVecRomTestResultFailed:
    default:
    {
      /* Failed: continue with next block */
      vecRomTestState = kVecRomTestStateSetupBlock;

      /* Notify application */
      VEC_RomTest_FatalError(kVecRomTestFatalErrorNvAccessFailed);

      break;
    }
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_StateSetupValidity
***********************************************************************************************************************/
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateSetupValidity( void )
{
  tVecRomTestResult result;
  tVecRomTestResult validity;

  result = kVecRomTestResultOk;

  /* Evaluate block validity */
  switch (VEC_RomTest_GetBlockValidity(vecRomTestBlockIdx, &validity))
  {
    case kVecRomTestResultOk:
    {
      Rte_Enter_VEC_ExclusiveArea();
        if (kVecRomTestResultOk == validity)
        {
          /* Block valid */
          vecRomTestBlockState[vecRomTestBlockIdx] = kVecRomTestBlockStateValid;
        }
        else
        {
          /* Block invalid => signature failure */
          vecRomTestBlockState[vecRomTestBlockIdx] = kVecRomTestBlockStateSignatureFailure;
        }
      Rte_Exit_VEC_ExclusiveArea();

      /* Continue with next block */
      vecRomTestState = kVecRomTestStateSetupBlock;

      break;
    }
    case kVecRomTestResultBusy:
    {
      /* Busy, retry on next cycle */
      break;
    }
    case kVecRomTestResultFailed:
    default:
    {
      /* Failed: continue with next block */
      vecRomTestState = kVecRomTestStateSetupBlock;

      /* Notify application */
      VEC_RomTest_FatalError(kVecRomTestFatalErrorNvAccessFailed);

      break;
    }
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_StateSetupBlock
***********************************************************************************************************************/
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateSetupBlock( void )
{
  tVecRomTestResult result;

  result = kVecRomTestResultOk;

  /* Next block */
  vecRomTestBlockIdx++;

  /* Last block reached, start actual processing */
  if (vecRomTestBlockIdx >= vecRomTestBlockCount)
  {
    vecRomTestBlockIdx = 0u;
    vecRomTestState = kVecRomTestStateIdle;
  }
  else
  {
    /* Setup next block */
    vecRomTestState = kVecRomTestStateSetupType;
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_StateIdle
***********************************************************************************************************************/
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateIdle( void )
{
  tVecRomTestResult result;

  result = kVecRomTestResultOk;

  /* Last block reached, start over at first block */
  if (vecRomTestBlockIdx >= vecRomTestBlockCount)
  {
    vecRomTestBlockIdx = 0u;
  }

  /* Check current block state */
  if (kVecRomTestBlockStateValid == vecRomTestBlockState[vecRomTestBlockIdx])
  {
    /* Get signature type */
    vecRomTestSigType = vecRomTestSigTypes[vecRomTestBlockIdx];

    switch (vecRomTestSigType)
    {
      case kVecRomTestSignatureTypeChecksum:
      case kVecRomTestSignatureTypeSignature:
      {
        /* Start verification of current block */
        vecRomTestState = kVecRomTestStateInit;

        break;
      }
      case kVecRomTestSignatureTypeInvalid:
      default:
      {
        /* Invalid signature type, continue with next block */
        result = kVecRomTestResultFailed;

        /* Notify application */
        VEC_RomTest_FatalError(kVecRomTestFatalErrorInvalidSignatureType);

        break;
      }
    }
  }
  else
  {
    /* Block already marked invalid, continue with next block */
    result = kVecRomTestResultFailed;
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_StateIdle
***********************************************************************************************************************/
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateInit( void )
{
  tVecRomTestResult result;
  Csm_ReturnType csmResult;

  result = kVecRomTestResultOk;

  switch (vecRomTestSigType)
  {
    case kVecRomTestSignatureTypeChecksum:
    {
      /* Set CRC parameter */
      vecRomTestCrcParam.crcDataPtr = vecRomTestDataBuffer;
      vecRomTestCrcParam.crcIsFirstCall = TRUE;
      vecRomTestCrcParam.crcStartValue32 = 0u;

      /* Continue to read segment count */
      vecRomTestState = kVecRomTestStateSegmentCount;

      break;
    }
    case kVecRomTestSignatureTypeSignature:
    {
      /* Task may be interrupted: setup state before call to CSM */
      vecRomTestState = kVecRomTestStateInitPending;

      /* Initialize signature verification */
      csmResult = Rte_Call_CsmSignatureVerify_SignatureVerifyStart((P2CONST(AsymPublicKeyType, AUTOMATIC, RTE_VEC_ROMTEST_APPL_DATA))&vecRomTestKey);

      /* State handling */
      result = VEC_RomTest_HandleCsmResult(csmResult, kVecRomTestStateInit);

      break;
    }
    default:
    {
      /* Should never be reached */
      break;
    }
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_StateSegmentCount
***********************************************************************************************************************/
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateSegmentCount( void )
{
  tVecRomTestResult result;

  result = kVecRomTestResultOk;

  /* Read segment count */
  switch (VEC_RomTest_ReadSegmentNumber(vecRomTestBlockIdx, &vecRomTestSegmentCount))
  {
    case kVecRomTestResultOk:
    {
      /* Start with first segment */
      vecRomTestSegmentIdx = 0u;
      vecRomTestState = kVecRomTestStateSegmentAddress;

      break;
    }
    case kVecRomTestResultBusy:
    {
      /* Busy, retry on next cycle */
      break;
    }
    case kVecRomTestResultFailed:
    default:
    {
      /* Failed operation, continue with next block */
      result = kVecRomTestResultFailed;

      /* Notify application */
      VEC_RomTest_FatalError(kVecRomTestFatalErrorNvAccessFailed);

      break;
    }
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_StateSegmentAddress
***********************************************************************************************************************/
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateSegmentAddress( void )
{
  tVecRomTestResult result;

  result = kVecRomTestResultOk;

  if (vecRomTestSegmentIdx < vecRomTestSegmentCount)
  {
    /* Read segment start address */
    switch (VEC_RomTest_ReadSegmentStart(vecRomTestBlockIdx, vecRomTestSegmentIdx, &vecRomTestAddress))
    {
      case kVecRomTestResultOk:
      {
        /* Continue to read segment length */
        vecRomTestState = kVecRomTestStateSegmentLength;

        break;
      }
      case kVecRomTestResultBusy:
      {
        /* Busy, retry on next cycle */
        break;
      }
      case kVecRomTestResultFailed:
      default:
      {
        /* Failed operation, continue with next block */
        result = kVecRomTestResultFailed;

        /* Notify application */
        VEC_RomTest_FatalError(kVecRomTestFatalErrorNvAccessFailed);

        break;
      }
    }
  }
  else
  {
    /* Last segment processed */
    /* Read signature value for verification */
    vecRomTestState = kVecRomTestStateSignature;
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_StateSegmentLength
***********************************************************************************************************************/
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateSegmentLength( void )
{
  tVecRomTestResult result;

  result = kVecRomTestResultOk;

  /* Read length of current segment */
  switch (VEC_RomTest_ReadSegmentLength(vecRomTestBlockIdx, vecRomTestSegmentIdx, &vecRomTestLength))
  {
    case kVecRomTestResultOk:
    {
      switch (vecRomTestSigType)
      {
        case kVecRomTestSignatureTypeChecksum:
        {
          /* Nothing to do for CRC checksum */
          /* Read first chunk of data */
          vecRomTestState = kVecRomTestStateReadData;

          break;
        }
        case kVecRomTestSignatureTypeSignature:
        {
          /* Signature hash includes address and length information */
          VEC_RomTest_SetInteger(sizeof(vecRomTestAddress), vecRomTestAddress, vecRomTestDataBuffer);
          VEC_RomTest_SetInteger(sizeof(vecRomTestLength), vecRomTestLength, &vecRomTestDataBuffer[sizeof(vecRomTestAddress)]);

          /* Proceed to update hash with address and length information */
          vecRomTestState = kVecRomTestStateComputeMeta;

          break;
        }
        default:
        {
          /* Should never be reached */
          break;
        }
      }

      break;
    }
    case kVecRomTestResultBusy:
    {
      /* Busy, retry on next cycle */
      break;
    }
    case kVecRomTestResultFailed:
    default:
    {
      /* Failed operation, continue with next block */
      result = kVecRomTestResultFailed;

      /* Notify application */
      VEC_RomTest_FatalError(kVecRomTestFatalErrorNvAccessFailed);

      break;
    }
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_StateComputeMeta
***********************************************************************************************************************/
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateComputeMeta( void )
{
  tVecRomTestResult result;
  Csm_ReturnType csmResult;

  /* Task may be interrupted: setup state before call to CSM */
  vecRomTestState = kVecRomTestStateComputePending;

  /* Update hash */
  csmResult = Rte_Call_CsmSignatureVerify_SignatureVerifyUpdate(vecRomTestDataBuffer, sizeof(vecRomTestAddress) + sizeof(vecRomTestLength));

  /* State handling */
  result = VEC_RomTest_HandleCsmResult(csmResult, kVecRomTestStateComputeMeta);

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_StateReadData
***********************************************************************************************************************/
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateReadData( void )
{
  tVecRomTestResult result;
  uint32 readLength;

  result = kVecRomTestResultOk;

  /* Check for segment end */
  if (0u == vecRomTestLength)
  {
    /* Proceed with next segment */
    vecRomTestSegmentIdx++;

    /* Return to "Segment" state */
    vecRomTestState = kVecRomTestStateSegmentAddress;
  }
  else
  {
    /* Number of bytes to handle in this loop */
    readLength = vecRomTestLength;
    if (readLength > VEC_ROMTEST_VERIFY_BYTES)
    {
      readLength = VEC_ROMTEST_VERIFY_BYTES;
    }

    vecRomTestReadLength = readLength;

    /* Store memory contents in local RAM buffer */
    switch (VEC_RomTest_ReadMemory(vecRomTestAddress, &vecRomTestReadLength, vecRomTestDataBuffer))
    {
      case kVecRomTestReadResultOk:
      {
        /* Update read position */
        vecRomTestAddress += readLength;
        vecRomTestLength  -= readLength;

        /* Data successfully read, continue to process it */
        vecRomTestState = kVecRomTestStateCompute;

        break;
      }
      case kVecRomTestReadResultBusy:
      {
        /* Memory device busy, retry later */
        break;
      }
      case kVecRomTestReadResultFailed:
      {
        /* Read error occurred, change block state */
        VEC_RomTest_ChangeBlockState(vecRomTestBlockIdx, kVecRomTestBlockStateReadFailure);

        break;
      }
      /* Memory region currently unavailable */
      case kVecRomTestReadResultUnavailable:
      default:
      {
        /* Continue with next block */
        result = kVecRomTestResultFailed;

        break;
      }
    }
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_StateCompute
***********************************************************************************************************************/
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateCompute( void )
{
  tVecRomTestResult result;
  Csm_ReturnType csmResult;

  result = kVecRomTestResultOk;

  switch (vecRomTestSigType)
  {
    case kVecRomTestSignatureTypeChecksum:
    {
      /* Update input length */
      vecRomTestCrcParam.crcLength = vecRomTestReadLength;

      /* Update CRC */
      vecRomTestCrcParam.crcStartValue32 = Crc_CalculateCRC32( vecRomTestCrcParam.crcDataPtr,
                                                               vecRomTestCrcParam.crcLength,
                                                               vecRomTestCrcParam.crcStartValue32,
                                                               vecRomTestCrcParam.crcIsFirstCall );

      /* Reset initialize flag */
      vecRomTestCrcParam.crcIsFirstCall = FALSE;

      /* Continue with next chunk of data */
      vecRomTestState = kVecRomTestStateReadData;

      break;
    }
    case kVecRomTestSignatureTypeSignature:
    {
      /* Task may be interrupted: setup state before call to CSM */
      vecRomTestState = kVecRomTestStateComputePending;

      /* Update hash */
      csmResult = Rte_Call_CsmSignatureVerify_SignatureVerifyUpdate(vecRomTestDataBuffer, vecRomTestReadLength);

      /* State handling */
      result = VEC_RomTest_HandleCsmResult(csmResult, kVecRomTestStateCompute);

      break;
    }
    default:
    {
      /* Should never be reached */
      break;
    }
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_StateSignature
***********************************************************************************************************************/
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateSignature( void )
{
  tVecRomTestResult result;

  result = kVecRomTestResultOk;

  /* Read stored reference signature */
  switch (VEC_RomTest_ReadSignatureValue(vecRomTestBlockIdx, vecRomTestSigBuffer, sizeof(vecRomTestSigBuffer)))
  {
    case kVecRomTestResultOk:
    {
      /* Verify signature */
      vecRomTestState = kVecRomTestStateCompare;

      break;
    }
    case kVecRomTestResultBusy:
    {
      /* Busy, retry on next cycle */
      break;
    }
    case kVecRomTestResultFailed:
    default:
    {
      /* Failed operation, continue with next block */
      result = kVecRomTestResultFailed;

      /* Notify application */
      VEC_RomTest_FatalError(kVecRomTestFatalErrorNvAccessFailed);

      break;
    }
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_StateCompare
***********************************************************************************************************************/
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateCompare( void )
{
  tVecRomTestResult result;
  Csm_ReturnType csmResult;
  uint32 crcValue;

  result = kVecRomTestResultOk;

  switch (vecRomTestSigType)
  {
    case kVecRomTestSignatureTypeSignature:
    {
      /* Task may be interrupted: setup state before call to CSM */
      vecRomTestState = kVecRomTestStateComparePending;
      vecRomTestVerifyResult = CSM_E_VER_NOT_OK;

      /* Verify signature */
      csmResult = Rte_Call_CsmSignatureVerify_SignatureVerifyFinish(vecRomTestSigBuffer, sizeof(vecRomTestSigBuffer), &vecRomTestVerifyResult);

      /* State handling */
      result = VEC_RomTest_HandleCsmResult(csmResult, kVecRomTestStateCompare);

      break;
    }
    case kVecRomTestSignatureTypeChecksum:
    {
      /* Compare CRC */
      crcValue = VEC_RomTest_GetInteger(sizeof(crcValue), vecRomTestSigBuffer);
      if (crcValue != vecRomTestCrcParam.crcStartValue32)
      {
        /* Comparison failed, update block state */
        VEC_RomTest_ChangeBlockState(vecRomTestBlockIdx, kVecRomTestBlockStateSignatureFailure);
      }
      else
      {
        /* Continue with next block */
        result = kVecRomTestResultFailed;
      }

      break;
    }
    default:
    {
      /* Continue with next block */
      result = kVecRomTestResultFailed;

      break;
    }
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_StateCompare
***********************************************************************************************************************/
static FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_StateNotify( void )
{
  tVecRomTestResult result;

  result = kVecRomTestResultOk;

  /* Notify application */
  switch (VEC_RomTest_FailureNotification(vecRomTestBlockIdx, vecRomTestNotifyBlockState))
  {
    case kVecRomTestResultBusy:
    {
      /* Busy, retry on next cycle */
      break;
    }
    case kVecRomTestResultOk:
    case kVecRomTestResultFailed:
    default:
    {
      Rte_Enter_VEC_ExclusiveArea();
        /* Set changed state */
        vecRomTestBlockState[vecRomTestBlockIdx] = vecRomTestNotifyBlockState;
      Rte_Exit_VEC_ExclusiveArea();

      /* Continue with next block */
      result = kVecRomTestResultFailed;

      break;
    }
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_GetInteger
***********************************************************************************************************************/
FUNC(uint32, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_GetInteger( uint8 count, P2VAR(uint8, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) buffer )
{
  uint32 result = 0u;
  uint8 index = 0u;

  while (count > 0u)
  {
    result <<= 8u;
    result |= (uint32)buffer[index];
    index++;
    count--;
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_SetInteger
***********************************************************************************************************************/
FUNC(void, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_SetInteger( uint8 count, uint32 input, P2VAR(uint8, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) buffer )
{
  while (count > 0u)
  {
    count--;
    buffer[count] = (uint8)(input & 0xFFu);
    input >>= 8u;
  }
}

#define VEC_RomTest_STOP_SEC_CODE
#include "VEC_RomTest_MemMap.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0777:  MISRA rule: 5.1
     Reason:     The defined RTE naming convention may result in identifiers with more than 31 characters. The compliance to this rule is under user's control.
                 This is covered in the MISRA C compliance section of the Rte specification.
     Risk:       Ambiguous identifiers can lead to compiler errors / warnings.
     Prevention: Verified during compile time. If the compiler reports an error / warning. The user has to rename the objects to be unique within the significant characters.

   MD_Rte_0779:  MISRA rule: 5.1
     Reason:     The defined RTE naming convention may result in identifiers with more than 31 characters. The compliance to this rule is under user's control.
                 This is covered in the MISRA C compliance section of the Rte specification.
     Risk:       Ambiguous identifiers can lead to compiler errors / warnings.
     Prevention: Verified during compile time. If the compiler reports an error / warning. The user has to rename the objects to be unique within the significant characters.

*/
