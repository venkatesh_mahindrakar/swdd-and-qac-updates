/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2016 by Vector Informatik GmbH.                                                  All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *         File:  VEC_RomTest_callouts.c
 *    Component:  Diag_AsrSwcRomTest_Volvo_AB
 *       Module:  -
 *    Generator:  -
 *
 *  Description:  User callouts of module Diag_AsrSwcRomTest_Volvo_AB
 *  
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  REVISION HISTORY
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the module's header file.
 *
 *  FILE VERSION
 *  -------------------------------------------------------------------------------------------------------------------
 *  Refer to the VERSION CHECK below.
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "Rte_VEC_ROMTest.h"
#include "VEC_RomTest.h"
#include "VEC_RomTest_Cfg.h"

#include "WrapNv_inc.h"

#include "vstdlib.h"

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/
/* vendor specific version information is BCD coded */
#if (  (VEC_ROMTEST_SW_MAJOR_VERSION != (0x04)) \
    || (VEC_ROMTEST_SW_MINOR_VERSION != (0x00)) \
    || (VEC_ROMTEST_SW_PATCH_VERSION != (0x01)) )
# error "Vendor specific version numbers of VEC_ROMTest_callouts.c and VEC_RomTest.h are inconsistent"
#endif

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 **********************************************************************************************************************/

#define VEC_ROMTEST_MAX_NV_SIZE     4u

#if ( kEepSizeValidityFlags > VEC_ROMTEST_MAX_NV_SIZE )
# error "Insufficient size of non-volatile read buffer!"
#endif
#if ( kEepSizeSigStart > VEC_ROMTEST_MAX_NV_SIZE )
# error "Insufficient size of non-volatile read buffer!"
#endif
#if ( kEepSizeSigLength > VEC_ROMTEST_MAX_NV_SIZE )
# error "Insufficient size of non-volatile read buffer!"
#endif
#if ( kEepSizeSigSegmentStart > VEC_ROMTEST_MAX_NV_SIZE )
# error "Insufficient size of non-volatile read buffer!"
#endif
#if ( kEepSizeSigSegmentLength > VEC_ROMTEST_MAX_NV_SIZE )
# error "Insufficient size of non-volatile read buffer!"
#endif


/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 **********************************************************************************************************************/

/** State of notification operation */
typedef enum
{
  kVecRomTestNotificationStateReadPending = 0,    /**< Read pending */
  kVecRomTestNotificationStateWritePending,       /**< Write pending */
} tVecRomTestNotificationState;

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 **********************************************************************************************************************/

#define VEC_RomTest_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "VEC_RomTest_MemMap.h"

/** Non-volatile memory operation status */
VAR(tWrapNvOpStatus, VEC_RomTest_VAR_NOINIT) gNvOpStatus;
/** Read buffer for Non-volatile memory operations */
VAR(uint8, VEC_RomTest_VAR_NOINIT) gNvBuffer[VEC_ROMTEST_MAX_NV_SIZE];

/** State of notification operation */
VAR(tVecRomTestNotificationState, VEC_RomTest_VAR_NOINIT) gNotificationState;

#define VEC_RomTest_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "VEC_RomTest_MemMap.h"

/**********************************************************************************************************************
 *  GLOBAL DATA
 **********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 **********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 **********************************************************************************************************************/

/* ***********************************************************************************************************************
*  VEC_RomTest_HandleNvState
***********************************************************************************************************************/
/*! \brief  Advance non-volatile memory operation status, depending on return code
*  \return  Return code remapped to ROM test type
*********************************************************************************************************************** */
FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_HandleNvState( WrapNv_ReturnType nvResult )
{
  tVecRomTestResult result;

  switch (nvResult)
  {
    /* NV-operation successfully finished */
    case WRAPNV_E_OK:
    {
      /* Setup initial state for next NV-operation */
      gNvOpStatus = WRAPNV_OPSTATUS_INIT;
      result      = kVecRomTestResultOk;

      break;
    }
    /* NV-operation pending */
    case WRAPNV_E_PENDING:
    {
      /* Retry current NV-operation */
      gNvOpStatus = WRAPNV_OPSTATUS_PENDING;
      result      = kVecRomTestResultBusy;

      break;
    }
    /* NV-operation failed */
    case WRAPNV_E_NOT_OK:
    default:
    {
      /* Setup initial state for next NV-operation */
      gNvOpStatus = WRAPNV_OPSTATUS_INIT;
      result      = kVecRomTestResultFailed;

      break;
    }
  }

  return result;
}

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 **********************************************************************************************************************/
#define VEC_RomTest_START_SEC_CODE
#include "VEC_RomTest_MemMap.h"

/* ***********************************************************************************************************************
*  VEC_RomTest_InitCallouts
***********************************************************************************************************************/
/*! \brief  Initialize state variables
*********************************************************************************************************************** */
FUNC(void, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_InitCallouts( void )
{
  gNvOpStatus         = WRAPNV_OPSTATUS_INIT;
  gNotificationState  = kVecRomTestNotificationStateReadPending;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_GetModulusSize
***********************************************************************************************************************/
/*! \brief  Get size of public RSA key modulus
*  \return  Size of RSA key modulus
*********************************************************************************************************************** */
FUNC(uint16, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_GetModulusSize( void )
{
   return (uint16)(fblCommonData.secSigRsaKey.modulus.size & 0xFFFFU);
}

/* ***********************************************************************************************************************
*  VEC_RomTest_GetExponentSize
***********************************************************************************************************************/
/*! \brief  Get size of public RSA key exponent
*  \return  Size of RSA key modulus
*********************************************************************************************************************** */
FUNC(uint16, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_GetExponentSize( void )
{
   return (uint16)(fblCommonData.secSigRsaKey.exponent.size & 0xFFFFU);
}

/* ***********************************************************************************************************************
*  VEC_RomTest_GetModulus
***********************************************************************************************************************/
/*! \brief  Get public RSA key modulus
*  \return  Pointer to RSA key modulus
*********************************************************************************************************************** */
FUNC(P2CONST(uint8, AUTOMATIC, RTE_VEC_ROMTEST_APPL_CONST), RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_GetModulus( void )
{
   return fblCommonData.secSigRsaKey.modulus.pData;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_GetExponent
***********************************************************************************************************************/
/*! \brief  Get public RSA key exponent
*  \return  Pointer to RSA key exponent
*********************************************************************************************************************** */
FUNC(P2CONST(uint8, AUTOMATIC, RTE_VEC_ROMTEST_APPL_CONST), RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_GetExponent( void )
{
   return fblCommonData.secSigRsaKey.exponent.pData;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_GetBlockNumber
***********************************************************************************************************************/
/*! \brief  Get total number configured logical blocks
*  \return  Total number configured logical blocks
*********************************************************************************************************************** */
FUNC(uint8, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_GetBlockCount( void )
{
  return FblHeaderTable->kLbtAddress->noOfBlocks;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_GetBlockValidity
***********************************************************************************************************************/
/*! \brief  Call out to get validity information of logical block
*  \param[in]  blockIdx  Index of block
*  \param[out]  pValidity  Validity information
*  \return     Result of operation
*  \retval     kVecRomTestResultOk  - Operation successful
*  \retval     kVecRomTestResultFailed  - Operation failed
*  \retval     kVecRomTestResultBusy  - Operation pending
*********************************************************************************************************************** */
FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_GetBlockValidity( uint8 blockIdx,
  P2VAR(tVecRomTestResult, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) pValidity )
{
  tVecRomTestResult result;
  tVecRomTestResult validity;
  uint8 validityIdx;
  uint8 bitPos;

  /* Initialize validity */
  validity = kVecRomTestResultFailed;

  /* Prevent task switch while non-reentrant NV function is executed (e.g. EepM) */
  Rte_Enter_VEC_ExclusiveArea();

  /* Read validity flags */
  result = VEC_RomTest_HandleNvState(ApplFblNvReadAsyncValidityFlags(0u, gNvBuffer, gNvOpStatus));

  Rte_Exit_VEC_ExclusiveArea();

  if (kVecRomTestResultOk == result)
  {
    /* Calculate corresponding bit position in validity information */
    validityIdx = (uint8)(blockIdx >> 3u);
    bitPos      = (uint8)(blockIdx & 0x07u);

    if ((gNvBuffer[validityIdx] & (uint8)(1u << bitPos)) == (uint8)0x00u)
    {
      validity = kVecRomTestResultOk;
    }
  }

  *pValidity = validity;

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_ReadSignatureType
***********************************************************************************************************************/
/*! \brief  Call out to read signature type of a specific block
*  \param[in]  blockIdx  Index of block
*  \param[out]  pType  Signature type
*  \return     Result of operation
*  \retval     kVecRomTestResultOk  - Operation successful
*  \retval     kVecRomTestResultFailed  - Operation failed
*  \retval     kVecRomTestResultBusy  - Operation pending
*********************************************************************************************************************** */
FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_ReadSignatureType( uint8 blockIdx,
  P2VAR(tVecRomTestSignatureType, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) pType )
{
  tVecRomTestResult result;
  tVecRomTestSignatureType type;
  uint8 value;

  /* Initialize signature type  */
  type = kVecRomTestSignatureTypeInvalid;

  /* Prevent task switch while non-reentrant NV function is executed (e.g. EepM) */
  Rte_Enter_VEC_ExclusiveArea();

  /* Read information from block meta data */
  result = VEC_RomTest_HandleNvState(ApplFblNvReadAsyncSigType(blockIdx, &value, gNvOpStatus));

  Rte_Exit_VEC_ExclusiveArea();

  if (kVecRomTestResultOk == result)
  {
     type = (tVecRomTestSignatureType)value;
  }

  *pType = type;

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_ReadSignatureValue
***********************************************************************************************************************/
/*! \brief  Call out to read signature value of a specific block
*  \param[in]  blockIdx  Index of block
*  \param[out]  pValue  Pointer to output buffer
*  \param[in]  length  Maximum buffer length
*  \return     Result of operation
*  \retval     kVecRomTestResultOk  - Operation successful
*  \retval     kVecRomTestResultFailed  - Operation failed or provided buffer to small
*  \retval     kVecRomTestResultBusy  - Operation pending
*********************************************************************************************************************** */
FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_ReadSignatureValue( uint8 blockIdx,
  P2VAR(uint8, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) pValue, uint32 length )
{
  tVecRomTestResult result;

  result = kVecRomTestResultFailed;

  if (length >= kEepSizeSigValue)
  {
    /* Prevent task switch while non-reentrant NV function is executed (e.g. EepM) */
    Rte_Enter_VEC_ExclusiveArea();

    /* Read information from block meta data */
    result = VEC_RomTest_HandleNvState(ApplFblNvReadAsyncSigValue(blockIdx, pValue, gNvOpStatus));

    Rte_Exit_VEC_ExclusiveArea();
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_ReadSegmentNumber
***********************************************************************************************************************/
/*! \brief  Call out to read number of segments which were included in signature calculation
*  \param[in]  blockIdx  Index of block
*  \param[out]  pNumber  Number of segments
*  \return     Result of operation
*  \retval     kVecRomTestResultOk  - Operation successful
*  \retval     kVecRomTestResultFailed  - Operation failed
*  \retval     kVecRomTestResultBusy  - Operation pending
*********************************************************************************************************************** */
FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_ReadSegmentNumber( uint8 blockIdx,
  P2VAR(uint8, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) pNumber )
{
  tVecRomTestResult result;
  uint8 number;

  /* Prevent task switch while non-reentrant NV function is executed (e.g. EepM) */
  Rte_Enter_VEC_ExclusiveArea();

  /* Read information from block meta data */
  result = VEC_RomTest_HandleNvState(ApplFblNvReadAsyncSigSegmentNumber(blockIdx, &number, gNvOpStatus));

  Rte_Exit_VEC_ExclusiveArea();

  *pNumber = number;

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_ReadSegmentStart
***********************************************************************************************************************/
/*! \brief  Call out to read start address of single segment which was included in signature calculation
*  \param[in]  blockIdx  Index of block
*  \param[out]  pAddress  Start address of segment
*  \return     Result of operation
*  \retval     kVecRomTestResultOk  - Operation successful
*  \retval     kVecRomTestResultFailed  - Operation failed
*  \retval     kVecRomTestResultBusy  - Operation pending
*********************************************************************************************************************** */
FUNC(tVecRomTestAddress, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_ReadSegmentStart( uint8 blockIdx, uint8 segment,
  P2VAR(tVecRomTestAddress, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) pAddress )
{
  tVecRomTestResult result;
  tVecRomTestAddress address;

  /* Initialize address */
  address = 0u;

  /* Prevent task switch while non-reentrant NV function is executed (e.g. EepM) */
  Rte_Enter_VEC_ExclusiveArea();

  /* Read information from block meta data */
  result = VEC_RomTest_HandleNvState(ApplFblNvReadAsyncSigSegmentStart((blockIdx * WRAPNV_COUNT_SEGMENTS) + segment,
    gNvBuffer, gNvOpStatus));

  Rte_Exit_VEC_ExclusiveArea();

  if (kVecRomTestResultOk == result)
  {
    /* Convert byte array to integer value */
    address = VEC_RomTest_GetInteger(kEepSizeSigSegmentStart, gNvBuffer);
  }

  *pAddress = address;

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_ReadSegmentLength
***********************************************************************************************************************/
/*! \brief  Call out to read length of single segment which was included in signature calculation
*  \param[in]  blockIdx  Index of block
*  \param[out]  pLength  Length of segment
*  \return     Result of operation
*  \retval     kVecRomTestResultOk  - Operation successful
*  \retval     kVecRomTestResultFailed  - Operation failed
*  \retval     kVecRomTestResultBusy  - Operation pending
*********************************************************************************************************************** */
FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_ReadSegmentLength( uint8 blockIdx, uint8 segment,
  P2VAR(uint32, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) pLength )
{
  tVecRomTestResult result;
  uint32 length;

  /* Initialize length */
  length = 0u;

  /* Prevent task switch while non-reentrant NV function is executed (e.g. EepM) */
  Rte_Enter_VEC_ExclusiveArea();

  /* Read information from block meta data */
  result = VEC_RomTest_HandleNvState(ApplFblNvReadAsyncSigSegmentLength((blockIdx * WRAPNV_COUNT_SEGMENTS) + segment,
    gNvBuffer, gNvOpStatus));

  Rte_Exit_VEC_ExclusiveArea();

  if (kVecRomTestResultOk == result)
  {
    /* Convert byte array to integer value */
    length = VEC_RomTest_GetInteger(kEepSizeSigSegmentLength, gNvBuffer);
  }

  *pLength = length;

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_ReadMemory
***********************************************************************************************************************/
/*! \brief  Call out to read memory contents
*  \param[in]  address  Start address (including memory identifer) of read operation
*  \param[in,out]  length  Pointer to requested read length, output value contains actual number of read bytes
*  \param[in]  buffer  Pointer to output buffer
*  \return     Result of read operation
*  \retval     kVecRomTestReadResultOk  - success
*  \retval     kVecRomTestReadResultFailed  - failed
*  \retval     kVecRomTestReadResultBusy  - memory device busy, retry later
*  \retval     kVecRomTestReadResultUnavailable  - memory area currently unavailable
*********************************************************************************************************************** */
FUNC(tVecRomTestReadResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_ReadMemory( tVecRomTestAddress address,
  P2VAR(uint32, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) length, P2VAR(uint8, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) buffer )
{
  /* Use plain memcpy to read from address */
  VStdRamMemCpy((void *)buffer, (void *)address, (uint16)(*length));

  return kVecRomTestReadResultOk;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_FailureNotification
***********************************************************************************************************************/
/*! \brief  Call out to notify application about changed block state
*  \param[in]  blockIdx  Index of block
*  \param[in]  blockState  New state of block
*  \return     Result of operation
*  \retval     kVecRomTestResultOk  - Operation successful
*  \retval     kVecRomTestResultFailed  - Operation failed
*  \retval     kVecRomTestResultBusy  - Operation pending
*********************************************************************************************************************** */
FUNC(tVecRomTestResult, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_FailureNotification( uint8 blockIdx, tVecRomTestBlockState blockState )
{
  tVecRomTestResult result;
  uint8 validityIdx;
  uint8 bitPos;
  tVecRomTestResult loop;

  result = kVecRomTestResultFailed;

  switch (blockState)
  {
    case kVecRomTestBlockStateReadFailure:
    case kVecRomTestBlockStateSignatureFailure:
    {
      /* Mark block invalid */

      loop = kVecRomTestResultOk;

      while (kVecRomTestResultOk == loop)
      {
        loop = kVecRomTestResultFailed;

        switch (gNotificationState)
        {
          case kVecRomTestNotificationStateReadPending:
          {
            /* Prevent task switch while non-reentrant NV function is executed (e.g. EepM) */
            Rte_Enter_VEC_ExclusiveArea();

            /* Read validity information */
            result = VEC_RomTest_HandleNvState(ApplFblNvReadAsyncValidityFlags(0u, gNvBuffer, gNvOpStatus));

            Rte_Exit_VEC_ExclusiveArea();

            if (kVecRomTestResultOk == result)
            {
              /* Calculate corresponding bit position in validity information */
              validityIdx = (uint8)(blockIdx >> 3u);
              bitPos      = (uint8)(blockIdx & 0x07u);

              /* Modify validity flags */
              gNvBuffer[validityIdx] |= (uint8)(1u << bitPos);

              gNotificationState = kVecRomTestNotificationStateWritePending;
              loop = kVecRomTestResultOk;
            }

            break;
          }
          case kVecRomTestNotificationStateWritePending:
          {
            /* Prevent task switch while non-reentrant NV function is executed (e.g. EepM) */
            Rte_Enter_VEC_ExclusiveArea();

            /* Store modified flags */
            result = VEC_RomTest_HandleNvState(ApplFblNvWriteAsyncValidityFlags(0u, gNvBuffer, gNvOpStatus));

            Rte_Exit_VEC_ExclusiveArea();

            if (kVecRomTestResultBusy != result)
            {
              gNotificationState = kVecRomTestNotificationStateReadPending;
            }

            break;
          }
          default:
          {
            gNotificationState = kVecRomTestNotificationStateReadPending;

            break;
          }
        }
      }

      break;
    }
    case kVecRomTestBlockStateValid:
    case kVecRomTestBlockStateNotPresent:
    default:
    {
      /* Do nothing */
      gNotificationState = kVecRomTestNotificationStateReadPending;
      result = kVecRomTestResultOk;

      break;
    }
  }

  return result;
}

/* ***********************************************************************************************************************
*  VEC_RomTest_FatalError
***********************************************************************************************************************/
/*! \brief  Call out to notify application about fatal error in software component
*  \param[in]  errorCode  Error code
*********************************************************************************************************************** */
FUNC(void, RTE_VEC_ROMTEST_APPL_CODE) VEC_RomTest_FatalError( tVecRomTestFatalError errorCode )
{
  switch (errorCode)
  {
    case kVecRomTestFatalErrorNone:
    {
      /* Do nothing */
      break;
    }
    case kVecRomTestFatalErrorInsufficientBlockStateSize:
    case kVecRomTestFatalErrorInvalidSignatureType:
    case kVecRomTestFatalErrorSecurityRoutineFailed:
    default:
    {
      /* Notify application */
      break;
    }
  }
}

#define VEC_RomTest_STOP_SEC_CODE
#include "VEC_RomTest_MemMap.h"

/**********************************************************************************************************************
 *  END OF FILE: VEC_RomTest_callouts.c
 *********************************************************************************************************************/
