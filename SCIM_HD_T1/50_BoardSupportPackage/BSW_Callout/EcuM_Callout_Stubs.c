/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: EcuM
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: EcuM_Callout_Stubs.c
 *   Generation Time: 2020-11-04 14:01:26
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK User Version>                           DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/*********************************************************************************************************************
    INCLUDES
 *********************************************************************************************************************/
#define ECUM_CALLOUT_STUBS_SOURCE
#include "EcuM.h"

#define ECUM_PRIVATE_CFG_INCLUDE
#include "EcuM_PrivateCfg.h"
#undef ECUM_PRIVATE_CFG_INCLUDE /* PRQA S 0841 */ /* MD_EcuM_0841 */



/**********************************************************************************************************************
 *  All configured EcuM Wakeup Sources (as bitmasks) for usage in Callouts
 *********************************************************************************************************************/
/*
 * ECUM_WKSOURCE_NONE                       (EcuM_WakeupSourceType)(0x00000000uL) 
 * ECUM_WKSOURCE_ALL_SOURCES                (EcuM_WakeupSourceType)(~((EcuM_WakeupSourceType)0x00UL)) 
 * ECUM_WKSOURCE_POWER                      (EcuM_WakeupSourceType)(1uL) 
 * ECUM_WKSOURCE_RESET                      (EcuM_WakeupSourceType)(2uL) 
 * ECUM_WKSOURCE_INTERNAL_RESET             (EcuM_WakeupSourceType)(4uL) 
 * ECUM_WKSOURCE_INTERNAL_WDG               (EcuM_WakeupSourceType)(8uL) 
 * ECUM_WKSOURCE_EXTERNAL_WDG               (EcuM_WakeupSourceType)(16uL) 
 * ECUM_WKSOURCE_CN_Backbone2_78967e2c      (EcuM_WakeupSourceType)(32uL) 
 * ECUM_WKSOURCE_CN_CAN6_b040c073           (EcuM_WakeupSourceType)(64uL) 
 * ECUM_WKSOURCE_CN_CabSubnet_9ea693f1      (EcuM_WakeupSourceType)(128uL) 
 * ECUM_WKSOURCE_CN_SecuritySubnet_e7a0ee54 (EcuM_WakeupSourceType)(256uL) 
 * ECUM_WKSOURCE_CN_Backbone1J1939_0b1f4bae (EcuM_WakeupSourceType)(512uL) 
 * ECUM_WKSOURCE_CN_FMSNet_fce1aae5         (EcuM_WakeupSourceType)(1024uL) 
 * ECUM_WKSOURCE_CN_LIN00_2cd9a7df          (EcuM_WakeupSourceType)(2048uL) 
 * ECUM_WKSOURCE_CN_LIN01_5bde9749          (EcuM_WakeupSourceType)(4096uL) 
 * ECUM_WKSOURCE_CN_LIN02_c2d7c6f3          (EcuM_WakeupSourceType)(8192uL) 
 * ECUM_WKSOURCE_CN_LIN03_b5d0f665          (EcuM_WakeupSourceType)(16384uL) 
 * ECUM_WKSOURCE_CN_LIN04_2bb463c6          (EcuM_WakeupSourceType)(32768uL) 
 * ECUM_WKSOURCE_LFIC                       (EcuM_WakeupSourceType)(65536uL) 
 * ECUM_WKSOURCE_CN_LIN05_5cb35350          (EcuM_WakeupSourceType)(131072uL) 
 * ECUM_WKSOURCE_RFIC                       (EcuM_WakeupSourceType)(262144uL) 
 * ECUM_WKSOURCE_CN_LIN06_c5ba02ea          (EcuM_WakeupSourceType)(524288uL) 
 * ECUM_WKSOURCE_RTC                        (EcuM_WakeupSourceType)(1048576uL) 
 * ECUM_WKSOURCE_CN_LIN07_b2bd327c          (EcuM_WakeupSourceType)(2097152uL) 
 */

/**********************************************************************************************************************
 *  Additional configured User includes
 *********************************************************************************************************************/
#include "Dio.h" 
#include "BswM.h" 
#include "CanIf.h" 
#include "CanNm.h" 
#include "CanSM_EcuM.h" 
#include "CanTp.h" 
#include "Can.h" 
#include "ComM.h" 
#include "Com.h" 
#include "Dcm.h" 
#include "Det.h" 
#include "Issm.h" 
#include "J1939Nm.h" 
#include "J1939Rm.h" 
#include "J1939Tp.h" 
#include "LinIf.h" 
#include "LinSM.h" 
#include "Lin.h" 
#include "Nm.h" 
#include "PduR.h" 
#include "Rte_Main.h" 
#include "Dem.h" 
#include "CanTrcv_30_GenericCan.h" 
#include "LinTrcv_30_Generic.h" 
#include "Cry_30_LibCv.h" 
#include "Csm.h" 
#include "Rtm.h" 
#include "Xcp.h" 
#include "CanXcp.h" 
#include "RamTst.h" 
#include "Mcu.h" 
#include "Port.h" 
#include "Adc.h" 
#include "Gpt.h" 
#include "CDD_Mcl.h" 
#include "Pwm.h" 
#include "Spi.h" 
#include "Fls.h" 
#include "Icu.h" 
#include "Reg_eSys_MC_ME.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK User Includes>                          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "Os_Hal_Coreint.h" 
#include "Os_Hal_StartupInt.h"
#include"Os_Hal_Core_Lcfg.h"
#include"IoHwAb_ASIL_Core.h"
#include"Rte_IoHwAb_ASIL_Core.h"
#include"Bswinit.h"
#include"ResetProcess_If.h"


typedef volatile uint32 vuint32_t;


extern uint8 Wdg_PIN;
extern uint32 LPU_TimeLogCounter;
extern VapToLpuRAMType VapToLpuRAM;
extern uint32 LIN8_Wakeup;
extern volatile uint8 rfic_off_state;
extern void RFIC_OFF_REQ(void);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/




/**********************************************************************************************************************
 *  CALLOUT FUNCTIONS
 *********************************************************************************************************************/
#define ECUM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */




/**********************************************************************************************************************
 *  GENERIC CALLOUTS
 *********************************************************************************************************************/

FUNC(void, ECUM_CODE) EcuM_ErrorHook(Std_ReturnType reason) /* COV_ECUM_CALLOUT */ /* PRQA S 3206 */ /* MD_EcuM_3206 */
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_ErrorHook>                         DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
#if (ECUM_USE_DUMMY_STATEMENT == STD_ON)
   /* dummy assignment to prevent compiler warnings on most of the compilers. */
   (void)reason; /* PRQA S 3112 *//* MD_EcuM_3112 */
#endif
   /* Add implementation of EcuM_ErrorHook() */

   /* In case of an inconsistent configuration data, abort initialization here */
   if (reason == ECUM_E_HOOK_CONFIGURATION_DATA_INCONSISTENT)
   {
      while (1)
      {
      }
   }

# if(ECUM_NUMBER_OF_CORES > 1)
   /* In case of an invalid coreId, returned by the OS */
   if(reason == ECUM_E_HOOK_INVALID_COREID)
   {
      while(1)
      {
      }
   }
# endif

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_ErrorHook() */




/**********************************************************************************************************************
 *  CALLOUTS FROM THE SHUTDOWN PHASE
 *********************************************************************************************************************/



FUNC(void, ECUM_CODE) EcuM_OnGoOffOne(void)
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_OnGoOffOne>                        DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_OnGoOffOne() */
   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_OnGoOffOne() */

FUNC(void, ECUM_CODE) EcuM_ShutdownOS(Std_ReturnType ErrCode)
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_ShutdownOS>                        DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_ShutdownOS() */
#if ( ECUM_NUMBER_OF_CORES > 1 )
   ShutdownAllCores(ErrCode);
#else
   ShutdownOS(ErrCode);
#endif

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_ShutdownOS() */

FUNC(void, ECUM_CODE) EcuM_OnGoOffTwo(void)
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_OnGoOffTwo>                        DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_OnGoOffTwo() */
   Adc_DeInit();
   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_OnGoOffTwo() */

FUNC(void, ECUM_CODE) EcuM_AL_SwitchOff(void)
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_AL_SwitchOff>                      DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_AL_SwitchOff() */

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_AL_SwitchOff() */

FUNC(void, ECUM_CODE) EcuM_AL_Reset(EcuM_ResetType Reset) /* PRQA S 3206 */ /* MD_EcuM_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_AL_Reset>                          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#if (ECUM_USE_DUMMY_STATEMENT == STD_ON)
  /* dummy assignment to prevent compiler warnings on most of the compilers. */
  (void)Reset;  /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
#endif

    Mcu_PerformReset();
	//ResetProcess_TriggerReset(Diagnostic_Reset); //Diagnostic Reset -5 
   
   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_AL_Reset() */



/**********************************************************************************************************************
 *  CALLOUTS FROM THE STARTUP PHASE
 *********************************************************************************************************************/


/**********************************************************************************************************************
* EcuM_AL_DriverInitZero
**********************************************************************************************************************/
FUNC(void, ECUM_CODE) EcuM_AL_DriverInitZero(void) 
{

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_AL_DriverInitZero>                 DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* Add implementation of EcuM_AL_DriverInitZero  */
Can_InitMemory();
Lin_InitMemory();
Cry_30_LibCv_InitMemory();
ComM_InitMemory();
Nm_InitMemory();

if (WakeupTimerFunc!=WAKEUPTOFLASH){

  BswM_InitMemory();
  CanIf_InitMemory();
  CanNm_InitMemory();
  CanSM_InitMemory();
  CanTp_InitMemory();

  
  Com_InitMemory();
  Dcm_InitMemory();
  Det_InitMemory();
  Issm_InitMemory();
  J1939Nm_InitMemory();
  J1939Rm_InitMemory();
  J1939Tp_InitMemory();
  LinIf_InitMemory();
  LinSM_InitMemory();
 
  
  PduR_InitMemory();
  Rte_InitMemory();
  Dem_InitMemory();
  CanTrcv_30_GenericCan_InitMemory();
  LinTrcv_30_Generic_InitMemory();
 
  Csm_InitMemory();
  Det_Init( Det_Config_Ptr );
  Dem_PreInit( Dem_Config_Ptr );
}
  return;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
* EcuM_AL_DriverInitOne
**********************************************************************************************************************/
FUNC(void, ECUM_CODE) EcuM_AL_DriverInitOne(void) 
{
  Mcu_Init( &McuModuleConfiguration );
  Mcu_InitClock(0);
  while (MCU_PLL_LOCKED != Mcu_GetPllStatus());
  Mcu_DistributePllClock();
  Port_Init( &PortConfigSet );
  Adc_Init( &AdcConfigSet );
  Gpt_Init( &GptChannelConfigSet );
  Mcl_Init( &MclConfigSet_0 );
  Pwm_Init( &PwmChannelConfigSet );
  Spi_Init( NULL_PTR );
  Fls_Init( &FlsConfigSet );
  Icu_Init( &IcuConfigSet );

/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_AL_DriverInitOne>                  DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_AL_DriverInitOne  */
   Dio_FlipChannel(DioConf_DioChannel_ExWdg_WDI);

   Mcu_SetMode(McuConf_McuModeSettingConf_McuModeSettingConf_RUN0);
   Gpt_SetMode(GPT_MODE_NORMAL);
   Gpt_StartTimer(GptConf_GptChannelConfiguration_GptRtmCounter,0xFFFFFFFF);

   Icu_EnableEdgeDetection(IcuChannel_LFIC_IRQ);
   Icu_EnableEdgeDetection(IcuChannel_RFIC_IRQ);

   Icu_EnableNotification(IcuChannel_LFIC_IRQ);
   Icu_EnableNotification(IcuChannel_RFIC_IRQ);

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
}

#if (ECUM_SLEEPMODELIST == STD_ON)
/**********************************************************************************************************************
* EcuM_AL_DriverRestartList
**********************************************************************************************************************/
FUNC(void, ECUM_CODE) EcuM_AL_DriverRestartList(void) 
{

/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_AL_DriverRestartList>              DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_AL_DriverRestartList  */

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
}
#endif




FUNC(void, ECUM_CODE) EcuM_StartOS(AppModeType appMode)
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_StartOS>                           DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_StartOS() */
#if ( ECUM_NUMBER_OF_CORES > 1 )
   uint8 coreId;
   StatusType status;
   if(GetCoreID()==ECUM_CORE_ID_STARTUP)
   {
      for(coreId=0; coreId < ECUM_NUMBER_OF_CORES; coreId++)
      {
         if(coreId!=ECUM_CORE_ID_STARTUP)
         {
            StartCore(coreId, &status); /* SBSW_ECUM_ADRESSPARAMETER_TOOS */
         }
      }
   }
#endif
  #if 1
  uint32 curRunMode;
  uint16 coreRunMode;

 
 


  curRunMode = (uint32)((*(volatile uint32*)(OS_HAL_MC_ME_BASE_ADDRESS + OS_HAL_MC_ME_MODE_CTRL_REGISTER_OFFSET))       /* PRQA S 0303 */ /* MD_Os_Hal_Rule11.4_0303 */
                       & 0xF0000000uL);

  coreRunMode = (uint16)(1u << (curRunMode >> 28u));  

  *(volatile uint32*)(OS_HAL_MC_ME_BASE_ADDRESS + OS_HAL_MC_ME_CORE1_ADDR_REGISTER_OFFSET) =                          /* PRQA S 0303 */ /* MD_Os_Hal_Rule11.4_0303 */ /* SBSW_OS_HAL_PERIPHERAL_ACCESS */
                                                                                (OsCfg_Hal_Core_OsCore0.CoreStartAddress) + 1u;
    *(volatile uint16*)(OS_HAL_MC_ME_BASE_ADDRESS + OS_HAL_MC_ME_CORE1_CTRL_REGISTER_OFFSET) |= coreRunMode;     


  *(volatile uint32*)(OS_HAL_MC_ME_BASE_ADDRESS + OS_HAL_MC_ME_MODE_CTRL_REGISTER_OFFSET) = 							/* PRQA S 0303 */ /* MD_Os_Hal_Rule11.4_0303 */ /* SBSW_OS_HAL_PERIPHERAL_ACCESS */
																		   OS_HAL_MCTL_KEY			| curRunMode;
	*(volatile uint32*)(OS_HAL_MC_ME_BASE_ADDRESS + OS_HAL_MC_ME_MODE_CTRL_REGISTER_OFFSET) =							  /* PRQA S 0303 */ /* MD_Os_Hal_Rule11.4_0303 */ /* SBSW_OS_HAL_PERIPHERAL_ACCESS */
																		   OS_HAL_MCTL_INVERTED_KEY | curRunMode;
  
	/* #30 Wait Until transition completed. */
	while (((*(volatile uint32*)(OS_HAL_MC_ME_BASE_ADDRESS + OS_HAL_MC_ME_INT_STATUS_REGISTER_OFFSET)) & 0x00000001uL)	  /* PRQA S 0303 */ /* MD_Os_Hal_Rule11.4_0303 */
		   != 1u)																										   /* COV_OS_HALPPCSTARTUPMCME */
	{
  
	}
	/* #40 Clear mode transition completed flag. */
	*(volatile uint32*)(OS_HAL_MC_ME_BASE_ADDRESS + OS_HAL_MC_ME_INT_STATUS_REGISTER_OFFSET) = 0x00000009uL;	
#endif
   /* Start OS must be called for each core */
   StartOS(appMode);

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_StartOS() */

FUNC(void, ECUM_CODE) EcuM_AL_SetProgrammableInterrupts(void)
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_AL_SetProgrammableInterrupts>      DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_AL_SetProgrammableInterrupts() */

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_AL_SetProgrammableInterrupts() */

#if((ECUM_CONFIGURATION_VARIANT == ECUM_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE) || (ECUM_POSTBUILD_VARIANT_SUPPORT == STD_ON))
FUNC(EcuM_GlobalConfigRefType, ECUM_CODE) EcuM_DeterminePbConfiguration(void)
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_DeterminePbConfiguration>          DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_DeterminePbConfiguration() */

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_DeterminePbConfiguration() */
#endif

#if (ECUM_ALARM_CLOCK_PRESENT == STD_ON)
FUNC(void, ECUM_CODE) EcuM_GptStartClock(Gpt_ChannelType GptChannel, Gpt_ModeType Mode, Gpt_ValueType Value)
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_GptStartClock>                     DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_GptStartClock() */

   Gpt_EnableNotification(GptChannel);
   Gpt_StartTimer(GptChannel, Value);
   Gpt_SetMode(Mode);

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_GptStartClock() */
#endif




/**********************************************************************************************************************
 *  CALLOUTS FROM THE SLEEP PHASE
 *********************************************************************************************************************/



#if(ECUM_SLAVE_CORE_HANDLING == STD_ON)
FUNC(void, ECUM_CODE) EcuM_WaitForSlaveCores(void)
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_WaitForSlaveCores>                 DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_WaitForSlaveCores() */

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_WaitForSlaveCores() */
#endif

#if(ECUM_SLEEPMODELIST == STD_ON)

FUNC(void, ECUM_CODE) EcuM_GenerateRamHash(void)
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_GenerateRamHash>                   DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_GenerateRamHash() */

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_GenerateRamHash() */

FUNC(uint8, ECUM_CODE) EcuM_CheckRamHash(void)
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_CheckRamHash>                      DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_CheckRamHash() */

   return (1u);
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_CheckRamHash() */

FUNC(void, ECUM_CODE) EcuM_McuSetMode(Mcu_ModeType McuMode)
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_McuSetMode>                        DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_McuSetMode() */
   VapToLpEntryRunnable();
Mcu_SetMode(McuMode);

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_McuSetMode() */

#if(ECUM_POLLINGOFSLEEPMODELIST == STD_ON)
FUNC(void, ECUM_CODE) EcuM_SleepActivity(void)
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_SleepActivity>                     DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_SleepActivity() */

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_SleepActivity() */
#endif

FUNC(void, ECUM_CODE) EcuM_EnableWakeupSources(EcuM_WakeupSourceType wakeupSource) /* PRQA S 3206 */ /* MD_EcuM_3206 */
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_EnableWakeupSources>               DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
#if (ECUM_USE_DUMMY_STATEMENT == STD_ON)
   /* dummy assignment to prevent compiler warnings on most of the compilers. */
   (void)wakeupSource; /* PRQA S 3112 *//* MD_EcuM_3112 */
#endif
   /* Add implementation of EcuM_EnableWakeupSources() */

   Icu_EnableNotification(IcuChannel_LIN1_RX);
   Icu_EnableNotification(IcuChannel_LIN2_RX);
   Icu_EnableNotification(IcuChannel_LIN3_RX);
   Icu_EnableNotification(IcuChannel_LIN4_RX);
   Icu_EnableNotification(IcuChannel_LIN5_RX);
  Icu_EnableNotification(IcuChannel_LFIC_IRQ); 
  Icu_EnableNotification(IcuChannel_RFIC_IRQ);

   Icu_EnableEdgeDetection(IcuChannel_LIN1_RX);
   Icu_EnableEdgeDetection(IcuChannel_LIN2_RX);
   Icu_EnableEdgeDetection(IcuChannel_LIN3_RX);
   Icu_EnableEdgeDetection(IcuChannel_LIN4_RX);
   Icu_EnableEdgeDetection(IcuChannel_LIN5_RX);
  Icu_EnableEdgeDetection(IcuChannel_LFIC_IRQ); 
  Icu_EnableEdgeDetection(IcuChannel_RFIC_IRQ);

   Icu_EnableWakeup(IcuChannel_CAN3RX);
   Icu_EnableWakeup(IcuChannel_CAN4RX);
   Icu_EnableWakeup(IcuChannel_CAN2RX);
   Icu_EnableWakeup(IcuChannel_LIN1_RX);
   Icu_EnableWakeup(IcuChannel_LIN2_RX);
   Icu_EnableWakeup(IcuChannel_LIN3_RX);
   Icu_EnableWakeup(IcuChannel_LIN4_RX);
   Icu_EnableWakeup(IcuChannel_LIN5_RX);
   Icu_EnableWakeup(IcuChannel_LFIC_IRQ);
   Icu_EnableWakeup(IcuChannel_RFIC_IRQ);

   

   /* enable RTC timer wakeup */
   Icu_EnableNotification(IcuConf_IcuChannel_IcuChannel_RTC);
   Icu_EnableEdgeDetection(IcuConf_IcuChannel_IcuChannel_RTC);
   Icu_EnableWakeup(IcuConf_IcuChannel_IcuChannel_RTC);

   //Gpt_EnableNotification(GptConf_GptChannelConfiguration_GptChannel_ExternalWdg);
   
   //Gpt_EnableWakeup(GptConf_GptChannelConfiguration_GptChannel_ExternalWdg);

#if 1
   Gpt_DisableNotification(GptConf_GptChannelConfiguration_GptChannelConfiguration_Wdg);
   Gpt_DisableNotification(GptConf_GptChannelConfiguration_GptRFControl);
   Gpt_DisableNotification(GptConf_GptChannelConfiguration_GptLFControl);
   Gpt_StopTimer(GptConf_GptChannelConfiguration_GptChannelConfiguration_Wdg);
   Gpt_StopTimer(GptConf_GptChannelConfiguration_GptRFControl);
   Gpt_StopTimer(GptConf_GptChannelConfiguration_GptLFControl);
#endif


 // Gpt_StartTimer(GptConf_GptChannelConfiguration_GptChannel_ExternalWdg, 2000);// 128*70 = 8960
  
   //GPR.CTL.R = 0x0E001000;              /* set RTC_SXOSC_FREE_RUNNING=1 to keep SXOSC working seamlessly over resets */
   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_EnableWakeupSources() */

FUNC(void, ECUM_CODE) EcuM_DisableWakeupSources(EcuM_WakeupSourceType wakeupSource) /* PRQA S 3206 */ /* MD_EcuM_3206 */
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_DisableWakeupSources>              DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
#if (ECUM_USE_DUMMY_STATEMENT == STD_ON)
   /* dummy assignment to prevent compiler warnings on most of the compilers. */
   (void)wakeupSource; /* PRQA S 3112 *//* MD_EcuM_3112 */
#endif
   /* Add implementation of EcuM_DisableWakeupSources() */

 // Gpt_StopTimer(GptConf_GptChannelConfiguration_GptChannel_ExternalWdg);


  /* disable PIT */
  (void)Disable_PIT(0);		//Enabling PIT -0

   Pwm_Init( &PwmChannelConfigSet );
  // Spi_Init(NULL_PTR);
   Adc_Init(&AdcConfigSet); 

   Icu_SetMode(ICU_MODE_NORMAL);
  //  Gpt_DisableNotification(GptConf_GptChannelConfiguration_GptChannel_ExternalWdg);
  //  Gpt_DisableWakeup(GptConf_GptChannelConfiguration_GptChannel_ExternalWdg); 
  
  // Gpt_SetMode(GPT_MODE_NORMAL);   
  

   /* enable RTC timer wakeup */
   Icu_DisableEdgeDetection(IcuConf_IcuChannel_IcuChannel_RTC);
   Icu_DisableWakeup(IcuConf_IcuChannel_IcuChannel_RTC);
   Icu_DisableNotification(IcuConf_IcuChannel_IcuChannel_RTC);

   Icu_DisableWakeup(IcuChannel_LIN1_RX);
   Icu_DisableWakeup(IcuChannel_LIN2_RX);
   Icu_DisableWakeup(IcuChannel_LIN3_RX);
   Icu_DisableWakeup(IcuChannel_LIN4_RX);
   Icu_DisableWakeup(IcuChannel_LIN5_RX);
   Icu_DisableWakeup(IcuChannel_LFIC_IRQ);
   Icu_DisableWakeup(IcuChannel_RFIC_IRQ);

   Icu_DisableEdgeDetection(IcuChannel_LIN1_RX);
   Icu_DisableEdgeDetection(IcuChannel_LIN2_RX);
   Icu_DisableEdgeDetection(IcuChannel_LIN3_RX);
   Icu_DisableEdgeDetection(IcuChannel_LIN4_RX);
   Icu_DisableEdgeDetection(IcuChannel_LIN5_RX);
  Icu_DisableEdgeDetection(IcuChannel_LFIC_IRQ); 
  Icu_DisableEdgeDetection(IcuChannel_RFIC_IRQ);

   Icu_DisableNotification(IcuChannel_LIN1_RX);
   Icu_DisableNotification(IcuChannel_LIN2_RX);
   Icu_DisableNotification(IcuChannel_LIN3_RX);
   Icu_DisableNotification(IcuChannel_LIN4_RX);
  Icu_DisableNotification(IcuChannel_LIN5_RX); 
  Icu_DisableNotification(IcuChannel_LFIC_IRQ); 
  Icu_DisableNotification(IcuChannel_RFIC_IRQ);

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_DisableWakeupSources() */
#endif

FUNC(void, ECUM_CODE) EcuM_CheckWakeup(EcuM_WakeupSourceType wakeupSource) /* COV_ECUM_CALLOUT */
{
#if (ECUM_CHECKWAKEUPTIMEOFWAKEUPSOURCELIST == STD_ON)
  /* Do not remove the following function call. It is necessary for the CheckWakeup timeout mechanism */
  EcuM_StartCheckWakeup(wakeupSource);
#endif

#if (ECUM_ALARM_CLOCK_PRESENT == STD_ON)
  if((ECUM_ALARM_WKSOURCE & wakeupSource) != 0u)
  {
    EcuM_AlarmCheckWakeup();
  }
#endif

/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_CheckWakeup>                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
#if (ECUM_USE_DUMMY_STATEMENT == STD_ON)
   /* dummy assignment to prevent compiler warnings on most of the compilers. */
   (void)wakeupSource; /* PRQA S 3112 *//* MD_EcuM_3112 */
#endif
   /* Add implementation of EcuM_CheckWakeup() */


   if ((wakeupSource & ECUM_WKSOURCE_RTC) != 0)
   {
     // Icu_CheckWakeup(ECUM_WKSOURCE_RTC);
   }

    if ((wakeupSource & ECUM_WKSOURCE_CN_LIN00_2cd9a7df) != 0)
   {
      LinIf_CheckWakeup(ECUM_WKSOURCE_CN_LIN00_2cd9a7df);
   }

   if ((wakeupSource & ECUM_WKSOURCE_CN_LIN01_5bde9749) != 0)
   {
      LinIf_CheckWakeup(ECUM_WKSOURCE_CN_LIN01_5bde9749);
   }

   if ((wakeupSource & ECUM_WKSOURCE_CN_LIN02_c2d7c6f3) != 0)
   {
      LinIf_CheckWakeup(ECUM_WKSOURCE_CN_LIN02_c2d7c6f3);
   }

   if ((wakeupSource & ECUM_WKSOURCE_CN_LIN03_b5d0f665) != 0)
   {
      LinIf_CheckWakeup(ECUM_WKSOURCE_CN_LIN03_b5d0f665);
   }

   if ((wakeupSource & ECUM_WKSOURCE_CN_LIN04_2bb463c6) != 0)
   {
      LinIf_CheckWakeup(ECUM_WKSOURCE_CN_LIN04_2bb463c6);
   }
   

   if ((wakeupSource & ECUM_WKSOURCE_LFIC) != 0)
   {
      Icu_CheckWakeup(ECUM_WKSOURCE_LFIC);
   }

   if ((wakeupSource & ECUM_WKSOURCE_RFIC) != 0)
   {
      Icu_CheckWakeup(ECUM_WKSOURCE_RFIC);
   }

   if ((wakeupSource & ECUM_WKSOURCE_CN_Backbone2_78967e2c) != 0)
   {
      Icu_CheckWakeup(ECUM_WKSOURCE_CN_Backbone2_78967e2c);
   }

   if ((wakeupSource & ECUM_WKSOURCE_CN_CabSubnet_9ea693f1) != 0)
   {
      Icu_CheckWakeup(ECUM_WKSOURCE_CN_CabSubnet_9ea693f1);
   }

   if ((wakeupSource & ECUM_WKSOURCE_CN_SecuritySubnet_e7a0ee54) != 0)
   {
      Icu_CheckWakeup(ECUM_WKSOURCE_CN_SecuritySubnet_e7a0ee54);
   }

   if ((wakeupSource & ECUM_WKSOURCE_CN_Backbone1J1939_0b1f4bae) != 0)
   {
      Icu_CheckWakeup(ECUM_WKSOURCE_CN_Backbone1J1939_0b1f4bae);
   }

   if ((wakeupSource & ECUM_WKSOURCE_CN_FMSNet_fce1aae5) != 0)
   {
      Icu_CheckWakeup(ECUM_WKSOURCE_CN_FMSNet_fce1aae5);
   }

  

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_CheckWakeup() */

#if ((ECUM_ALARM_CLOCK_PRESENT == STD_ON) && (ECUM_SLEEPMODELIST == STD_ON))
FUNC(void, ECUM_CODE) EcuM_GptSetNormal(Gpt_ChannelType GptChannel, Gpt_ModeType Mode)
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_GptSetNormal>                      DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_GptSetNormal() */

   Gpt_EnableNotification(GptChannel);
   Gpt_SetMode(Mode);

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_GptSetNormal() */

FUNC(void, ECUM_CODE) EcuM_GptSetSleep(Gpt_ChannelType GptChannel, Gpt_ModeType Mode)
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_GptSetSleep>                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
   /* Add implementation of EcuM_GptSetSleep() */

   Gpt_EnableWakeup(GptChannel);
   Gpt_SetMode(Mode);

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_GptSetSleep() */
#endif



/**********************************************************************************************************************
 *  CALLOUTS FROM THE UP PHASE
 *********************************************************************************************************************/

#if(ECUM_VALIDATIONTIMEOFWAKEUPSOURCELIST == STD_ON)

FUNC(void, ECUM_CODE) EcuM_StartWakeupSources(EcuM_WakeupSourceType wakeupSource) /* PRQA S 3206 */ /* MD_EcuM_3206 */
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_StartWakeupSources>                DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
#if (ECUM_USE_DUMMY_STATEMENT == STD_ON)
   /* dummy assignment to prevent compiler warnings on most of the compilers. */
   (void)wakeupSource; /* PRQA S 3112 *//* MD_EcuM_3112 */
#endif
   /* Add implementation of EcuM_StartWakeupSources() */

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_StartWakeupSources() */

FUNC(void, ECUM_CODE) EcuM_StopWakeupSources(EcuM_WakeupSourceType wakeupSource) /* PRQA S 3206 */ /* MD_EcuM_3206 */
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_StopWakeupSources>                 DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
#if (ECUM_USE_DUMMY_STATEMENT == STD_ON)
   /* dummy assignment to prevent compiler warnings on most of the compilers. */
   (void)wakeupSource; /* PRQA S 3112 *//* MD_EcuM_3112 */
#endif
   /* Add implementation of EcuM_StopWakeupSources() */

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_StopWakeupSources() */

FUNC(void, ECUM_CODE) EcuM_CheckValidation(EcuM_WakeupSourceType wakeupSource)
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_CheckValidation>                   DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
#if (ECUM_USE_DUMMY_STATEMENT == STD_ON)
   /* dummy assignment to prevent compiler warnings on most of the compilers. */
   (void)wakeupSource; /* PRQA S 3112 *//* MD_EcuM_3112 */
#endif
   /* Add implementation of EcuM_CheckValidation() */

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_CheckValidation() */

#endif



#if (ECUM_BSW_ERROR_HOOK == STD_ON)
/**********************************************************************************************************************
 *  Errorhook for BSW errors during initialization
 *********************************************************************************************************************/
FUNC(void, ECUM_CODE) EcuM_BswErrorHook(uint16 BswModuleId, uint8 ErrorId) /* PRQA S 3206 */ /* MD_EcuM_3206 */ /* COV_ECUM_CALLOUT */
{
/**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           <USERBLOCK EcuM_BswErrorHook>                      DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/

#if (ECUM_USE_DUMMY_STATEMENT == STD_ON)
   /* dummy assignment to prevent compiler warnings on most of the compilers. */
   (void)BswModuleId; /* PRQA S 3112 *//* MD_EcuM_3112 */
   (void)ErrorId; /* PRQA S 3112 *//* MD_EcuM_3112 */
#endif
   /* Add implementation of EcuM_BswErrorHook() */

   if(BswModuleId == ECUM_MODULE_ID)
   {
      /* Abort initialization per default if the EcuM has reported an BswError */
      while(1)
      {
      }
   }

   return;
   /**********************************************************************************************************************
    * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
    *********************************************************************************************************************/
} /* End of EcuM_BswErrorHook() */
#endif

#define ECUM_STOP_SEC_CODE
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_MemMap */

#if 0
#endif


/**********************************************************************************************************************
 *  END OF FILE: ECUM_CALLOUT_STUBS.C
 *********************************************************************************************************************/


