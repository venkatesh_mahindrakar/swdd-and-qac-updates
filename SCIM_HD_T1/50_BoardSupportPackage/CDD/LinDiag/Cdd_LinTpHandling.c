/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  Cdd_LinTpHandling.c
 *        Config:  C:/Personal/GIT/Prj/scim_ecu_hd_t1/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  Cdd_LinTpHandling
 *  Generated at:  Wed Jul 24 11:31:04 2019
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <Cdd_LinTpHandling>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file Cdd_LinTpHandling.c
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_VehicleControlAndActiveSafety 
//! @{
//! @addtogroup ActiveSafety 
//! @{
//! @addtogroup Cdd_LinTpHandling
//! @{
//!
//! \brief
//! Cdd_LinTpHandling SWC.
//! ASIL Level : QM.
//! This module implements the logic for the Cdd_LinTpHandling runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_Cdd_LinTpHandling.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "Cdd_LinTpHandling.h"
#include "PduR.h"
#include "ComM.h"

/**********************************************************************************************************************
 *   LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

static uint8 LinDiagReqLength[MAX_NUMBER_OF_LINNetworks][MAX_NUMBER_OF_REQPENDING];
static uint8 LinDiagReqData[MAX_NUMBER_OF_LINNetworks][MAX_NUMBER_OF_REQPENDING][MAX_LINTP_REQ_LENGTH];
static uint8 LinDiagRespBuffer[MAX_NUMBER_OF_LINNetworks][MAX_LINTP_RESP_LENGTH];
static uint8 LinTpStarted;

CddLinDiagReqPendingType LINPendingReq[MAX_NUMBER_OF_LINNetworks][MAX_NUMBER_OF_REQPENDING];
CddLinDiagPduMappingType CddLinPduMap[MAX_NUMBER_OF_LINTP_PDUs] = {{CddConf_CddPduRUpperLayerTxPdu_MasterReq_CCFW_oLIN03_98357989_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_2428ced7,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_CCFW_oLIN03_d7124ce9_Rx,
                                                                    CONST_CDD_LINTP_BUS3},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_DLFW_oLIN03_40053787_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_415f4d5f,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_DLFW_oLIN03_0f2202e7_Rx,
                                                                    CONST_CDD_LINTP_BUS3},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_ELCP1_oLIN03_cbecb6de_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_d6070b6b,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_ELCP1_oLIN03_8611f0b3_Rx,
                                                                    CONST_CDD_LINTP_BUS3},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_ELCP2_oLIN03_4563b13d_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_4290d944,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_ELCP2_oLIN03_089ef750_Rx,
                                                                    CONST_CDD_LINTP_BUS3},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_ILCP1_oLIN00_57efaae5_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_811e958b,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_ILCP1_oLIN00_1a12ec88_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_ILCP2_oLIN03_4069fcbc_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_a3dfff04,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_ILCP2_oLIN03_0d94bad1_Rx,
                                                                    CONST_CDD_LINTP_BUS3},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_LECM2_oLIN00_670141b5_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_8fec9c64,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_LECM2_oLIN00_2afc07d8_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_LECMBasic_oLIN00_16bdebb7_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_47b56dbe,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_LECMBasic_oLIN00_029997c6_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_RCECS_oLIN04_b46d7afd_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_a24245d0,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_RCECS_oLIN04_f9903c90_Rx,
                                                                    CONST_CDD_LINTP_BUS4},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_TCP_oLIN02_4b422897_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_5862727a,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_TCP_oLIN02_b3851a34_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L1_oLIN00_2de78d2a_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_d25a31a4,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_L1_oLIN00_620b3198_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L2_oLIN01_d46fba5f_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_a3b679cc,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_L2_oLIN01_9b8306ed_Rx,
                                                                    CONST_CDD_LINTP_BUS1},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L3_oLIN02_81cceb7b_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_292228a9,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_L3_oLIN02_ce2057c9_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L4_oLIN03_fc0ed2f4_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_4e85369a,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_L4_oLIN03_b3e26e46_Rx,
                                                                    CONST_CDD_LINTP_BUS3},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L5_oLIN04_aec047c9_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_6d3fc1f8,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_L5_oLIN04_e12cfb7b_Rx,
                                                                    CONST_CDD_LINTP_BUS4},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L1_oLIN00_b405eb2b_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_991b9435,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_L1_oLIN00_fbe95799_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L2_oLIN01_4d8ddc5e_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_a2b3fcad,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_L2_oLIN01_026160ec_Rx,
                                                                    CONST_CDD_LINTP_BUS1},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L3_oLIN02_182e8d7a_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_fd379ced,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_L3_oLIN02_57c231c8_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_L2_oLIN01_8c03039e_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_2bd9ef52,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_L2_oLIN01_c3efbf2c_Rx,
                                                                    CONST_CDD_LINTP_BUS1},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP4_L2_oLIN01_a538161d_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_f008f462,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP4_L2_oLIN01_ead4aaaf_Rx,
                                                                    CONST_CDD_LINTP_BUS1},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN00_4a2bb011_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_927d3065,
                                                                    CddConf_NoRxPdu,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN01_3d2c8087_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_e38dbced,
                                                                    CddConf_NoRxPdu,
                                                                    CONST_CDD_LINTP_BUS1},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN02_a425d13d_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_d58909f4,
                                                                    CddConf_NoRxPdu,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN03_d322e1ab_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_509ea690,
                                                                    CddConf_NoRxPdu,
                                                                    CONST_CDD_LINTP_BUS3},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN04_4d467408_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_bc8ff73d,
                                                                    CddConf_NoRxPdu,
                                                                    CONST_CDD_LINTP_BUS4},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4A_oLIN00_cacc7d77_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_3d06fbde,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_4A_oLIN00_8520c1c5_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4B_oLIN00_44437a94_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_224a6cfa,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_4B_oLIN00_0bafc626_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4C_oLIN00_88e97a0a_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_5221f1e9,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_4C_oLIN00_c705c6b8_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4D_oLIN00_822c7313_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_6bb04367,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_4D_oLIN00_cdc0cfa1_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4E_oLIN00_4e86738d_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_8c275581,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_4E_oLIN00_016acf3f_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4F_oLIN00_c009746e_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_5547c4e8,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_4F_oLIN00_8fe5c8dc_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5A_oLIN00_ddb76934_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_5a8b564b,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_5A_oLIN00_925bd586_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5B_oLIN00_53386ed7_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_4819b774,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_5B_oLIN00_1cd4d265_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5C_oLIN00_9f926e49_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_a47b7d57,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_5C_oLIN00_d07ed2fb_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5D_oLIN00_95576750_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_b8e6d439,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_5D_oLIN00_dabbdbe2_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5E_oLIN00_59fd67ce_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_1651d15b,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_5E_oLIN00_1611db7c_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5F_oLIN00_d772602d_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_48abb014,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_5F_oLIN00_989edc9f_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_40_oLIN00_aa36ec79_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_e9cb21a0,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_40_oLIN00_e5da50cb_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_41_oLIN00_669cece7_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_ac05d3d4,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_41_oLIN00_29705055_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_42_oLIN00_e813eb04_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_3bdd78a6,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_42_oLIN00_a7ff57b6_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_43_oLIN00_24b9eb9a_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_3edf9753,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_43_oLIN00_6b555728_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_44_oLIN00_2e7ce283_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_2584d1ef,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_44_oLIN00_61905e31_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_45_oLIN00_e2d6e21d_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_477d6a44,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_45_oLIN00_ad3a5eaf_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_46_oLIN00_6c59e5fe_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_ccdd7242,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_46_oLIN00_23b5594c_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_47_oLIN00_a0f3e560_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_b4c3edc8,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_47_oLIN00_ef1f59d2_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_48_oLIN00_79d3f7cc_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_dfdfd2a3,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_48_oLIN00_363f4b7e_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_49_oLIN00_b579f752_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_b7904bda,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_49_oLIN00_fa954be0_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_50_oLIN00_bd4df83a_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_d620f52d,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_50_oLIN00_f2a14488_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_51_oLIN00_71e7f8a4_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_e513302a,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_51_oLIN00_3e0b4416_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_52_oLIN00_ff68ff47_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_ee7f0bf8,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_52_oLIN00_b08443f5_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_53_oLIN00_33c2ffd9_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_ac05acdd,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_53_oLIN00_7c2e436b_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_54_oLIN00_3907f6c0_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_9afa723d,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_54_oLIN00_76eb4a72_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_55_oLIN00_f5adf65e_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_1948bc78,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_55_oLIN00_ba414aec_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_56_oLIN00_7b22f1bd_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_a305ad93,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_56_oLIN00_34ce4d0f_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_57_oLIN00_b788f123_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_fa91c439,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_57_oLIN00_f8644d91_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_58_oLIN00_6ea8e38f_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_e73ae741,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_58_oLIN00_21445f3d_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_59_oLIN00_a202e311_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_27339506,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_59_oLIN00_edee5fa3_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_60_oLIN00_84c0c4ff_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_01ba327b,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_60_oLIN00_cb2c784d_Rx,
                                                                    CONST_CDD_LINTP_BUS0},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4A_oLIN01_24292be0_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_8ad2861d,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_4A_oLIN01_6bc59752_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4B_oLIN01_aaa62c03_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_20ff6e11,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_4B_oLIN01_e54a90b1_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4C_oLIN01_660c2c9d_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_6cf0fd3c,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_4C_oLIN01_29e0902f_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4D_oLIN01_6cc92584_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_710a49e6,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_4D_oLIN01_23259936_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4E_oLIN01_a063251a_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_4f0d025c,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_4E_oLIN01_ef8f99a8_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4F_oLIN01_2eec22f9_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_d9b333e4,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_4F_oLIN01_61009e4b_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5A_oLIN01_33523fa3_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_d582f029,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_5A_oLIN01_7cbe8311_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5B_oLIN01_bddd3840_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_ba710b90,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_5B_oLIN01_f23184f2_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5C_oLIN01_717738de_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_dd74b571,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_5C_oLIN01_3e9b846c_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5D_oLIN01_7bb231c7_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_ea1d6e05,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_5D_oLIN01_345e8d75_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5E_oLIN01_b7183159_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_543d450a,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_5E_oLIN01_f8f48deb_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5F_oLIN01_399736ba_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_a71a8e0d,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_5F_oLIN01_767b8a08_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_40_oLIN01_44d3baee_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_01d86c9d,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_40_oLIN01_0b3f065c_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_41_oLIN01_8879ba70_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_108627d2,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_41_oLIN01_c79506c2_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_42_oLIN01_06f6bd93_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_5a647c4b,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_42_oLIN01_491a0121_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_43_oLIN01_ca5cbd0d_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_8844b462,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_43_oLIN01_85b001bf_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_44_oLIN01_c099b414_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_e7e7cd78,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_44_oLIN01_8f7508a6_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_45_oLIN01_0c33b48a_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_c4d27be7,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_45_oLIN01_43df0838_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_46_oLIN01_82bcb369_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_1bed3579,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_46_oLIN01_cd500fdb_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_47_oLIN01_4e16b3f7_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_a18d3920,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_47_oLIN01_01fa0f45_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_48_oLIN01_9736a15b_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_7ad18be8,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_48_oLIN01_d8da1de9_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_49_oLIN01_5b9ca1c5_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_23f01529,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_49_oLIN01_14701d77_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_50_oLIN01_53a8aead_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_4de8eb8d,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_50_oLIN01_1c44121f_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_51_oLIN01_9f02ae33_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_9d48ed4d,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_51_oLIN01_d0ee1281_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_52_oLIN01_118da9d0_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_18b1f013,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_52_oLIN01_5e611562_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_53_oLIN01_dd27a94e_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_a145b9df,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_53_oLIN01_92cb15fc_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_54_oLIN01_d7e2a057_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_379abf8d,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_54_oLIN01_980e1ce5_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_55_oLIN01_1b48a0c9_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_a74cbc04,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_55_oLIN01_54a41c7b_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_56_oLIN01_95c7a72a_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_30324227,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_56_oLIN01_da2b1b98_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_57_oLIN01_596da7b4_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_62dfcb6f,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_57_oLIN01_16811b06_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_58_oLIN01_804db518_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_4e724430,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_58_oLIN01_cfa109aa_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_59_oLIN01_4ce7b586_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_206364a8,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_59_oLIN01_030b0934_Rx,
                                                                    CONST_CDD_LINTP_BUS1},  
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_60_oLIN01_6a259268_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_c8760d26,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_60_oLIN01_25c92eda_Rx,
                                                                    CONST_CDD_LINTP_BUS1}, 
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4A_oLIN02_7caea59a_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_42462b26,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_4A_oLIN02_33421928_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4B_oLIN02_f221a279_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_3436a065,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_4B_oLIN02_bdcd1ecb_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4C_oLIN02_3e8ba2e7_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_effbe257,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_4C_oLIN02_71671e55_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4D_oLIN02_344eabfe_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_9eb2e6b1,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_4D_oLIN02_7ba2174c_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4E_oLIN02_f8e4ab60_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_d065da29,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_4E_oLIN02_b70817d2_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4F_oLIN02_766bac83_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_667ae06c,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_4F_oLIN02_39871031_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5A_oLIN02_6bd5b1d9_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_8822de73,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_5A_oLIN02_24390d6b_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5B_oLIN02_e55ab63a_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_b0d5198a,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_5B_oLIN02_aab60a88_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5C_oLIN02_29f0b6a4_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_f258e735,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_5C_oLIN02_661c0a16_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5D_oLIN02_2335bfbd_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_f438a6b7,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_5D_oLIN02_6cd9030f_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5E_oLIN02_ef9fbf23_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_bfa99e31,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_5E_oLIN02_a0730391_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5F_oLIN02_6110b8c0_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_a3478da4,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_5F_oLIN02_2efc0472_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_40_oLIN02_1c543494_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_35a1b692,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_40_oLIN02_53b88826_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_41_oLIN02_d0fe340a_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_289cd2f3,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_41_oLIN02_9f1288b8_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_42_oLIN02_5e7133e9_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_36322f96,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_42_oLIN02_119d8f5b_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_43_oLIN02_92db3377_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_30fd7928,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_43_oLIN02_dd378fc5_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_44_oLIN02_981e3a6e_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_e9b90537,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_44_oLIN02_d7f286dc_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_45_oLIN02_54b43af0_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_8a6bf288,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_45_oLIN02_1b588642_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_46_oLIN02_da3b3d13_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_fde47258,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_46_oLIN02_95d781a1_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_47_oLIN02_16913d8d_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_dfef8e98,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_47_oLIN02_597d813f_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_48_oLIN02_cfb12f21_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_976d8e31,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_48_oLIN02_805d9393_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_49_oLIN02_031b2fbf_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_b2d43218,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_49_oLIN02_4cf7930d_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_50_oLIN02_0b2f20d7_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_1885f5b6,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_50_oLIN02_44c39c65_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_51_oLIN02_c7852049_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_55c8fe9a,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_51_oLIN02_88699cfb_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_52_oLIN02_490a27aa_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_3cd6432e,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_52_oLIN02_06e69b18_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_53_oLIN02_85a02734_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_0b32cf77,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_53_oLIN02_ca4c9b86_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_54_oLIN02_8f652e2d_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_51adf99a,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_54_oLIN02_c089929f_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_55_oLIN02_43cf2eb3_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_ec4815db,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_55_oLIN02_0c239201_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_56_oLIN02_cd402950_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_2f1cafc0,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_56_oLIN02_82ac95e2_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_57_oLIN02_01ea29ce_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_433f0139,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_57_oLIN02_4e06957c_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_58_oLIN02_d8ca3b62_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_1edf09de,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_58_oLIN02_972687d0_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_59_oLIN02_14603bfc_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_8286f31d,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_59_oLIN02_5b8c874e_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_60_oLIN02_32a21c12_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_a501c7de,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_60_oLIN02_7d4ea0a0_Rx,
                                                                    CONST_CDD_LINTP_BUS2},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP4_40_oLIN03_42681181_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_1ac1c61f,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP4_40_oLIN03_0d84ad33_Rx,
                                                                    CONST_CDD_LINTP_BUS3},
                                                                   {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP5_40_oLIN04_1d825be2_Tx,
                                                                    PduRConf_PduRSrcPdu_PduRSrcPdu_ccfd4d5a,
                                                                    CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP5_40_oLIN04_526ee750_Rx,
                                                                    CONST_CDD_LINTP_BUS4}
                                                                  };

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * CddLinTp_Status: Enumeration of integer in interval [0...255] with enumerators
 *   CDD_LIN_NOT_OK (11U)
 *   CDD_LIN_TX_OK (0U)
 *   CDD_LIN_TX_BUSY (1U)
 *   CDD_LIN_TX_HEADER_ERROR (2U)
 *   CDD_LIN_TX_ERROR (3U)
 *   CDD_LIN_RX_OK (4U)
 *   CDD_LIN_RX_BUSY (5U)
 *   CDD_LIN_RX_ERROR (6U)
 *   CDD_LIN_RX_NO_RESPONSE (7U)
 *   CDD_LIN_NONE (9U)
 *
 *********************************************************************************************************************/


#define Cdd_LinTpHandling_START_SEC_CODE
#include "Cdd_LinTpHandling_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinTpClearRequest
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ClearRequest> of PortPrototype <CddLinClearRequest>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void Cdd_LinTpClearRequest(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpClearRequest_doc
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief
//! This function implements the logic for clearing Cdd_LinTp Transmit requests
//!
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpClearRequest(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpClearRequest
 *********************************************************************************************************************/
   uint8 PRIndex     = 0U;
   uint8 LinBusIndex = 0U;
   uint8 Length      = 0U;

   //! ###### Process of clearing CDD_LinTp transmit requests
   for (LinBusIndex = 0U; LinBusIndex < MAX_NUMBER_OF_LINNetworks; LinBusIndex++)
   {
      for (PRIndex = 0U; PRIndex < MAX_NUMBER_OF_REQPENDING; PRIndex++)
      {
         //! ##### Clear the stored and already processing transmit requests
         //! ##### Set default values to internal transmit buffers
         LINPendingReq[LinBusIndex][PRIndex].PendingFlag = CDD_LINTP_NONE;
         LINPendingReq[LinBusIndex][PRIndex].ReqId       = CddConf_NoTxPdu;
         LINPendingReq[LinBusIndex][PRIndex].TimeoutVal  = CDD_LINTP_TIMEOUT;
         LinDiagReqLength[LinBusIndex][PRIndex]          = 0U;
         for (Length = 0U; Length < MAX_LINTP_REQ_LENGTH; Length++)
         {
            LinDiagReqData[LinBusIndex][PRIndex][Length] = 0U;
         }
      }
      //! ##### Set default values to internal responce buffer
      for (Length = 0U; Length < MAX_LINTP_RESP_LENGTH; Length++)
      {
         LinDiagRespBuffer[LinBusIndex][Length] = 0U;
      }
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinTpLin1EndofNotification
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN1SchTableState>
 *
 **********************************************************************************************************************
 *
 * Mode Interfaces:
 * ================
 *   uint8 Rte_Mode_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN1Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_NULL
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table1
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table2
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN1Schedule
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin1EndofNotification_doc
 *********************************************************************************************************************/

//!======================================================================================
//!
//! \brief
//! This function implements the logic for process the transmit requests at the end of LIN1 schedule table    
//! This function is triggered on entering end of Lin1 schedule table 
//!  
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpLin1EndofNotification(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin1EndofNotification
 *********************************************************************************************************************/
   uint8       Mode        = 0U;
   PduInfoType LocalPduInfo;
   uint8       LinBusIndex = 0U;

   //! ###### Processing the transmit requests at the end of LIN1 schedule table
   Mode = Rte_Mode_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule();
   //! ##### Check whether the current schedule table will process the diagnostic messages or not
   if ((RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2 == Mode)
      || ((RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1 == Mode))
      || ((RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp == Mode)))
   {
      //! #### Check for transmit pending requests 
      //! #### Prepare the transmit buffers and trigger the transmittion (to LinTP through PDUR)
      //! #### Update the transmit request flag to triggered
      if ((CddConf_NoTxPdu != LINPendingReq[LinBusIndex][0].ReqId)
         && ((CDD_LINTP_TX_PENDING == LINPendingReq[LinBusIndex][0].PendingFlag)))
      {
         LocalPduInfo.SduLength    = LinDiagReqLength[LinBusIndex][0];
         LocalPduInfo.SduDataPtr   = &LinDiagReqData[LinBusIndex][0][0];
         PduR_CddTransmit(CddLinPduMap[LINPendingReq[LinBusIndex][0].ReqId].TxDstPduId,
                          &LocalPduInfo);
         LINPendingReq[LinBusIndex][0].PendingFlag = CDD_LINTP_TX_TRIG;
         LINPendingReq[LinBusIndex][0].TimeoutVal  = CDD_LINTP_TIMERLOADVAL;
      }
      else
      {
         // Do nothing
      }
   }
   else
   {
      // Do nothing
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinTpLin2EndofNotification
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN2SchTableState>
 *
 **********************************************************************************************************************
 *
 * Mode Interfaces:
 * ================
 *   uint8 Rte_Mode_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN2Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_NULL
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE0
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN2Schedule
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin2EndofNotification_doc
 *********************************************************************************************************************/

//!======================================================================================
//!
//! \brief
//! This function implements the logic for process the transmit requests at the end of LIN2 schedule table   
//! This function is triggered on entering end of Lin2 schedule table
//!
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpLin2EndofNotification(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin2EndofNotification
 *********************************************************************************************************************/
   uint8       Mode        = 0U;
   PduInfoType LocalPduInfo;
   uint8       LinBusIndex = 1U;

   //! ###### Processing the transmit requests at the end of LIN2 schedule table
   Mode = Rte_Mode_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule();
   //! ##### Check whether the current schedule table will process the diagnostic messages or not
   if ((RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0 == Mode)
      || (RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp == Mode))
   {
      //! #### Check for transmit pending requests 
      //! #### Prepare the transmit buffers and trigger the transmittion (to LinTP through PDUR)
      //! #### Update the transmit request flag to triggered
      if ((CddConf_NoTxPdu != LINPendingReq[LinBusIndex][0].ReqId)
         && ((CDD_LINTP_TX_PENDING == LINPendingReq[LinBusIndex][0].PendingFlag)))
      {
         LocalPduInfo.SduLength    = LinDiagReqLength[LinBusIndex][0];
         LocalPduInfo.SduDataPtr   = &LinDiagReqData[LinBusIndex][0][0];
         PduR_CddTransmit(CddLinPduMap[LINPendingReq[LinBusIndex][0].ReqId].TxDstPduId,
                          &LocalPduInfo);
         LINPendingReq[LinBusIndex][0].PendingFlag = CDD_LINTP_TX_TRIG;
         LINPendingReq[LinBusIndex][0].TimeoutVal  = CDD_LINTP_TIMERLOADVAL;
      } 
      else
      {
         // Do nothing
      }
   }
   else
   {
      // Do nothing
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinTpLin3EndofNotification
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN3SchTableState>
 *
 **********************************************************************************************************************
 *
 * Mode Interfaces:
 * ================
 *   uint8 Rte_Mode_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN3Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_NULL
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE1
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE2
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN3Schedule
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin3EndofNotification_doc
 *********************************************************************************************************************/

//!======================================================================================
//!
//! \brief
//! This function implements the logic for Process the Transmit requests at the End of LIN3 Schedule table    
//! This function is triggered on entering End of Lin3 schedule table
//!
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpLin3EndofNotification(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin3EndofNotification
 *********************************************************************************************************************/
   uint8       Mode        = 0U;
   PduInfoType LocalPduInfo;
   uint8       LinBusIndex = 2U;

   //! ###### Processing the transmit requests at the end of LIN3 schedule table
   Mode = Rte_Mode_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule();
   //! ##### Check whether the current schedule table will process the diagnostic messages or not
   if ((RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2 == Mode)
      || (RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1 == Mode)
      || (RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp == Mode))
   {
      //! #### Check for transmit pending requests 
      //! #### Prepare the transmit buffers and trigger the transmittion (to LinTP through PDUR)
      //! #### Update the transmit request flag to triggered
      if ((CddConf_NoTxPdu != LINPendingReq[LinBusIndex][0].ReqId)
         && ((CDD_LINTP_TX_PENDING == LINPendingReq[LinBusIndex][0].PendingFlag)))
      {
         LocalPduInfo.SduLength    = LinDiagReqLength[LinBusIndex][0];
         LocalPduInfo.SduDataPtr   = &LinDiagReqData[LinBusIndex][0][0];
         PduR_CddTransmit(CddLinPduMap[LINPendingReq[LinBusIndex][0].ReqId].TxDstPduId,
                          &LocalPduInfo);
         LINPendingReq[LinBusIndex][0].PendingFlag = CDD_LINTP_TX_TRIG;
         LINPendingReq[LinBusIndex][0].TimeoutVal  = CDD_LINTP_TIMERLOADVAL;
      }
      else
      {
         // Do nothing
      }
   }
   else
   {
      // Do nothing
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinTpLin4EndofNotification
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN4SchTableState>
 *
 **********************************************************************************************************************
 *
 * Mode Interfaces:
 * ================
 *   uint8 Rte_Mode_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN4Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_NULL
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE1
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE2
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN4Schedule
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin4EndofNotification_doc
 *********************************************************************************************************************/

//!======================================================================================
//!
//! \brief
//! This function implements the logic for process the transmit requests at the end of LIN4 schedule table    
//! This function is triggered on entering end of Lin4 schedule table
//!
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpLin4EndofNotification(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin4EndofNotification
 *********************************************************************************************************************/
   uint8       Mode        = 0U;
   PduInfoType LocalPduInfo;
   uint8       LinBusIndex = 3U;

   //! ###### Processing the transmit requests at the end of LIN4 schedule table
   Mode = Rte_Mode_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule();
   //! ##### Check whether the current schedule table will process the diagnostic messages or not
   if ((RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2 == Mode)
      || (RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1 == Mode)
      || (RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp == Mode))
   {
      //! #### Check for transmit pending requests 
      //! #### Prepare the transmit buffers and trigger the transmittion (to LinTP through PDUR)
      //! #### Update the transmit request flag to triggered
      if ((CddConf_NoTxPdu != LINPendingReq[LinBusIndex][0].ReqId) 
         && ((CDD_LINTP_TX_PENDING == LINPendingReq[LinBusIndex][0].PendingFlag)))
      {
         LocalPduInfo.SduLength    = LinDiagReqLength[LinBusIndex][0];
         LocalPduInfo.SduDataPtr   = &LinDiagReqData[LinBusIndex][0][0];
         PduR_CddTransmit(CddLinPduMap[LINPendingReq[LinBusIndex][0].ReqId].TxDstPduId,
                          &LocalPduInfo);
         LINPendingReq[LinBusIndex][0].PendingFlag = CDD_LINTP_TX_TRIG;
         LINPendingReq[LinBusIndex][0].TimeoutVal = CDD_LINTP_TIMERLOADVAL;
      }
      else
      {
         // Do nothing
      }
   }
   else
   {
      // Do nothing
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinTpLin5EndofNotification
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN5SchTableState>
 *
 **********************************************************************************************************************
 *
 * Mode Interfaces:
 * ================
 *   uint8 Rte_Mode_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN5Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_NULL
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE1
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE2
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN5Schedule
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin5EndofNotification_doc
 *********************************************************************************************************************/

//!======================================================================================
//!
//! \brief
//! This function implements the logic for process the transmit requests at the end of LIN5 schedule table     
//! This function is triggered on entering end of Lin5 schedule table
//!
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpLin5EndofNotification(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin5EndofNotification
 *********************************************************************************************************************/
   uint8       Mode        = 0U;
   PduInfoType LocalPduInfo;
   uint8       LinBusIndex = 4U;

   //! ###### Processing the transmit requests at the end of LIN5 schedule table
   Mode = Rte_Mode_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule();
   //! ##### Check whether the current schedule table will process the diagnostic messages or not
   if ((RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2 == Mode)
      || (RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1 == Mode)
      || (RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp == Mode))
   {
      //! #### Check for transmit pending requests 
      //! #### Prepare the transmit buffers and trigger the transmittion (to LinTP through PDUR)
      //! #### Update the transmit request flag to triggered
      if ((CddConf_NoTxPdu != LINPendingReq[LinBusIndex][0].ReqId)
         && ((CDD_LINTP_TX_PENDING == LINPendingReq[LinBusIndex][0].PendingFlag)))
      {
         LocalPduInfo.SduLength    = LinDiagReqLength[LinBusIndex][0];
         LocalPduInfo.SduDataPtr   = &LinDiagReqData[LinBusIndex][0][0];
         PduR_CddTransmit(CddLinPduMap[LINPendingReq[LinBusIndex][0].ReqId].TxDstPduId,&LocalPduInfo);
         LINPendingReq[LinBusIndex][0].PendingFlag = CDD_LINTP_TX_TRIG;
         LINPendingReq[LinBusIndex][0].TimeoutVal  = CDD_LINTP_TIMERLOADVAL;
      }
      else
      {
         // Do nothing
      }
   }
   else
   {
      // Do nothing
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
/**********************************************************************************************************************
 *
 * Runnable Entity Name: CddLinTp_Transmit
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Transmit> of PortPrototype <CddLinTxHandling>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void CddLinTp_Transmit(uint8 TxId, uint8 *TxData, uint8 Length)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinTp_Transmit_doc
 *********************************************************************************************************************/

//!======================================================================================
//!
//! \brief
//! This function implements the logic for stores the CDD to LinTP transmit requests
//!
//! \param   TxId     Transmit message identifier   
//! \param   TxData   Transmit message data
//! \param   Length   Transmit message length
//!
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpTransmit(uint8 TxId, 
                                                     P2VAR(uint8, AUTOMATIC, RTE_CDD_LINTPHANDLING_APPL_VAR) TxData, 
                                                     uint8 Length) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinTp_Transmit
 *********************************************************************************************************************/
   Std_ReturnType retval = E_NOT_OK;
   uint8 index           = 0U;
   uint8 i               = 0U;
   uint8 PRIndex         = 0U;
   uint8 LinBusIndex     = 0U;
   LinBusIndex           = CddLinPduMap[TxId].LinBus;
   LinTpStarted          = 1U;

   //! ###### Process of storing the CDD to LinTP transmit requests
   //! ##### Check for free spaces in transmit request buffer
   while (CDD_LINTP_NONE != LINPendingReq[LinBusIndex][PRIndex].PendingFlag)
   {
      PRIndex++;
   }
   //! #### Stores the transmit message ID, data and length
   //! #### Update the transmit request flag to pending
   if (PRIndex < MAX_NUMBER_OF_REQPENDING)
   {
      LINPendingReq[LinBusIndex][PRIndex].ReqId        = TxId;
      LINPendingReq[LinBusIndex][PRIndex].PendingFlag  = CDD_LINTP_TX_PENDING;
      LINPendingReq[LinBusIndex][PRIndex].TimeoutVal   = CDD_LINTP_TIMEOUT;
      LinDiagReqLength[LinBusIndex][PRIndex]           = Length;
      for (i = 0U; i < Length; i++)
      {
         LinDiagReqData[LinBusIndex][PRIndex][i] = TxData[i];
      }
   }
   else
   {
      // Error No of requests more than expected
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinTpMainfunction
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_CddLinRxHandling_ReceiveIndication(uint8 TxId, uint8 SubServiceId, uint8 *RxData, uint8 Length, CddLinTp_Status Status)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpMainfunction_doc
 *********************************************************************************************************************/

//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_LinTpMainfunction
//!
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpMainfunction(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpMainfunction
 *********************************************************************************************************************/
   uint8  LinBusIndex     = 0U;
   uint8  Index           = 0U;
   static uint8 FirstTime = 0U;

   //! ###### Processing  Cdd_LinTpMainfunction
   //! ##### Initialize transmit request buffers and flags
   //! #### At ECU startup (Only Onetime)
   if (0U == FirstTime)
   {
      FirstTime = 1U;
      for (LinBusIndex = 0U; LinBusIndex < MAX_NUMBER_OF_LINNetworks; LinBusIndex++)
      { 
         for (Index = 0U; Index < MAX_NUMBER_OF_REQPENDING; Index++) 
         {
            LINPendingReq[LinBusIndex][Index].ReqId = CddConf_NoTxPdu;
         }
      }
   }
   else
   {
      //! #### On completion of transmittion process
      if ((CDD_LINTP_NONE == LINPendingReq[0][0].PendingFlag)
         && (CDD_LINTP_NONE == LINPendingReq[1][0].PendingFlag)
         && (CDD_LINTP_NONE == LINPendingReq[2][0].PendingFlag)
         && (CDD_LINTP_NONE == LINPendingReq[3][0].PendingFlag)
         && (CDD_LINTP_NONE == LINPendingReq[4][0].PendingFlag)
         && (1U == LinTpStarted))
      {   
         LinTpStarted = 0U;
         for (LinBusIndex = 0U; LinBusIndex < MAX_NUMBER_OF_LINNetworks; LinBusIndex++)
         {            
            for (Index = 0U; Index < MAX_NUMBER_OF_REQPENDING; Index++)
            {
               if (LinDiagReqData[LinBusIndex][Index][0] != 0U)
               {
                  LinDiagReqData[LinBusIndex][Index][0] = 0U;
               }
               else
               {
                  // Do nothing
               }
            }
         }
      }
      else
      {
         // Do nothing
      }
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define Cdd_LinTpHandling_STOP_SEC_CODE
#include "Cdd_LinTpHandling_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_RxIndication
//!
//! \param   RxPduId      Providing the receive Pdu ID
//! \param   PduInfoPtr   Providing the receive Pdu Info pointer
//!
//!======================================================================================
FUNC(void, CDD_CODE) Cdd_RxIndication(PduIdType RxPduId, 
                                      P2CONST(PduInfoType, AUTOMATIC, CDD_COM_APPL_DATA) PduInfoPtr)
{
   //! ###### Processing Cdd_RxIndication
   Unused_Parameter(RxPduId);
   Unused_Parameter(PduInfoPtr);
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_TxConfirmation
//!
//! \param   TxPduId   Providing the transmit Pdu ID   
//!
//!======================================================================================
FUNC(void, CDD_CODE) Cdd_TxConfirmation(PduIdType TxPduId)
{
   //! ###### Processing Cdd_TxConfirmation
   Unused_Parameter(TxPduId);
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_Transmit
//!
//! \param   TxPduId      Providing the transmit Pdu ID
//! \param   PduInfoPtr   Providing the transmit Pdu Info pointer 
//!
//!======================================================================================
FUNC(Std_ReturnType, CDD_CODE) Cdd_Transmit(PduIdType TxPduId,
                                            P2CONST(PduInfoType, AUTOMATIC, CDD_LOWER_APPL_DATA) PduInfoPtr)
{
   //! ###### Processing Cdd_Transmit
   Unused_Parameter(TxPduId);
   Unused_Parameter(PduInfoPtr);
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_StartOfReception
//!
//! \param   id              Providing the receive Pdu ID
//! \param   info            Providing the receive Pdu Info
//! \param   TpSduLength     Providing the receive Tp SDu Length
//! \param   bufferSizePtr   Providing the receive data buffersize pointer 
//!
//!======================================================================================
FUNC(BufReq_ReturnType, CDD_CODE) Cdd_StartOfReception(PduIdType id,
                                                       P2VAR(PduInfoType, AUTOMATIC, CDD_APPL_DATA) info,
                                                       PduLengthType TpSduLength,
                                                       P2VAR(PduLengthType, AUTOMATIC, CDD_APPL_DATA) bufferSizePtr)
{
   //! ###### Processing Cdd_StartOfReception
   Unused_Parameter(id);
   Unused_Parameter(info);
   Unused_Parameter(TpSduLength);
   //! ##### send available buffer size
   *bufferSizePtr = 16U;
   return BUFREQ_OK;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_CopyRxData
//!
//! \param   id              Providing the receive Pdu ID
//! \param   info            Providing the receive Pdu Info
//! \param   bufferSizePtr   Providing the receive data buffersize pointer 
//!
//!======================================================================================
FUNC(BufReq_ReturnType, CDD_CODE) Cdd_CopyRxData(PduIdType id,
                                                 P2VAR(PduInfoType, AUTOMATIC, CDD_APPL_DATA) info,
                                                 P2VAR(PduLengthType, AUTOMATIC, CDD_APPL_DATA) bufferSizePtr)
{
   uint8 i             = 0U;
   uint8 Index         = 0U;
   uint8 LinBusIndex   = 0U;
   uint8 RxIDValidFlag = 0U;

   //! ###### Processing Cdd_CopyRxData
   //! ##### Check the receive message id is valid or not
   for (Index = 0U; Index < MAX_NUMBER_OF_LINTP_PDUs; Index++)
   {
      if (id == CddLinPduMap[Index].RxSrcPduId)
      {
         LinBusIndex   = CddLinPduMap[Index].LinBus;
         RxIDValidFlag = 1U;
         break;
      }
      else
      {
         // Do nothing
      } 
   }
   //! #### Copy the receive message data to corresponding Internal response buffer
   if (1U == RxIDValidFlag)
   {
      for (i = 0U; i < info->SduLength; i++)
      {
         LinDiagRespBuffer[LinBusIndex][i]= info->SduDataPtr[i];
      }
   }
   else
   {
      // Do nothing
   }
   *bufferSizePtr = 0U;
   return BUFREQ_OK;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_TpRxIndication
//!
//! \param   id       Providing the receive Pdu ID
//! \param   result   Providing the status of receive Lin frame    
//!
//!======================================================================================
FUNC(void, CDD_CODE) Cdd_TpRxIndication(PduIdType id, 
                                        Std_ReturnType result)
{
   static uint8 RxIndErr = 0U;
   uint8 LinBusIndex     = 0U;
   uint8 Index           = 0U;
   uint8 TxId            = 0U;
   uint8 RxIDValidFlag   = 0U;
   uint8 PRIndex         = 0U;

   //! ###### Processing Cdd_TpRxIndication
   Unused_Parameter(result);
   //! ##### Check the response (receive) message id is valid or not
   for (Index = 0U; Index < MAX_NUMBER_OF_LINTP_PDUs; Index++)
   {
      //! #### findout request (transmit) message id
      if (id == CddLinPduMap[Index].RxSrcPduId)
      {
         LinBusIndex   = CddLinPduMap[Index].LinBus;
         TxId          = CddLinPduMap[Index].TxSrcPduId;
         RxIDValidFlag = 1U;
         break;
      }
      else
      {
         // Do nothing
      }
   }
   if ((1U == RxIDValidFlag)
      && (CDD_LINTP_RX_PENDING == LINPendingReq[LinBusIndex][0].PendingFlag))
   {
      //! #### Provide the receive indication to requester with response data 
      Rte_Call_CddLinRxHandling_ReceiveIndication(TxId,
                                                  LinDiagReqData[LinBusIndex][0][1],
                                                  &LinDiagRespBuffer[LinBusIndex][0],
                                                  6,
                                                  CDD_LIN_RX_OK);
      //! #### Dequeue (remove) the present transmit request from internal Tx Queue
      for (PRIndex = 0U; PRIndex < MAX_NUMBER_OF_REQPENDING-1U; PRIndex++)
      {
         for (Index = 0U; Index < MAX_LINTP_REQ_LENGTH; Index++)
         {
            LinDiagReqData[LinBusIndex][PRIndex][Index] = LinDiagReqData[LinBusIndex][PRIndex+1][Index];
         }
      }
      PRIndex = 0U;
      while (PRIndex < MAX_NUMBER_OF_REQPENDING-1U)
      {
         if (CDD_LINTP_NONE != LINPendingReq[LinBusIndex][PRIndex+1U].PendingFlag)
         {
            LINPendingReq[LinBusIndex][PRIndex].ReqId       = LINPendingReq[LinBusIndex][PRIndex+1].ReqId;
            LINPendingReq[LinBusIndex][PRIndex].PendingFlag = LINPendingReq[LinBusIndex][PRIndex+1].PendingFlag;
            LINPendingReq[LinBusIndex][PRIndex].TimeoutVal  = LINPendingReq[LinBusIndex][PRIndex+1].TimeoutVal;
            PRIndex = PRIndex+1;
         }
         else
         {
            LINPendingReq[LinBusIndex][PRIndex].PendingFlag = CDD_LINTP_NONE;
            LINPendingReq[LinBusIndex][PRIndex].ReqId       = CddConf_NoTxPdu;
            LINPendingReq[LinBusIndex][PRIndex].TimeoutVal  = CDD_LINTP_TIMEOUT;
            break;
         }
      }
   }
   else
   {
      RxIndErr = 1U; // Do nothing
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_CopyTxData
//!
//! \param   id                  Providing the Transmit Pdu ID 
//! \param   info                Providing the Transmit Pdu Info
//! \param   retry               Providing the Transmit Pdu retry
//! \param   availableDataPtr    Providing the Transmit Pdu Data Pointer
//!
//!======================================================================================
FUNC(BufReq_ReturnType, CDD_CODE) Cdd_CopyTxData(PduIdType id, 
                                                 P2VAR(PduInfoType, AUTOMATIC, CDD_APPL_DATA) info,
                                                 P2VAR(RetryInfoType, AUTOMATIC, CDD_APPL_DATA) retry, 
                                                 P2VAR(PduLengthType, AUTOMATIC, CDD_APPL_DATA) availableDataPtr)
{
   uint8 Index       = 0U;
   uint8 LinBusIndex = 0U;

   //! ###### processing Cdd_CopyTxData
   Unused_Parameter(retry);
   Unused_Parameter(availableDataPtr);
   //! ##### Findout Lin bus information from Pdu ID
   LinBusIndex = CddLinPduMap[id].LinBus;
   //! ##### Copy corresponding TransmitId data from internal transmit request buffer to SDU data pointer (PduR buffer) 
   for (Index = 0U; Index < LinDiagReqLength[LinBusIndex][0]; Index++)
   {
     info->SduDataPtr[Index] = LinDiagReqData[LinBusIndex][0][Index];
   }
   return BUFREQ_OK;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_TpTxConfirmation
//! It is callback function, will trigger at the end of Lin message transmittion
//!
//! \param   id       Providing the transmit Pdu ID
//! \param   result   Providing the status of transmit Lin frame    
//!
//!======================================================================================
FUNC(void, CDD_CODE) Cdd_TpTxConfirmation(PduIdType id, 
                                          Std_ReturnType result)
{
#if 0
   uint8 LinBusIndex = 0U;
   LinBusIndex       = CddLinPduMap[id].LinBus;

   //! ###### processing of LinTP messages confirmation
   if (id == LINPendingReq[LinBusIndex][0].ReqId)
   { 
      //! ##### check the transmit opeation is successful or not
      //! #### Set the transmit status flag (TX_Error or TX_OK)
      if (E_OK == result)
      {
         LINPendingReq[LinBusIndex][0].PendingFlag = CDD_LINTP_TX_OK;
      }
      else
      {
         LINPendingReq[LinBusIndex][0].PendingFlag = CDD_LINTP_TX_ERROR;
      }
   }
   else
   {
      LINPendingReq[LinBusIndex][0].PendingFlag = CDD_LINTP_TX_ERROR;      
   }
#endif
}


//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Appl_FspAssign_LinIfGetLinStatus
//! This function is trigged by LinIf module with Lin Frame Id and status
//!
//! \param   Channel   Providing the information about Lin channel
//! \param   Pid       Providing the information about Lin Frame Identifier
//! \param   Status    Providing the status of Lin Frame
//!
//!======================================================================================
void Appl_FspAssign_LinIfGetLinStatus(NetworkHandleType Channel, 
                            Lin_FramePidType Pid,
                            Lin_StatusType Status)
{
   static Lin_StatusType LocalStatus[5];
   static uint8 ErrInd = 0U;
   uint8 LinBusIndex   = 0xFFU;
   uint8 PRIndex       = 0U;
   uint8 Index         = 0U;

   //! ###### Processing of  Appl_LinIfGetLinStatus
   //! ##### Check whether Lin frame id is MasterReq_ID, SlaveResp_ID or Anyother ID
   if ((LIN_MasterReq_PID == Pid)
      || (LIN_SlaveResp_PID == Pid))
   {
      //! #### Findout the Lin bus number
      switch (Channel)
      {
         case ComMConf_ComMChannel_CN_LIN00_2cd9a7df:
            LinBusIndex = 0U;
         break;
         case ComMConf_ComMChannel_CN_LIN01_5bde9749:
            LinBusIndex = 1U;
         break;
         case ComMConf_ComMChannel_CN_LIN02_c2d7c6f3:
            LinBusIndex = 2U;
         break;
         case ComMConf_ComMChannel_CN_LIN03_b5d0f665:
            LinBusIndex = 3U;
         break;
         case ComMConf_ComMChannel_CN_LIN04_2bb463c6:
            LinBusIndex = 4U;
         break;
         default:
            LinBusIndex = 0xFF;
         break;
      }
      if ((0xFF != LinBusIndex)
         && (0xFF != LINPendingReq[LinBusIndex][0].ReqId))
      {
         LocalStatus[LinBusIndex] = Status;
         //! #####  Master request message status processing
         if (LIN_MasterReq_PID == Pid) // Tx Processing
         {
            //! ##### Check whether any master request message triggered or not
            if ((CDD_LINTP_TX_OK == LINPendingReq[LinBusIndex][0].PendingFlag)
               || (CDD_LINTP_TX_ERROR == LINPendingReq[LinBusIndex][0].PendingFlag)
               || (CDD_LINTP_TX_TRIG == LINPendingReq[LinBusIndex][0].PendingFlag))
            {
               if (((LIN_TX_ERROR == LocalStatus[LinBusIndex])
                  || (LIN_TX_HEADER_ERROR == LocalStatus[LinBusIndex])
                  || (LIN_TX_BUSY == LocalStatus[LinBusIndex]))) /* Transmit Frame Error Reason: LIN BUS problem*/
               {
                  //! #### Frame status is error then provide the receive indication to requester with status information 
                  Rte_Call_CddLinRxHandling_ReceiveIndication(LINPendingReq[LinBusIndex][0].ReqId,
                                                              LinDiagReqData[LinBusIndex][0][1],
                                                              NULL_PTR,
                                                              0,
                                                              LocalStatus[LinBusIndex]);
                  //! #### Dequeue (remove) the present transmit request from internal Tx Queue
                  for (PRIndex = 0U; PRIndex < MAX_NUMBER_OF_REQPENDING-1; PRIndex++)
                  {
                     for (Index = 0U;Index < MAX_LINTP_REQ_LENGTH; Index++)
                     {
                        LinDiagReqData[LinBusIndex][PRIndex][Index] = LinDiagReqData[LinBusIndex][PRIndex+1][Index];
                     }
                  }
                  PRIndex = 0U;
                  while (PRIndex < MAX_NUMBER_OF_REQPENDING-1)
                  {
                     if (CDD_LINTP_NONE != LINPendingReq[LinBusIndex][PRIndex+1].PendingFlag)
                     {
                        LINPendingReq[LinBusIndex][PRIndex].ReqId       = LINPendingReq[LinBusIndex][PRIndex+1].ReqId;
                        LINPendingReq[LinBusIndex][PRIndex].PendingFlag = LINPendingReq[LinBusIndex][PRIndex+1].PendingFlag;
                        LINPendingReq[LinBusIndex][PRIndex].TimeoutVal  = LINPendingReq[LinBusIndex][PRIndex+1].TimeoutVal;
                        PRIndex                                         = PRIndex+1;
                     }
                     else
                     {
                        LINPendingReq[LinBusIndex][PRIndex].PendingFlag = CDD_LINTP_NONE;
                        LINPendingReq[LinBusIndex][PRIndex].ReqId       = CddConf_NoTxPdu;
                        LINPendingReq[LinBusIndex][PRIndex].TimeoutVal  = CDD_LINTP_TIMEOUT;
                        break;
                     }
                  }
               }
               else if (LIN_TX_OK == LocalStatus[LinBusIndex])
               {
                  //! #### Frame status is error then update the LinMessage status flag to Rx pending
                  LINPendingReq[LinBusIndex][0].PendingFlag = CDD_LINTP_RX_PENDING;
               }
               else
               {
                  // Do nothing
               }
            }
            else
            {
               // Do nothing
            }
         }
         //! ##### Slave response message status processing  
         else if (LIN_SlaveResp_PID==Pid)// Rx Processing
         {
            if (CDD_LINTP_RX_PENDING == LINPendingReq[LinBusIndex][0].PendingFlag)
            {
               if ((LIN_RX_NO_RESPONSE == LocalStatus[LinBusIndex])
                  || (LIN_RX_ERROR == LocalStatus[LinBusIndex])
                  || (LIN_RX_BUSY == LocalStatus[LinBusIndex])
                  || ((LIN_RX_OK == LocalStatus[LinBusIndex])
                  && (CddConf_NoRxPdu == CddLinPduMap[LINPendingReq[LinBusIndex][0].ReqId].RxSrcPduId))) /*Receive Frame Error or NO Response: Reasons : 1. LIN slave Problem or BUS problem*/
               {
                  //! #### Provide the receive indication to requester with status information if ResponseFrame is not required otherwise donot.
                  Rte_Call_CddLinRxHandling_ReceiveIndication(LINPendingReq[LinBusIndex][0].ReqId, 
                                                              LinDiagReqData[LinBusIndex][0][1], 
                                                              NULL_PTR,
                                                              0, 
                                                              LocalStatus[LinBusIndex]); 
                                                              
                  //! #### Dequeue (remove) the present transmit request from internal Tx Queue
                  for (PRIndex = 0U; PRIndex < MAX_NUMBER_OF_REQPENDING-1; PRIndex++)
                  {
                     for (Index = 0U; Index < MAX_LINTP_REQ_LENGTH; Index++)
                     {
                        LinDiagReqData[LinBusIndex][PRIndex][Index] = LinDiagReqData[LinBusIndex][PRIndex+1][Index];
                     }
                  }
                  PRIndex = 0U;
                  while (PRIndex < MAX_NUMBER_OF_REQPENDING-1U)
                  {
                     if (CDD_LINTP_NONE != LINPendingReq[LinBusIndex][PRIndex+1].PendingFlag)
                     {
                        LINPendingReq[LinBusIndex][PRIndex].ReqId       = LINPendingReq[LinBusIndex][PRIndex+1].ReqId;
                        LINPendingReq[LinBusIndex][PRIndex].PendingFlag = LINPendingReq[LinBusIndex][PRIndex+1].PendingFlag;
                        LINPendingReq[LinBusIndex][PRIndex].TimeoutVal  = LINPendingReq[LinBusIndex][PRIndex+1].TimeoutVal;
                        PRIndex                                         = PRIndex+1U;
                     }
                     else
                     {
                        LINPendingReq[LinBusIndex][PRIndex].PendingFlag = CDD_LINTP_NONE;
                        LINPendingReq[LinBusIndex][PRIndex].ReqId       = CddConf_NoTxPdu;
                        LINPendingReq[LinBusIndex][PRIndex].TimeoutVal  = CDD_LINTP_TIMEOUT;
                        break;
                     }
                  }
               }
               else
               {
                  // Do nothing ... LIN_RX_OK with Receive Datacase(PNSN)....
               }
            }
            else
            {
               // Do nothing impossible Case
            }
         }
         else
         {
            // Do nothing impossible Case
         }
      }
      else
      {
         // Do nothing
      }
   }
   else
   {
      // Do nothing
   }
}
//! @}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

