/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  Cdd_LinDiagnostics.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  Cdd_LinDiagnostics
 *  Generated at:  Thu Jul  9 10:51:19 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <Cdd_LinDiagnostics>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

//!======================================================================================
//! \file Cdd_LinDiagnostics.c
//!
//! @addtogroup ApplicationLayer 
//! @{
//! @addtogroup Application_VehicleControlAndActiveSafety 
//! @{
//! @addtogroup ActiveSafety 
//! @{
//! @addtogroup Cdd_LinDiagnostics
//! @{
//!
//! \brief
//! Cdd_LinDiagnostics SWC.
//! ASIL Level : QM.
//! This module implements the logic for the Cdd_LinDiagnostics runnables.
//!======================================================================================

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_LIN_topology_P1AJR_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_LIN_topology_P1AJR_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_Cdd_LinDiagnostics.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Cdd_Cbk.h"
#include "Cdd_LinDiagnostics.h"
#include "LinIf.h"

extern void Appl_ScimLinmgr_LinIfGetLinStatus(NetworkHandleType Channel,Lin_FramePidType Pid,Lin_StatusType Status);
extern void Appl_FspAssign_LinIfGetLinStatus(NetworkHandleType Channel,Lin_FramePidType Pid,Lin_StatusType Status);



// Global variables
static CddLinDiagMsgStatusType CddLinDiag_TxMsgStatus[MAX_NUMBER_OF_TXMsgSTATUS];

// Need to create IRV
static uint8 LinDiagNoOfSlavesPN                       = 0U;
static uint8 LinDiagNoOfSlavesSN                       = 0U;
static LinDiagServiceStatus LinDiag_PNSNRequestStatus  = LinDiagService_None;
static LinDiagServiceStatus LinDiag_CCNADRequestStatus = LinDiagService_None;
static uint8 LinDiagCCNADRequestStoppedFlag            = 0U;
static char LinDiagPartNumber[MAX_NUMBER_OF_SLAVENODES][PART_NUMBER_LENGTH];
static char LinDiagSerialNumber[MAX_NUMBER_OF_SLAVENODES][SERIAL_NUMBER_LENGTH];
static uint8 CddLinDiag_AvailableSlaves[CDD_LINDIAG_NoOfLINNetworks] = {0,0,0,0,0};
static uint8 CddLinDiag_SlavesErrStatus[CDD_LINDIAG_NoOfLINNetworks] = {0,0,0,0,0};
static uint8 CddLinDiag_FspNVData[CDD_LINDIAG_MaxLINNetworks][CDD_LINDIAG_MaxFSPs];
static uint8 CddLinDia_CurrentFspIndex[CDD_LINDIAG_MaxLINNetworks];

const static stCddLinFspPID_Type CddLinDiag_FspPID[CDD_LINDIAG_MaxFSPs]                  = {{0x42, 0xF0, 0xB4, 0xF5},
                                                                                            {0x03, 0xB1, 0xB4, 0xF5},
                                                                                            {0xC4, 0x32, 0xB4, 0x76},
                                                                                            {0x85, 0x73, 0xB4, 0x76}};
const static uint8  CddLinDiag_FspTxID[CDD_LINDIAG_NoOfLINNetworks][CDD_LINDIAG_MaxFSPs] = {{CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L1_oLIN00_2de78d2a_Tx,
                                                                                             CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L1_oLIN00_b405eb2b_Tx,
                                                                                             0xFFU,
                                                                                             0xFFU},
                                                                                            {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L2_oLIN01_d46fba5f_Tx,
                                                                                             CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L2_oLIN01_4d8ddc5e_Tx,
                                                                                             CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_L2_oLIN01_8c03039e_Tx,
                                                                                             CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP4_L2_oLIN01_a538161d_Tx},
                                                                                            {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L3_oLIN02_81cceb7b_Tx,
                                                                                             CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L3_oLIN02_182e8d7a_Tx,
                                                                                             0xFFU,
                                                                                             0xFFU},
                                                                                            {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L4_oLIN03_fc0ed2f4_Tx,
                                                                                             0xFFU,
                                                                                             0xFFU,
                                                                                             0xFFU},
                                                                                            {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L5_oLIN04_aec047c9_Tx,
                                                                                             0xFFU,
                                                                                             0xFFU,
                                                                                             0xFFU}};

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_LIN_topology_P1AJR_T: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * CddLinTp_Status: Enumeration of integer in interval [0...255] with enumerators
 *   CDD_LIN_NOT_OK (11U)
 *   CDD_LIN_TX_OK (0U)
 *   CDD_LIN_TX_BUSY (1U)
 *   CDD_LIN_TX_HEADER_ERROR (2U)
 *   CDD_LIN_TX_ERROR (3U)
 *   CDD_LIN_RX_OK (4U)
 *   CDD_LIN_RX_BUSY (5U)
 *   CDD_LIN_RX_ERROR (6U)
 *   CDD_LIN_RX_NO_RESPONSE (7U)
 *   CDD_LIN_NONE (9U)
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * LinDiagBusInfo: Enumeration of integer in interval [0...255] with enumerators
 *   None (0U)
 *   LinDiag_BUS1 (1U)
 *   LinDiag_BUS2 (2U)
 *   LinDiag_BUS3 (3U)
 *   LinDiag_BUS4 (4U)
 *   LinDiag_BUS5 (5U)
 *   LinDiag_SpareBUS1 (6U)
 *   LinDiag_SpareBUS2 (7U)
 *   LinDiag_ALL_BUSSES (8U)
 * LinDiagRequest_T: Enumeration of integer in interval [0...255] with enumerators
 *   NO_OPEARATION (0U)
 *   START_OPEARATION (1U)
 *   STOP_OPERATION (2U)
 * LinDiagServiceStatus: Enumeration of integer in interval [0...255] with enumerators
 *   LinDiagService_None (0U)
 *   LinDiagService_Pending (1U)
 *   LinDiagService_Completed (2U)
 *   LinDiagService_Error (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_LIN_topology_P1AJR_T Rte_Prm_P1AJR_LIN_topology_v(void)
 *
 *********************************************************************************************************************/


#define Cdd_LinDiagnostics_START_SEC_CODE
#include "Cdd_LinDiagnostics_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CddLinDiagServices_FSPAssignReq
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FSPAssignReq> of PortPrototype <CddLinDiagServices>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_LinDiagRequestFlag_CCNADRequest(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void CddLinDiagServices_FSPAssignReq(LinDiagBusInfo LinBusInfo, LinDiagRequest_T RequestType)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinDiagServices_FSPAssignReq_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinDiagnostics_CODE) CddLinDiagServices_FSPAssignReq(LinDiagBusInfo LinBusInfo, LinDiagRequest_T RequestType) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinDiagServices_FSPAssignReq
 *********************************************************************************************************************/
   uint8 Index    = 0U;
   uint8 FspIndex = 0U;

   //! ###### Processing CddLinDiagServices_FSPAssignReq 
   //! ##### Checking if RequestType is equal to START_OPEARATION
   if (START_OPEARATION == RequestType)
   {
      LinDiagCCNADRequestStoppedFlag = 0U;
      // LinBusInfo = LinDiag_ALL_BUSSES;
      //! #### Check the Lin Bus info and set the Fsp Assignment request state to Pending state
      if (0x1FU == (LinBusInfo & 0x1FU))
      {
         Rte_Write_LinDiagRequestFlag_CCNADRequest(0x1U);
         for (Index = 0U; Index < CDD_LINDIAG_MaxLINNetworks; Index++)
         {
            for (FspIndex = 0U; FspIndex<CDD_LINDIAG_MaxFSPs; FspIndex++)
            {
               CddLinDiag_FspNVData[Index][FspIndex] = 0U;
            }
            CddLinDia_CurrentFspIndex[Index] = 0U;
         }
         LinDiag_CCNADRequestStatus = LinDiagService_Pending;
      }
      else
      {
         //! #### If Lin bus info is out of range then set the FSP assignment request state to completed state  
         (void)memset(&CddLinDiag_AvailableSlaves[0U],(uint8)0U,CDD_LINDIAG_NoOfLINNetworks);
         (void)memset(&CddLinDiag_SlavesErrStatus[0U],(uint8)0U,CDD_LINDIAG_NoOfLINNetworks);
         (void)memset(&CddLinDiag_FspNVData[0U],(uint8)0U,CDD_LINDIAG_MaxFSPs*CDD_LINDIAG_MaxLINNetworks);
         LinDiag_CCNADRequestStatus = LinDiagService_Completed;
      }
   }
   //! ##### Checking if RequestType is equal to STOP_OPERATION
   else if (STOP_OPERATION == RequestType)
   {
      //! #### set the FSP assignment request state to stop state
      LinDiagCCNADRequestStoppedFlag = 1U; //IRV
   }
   else
   {
      //! ##### Checking if RequestType is out of range then set the FSP assignment request state to completed state
      (void)memset(&CddLinDiag_AvailableSlaves[0U],(uint8)0U,CDD_LINDIAG_NoOfLINNetworks);
      (void)memset(&CddLinDiag_SlavesErrStatus[0U],(uint8)0U,CDD_LINDIAG_NoOfLINNetworks);
      (void)memset(&CddLinDiag_FspNVData[0U],(uint8)0U,CDD_LINDIAG_MaxFSPs*CDD_LINDIAG_MaxLINNetworks);
      LinDiag_CCNADRequestStatus = LinDiagService_Completed;
   }

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CddLinDiagServices_FSPAssignResp
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FSPAssignResp> of PortPrototype <CddLinDiagServices>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void CddLinDiagServices_FSPAssignResp(LinDiagServiceStatus *pDiagServiceStatus, uint8 *pAvailableFSPCount, uint8 *pFspErrorStatus, uint8 *pFspNvData)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinDiagServices_FSPAssignResp_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinDiagnostics_CODE) CddLinDiagServices_FSPAssignResp(P2VAR(LinDiagServiceStatus, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) pDiagServiceStatus, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) pAvailableFSPCount, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) pFspErrorStatus, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) pFspNvData) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinDiagServices_FSPAssignResp
 *********************************************************************************************************************/
   //! ###### Processing CddLinDiagServices_FSPAssignResp
   //! ##### Check the FSP Assignment request state
   if (LinDiagService_Pending == LinDiag_CCNADRequestStatus)
   {
      //! #### If it is in Pending state then provide Pending status
      *pDiagServiceStatus = LinDiagService_Pending;
   }
   else if (LinDiagService_Completed == LinDiag_CCNADRequestStatus)
   {
      //! #### If it is in completed state then provide available Lin slaves and completed status
      (void)memcpy(&pAvailableFSPCount[0U],
                   &CddLinDiag_AvailableSlaves[0U],
                   CDD_LINDIAG_NoOfLINNetworks);
      (void)memcpy(&pFspErrorStatus[0U],
                   &CddLinDiag_SlavesErrStatus[0U],
                   CDD_LINDIAG_NoOfLINNetworks);
      (void)memcpy(&pFspNvData[0U],
                   &CddLinDiag_FspNVData[0U],
                   CDD_LINDIAG_MaxFSPs*CDD_LINDIAG_MaxLINNetworks);
      *pDiagServiceStatus = LinDiagService_Completed;
   }
   else
   {
      //! #### if it is in any other state then Provide the Error status 
      *pDiagServiceStatus = LinDiagService_Error;
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CddLinDiagServices_SlaveNodePnSnReq
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SlaveNodePnSnReq> of PortPrototype <CddLinDiagServices>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_LinDiagRequestFlag_PNSNRequest(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void CddLinDiagServices_SlaveNodePnSnReq(LinDiagBusInfo LinBusInfo)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinDiagServices_SlaveNodePnSnReq_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinDiagnostics_CODE) CddLinDiagServices_SlaveNodePnSnReq(LinDiagBusInfo LinBusInfo) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinDiagServices_SlaveNodePnSnReq
 *********************************************************************************************************************/
   //! ###### Processing CddLinDiagServices_SlaveNodePnSnReq
   //! ##### Check the Lin bus info is with in range or not
   if (LinDiag_ALL_BUSSES == LinBusInfo)
   {
      //! #### Set the Lin slave PartNumber and serial number request state to pending state
      LinDiag_PNSNRequestStatus = LinDiagService_Pending;
   }
   else
   {
      //Do nothing
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CddLinDiagServices_SlaveNodePnSnResp
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SlaveNodePnSnResp> of PortPrototype <CddLinDiagServices>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void CddLinDiagServices_SlaveNodePnSnResp(LinDiagServiceStatus *DiagServiceStatus, uint8 *NoOfLinSlaves, uint8 *LinDiagRespPNSN)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinDiagServices_SlaveNodePnSnResp_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinDiagnostics_CODE) CddLinDiagServices_SlaveNodePnSnResp(P2VAR(LinDiagServiceStatus, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) DiagServiceStatus, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) NoOfLinSlaves, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) LinDiagRespPNSN) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinDiagServices_SlaveNodePnSnResp
 *********************************************************************************************************************/
   uint8 Index            = 0U;
   uint16 NextIndexLength = 0U;

   //! ###### Processing CddLinDiagServices_SlaveNodePnSnResp
   //! ##### Check the Lin slave PartNumber and serial number request state
   if (LinDiagService_Pending == LinDiag_PNSNRequestStatus)
   {
      //! #### if it is in pending state then provide pending status
      *DiagServiceStatus = LinDiagService_Pending;
   }
   else if (LinDiagService_Completed == LinDiag_PNSNRequestStatus)
   {
      //! #### if it is in completed state then provide the No.of Lin slaves, part & serial number with completed status
      if (LinDiagNoOfSlavesPN > LinDiagNoOfSlavesSN)
      {
         *NoOfLinSlaves = LinDiagNoOfSlavesSN;
      }
      else if (LinDiagNoOfSlavesPN < LinDiagNoOfSlavesSN)
      {
         *NoOfLinSlaves = LinDiagNoOfSlavesPN;
      }
      else
      {
         *NoOfLinSlaves = LinDiagNoOfSlavesSN;
      }
      for (Index = 0U; Index < (*NoOfLinSlaves); Index++)
      {
         (void)memcpy(&LinDiagRespPNSN[NextIndexLength],
                      &LinDiagPartNumber[Index][0U],
                      PART_NUMBER_LENGTH);
         NextIndexLength = NextIndexLength + PART_NUMBER_LENGTH;
         (void)memcpy(&LinDiagRespPNSN[NextIndexLength],
                      &LinDiagSerialNumber[Index][0U],
                      SERIAL_NUMBER_LENGTH);
         NextIndexLength = NextIndexLength+SERIAL_NUMBER_LENGTH;
      }
      LinDiagNoOfSlavesPN = 0U;
      LinDiagNoOfSlavesSN = 0U;
      *DiagServiceStatus  = LinDiagService_Completed;
   }
   else
   {
      //! #### if it is in any other state then Provide Error status
      LinDiagNoOfSlavesPN = 0U;
      LinDiagNoOfSlavesSN = 0U;
      *NoOfLinSlaves      = 0U;
      *DiagServiceStatus  = LinDiagService_Error;
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CddLinRxHandling_ReceiveIndication
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReceiveIndication> of PortPrototype <CddLinRxHandling>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void CddLinRxHandling_ReceiveIndication(uint8 TxId, uint8 SubServiceId, uint8 *RxData, uint8 Length, CddLinTp_Status Status)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinRxHandling_ReceiveIndication_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinDiagnostics_CODE) CddLinRxHandling_ReceiveIndication(uint8 TxId, uint8 SubServiceId, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) RxData, uint8 Length, CddLinTp_Status Status) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinRxHandling_ReceiveIndication
 *********************************************************************************************************************/
   uint8 Index = 0U;

   //! ###### Processing CddLinRxHandling_ReceiveIndication
   for (Index = 0U; Index < MAX_NUMBER_OF_TXMsgSTATUS; Index++)
   {
      //! ##### Check receive indication status
      if ((TxId == CddLinDiag_TxMsgStatus[Index].TxId) 
         && (CddLinDiag_TxMsgStatus[Index].Status == LinDiagService_Pending))
      {
         CddLinDiag_TxMsgStatus[Index].ReasonForError = Status;
         if (CDD_LIN_RX_OK == Status)
         {
            //! #### If it is RX_OK then read the Rxdata to buffer and update corresponding Tx message status to completed
            CddLinDiag_TxMsgStatus[Index].Status       = LinDiagService_Completed;
            CddLinDiag_TxMsgStatus[Index].SubServiceId = SubServiceId;
            CddLinDiag_TxMsgStatus[Index].Length       = Length;
            (void)memcpy(&CddLinDiag_TxMsgStatus[Index].RxData[0U],
                         &RxData[0U],
                         Length);
         }
         else
         {
            //! #### Else corresponding Tx message status to Error
            CddLinDiag_TxMsgStatus[Index].Status = LinDiagService_Error;
         }
         break;
      }
      else
      {
         // Do nothing
      }
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinDiagnostics_10ms_Runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN2_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN3_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN5_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DiagActiveState_P_isDiagActive(DiagActiveState_T *data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_CddLinClearRequest_ClearRequest(void)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_CddLinTxHandling_Transmit(uint8 TxId, uint8 *TxData, uint8 Length)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinDiagnostics_10ms_Runnable_doc
 *********************************************************************************************************************/
 
//!====================================================================================================================
//! 
//! \brief
//! This function implements the cyclic execution runnable logic for the Cdd_LinDiagnostics_10ms_Runnable
//!
//!====================================================================================================================


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinDiagnostics_CODE) Cdd_LinDiagnostics_10ms_Runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinDiagnostics_10ms_Runnable
 *********************************************************************************************************************/
   // Local logic Variables 
   static uint8 PendingTimeout = ResponsePendingTimeoutValue; 
   static uint8 SM_CddDiag10ms = CDD_DIAG_INIT;
   static CDDLinDiagStoredbyteType CddLinDiag_TxStoredData[CDD_LINDIAG_NoOfLINNetworks];
   static CDDLinDiagByteMaskType   CddLinDiag_TxDataInfo[CDD_LINDIAG_NoOfLINNetworks] = {{1,1,1,1,0x41,0,2},
                                                                                         {1,1,1,1,0x41,0,4},
                                                                                         {1,1,1,1,0x41,0,2},
                                                                                         {1,1,1,1,0x41,0,1},
                                                                                         {1,1,1,1,0x41,0,1}};
   static ENUM_CDD_LINDIAG LinDiag_CCNAD_TokenPhase[CDD_LINDIAG_NoOfLINNetworks] = {CONST_CDD_LINDIAG_NO_TOKEN_PHASE,
                                                                                    CONST_CDD_LINDIAG_NO_TOKEN_PHASE,
                                                                                    CONST_CDD_LINDIAG_NO_TOKEN_PHASE,
                                                                                    CONST_CDD_LINDIAG_NO_TOKEN_PHASE,
                                                                                    CONST_CDD_LINDIAG_NO_TOKEN_PHASE};
                                                                                    
   static uint8 LinDiag_NADTxId_LIN0[CDD_LINDIAG_NoOfNADTxId_LIN0] = {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_40_oLIN00_aa36ec79_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_41_oLIN00_669cece7_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_42_oLIN00_e813eb04_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_43_oLIN00_24b9eb9a_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_44_oLIN00_2e7ce283_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_45_oLIN00_e2d6e21d_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_46_oLIN00_6c59e5fe_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_47_oLIN00_a0f3e560_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_48_oLIN00_79d3f7cc_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_49_oLIN00_b579f752_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4A_oLIN00_cacc7d77_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4B_oLIN00_44437a94_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4C_oLIN00_88e97a0a_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4D_oLIN00_822c7313_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4E_oLIN00_4e86738d_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4F_oLIN00_c009746e_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_50_oLIN00_bd4df83a_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_51_oLIN00_71e7f8a4_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_52_oLIN00_ff68ff47_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_53_oLIN00_33c2ffd9_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_54_oLIN00_3907f6c0_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_55_oLIN00_f5adf65e_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_56_oLIN00_7b22f1bd_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_57_oLIN00_b788f123_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_58_oLIN00_6ea8e38f_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_59_oLIN00_a202e311_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5A_oLIN00_ddb76934_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5B_oLIN00_53386ed7_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5C_oLIN00_9f926e49_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5D_oLIN00_95576750_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5E_oLIN00_59fd67ce_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5F_oLIN00_d772602d_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_60_oLIN00_84c0c4ff_Tx};

   static uint8 LinDiag_NADTxId_LIN1[CDD_LINDIAG_NoOfNADTxId_LIN1] = {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_40_oLIN01_44d3baee_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_41_oLIN01_8879ba70_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_42_oLIN01_06f6bd93_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_43_oLIN01_ca5cbd0d_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_44_oLIN01_c099b414_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_45_oLIN01_0c33b48a_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_46_oLIN01_82bcb369_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_47_oLIN01_4e16b3f7_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_48_oLIN01_9736a15b_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_49_oLIN01_5b9ca1c5_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4A_oLIN01_24292be0_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4B_oLIN01_aaa62c03_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4C_oLIN01_660c2c9d_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4D_oLIN01_6cc92584_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4E_oLIN01_a063251a_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4F_oLIN01_2eec22f9_Tx, 
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_50_oLIN01_53a8aead_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_51_oLIN01_9f02ae33_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_52_oLIN01_118da9d0_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_53_oLIN01_dd27a94e_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_54_oLIN01_d7e2a057_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_55_oLIN01_1b48a0c9_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_56_oLIN01_95c7a72a_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_57_oLIN01_596da7b4_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_58_oLIN01_804db518_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_59_oLIN01_4ce7b586_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5A_oLIN01_33523fa3_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5B_oLIN01_bddd3840_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5C_oLIN01_717738de_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5D_oLIN01_7bb231c7_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5E_oLIN01_b7183159_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5F_oLIN01_399736ba_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_60_oLIN01_6a259268_Tx};

   static uint8 LinDiag_NADTxId_LIN2[CDD_LINDIAG_NoOfNADTxId_LIN2] = {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_40_oLIN02_1c543494_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_41_oLIN02_d0fe340a_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_42_oLIN02_5e7133e9_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_43_oLIN02_92db3377_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_44_oLIN02_981e3a6e_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_45_oLIN02_54b43af0_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_46_oLIN02_da3b3d13_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_47_oLIN02_16913d8d_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_48_oLIN02_cfb12f21_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_49_oLIN02_031b2fbf_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4A_oLIN02_7caea59a_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4B_oLIN02_f221a279_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4C_oLIN02_3e8ba2e7_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4D_oLIN02_344eabfe_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4E_oLIN02_f8e4ab60_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4F_oLIN02_766bac83_Tx,                                                                 
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_50_oLIN02_0b2f20d7_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_51_oLIN02_c7852049_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_52_oLIN02_490a27aa_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_53_oLIN02_85a02734_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_54_oLIN02_8f652e2d_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_55_oLIN02_43cf2eb3_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_56_oLIN02_cd402950_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_57_oLIN02_01ea29ce_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_58_oLIN02_d8ca3b62_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_59_oLIN02_14603bfc_Tx,
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5A_oLIN02_6bd5b1d9_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5B_oLIN02_e55ab63a_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5C_oLIN02_29f0b6a4_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5D_oLIN02_2335bfbd_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5E_oLIN02_ef9fbf23_Tx,  
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5F_oLIN02_6110b8c0_Tx,                                                                 
                                                                      CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_60_oLIN02_32a21c12_Tx};

   static uint8 LinDiag_NADTxId_LIN3[CDD_LINDIAG_NoOfNADTxId_LIN3] = {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP4_40_oLIN03_42681181_Tx};
   static uint8 LinDiag_NADTxId_LIN4[CDD_LINDIAG_NoOfNADTxId_LIN4 ]= {CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP5_40_oLIN04_1d825be2_Tx};
   static CddLinDiagNADTxID_Type LinDiag_NADTxId[CDD_LINDIAG_NoOfLINNetworks] = {{CDD_LINDIAG_NoOfNADTxId_LIN0,0,&LinDiag_NADTxId_LIN0[0]},
                                                                              {CDD_LINDIAG_NoOfNADTxId_LIN1,0,&LinDiag_NADTxId_LIN1[0]},
                                                                              {CDD_LINDIAG_NoOfNADTxId_LIN2,0,&LinDiag_NADTxId_LIN2[0]},
                                                                              {CDD_LINDIAG_NoOfNADTxId_LIN3,0,&LinDiag_NADTxId_LIN3[0]},
                                                                              {CDD_LINDIAG_NoOfNADTxId_LIN4,0,&LinDiag_NADTxId_LIN4[0]}};
   // Define input RteInData Common
   static Cdd_LinDiagnostics_In_struct_Type  RteDataRead_Common;
   uint8 LinBusIndex           = 0xFFU;
   uint8 Index                 = 0U;
   uint8 IsStillProcessingFlag = 0U;
   //! ###### Processing Cdd_LinDiagnostics_10ms_Runnable
   //! ##### Read RTE indata common logic: 'Get_RteDataRead_Common()'
   Get_RteDataRead_Common(&RteDataRead_Common);

   // Check the Tester Present or not
   //! ###### On change of Tester Disconnet then reset the statemachine to Idle and variable to default values

   if((RteDataRead_Common.DiagActive.New!=RteDataRead_Common.DiagActive.Prev)
   	&&(Diag_Active_FALSE==RteDataRead_Common.DiagActive.New))
   {
        if(LinDiagService_Pending==LinDiag_CCNADRequestStatus)
        {
			// Reinitialize the Data
			for (Index = 0U; Index < CDD_LINDIAG_NoOfLINNetworks; Index++)
			{
			CddLinDiag_TxDataInfo[Index].FinalNAD	   = 0U;
			CddLinDiag_TxDataInfo[Index].Byte		   = 1U;
			CddLinDiag_TxDataInfo[Index].LastRsdByte   = 1U;
			CddLinDiag_TxDataInfo[Index].LastRsdMask   = 1U;
			CddLinDiag_TxDataInfo[Index].Mask		   = 1U;
			LinDiag_NADTxId[Index].CurrentIdIndex	   = 0U;
			LinDiag_CCNAD_TokenPhase[Index] 		   = CONST_CDD_LINDIAG_NO_TOKEN_PHASE;
			CddLinDiag_TxStoredData[Index].StoredIndex = 0U;
			}
			Rte_Write_LinDiagRequestFlag_CCNADRequest(0x0);
			LinDiag_CCNADRequestStatus = LinDiagService_Error; // LinDiagService_Error or LinDiagService_Completed
		}
		else
		{
          // Do nothing
		}
		

	    if(LinDiagService_Pending==LinDiag_PNSNRequestStatus)
	    {
			Rte_Write_LinDiagRequestFlag_PNSNRequest(0x0); 
			LinDiag_PNSNRequestStatus = LinDiagService_Error;
		}
		else
		{
          // Do nothing
		}

		CddLinDiag_TxMsgStatusClear();		
		Rte_Call_CddLinClearRequest_ClearRequest();
		SM_CddDiag10ms = CDD_DIAG_IDLE;
   }

   
   // State Machine
   //! ###### Processing of CDD Diagnostic StateMachine
   
   switch (SM_CddDiag10ms)
   {
      case CDD_DIAG_PNSN_REQ:
         //! ##### 'PartNumber and SerialNumber read request' state
         if ((Diagnostic == RteDataRead_Common.ComMode_LIN1)
            && (Diagnostic == RteDataRead_Common.ComMode_LIN2)
            && (Diagnostic == RteDataRead_Common.ComMode_LIN3)
            && (Diagnostic == RteDataRead_Common.ComMode_LIN4)
            && (Diagnostic == RteDataRead_Common.ComMode_LIN5))
         {
            //! #### Check whether Lin Communication mode is Diagnostic Mode and Process the Cdd_LinDiagnostics_SlaveNodePN logic
            PendingTimeout = ResponsePendingTimeoutValue;
            Cdd_LinDiagnostics_SlaveNodePN(); 
            SM_CddDiag10ms = CDD_DIAG_PNSN_PENDING;
         }
         else
         {
            //! #### Else wait for pending time, after that trigger the PNSN requested completed and change state to Idle
            PendingTimeout--;
            if (0U == PendingTimeout)
            {
               Rte_Write_LinDiagRequestFlag_PNSNRequest(0U);
               SM_CddDiag10ms = CDD_DIAG_IDLE;
               PendingTimeout = ResponsePendingTimeoutValue;
					LinDiag_PNSNRequestStatus = LinDiagService_Error;

            }
            else
            {
               // Do nothing
            }
         }
      break;
      case CDD_DIAG_PNSN_PENDING:
         //! ##### 'PartNumber and Serial Number read process pending' state
         //! #### Check whether Lin Communication mode still in Diagnostic Mode
         if ((Diagnostic == RteDataRead_Common.ComMode_LIN1)
            && (Diagnostic == RteDataRead_Common.ComMode_LIN2)
            && (Diagnostic == RteDataRead_Common.ComMode_LIN3)
            && (Diagnostic == RteDataRead_Common.ComMode_LIN4)
            && (Diagnostic == RteDataRead_Common.ComMode_LIN5))
         {
            //! #### Check whether transmittion process completed and process the Part & Serial numbers
            //! #### Change the state to completed state
            IsStillProcessingFlag = 0U;
			
            for (Index = 0U; Index < MAX_NUMBER_OF_TXMsgSTATUS; Index++)
            {
               if (CddLinDiag_TxMsgStatus[Index].Status == LinDiagService_Pending)
               {
                  IsStillProcessingFlag = 1U;
                  break;
               }
               else
               {
                  // Do nothing
               }
            }

			
            if (0U == IsStillProcessingFlag) 
            {
               for (Index = 0U;Index < MAX_NUMBER_OF_TXMsgSTATUS;Index++)
               {
                  if (LinDiagService_Completed == CddLinDiag_TxMsgStatus[Index].Status)
                  {
                     if (LINSlaveNode_ReadDataByID_ResponseServiceID == CddLinDiag_TxMsgStatus[Index].RxData[0])
                     {
                        if (LINSlaveNode_SERIAL_NUMBER_SubServiceID == CddLinDiag_TxMsgStatus[Index].SubServiceId)
                        {
                           RawFormatToSerialNumberFormatConversion(CddLinDiag_TxMsgStatus[Index].TxId,
                                                                   &CddLinDiag_TxMsgStatus[Index].RxData[1],
                                                                   CddLinDiag_TxMsgStatus[Index].Length-1);
                        }
                        else if (LINSlaveNode_PART_NUMBER_SubServiceID == CddLinDiag_TxMsgStatus[Index].SubServiceId)
                        {
                           RawFormatToPartNumberFormatConversion(CddLinDiag_TxMsgStatus[Index].TxId,
                                                                 &CddLinDiag_TxMsgStatus[Index].RxData[1],
                                                                 CddLinDiag_TxMsgStatus[Index].Length-1);
                        }
                        else
                        {
                           // Do nothing
                        }
                     }
                     else
                     {
                        // Do nothing
                     }
                  }
                  else
                  {
                     // Do nothing
                  }
               }
               Rte_Write_LinDiagRequestFlag_PNSNRequest(0x0);
               CddLinDiag_TxMsgStatusClear();
               LinDiag_PNSNRequestStatus = LinDiagService_Completed;
               SM_CddDiag10ms            = CDD_DIAG_IDLE;
            }
            else
            {
               // Do nothing
            }
         }
         else
         {
            //! #### If Lin communication mode changes to other than Diagnostic
            //! #### Then clear transmit requests and change state to idle
            CddLinDiag_TxMsgStatusClear();
            Rte_Call_CddLinClearRequest_ClearRequest();
            Rte_Write_LinDiagRequestFlag_PNSNRequest(0x0); 
            LinDiag_PNSNRequestStatus = LinDiagService_Error;
            SM_CddDiag10ms            = CDD_DIAG_IDLE;
         }
      break; 
      case CDD_DIAG_CCNAD_REQ:
         //! ##### 'CCNAD Request' state
         //! #### Check Whether CCNAD operation stop requested then clear transmit requests and change state to Idle
         if (1U == LinDiagCCNADRequestStoppedFlag)
         {
            Rte_Write_LinDiagRequestFlag_CCNADRequest(0x0);
            LinDiag_CCNADRequestStatus = LinDiagService_Completed; // LinDiagService_Error or LinDiagService_Completed
            Rte_Call_CddLinClearRequest_ClearRequest();
            SM_CddDiag10ms             = CDD_DIAG_IDLE;
         }
         //! #### Check whether Lin Communication mode still in Diagnostic Mode
         else if ((Calibration == RteDataRead_Common.ComMode_LIN1)
                 && (Calibration == RteDataRead_Common.ComMode_LIN2)
                 && (Calibration == RteDataRead_Common.ComMode_LIN3)
                 && (Calibration == RteDataRead_Common.ComMode_LIN4)
                 && (Calibration == RteDataRead_Common.ComMode_LIN5))
         {
            //! #### then trigger the CCNAD request Cdd_LinDiagnostics_CCNAD_SetInitialNAD and change the state to CCNAD process Pending state
            PendingTimeout = ResponsePendingTimeoutValue;
            for (Index = 0U;Index < CDD_LINDIAG_NoOfLINNetworks; Index++)
            {
              CddLinDiag_AvailableSlaves[Index] = 0U;
            }
            Cdd_LinDiagnostics_CCNAD_SetInitialNAD(LinDiag_CCNAD_TokenPhase); 
            SM_CddDiag10ms = CDD_DIAG_CCNAD_PENDING;
         }
         else
         {
            //! #### Else wait for pending time, after that Trigger the CCNAD requested completed and change state to Idle
            PendingTimeout--;
            if (0U == PendingTimeout)
            {
               Rte_Write_LinDiagRequestFlag_CCNADRequest(0x0);
               LinDiag_CCNADRequestStatus = LinDiagService_Completed; // LinDiagService_Error or LinDiagService_Completed
               Rte_Call_CddLinClearRequest_ClearRequest();
               SM_CddDiag10ms             = CDD_DIAG_IDLE;
               PendingTimeout             = ResponsePendingTimeoutValue;
            }
            else
            {
               // Do nothing
            }
         }
      break; 
      case CDD_DIAG_CCNAD_PENDING:
         //! ##### 'CCNAD Process Pending' state
         //! #### Check Whether CCNAD operation stop requested then clear Transmit Requests and change state to Idle
         if (1U == LinDiagCCNADRequestStoppedFlag)
         {
            Rte_Write_LinDiagRequestFlag_CCNADRequest(0x0);
            LinDiag_CCNADRequestStatus = LinDiagService_Completed; // LinDiagService_Error or LinDiagService_Completed
            CddLinDiag_TxMsgStatusClear();
            // Reinitialize the Data
            for (Index = 0U; Index < CDD_LINDIAG_NoOfLINNetworks; Index++)
            {
               CddLinDiag_TxDataInfo[Index].FinalNAD      = 0U;
               CddLinDiag_TxDataInfo[Index].Byte          = 1U;
               CddLinDiag_TxDataInfo[Index].LastRsdByte   = 1U;
               CddLinDiag_TxDataInfo[Index].LastRsdMask   = 1U;
               CddLinDiag_TxDataInfo[Index].Mask          = 1U;
               LinDiag_NADTxId[Index].CurrentIdIndex      = 0U;
               LinDiag_CCNAD_TokenPhase[Index]            = CONST_CDD_LINDIAG_NO_TOKEN_PHASE;
               CddLinDiag_TxStoredData[Index].StoredIndex = 0U;
            }
            Rte_Call_CddLinClearRequest_ClearRequest();
            SM_CddDiag10ms = CDD_DIAG_IDLE;
         }
         //! #### Check whether Lin Communication mode still in Diagnostic Mode
         else if ((Calibration == RteDataRead_Common.ComMode_LIN1)
                 && (Calibration == RteDataRead_Common.ComMode_LIN2)
                 && (Calibration == RteDataRead_Common.ComMode_LIN3)
                 && (Calibration == RteDataRead_Common.ComMode_LIN4)
                 && (Calibration == RteDataRead_Common.ComMode_LIN5))
         {
            // Tester Tool Disconnected Condition
            //! #### Process the CCNAD Opearation 
            for (Index = 0U; Index < MAX_NUMBER_OF_TXMsgSTATUS; Index++)
            {
               if ((LinDiagService_Pending != CddLinDiag_TxMsgStatus[Index].Status)
                  && (LinDiagService_None != CddLinDiag_TxMsgStatus[Index].Status))
               {
                  LinBusIndex = CddLinDiag_TxIdBusIndex(CddLinDiag_TxMsgStatus[Index].TxId,
                                                        LinDiag_NADTxId);
                  if (0xFF != LinBusIndex)
                  {
                     switch (LinDiag_CCNAD_TokenPhase[LinBusIndex])
                     {
                        case CONST_CDD_LINDIAG_INITIAL_TOKEN_PHASE:
                           Cdd_LinDiagnostics_StartCCNAD(LinBusIndex,
                                                         Index,
                                                         LinDiag_CCNAD_TokenPhase,
                                                         CddLinDiag_TxDataInfo,
                                                         LinDiag_NADTxId);
                        break;
                        case CONST_CDD_LINDIAG_FORWARD_TOKEN_PHASE:
                           Cdd_LinDiagnostics_ForwardCCNADProcess(LinBusIndex,
                                                                  Index,
                                                                  LinDiag_CCNAD_TokenPhase,
                                                                  CddLinDiag_TxDataInfo,
                                                                  LinDiag_NADTxId,
                                                                  CddLinDiag_TxStoredData);
                        break;
                        case CONST_CDD_LINDIAG_BACKWARD_TOKEN_PHASE:
                           Cdd_LinDiagnostics_BackwardCCNADProcess(LinBusIndex,
                                                                   Index,
                                                                   LinDiag_CCNAD_TokenPhase,
                                                                   CddLinDiag_TxDataInfo,
                                                                   LinDiag_NADTxId,
                                                                   CddLinDiag_TxStoredData);
                        break;
                        case CONST_CDD_LINDIAG_CONSITENCY_TOKEN_PHASE:
                           Cdd_LinDiagnostics_CCNADConsistencyCheck(LinBusIndex,
                                                                    Index,
                                                                    LinDiag_CCNAD_TokenPhase,
                                                                    LinDiag_NADTxId,
                                                                    CddLinDiag_TxDataInfo);
                        break;
                        case CONST_CDD_LINDIAG_FINAL_TOKEN_PHASE:
                           Cdd_LinDiagnostics_FinalCCNAD(LinBusIndex,
                                                         Index,
                                                         LinDiag_CCNAD_TokenPhase,
                                                         CddLinDiag_TxDataInfo,
                                                         LinDiag_NADTxId);
                        break;
                        case CONST_CDD_LINDIAG_COMPLETED_TOKEN_PHASE:
                           // Do nothing
                        break;
                        default:
                           //Do nothing
                        break;
                     }
                  }
                  else
                  {
                     // Do nothing
                  }
               }
               else
               {
                  // Do nothing
               }
            }
            IsStillProcessingFlag = 0U;
            for (Index = 0U; Index < CDD_LINDIAG_NoOfLINNetworks; Index++)
            {
               if (CONST_CDD_LINDIAG_COMPLETED_TOKEN_PHASE != LinDiag_CCNAD_TokenPhase[Index])
               {
                  IsStillProcessingFlag = 1U;
                  break;
               }
               else
               {
                  // Do nothing
               }
            }
            if (0U == IsStillProcessingFlag)
            {
               //! #### Change the state to Frame Id Assignment state After successfully completion of CCNAD Opearation
               // Store Global Data about No Of Slaves on Each Bus Network
               // Reinitialize the Data
               for (Index = 0U; Index < CDD_LINDIAG_NoOfLINNetworks; Index++)
               {
                  CddLinDiag_TxDataInfo[Index].FinalNAD      = 0U;
                  CddLinDiag_TxDataInfo[Index].Byte          = 1U;
                  CddLinDiag_TxDataInfo[Index].LastRsdByte   = 1U;
                  CddLinDiag_TxDataInfo[Index].LastRsdMask   = 1U;
                  CddLinDiag_TxDataInfo[Index].Mask          = 1U;
                  LinDiag_NADTxId[Index].CurrentIdIndex      = 0U;
                  LinDiag_CCNAD_TokenPhase[Index]            = CONST_CDD_LINDIAG_NO_TOKEN_PHASE;
                  CddLinDiag_TxStoredData[Index].StoredIndex = 0U;
               }
               CddLinDiag_TxMsgStatusClear();
               SM_CddDiag10ms = CDD_DIAG_FrameAssign_PENDING;
            }
            else
            {
               // Do nothing
            }
         }
         else
         {
            //! #### If Lin Communication Mode changes to other than Diagnostic
            //! #### Then clear Transmit Requests and change state to Idle
            Rte_Write_LinDiagRequestFlag_CCNADRequest(0x0);
            LinDiag_CCNADRequestStatus = LinDiagService_Error; 
            CddLinDiag_TxMsgStatusClear();
            Rte_Call_CddLinClearRequest_ClearRequest();
            SM_CddDiag10ms = CDD_DIAG_IDLE;
         }
      break; 
      case CDD_DIAG_FrameAssign_PENDING:
         //! ##### 'Frame Id Assignement Process pending' state
         //! #### Check Whether CCNAD operation stop requested then clear Transmit Requests and change state to Idle
         if (1U == LinDiagCCNADRequestStoppedFlag)
         {
            Rte_Write_LinDiagRequestFlag_CCNADRequest(0x0);
            LinDiag_CCNADRequestStatus = LinDiagService_Completed; // LinDiagService_Error or LinDiagService_Completed
            Rte_Call_CddLinClearRequest_ClearRequest();
            SM_CddDiag10ms = CDD_DIAG_IDLE;
         }
         //! #### Check whether Lin Communication mode still in Diagnostic Mode
         else if ((Calibration == RteDataRead_Common.ComMode_LIN1)
                 && (Calibration == RteDataRead_Common.ComMode_LIN2)
                 && (Calibration == RteDataRead_Common.ComMode_LIN3)
                 && (Calibration == RteDataRead_Common.ComMode_LIN4)
                 && (Calibration == RteDataRead_Common.ComMode_LIN5))
         {
            //! #### Process the Frame Id Assignment Opearation 
            //! #### Change state to Idle state After successfully completion of Frame Id Assignement Opearation 
            IsStillProcessingFlag = 1U;
            IsStillProcessingFlag = Cdd_LinDiagnostics_AssignFrameID();
            if (0U == IsStillProcessingFlag)
            {
               Rte_Write_LinDiagRequestFlag_CCNADRequest(0x0);
               LinDiag_CCNADRequestStatus = LinDiagService_Completed;
               CddLinDiag_TxMsgStatusClear();
               SM_CddDiag10ms = CDD_DIAG_IDLE;
            }
            else
            {
               // Do nothing
            }
         }
         else
         {
            //! #### If Lin Communication Mode changes to other than Diagnostic
            //! #### Then clear Transmit Requests and change state to Idle 
            Rte_Write_LinDiagRequestFlag_CCNADRequest(0x0);
            LinDiag_CCNADRequestStatus = LinDiagService_Error;
            CddLinDiag_TxMsgStatusClear();
            Rte_Call_CddLinClearRequest_ClearRequest();
            SM_CddDiag10ms = CDD_DIAG_IDLE;
         }
      break;
      case CDD_DIAG_IDLE:
         //! ##### 'Idle' state
         //! #### Check whether Lin Communication mode still in Diagnostic Mode, Then
         if ((Inactive != RteDataRead_Common.ComMode_LIN1)
            && (Inactive != RteDataRead_Common.ComMode_LIN2)
            && (Inactive != RteDataRead_Common.ComMode_LIN3)
            && (Inactive != RteDataRead_Common.ComMode_LIN4)
            && (Inactive != RteDataRead_Common.ComMode_LIN5)) 
         {
            //! #### Check whether Part and SerialNumber read Operation requested then change the state to PNSN Requested state
            if (LinDiagService_Pending == LinDiag_PNSNRequestStatus) // Check the Conditions for PNSN Diagnostic Service 
            {
               CddLinDiag_TxMsgStatusClear();
               Rte_Call_CddLinClearRequest_ClearRequest();
               Rte_Write_LinDiagRequestFlag_PNSNRequest(0x1);
               SM_CddDiag10ms = CDD_DIAG_PNSN_REQ;
            }
            //! #### Check whether CCNAD Operation requested then change the state to CCNAD Requested state
            else if (LinDiagService_Pending == LinDiag_CCNADRequestStatus) // check the conditions for CCNAD Request
            {
               CddLinDiag_TxMsgStatusClear();
               Rte_Call_CddLinClearRequest_ClearRequest();
               Rte_Write_LinDiagRequestFlag_CCNADRequest(0x1);
               SM_CddDiag10ms = CDD_DIAG_CCNAD_REQ;
            }
            else
            {
               // Do nothing
            }
         }
         else
         {
            //! #### If Lin Communication Mode changes to other than Diagnostic, Then
            //! #### Check whether Part and SerialNumber read Operation requested then change PNSN request status to Error
            if (LinDiagService_Pending == LinDiag_PNSNRequestStatus) // Check the Conditions for PNSN Diagnostic Service 
            {   
               Rte_Write_LinDiagRequestFlag_PNSNRequest(0x0);
               LinDiag_PNSNRequestStatus = LinDiagService_Error;
            }
            //! #### Check whether CCNAD Operation requested then change CCNAD Request status to Error
            else if (LinDiagService_Pending == LinDiag_CCNADRequestStatus) // check the conditions for CCNAD Request
            {
               Rte_Write_LinDiagRequestFlag_CCNADRequest(0x0);
               LinDiag_CCNADRequestStatus = LinDiagService_Error;
            }
            else
            {
               // Do nothing
            }
         }
      break;
      case CDD_DIAG_INIT:
         //! ##### 'Initialization' state
         //! #### Check the 'LIN_Topology' is equal to value 1,Then set Volvo Truck Maximum Available FSPs value to Local buffer
         if (1U == Rte_Prm_P1AJR_LIN_topology_v())  
         {
            CddLinDiag_TxDataInfo[0].MaxNoOfFspNAD = 2;
            CddLinDiag_TxDataInfo[1].MaxNoOfFspNAD = 4;
            CddLinDiag_TxDataInfo[2].MaxNoOfFspNAD = 2;
            CddLinDiag_TxDataInfo[3].MaxNoOfFspNAD = 1;
            CddLinDiag_TxDataInfo[4].MaxNoOfFspNAD = 1;
         }
         //! #### Check the 'LIN_Topology' is equal to value 2,Then set Renault Truck Maximum Available FSPs value to Local buffer
         else if (2U == Rte_Prm_P1AJR_LIN_topology_v()) 
         {
            CddLinDiag_TxDataInfo[0].MaxNoOfFspNAD = 0;
            CddLinDiag_TxDataInfo[1].MaxNoOfFspNAD = 4;
            CddLinDiag_TxDataInfo[2].MaxNoOfFspNAD = 1;
            CddLinDiag_TxDataInfo[3].MaxNoOfFspNAD = 0;
            CddLinDiag_TxDataInfo[4].MaxNoOfFspNAD = 0;
         }
         //! #### Check the 'LIN_Topology' is other than 1(VT) or 2(RT),Then set Volvo Truck Maximum Available FSPs value to Local buffer
         else
         {
            // considering VT by default
            CddLinDiag_TxDataInfo[0].MaxNoOfFspNAD = 2;
            CddLinDiag_TxDataInfo[1].MaxNoOfFspNAD = 4;
            CddLinDiag_TxDataInfo[2].MaxNoOfFspNAD = 2;
            CddLinDiag_TxDataInfo[3].MaxNoOfFspNAD = 1;
            CddLinDiag_TxDataInfo[4].MaxNoOfFspNAD = 1;
         }
         //! #### Change the state to Idle state
         SM_CddDiag10ms = CDD_DIAG_IDLE;
      break;
      default:
         //! #### Change the state to Initialization state
         SM_CddDiag10ms = CDD_DIAG_INIT;
      break;
   }
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define Cdd_LinDiagnostics_STOP_SEC_CODE
#include "Cdd_LinDiagnostics_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the CddLinDiag_TxIdBusIndex
//!
//! \param    TxId_Index           Provides the Transmit Id 
//! \param    LinDiag_NADTxIdBus   Provides the NAD to Tx Id Table
//!
//! \return   uint8                returns 'LinBusIndex' variable value
//!
//!======================================================================================
static uint8 CddLinDiag_TxIdBusIndex(uint8 TxId_Index,
                                     CddLinDiagNADTxID_Type LinDiag_NADTxIdBus[CDD_LINDIAG_NoOfLINNetworks])
{
   uint8 LinBusIndex = 0xFFU;
   uint8 Index       = 0U;
   uint8 CIndex      = 0U;

   //! ###### Processing CddLinDiag_TxIdBusIndex
   //! #### Findout the Lin bus index from Transmit ID and return the Lin bus index
   switch (TxId_Index)
   {
      case CDD_LINDIAG_AssignNAD_LIN0_TXID:
         LinBusIndex = 0U;
      break;
      case CDD_LINDIAG_AssignNAD_LIN1_TXID:
         LinBusIndex = 1U;
      break;
      case CDD_LINDIAG_AssignNAD_LIN2_TXID:
         LinBusIndex = 2U;
      break;
      case CDD_LINDIAG_AssignNAD_LIN3_TXID:
         LinBusIndex = 3U;
      break;
      case CDD_LINDIAG_AssignNAD_LIN4_TXID:
         LinBusIndex = 4U;
      break;
      default:
         LinBusIndex = 0xFFU;
      break;
   }
   if (0xFFU == LinBusIndex)
   {
      for (Index = 0U; Index < CDD_LINDIAG_NoOfLINNetworks; Index++)
      {
         for (CIndex = 0U; CIndex <= LinDiag_NADTxIdBus[Index].MaxNoOfIds; CIndex++)
         {
            if (LinDiag_NADTxIdBus[Index].NADTxID[CIndex] == TxId_Index)
            {
               LinBusIndex = Index;
               Index       = CDD_LINDIAG_NoOfLINNetworks;
               break;
            }
            else
            {
               //Do nothing
            }
         }
      }
   }
   else
   {
      // Do nothing
   }
   return LinBusIndex;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Get_RteDataRead_Common'
//!
//! \param   *pRteDataRead_Common    Write data for data structure to fill with data
//!
//!======================================================================================
static void Get_RteDataRead_Common(Cdd_LinDiagnostics_In_struct_Type *pRteDataRead_Common)
{
   //! ###### Processing Get_RteDataRead_Common
   Rte_Read_ComMode_LIN1_ComMode_LIN(&pRteDataRead_Common->ComMode_LIN1);
   Rte_Read_ComMode_LIN2_ComMode_LIN(&pRteDataRead_Common->ComMode_LIN2);
   Rte_Read_ComMode_LIN3_ComMode_LIN(&pRteDataRead_Common->ComMode_LIN3);
   Rte_Read_ComMode_LIN4_ComMode_LIN(&pRteDataRead_Common->ComMode_LIN4);
   Rte_Read_ComMode_LIN5_ComMode_LIN(&pRteDataRead_Common->ComMode_LIN5);
   pRteDataRead_Common->DiagActive.Prev = pRteDataRead_Common->DiagActive.New;
   Rte_Read_DiagActiveState_P_isDiagActive(&pRteDataRead_Common->DiagActive.New);
   
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'CddLinDiag_TxMsgStatusClear'
//!
//!======================================================================================
static void CddLinDiag_TxMsgStatusClear(void)
{
   uint8 Index   = 0U;
   uint8 RxIndex = 0U;

   //! ###### Processing CddLinDiag_TxMsgStatusClear
   //! ##### Clear the transmit message request, status and internal buffers
   for (Index = 0U; Index < MAX_NUMBER_OF_TXMsgSTATUS; Index++)
   {
      CddLinDiag_TxMsgStatus[Index].TxId           = 0xFFU;
      CddLinDiag_TxMsgStatus[Index].Status         = LinDiagService_None;
      CddLinDiag_TxMsgStatus[Index].ReasonForError = CDD_LIN_NONE;
      CddLinDiag_TxMsgStatus[Index].Length         = 0U;
      CddLinDiag_TxMsgStatus[Index].SubServiceId   = 0U;
      for (RxIndex = 0U; RxIndex < 8; RxIndex++)
      {
         CddLinDiag_TxMsgStatus[Index].RxData[RxIndex] = 0U;
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'Cdd_LinDiagnostics_Transmit'
//!
//! \param   TxId      Provides the Transmit Id
//! \param   TxData    Provides the Transmit message data
//! \param   Length    Provides the Transmit message length
//!
//!======================================================================================
static void Cdd_LinDiagnostics_Transmit(uint8 TxId,
                                        P2VAR(uint8, AUTOMATIC, RTE_CDD_LINTPHANDLING_APPL_VAR) TxData,
                                        uint8 Length)
{
   uint8 Index = 0U;

   //! ###### Processing Cdd_LinDiagnostics_Transmit
   //! ##### Check for Free Space is avaialble Transmit buffer 
   while (CddLinDiag_TxMsgStatus[Index].TxId != 0xFFU)
   {
      Index++;
   }
   //! #### If Available then fill the Transmit buffer with ID,Length,Data and Change the status to Tx Pending  
   if (Index < MAX_NUMBER_OF_TXMsgSTATUS)
   {
      CddLinDiag_TxMsgStatus[Index].TxId           = TxId;
      CddLinDiag_TxMsgStatus[Index].Status         = LinDiagService_Pending;
      CddLinDiag_TxMsgStatus[Index].ReasonForError = CDD_LIN_TX_BUSY;
      Rte_Call_CddLinTxHandling_Transmit(TxId,TxData,Length);
   }
   else
   {
      // Do nothing
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_LinDiag_AsssignFrameInit
//!
//! \param   *pState_Init       Provides the Pointer to Fsp Assignment state variable
//! \param   *pFspIndex_Init    Provides the Pointer to Fsp Assignment Index
//! \param   LinBusIndex_Init   Provides the LIN Bus index
//!
//!======================================================================================
static void Cdd_LinDiag_AsssignFrameInit(uint8 *pState_Init,
                                         stCddLinFspIndex_Type *pFspIndex_Init,
                                         uint8 LinBusIndex_Init)
{
   //! ###### Processing Cdd_LinDiag_AsssignFrameInit logic
   pFspIndex_Init->CurrentFspIndex = 0U;

   //! ##### Check Whether CCNAD opearation Sucessful or not
   if (CddLinDiag_SlavesErrStatus[LinBusIndex_Init] == 0U)
   {
      //! #### If Sucessful then Read the Maximum Avaialble FSP from CCNAD result 
      pFspIndex_Init->MaxAvailableFSP = CddLinDiag_AvailableSlaves[LinBusIndex_Init];
   }
   else
   {
      //! #### else Set the Maximum Avaialble FSP to zero 
      pFspIndex_Init->MaxAvailableFSP = 0x0U;
   }
   //! ##### Change the FSP Assignment state to Send Assign Frame ID State
   *pState_Init = SendAssignFrameID;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_LinDiag_AsssignFrameSendFrameId
//!
//! \param   *pState_FrameId       Provides the Pointer to Fsp Assignment state variable
//! \param   *pFspIndex_FrameId    Provides the Pointer to Fsp Assignment Index
//! \param   LinBusIndex_FrameId   Provides the LIN Bus index
//!
//!======================================================================================
static void Cdd_LinDiag_AsssignFrameSendFrameId(uint8 *pState_FrameId,
                                                stCddLinFspIndex_Type *pFspIndex_FrameId,
                                                uint8 LinBusIndex_FrameId)
{
   uint8 TxArray[8U];
   uint8 Length = 0U;

   //! ###### Processing Cdd_LinDiag_AsssignFrameSendFrameId
   //! ###### Check Current FSP Index reaches to Maximum Available FSPs or not
   if ((pFspIndex_FrameId->CurrentFspIndex < pFspIndex_FrameId->MaxAvailableFSP)
      && (0xFFU != CddLinDiag_FspTxID[LinBusIndex_FrameId][pFspIndex_FrameId->CurrentFspIndex]))
   {
      //! ##### If the Current FSP Index below Maximum Avaialble FSPs    
      //! #### Prepare the Transmit data to Assign the Frame Ids and trigger the transmittion
      TxArray[0U] = LINSlaveNode_AssignFrameId_ServiceID;
      TxArray[1U] = 0x00U;
      TxArray[2U] = CddLinDiag_FspPID[pFspIndex_FrameId->CurrentFspIndex].PID1;
      TxArray[3U] = CddLinDiag_FspPID[pFspIndex_FrameId->CurrentFspIndex].PID2;
      TxArray[4U] = CddLinDiag_FspPID[pFspIndex_FrameId->CurrentFspIndex].PID3;
      TxArray[5U] = CddLinDiag_FspPID[pFspIndex_FrameId->CurrentFspIndex].PID4;
      Length      = 6U;
      Cdd_LinDiagnostics_Transmit(CddLinDiag_FspTxID[LinBusIndex_FrameId][pFspIndex_FrameId->CurrentFspIndex],
                                  &TxArray[0],
                                  Length);
      //! #### Change the FSP Assignment state to Wait for Response State
      *pState_FrameId = WaitForRespAssignFrameID;
   }
   else
   {
      //! ##### If the Current FSP Index reaches to Maximum Avaialble FSPs
      //! #### Change the FSP Assignment state to Delay Time State 
      *pState_FrameId = DelayTimeAsignFrame;
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_LinDiag_AsssignFrameWaitForResp
//!
//! \param   *pState_Resp       Provides the Pointer to Fsp Assignment state variable
//! \param   *pFspIndex_Resp    Provides the Pointer to Fsp Assignment Index
//! \param   LinBusIndex_Resp   Provides the LIN Bus index
//!
//!======================================================================================
static void Cdd_LinDiag_AsssignFrameWaitForResp(uint8 *pState_Resp,
                                                stCddLinFspIndex_Type *pFspIndex_Resp,
                                                uint8 LinBusIndex_Resp)
{
   uint8 Index = 0U;

   //! ###### Processing Cdd_LinDiag_AsssignFrameWaitForResp
   //! ###### Check for Transmit message status
   for (Index = 0U; Index<MAX_NUMBER_OF_TXMsgSTATUS; Index++)
   {
      if (CddLinDiag_FspTxID[LinBusIndex_Resp][pFspIndex_Resp->CurrentFspIndex] == CddLinDiag_TxMsgStatus[Index].TxId)
      {
         //! ##### If Transmit message status is completed 
         if (LinDiagService_Completed == CddLinDiag_TxMsgStatus[Index].Status)
         {
            //! #### Change the FSP Assignment state to Save Configuration Request State
            *pState_Resp = SaveConfigRequest;
         }
         //! ##### If Transmit message status is Error 
         else if (LinDiagService_Error == CddLinDiag_TxMsgStatus[Index].Status)
         {
            //! #### Change the FSP Assignment state to Completed Assign Frame State
            *pState_Resp = CompletedAsignFrame; // Error status need to be updated 
         }
         else
         {
            // Do Nothing and Wait For Resp..
         }
      }
      else
      {
         // Do nothing
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_LinDiag_AsssignFrameSaveConfigReq
//!
//! \param   *pState_Req       Provides the Pointer to Fsp Assignment state variable
//! \param   *pFspIndex_Req    Provides the Pointer to Fsp Assignment Index
//! \param   LinBusIndex_Req   Provides the LIN Bus index
//!
//!======================================================================================
static void Cdd_LinDiag_AsssignFrameSaveConfigReq(uint8 *pState_Req,
                                                  stCddLinFspIndex_Type *pFspIndex_Req,
                                                  uint8 LinBusIndex_Req)
{
   uint8 TxArray[8U];
   uint8 Length = 0U;

   //! ###### Processing Cdd_LinDiag_AsssignFrameSaveConfigReq
   //! ###### Check Current FSP Index reaches to Maximum Available FSPs or not
   if ((pFspIndex_Req->CurrentFspIndex < pFspIndex_Req->MaxAvailableFSP)
      && (0xFFU != CddLinDiag_FspTxID[LinBusIndex_Req][pFspIndex_Req->CurrentFspIndex]))
   {
      //! ##### If the Current FSP Index below Maximum Avaialble FSPs    
      //! #### Prepare the Transmit data to Save the Frame IDs and trigger the transmittion
      TxArray[0U] = LINSlaveNode_SaveConfig_ServiceID;
      TxArray[1U] = 0xFFU;
      TxArray[2U] = 0xFFU;
      TxArray[3U] = 0xFFU;
      TxArray[4U] = 0xFFU;
      TxArray[5U] = 0xFFU;
      Length      = 6U;
      Cdd_LinDiagnostics_Transmit(CddLinDiag_FspTxID[LinBusIndex_Req][pFspIndex_Req->CurrentFspIndex],
                                  &TxArray[0U],
                                  Length);
      //! #### Change the FSP Assignment state to Wait for Save Configuration State
      *pState_Req = WaitForSaveConfig;
   }
   else
   {
      //! ##### If the Current FSP Index reaches to Maximum Avaialble FSPs
      //! #### Change the FSP Assignment state to Delay Time State 
      *pState_Req = DelayTimeAsignFrame;  
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_LinDiag_AsssignFrameWaitForSavingConfig
//!
//! \param   *pState       Provides the Pointer to Fsp Assignment state variable
//! \param   *pFspIndex    Provides the Pointer to Fsp Assignment Index
//! \param   LinBusIndex   Provides the LIN Bus index
//!
//!======================================================================================
static void Cdd_LinDiag_AsssignFrameWaitForSavingConfig(uint8 *pState,
                                                        stCddLinFspIndex_Type *pFspIndex,
                                                        uint8 LinBusIndex)
{
   uint8 Index = 0U;

   //! ###### Processing Cdd_LinDiag_AsssignFrameWaitForSavingConfig
   //! ###### Check for Transmit message status
   for (Index = 0U; Index < MAX_NUMBER_OF_TXMsgSTATUS; Index++)
   {
      if (CddLinDiag_FspTxID[LinBusIndex][pFspIndex->CurrentFspIndex] == CddLinDiag_TxMsgStatus[Index].TxId)
      {
         //! ##### If Transmit message status is completed 
         if (LinDiagService_Completed == CddLinDiag_TxMsgStatus[Index].Status)
         {
            //! #### Change the FSP Assignment state to Send Assign Frame ID State
            *pState = SendAssignFrameID;
            //! #### Increment the Current FSP Index
            pFspIndex->CurrentFspIndex++; // Increase the Current Index
         }
         //! ##### If Transmit message status is Error 
         else if (LinDiagService_Error == CddLinDiag_TxMsgStatus[Index].Status)
         {
            //! #### Change the FSP Assignment state to Completed Assign Frame State
            *pState = CompletedAsignFrame; // Error status need to be updated 
         }
         else
         {
            // Do Nothing and Wait For Resp..
         }
      }
      else
      {
         // Do nothing
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_LinDiag_AsssignFrameCompleted
//!
//! \param   *pState                    Provides the Pointer to Fsp Assignment state variable
//! \param   *pFspIndex_FrameComplete   Provides the Pointer to Fsp Assignment Index
//!
//!======================================================================================
static void Cdd_LinDiag_AsssignFrameCompleted(uint8 *pState,
                                              stCddLinFspIndex_Type *pFspIndex_FrameComplete)
{
   //! ###### Processing Cdd_LinDiag_AsssignFrameCompleted
   //! ##### Set the Current FSP Index and Maximum Available FSP to Zero
   Unused_Parameter(*pState);
   pFspIndex_FrameComplete->CurrentFspIndex = 0U;
   pFspIndex_FrameComplete->MaxAvailableFSP = 0U;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_LinDiagnostics_AssignFrameID
//!
//!======================================================================================
static uint8 Cdd_LinDiagnostics_AssignFrameID(void)
{
   static uint8 SM_AssignFrameID[CDD_LINDIAG_NoOfLINNetworks] = {AssignFrameIdInit,AssignFrameIdInit,AssignFrameIdInit,AssignFrameIdInit,AssignFrameIdInit};
   static stCddLinFspIndex_Type CddLinDid_FspIndex[CDD_LINDIAG_NoOfLINNetworks];
   static uint16 FSPSavingDelayTimer[CDD_LINDIAG_NoOfLINNetworks] = {0U,0U,0U,0U,0U};
   uint8 LinBusIndex = 0U;
   uint8 RetValue    = 0U;

   //! ###### Processing Cdd_LinDiagnostics_AssignFrameID
   //! ###### Process the Frame ID Assignment to All FSP on All LIN Bus Networks 
   for (LinBusIndex = 0U; LinBusIndex < CDD_LINDIAG_NoOfLINNetworks; LinBusIndex++)
   {
      if (SM_AssignFrameID[LinBusIndex] != CompletedAsignFrame)
      {
         RetValue = 1U;
      }
      else
      {
         // Do nothing
      }
      //! ##### Processing of Frame ID assignment state machine
      switch (SM_AssignFrameID[LinBusIndex])
      {
         //! ##### At assign FrameId initialization state
         case AssignFrameIdInit:
         //! #### Processing of Cdd_LinDiag_AsssignFrameInit()
            Cdd_LinDiag_AsssignFrameInit(&SM_AssignFrameID[LinBusIndex],
                                         &CddLinDid_FspIndex[LinBusIndex],
                                         LinBusIndex);
         break;
         //! ##### At send assign FrameId state
         case SendAssignFrameID:
         //! #### Processing of Cdd_LinDiag_AsssignFrameSendFrameId()
            Cdd_LinDiag_AsssignFrameSendFrameId(&SM_AssignFrameID[LinBusIndex],
                                                &CddLinDid_FspIndex[LinBusIndex],
                                                LinBusIndex);
         break;
         //! ##### At wait for assign FrameID response state
         case WaitForRespAssignFrameID:
         //! #### Processing of Cdd_LinDiag_AsssignFrameWaitForResp()
            Cdd_LinDiag_AsssignFrameWaitForResp(&SM_AssignFrameID[LinBusIndex],
                                                &CddLinDid_FspIndex[LinBusIndex],
                                                LinBusIndex);
         break;
         //! ##### At send configuration request state
         case SaveConfigRequest:
         //! #### Processing of Cdd_LinDiag_AsssignFrameSaveConfigReq()
            Cdd_LinDiag_AsssignFrameSaveConfigReq(&SM_AssignFrameID[LinBusIndex],
                                                  &CddLinDid_FspIndex[LinBusIndex],
                                                  LinBusIndex);
         break;
         //! ##### At wait for save configuration state
         case WaitForSaveConfig:
         //! #### Processing of Cdd_LinDiag_AsssignFrameWaitForSavingConfig()
            Cdd_LinDiag_AsssignFrameWaitForSavingConfig(&SM_AssignFrameID[LinBusIndex],
                                                        &CddLinDid_FspIndex[LinBusIndex],
                                                        LinBusIndex);
         break;
         //! ##### At completed assign FrameId state
         case CompletedAsignFrame:
         //! #### Processing of Cdd_LinDiag_AsssignFrameCompleted()
            Cdd_LinDiag_AsssignFrameCompleted(&SM_AssignFrameID[LinBusIndex],
                                              &CddLinDid_FspIndex[LinBusIndex]);
         break;
         //! ##### At delay timer state
         case DelayTimeAsignFrame:
         //! #### Processing of DelayTime state
            if (FSPSavingDelayTimer[LinBusIndex] >= 100U)
            {
               SM_AssignFrameID[LinBusIndex]    = CompletedAsignFrame;
               FSPSavingDelayTimer[LinBusIndex] = 0U;
            }
            else
            {
               FSPSavingDelayTimer[LinBusIndex]++;
            }
         break;
         default:
            // Maintain previous state
         break;
      }
   }
   if (0U == RetValue) 
   {
      for (LinBusIndex = 0U; LinBusIndex < CDD_LINDIAG_NoOfLINNetworks; LinBusIndex++)
      {
         SM_AssignFrameID[LinBusIndex] = AssignFrameIdInit;
      }
   }
   else
   {
      // Do nothing
   }
   return RetValue; 
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_LinDiagnostics_SlaveNodePN
//!
//!======================================================================================
static void Cdd_LinDiagnostics_SlaveNodePN(void)
{   
   uint8 TxArray[6U];
   uint8 Length = 0U;
   TxArray[0U] = LINSlaveNode_ReadDataByID_ServiceID;
   TxArray[1U] = LINSlaveNode_SERIAL_NUMBER_SubServiceID;
   TxArray[2U] = 0xFFU;
   TxArray[3U] = 0x7FU;
   TxArray[4U] = 0xFFU;
   TxArray[5U] = 0xFFU;
   Length      = 6U;

   //! ###### Processing of slave node PartNumber and SerialNumber
   //! ###### Check for Lin Topology variant
   if (2U == Rte_Prm_P1AJR_LIN_topology_v())
   {
      //! ##### If Lin topology variant is Renault Truck 
      //! ####  Request for reading Renault Truck related slaves PartNumbers and SerialNumbers  
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_LECMBasic_oLIN00_16bdebb7_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_LECM2_oLIN00_670141b5_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L2_oLIN01_d46fba5f_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L2_oLIN01_4d8ddc5e_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_L2_oLIN01_8c03039e_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP4_L2_oLIN01_a538161d_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L3_oLIN02_81cceb7b_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_ELCP2_oLIN03_4563b13d_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_CCFW_oLIN03_98357989_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_DLFW_oLIN03_40053787_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_ILCP2_oLIN03_4069fcbc_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_RCECS_oLIN04_b46d7afd_Tx,
                                  &TxArray[0U],
                                  Length);
      TxArray[1U] = LINSlaveNode_PART_NUMBER_SubServiceID;
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_LECMBasic_oLIN00_16bdebb7_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_LECM2_oLIN00_670141b5_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L2_oLIN01_d46fba5f_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L2_oLIN01_4d8ddc5e_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_L2_oLIN01_8c03039e_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP4_L2_oLIN01_a538161d_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L3_oLIN02_81cceb7b_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_ELCP2_oLIN03_4563b13d_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_CCFW_oLIN03_98357989_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_DLFW_oLIN03_40053787_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_ILCP2_oLIN03_4069fcbc_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_RCECS_oLIN04_b46d7afd_Tx,
                                  &TxArray[0U],
                                  Length);
   }
   else
   {
      //! ##### If Lin topology variant is Volvo Truck
      //! ####  Request for reading Volvo Truck related slaves PartNumbers and SerialNumbers  
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L1_oLIN00_2de78d2a_Tx, 
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L1_oLIN00_b405eb2b_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_ILCP1_oLIN00_57efaae5_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L2_oLIN01_d46fba5f_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L2_oLIN01_4d8ddc5e_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_L2_oLIN01_8c03039e_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP4_L2_oLIN01_a538161d_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L3_oLIN02_81cceb7b_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L3_oLIN02_182e8d7a_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_TCP_oLIN02_4b422897_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L4_oLIN03_fc0ed2f4_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_ELCP1_oLIN03_cbecb6de_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L5_oLIN04_aec047c9_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_RCECS_oLIN04_b46d7afd_Tx,
                                  &TxArray[0U],
                                  Length);
      TxArray[1U] = LINSlaveNode_PART_NUMBER_SubServiceID;
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L1_oLIN00_2de78d2a_Tx, 
                                  &TxArray[0U], 
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L1_oLIN00_b405eb2b_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_ILCP1_oLIN00_57efaae5_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L2_oLIN01_d46fba5f_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L2_oLIN01_4d8ddc5e_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_L2_oLIN01_8c03039e_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP4_L2_oLIN01_a538161d_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L3_oLIN02_81cceb7b_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L3_oLIN02_182e8d7a_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_TCP_oLIN02_4b422897_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L4_oLIN03_fc0ed2f4_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_ELCP1_oLIN03_cbecb6de_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L5_oLIN04_aec047c9_Tx,
                                  &TxArray[0U],
                                  Length);
      Cdd_LinDiagnostics_Transmit(CddConf_CddPduRUpperLayerTxPdu_MasterReq_RCECS_oLIN04_b46d7afd_Tx,
                                  &TxArray[0U],
                                  Length);
  }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the RawFormatToPartNumberASCIIConvertion
//!
//! \param   *pIndata   Provide the pointer to Indata buffer
//!
//!======================================================================================
static void RawFormatToPartNumberASCIIConvertion(uint8 *pIndata)
{
   //! ###### Processing RawFormatToPartNumberASCIIConvertion
   //! ##### Convert the Decimal Number to ASCII characters
   if (*pIndata <= 9U)
   {
      *pIndata = *pIndata+ASCII_OFFSET_FOR_NUMBERS;
   }
   else
   {
      switch (*pIndata)
      {
         case 0xA:
            *pIndata = 'A';
         break;
         case 0xB:
            *pIndata = 'B';
         break;
         case 0xC:
            *pIndata = 'C';
         break;
         case 0xD:
            *pIndata = 'D';
         break;
         case 0xE:
            *pIndata = 'E';
         break;
         case 0xF:
            *pIndata = 'F';
         break;
         default:
            *pIndata = '0';
         break;
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the RawFormatToPartNumberFormatConversion
//!
//! \param   TxId_RawtoPNF       Provides the Transmit ID
//! \param   *pIndata_RawtoPNF   Provides the pointer to the Indata buffer
//! \param   Length_RawtoPNF     Provides the Indata Length
//!
//!======================================================================================
static void RawFormatToPartNumberFormatConversion(uint8 TxId_RawtoPNF,
                                                  const uint8 *pIndata_RawtoPNF, 
                                                  uint8 Length_RawtoPNF)
{
   uint8 i    = 0U;
   uint8 j    = 3U;
   uint8 Temp = 0U;
   uint8 *Outdata;

   //! ###### Process of Converting PartNumber from Decimal to Volvo Specified Format
   //! ###### Check the Length of Decimal Format Partnumber
   if (5U == Length_RawtoPNF)
   {
      //! ##### If Length is equal to 5
      //! #### Read Available Address of Lin PartNumber buffer
      Outdata     = &LinDiagPartNumber[LinDiagNoOfSlavesPN][0U];
      //! #### Read first Four Bytes of Decimal PartNumber and Convert each nibble in byte to ASCII Character
      Outdata[0U] = 0x02U; // Module Type SUB HW Type    
      for (i = 1U; i < 9U; i = i + 2U)
      {
         Outdata[i]       = (char)((pIndata_RawtoPNF[j] >> 4U) & 0x0F); // ASCII conversion--> 16 means '0' character
         RawFormatToPartNumberASCIIConvertion(&Outdata[i]);
         Outdata[i + 1U]  = (char)((pIndata_RawtoPNF[j]) & 0x0F); // ASCII conversion --> 16 means '0' character
         RawFormatToPartNumberASCIIConvertion(&Outdata[i + 1U]);
         j--;
      }
      //! #### Read Fifth(Last) Byte of Decimal PartNumber and Convert as per Volvo Revision format
      Temp = ((pIndata_RawtoPNF[4U] >> 6U) & 0x03U);
      switch (Temp)
      {
         case 3:
            Outdata[9U] = 'P';
         break;
         case 2:
            Outdata[9U] = 'C';
         break;
         case 1:
            Outdata[9U] = 'B';
         break;
         case 0:
         default:
            Outdata[9U] = 'A';
         break;
      }
      Temp                 = ((pIndata_RawtoPNF[4U]) & 0x3F);         
      Outdata[10U]         = (char)((uint8)(Temp / 10U) + ASCII_OFFSET_FOR_NUMBERS);
      Outdata[11U]         = (char)((Temp % 10U) + ASCII_OFFSET_FOR_NUMBERS);
      LinDiagNoOfSlavesPN  = LinDiagNoOfSlavesPN + 1U;
   }
   else
   {
      // Length Error
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the RawFormatToSerialNumberFormatConversion
//!
//! \param   TxId_RawtoSN       Provides the Transmit ID
//! \param   *pIndata_RawtoSN   Provides the pointer to the Indata buffer
//! \param   Length_RawtoSN     Provides the Indata Length
//!
//!======================================================================================
static void RawFormatToSerialNumberFormatConversion(uint8 TxId_RawtoSN,
                                                    const uint8 *pIndata_RawtoSN, 
                                                    uint8 Length_RawtoSN)
{
   uint8 i = 0U;
   uint8 j = 3U;
   uint8 *Outdata;
   //! ###### Process of converting SerialNumber from decimal to Volvo Specified format
   //! ###### Check the length of decimal format SerialNumber
   if (5U == Length_RawtoSN)
   {
      //! ##### If length is equal to 5
      //! #### Read available address of Lin SerialNumber buffer
      Outdata = &LinDiagSerialNumber[LinDiagNoOfSlavesSN][0U];
      //! #### Read decimal SerialNumber and convert each nibble in byte to ASCII character
      for (i = 0U; i < 8U; i = i + 2U)
      {
         Outdata[i]       = (char)(((pIndata_RawtoSN[j] >> 4U) & 0x0F) + ASCII_OFFSET_FOR_NUMBERS); // ASCII conversion--> 16 means '0' character
         Outdata[i + 1U]  = (char)(((pIndata_RawtoSN[j]) & 0x0F) + ASCII_OFFSET_FOR_NUMBERS); // ASCII conversion --> 16 means '0' character
         j--;
      }
      LinDiagNoOfSlavesSN = LinDiagNoOfSlavesSN + 1U;
   }
   else
   {
      // Length error
   } 
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_LinDiagnostics_CCNAD_SetInitialNAD
//!
//! \param   LinDiag_CCNAD_TokenPhaseInitial    Provides the CCNAD Token Phase Information
//!
//!======================================================================================
static void Cdd_LinDiagnostics_CCNAD_SetInitialNAD(ENUM_CDD_LINDIAG LinDiag_CCNAD_TokenPhaseInitial[CDD_LINDIAG_NoOfLINNetworks])
{
   uint8 TxArray[8U];
   uint8 Length = 0U;

   //! ###### Processing the CCNAD initialization token phase
   //! ###### Check whether current token phase is no token phase or not
   if ((CONST_CDD_LINDIAG_NO_TOKEN_PHASE == LinDiag_CCNAD_TokenPhaseInitial[0U])
      && (CONST_CDD_LINDIAG_NO_TOKEN_PHASE == LinDiag_CCNAD_TokenPhaseInitial[1U])
      && (CONST_CDD_LINDIAG_NO_TOKEN_PHASE == LinDiag_CCNAD_TokenPhaseInitial[2U])
      && (CONST_CDD_LINDIAG_NO_TOKEN_PHASE == LinDiag_CCNAD_TokenPhaseInitial[3U])
      && (CONST_CDD_LINDIAG_NO_TOKEN_PHASE == LinDiag_CCNAD_TokenPhaseInitial[4U]))
   {
      //! ##### If it is no token phase
      //! #### Fill the transmit data to set the FSP LIN NAD IDs to 0x40 and trigger the transmittion
      TxArray[0U] = LINSlaveNode_AssignNAD_ServiceID;
      TxArray[1U] = 0x5B; //ABVolvo
      TxArray[2U] = 0x00U; //ABVolvo
      TxArray[3U] = 0x01U; //FunctionID
      TxArray[4U] = 0x00U; //FunctionID
      TxArray[5U] = 0x40U; //InitialNAD
      Length      = 6U;
      Cdd_LinDiagnostics_Transmit(CDD_LINDIAG_AssignNAD_LIN0_TXID,&TxArray[0U],Length);
      Cdd_LinDiagnostics_Transmit(CDD_LINDIAG_AssignNAD_LIN1_TXID,&TxArray[0U],Length);
      Cdd_LinDiagnostics_Transmit(CDD_LINDIAG_AssignNAD_LIN2_TXID,&TxArray[0U],Length);
      Cdd_LinDiagnostics_Transmit(CDD_LINDIAG_AssignNAD_LIN3_TXID,&TxArray[0U],Length);
      Cdd_LinDiagnostics_Transmit(CDD_LINDIAG_AssignNAD_LIN4_TXID,&TxArray[0U],Length);
      //! #### Change the Token phase to initialization token phase
      LinDiag_CCNAD_TokenPhaseInitial[0U] = CONST_CDD_LINDIAG_INITIAL_TOKEN_PHASE;
      LinDiag_CCNAD_TokenPhaseInitial[1U] = CONST_CDD_LINDIAG_INITIAL_TOKEN_PHASE;
      LinDiag_CCNAD_TokenPhaseInitial[2U] = CONST_CDD_LINDIAG_INITIAL_TOKEN_PHASE;
      LinDiag_CCNAD_TokenPhaseInitial[3U] = CONST_CDD_LINDIAG_INITIAL_TOKEN_PHASE;
      LinDiag_CCNAD_TokenPhaseInitial[4U] = CONST_CDD_LINDIAG_INITIAL_TOKEN_PHASE;
   }
   else
   {
      // Do nothing
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_LinDiagnostics_ForwardCCNADProcess
//!
//! \param   LinBusIndex_Fwd                Provides the LIN Bus index
//! \param   CurrentIndex_Fwd               Provides the Current index
//! \param   LinDiag_CCNAD_TokenPhaseFwd    Provides the CCNAD Token Phase Information
//! \param   CddLinDiag_TxDataInfoFwd       Provides the LIN Transmit data buffer
//! \param   LinDiag_NADTxIdFwd             Provides the LIN NAD to Tx ID buffer
//! \param   CddLinDiag_TxStoredDataFwd     Provides the LIN Transmit Stored data
//!
//!======================================================================================
static void Cdd_LinDiagnostics_ForwardCCNADProcess(uint8 LinBusIndex_Fwd,
                                                   uint8 CurrentIndex_Fwd,
                                                   ENUM_CDD_LINDIAG LinDiag_CCNAD_TokenPhaseFwd[CDD_LINDIAG_NoOfLINNetworks],
                                                   CDDLinDiagByteMaskType  CddLinDiag_TxDataInfoFwd[CDD_LINDIAG_NoOfLINNetworks],
                                                   CddLinDiagNADTxID_Type  LinDiag_NADTxIdFwd[CDD_LINDIAG_NoOfLINNetworks],
                                                   CDDLinDiagStoredbyteType CddLinDiag_TxStoredDataFwd[CDD_LINDIAG_NoOfLINNetworks])
{
   uint8 TxId         = 0U;
   uint8 TxArray[8U];
   uint8 Length       = 0U;
   uint8 *Array;
   static uint8 MaskByteOverflowFlag[CDD_LINDIAG_NoOfLINNetworks] = {0,0,0,0,0};

   //! ###### Processing CCNAD forward token phase
   //! ###### Check whether transmition is in completed state or error state
   if ((LinDiagService_Completed == CddLinDiag_TxMsgStatus[CurrentIndex_Fwd].Status)
      || (LinDiagService_Error == CddLinDiag_TxMsgStatus[CurrentIndex_Fwd].Status))
   {
      //! ###### Check the current processing FSP NAD ID is less than Maximum FSP NAD ID on this Lin bus
      if ((LinDiag_NADTxIdFwd[LinBusIndex_Fwd].CurrentIdIndex < (LinDiag_NADTxIdFwd[LinBusIndex_Fwd].MaxNoOfIds - 1U))
         && (0U == MaskByteOverflowFlag[LinBusIndex_Fwd]))
      {
         if ((CDD_LIN_RX_ERROR == CddLinDiag_TxMsgStatus[CurrentIndex_Fwd].ReasonForError)
            || (CDD_LIN_RX_OK == CddLinDiag_TxMsgStatus[CurrentIndex_Fwd].ReasonForError))
         {
            if (CddLinDiag_TxStoredDataFwd[LinBusIndex_Fwd].StoredIndex < CDD_LINDIAG_NoOfNADTxId)
            {
               CddLinDiag_TxStoredDataFwd[LinBusIndex_Fwd].StoredData[CddLinDiag_TxStoredDataFwd[LinBusIndex_Fwd].StoredIndex].LastNAD     =  LinDiag_NADTxIdFwd[LinBusIndex_Fwd].CurrentIdIndex;
               CddLinDiag_TxStoredDataFwd[LinBusIndex_Fwd].StoredData[CddLinDiag_TxStoredDataFwd[LinBusIndex_Fwd].StoredIndex].LastRsdMask = CddLinDiag_TxDataInfoFwd[LinBusIndex_Fwd].Mask;
               CddLinDiag_TxStoredDataFwd[LinBusIndex_Fwd].StoredData[CddLinDiag_TxStoredDataFwd[LinBusIndex_Fwd].StoredIndex].LastRsdByte = CddLinDiag_TxDataInfoFwd[LinBusIndex_Fwd].Byte;
               CddLinDiag_TxStoredDataFwd[LinBusIndex_Fwd].StoredIndex++;
            }
            else
            {
               // Do nothing
            }
            LinDiag_NADTxIdFwd[LinBusIndex_Fwd].CurrentIdIndex++;
         }
         else if (CDD_LIN_RX_NO_RESPONSE == CddLinDiag_TxMsgStatus[CurrentIndex_Fwd].ReasonForError)
         {
            // Do nothing
         }
         else
         {
            // Do nothing
         }
         Array       = LinDiag_NADTxIdFwd[LinBusIndex_Fwd].NADTxID;
         TxId        = Array[LinDiag_NADTxIdFwd[LinBusIndex_Fwd].CurrentIdIndex];
         TxArray[0U] = LINSlaveNode_CCNAD_ServiceID;
         TxArray[1U] = 0x01U;                                           //Serial Number
         TxArray[2U] = CddLinDiag_TxDataInfoFwd[LinBusIndex_Fwd].Byte; //Byte
         TxArray[3U] = CddLinDiag_TxDataInfoFwd[LinBusIndex_Fwd].Mask; //Mask
         TxArray[4U] = 0xFFU;                                           //Invert
         TxArray[5U] = 0x40U + LinDiag_NADTxIdFwd[LinBusIndex_Fwd].CurrentIdIndex + 1U;
         Length      = 6U;
         Rte_Call_CddLinTxHandling_Transmit(TxId,TxArray,Length);
         CddLinDiag_TxMsgStatus[CurrentIndex_Fwd].TxId           = TxId;
         CddLinDiag_TxMsgStatus[CurrentIndex_Fwd].Status         = LinDiagService_Pending;
         CddLinDiag_TxMsgStatus[CurrentIndex_Fwd].ReasonForError = CDD_LIN_NONE;
         LinDiag_CCNAD_TokenPhaseFwd[LinBusIndex_Fwd]            = CONST_CDD_LINDIAG_FORWARD_TOKEN_PHASE;      
         // Update the Byte and Mask
         if (((CddLinDiag_TxDataInfoFwd[LinBusIndex_Fwd].Mask) & (0x80U)) != 0x80U)
         {
            CddLinDiag_TxDataInfoFwd[LinBusIndex_Fwd].Mask = (CddLinDiag_TxDataInfoFwd[LinBusIndex_Fwd].Mask << 1U);
         }
         else
         {
            if (CddLinDiag_TxDataInfoFwd[LinBusIndex_Fwd].Byte < 0x4U)
            {
               CddLinDiag_TxDataInfoFwd[LinBusIndex_Fwd].Byte = CddLinDiag_TxDataInfoFwd[LinBusIndex_Fwd].Byte + 1U;
               CddLinDiag_TxDataInfoFwd[LinBusIndex_Fwd].Mask = 0x1U;
            }
            else
            {
               CddLinDiag_TxDataInfoFwd[LinBusIndex_Fwd].Byte = 1U;
               CddLinDiag_TxDataInfoFwd[LinBusIndex_Fwd].Mask = 1U;
               MaskByteOverflowFlag[LinBusIndex_Fwd]          = 1U;
            }
         }
      }
      else
      {
         //! ###### Processing of CCNAD forward token phase when current FSP ID reaches to maximum limit
         //! ##### Check if Lin receive message is successfully receiced or collision occured (receive error) 
         if ((CDD_LIN_RX_ERROR == CddLinDiag_TxMsgStatus[CurrentIndex_Fwd].ReasonForError)
            || (CDD_LIN_RX_OK == CddLinDiag_TxMsgStatus[CurrentIndex_Fwd].ReasonForError))
         {
           //! #### Increament the current NAD ID
            LinDiag_NADTxIdFwd[LinBusIndex_Fwd].CurrentIdIndex++;
         }
         else if (CDD_LIN_RX_NO_RESPONSE == CddLinDiag_TxMsgStatus[CurrentIndex_Fwd].ReasonForError)
         {
            // Do nothing
         }
         else
         {
            // Do nothing
         }
         //! ##### Transmit the Lin diagnostic message for reading serial number from slave 
         //! #### Update the transmit buffer for reading serial number from slave with current NAD ID
         MaskByteOverflowFlag[LinBusIndex_Fwd] = 0U;
         Array       = LinDiag_NADTxIdFwd[LinBusIndex_Fwd].NADTxID;
         TxId        = Array[LinDiag_NADTxIdFwd[LinBusIndex_Fwd].CurrentIdIndex];      
         TxArray[0U] = LINSlaveNode_ReadDataByID_ServiceID;
         TxArray[1U] = LINSlaveNode_SERIAL_NUMBER_SubServiceID;
         TxArray[2U] = 0x5BU;
         TxArray[3U] = 0x00U;
         TxArray[4U] = 0x01U;
         TxArray[5U] = 0x00U;
         Length      = 6U;
         //! #### Trigger the transmittion with TxId and transmit buffer
         Rte_Call_CddLinTxHandling_Transmit(TxId,TxArray,Length);
         //! #### Update the transmit status to pending state
         CddLinDiag_TxMsgStatus[CurrentIndex_Fwd].TxId           = TxId;
         CddLinDiag_TxMsgStatus[CurrentIndex_Fwd].Status         = LinDiagService_Pending;
         CddLinDiag_TxMsgStatus[CurrentIndex_Fwd].ReasonForError = CDD_LIN_NONE;
         //! #### Change the token Phase to consitency check phase
         LinDiag_CCNAD_TokenPhaseFwd[LinBusIndex_Fwd]            = CONST_CDD_LINDIAG_CONSITENCY_TOKEN_PHASE;
      }
   }
   else
   {
      // Do nothing
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_LinDiagnostics_BackwardCCNADProcess
//!
//! \param   LinBusIndex_Bwd               Provides the LIN Bus index
//! \param   CurrentIndex_Bwd              Provides the Current index
//! \param   LinDiag_CCNAD_TokenPhaseBwd   Provides the CCNAD Token Phase Information
//! \param   CddLinDiag_TxDataInfoBwd      Provides the LIN Transmit data buffer
//! \param   LinDiag_NADTxIdBwd            Provides the LIN NAD to Tx ID buffer
//! \param   CddLinDiag_TxStoredDataBwd    Provides the LIN Transmit Stored data
//! 
//!======================================================================================
static void Cdd_LinDiagnostics_BackwardCCNADProcess(uint8 LinBusIndex_Bwd,
                                                    uint8 CurrentIndex_Bwd,
                                                    ENUM_CDD_LINDIAG LinDiag_CCNAD_TokenPhaseBwd[CDD_LINDIAG_NoOfLINNetworks],
                                                    CDDLinDiagByteMaskType  CddLinDiag_TxDataInfoBwd[CDD_LINDIAG_NoOfLINNetworks],
                                                    CddLinDiagNADTxID_Type  LinDiag_NADTxIdBwd[CDD_LINDIAG_NoOfLINNetworks],
                                                    CDDLinDiagStoredbyteType CddLinDiag_TxStoredDataBwd[CDD_LINDIAG_NoOfLINNetworks])
{
   uint8 TxId;
   uint8 TxArray[8U];
   uint8 Length = 0U;
   uint8 *Array;
   uint8 Index  = 0U;

   //! ###### processing Cdd_LinDiagnostics_BackwardCCNADProcess
   //! ###### Check whether no response error occured
   if ((LinDiagService_Error == CddLinDiag_TxMsgStatus[CurrentIndex_Bwd].Status)
      && (CDD_LIN_RX_NO_RESPONSE == CddLinDiag_TxMsgStatus[CurrentIndex_Bwd].ReasonForError))
   {
      //! ##### Processing CCNAD backward token phase when current FSP ID is not equal to zero
      if (LinDiag_NADTxIdBwd[LinBusIndex_Bwd].CurrentIdIndex != 0U)
      {
         //! #### Prepare the transmit buffer for reading serial number from slave
         LinDiag_NADTxIdBwd[LinBusIndex_Bwd].CurrentIdIndex = LinDiag_NADTxIdBwd[LinBusIndex_Bwd].CurrentIdIndex - 1U;
         Array       = LinDiag_NADTxIdBwd[LinBusIndex_Bwd].NADTxID;
         TxId        = Array[LinDiag_NADTxIdBwd[LinBusIndex_Bwd].CurrentIdIndex];
         TxArray[0U] = LINSlaveNode_ReadDataByID_ServiceID;
         TxArray[1U] = LINSlaveNode_SERIAL_NUMBER_SubServiceID;
         TxArray[2U] = 0x5BU;
         TxArray[3U] = 0x00U;
         TxArray[4U] = 0x01U;
         TxArray[5U] = 0x00U;  
         Length      = 6U;
         //! #### Trigger the transmittion with TxId and transmit buffer
         Rte_Call_CddLinTxHandling_Transmit(TxId,
                                            TxArray,
                                            Length);
         //! #### Update the transmit status to pending state
         CddLinDiag_TxMsgStatus[CurrentIndex_Bwd].TxId           = TxId;
         CddLinDiag_TxMsgStatus[CurrentIndex_Bwd].Status         = LinDiagService_Pending;
         CddLinDiag_TxMsgStatus[CurrentIndex_Bwd].ReasonForError = CDD_LIN_NONE; 
         //! #### Change the token phase to backward phase
         LinDiag_CCNAD_TokenPhaseBwd[LinBusIndex_Bwd]            = CONST_CDD_LINDIAG_BACKWARD_TOKEN_PHASE;
      }
      else
      {
         //! ##### Processing CCNAD backward token phase when current FSP ID is equal to zero 
         //! #### Change the token phase to completed phase
         LinDiag_CCNAD_TokenPhaseBwd[LinBusIndex_Bwd] = CONST_CDD_LINDIAG_COMPLETED_TOKEN_PHASE;
         // CCNAD algorithm successfully completed .... Set Failure Status as 0
         //! #### Set Diagnostic error status flag to no error, successfully completed (0U) 
         CddLinDiag_SlavesErrStatus[LinBusIndex_Bwd]  = 0U;
      }
   }
   //! ###### Check the LIN message status is Receive_OK or Receive_Error
   else if (((LinDiagService_Completed == CddLinDiag_TxMsgStatus[CurrentIndex_Bwd].Status)
           && (CDD_LIN_RX_OK == CddLinDiag_TxMsgStatus[CurrentIndex_Bwd].ReasonForError))
           || ((LinDiagService_Error == CddLinDiag_TxMsgStatus[CurrentIndex_Bwd].Status) 
           && (CDD_LIN_RX_ERROR == CddLinDiag_TxMsgStatus[CurrentIndex_Bwd].ReasonForError)))
   {
      CddLinDiag_TxDataInfoBwd[LinBusIndex_Bwd].Byte = 1U;
      CddLinDiag_TxDataInfoBwd[LinBusIndex_Bwd].Mask = 1U;
      //! ##### Findout the last byte and last mask bit from static buffer and copy to local buffer
      for (Index = 0U; Index < CddLinDiag_TxStoredDataBwd[LinBusIndex_Bwd].StoredIndex; Index++)
      {
         if (CddLinDiag_TxStoredDataBwd[LinBusIndex_Bwd].StoredData[Index].LastNAD == LinDiag_NADTxIdBwd[LinBusIndex_Bwd].CurrentIdIndex)
         {
            CddLinDiag_TxDataInfoBwd[LinBusIndex_Bwd].Byte = CddLinDiag_TxStoredDataBwd[LinBusIndex_Bwd].StoredData[Index].LastRsdByte;
            CddLinDiag_TxDataInfoBwd[LinBusIndex_Bwd].Mask = CddLinDiag_TxStoredDataBwd[LinBusIndex_Bwd].StoredData[Index].LastRsdMask;
            break;
         }
         else
         {
            // Do nothing
         }
      }
      //! #### Prepare the transmit buffer for Setting NAD ID based on serial number mask byte & Bit
      CddLinDiag_TxStoredDataBwd[LinBusIndex_Bwd].StoredIndex = 0U;
      Array       = LinDiag_NADTxIdBwd[LinBusIndex_Bwd].NADTxID;
      TxId        = Array[LinDiag_NADTxIdBwd[LinBusIndex_Bwd].CurrentIdIndex];
      TxArray[0U] = LINSlaveNode_CCNAD_ServiceID;
      TxArray[1U] = 0x01U;                                               //Serial Number
      TxArray[2U] = CddLinDiag_TxDataInfoBwd[LinBusIndex_Bwd].Byte;     //Byte
      TxArray[3U] = CddLinDiag_TxDataInfoBwd[LinBusIndex_Bwd].Mask;     //Mask
      TxArray[4U] = 0xFFU;                                               //Invert
      TxArray[5U] = 0x40U + LinDiag_NADTxIdBwd[LinBusIndex_Bwd].CurrentIdIndex + 1U; 
      Length      = 6U;
      //! #### Trigger the transmittion with TxId and transmit buffer
      Rte_Call_CddLinTxHandling_Transmit(TxId,
                                         TxArray,
                                         Length);
      //! #### Update the transmit status to pending state
      CddLinDiag_TxMsgStatus[CurrentIndex_Bwd].TxId           = TxId;
      CddLinDiag_TxMsgStatus[CurrentIndex_Bwd].Status         = LinDiagService_Pending;
      CddLinDiag_TxMsgStatus[CurrentIndex_Bwd].ReasonForError = CDD_LIN_NONE; 
      //! #### Change the token phase to forward phase 
      LinDiag_CCNAD_TokenPhaseBwd[LinBusIndex_Bwd]            = CONST_CDD_LINDIAG_FORWARD_TOKEN_PHASE;
      // Update the byte and mask
      //! #### Update the mask byte and bit to next position
      if (0x80U != ((CddLinDiag_TxDataInfoBwd[LinBusIndex_Bwd].Mask) & (0x80U)))
      {
         CddLinDiag_TxDataInfoBwd[LinBusIndex_Bwd].Mask = (CddLinDiag_TxDataInfoBwd[LinBusIndex_Bwd].Mask << 1U);
      }
      else
      {
         if (CddLinDiag_TxDataInfoBwd[LinBusIndex_Bwd].Byte < 0x4U)
         {
            CddLinDiag_TxDataInfoBwd[LinBusIndex_Bwd].Byte = CddLinDiag_TxDataInfoBwd[LinBusIndex_Bwd].Byte + 1U;
            CddLinDiag_TxDataInfoBwd[LinBusIndex_Bwd].Mask = 0x1U;
         }
         else
         {
            //! #### If mask byte and bit reaches to last postion then Set the mask byte and Bit to 1 (Starting Position)
            CddLinDiag_TxDataInfoBwd[LinBusIndex_Bwd].Byte = 1U;
            CddLinDiag_TxDataInfoBwd[LinBusIndex_Bwd].Mask = 1U;
         }
      }
   }
   //! ###### Check the LIN message status is other than Rx_OK,RX_Error or NO_Response
   else
   {
      //! ##### Change the token phase to completed phase 
      LinDiag_CCNAD_TokenPhaseBwd[LinBusIndex_Bwd] = CONST_CDD_LINDIAG_COMPLETED_TOKEN_PHASE;
      // Failure in CCNAD Algorithm.... Set Failure Status as 4
      //! #### Set diagnostic error status flag to CCNAD algorithm failure (4U) 
      CddLinDiag_SlavesErrStatus[LinBusIndex_Bwd]  = 4U;
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_LinDiagnostics_FinalCCNAD
//!
//! \param   LinBusIndex_Final               Provides the LIN Bus index
//! \param   CurrentIndex_Final              Provides the Current index
//! \param   LinDiag_CCNAD_TokenPhaseFinal   Provides the CCNAD Token Phase Information
//! \param   CddLinDiag_TxDataInfoFinal      Provides the LIN Transmit data buffer
//! \param   LinDiag_NADTxIdFinal            Provides the LIN NAD to Tx ID buffer
//!
//!======================================================================================
static void Cdd_LinDiagnostics_FinalCCNAD(uint8 LinBusIndex_Final,
                                          uint8 CurrentIndex_Final,
                                          ENUM_CDD_LINDIAG LinDiag_CCNAD_TokenPhaseFinal[CDD_LINDIAG_NoOfLINNetworks],
                                          CDDLinDiagByteMaskType  CddLinDiag_TxDataInfoFinal[CDD_LINDIAG_NoOfLINNetworks],
                                          CddLinDiagNADTxID_Type LinDiag_NADTxIdFinal[CDD_LINDIAG_NoOfLINNetworks])
{
   uint8 TxId   = 0U;
   uint8 TxArray[8U];
   uint8 Length = 0U;
   uint8 *Array;

   //! ###### Processing Cdd_LinDiagnostics_FinalCCNAD
   //! ###### Check the transmit message status
   if (LinDiagService_Completed == CddLinDiag_TxMsgStatus[CurrentIndex_Final].Status)
   {
      //! ###### Processing of CCNAD final token phase when transmit message status is in completed state
      CddLinDiag_FspNVData[LinBusIndex_Final][CddLinDia_CurrentFspIndex[LinBusIndex_Final]] = 1U;
      CddLinDia_CurrentFspIndex[LinBusIndex_Final] = CddLinDia_CurrentFspIndex[LinBusIndex_Final] + 1U;
      //! ###### Check the current processing FSP ID
      if (0U != LinDiag_NADTxIdFinal[LinBusIndex_Final].CurrentIdIndex)
      {
         //! ##### Processing of CCNAD final token phase when current FSP ID is not equal to zero
         //! #### Prepare the Transmit buffer for Reading serial Number from Previous NAD ID
         LinDiag_NADTxIdFinal[LinBusIndex_Final].CurrentIdIndex = LinDiag_NADTxIdFinal[LinBusIndex_Final].CurrentIdIndex - 1U;
         Array       = LinDiag_NADTxIdFinal[LinBusIndex_Final].NADTxID; 
         TxId        = Array[LinDiag_NADTxIdFinal[LinBusIndex_Final].CurrentIdIndex];
         TxArray[0U] = LINSlaveNode_ReadDataByID_ServiceID;
         TxArray[1U] = LINSlaveNode_SERIAL_NUMBER_SubServiceID;
         TxArray[2U] = 0x5BU;
         TxArray[3U] = 0x00U;
         TxArray[4U] = 0x01U;
         TxArray[5U] = 0x00U;
         Length      = 6U;
         //! #### Trigger the transmittion with TxId and Transmit buffer
         Rte_Call_CddLinTxHandling_Transmit(TxId,
                                            TxArray,
                                            Length);
         //! #### Update the Transmit status to pending state
         CddLinDiag_TxMsgStatus[CurrentIndex_Final].TxId           = TxId;
         CddLinDiag_TxMsgStatus[CurrentIndex_Final].Status         = LinDiagService_Pending;
         CddLinDiag_TxMsgStatus[CurrentIndex_Final].ReasonForError = CDD_LIN_NONE;
         //! #### Change the Token Phase to Backward Phase 
         LinDiag_CCNAD_TokenPhaseFinal[LinBusIndex_Final]          = CONST_CDD_LINDIAG_BACKWARD_TOKEN_PHASE;
      }
      else
      {
         //! ##### Processing of CCNAD final token phase when current FSP ID is equal to zero  
         //! #### Change the token phase to completed Phase 
         LinDiag_CCNAD_TokenPhaseFinal[LinBusIndex_Final] = CONST_CDD_LINDIAG_COMPLETED_TOKEN_PHASE; // This issue for LIN 4 & 5 (Presently configured only one Diagnostics PDU)
         //! #### Set diagnostic error status flag to No error, successfully completed (0U)
         CddLinDiag_SlavesErrStatus[LinBusIndex_Final]    = 0U;
      }
   }
   else
   {
      //! ###### Processing of CCNAD final token phase when transmit message status is not in completed state
      //! ##### Change the token phase to completed phase
      LinDiag_CCNAD_TokenPhaseFinal[LinBusIndex_Final] = CONST_CDD_LINDIAG_COMPLETED_TOKEN_PHASE;
      // Failure in CCNAD Algorithm.... Set Failure Status as 4  // Assign NAD Frame Error
      //! ##### Set diagnostic error status flag to CCNAD algorithm failure (4U)
      CddLinDiag_SlavesErrStatus[LinBusIndex_Final]    = 4U;
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_LinDiagnostics_CCNADConsistencyCheck
//!
//! \param   LinBusIndex_Cons               Provides the LIN Bus index
//! \param   CurrentIndex_Cons              Provides the Current index
//! \param   LinDiag_CCNAD_TokenPhaseCons   Provides the CCNAD Token Phase Information 
//! \param   LinDiag_NADTxIdCons            Provides the LIN Transmit data buffer
//! \param   CddLinDiag_TxDataInfoCons      Provides the LIN NAD to Tx ID buffer
//!
//!======================================================================================
static void Cdd_LinDiagnostics_CCNADConsistencyCheck(uint8 LinBusIndex_Cons,
                                                     uint8 CurrentIndex_Cons,
                                                     ENUM_CDD_LINDIAG LinDiag_CCNAD_TokenPhaseCons[CDD_LINDIAG_NoOfLINNetworks],
                                                     CddLinDiagNADTxID_Type  LinDiag_NADTxIdCons[CDD_LINDIAG_NoOfLINNetworks],
                                                     CDDLinDiagByteMaskType CddLinDiag_TxDataInfoCons[CDD_LINDIAG_NoOfLINNetworks])
{
   static uint8 LinDiag_ConsistencyCheckSN[CDD_LINDIAG_NoOfLINNetworks][8U] = {{0,0,0,0,0,0,0,0},
                                                                               {0,0,0,0,0,0,0,0},
                                                                               {0,0,0,0,0,0,0,0},
                                                                               {0,0,0,0,0,0,0,0},
                                                                               {0,0,0,0,0,0,0,0}};
   uint8 TxId   = 0U;
   uint8 TxArray[8U];
   uint8 Length = 0U;
   uint8 *Array;

   //! ###### Processing Cdd_LinDiagnostics_CCNADConsistencyCheck
   //! ###### Check the transmit message status
   if (LinDiagService_Completed == CddLinDiag_TxMsgStatus[CurrentIndex_Cons].Status)
   {
      //! ###### Processing of CCNAD final token phase when transmit message status is in completed state
      //! ###### Check Subservice ID in transmit message buffer is reading SerialNumber Subservice ID
      if (LINSlaveNode_SERIAL_NUMBER_SubServiceID == CddLinDiag_TxMsgStatus[CurrentIndex_Cons].SubServiceId)
      {
         //! ##### Processing of SerialNumber sub service 
         //! ##### Prepare the transmit buffer for reading inverted of serialNumber from current NAD ID
         TxId        = CddLinDiag_TxMsgStatus[CurrentIndex_Cons].TxId;   
         TxArray[0U] = LINSlaveNode_ReadDataByID_ServiceID;
         TxArray[1U] = LINSlaveNode_INVERTED_SERIAL_NUMBER_SubServiceID;
         TxArray[2U] = 0x5BU;
         TxArray[3U] = 0x00U;
         TxArray[4U] = 0x01U;
         TxArray[5U] = 0x00U; 
         Length      = 6U;
         //! #### Trigger the transmittion with TxId and transmit buffer
         Rte_Call_CddLinTxHandling_Transmit(TxId,
                                            TxArray,
                                            Length);
          //! #### Update the transmit status to pending state
         CddLinDiag_TxMsgStatus[CurrentIndex_Cons].TxId           = TxId;
         CddLinDiag_TxMsgStatus[CurrentIndex_Cons].Status         = LinDiagService_Pending;
         CddLinDiag_TxMsgStatus[CurrentIndex_Cons].ReasonForError = CDD_LIN_NONE;
         //! #### Copy the received SerialNumber to local static buffer
         LinDiag_ConsistencyCheckSN[LinBusIndex_Cons][0U]         = CddLinDiag_TxMsgStatus[CurrentIndex_Cons].RxData[1U];
         LinDiag_ConsistencyCheckSN[LinBusIndex_Cons][1U]         = CddLinDiag_TxMsgStatus[CurrentIndex_Cons].RxData[2U];
         LinDiag_ConsistencyCheckSN[LinBusIndex_Cons][2U]         = CddLinDiag_TxMsgStatus[CurrentIndex_Cons].RxData[3U];
         LinDiag_ConsistencyCheckSN[LinBusIndex_Cons][3U]         = CddLinDiag_TxMsgStatus[CurrentIndex_Cons].RxData[4U];
         //! #### Change the token phase to consitency check Phase 
         LinDiag_CCNAD_TokenPhaseCons[LinBusIndex_Cons]           = CONST_CDD_LINDIAG_CONSITENCY_TOKEN_PHASE;
      }
      //! ###### Check Subservice ID in transmit message buffer is reading Inverted SerialNumber Subservice ID
      else if (LINSlaveNode_INVERTED_SERIAL_NUMBER_SubServiceID == CddLinDiag_TxMsgStatus[CurrentIndex_Cons].SubServiceId)
      {
         //! ##### Processing of Inverted Serial Number Sub Service 
         if ((0xFFU == (LinDiag_ConsistencyCheckSN[LinBusIndex_Cons][0U])^(CddLinDiag_TxMsgStatus[CurrentIndex_Cons].RxData[1U]))
            && (0xFFU == (LinDiag_ConsistencyCheckSN[LinBusIndex_Cons][1U])^(CddLinDiag_TxMsgStatus[CurrentIndex_Cons].RxData[2U]))
            && (0xFFU == (LinDiag_ConsistencyCheckSN[LinBusIndex_Cons][2U])^(CddLinDiag_TxMsgStatus[CurrentIndex_Cons].RxData[3U]))
            && (0xFFU == (LinDiag_ConsistencyCheckSN[LinBusIndex_Cons][3U])^(CddLinDiag_TxMsgStatus[CurrentIndex_Cons].RxData[4U])))
         {
            //! ##### Processing CCNAD consistency phase when Serial and Inverted Serial numbers are received properly
            //! #### Increment Avaialble FSP count
            CddLinDiag_AvailableSlaves[LinBusIndex_Cons] = CddLinDiag_AvailableSlaves[LinBusIndex_Cons] + 1U;
            //! ##### Check whether available FSP count less than Maximum Number of expected FSP slaves
            if (CddLinDiag_TxDataInfoCons[LinBusIndex_Cons].FinalNAD < CddLinDiag_TxDataInfoCons[LinBusIndex_Cons].MaxNoOfFspNAD)
            {
               //! #### Processing CCNAD Consistency Phase when available FSP count less than Maximum Number of expected FSP slaves
               //! #### Set Mask Byte to 1 and Mask Bit to 0
               CddLinDiag_TxDataInfoCons[LinBusIndex_Cons].Byte = 0x1U;
               CddLinDiag_TxDataInfoCons[LinBusIndex_Cons].Mask = 0x0U; 
               //! #### Prepare the transmit buffer for Setting NAD ID based on serial number mask byte & bit
               TxArray[0U] = LINSlaveNode_CCNAD_ServiceID;
               TxArray[1U] = 0x01U;                                      //Serial Number
               TxArray[2U] = CddLinDiag_TxDataInfoCons[LinBusIndex_Cons].Byte;     //Byte
               TxArray[3U] = CddLinDiag_TxDataInfoCons[LinBusIndex_Cons].Mask;     //Mask
               TxArray[4U] = 0xFFU;                                      //Invert
               TxArray[5U] = CddLinDiag_TxDataInfoCons[LinBusIndex_Cons].FinalNAD + 2U;
               Length      = 6U;
               TxId        = CddLinDiag_TxMsgStatus[CurrentIndex_Cons].TxId;
               //! #### Trigger the transmittion with TxId and transmit buffer
               Rte_Call_CddLinTxHandling_Transmit(TxId,
                                                  TxArray,
                                                  Length);
               //! #### Update the transmit status to pending state
               CddLinDiag_TxMsgStatus[CurrentIndex_Cons].TxId           = TxId;
               CddLinDiag_TxMsgStatus[CurrentIndex_Cons].Status         = LinDiagService_Pending;
               CddLinDiag_TxMsgStatus[CurrentIndex_Cons].ReasonForError = CDD_LIN_NONE;
               //! #### Increment the mask byte and bit
               CddLinDiag_TxDataInfoCons[LinBusIndex_Cons].Mask         = CddLinDiag_TxDataInfoCons[LinBusIndex_Cons].Mask + 1U;
               CddLinDiag_TxDataInfoCons[LinBusIndex_Cons].FinalNAD     = CddLinDiag_TxDataInfoCons[LinBusIndex_Cons].FinalNAD + 1U;
               //! #### Change the token phase to final phase
               LinDiag_CCNAD_TokenPhaseCons[LinBusIndex_Cons]           = CONST_CDD_LINDIAG_FINAL_TOKEN_PHASE;
            }
            else
            {
               //! ##### Processing CCNAD Consistency Phase when available FSP count more than Maximum Number of expected FSP slaves
               //! #### Change the Token Phase to Completed Phase
               LinDiag_CCNAD_TokenPhaseCons[LinBusIndex_Cons] = CONST_CDD_LINDIAG_COMPLETED_TOKEN_PHASE;
               // More than Required FSP Slaves are connected... Set Failure Status as 1
               //! #### Set Diagnostic error status flag to more than expected FSPs connected failure (1U)
               CddLinDiag_SlavesErrStatus[LinBusIndex_Cons]   = 1U; // More than Maximum FSPs
            }
         }
         else
         {
            //! ##### Processing CCNAD consistency phase when Serial and Inverted Serial numbers are not received Properly
            //! #### Change the Token Phase to Completed Phase
            LinDiag_CCNAD_TokenPhaseCons[LinBusIndex_Cons] = CONST_CDD_LINDIAG_COMPLETED_TOKEN_PHASE;
            // Two Slaves with Same Serial Number... Set Failure Status as 4
            //! #### Set Diagnostic error status flag to CCNAD Algorithm failure (4U)
            CddLinDiag_SlavesErrStatus[LinBusIndex_Cons]   = 4U;  // Consistency Check Error
         }
      }
      else
      {
         //! ##### Processing CCNAD consistency Phase when subservice Id other than Serial or Inverted serial number
         //! #### Change the token phase to completed phase
         LinDiag_CCNAD_TokenPhaseCons[LinBusIndex_Cons] = CONST_CDD_LINDIAG_COMPLETED_TOKEN_PHASE;
         //Failure in CCNAD Algorithm.... Set Failure Status as 4
         //! #### Set Diagnostic Error status flag to CCNAD Algorithm failure (4U)
         CddLinDiag_SlavesErrStatus[LinBusIndex_Cons]   = 4U;  // UnKnown Service ID... Not possible Case
      }
   }
   else if (LinDiagService_Error == CddLinDiag_TxMsgStatus[CurrentIndex_Cons].Status)
   {
      //! ###### Processing of CCNAD consistency token phase when transmit message status is not in completed state
      // No response during consistancy check: no LIN slave present OR forward token lost => engage backward 
      //! ##### Check whether response is received or not
      if (CDD_LIN_RX_NO_RESPONSE == CddLinDiag_TxMsgStatus[CurrentIndex_Cons].ReasonForError) 
      {
         //! ##### Processing of CCNAD consistency token phase when nO response is received from slaves
         //! ##### Check the current processing FSP ID
         if (0U != LinDiag_NADTxIdCons[LinBusIndex_Cons].CurrentIdIndex)
         {
            //! ##### Processing of CCNAD consistency token phase when current FSP ID is not equal to Zero   
            //! #### Prepare the transmit buffer for reading serial Number from previous NAD ID 
            LinDiag_NADTxIdCons[LinBusIndex_Cons].CurrentIdIndex = LinDiag_NADTxIdCons[LinBusIndex_Cons].CurrentIdIndex-1U;
            Array = LinDiag_NADTxIdCons[LinBusIndex_Cons].NADTxID;
            TxId = Array[LinDiag_NADTxIdCons[LinBusIndex_Cons].CurrentIdIndex];
            TxArray[0U] = LINSlaveNode_ReadDataByID_ServiceID;
            TxArray[1U] = LINSlaveNode_SERIAL_NUMBER_SubServiceID;
            TxArray[2U] = 0x5BU;
            TxArray[3U] = 0x00U;
            TxArray[4U] = 0x01U;
            TxArray[5U] = 0x00U;  
            Length = 6U;
            //! #### Trigger the transmittion with TxId and transmit buffer
            Rte_Call_CddLinTxHandling_Transmit(TxId,TxArray,Length);
            //! #### Update the transmit status to pending state 
            CddLinDiag_TxMsgStatus[CurrentIndex_Cons].TxId= TxId;
            CddLinDiag_TxMsgStatus[CurrentIndex_Cons].Status=LinDiagService_Pending;
            CddLinDiag_TxMsgStatus[CurrentIndex_Cons].ReasonForError=CDD_LIN_NONE; 
            //! #### Change the token phase to backward phase
            LinDiag_CCNAD_TokenPhaseCons[LinBusIndex_Cons] = CONST_CDD_LINDIAG_BACKWARD_TOKEN_PHASE;
         }
         else
         {
            //! ##### Processing of CCNAD consistency token phase when current FSP ID is equal to zero
            //! #### Change the token phase to completed phase
            LinDiag_CCNAD_TokenPhaseCons[LinBusIndex_Cons] = CONST_CDD_LINDIAG_COMPLETED_TOKEN_PHASE;
            //CCNAD Algorithm Successfully completed .... Set Failure Status as 0
            //! #### Set Diagnostic Error status flag to sucessfully completed status(0U)
            CddLinDiag_SlavesErrStatus[LinBusIndex_Cons] = 0U;
         }
      }
      else // LIN Rx ERROR or LIN RX BUSY
      {
         //! ##### Processing CCNAD consistency phase when subservice Id other than serial or inverted serial number
         //! #### Change the token phase to completed phase
         LinDiag_CCNAD_TokenPhaseCons[LinBusIndex_Cons] = CONST_CDD_LINDIAG_COMPLETED_TOKEN_PHASE;
         // Multiple Slave with same Serial Number
         //! #### Set Diagnostic error status flag to CCNAD algorithm failure (4U)
         CddLinDiag_SlavesErrStatus[LinBusIndex_Cons] = 4U;  // Multiple FSPs with Same Serial Number
      }
   }
   else
   {
      // Do nothing
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Cdd_LinDiagnostics_StartCCNAD
//!
//! \param   LinBusIndex_Start               Provides the LIN bus index
//! \param   CurrentIndex_Start              Provides the current index
//! \param   LinDiag_CCNAD_TokenPhaseStart   Provides the CCNAD token phase information
//! \param   CddLinDiag_TxDataInfoStart      Provides the LIN transmit data buffer
//! \param   LinDiag_NADTxIdStart            Provides the LIN NAD to Tx ID buffer
//!
//!======================================================================================
static void Cdd_LinDiagnostics_StartCCNAD(uint8 LinBusIndex_Start,
                                          uint8 CurrentIndex_Start,
                                          ENUM_CDD_LINDIAG LinDiag_CCNAD_TokenPhaseStart[CDD_LINDIAG_NoOfLINNetworks],
                                          CDDLinDiagByteMaskType CddLinDiag_TxDataInfoStart[CDD_LINDIAG_NoOfLINNetworks],
                                          CddLinDiagNADTxID_Type LinDiag_NADTxIdStart[CDD_LINDIAG_NoOfLINNetworks])
{
   uint8 TxId   = 0U;
   uint8 TxArray[8U];
   uint8 Length = 0U;
   uint8 *Array;

   //! ###### Processing Cdd_LinDiagnostics_StartCCNAD
   //! ###### Check the status of response message
   if (((LinDiagService_Completed == CddLinDiag_TxMsgStatus[LinBusIndex_Start].Status)
      && (CDD_LIN_RX_OK == CddLinDiag_TxMsgStatus[LinBusIndex_Start].ReasonForError))
      || ((LinDiagService_Error == CddLinDiag_TxMsgStatus[LinBusIndex_Start].Status) 
      && (CDD_LIN_RX_ERROR == CddLinDiag_TxMsgStatus[LinBusIndex_Start].ReasonForError)))
   {
      //! ##### Processing of CCNAD start operation when response is received(single slave response) or receive error (Multiple slaves responded)
      //! ##### Check the current index of FSP ID buffer 
      if (LinDiag_NADTxIdStart[LinBusIndex_Start].CurrentIdIndex < (LinDiag_NADTxIdStart[LinBusIndex_Start].MaxNoOfIds - 1U))
      {
         //! ##### Processing of CCNAD start operation when current FSP index less than last index 
         //! #### Prepare the transmit buffer for setting NAD ID based on serial number mask byte & bit
         Array       = LinDiag_NADTxIdStart[LinBusIndex_Start].NADTxID;  
         TxId        = Array[LinDiag_NADTxIdStart[LinBusIndex_Start].CurrentIdIndex];
         TxArray[0U] = LINSlaveNode_CCNAD_ServiceID;
         TxArray[1U] = 0x01U;                                                                  //Serial Number
         TxArray[2U] = CddLinDiag_TxDataInfoStart[LinBusIndex_Start].Byte;                    //Byte
         TxArray[3U] = CddLinDiag_TxDataInfoStart[LinBusIndex_Start].Mask;                    //Mask
         TxArray[4U] = 0xFFU;                                                                  //Invert 
         TxArray[5U] = 0x40U + LinDiag_NADTxIdStart[LinBusIndex_Start].CurrentIdIndex + 1U;
         Length      = 6U;
         //! #### Trigger the transmittion with TxId and transmit buffer
         Rte_Call_CddLinTxHandling_Transmit(TxId,
                                            TxArray,
                                            Length);
         //! #### Update the transmit status to pending state
         CddLinDiag_TxMsgStatus[CurrentIndex_Start].TxId            = TxId;
         CddLinDiag_TxMsgStatus[CurrentIndex_Start].Status          = LinDiagService_Pending;
         CddLinDiag_TxMsgStatus[CurrentIndex_Start].ReasonForError  = CDD_LIN_NONE;
         //LinDiag_NADTxIdStart[LinBusIndex_Start].CurrentIdIndex++; 
         //! #### Change the token phase to forward phase 
         LinDiag_CCNAD_TokenPhaseStart[LinBusIndex_Start]           = CONST_CDD_LINDIAG_FORWARD_TOKEN_PHASE;    
         // Update the Byte and Mask
         //! #### Update the mask byte and bit to next position
         if (0x80U != ((CddLinDiag_TxDataInfoStart[LinBusIndex_Start].Mask) & (0x80U)))
         {
            CddLinDiag_TxDataInfoStart[LinBusIndex_Start].Mask = (CddLinDiag_TxDataInfoStart[LinBusIndex_Start].Mask<<1);
         }
         else
         {
            if (CddLinDiag_TxDataInfoStart[LinBusIndex_Start].Byte < 0x4U)
            {
               CddLinDiag_TxDataInfoStart[LinBusIndex_Start].Byte = CddLinDiag_TxDataInfoStart[LinBusIndex_Start].Byte+1;
               CddLinDiag_TxDataInfoStart[LinBusIndex_Start].Mask = 0x1U;
            }
            else
            {
               CddLinDiag_TxDataInfoStart[LinBusIndex_Start].Byte = 1U;
               CddLinDiag_TxDataInfoStart[LinBusIndex_Start].Mask = 1U;
            }
         }
      }
      else
      {
         //! ##### Processing of CCNAD start operation when current FSP index is equal to last index
         //! #### Prepare the transmit buffer for reading SerialNumber
         Array       = LinDiag_NADTxIdStart[LinBusIndex_Start].NADTxID;
         TxId        = Array[LinDiag_NADTxIdStart[LinBusIndex_Start].CurrentIdIndex];
         TxArray[0U] = LINSlaveNode_ReadDataByID_ServiceID;
         TxArray[1U] = LINSlaveNode_SERIAL_NUMBER_SubServiceID;
         TxArray[2U] = 0x5BU;
         TxArray[3U] = 0x00U;
         TxArray[4U] = 0x01U;
         TxArray[5U] = 0x00U;
         Length      = 6U;
         //! #### Trigger the transmittion with TxId and transmit buffer
         Rte_Call_CddLinTxHandling_Transmit(TxId,TxArray,Length);
         //! #### Update the transmit status to pending state 
         CddLinDiag_TxMsgStatus[CurrentIndex_Start].TxId           = TxId;
         CddLinDiag_TxMsgStatus[CurrentIndex_Start].Status         = LinDiagService_Pending;
         CddLinDiag_TxMsgStatus[CurrentIndex_Start].ReasonForError = CDD_LIN_NONE;
         LinDiag_NADTxIdStart[LinBusIndex_Start].CurrentIdIndex    = 0U;
         //! #### Change the token phase to consitency check phase
         LinDiag_CCNAD_TokenPhaseStart[LinBusIndex_Start]          = CONST_CDD_LINDIAG_CONSITENCY_TOKEN_PHASE;
      }
   }
   else
   {
      //! ##### Processing of CCNAD start operation when no response is received from slaves
      //! #### Change the token phase to completed phase
      LinDiag_CCNAD_TokenPhaseStart[LinBusIndex_Start] = CONST_CDD_LINDIAG_COMPLETED_TOKEN_PHASE;
      //! #### Set Diagnostic error status flag to sucessfully completed status (0U), No FSPs are connected
      CddLinDiag_SlavesErrStatus[LinBusIndex_Start]    = 0U;  // No FSPs are connected
   }
}



//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Appl_LinIfGetLinStatus
//! This function is trigged by LinIf module with Lin Frame Id and status
//!
//! \param   Channel   Providing the information about Lin channel
//! \param   Pid       Providing the information about Lin Frame Identifier
//! \param   Status    Providing the status of Lin Frame
//!
//!======================================================================================
void Appl_LinIfGetLinStatus(NetworkHandleType Channel, 
                            Lin_FramePidType Pid,
                            Lin_StatusType Status)

{

  if((LinDiagService_Pending==LinDiag_CCNADRequestStatus)
  	||(LinDiagService_Pending==LinDiag_PNSNRequestStatus))
  {
   	Appl_FspAssign_LinIfGetLinStatus(Channel,Pid, Status);
  }

  Appl_ScimLinmgr_LinIfGetLinStatus(Channel,Pid, Status);

}


//! @}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
