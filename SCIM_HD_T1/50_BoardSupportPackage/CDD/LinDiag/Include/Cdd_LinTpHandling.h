/*
 ***************************************************************************************
 ***************************************************************************************
 ***
 ***     File: Cdd_LinTpHandling.h
 ***
 ***     Project: SCIM
 ***
 ***
 ***
 ***************************************************************************************
 ***************************************************************************************
 */

 /*!
  ***************************************************************************************
  * \file
  *
  * \ingroup application_swc
  *
  * \subject
  * Definition file for the Cdd_LinTpHandling runnables.
  *
  * \version
  *
  *
  * \{
  ***************************************************************************************
  */
#ifndef Cdd_LinTpHandling_H
#define Cdd_LinTpHandling_H

/*
 **=====================================================================================
 ** Public macros
 **=====================================================================================
 */
//Unused_Parameter as macro
#define Unused_Parameter(x) if(x!=0){}

#define MAX_NUMBER_OF_LINTP_PDUs    (126U)
#define MAX_NUMBER_OF_LINNetworks   (5U)
#define MAX_NUMBER_OF_REQPENDING    (25U)
#define MAX_LINTP_RESP_LENGTH       (16U)
#define MAX_LINTP_REQ_LENGTH        (8U)
#define CDD_LINTP_NONE              (0x0)
#define CDD_LINTP_TX_TRIG           (0x1)
#define CDD_LINTP_TX_PENDING        (0x2)
#define CDD_LINTP_TX_OK             (0x3)
#define CDD_LINTP_RX_PENDING        (0x4)
#define CDD_LINTP_TX_ERROR          (0x5)
#define CDD_LINTP_RX_ERROR          (0x6)
#define CDD_LINTP_TIMERLOADVAL      (0x10)
#define CDD_LINTP_TIMEOUT           (0x0)
#define LIN_MasterReq_PID           (0x3C)
#define LIN_SlaveResp_PID           (0x7D)
#define CddConf_NoTxPdu             (0xFF)
#define CddConf_NoRxPdu             (0xFF)

/*
 **=====================================================================================
 ** Public definitions
 **=====================================================================================
 */


/*
 **=====================================================================================
 ** Public type definitions
 **=====================================================================================
 */

typedef enum
{
   CONST_CDD_LINTP_BUS0 = 0,
   CONST_CDD_LINTP_BUS1 = 1,
   CONST_CDD_LINTP_BUS2 = 2,
   CONST_CDD_LINTP_BUS3 = 3,
   CONST_CDD_LINTP_BUS4 = 4
} ENUM_CDD_LINTP_BUS;

typedef struct
{
   uint8 ValidFlag;
   uint8 ReqData[MAX_LINTP_REQ_LENGTH];
} CddLinReqDataType;

typedef struct
{
   uint8 ValidFlag;
   uint8 RespData[MAX_LINTP_RESP_LENGTH];
} CddLinRespDataType;

typedef struct
{
   uint8 ReqId;
   uint8 PendingFlag;
   uint8 TimeoutVal;
} CddLinDiagReqPendingType;

typedef struct
{
   uint16 TxSrcPduId;
   uint16 TxDstPduId;
   uint16 RxSrcPduId;
   uint8 LinBus;
} CddLinDiagPduMappingType;
/*
 **=====================================================================================
 ** Public data declarations
 **=====================================================================================
 */
 
/*
 **=====================================================================================
 ** Public function declarations
 **=====================================================================================
 */
/* Function declaration */


/*
 **=====================================================================================
 ** End of file
 **=====================================================================================
 */
 /*!
 * \}
  ************************************ End of file **************************************
  */

#endif 
