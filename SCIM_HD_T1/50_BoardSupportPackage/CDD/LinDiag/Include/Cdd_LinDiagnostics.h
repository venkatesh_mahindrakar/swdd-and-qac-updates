/*
 ***************************************************************************************
 ***************************************************************************************
 ***
 ***     File: Cdd_LinDiagnostics.h
 ***
 ***     Project: SCIM
 ***
 ***
 ***
 ***************************************************************************************
 ***************************************************************************************
 */

 /*!
  ***************************************************************************************
  * \file
  *
  * \ingroup application_swc
  *
  * \subject
  * Definition file for the Cdd_LinDiagnostics runnables.
  *
  * \version
  *
  *
  * \{
  ***************************************************************************************
  */
#ifndef Cdd_LinDiagnostics_H
#define Cdd_LinDiagnostics_H

/*
 **=====================================================================================
 ** Public macros
 **=====================================================================================
 */
//Unused_Parameter as macro
#define Unused_Parameter(x) if(x!=0){}

#define ASCII_OFFSET_FOR_NUMBERS        (48U)
#define PART_NUMBER_LENGTH              (12U)
#define SERIAL_NUMBER_LENGTH            (8U)
#define MAX_NUMBER_OF_SLAVENODES        (25U)
#define MAX_NUMBER_OF_TXMsgSTATUS       (50U)

#define LINSlaveNode_SERIAL_NUMBER_SubServiceID          (0x01U)
#define LINSlaveNode_INVERTED_SERIAL_NUMBER_SubServiceID (0x20U)
#define LINSlaveNode_PART_NUMBER_SubServiceID            (0x21U)
#define LINSlaveNode_AssignNAD_ServiceID                 (0xB0U)
#define LINSlaveNode_ReadDataByID_ServiceID              (0xB2U)
#define LINSlaveNode_CCNAD_ServiceID                     (0xB3U)
#define LINSlaveNode_AssignFrameId_ServiceID             (0xB7U)
#define LINSlaveNode_SaveConfig_ServiceID                (0xB6U)
#define LINSlaveNode_AssignNAD_ResponseServiceID         (0xF0U)
#define LINSlaveNode_ReadDataByID_ResponseServiceID      (0xF2U)
#define LINSlaveNode_CCNAD_ResponseServiceID             (0xF3U)
#define LINSlaveNode_AssignFrameId_ResponseServiceID     (0xF7U)
#define LINSlaveNode_SaveConfig_ResponseServiceID        (0xF6U)

#define CDD_LINDIAG_MaxLINNetworks      (7U)
#define CDD_LINDIAG_MaxFSPs             (4U)
#define CDD_LINDIAG_NoOfLINNetworks     (5U)
#define CDD_LINDIAG_NoOfNADTxId         (33U)
#define CDD_LINDIAG_NoOfNADTxId_LIN0    (33U)
#define CDD_LINDIAG_NoOfNADTxId_LIN1    (33U)
#define CDD_LINDIAG_NoOfNADTxId_LIN2    (33U)
#define CDD_LINDIAG_NoOfNADTxId_LIN3    (1U)
#define CDD_LINDIAG_NoOfNADTxId_LIN4    (1U)
#define CDD_LINDIAG_AssignNAD_LIN0_TXID (CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN00_4a2bb011_Tx)
#define CDD_LINDIAG_AssignNAD_LIN1_TXID (CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN01_3d2c8087_Tx)
#define CDD_LINDIAG_AssignNAD_LIN2_TXID (CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN02_a425d13d_Tx)
#define CDD_LINDIAG_AssignNAD_LIN3_TXID (CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN03_d322e1ab_Tx)
#define CDD_LINDIAG_AssignNAD_LIN4_TXID (CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN04_4d467408_Tx)
#define AssignFrameIdInit               (0x00U)
#define SendAssignFrameID               (0x01U)
#define WaitForRespAssignFrameID        (0x02U)
#define SaveConfigRequest               (0x03U)
#define WaitForSaveConfig               (0x04U)
#define DelayTimeAsignFrame             (0x05U)
#define CompletedAsignFrame             (0x06U)
#define CDD_DIAG_INIT                   (0xFFU)
#define CDD_DIAG_IDLE                   (0x0U)
#define CDD_DIAG_PNSN_REQ               (0x1U)
#define CDD_DIAG_PNSN_PENDING           (0x2U)
#define CDD_DIAG_CCNAD_REQ              (0x3U)
#define CDD_DIAG_CCNAD_PENDING          (0x4U)
#define CDD_DIAG_FrameAssign_PENDING    (0x5U)
#define ResponsePendingTimeoutValue     (100U)
#define FspResponsePendingTimeoutValue  (30U)

/*
 **=====================================================================================
 ** Public definitions
 **=====================================================================================
 */


/*
 **=====================================================================================
 ** Public type definitions
 **=====================================================================================
 */

typedef enum
{
   CONST_CDD_LINDIAG_NO_TOKEN_PHASE         = 0,
   CONST_CDD_LINDIAG_INITIAL_TOKEN_PHASE    = 1,
   CONST_CDD_LINDIAG_FORWARD_TOKEN_PHASE    = 2,
   CONST_CDD_LINDIAG_BACKWARD_TOKEN_PHASE   = 3,
   CONST_CDD_LINDIAG_CONSITENCY_TOKEN_PHASE = 4,
   CONST_CDD_LINDIAG_FINAL_TOKEN_PHASE      = 5,
   CONST_CDD_LINDIAG_COMPLETED_TOKEN_PHASE  = 6
} ENUM_CDD_LINDIAG;

typedef struct
{
   uint8 LastRsdByte;
   uint8 LastRsdMask;
   uint8 Byte;
   uint8 Mask;
   uint8 NewNAD;
   uint8 FinalNAD;
   uint8 MaxNoOfFspNAD;
} CDDLinDiagByteMaskType;

typedef struct
{
   uint8 LastRsdByte;
   uint8 LastRsdMask;
   uint8 LastNAD;
} CDDLinDiagLastDatType;

typedef struct
{
   uint8                 StoredIndex;
   CDDLinDiagLastDatType StoredData[CDD_LINDIAG_NoOfNADTxId];
} CDDLinDiagStoredbyteType;

typedef struct
{
   uint8 MaxNoOfIds;
   uint8 CurrentIdIndex;
   uint8 *NADTxID;
} CddLinDiagNADTxID_Type;

typedef struct
{
   uint8                 TxId;
   LinDiagServiceStatus  Status;
   CddLinTp_Status       ReasonForError;
   uint8                 SubServiceId;
   uint8                 RxData[8];
   uint8                 Length;
} CddLinDiagMsgStatusType;


typedef struct
{
 DiagActiveState_T New;
 DiagActiveState_T Prev;
}DiagActiveStateType;

typedef struct
{
   ComMode_LIN_Type    ComMode_LIN1;
   ComMode_LIN_Type    ComMode_LIN2;
   ComMode_LIN_Type    ComMode_LIN3;
   ComMode_LIN_Type    ComMode_LIN4;
   ComMode_LIN_Type    ComMode_LIN5;
   DiagActiveStateType DiagActive;
} Cdd_LinDiagnostics_In_struct_Type;

typedef struct
{
   uint8 PID1;
   uint8 PID2;
   uint8 PID3;
   uint8 PID4;
}stCddLinFspPID_Type;

typedef struct
{
   uint8 CurrentFspIndex;
   uint8 MaxAvailableFSP;
}stCddLinFspIndex_Type;

/*
 **=====================================================================================
 ** Public data declarations
 **=====================================================================================
 */
 
/*
 **=====================================================================================
 ** Public function declarations
 **=====================================================================================
 */
/* Function declaration */
static void Get_RteDataRead_Common(Cdd_LinDiagnostics_In_struct_Type  *pRteDataRead_Common);
static void Cdd_LinDiagnostics_CCNAD_SetInitialNAD(ENUM_CDD_LINDIAG LinDiag_CCNAD_TokenPhaseInitial[CDD_LINDIAG_NoOfLINNetworks]);
static void Cdd_LinDiagnostics_StartCCNAD(uint8 LinBusIndex_Start,
                                          uint8 CurrentIndex_Start,
                                          ENUM_CDD_LINDIAG LinDiag_CCNAD_TokenPhaseStart[CDD_LINDIAG_NoOfLINNetworks],
                                          CDDLinDiagByteMaskType  CddLinDiag_TxDataInfoStart[CDD_LINDIAG_NoOfLINNetworks],
                                          CddLinDiagNADTxID_Type LinDiag_NADTxIdStart[CDD_LINDIAG_NoOfLINNetworks]);
static void Cdd_LinDiagnostics_CCNADConsistencyCheck(uint8 LinBusIndex_Cons,
                                                     uint8 CurrentIndex_Cons,
                                                     ENUM_CDD_LINDIAG LinDiag_CCNAD_TokenPhaseCons[CDD_LINDIAG_NoOfLINNetworks],
                                                     CddLinDiagNADTxID_Type  LinDiag_NADTxIdCons[CDD_LINDIAG_NoOfLINNetworks],
                                                     CDDLinDiagByteMaskType  CddLinDiag_TxDataInfoCons[CDD_LINDIAG_NoOfLINNetworks]);
static void Cdd_LinDiagnostics_FinalCCNAD(uint8 LinBusIndex_Final,
                                          uint8 CurrentIndex_Final,
                                          ENUM_CDD_LINDIAG LinDiag_CCNAD_TokenPhaseFinal[CDD_LINDIAG_NoOfLINNetworks],
                                          CDDLinDiagByteMaskType  CddLinDiag_TxDataInfoFinal[CDD_LINDIAG_NoOfLINNetworks],
                                          CddLinDiagNADTxID_Type LinDiag_NADTxIdFinal[CDD_LINDIAG_NoOfLINNetworks]);
static void Cdd_LinDiagnostics_ForwardCCNADProcess(uint8 LinBusIndex_Fwd,
                                                   uint8 CurrentIndex_Fwd,
                                                   ENUM_CDD_LINDIAG LinDiag_CCNAD_TokenPhaseFwd[CDD_LINDIAG_NoOfLINNetworks],
                                                   CDDLinDiagByteMaskType  CddLinDiag_TxDataInfoFwd[CDD_LINDIAG_NoOfLINNetworks],
                                                   CddLinDiagNADTxID_Type LinDiag_NADTxIdFwd[CDD_LINDIAG_NoOfLINNetworks],
                                                   CDDLinDiagStoredbyteType CddLinDiag_TxStoredDataFwd[CDD_LINDIAG_NoOfLINNetworks]);
static void Cdd_LinDiagnostics_BackwardCCNADProcess(uint8 LinBusIndex_Bwd,
                                                    uint8 CurrentIndex_Bwd,
                                                    ENUM_CDD_LINDIAG LinDiag_CCNAD_TokenPhaseBwd[CDD_LINDIAG_NoOfLINNetworks],
                                                    CDDLinDiagByteMaskType  CddLinDiag_TxDataInfoBwd[CDD_LINDIAG_NoOfLINNetworks],
                                                    CddLinDiagNADTxID_Type LinDiag_NADTxIdBwd[CDD_LINDIAG_NoOfLINNetworks],
                                                    CDDLinDiagStoredbyteType CddLinDiag_TxStoredDataBwd[CDD_LINDIAG_NoOfLINNetworks]);
static uint8 CddLinDiag_TxIdBusIndex(uint8 TxId_Index,
                                     CddLinDiagNADTxID_Type LinDiag_NADTxIdBus[CDD_LINDIAG_NoOfLINNetworks]);
static void CddLinDiag_TxMsgStatusClear(void);
static void Cdd_LinDiagnostics_SlaveNodePN(void);
static void RawFormatToPartNumberASCIIConvertion(uint8 *pIndata);
static void RawFormatToPartNumberFormatConversion(uint8 TxId_RawtoPNF,
                                                  const uint8 *pIndata_RawtoPNF, 
                                                  uint8 Length_RawtoPNF);
static void RawFormatToSerialNumberFormatConversion(uint8 TxId_RawtoSN,
                                                    const uint8 *pIndata_RawtoSN, 
                                                    uint8 Length_RawtoSN);
static void Cdd_LinDiagnostics_Transmit(uint8 TxId, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINTPHANDLING_APPL_VAR) TxData, uint8 Length);
static uint8 Cdd_LinDiagnostics_AssignFrameID(void);
static void Cdd_LinDiag_AsssignFrameInit(uint8 *pState_Init,
                                         stCddLinFspIndex_Type *pFspIndex_Init,
                                         uint8 LinBusIndex_Init);
static void Cdd_LinDiag_AsssignFrameSendFrameId(uint8 *pState_FrameId,
                                                stCddLinFspIndex_Type *pFspIndex_FrameId,
                                                uint8 LinBusIndex_FrameId);
static void Cdd_LinDiag_AsssignFrameWaitForResp(uint8 *pState_Resp,
                                                stCddLinFspIndex_Type *pFspIndex_Resp,
                                                uint8 LinBusIndex_Resp);
static void Cdd_LinDiag_AsssignFrameSaveConfigReq(uint8 *pState_Req,
                                                  stCddLinFspIndex_Type *pFspIndex_Req,
                                                  uint8 LinBusIndex_Req);
static void Cdd_LinDiag_AsssignFrameWaitForSavingConfig(uint8 *pState,
                                                        stCddLinFspIndex_Type *pFspIndex,
                                                        uint8 LinBusIndex);
static void Cdd_LinDiag_AsssignFrameCompleted(uint8 *pState,
                                              stCddLinFspIndex_Type *pFspIndex_FrameComplete);


/*
 **=====================================================================================
 ** End of file
 **=====================================================================================
 */
 /*!
 * \}
  ************************************ End of file **************************************
  */

#endif 
