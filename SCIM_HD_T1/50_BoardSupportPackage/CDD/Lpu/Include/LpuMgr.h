/*
 ***************************************************************************************
 ***************************************************************************************
 ***
 ***     File: LpuMgr.h
 ***
 ***     Project: SCIM
 ***
 ***
 ***
 ***************************************************************************************
 ***************************************************************************************
 */

#ifdef LPU_MGR_H
#error LPU_MGR_H unexpected multi-inclusion
#else
#define LPU_MGR_H

//=====================================================================================
// Private macros
//=====================================================================================

//Unused_Parameter as macro

#define DisableExtIntr \
asm\
(\
" wrteei 0"\
)



//=====================================================================================
// Public declarations
//=====================================================================================


extern volatile uint8 Test_Stop;

extern  unsigned long __RTE_VAR_MEM;
extern  unsigned long __RTE_VAR_SIZE;
extern  unsigned long __RTECOPY_START;
extern  unsigned long __RTECOPY_END;

extern EcuHwFaultValues_T    Rte_Irv_IoHwAb_QM_IO_IrvEcuIoQmFaultStatus;
extern void Disable_PIT(uint8 data);
extern uint8 GpioCtrl[10];
extern LPModeDyntype LPModeDynOutput[2];
extern LPU_ADCMapType ADCMapping[ADC_CHANNELS];


//=====================================================================================
// Private type definitions
//=====================================================================================
typedef struct {
					uint8 Wakupbit;
					uint8 Icuchannel;
					uint8 TransvrPin;
					void (*WakeupNotif)(void) ;
               }ComWakeupChecktype ;




//=====================================================================================
// Public data declarations
//=====================================================================================
#define NO_OF_ADC_HW (2U)
 
//=====================================================================================
// Public function declarations
//=====================================================================================

void CDD_WakeupStart(void);
extern void CanTrcvWakeUpNotification_CAN2(void);
extern void CanTrcvWakeUpNotification_CAN3(void);
extern void CanTrcvWakeUpNotification_CAN4(void);
extern void  LINWakeUpNotification_LIN1(void);
extern void  LINWakeUpNotification_LIN2(void);
extern void  LINWakeUpNotification_LIN3(void);
extern void  LINWakeUpNotification_LIN4(void);
extern void  LINWakeUpNotification_LIN5(void);
//=====================================================================================
// End of file
//=====================================================================================
#endif 
