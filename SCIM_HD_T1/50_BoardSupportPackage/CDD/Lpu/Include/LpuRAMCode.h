/*
 ***************************************************************************************
 ***************************************************************************************
 ***
 ***     File: LpuRAMCode.h
 ***
 ***     Project: SCIM
 ***
 ***
 ***
 ***************************************************************************************
 ***************************************************************************************
 */

#ifdef LPURAMCODE_H
#error LPURAMCODE_H unexpected multi-inclusion
#else
#define LPURAMCODE_H
//=====================================================================================
// Included header files
//=====================================================================================
#include"CanSM.h"

#include"CanNM.h"

#include "Adc_CfgDefines.h"


/**********************************************************************************************************************
  Define
**********************************************************************************************************************/
/*
00(0000) 0 : RESET
01(0001) 1 : Reserved
02(0010) 2 : SAFE
03(0011) 3 : DRUN
04(0100) 4 : RUN0
05(0101) 5 : RUN1
06(0110) 6 : RUN2
07(0111) 7 : RUN3
08(1000) 8 : Reserved
09(1001) 9 : Reserved
10(1010) A : STOP0
11(1011) B : Reserved // LPU_RUN
12(1100) C : Reserved // LPU_STOP
13(1101) D : STANDBY0
14(1110) E : Reserved // LPU_STANDBY
15(1111) F : Reserved
*/
//=======================================================================================
// Public macros
//=======================================================================================
#define POWER_MODE_DRUN       0x30000000
#define POWER_MODE_STANDBY    0xD0000000

#define POWER_MODE_KEY        0x00005AF0
#define POWER_MODE_INVERT_KEY 0x0000A50F

#define ADI_CHANNELS 16 //12
#define DAI_CHANNELS 2

#define ADC_CHANNELS ADI_CHANNELS+DAI_CHANNELS+3

#define RUNFROMRAM      2
#define RUNFROMFLASH    3

#define LPU_RTC_PERIOD_1ms    128u
#define LPU_RTC_PERIOD_2ms   (LPU_RTC_PERIOD_1ms*2u)
#define LPU_RTC_PERIOD_10ms   (LPU_RTC_PERIOD_1ms*10u)
#define LPU_RTC_PERIOD_40ms   (LPU_RTC_PERIOD_1ms*40u)
#define LPU_RTC_PERIOD_50ms   (LPU_RTC_PERIOD_1ms*50u)
#define LPU_RTC_PERIOD_60ms   (LPU_RTC_PERIOD_1ms*60u)

#define LPU_RTC_PERIOD        (LPU_RTC_PERIOD_1ms*90u)

//#define LPU_RTC_PERIOD        (LPU_RTC_PERIOD_1ms*50u)

//#define LPU_RTC_PERIOD_HIBERNATE  (LPU_RTC_PERIOD_1ms*60u)
//#define LPU_RTC_PERIOD_HIBERNATE  (LPU_RTC_PERIOD_1ms*100u)
//#define LPU_RTC_PERIOD_HIBERNATE  (LPU_RTC_PERIOD_1ms*200u)
#define LPU_RTC_PERIOD_HIBERNATE_LOW  (LPU_RTC_PERIOD_1ms*160u)
#define LPU_RTC_PERIOD_HIBERNATE_HIGH  (LPU_RTC_PERIOD_1ms*5u)

//#define LPU_RTC_PERIOD_ADI_PULLUP (LPU_RTC_PERIOD_1ms*3u) // (LPU_RTC_PERIOD_1ms*10u)
#define LPU_RTC_PERIOD_ADI_PULLUP (LPU_RTC_PERIOD_1ms*10u) // (LPU_RTC_PERIOD_1ms*10u)

// define GPIO
#define GPIO_EXTWDG_WDI    13u

#define GPIO_LIN1_EN       126u
#define GPIO_LIN2_EN       127u
#define GPIO_LIN3_EN       74u
#define GPIO_LIN4_EN       64u
#define GPIO_LIN5_EN       15u
#define GPIO_LIN6_EN       14u
#define GPIO_LIN7_EN       71u
#define GPIO_LINSECU_EN    44u

#define GPIO_LIN1_RXD      101u
#define GPIO_LIN2_RXD      129u
#define GPIO_LIN3_RXD      105u
#define GPIO_LIN4_RXD      103u
#define GPIO_LIN5_RXD      39u
#define GPIO_LIN6_RXD      91u
#define GPIO_LIN7_RXD      131u
#define GPIO_LINSECU_RXD   19u

#define GPIO_CAN1_STB      148u
#define GPIO_CAN2_STB      2u
#define GPIO_CAN3_STB      89u
#define GPIO_CAN4_STB      125u
#define GPIO_CAN5_STB      65u
#define GPIO_CAN6_STB      66u

#define GPIO_CAN1_RX       47u
#define GPIO_CAN2_RX       99u
#define GPIO_CAN3_RX       26u
#define GPIO_CAN4_RX       43u
#define GPIO_CAN5_RX       73u
#define GPIO_CAN6_RX       17u

#define GPIO_12V_PARKED_OUT   143u
#define GPIO_12V_LIVING_OUT   142u

#define GPIO_12V_DCDC_EN      12u

#define GPIO_WETTING_PULLUP   88u
#define GPIO_LOWPOWER_PULLUP  92u
#define  GPIO_LFIC_NRST       108u
#define  GPIO_RFIC_NRST       106u


#define GPIO_ADI1          25u
#define GPIO_ADI2          81u
#define GPIO_ADI3          83u
#define GPIO_ADI4          85u
#define GPIO_ADI5          24u
#define GPIO_ADI6          82u
#define GPIO_ADI7          84u
#define GPIO_ADI8          50u
#define GPIO_ADI9          52u
#define GPIO_ADI10         51u
#define GPIO_ADI11         54u
#define GPIO_ADI12         53u
#define GPIO_ADI13         94u
#define GPIO_ADI14         77u
#define GPIO_ADI15         96u
#define GPIO_ADI16         95u

#define GPIO_DAI1          10u
#define GPIO_DAI2          11u

#define GPIO_PwrSupply     86u
#define GPIO_12V     20u

#define GPIO_12VLIVING_MON     56u
#define GPIO_12VPARKED_MON     55u

#define GPIO_DOBHS1_OUT     146u
#define GPIO_DOBHS2_OUT     147u
#define GPIO_DOBHS3_OUT     87u
#define GPIO_DOBHS4_OUT     145u
#define NO_OF_ADC_HW        (2U)


#ifndef FBL_IOS
# define FBL_IOS(type, base, offset) (*((volatile type *)((base) + (offset))))
#endif

#define FBL_SIUL2_BASE     0xFFFC0000ul      /**< SIU lite base address */

#define PIT (*(volatile struct PIT_tag *) 0xFFF84000UL)
#define GPR (*(volatile struct GPR_tag *) 0xFFF94000UL)
#define WKPU (*(volatile struct WKPU_tag *) 0xFFF98000UL)
# define FBL_SIUL2_GPDO(x)    FBL_IOS(uint8,  FBL_SIUL2_BASE, 0x1300ul + (x)) 
# define FBL_SIUL2_MSCR(x)    FBL_IOS(uint32, FBL_SIUL2_BASE, 0x0240ul + ((x) * 4ul))

//=====================================================================================
// Private type definitions
//=====================================================================================
typedef volatile uint32 vuint32_t;

enum{
   DCDC_EN,
   LIVING_OUT,
   PARKED_OUT,
   DOBHS1_OUT,
   DOBHS2_OUT,
   DOBHS3_OUT,
   DOBHS4_OUT,
   LFIC_NRST,
   LOWPOWER_PULLUP,
   WETTING_PULLUP,
   Max_Gpio
};
typedef enum
{
   NONE=255,
   WAKEUP1=0,
   WAKEUP2,
   WAKEUPTOFLASH,
   VAP2LPU
}Wakeuptimetype;

extern uint8 Wdg_PIN;
extern uint32 LPU_TimeLogCounter;


typedef  struct 
{
   vuint32_t  :12;
   vuint32_t VALID:1;               /* Used to notify when the data is valid (a new value has been written). It is automatically cleared when data is read. */
   vuint32_t OVERW:1;               /* Overwrite data */
   vuint32_t RESULT:2;
   vuint32_t CDATA:16;              /* Converted Data 11:0. */
} ADC_ResultType;

typedef struct
{
   uint16 ThresholdHigh;
   uint16 ThresholdLow;
   Boolean isActive;
}AdiWakeUpConfigType;

typedef struct
{
   uint8 ADINumber;
   uint8 ADCSource;
   uint8 ANNumber;
   uint16 GPIO;
}LPU_ADCMapType;

typedef struct
{
   uint8 VehicleMode;
	uint8 FSC_OperatingMode;
	uint8 LPPullUpAct_Living;		//pullup activation living
	uint8 LPPullUpAct_Parked;		//pullup activation parked
	uint8 LP12VOutputAct_Living;	//12V output Activation living
	uint8 LP12VOutputAct_Parked;	//12V output Activation living
	uint8 DAI_Installed;
   uint8 Dobhs_Act[4];
	uint8 SecurityLIN;				//LIN8 Active
	uint8 IsReduced;
   uint8 CANAct[6];
   uint8 LINAct[7];
   uint8 Dobhs_Status[4];
	AdiWakeUpConfigType AdiWakeUp[ADC_CHANNELS];
}VapToLpuRAMType;

extern uint32 LIN8_Wakeup;
typedef struct {
   uint8 RtcTime;
   uint8 LivingPullupOut;
	uint8 ParkedPullupOut;
}LPModeDyntype; 
//=====================================================================================
// Public data declarations
//=====================================================================================
extern LPModeDyntype LPModeDynOutput[];
extern VapToLpuRAMType VapToLpuRAM;

extern uint32 LIN8_Wakeup;
extern AdiWakeUpConfigType AdiWakeUpConfig[];
extern Wakeuptimetype WakeupTimerFunc;
extern uint8 GpioCtrl[];
extern uint32 Wakeup_Status;
extern uint8 ADCSamplingEn[NO_OF_ADC_HW][ADC_NCMR_REGS];
//=====================================================================================
// Public function prototypes
//=====================================================================================
void RteVar_CopytoBuffer(void);
void RteVar_CopyfromBuffer(void);
void DisableInternalWatchdog(void);
//=====================================================================================
// Public function prototypes
//=====================================================================================
#endif 
