/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  LpuRAMcode.c
 *           Config:  D:/SCIM_LPu/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  LpuRAMcode
 *  Generation Time:  2020-09-28 15:51:00
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <LpuRAMcode>
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
//!======================================================================================
//! \file LpuRAMcode.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup Platform 
//! @{
//! @addtogroup CDD 
//! @{
//! @addtogroup LpuMgr
//! @{
//! @addtogroup LpuRAMcode
//! @{
//!
//! \brief
//! LpuRAMcode.
//! ASIL Level : QM.
//! This module implements the logic for the LpuRAMcode.
//!======================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "IoHwAb_Adc.h"
#include "Rte_IoHwAb_QM_IO_Type.h"
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
#include "LpuRAMCode.h" 
#include "Mpc5746c.h" 
extern void _start(void);
#define resetentrypoint (unsigned long) _start
#define EnableIntr() \
asm\
(\
" wrteei 1"\
)
//Macro for Assembly code  asm(" wrteei 1");
#define DisableIntr() \
asm\
(\
" wrteei 0"\
)
//Macro for Assembly code  asm(" wrteei 0");
#define GpioOutput_Init(i)   SIUL2.MSCR[GpioPin_No[i]].R	       = 0x02000000;
#define GpioOutput_Write(i)  SIUL2.GPDO[GpioPin_No[i]].B.PDO_4n = GpioCtrl[i];
static volatile uint16           WakeupTrigger      = 1u;
                uint8            Wdg_PIN            = 1u;
                uint32           LPU_TimeLogCounter = 0U;
                uint32           LIN8_Wakeup;
                VapToLpuRAMType  VapToLpuRAM;
                Wakeuptimetype   WakeupTimerFunc;
/**Temporary AdiWakeUpConfig parameter** To be filled from P1WMD parameter in LPUMgr*/
AdiWakeUpConfigType AdiWakeUpConfig[]        =   { {100,0,FALSE},
																 {100,0, FALSE},
																 {100,0, FALSE},
																 {100,0, FALSE},
																 {100,0, FALSE},
																 {100,0, FALSE},
																 {100,0, FALSE},
																 {100,0, FALSE},
																 {100,0, FALSE},
																 {100,0, FALSE},
																 {100,0, FALSE},
																 {100,0, FALSE},
																 {100,0, FALSE},
																 {100,0, FALSE},
																 {100,0, FALSE},
																 {100,0, FALSE}};
																																		
static uint16      GpioPin_No[Max_Gpio]       = { GPIO_12V_DCDC_EN,
																GPIO_12V_LIVING_OUT,
																GPIO_12V_PARKED_OUT,
																GPIO_DOBHS1_OUT,
																GPIO_DOBHS2_OUT,
																GPIO_DOBHS3_OUT,
																GPIO_DOBHS4_OUT,
																GPIO_LFIC_NRST,
																GPIO_LOWPOWER_PULLUP,
																GPIO_WETTING_PULLUP} ;
uint32      Wakeup_Status                    = 0u;
											// ADI number,   ADC Source, ANnumber,  GPIO no.


LPU_ADCMapType ADCMapping[ADC_CHANNELS]    	=  {{1,           0,          3,           GPIO_ADI1},
                                                         {2,            0,          11,          GPIO_ADI2},
                                                         {3,            0,          32,          GPIO_ADI3},
                                                         {4,            0,          34,          GPIO_ADI4},
                                                         {5,            0,          4,           GPIO_ADI5},
                                                         {6,            0,          12,          GPIO_ADI6},
                                                         {7,            0,          33,          GPIO_ADI7},
                                                         {8,            1,          2,           GPIO_ADI8},
                                                         {9,            1,          4,           GPIO_ADI9},
                                                         {10,           1,          3,           GPIO_ADI10},
                                                         {11,           1,          6,           GPIO_ADI11},
                                                         {12,           1,          5,           GPIO_ADI12},
                                                         {13,           1,          80,          GPIO_ADI13},
                                                         {14,           1,          88,          GPIO_ADI14},
                                                         {15,           1,          64,          GPIO_ADI15},
                                                         {16,           1,          72,          GPIO_ADI16},
                                                         {17,           1,          45,          GPIO_DAI1}, //DAI1
                                                         {18,           1,          46,          GPIO_DAI2},  //DAI2
                                                         {19,				0, 			35,		    GPIO_PwrSupply},			// threshold 16V->->1.585v(divider)
                                                         {20,				1, 			8,			    GPIO_12VLIVING_MON},		//	Threshold 8V 
                                                         {21,				1, 			7,				 GPIO_12VPARKED_MON}};		// Threshold 8V

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//#define LpuRAMcode_START_SEC_CODE
//#include "LpuRAMcode_MemMap.h"  PRQA S 5087  MD_MSR_19.1 
/**********************************************************************************************************************
 *
 * Runnable Entity Name: LPU_RUN_Function
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * This runnable is never executed by the RTE.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: LPU_RUN_Function_doc
 *********************************************************************************************************************/
uint8 GpioCtrl[Max_Gpio]={0,0,0,0,0,0,0,0,0,0};
LPModeDyntype LPModeDynOutput[2]={{0,0,0},{0,0,0}};
#pragma section CODE ".Core1start"
//********************************************************************************
// Start of Low Power RAM code section
//********************************************************************************
void LPU_RUN_Function(void);
static void WakeupCheck_Event(void);
static void WakeupCheck_AdcPolling(void);
static void ChangePowerMode(const uint32 targetMode);
static void BatteryVoltagemonitor(void); 
//void DebugStopFunc(void);
static void LpuSw_GpioOut_Static(void);
static void LPU_InitPortSetting(void);
static void LPU_ToggleExternalWatchdogInputPin(void);
static void LPU_EnableModules(uint8 enable);
static void ADI_Processing(void);
static void DRUN_Exit(void);
static void Voltage12V_monitor(void);
static void RTC_Config(void );
static void LpuSw_GpioOut_Dyn(void);
static void (*Jump2AUTOSAR)(void);
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the LPU_RUN_Function
//!
//!======================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
void LPU_RUN_Function(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: LPU_RUN_Function
 *********************************************************************************************************************/
   while (MC_ME.GS.B.S_MTRANS != 0U) // wait for transition done
   {
      //Do nothing
   }
	//! ##### Disabling the internal watchdog logic : 'DisableInternalWatchdog()'
	DisableInternalWatchdog();
	//! ##### Processing LpuSw_GpioOut_Static logic : 'LpuSw_GpioOut_Static()' 
	LpuSw_GpioOut_Static();
	//! ##### Processing the initialize the port setting of LPU logic: 'LPU_InitPortSetting()' 
	LPU_InitPortSetting();
	//! ##### Processing the toggle external watchdog input pin of LPU logic : 'LPU_ToggleExternalWatchdogInputPin()'
	LPU_ToggleExternalWatchdogInputPin();
	//! ##### Processing the enable modules of LPU logic: 'LPU_EnableModules()'
	LPU_EnableModules(1u);  
	//! ##### Processing the wake up logic : 'WakeupCheck_Event()'
	WakeupCheck_Event();
	//! ##### Processing the WakeupCheck_AdcPolling : 'WakeupCheck_AdcPolling()'
	WakeupCheck_AdcPolling();
	//Voltage12V_monitor();
		//! ##### Processing the LpuSw_GpioOut_Dyn logic : 'LpuSw_GpioOut_Dyn()'
	LpuSw_GpioOut_Dyn();
	//! ##### Processing to configure the RTC logic : 'RTC_Config()'
	RTC_Config();

	//! ##### Processing the  DRUN exit logic: 'DRUN_Exit()'
	DRUN_Exit();
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the LpuSw_GpioOut_Static
//!
//!======================================================================================
static void LpuSw_GpioOut_Static(void)
{
	uint8 index = 0;
	//! ###### This function processes the Gpio output initialisation and writing
	//! ##### Initialising all the Gpio 
	for(index=0;index<Max_Gpio;index++)
	{
		//! #### Processing GpioOutput_Init logic : 'GpioOutput_Init()'
		GpioOutput_Init(index);
		//! #### Processing GpioOutput_Write logic : 'GpioOutput_Write()'
		GpioOutput_Write(index);
	}
	PMCDIG.RDCR.B.PAD_KEEP_EN = 0x0;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the RTC_Config
//!
//!======================================================================================
static void RTC_Config(void)
{
   //! ###### Processing configuration for RTC
   //! ##### Disable RTC Timer
   RTC.RTCC.B.CNTEN  = 0U;
   RTC.RTCC.B.APIEN  = 0U;
   //RTC.RTCC.B.FRZEN = 1; // Stop Cnt in debug mode
   RTC.RTCS.B.APIF   = 1U; // Clear API Interrupt Flag
   RTC.RTCC.B.CLKSEL = 1U;
   RTC.RTCC.B.APIIE  = 0U;
	//! ##### Updating LPU_TimeLogCounter
	if (LPU_TimeLogCounter != 0U)	// To check wakeup from STANDBY
	{
      if((uint8)WakeupTimerFunc < 2U)
      {
         LPU_TimeLogCounter += LPModeDynOutput[WakeupTimerFunc].RtcTime;
      }
	}
	else
	{
		LPU_TimeLogCounter = 1U; 
	}
   //! ##### If the wakeupTimer is WAKEUP2 swap it with WAKEUP1
	if (WakeupTimerFunc == WAKEUP2)
	{
		WakeupTimerFunc = WAKEUP1;
	}
	//! ##### Else update wakeupTimer with WAKEUP2
	else
	{
		WakeupTimerFunc = WAKEUP2;
	}
   RTC.APIVAL.R = (uint32)LPModeDynOutput[WakeupTimerFunc].RtcTime*120U; // Low -> High
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the ChangePowerMode
//!
//! \param   *targetMode   Providing the current value
//!
//!======================================================================================
static void ChangePowerMode(const uint32 targetMode)
{
	//! ##### Processing to change the power mode and waiting for the transition to occur successfully
   MC_ME.MCTL.R = (targetMode | (uint32)POWER_MODE_KEY);
   MC_ME.MCTL.R = (targetMode | (uint32)POWER_MODE_INVERT_KEY);
   while(MC_ME.GS.B.S_MTRANS != 0U)
   {
      //Do nothing
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the DisableInternalWatchdog
//!
//!======================================================================================
void DisableInternalWatchdog(void)
{
   /*Disable Watchdog*/
   //! ##### Disable the watchdog 
   SWT_0.SR.R     = 0xC520; //clear soft lock bit 1/2
   SWT_0.SR.R     = 0xD928; //clear soft lock bit 2/2
   SWT_0.CR.B.WEN = 0U;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the BatteryVoltagemonitor
//!
//!======================================================================================
static void BatteryVoltagemonitor(void)
{
   //! ###### Processing voltage monitoring of battery
   SIUL2.MSCR[GPIO_PwrSupply].R = 0x00480000;  //ADI#35	
   //ADC0_S[14] -->ADC 35 channel
   //   ADC_0.MCR.B.PWDN     = 0;     // Enable SAR_ADC
   ADC_0.MCR.B.CTU_MODE = 0U;     // CTU mode is off
   ADC_0.MCR.B.ADCLKSEL = 1U;     // ADC clock frequency is equal to bus clock = 80MHz
   ADC_0.MCR.B.PWDN     = 0U;     // Enable SAR_ADC
   ADC_0.NCMR0.R        = 0x00000000; // nothing to set
   ADC_0.NCMR1.R        = 0x00000008;   // Enable conversion on channel ADC_35
   ADC_0.NCMR2.R        = 0x00000000; // nothing to set
   ADC_0.MCR.B.NSTART   = 1U;
   //! ##### Checking conditions to wakeup trigger
   while ((ADC_0.MSR.B.ADCSTATUS > 0u) || (ADC_0.MSR.B.NSTART > 0u))
   {
      //Do nothing
   }
   //! ##### Processing the conditions for Wakeup Trigger
   if ((ADC_0.CDR[35].B.VALID > 0u))
   {
      if ((VapToLpuRAM.IsReduced == FALSE)&&((ADC_0.CDR[35].B.CDATA) < 324U))	//16v  ->1.585v(divider) -->324 Threshold value shall be modified
      {
         WakeupTrigger = RUNFROMFLASH; //to wakeup to DRUN flash
      }
      if ((VapToLpuRAM.IsReduced == TRUE)&&((ADC_0.CDR[35].B.CDATA) > 324U))	//16v ->1.585Threshold value shall be modified
      {
         WakeupTrigger = RUNFROMFLASH; //to wakeup to DRUN flash
      }
   }
   SIUL2.MSCR[GPIO_PwrSupply].R = 0x00000000;  
//   ADC_0.MCR.B.PWDN	 = 1;	  // Disable SAR_ADC
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Voltage12V_monitor
//!
//!======================================================================================
static void Voltage12V_monitor(void)
{
	//! ##### Check the vehicle mode is equal to Living or Accessory and updating the registers
	if (((VehicleMode_Living == VapToLpuRAM.VehicleMode)
	   || (VehicleMode_Accessory == VapToLpuRAM.VehicleMode))
	   && (VapToLpuRAM.LP12VOutputAct_Living == TRUE))
	{
		SIUL2.MSCR[GPIO_12VLIVING_MON].R = 0x00480000;  //ADI#35
		SIUL2.MSCR[GPIO_12VPARKED_MON].R = 0x00480000;	//ADI#35
		//PArked -->ADC 7 channel 
   	//Living -->ADC 8 channel 
		//! ##### Enabling the SAR_ADC
   	ADC_1.MCR.B.PWDN     = 0U;     // Enable SAR_ADC
		//! ##### Switching of the CTU mode
   	ADC_1.MCR.B.CTU_MODE = 0U;     // CTU mode is off
		//! ##### Setting the ADC clock frequency
   	ADC_1.MCR.B.ADCLKSEL = 1U;     // ADC clock frequency is equal to bus clock = 80MHz
		//! ##### Enabling the conversion on channel ADC_35
   	ADC_1.NCMR0.R        = 0x00000180;   // Enable conversion on channel ADC_35
   	ADC_1.MCR.B.NSTART   = 1U;
   	while ((ADC_1.MSR.B.ADCSTATUS > 0u) || (ADC_1.MSR.B.NSTART > 0u))
      {
         //Do nothing
      }
      if (ADC_1.CDR[8].B.VALID > 0u)
   	{
         if ((ADC_1.CDR[8].B.CDATA) < 653)	//<8v  -> Threshold value shall be modified
         {
            WakeupTrigger = RUNFROMFLASH; //to wakeup to DRUN flash
         }
      }   
   	ADC_1.MCR.B.PWDN  = 1U;     // Disable SAR_ADC
		SIUL2.MSCR[GPIO_12VLIVING_MON].R = 0x00000000;  //ADI#35
		SIUL2.MSCR[GPIO_12VPARKED_MON].R = 0x00000000;	//ADI#35
	}
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the WakeupCheck_Event
//!
//!======================================================================================
static void WakeupCheck_Event(void)
{
// check Wakeup/Interrupt Status Flag Register to see the reason of wakeup
 //if ( (WKPU.WISR.R & 0x1) == 0x0) // Wakup by other than API
 //! ##### Check the wake up by other than API
	if ((WKPU.WISR.R & (0x09361120U|LIN8_Wakeup)) > 0u) // Wakup by other than API
	{ 
		//! #### Enable the modules of LPU : 'LPU_EnableModules()'
		LPU_EnableModules(0u);
		Wakeup_Status  = WKPU.WISR.R; // Save wakeup sources
		WKPU.WRER.R	 = 0x00000000; // disable all wakeup interrupt
		WKPU.WISR.R	 = 0xFFFFFFFFU; // clear isr
		MC_ME.CADDR1.R = resetentrypoint; // set start address to FLASH
		MC_ME.DRUN_MC.B.FLAON	 = 3u; // Flash is in normal mode and available for use
		do
		{
			//! #### Process the change power mode logic: 'ChangePowerMode()'
			ChangePowerMode(POWER_MODE_DRUN);
		}
		while (MC_ME.GS.B.S_CURRENT_MODE != 0x3);
		//! #### Processing the wait until main domain is powered
		while (LPU.SR.B.PD2_PWR_STAT == 0U) 
		{
		 //Do nothing
		}
		while (MC_ME.GS.B.S_FLA != 0x3)
		{
		 //do nothing
		}
		WakeupTrigger = 0U;
		WakeupTimerFunc = WAKEUPTOFLASH;
		//! ####  Go to Flash code(AUTOSAR)
		Jump2AUTOSAR = (void (*)(void))resetentrypoint;
		//! #### Jump to AUTOSAR logic : 'Jump2AUTOSAR()'
		Jump2AUTOSAR();
		// will not execute below code
	}
	//! ##### Processing the Wakeup by API
	else
	{
	}
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the DRUN_Exit
//!
//!======================================================================================
static void DRUN_Exit(void)
{
   //! ##### Process the enable modules of LPU logic : 'LPU_EnableModules()'
   LPU_EnableModules(0u);
   //! ##### Check the status of wake up trigger
   if (WakeupTrigger <= RUNFROMRAM) //To decide wakeup to DRUN(RAM) or DRUN(Flash)
   {
      // wakeup by RTC, stay in LPU
      MC_ME.CADDR1.R          = 0x40000000; // RAM
	   //! #### Check the status of vehicle mode is 'Hibernate' and process the wakeup status register accordingly
      if (VehicleMode_Hibernate == VapToLpuRAM.VehicleMode)
      {
         WKPU.WISR.R   = 0xFFFFFFFFU; //clear wakeup status
         WKPU.WRER.R   = 0x00020001U; // only CAN2(WKPU[17]) & RTC(WKPU[0])
         WKPU.WIREER.R = 0x00000001U; //rising edge detection
         WKPU.WIFEER.R = 0x00020000U; //falling edge detection
      }
      else
      {
         WKPU.WISR.R   = 0xFFFFFFFFU; //clear wakeup status
         WKPU.WRER.R   = 0x09361121U|LIN8_Wakeup;//LIN8 -->SecurityLIN
         WKPU.WIREER.R = 0x08000001; //rising edge detection
         WKPU.WIFEER.R = 0x01361120U|LIN8_Wakeup; //falling edge detection
      }
   }
	//! ##### Processing the wakeup by ADI
   else
   {
      Wakeup_Status = 0U;
      //! #### Set the start address to Flash
      MC_ME.CADDR1.R = resetentrypoint; // set start address to FLASH
      MC_ME.DRUN_MC.B.FLAON   = 3u; // Flash is in normal mode and available for use
      do
      {
		 //! #### Process the  change power mode logic : 'ChangePowerMode()'
         ChangePowerMode(POWER_MODE_DRUN);
      }
      while (MC_ME.GS.B.S_CURRENT_MODE != 0x3);
		//! #### Procesing the wait until the main domain is powered
      while (LPU.SR.B.PD2_PWR_STAT == 0U) // wait until Main domain is powered. 
      {
         //Do nothing
      }
      while (MC_ME.GS.B.S_FLA != 0x3)
      {
         //Do Nothing
      }
      WakeupTrigger = 0U;
      WakeupTimerFunc = WAKEUPTOFLASH;
      //asm(" wrteei 1"); // Enable Interrupt
      SIUL2.GPDO[76].B.PDO_4n = 0; // Debug P133 : LPU issue test code
		//! ####  Go to Flash code(AUTOSAR)
		Jump2AUTOSAR = (void (*)(void))resetentrypoint;
		//! #### Jump to AUTOSAR logic: 'Jump2AUTOSAR()'
		Jump2AUTOSAR();
		// will not execute below code
   }
   MC_ME.STANDBY_MC.B.PDO     = 0U;
   MC_ME.STANDBY_MC.B.FIRCON  = 0U;
   MC_ME.STANDBY_MC.B.SIRCON  = 1U;
	//! ##### Processing wait for getting stable clock
   while(MC_ME.GS.B.S_SIRCON == 0U) 
   {
      // Do nothing
   }   
	//! ##### Processing the PAD-KEEP registers
   PMCDIG.RDCR.B.PAD_KEEP_EN     = 1U;
   PMCDIG.RDCR.B.RD256_RET       = 1u;
   PMCDIG.MCR.B.LVD_PD2_COLD_REE = 0U; // Before entering STANDBY0 Mode, PMCDIG_MCR[LVD_PD2_COLD_REE] bit is programmed to 0.
   RTC.RTCC.B.APIEN              = 1U;
   RTC.RTCC.B.APIIE              = 1U;
   RTC.RTCC.B.CNTEN              = 1U;
	//! ##### Enabling the interrupt
   EnableIntr(); 
	//! ##### while in current mode
   do
   {
		//! #### clear wakeup status
      WKPU.WISR.R = 0xFFFFFFFFU; 
		//! #### If low power transition is in progress 
      if (PMCDIG.MCR.B.LP_ST_CHG_IN_PRGRS == 0u) 
      {
			//! #### Go to Standby and stay in RAM code : 'ChangePowerMode()'
         ChangePowerMode(POWER_MODE_STANDBY); 
      }
      else
      {
         // Do nothing
      }
   }
   while(MC_ME.GS.B.S_CURRENT_MODE != 0xD);
   // will not execute below code
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the WakeupCheck_AdcPolling
//!
//!======================================================================================
static void WakeupCheck_AdcPolling(void)
{
                                     // ADI number,   ADC Source, ANnumber,  GPIO no.
   
   static uint8         rficResetFlag                 = 0u;
          uint8         ADI                           = 0U;
	if(VehicleMode_Hibernate != VapToLpuRAM.VehicleMode)
   {
				//! #### Check the status of wake up timer function
		if (WakeupTimerFunc == WAKEUP2)
		{
			// ADC Conversion
					//! #### Process ADC conversion
			for(ADI = 0U; ADI < (ADI_CHANNELS+DAI_CHANNELS); ADI++)
			{
				SIUL2.MSCR[ADCMapping[ADI].GPIO].R = 0x00480000;
			}

			ADC_0.MCR.B.CTU_MODE = 0U;     // CTU mode is off
			ADC_0.MCR.B.ADCLKSEL = 1U;     // ADC clock frequency is equal to bus clock = 80MHz
			ADC_0.MCR.B.PWDN     = 0U;     // Enable SAR_ADC
			ADC_0.NCMR0.R        = ADCSamplingEn[0][ADC_NCMR0_INDEX]; // notthing to set
			ADC_0.NCMR1.R        = ADCSamplingEn[0][ADC_NCMR1_INDEX]; // notthing to set
			ADC_0.NCMR2.R        = ADCSamplingEn[0][ADC_NCMR2_INDEX]; // notthing to set
			ADC_0.MCR.B.NSTART   = 1U;
			ADC_1.MCR.B.CTU_MODE = 0U;     // CTU mode is off
			ADC_1.MCR.B.ADCLKSEL = 1U;     // ADC clock frequency is equal to bus clock = 80MHz
			ADC_1.MCR.B.PWDN     = 0U;     // Enable SAR_ADC
			ADC_1.NCMR0.R        = ADCSamplingEn[1][ADC_NCMR0_INDEX]; // notthing to set
			ADC_1.NCMR1.R        = ADCSamplingEn[1][ADC_NCMR1_INDEX]; // notthing to set
			ADC_1.NCMR2.R        = ADCSamplingEn[1][ADC_NCMR2_INDEX]; // notthing to set
			ADC_1.MCR.B.NSTART   = 1;
			while ((ADC_0.MSR.B.ADCSTATUS > 0u) 
					|| (ADC_0.MSR.B.NSTART > 0u) )
			{
			//Do nothing
			}
			while ((ADC_1.MSR.B.ADCSTATUS > 0u) 
			    || (ADC_1.MSR.B.NSTART > 0u) )
			{
			//Do nothing
			}
			//! #### Checking for all the ADI channels if it is active 
			for (ADI = 0U; ADI < (ADI_CHANNELS+DAI_CHANNELS); ADI++)
			{
				if (VapToLpuRAM.AdiWakeUp[ADI].isActive == TRUE)
				{
					#if 1
					//! #### Then processing for the Wakeup trigger based on ADI and threshold values
					if (ADI < 7U)
					{
						if ((ADC_0.CDR[ADCMapping[ADI].ANNumber].B.VALID > 0u))
						{
							if (((ADC_0.CDR[ADCMapping[ADI].ANNumber].B.CDATA)) < VapToLpuRAM.AdiWakeUp[ADI].ThresholdLow) //Threshold value shall be modified
							{
								WakeupTrigger = RUNFROMFLASH; //to wakeup to DRUN flash
							}
						}
					}
					else
					{
						if (ADC_1.CDR[ADCMapping[ADI].ANNumber].B.VALID > 0u)
						{
							if (((ADC_1.CDR[ADCMapping[ADI].ANNumber].B.CDATA)) < VapToLpuRAM.AdiWakeUp[ADI].ThresholdLow)
							{
								WakeupTrigger = RUNFROMFLASH; //to wakeup to DRUN flash
							}
						}
					}
					#endif
				}
			}
			//! #### Reset the MSCR for ADIs			
			for(ADI = 0; ADI < (ADC_CHANNELS); ADI++)
			{
				SIUL2.MSCR[ADCMapping[ADI].GPIO].R = 0x00000000;
			}
			//! #### Processing the Battery Voltage monitoring logic: 'BatteryVoltagemonitor()'
			BatteryVoltagemonitor();
			//! #### Processing the Voltage12V_monitor logic: 'Voltage12V_monitor()'
			Voltage12V_monitor(); 
      }
		else
		{
			// Do nothing
			//DebugStopFunc();
		}
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the LPU_InitPortSetting
//!
//!======================================================================================
static void LPU_InitPortSetting(void)
{
	DisableIntr(); // Disable Interrupt
   //! ###### Processing the initialisation of ports
   //! ##### Initialize the MSCR port  
   // MSCR
   //! ##### Initialize the CAN Standby port
   // GPDO : GPIO Pad Data Output Register
   // CAN Standby
   SIUL2.GPDO[GPIO_CAN1_STB].B.PDO_4n   = 1U; // PJ[4], PIN=5, CAN1[BB1] STB
   SIUL2.GPDO[GPIO_CAN2_STB].B.PDO_4n   = 1U; // PA[2], PIN=17, CAN2[BB2] STB
   SIUL2.GPDO[GPIO_CAN3_STB].B.PDO_4n   = 1U; // PF[9], PIN=41, CAN3[CabSubnet] STB
   SIUL2.GPDO[GPIO_CAN4_STB].B.PDO_4n   = 1U; // PH[13], PIN=9, CAN4[SecuritySubnet] STB
   SIUL2.GPDO[GPIO_CAN5_STB].B.PDO_4n   = 1U; // PE[1], PIN=20, CAN5[FMSNet] STB
   //SIUL2.GPDO[GPIO_CAN6_STB].B.PDO_4n    = 1; // PE[2], PIN=156, CAN6[CAN-FD] STB
   SIUL2.GPDO[GPIO_CAN6_STB].B.PDO_4n   = 1U; // PE[2], PIN=156, CAN6[CAN-FD] STB // Ref board has no tranceiver for CAN6(FD)
   //! ##### Initialize the CAN Rx port
   // CAN Rx
   SIUL2.MSCR[GPIO_CAN1_RX].R = 0x000B0000; // CAN1 Rx pin : PullUp
   SIUL2.MSCR[GPIO_CAN2_RX].R = 0x000B0000; // CAN2 Rx pin : PullUp
   SIUL2.MSCR[GPIO_CAN3_RX].R = 0x000B0000; // CAN3 Rx pin : PullUp
   SIUL2.MSCR[GPIO_CAN4_RX].R = 0x000B0000; // CAN4 Rx pin : PullUp
   SIUL2.MSCR[GPIO_CAN5_RX].R = 0x000B0000; // CAN5 Rx pin : PullUp
   SIUL2.MSCR[GPIO_CAN6_RX].R = 0x000B0000; // CAN6 Rx pin : PullUp
   //! ##### Enable the LIN port
   // LIN Enable
   SIUL2.GPDO[GPIO_LIN1_EN].B.PDO_4n      = 0U; // LIN1 EN
   SIUL2.GPDO[GPIO_LIN2_EN].B.PDO_4n      = 0U; // LIN2 EN
   SIUL2.GPDO[GPIO_LIN3_EN].B.PDO_4n      = 0U; // LIN3 EN
   SIUL2.GPDO[GPIO_LIN4_EN].B.PDO_4n      = 0U; // LIN4 EN
   SIUL2.GPDO[GPIO_LIN5_EN].B.PDO_4n      = 0U; // LIN5 EN
   SIUL2.GPDO[GPIO_LIN6_EN].B.PDO_4n      = 0U; // LIN6 EN
   SIUL2.GPDO[GPIO_LIN7_EN].B.PDO_4n      = 0U; // LIN7 EN
   SIUL2.GPDO[GPIO_LINSECU_EN].B.PDO_4n   = 0U; // LINSECU EN
   //! ##### Initialize the LIN Rx port
   // LIN Rx
   SIUL2.MSCR[GPIO_LIN1_RXD].R	  = 0x000B0000; // LIN1 Rx pin : PullUp
   SIUL2.MSCR[GPIO_LIN2_RXD].R	  = 0x000B0000; // LIN2 Rx pin : PullUp
   SIUL2.MSCR[GPIO_LIN3_RXD].R 	  = 0x000B0000; // LIN3 Rx pin : PullUp
   SIUL2.MSCR[GPIO_LIN4_RXD].R	  = 0x000B0000; // LIN4 Rx pin : PullUp
   SIUL2.MSCR[GPIO_LIN5_RXD].R	  = 0x000B0000; // LIN5 Rx pin : PullUp
   SIUL2.MSCR[GPIO_LIN6_RXD].R     = 0x000B0000; // LIN6 Rx pin : PullUp
   SIUL2.MSCR[GPIO_LIN7_RXD].R     = 0x000B0000; // LIN7 Rx pin : PullUp
   SIUL2.MSCR[GPIO_LINSECU_RXD].R  = 0x000B0000; // LINSECU Rx pin : PullUp
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the LPU_ToggleExternalWatchdogInputPin
//!
//!======================================================================================
static void LPU_ToggleExternalWatchdogInputPin(void)
{
   //! ##### Toggling the input pin of watchdog
   Wdg_PIN =! Wdg_PIN;  //watcdog toggle
   SIUL2.MSCR[GPIO_EXTWDG_WDI].R	  = 0x02000000;
   SIUL2.GPDO[GPIO_EXTWDG_WDI].B.PDO_4n = Wdg_PIN; // WDI
	//! ##### Processing for wakeup1 or wakeup2
	if(VAP2LPU == WakeupTimerFunc)
	{
		if(Wdg_PIN == 0U)
		{
			WakeupTimerFunc = WAKEUP2;
		}
		else
		{
			WakeupTimerFunc = WAKEUP1;
		}
	}
	else
	{
	
	}
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the LPU_EnableModules
//!
//! \param   *enable   Providing the current value
//!
//!======================================================================================
static void LPU_EnableModules(uint8 enable)
{
   //! ###### Processing Lpu enable modules
   //! ##### If in Low power mode
   if (enable > 0u) // stay in low power mode
   {
      //! #### Flash is set to  power-down mode
      MC_ME.DRUN_MC.B.FLAON   = 1u; // Flash is in power-down mode
      MC_ME.DRUN_MC.B.SYSCLK  = 0U; // 16MHz internal OSC
      while (MC_ME.GS.B.S_FIRCON == 0u) // wait for getting stable clock
      {
         //Do nothing
      }
      //! #### Enable all modes 
      MC_ME.CCTL1.R              = 0xFC; // Enable all modes
      //MC_ME.RUN_PC[1].R        = 0x08; // Active peripheral in DRUN mode
      MC_ME.RUN_PC[1].B.RUN3     = 0U; // Frozen peripheral in DRUN mode
      MC_ME.RUN_PC[1].B.RUN2     = 0U; // Frozen peripheral in DRUN mode
      MC_ME.RUN_PC[1].B.RUN1     = 0U; // Frozen peripheral in DRUN mode
      MC_ME.RUN_PC[1].B.RUN0     = 0U; // Frozen peripheral in DRUN mode
      MC_ME.RUN_PC[1].B.DRUN     = 1U; // Active peripheral in DRUN mode
      MC_ME.RUN_PC[1].B.SAFE     = 0U; // Frozen peripheral in DRUN mode
      MC_ME.LP_PC[1].B.STANDBY0  = 1U; // Active peripheral in STANDBY0 mode
      MC_ME.LP_PC[1].B.STOP0     = 0U; // Frozen peripheral in STOP0 mode
      //! #### Select RUN_PC[1] for ADC_0
      MC_ME.PCTL[24].B.RUN_CFG   = 1U; // Select RUN_PC[1] for ADC_0
      MC_ME.PCTL[25].B.RUN_CFG   = 1U; // Select RUN_PC[1] for ADC_1
      MC_ME.PCTL[93].R           = 0x09; // Select LP_PC[1] and RUN_PC[1] for WKPU
      MC_ME.PCTL[102].R          = 0x09; // Select LP_PC[1] and RUN_PC[1] for RTC/API
      do
      {
         ChangePowerMode(POWER_MODE_DRUN);
      }while (MC_ME.GS.B.S_CURRENT_MODE != 0x3);
      //while(LPU.SR.B.PD2_PWR_STAT == 1); // wait until Main domain is power gated.
      while (MC_ME.GS.B.S_FLA != 0x1)
      {
         //Do nothing
      }
   }
   //! ##### Else if it is not in Low power mode 
   else
   {
      //! #### Disable SAR_ADC and wait for power down
      ADC_0.MCR.B.PWDN = 1U;   // disable SAR_ADC
      ADC_1.MCR.B.PWDN = 1U;   // disable SAR_ADC
      while (ADC_0.MSR.B.ADCSTATUS != 1u) // wait for power down
      {
         //Do nothing
      }
      while (ADC_1.MSR.B.ADCSTATUS != 1u) // wait for power down
      {
         //Do nothing
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the LpuSw_GpioOut_Dyn
//!
//!======================================================================================
static void LpuSw_GpioOut_Dyn(void)
{
	//! ##### Setting the Gpio for low power pull up 
	GpioCtrl[LOWPOWER_PULLUP] = LPModeDynOutput[WakeupTimerFunc].ParkedPullupOut;
	//! ##### Setting the Gpio for wetting pull up 
	GpioCtrl[WETTING_PULLUP] = LPModeDynOutput[WakeupTimerFunc].LivingPullupOut;
	//! ##### Processing the write for wetting pull up : 'GpioOutput_Write()'
	GpioOutput_Write(WETTING_PULLUP);
	//! ##### Processing the write for wetting pull up :
	GpioOutput_Write(LOWPOWER_PULLUP);
	PMCDIG.RDCR.B.PAD_KEEP_EN = 0x1;
}
#pragma section CODE
//#define LpuRAMcode_STOP_SEC_CODE
//#include "LpuRAMcode_MemMap.h"        PRQA S 5087   MD_MSR_19.1 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//! @}
//! @}
//! @}
//! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
