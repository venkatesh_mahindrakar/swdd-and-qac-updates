/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *          File:  LpuMgr.c
 *        Config:  C:/Personal/GIT/ExternalGit/MSW/SCIM-BSP/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *     SW-C Type:  LpuMgr
 *  Generated at:  Fri Jun 12 18:00:03 2020
 *
 *     Generator:  MICROSAR RTE Generator Version 4.19.0
 *                 RTE Core Version 1.19.0
 *       License:  CBD1800194
 *
 *   Description:  C-Code implementation template for SW-C <LpuMgr>
 *********************************************************************************************************************/
//!======================================================================================
//! \file LpuMgr.c
//! \copyright Volvo Group Trucks Technology  2019
//!
//! The copying, distribution and utilization of this SW artefact as well as the
//! communication of its contents to others without expressed authorization is
//! prohibited. Offenders will be held liable for payment of damages. All rights
//! reserved in the event of the grant of a patent, utility model or ornamental
//! design registration.
//!
//! @addtogroup LpuMgr 
//!
//! \brief
//! LpuMgr SWC.
//! ASIL Level : QM.
//! This module implements the logic for the LpuMgr runnables.
//!======================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
#include"LpuRAMCode.h"
#include"EcuM.h"
#include"Gpt.h"
#include"Dio.h"
#include"Mcu.h"
#include"Rte_Type.h"
#include "IoHwAb_Adc.h"
#include "CoreTests_If.h"
#include "LpuMgr.h"
#include "Mpc5746c.h" 
#include "Icu.h"
#include "IoHwAb_Adc.h"
#include "Adc_CfgDefines.h"
const  unsigned long *const Rte_StartAddr = (unsigned long *)&__RTE_VAR_MEM;
static void DIO_Processing_LPUEntry(void);
static void LPURAMCopy_ADI(void);
static void LPURAMCopy_COM(void);
static void LPURAMCopy_Gpio(void);
static void Disable_modules(void);
static void EntrytoDRUNRAM(void);
static void BusWakeupStatus_Check (void);
static uint8 Rte_Var_Copy[3000]; // Current "__RTE_VAR_SIZE" is 2500(Dec), 17-July-2020
uint8 ADCSamplingEn[NO_OF_ADC_HW][ADC_NCMR_REGS]= {{0,0,0},{0,0,0}};
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_HwToleranceThreshold_X1C04_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1WMD_ThresholdHigh_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1WMD_ThresholdLow_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_Adi_X1CXW_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_CanInterfaces_X1CX2_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_DOWHS_X1CXY_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_DOWLS_X1CXZ_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_DoorAccessIf_X1CX3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PcbConfig_LinInterfaces_X1CX0_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CX4_P1Interface_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CX4_P2Interface_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CX4_P3Interface_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CX4_P4Interface_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CX4_PiInterface_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CY1_DigitalBiLevelHigh_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CY1_DigitalBiLevelLow_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/
#include "Rte_LpuMgr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_HwToleranceThreshold_X1C04_T: Integer in interval [0...255]
 * SEWS_P1WMD_ThresholdHigh_T: Integer in interval [0...255]
 * SEWS_P1WMD_ThresholdLow_T: Integer in interval [0...255]
 * SEWS_PcbConfig_DoorAccessIf_X1CX3_T: Integer in interval [0...255]
 * SEWS_X1CX4_P1Interface_T: Integer in interval [0...255]
 * SEWS_X1CX4_P2Interface_T: Integer in interval [0...255]
 * SEWS_X1CX4_P3Interface_T: Integer in interval [0...255]
 * SEWS_X1CX4_P4Interface_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelHigh_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelLow_T: Integer in interval [0...255]
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Fsc_OperationalMode_T: Enumeration of integer in interval [0...255] with enumerators
 *   FSC_ShutdownReady (0U)
 *   FSC_Reduced_12vDcDcLimit (1U)
 *   FSC_Reduced (2U)
 *   FSC_Operating (3U)
 *   FSC_Protecting (4U)
 *   FSC_Withstand (5U)
 *   FSC_NotAvailable (6U)
 * SEWS_PcbConfig_Adi_X1CXW_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_Adi_X1CXW_T_NotPopulated (0U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration (1U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration (2U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration (3U)
 * SEWS_PcbConfig_CanInterfaces_X1CX2_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_CanInterfaces_X1CX2_T_NotPopulated (0U)
 *   SEWS_PcbConfig_CanInterfaces_X1CX2_T_Populated (1U)
 * SEWS_PcbConfig_DOWHS_X1CXY_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOWHS_X1CXY_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOWHS_X1CXY_T_Populated (1U)
 * SEWS_PcbConfig_DOWLS_X1CXZ_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T_Populated (1U)
 * SEWS_PcbConfig_LinInterfaces_X1CX0_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T_NotPopulated (0U)
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated (1U)
 * SEWS_X1CX4_PiInterface_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_X1CX4_PiInterface_T_NotPopulated (0U)
 *   SEWS_X1CX4_PiInterface_T_Populated (1U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 * boolean: Enumeration of integer in interval [0...1] with enumerators
 *   FALSE (0U)
 *   TRUE (1U)
 *
 * Array Types:
 * ============
 * SEWS_AdiWakeUpConfig_P1WMD_a_T: Array with 16 element(s) of type SEWS_AdiWakeUpConfig_P1WMD_s_T
 * SEWS_PcbConfig_Adi_X1CXW_a_T: Array with 19 element(s) of type SEWS_PcbConfig_Adi_X1CXW_T
 * SEWS_PcbConfig_CanInterfaces_X1CX2_a_T: Array with 6 element(s) of type SEWS_PcbConfig_CanInterfaces_X1CX2_T
 * SEWS_PcbConfig_DOWHS_X1CXY_a_T: Array with 2 element(s) of type SEWS_PcbConfig_DOWHS_X1CXY_T
 * SEWS_PcbConfig_DOWLS_X1CXZ_a_T: Array with 3 element(s) of type SEWS_PcbConfig_DOWLS_X1CXZ_T
 * SEWS_PcbConfig_LinInterfaces_X1CX0_a_T: Array with 7 element(s) of type SEWS_PcbConfig_LinInterfaces_X1CX0_T
 *
 * Record Types:
 * =============
 * SEWS_AdiWakeUpConfig_P1WMD_s_T: Record with elements
 *   ThresholdHigh of type SEWS_P1WMD_ThresholdHigh_T
 *   ThresholdLow of type SEWS_P1WMD_ThresholdLow_T
 *   isActiveInLiving of type boolean
 *   isActiveInParked of type boolean
 * SEWS_DAI_Installed_P1WMP_s_T: Record with elements
 *   LeftDoor of type boolean
 *   RightDoor of type boolean
 * SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T: Record with elements
 *   DigitalBiLevelLow of type SEWS_X1CY1_DigitalBiLevelLow_T
 *   DigitalBiLevelHigh of type SEWS_X1CY1_DigitalBiLevelHigh_T
 * SEWS_PcbConfig_AdiPullUp_X1CX5_s_T: Record with elements
 *   AdiPullupLiving of type boolean
 *   AdiPullupParked of type boolean
 * SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T: Record with elements
 *   PiInterface of type SEWS_X1CX4_PiInterface_T
 *   P1Interface of type SEWS_X1CX4_P1Interface_T
 *   P2Interface of type SEWS_X1CX4_P2Interface_T
 *   P3Interface of type SEWS_X1CX4_P3Interface_T
 *   P4Interface of type SEWS_X1CX4_P4Interface_T
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_HwToleranceThreshold_X1C04_T Rte_Prm_X1C04_HwToleranceThreshold_v(void)
 *   SEWS_PcbConfig_DoorAccessIf_X1CX3_T Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v(void)
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T *Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(void)
 *     Returnvalue: SEWS_PcbConfig_LinInterfaces_X1CX0_T* is of type SEWS_PcbConfig_LinInterfaces_X1CX0_a_T
 *   SEWS_PcbConfig_CanInterfaces_X1CX2_T *Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(void)
 *     Returnvalue: SEWS_PcbConfig_CanInterfaces_X1CX2_T* is of type SEWS_PcbConfig_CanInterfaces_X1CX2_a_T
 *   SEWS_PcbConfig_Adi_X1CXW_T *Rte_Prm_X1CXW_PcbConfig_Adi_v(void)
 *     Returnvalue: SEWS_PcbConfig_Adi_X1CXW_T* is of type SEWS_PcbConfig_Adi_X1CXW_a_T
 *   SEWS_PcbConfig_DOWHS_X1CXY_T *Rte_Prm_X1CXY_PcbConfig_DOWHS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOWHS_X1CXY_T* is of type SEWS_PcbConfig_DOWHS_X1CXY_a_T
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T *Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOWLS_X1CXZ_T* is of type SEWS_PcbConfig_DOWLS_X1CXZ_a_T
 *   SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T *Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v(void)
 *   SEWS_PcbConfig_AdiPullUp_X1CX5_s_T *Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v(void)
 *   SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void)
 *   boolean Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v(void)
 *   boolean Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v(void)
 *   boolean Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v(void)
 *   boolean Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v(void)
 *   SEWS_AdiWakeUpConfig_P1WMD_s_T *Rte_Prm_P1WMD_AdiWakeUpConfig_v(void)
 *     Returnvalue: SEWS_AdiWakeUpConfig_P1WMD_s_T* is of type SEWS_AdiWakeUpConfig_P1WMD_a_T
 *   SEWS_DAI_Installed_P1WMP_s_T *Rte_Prm_P1WMP_DAI_Installed_v(void)
 *   boolean Rte_Prm_P1WPP_isSecurityLinActive_v(void)
 *
 *********************************************************************************************************************/
#define LpuMgr_START_SEC_CODE
#include "LpuMgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
/**********************************************************************************************************************
 *
 * Runnable Entity Name: LpToVapEntryRunnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_LpModeRunTime_P_LpModeRunTime(uint32 data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: LpToVapEntryRunnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//!
//! \brief
//! This function implements the logic for the LpToVapEntryRunnable
//!
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
FUNC(void, LpuMgr_CODE) LpToVapEntryRunnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: LpToVapEntryRunnable
 *********************************************************************************************************************/
	
  if(WakeupTimerFunc==WAKEUPTOFLASH)
	{
		RteVar_CopyfromBuffer();
	}
   //! ##### Processing to write the Lpu_timelog counter value: 'Rte_Write_LpModeRunTime_P_LpModeRunTime()'
	Rte_Write_LpModeRunTime_P_LpModeRunTime(LPU_TimeLogCounter);
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}
/**********************************************************************************************************************
 *
 * Runnable Entity Name: VapToLpEntryRunnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * This runnable is never executed by the RTE.
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_FSC_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VapToLpEntryRunnable_doc
 *********************************************************************************************************************/
//!====================================================================================================================
//!
//! \brief
//! This function implements the  runnable logic for the VapToLpEntryRunnable
//!
//!====================================================================================================================
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
FUNC(void, LpuMgr_CODE) VapToLpEntryRunnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VapToLpEntryRunnable
 *********************************************************************************************************************/
	uint8 index=0U;
	VehicleMode_T CurrentVehicleMode;
	//! ##### Reading the current vehicle mode 
	Rte_Read_VehicleModeInternal_VehicleMode(&CurrentVehicleMode);
	(void)Rte_MemClr(&VapToLpuRAM,sizeof(VapToLpuRAMType));
	VapToLpuRAM.VehicleMode = CurrentVehicleMode; 
	for(index=0;index<Max_Gpio;index++)
	{
		GpioCtrl[index]=0;
	}
	LIN8_Wakeup = 0x0;
	//! ##### Processing the disabling of modules logic :'Disable_modules()'
	Disable_modules();
	//! ##### Processing the LPURAMCopy_Gpio : 'LPURAMCopy_Gpio()' 
	LPURAMCopy_Gpio();
	//! ##### Processing the LPURAMCopy_COM : 'LPURAMCopy_COM()'
	LPURAMCopy_COM();
	//! ##### Processing the LPURAMCopy_ADI : 'LPURAMCopy_ADI()'
	LPURAMCopy_ADI();
	//! ##### Process the DIO processing LPU entry logic : 'DIO_Processing_LPUEntry()'
	DIO_Processing_LPUEntry();
	//Gpt_StartTimer(GptConf_GptChannelConfiguration_GptChannel_ExternalWdg, 1280); //10ms timer wakeup
	//! ##### Process the Standby to DRUNRAM logic : 'EntrytoDRUNRAM()'
	(void)EntrytoDRUNRAM();
	
	FBL_SIUL2_GPDO(13) ^= 0x01u; // Watchdog WDI
	Wdg_PIN = FBL_SIUL2_GPDO(13);
	(void)LPU_RUN_Function();
} 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!====================================================================================================================
//!
//! \brief
//! This function implements the  runnable logic for the Disable_modules
//!
//!====================================================================================================================
static void Disable_modules(void)
{
	//! ##### Set the ICU module to sleep
	(void)Icu_SetMode(1);
	//! ##### Disabling RAMTst : 'RamTst_Deinit()'
	RamTst_Deinit();
	//! ##### Disabling Core test  : 'CoreTest_CpuLoadDeInit()'
	CoreTest_CpuLoadDeInit();
	//! ##### Disabling PIT : 'Disable_PIT()' 
	(void)Disable_PIT(1);
	//! ##### Disabling Pwm : 'Pwm_DeInit()'
	(void)Pwm_DeInit();
	//! ##### Disabling ADC : 'Adc_DeInit()'
	(void)Adc_DeInit();
	//! ##### Disabling Mcl : 'Mcl_DeInit()'
	(void)Mcl_DeInit();
	//! ##### Disable the RFIC Chip using NRST pin
	Dio_WriteChannel (DioConf_DioChannel_RFIC_NRST,1U);
	//! ##### Disabling the external interrupt  
	DisableExtIntr; // Disable external interrupts
	//FBL_SIUL2_GPDO(76) ^= 0x01u; // Dio_WriteChannel(DioConf_DioChannel_Debug_PE12_Pin133,1); // 
	FBL_SIUL2_GPDO(13) ^= 0x01u; // Watchdog WDI
	Wdg_PIN             = FBL_SIUL2_GPDO(13);
	WakeupTimerFunc     = VAP2LPU;
}
//!====================================================================================================================
//!
//! \brief
//! This function implements the  runnable logic for the LPURAMCopy_Gpio
//!
//!====================================================================================================================
static void LPURAMCopy_Gpio(void)
{
	//! ###### This function processes the GPIO for Lp mode
	uint8 DobhsRead_Status[4] = {0,0,0,0};
	uint8 index               = 0U;
	Fsc_OperationalMode_T FSC_Mode;
	SEWS_PcbConfig_AdiPullUp_X1CX5_s_T *PcbConfig_AdiPullup       = {NULL_PTR,};
	SEWS_PcbConfig_DoorAccessIf_X1CX3_T PcbConfig_DaiInterfaces   = 0U;
	SEWS_PcbConfig_DOBHS_X1CXX_T       *PcbConfig_DobhsInterfaces = {NULL_PTR,};
	//! ##### Reading the FSC mode
	Rte_Read_FSC_OperationalMode_P_Fsc_OperationalMode(&FSC_Mode);
	//! ##### Process to read DAI,ADI, DOBHS intefaces for Pcb configuration
	PcbConfig_DaiInterfaces   = (SEWS_PcbConfig_DoorAccessIf_X1CX3_T) Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
	PcbConfig_AdiPullup       = (SEWS_PcbConfig_AdiPullUp_X1CX5_s_T*) Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
	PcbConfig_DobhsInterfaces = (SEWS_PcbConfig_DOBHS_X1CXX_T *)Rte_Prm_X1CXX_PcbConfig_DOBHS_v();
	//! ##### Reading the Dobhs channel status
	VapToLpuRAM.Dobhs_Status[0]	     = Dio_ReadChannel(DioConf_DioChannel_Dobhs_OutputCh1);
	VapToLpuRAM.Dobhs_Status[1]        = Dio_ReadChannel(DioConf_DioChannel_Dobhs_OutputCh2);
	VapToLpuRAM.Dobhs_Status[2] 	     = Dio_ReadChannel(DioConf_DioChannel_Dobhs_OutputCh3);
	VapToLpuRAM.Dobhs_Status[3] 	     = Dio_ReadChannel(DioConf_DioChannel_Dobhs_OutputCh4);
	VapToLpuRAM.LP12VOutputAct_Living  = Dio_ReadChannel(DioConf_DioChannel_Do12V_OutputLiving);
	VapToLpuRAM.LP12VOutputAct_Parked  = Dio_ReadChannel(DioConf_DioChannel_Do12V_OutputParked);
	//! ##### Copy HW wakeup configuration to LPW RAM
	VapToLpuRAM.FSC_OperatingMode = FSC_Mode;	//Assigned with actual parameter values
	if ((FSC_Mode == FSC_Reduced)
	  ||(FSC_Mode == FSC_ShutdownReady))
	{
	  VapToLpuRAM.IsReduced = TRUE;
	}
	else 
	{
	  VapToLpuRAM.IsReduced = FALSE;
	}
	//! ##### Checking the vehicle mode and accordingly processing the LP pull up activation
	if ((FALSE == VapToLpuRAM.IsReduced)
		&&(VapToLpuRAM.VehicleMode!=VehicleMode_Hibernate))
	{ 
		if ((PcbConfig_AdiPullup->AdiPullupLiving == TRUE)
			&&(Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v() == TRUE)
			&&(VapToLpuRAM.VehicleMode==VehicleMode_Living))
		{
			VapToLpuRAM.LPPullUpAct_Living = TRUE;
			LPModeDynOutput[WAKEUP1].LivingPullupOut=1;
		}
		else
		{
			VapToLpuRAM.LPPullUpAct_Living = FALSE;
			LPModeDynOutput[WAKEUP1].LivingPullupOut=0;
		}
		if ((PcbConfig_AdiPullup->AdiPullupParked == TRUE)
			&&(Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v() == TRUE))
		{
			VapToLpuRAM.LPPullUpAct_Parked = TRUE;
			LPModeDynOutput[WAKEUP1].ParkedPullupOut=1;
			
		}
		else
		{
		  VapToLpuRAM.LPPullUpAct_Parked = FALSE;
		  LPModeDynOutput[WAKEUP1].ParkedPullupOut=0;
		}
		VapToLpuRAM.DAI_Installed = PcbConfig_DaiInterfaces;
		//! ##### Updating the Dobhs activation interface and status 			
		for (index=0U;index<4U;index++)
		{   
			VapToLpuRAM.Dobhs_Act[index] = PcbConfig_DobhsInterfaces[index];
			VapToLpuRAM.Dobhs_Status[index] = DobhsRead_Status[index];
		} 
	}
	else
	{ 
		// Do nothing
	}
}
//!====================================================================================================================
//!
//! \brief
//! This function implements the  runnable logic for the LPURAMCopy_COM
//!
//!====================================================================================================================
static void LPURAMCopy_COM(void)
{
	uint8                                index                    = 0U;
	SEWS_PcbConfig_LinInterfaces_X1CX0_T *PcbConfig_LinInterfaces = {NULL_PTR,};
	SEWS_PcbConfig_CanInterfaces_X1CX2_T *PcbConfig_CanInterfaces = {NULL_PTR,};
	//! ##### Process to read CAN, LIN intefaces for Pcb configuration
	PcbConfig_LinInterfaces = (SEWS_PcbConfig_LinInterfaces_X1CX0_T *)Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v();
	PcbConfig_CanInterfaces = (SEWS_PcbConfig_CanInterfaces_X1CX2_T *)Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v();
	//! ##### Checking the vehicle mode and accordingly processing the Can/Lin activation
	if ((FALSE == VapToLpuRAM.IsReduced)
	&&(VapToLpuRAM.VehicleMode!=VehicleMode_Hibernate))
	{
		//! #### Updating the CAN activation interface 
		for (index=0U;index<6U;index++)
		{   
			VapToLpuRAM.CANAct[index] = PcbConfig_CanInterfaces[index];
		} 
		//! #### Updating the LIN activation interface 
		for (index=0U;index<7U;index++)
		{   
			VapToLpuRAM.LINAct[index] = PcbConfig_LinInterfaces[index];
		}
      //VapToLpuRAM.LINAct[7]= Rte_Prm_P1WPP_isSecurityLinActive_v();
		//! #### Determining the wakeup nodes
		if (VapToLpuRAM.CANAct[5] == TRUE)
		{
			LIN8_Wakeup|= ((uint32)0x1U << 4U);
		}
		if (VapToLpuRAM.LINAct[5]==TRUE)
		{
			LIN8_Wakeup|= ((uint32)0x1 << 15U);
		}
		if (VapToLpuRAM.LINAct[6]==TRUE)
		{
			LIN8_Wakeup|= ((uint32)0x1 << 23U);
		}
	   if (Rte_Prm_P1WPP_isSecurityLinActive_v()==TRUE)
		{
			LIN8_Wakeup|= ((uint32)0x1 << 11U);
		}
	}
	else
	{ 
		// Do nothing
	}	
}
//!====================================================================================================================
//!
//! \brief
//! This function implements the  runnable logic for the LPURAMCopy_ADI
//!
//!====================================================================================================================
static void LPURAMCopy_ADI(void)
{
	uint8 ADI = 0U;
	uint8 Offset = 0;
	uint8 Index = 0;
	SEWS_AdiWakeUpConfig_P1WMD_s_T *AdiWakeupcfg;
	AdiWakeupcfg= (SEWS_AdiWakeUpConfig_P1WMD_s_T *) Rte_Prm_P1WMD_AdiWakeUpConfig_v();
	//! ##### Checking the vehicle mode and accordingly processing the ADI
	if ((FALSE == VapToLpuRAM.IsReduced)
		&&(VapToLpuRAM.VehicleMode!=VehicleMode_Hibernate))
	{
		//! #### Checking the Vehicle mode and low power pull up activation criteria and setting the ADI wakeup
		for(ADI=0; ADI<ADI_CHANNELS; ADI++)
		{
			if (((VapToLpuRAM.VehicleMode==VehicleMode_Living)
				||(VapToLpuRAM.VehicleMode==VehicleMode_Accessory))
				&&((VapToLpuRAM.LPPullUpAct_Living==TRUE)
				||(VapToLpuRAM.LPPullUpAct_Parked==TRUE)))
			{
				VapToLpuRAM.AdiWakeUp[ADI].isActive = AdiWakeupcfg[ADI].isActiveInLiving;	// Assigned with actual parameter values
				

				if (1 == VapToLpuRAM.AdiWakeUp[ADI].isActive)
				{
				
					if (ADCMapping[ADI].ANNumber >63)
					{
						Offset = 64;
						Index = 2;
					}
					else if(ADCMapping[ADI].ANNumber >31)
					{
						Offset = 32;
						Index = 1;
					}
					else 
					{
						Offset = 0;
						Index = 0;
					}
					ADCSamplingEn[ADCMapping[ADI].ADCSource][Index] |= (1<<ADCMapping[ADI].ANNumber - Offset); 
				}
			}
			else if((VapToLpuRAM.VehicleMode==VehicleMode_Parked)
				&&(VapToLpuRAM.LPPullUpAct_Parked==TRUE))
			{
				VapToLpuRAM.AdiWakeUp[ADI].isActive = AdiWakeupcfg[ADI].isActiveInParked;
				if (1 == VapToLpuRAM.AdiWakeUp[ADI].isActive)
				{
					if (ADCMapping[ADI].ANNumber >63)
					{
						Offset = 64;
						Index = 2;
					}
					else if(ADCMapping[ADI].ANNumber >31)
					{
						Offset = 32;
						Index = 1;
					}
					else 
					{
						Offset = 0;
						Index = 0;
					}
					ADCSamplingEn[ADCMapping[ADI].ADCSource][Index] |= (1<<ADCMapping[ADI].ANNumber - Offset); 
				}
			}
			else
			{
				//Do nothing
			}
			//! #### If ADI is less than 7 processing the threshold values with ADC0 conversion factor
			if (ADI < 7)
			{
				VapToLpuRAM.AdiWakeUp[ADI].ThresholdHigh    = ((uint16)((24U*(AdiWakeupcfg[ADI].ThresholdHigh))/100U)*(uint16)(5/ADC0_CONVFACTOR_ADI_WETTINGPULLUP));		
				VapToLpuRAM.AdiWakeUp[ADI].ThresholdLow     = ((uint16)((24U*(AdiWakeupcfg[ADI].ThresholdLow))/100U)*(uint16)(5/ADC0_CONVFACTOR_ADI_WETTINGPULLUP));
			}
			//! #### Else processing the threshold values with ADC1 conversion factor
			else
			{
				VapToLpuRAM.AdiWakeUp[ADI].ThresholdHigh    = (uint16)(((24U*(AdiWakeupcfg[ADI].ThresholdHigh)/100U))*(uint16)(5/ADC1_CONVFACTOR_ADI_WETTINGPULLUP));		
				VapToLpuRAM.AdiWakeUp[ADI].ThresholdLow     = (uint16)(((24U*(AdiWakeupcfg[ADI].ThresholdLow)/100U))*(uint16)(5/ADC1_CONVFACTOR_ADI_WETTINGPULLUP));					
			}
		}
		//! #### If the Lp output activation is Living set the ADI wakeup and threshold value
		if (VapToLpuRAM.LP12VOutputAct_Living == TRUE)
		{
			VapToLpuRAM.AdiWakeUp[19].isActive = VapToLpuRAM.LP12VOutputAct_Living;
			VapToLpuRAM.AdiWakeUp[19].ThresholdLow	 = 653U	;							//8V digital value 
		}
		//! #### If the Lp output activation is Parked set the ADI wakeup and threshold value
		if (VapToLpuRAM.LP12VOutputAct_Parked == TRUE)
		{
			VapToLpuRAM.AdiWakeUp[20].isActive = VapToLpuRAM.LP12VOutputAct_Parked;
			VapToLpuRAM.AdiWakeUp[20].ThresholdLow	= 653U;								//8V digital value
		}	
	}
	else
	{ 
		// Do nothing
	}			
}	
#define LpuMgr_STOP_SEC_CODE
#include "LpuMgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
//!====================================================================================================================
//!
//! \brief
//! This function implements the  logic for the DIO_Processing_LPUEntry
//!
//!====================================================================================================================
static void DIO_Processing_LPUEntry(void)
{
   // Rte.c : Global variable
   //! ##### Based on the status of vehicle mode update the GpioCtrl
   switch(VapToLpuRAM.VehicleMode)
   {	
      //! #### Updating the GpioCtrl in case when Status of vehicle mode is 'Hibernate' 
      case VehicleMode_Hibernate:
			GpioCtrl[DCDC_EN] = 0;
			GpioCtrl[LIVING_OUT] = 0;
			GpioCtrl[PARKED_OUT] = 0;
			GpioCtrl[DOBHS1_OUT] = 0;
			GpioCtrl[DOBHS2_OUT] = 0;
			GpioCtrl[DOBHS3_OUT] = 0;
			GpioCtrl[DOBHS4_OUT] = 0;
			LPModeDynOutput[WAKEUP2].RtcTime = 5;
			LPModeDynOutput[WAKEUP1].RtcTime = 160;
      break;
      //! #### Updating the GpioCtrl in case when  Status of vehicle mode is 'Parked'
      case VehicleMode_Parked:   // Parked without LIN
			GpioCtrl[DCDC_EN] = 0;    // V12DCDC
			GpioCtrl[LIVING_OUT] = 0; // Living Ctrl
			//! #### Set the LP12VOutputAct_Parked status
			GpioCtrl[PARKED_OUT]=VapToLpuRAM.LP12VOutputAct_Parked;
			LPModeDynOutput[WAKEUP2].RtcTime = 10;
			LPModeDynOutput[WAKEUP1].RtcTime = 90;
      break;
      //! #### Updating the GpioCtrl in case when Status of vehicle mode is 'Living'
      case VehicleMode_Living:
	   //! #### Updating the GpioCtrl in case when Status of vehicle mode is 'Accessory'
      case VehicleMode_Accessory:
			GpioCtrl[LIVING_OUT] =VapToLpuRAM.LP12VOutputAct_Living; // Living Ctrl
			GpioCtrl[PARKED_OUT]=VapToLpuRAM.LP12VOutputAct_Parked;
			GpioCtrl[DOBHS1_OUT]=VapToLpuRAM.Dobhs_Status[0];
			GpioCtrl[DOBHS2_OUT]=VapToLpuRAM.Dobhs_Status[1];
			GpioCtrl[DOBHS3_OUT]=VapToLpuRAM.Dobhs_Status[2];
			GpioCtrl[DOBHS4_OUT]=VapToLpuRAM.Dobhs_Status[3];
         GpioCtrl[DCDC_EN] =(GpioCtrl[LIVING_OUT])|(GpioCtrl[PARKED_OUT]); // V12DCDC
			LPModeDynOutput[WAKEUP2].RtcTime = 10;
			LPModeDynOutput[WAKEUP1].RtcTime = 90;
      break;    
      default:
      //Do nothing
       break;	
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the StandbytoDRUNRAM
//!
//!======================================================================================
static void EntrytoDRUNRAM(void)
{
	//! ###### Processing the registers for StandbytoDrunRam
   //! ##### Process the RteVar_CopytoBuffer : 'RteVar_CopytoBuffer()'
   RteVar_CopytoBuffer(); // jinwoo
   LPU_TimeLogCounter = 0U; //Local time logging requirement
   WKPU.IRER.R = 0x00000000;	
	//! ##### Clear all Interrupt Status Flag Register(WKPU_WISR)
	WKPU.WISR.R       = 0xFFFFFFFFU; 
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the CDD_WakeupStart
//!
//!======================================================================================
void CDD_WakeupStart(void)
{
  // PMCDIG.RDCR.B.PAD_KEEP_EN = 0x0;
#if 0
   SIUL2.GPDO[GPIO_12V_DCDC_EN].B.PDO_4n = 1U;  // 12V DCDC Enable
   SIUL2.GPDO[GPIO_12V_LIVING_OUT].B.PDO_4n = 1U; // Living Ctrl
   SIUL2.GPDO[GPIO_12V_PARKED_OUT].B.PDO_4n = 1U; // Parked Ctrl
   SIUL2.GPDO[GPIO_WETTING_PULLUP].B.PDO_4n = 1U;	// Living Pullup 
   SIUL2.GPDO[GPIO_LOWPOWER_PULLUP].B.PDO_4n = 1U;	// Parked pullup
   //need to implement Pullup_DAI as disabled
   //need to implement DAI as disabled
   SIUL2.GPDO[GPIO_LFIC_NRST].B.PDO_4n = 1U; // LF NRES disabed in all
   SIUL2.GPDO[GPIO_RFIC_NRST].B.PDO_4n = 1U;  // RF NRES
#endif
	//! ##### Process the RE_rfic_init : 'RE_rfic_init()'
	(void)RE_rfic_init();
   BusWakeupStatus_Check();
#if 1 // for Full board
   //! ##### Check if the wakeup status is LIN6 wakeup
   if (((Wakeup_Status >> 15U) & 1U) == 1U) // LIN6 wakeup
   {
      //LINWakeUpNotification_LIN6();
		EcuM_SetWakeupEvent(ECUM_WKSOURCE_CN_LIN05_5cb35350); 
	}
   else
   {
     // SIUL2.GPDO[GPIO_LIN6_EN].B.PDO_4n 	  = 0U; // LIN6 EN
      //Icu_EnableNotification(IcuChannel_LIN6_RX);
     // Icu_EnableEdgeDetection(IcuChannel_LIN6_RX);
   }
   //! ##### Check if the wakeup status is LIN7 wakeup
   if (((Wakeup_Status >> 23U) &1U) == 1U) // LIN7 wakeup
   {
      //LINWakeUpNotification_LIN7();
		EcuM_SetWakeupEvent(ECUM_WKSOURCE_CN_LIN06_c5ba02ea); 
	}
   else
   {
      //SIUL2.GPDO[GPIO_LIN7_EN].B.PDO_4n 	  = 0; // LIN5 EN
      //Icu_EnableNotification(IcuChannel_LIN7_RX);
     // Icu_EnableEdgeDetection(IcuChannel_LIN7_RX);
   }
   //! ##### Check if the wakeup status is LINSecurity wakeup   
   if (((Wakeup_Status >> 11U)&1U) == 1U) // LINSecurity wakeup
   {
      //LINWakeUpNotification_LIN8();
		EcuM_SetWakeupEvent(ECUM_WKSOURCE_CN_LIN07_b2bd327c); 
	}
   else
   {
		//SIUL2.GPDO[GPIO_LINSECU_EN].B.PDO_4n 	  = 0U; // LINSecurity EN
		//    Icu_EnableNotification(IcuChannel_LIN8_RX);
		//      Icu_EnableEdgeDetection(IcuChannel_LIN8_RX);
   }
#endif // end of for Full board
   //Wakeup_Status = 0U;
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the 'BusWakeupStatus_Check'
//!
//!======================================================================================
static void BusWakeupStatus_Check (void)
{
   ComWakeupChecktype ComWakeupcheck[8] = {{17u,IcuChannel_CAN2RX,GPIO_CAN2_STB,&CanTrcvWakeUpNotification_CAN2},
                                           {8u,IcuChannel_CAN3RX,GPIO_CAN3_STB,&CanTrcvWakeUpNotification_CAN3},
                                           {5u,IcuChannel_CAN4RX,GPIO_CAN4_STB,&CanTrcvWakeUpNotification_CAN4},
                                           {18U,IcuChannel_LIN1_RX,GPIO_LIN1_EN,&LINWakeUpNotification_LIN1},
                                           {24U,IcuChannel_LIN2_RX,GPIO_LIN2_EN,&LINWakeUpNotification_LIN2},
                                           {21U,IcuChannel_LIN3_RX,GPIO_LIN3_EN,&LINWakeUpNotification_LIN3},
                                           {20U,IcuChannel_LIN4_RX,GPIO_LIN4_EN,&LINWakeUpNotification_LIN4},
                                           {12U,IcuChannel_LIN5_RX,GPIO_LIN5_EN,&LINWakeUpNotification_LIN5}};                                          
   uint8              i                 = 0u;
   //! ##### Check the wakeup status is RFIC wakeup
   for(i=0;i<8;i++)
   {   
      //! ##### Check the wakeup status
      if (((Wakeup_Status >> ComWakeupcheck[i].Wakupbit)&1U) == 1U) // LIN1 wakeup
      {
         ComWakeupcheck[i].WakeupNotif();
      }
      else
      {
         SIUL2.GPDO[ComWakeupcheck[i].TransvrPin].B.PDO_4n = 0U; // LIN1 EN
         Icu_EnableNotification(ComWakeupcheck[i].Icuchannel);
         Icu_EnableEdgeDetection(ComWakeupcheck[i].Icuchannel);
      }
   }
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the RteVar_CopytoBuffer
//!
//!======================================================================================
 void RteVar_CopytoBuffer(void) 
{
   //! ##### Copying the variable data into buffer
   (void)Rte_MemCpy(&Rte_Var_Copy , Rte_StartAddr, &__RTE_VAR_SIZE);
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the RteVar_CopyfromBuffer
//!
//!======================================================================================
 void RteVar_CopyfromBuffer(void) 
{
   //! ##### Copying the data from buffer into given address
   (void)Rte_MemCpy(Rte_StartAddr , &Rte_Var_Copy, &__RTE_VAR_SIZE);
}
//!======================================================================================
//!
//! \brief
//! This function implements the logic for the Disable_PIT
//!
//!======================================================================================
void Disable_PIT(uint8 data)
{
   PIT.MCR.B.MDIS = data;
} 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 //! @}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
