/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Isr_Lcfg.h
 *   Generation Time: 2020-10-30 14:38:54
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

#ifndef OS_ISR_LCFG_H
# define OS_ISR_LCFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
# include "Std_Types.h"

/* Os module declarations */
# include "Os_Isr_Types.h"

/* Os kernel module dependencies */
# include "Os_Lcfg.h"
# include "Os_Timer_Types.h"
# include "Os_XSignal_Types.h"

/* Os hal dependencies */


/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA PROTOTYPES
 *********************************************************************************************************************/

# define OS_START_SEC_CORE0_CONST_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! ISR configuration data: Ivor1Handler */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Cat0Isr_Ivor1Handler_HwConfig;

/*! ISR configuration data: CanIsr_0_MB00To03 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB00To03_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB00To03;

/*! ISR configuration data: CanIsr_0_MB04To07 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB04To07_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB04To07;

/*! ISR configuration data: CanIsr_0_MB08To11 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB08To11_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB08To11;

/*! ISR configuration data: CanIsr_0_MB12To15 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB12To15_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB12To15;

/*! ISR configuration data: CanIsr_0_MB16To31 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB16To31_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB16To31;

/*! ISR configuration data: CanIsr_0_MB32To63 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB32To63_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB32To63;

/*! ISR configuration data: CanIsr_0_MB64To95 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB64To95_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB64To95;

/*! ISR configuration data: CanIsr_1_MB00To03 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB00To03_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB00To03;

/*! ISR configuration data: CanIsr_1_MB04To07 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB04To07_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB04To07;

/*! ISR configuration data: CanIsr_1_MB08To11 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB08To11_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB08To11;

/*! ISR configuration data: CanIsr_1_MB12To15 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB12To15_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB12To15;

/*! ISR configuration data: CanIsr_1_MB16To31 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB16To31_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB16To31;

/*! ISR configuration data: CanIsr_1_MB32To63 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB32To63_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB32To63;

/*! ISR configuration data: CanIsr_1_MB64To95 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB64To95_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB64To95;

/*! ISR configuration data: CanIsr_2_MB00To03 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB00To03_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB00To03;

/*! ISR configuration data: CanIsr_2_MB04To07 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB04To07_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB04To07;

/*! ISR configuration data: CanIsr_2_MB08To11 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB08To11_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB08To11;

/*! ISR configuration data: CanIsr_2_MB12To15 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB12To15_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB12To15;

/*! ISR configuration data: CanIsr_2_MB16To31 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB16To31_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB16To31;

/*! ISR configuration data: CanIsr_2_MB32To63 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB32To63_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB32To63;

/*! ISR configuration data: CanIsr_2_MB64To95 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB64To95_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB64To95;

/*! ISR configuration data: CanIsr_4_MB00To03 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB00To03_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB00To03;

/*! ISR configuration data: CanIsr_4_MB04To07 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB04To07_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB04To07;

/*! ISR configuration data: CanIsr_4_MB08To11 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB08To11_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB08To11;

/*! ISR configuration data: CanIsr_4_MB12To15 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB12To15_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB12To15;

/*! ISR configuration data: CanIsr_4_MB16To31 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB16To31_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB16To31;

/*! ISR configuration data: CanIsr_4_MB32To63 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB32To63_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB32To63;

/*! ISR configuration data: CanIsr_4_MB64To95 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB64To95_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB64To95;

/*! ISR configuration data: CanIsr_6_MB00To03 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB00To03_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB00To03;

/*! ISR configuration data: CanIsr_6_MB04To07 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB04To07_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB04To07;

/*! ISR configuration data: CanIsr_6_MB08To11 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB08To11_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB08To11;

/*! ISR configuration data: CanIsr_6_MB12To15 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB12To15_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB12To15;

/*! ISR configuration data: CanIsr_6_MB16To31 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB16To31_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB16To31;

/*! ISR configuration data: CanIsr_6_MB32To63 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB32To63_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB32To63;

/*! ISR configuration data: CanIsr_6_MB64To95 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB64To95_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB64To95;

/*! ISR configuration data: CanIsr_7_MB00To03 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB00To03_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB00To03;

/*! ISR configuration data: CanIsr_7_MB04To07 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB04To07_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB04To07;

/*! ISR configuration data: CanIsr_7_MB08To11 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB08To11_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB08To11;

/*! ISR configuration data: CanIsr_7_MB12To15 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB12To15_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB12To15;

/*! ISR configuration data: CanIsr_7_MB16To31 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB16To31_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB16To31;

/*! ISR configuration data: CanIsr_7_MB32To63 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB32To63_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB32To63;

/*! ISR configuration data: CanIsr_7_MB64To95 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB64To95_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB64To95;

/*! ISR configuration data: CounterIsr_SystemTimer */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CounterIsr_SystemTimer_HwConfig;
extern CONST(Os_TimerIsrConfigType, OS_CONST) OsCfg_Isr_CounterIsr_SystemTimer;

/*! ISR configuration data: DOWHS1_EMIOS0_CH3_ISR */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_DOWHS1_EMIOS0_CH3_ISR_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_DOWHS1_EMIOS0_CH3_ISR;

/*! ISR configuration data: DOWHS2_EMIOS0_CH5_ISR */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_DOWHS2_EMIOS0_CH5_ISR_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_DOWHS2_EMIOS0_CH5_ISR;

/*! ISR configuration data: DOWLS2_EMIOS0_CH13_ISR */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_DOWLS2_EMIOS0_CH13_ISR_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_DOWLS2_EMIOS0_CH13_ISR;

/*! ISR configuration data: DOWLS2_EMIOS0_CH9_ISR */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_DOWLS2_EMIOS0_CH9_ISR_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_DOWLS2_EMIOS0_CH9_ISR;

/*! ISR configuration data: DOWLS3_EMIOS0_CH14_ISR */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_DOWLS3_EMIOS0_CH14_ISR_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_DOWLS3_EMIOS0_CH14_ISR;

/*! ISR configuration data: Gpt_PIT_0_TIMER_0_ISR */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Gpt_PIT_0_TIMER_0_ISR_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Gpt_PIT_0_TIMER_0_ISR;

/*! ISR configuration data: Gpt_PIT_0_TIMER_1_ISR */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Gpt_PIT_0_TIMER_1_ISR_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Gpt_PIT_0_TIMER_1_ISR;

/*! ISR configuration data: Gpt_PIT_0_TIMER_2_ISR */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Gpt_PIT_0_TIMER_2_ISR_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Gpt_PIT_0_TIMER_2_ISR;

/*! ISR configuration data: Lin_Channel_0_ERR */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_0_ERR_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_0_ERR;

/*! ISR configuration data: Lin_Channel_0_RXI */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_0_RXI_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_0_RXI;

/*! ISR configuration data: Lin_Channel_0_TXI */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_0_TXI_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_0_TXI;

/*! ISR configuration data: Lin_Channel_10_ERR */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_10_ERR_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_10_ERR;

/*! ISR configuration data: Lin_Channel_10_RXI */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_10_RXI_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_10_RXI;

/*! ISR configuration data: Lin_Channel_10_TXI */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_10_TXI_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_10_TXI;

/*! ISR configuration data: Lin_Channel_1_ERR */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_1_ERR_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_1_ERR;

/*! ISR configuration data: Lin_Channel_1_RXI */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_1_RXI_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_1_RXI;

/*! ISR configuration data: Lin_Channel_1_TXI */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_1_TXI_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_1_TXI;

/*! ISR configuration data: Lin_Channel_4_ERR */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_4_ERR_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_4_ERR;

/*! ISR configuration data: Lin_Channel_4_RXI */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_4_RXI_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_4_RXI;

/*! ISR configuration data: Lin_Channel_4_TXI */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_4_TXI_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_4_TXI;

/*! ISR configuration data: Lin_Channel_6_ERR */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_6_ERR_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_6_ERR;

/*! ISR configuration data: Lin_Channel_6_RXI */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_6_RXI_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_6_RXI;

/*! ISR configuration data: Lin_Channel_6_TXI */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_6_TXI_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_6_TXI;

/*! ISR configuration data: Lin_Channel_7_ERR */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_7_ERR_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_7_ERR;

/*! ISR configuration data: Lin_Channel_7_RXI */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_7_RXI_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_7_RXI;

/*! ISR configuration data: Lin_Channel_7_TXI */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_7_TXI_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_7_TXI;

/*! ISR configuration data: Lin_Channel_8_ERR */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_8_ERR_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_8_ERR;

/*! ISR configuration data: Lin_Channel_8_RXI */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_8_RXI_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_8_RXI;

/*! ISR configuration data: Lin_Channel_8_TXI */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_8_TXI_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_8_TXI;

/*! ISR configuration data: Lin_Channel_9_ERR */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_9_ERR_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_9_ERR;

/*! ISR configuration data: Lin_Channel_9_RXI */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_9_RXI_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_9_RXI;

/*! ISR configuration data: Lin_Channel_9_TXI */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_9_TXI_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_9_TXI;

/*! ISR configuration data: MCU_PLL_LossOfLock */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_MCU_PLL_LossOfLock_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_MCU_PLL_LossOfLock;

/*! ISR configuration data: WKUP_IRQ0 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_WKUP_IRQ0_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_WKUP_IRQ0;

/*! ISR configuration data: WKUP_IRQ1 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_WKUP_IRQ1_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_WKUP_IRQ1;

/*! ISR configuration data: WKUP_IRQ2 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_WKUP_IRQ2_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_WKUP_IRQ2;

/*! ISR configuration data: WKUP_IRQ3 */
extern CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_WKUP_IRQ3_HwConfig;
extern CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_WKUP_IRQ3;

# define OS_STOP_SEC_CORE0_CONST_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_CONST_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Object reference table for category 2 ISRs. */
extern CONSTP2CONST(Os_IsrConfigType, OS_CONST, OS_CONST) OsCfg_IsrRefs[OS_ISRID_COUNT + 1];  /* PRQA S 4521 */ /* MD_Os_Rule10.1_4521 */

# define OS_STOP_SEC_CONST_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/


#endif /* OS_ISR_LCFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_Isr_Lcfg.h
 *********************************************************************************************************************/
