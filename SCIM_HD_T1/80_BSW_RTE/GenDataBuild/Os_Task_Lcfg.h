/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Task_Lcfg.h
 *   Generation Time: 2020-08-20 13:43:07
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/

#ifndef OS_TASK_LCFG_H
# define OS_TASK_LCFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
# include "Std_Types.h"

/* Os module declarations */
# include "Os_Task_Types.h"

/* Os kernel module dependencies */
# include "Os_Ioc_Types.h"
# include "Os_Lcfg.h"

/* Os hal dependencies */


/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA PROTOTYPES
 *********************************************************************************************************************/

# define OS_START_SEC_CORE0_CONST_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Task configuration data: ASW_10ms_Task */
extern CONST(Os_TaskConfigType, OS_CONST) OsCfg_Task_ASW_10ms_Task;

/*! Task configuration data: ASW_20ms_Task */
extern CONST(Os_TaskConfigType, OS_CONST) OsCfg_Task_ASW_20ms_Task;

/*! Task configuration data: ASW_Async_Task */
extern CONST(Os_TaskConfigType, OS_CONST) OsCfg_Task_ASW_Async_Task;

/*! Task configuration data: ASW_Init_Task */
extern CONST(Os_TaskConfigType, OS_CONST) OsCfg_Task_ASW_Init_Task;

/*! Task configuration data: BSW_10ms_Task */
extern CONST(Os_TaskConfigType, OS_CONST) OsCfg_Task_BSW_10ms_Task;

/*! Task configuration data: BSW_5ms_Task */
extern CONST(Os_TaskConfigType, OS_CONST) OsCfg_Task_BSW_5ms_Task;

/*! Task configuration data: BSW_Async_Task */
extern CONST(Os_TaskConfigType, OS_CONST) OsCfg_Task_BSW_Async_Task;

/*! Task configuration data: BSW_Diag_Task */
extern CONST(Os_TaskConfigType, OS_CONST) OsCfg_Task_BSW_Diag_Task;

/*! Task configuration data: BSW_Lin_Task */
extern CONST(Os_TaskConfigType, OS_CONST) OsCfg_Task_BSW_Lin_Task;

/*! Task configuration data: CpuLoadIdleTask */
extern CONST(Os_TaskConfigType, OS_CONST) OsCfg_Task_CpuLoadIdleTask;

/*! Task configuration data: IdleTask_OsCore0 */
extern CONST(Os_TaskConfigType, OS_CONST) OsCfg_Task_IdleTask_OsCore0;

/*! Task configuration data: Init_Task */
extern CONST(Os_TaskConfigType, OS_CONST) OsCfg_Task_Init_Task;

# define OS_STOP_SEC_CORE0_CONST_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_CONST_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Object reference table for tasks. */
extern CONSTP2CONST(Os_TaskConfigType, OS_CONST, OS_CONST) OsCfg_TaskRefs[OS_TASKID_COUNT + 1];  /* PRQA S 4521 */ /* MD_Os_Rule10.1_4521 */

# define OS_STOP_SEC_CONST_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/


#endif /* OS_TASK_LCFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_Task_Lcfg.h
 *********************************************************************************************************************/
