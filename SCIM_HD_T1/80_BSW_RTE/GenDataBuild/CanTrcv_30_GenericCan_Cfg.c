/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: CanTrcv
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: CanTrcv_30_GenericCan_Cfg.c
 *   Generation Time: 2020-08-20 13:43:05
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/


/* PRQA S 0777 EOF */ /* MD_MSR_Rule5.1 */
/* PRQA S 0779 EOF */ /* MD_MSR_Rule5.2 */

/**********************************************************************************************************************
  INCLUDES
**********************************************************************************************************************/
#include "CanTrcv_30_GenericCan.h"

/**********************************************************************************************************************
  VERSION DEFINES (Adapted with: ESCAN00086365)
**********************************************************************************************************************/
#if (DRVTRANS_CAN_30_GENERICCAN_GENTOOL_CFG5_MAJOR_VERSION != 0x03u)
# error "CanTrcv_30_30_GenericCan_Cfg.c : Incompatible DRVTRANS_30_GENERICCAN_GENTOOL_CFG5_MAJOR_VERSION in generated File!"
#endif

#if (DRVTRANS_CAN_30_GENERICCAN_GENTOOL_CFG5_MINOR_VERSION != 0x01u)
# error "CanTrcv_30_30_GenericCan_Cfg.c : Incompatible DRVTRANS_30_GENERICCAN_GENTOOL_CFG5_MINOR_VERSION in generated File!"
#endif

#if (DRVTRANS_CAN_30_GENERICCAN_GENTOOL_CFG5_PATCH_VERSION != 0x00u)
# error "CanTrcv_30_30_GenericCan_Cfg.c : Incompatible DRVTRANS_30_GENERICCAN_GENTOOL_CFG5_PATCH_VERSION in generated File!"
#endif


/**********************************************************************************************************************
    ADDITIONAL (HW SPECIFIC)
**********************************************************************************************************************/



/**********************************************************************************************************************
  ComStackLib
**********************************************************************************************************************/
/**********************************************************************************************************************
  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/


/**********************************************************************************************************************
  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CanTrcv_30_GenericCan_Channel
**********************************************************************************************************************/
/** 
  \var    CanTrcv_30_GenericCan_Channel
  \details
  Element          Description
  IcuChannelSet
  IcuChannel   
*/ 
#define CANTRCV_30_GENERICCAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanTrcv_30_GenericCan_ChannelType, CANTRCV_30_GENERICCAN_CONST) CanTrcv_30_GenericCan_Channel[3] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    IcuChannelSet  IcuChannel                           */
  { /*     0 */          TRUE, IcuConf_IcuChannel_IcuChannel_CAN2RX },
  { /*     1 */          TRUE, IcuConf_IcuChannel_IcuChannel_CAN3RX },
  { /*     2 */          TRUE, IcuConf_IcuChannel_IcuChannel_CAN4RX }
};
#define CANTRCV_30_GENERICCAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTrcv_30_GenericCan_ChannelUsed
**********************************************************************************************************************/
#define CANTRCV_30_GENERICCAN_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanTrcv_30_GenericCan_ChannelUsedType, CANTRCV_30_GENERICCAN_CONST) CanTrcv_30_GenericCan_ChannelUsed[3] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     ChannelUsed  */
  /*     0 */         TRUE,
  /*     1 */         TRUE,
  /*     2 */         TRUE
};
#define CANTRCV_30_GENERICCAN_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTrcv_30_GenericCan_DioConfiguration
**********************************************************************************************************************/
/** 
  \var    CanTrcv_30_GenericCan_DioConfiguration
  \details
  Element    Description
  CANSTB 
*/ 
#define CANTRCV_30_GENERICCAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanTrcv_30_GenericCan_DioConfigurationType, CANTRCV_30_GENERICCAN_CONST) CanTrcv_30_GenericCan_DioConfiguration[3] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    CANSTB                      */
  { /*     0 */ DioConf_DioChannel_CAN2_STB },
  { /*     1 */ DioConf_DioChannel_CAN3_STB },
  { /*     2 */ DioConf_DioChannel_CAN4_STB }
};
#define CANTRCV_30_GENERICCAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTrcv_30_GenericCan_WakeupByBusUsed
**********************************************************************************************************************/
#define CANTRCV_30_GENERICCAN_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanTrcv_30_GenericCan_WakeupByBusUsedType, CANTRCV_30_GENERICCAN_CONST) CanTrcv_30_GenericCan_WakeupByBusUsed[3] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     WakeupByBusUsed  */
  /*     0 */            FALSE,
  /*     1 */            FALSE,
  /*     2 */            FALSE
};
#define CANTRCV_30_GENERICCAN_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTrcv_30_GenericCan_WakeupSource
**********************************************************************************************************************/
#define CANTRCV_30_GENERICCAN_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanTrcv_30_GenericCan_WakeupSourceType, CANTRCV_30_GENERICCAN_CONST) CanTrcv_30_GenericCan_WakeupSource[3] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     WakeupSource  */
  /*     0 */         0x00u,
  /*     1 */         0x00u,
  /*     2 */         0x00u
};
#define CANTRCV_30_GENERICCAN_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA
**********************************************************************************************************************/





