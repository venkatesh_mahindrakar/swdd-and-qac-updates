/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Hal_Interrupt_Lcfg.c
 *   Generation Time: 2020-10-30 14:38:53
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

/* PRQA S 0777, 0779, 0828 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2, MD_MSR_Dir1.1 */

#define OS_HAL_INTERRUPT_LCFG_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
#include "Std_Types.h"

/* Os module declarations */
#include "Os_Hal_Interrupt_Lcfg.h"
#include "Os_Hal_Interrupt.h"

/* Os kernel module dependencies */

/* Os hal dependencies */
#include "Os_Hal_Core.h"


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/



/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA
 *********************************************************************************************************************/

#define OS_START_SEC_CORE0_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! HAL ISR configuration data: CanIsr_0_MB00To03 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_0_MB00To03 =
{
  /* .Source         = */ 568,
  /* .Level          = */ OS_ISR_CANISR_0_MB00TO03_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_0_MB00To03 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_0_MB04To07 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_0_MB04To07 =
{
  /* .Source         = */ 569,
  /* .Level          = */ OS_ISR_CANISR_0_MB04TO07_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_0_MB04To07 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_0_MB08To11 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_0_MB08To11 =
{
  /* .Source         = */ 570,
  /* .Level          = */ OS_ISR_CANISR_0_MB08TO11_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_0_MB08To11 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_0_MB12To15 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_0_MB12To15 =
{
  /* .Source         = */ 571,
  /* .Level          = */ OS_ISR_CANISR_0_MB12TO15_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_0_MB12To15 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_0_MB16To31 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_0_MB16To31 =
{
  /* .Source         = */ 572,
  /* .Level          = */ OS_ISR_CANISR_0_MB16TO31_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_0_MB16To31 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_0_MB32To63 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_0_MB32To63 =
{
  /* .Source         = */ 573,
  /* .Level          = */ OS_ISR_CANISR_0_MB32TO63_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_0_MB32To63 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_0_MB64To95 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_0_MB64To95 =
{
  /* .Source         = */ 574,
  /* .Level          = */ OS_ISR_CANISR_0_MB64TO95_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_0_MB64To95 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_1_MB00To03 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_1_MB00To03 =
{
  /* .Source         = */ 580,
  /* .Level          = */ OS_ISR_CANISR_1_MB00TO03_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_1_MB00To03 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_1_MB04To07 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_1_MB04To07 =
{
  /* .Source         = */ 581,
  /* .Level          = */ OS_ISR_CANISR_1_MB04TO07_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_1_MB04To07 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_1_MB08To11 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_1_MB08To11 =
{
  /* .Source         = */ 582,
  /* .Level          = */ OS_ISR_CANISR_1_MB08TO11_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_1_MB08To11 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_1_MB12To15 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_1_MB12To15 =
{
  /* .Source         = */ 583,
  /* .Level          = */ OS_ISR_CANISR_1_MB12TO15_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_1_MB12To15 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_1_MB16To31 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_1_MB16To31 =
{
  /* .Source         = */ 584,
  /* .Level          = */ OS_ISR_CANISR_1_MB16TO31_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_1_MB16To31 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_1_MB32To63 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_1_MB32To63 =
{
  /* .Source         = */ 585,
  /* .Level          = */ OS_ISR_CANISR_1_MB32TO63_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_1_MB32To63 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_1_MB64To95 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_1_MB64To95 =
{
  /* .Source         = */ 586,
  /* .Level          = */ OS_ISR_CANISR_1_MB64TO95_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_1_MB64To95 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_2_MB00To03 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_2_MB00To03 =
{
  /* .Source         = */ 592,
  /* .Level          = */ OS_ISR_CANISR_2_MB00TO03_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_2_MB00To03 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_2_MB04To07 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_2_MB04To07 =
{
  /* .Source         = */ 593,
  /* .Level          = */ OS_ISR_CANISR_2_MB04TO07_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_2_MB04To07 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_2_MB08To11 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_2_MB08To11 =
{
  /* .Source         = */ 594,
  /* .Level          = */ OS_ISR_CANISR_2_MB08TO11_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_2_MB08To11 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_2_MB12To15 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_2_MB12To15 =
{
  /* .Source         = */ 595,
  /* .Level          = */ OS_ISR_CANISR_2_MB12TO15_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_2_MB12To15 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_2_MB16To31 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_2_MB16To31 =
{
  /* .Source         = */ 596,
  /* .Level          = */ OS_ISR_CANISR_2_MB16TO31_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_2_MB16To31 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_2_MB32To63 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_2_MB32To63 =
{
  /* .Source         = */ 597,
  /* .Level          = */ OS_ISR_CANISR_2_MB32TO63_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_2_MB32To63 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_2_MB64To95 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_2_MB64To95 =
{
  /* .Source         = */ 598,
  /* .Level          = */ OS_ISR_CANISR_2_MB64TO95_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_2_MB64To95 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_4_MB00To03 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_4_MB00To03 =
{
  /* .Source         = */ 616,
  /* .Level          = */ OS_ISR_CANISR_4_MB00TO03_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_4_MB00To03 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_4_MB04To07 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_4_MB04To07 =
{
  /* .Source         = */ 617,
  /* .Level          = */ OS_ISR_CANISR_4_MB04TO07_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_4_MB04To07 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_4_MB08To11 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_4_MB08To11 =
{
  /* .Source         = */ 618,
  /* .Level          = */ OS_ISR_CANISR_4_MB08TO11_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_4_MB08To11 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_4_MB12To15 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_4_MB12To15 =
{
  /* .Source         = */ 619,
  /* .Level          = */ OS_ISR_CANISR_4_MB12TO15_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_4_MB12To15 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_4_MB16To31 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_4_MB16To31 =
{
  /* .Source         = */ 620,
  /* .Level          = */ OS_ISR_CANISR_4_MB16TO31_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_4_MB16To31 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_4_MB32To63 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_4_MB32To63 =
{
  /* .Source         = */ 621,
  /* .Level          = */ OS_ISR_CANISR_4_MB32TO63_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_4_MB32To63 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_4_MB64To95 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_4_MB64To95 =
{
  /* .Source         = */ 622,
  /* .Level          = */ OS_ISR_CANISR_4_MB64TO95_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_4_MB64To95 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_6_MB00To03 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_6_MB00To03 =
{
  /* .Source         = */ 640,
  /* .Level          = */ OS_ISR_CANISR_6_MB00TO03_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_6_MB00To03 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_6_MB04To07 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_6_MB04To07 =
{
  /* .Source         = */ 641,
  /* .Level          = */ OS_ISR_CANISR_6_MB04TO07_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_6_MB04To07 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_6_MB08To11 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_6_MB08To11 =
{
  /* .Source         = */ 642,
  /* .Level          = */ OS_ISR_CANISR_6_MB08TO11_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_6_MB08To11 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_6_MB12To15 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_6_MB12To15 =
{
  /* .Source         = */ 643,
  /* .Level          = */ OS_ISR_CANISR_6_MB12TO15_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_6_MB12To15 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_6_MB16To31 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_6_MB16To31 =
{
  /* .Source         = */ 644,
  /* .Level          = */ OS_ISR_CANISR_6_MB16TO31_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_6_MB16To31 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_6_MB32To63 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_6_MB32To63 =
{
  /* .Source         = */ 645,
  /* .Level          = */ OS_ISR_CANISR_6_MB32TO63_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_6_MB32To63 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_6_MB64To95 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_6_MB64To95 =
{
  /* .Source         = */ 646,
  /* .Level          = */ OS_ISR_CANISR_6_MB64TO95_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_6_MB64To95 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_7_MB00To03 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_7_MB00To03 =
{
  /* .Source         = */ 652,
  /* .Level          = */ OS_ISR_CANISR_7_MB00TO03_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_7_MB00To03 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_7_MB04To07 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_7_MB04To07 =
{
  /* .Source         = */ 653,
  /* .Level          = */ OS_ISR_CANISR_7_MB04TO07_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_7_MB04To07 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_7_MB08To11 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_7_MB08To11 =
{
  /* .Source         = */ 654,
  /* .Level          = */ OS_ISR_CANISR_7_MB08TO11_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_7_MB08To11 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_7_MB12To15 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_7_MB12To15 =
{
  /* .Source         = */ 655,
  /* .Level          = */ OS_ISR_CANISR_7_MB12TO15_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_7_MB12To15 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_7_MB16To31 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_7_MB16To31 =
{
  /* .Source         = */ 656,
  /* .Level          = */ OS_ISR_CANISR_7_MB16TO31_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_7_MB16To31 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_7_MB32To63 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_7_MB32To63 =
{
  /* .Source         = */ 657,
  /* .Level          = */ OS_ISR_CANISR_7_MB32TO63_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_7_MB32To63 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CanIsr_7_MB64To95 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CanIsr_7_MB64To95 =
{
  /* .Source         = */ 658,
  /* .Level          = */ OS_ISR_CANISR_7_MB64TO95_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CanIsr_7_MB64To95 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: CounterIsr_SystemTimer */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_CounterIsr_SystemTimer =
{
  /* .Source         = */ 36,
  /* .Level          = */ OS_ISR_COUNTERISR_SYSTEMTIMER_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_CounterIsr_SystemTimer =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: DOWHS1_EMIOS0_CH3_ISR */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_DOWHS1_EMIOS0_CH3_ISR =
{
  /* .Source         = */ 707,
  /* .Level          = */ OS_ISR_DOWHS1_EMIOS0_CH3_ISR_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_DOWHS1_EMIOS0_CH3_ISR =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: DOWHS2_EMIOS0_CH5_ISR */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_DOWHS2_EMIOS0_CH5_ISR =
{
  /* .Source         = */ 708,
  /* .Level          = */ OS_ISR_DOWHS2_EMIOS0_CH5_ISR_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_DOWHS2_EMIOS0_CH5_ISR =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: DOWLS2_EMIOS0_CH13_ISR */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_DOWLS2_EMIOS0_CH13_ISR =
{
  /* .Source         = */ 712,
  /* .Level          = */ OS_ISR_DOWLS2_EMIOS0_CH13_ISR_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_DOWLS2_EMIOS0_CH13_ISR =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: DOWLS2_EMIOS0_CH9_ISR */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_DOWLS2_EMIOS0_CH9_ISR =
{
  /* .Source         = */ 710,
  /* .Level          = */ OS_ISR_DOWLS2_EMIOS0_CH9_ISR_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_DOWLS2_EMIOS0_CH9_ISR =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: DOWLS3_EMIOS0_CH14_ISR */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_DOWLS3_EMIOS0_CH14_ISR =
{
  /* .Source         = */ 713,
  /* .Level          = */ OS_ISR_DOWLS3_EMIOS0_CH14_ISR_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_DOWLS3_EMIOS0_CH14_ISR =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Gpt_PIT_0_TIMER_0_ISR */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Gpt_PIT_0_TIMER_0_ISR =
{
  /* .Source         = */ 226,
  /* .Level          = */ OS_ISR_GPT_PIT_0_TIMER_0_ISR_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Gpt_PIT_0_TIMER_0_ISR =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Gpt_PIT_0_TIMER_1_ISR */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Gpt_PIT_0_TIMER_1_ISR =
{
  /* .Source         = */ 227,
  /* .Level          = */ OS_ISR_GPT_PIT_0_TIMER_1_ISR_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Gpt_PIT_0_TIMER_1_ISR =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Gpt_PIT_0_TIMER_2_ISR */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Gpt_PIT_0_TIMER_2_ISR =
{
  /* .Source         = */ 228,
  /* .Level          = */ OS_ISR_GPT_PIT_0_TIMER_2_ISR_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Gpt_PIT_0_TIMER_2_ISR =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Ivor1Handler */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Ivor1Handler =
{
  /* .Source         = */ 1,
  /* .Level          = */ OS_ISR_IVOR1HANDLER_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Ivor1Handler =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_0_ERR */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_0_ERR =
{
  /* .Source         = */ 378,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_0_ERR_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_0_ERR =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_0_RXI */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_0_RXI =
{
  /* .Source         = */ 376,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_0_RXI_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_0_RXI =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_0_TXI */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_0_TXI =
{
  /* .Source         = */ 377,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_0_TXI_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_0_TXI =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_10_ERR */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_10_ERR =
{
  /* .Source         = */ 408,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_10_ERR_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_10_ERR =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_10_RXI */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_10_RXI =
{
  /* .Source         = */ 406,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_10_RXI_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_10_RXI =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_10_TXI */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_10_TXI =
{
  /* .Source         = */ 407,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_10_TXI_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_10_TXI =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_1_ERR */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_1_ERR =
{
  /* .Source         = */ 381,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_1_ERR_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_1_ERR =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_1_RXI */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_1_RXI =
{
  /* .Source         = */ 379,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_1_RXI_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_1_RXI =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_1_TXI */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_1_TXI =
{
  /* .Source         = */ 380,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_1_TXI_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_1_TXI =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_4_ERR */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_4_ERR =
{
  /* .Source         = */ 390,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_4_ERR_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_4_ERR =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_4_RXI */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_4_RXI =
{
  /* .Source         = */ 388,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_4_RXI_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_4_RXI =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_4_TXI */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_4_TXI =
{
  /* .Source         = */ 389,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_4_TXI_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_4_TXI =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_6_ERR */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_6_ERR =
{
  /* .Source         = */ 396,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_6_ERR_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_6_ERR =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_6_RXI */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_6_RXI =
{
  /* .Source         = */ 394,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_6_RXI_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_6_RXI =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_6_TXI */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_6_TXI =
{
  /* .Source         = */ 395,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_6_TXI_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_6_TXI =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_7_ERR */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_7_ERR =
{
  /* .Source         = */ 399,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_7_ERR_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_7_ERR =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_7_RXI */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_7_RXI =
{
  /* .Source         = */ 397,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_7_RXI_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_7_RXI =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_7_TXI */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_7_TXI =
{
  /* .Source         = */ 398,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_7_TXI_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_7_TXI =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_8_ERR */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_8_ERR =
{
  /* .Source         = */ 402,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_8_ERR_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_8_ERR =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_8_RXI */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_8_RXI =
{
  /* .Source         = */ 400,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_8_RXI_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_8_RXI =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_8_TXI */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_8_TXI =
{
  /* .Source         = */ 401,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_8_TXI_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_8_TXI =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_9_ERR */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_9_ERR =
{
  /* .Source         = */ 405,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_9_ERR_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_9_ERR =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_9_RXI */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_9_RXI =
{
  /* .Source         = */ 403,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_9_RXI_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_9_RXI =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: Lin_Channel_9_TXI */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_Lin_Channel_9_TXI =
{
  /* .Source         = */ 404,
  /* .Level          = */ OS_ISR_LIN_CHANNEL_9_TXI_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_Lin_Channel_9_TXI =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: MCU_PLL_LossOfLock */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_MCU_PLL_LossOfLock =
{
  /* .Source         = */ 480,
  /* .Level          = */ OS_ISR_MCU_PLL_LOSSOFLOCK_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_MCU_PLL_LossOfLock =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: WKUP_IRQ0 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_WKUP_IRQ0 =
{
  /* .Source         = */ 668,
  /* .Level          = */ OS_ISR_WKUP_IRQ0_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_WKUP_IRQ0 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: WKUP_IRQ1 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_WKUP_IRQ1 =
{
  /* .Source         = */ 669,
  /* .Level          = */ OS_ISR_WKUP_IRQ1_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_WKUP_IRQ1 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: WKUP_IRQ2 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_WKUP_IRQ2 =
{
  /* .Source         = */ 670,
  /* .Level          = */ OS_ISR_WKUP_IRQ2_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_WKUP_IRQ2 =
{
  /* .Dummy         = */ (uint32)0
};

/*! HAL ISR configuration data: WKUP_IRQ3 */
CONST(Os_Hal_IntIsrConfigType, OS_CONST) OsCfg_Hal_IntIsr_WKUP_IRQ3 =
{
  /* .Source         = */ 671,
  /* .Level          = */ OS_ISR_WKUP_IRQ3_LEVEL,
  /* .CoreAssignment = */ OS_HAL_INTC_ISR_2_CORE(0u)
};

CONST(Os_Hal_IntIsrMapConfigType, OS_CONST) OsCfg_Hal_IntIsrMap_WKUP_IRQ3 =
{
  /* .Dummy         = */ (uint32)0
};

#define OS_STOP_SEC_CORE0_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  END OF FILE: Os_Hal_Interrupt_Lcfg.c
 *********************************************************************************************************************/
