/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Isr_Lcfg.c
 *   Generation Time: 2020-10-30 14:38:54
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

/* PRQA S 0777, 0779, 0828 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2, MD_MSR_Dir1.1 */

#define OS_ISR_LCFG_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
#include "Std_Types.h"

/* Os module declarations */
#include "Os_Isr_Lcfg.h"
#include "Os_Isr.h"

/* Os kernel module dependencies */
#include "Os_AccessCheck_Lcfg.h"
#include "Os_Application_Lcfg.h"
#include "Os_Common.h"
#include "Os_Core_Lcfg.h"
#include "Os_Counter_Lcfg.h"
#include "Os_MemoryProtection_Lcfg.h"
#include "Os_Stack_Lcfg.h"
#include "Os_Thread.h"
#include "Os_Timer.h"
#include "Os_TimingProtection_Lcfg.h"
#include "Os_Trace_Lcfg.h"
#include "Os_XSignal_Lcfg.h"
#include "Os_XSignal.h"

/* Os hal dependencies */
#include "Os_Hal_Context_Lcfg.h"
#include "Os_Hal_Interrupt_Lcfg.h"


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

#define OS_START_SEC_CORE0_VAR_NOINIT_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Dynamic ISR data: CanIsr_0_MB00To03 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_0_MB00To03_Dyn;

/*! Dynamic ISR data: CanIsr_0_MB04To07 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_0_MB04To07_Dyn;

/*! Dynamic ISR data: CanIsr_0_MB08To11 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_0_MB08To11_Dyn;

/*! Dynamic ISR data: CanIsr_0_MB12To15 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_0_MB12To15_Dyn;

/*! Dynamic ISR data: CanIsr_0_MB16To31 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_0_MB16To31_Dyn;

/*! Dynamic ISR data: CanIsr_0_MB32To63 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_0_MB32To63_Dyn;

/*! Dynamic ISR data: CanIsr_0_MB64To95 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_0_MB64To95_Dyn;

/*! Dynamic ISR data: CanIsr_1_MB00To03 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_1_MB00To03_Dyn;

/*! Dynamic ISR data: CanIsr_1_MB04To07 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_1_MB04To07_Dyn;

/*! Dynamic ISR data: CanIsr_1_MB08To11 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_1_MB08To11_Dyn;

/*! Dynamic ISR data: CanIsr_1_MB12To15 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_1_MB12To15_Dyn;

/*! Dynamic ISR data: CanIsr_1_MB16To31 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_1_MB16To31_Dyn;

/*! Dynamic ISR data: CanIsr_1_MB32To63 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_1_MB32To63_Dyn;

/*! Dynamic ISR data: CanIsr_1_MB64To95 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_1_MB64To95_Dyn;

/*! Dynamic ISR data: CanIsr_2_MB00To03 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_2_MB00To03_Dyn;

/*! Dynamic ISR data: CanIsr_2_MB04To07 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_2_MB04To07_Dyn;

/*! Dynamic ISR data: CanIsr_2_MB08To11 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_2_MB08To11_Dyn;

/*! Dynamic ISR data: CanIsr_2_MB12To15 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_2_MB12To15_Dyn;

/*! Dynamic ISR data: CanIsr_2_MB16To31 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_2_MB16To31_Dyn;

/*! Dynamic ISR data: CanIsr_2_MB32To63 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_2_MB32To63_Dyn;

/*! Dynamic ISR data: CanIsr_2_MB64To95 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_2_MB64To95_Dyn;

/*! Dynamic ISR data: CanIsr_4_MB00To03 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_4_MB00To03_Dyn;

/*! Dynamic ISR data: CanIsr_4_MB04To07 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_4_MB04To07_Dyn;

/*! Dynamic ISR data: CanIsr_4_MB08To11 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_4_MB08To11_Dyn;

/*! Dynamic ISR data: CanIsr_4_MB12To15 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_4_MB12To15_Dyn;

/*! Dynamic ISR data: CanIsr_4_MB16To31 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_4_MB16To31_Dyn;

/*! Dynamic ISR data: CanIsr_4_MB32To63 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_4_MB32To63_Dyn;

/*! Dynamic ISR data: CanIsr_4_MB64To95 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_4_MB64To95_Dyn;

/*! Dynamic ISR data: CanIsr_6_MB00To03 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_6_MB00To03_Dyn;

/*! Dynamic ISR data: CanIsr_6_MB04To07 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_6_MB04To07_Dyn;

/*! Dynamic ISR data: CanIsr_6_MB08To11 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_6_MB08To11_Dyn;

/*! Dynamic ISR data: CanIsr_6_MB12To15 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_6_MB12To15_Dyn;

/*! Dynamic ISR data: CanIsr_6_MB16To31 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_6_MB16To31_Dyn;

/*! Dynamic ISR data: CanIsr_6_MB32To63 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_6_MB32To63_Dyn;

/*! Dynamic ISR data: CanIsr_6_MB64To95 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_6_MB64To95_Dyn;

/*! Dynamic ISR data: CanIsr_7_MB00To03 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_7_MB00To03_Dyn;

/*! Dynamic ISR data: CanIsr_7_MB04To07 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_7_MB04To07_Dyn;

/*! Dynamic ISR data: CanIsr_7_MB08To11 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_7_MB08To11_Dyn;

/*! Dynamic ISR data: CanIsr_7_MB12To15 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_7_MB12To15_Dyn;

/*! Dynamic ISR data: CanIsr_7_MB16To31 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_7_MB16To31_Dyn;

/*! Dynamic ISR data: CanIsr_7_MB32To63 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_7_MB32To63_Dyn;

/*! Dynamic ISR data: CanIsr_7_MB64To95 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CanIsr_7_MB64To95_Dyn;

/*! Dynamic ISR data: CounterIsr_SystemTimer */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_CounterIsr_SystemTimer_Dyn;

/*! Dynamic ISR data: DOWHS1_EMIOS0_CH3_ISR */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_DOWHS1_EMIOS0_CH3_ISR_Dyn;

/*! Dynamic ISR data: DOWHS2_EMIOS0_CH5_ISR */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_DOWHS2_EMIOS0_CH5_ISR_Dyn;

/*! Dynamic ISR data: DOWLS2_EMIOS0_CH13_ISR */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_DOWLS2_EMIOS0_CH13_ISR_Dyn;

/*! Dynamic ISR data: DOWLS2_EMIOS0_CH9_ISR */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_DOWLS2_EMIOS0_CH9_ISR_Dyn;

/*! Dynamic ISR data: DOWLS3_EMIOS0_CH14_ISR */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_DOWLS3_EMIOS0_CH14_ISR_Dyn;

/*! Dynamic ISR data: Gpt_PIT_0_TIMER_0_ISR */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Gpt_PIT_0_TIMER_0_ISR_Dyn;

/*! Dynamic ISR data: Gpt_PIT_0_TIMER_1_ISR */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Gpt_PIT_0_TIMER_1_ISR_Dyn;

/*! Dynamic ISR data: Gpt_PIT_0_TIMER_2_ISR */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Gpt_PIT_0_TIMER_2_ISR_Dyn;

/*! Dynamic ISR data: Lin_Channel_0_ERR */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_0_ERR_Dyn;

/*! Dynamic ISR data: Lin_Channel_0_RXI */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_0_RXI_Dyn;

/*! Dynamic ISR data: Lin_Channel_0_TXI */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_0_TXI_Dyn;

/*! Dynamic ISR data: Lin_Channel_10_ERR */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_10_ERR_Dyn;

/*! Dynamic ISR data: Lin_Channel_10_RXI */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_10_RXI_Dyn;

/*! Dynamic ISR data: Lin_Channel_10_TXI */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_10_TXI_Dyn;

/*! Dynamic ISR data: Lin_Channel_1_ERR */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_1_ERR_Dyn;

/*! Dynamic ISR data: Lin_Channel_1_RXI */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_1_RXI_Dyn;

/*! Dynamic ISR data: Lin_Channel_1_TXI */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_1_TXI_Dyn;

/*! Dynamic ISR data: Lin_Channel_4_ERR */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_4_ERR_Dyn;

/*! Dynamic ISR data: Lin_Channel_4_RXI */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_4_RXI_Dyn;

/*! Dynamic ISR data: Lin_Channel_4_TXI */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_4_TXI_Dyn;

/*! Dynamic ISR data: Lin_Channel_6_ERR */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_6_ERR_Dyn;

/*! Dynamic ISR data: Lin_Channel_6_RXI */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_6_RXI_Dyn;

/*! Dynamic ISR data: Lin_Channel_6_TXI */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_6_TXI_Dyn;

/*! Dynamic ISR data: Lin_Channel_7_ERR */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_7_ERR_Dyn;

/*! Dynamic ISR data: Lin_Channel_7_RXI */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_7_RXI_Dyn;

/*! Dynamic ISR data: Lin_Channel_7_TXI */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_7_TXI_Dyn;

/*! Dynamic ISR data: Lin_Channel_8_ERR */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_8_ERR_Dyn;

/*! Dynamic ISR data: Lin_Channel_8_RXI */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_8_RXI_Dyn;

/*! Dynamic ISR data: Lin_Channel_8_TXI */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_8_TXI_Dyn;

/*! Dynamic ISR data: Lin_Channel_9_ERR */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_9_ERR_Dyn;

/*! Dynamic ISR data: Lin_Channel_9_RXI */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_9_RXI_Dyn;

/*! Dynamic ISR data: Lin_Channel_9_TXI */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_Lin_Channel_9_TXI_Dyn;

/*! Dynamic ISR data: MCU_PLL_LossOfLock */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_MCU_PLL_LossOfLock_Dyn;

/*! Dynamic ISR data: WKUP_IRQ0 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_WKUP_IRQ0_Dyn;

/*! Dynamic ISR data: WKUP_IRQ1 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_WKUP_IRQ1_Dyn;

/*! Dynamic ISR data: WKUP_IRQ2 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_WKUP_IRQ2_Dyn;

/*! Dynamic ISR data: WKUP_IRQ3 */
OS_LOCAL VAR(Os_IsrType, OS_VAR_NOINIT) OsCfg_Isr_WKUP_IRQ3_Dyn;

#define OS_STOP_SEC_CORE0_VAR_NOINIT_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA
 *********************************************************************************************************************/

#define OS_START_SEC_CORE0_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! ISR configuration data: CanIsr_0_MB00To03 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB00To03_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_0_MB00To03,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_0_MB00To03,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB00To03 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_0_MB00To03,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level10_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_0_MB00To03_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_0_MB00To03,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_0_MB00To03_HwConfig,
  /* .IsrId                     = */ CanIsr_0_MB00To03,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_0_MB04To07 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB04To07_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_0_MB04To07,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_0_MB04To07,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB04To07 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_0_MB04To07,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level10_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_0_MB04To07_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_0_MB04To07,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_0_MB04To07_HwConfig,
  /* .IsrId                     = */ CanIsr_0_MB04To07,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_0_MB08To11 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB08To11_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_0_MB08To11,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_0_MB08To11,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB08To11 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_0_MB08To11,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level10_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_0_MB08To11_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_0_MB08To11,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_0_MB08To11_HwConfig,
  /* .IsrId                     = */ CanIsr_0_MB08To11,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_0_MB12To15 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB12To15_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_0_MB12To15,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_0_MB12To15,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB12To15 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_0_MB12To15,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level10_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_0_MB12To15_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_0_MB12To15,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_0_MB12To15_HwConfig,
  /* .IsrId                     = */ CanIsr_0_MB12To15,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_0_MB16To31 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB16To31_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_0_MB16To31,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_0_MB16To31,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB16To31 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_0_MB16To31,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level10_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_0_MB16To31_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_0_MB16To31,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_0_MB16To31_HwConfig,
  /* .IsrId                     = */ CanIsr_0_MB16To31,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_0_MB32To63 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB32To63_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_0_MB32To63,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_0_MB32To63,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB32To63 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_0_MB32To63,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level10_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_0_MB32To63_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_0_MB32To63,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_0_MB32To63_HwConfig,
  /* .IsrId                     = */ CanIsr_0_MB32To63,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_0_MB64To95 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB64To95_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_0_MB64To95,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_0_MB64To95,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_0_MB64To95 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_0_MB64To95,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level10_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_0_MB64To95_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_0_MB64To95,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_0_MB64To95_HwConfig,
  /* .IsrId                     = */ CanIsr_0_MB64To95,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_1_MB00To03 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB00To03_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_1_MB00To03,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_1_MB00To03,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB00To03 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_1_MB00To03,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level6_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_1_MB00To03_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_1_MB00To03,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_1_MB00To03_HwConfig,
  /* .IsrId                     = */ CanIsr_1_MB00To03,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_1_MB04To07 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB04To07_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_1_MB04To07,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_1_MB04To07,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB04To07 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_1_MB04To07,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level6_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_1_MB04To07_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_1_MB04To07,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_1_MB04To07_HwConfig,
  /* .IsrId                     = */ CanIsr_1_MB04To07,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_1_MB08To11 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB08To11_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_1_MB08To11,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_1_MB08To11,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB08To11 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_1_MB08To11,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level6_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_1_MB08To11_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_1_MB08To11,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_1_MB08To11_HwConfig,
  /* .IsrId                     = */ CanIsr_1_MB08To11,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_1_MB12To15 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB12To15_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_1_MB12To15,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_1_MB12To15,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB12To15 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_1_MB12To15,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level6_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_1_MB12To15_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_1_MB12To15,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_1_MB12To15_HwConfig,
  /* .IsrId                     = */ CanIsr_1_MB12To15,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_1_MB16To31 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB16To31_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_1_MB16To31,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_1_MB16To31,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB16To31 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_1_MB16To31,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level6_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_1_MB16To31_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_1_MB16To31,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_1_MB16To31_HwConfig,
  /* .IsrId                     = */ CanIsr_1_MB16To31,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_1_MB32To63 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB32To63_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_1_MB32To63,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_1_MB32To63,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB32To63 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_1_MB32To63,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level6_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_1_MB32To63_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_1_MB32To63,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_1_MB32To63_HwConfig,
  /* .IsrId                     = */ CanIsr_1_MB32To63,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_1_MB64To95 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB64To95_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_1_MB64To95,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_1_MB64To95,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_1_MB64To95 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_1_MB64To95,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level6_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_1_MB64To95_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_1_MB64To95,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_1_MB64To95_HwConfig,
  /* .IsrId                     = */ CanIsr_1_MB64To95,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_2_MB00To03 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB00To03_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_2_MB00To03,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_2_MB00To03,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB00To03 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_2_MB00To03,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level5_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_2_MB00To03_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_2_MB00To03,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_2_MB00To03_HwConfig,
  /* .IsrId                     = */ CanIsr_2_MB00To03,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_2_MB04To07 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB04To07_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_2_MB04To07,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_2_MB04To07,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB04To07 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_2_MB04To07,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level5_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_2_MB04To07_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_2_MB04To07,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_2_MB04To07_HwConfig,
  /* .IsrId                     = */ CanIsr_2_MB04To07,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_2_MB08To11 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB08To11_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_2_MB08To11,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_2_MB08To11,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB08To11 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_2_MB08To11,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level5_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_2_MB08To11_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_2_MB08To11,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_2_MB08To11_HwConfig,
  /* .IsrId                     = */ CanIsr_2_MB08To11,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_2_MB12To15 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB12To15_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_2_MB12To15,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_2_MB12To15,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB12To15 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_2_MB12To15,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level5_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_2_MB12To15_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_2_MB12To15,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_2_MB12To15_HwConfig,
  /* .IsrId                     = */ CanIsr_2_MB12To15,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_2_MB16To31 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB16To31_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_2_MB16To31,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_2_MB16To31,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB16To31 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_2_MB16To31,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level5_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_2_MB16To31_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_2_MB16To31,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_2_MB16To31_HwConfig,
  /* .IsrId                     = */ CanIsr_2_MB16To31,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_2_MB32To63 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB32To63_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_2_MB32To63,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_2_MB32To63,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB32To63 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_2_MB32To63,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level5_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_2_MB32To63_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_2_MB32To63,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_2_MB32To63_HwConfig,
  /* .IsrId                     = */ CanIsr_2_MB32To63,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_2_MB64To95 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB64To95_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_2_MB64To95,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_2_MB64To95,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_2_MB64To95 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_2_MB64To95,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level5_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_2_MB64To95_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_2_MB64To95,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_2_MB64To95_HwConfig,
  /* .IsrId                     = */ CanIsr_2_MB64To95,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_4_MB00To03 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB00To03_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_4_MB00To03,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_4_MB00To03,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB00To03 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_4_MB00To03,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level8_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_4_MB00To03_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_4_MB00To03,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_4_MB00To03_HwConfig,
  /* .IsrId                     = */ CanIsr_4_MB00To03,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_4_MB04To07 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB04To07_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_4_MB04To07,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_4_MB04To07,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB04To07 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_4_MB04To07,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level8_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_4_MB04To07_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_4_MB04To07,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_4_MB04To07_HwConfig,
  /* .IsrId                     = */ CanIsr_4_MB04To07,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_4_MB08To11 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB08To11_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_4_MB08To11,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_4_MB08To11,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB08To11 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_4_MB08To11,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level8_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_4_MB08To11_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_4_MB08To11,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_4_MB08To11_HwConfig,
  /* .IsrId                     = */ CanIsr_4_MB08To11,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_4_MB12To15 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB12To15_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_4_MB12To15,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_4_MB12To15,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB12To15 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_4_MB12To15,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level8_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_4_MB12To15_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_4_MB12To15,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_4_MB12To15_HwConfig,
  /* .IsrId                     = */ CanIsr_4_MB12To15,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_4_MB16To31 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB16To31_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_4_MB16To31,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_4_MB16To31,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB16To31 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_4_MB16To31,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level8_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_4_MB16To31_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_4_MB16To31,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_4_MB16To31_HwConfig,
  /* .IsrId                     = */ CanIsr_4_MB16To31,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_4_MB32To63 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB32To63_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_4_MB32To63,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_4_MB32To63,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB32To63 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_4_MB32To63,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level8_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_4_MB32To63_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_4_MB32To63,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_4_MB32To63_HwConfig,
  /* .IsrId                     = */ CanIsr_4_MB32To63,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_4_MB64To95 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB64To95_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_4_MB64To95,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_4_MB64To95,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_4_MB64To95 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_4_MB64To95,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level8_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_4_MB64To95_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_4_MB64To95,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_4_MB64To95_HwConfig,
  /* .IsrId                     = */ CanIsr_4_MB64To95,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_6_MB00To03 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB00To03_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_6_MB00To03,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_6_MB00To03,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB00To03 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_6_MB00To03,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level9_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_6_MB00To03_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_6_MB00To03,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_6_MB00To03_HwConfig,
  /* .IsrId                     = */ CanIsr_6_MB00To03,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_6_MB04To07 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB04To07_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_6_MB04To07,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_6_MB04To07,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB04To07 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_6_MB04To07,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level9_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_6_MB04To07_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_6_MB04To07,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_6_MB04To07_HwConfig,
  /* .IsrId                     = */ CanIsr_6_MB04To07,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_6_MB08To11 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB08To11_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_6_MB08To11,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_6_MB08To11,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB08To11 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_6_MB08To11,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level9_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_6_MB08To11_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_6_MB08To11,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_6_MB08To11_HwConfig,
  /* .IsrId                     = */ CanIsr_6_MB08To11,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_6_MB12To15 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB12To15_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_6_MB12To15,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_6_MB12To15,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB12To15 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_6_MB12To15,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level9_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_6_MB12To15_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_6_MB12To15,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_6_MB12To15_HwConfig,
  /* .IsrId                     = */ CanIsr_6_MB12To15,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_6_MB16To31 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB16To31_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_6_MB16To31,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_6_MB16To31,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB16To31 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_6_MB16To31,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level9_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_6_MB16To31_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_6_MB16To31,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_6_MB16To31_HwConfig,
  /* .IsrId                     = */ CanIsr_6_MB16To31,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_6_MB32To63 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB32To63_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_6_MB32To63,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_6_MB32To63,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB32To63 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_6_MB32To63,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level9_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_6_MB32To63_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_6_MB32To63,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_6_MB32To63_HwConfig,
  /* .IsrId                     = */ CanIsr_6_MB32To63,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_6_MB64To95 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB64To95_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_6_MB64To95,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_6_MB64To95,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_6_MB64To95 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_6_MB64To95,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level9_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_6_MB64To95_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_6_MB64To95,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_6_MB64To95_HwConfig,
  /* .IsrId                     = */ CanIsr_6_MB64To95,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_7_MB00To03 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB00To03_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_7_MB00To03,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_7_MB00To03,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB00To03 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_7_MB00To03,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level7_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_7_MB00To03_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_7_MB00To03,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_7_MB00To03_HwConfig,
  /* .IsrId                     = */ CanIsr_7_MB00To03,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_7_MB04To07 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB04To07_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_7_MB04To07,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_7_MB04To07,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB04To07 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_7_MB04To07,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level7_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_7_MB04To07_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_7_MB04To07,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_7_MB04To07_HwConfig,
  /* .IsrId                     = */ CanIsr_7_MB04To07,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_7_MB08To11 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB08To11_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_7_MB08To11,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_7_MB08To11,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB08To11 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_7_MB08To11,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level7_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_7_MB08To11_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_7_MB08To11,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_7_MB08To11_HwConfig,
  /* .IsrId                     = */ CanIsr_7_MB08To11,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_7_MB12To15 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB12To15_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_7_MB12To15,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_7_MB12To15,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB12To15 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_7_MB12To15,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level7_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_7_MB12To15_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_7_MB12To15,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_7_MB12To15_HwConfig,
  /* .IsrId                     = */ CanIsr_7_MB12To15,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_7_MB16To31 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB16To31_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_7_MB16To31,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_7_MB16To31,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB16To31 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_7_MB16To31,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level7_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_7_MB16To31_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_7_MB16To31,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_7_MB16To31_HwConfig,
  /* .IsrId                     = */ CanIsr_7_MB16To31,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_7_MB32To63 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB32To63_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_7_MB32To63,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_7_MB32To63,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB32To63 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_7_MB32To63,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level7_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_7_MB32To63_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_7_MB32To63,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_7_MB32To63_HwConfig,
  /* .IsrId                     = */ CanIsr_7_MB32To63,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CanIsr_7_MB64To95 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB64To95_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CanIsr_7_MB64To95,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CanIsr_7_MB64To95,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_CanIsr_7_MB64To95 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CanIsr_7_MB64To95,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level7_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CanIsr_7_MB64To95_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CanIsr_7_MB64To95,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CanIsr_7_MB64To95_HwConfig,
  /* .IsrId                     = */ CanIsr_7_MB64To95,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: CounterIsr_SystemTimer */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_CounterIsr_SystemTimer_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_CounterIsr_SystemTimer,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_CounterIsr_SystemTimer,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_TimerIsrConfigType, OS_CONST) OsCfg_Isr_CounterIsr_SystemTimer =
{
  /* .Isr     = */
  {
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_CounterIsr_SystemTimer,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level15_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_CounterIsr_SystemTimer_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_SystemApplication_OsCore0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_CounterIsr_SystemTimer,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_CounterIsr_SystemTimer_HwConfig,
  /* .IsrId                     = */ CounterIsr_SystemTimer,
  /* .IsEnabledOnInitialization = */ FALSE
}
,
  /* .Counter = */ OS_COUNTER_CASTCONFIG_TIMERPFRT_2_COUNTER(OsCfg_Counter_SystemTimer)
};
/*! ISR configuration data: DOWHS1_EMIOS0_CH3_ISR */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_DOWHS1_EMIOS0_CH3_ISR_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_DOWHS1_EMIOS0_CH3_ISR,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_DOWHS1_EMIOS0_CH3_ISR,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_DOWHS1_EMIOS0_CH3_ISR =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_DOWHS1_EMIOS0_CH3_ISR,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level2_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_DOWHS1_EMIOS0_CH3_ISR_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_DOWHS1_EMIOS0_CH3_ISR,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_DOWHS1_EMIOS0_CH3_ISR_HwConfig,
  /* .IsrId                     = */ DOWHS1_EMIOS0_CH3_ISR,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: DOWHS2_EMIOS0_CH5_ISR */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_DOWHS2_EMIOS0_CH5_ISR_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_DOWHS2_EMIOS0_CH5_ISR,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_DOWHS2_EMIOS0_CH5_ISR,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_DOWHS2_EMIOS0_CH5_ISR =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_DOWHS2_EMIOS0_CH5_ISR,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level3_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_DOWHS2_EMIOS0_CH5_ISR_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_DOWHS2_EMIOS0_CH5_ISR,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_DOWHS2_EMIOS0_CH5_ISR_HwConfig,
  /* .IsrId                     = */ DOWHS2_EMIOS0_CH5_ISR,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: DOWLS2_EMIOS0_CH13_ISR */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_DOWLS2_EMIOS0_CH13_ISR_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_DOWLS2_EMIOS0_CH13_ISR,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_DOWLS2_EMIOS0_CH13_ISR,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_DOWLS2_EMIOS0_CH13_ISR =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_DOWLS2_EMIOS0_CH13_ISR,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level6_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_DOWLS2_EMIOS0_CH13_ISR_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_DOWLS2_EMIOS0_CH13_ISR,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_DOWLS2_EMIOS0_CH13_ISR_HwConfig,
  /* .IsrId                     = */ DOWLS2_EMIOS0_CH13_ISR,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: DOWLS2_EMIOS0_CH9_ISR */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_DOWLS2_EMIOS0_CH9_ISR_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_DOWLS2_EMIOS0_CH9_ISR,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_DOWLS2_EMIOS0_CH9_ISR,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_DOWLS2_EMIOS0_CH9_ISR =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_DOWLS2_EMIOS0_CH9_ISR,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level5_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_DOWLS2_EMIOS0_CH9_ISR_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_DOWLS2_EMIOS0_CH9_ISR,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_DOWLS2_EMIOS0_CH9_ISR_HwConfig,
  /* .IsrId                     = */ DOWLS2_EMIOS0_CH9_ISR,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: DOWLS3_EMIOS0_CH14_ISR */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_DOWLS3_EMIOS0_CH14_ISR_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_DOWLS3_EMIOS0_CH14_ISR,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_DOWLS3_EMIOS0_CH14_ISR,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_DOWLS3_EMIOS0_CH14_ISR =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_DOWLS3_EMIOS0_CH14_ISR,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level4_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_DOWLS3_EMIOS0_CH14_ISR_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_DOWLS3_EMIOS0_CH14_ISR,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_DOWLS3_EMIOS0_CH14_ISR_HwConfig,
  /* .IsrId                     = */ DOWLS3_EMIOS0_CH14_ISR,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Gpt_PIT_0_TIMER_0_ISR */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Gpt_PIT_0_TIMER_0_ISR_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Gpt_PIT_0_TIMER_0_ISR,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Gpt_PIT_0_TIMER_0_ISR,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Gpt_PIT_0_TIMER_0_ISR =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Gpt_PIT_0_TIMER_0_ISR,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level1_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Gpt_PIT_0_TIMER_0_ISR_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Gpt_PIT_0_TIMER_0_ISR,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Gpt_PIT_0_TIMER_0_ISR_HwConfig,
  /* .IsrId                     = */ Gpt_PIT_0_TIMER_0_ISR,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Gpt_PIT_0_TIMER_1_ISR */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Gpt_PIT_0_TIMER_1_ISR_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Gpt_PIT_0_TIMER_1_ISR,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Gpt_PIT_0_TIMER_1_ISR,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Gpt_PIT_0_TIMER_1_ISR =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Gpt_PIT_0_TIMER_1_ISR,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level3_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Gpt_PIT_0_TIMER_1_ISR_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Gpt_PIT_0_TIMER_1_ISR,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Gpt_PIT_0_TIMER_1_ISR_HwConfig,
  /* .IsrId                     = */ Gpt_PIT_0_TIMER_1_ISR,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Gpt_PIT_0_TIMER_2_ISR */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Gpt_PIT_0_TIMER_2_ISR_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Gpt_PIT_0_TIMER_2_ISR,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Gpt_PIT_0_TIMER_2_ISR,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Gpt_PIT_0_TIMER_2_ISR =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Gpt_PIT_0_TIMER_2_ISR,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level4_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Gpt_PIT_0_TIMER_2_ISR_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Gpt_PIT_0_TIMER_2_ISR,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Gpt_PIT_0_TIMER_2_ISR_HwConfig,
  /* .IsrId                     = */ Gpt_PIT_0_TIMER_2_ISR,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Ivor1Handler */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Cat0Isr_Ivor1Handler_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Ivor1Handler,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Ivor1Handler,
  /* .IsMapped                  = */ TRUE
}
;  
/*! ISR configuration data: Lin_Channel_0_ERR */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_0_ERR_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_0_ERR,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_0_ERR,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_0_ERR =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_0_ERR,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level13_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_0_ERR_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_0_ERR,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_0_ERR_HwConfig,
  /* .IsrId                     = */ Lin_Channel_0_ERR,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_0_RXI */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_0_RXI_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_0_RXI,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_0_RXI,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_0_RXI =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_0_RXI,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level13_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_0_RXI_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_0_RXI,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_0_RXI_HwConfig,
  /* .IsrId                     = */ Lin_Channel_0_RXI,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_0_TXI */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_0_TXI_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_0_TXI,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_0_TXI,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_0_TXI =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_0_TXI,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level13_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_0_TXI_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_0_TXI,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_0_TXI_HwConfig,
  /* .IsrId                     = */ Lin_Channel_0_TXI,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_10_ERR */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_10_ERR_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_10_ERR,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_10_ERR,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_10_ERR =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_10_ERR,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level14_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_10_ERR_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_10_ERR,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_10_ERR_HwConfig,
  /* .IsrId                     = */ Lin_Channel_10_ERR,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_10_RXI */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_10_RXI_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_10_RXI,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_10_RXI,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_10_RXI =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_10_RXI,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level14_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_10_RXI_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_10_RXI,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_10_RXI_HwConfig,
  /* .IsrId                     = */ Lin_Channel_10_RXI,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_10_TXI */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_10_TXI_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_10_TXI,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_10_TXI,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_10_TXI =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_10_TXI,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level14_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_10_TXI_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_10_TXI,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_10_TXI_HwConfig,
  /* .IsrId                     = */ Lin_Channel_10_TXI,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_1_ERR */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_1_ERR_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_1_ERR,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_1_ERR,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_1_ERR =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_1_ERR,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level11_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_1_ERR_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_1_ERR,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_1_ERR_HwConfig,
  /* .IsrId                     = */ Lin_Channel_1_ERR,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_1_RXI */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_1_RXI_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_1_RXI,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_1_RXI,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_1_RXI =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_1_RXI,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level11_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_1_RXI_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_1_RXI,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_1_RXI_HwConfig,
  /* .IsrId                     = */ Lin_Channel_1_RXI,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_1_TXI */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_1_TXI_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_1_TXI,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_1_TXI,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_1_TXI =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_1_TXI,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level11_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_1_TXI_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_1_TXI,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_1_TXI_HwConfig,
  /* .IsrId                     = */ Lin_Channel_1_TXI,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_4_ERR */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_4_ERR_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_4_ERR,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_4_ERR,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_4_ERR =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_4_ERR,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level13_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_4_ERR_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_4_ERR,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_4_ERR_HwConfig,
  /* .IsrId                     = */ Lin_Channel_4_ERR,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_4_RXI */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_4_RXI_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_4_RXI,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_4_RXI,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_4_RXI =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_4_RXI,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level13_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_4_RXI_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_4_RXI,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_4_RXI_HwConfig,
  /* .IsrId                     = */ Lin_Channel_4_RXI,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_4_TXI */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_4_TXI_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_4_TXI,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_4_TXI,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_4_TXI =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_4_TXI,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level13_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_4_TXI_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_4_TXI,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_4_TXI_HwConfig,
  /* .IsrId                     = */ Lin_Channel_4_TXI,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_6_ERR */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_6_ERR_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_6_ERR,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_6_ERR,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_6_ERR =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_6_ERR,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level12_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_6_ERR_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_6_ERR,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_6_ERR_HwConfig,
  /* .IsrId                     = */ Lin_Channel_6_ERR,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_6_RXI */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_6_RXI_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_6_RXI,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_6_RXI,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_6_RXI =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_6_RXI,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level12_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_6_RXI_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_6_RXI,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_6_RXI_HwConfig,
  /* .IsrId                     = */ Lin_Channel_6_RXI,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_6_TXI */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_6_TXI_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_6_TXI,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_6_TXI,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_6_TXI =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_6_TXI,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level12_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_6_TXI_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_6_TXI,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_6_TXI_HwConfig,
  /* .IsrId                     = */ Lin_Channel_6_TXI,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_7_ERR */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_7_ERR_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_7_ERR,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_7_ERR,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_7_ERR =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_7_ERR,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level13_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_7_ERR_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_7_ERR,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_7_ERR_HwConfig,
  /* .IsrId                     = */ Lin_Channel_7_ERR,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_7_RXI */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_7_RXI_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_7_RXI,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_7_RXI,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_7_RXI =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_7_RXI,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level13_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_7_RXI_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_7_RXI,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_7_RXI_HwConfig,
  /* .IsrId                     = */ Lin_Channel_7_RXI,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_7_TXI */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_7_TXI_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_7_TXI,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_7_TXI,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_7_TXI =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_7_TXI,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level13_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_7_TXI_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_7_TXI,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_7_TXI_HwConfig,
  /* .IsrId                     = */ Lin_Channel_7_TXI,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_8_ERR */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_8_ERR_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_8_ERR,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_8_ERR,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_8_ERR =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_8_ERR,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level10_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_8_ERR_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_8_ERR,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_8_ERR_HwConfig,
  /* .IsrId                     = */ Lin_Channel_8_ERR,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_8_RXI */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_8_RXI_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_8_RXI,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_8_RXI,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_8_RXI =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_8_RXI,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level10_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_8_RXI_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_8_RXI,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_8_RXI_HwConfig,
  /* .IsrId                     = */ Lin_Channel_8_RXI,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_8_TXI */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_8_TXI_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_8_TXI,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_8_TXI,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_8_TXI =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_8_TXI,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level10_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_8_TXI_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_8_TXI,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_8_TXI_HwConfig,
  /* .IsrId                     = */ Lin_Channel_8_TXI,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_9_ERR */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_9_ERR_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_9_ERR,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_9_ERR,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_9_ERR =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_9_ERR,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level13_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_9_ERR_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_9_ERR,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_9_ERR_HwConfig,
  /* .IsrId                     = */ Lin_Channel_9_ERR,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_9_RXI */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_9_RXI_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_9_RXI,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_9_RXI,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_9_RXI =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_9_RXI,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level13_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_9_RXI_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_9_RXI,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_9_RXI_HwConfig,
  /* .IsrId                     = */ Lin_Channel_9_RXI,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: Lin_Channel_9_TXI */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_9_TXI_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_Lin_Channel_9_TXI,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_Lin_Channel_9_TXI,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_Lin_Channel_9_TXI =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_Lin_Channel_9_TXI,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level13_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_Lin_Channel_9_TXI_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_Lin_Channel_9_TXI,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_Lin_Channel_9_TXI_HwConfig,
  /* .IsrId                     = */ Lin_Channel_9_TXI,
  /* .IsEnabledOnInitialization = */ FALSE
}
;
/*! ISR configuration data: MCU_PLL_LossOfLock */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_MCU_PLL_LossOfLock_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_MCU_PLL_LossOfLock,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_MCU_PLL_LossOfLock,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_MCU_PLL_LossOfLock =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_MCU_PLL_LossOfLock,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level10_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_MCU_PLL_LossOfLock_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_MCU_PLL_LossOfLock,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_MCU_PLL_LossOfLock_HwConfig,
  /* .IsrId                     = */ MCU_PLL_LossOfLock,
  /* .IsEnabledOnInitialization = */ TRUE
}
;
/*! ISR configuration data: WKUP_IRQ0 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_WKUP_IRQ0_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_WKUP_IRQ0,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_WKUP_IRQ0,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_WKUP_IRQ0 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_WKUP_IRQ0,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level3_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_WKUP_IRQ0_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_WKUP_IRQ0,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_WKUP_IRQ0_HwConfig,
  /* .IsrId                     = */ WKUP_IRQ0,
  /* .IsEnabledOnInitialization = */ TRUE
}
;
/*! ISR configuration data: WKUP_IRQ1 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_WKUP_IRQ1_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_WKUP_IRQ1,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_WKUP_IRQ1,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_WKUP_IRQ1 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_WKUP_IRQ1,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level3_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_WKUP_IRQ1_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_WKUP_IRQ1,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_WKUP_IRQ1_HwConfig,
  /* .IsrId                     = */ WKUP_IRQ1,
  /* .IsEnabledOnInitialization = */ TRUE
}
;
/*! ISR configuration data: WKUP_IRQ2 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_WKUP_IRQ2_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_WKUP_IRQ2,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_WKUP_IRQ2,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_WKUP_IRQ2 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_WKUP_IRQ2,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level3_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_WKUP_IRQ2_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_Untrusted_Core0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_WKUP_IRQ2,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_WKUP_IRQ2_HwConfig,
  /* .IsrId                     = */ WKUP_IRQ2,
  /* .IsEnabledOnInitialization = */ TRUE
}
;
/*! ISR configuration data: WKUP_IRQ3 */
CONST(Os_IsrHwConfigType, OS_CONST) OsCfg_Isr_WKUP_IRQ3_HwConfig =
{
  /* .HwConfig                  = */ &OsCfg_Hal_IntIsr_WKUP_IRQ3,
  /* .MapConfig                 = */ &OsCfg_Hal_IntIsrMap_WKUP_IRQ3,
  /* .IsMapped                  = */ TRUE
}
;  
CONST(Os_IsrConfigType, OS_CONST) OsCfg_Isr_WKUP_IRQ3 =
{
  /* .Thread   = */
  {
    /* .ContextConfig         = */ &OsCfg_Hal_Context_WKUP_IRQ3,
    /* .Context               = */ &OsCfg_Hal_Context_OsCore0_Isr_Level1_Dyn,
    /* .Stack                 = */ &OsCfg_Stack_OsCore0_Isr_Core,
    /* .Dyn                   = */ OS_ISR_CASTDYN_ISR_2_THREAD(OsCfg_Isr_WKUP_IRQ3_Dyn),
    /* .OwnerApplication      = */ &OsCfg_App_SystemApplication_OsCore0,
    /* .Core                  = */ &OsCfg_Core_OsCore0,
    /* .IntApiState           = */ &OsCfg_Core_OsCore0_Dyn.IntApiState,
    /* .TimeProtConfig        = */ NULL_PTR,
    /* .MpAccessRightsInitial = */ NULL_PTR,
    /* .AccessRights          = */ &OsCfg_AccessCheck_NoAccess,
    /* .Trace                 = */ &OsCfg_Trace_WKUP_IRQ3,
    /* .FpuContext            = */ NULL_PTR,
    /* .InitialCallContext    = */ OS_CALLCONTEXT_ISR2,
    /* .PreThreadHook         = */ NULL_PTR,
    /* .InitDuringStartUp     = */ FALSE,
    /* .UsesFpu               = */ FALSE
  },
  /* .SourceConfig              = */ &OsCfg_Isr_WKUP_IRQ3_HwConfig,
  /* .IsrId                     = */ WKUP_IRQ3,
  /* .IsEnabledOnInitialization = */ TRUE
}
;
#define OS_STOP_SEC_CORE0_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#define OS_START_SEC_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Object reference table for category 2 ISRs. */
CONSTP2CONST(Os_IsrConfigType, OS_CONST, OS_CONST) OsCfg_IsrRefs[OS_ISRID_COUNT + 1] =  /* PRQA S 4521 */ /* MD_Os_Rule10.1_4521 */
{
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_0_MB00To03),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_0_MB04To07),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_0_MB08To11),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_0_MB12To15),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_0_MB16To31),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_0_MB32To63),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_0_MB64To95),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_1_MB00To03),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_1_MB04To07),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_1_MB08To11),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_1_MB12To15),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_1_MB16To31),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_1_MB32To63),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_1_MB64To95),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_2_MB00To03),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_2_MB04To07),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_2_MB08To11),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_2_MB12To15),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_2_MB16To31),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_2_MB32To63),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_2_MB64To95),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_4_MB00To03),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_4_MB04To07),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_4_MB08To11),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_4_MB12To15),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_4_MB16To31),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_4_MB32To63),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_4_MB64To95),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_6_MB00To03),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_6_MB04To07),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_6_MB08To11),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_6_MB12To15),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_6_MB16To31),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_6_MB32To63),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_6_MB64To95),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_7_MB00To03),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_7_MB04To07),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_7_MB08To11),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_7_MB12To15),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_7_MB16To31),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_7_MB32To63),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_CanIsr_7_MB64To95),
  OS_TIMER_CASTCONFIG_TIMERISR_2_ISR(OsCfg_Isr_CounterIsr_SystemTimer),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_DOWHS1_EMIOS0_CH3_ISR),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_DOWHS2_EMIOS0_CH5_ISR),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_DOWLS2_EMIOS0_CH13_ISR),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_DOWLS2_EMIOS0_CH9_ISR),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_DOWLS3_EMIOS0_CH14_ISR),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Gpt_PIT_0_TIMER_0_ISR),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Gpt_PIT_0_TIMER_1_ISR),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Gpt_PIT_0_TIMER_2_ISR),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_0_ERR),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_0_RXI),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_0_TXI),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_10_ERR),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_10_RXI),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_10_TXI),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_1_ERR),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_1_RXI),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_1_TXI),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_4_ERR),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_4_RXI),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_4_TXI),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_6_ERR),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_6_RXI),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_6_TXI),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_7_ERR),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_7_RXI),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_7_TXI),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_8_ERR),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_8_RXI),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_8_TXI),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_9_ERR),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_9_RXI),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_Lin_Channel_9_TXI),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_MCU_PLL_LossOfLock),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_WKUP_IRQ0),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_WKUP_IRQ1),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_WKUP_IRQ2),
  OS_ISR_CASTCONFIG_ISR_2_ISR(OsCfg_Isr_WKUP_IRQ3),
  NULL_PTR
};

#define OS_STOP_SEC_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  END OF FILE: Os_Isr_Lcfg.c
 *********************************************************************************************************************/
