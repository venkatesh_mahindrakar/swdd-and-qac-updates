/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Hal_Cfg.h
 *   Generation Time: 2020-10-30 14:38:53
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/


                                                                                                                        /* PRQA S 0388  EOF */ /* MD_MSR_Dir1.1 */

#ifndef OS_HAL_CFG_H
# define OS_HAL_CFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/*! HAL configuration major version identification. */
# define OS_CFG_HAL_MAJOR_VERSION                (2u)

/*! HAL configuration minor version identification. */
# define OS_CFG_HAL_MINOR_VERSION                (24u)

/* ISR core and level definitions */
# define OS_ISR_CANISR_0_MB00TO03_CORE      (0)
# define OS_ISR_CANISR_0_MB00TO03_LEVEL     (10)
# define OS_ISR_CANISR_0_MB04TO07_CORE      (0)
# define OS_ISR_CANISR_0_MB04TO07_LEVEL     (10)
# define OS_ISR_CANISR_0_MB08TO11_CORE      (0)
# define OS_ISR_CANISR_0_MB08TO11_LEVEL     (10)
# define OS_ISR_CANISR_0_MB12TO15_CORE      (0)
# define OS_ISR_CANISR_0_MB12TO15_LEVEL     (10)
# define OS_ISR_CANISR_0_MB16TO31_CORE      (0)
# define OS_ISR_CANISR_0_MB16TO31_LEVEL     (10)
# define OS_ISR_CANISR_0_MB32TO63_CORE      (0)
# define OS_ISR_CANISR_0_MB32TO63_LEVEL     (10)
# define OS_ISR_CANISR_0_MB64TO95_CORE      (0)
# define OS_ISR_CANISR_0_MB64TO95_LEVEL     (10)
# define OS_ISR_CANISR_1_MB00TO03_CORE      (0)
# define OS_ISR_CANISR_1_MB00TO03_LEVEL     (6)
# define OS_ISR_CANISR_1_MB04TO07_CORE      (0)
# define OS_ISR_CANISR_1_MB04TO07_LEVEL     (6)
# define OS_ISR_CANISR_1_MB08TO11_CORE      (0)
# define OS_ISR_CANISR_1_MB08TO11_LEVEL     (6)
# define OS_ISR_CANISR_1_MB12TO15_CORE      (0)
# define OS_ISR_CANISR_1_MB12TO15_LEVEL     (6)
# define OS_ISR_CANISR_1_MB16TO31_CORE      (0)
# define OS_ISR_CANISR_1_MB16TO31_LEVEL     (6)
# define OS_ISR_CANISR_1_MB32TO63_CORE      (0)
# define OS_ISR_CANISR_1_MB32TO63_LEVEL     (6)
# define OS_ISR_CANISR_1_MB64TO95_CORE      (0)
# define OS_ISR_CANISR_1_MB64TO95_LEVEL     (6)
# define OS_ISR_CANISR_2_MB00TO03_CORE      (0)
# define OS_ISR_CANISR_2_MB00TO03_LEVEL     (5)
# define OS_ISR_CANISR_2_MB04TO07_CORE      (0)
# define OS_ISR_CANISR_2_MB04TO07_LEVEL     (5)
# define OS_ISR_CANISR_2_MB08TO11_CORE      (0)
# define OS_ISR_CANISR_2_MB08TO11_LEVEL     (5)
# define OS_ISR_CANISR_2_MB12TO15_CORE      (0)
# define OS_ISR_CANISR_2_MB12TO15_LEVEL     (5)
# define OS_ISR_CANISR_2_MB16TO31_CORE      (0)
# define OS_ISR_CANISR_2_MB16TO31_LEVEL     (5)
# define OS_ISR_CANISR_2_MB32TO63_CORE      (0)
# define OS_ISR_CANISR_2_MB32TO63_LEVEL     (5)
# define OS_ISR_CANISR_2_MB64TO95_CORE      (0)
# define OS_ISR_CANISR_2_MB64TO95_LEVEL     (5)
# define OS_ISR_CANISR_4_MB00TO03_CORE      (0)
# define OS_ISR_CANISR_4_MB00TO03_LEVEL     (8)
# define OS_ISR_CANISR_4_MB04TO07_CORE      (0)
# define OS_ISR_CANISR_4_MB04TO07_LEVEL     (8)
# define OS_ISR_CANISR_4_MB08TO11_CORE      (0)
# define OS_ISR_CANISR_4_MB08TO11_LEVEL     (8)
# define OS_ISR_CANISR_4_MB12TO15_CORE      (0)
# define OS_ISR_CANISR_4_MB12TO15_LEVEL     (8)
# define OS_ISR_CANISR_4_MB16TO31_CORE      (0)
# define OS_ISR_CANISR_4_MB16TO31_LEVEL     (8)
# define OS_ISR_CANISR_4_MB32TO63_CORE      (0)
# define OS_ISR_CANISR_4_MB32TO63_LEVEL     (8)
# define OS_ISR_CANISR_4_MB64TO95_CORE      (0)
# define OS_ISR_CANISR_4_MB64TO95_LEVEL     (8)
# define OS_ISR_CANISR_6_MB00TO03_CORE      (0)
# define OS_ISR_CANISR_6_MB00TO03_LEVEL     (9)
# define OS_ISR_CANISR_6_MB04TO07_CORE      (0)
# define OS_ISR_CANISR_6_MB04TO07_LEVEL     (9)
# define OS_ISR_CANISR_6_MB08TO11_CORE      (0)
# define OS_ISR_CANISR_6_MB08TO11_LEVEL     (9)
# define OS_ISR_CANISR_6_MB12TO15_CORE      (0)
# define OS_ISR_CANISR_6_MB12TO15_LEVEL     (9)
# define OS_ISR_CANISR_6_MB16TO31_CORE      (0)
# define OS_ISR_CANISR_6_MB16TO31_LEVEL     (9)
# define OS_ISR_CANISR_6_MB32TO63_CORE      (0)
# define OS_ISR_CANISR_6_MB32TO63_LEVEL     (9)
# define OS_ISR_CANISR_6_MB64TO95_CORE      (0)
# define OS_ISR_CANISR_6_MB64TO95_LEVEL     (9)
# define OS_ISR_CANISR_7_MB00TO03_CORE      (0)
# define OS_ISR_CANISR_7_MB00TO03_LEVEL     (7)
# define OS_ISR_CANISR_7_MB04TO07_CORE      (0)
# define OS_ISR_CANISR_7_MB04TO07_LEVEL     (7)
# define OS_ISR_CANISR_7_MB08TO11_CORE      (0)
# define OS_ISR_CANISR_7_MB08TO11_LEVEL     (7)
# define OS_ISR_CANISR_7_MB12TO15_CORE      (0)
# define OS_ISR_CANISR_7_MB12TO15_LEVEL     (7)
# define OS_ISR_CANISR_7_MB16TO31_CORE      (0)
# define OS_ISR_CANISR_7_MB16TO31_LEVEL     (7)
# define OS_ISR_CANISR_7_MB32TO63_CORE      (0)
# define OS_ISR_CANISR_7_MB32TO63_LEVEL     (7)
# define OS_ISR_CANISR_7_MB64TO95_CORE      (0)
# define OS_ISR_CANISR_7_MB64TO95_LEVEL     (7)
# define OS_ISR_COUNTERISR_SYSTEMTIMER_CORE      (0)
# define OS_ISR_COUNTERISR_SYSTEMTIMER_LEVEL     (15)
# define OS_ISR_DOWHS1_EMIOS0_CH3_ISR_CORE      (0)
# define OS_ISR_DOWHS1_EMIOS0_CH3_ISR_LEVEL     (2)
# define OS_ISR_DOWHS2_EMIOS0_CH5_ISR_CORE      (0)
# define OS_ISR_DOWHS2_EMIOS0_CH5_ISR_LEVEL     (3)
# define OS_ISR_DOWLS2_EMIOS0_CH13_ISR_CORE      (0)
# define OS_ISR_DOWLS2_EMIOS0_CH13_ISR_LEVEL     (6)
# define OS_ISR_DOWLS2_EMIOS0_CH9_ISR_CORE      (0)
# define OS_ISR_DOWLS2_EMIOS0_CH9_ISR_LEVEL     (5)
# define OS_ISR_DOWLS3_EMIOS0_CH14_ISR_CORE      (0)
# define OS_ISR_DOWLS3_EMIOS0_CH14_ISR_LEVEL     (4)
# define OS_ISR_GPT_PIT_0_TIMER_0_ISR_CORE      (0)
# define OS_ISR_GPT_PIT_0_TIMER_0_ISR_LEVEL     (1)
# define OS_ISR_GPT_PIT_0_TIMER_1_ISR_CORE      (0)
# define OS_ISR_GPT_PIT_0_TIMER_1_ISR_LEVEL     (3)
# define OS_ISR_GPT_PIT_0_TIMER_2_ISR_CORE      (0)
# define OS_ISR_GPT_PIT_0_TIMER_2_ISR_LEVEL     (4)
# define OS_ISR_IVOR1HANDLER_CORE      (0)
# define OS_ISR_IVOR1HANDLER_LEVEL     (1)
# define OS_ISR_LIN_CHANNEL_0_ERR_CORE      (0)
# define OS_ISR_LIN_CHANNEL_0_ERR_LEVEL     (13)
# define OS_ISR_LIN_CHANNEL_0_RXI_CORE      (0)
# define OS_ISR_LIN_CHANNEL_0_RXI_LEVEL     (13)
# define OS_ISR_LIN_CHANNEL_0_TXI_CORE      (0)
# define OS_ISR_LIN_CHANNEL_0_TXI_LEVEL     (13)
# define OS_ISR_LIN_CHANNEL_10_ERR_CORE      (0)
# define OS_ISR_LIN_CHANNEL_10_ERR_LEVEL     (14)
# define OS_ISR_LIN_CHANNEL_10_RXI_CORE      (0)
# define OS_ISR_LIN_CHANNEL_10_RXI_LEVEL     (14)
# define OS_ISR_LIN_CHANNEL_10_TXI_CORE      (0)
# define OS_ISR_LIN_CHANNEL_10_TXI_LEVEL     (14)
# define OS_ISR_LIN_CHANNEL_1_ERR_CORE      (0)
# define OS_ISR_LIN_CHANNEL_1_ERR_LEVEL     (11)
# define OS_ISR_LIN_CHANNEL_1_RXI_CORE      (0)
# define OS_ISR_LIN_CHANNEL_1_RXI_LEVEL     (11)
# define OS_ISR_LIN_CHANNEL_1_TXI_CORE      (0)
# define OS_ISR_LIN_CHANNEL_1_TXI_LEVEL     (11)
# define OS_ISR_LIN_CHANNEL_4_ERR_CORE      (0)
# define OS_ISR_LIN_CHANNEL_4_ERR_LEVEL     (13)
# define OS_ISR_LIN_CHANNEL_4_RXI_CORE      (0)
# define OS_ISR_LIN_CHANNEL_4_RXI_LEVEL     (13)
# define OS_ISR_LIN_CHANNEL_4_TXI_CORE      (0)
# define OS_ISR_LIN_CHANNEL_4_TXI_LEVEL     (13)
# define OS_ISR_LIN_CHANNEL_6_ERR_CORE      (0)
# define OS_ISR_LIN_CHANNEL_6_ERR_LEVEL     (12)
# define OS_ISR_LIN_CHANNEL_6_RXI_CORE      (0)
# define OS_ISR_LIN_CHANNEL_6_RXI_LEVEL     (12)
# define OS_ISR_LIN_CHANNEL_6_TXI_CORE      (0)
# define OS_ISR_LIN_CHANNEL_6_TXI_LEVEL     (12)
# define OS_ISR_LIN_CHANNEL_7_ERR_CORE      (0)
# define OS_ISR_LIN_CHANNEL_7_ERR_LEVEL     (13)
# define OS_ISR_LIN_CHANNEL_7_RXI_CORE      (0)
# define OS_ISR_LIN_CHANNEL_7_RXI_LEVEL     (13)
# define OS_ISR_LIN_CHANNEL_7_TXI_CORE      (0)
# define OS_ISR_LIN_CHANNEL_7_TXI_LEVEL     (13)
# define OS_ISR_LIN_CHANNEL_8_ERR_CORE      (0)
# define OS_ISR_LIN_CHANNEL_8_ERR_LEVEL     (10)
# define OS_ISR_LIN_CHANNEL_8_RXI_CORE      (0)
# define OS_ISR_LIN_CHANNEL_8_RXI_LEVEL     (10)
# define OS_ISR_LIN_CHANNEL_8_TXI_CORE      (0)
# define OS_ISR_LIN_CHANNEL_8_TXI_LEVEL     (10)
# define OS_ISR_LIN_CHANNEL_9_ERR_CORE      (0)
# define OS_ISR_LIN_CHANNEL_9_ERR_LEVEL     (13)
# define OS_ISR_LIN_CHANNEL_9_RXI_CORE      (0)
# define OS_ISR_LIN_CHANNEL_9_RXI_LEVEL     (13)
# define OS_ISR_LIN_CHANNEL_9_TXI_CORE      (0)
# define OS_ISR_LIN_CHANNEL_9_TXI_LEVEL     (13)
# define OS_ISR_MCU_PLL_LOSSOFLOCK_CORE      (0)
# define OS_ISR_MCU_PLL_LOSSOFLOCK_LEVEL     (10)
# define OS_ISR_WKUP_IRQ0_CORE      (0)
# define OS_ISR_WKUP_IRQ0_LEVEL     (3)
# define OS_ISR_WKUP_IRQ1_CORE      (0)
# define OS_ISR_WKUP_IRQ1_LEVEL     (3)
# define OS_ISR_WKUP_IRQ2_CORE      (0)
# define OS_ISR_WKUP_IRQ2_LEVEL     (3)
# define OS_ISR_WKUP_IRQ3_CORE      (0)
# define OS_ISR_WKUP_IRQ3_LEVEL     (1)

/* Hardware counter timing macros */

/* Counter timing macros and constants: SystemTimer */
# define OSMAXALLOWEDVALUE_SystemTimer     (1073741823uL) /* 0x3FFFFFFFuL */
# define OSMINCYCLE_SystemTimer            (1uL)
# define OSTICKSPERBASE_SystemTimer        (80000uL)
# define OSTICKDURATION_SystemTimer        (1000000uL)

/* OSEK compatibility for the system timer. */
# define OSMAXALLOWEDVALUE     (OSMAXALLOWEDVALUE_SystemTimer)
# define OSMINCYCLE            (OSMINCYCLE_SystemTimer)
# define OSTICKSPERBASE        (OSTICKSPERBASE_SystemTimer)
# define OSTICKDURATION        (OSTICKDURATION_SystemTimer)

/*! Macro OS_NS2TICKS_SystemTimer was approximated with a deviation of 0.0ppm. */
# define OS_NS2TICKS_SystemTimer(x)     ( (TickType) (((((uint32)(x)) * 1) + 500000) / 1000000) ) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
/*! Macro OS_TICKS2NS_SystemTimer was approximated with a deviation of 0.0ppm. */
# define OS_TICKS2NS_SystemTimer(x)     ( (PhysicalTimeType) (((((uint32)(x)) * 1000000) + 0) / 1) ) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/*! Macro OS_US2TICKS_SystemTimer was approximated with a deviation of 0.0ppm. */
# define OS_US2TICKS_SystemTimer(x)     ( (TickType) (((((uint32)(x)) * 1) + 500) / 1000) ) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
/*! Macro OS_TICKS2US_SystemTimer was approximated with a deviation of 1.1102230246251565E-10ppm. */
# define OS_TICKS2US_SystemTimer(x)     ( (PhysicalTimeType) (((((uint32)(x)) * 1000) + 0) / 1) ) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/*! Macro OS_MS2TICKS_SystemTimer was approximated with a deviation of 0.0ppm. */
# define OS_MS2TICKS_SystemTimer(x)     ( (TickType) (((((uint32)(x)) * 1) + 0) / 1) ) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
/*! Macro OS_TICKS2MS_SystemTimer was approximated with a deviation of 0.0ppm. */
# define OS_TICKS2MS_SystemTimer(x)     ( (PhysicalTimeType) (((((uint32)(x)) * 1) + 0) / 1) ) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/*! Macro OS_SEC2TICKS_SystemTimer was approximated with a deviation of 0.0ppm. */
# define OS_SEC2TICKS_SystemTimer(x)     ( (TickType) (((((uint32)(x)) * 1000) + 0) / 1) ) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
/*! Macro OS_TICKS2SEC_SystemTimer was approximated with a deviation of 0.0ppm. */
# define OS_TICKS2SEC_SystemTimer(x)     ( (PhysicalTimeType) (((((uint32)(x)) * 1) + 500) / 1000) ) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */







/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/


#endif /* OS_HAL_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_Hal_Cfg.h
 *********************************************************************************************************************/
