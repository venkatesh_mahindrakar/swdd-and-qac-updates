/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: EcuM
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: EcuM_Generated_Types.h
 *   Generation Time: 2020-08-20 13:43:06
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/

#if !defined(ECUM_GENERATEDTYPES_H)
#define ECUM_GENERATEDTYPES_H

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/

# include "Rte_EcuM_Type.h"
# include "Std_Types.h"

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/
/*! This type is a 32bit bitmask where each bit represents a wakeup source. */
 typedef uint32 EcuM_WakeupSourceType;
 
 

/* ------------------------------------- Symblic Name Defines for EcuM_WakeupSourceType ---------------------------- */
#define EcuMConf_EcuMWakeupSource_ECUM_WKSOURCE_POWER          (0uL) 
#define EcuMConf_EcuMWakeupSource_ECUM_WKSOURCE_RESET          (1uL) 
#define EcuMConf_EcuMWakeupSource_ECUM_WKSOURCE_INTERNAL_RESET (2uL) 
#define EcuMConf_EcuMWakeupSource_ECUM_WKSOURCE_INTERNAL_WDG   (3uL) 
#define EcuMConf_EcuMWakeupSource_ECUM_WKSOURCE_EXTERNAL_WDG   (4uL) 
#define EcuMConf_EcuMWakeupSource_CN_Backbone2_78967e2c        (5uL) 
#define EcuMConf_EcuMWakeupSource_CN_CAN6_b040c073             (6uL) 
#define EcuMConf_EcuMWakeupSource_CN_CabSubnet_9ea693f1        (7uL) 
#define EcuMConf_EcuMWakeupSource_CN_SecuritySubnet_e7a0ee54   (8uL) 
#define EcuMConf_EcuMWakeupSource_CN_Backbone1J1939_0b1f4bae   (9uL) 
#define EcuMConf_EcuMWakeupSource_CN_FMSNet_fce1aae5           (10uL) 
#define EcuMConf_EcuMWakeupSource_CN_LIN00_2cd9a7df            (11uL) 
#define EcuMConf_EcuMWakeupSource_CN_LIN01_5bde9749            (12uL) 
#define EcuMConf_EcuMWakeupSource_CN_LIN02_c2d7c6f3            (13uL) 
#define EcuMConf_EcuMWakeupSource_CN_LIN03_b5d0f665            (14uL) 
#define EcuMConf_EcuMWakeupSource_CN_LIN04_2bb463c6            (15uL) 
#define EcuMConf_EcuMWakeupSource_ECUM_WKSOURCE_LFIC           (16uL) 
#define EcuMConf_EcuMWakeupSource_CN_LIN05_5cb35350            (17uL) 
#define EcuMConf_EcuMWakeupSource_ECUM_WKSOURCE_RFIC           (18uL) 
#define EcuMConf_EcuMWakeupSource_CN_LIN06_c5ba02ea            (19uL) 
#define EcuMConf_EcuMWakeupSource_ECUM_WKSOURCE_RTC            (20uL) 
#define EcuMConf_EcuMWakeupSource_CN_LIN07_b2bd327c            (21uL) 

 
/* ------------------------------------- Range of EcuM_WakeupSourceType -------------------------------------------- */
#define ECUM_WKSOURCE_NONE                       (EcuM_WakeupSourceType)(0x00000000uL) 
#define ECUM_WKSOURCE_ALL_SOURCES                (EcuM_WakeupSourceType)(~((EcuM_WakeupSourceType)0x00UL)) 
#define ECUM_WKSOURCE_POWER                      (EcuM_WakeupSourceType)(1uL) 
#define ECUM_WKSOURCE_RESET                      (EcuM_WakeupSourceType)(2uL) 
#define ECUM_WKSOURCE_INTERNAL_RESET             (EcuM_WakeupSourceType)(4uL) 
#define ECUM_WKSOURCE_INTERNAL_WDG               (EcuM_WakeupSourceType)(8uL) 
#define ECUM_WKSOURCE_EXTERNAL_WDG               (EcuM_WakeupSourceType)(16uL) 
#define ECUM_WKSOURCE_CN_Backbone2_78967e2c      (EcuM_WakeupSourceType)(32uL) 
#define ECUM_WKSOURCE_CN_CAN6_b040c073           (EcuM_WakeupSourceType)(64uL) 
#define ECUM_WKSOURCE_CN_CabSubnet_9ea693f1      (EcuM_WakeupSourceType)(128uL) 
#define ECUM_WKSOURCE_CN_SecuritySubnet_e7a0ee54 (EcuM_WakeupSourceType)(256uL) 
#define ECUM_WKSOURCE_CN_Backbone1J1939_0b1f4bae (EcuM_WakeupSourceType)(512uL) 
#define ECUM_WKSOURCE_CN_FMSNet_fce1aae5         (EcuM_WakeupSourceType)(1024uL) 
#define ECUM_WKSOURCE_CN_LIN00_2cd9a7df          (EcuM_WakeupSourceType)(2048uL) 
#define ECUM_WKSOURCE_CN_LIN01_5bde9749          (EcuM_WakeupSourceType)(4096uL) 
#define ECUM_WKSOURCE_CN_LIN02_c2d7c6f3          (EcuM_WakeupSourceType)(8192uL) 
#define ECUM_WKSOURCE_CN_LIN03_b5d0f665          (EcuM_WakeupSourceType)(16384uL) 
#define ECUM_WKSOURCE_CN_LIN04_2bb463c6          (EcuM_WakeupSourceType)(32768uL) 
#define ECUM_WKSOURCE_LFIC                       (EcuM_WakeupSourceType)(65536uL) 
#define ECUM_WKSOURCE_CN_LIN05_5cb35350          (EcuM_WakeupSourceType)(131072uL) 
#define ECUM_WKSOURCE_RFIC                       (EcuM_WakeupSourceType)(262144uL) 
#define ECUM_WKSOURCE_CN_LIN06_c5ba02ea          (EcuM_WakeupSourceType)(524288uL) 
#define ECUM_WKSOURCE_RTC                        (EcuM_WakeupSourceType)(1048576uL) 
#define ECUM_WKSOURCE_CN_LIN07_b2bd327c          (EcuM_WakeupSourceType)(2097152uL) 



  /* ------------------------------------- Range of EcuM_StateType ------------------------------------------------- */
#define ECUM_SUBSTATE_MASK                                  (0x0Fu)
#define ECUM_STATE_STARTUP                                  (0x10u)
#define ECUM_STATE_STARTUP_ONE                              (0x11u)
#define ECUM_STATE_STARTUP_TWO                              (0x12u)
#define ECUM_STATE_WAKEUP                                   (0x20u)
#define ECUM_STATE_WAKEUP_ONE                               (0x21u)
#define ECUM_STATE_WAKEUP_VALIDATION                        (0x22u)
#define ECUM_STATE_WAKEUP_REACTION                          (0x23u)
#define ECUM_STATE_WAKEUP_TWO                               (0x24u)
#define ECUM_STATE_WAKEUP_WAKESLEEP                         (0x25u)
#define ECUM_STATE_WAKEUP_TTII                              (0x26u)
#define ECUM_STATE_RUN                                      (0x30u)
#define ECUM_STATE_APP_RUN                                  (0x32u)
#define ECUM_STATE_APP_POST_RUN                             (0x33u)
#define ECUM_STATE_SHUTDOWN                                 (0x40u)
#define ECUM_STATE_PREP_SHUTDOWN                            (0x44u)
#define ECUM_STATE_GO_SLEEP                                 (0x49u)
#define ECUM_STATE_GO_OFF_ONE                               (0x4Du)
#define ECUM_STATE_GO_OFF_TWO                               (0x4Eu)
/* State OFF, RESET AND SLEEP are defined by the RTE */
#define ECUM_STATE_ERROR                                    (0xFFu)


typedef uint8 EcuM_WakeupStateType;
/* ------------------------------------- Range of EcuM_WakeupStatusType -------------------------------------------- */
#define ECUM_WKSTATUS_NONE                                  (0u)
#define ECUM_WKSTATUS_PENDING                               (1u)
#define ECUM_WKSTATUS_VALIDATED                             (2u)
#define ECUM_WKSTATUS_EXPIRED                               (3u)

/* The following state was introduced by Vector to support asynchronous transceiver handling */
#define ECUM_WKSTATUS_CHECKWAKEUP                           (5u)
#define ECUM_WKSTATUS_ENABLED                               (6u)

/* ------------------------------------- Range of Alarm Clocks ----------------------------------------------------- */

/* ------------------------------------- Range of EcuM RunStatus Types --------------------------------------------- */
#define ECUM_RUNSTATUS_UNKNOWN                              (0u)
#define ECUM_RUNSTATUS_REQUESTED                            (1u)
#define ECUM_RUNSTATUS_RELEASED                             (2u)


typedef uint8 EcuM_RunStatusType;

typedef EcuM_ModeType EcuM_ResetType;

#endif /* ECUM_GENERATEDTYPES_H */
/**********************************************************************************************************************
 *  END OF FILE: ECUM_GENERATEDTYPES.H
 *********************************************************************************************************************/


