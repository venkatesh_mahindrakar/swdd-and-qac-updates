/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Trace_Lcfg.h
 *   Generation Time: 2020-10-30 14:38:57
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

#ifndef OS_TRACE_LCFG_H
# define OS_TRACE_LCFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
# include "Std_Types.h"

/* Os module declarations */
# include "Os_Trace_Types.h"

/* Os kernel module dependencies */

/* Os hal dependencies */

/* User file includes */


/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

# define OS_START_SEC_PUBLIC_CORE0_VAR_NOINIT_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Dynamic trace data: OsCore0 */
extern VAR(Os_TraceCoreType, OS_VAR_NOINIT) OsCfg_Trace_OsCore0_Dyn;

# define OS_STOP_SEC_PUBLIC_CORE0_VAR_NOINIT_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA PROTOTYPES
 *********************************************************************************************************************/

# define OS_START_SEC_CONST_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Trace configuration data: ASW_10ms_Task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_ASW_10ms_Task;

/*! Trace configuration data: ASW_20ms_Task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_ASW_20ms_Task;

/*! Trace configuration data: ASW_Async_Task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_ASW_Async_Task;

/*! Trace configuration data: ASW_Init_Task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_ASW_Init_Task;

/*! Trace configuration data: BSW_10ms_Task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_BSW_10ms_Task;

/*! Trace configuration data: BSW_5ms_Task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_BSW_5ms_Task;

/*! Trace configuration data: BSW_Async_Task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_BSW_Async_Task;

/*! Trace configuration data: BSW_Diag_Task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_BSW_Diag_Task;

/*! Trace configuration data: BSW_Lin_Task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_BSW_Lin_Task;

/*! Trace configuration data: CpuLoadIdleTask */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CpuLoadIdleTask;

/*! Trace configuration data: IdleTask_OsCore0 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_IdleTask_OsCore0;

/*! Trace configuration data: Init_Task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Init_Task;

/*! Trace configuration data: CanIsr_0_MB00To03 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_0_MB00To03;

/*! Trace configuration data: CanIsr_0_MB04To07 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_0_MB04To07;

/*! Trace configuration data: CanIsr_0_MB08To11 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_0_MB08To11;

/*! Trace configuration data: CanIsr_0_MB12To15 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_0_MB12To15;

/*! Trace configuration data: CanIsr_0_MB16To31 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_0_MB16To31;

/*! Trace configuration data: CanIsr_0_MB32To63 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_0_MB32To63;

/*! Trace configuration data: CanIsr_0_MB64To95 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_0_MB64To95;

/*! Trace configuration data: CanIsr_1_MB00To03 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_1_MB00To03;

/*! Trace configuration data: CanIsr_1_MB04To07 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_1_MB04To07;

/*! Trace configuration data: CanIsr_1_MB08To11 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_1_MB08To11;

/*! Trace configuration data: CanIsr_1_MB12To15 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_1_MB12To15;

/*! Trace configuration data: CanIsr_1_MB16To31 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_1_MB16To31;

/*! Trace configuration data: CanIsr_1_MB32To63 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_1_MB32To63;

/*! Trace configuration data: CanIsr_1_MB64To95 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_1_MB64To95;

/*! Trace configuration data: CanIsr_2_MB00To03 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_2_MB00To03;

/*! Trace configuration data: CanIsr_2_MB04To07 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_2_MB04To07;

/*! Trace configuration data: CanIsr_2_MB08To11 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_2_MB08To11;

/*! Trace configuration data: CanIsr_2_MB12To15 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_2_MB12To15;

/*! Trace configuration data: CanIsr_2_MB16To31 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_2_MB16To31;

/*! Trace configuration data: CanIsr_2_MB32To63 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_2_MB32To63;

/*! Trace configuration data: CanIsr_2_MB64To95 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_2_MB64To95;

/*! Trace configuration data: CanIsr_4_MB00To03 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_4_MB00To03;

/*! Trace configuration data: CanIsr_4_MB04To07 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_4_MB04To07;

/*! Trace configuration data: CanIsr_4_MB08To11 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_4_MB08To11;

/*! Trace configuration data: CanIsr_4_MB12To15 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_4_MB12To15;

/*! Trace configuration data: CanIsr_4_MB16To31 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_4_MB16To31;

/*! Trace configuration data: CanIsr_4_MB32To63 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_4_MB32To63;

/*! Trace configuration data: CanIsr_4_MB64To95 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_4_MB64To95;

/*! Trace configuration data: CanIsr_6_MB00To03 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_6_MB00To03;

/*! Trace configuration data: CanIsr_6_MB04To07 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_6_MB04To07;

/*! Trace configuration data: CanIsr_6_MB08To11 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_6_MB08To11;

/*! Trace configuration data: CanIsr_6_MB12To15 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_6_MB12To15;

/*! Trace configuration data: CanIsr_6_MB16To31 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_6_MB16To31;

/*! Trace configuration data: CanIsr_6_MB32To63 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_6_MB32To63;

/*! Trace configuration data: CanIsr_6_MB64To95 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_6_MB64To95;

/*! Trace configuration data: CanIsr_7_MB00To03 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_7_MB00To03;

/*! Trace configuration data: CanIsr_7_MB04To07 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_7_MB04To07;

/*! Trace configuration data: CanIsr_7_MB08To11 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_7_MB08To11;

/*! Trace configuration data: CanIsr_7_MB12To15 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_7_MB12To15;

/*! Trace configuration data: CanIsr_7_MB16To31 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_7_MB16To31;

/*! Trace configuration data: CanIsr_7_MB32To63 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_7_MB32To63;

/*! Trace configuration data: CanIsr_7_MB64To95 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_7_MB64To95;

/*! Trace configuration data: CounterIsr_SystemTimer */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CounterIsr_SystemTimer;

/*! Trace configuration data: DOWHS1_EMIOS0_CH3_ISR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_DOWHS1_EMIOS0_CH3_ISR;

/*! Trace configuration data: DOWHS2_EMIOS0_CH5_ISR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_DOWHS2_EMIOS0_CH5_ISR;

/*! Trace configuration data: DOWLS2_EMIOS0_CH13_ISR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_DOWLS2_EMIOS0_CH13_ISR;

/*! Trace configuration data: DOWLS2_EMIOS0_CH9_ISR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_DOWLS2_EMIOS0_CH9_ISR;

/*! Trace configuration data: DOWLS3_EMIOS0_CH14_ISR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_DOWLS3_EMIOS0_CH14_ISR;

/*! Trace configuration data: Gpt_PIT_0_TIMER_0_ISR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Gpt_PIT_0_TIMER_0_ISR;

/*! Trace configuration data: Gpt_PIT_0_TIMER_1_ISR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Gpt_PIT_0_TIMER_1_ISR;

/*! Trace configuration data: Gpt_PIT_0_TIMER_2_ISR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Gpt_PIT_0_TIMER_2_ISR;

/*! Trace configuration data: Lin_Channel_0_ERR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_0_ERR;

/*! Trace configuration data: Lin_Channel_0_RXI */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_0_RXI;

/*! Trace configuration data: Lin_Channel_0_TXI */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_0_TXI;

/*! Trace configuration data: Lin_Channel_10_ERR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_10_ERR;

/*! Trace configuration data: Lin_Channel_10_RXI */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_10_RXI;

/*! Trace configuration data: Lin_Channel_10_TXI */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_10_TXI;

/*! Trace configuration data: Lin_Channel_1_ERR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_1_ERR;

/*! Trace configuration data: Lin_Channel_1_RXI */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_1_RXI;

/*! Trace configuration data: Lin_Channel_1_TXI */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_1_TXI;

/*! Trace configuration data: Lin_Channel_4_ERR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_4_ERR;

/*! Trace configuration data: Lin_Channel_4_RXI */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_4_RXI;

/*! Trace configuration data: Lin_Channel_4_TXI */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_4_TXI;

/*! Trace configuration data: Lin_Channel_6_ERR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_6_ERR;

/*! Trace configuration data: Lin_Channel_6_RXI */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_6_RXI;

/*! Trace configuration data: Lin_Channel_6_TXI */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_6_TXI;

/*! Trace configuration data: Lin_Channel_7_ERR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_7_ERR;

/*! Trace configuration data: Lin_Channel_7_RXI */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_7_RXI;

/*! Trace configuration data: Lin_Channel_7_TXI */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_7_TXI;

/*! Trace configuration data: Lin_Channel_8_ERR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_8_ERR;

/*! Trace configuration data: Lin_Channel_8_RXI */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_8_RXI;

/*! Trace configuration data: Lin_Channel_8_TXI */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_8_TXI;

/*! Trace configuration data: Lin_Channel_9_ERR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_9_ERR;

/*! Trace configuration data: Lin_Channel_9_RXI */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_9_RXI;

/*! Trace configuration data: Lin_Channel_9_TXI */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_9_TXI;

/*! Trace configuration data: MCU_PLL_LossOfLock */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_MCU_PLL_LossOfLock;

/*! Trace configuration data: WKUP_IRQ0 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_WKUP_IRQ0;

/*! Trace configuration data: WKUP_IRQ1 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_WKUP_IRQ1;

/*! Trace configuration data: WKUP_IRQ2 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_WKUP_IRQ2;

/*! Trace configuration data: WKUP_IRQ3 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_WKUP_IRQ3;

# define OS_STOP_SEC_CONST_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/


#endif /* OS_TRACE_LCFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_Trace_Lcfg.h
 *********************************************************************************************************************/
