/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: LinTp
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: LinTp_Cfg.h
 *   Generation Time: 2020-08-20 13:43:05
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/


#if !defined(LINTP_CFG_H)
#define LINTP_CFG_H

/* Tp_Asr4TpLin CFG5 Generatorversion 6.00.01 */

/**********************************************************************************************************************
 *  Includes
 *********************************************************************************************************************/
#include "ComStack_Types.h"
#include "Lin_GeneralTypes.h"


/**********************************************************************************************************************
 *  Defines
 *********************************************************************************************************************/
#define LINTP_VERSION_INFO_API                          STD_OFF
#define LINTP_CANCEL_RECEIVE_SUPPORTED                  STD_OFF
#define LINTP_CANCEL_TRANSMIT_SUPPORTED                 STD_OFF
#define LINTP_CHANGE_PARAMETER_SUPPORTED                STD_OFF
#define LINTP_FUNCTIONAL_REQUEST_SUPPORTED              STD_ON
#define LINTP_FORWARD_RESPONSEPENDING_TO_PDUR           STD_ON
#define LINTP_RUNTIME_MEASUREMENT_SUPPORT               STD_ON
#define LINTP_BROADCAST_REQUEST_HANDLING                STD_OFF

/* above defines are required inside LinTp_Types.h */
#include "LinTp_Types.h"

/**********************************************************************************************************************
 *  General Defines
 *********************************************************************************************************************/
#ifndef LINTP_USE_DUMMY_STATEMENT
#define LINTP_USE_DUMMY_STATEMENT STD_OFF /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef LINTP_DUMMY_STATEMENT
#define LINTP_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef LINTP_DUMMY_STATEMENT_CONST
#define LINTP_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef LINTP_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define LINTP_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef LINTP_ATOMIC_VARIABLE_ACCESS
#define LINTP_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef LINTP_PROCESSOR_MPC5746C
#define LINTP_PROCESSOR_MPC5746C
#endif
#ifndef LINTP_COMP_DIAB
#define LINTP_COMP_DIAB
#endif
#ifndef LINTP_GEN_GENERATOR_MSR
#define LINTP_GEN_GENERATOR_MSR
#endif
#ifndef LINTP_CPUTYPE_BITORDER_MSB2LSB
#define LINTP_CPUTYPE_BITORDER_MSB2LSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef LINTP_CONFIGURATION_VARIANT_PRECOMPILE
#define LINTP_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef LINTP_CONFIGURATION_VARIANT_LINKTIME
#define LINTP_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef LINTP_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define LINTP_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef LINTP_CONFIGURATION_VARIANT
#define LINTP_CONFIGURATION_VARIANT LINTP_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef LINTP_POSTBUILD_VARIANT_SUPPORT
#define LINTP_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif


/**********************************************************************************************************************
 *  Symbolic Name Values for TxNSdus
 *********************************************************************************************************************/



/**
 * \defgroup LinTpHandleIds Handle IDs.
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define LinTpConf_LinTpTxNSdu_MasterReq_CCFW_oLIN03_98357989_Tx       0u
#define LinTpConf_LinTpTxNSdu_MasterReq_DLFW_oLIN03_40053787_Tx       1u
#define LinTpConf_LinTpTxNSdu_MasterReq_ELCP1_oLIN03_cbecb6de_Tx      2u
#define LinTpConf_LinTpTxNSdu_MasterReq_ELCP2_oLIN03_4563b13d_Tx      3u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_4A_oLIN00_cacc7d77_Tx    4u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_4B_oLIN00_44437a94_Tx    5u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_4C_oLIN00_88e97a0a_Tx    6u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_4D_oLIN00_822c7313_Tx    7u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_4E_oLIN00_4e86738d_Tx    8u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_4F_oLIN00_c009746e_Tx    9u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_5A_oLIN00_ddb76934_Tx    10u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_5B_oLIN00_53386ed7_Tx    11u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_5C_oLIN00_9f926e49_Tx    12u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_5D_oLIN00_95576750_Tx    13u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_5E_oLIN00_59fd67ce_Tx    14u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_5F_oLIN00_d772602d_Tx    15u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_40_oLIN00_aa36ec79_Tx    16u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_41_oLIN00_669cece7_Tx    17u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_42_oLIN00_e813eb04_Tx    18u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_43_oLIN00_24b9eb9a_Tx    19u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_44_oLIN00_2e7ce283_Tx    20u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_45_oLIN00_e2d6e21d_Tx    21u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_46_oLIN00_6c59e5fe_Tx    22u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_47_oLIN00_a0f3e560_Tx    23u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_48_oLIN00_79d3f7cc_Tx    24u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_49_oLIN00_b579f752_Tx    25u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_50_oLIN00_bd4df83a_Tx    26u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_51_oLIN00_71e7f8a4_Tx    27u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_52_oLIN00_ff68ff47_Tx    28u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_53_oLIN00_33c2ffd9_Tx    29u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_54_oLIN00_3907f6c0_Tx    30u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_55_oLIN00_f5adf65e_Tx    31u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_56_oLIN00_7b22f1bd_Tx    32u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_57_oLIN00_b788f123_Tx    33u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_58_oLIN00_6ea8e38f_Tx    34u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_59_oLIN00_a202e311_Tx    35u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_60_oLIN00_84c0c4ff_Tx    36u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_L1_oLIN00_2de78d2a_Tx    37u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_L2_oLIN01_d46fba5f_Tx    38u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_L3_oLIN02_81cceb7b_Tx    39u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_L4_oLIN03_fc0ed2f4_Tx    40u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP1_L5_oLIN04_aec047c9_Tx    41u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_4A_oLIN01_24292be0_Tx    42u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_4B_oLIN01_aaa62c03_Tx    43u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_4C_oLIN01_660c2c9d_Tx    44u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_4D_oLIN01_6cc92584_Tx    45u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_4E_oLIN01_a063251a_Tx    46u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_4F_oLIN01_2eec22f9_Tx    47u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_5A_oLIN01_33523fa3_Tx    48u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_5B_oLIN01_bddd3840_Tx    49u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_5C_oLIN01_717738de_Tx    50u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_5D_oLIN01_7bb231c7_Tx    51u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_5E_oLIN01_b7183159_Tx    52u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_5F_oLIN01_399736ba_Tx    53u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_40_oLIN01_44d3baee_Tx    54u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_41_oLIN01_8879ba70_Tx    55u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_42_oLIN01_06f6bd93_Tx    56u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_43_oLIN01_ca5cbd0d_Tx    57u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_44_oLIN01_c099b414_Tx    58u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_45_oLIN01_0c33b48a_Tx    59u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_46_oLIN01_82bcb369_Tx    60u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_47_oLIN01_4e16b3f7_Tx    61u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_48_oLIN01_9736a15b_Tx    62u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_49_oLIN01_5b9ca1c5_Tx    63u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_50_oLIN01_53a8aead_Tx    64u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_51_oLIN01_9f02ae33_Tx    65u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_52_oLIN01_118da9d0_Tx    66u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_53_oLIN01_dd27a94e_Tx    67u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_54_oLIN01_d7e2a057_Tx    68u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_55_oLIN01_1b48a0c9_Tx    69u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_56_oLIN01_95c7a72a_Tx    70u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_57_oLIN01_596da7b4_Tx    71u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_58_oLIN01_804db518_Tx    72u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_59_oLIN01_4ce7b586_Tx    73u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_60_oLIN01_6a259268_Tx    74u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_L1_oLIN00_b405eb2b_Tx    75u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_L2_oLIN01_4d8ddc5e_Tx    76u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP2_L3_oLIN02_182e8d7a_Tx    77u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_4A_oLIN02_7caea59a_Tx    78u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_4B_oLIN02_f221a279_Tx    79u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_4C_oLIN02_3e8ba2e7_Tx    80u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_4D_oLIN02_344eabfe_Tx    81u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_4E_oLIN02_f8e4ab60_Tx    82u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_4F_oLIN02_766bac83_Tx    83u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_5A_oLIN02_6bd5b1d9_Tx    84u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_5B_oLIN02_e55ab63a_Tx    85u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_5C_oLIN02_29f0b6a4_Tx    86u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_5D_oLIN02_2335bfbd_Tx    87u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_5E_oLIN02_ef9fbf23_Tx    88u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_5F_oLIN02_6110b8c0_Tx    89u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_40_oLIN02_1c543494_Tx    90u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_41_oLIN02_d0fe340a_Tx    91u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_42_oLIN02_5e7133e9_Tx    92u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_43_oLIN02_92db3377_Tx    93u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_44_oLIN02_981e3a6e_Tx    94u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_45_oLIN02_54b43af0_Tx    95u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_46_oLIN02_da3b3d13_Tx    96u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_47_oLIN02_16913d8d_Tx    97u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_48_oLIN02_cfb12f21_Tx    98u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_49_oLIN02_031b2fbf_Tx    99u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_50_oLIN02_0b2f20d7_Tx    100u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_51_oLIN02_c7852049_Tx    101u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_52_oLIN02_490a27aa_Tx    102u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_53_oLIN02_85a02734_Tx    103u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_54_oLIN02_8f652e2d_Tx    104u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_55_oLIN02_43cf2eb3_Tx    105u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_56_oLIN02_cd402950_Tx    106u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_57_oLIN02_01ea29ce_Tx    107u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_58_oLIN02_d8ca3b62_Tx    108u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_59_oLIN02_14603bfc_Tx    109u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_60_oLIN02_32a21c12_Tx    110u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP3_L2_oLIN01_8c03039e_Tx    111u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP4_40_oLIN03_42681181_Tx    112u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP4_L2_oLIN01_a538161d_Tx    113u
#define LinTpConf_LinTpTxNSdu_MasterReq_FSP5_40_oLIN04_1d825be2_Tx    114u
#define LinTpConf_LinTpTxNSdu_MasterReq_ILCP1_oLIN00_57efaae5_Tx      115u
#define LinTpConf_LinTpTxNSdu_MasterReq_ILCP2_oLIN03_4069fcbc_Tx      116u
#define LinTpConf_LinTpTxNSdu_MasterReq_LECM2_oLIN00_670141b5_Tx      117u
#define LinTpConf_LinTpTxNSdu_MasterReq_LECMBasic_oLIN00_16bdebb7_Tx  118u
#define LinTpConf_LinTpTxNSdu_MasterReq_RCECS_oLIN04_b46d7afd_Tx      119u
#define LinTpConf_LinTpTxNSdu_MasterReq_TCP_oLIN02_4b422897_Tx        120u
#define LinTpConf_LinTpTxNSdu_MasterReq_oLIN00_4a2bb011_Tx            121u
#define LinTpConf_LinTpTxNSdu_MasterReq_oLIN01_3d2c8087_Tx            122u
#define LinTpConf_LinTpTxNSdu_MasterReq_oLIN02_a425d13d_Tx            123u
#define LinTpConf_LinTpTxNSdu_MasterReq_oLIN03_d322e1ab_Tx            124u
#define LinTpConf_LinTpTxNSdu_MasterReq_oLIN04_4d467408_Tx            125u
#define LinTpConf_LinTpTxNSdu_MasterReq_oLIN05_3a41449e_Tx            126u
#define LinTpConf_LinTpTxNSdu_MasterReq_oLIN06_a3481524_Tx            127u
#define LinTpConf_LinTpTxNSdu_MasterReq_oLIN07_d44f25b2_Tx            128u
/**\} */


/**********************************************************************************************************************
 *  Symbolic Name Values for RxNSdus
 *********************************************************************************************************************/



/**
 * \defgroup LinTpHandleIds Handle IDs.
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define LinTpConf_LinTpRxNSdu_SlaveResp_CCFW_oLIN03_d7124ce9_Rx       0u
#define LinTpConf_LinTpRxNSdu_SlaveResp_DLFW_oLIN03_0f2202e7_Rx       1u
#define LinTpConf_LinTpRxNSdu_SlaveResp_ELCP1_oLIN03_8611f0b3_Rx      2u
#define LinTpConf_LinTpRxNSdu_SlaveResp_ELCP2_oLIN03_089ef750_Rx      3u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_4A_oLIN00_8520c1c5_Rx    4u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_4B_oLIN00_0bafc626_Rx    5u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_4C_oLIN00_c705c6b8_Rx    6u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_4D_oLIN00_cdc0cfa1_Rx    7u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_4E_oLIN00_016acf3f_Rx    8u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_4F_oLIN00_8fe5c8dc_Rx    9u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_5A_oLIN00_925bd586_Rx    10u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_5B_oLIN00_1cd4d265_Rx    11u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_5C_oLIN00_d07ed2fb_Rx    12u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_5D_oLIN00_dabbdbe2_Rx    13u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_5E_oLIN00_1611db7c_Rx    14u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_5F_oLIN00_989edc9f_Rx    15u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_40_oLIN00_e5da50cb_Rx    16u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_41_oLIN00_29705055_Rx    17u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_42_oLIN00_a7ff57b6_Rx    18u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_43_oLIN00_6b555728_Rx    19u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_44_oLIN00_61905e31_Rx    20u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_45_oLIN00_ad3a5eaf_Rx    21u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_46_oLIN00_23b5594c_Rx    22u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_47_oLIN00_ef1f59d2_Rx    23u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_48_oLIN00_363f4b7e_Rx    24u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_49_oLIN00_fa954be0_Rx    25u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_50_oLIN00_f2a14488_Rx    26u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_51_oLIN00_3e0b4416_Rx    27u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_52_oLIN00_b08443f5_Rx    28u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_53_oLIN00_7c2e436b_Rx    29u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_54_oLIN00_76eb4a72_Rx    30u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_55_oLIN00_ba414aec_Rx    31u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_56_oLIN00_34ce4d0f_Rx    32u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_57_oLIN00_f8644d91_Rx    33u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_58_oLIN00_21445f3d_Rx    34u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_59_oLIN00_edee5fa3_Rx    35u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_60_oLIN00_cb2c784d_Rx    36u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_L1_oLIN00_620b3198_Rx    37u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_L2_oLIN01_9b8306ed_Rx    38u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_L3_oLIN02_ce2057c9_Rx    39u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_L4_oLIN03_b3e26e46_Rx    40u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_L5_oLIN04_e12cfb7b_Rx    41u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_4A_oLIN01_6bc59752_Rx    42u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_4B_oLIN01_e54a90b1_Rx    43u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_4C_oLIN01_29e0902f_Rx    44u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_4D_oLIN01_23259936_Rx    45u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_4E_oLIN01_ef8f99a8_Rx    46u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_4F_oLIN01_61009e4b_Rx    47u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_5A_oLIN01_7cbe8311_Rx    48u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_5B_oLIN01_f23184f2_Rx    49u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_5C_oLIN01_3e9b846c_Rx    50u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_5D_oLIN01_345e8d75_Rx    51u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_5E_oLIN01_f8f48deb_Rx    52u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_5F_oLIN01_767b8a08_Rx    53u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_40_oLIN01_0b3f065c_Rx    54u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_41_oLIN01_c79506c2_Rx    55u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_42_oLIN01_491a0121_Rx    56u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_43_oLIN01_85b001bf_Rx    57u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_44_oLIN01_8f7508a6_Rx    58u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_45_oLIN01_43df0838_Rx    59u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_46_oLIN01_cd500fdb_Rx    60u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_47_oLIN01_01fa0f45_Rx    61u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_48_oLIN01_d8da1de9_Rx    62u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_49_oLIN01_14701d77_Rx    63u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_50_oLIN01_1c44121f_Rx    64u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_51_oLIN01_d0ee1281_Rx    65u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_52_oLIN01_5e611562_Rx    66u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_53_oLIN01_92cb15fc_Rx    67u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_54_oLIN01_980e1ce5_Rx    68u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_55_oLIN01_54a41c7b_Rx    69u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_56_oLIN01_da2b1b98_Rx    70u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_57_oLIN01_16811b06_Rx    71u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_58_oLIN01_cfa109aa_Rx    72u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_59_oLIN01_030b0934_Rx    73u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_60_oLIN01_25c92eda_Rx    74u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_L1_oLIN00_fbe95799_Rx    75u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_L2_oLIN01_026160ec_Rx    76u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_L3_oLIN02_57c231c8_Rx    77u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_4A_oLIN02_33421928_Rx    78u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_4B_oLIN02_bdcd1ecb_Rx    79u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_4C_oLIN02_71671e55_Rx    80u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_4D_oLIN02_7ba2174c_Rx    81u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_4E_oLIN02_b70817d2_Rx    82u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_4F_oLIN02_39871031_Rx    83u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_5A_oLIN02_24390d6b_Rx    84u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_5B_oLIN02_aab60a88_Rx    85u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_5C_oLIN02_661c0a16_Rx    86u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_5D_oLIN02_6cd9030f_Rx    87u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_5E_oLIN02_a0730391_Rx    88u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_5F_oLIN02_2efc0472_Rx    89u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_40_oLIN02_53b88826_Rx    90u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_41_oLIN02_9f1288b8_Rx    91u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_42_oLIN02_119d8f5b_Rx    92u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_43_oLIN02_dd378fc5_Rx    93u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_44_oLIN02_d7f286dc_Rx    94u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_45_oLIN02_1b588642_Rx    95u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_46_oLIN02_95d781a1_Rx    96u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_47_oLIN02_597d813f_Rx    97u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_48_oLIN02_805d9393_Rx    98u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_49_oLIN02_4cf7930d_Rx    99u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_50_oLIN02_44c39c65_Rx    100u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_51_oLIN02_88699cfb_Rx    101u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_52_oLIN02_06e69b18_Rx    102u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_53_oLIN02_ca4c9b86_Rx    103u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_54_oLIN02_c089929f_Rx    104u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_55_oLIN02_0c239201_Rx    105u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_56_oLIN02_82ac95e2_Rx    106u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_57_oLIN02_4e06957c_Rx    107u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_58_oLIN02_972687d0_Rx    108u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_59_oLIN02_5b8c874e_Rx    109u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_60_oLIN02_7d4ea0a0_Rx    110u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_L2_oLIN01_c3efbf2c_Rx    111u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP4_40_oLIN03_0d84ad33_Rx    112u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP4_L2_oLIN01_ead4aaaf_Rx    113u
#define LinTpConf_LinTpRxNSdu_SlaveResp_FSP5_40_oLIN04_526ee750_Rx    114u
#define LinTpConf_LinTpRxNSdu_SlaveResp_ILCP1_oLIN00_1a12ec88_Rx      115u
#define LinTpConf_LinTpRxNSdu_SlaveResp_ILCP2_oLIN03_0d94bad1_Rx      116u
#define LinTpConf_LinTpRxNSdu_SlaveResp_LECM2_oLIN00_2afc07d8_Rx      117u
#define LinTpConf_LinTpRxNSdu_SlaveResp_LECMBasic_oLIN00_029997c6_Rx  118u
#define LinTpConf_LinTpRxNSdu_SlaveResp_LinSlave_L8_oLIN05_ae44c72a_Rx 121u
#define LinTpConf_LinTpRxNSdu_SlaveResp_LinSlave_L8_oLIN06_374d9690_Rx 122u
#define LinTpConf_LinTpRxNSdu_SlaveResp_LinSlave_L8_oLIN07_404aa606_Rx 123u
#define LinTpConf_LinTpRxNSdu_SlaveResp_RCECS_oLIN04_f9903c90_Rx      119u
#define LinTpConf_LinTpRxNSdu_SlaveResp_TCP_oLIN02_b3851a34_Rx        120u
/**\} */


/**********************************************************************************************************************
  GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  LinTpPCDataSwitches  LinTp Data Switches  (PRE_COMPILE)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define LINTP_CHANNELCONFIG                                           STD_ON
#define LINTP_LINTP_SCHEDCHANGENOTIFYOFCHANNELCONFIG                  STD_ON
#define LINTP_LINTP_STRICTNADCHECKOFCHANNELCONFIG                     STD_ON
#define LINTP_CTRL                                                    STD_ON
#define LINTP_FINALMAGICNUMBER                                        STD_OFF  /**< Deactivateable: 'LinTp_FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define LINTP_INITDATAHASHCODE                                        STD_OFF  /**< Deactivateable: 'LinTp_InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define LINTP_LINIFTOLINTPCHANNEL                                     STD_ON
#define LINTP_CHANNELOFLINIFTOLINTPCHANNEL                            STD_ON
#define LINTP_MAXRESPPENDINGFRAMES                                    STD_ON
#define LINTP_NUMBEROFLINTPRXNSDU                                     STD_ON
#define LINTP_NUMBEROFLINTPTXNSDU                                     STD_ON
#define LINTP_P2MAXTIME                                               STD_ON
#define LINTP_P2TIME                                                  STD_ON
#define LINTP_RXNSDU                                                  STD_ON
#define LINTP_CTRLIDXOFRXNSDU                                         STD_ON
#define LINTP_INVALIDHNDOFRXNSDU                                      STD_OFF  /**< Deactivateable: 'LinTp_RxNSdu.InvalidHnd' Reason: 'the value of LinTp_InvalidHndOfRxNSdu is always 'false' due to this, the array is deactivated.' */
#define LINTP_NADOFRXNSDU                                             STD_ON
#define LINTP_NCROFRXNSDU                                             STD_ON
#define LINTP_UPPERLAYERPDUIDOFRXNSDU                                 STD_ON
#define LINTP_SIZEOFCTRL                                              STD_ON
#define LINTP_TXNSDU                                                  STD_ON
#define LINTP_ASSOCIATEDRXNSDUIDOFTXNSDU                              STD_ON
#define LINTP_CTRLIDXOFTXNSDU                                         STD_ON
#define LINTP_INVALIDHNDOFTXNSDU                                      STD_OFF  /**< Deactivateable: 'LinTp_TxNSdu.InvalidHnd' Reason: 'the value of LinTp_InvalidHndOfTxNSdu is always 'false' due to this, the array is deactivated.' */
#define LINTP_NADOFTXNSDU                                             STD_ON
#define LINTP_NASOFTXNSDU                                             STD_ON
#define LINTP_NCSOFTXNSDU                                             STD_ON
#define LINTP_UPPERLAYERPDUIDOFTXNSDU                                 STD_ON
#define LINTP_PCCONFIG                                                STD_ON
#define LINTP_CHANNELCONFIGOFPCCONFIG                                 STD_ON
#define LINTP_CTRLOFPCCONFIG                                          STD_ON
#define LINTP_FINALMAGICNUMBEROFPCCONFIG                              STD_OFF  /**< Deactivateable: 'LinTp_PCConfig.FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define LINTP_INITDATAHASHCODEOFPCCONFIG                              STD_OFF  /**< Deactivateable: 'LinTp_PCConfig.InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define LINTP_LINIFTOLINTPCHANNELOFPCCONFIG                           STD_ON
#define LINTP_MAXRESPPENDINGFRAMESOFPCCONFIG                          STD_ON
#define LINTP_NUMBEROFLINTPRXNSDUOFPCCONFIG                           STD_ON
#define LINTP_NUMBEROFLINTPTXNSDUOFPCCONFIG                           STD_ON
#define LINTP_P2MAXTIMEOFPCCONFIG                                     STD_ON
#define LINTP_P2TIMEOFPCCONFIG                                        STD_ON
#define LINTP_RXNSDUOFPCCONFIG                                        STD_ON
#define LINTP_SIZEOFCTRLOFPCCONFIG                                    STD_ON
#define LINTP_TXNSDUOFPCCONFIG                                        STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCIsReducedToDefineDefines  LinTp Is Reduced To Define Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define is STD_ON else STD_OFF.
  \{
*/ 
#define LINTP_ISDEF_LINTP_SCHEDCHANGENOTIFYOFCHANNELCONFIG            STD_OFF
#define LINTP_ISDEF_LINTP_STRICTNADCHECKOFCHANNELCONFIG               STD_OFF
#define LINTP_ISDEF_CHANNELOFLINIFTOLINTPCHANNEL                      STD_OFF
#define LINTP_ISDEF_CTRLIDXOFRXNSDU                                   STD_OFF
#define LINTP_ISDEF_NADOFRXNSDU                                       STD_OFF
#define LINTP_ISDEF_NCROFRXNSDU                                       STD_OFF
#define LINTP_ISDEF_UPPERLAYERPDUIDOFRXNSDU                           STD_OFF
#define LINTP_ISDEF_ASSOCIATEDRXNSDUIDOFTXNSDU                        STD_OFF
#define LINTP_ISDEF_CTRLIDXOFTXNSDU                                   STD_OFF
#define LINTP_ISDEF_NADOFTXNSDU                                       STD_OFF
#define LINTP_ISDEF_NASOFTXNSDU                                       STD_OFF
#define LINTP_ISDEF_NCSOFTXNSDU                                       STD_OFF
#define LINTP_ISDEF_UPPERLAYERPDUIDOFTXNSDU                           STD_OFF
#define LINTP_ISDEF_CHANNELCONFIGOFPCCONFIG                           STD_ON
#define LINTP_ISDEF_CTRLOFPCCONFIG                                    STD_ON
#define LINTP_ISDEF_LINIFTOLINTPCHANNELOFPCCONFIG                     STD_ON
#define LINTP_ISDEF_RXNSDUOFPCCONFIG                                  STD_ON
#define LINTP_ISDEF_TXNSDUOFPCCONFIG                                  STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCEqualsAlwaysToDefines  LinTp Equals Always To Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define contains the always equals value.
  \{
*/ 
#define LINTP_EQ2_LINTP_SCHEDCHANGENOTIFYOFCHANNELCONFIG              
#define LINTP_EQ2_LINTP_STRICTNADCHECKOFCHANNELCONFIG                 
#define LINTP_EQ2_CHANNELOFLINIFTOLINTPCHANNEL                        
#define LINTP_EQ2_CTRLIDXOFRXNSDU                                     
#define LINTP_EQ2_NADOFRXNSDU                                         
#define LINTP_EQ2_NCROFRXNSDU                                         
#define LINTP_EQ2_UPPERLAYERPDUIDOFRXNSDU                             
#define LINTP_EQ2_ASSOCIATEDRXNSDUIDOFTXNSDU                          
#define LINTP_EQ2_CTRLIDXOFTXNSDU                                     
#define LINTP_EQ2_NADOFTXNSDU                                         
#define LINTP_EQ2_NASOFTXNSDU                                         
#define LINTP_EQ2_NCSOFTXNSDU                                         
#define LINTP_EQ2_UPPERLAYERPDUIDOFTXNSDU                             
#define LINTP_EQ2_CHANNELCONFIGOFPCCONFIG                             LinTp_ChannelConfig
#define LINTP_EQ2_CTRLOFPCCONFIG                                      LinTp_Ctrl.raw
#define LINTP_EQ2_LINIFTOLINTPCHANNELOFPCCONFIG                       LinTp_LinIfToLinTpChannel
#define LINTP_EQ2_RXNSDUOFPCCONFIG                                    LinTp_RxNSdu
#define LINTP_EQ2_TXNSDUOFPCCONFIG                                    LinTp_TxNSdu
/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCSymbolicInitializationPointers  LinTp Symbolic Initialization Pointers (PRE_COMPILE)
  \brief  Symbolic initialization pointers to be used in the call of a preinit or init function.
  \{
*/ 
#define LinTp_Config_Ptr                                              NULL_PTR  /**< symbolic identifier which shall be used to initialize 'LinTp' */
/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCInitializationSymbols  LinTp Initialization Symbols (PRE_COMPILE)
  \brief  Symbolic initialization pointers which may be used in the call of a preinit or init function. Please note, that the defined value can be a 'NULL_PTR' and the address operator is not usable.
  \{
*/ 
#define LinTp_Config                                                  NULL_PTR  /**< symbolic identifier which could be used to initialize 'LinTp */
/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCGeneral  LinTp General (PRE_COMPILE)
  \brief  General constant defines not associated with a group of defines.
  \{
*/ 
#define LINTP_CHECK_INIT_POINTER                                      STD_OFF  /**< STD_ON if the init pointer shall not be used as NULL_PTR and a check shall validate this. */
#define LINTP_FINAL_MAGIC_NUMBER                                      0x3E1Eu  /**< the precompile constant to validate the size of the initialization structure at initialization time of LinTp */
#define LINTP_INDIVIDUAL_POSTBUILD                                    STD_OFF  /**< the precompile constant to check, that the module is individual postbuildable. The module 'LinTp' is not configured to be postbuild capable. */
#define LINTP_INIT_DATA                                               LINTP_CONST  /**< CompilerMemClassDefine for the initialization data. */
#define LINTP_INIT_DATA_HASH_CODE                                     1633764943  /**< the precompile constant to validate the initialization structure at initialization time of LinTp with a hashcode. The seed value is '0x3E1Eu' */
#define LINTP_USE_ECUM_BSW_ERROR_HOOK                                 STD_OFF  /**< STD_ON if the EcuM_BswErrorHook shall be called in the ConfigPtr check. */
#define LINTP_USE_INIT_POINTER                                        STD_OFF  /**< STD_ON if the init pointer LinTp shall be used. */
/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  LinTpLTDataSwitches  LinTp Data Switches  (LINK)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define LINTP_LTCONFIG                                                STD_OFF  /**< Deactivateable: 'LinTp_LTConfig' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  LinTpPBDataSwitches  LinTp Data Switches  (POST_BUILD)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define LINTP_PBCONFIG                                                STD_OFF  /**< Deactivateable: 'LinTp_PBConfig' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define LINTP_LTCONFIGIDXOFPBCONFIG                                   STD_OFF  /**< Deactivateable: 'LinTp_PBConfig.LTConfigIdx' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define LINTP_PCCONFIGIDXOFPBCONFIG                                   STD_OFF  /**< Deactivateable: 'LinTp_PBConfig.PCConfigIdx' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
/** 
  \}
*/ 



/**********************************************************************************************************************
  GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
/** 
  \defgroup  LinTpPCGetConstantDuplicatedRootDataMacros  LinTp Get Constant Duplicated Root Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated by constance root data elements.
  \{
*/ 
#define LinTp_GetChannelConfigOfPCConfig()                            LinTp_ChannelConfig  /**< the pointer to LinTp_ChannelConfig */
#define LinTp_GetCtrlOfPCConfig()                                     LinTp_Ctrl.raw  /**< the pointer to LinTp_Ctrl */
#define LinTp_GetLinIfToLinTpChannelOfPCConfig()                      LinTp_LinIfToLinTpChannel  /**< the pointer to LinTp_LinIfToLinTpChannel */
#define LinTp_GetMaxRespPendingFramesOfPCConfig()                     0x03u  /**< Max number of RP frames */
#define LinTp_GetNumberOfLinTpRxNSduOfPCConfig()                      124u  /**< Size of LinTp_RxNSdu */
#define LinTp_GetNumberOfLinTpTxNSduOfPCConfig()                      129u  /**< Size of LinTp_TxNSdu */
#define LinTp_GetP2MaxTimeOfPCConfig()                                0x012Du  /**< P2Max timeout in ticks */
#define LinTp_GetP2TimeOfPCConfig()                                   0x15u  /**< P2 timeout in ticks */
#define LinTp_GetRxNSduOfPCConfig()                                   LinTp_RxNSdu  /**< the pointer to LinTp_RxNSdu */
#define LinTp_GetSizeOfCtrlOfPCConfig()                               8u  /**< the number of accomplishable value elements in LinTp_Ctrl */
#define LinTp_GetTxNSduOfPCConfig()                                   LinTp_TxNSdu  /**< the pointer to LinTp_TxNSdu */
/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCGetDataMacros  LinTp Get Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read CONST and VAR data.
  \{
*/ 
#define LinTp_IsLinTp_SchedChangeNotifyOfChannelConfig(Index)         ((LinTp_GetChannelConfigOfPCConfig()[(Index)].LinTp_SchedChangeNotifyOfChannelConfig) != FALSE)
#define LinTp_IsLinTp_StrictNADCheckOfChannelConfig(Index)            ((LinTp_GetChannelConfigOfPCConfig()[(Index)].LinTp_StrictNADCheckOfChannelConfig) != FALSE)
#define LinTp_GetCtrl(Index)                                          (LinTp_GetCtrlOfPCConfig()[(Index)])
#define LinTp_GetChannelOfLinIfToLinTpChannel(Index)                  (LinTp_GetLinIfToLinTpChannelOfPCConfig()[(Index)].ChannelOfLinIfToLinTpChannel)
#define LinTp_GetCtrlIdxOfRxNSdu(Index)                               (LinTp_GetRxNSduOfPCConfig()[(Index)].CtrlIdxOfRxNSdu)
#define LinTp_GetNADOfRxNSdu(Index)                                   (LinTp_GetRxNSduOfPCConfig()[(Index)].NADOfRxNSdu)
#define LinTp_GetNcrOfRxNSdu(Index)                                   (LinTp_GetRxNSduOfPCConfig()[(Index)].NcrOfRxNSdu)
#define LinTp_GetUpperLayerPduIdOfRxNSdu(Index)                       (LinTp_GetRxNSduOfPCConfig()[(Index)].UpperLayerPduIdOfRxNSdu)
#define LinTp_GetAssociatedRxNSduIdOfTxNSdu(Index)                    (LinTp_GetTxNSduOfPCConfig()[(Index)].AssociatedRxNSduIdOfTxNSdu)
#define LinTp_GetCtrlIdxOfTxNSdu(Index)                               (LinTp_GetTxNSduOfPCConfig()[(Index)].CtrlIdxOfTxNSdu)
#define LinTp_GetNADOfTxNSdu(Index)                                   (LinTp_GetTxNSduOfPCConfig()[(Index)].NADOfTxNSdu)
#define LinTp_GetNasOfTxNSdu(Index)                                   (LinTp_GetTxNSduOfPCConfig()[(Index)].NasOfTxNSdu)
#define LinTp_GetNcsOfTxNSdu(Index)                                   (LinTp_GetTxNSduOfPCConfig()[(Index)].NcsOfTxNSdu)
#define LinTp_GetUpperLayerPduIdOfTxNSdu(Index)                       (LinTp_GetTxNSduOfPCConfig()[(Index)].UpperLayerPduIdOfTxNSdu)
/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCGetDeduplicatedDataMacros  LinTp Get Deduplicated Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated data elements.
  \{
*/ 
#define LinTp_GetMaxRespPendingFrames()                               LinTp_GetMaxRespPendingFramesOfPCConfig()
#define LinTp_GetNumberOfLinTpRxNSdu()                                LinTp_GetNumberOfLinTpRxNSduOfPCConfig()
#define LinTp_GetNumberOfLinTpTxNSdu()                                LinTp_GetNumberOfLinTpTxNSduOfPCConfig()
#define LinTp_GetP2MaxTime()                                          LinTp_GetP2MaxTimeOfPCConfig()
#define LinTp_GetP2Time()                                             LinTp_GetP2TimeOfPCConfig()
#define LinTp_GetSizeOfCtrl()                                         LinTp_GetSizeOfCtrlOfPCConfig()
/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCSetDataMacros  LinTp Set Data Macros (PRE_COMPILE)
  \brief  These macros can be used to write data.
  \{
*/ 
#define LinTp_SetCtrl(Index, Value)                                   LinTp_GetCtrlOfPCConfig()[(Index)] = (Value)
/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCGetAddressOfDataMacros  LinTp Get Address Of Data Macros (PRE_COMPILE)
  \brief  These macros can be used to get the data by the address operator.
  \{
*/ 
#define LinTp_GetAddrCtrl(Index)                                      (&LinTp_GetCtrl(Index))
/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCHasMacros  LinTp Has Macros (PRE_COMPILE)
  \brief  These macros can be used to detect at runtime a deactivated piece of information. TRUE in the CONFIGURATION_VARIANT PRE-COMPILE, TRUE or FALSE in the CONFIGURATION_VARIANT POST-BUILD.
  \{
*/ 
#define LinTp_HasChannelConfig()                                      (TRUE != FALSE)
#define LinTp_HasLinTp_SchedChangeNotifyOfChannelConfig()             (TRUE != FALSE)
#define LinTp_HasLinTp_StrictNADCheckOfChannelConfig()                (TRUE != FALSE)
#define LinTp_HasCtrl()                                               (TRUE != FALSE)
#define LinTp_HasLinIfToLinTpChannel()                                (TRUE != FALSE)
#define LinTp_HasChannelOfLinIfToLinTpChannel()                       (TRUE != FALSE)
#define LinTp_HasMaxRespPendingFrames()                               (TRUE != FALSE)
#define LinTp_HasNumberOfLinTpRxNSdu()                                (TRUE != FALSE)
#define LinTp_HasNumberOfLinTpTxNSdu()                                (TRUE != FALSE)
#define LinTp_HasP2MaxTime()                                          (TRUE != FALSE)
#define LinTp_HasP2Time()                                             (TRUE != FALSE)
#define LinTp_HasRxNSdu()                                             (TRUE != FALSE)
#define LinTp_HasCtrlIdxOfRxNSdu()                                    (TRUE != FALSE)
#define LinTp_HasNADOfRxNSdu()                                        (TRUE != FALSE)
#define LinTp_HasNcrOfRxNSdu()                                        (TRUE != FALSE)
#define LinTp_HasUpperLayerPduIdOfRxNSdu()                            (TRUE != FALSE)
#define LinTp_HasSizeOfCtrl()                                         (TRUE != FALSE)
#define LinTp_HasTxNSdu()                                             (TRUE != FALSE)
#define LinTp_HasAssociatedRxNSduIdOfTxNSdu()                         (TRUE != FALSE)
#define LinTp_HasCtrlIdxOfTxNSdu()                                    (TRUE != FALSE)
#define LinTp_HasNADOfTxNSdu()                                        (TRUE != FALSE)
#define LinTp_HasNasOfTxNSdu()                                        (TRUE != FALSE)
#define LinTp_HasNcsOfTxNSdu()                                        (TRUE != FALSE)
#define LinTp_HasUpperLayerPduIdOfTxNSdu()                            (TRUE != FALSE)
#define LinTp_HasPCConfig()                                           (TRUE != FALSE)
#define LinTp_HasChannelConfigOfPCConfig()                            (TRUE != FALSE)
#define LinTp_HasCtrlOfPCConfig()                                     (TRUE != FALSE)
#define LinTp_HasLinIfToLinTpChannelOfPCConfig()                      (TRUE != FALSE)
#define LinTp_HasMaxRespPendingFramesOfPCConfig()                     (TRUE != FALSE)
#define LinTp_HasNumberOfLinTpRxNSduOfPCConfig()                      (TRUE != FALSE)
#define LinTp_HasNumberOfLinTpTxNSduOfPCConfig()                      (TRUE != FALSE)
#define LinTp_HasP2MaxTimeOfPCConfig()                                (TRUE != FALSE)
#define LinTp_HasP2TimeOfPCConfig()                                   (TRUE != FALSE)
#define LinTp_HasRxNSduOfPCConfig()                                   (TRUE != FALSE)
#define LinTp_HasSizeOfCtrlOfPCConfig()                               (TRUE != FALSE)
#define LinTp_HasTxNSduOfPCConfig()                                   (TRUE != FALSE)
/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCIncrementDataMacros  LinTp Increment Data Macros (PRE_COMPILE)
  \brief  These macros can be used to increment VAR data with numerical nature.
  \{
*/ 
#define LinTp_IncCtrl(Index)                                          LinTp_GetCtrl(Index)++
/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCDecrementDataMacros  LinTp Decrement Data Macros (PRE_COMPILE)
  \brief  These macros can be used to decrement VAR data with numerical nature.
  \{
*/ 
#define LinTp_DecCtrl(Index)                                          LinTp_GetCtrl(Index)--
/** 
  \}
*/ 

  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/

/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/

/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  LinTpPCIterableTypes  LinTp Iterable Types (PRE_COMPILE)
  \brief  These type definitions are used to iterate over an array with least processor cycles for variable access as possible.
  \{
*/ 
/**   \brief  type used to iterate LinTp_ChannelConfig */
typedef uint8_least LinTp_ChannelConfigIterType;

/**   \brief  type used to iterate LinTp_Ctrl */
typedef uint8_least LinTp_CtrlIterType;

/**   \brief  type used to iterate LinTp_LinIfToLinTpChannel */
typedef uint8_least LinTp_LinIfToLinTpChannelIterType;

/**   \brief  type used to iterate LinTp_RxNSdu */
typedef uint8_least LinTp_RxNSduIterType;

/**   \brief  type used to iterate LinTp_TxNSdu */
typedef uint8_least LinTp_TxNSduIterType;

/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCValueTypes  LinTp Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value based data representations.
  \{
*/ 
/**   \brief  value based type definition for LinTp_LinTp_SchedChangeNotifyOfChannelConfig */
typedef boolean LinTp_LinTp_SchedChangeNotifyOfChannelConfigType;

/**   \brief  value based type definition for LinTp_LinTp_StrictNADCheckOfChannelConfig */
typedef boolean LinTp_LinTp_StrictNADCheckOfChannelConfigType;

/**   \brief  value based type definition for LinTp_ChannelOfLinIfToLinTpChannel */
typedef uint8 LinTp_ChannelOfLinIfToLinTpChannelType;

/**   \brief  value based type definition for LinTp_MaxRespPendingFrames */
typedef uint8 LinTp_MaxRespPendingFramesType;

/**   \brief  value based type definition for LinTp_NumberOfLinTpRxNSdu */
typedef uint8 LinTp_NumberOfLinTpRxNSduType;

/**   \brief  value based type definition for LinTp_NumberOfLinTpTxNSdu */
typedef uint8 LinTp_NumberOfLinTpTxNSduType;

/**   \brief  value based type definition for LinTp_P2MaxTime */
typedef uint16 LinTp_P2MaxTimeType;

/**   \brief  value based type definition for LinTp_P2Time */
typedef uint8 LinTp_P2TimeType;

/**   \brief  value based type definition for LinTp_CtrlIdxOfRxNSdu */
typedef uint8 LinTp_CtrlIdxOfRxNSduType;

/**   \brief  value based type definition for LinTp_NADOfRxNSdu */
typedef uint8 LinTp_NADOfRxNSduType;

/**   \brief  value based type definition for LinTp_NcrOfRxNSdu */
typedef uint8 LinTp_NcrOfRxNSduType;

/**   \brief  value based type definition for LinTp_UpperLayerPduIdOfRxNSdu */
typedef PduIdType LinTp_UpperLayerPduIdOfRxNSduType;

/**   \brief  value based type definition for LinTp_SizeOfCtrl */
typedef uint8 LinTp_SizeOfCtrlType;

/**   \brief  value based type definition for LinTp_AssociatedRxNSduIdOfTxNSdu */
typedef uint8 LinTp_AssociatedRxNSduIdOfTxNSduType;

/**   \brief  value based type definition for LinTp_CtrlIdxOfTxNSdu */
typedef uint8 LinTp_CtrlIdxOfTxNSduType;

/**   \brief  value based type definition for LinTp_NADOfTxNSdu */
typedef uint8 LinTp_NADOfTxNSduType;

/**   \brief  value based type definition for LinTp_NasOfTxNSdu */
typedef uint8 LinTp_NasOfTxNSduType;

/**   \brief  value based type definition for LinTp_NcsOfTxNSdu */
typedef uint8 LinTp_NcsOfTxNSduType;

/**   \brief  value based type definition for LinTp_UpperLayerPduIdOfTxNSdu */
typedef PduIdType LinTp_UpperLayerPduIdOfTxNSduType;

/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  LinTpPCStructTypes  LinTp Struct Types (PRE_COMPILE)
  \brief  These type definitions are used for structured data representations.
  \{
*/ 
/**   \brief  type used in LinTp_ChannelConfig */
typedef struct sLinTp_ChannelConfigType
{
  LinTp_LinTp_SchedChangeNotifyOfChannelConfigType LinTp_SchedChangeNotifyOfChannelConfig;
  LinTp_LinTp_StrictNADCheckOfChannelConfigType LinTp_StrictNADCheckOfChannelConfig;  /**< My comment /ActiveEcuC/LinTp/LinTpGlobalConfig/CHNL_5864b8ff */
} LinTp_ChannelConfigType;

/**   \brief  type used in LinTp_LinIfToLinTpChannel */
typedef struct sLinTp_LinIfToLinTpChannelType
{
  LinTp_ChannelOfLinIfToLinTpChannelType ChannelOfLinIfToLinTpChannel;
} LinTp_LinIfToLinTpChannelType;

/**   \brief  type used in LinTp_RxNSdu */
typedef struct sLinTp_RxNSduType
{
  LinTp_UpperLayerPduIdOfRxNSduType UpperLayerPduIdOfRxNSdu;  /**< RxNSdu external ID (SNV) */
  LinTp_CtrlIdxOfRxNSduType CtrlIdxOfRxNSdu;  /**< the index of the 1:1 relation pointing to LinTp_Ctrl */
  LinTp_NADOfRxNSduType NADOfRxNSdu;  /**< NAD */
  LinTp_NcrOfRxNSduType NcrOfRxNSdu;  /**< Ncr timeout in ticks */
} LinTp_RxNSduType;

/**   \brief  type used in LinTp_TxNSdu */
typedef struct sLinTp_TxNSduType
{
  LinTp_UpperLayerPduIdOfTxNSduType UpperLayerPduIdOfTxNSdu;  /**< TxNSdu external ID (SNV) */
  LinTp_AssociatedRxNSduIdOfTxNSduType AssociatedRxNSduIdOfTxNSdu;
  LinTp_CtrlIdxOfTxNSduType CtrlIdxOfTxNSdu;  /**< the index of the 1:1 relation pointing to LinTp_Ctrl */
  LinTp_NADOfTxNSduType NADOfTxNSdu;  /**< NAD */
  LinTp_NasOfTxNSduType NasOfTxNSdu;  /**< Nas timeout in ticks */
  LinTp_NcsOfTxNSduType NcsOfTxNSdu;  /**< Ncs timeout in ticks */
} LinTp_TxNSduType;

/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCSymbolicStructTypes  LinTp Symbolic Struct Types (PRE_COMPILE)
  \brief  These structs are used in unions to have a symbol based data representation style.
  \{
*/ 
/**   \brief  type to be used as symbolic data element access to LinTp_Ctrl */
typedef struct LinTp_CtrlStructSTag
{
  LinTp_ControlType CHNL_45618847;
  LinTp_ControlType CHNL_8e3d5be2;
  LinTp_ControlType CHNL_08a9294c;
  LinTp_ControlType CHNL_c3f5fae9;
  LinTp_ControlType CHNL_def0ca51;
  LinTp_ControlType CHNL_15ac19f4;
  LinTp_ControlType CHNL_93386b5a;
  LinTp_ControlType CHNL_5864b8ff;
} LinTp_CtrlStructSType;

/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCUnionIndexAndSymbolTypes  LinTp Union Index And Symbol Types (PRE_COMPILE)
  \brief  These unions are used to access arrays in an index and symbol based style.
  \{
*/ 
/**   \brief  type to access LinTp_Ctrl in an index and symbol based style. */
typedef union LinTp_CtrlUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  LinTp_ControlType raw[8];
  LinTp_CtrlStructSType str;
} LinTp_CtrlUType;

/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCRootPointerTypes  LinTp Root Pointer Types (PRE_COMPILE)
  \brief  These type definitions are used to point from the config root to symbol instances.
  \{
*/ 
/**   \brief  type used to point to LinTp_ChannelConfig */
typedef P2CONST(LinTp_ChannelConfigType, TYPEDEF, LINTP_CONST) LinTp_ChannelConfigPtrType;

/**   \brief  type used to point to LinTp_Ctrl */
typedef P2VAR(LinTp_ControlType, TYPEDEF, LINTP_VAR_NOINIT) LinTp_CtrlPtrType;

/**   \brief  type used to point to LinTp_LinIfToLinTpChannel */
typedef P2CONST(LinTp_LinIfToLinTpChannelType, TYPEDEF, LINTP_CONST) LinTp_LinIfToLinTpChannelPtrType;

/**   \brief  type used to point to LinTp_RxNSdu */
typedef P2CONST(LinTp_RxNSduType, TYPEDEF, LINTP_CONST) LinTp_RxNSduPtrType;

/**   \brief  type used to point to LinTp_TxNSdu */
typedef P2CONST(LinTp_TxNSduType, TYPEDEF, LINTP_CONST) LinTp_TxNSduPtrType;

/** 
  \}
*/ 

/** 
  \defgroup  LinTpPCRootValueTypes  LinTp Root Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value representations in root arrays.
  \{
*/ 
/**   \brief  type used in LinTp_PCConfig */
typedef struct sLinTp_PCConfigType
{
  uint8 LinTp_PCConfigNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} LinTp_PCConfigType;

typedef LinTp_PCConfigType LinTp_ConfigType;  /**< A structure type is present for data in each configuration class. This typedef redefines the probably different name to the specified one. */

/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  LinTp_ChannelConfig
**********************************************************************************************************************/
/** 
  \var    LinTp_ChannelConfig
  \details
  Element                    Description
  LinTp_SchedChangeNotify
  LinTp_StrictNADCheck       My comment /ActiveEcuC/LinTp/LinTpGlobalConfig/CHNL_5864b8ff
*/ 
#define LINTP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(LinTp_ChannelConfigType, LINTP_CONST) LinTp_ChannelConfig[8];
#define LINTP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinTp_LinIfToLinTpChannel
**********************************************************************************************************************/
/** 
  \var    LinTp_LinIfToLinTpChannel
  \details
  Element    Description
  Channel
*/ 
#define LINTP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(LinTp_LinIfToLinTpChannelType, LINTP_CONST) LinTp_LinIfToLinTpChannel[8];
#define LINTP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinTp_RxNSdu
**********************************************************************************************************************/
/** 
  \var    LinTp_RxNSdu
  \brief  List of all LinTp RxNsdus sorted by their PduId
  \details
  Element            Description
  UpperLayerPduId    RxNSdu external ID (SNV)
  CtrlIdx            the index of the 1:1 relation pointing to LinTp_Ctrl
  NAD                NAD
  Ncr                Ncr timeout in ticks
*/ 
#define LINTP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(LinTp_RxNSduType, LINTP_CONST) LinTp_RxNSdu[124];
#define LINTP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinTp_TxNSdu
**********************************************************************************************************************/
/** 
  \var    LinTp_TxNSdu
  \brief  List of all LinTp TxNsdus sorted by their PduId
  \details
  Element               Description
  UpperLayerPduId       TxNSdu external ID (SNV)
  AssociatedRxNSduId
  CtrlIdx               the index of the 1:1 relation pointing to LinTp_Ctrl
  NAD                   NAD
  Nas                   Nas timeout in ticks
  Ncs                   Ncs timeout in ticks
*/ 
#define LINTP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(LinTp_TxNSduType, LINTP_CONST) LinTp_TxNSdu[129];
#define LINTP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinTp_Ctrl
**********************************************************************************************************************/
#define LINTP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(LinTp_CtrlUType, LINTP_VAR_NOINIT) LinTp_Ctrl;  /* PRQA S 0759 */  /* MD_CSL_Union */  /* Data structure per LinIf channel */
#define LINTP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/




#endif


