/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Can
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Can_Cfg.h
 *   Generation Time: 2020-09-01 16:17:06
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/

/* -----------------------------------------------------------------------------
    Generator Info
 ----------------------------------------------------------------------------- 
  Name:     MICROSAR Can Flexcan3 driver Generator
  Version:  3.09.00
  MSN:      Can
  Origin:   CAN
  Descrip:  MICROSAR Can driver generator
  JavaPack: com.vector.cfg.gen.DrvCan_ImxFlexcan3Asr
 ----------------------------------------------------------------------------- */

#if !defined(CAN_CFG_H)
#define CAN_CFG_H

/* CAN222, CAN389, CAN365, CAN366, CAN367 */
/* CAN022, CAN047, CAN388, CAN397, CAN390, CAN392  */

/* PRQA S 0779 EOF */ /* MD_MSR_Rule5.2 */

/* -----------------------------------------------------------------------------
    Includes
 ----------------------------------------------------------------------------- */

#include "ComStack_Types.h"

#include "Can_GeneralTypes.h" /* CAN435 */

#if defined (CAN_LCFG_SOURCE) || defined (C_DRV_INTERNAL) /* ESCAN00070085 */
# include "Os.h"
#endif

/* -----------------------------------------------------------------------------
    General Switches for CAN module
 ----------------------------------------------------------------------------- */

#ifndef CAN_USE_DUMMY_STATEMENT
#define CAN_USE_DUMMY_STATEMENT STD_OFF /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef CAN_DUMMY_STATEMENT
#define CAN_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef CAN_DUMMY_STATEMENT_CONST
#define CAN_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef CAN_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define CAN_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef CAN_ATOMIC_VARIABLE_ACCESS
#define CAN_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef CAN_PROCESSOR_MPC5746C
#define CAN_PROCESSOR_MPC5746C
#endif
#ifndef CAN_COMP_DIAB
#define CAN_COMP_DIAB
#endif
#ifndef CAN_GEN_GENERATOR_MSR
#define CAN_GEN_GENERATOR_MSR
#endif
#ifndef CAN_CPUTYPE_BITORDER_MSB2LSB
#define CAN_CPUTYPE_BITORDER_MSB2LSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef CAN_CONFIGURATION_VARIANT_PRECOMPILE
#define CAN_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef CAN_CONFIGURATION_VARIANT_LINKTIME
#define CAN_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef CAN_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define CAN_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef CAN_CONFIGURATION_VARIANT
#define CAN_CONFIGURATION_VARIANT CAN_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef CAN_POSTBUILD_VARIANT_SUPPORT
#define CAN_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif


/* -----------------------------------------------------------------------------
    General Switches from old v_cfg.h
 ----------------------------------------------------------------------------- */
#if !defined(V_GEN_GENERATOR5)
# define V_GEN_GENERATOR5 /* need by LL */
#endif
#if !defined(V_ENABLE_CAN_ASR_ABSTRACTION)
# define V_ENABLE_CAN_ASR_ABSTRACTION /* ATK */
#endif
#define CAN_GEN_COM_STACK_LIB

#if !defined( V_OSTYPE_AUTOSAR )
# define V_OSTYPE_AUTOSAR
#endif

#if (CPU_TYPE == CPU_TYPE_32)
# if !defined( C_CPUTYPE_32BIT )
#  define C_CPUTYPE_32BIT
# endif
#endif
#if (CPU_TYPE == CPU_TYPE_16)
# if !defined( C_CPUTYPE_16BIT )
#  define C_CPUTYPE_16BIT
# endif
#endif
#if (CPU_TYPE == CPU_TYPE_8)
# if !defined( C_CPUTYPE_8BIT )
#  define C_CPUTYPE_8BIT
# endif
#endif
#if (CPU_BIT_ORDER == LSB_FIRST)
# if !defined( C_CPUTYPE_BITORDER_LSB2MSB )
#  define C_CPUTYPE_BITORDER_LSB2MSB
# endif
#endif
#if (CPU_BIT_ORDER == MSB_FIRST)
# if !defined( C_CPUTYPE_BITORDER_MSB2LSB )
#  define C_CPUTYPE_BITORDER_MSB2LSB
# endif
#endif
#if (CPU_BYTE_ORDER == LOW_BYTE_FIRST)
# if !defined( C_CPUTYPE_LITTLEENDIAN )
#  define C_CPUTYPE_LITTLEENDIAN
# endif
#endif
#if (CPU_BYTE_ORDER == HIGH_BYTE_FIRST)
# if !defined( C_CPUTYPE_BIGENDIAN )
#  define C_CPUTYPE_BIGENDIAN
# endif
#endif

#if !defined( V_DISABLE_USE_DUMMY_FUNCTIONS )
# define V_DISABLE_USE_DUMMY_FUNCTIONS
#endif
#if !defined( V_DISABLE_USE_DUMMY_STATEMENT )
# define V_DISABLE_USE_DUMMY_STATEMENT
#endif

#if !defined( V_CPU_MPC5700 )
# define V_CPU_MPC5700
#endif

#if !defined( C_PROCESSOR_MPC5746C )
# define C_PROCESSOR_MPC5746C
#endif
#if !defined( V_PROCESSOR_MPC5746C )
# define V_PROCESSOR_MPC5746C
#endif

#if !defined( C_COMP_DIABDATA_MPC5700_FLEXCAN3 )
#define C_COMP_DIABDATA_MPC5700_FLEXCAN3
#endif
#if !defined( V_COMP_DIABDATA )
# define V_COMP_DIABDATA
#endif
#if !defined( V_COMP_DIABDATA_MPC5700 )
# define V_COMP_DIABDATA_MPC5700
#endif

#if !defined( V_SUPPRESS_EXTENDED_VERSION_CHECK )
# define V_SUPPRESS_EXTENDED_VERSION_CHECK
#endif

/* -----------------------------------------------------------------------------
    Version defines
 ----------------------------------------------------------------------------- */

/* CAN024, CAN023 */
#define CAN_ASR_VERSION              0x0400u
#define CAN_GEN_BASE_CFG5_VERSION    0x0103u
#define CAN_GEN_BASESASR_VERSION     0x0407u
#define CAN_GEN_ImxFlexcan3Asr_VERSION              0x0101u
#define CAN_MICROSAR_VERSION         CAN_MSR403

/* -----------------------------------------------------------------------------
    Hardcoded defines
 ----------------------------------------------------------------------------- */

#define CAN_INSTANCE_ID           0

#define CAN_RX_BASICCAN_TYPE                 0u
#define CAN_RX_FULLCAN_TYPE                  1u
#define CAN_TX_BASICCAN_TYPE                 2u
#define CAN_TX_FULLCAN_TYPE                  3u
#define CAN_UNUSED_CAN_TYPE                  4u
#define CAN_TX_BASICCAN_MUX_TYPE             5u
#define CAN_TX_BASICCAN_FIFO_TYPE            6u

#define CAN_INTERRUPT                        0u
#define CAN_POLLING                          1u

#define kCanChannelNotUsed                     CAN_NO_CANIFCHANNELID 

#define CAN_NONE                             0u
/* RAM check (also  none) */
#define CAN_NOTIFY_ISSUE                     1u
#define CAN_NOTIFY_MAILBOX                   2u
#define CAN_EXTENDED                         3u
/* Interrupt lock (also  none) */
#define CAN_DRIVER                           1u
#define CAN_APPL                             2u
#define CAN_BOTH                             3u
/* Overrun Notification (als none,appl) */
#define CAN_DET                              1u
/* CAN FD Support */
#define CAN_BRS                              1u
#define CAN_FULL                             2u
/* CAN FD Configuration */
#define CAN_FD_RXONLY                        2u /* FD Baudrate exist (RX) */
#define CAN_FD_RXTX                          1u /* FD Baudrate also used for TX */
/* Generic Confirmation */
#define CAN_API1                             1u
#define CAN_API2                             2u

#define CAN_OS_TICK2MS(tick)     OS_TICKS2MS_SystemTimer((tick))     /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#define CAN_OS_COUNTER_ID        SystemTimer

/* -----------------------------------------------------------------------------
    Defines / Switches
 ----------------------------------------------------------------------------- */

/* Version and Issue detection */
#define CAN_VERSION_INFO_API                 STD_OFF   /* CAN106_Conf */
#define CAN_DEV_ERROR_DETECT                 STD_OFF   /* CAN064_Conf */
#define CAN_DEV_ERROR_REPORT                 STD_OFF
#define CAN_SAFE_BSW                         STD_OFF

/* Interrupt / Polling */
#define CAN_TX_PROCESSING                    CAN_INTERRUPT   /* CAN318_Conf */
#define CAN_RX_PROCESSING                    CAN_INTERRUPT   /* CAN317_Conf */
#define CAN_BUSOFF_PROCESSING                CAN_POLLING   /* CAN314_Conf */
#define CAN_WAKEUP_PROCESSING                CAN_INTERRUPT   /* CAN319_Conf */
#define CAN_INDIVIDUAL_PROCESSING            STD_OFF
#define CAN_INTERRUPT_USED                   STD_ON
#define CAN_NESTED_INTERRUPTS                STD_OFF
#define C_ENABLE_OSEK_OS_INTCAT2
#define C_DISABLE_ISRVOID
#define CAN_INTLOCK                          CAN_DRIVER

/* Tx Handling */
#define CAN_MULTIPLEXED_TX_MAX               1u
#define CAN_CANCEL_SUPPORT_API               STD_ON
#define CAN_TRANSMIT_BUFFER                  STD_ON
#define CAN_MULTIPLEXED_TRANSMISSION         STD_OFF   /* CAN095_Conf */
#define CAN_TX_HW_FIFO                       STD_OFF
#define CAN_HW_TRANSMIT_CANCELLATION         STD_OFF   /* CAN069_Conf */
#define CAN_IDENTICAL_ID_CANCELLATION        STD_OFF   /* CAN378_Conf */
#define CAN_MULTIPLE_BASICCAN_TX             STD_OFF

/* Rx Handling */
#define CAN_MULTIPLE_BASICCAN                STD_OFF
#define CAN_RX_QUEUE                         STD_OFF
#define CAN_OVERRUN_NOTIFICATION             CAN_NONE

/* Sleep Wakeup */
#define CAN_SLEEP_SUPPORT                    STD_OFF
#define CAN_WAKEUP_SUPPORT                   STD_OFF   /* CAN330_Conf */

/* Hardware loop check */
#define CAN_HARDWARE_CANCELLATION            STD_ON
#define CAN_TIMEOUT_DURATION                 10uL   /* CAN113_Conf */
#define CAN_LOOP_MAX                         5u

/* Appl calls */
#define CAN_HW_LOOP_SUPPORT_API              STD_OFF
#define CAN_GENERIC_PRECOPY                  STD_OFF
#define CAN_GENERIC_CONFIRMATION             STD_OFF
#define CAN_GENERIC_PRETRANSMIT              STD_OFF
#define CAN_USE_OS_INTERRUPT_CONTROL         STD_ON

/* Optimization */
#define CAN_RX_FULLCAN_OBJECTS               STD_ON
#define CAN_TX_FULLCAN_OBJECTS               STD_ON
#define CAN_RX_BASICCAN_OBJECTS              STD_ON
#define CAN_EXTENDED_ID                      STD_ON
#define CAN_MIXED_ID                         STD_ON
#define CAN_ONE_CONTROLLER_OPTIMIZATION      STD_OFF
#define CAN_CHANGE_BAUDRATE_API              STD_OFF   /* CAN460_Conf */
#define CAN_FD_HW_BUFFER_OPTIMIZATION        STD_ON

/* CAN FD */
#define CAN_SET_BAUDRATE_API                 STD_OFF   /* CAN482_Conf */
#define CAN_FD_SUPPORT                       CAN_BRS

/* Other */
#define CAN_COMMON_CAN                       STD_OFF
#define CAN_RAM_CHECK                        CAN_NONE
#define CAN_REINIT_START                     STD_OFF
#define CAN_GET_STATUS                       STD_OFF
#define CAN_RUNTIME_MEASUREMENT_SUPPORT      STD_ON
#define CAN_PROTECTED_MODE                   STD_OFF
#define CAN_MIRROR_MODE                      STD_OFF
#define CAN_SILENT_MODE                      STD_OFF
#define CAN_CHECK_WAKEUP_CAN_RET_TYPE        STD_OFF
/* -----------------------------------------------------------------------------
    Channel And Mailbox
 ----------------------------------------------------------------------------- */
#ifndef C_DRV_INTERNAL
# ifndef kCanNumberOfChannels
#  define kCanNumberOfChannels               6u
# endif
# ifndef kCanNumberOfHwChannels
#  define kCanNumberOfHwChannels             6u
# endif
#endif
#ifndef kCanNumberOfUsedChannels /* ATK only */
# define kCanNumberOfUsedChannels            6u
#endif

#define kCanPhysToLogChannelIndex_0 2u
#define kCanPhysToLogChannelIndex_1 5u
#define kCanPhysToLogChannelIndex_2 4u
#define kCanPhysToLogChannelIndex_4 0u
#define kCanPhysToLogChannelIndex_6 3u
#define kCanPhysToLogChannelIndex_7 1u

#define kCanNumberOfPhysChannels             8u

/* -----------------------------------------------------------------------------
    Symbolic Name Values for Controller, HardwareObject and Baudrates
 ----------------------------------------------------------------------------- */
/* These definitions can change at Link-time and Post-build configuration time. Use them wisely. */



/**
 * \defgroup CanHandleIdsactivated Handle IDs of handle space activated.
 * \brief controllers by CanControllerActivation
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define CanConf_CanController_CT_Backbone1J1939_198bcf1c              0u
#define CanConf_CanController_CT_Backbone2_34cfe263                   1u
#define CanConf_CanController_CT_CAN6_120de18e                        2u
#define CanConf_CanController_CT_CabSubnet_d2ff0fbe                   3u
#define CanConf_CanController_CT_FMSNet_119a8706                      4u
#define CanConf_CanController_CT_SecuritySubnet_f5346ae6              5u
/**\} */
#define CanConf_CN_Backbone1J1939_0b1f4bae_Tx 4u
#define CanConf_CN_Backbone1J1939_6e8ec372_1_Rx 5u
#define CanConf_CN_Backbone1J1939_6e8ec372_Rx 6u
#define CanConf_CN_Backbone2_78967e2c_Tx 26u
#define CanConf_CN_Backbone2_7b3866ec_1_Rx 80u
#define CanConf_CN_CAN6_20078678_1_Rx 86u
#define CanConf_CN_CAN6_b040c073_Tx 84u
#define CanConf_CN_CabSubnet_9ea693f1_Tx 101u
#define CanConf_CN_CabSubnet_eedbadff_1_Rx 155u
#define CanConf_CN_FMSNet_41e51372_1_Rx 166u
#define CanConf_CN_FMSNet_fce1aae5_Tx 164u
#define CanConf_CN_SecuritySubnet_3840872e_1_Rx 225u
#define CanConf_CN_SecuritySubnet_e7a0ee54_Tx 185u
#define CanConf_CanHardwareObject 154u
#define CanConf_CanHardwareObject_001 224u
#define CanConf_CanHardwareObject_002 165u
#define CanConf_CanHardwareObject_003 85u
#define CanConf_CanHardwareObject_004 79u
#define CanConf_CanHardwareObject_AckmTxPdu_Backbone1J1939_54966c1b 0u
#define CanConf_CanHardwareObject_Alarm_Sec_02P_oSecuritySubnet_a902993f_Rx 223u
#define CanConf_CanHardwareObject_Alarm_Sec_03S_Tp_oSecuritySubnet_838659b7_Rx 219u
#define CanConf_CanHardwareObject_Alarm_Sec_06S_Tp_oSecuritySubnet_2b24d209_Rx 211u
#define CanConf_CanHardwareObject_Alarm_Sec_07S_Tp_oSecuritySubnet_bf7e0710_Rx 209u
#define CanConf_CanHardwareObject_AnmMsg_Alarm_SecuritySubnet_oSecuritySubnet_38c19358_Rx 201u
#define CanConf_CanHardwareObject_AnmMsg_CCM_CabSubnet_oCabSubnet_7e592096_Rx 122u
#define CanConf_CanHardwareObject_AnmMsg_CIOM_Backbone2_oBackbone2_d362d40e_Tx 18u
#define CanConf_CanHardwareObject_AnmMsg_CIOM_CabSubnet_oCabSubnet_e295c8bd_Tx 100u
#define CanConf_CanHardwareObject_AnmMsg_CIOM_SecuritySubnet_oSecuritySubnet_d0267e59_Tx 173u
#define CanConf_CanHardwareObject_AnmMsg_DDM_SecuritySubnet_oSecuritySubnet_669221a0_Rx 200u
#define CanConf_CanHardwareObject_AnmMsg_ECUspare5_SecuritySubnet_oSecuritySubnet_25af973c_Rx 198u
#define CanConf_CanHardwareObject_AnmMsg_ECUspare6_CabSubnet_oCabSubnet_582cafe8_Rx 118u
#define CanConf_CanHardwareObject_AnmMsg_LECM1_CabSubnet_oCabSubnet_ba3b1140_Rx 121u
#define CanConf_CanHardwareObject_AnmMsg_PDM_SecuritySubnet_oSecuritySubnet_d922132b_Rx 199u
#define CanConf_CanHardwareObject_AnmMsg_SRS_CabSubnet_oCabSubnet_690513e9_Rx 120u
#define CanConf_CanHardwareObject_AnmMsg_WRCS_CabSubnet_oCabSubnet_62e3be6f_Rx 119u
#define CanConf_CanHardwareObject_BBM_BB2_01P_oBackbone2_0057383e_Rx 73u
#define CanConf_CanHardwareObject_BBM_BB2_03S_CIOM_Tp_oBackbone2_8a8d09f2_Rx 59u
#define CanConf_CanHardwareObject_CCM_Cab_01P_oCabSubnet_27c0fc91_Rx 152u
#define CanConf_CanHardwareObject_CCM_Cab_03P_Tp_oCabSubnet_bfb45635_Rx 153u
#define CanConf_CanHardwareObject_CCM_Cab_04P_oCabSubnet_4f3902ac_Rx 151u
#define CanConf_CanHardwareObject_CCM_Cab_06P_oCabSubnet_ae969834_Rx 148u
#define CanConf_CanHardwareObject_CCM_Cab_07P_oCabSubnet_de415578_Rx 149u
#define CanConf_CanHardwareObject_CCM_Cab_08P_oCabSubnet_674b573f_Rx 150u
#define CanConf_CanHardwareObject_CCVS_X_CIOMFMS_oFMSNet_1dbcfa70_Tx 162u
#define CanConf_CanHardwareObject_CIOM_BB1_01P_oBackbone1J1939_55a00301_Tx 2u
#define CanConf_CanHardwareObject_CIOM_BB2_01P_oBackbone2_379f1438_Tx 10u
#define CanConf_CanHardwareObject_CIOM_BB2_02P_oBackbone2_9fdb9738_Tx 8u
#define CanConf_CanHardwareObject_CIOM_BB2_03P_oBackbone2_f8181638_Tx 9u
#define CanConf_CanHardwareObject_CIOM_BB2_04P_oBackbone2_14239779_Tx 7u
#define CanConf_CanHardwareObject_CIOM_BB2_05P_oBackbone2_73e01679_Tx 12u
#define CanConf_CanHardwareObject_CIOM_BB2_06P_oBackbone2_dba49579_Tx 13u
#define CanConf_CanHardwareObject_CIOM_BB2_07P_oBackbone2_bc671479_Tx 16u
#define CanConf_CanHardwareObject_CIOM_BB2_10P_oBackbone2_503b56b9_Tx 14u
#define CanConf_CanHardwareObject_CIOM_BB2_11P_oBackbone2_37f8d7b9_Tx 15u
#define CanConf_CanHardwareObject_CIOM_BB2_22S_FCM_Tp_oBackbone2_7818b16f_Rx 62u
#define CanConf_CanHardwareObject_CIOM_BB2_28P_oBackbone2_d86d16b8_Tx 17u
#define CanConf_CanHardwareObject_CIOM_BB2_29P_oBackbone2_bfae97b8_Tx 11u
#define CanConf_CanHardwareObject_CIOM_BB2_30S_FCM_Tp_oBackbone2_d656145d_Rx 67u
#define CanConf_CanHardwareObject_CIOM_Cab_01P_oCabSubnet_c1019bea_Tx 89u
#define CanConf_CanHardwareObject_CIOM_Cab_02P_oCabSubnet_694518ea_Tx 87u
#define CanConf_CanHardwareObject_CIOM_Cab_03P_oCabSubnet_0e8699ea_Tx 90u
#define CanConf_CanHardwareObject_CIOM_Cab_04P_FCM_Tp_oCabSubnet_b91c0468_Rx 147u
#define CanConf_CanHardwareObject_CIOM_Cab_05P_FCM_Tp_oCabSubnet_fd75fa58_Rx 146u
#define CanConf_CanHardwareObject_CIOM_Cab_06P_oCabSubnet_2d3a1aab_Tx 91u
#define CanConf_CanHardwareObject_CIOM_Cab_09P_oCabSubnet_49ff9f68_Tx 93u
#define CanConf_CanHardwareObject_CIOM_Cab_10P_oCabSubnet_a6a5d96b_Tx 97u
#define CanConf_CanHardwareObject_CIOM_Cab_11S_FCM_Tp_oCabSubnet_a74b983f_Rx 135u
#define CanConf_CanHardwareObject_CIOM_Cab_13S_FCM_Tp_oCabSubnet_2f98645f_Rx 134u
#define CanConf_CanHardwareObject_CIOM_Cab_20S_FCM_Tp_oCabSubnet_88858df9_Rx 127u
#define CanConf_CanHardwareObject_CIOM_Cab_23P_oCabSubnet_0e491ee8_Tx 95u
#define CanConf_CanHardwareObject_CIOM_Cab_24P_oCabSubnet_e2729fa9_Tx 98u
#define CanConf_CanHardwareObject_CIOM_Cab_25S_FCM_Tp_oCabSubnet_063a8d48_Rx 132u
#define CanConf_CanHardwareObject_CIOM_Cab_26P_oCabSubnet_2df59da9_Tx 96u
#define CanConf_CanHardwareObject_CIOM_Cab_27P_oCabSubnet_4a361ca9_Tx 94u
#define CanConf_CanHardwareObject_CIOM_Cab_29S_FCM_Tp_oCabSubnet_8230888a_Rx 144u
#define CanConf_CanHardwareObject_CIOM_Cab_30S_FCM_Tp_oCabSubnet_ae18d4ab_Rx 131u
#define CanConf_CanHardwareObject_CIOM_Cab_31P_oCabSubnet_c1a9df69_Tx 99u
#define CanConf_CanHardwareObject_CIOM_Cab_32P_oCabSubnet_69ed5c69_Tx 88u
#define CanConf_CanHardwareObject_CIOM_Cab_33P_oCabSubnet_0e2edd69_Tx 92u
#define CanConf_CanHardwareObject_CIOM_Cab_34P_FCM_Tp_oCabSubnet_d2bbef9e_Rx 130u
#define CanConf_CanHardwareObject_CIOM_Sec_01P_oSecuritySubnet_de08418d_Tx 167u
#define CanConf_CanHardwareObject_CIOM_Sec_02P_oSecuritySubnet_04f0ad2a_Tx 172u
#define CanConf_CanHardwareObject_CIOM_Sec_03P_oSecuritySubnet_4d58f6b7_Tx 171u
#define CanConf_CanHardwareObject_CIOM_Sec_04P_oSecuritySubnet_6a707225_Tx 168u
#define CanConf_CanHardwareObject_CIOM_Sec_05S_FCM_Tp_oSecuritySubnet_02ea2344_Rx 206u
#define CanConf_CanHardwareObject_CIOM_Sec_06S_FCM_Tp_oSecuritySubnet_76df7446_Rx 218u
#define CanConf_CanHardwareObject_CIOM_Sec_07S_FCM_Tp_oSecuritySubnet_5acc46b8_Rx 214u
#define CanConf_CanHardwareObject_CIOM_Sec_08S_FCM_Tp_oSecuritySubnet_255d4bf3_Rx 220u
#define CanConf_CanHardwareObject_CIOM_Sec_09S_FCM_Tp_oSecuritySubnet_094e790d_Rx 217u
#define CanConf_CanHardwareObject_CIOM_Sec_10S_FCM_Tp_oSecuritySubnet_c49c166b_Rx 216u
#define CanConf_CanHardwareObject_CIOM_Sec_10S_Tp_oSecuritySubnet_8c7f36ae_Tx 169u
#define CanConf_CanHardwareObject_CIOM_Sec_11S_FCM_Tp_oSecuritySubnet_e88f2495_Rx 215u
#define CanConf_CanHardwareObject_CIOM_Sec_12S_FCM_Tp_oSecuritySubnet_9cba7397_Rx 213u
#define CanConf_CanHardwareObject_CIOM_Sec_12S_Tp_oSecuritySubnet_2d9a0e12_Tx 170u
#define CanConf_CanHardwareObject_DACU_BB2_02P_oBackbone2_0b8371c6_Rx 76u
#define CanConf_CanHardwareObject_DDM_Sec_01P_oSecuritySubnet_09e2b30b_Rx 222u
#define CanConf_CanHardwareObject_DDM_Sec_03S_Tp_oSecuritySubnet_b8db9c2b_Rx 212u
#define CanConf_CanHardwareObject_DDM_Sec_04S_Tp_oSecuritySubnet_c3efbadc_Rx 210u
#define CanConf_CanHardwareObject_DDM_Sec_05S_Tp_oSecuritySubnet_5a1ed97b_Rx 208u
#define CanConf_CanHardwareObject_Debug02_CIOM_BB2_oBackbone2_423c0750_Tx 19u
#define CanConf_CanHardwareObject_Debug03_CIOM_BB2_oBackbone2_2d143e88_Tx 20u
#define CanConf_CanHardwareObject_Debug04_CIOM_BB2_oBackbone2_fbbd95c1_Tx 21u
#define CanConf_CanHardwareObject_Debug05_CIOM_BB2_oBackbone2_9495ac19_Tx 22u
#define CanConf_CanHardwareObject_Debug06_CIOM_BB2_oBackbone2_25ede671_Tx 23u
#define CanConf_CanHardwareObject_Debug07_CIOM_BB2_oBackbone2_4ac5dfa9_Tx 24u
#define CanConf_CanHardwareObject_DebugCtrl1_CIOM_BB2_oBackbone2_ca351e0d_Rx 41u
#define CanConf_CanHardwareObject_DiagFaultStat_Alarm_Sec_oSecuritySubnet_6cedaf7c_Rx 204u
#define CanConf_CanHardwareObject_DiagFaultStat_CCM_Cab_oCabSubnet_2b754d89_Rx 126u
#define CanConf_CanHardwareObject_DiagFaultStat_DDM_Sec_oSecuritySubnet_3b5b41e4_Rx 203u
#define CanConf_CanHardwareObject_DiagFaultStat_LECM_Cab_oCabSubnet_6e0076c7_Rx 125u
#define CanConf_CanHardwareObject_DiagFaultStat_PDM_Sec_oSecuritySubnet_b17b40b8_Rx 202u
#define CanConf_CanHardwareObject_DiagFaultStat_SRS_Cab_oCabSubnet_e5f44b73_Rx 124u
#define CanConf_CanHardwareObject_DiagFaultStat_WRCS_Cab_oCabSubnet_b86e4ab7_Rx 123u
#define CanConf_CanHardwareObject_DiagReqMsgIntHMIIOM_26_F3_BB2_Tp_oBackbone2_3a21aae6_Rx 39u
#define CanConf_CanHardwareObject_DiagReqMsgIntHMIIOM_A0_F3_BB2_Tp_oBackbone2_3d55549e_Rx 36u
#define CanConf_CanHardwareObject_DiagReqMsgIntHMIIOM_A0_F3_Sec_Tp_oSecuritySubnet_09265670_Tx 175u
#define CanConf_CanHardwareObject_DiagReqMsgIntHMIIOM_A1_F3_BB2_Tp_oBackbone2_ba062351_Rx 33u
#define CanConf_CanHardwareObject_DiagReqMsgIntHMIIOM_A1_F3_Sec_Tp_oSecuritySubnet_a1b03b66_Tx 178u
#define CanConf_CanHardwareObject_DiagReqMsgIntHMIIOM_A2_F3_BB2_Tp_oBackbone2_e882bd41_Rx 30u
#define CanConf_CanHardwareObject_DiagReqMsgIntHMIIOM_C0_F3_Sec_Tp_oSecuritySubnet_3aaf1649_Tx 181u
#define CanConf_CanHardwareObject_DiagReqMsgIntTGW2_26_F4_BB2_Tp_oBackbone2_ce56ef15_Rx 38u
#define CanConf_CanHardwareObject_DiagReqMsgIntTGW2_A0_F4_BB2_Tp_oBackbone2_9c9945bf_Rx 35u
#define CanConf_CanHardwareObject_DiagReqMsgIntTGW2_A0_F4_Sec_Tp_oSecuritySubnet_8adef0bc_Tx 176u
#define CanConf_CanHardwareObject_DiagReqMsgIntTGW2_A1_F4_BB2_Tp_oBackbone2_91dc1a3b_Rx 32u
#define CanConf_CanHardwareObject_DiagReqMsgIntTGW2_A1_F4_Sec_Tp_oSecuritySubnet_eb89cc56_Tx 179u
#define CanConf_CanHardwareObject_DiagReqMsgIntTGW2_A2_F4_BB2_Tp_oBackbone2_8613fab7_Rx 29u
#define CanConf_CanHardwareObject_DiagReqMsgIntTGW2_C0_F4_Sec_Tp_oSecuritySubnet_0ba24808_Tx 182u
#define CanConf_CanHardwareObject_DiagRespMsgIntHMIIOM_F3_26_Cab_Tp_oCabSubnet_68d46304_Rx 113u
#define CanConf_CanHardwareObject_DiagRespMsgIntHMIIOM_F3_53_Cab_Tp_oCabSubnet_507b9b04_Rx 112u
#define CanConf_CanHardwareObject_DiagRespMsgIntHMIIOM_F3_98_Cab_Tp_oCabSubnet_b05496ae_Rx 111u
#define CanConf_CanHardwareObject_DiagRespMsgIntHMIIOM_F3_A0_Sec_Tp_oSecuritySubnet_95b29252_Rx 194u
#define CanConf_CanHardwareObject_DiagRespMsgIntHMIIOM_F3_A1_Sec_Tp_oSecuritySubnet_052f8796_Rx 193u
#define CanConf_CanHardwareObject_DiagRespMsgIntHMIIOM_F3_A2_Cab_Tp_oCabSubnet_15c35ee3_Rx 110u
#define CanConf_CanHardwareObject_DiagRespMsgIntHMIIOM_F3_C0_Sec_Tp_oSecuritySubnet_77f0a36b_Rx 192u
#define CanConf_CanHardwareObject_DiagRespMsgIntTGW2_F4_26_Cab_Tp_oCabSubnet_0b131bbc_Rx 109u
#define CanConf_CanHardwareObject_DiagRespMsgIntTGW2_F4_53_Cab_Tp_oCabSubnet_781e6a27_Rx 108u
#define CanConf_CanHardwareObject_DiagRespMsgIntTGW2_F4_98_Cab_Tp_oCabSubnet_d506a20a_Rx 107u
#define CanConf_CanHardwareObject_DiagRespMsgIntTGW2_F4_A0_Sec_Tp_oSecuritySubnet_d9469eac_Rx 191u
#define CanConf_CanHardwareObject_DiagRespMsgIntTGW2_F4_A1_Sec_Tp_oSecuritySubnet_efa76b9d_Rx 190u
#define CanConf_CanHardwareObject_DiagRespMsgIntTGW2_F4_A2_Cab_Tp_oCabSubnet_748ab027_Rx 106u
#define CanConf_CanHardwareObject_DiagRespMsgIntTGW2_F4_C0_Sec_Tp_oSecuritySubnet_7a975d32_Rx 189u
#define CanConf_CanHardwareObject_DiagUUDTRespMsg1_F2_26_Cab_oCabSubnet_8c457eb7_Rx 105u
#define CanConf_CanHardwareObject_DiagUUDTRespMsg1_F2_53_Cab_oCabSubnet_d5110ca2_Rx 104u
#define CanConf_CanHardwareObject_DiagUUDTRespMsg1_F2_98_Cab_oCabSubnet_2175e3c9_Rx 103u
#define CanConf_CanHardwareObject_DiagUUDTRespMsg1_F2_A0_Sec_oSecuritySubnet_46825d6a_Rx 188u
#define CanConf_CanHardwareObject_DiagUUDTRespMsg1_F2_A1_Sec_oSecuritySubnet_67c5eccf_Rx 187u
#define CanConf_CanHardwareObject_DiagUUDTRespMsg1_F2_A2_Cab_oCabSubnet_faf51f79_Rx 102u
#define CanConf_CanHardwareObject_DiagUUDTRespMsg1_F2_C0_Sec_oSecuritySubnet_d0c97a87_Rx 186u
#define CanConf_CanHardwareObject_EEC1_X_CIOMFMS_oFMSNet_954d78de_Tx 157u
#define CanConf_CanHardwareObject_EEC2_X_CIOMFMS_oFMSNet_d3e60e6a_Tx 156u
#define CanConf_CanHardwareObject_EMS_BB2_04P_oBackbone2_f5214579_Rx 55u
#define CanConf_CanHardwareObject_EMS_BB2_05P_oBackbone2_85f68835_Rx 57u
#define CanConf_CanHardwareObject_EMS_BB2_06P_oBackbone2_148edfe1_Rx 50u
#define CanConf_CanHardwareObject_EMS_BB2_08P_oBackbone2_dd5310ea_Rx 49u
#define CanConf_CanHardwareObject_EMS_BB2_09S_Tp_oBackbone2_ef0c9958_Rx 54u
#define CanConf_CanHardwareObject_EMS_BB2_13P_oBackbone2_036dfbaa_Rx 56u
#define CanConf_CanHardwareObject_ERC1_x_EMSRetFMS_oFMSNet_d863a207_Tx 159u
#define CanConf_CanHardwareObject_ERC1_x_RECUFMS_oFMSNet_338e7918_Tx 160u
#define CanConf_CanHardwareObject_FcNPdu_Backbone1J1939_dba64907 1u
#define CanConf_CanHardwareObject_HMIIOM_BB2_03P_oBackbone2_7d4615c2_Rx 71u
#define CanConf_CanHardwareObject_HMIIOM_BB2_05P_oBackbone2_e7151824_Rx 58u
#define CanConf_CanHardwareObject_HMIIOM_BB2_06S_Tp_oBackbone2_2c2d06cb_Rx 66u
#define CanConf_CanHardwareObject_HMIIOM_BB2_09P_oBackbone2_08c205a9_Rx 63u
#define CanConf_CanHardwareObject_HMIIOM_BB2_11P_oBackbone2_17206880_Rx 46u
#define CanConf_CanHardwareObject_HMIIOM_BB2_12P_oBackbone2_5a09ee73_Rx 45u
#define CanConf_CanHardwareObject_HMIIOM_BB2_15P_oBackbone2_fb4261c4_Rx 43u
#define CanConf_CanHardwareObject_HMIIOM_BB2_18P_oBackbone2_2f8dfe18_Rx 42u
#define CanConf_CanHardwareObject_HMIIOM_BB2_19P_CIOM_Tp_oBackbone2_470ca910_Rx 51u
#define CanConf_CanHardwareObject_HMIIOM_BB2_23P_oBackbone2_45e8e602_Rx 61u
#define CanConf_CanHardwareObject_HMIIOM_BB2_27S_Tp_oBackbone2_aeede795_Rx 48u
#define CanConf_CanHardwareObject_HMIIOM_BB2_33P_oBackbone2_59bf9fe2_Rx 70u
#define CanConf_CanHardwareObject_HMIIOM_BB2_34S_oBackbone2_6313bb35_Rx 64u
#define CanConf_CanHardwareObject_HMIIOM_BB2_35S_Tp_oBackbone2_61dbf5f7_Rx 65u
#define CanConf_CanHardwareObject_HMIIOM_BB2_38P_oBackbone2_17230dd8_Rx 44u
#define CanConf_CanHardwareObject_IntTesterTGW2FuncDiagMsg_BB2_Tp_oBackbone2_c6fcfce9_Rx 27u
#define CanConf_CanHardwareObject_IntTesterTGW2FuncDiagMsg_Sec_Tp_oSecuritySubnet_57d96404_Tx 184u
#define CanConf_CanHardwareObject_J1939NmTxPdu_fa509995 3u
#define CanConf_CanHardwareObject_LECM1_Cab_02P_oCabSubnet_e0d2bc45_Rx 142u
#define CanConf_CanHardwareObject_LECM1_Cab_03S_oCabSubnet_e20ca5cb_Rx 133u
#define CanConf_CanHardwareObject_LECM1_Cab_04P_oCabSubnet_b355ee77_Rx 141u
#define CanConf_CanHardwareObject_LECM1_Cab_05S_oCabSubnet_b18bf7f9_Rx 139u
#define CanConf_CanHardwareObject_LFE_X_CIOMFMS_oFMSNet_adac24c8_Tx 163u
#define CanConf_CanHardwareObject_PDM_Sec_01P_oSecuritySubnet_073fa36a_Rx 221u
#define CanConf_CanHardwareObject_PDM_Sec_03S_Tp_oSecuritySubnet_225f8534_Rx 207u
#define CanConf_CanHardwareObject_PDM_Sec_04S_Tp_oSecuritySubnet_596ba3c3_Rx 205u
#define CanConf_CanHardwareObject_PTODE_X_CIOMFMS_oFMSNet_8abc23b8_Tx 161u
#define CanConf_CanHardwareObject_PhysDiagReqMsg_26_F2_BB2_Tp_oBackbone2_680a80cc_Rx 40u
#define CanConf_CanHardwareObject_PhysDiagReqMsg_A0_F2_BB2_Tp_oBackbone2_b0139fa8_Rx 37u
#define CanConf_CanHardwareObject_PhysDiagReqMsg_A0_F2_Sec_Tp_oSecuritySubnet_cb87d97b_Tx 174u
#define CanConf_CanHardwareObject_PhysDiagReqMsg_A1_F2_BB2_Tp_oBackbone2_9bf67e6d_Rx 34u
#define CanConf_CanHardwareObject_PhysDiagReqMsg_A1_F2_Sec_Tp_oSecuritySubnet_5d2e792a_Tx 177u
#define CanConf_CanHardwareObject_PhysDiagReqMsg_A2_F2_BB2_Tp_oBackbone2_e7d85c22_Rx 31u
#define CanConf_CanHardwareObject_PhysDiagReqMsg_C0_F2_Sec_Tp_oSecuritySubnet_f27248ff_Tx 180u
#define CanConf_CanHardwareObject_PhysDiagRespMsg_F1_53_Cab_Tp_oCabSubnet_1d55a011_Rx 136u
#define CanConf_CanHardwareObject_PhysDiagRespMsg_F2_26_Cab_Tp_oCabSubnet_48eedf32_Rx 117u
#define CanConf_CanHardwareObject_PhysDiagRespMsg_F2_40_BB2_Tp_oBackbone2_bd4d3e52_Tx 25u
#define CanConf_CanHardwareObject_PhysDiagRespMsg_F2_53_Cab_Tp_oCabSubnet_4c48e914_Rx 116u
#define CanConf_CanHardwareObject_PhysDiagRespMsg_F2_98_Cab_Tp_oCabSubnet_af4ee02a_Rx 115u
#define CanConf_CanHardwareObject_PhysDiagRespMsg_F2_A0_Sec_Tp_oSecuritySubnet_207d0e64_Rx 197u
#define CanConf_CanHardwareObject_PhysDiagRespMsg_F2_A1_Sec_Tp_oSecuritySubnet_d47cc691_Rx 196u
#define CanConf_CanHardwareObject_PhysDiagRespMsg_F2_A2_Cab_Tp_oCabSubnet_89846943_Rx 114u
#define CanConf_CanHardwareObject_PhysDiagRespMsg_F2_C0_Sec_Tp_oSecuritySubnet_614a0693_Rx 195u
#define CanConf_CanHardwareObject_SCIM_BB1toCAN6_oCAN6_cb3fc040_Tx 82u
#define CanConf_CanHardwareObject_SCIM_BB2toCAN6_oCAN6_adf5f68f_Tx 83u
#define CanConf_CanHardwareObject_SCIM_LINtoCAN6_oCAN6_6b855022_Tx 81u
#define CanConf_CanHardwareObject_SRS_Cab_01P_oCabSubnet_3796a617_Rx 143u
#define CanConf_CanHardwareObject_SRS_Cab_03P_oCabSubnet_d6393c8f_Rx 138u
#define CanConf_CanHardwareObject_SRS_Cab_04P_oCabSubnet_5f6f582a_Rx 137u
#define CanConf_CanHardwareObject_SRS_Cab_05P_oCabSubnet_2fb89566_Rx 129u
#define CanConf_CanHardwareObject_SRS_Cab_06P_oCabSubnet_bec0c2b2_Rx 128u
#define CanConf_CanHardwareObject_TCO1_X_CIOMFMS_oFMSNet_567baf3d_Tx 158u
#define CanConf_CanHardwareObject_TECU_BB2_06S_Tp_oBackbone2_0c47cb8e_Rx 47u
#define CanConf_CanHardwareObject_TesterFuncDiagMsg_BB2_Tp_oBackbone2_6ec04186_Rx 28u
#define CanConf_CanHardwareObject_TesterFuncDiagMsg_Sec_Tp_oSecuritySubnet_7f5a7347_Tx 183u
#define CanConf_CanHardwareObject_VMCU_BB2_02P_oBackbone2_4ec96099_Rx 78u
#define CanConf_CanHardwareObject_VMCU_BB2_04P_oBackbone2_c53160d8_Rx 75u
#define CanConf_CanHardwareObject_VMCU_BB2_05P_oBackbone2_a2f2e1d8_Rx 74u
#define CanConf_CanHardwareObject_VMCU_BB2_31S_Tp_oBackbone2_210b49cb_Rx 52u
#define CanConf_CanHardwareObject_VMCU_BB2_32S_Tp_oBackbone2_2c992e71_Rx 53u
#define CanConf_CanHardwareObject_VMCU_BB2_52P_oBackbone2_4f31ad1c_Rx 72u
#define CanConf_CanHardwareObject_VMCU_BB2_53P_oBackbone2_28f22c1c_Rx 69u
#define CanConf_CanHardwareObject_VMCU_BB2_54P_oBackbone2_c4c9ad5d_Rx 68u
#define CanConf_CanHardwareObject_VMCU_BB2_55P_oBackbone2_a30a2c5d_Rx 60u
#define CanConf_CanHardwareObject_VMCU_BB2_82P_oBackbone2_4df77c91_Rx 77u
#define CanConf_CanHardwareObject_WRCS_Cab_02P_oCabSubnet_506736c4_Rx 145u
#define CanConf_CanHardwareObject_WRCS_Cab_03P_oCabSubnet_37a4b7c4_Rx 140u

#define CanConf_ControllerBaudrateConfig_CT_Backbone1J1939_198bcf1c_CanControllerBaudrateConfig 0u
#define CanConf_ControllerBaudrateConfig_CT_Backbone2_34cfe263_CanControllerBaudrateConfig 0u
#define CanConf_ControllerBaudrateConfig_CT_CAN6_120de18e_CanControllerBaudrateConfig 0u
#define CanConf_ControllerBaudrateConfig_CT_CabSubnet_d2ff0fbe_CanControllerBaudrateConfig 0u
#define CanConf_ControllerBaudrateConfig_CT_FMSNet_119a8706_CanControllerBaudrateConfig 0u
#define CanConf_ControllerBaudrateConfig_CT_SecuritySubnet_f5346ae6_CanControllerBaudrateConfig 0u

#define CanConf_MemorySection_Memory_0 0u
#define CanConf_MemorySection_Memory_1 1u
#define CanConf_MemorySection_Memory_2 2u
#define CanConf_MemorySection_Memory_3 3u
#define CanConf_MemorySection_Memory_4 4u
#define CanConf_MemorySection_Memory_5 5u



/* -----------------------------------------------------------------------------
    Types
 ----------------------------------------------------------------------------- */
/* HW specific BEGIN */
/* Hw specific defines */
#define C_DISABLE_TASD
#define C_DISABLE_GLITCH_FILTER
#define C_DISABLE_FLEXCAN_STOP_MODE
#define C_DISABLE_FLEXCAN_PARITY_CHECK_AVAILABLE
#define C_ENABLE_MB32TO63
#define C_ENABLE_MB64TO95
#define C_DISABLE_MB96TO127
#define C_DISABLE_ASYM_MAILBOXES
#define C_DISABLE_INTERRUPT_SOURCE_SINGLE
#define C_ENABLE_INTERRUPT_SOURCE_MULTIPLE

#if !defined( C_ENABLE_EXTENDED_BITTIMING ) && !defined( C_DISABLE_EXTENDED_BITTIMING )
# define C_ENABLE_EXTENDED_BITTIMING
#endif

#define C_DISABLE_WORKAROUND_ERR005829

#define C_DISABLE_WORKAROUND_ERR010368

#define C_ENABLE_ISO_CANFD

typedef struct tCanLLCanIntOldTag
{
  uint32 flags1;
  uint32 flags2;
  uint32 flags3;
  uint32 canctrl1;
  uint16 canmcr;
} tCanLLCanIntOld;

typedef struct tCanRxMsgBufferTag
{
  uint16 control;
  uint16 timestamp;
  uint32 msgID;
  uint32 data[2];
} tCanRxMsgBuffer;
 
/* HW specific END */ 

typedef VAR(uint16, TYPEDEF) CanChannelHandle;

#if defined (CAN_LCFG_SOURCE) || defined (C_DRV_INTERNAL)
typedef TickType Can_ExternalTickType;
#else
typedef uint32 Can_ExternalTickType;
#endif

typedef VAR(Can_ExternalTickType, TYPEDEF) Can_LoopTimeout_dim_type[CAN_LOOP_MAX+1u];




/* -----------------------------------------------------------------------------
    CONST Declarations
 ----------------------------------------------------------------------------- */

/* -----------------------------------------------------------------------------
    VAR Declarations
 ----------------------------------------------------------------------------- */

/* -----------------------------------------------------------------------------
    Hw specific
 ----------------------------------------------------------------------------- */
/* HW specific BEGIN */

/* HW specific END */


/**********************************************************************************************************************
  ComStackLib
**********************************************************************************************************************/
/* Can_GlobalConfig: CAN354_Conf */
/* Can_ConfigType: CAN413 */

/**********************************************************************************************************************
  GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  CanPCDataSwitches  Can Data Switches  (PRE_COMPILE)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define CAN_ACTIVESENDOBJECT                                          STD_ON
#define CAN_PDUOFACTIVESENDOBJECT                                     STD_ON
#define CAN_STATEOFACTIVESENDOBJECT                                   STD_ON
#define CAN_BASEDLL_GENERATORVERSION                                  STD_ON
#define CAN_CANIFCHANNELID                                            STD_ON
#define CAN_CONTROLLERCONFIG                                          STD_ON
#define CAN_BASEADDRESSOFCONTROLLERCONFIG                             STD_ON
#define CAN_CANCONTROLLERDEFAULTBAUDRATEIDXOFCONTROLLERCONFIG         STD_ON
#define CAN_CANCONTROLLERDEFAULTBAUDRATEOFCONTROLLERCONFIG            STD_ON
#define CAN_HASCANFDBAUDRATEOFCONTROLLERCONFIG                        STD_ON
#define CAN_INTERRUPTMASK1OFCONTROLLERCONFIG                          STD_ON
#define CAN_INTERRUPTMASK2OFCONTROLLERCONFIG                          STD_ON
#define CAN_INTERRUPTMASK3OFCONTROLLERCONFIG                          STD_ON
#define CAN_MAILBOXRXBASICENDIDXOFCONTROLLERCONFIG                    STD_ON
#define CAN_MAILBOXRXBASICLENGTHOFCONTROLLERCONFIG                    STD_ON
#define CAN_MAILBOXRXBASICSTARTIDXOFCONTROLLERCONFIG                  STD_ON
#define CAN_MAILBOXRXBASICUSEDOFCONTROLLERCONFIG                      STD_ON
#define CAN_MAILBOXRXFULLENDIDXOFCONTROLLERCONFIG                     STD_ON
#define CAN_MAILBOXRXFULLLENGTHOFCONTROLLERCONFIG                     STD_ON
#define CAN_MAILBOXRXFULLSTARTIDXOFCONTROLLERCONFIG                   STD_ON
#define CAN_MAILBOXRXFULLUSEDOFCONTROLLERCONFIG                       STD_ON
#define CAN_MAILBOXTXBASICENDIDXOFCONTROLLERCONFIG                    STD_ON
#define CAN_MAILBOXTXBASICLENGTHOFCONTROLLERCONFIG                    STD_ON
#define CAN_MAILBOXTXBASICSTARTIDXOFCONTROLLERCONFIG                  STD_ON
#define CAN_MAILBOXTXBASICUSEDOFCONTROLLERCONFIG                      STD_ON
#define CAN_MAILBOXTXFULLENDIDXOFCONTROLLERCONFIG                     STD_ON
#define CAN_MAILBOXTXFULLLENGTHOFCONTROLLERCONFIG                     STD_ON
#define CAN_MAILBOXTXFULLSTARTIDXOFCONTROLLERCONFIG                   STD_ON
#define CAN_MAILBOXTXFULLUSEDOFCONTROLLERCONFIG                       STD_ON
#define CAN_MAILBOXUNUSEDENDIDXOFCONTROLLERCONFIG                     STD_OFF  /**< Deactivateable: 'Can_ControllerConfig.MailboxUnusedEndIdx' Reason: 'the optional indirection is deactivated because MailboxUnusedUsedOfControllerConfig is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define CAN_MAILBOXUNUSEDLENGTHOFCONTROLLERCONFIG                     STD_OFF  /**< Deactivateable: 'Can_ControllerConfig.MailboxUnusedLength' Reason: 'the optional indirection is deactivated because MailboxUnusedUsedOfControllerConfig is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define CAN_MAILBOXUNUSEDSTARTIDXOFCONTROLLERCONFIG                   STD_OFF  /**< Deactivateable: 'Can_ControllerConfig.MailboxUnusedStartIdx' Reason: 'the optional indirection is deactivated because MailboxUnusedUsedOfControllerConfig is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define CAN_MAILBOXUNUSEDUSEDOFCONTROLLERCONFIG                       STD_OFF  /**< Deactivateable: 'Can_ControllerConfig.MailboxUnusedUsed' Reason: 'the optional indirection is deactivated because MailboxUnusedUsedOfControllerConfig is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define CAN_NUMBEROFFILTERSOFCONTROLLERCONFIG                         STD_ON
#define CAN_NUMBEROFFULLCONFIGURABLEFILTERSOFCONTROLLERCONFIG         STD_ON
#define CAN_NUMBEROFMAXMAILBOXESOFCONTROLLERCONFIG                    STD_ON
#define CAN_RFFNOFCONTROLLERCONFIG                                    STD_ON
#define CAN_RXBASICHWSTARTOFCONTROLLERCONFIG                          STD_ON
#define CAN_RXBASICHWSTOPOFCONTROLLERCONFIG                           STD_ON
#define CAN_RXFULLHWSTARTOFCONTROLLERCONFIG                           STD_ON
#define CAN_RXFULLHWSTOPOFCONTROLLERCONFIG                            STD_ON
#define CAN_TXBASICHWSTARTOFCONTROLLERCONFIG                          STD_ON
#define CAN_TXBASICHWSTOPOFCONTROLLERCONFIG                           STD_ON
#define CAN_TXFULLHWSTARTOFCONTROLLERCONFIG                           STD_ON
#define CAN_TXFULLHWSTOPOFCONTROLLERCONFIG                            STD_ON
#define CAN_UNUSEDHWSTARTOFCONTROLLERCONFIG                           STD_ON
#define CAN_UNUSEDHWSTOPOFCONTROLLERCONFIG                            STD_ON
#define CAN_CONTROLLERDATA                                            STD_ON
#define CAN_BUSOFFCOUNTEROFCONTROLLERDATA                             STD_ON
#define CAN_BUSOFFTRANSITIONREQUESTOFCONTROLLERDATA                   STD_ON
#define CAN_CANINTERRUPTCOUNTEROFCONTROLLERDATA                       STD_ON
#define CAN_CANINTERRUPTOLDSTATUSOFCONTROLLERDATA                     STD_ON
#define CAN_ISBUSOFFOFCONTROLLERDATA                                  STD_ON
#define CAN_LASTINITOBJECTOFCONTROLLERDATA                            STD_ON
#define CAN_LOGSTATUSOFCONTROLLERDATA                                 STD_ON
#define CAN_LOOPTIMEOUTOFCONTROLLERDATA                               STD_ON
#define CAN_MODETRANSITIONREQUESTOFCONTROLLERDATA                     STD_ON
#define CAN_RAMCHECKTRANSITIONREQUESTOFCONTROLLERDATA                 STD_ON
#define CAN_RXMSGBUFFEROFCONTROLLERDATA                               STD_ON
#define CAN_STARTMODEREQUESTEDOFCONTROLLERDATA                        STD_ON
#define CAN_FINALMAGICNUMBER                                          STD_OFF  /**< Deactivateable: 'Can_FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define CAN_INITBASICCAN                                              STD_ON
#define CAN_INITCODEOFINITBASICCAN                                    STD_ON
#define CAN_INITMASKOFINITBASICCAN                                    STD_ON
#define CAN_INITBASICCANINDEX                                         STD_ON
#define CAN_INITDATAHASHCODE                                          STD_OFF  /**< Deactivateable: 'Can_InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define CAN_INITOBJECT                                                STD_ON
#define CAN_CBTOFINITOBJECT                                           STD_ON
#define CAN_CONTROL1OFINITOBJECT                                      STD_ON
#define CAN_INITOBJECTBAUDRATE                                        STD_ON
#define CAN_INITOBJECTFD                                              STD_ON
#define CAN_FDCBTOFINITOBJECTFD                                       STD_ON
#define CAN_FDCTRLOFINITOBJECTFD                                      STD_ON
#define CAN_INITOBJECTFDBRSCONFIG                                     STD_ON
#define CAN_INITOBJECTSTARTINDEX                                      STD_ON
#define CAN_MAILBOX                                                   STD_ON
#define CAN_ACTIVESENDOBJECTOFMAILBOX                                 STD_ON
#define CAN_CONTROLLERCONFIGIDXOFMAILBOX                              STD_ON
#define CAN_HWHANDLEOFMAILBOX                                         STD_ON
#define CAN_IDVALUEOFMAILBOX                                          STD_ON
#define CAN_MAILBOXSIZEOFMAILBOX                                      STD_ON
#define CAN_MAILBOXTYPEOFMAILBOX                                      STD_ON
#define CAN_MAXDATALENOFMAILBOX                                       STD_ON
#define CAN_MEMORYSECTIONSINDEXOFMAILBOX                              STD_ON
#define CAN_MEMORYSECTIONINFO                                         STD_ON
#define CAN_MEMORYSECTIONSTARTOFMEMORYSECTIONINFO                     STD_ON
#define CAN_MEMORYSTARTADDRESSOFMEMORYSECTIONINFO                     STD_ON
#define CAN_MEMORYSECTIONOBJECTS                                      STD_ON
#define CAN_HWHANDLEOFMEMORYSECTIONOBJECTS                            STD_ON
#define CAN_MAILBOXELEMENTOFMEMORYSECTIONOBJECTS                      STD_ON
#define CAN_MAILBOXHANDLEOFMEMORYSECTIONOBJECTS                       STD_ON
#define CAN_PLATFORMDLL_GENERATORVERSION                              STD_ON
#define CAN_SIZEOFACTIVESENDOBJECT                                    STD_ON
#define CAN_SIZEOFCANIFCHANNELID                                      STD_ON
#define CAN_SIZEOFCONTROLLERCONFIG                                    STD_ON
#define CAN_SIZEOFCONTROLLERDATA                                      STD_ON
#define CAN_SIZEOFINITBASICCAN                                        STD_ON
#define CAN_SIZEOFINITBASICCANINDEX                                   STD_ON
#define CAN_SIZEOFINITOBJECT                                          STD_ON
#define CAN_SIZEOFINITOBJECTBAUDRATE                                  STD_ON
#define CAN_SIZEOFINITOBJECTFD                                        STD_ON
#define CAN_SIZEOFINITOBJECTFDBRSCONFIG                               STD_ON
#define CAN_SIZEOFINITOBJECTSTARTINDEX                                STD_ON
#define CAN_SIZEOFMAILBOX                                             STD_ON
#define CAN_SIZEOFMEMORYSECTIONINFO                                   STD_ON
#define CAN_SIZEOFMEMORYSECTIONOBJECTS                                STD_ON
#define CAN_PCCONFIG                                                  STD_ON
#define CAN_ACTIVESENDOBJECTOFPCCONFIG                                STD_ON
#define CAN_BASEDLL_GENERATORVERSIONOFPCCONFIG                        STD_ON
#define CAN_CANIFCHANNELIDOFPCCONFIG                                  STD_ON
#define CAN_CONTROLLERCONFIGOFPCCONFIG                                STD_ON
#define CAN_CONTROLLERDATAOFPCCONFIG                                  STD_ON
#define CAN_FINALMAGICNUMBEROFPCCONFIG                                STD_OFF  /**< Deactivateable: 'Can_PCConfig.FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define CAN_INITBASICCANINDEXOFPCCONFIG                               STD_ON
#define CAN_INITBASICCANOFPCCONFIG                                    STD_ON
#define CAN_INITDATAHASHCODEOFPCCONFIG                                STD_OFF  /**< Deactivateable: 'Can_PCConfig.InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define CAN_INITOBJECTBAUDRATEOFPCCONFIG                              STD_ON
#define CAN_INITOBJECTFDOFPCCONFIG                                    STD_ON
#define CAN_INITOBJECTFDBRSCONFIGOFPCCONFIG                           STD_ON
#define CAN_INITOBJECTOFPCCONFIG                                      STD_ON
#define CAN_INITOBJECTSTARTINDEXOFPCCONFIG                            STD_ON
#define CAN_MAILBOXOFPCCONFIG                                         STD_ON
#define CAN_MEMORYSECTIONINFOOFPCCONFIG                               STD_ON
#define CAN_MEMORYSECTIONOBJECTSOFPCCONFIG                            STD_ON
#define CAN_PLATFORMDLL_GENERATORVERSIONOFPCCONFIG                    STD_ON
#define CAN_SIZEOFACTIVESENDOBJECTOFPCCONFIG                          STD_ON
#define CAN_SIZEOFCANIFCHANNELIDOFPCCONFIG                            STD_ON
#define CAN_SIZEOFCONTROLLERCONFIGOFPCCONFIG                          STD_ON
#define CAN_SIZEOFCONTROLLERDATAOFPCCONFIG                            STD_ON
#define CAN_SIZEOFINITBASICCANINDEXOFPCCONFIG                         STD_ON
#define CAN_SIZEOFINITBASICCANOFPCCONFIG                              STD_ON
#define CAN_SIZEOFINITOBJECTBAUDRATEOFPCCONFIG                        STD_ON
#define CAN_SIZEOFINITOBJECTFDOFPCCONFIG                              STD_ON
#define CAN_SIZEOFINITOBJECTFDBRSCONFIGOFPCCONFIG                     STD_ON
#define CAN_SIZEOFINITOBJECTOFPCCONFIG                                STD_ON
#define CAN_SIZEOFINITOBJECTSTARTINDEXOFPCCONFIG                      STD_ON
#define CAN_SIZEOFMAILBOXOFPCCONFIG                                   STD_ON
#define CAN_SIZEOFMEMORYSECTIONINFOOFPCCONFIG                         STD_ON
#define CAN_SIZEOFMEMORYSECTIONOBJECTSOFPCCONFIG                      STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  CanPCMinNumericValueDefines  Can Min Numeric Value Defines (PRE_COMPILE)
  \brief  These defines are used to implement against the minimum value in numerical based data.
  \{
*/ 
#define CAN_MIN_PDUOFACTIVESENDOBJECT                                 0u
#define CAN_MIN_STATEOFACTIVESENDOBJECT                               0u
#define CAN_MIN_BUSOFFCOUNTEROFCONTROLLERDATA                         0u
#define CAN_MIN_BUSOFFTRANSITIONREQUESTOFCONTROLLERDATA               0u
#define CAN_MIN_CANINTERRUPTCOUNTEROFCONTROLLERDATA                   0u
#define CAN_MIN_LASTINITOBJECTOFCONTROLLERDATA                        0u
#define CAN_MIN_LOGSTATUSOFCONTROLLERDATA                             0u
#define CAN_MIN_MODETRANSITIONREQUESTOFCONTROLLERDATA                 0u
#define CAN_MIN_RAMCHECKTRANSITIONREQUESTOFCONTROLLERDATA             0u
/** 
  \}
*/ 

/** 
  \defgroup  CanPCMaxNumericValueDefines  Can Max Numeric Value Defines (PRE_COMPILE)
  \brief  These defines are used to implement against the maximum value in numerical based data.
  \{
*/ 
#define CAN_MAX_PDUOFACTIVESENDOBJECT                                 65535u
#define CAN_MAX_STATEOFACTIVESENDOBJECT                               65535u
#define CAN_MAX_BUSOFFCOUNTEROFCONTROLLERDATA                         255u
#define CAN_MAX_BUSOFFTRANSITIONREQUESTOFCONTROLLERDATA               255u
#define CAN_MAX_CANINTERRUPTCOUNTEROFCONTROLLERDATA                   255u
#define CAN_MAX_LASTINITOBJECTOFCONTROLLERDATA                        255u
#define CAN_MAX_LOGSTATUSOFCONTROLLERDATA                             255u
#define CAN_MAX_MODETRANSITIONREQUESTOFCONTROLLERDATA                 255u
#define CAN_MAX_RAMCHECKTRANSITIONREQUESTOFCONTROLLERDATA             255u
/** 
  \}
*/ 

/** 
  \defgroup  CanPCNoReferenceDefines  Can No Reference Defines (PRE_COMPILE)
  \brief  These defines are used to indicate unused indexes in data relations.
  \{
*/ 
#define CAN_NO_CANIFCHANNELID                                         255u
#define CAN_NO_CANCONTROLLERDEFAULTBAUDRATEIDXOFCONTROLLERCONFIG      255u
#define CAN_NO_CANCONTROLLERDEFAULTBAUDRATEOFCONTROLLERCONFIG         65535u
#define CAN_NO_MAILBOXRXBASICENDIDXOFCONTROLLERCONFIG                 255u
#define CAN_NO_MAILBOXRXBASICSTARTIDXOFCONTROLLERCONFIG               255u
#define CAN_NO_MAILBOXRXFULLENDIDXOFCONTROLLERCONFIG                  255u
#define CAN_NO_MAILBOXRXFULLSTARTIDXOFCONTROLLERCONFIG                255u
#define CAN_NO_MAILBOXTXBASICENDIDXOFCONTROLLERCONFIG                 255u
#define CAN_NO_MAILBOXTXBASICSTARTIDXOFCONTROLLERCONFIG               255u
#define CAN_NO_MAILBOXTXFULLENDIDXOFCONTROLLERCONFIG                  255u
#define CAN_NO_MAILBOXTXFULLSTARTIDXOFCONTROLLERCONFIG                255u
#define CAN_NO_RXBASICHWSTARTOFCONTROLLERCONFIG                       255u
#define CAN_NO_RXBASICHWSTOPOFCONTROLLERCONFIG                        255u
#define CAN_NO_RXFULLHWSTARTOFCONTROLLERCONFIG                        255u
#define CAN_NO_RXFULLHWSTOPOFCONTROLLERCONFIG                         255u
#define CAN_NO_TXBASICHWSTARTOFCONTROLLERCONFIG                       255u
#define CAN_NO_TXBASICHWSTOPOFCONTROLLERCONFIG                        255u
#define CAN_NO_TXFULLHWSTARTOFCONTROLLERCONFIG                        255u
#define CAN_NO_TXFULLHWSTOPOFCONTROLLERCONFIG                         255u
#define CAN_NO_UNUSEDHWSTARTOFCONTROLLERCONFIG                        255u
#define CAN_NO_UNUSEDHWSTOPOFCONTROLLERCONFIG                         255u
/** 
  \}
*/ 

/** 
  \defgroup  CanPCEnumExistsDefines  Can Enum Exists Defines (PRE_COMPILE)
  \brief  These defines can be used to deactivate enumeration based code sequences if the enumeration value does not exist in the configuration data.
  \{
*/ 
#define CAN_EXISTS_NONE_INITOBJECTFDBRSCONFIG                         STD_ON
#define CAN_EXISTS_FD_RXTX_INITOBJECTFDBRSCONFIG                      STD_ON
#define CAN_EXISTS_FD_RXONLY_INITOBJECTFDBRSCONFIG                    STD_OFF
#define CAN_EXISTS_RX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX              STD_ON
#define CAN_EXISTS_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX               STD_ON
#define CAN_EXISTS_TX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX              STD_ON
#define CAN_EXISTS_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX               STD_ON
#define CAN_EXISTS_UNUSED_CAN_TYPE_MAILBOXTYPEOFMAILBOX               STD_OFF
#define CAN_EXISTS_TX_BASICCAN_MUX_TYPE_MAILBOXTYPEOFMAILBOX          STD_OFF
#define CAN_EXISTS_TX_BASICCAN_FIFO_TYPE_MAILBOXTYPEOFMAILBOX         STD_OFF
/** 
  \}
*/ 

/** 
  \defgroup  CanPCEnumDefines  Can Enum Defines (PRE_COMPILE)
  \brief  These defines are the enumeration values of enumeration based CONST and VAR data.
  \{
*/ 
#define CAN_NONE_INITOBJECTFDBRSCONFIG                                0x00u
#define CAN_FD_RXTX_INITOBJECTFDBRSCONFIG                             0x01u
#define CAN_RX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX                     0x00u
#define CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX                      0x01u
#define CAN_TX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX                     0x02u
#define CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX                      0x03u
/** 
  \}
*/ 

/** 
  \defgroup  CanPCIsReducedToDefineDefines  Can Is Reduced To Define Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define is STD_ON else STD_OFF.
  \{
*/ 
#define CAN_ISDEF_CANIFCHANNELID                                      STD_OFF
#define CAN_ISDEF_BASEADDRESSOFCONTROLLERCONFIG                       STD_OFF
#define CAN_ISDEF_CANCONTROLLERDEFAULTBAUDRATEIDXOFCONTROLLERCONFIG   STD_OFF
#define CAN_ISDEF_CANCONTROLLERDEFAULTBAUDRATEOFCONTROLLERCONFIG      STD_OFF
#define CAN_ISDEF_HASCANFDBAUDRATEOFCONTROLLERCONFIG                  STD_OFF
#define CAN_ISDEF_INTERRUPTMASK1OFCONTROLLERCONFIG                    STD_OFF
#define CAN_ISDEF_INTERRUPTMASK2OFCONTROLLERCONFIG                    STD_OFF
#define CAN_ISDEF_INTERRUPTMASK3OFCONTROLLERCONFIG                    STD_OFF
#define CAN_ISDEF_MAILBOXRXBASICENDIDXOFCONTROLLERCONFIG              STD_OFF
#define CAN_ISDEF_MAILBOXRXBASICLENGTHOFCONTROLLERCONFIG              STD_OFF
#define CAN_ISDEF_MAILBOXRXBASICSTARTIDXOFCONTROLLERCONFIG            STD_OFF
#define CAN_ISDEF_MAILBOXRXBASICUSEDOFCONTROLLERCONFIG                STD_OFF
#define CAN_ISDEF_MAILBOXRXFULLENDIDXOFCONTROLLERCONFIG               STD_OFF
#define CAN_ISDEF_MAILBOXRXFULLLENGTHOFCONTROLLERCONFIG               STD_OFF
#define CAN_ISDEF_MAILBOXRXFULLSTARTIDXOFCONTROLLERCONFIG             STD_OFF
#define CAN_ISDEF_MAILBOXRXFULLUSEDOFCONTROLLERCONFIG                 STD_OFF
#define CAN_ISDEF_MAILBOXTXBASICENDIDXOFCONTROLLERCONFIG              STD_OFF
#define CAN_ISDEF_MAILBOXTXBASICLENGTHOFCONTROLLERCONFIG              STD_OFF
#define CAN_ISDEF_MAILBOXTXBASICSTARTIDXOFCONTROLLERCONFIG            STD_OFF
#define CAN_ISDEF_MAILBOXTXBASICUSEDOFCONTROLLERCONFIG                STD_OFF
#define CAN_ISDEF_MAILBOXTXFULLENDIDXOFCONTROLLERCONFIG               STD_OFF
#define CAN_ISDEF_MAILBOXTXFULLLENGTHOFCONTROLLERCONFIG               STD_OFF
#define CAN_ISDEF_MAILBOXTXFULLSTARTIDXOFCONTROLLERCONFIG             STD_OFF
#define CAN_ISDEF_MAILBOXTXFULLUSEDOFCONTROLLERCONFIG                 STD_OFF
#define CAN_ISDEF_NUMBEROFFILTERSOFCONTROLLERCONFIG                   STD_OFF
#define CAN_ISDEF_NUMBEROFFULLCONFIGURABLEFILTERSOFCONTROLLERCONFIG   STD_OFF
#define CAN_ISDEF_NUMBEROFMAXMAILBOXESOFCONTROLLERCONFIG              STD_OFF
#define CAN_ISDEF_RFFNOFCONTROLLERCONFIG                              STD_OFF
#define CAN_ISDEF_RXBASICHWSTARTOFCONTROLLERCONFIG                    STD_OFF
#define CAN_ISDEF_RXBASICHWSTOPOFCONTROLLERCONFIG                     STD_OFF
#define CAN_ISDEF_RXFULLHWSTARTOFCONTROLLERCONFIG                     STD_OFF
#define CAN_ISDEF_RXFULLHWSTOPOFCONTROLLERCONFIG                      STD_OFF
#define CAN_ISDEF_TXBASICHWSTARTOFCONTROLLERCONFIG                    STD_OFF
#define CAN_ISDEF_TXBASICHWSTOPOFCONTROLLERCONFIG                     STD_OFF
#define CAN_ISDEF_TXFULLHWSTARTOFCONTROLLERCONFIG                     STD_OFF
#define CAN_ISDEF_TXFULLHWSTOPOFCONTROLLERCONFIG                      STD_OFF
#define CAN_ISDEF_UNUSEDHWSTARTOFCONTROLLERCONFIG                     STD_OFF
#define CAN_ISDEF_UNUSEDHWSTOPOFCONTROLLERCONFIG                      STD_OFF
#define CAN_ISDEF_INITCODEOFINITBASICCAN                              STD_OFF
#define CAN_ISDEF_INITMASKOFINITBASICCAN                              STD_OFF
#define CAN_ISDEF_INITBASICCANINDEX                                   STD_OFF
#define CAN_ISDEF_CBTOFINITOBJECT                                     STD_OFF
#define CAN_ISDEF_CONTROL1OFINITOBJECT                                STD_OFF
#define CAN_ISDEF_INITOBJECTBAUDRATE                                  STD_OFF
#define CAN_ISDEF_FDCBTOFINITOBJECTFD                                 STD_OFF
#define CAN_ISDEF_FDCTRLOFINITOBJECTFD                                STD_OFF
#define CAN_ISDEF_INITOBJECTFDBRSCONFIG                               STD_OFF
#define CAN_ISDEF_INITOBJECTSTARTINDEX                                STD_OFF
#define CAN_ISDEF_ACTIVESENDOBJECTOFMAILBOX                           STD_OFF
#define CAN_ISDEF_CONTROLLERCONFIGIDXOFMAILBOX                        STD_OFF
#define CAN_ISDEF_HWHANDLEOFMAILBOX                                   STD_OFF
#define CAN_ISDEF_IDVALUEOFMAILBOX                                    STD_OFF
#define CAN_ISDEF_MAILBOXSIZEOFMAILBOX                                STD_OFF
#define CAN_ISDEF_MAILBOXTYPEOFMAILBOX                                STD_OFF
#define CAN_ISDEF_MAXDATALENOFMAILBOX                                 STD_OFF
#define CAN_ISDEF_MEMORYSECTIONSINDEXOFMAILBOX                        STD_OFF
#define CAN_ISDEF_MEMORYSECTIONSTARTOFMEMORYSECTIONINFO               STD_OFF
#define CAN_ISDEF_MEMORYSTARTADDRESSOFMEMORYSECTIONINFO               STD_OFF
#define CAN_ISDEF_HWHANDLEOFMEMORYSECTIONOBJECTS                      STD_OFF
#define CAN_ISDEF_MAILBOXELEMENTOFMEMORYSECTIONOBJECTS                STD_OFF
#define CAN_ISDEF_MAILBOXHANDLEOFMEMORYSECTIONOBJECTS                 STD_OFF
#define CAN_ISDEF_ACTIVESENDOBJECTOFPCCONFIG                          STD_ON
#define CAN_ISDEF_CANIFCHANNELIDOFPCCONFIG                            STD_ON
#define CAN_ISDEF_CONTROLLERCONFIGOFPCCONFIG                          STD_ON
#define CAN_ISDEF_CONTROLLERDATAOFPCCONFIG                            STD_ON
#define CAN_ISDEF_INITBASICCANINDEXOFPCCONFIG                         STD_ON
#define CAN_ISDEF_INITBASICCANOFPCCONFIG                              STD_ON
#define CAN_ISDEF_INITOBJECTBAUDRATEOFPCCONFIG                        STD_ON
#define CAN_ISDEF_INITOBJECTFDOFPCCONFIG                              STD_ON
#define CAN_ISDEF_INITOBJECTFDBRSCONFIGOFPCCONFIG                     STD_ON
#define CAN_ISDEF_INITOBJECTOFPCCONFIG                                STD_ON
#define CAN_ISDEF_INITOBJECTSTARTINDEXOFPCCONFIG                      STD_ON
#define CAN_ISDEF_MAILBOXOFPCCONFIG                                   STD_ON
#define CAN_ISDEF_MEMORYSECTIONINFOOFPCCONFIG                         STD_ON
#define CAN_ISDEF_MEMORYSECTIONOBJECTSOFPCCONFIG                      STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  CanPCEqualsAlwaysToDefines  Can Equals Always To Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define contains the always equals value.
  \{
*/ 
#define CAN_EQ2_CANIFCHANNELID                                        
#define CAN_EQ2_BASEADDRESSOFCONTROLLERCONFIG                         
#define CAN_EQ2_CANCONTROLLERDEFAULTBAUDRATEIDXOFCONTROLLERCONFIG     
#define CAN_EQ2_CANCONTROLLERDEFAULTBAUDRATEOFCONTROLLERCONFIG        
#define CAN_EQ2_HASCANFDBAUDRATEOFCONTROLLERCONFIG                    
#define CAN_EQ2_INTERRUPTMASK1OFCONTROLLERCONFIG                      
#define CAN_EQ2_INTERRUPTMASK2OFCONTROLLERCONFIG                      
#define CAN_EQ2_INTERRUPTMASK3OFCONTROLLERCONFIG                      
#define CAN_EQ2_MAILBOXRXBASICENDIDXOFCONTROLLERCONFIG                
#define CAN_EQ2_MAILBOXRXBASICLENGTHOFCONTROLLERCONFIG                
#define CAN_EQ2_MAILBOXRXBASICSTARTIDXOFCONTROLLERCONFIG              
#define CAN_EQ2_MAILBOXRXBASICUSEDOFCONTROLLERCONFIG                  
#define CAN_EQ2_MAILBOXRXFULLENDIDXOFCONTROLLERCONFIG                 
#define CAN_EQ2_MAILBOXRXFULLLENGTHOFCONTROLLERCONFIG                 
#define CAN_EQ2_MAILBOXRXFULLSTARTIDXOFCONTROLLERCONFIG               
#define CAN_EQ2_MAILBOXRXFULLUSEDOFCONTROLLERCONFIG                   
#define CAN_EQ2_MAILBOXTXBASICENDIDXOFCONTROLLERCONFIG                
#define CAN_EQ2_MAILBOXTXBASICLENGTHOFCONTROLLERCONFIG                
#define CAN_EQ2_MAILBOXTXBASICSTARTIDXOFCONTROLLERCONFIG              
#define CAN_EQ2_MAILBOXTXBASICUSEDOFCONTROLLERCONFIG                  
#define CAN_EQ2_MAILBOXTXFULLENDIDXOFCONTROLLERCONFIG                 
#define CAN_EQ2_MAILBOXTXFULLLENGTHOFCONTROLLERCONFIG                 
#define CAN_EQ2_MAILBOXTXFULLSTARTIDXOFCONTROLLERCONFIG               
#define CAN_EQ2_MAILBOXTXFULLUSEDOFCONTROLLERCONFIG                   
#define CAN_EQ2_NUMBEROFFILTERSOFCONTROLLERCONFIG                     
#define CAN_EQ2_NUMBEROFFULLCONFIGURABLEFILTERSOFCONTROLLERCONFIG     
#define CAN_EQ2_NUMBEROFMAXMAILBOXESOFCONTROLLERCONFIG                
#define CAN_EQ2_RFFNOFCONTROLLERCONFIG                                
#define CAN_EQ2_RXBASICHWSTARTOFCONTROLLERCONFIG                      
#define CAN_EQ2_RXBASICHWSTOPOFCONTROLLERCONFIG                       
#define CAN_EQ2_RXFULLHWSTARTOFCONTROLLERCONFIG                       
#define CAN_EQ2_RXFULLHWSTOPOFCONTROLLERCONFIG                        
#define CAN_EQ2_TXBASICHWSTARTOFCONTROLLERCONFIG                      
#define CAN_EQ2_TXBASICHWSTOPOFCONTROLLERCONFIG                       
#define CAN_EQ2_TXFULLHWSTARTOFCONTROLLERCONFIG                       
#define CAN_EQ2_TXFULLHWSTOPOFCONTROLLERCONFIG                        
#define CAN_EQ2_UNUSEDHWSTARTOFCONTROLLERCONFIG                       
#define CAN_EQ2_UNUSEDHWSTOPOFCONTROLLERCONFIG                        
#define CAN_EQ2_INITCODEOFINITBASICCAN                                
#define CAN_EQ2_INITMASKOFINITBASICCAN                                
#define CAN_EQ2_INITBASICCANINDEX                                     
#define CAN_EQ2_CBTOFINITOBJECT                                       
#define CAN_EQ2_CONTROL1OFINITOBJECT                                  
#define CAN_EQ2_INITOBJECTBAUDRATE                                    
#define CAN_EQ2_FDCBTOFINITOBJECTFD                                   
#define CAN_EQ2_FDCTRLOFINITOBJECTFD                                  
#define CAN_EQ2_INITOBJECTFDBRSCONFIG                                 
#define CAN_EQ2_INITOBJECTSTARTINDEX                                  
#define CAN_EQ2_ACTIVESENDOBJECTOFMAILBOX                             
#define CAN_EQ2_CONTROLLERCONFIGIDXOFMAILBOX                          
#define CAN_EQ2_HWHANDLEOFMAILBOX                                     
#define CAN_EQ2_IDVALUEOFMAILBOX                                      
#define CAN_EQ2_MAILBOXSIZEOFMAILBOX                                  
#define CAN_EQ2_MAILBOXTYPEOFMAILBOX                                  
#define CAN_EQ2_MAXDATALENOFMAILBOX                                   
#define CAN_EQ2_MEMORYSECTIONSINDEXOFMAILBOX                          
#define CAN_EQ2_MEMORYSECTIONSTARTOFMEMORYSECTIONINFO                 
#define CAN_EQ2_MEMORYSTARTADDRESSOFMEMORYSECTIONINFO                 
#define CAN_EQ2_HWHANDLEOFMEMORYSECTIONOBJECTS                        
#define CAN_EQ2_MAILBOXELEMENTOFMEMORYSECTIONOBJECTS                  
#define CAN_EQ2_MAILBOXHANDLEOFMEMORYSECTIONOBJECTS                   
#define CAN_EQ2_ACTIVESENDOBJECTOFPCCONFIG                            Can_ActiveSendObject
#define CAN_EQ2_CANIFCHANNELIDOFPCCONFIG                              Can_CanIfChannelId
#define CAN_EQ2_CONTROLLERCONFIGOFPCCONFIG                            Can_ControllerConfig
#define CAN_EQ2_CONTROLLERDATAOFPCCONFIG                              Can_ControllerData
#define CAN_EQ2_INITBASICCANINDEXOFPCCONFIG                           Can_InitBasicCanIndex
#define CAN_EQ2_INITBASICCANOFPCCONFIG                                Can_InitBasicCan
#define CAN_EQ2_INITOBJECTBAUDRATEOFPCCONFIG                          Can_InitObjectBaudrate
#define CAN_EQ2_INITOBJECTFDOFPCCONFIG                                Can_InitObjectFD
#define CAN_EQ2_INITOBJECTFDBRSCONFIGOFPCCONFIG                       Can_InitObjectFdBrsConfig
#define CAN_EQ2_INITOBJECTOFPCCONFIG                                  Can_InitObject
#define CAN_EQ2_INITOBJECTSTARTINDEXOFPCCONFIG                        Can_InitObjectStartIndex
#define CAN_EQ2_MAILBOXOFPCCONFIG                                     Can_Mailbox
#define CAN_EQ2_MEMORYSECTIONINFOOFPCCONFIG                           Can_MemorySectionInfo
#define CAN_EQ2_MEMORYSECTIONOBJECTSOFPCCONFIG                        Can_MemorySectionObjects
/** 
  \}
*/ 

/** 
  \defgroup  CanPCSymbolicInitializationPointers  Can Symbolic Initialization Pointers (PRE_COMPILE)
  \brief  Symbolic initialization pointers to be used in the call of a preinit or init function.
  \{
*/ 
#define Can_Config_Ptr                                                NULL_PTR  /**< symbolic identifier which shall be used to initialize 'Can' */
/** 
  \}
*/ 

/** 
  \defgroup  CanPCInitializationSymbols  Can Initialization Symbols (PRE_COMPILE)
  \brief  Symbolic initialization pointers which may be used in the call of a preinit or init function. Please note, that the defined value can be a 'NULL_PTR' and the address operator is not usable.
  \{
*/ 
#define Can_Config                                                    NULL_PTR  /**< symbolic identifier which could be used to initialize 'Can */
/** 
  \}
*/ 

/** 
  \defgroup  CanPCGeneral  Can General (PRE_COMPILE)
  \brief  General constant defines not associated with a group of defines.
  \{
*/ 
#define CAN_CHECK_INIT_POINTER                                        STD_OFF  /**< STD_ON if the init pointer shall not be used as NULL_PTR and a check shall validate this. */
#define CAN_FINAL_MAGIC_NUMBER                                        0x501Eu  /**< the precompile constant to validate the size of the initialization structure at initialization time of Can */
#define CAN_INDIVIDUAL_POSTBUILD                                      STD_OFF  /**< the precompile constant to check, that the module is individual postbuildable. The module 'Can' is not configured to be postbuild capable. */
#define CAN_INIT_DATA                                                 CAN_CONST  /**< CompilerMemClassDefine for the initialization data. */
#define CAN_INIT_DATA_HASH_CODE                                       -1893185136  /**< the precompile constant to validate the initialization structure at initialization time of Can with a hashcode. The seed value is '0x501Eu' */
#define CAN_USE_ECUM_BSW_ERROR_HOOK                                   STD_OFF  /**< STD_ON if the EcuM_BswErrorHook shall be called in the ConfigPtr check. */
#define CAN_USE_INIT_POINTER                                          STD_OFF  /**< STD_ON if the init pointer Can shall be used. */
/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  CanLTDataSwitches  Can Data Switches  (LINK)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define CAN_LTCONFIG                                                  STD_OFF  /**< Deactivateable: 'Can_LTConfig' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  CanPBDataSwitches  Can Data Switches  (POST_BUILD)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define CAN_PBCONFIG                                                  STD_OFF  /**< Deactivateable: 'Can_PBConfig' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define CAN_LTCONFIGIDXOFPBCONFIG                                     STD_OFF  /**< Deactivateable: 'Can_PBConfig.LTConfigIdx' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define CAN_PCCONFIGIDXOFPBCONFIG                                     STD_OFF  /**< Deactivateable: 'Can_PBConfig.PCConfigIdx' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
/** 
  \}
*/ 



/**********************************************************************************************************************
  GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
/** 
  \defgroup  CanPCGetConstantDuplicatedRootDataMacros  Can Get Constant Duplicated Root Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated by constance root data elements.
  \{
*/ 
#define Can_GetActiveSendObjectOfPCConfig()                           Can_ActiveSendObject  /**< the pointer to Can_ActiveSendObject */
#define Can_GetBaseDll_GeneratorVersionOfPCConfig()                   0x0103u
#define Can_GetCanIfChannelIdOfPCConfig()                             Can_CanIfChannelId  /**< the pointer to Can_CanIfChannelId */
#define Can_GetControllerConfigOfPCConfig()                           Can_ControllerConfig  /**< the pointer to Can_ControllerConfig */
#define Can_GetControllerDataOfPCConfig()                             Can_ControllerData  /**< the pointer to Can_ControllerData */
#define Can_GetInitBasicCanIndexOfPCConfig()                          Can_InitBasicCanIndex  /**< the pointer to Can_InitBasicCanIndex */
#define Can_GetInitBasicCanOfPCConfig()                               Can_InitBasicCan  /**< the pointer to Can_InitBasicCan */
#define Can_GetInitObjectBaudrateOfPCConfig()                         Can_InitObjectBaudrate  /**< the pointer to Can_InitObjectBaudrate */
#define Can_GetInitObjectFDOfPCConfig()                               Can_InitObjectFD  /**< the pointer to Can_InitObjectFD */
#define Can_GetInitObjectFdBrsConfigOfPCConfig()                      Can_InitObjectFdBrsConfig  /**< the pointer to Can_InitObjectFdBrsConfig */
#define Can_GetInitObjectOfPCConfig()                                 Can_InitObject  /**< the pointer to Can_InitObject */
#define Can_GetInitObjectStartIndexOfPCConfig()                       Can_InitObjectStartIndex  /**< the pointer to Can_InitObjectStartIndex */
#define Can_GetMailboxOfPCConfig()                                    Can_Mailbox  /**< the pointer to Can_Mailbox */
#define Can_GetMemorySectionInfoOfPCConfig()                          Can_MemorySectionInfo  /**< the pointer to Can_MemorySectionInfo */
#define Can_GetMemorySectionObjectsOfPCConfig()                       Can_MemorySectionObjects  /**< the pointer to Can_MemorySectionObjects */
#define Can_GetPlatformDll_GeneratorVersionOfPCConfig()               0x0101u
#define Can_GetSizeOfActiveSendObjectOfPCConfig()                     72u  /**< the number of accomplishable value elements in Can_ActiveSendObject */
#define Can_GetSizeOfCanIfChannelIdOfPCConfig()                       6u  /**< the number of accomplishable value elements in Can_CanIfChannelId */
#define Can_GetSizeOfControllerConfigOfPCConfig()                     6u  /**< the number of accomplishable value elements in Can_ControllerConfig */
#define Can_GetSizeOfInitBasicCanIndexOfPCConfig()                    6u  /**< the number of accomplishable value elements in Can_InitBasicCanIndex */
#define Can_GetSizeOfInitBasicCanOfPCConfig()                         26u  /**< the number of accomplishable value elements in Can_InitBasicCan */
#define Can_GetSizeOfInitObjectBaudrateOfPCConfig()                   6u  /**< the number of accomplishable value elements in Can_InitObjectBaudrate */
#define Can_GetSizeOfInitObjectFDOfPCConfig()                         6u  /**< the number of accomplishable value elements in Can_InitObjectFD */
#define Can_GetSizeOfInitObjectFdBrsConfigOfPCConfig()                6u  /**< the number of accomplishable value elements in Can_InitObjectFdBrsConfig */
#define Can_GetSizeOfInitObjectOfPCConfig()                           6u  /**< the number of accomplishable value elements in Can_InitObject */
#define Can_GetSizeOfInitObjectStartIndexOfPCConfig()                 7u  /**< the number of accomplishable value elements in Can_InitObjectStartIndex */
#define Can_GetSizeOfMailboxOfPCConfig()                              226u  /**< the number of accomplishable value elements in Can_Mailbox */
#define Can_GetSizeOfMemorySectionInfoOfPCConfig()                    6u  /**< the number of accomplishable value elements in Can_MemorySectionInfo */
#define Can_GetSizeOfMemorySectionObjectsOfPCConfig()                 576u  /**< the number of accomplishable value elements in Can_MemorySectionObjects */
/** 
  \}
*/ 

/** 
  \defgroup  CanPCGetDuplicatedRootDataMacros  Can Get Duplicated Root Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated root data elements.
  \{
*/ 
#define Can_GetSizeOfControllerDataOfPCConfig()                       Can_GetSizeOfControllerConfigOfPCConfig()  /**< the number of accomplishable value elements in Can_ControllerData */
/** 
  \}
*/ 

/** 
  \defgroup  CanPCGetDataMacros  Can Get Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read CONST and VAR data.
  \{
*/ 
#define Can_GetPduOfActiveSendObject(Index)                           (Can_GetActiveSendObjectOfPCConfig()[(Index)].PduOfActiveSendObject)
#define Can_GetStateOfActiveSendObject(Index)                         (Can_GetActiveSendObjectOfPCConfig()[(Index)].StateOfActiveSendObject)
#define Can_GetCanIfChannelId(Index)                                  (Can_GetCanIfChannelIdOfPCConfig()[(Index)])
#define Can_GetBaseAddressOfControllerConfig(Index)                   (Can_GetControllerConfigOfPCConfig()[(Index)].BaseAddressOfControllerConfig)
#define Can_GetCanControllerDefaultBaudrateIdxOfControllerConfig(Index) (Can_GetControllerConfigOfPCConfig()[(Index)].CanControllerDefaultBaudrateIdxOfControllerConfig)
#define Can_GetCanControllerDefaultBaudrateOfControllerConfig(Index)  (Can_GetControllerConfigOfPCConfig()[(Index)].CanControllerDefaultBaudrateOfControllerConfig)
#define Can_IsHasCANFDBaudrateOfControllerConfig(Index)               ((Can_GetControllerConfigOfPCConfig()[(Index)].HasCANFDBaudrateOfControllerConfig) != FALSE)
#define Can_GetInterruptMask1OfControllerConfig(Index)                (Can_GetControllerConfigOfPCConfig()[(Index)].InterruptMask1OfControllerConfig)
#define Can_GetInterruptMask2OfControllerConfig(Index)                (Can_GetControllerConfigOfPCConfig()[(Index)].InterruptMask2OfControllerConfig)
#define Can_GetInterruptMask3OfControllerConfig(Index)                (Can_GetControllerConfigOfPCConfig()[(Index)].InterruptMask3OfControllerConfig)
#define Can_GetMailboxRxBasicEndIdxOfControllerConfig(Index)          (Can_GetControllerConfigOfPCConfig()[(Index)].MailboxRxBasicEndIdxOfControllerConfig)
#define Can_GetMailboxRxBasicLengthOfControllerConfig(Index)          (Can_GetControllerConfigOfPCConfig()[(Index)].MailboxRxBasicLengthOfControllerConfig)
#define Can_GetMailboxRxBasicStartIdxOfControllerConfig(Index)        (Can_GetControllerConfigOfPCConfig()[(Index)].MailboxRxBasicStartIdxOfControllerConfig)
#define Can_GetMailboxRxFullEndIdxOfControllerConfig(Index)           (Can_GetControllerConfigOfPCConfig()[(Index)].MailboxRxFullEndIdxOfControllerConfig)
#define Can_GetMailboxRxFullLengthOfControllerConfig(Index)           (Can_GetControllerConfigOfPCConfig()[(Index)].MailboxRxFullLengthOfControllerConfig)
#define Can_GetMailboxRxFullStartIdxOfControllerConfig(Index)         (Can_GetControllerConfigOfPCConfig()[(Index)].MailboxRxFullStartIdxOfControllerConfig)
#define Can_GetMailboxTxBasicEndIdxOfControllerConfig(Index)          (Can_GetControllerConfigOfPCConfig()[(Index)].MailboxTxBasicEndIdxOfControllerConfig)
#define Can_GetMailboxTxBasicLengthOfControllerConfig(Index)          (Can_GetControllerConfigOfPCConfig()[(Index)].MailboxTxBasicLengthOfControllerConfig)
#define Can_GetMailboxTxBasicStartIdxOfControllerConfig(Index)        (Can_GetControllerConfigOfPCConfig()[(Index)].MailboxTxBasicStartIdxOfControllerConfig)
#define Can_GetMailboxTxFullEndIdxOfControllerConfig(Index)           (Can_GetControllerConfigOfPCConfig()[(Index)].MailboxTxFullEndIdxOfControllerConfig)
#define Can_GetMailboxTxFullLengthOfControllerConfig(Index)           (Can_GetControllerConfigOfPCConfig()[(Index)].MailboxTxFullLengthOfControllerConfig)
#define Can_GetMailboxTxFullStartIdxOfControllerConfig(Index)         (Can_GetControllerConfigOfPCConfig()[(Index)].MailboxTxFullStartIdxOfControllerConfig)
#define Can_GetNumberOfFiltersOfControllerConfig(Index)               (Can_GetControllerConfigOfPCConfig()[(Index)].NumberOfFiltersOfControllerConfig)
#define Can_GetNumberOfFullConfigurableFiltersOfControllerConfig(Index) (Can_GetControllerConfigOfPCConfig()[(Index)].NumberOfFullConfigurableFiltersOfControllerConfig)
#define Can_GetNumberOfMaxMailboxesOfControllerConfig(Index)          (Can_GetControllerConfigOfPCConfig()[(Index)].NumberOfMaxMailboxesOfControllerConfig)
#define Can_GetRFFNOfControllerConfig(Index)                          (Can_GetControllerConfigOfPCConfig()[(Index)].RFFNOfControllerConfig)
#define Can_GetRxBasicHwStartOfControllerConfig(Index)                (Can_GetControllerConfigOfPCConfig()[(Index)].RxBasicHwStartOfControllerConfig)
#define Can_GetRxBasicHwStopOfControllerConfig(Index)                 (Can_GetControllerConfigOfPCConfig()[(Index)].RxBasicHwStopOfControllerConfig)
#define Can_GetRxFullHwStartOfControllerConfig(Index)                 (Can_GetControllerConfigOfPCConfig()[(Index)].RxFullHwStartOfControllerConfig)
#define Can_GetRxFullHwStopOfControllerConfig(Index)                  (Can_GetControllerConfigOfPCConfig()[(Index)].RxFullHwStopOfControllerConfig)
#define Can_GetTxBasicHwStartOfControllerConfig(Index)                (Can_GetControllerConfigOfPCConfig()[(Index)].TxBasicHwStartOfControllerConfig)
#define Can_GetTxBasicHwStopOfControllerConfig(Index)                 (Can_GetControllerConfigOfPCConfig()[(Index)].TxBasicHwStopOfControllerConfig)
#define Can_GetTxFullHwStartOfControllerConfig(Index)                 (Can_GetControllerConfigOfPCConfig()[(Index)].TxFullHwStartOfControllerConfig)
#define Can_GetTxFullHwStopOfControllerConfig(Index)                  (Can_GetControllerConfigOfPCConfig()[(Index)].TxFullHwStopOfControllerConfig)
#define Can_GetUnusedHwStartOfControllerConfig(Index)                 (Can_GetControllerConfigOfPCConfig()[(Index)].UnusedHwStartOfControllerConfig)
#define Can_GetUnusedHwStopOfControllerConfig(Index)                  (Can_GetControllerConfigOfPCConfig()[(Index)].UnusedHwStopOfControllerConfig)
#define Can_GetBusOffCounterOfControllerData(Index)                   (Can_GetControllerDataOfPCConfig()[(Index)].BusOffCounterOfControllerData)
#define Can_GetBusOffTransitionRequestOfControllerData(Index)         (Can_GetControllerDataOfPCConfig()[(Index)].BusOffTransitionRequestOfControllerData)
#define Can_GetCanInterruptCounterOfControllerData(Index)             (Can_GetControllerDataOfPCConfig()[(Index)].CanInterruptCounterOfControllerData)
#define Can_GetCanInterruptOldStatusOfControllerData(Index)           (Can_GetControllerDataOfPCConfig()[(Index)].CanInterruptOldStatusOfControllerData)
#define Can_IsIsBusOffOfControllerData(Index)                         ((Can_GetControllerDataOfPCConfig()[(Index)].IsBusOffOfControllerData) != FALSE)
#define Can_GetLastInitObjectOfControllerData(Index)                  (Can_GetControllerDataOfPCConfig()[(Index)].LastInitObjectOfControllerData)
#define Can_GetLogStatusOfControllerData(Index)                       (Can_GetControllerDataOfPCConfig()[(Index)].LogStatusOfControllerData)
#define Can_GetLoopTimeoutOfControllerData(Index)                     (Can_GetControllerDataOfPCConfig()[(Index)].LoopTimeoutOfControllerData)
#define Can_GetModeTransitionRequestOfControllerData(Index)           (Can_GetControllerDataOfPCConfig()[(Index)].ModeTransitionRequestOfControllerData)
#define Can_GetRamCheckTransitionRequestOfControllerData(Index)       (Can_GetControllerDataOfPCConfig()[(Index)].RamCheckTransitionRequestOfControllerData)
#define Can_GetRxMsgBufferOfControllerData(Index)                     (Can_GetControllerDataOfPCConfig()[(Index)].RxMsgBufferOfControllerData)
#define Can_IsStartModeRequestedOfControllerData(Index)               ((Can_GetControllerDataOfPCConfig()[(Index)].StartModeRequestedOfControllerData) != FALSE)
#define Can_GetInitCodeOfInitBasicCan(Index)                          (Can_GetInitBasicCanOfPCConfig()[(Index)].InitCodeOfInitBasicCan)
#define Can_GetInitMaskOfInitBasicCan(Index)                          (Can_GetInitBasicCanOfPCConfig()[(Index)].InitMaskOfInitBasicCan)
#define Can_GetInitBasicCanIndex(Index)                               (Can_GetInitBasicCanIndexOfPCConfig()[(Index)])
#define Can_GetCBTOfInitObject(Index)                                 (Can_GetInitObjectOfPCConfig()[(Index)].CBTOfInitObject)
#define Can_GetControl1OfInitObject(Index)                            (Can_GetInitObjectOfPCConfig()[(Index)].Control1OfInitObject)
#define Can_GetInitObjectBaudrate(Index)                              (Can_GetInitObjectBaudrateOfPCConfig()[(Index)])
#define Can_GetFDCBTOfInitObjectFD(Index)                             (Can_GetInitObjectFDOfPCConfig()[(Index)].FDCBTOfInitObjectFD)
#define Can_GetFDCTRLOfInitObjectFD(Index)                            (Can_GetInitObjectFDOfPCConfig()[(Index)].FDCTRLOfInitObjectFD)
#define Can_GetInitObjectFdBrsConfig(Index)                           (Can_GetInitObjectFdBrsConfigOfPCConfig()[(Index)])
#define Can_GetInitObjectStartIndex(Index)                            (Can_GetInitObjectStartIndexOfPCConfig()[(Index)])
#define Can_GetActiveSendObjectOfMailbox(Index)                       (Can_GetMailboxOfPCConfig()[(Index)].ActiveSendObjectOfMailbox)
#define Can_GetControllerConfigIdxOfMailbox(Index)                    (Can_GetMailboxOfPCConfig()[(Index)].ControllerConfigIdxOfMailbox)
#define Can_GetHwHandleOfMailbox(Index)                               (Can_GetMailboxOfPCConfig()[(Index)].HwHandleOfMailbox)
#define Can_GetIDValueOfMailbox(Index)                                (Can_GetMailboxOfPCConfig()[(Index)].IDValueOfMailbox)
#define Can_GetMailboxSizeOfMailbox(Index)                            (Can_GetMailboxOfPCConfig()[(Index)].MailboxSizeOfMailbox)
#define Can_GetMailboxTypeOfMailbox(Index)                            (Can_GetMailboxOfPCConfig()[(Index)].MailboxTypeOfMailbox)
#define Can_GetMaxDataLenOfMailbox(Index)                             (Can_GetMailboxOfPCConfig()[(Index)].MaxDataLenOfMailbox)
#define Can_GetMemorySectionsIndexOfMailbox(Index)                    (Can_GetMailboxOfPCConfig()[(Index)].MemorySectionsIndexOfMailbox)
#define Can_GetMemorySectionStartOfMemorySectionInfo(Index)           (Can_GetMemorySectionInfoOfPCConfig()[(Index)].MemorySectionStartOfMemorySectionInfo)
#define Can_GetMemoryStartAddressOfMemorySectionInfo(Index)           (Can_GetMemorySectionInfoOfPCConfig()[(Index)].MemoryStartAddressOfMemorySectionInfo)
#define Can_GetHwHandleOfMemorySectionObjects(Index)                  (Can_GetMemorySectionObjectsOfPCConfig()[(Index)].HwHandleOfMemorySectionObjects)
#define Can_GetMailboxElementOfMemorySectionObjects(Index)            (Can_GetMemorySectionObjectsOfPCConfig()[(Index)].MailboxElementOfMemorySectionObjects)
#define Can_GetMailboxHandleOfMemorySectionObjects(Index)             (Can_GetMemorySectionObjectsOfPCConfig()[(Index)].MailboxHandleOfMemorySectionObjects)
/** 
  \}
*/ 

/** 
  \defgroup  CanPCGetDeduplicatedDataMacros  Can Get Deduplicated Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated data elements.
  \{
*/ 
#define Can_GetBaseDll_GeneratorVersion()                             Can_GetBaseDll_GeneratorVersionOfPCConfig()
#define Can_IsMailboxRxBasicUsedOfControllerConfig(Index)             (((boolean)(Can_GetMailboxRxBasicLengthOfControllerConfig(Index) != 0u)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to Can_Mailbox */
#define Can_IsMailboxRxFullUsedOfControllerConfig(Index)              (((boolean)(Can_GetMailboxRxFullLengthOfControllerConfig(Index) != 0u)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to Can_Mailbox */
#define Can_IsMailboxTxBasicUsedOfControllerConfig(Index)             (((boolean)(Can_GetMailboxTxBasicLengthOfControllerConfig(Index) != 0u)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to Can_Mailbox */
#define Can_IsMailboxTxFullUsedOfControllerConfig(Index)              (((boolean)(Can_GetMailboxTxFullLengthOfControllerConfig(Index) != 0u)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to Can_Mailbox */
#define Can_GetPlatformDll_GeneratorVersion()                         Can_GetPlatformDll_GeneratorVersionOfPCConfig()
#define Can_GetSizeOfActiveSendObject()                               Can_GetSizeOfActiveSendObjectOfPCConfig()
#define Can_GetSizeOfCanIfChannelId()                                 Can_GetSizeOfCanIfChannelIdOfPCConfig()
#define Can_GetSizeOfControllerConfig()                               Can_GetSizeOfControllerConfigOfPCConfig()
#define Can_GetSizeOfControllerData()                                 Can_GetSizeOfControllerDataOfPCConfig()
#define Can_GetSizeOfInitBasicCan()                                   Can_GetSizeOfInitBasicCanOfPCConfig()
#define Can_GetSizeOfInitBasicCanIndex()                              Can_GetSizeOfInitBasicCanIndexOfPCConfig()
#define Can_GetSizeOfInitObject()                                     Can_GetSizeOfInitObjectOfPCConfig()
#define Can_GetSizeOfInitObjectBaudrate()                             Can_GetSizeOfInitObjectBaudrateOfPCConfig()
#define Can_GetSizeOfInitObjectFD()                                   Can_GetSizeOfInitObjectFDOfPCConfig()
#define Can_GetSizeOfInitObjectFdBrsConfig()                          Can_GetSizeOfInitObjectFdBrsConfigOfPCConfig()
#define Can_GetSizeOfInitObjectStartIndex()                           Can_GetSizeOfInitObjectStartIndexOfPCConfig()
#define Can_GetSizeOfMailbox()                                        Can_GetSizeOfMailboxOfPCConfig()
#define Can_GetSizeOfMemorySectionInfo()                              Can_GetSizeOfMemorySectionInfoOfPCConfig()
#define Can_GetSizeOfMemorySectionObjects()                           Can_GetSizeOfMemorySectionObjectsOfPCConfig()
/** 
  \}
*/ 

/** 
  \defgroup  CanPCSetDataMacros  Can Set Data Macros (PRE_COMPILE)
  \brief  These macros can be used to write data.
  \{
*/ 
#define Can_SetPduOfActiveSendObject(Index, Value)                    Can_GetActiveSendObjectOfPCConfig()[(Index)].PduOfActiveSendObject = (Value)
#define Can_SetStateOfActiveSendObject(Index, Value)                  Can_GetActiveSendObjectOfPCConfig()[(Index)].StateOfActiveSendObject = (Value)
#define Can_SetBusOffCounterOfControllerData(Index, Value)            Can_GetControllerDataOfPCConfig()[(Index)].BusOffCounterOfControllerData = (Value)
#define Can_SetBusOffTransitionRequestOfControllerData(Index, Value)  Can_GetControllerDataOfPCConfig()[(Index)].BusOffTransitionRequestOfControllerData = (Value)
#define Can_SetCanInterruptCounterOfControllerData(Index, Value)      Can_GetControllerDataOfPCConfig()[(Index)].CanInterruptCounterOfControllerData = (Value)
#define Can_SetCanInterruptOldStatusOfControllerData(Index, Value)    Can_GetControllerDataOfPCConfig()[(Index)].CanInterruptOldStatusOfControllerData = (Value)
#define Can_SetIsBusOffOfControllerData(Index, Value)                 Can_GetControllerDataOfPCConfig()[(Index)].IsBusOffOfControllerData = (Value)
#define Can_SetLastInitObjectOfControllerData(Index, Value)           Can_GetControllerDataOfPCConfig()[(Index)].LastInitObjectOfControllerData = (Value)
#define Can_SetLogStatusOfControllerData(Index, Value)                Can_GetControllerDataOfPCConfig()[(Index)].LogStatusOfControllerData = (Value)
#define Can_SetLoopTimeoutOfControllerData(Index, Value)              Can_GetControllerDataOfPCConfig()[(Index)].LoopTimeoutOfControllerData = (Value)
#define Can_SetModeTransitionRequestOfControllerData(Index, Value)    Can_GetControllerDataOfPCConfig()[(Index)].ModeTransitionRequestOfControllerData = (Value)
#define Can_SetRamCheckTransitionRequestOfControllerData(Index, Value) Can_GetControllerDataOfPCConfig()[(Index)].RamCheckTransitionRequestOfControllerData = (Value)
#define Can_SetRxMsgBufferOfControllerData(Index, Value)              Can_GetControllerDataOfPCConfig()[(Index)].RxMsgBufferOfControllerData = (Value)
#define Can_SetStartModeRequestedOfControllerData(Index, Value)       Can_GetControllerDataOfPCConfig()[(Index)].StartModeRequestedOfControllerData = (Value)
/** 
  \}
*/ 

/** 
  \defgroup  CanPCHasMacros  Can Has Macros (PRE_COMPILE)
  \brief  These macros can be used to detect at runtime a deactivated piece of information. TRUE in the CONFIGURATION_VARIANT PRE-COMPILE, TRUE or FALSE in the CONFIGURATION_VARIANT POST-BUILD.
  \{
*/ 
#define Can_HasActiveSendObject()                                     (TRUE != FALSE)
#define Can_HasPduOfActiveSendObject()                                (TRUE != FALSE)
#define Can_HasStateOfActiveSendObject()                              (TRUE != FALSE)
#define Can_HasBaseDll_GeneratorVersion()                             (TRUE != FALSE)
#define Can_HasCanIfChannelId()                                       (TRUE != FALSE)
#define Can_HasControllerConfig()                                     (TRUE != FALSE)
#define Can_HasBaseAddressOfControllerConfig()                        (TRUE != FALSE)
#define Can_HasCanControllerDefaultBaudrateIdxOfControllerConfig()    (TRUE != FALSE)
#define Can_HasCanControllerDefaultBaudrateOfControllerConfig()       (TRUE != FALSE)
#define Can_HasHasCANFDBaudrateOfControllerConfig()                   (TRUE != FALSE)
#define Can_HasInterruptMask1OfControllerConfig()                     (TRUE != FALSE)
#define Can_HasInterruptMask2OfControllerConfig()                     (TRUE != FALSE)
#define Can_HasInterruptMask3OfControllerConfig()                     (TRUE != FALSE)
#define Can_HasMailboxRxBasicEndIdxOfControllerConfig()               (TRUE != FALSE)
#define Can_HasMailboxRxBasicLengthOfControllerConfig()               (TRUE != FALSE)
#define Can_HasMailboxRxBasicStartIdxOfControllerConfig()             (TRUE != FALSE)
#define Can_HasMailboxRxBasicUsedOfControllerConfig()                 (TRUE != FALSE)
#define Can_HasMailboxRxFullEndIdxOfControllerConfig()                (TRUE != FALSE)
#define Can_HasMailboxRxFullLengthOfControllerConfig()                (TRUE != FALSE)
#define Can_HasMailboxRxFullStartIdxOfControllerConfig()              (TRUE != FALSE)
#define Can_HasMailboxRxFullUsedOfControllerConfig()                  (TRUE != FALSE)
#define Can_HasMailboxTxBasicEndIdxOfControllerConfig()               (TRUE != FALSE)
#define Can_HasMailboxTxBasicLengthOfControllerConfig()               (TRUE != FALSE)
#define Can_HasMailboxTxBasicStartIdxOfControllerConfig()             (TRUE != FALSE)
#define Can_HasMailboxTxBasicUsedOfControllerConfig()                 (TRUE != FALSE)
#define Can_HasMailboxTxFullEndIdxOfControllerConfig()                (TRUE != FALSE)
#define Can_HasMailboxTxFullLengthOfControllerConfig()                (TRUE != FALSE)
#define Can_HasMailboxTxFullStartIdxOfControllerConfig()              (TRUE != FALSE)
#define Can_HasMailboxTxFullUsedOfControllerConfig()                  (TRUE != FALSE)
#define Can_HasNumberOfFiltersOfControllerConfig()                    (TRUE != FALSE)
#define Can_HasNumberOfFullConfigurableFiltersOfControllerConfig()    (TRUE != FALSE)
#define Can_HasNumberOfMaxMailboxesOfControllerConfig()               (TRUE != FALSE)
#define Can_HasRFFNOfControllerConfig()                               (TRUE != FALSE)
#define Can_HasRxBasicHwStartOfControllerConfig()                     (TRUE != FALSE)
#define Can_HasRxBasicHwStopOfControllerConfig()                      (TRUE != FALSE)
#define Can_HasRxFullHwStartOfControllerConfig()                      (TRUE != FALSE)
#define Can_HasRxFullHwStopOfControllerConfig()                       (TRUE != FALSE)
#define Can_HasTxBasicHwStartOfControllerConfig()                     (TRUE != FALSE)
#define Can_HasTxBasicHwStopOfControllerConfig()                      (TRUE != FALSE)
#define Can_HasTxFullHwStartOfControllerConfig()                      (TRUE != FALSE)
#define Can_HasTxFullHwStopOfControllerConfig()                       (TRUE != FALSE)
#define Can_HasUnusedHwStartOfControllerConfig()                      (TRUE != FALSE)
#define Can_HasUnusedHwStopOfControllerConfig()                       (TRUE != FALSE)
#define Can_HasControllerData()                                       (TRUE != FALSE)
#define Can_HasBusOffCounterOfControllerData()                        (TRUE != FALSE)
#define Can_HasBusOffTransitionRequestOfControllerData()              (TRUE != FALSE)
#define Can_HasCanInterruptCounterOfControllerData()                  (TRUE != FALSE)
#define Can_HasCanInterruptOldStatusOfControllerData()                (TRUE != FALSE)
#define Can_HasIsBusOffOfControllerData()                             (TRUE != FALSE)
#define Can_HasLastInitObjectOfControllerData()                       (TRUE != FALSE)
#define Can_HasLogStatusOfControllerData()                            (TRUE != FALSE)
#define Can_HasLoopTimeoutOfControllerData()                          (TRUE != FALSE)
#define Can_HasModeTransitionRequestOfControllerData()                (TRUE != FALSE)
#define Can_HasRamCheckTransitionRequestOfControllerData()            (TRUE != FALSE)
#define Can_HasRxMsgBufferOfControllerData()                          (TRUE != FALSE)
#define Can_HasStartModeRequestedOfControllerData()                   (TRUE != FALSE)
#define Can_HasInitBasicCan()                                         (TRUE != FALSE)
#define Can_HasInitCodeOfInitBasicCan()                               (TRUE != FALSE)
#define Can_HasInitMaskOfInitBasicCan()                               (TRUE != FALSE)
#define Can_HasInitBasicCanIndex()                                    (TRUE != FALSE)
#define Can_HasInitObject()                                           (TRUE != FALSE)
#define Can_HasCBTOfInitObject()                                      (TRUE != FALSE)
#define Can_HasControl1OfInitObject()                                 (TRUE != FALSE)
#define Can_HasInitObjectBaudrate()                                   (TRUE != FALSE)
#define Can_HasInitObjectFD()                                         (TRUE != FALSE)
#define Can_HasFDCBTOfInitObjectFD()                                  (TRUE != FALSE)
#define Can_HasFDCTRLOfInitObjectFD()                                 (TRUE != FALSE)
#define Can_HasInitObjectFdBrsConfig()                                (TRUE != FALSE)
#define Can_HasInitObjectStartIndex()                                 (TRUE != FALSE)
#define Can_HasMailbox()                                              (TRUE != FALSE)
#define Can_HasActiveSendObjectOfMailbox()                            (TRUE != FALSE)
#define Can_HasControllerConfigIdxOfMailbox()                         (TRUE != FALSE)
#define Can_HasHwHandleOfMailbox()                                    (TRUE != FALSE)
#define Can_HasIDValueOfMailbox()                                     (TRUE != FALSE)
#define Can_HasMailboxSizeOfMailbox()                                 (TRUE != FALSE)
#define Can_HasMailboxTypeOfMailbox()                                 (TRUE != FALSE)
#define Can_HasMaxDataLenOfMailbox()                                  (TRUE != FALSE)
#define Can_HasMemorySectionsIndexOfMailbox()                         (TRUE != FALSE)
#define Can_HasMemorySectionInfo()                                    (TRUE != FALSE)
#define Can_HasMemorySectionStartOfMemorySectionInfo()                (TRUE != FALSE)
#define Can_HasMemoryStartAddressOfMemorySectionInfo()                (TRUE != FALSE)
#define Can_HasMemorySectionObjects()                                 (TRUE != FALSE)
#define Can_HasHwHandleOfMemorySectionObjects()                       (TRUE != FALSE)
#define Can_HasMailboxElementOfMemorySectionObjects()                 (TRUE != FALSE)
#define Can_HasMailboxHandleOfMemorySectionObjects()                  (TRUE != FALSE)
#define Can_HasPlatformDll_GeneratorVersion()                         (TRUE != FALSE)
#define Can_HasSizeOfActiveSendObject()                               (TRUE != FALSE)
#define Can_HasSizeOfCanIfChannelId()                                 (TRUE != FALSE)
#define Can_HasSizeOfControllerConfig()                               (TRUE != FALSE)
#define Can_HasSizeOfControllerData()                                 (TRUE != FALSE)
#define Can_HasSizeOfInitBasicCan()                                   (TRUE != FALSE)
#define Can_HasSizeOfInitBasicCanIndex()                              (TRUE != FALSE)
#define Can_HasSizeOfInitObject()                                     (TRUE != FALSE)
#define Can_HasSizeOfInitObjectBaudrate()                             (TRUE != FALSE)
#define Can_HasSizeOfInitObjectFD()                                   (TRUE != FALSE)
#define Can_HasSizeOfInitObjectFdBrsConfig()                          (TRUE != FALSE)
#define Can_HasSizeOfInitObjectStartIndex()                           (TRUE != FALSE)
#define Can_HasSizeOfMailbox()                                        (TRUE != FALSE)
#define Can_HasSizeOfMemorySectionInfo()                              (TRUE != FALSE)
#define Can_HasSizeOfMemorySectionObjects()                           (TRUE != FALSE)
#define Can_HasPCConfig()                                             (TRUE != FALSE)
#define Can_HasActiveSendObjectOfPCConfig()                           (TRUE != FALSE)
#define Can_HasBaseDll_GeneratorVersionOfPCConfig()                   (TRUE != FALSE)
#define Can_HasCanIfChannelIdOfPCConfig()                             (TRUE != FALSE)
#define Can_HasControllerConfigOfPCConfig()                           (TRUE != FALSE)
#define Can_HasControllerDataOfPCConfig()                             (TRUE != FALSE)
#define Can_HasInitBasicCanIndexOfPCConfig()                          (TRUE != FALSE)
#define Can_HasInitBasicCanOfPCConfig()                               (TRUE != FALSE)
#define Can_HasInitObjectBaudrateOfPCConfig()                         (TRUE != FALSE)
#define Can_HasInitObjectFDOfPCConfig()                               (TRUE != FALSE)
#define Can_HasInitObjectFdBrsConfigOfPCConfig()                      (TRUE != FALSE)
#define Can_HasInitObjectOfPCConfig()                                 (TRUE != FALSE)
#define Can_HasInitObjectStartIndexOfPCConfig()                       (TRUE != FALSE)
#define Can_HasMailboxOfPCConfig()                                    (TRUE != FALSE)
#define Can_HasMemorySectionInfoOfPCConfig()                          (TRUE != FALSE)
#define Can_HasMemorySectionObjectsOfPCConfig()                       (TRUE != FALSE)
#define Can_HasPlatformDll_GeneratorVersionOfPCConfig()               (TRUE != FALSE)
#define Can_HasSizeOfActiveSendObjectOfPCConfig()                     (TRUE != FALSE)
#define Can_HasSizeOfCanIfChannelIdOfPCConfig()                       (TRUE != FALSE)
#define Can_HasSizeOfControllerConfigOfPCConfig()                     (TRUE != FALSE)
#define Can_HasSizeOfControllerDataOfPCConfig()                       (TRUE != FALSE)
#define Can_HasSizeOfInitBasicCanIndexOfPCConfig()                    (TRUE != FALSE)
#define Can_HasSizeOfInitBasicCanOfPCConfig()                         (TRUE != FALSE)
#define Can_HasSizeOfInitObjectBaudrateOfPCConfig()                   (TRUE != FALSE)
#define Can_HasSizeOfInitObjectFDOfPCConfig()                         (TRUE != FALSE)
#define Can_HasSizeOfInitObjectFdBrsConfigOfPCConfig()                (TRUE != FALSE)
#define Can_HasSizeOfInitObjectOfPCConfig()                           (TRUE != FALSE)
#define Can_HasSizeOfInitObjectStartIndexOfPCConfig()                 (TRUE != FALSE)
#define Can_HasSizeOfMailboxOfPCConfig()                              (TRUE != FALSE)
#define Can_HasSizeOfMemorySectionInfoOfPCConfig()                    (TRUE != FALSE)
#define Can_HasSizeOfMemorySectionObjectsOfPCConfig()                 (TRUE != FALSE)
/** 
  \}
*/ 

/** 
  \defgroup  CanPCIncrementDataMacros  Can Increment Data Macros (PRE_COMPILE)
  \brief  These macros can be used to increment VAR data with numerical nature.
  \{
*/ 
#define Can_IncPduOfActiveSendObject(Index)                           Can_GetPduOfActiveSendObject(Index)++
#define Can_IncStateOfActiveSendObject(Index)                         Can_GetStateOfActiveSendObject(Index)++
#define Can_IncBusOffCounterOfControllerData(Index)                   Can_GetBusOffCounterOfControllerData(Index)++
#define Can_IncBusOffTransitionRequestOfControllerData(Index)         Can_GetBusOffTransitionRequestOfControllerData(Index)++
#define Can_IncCanInterruptCounterOfControllerData(Index)             Can_GetCanInterruptCounterOfControllerData(Index)++
#define Can_IncCanInterruptOldStatusOfControllerData(Index)           Can_GetCanInterruptOldStatusOfControllerData(Index)++
#define Can_IncLastInitObjectOfControllerData(Index)                  Can_GetLastInitObjectOfControllerData(Index)++
#define Can_IncLogStatusOfControllerData(Index)                       Can_GetLogStatusOfControllerData(Index)++
#define Can_IncLoopTimeoutOfControllerData(Index)                     Can_GetLoopTimeoutOfControllerData(Index)++
#define Can_IncModeTransitionRequestOfControllerData(Index)           Can_GetModeTransitionRequestOfControllerData(Index)++
#define Can_IncRamCheckTransitionRequestOfControllerData(Index)       Can_GetRamCheckTransitionRequestOfControllerData(Index)++
#define Can_IncRxMsgBufferOfControllerData(Index)                     Can_GetRxMsgBufferOfControllerData(Index)++
/** 
  \}
*/ 

/** 
  \defgroup  CanPCDecrementDataMacros  Can Decrement Data Macros (PRE_COMPILE)
  \brief  These macros can be used to decrement VAR data with numerical nature.
  \{
*/ 
#define Can_DecPduOfActiveSendObject(Index)                           Can_GetPduOfActiveSendObject(Index)--
#define Can_DecStateOfActiveSendObject(Index)                         Can_GetStateOfActiveSendObject(Index)--
#define Can_DecBusOffCounterOfControllerData(Index)                   Can_GetBusOffCounterOfControllerData(Index)--
#define Can_DecBusOffTransitionRequestOfControllerData(Index)         Can_GetBusOffTransitionRequestOfControllerData(Index)--
#define Can_DecCanInterruptCounterOfControllerData(Index)             Can_GetCanInterruptCounterOfControllerData(Index)--
#define Can_DecCanInterruptOldStatusOfControllerData(Index)           Can_GetCanInterruptOldStatusOfControllerData(Index)--
#define Can_DecLastInitObjectOfControllerData(Index)                  Can_GetLastInitObjectOfControllerData(Index)--
#define Can_DecLogStatusOfControllerData(Index)                       Can_GetLogStatusOfControllerData(Index)--
#define Can_DecLoopTimeoutOfControllerData(Index)                     Can_GetLoopTimeoutOfControllerData(Index)--
#define Can_DecModeTransitionRequestOfControllerData(Index)           Can_GetModeTransitionRequestOfControllerData(Index)--
#define Can_DecRamCheckTransitionRequestOfControllerData(Index)       Can_GetRamCheckTransitionRequestOfControllerData(Index)--
#define Can_DecRxMsgBufferOfControllerData(Index)                     Can_GetRxMsgBufferOfControllerData(Index)--
/** 
  \}
*/ 

  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/

/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/

/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  CanPCIterableTypes  Can Iterable Types (PRE_COMPILE)
  \brief  These type definitions are used to iterate over an array with least processor cycles for variable access as possible.
  \{
*/ 
/**   \brief  type used to iterate Can_ActiveSendObject */
typedef uint8_least Can_ActiveSendObjectIterType;

/**   \brief  type used to iterate Can_CanIfChannelId */
typedef uint8_least Can_CanIfChannelIdIterType;

/**   \brief  type used to iterate Can_ControllerConfig */
typedef uint8_least Can_ControllerConfigIterType;

/**   \brief  type used to iterate Can_InitBasicCan */
typedef uint8_least Can_InitBasicCanIterType;

/**   \brief  type used to iterate Can_InitBasicCanIndex */
typedef uint8_least Can_InitBasicCanIndexIterType;

/**   \brief  type used to iterate Can_InitObject */
typedef uint8_least Can_InitObjectIterType;

/**   \brief  type used to iterate Can_InitObjectBaudrate */
typedef uint8_least Can_InitObjectBaudrateIterType;

/**   \brief  type used to iterate Can_InitObjectFD */
typedef uint8_least Can_InitObjectFDIterType;

/**   \brief  type used to iterate Can_InitObjectFdBrsConfig */
typedef uint8_least Can_InitObjectFdBrsConfigIterType;

/**   \brief  type used to iterate Can_InitObjectStartIndex */
typedef uint8_least Can_InitObjectStartIndexIterType;

/**   \brief  type used to iterate Can_Mailbox */
typedef uint8_least Can_MailboxIterType;

/**   \brief  type used to iterate Can_MemorySectionInfo */
typedef uint8_least Can_MemorySectionInfoIterType;

/**   \brief  type used to iterate Can_MemorySectionObjects */
typedef uint16_least Can_MemorySectionObjectsIterType;

/** 
  \}
*/ 

/** 
  \defgroup  CanPCIterableTypesWithSizeRelations  Can Iterable Types With Size Relations (PRE_COMPILE)
  \brief  These type definitions are used to iterate over a VAR based array with the same iterator as the related CONST array.
  \{
*/ 
/**   \brief  type used to iterate Can_ControllerData */
typedef Can_ControllerConfigIterType Can_ControllerDataIterType;

/** 
  \}
*/ 

/** 
  \defgroup  CanPCValueTypes  Can Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value based data representations.
  \{
*/ 
/**   \brief  value based type definition for Can_PduOfActiveSendObject */
typedef PduIdType Can_PduOfActiveSendObjectType;

/**   \brief  value based type definition for Can_StateOfActiveSendObject */
typedef uint16 Can_StateOfActiveSendObjectType;

/**   \brief  value based type definition for Can_BaseDll_GeneratorVersion */
typedef uint16 Can_BaseDll_GeneratorVersionType;

/**   \brief  value based type definition for Can_CanIfChannelId */
typedef uint8 Can_CanIfChannelIdType;

/**   \brief  value based type definition for Can_BaseAddressOfControllerConfig */
typedef uint32 Can_BaseAddressOfControllerConfigType;

/**   \brief  value based type definition for Can_CanControllerDefaultBaudrateIdxOfControllerConfig */
typedef uint8 Can_CanControllerDefaultBaudrateIdxOfControllerConfigType;

/**   \brief  value based type definition for Can_CanControllerDefaultBaudrateOfControllerConfig */
typedef uint16 Can_CanControllerDefaultBaudrateOfControllerConfigType;

/**   \brief  value based type definition for Can_HasCANFDBaudrateOfControllerConfig */
typedef boolean Can_HasCANFDBaudrateOfControllerConfigType;

/**   \brief  value based type definition for Can_InterruptMask1OfControllerConfig */
typedef uint32 Can_InterruptMask1OfControllerConfigType;

/**   \brief  value based type definition for Can_InterruptMask2OfControllerConfig */
typedef uint32 Can_InterruptMask2OfControllerConfigType;

/**   \brief  value based type definition for Can_InterruptMask3OfControllerConfig */
typedef uint16 Can_InterruptMask3OfControllerConfigType;

/**   \brief  value based type definition for Can_MailboxRxBasicEndIdxOfControllerConfig */
typedef uint8 Can_MailboxRxBasicEndIdxOfControllerConfigType;

/**   \brief  value based type definition for Can_MailboxRxBasicLengthOfControllerConfig */
typedef uint8 Can_MailboxRxBasicLengthOfControllerConfigType;

/**   \brief  value based type definition for Can_MailboxRxBasicStartIdxOfControllerConfig */
typedef uint8 Can_MailboxRxBasicStartIdxOfControllerConfigType;

/**   \brief  value based type definition for Can_MailboxRxBasicUsedOfControllerConfig */
typedef boolean Can_MailboxRxBasicUsedOfControllerConfigType;

/**   \brief  value based type definition for Can_MailboxRxFullEndIdxOfControllerConfig */
typedef uint8 Can_MailboxRxFullEndIdxOfControllerConfigType;

/**   \brief  value based type definition for Can_MailboxRxFullLengthOfControllerConfig */
typedef uint8 Can_MailboxRxFullLengthOfControllerConfigType;

/**   \brief  value based type definition for Can_MailboxRxFullStartIdxOfControllerConfig */
typedef uint8 Can_MailboxRxFullStartIdxOfControllerConfigType;

/**   \brief  value based type definition for Can_MailboxRxFullUsedOfControllerConfig */
typedef boolean Can_MailboxRxFullUsedOfControllerConfigType;

/**   \brief  value based type definition for Can_MailboxTxBasicEndIdxOfControllerConfig */
typedef uint8 Can_MailboxTxBasicEndIdxOfControllerConfigType;

/**   \brief  value based type definition for Can_MailboxTxBasicLengthOfControllerConfig */
typedef uint8 Can_MailboxTxBasicLengthOfControllerConfigType;

/**   \brief  value based type definition for Can_MailboxTxBasicStartIdxOfControllerConfig */
typedef uint8 Can_MailboxTxBasicStartIdxOfControllerConfigType;

/**   \brief  value based type definition for Can_MailboxTxBasicUsedOfControllerConfig */
typedef boolean Can_MailboxTxBasicUsedOfControllerConfigType;

/**   \brief  value based type definition for Can_MailboxTxFullEndIdxOfControllerConfig */
typedef uint8 Can_MailboxTxFullEndIdxOfControllerConfigType;

/**   \brief  value based type definition for Can_MailboxTxFullLengthOfControllerConfig */
typedef uint8 Can_MailboxTxFullLengthOfControllerConfigType;

/**   \brief  value based type definition for Can_MailboxTxFullStartIdxOfControllerConfig */
typedef uint8 Can_MailboxTxFullStartIdxOfControllerConfigType;

/**   \brief  value based type definition for Can_MailboxTxFullUsedOfControllerConfig */
typedef boolean Can_MailboxTxFullUsedOfControllerConfigType;

/**   \brief  value based type definition for Can_NumberOfFiltersOfControllerConfig */
typedef uint8 Can_NumberOfFiltersOfControllerConfigType;

/**   \brief  value based type definition for Can_NumberOfFullConfigurableFiltersOfControllerConfig */
typedef uint8 Can_NumberOfFullConfigurableFiltersOfControllerConfigType;

/**   \brief  value based type definition for Can_NumberOfMaxMailboxesOfControllerConfig */
typedef uint8 Can_NumberOfMaxMailboxesOfControllerConfigType;

/**   \brief  value based type definition for Can_RFFNOfControllerConfig */
typedef uint8 Can_RFFNOfControllerConfigType;

/**   \brief  value based type definition for Can_RxBasicHwStartOfControllerConfig */
typedef uint8 Can_RxBasicHwStartOfControllerConfigType;

/**   \brief  value based type definition for Can_RxBasicHwStopOfControllerConfig */
typedef uint8 Can_RxBasicHwStopOfControllerConfigType;

/**   \brief  value based type definition for Can_RxFullHwStartOfControllerConfig */
typedef uint8 Can_RxFullHwStartOfControllerConfigType;

/**   \brief  value based type definition for Can_RxFullHwStopOfControllerConfig */
typedef uint8 Can_RxFullHwStopOfControllerConfigType;

/**   \brief  value based type definition for Can_TxBasicHwStartOfControllerConfig */
typedef uint8 Can_TxBasicHwStartOfControllerConfigType;

/**   \brief  value based type definition for Can_TxBasicHwStopOfControllerConfig */
typedef uint8 Can_TxBasicHwStopOfControllerConfigType;

/**   \brief  value based type definition for Can_TxFullHwStartOfControllerConfig */
typedef uint8 Can_TxFullHwStartOfControllerConfigType;

/**   \brief  value based type definition for Can_TxFullHwStopOfControllerConfig */
typedef uint8 Can_TxFullHwStopOfControllerConfigType;

/**   \brief  value based type definition for Can_UnusedHwStartOfControllerConfig */
typedef uint8 Can_UnusedHwStartOfControllerConfigType;

/**   \brief  value based type definition for Can_UnusedHwStopOfControllerConfig */
typedef uint8 Can_UnusedHwStopOfControllerConfigType;

/**   \brief  value based type definition for Can_BusOffCounterOfControllerData */
typedef uint8 Can_BusOffCounterOfControllerDataType;

/**   \brief  value based type definition for Can_BusOffTransitionRequestOfControllerData */
typedef uint8 Can_BusOffTransitionRequestOfControllerDataType;

/**   \brief  value based type definition for Can_CanInterruptCounterOfControllerData */
typedef uint8 Can_CanInterruptCounterOfControllerDataType;

/**   \brief  value based type definition for Can_IsBusOffOfControllerData */
typedef boolean Can_IsBusOffOfControllerDataType;

/**   \brief  value based type definition for Can_LastInitObjectOfControllerData */
typedef uint8 Can_LastInitObjectOfControllerDataType;

/**   \brief  value based type definition for Can_LogStatusOfControllerData */
typedef uint8 Can_LogStatusOfControllerDataType;

/**   \brief  value based type definition for Can_ModeTransitionRequestOfControllerData */
typedef uint8 Can_ModeTransitionRequestOfControllerDataType;

/**   \brief  value based type definition for Can_RamCheckTransitionRequestOfControllerData */
typedef uint8 Can_RamCheckTransitionRequestOfControllerDataType;

/**   \brief  value based type definition for Can_StartModeRequestedOfControllerData */
typedef boolean Can_StartModeRequestedOfControllerDataType;

/**   \brief  value based type definition for Can_InitCodeOfInitBasicCan */
typedef uint32 Can_InitCodeOfInitBasicCanType;

/**   \brief  value based type definition for Can_InitMaskOfInitBasicCan */
typedef uint32 Can_InitMaskOfInitBasicCanType;

/**   \brief  value based type definition for Can_InitBasicCanIndex */
typedef uint8 Can_InitBasicCanIndexType;

/**   \brief  value based type definition for Can_CBTOfInitObject */
typedef uint32 Can_CBTOfInitObjectType;

/**   \brief  value based type definition for Can_Control1OfInitObject */
typedef uint32 Can_Control1OfInitObjectType;

/**   \brief  value based type definition for Can_InitObjectBaudrate */
typedef uint16 Can_InitObjectBaudrateType;

/**   \brief  value based type definition for Can_FDCBTOfInitObjectFD */
typedef uint16 Can_FDCBTOfInitObjectFDType;

/**   \brief  value based type definition for Can_FDCTRLOfInitObjectFD */
typedef uint32 Can_FDCTRLOfInitObjectFDType;

/**   \brief  value based type definition for Can_InitObjectFdBrsConfig */
typedef uint8 Can_InitObjectFdBrsConfigType;

/**   \brief  value based type definition for Can_InitObjectStartIndex */
typedef uint8 Can_InitObjectStartIndexType;

/**   \brief  value based type definition for Can_ActiveSendObjectOfMailbox */
typedef uint8 Can_ActiveSendObjectOfMailboxType;

/**   \brief  value based type definition for Can_ControllerConfigIdxOfMailbox */
typedef uint8 Can_ControllerConfigIdxOfMailboxType;

/**   \brief  value based type definition for Can_HwHandleOfMailbox */
typedef uint8 Can_HwHandleOfMailboxType;

/**   \brief  value based type definition for Can_IDValueOfMailbox */
typedef uint32 Can_IDValueOfMailboxType;

/**   \brief  value based type definition for Can_MailboxSizeOfMailbox */
typedef uint8 Can_MailboxSizeOfMailboxType;

/**   \brief  value based type definition for Can_MailboxTypeOfMailbox */
typedef uint8 Can_MailboxTypeOfMailboxType;

/**   \brief  value based type definition for Can_MaxDataLenOfMailbox */
typedef uint8 Can_MaxDataLenOfMailboxType;

/**   \brief  value based type definition for Can_MemorySectionsIndexOfMailbox */
typedef uint16 Can_MemorySectionsIndexOfMailboxType;

/**   \brief  value based type definition for Can_MemorySectionStartOfMemorySectionInfo */
typedef uint16 Can_MemorySectionStartOfMemorySectionInfoType;

/**   \brief  value based type definition for Can_MemoryStartAddressOfMemorySectionInfo */
typedef uint32 Can_MemoryStartAddressOfMemorySectionInfoType;

/**   \brief  value based type definition for Can_HwHandleOfMemorySectionObjects */
typedef uint8 Can_HwHandleOfMemorySectionObjectsType;

/**   \brief  value based type definition for Can_MailboxElementOfMemorySectionObjects */
typedef uint8 Can_MailboxElementOfMemorySectionObjectsType;

/**   \brief  value based type definition for Can_MailboxHandleOfMemorySectionObjects */
typedef uint8 Can_MailboxHandleOfMemorySectionObjectsType;

/**   \brief  value based type definition for Can_PlatformDll_GeneratorVersion */
typedef uint16 Can_PlatformDll_GeneratorVersionType;

/**   \brief  value based type definition for Can_SizeOfActiveSendObject */
typedef uint8 Can_SizeOfActiveSendObjectType;

/**   \brief  value based type definition for Can_SizeOfCanIfChannelId */
typedef uint8 Can_SizeOfCanIfChannelIdType;

/**   \brief  value based type definition for Can_SizeOfControllerConfig */
typedef uint8 Can_SizeOfControllerConfigType;

/**   \brief  value based type definition for Can_SizeOfControllerData */
typedef uint8 Can_SizeOfControllerDataType;

/**   \brief  value based type definition for Can_SizeOfInitBasicCan */
typedef uint8 Can_SizeOfInitBasicCanType;

/**   \brief  value based type definition for Can_SizeOfInitBasicCanIndex */
typedef uint8 Can_SizeOfInitBasicCanIndexType;

/**   \brief  value based type definition for Can_SizeOfInitObject */
typedef uint8 Can_SizeOfInitObjectType;

/**   \brief  value based type definition for Can_SizeOfInitObjectBaudrate */
typedef uint8 Can_SizeOfInitObjectBaudrateType;

/**   \brief  value based type definition for Can_SizeOfInitObjectFD */
typedef uint8 Can_SizeOfInitObjectFDType;

/**   \brief  value based type definition for Can_SizeOfInitObjectFdBrsConfig */
typedef uint8 Can_SizeOfInitObjectFdBrsConfigType;

/**   \brief  value based type definition for Can_SizeOfInitObjectStartIndex */
typedef uint8 Can_SizeOfInitObjectStartIndexType;

/**   \brief  value based type definition for Can_SizeOfMailbox */
typedef uint8 Can_SizeOfMailboxType;

/**   \brief  value based type definition for Can_SizeOfMemorySectionInfo */
typedef uint8 Can_SizeOfMemorySectionInfoType;

/**   \brief  value based type definition for Can_SizeOfMemorySectionObjects */
typedef uint16 Can_SizeOfMemorySectionObjectsType;

/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  CanPCStructTypes  Can Struct Types (PRE_COMPILE)
  \brief  These type definitions are used for structured data representations.
  \{
*/ 
/**   \brief  type used in Can_ActiveSendObject */
typedef struct sCan_ActiveSendObjectType
{
  Can_PduOfActiveSendObjectType PduOfActiveSendObject;  /**< buffered PduId for confirmation or cancellation */
  Can_StateOfActiveSendObjectType StateOfActiveSendObject;  /**< send state like cancelled or send activ */
} Can_ActiveSendObjectType;

/**   \brief  type used in Can_ControllerConfig */
typedef struct sCan_ControllerConfigType
{
  Can_BaseAddressOfControllerConfigType BaseAddressOfControllerConfig;
  Can_InterruptMask1OfControllerConfigType InterruptMask1OfControllerConfig;
  Can_InterruptMask2OfControllerConfigType InterruptMask2OfControllerConfig;
  Can_CanControllerDefaultBaudrateOfControllerConfigType CanControllerDefaultBaudrateOfControllerConfig;
  Can_InterruptMask3OfControllerConfigType InterruptMask3OfControllerConfig;
  Can_HasCANFDBaudrateOfControllerConfigType HasCANFDBaudrateOfControllerConfig;
  Can_CanControllerDefaultBaudrateIdxOfControllerConfigType CanControllerDefaultBaudrateIdxOfControllerConfig;
  Can_MailboxRxBasicEndIdxOfControllerConfigType MailboxRxBasicEndIdxOfControllerConfig;  /**< the end index of the 0:n relation pointing to Can_Mailbox */
  Can_MailboxRxBasicLengthOfControllerConfigType MailboxRxBasicLengthOfControllerConfig;  /**< the number of relations pointing to Can_Mailbox */
  Can_MailboxRxBasicStartIdxOfControllerConfigType MailboxRxBasicStartIdxOfControllerConfig;  /**< the start index of the 0:n relation pointing to Can_Mailbox */
  Can_MailboxRxFullEndIdxOfControllerConfigType MailboxRxFullEndIdxOfControllerConfig;  /**< the end index of the 0:n relation pointing to Can_Mailbox */
  Can_MailboxRxFullLengthOfControllerConfigType MailboxRxFullLengthOfControllerConfig;  /**< the number of relations pointing to Can_Mailbox */
  Can_MailboxRxFullStartIdxOfControllerConfigType MailboxRxFullStartIdxOfControllerConfig;  /**< the start index of the 0:n relation pointing to Can_Mailbox */
  Can_MailboxTxBasicEndIdxOfControllerConfigType MailboxTxBasicEndIdxOfControllerConfig;  /**< the end index of the 0:n relation pointing to Can_Mailbox */
  Can_MailboxTxBasicLengthOfControllerConfigType MailboxTxBasicLengthOfControllerConfig;  /**< the number of relations pointing to Can_Mailbox */
  Can_MailboxTxBasicStartIdxOfControllerConfigType MailboxTxBasicStartIdxOfControllerConfig;  /**< the start index of the 0:n relation pointing to Can_Mailbox */
  Can_MailboxTxFullEndIdxOfControllerConfigType MailboxTxFullEndIdxOfControllerConfig;  /**< the end index of the 0:n relation pointing to Can_Mailbox */
  Can_MailboxTxFullLengthOfControllerConfigType MailboxTxFullLengthOfControllerConfig;  /**< the number of relations pointing to Can_Mailbox */
  Can_MailboxTxFullStartIdxOfControllerConfigType MailboxTxFullStartIdxOfControllerConfig;  /**< the start index of the 0:n relation pointing to Can_Mailbox */
  Can_NumberOfFiltersOfControllerConfigType NumberOfFiltersOfControllerConfig;
  Can_NumberOfFullConfigurableFiltersOfControllerConfigType NumberOfFullConfigurableFiltersOfControllerConfig;
  Can_NumberOfMaxMailboxesOfControllerConfigType NumberOfMaxMailboxesOfControllerConfig;
  Can_RFFNOfControllerConfigType RFFNOfControllerConfig;
  Can_RxBasicHwStartOfControllerConfigType RxBasicHwStartOfControllerConfig;
  Can_RxBasicHwStopOfControllerConfigType RxBasicHwStopOfControllerConfig;
  Can_RxFullHwStartOfControllerConfigType RxFullHwStartOfControllerConfig;
  Can_RxFullHwStopOfControllerConfigType RxFullHwStopOfControllerConfig;
  Can_TxBasicHwStartOfControllerConfigType TxBasicHwStartOfControllerConfig;
  Can_TxBasicHwStopOfControllerConfigType TxBasicHwStopOfControllerConfig;
  Can_TxFullHwStartOfControllerConfigType TxFullHwStartOfControllerConfig;
  Can_TxFullHwStopOfControllerConfigType TxFullHwStopOfControllerConfig;
  Can_UnusedHwStartOfControllerConfigType UnusedHwStartOfControllerConfig;
  Can_UnusedHwStopOfControllerConfigType UnusedHwStopOfControllerConfig;
} Can_ControllerConfigType;

/**   \brief  type used in Can_ControllerData */
typedef struct sCan_ControllerDataType
{
  Can_BusOffCounterOfControllerDataType BusOffCounterOfControllerData;  /**< This variable stores the busoff recovery timeout counter. */
  Can_BusOffTransitionRequestOfControllerDataType BusOffTransitionRequestOfControllerData;  /**< CAN state request for each controller: ContinueBusOffRecovery=0x00, FinishBusOffRecovery=0x01 */
  Can_CanInterruptCounterOfControllerDataType CanInterruptCounterOfControllerData;  /**< CAN interrupt disable counter for each controller */
  Can_IsBusOffOfControllerDataType IsBusOffOfControllerData;  /**< CAN state for each controller: busoff occur */
  Can_LastInitObjectOfControllerDataType LastInitObjectOfControllerData;  /**< last set baudrate for reinit */
  Can_LogStatusOfControllerDataType LogStatusOfControllerData;  /**< CAN state for each controller: UNINIT=0x00, START=0x01, STOP=0x02, INIT=0x04, INCONSISTENT=0x08, WARNING =0x10, PASSIVE=0x20, BUSOFF=0x40, SLEEP=0x80 */
  Can_ModeTransitionRequestOfControllerDataType ModeTransitionRequestOfControllerData;  /**< CAN state request for each controller: INIT=0x00, SLEEP=0x01, WAKEUP=0x02, STOP+INIT=0x03, START=0x04, START+INIT=0x05, NONE=0x06 */
  Can_RamCheckTransitionRequestOfControllerDataType RamCheckTransitionRequestOfControllerData;  /**< CAN state request for each controller: kCanSuppressRamCheck=0x01, kCanExecuteRamCheck=0x00 */
  Can_StartModeRequestedOfControllerDataType StartModeRequestedOfControllerData;  /**< This variable stores if the start mode is of a special CAN Controller is already requested in the busoff recovery process. */
  tCanLLCanIntOld CanInterruptOldStatusOfControllerData;  /**< last CAN interrupt status for restore interrupt for each controller */
  Can_LoopTimeout_dim_type LoopTimeoutOfControllerData;  /**< hw loop timeout for each controller */
  tCanRxMsgBuffer RxMsgBufferOfControllerData;  /**< This variable stores received values (ID, DLC, DATA) in the reception process. */
} Can_ControllerDataType;

/**   \brief  type used in Can_InitBasicCan */
typedef struct sCan_InitBasicCanType
{
  Can_InitCodeOfInitBasicCanType InitCodeOfInitBasicCan;
  Can_InitMaskOfInitBasicCanType InitMaskOfInitBasicCan;
} Can_InitBasicCanType;

/**   \brief  type used in Can_InitObject */
typedef struct sCan_InitObjectType
{
  Can_CBTOfInitObjectType CBTOfInitObject;
  Can_Control1OfInitObjectType Control1OfInitObject;
} Can_InitObjectType;

/**   \brief  type used in Can_InitObjectFD */
typedef struct sCan_InitObjectFDType
{
  Can_FDCTRLOfInitObjectFDType FDCTRLOfInitObjectFD;
  Can_FDCBTOfInitObjectFDType FDCBTOfInitObjectFD;
} Can_InitObjectFDType;

/**   \brief  type used in Can_Mailbox */
typedef struct sCan_MailboxType
{
  Can_IDValueOfMailboxType IDValueOfMailbox;
  Can_MemorySectionsIndexOfMailboxType MemorySectionsIndexOfMailbox;
  Can_ActiveSendObjectOfMailboxType ActiveSendObjectOfMailbox;
  Can_ControllerConfigIdxOfMailboxType ControllerConfigIdxOfMailbox;  /**< the index of the 1:1 relation pointing to Can_ControllerConfig */
  Can_HwHandleOfMailboxType HwHandleOfMailbox;
  Can_MailboxSizeOfMailboxType MailboxSizeOfMailbox;
  Can_MailboxTypeOfMailboxType MailboxTypeOfMailbox;
  Can_MaxDataLenOfMailboxType MaxDataLenOfMailbox;
} Can_MailboxType;

/**   \brief  type used in Can_MemorySectionInfo */
typedef struct sCan_MemorySectionInfoType
{
  Can_MemoryStartAddressOfMemorySectionInfoType MemoryStartAddressOfMemorySectionInfo;
  Can_MemorySectionStartOfMemorySectionInfoType MemorySectionStartOfMemorySectionInfo;
} Can_MemorySectionInfoType;

/**   \brief  type used in Can_MemorySectionObjects */
typedef struct sCan_MemorySectionObjectsType
{
  Can_HwHandleOfMemorySectionObjectsType HwHandleOfMemorySectionObjects;
  Can_MailboxElementOfMemorySectionObjectsType MailboxElementOfMemorySectionObjects;
  Can_MailboxHandleOfMemorySectionObjectsType MailboxHandleOfMemorySectionObjects;
} Can_MemorySectionObjectsType;

/** 
  \}
*/ 

/** 
  \defgroup  CanPCRootPointerTypes  Can Root Pointer Types (PRE_COMPILE)
  \brief  These type definitions are used to point from the config root to symbol instances.
  \{
*/ 
/**   \brief  type used to point to Can_ActiveSendObject */
typedef P2VAR(Can_ActiveSendObjectType, TYPEDEF, CAN_VAR_NOINIT) Can_ActiveSendObjectPtrType;

/**   \brief  type used to point to Can_CanIfChannelId */
typedef P2CONST(Can_CanIfChannelIdType, TYPEDEF, CAN_CONST) Can_CanIfChannelIdPtrType;

/**   \brief  type used to point to Can_ControllerConfig */
typedef P2CONST(Can_ControllerConfigType, TYPEDEF, CAN_CONST) Can_ControllerConfigPtrType;

/**   \brief  type used to point to Can_ControllerData */
typedef P2VAR(Can_ControllerDataType, TYPEDEF, CAN_VAR_NOINIT) Can_ControllerDataPtrType;

/**   \brief  type used to point to Can_InitBasicCan */
typedef P2CONST(Can_InitBasicCanType, TYPEDEF, CAN_CONST) Can_InitBasicCanPtrType;

/**   \brief  type used to point to Can_InitBasicCanIndex */
typedef P2CONST(Can_InitBasicCanIndexType, TYPEDEF, CAN_CONST) Can_InitBasicCanIndexPtrType;

/**   \brief  type used to point to Can_InitObject */
typedef P2CONST(Can_InitObjectType, TYPEDEF, CAN_CONST) Can_InitObjectPtrType;

/**   \brief  type used to point to Can_InitObjectBaudrate */
typedef P2CONST(Can_InitObjectBaudrateType, TYPEDEF, CAN_CONST) Can_InitObjectBaudratePtrType;

/**   \brief  type used to point to Can_InitObjectFD */
typedef P2CONST(Can_InitObjectFDType, TYPEDEF, CAN_CONST) Can_InitObjectFDPtrType;

/**   \brief  type used to point to Can_InitObjectFdBrsConfig */
typedef P2CONST(Can_InitObjectFdBrsConfigType, TYPEDEF, CAN_CONST) Can_InitObjectFdBrsConfigPtrType;

/**   \brief  type used to point to Can_InitObjectStartIndex */
typedef P2CONST(Can_InitObjectStartIndexType, TYPEDEF, CAN_CONST) Can_InitObjectStartIndexPtrType;

/**   \brief  type used to point to Can_Mailbox */
typedef P2CONST(Can_MailboxType, TYPEDEF, CAN_CONST) Can_MailboxPtrType;

/**   \brief  type used to point to Can_MemorySectionInfo */
typedef P2CONST(Can_MemorySectionInfoType, TYPEDEF, CAN_CONST) Can_MemorySectionInfoPtrType;

/**   \brief  type used to point to Can_MemorySectionObjects */
typedef P2CONST(Can_MemorySectionObjectsType, TYPEDEF, CAN_CONST) Can_MemorySectionObjectsPtrType;

/** 
  \}
*/ 

/** 
  \defgroup  CanPCRootValueTypes  Can Root Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value representations in root arrays.
  \{
*/ 
/**   \brief  type used in Can_PCConfig */
typedef struct sCan_PCConfigType
{
  uint8 Can_PCConfigNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} Can_PCConfigType;

typedef Can_PCConfigType Can_ConfigType;  /**< A structure type is present for data in each configuration class. This typedef redefines the probably different name to the specified one. */

/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  Can_CanIfChannelId
**********************************************************************************************************************/
/** 
  \var    Can_CanIfChannelId
  \brief  indirection table Can to CanIf controller ID
*/ 
#define CAN_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(Can_CanIfChannelIdType, CAN_CONST) Can_CanIfChannelId[6];
#define CAN_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_ControllerConfig
**********************************************************************************************************************/
/** 
  \var    Can_ControllerConfig
  \brief  Global configuration for all controllers
  \details
  Element                            Description
  BaseAddress                    
  InterruptMask1                 
  InterruptMask2                 
  CanControllerDefaultBaudrate   
  InterruptMask3                 
  HasCANFDBaudrate               
  CanControllerDefaultBaudrateIdx
  MailboxRxBasicEndIdx               the end index of the 0:n relation pointing to Can_Mailbox
  MailboxRxBasicLength               the number of relations pointing to Can_Mailbox
  MailboxRxBasicStartIdx             the start index of the 0:n relation pointing to Can_Mailbox
  MailboxRxFullEndIdx                the end index of the 0:n relation pointing to Can_Mailbox
  MailboxRxFullLength                the number of relations pointing to Can_Mailbox
  MailboxRxFullStartIdx              the start index of the 0:n relation pointing to Can_Mailbox
  MailboxTxBasicEndIdx               the end index of the 0:n relation pointing to Can_Mailbox
  MailboxTxBasicLength               the number of relations pointing to Can_Mailbox
  MailboxTxBasicStartIdx             the start index of the 0:n relation pointing to Can_Mailbox
  MailboxTxFullEndIdx                the end index of the 0:n relation pointing to Can_Mailbox
  MailboxTxFullLength                the number of relations pointing to Can_Mailbox
  MailboxTxFullStartIdx              the start index of the 0:n relation pointing to Can_Mailbox
  NumberOfFilters                
  NumberOfFullConfigurableFilters
  NumberOfMaxMailboxes           
  RFFN                           
  RxBasicHwStart                 
  RxBasicHwStop                  
  RxFullHwStart                  
  RxFullHwStop                   
  TxBasicHwStart                 
  TxBasicHwStop                  
  TxFullHwStart                  
  TxFullHwStop                   
  UnusedHwStart                  
  UnusedHwStop                   
*/ 
#define CAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(Can_ControllerConfigType, CAN_CONST) Can_ControllerConfig[6];
#define CAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_InitBasicCan
**********************************************************************************************************************/
/** 
  \var    Can_InitBasicCan
  \brief  This table contains acceptance filter configuration values.
  \details
  Element     Description
  InitCode
  InitMask
*/ 
#define CAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(Can_InitBasicCanType, CAN_CONST) Can_InitBasicCan[26];
#define CAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_InitBasicCanIndex
**********************************************************************************************************************/
/** 
  \var    Can_InitBasicCanIndex
  \brief  This table contains start/stop indices for the Can_InitBasicCan table.
*/ 
#define CAN_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(Can_InitBasicCanIndexType, CAN_CONST) Can_InitBasicCanIndex[6];
#define CAN_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_InitObject
**********************************************************************************************************************/
/** 
  \var    Can_InitObject
  \brief  This table contains information about the init object: e.g. bustiming register contents.
  \details
  Element     Description
  CBT     
  Control1
*/ 
#define CAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(Can_InitObjectType, CAN_CONST) Can_InitObject[6];
#define CAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_InitObjectBaudrate
**********************************************************************************************************************/
/** 
  \var    Can_InitObjectBaudrate
  \brief  baudrates ('InitStruct' as index)
*/ 
#define CAN_START_SEC_CONST_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(Can_InitObjectBaudrateType, CAN_CONST) Can_InitObjectBaudrate[6];
#define CAN_STOP_SEC_CONST_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_InitObjectFD
**********************************************************************************************************************/
/** 
  \var    Can_InitObjectFD
  \brief  This table contains bittiming register values for the CAN-FD baudrate.
  \details
  Element    Description
  FDCTRL 
  FDCBT  
*/ 
#define CAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(Can_InitObjectFDType, CAN_CONST) Can_InitObjectFD[6];
#define CAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_InitObjectFdBrsConfig
**********************************************************************************************************************/
/** 
  \var    Can_InitObjectFdBrsConfig
  \brief  FD config ('BaudrateObject' as index)
*/ 
#define CAN_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(Can_InitObjectFdBrsConfigType, CAN_CONST) Can_InitObjectFdBrsConfig[6];
#define CAN_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_InitObjectStartIndex
**********************************************************************************************************************/
/** 
  \var    Can_InitObjectStartIndex
  \brief  Start index of 'InitStruct' / baudratesets (controllers as index)
*/ 
#define CAN_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(Can_InitObjectStartIndexType, CAN_CONST) Can_InitObjectStartIndex[7];
#define CAN_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_Mailbox
**********************************************************************************************************************/
/** 
  \var    Can_Mailbox
  \brief  mailbox configuration (over all controllers)
  \details
  Element                Description
  IDValue            
  MemorySectionsIndex
  ActiveSendObject   
  ControllerConfigIdx    the index of the 1:1 relation pointing to Can_ControllerConfig
  HwHandle           
  MailboxSize        
  MailboxType        
  MaxDataLen         
*/ 
#define CAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(Can_MailboxType, CAN_CONST) Can_Mailbox[226];
#define CAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_MemorySectionInfo
**********************************************************************************************************************/
/** 
  \var    Can_MemorySectionInfo
  \brief  Memory section description
  \details
  Element               Description
  MemoryStartAddress
  MemorySectionStart
*/ 
#define CAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(Can_MemorySectionInfoType, CAN_CONST) Can_MemorySectionInfo[6];
#define CAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_MemorySectionObjects
**********************************************************************************************************************/
/** 
  \var    Can_MemorySectionObjects
  \brief  Memory section objects description
  \details
  Element           Description
  HwHandle      
  MailboxElement
  MailboxHandle 
*/ 
#define CAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(Can_MemorySectionObjectsType, CAN_CONST) Can_MemorySectionObjects[576];
#define CAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_ActiveSendObject
**********************************************************************************************************************/
/** 
  \var    Can_ActiveSendObject
  \brief  temporary data for TX object
  \details
  Element    Description
  Pdu        buffered PduId for confirmation or cancellation
  State      send state like cancelled or send activ
*/ 
#define CAN_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(Can_ActiveSendObjectType, CAN_VAR_NOINIT) Can_ActiveSendObject[72];
#define CAN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_ControllerData
**********************************************************************************************************************/
/** 
  \var    Can_ControllerData
  \brief  struct for all controller related variable data
  \details
  Element                      Description
  BusOffCounter                This variable stores the busoff recovery timeout counter.
  BusOffTransitionRequest      CAN state request for each controller: ContinueBusOffRecovery=0x00, FinishBusOffRecovery=0x01
  CanInterruptCounter          CAN interrupt disable counter for each controller
  IsBusOff                     CAN state for each controller: busoff occur
  LastInitObject               last set baudrate for reinit
  LogStatus                    CAN state for each controller: UNINIT=0x00, START=0x01, STOP=0x02, INIT=0x04, INCONSISTENT=0x08, WARNING =0x10, PASSIVE=0x20, BUSOFF=0x40, SLEEP=0x80
  ModeTransitionRequest        CAN state request for each controller: INIT=0x00, SLEEP=0x01, WAKEUP=0x02, STOP+INIT=0x03, START=0x04, START+INIT=0x05, NONE=0x06
  RamCheckTransitionRequest    CAN state request for each controller: kCanSuppressRamCheck=0x01, kCanExecuteRamCheck=0x00
  StartModeRequested           This variable stores if the start mode is of a special CAN Controller is already requested in the busoff recovery process.
  CanInterruptOldStatus        last CAN interrupt status for restore interrupt for each controller
  LoopTimeout                  hw loop timeout for each controller
  RxMsgBuffer                  This variable stores received values (ID, DLC, DATA) in the reception process.
*/ 
#define CAN_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(Can_ControllerDataType, CAN_VAR_NOINIT) Can_ControllerData[6];
#define CAN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/


/* -----------------------------------------------------------------------------
    UserConfigFile
 ----------------------------------------------------------------------------- */
/* User Config File Start */

/* User Config File End */



#endif  /* CAN_CFG_H */
/**********************************************************************************************************************
  END OF FILE: Can_Cfg.h
**********************************************************************************************************************/
 

