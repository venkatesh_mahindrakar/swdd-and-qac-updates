/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Can
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Can_Lcfg.c
 *   Generation Time: 2020-09-14 17:18:18
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/


#define CAN_LCFG_SOURCE

/* PRQA S 0779 EOF */ /* MD_MSR_Rule5.2 */

/* -----------------------------------------------------------------------------
    Includes
 ----------------------------------------------------------------------------- */

#include "Can_Cfg.h"

/* -----------------------------------------------------------------------------
    Hw specific
 ----------------------------------------------------------------------------- */


/**********************************************************************************************************************
  ComStackLib
**********************************************************************************************************************/
/**********************************************************************************************************************
  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/


/**********************************************************************************************************************
  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  Can_CanIfChannelId
**********************************************************************************************************************/
/** 
  \var    Can_CanIfChannelId
  \brief  indirection table Can to CanIf controller ID
*/ 
#define CAN_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Can_CanIfChannelIdType, CAN_CONST) Can_CanIfChannelId[6] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     CanIfChannelId      Comment */
  /*     0 */              0u,  /* [CT_Backbone1J1939_198bcf1c] */
  /*     1 */              1u,  /* [CT_Backbone2_34cfe263] */
  /*     2 */              2u,  /* [CT_CAN6_120de18e] */
  /*     3 */              3u,  /* [CT_CabSubnet_d2ff0fbe] */
  /*     4 */              4u,  /* [CT_FMSNet_119a8706] */
  /*     5 */              5u   /* [CT_SecuritySubnet_f5346ae6] */
};
#define CAN_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_ControllerConfig
**********************************************************************************************************************/
/** 
  \var    Can_ControllerConfig
  \brief  Global configuration for all controllers
  \details
  Element                            Description
  BaseAddress                    
  InterruptMask1                 
  InterruptMask2                 
  CanControllerDefaultBaudrate   
  InterruptMask3                 
  HasCANFDBaudrate               
  CanControllerDefaultBaudrateIdx
  MailboxRxBasicEndIdx               the end index of the 0:n relation pointing to Can_Mailbox
  MailboxRxBasicLength               the number of relations pointing to Can_Mailbox
  MailboxRxBasicStartIdx             the start index of the 0:n relation pointing to Can_Mailbox
  MailboxRxFullEndIdx                the end index of the 0:n relation pointing to Can_Mailbox
  MailboxRxFullLength                the number of relations pointing to Can_Mailbox
  MailboxRxFullStartIdx              the start index of the 0:n relation pointing to Can_Mailbox
  MailboxTxBasicEndIdx               the end index of the 0:n relation pointing to Can_Mailbox
  MailboxTxBasicLength               the number of relations pointing to Can_Mailbox
  MailboxTxBasicStartIdx             the start index of the 0:n relation pointing to Can_Mailbox
  MailboxTxFullEndIdx                the end index of the 0:n relation pointing to Can_Mailbox
  MailboxTxFullLength                the number of relations pointing to Can_Mailbox
  MailboxTxFullStartIdx              the start index of the 0:n relation pointing to Can_Mailbox
  NumberOfFilters                
  NumberOfFullConfigurableFilters
  NumberOfMaxMailboxes           
  RFFN                           
  RxBasicHwStart                 
  RxBasicHwStop                  
  RxFullHwStart                  
  RxFullHwStop                   
  TxBasicHwStart                 
  TxBasicHwStop                  
  TxFullHwStart                  
  TxFullHwStop                   
  UnusedHwStart                  
  UnusedHwStop                   
*/ 
#define CAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Can_ControllerConfigType, CAN_CONST) Can_ControllerConfig[6] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    BaseAddress  InterruptMask1  InterruptMask2  CanControllerDefaultBaudrate  InterruptMask3  HasCANFDBaudrate  CanControllerDefaultBaudrateIdx  MailboxRxBasicEndIdx                                                                                                                                                    MailboxRxBasicLength                                                                                                                                                    MailboxRxBasicStartIdx                                                                                                                                                    MailboxRxFullEndIdx                                                                                                                                                                            MailboxRxFullLength                                                                                                                                                   MailboxRxFullStartIdx                                                                                                                                                                            MailboxTxBasicEndIdx                                                                                                                                                    MailboxTxBasicLength                                                                                                                                                    MailboxTxBasicStartIdx                                                                                                                                                    MailboxTxFullEndIdx                                                                                                                                                   MailboxTxFullLength                                                                                                                                                   MailboxTxFullStartIdx                                                                                                                                                   NumberOfFilters  NumberOfFullConfigurableFilters  NumberOfMaxMailboxes  RFFN   RxBasicHwStart  RxBasicHwStop  RxFullHwStart  RxFullHwStop  TxBasicHwStart  TxBasicHwStop  TxFullHwStart  TxFullHwStop  UnusedHwStart  UnusedHwStop        Comment                                  Referable Keys */
  { /*     0 */ 0xFFEC8000u,    0x000001FFu,    0x00000000u,                         250u,        0x0000u,            FALSE,                              0u,                   7u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                   2u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                     5u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */, CAN_NO_MAILBOXRXFULLENDIDXOFCONTROLLERCONFIG  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                  0u  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */, CAN_NO_MAILBOXRXFULLSTARTIDXOFCONTROLLERCONFIG  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                   5u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                   1u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                     4u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                  4u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                  4u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                    0u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,           0x00u,                           0x00u,                0x60u, 0x00u,             0u,            4u,            0u,           0u,             4u,            5u,            5u,           9u,            0u,           0u },  /* [CT_Backbone1J1939_198bcf1c] */  /* [/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c] */
  { /*     1 */ 0xFBECC000u,    0xFFFFFFFFu,    0xFFFFFFFFu,                         500u,        0x0FFFu,            FALSE,                              0u,                  81u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                   2u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                    79u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                                          79u  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                 52u  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                                            27u  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                  27u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                   1u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                    26u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                 26u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                 19u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                    7u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,           0x00u,                           0x00u,                0x60u, 0x00u,            52u,           56u,            0u,          52u,            56u,           57u,           57u,          76u,            0u,           0u },  /* [CT_Backbone2_34cfe263]      */  /* [/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263] */
  { /*     2 */ 0xFFEC0000u,    0x000000FFu,    0x00000000u,                         500u,        0x0000u,             TRUE,                              0u,                  87u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */          ,                   2u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */          ,                    85u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */          , CAN_NO_MAILBOXRXFULLENDIDXOFCONTROLLERCONFIG  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */          ,                  0u  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */          , CAN_NO_MAILBOXRXFULLSTARTIDXOFCONTROLLERCONFIG  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */          ,                  85u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */          ,                   1u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */          ,                    84u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */          ,                 84u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */          ,                  3u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */          ,                   81u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */          ,           0x00u,                           0x00u,                0x60u, 0x00u,             0u,            4u,            0u,           0u,             4u,            5u,            5u,           8u,            0u,           0u },  /* [CT_CAN6_120de18e]           */  /* [/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e] */
  { /*     3 */ 0xFFECC000u,    0xFFFFFFFFu,    0xFFFFFFFFu,                         500u,        0x007Fu,            FALSE,                              0u,                 156u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                   2u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                   154u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                                         154u  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                 52u  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                                           102u  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                 102u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                   1u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                   101u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                101u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                 14u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,                   87u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */     ,           0x00u,                           0x00u,                0x60u, 0x00u,            52u,           56u,            0u,          52u,            56u,           57u,           57u,          71u,            0u,           0u },  /* [CT_CabSubnet_d2ff0fbe]      */  /* [/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe] */
  { /*     4 */ 0xFFEC4000u,    0x00001FFFu,    0x00000000u,                         250u,        0x0000u,            FALSE,                              0u,                 167u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */        ,                   2u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */        ,                   165u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */        , CAN_NO_MAILBOXRXFULLENDIDXOFCONTROLLERCONFIG  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */        ,                  0u  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */        , CAN_NO_MAILBOXRXFULLSTARTIDXOFCONTROLLERCONFIG  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */        ,                 165u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */        ,                   1u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */        ,                   164u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */        ,                164u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */        ,                  8u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */        ,                  156u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */        ,           0x00u,                           0x00u,                0x60u, 0x00u,             0u,            4u,            0u,           0u,             4u,            5u,            5u,          13u,            0u,           0u },  /* [CT_FMSNet_119a8706]         */  /* [/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706] */
  { /*     5 */ 0xFBEC0000u,    0xFFFFFFFFu,    0x1FFFFFFFu,                         500u,        0x0000u,            FALSE,                              0u,                 226u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                   2u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                   224u  /* RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                                         224u  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                 38u  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                                           186u  /* RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                 186u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                   1u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                   185u  /* TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                185u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                 18u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,                  167u  /* TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController) */,           0x00u,                           0x00u,                0x60u, 0x00u,            38u,           42u,            0u,          38u,            42u,           43u,           43u,          61u,            0u,           0u }   /* [CT_SecuritySubnet_f5346ae6] */  /* [/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6] */
};
#define CAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_InitBasicCan
**********************************************************************************************************************/
/** 
  \var    Can_InitBasicCan
  \brief  This table contains acceptance filter configuration values.
  \details
  Element     Description
  InitCode
  InitMask
*/ 
#define CAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Can_InitBasicCanType, CAN_CONST) Can_InitBasicCan[26] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    InitCode     InitMask           Comment */
  { /*     0 */ 0x80E00000u, 0x03E00000u },  /* [Channel: CT_Backbone1J1939_198bcf1c, Init object: 0, Filter: CanFilterMask] */
  { /*     1 */ 0x80E00000u, 0x03E00000u },  /* [Channel: CT_Backbone1J1939_198bcf1c, Init object: 0, Filter: CanFilterMask] */
  { /*     2 */ 0x80E00000u, 0x03E00000u },  /* [Channel: CT_Backbone1J1939_198bcf1c, Init object: 0, Filter: CanFilterMask] */
  { /*     3 */ 0x80E00000u, 0x03E00000u },  /* [Channel: CT_Backbone1J1939_198bcf1c, Init object: 0, Filter: CanFilterMask] */
  { /*     4 */ 0x80000000u, 0x1FFFFFFFu },  /* [Channel: CT_Backbone1J1939_198bcf1c, Init object: 0, Filter: CanFilterMask_001] */
  { /*     5 */ 0x80000000u, 0x1FFFFFFFu },  /* [Channel: CT_Backbone1J1939_198bcf1c, Init object: 0, Filter: CanFilterMask_001] */
  { /*     6 */ 0x8CFF3FBFu, 0x1FFFFFFFu },  /* [Channel: CT_Backbone2_34cfe263, Init object: 1, Filter: CanFilterMask_001] */
  { /*     7 */ 0x8CFF3FBFu, 0x1FFFFFFFu },  /* [Channel: CT_Backbone2_34cfe263, Init object: 1, Filter: CanFilterMask_001] */
  { /*     8 */ 0x90400000u, 0x10402000u },  /* [Channel: CT_Backbone2_34cfe263, Init object: 1, Filter: CanFilterMask] */
  { /*     9 */ 0x90400000u, 0x10402000u },  /* [Channel: CT_Backbone2_34cfe263, Init object: 1, Filter: CanFilterMask] */
  { /*    10 */ 0x00000000u, 0x1FFFFFFFu },  /* [Channel: CT_CAN6_120de18e, Init object: 2, Filter: CanFilterMask_001] */
  { /*    11 */ 0x00000000u, 0x1FFFFFFFu },  /* [Channel: CT_CAN6_120de18e, Init object: 2, Filter: CanFilterMask_001] */
  { /*    12 */ 0x90FF77F0u, 0x1FFFFFFFu },  /* [Channel: CT_CAN6_120de18e, Init object: 2, Filter: CanFilterMask] */
  { /*    13 */ 0x90FF77F0u, 0x1FFFFFFFu },  /* [Channel: CT_CAN6_120de18e, Init object: 2, Filter: CanFilterMask] */
  { /*    14 */ 0x8CFF7F7Fu, 0x1FFFFFFFu },  /* [Channel: CT_CabSubnet_d2ff0fbe, Init object: 3, Filter: CanFilterMask_001] */
  { /*    15 */ 0x8CFF7F7Fu, 0x1FFFFFFFu },  /* [Channel: CT_CabSubnet_d2ff0fbe, Init object: 3, Filter: CanFilterMask_001] */
  { /*    16 */ 0x9CFF8000u, 0x1FFFFF00u },  /* [Channel: CT_CabSubnet_d2ff0fbe, Init object: 3, Filter: CanFilterMask] */
  { /*    17 */ 0x9CFF8000u, 0x1FFFFF00u },  /* [Channel: CT_CabSubnet_d2ff0fbe, Init object: 3, Filter: CanFilterMask] */
  { /*    18 */ 0x00000000u, 0x1FFFFFFFu },  /* [Channel: CT_FMSNet_119a8706, Init object: 4, Filter: CanFilterMask_001] */
  { /*    19 */ 0x00000000u, 0x1FFFFFFFu },  /* [Channel: CT_FMSNet_119a8706, Init object: 4, Filter: CanFilterMask_001] */
  { /*    20 */ 0x80E80000u, 0x03E80000u },  /* [Channel: CT_FMSNet_119a8706, Init object: 4, Filter: CanFilterMask] */
  { /*    21 */ 0x80E80000u, 0x03E80000u },  /* [Channel: CT_FMSNet_119a8706, Init object: 4, Filter: CanFilterMask] */
  { /*    22 */ 0x00000000u, 0x1FFFFFFFu },  /* [Channel: CT_SecuritySubnet_f5346ae6, Init object: 5, Filter: CanFilterMask_001] */
  { /*    23 */ 0x00000000u, 0x1FFFFFFFu },  /* [Channel: CT_SecuritySubnet_f5346ae6, Init object: 5, Filter: CanFilterMask_001] */
  { /*    24 */ 0x9CFF8000u, 0x1FFFFF00u },  /* [Channel: CT_SecuritySubnet_f5346ae6, Init object: 5, Filter: CanFilterMask] */
  { /*    25 */ 0x9CFF8000u, 0x1FFFFF00u }   /* [Channel: CT_SecuritySubnet_f5346ae6, Init object: 5, Filter: CanFilterMask] */
};
#define CAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_InitBasicCanIndex
**********************************************************************************************************************/
/** 
  \var    Can_InitBasicCanIndex
  \brief  This table contains start/stop indices for the Can_InitBasicCan table.
*/ 
#define CAN_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Can_InitBasicCanIndexType, CAN_CONST) Can_InitBasicCanIndex[6] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     InitBasicCanIndex                                                                                 */
  /*     0 */              0x00u  /* Filter Start Index: Channel: CT_Backbone1J1939_198bcf1c, Init object: 0 */,
  /*     1 */              0x06u  /* Filter Start Index: Channel: CT_Backbone2_34cfe263, Init object: 1 */     ,
  /*     2 */              0x0Au  /* Filter Start Index: Channel: CT_CAN6_120de18e, Init object: 2 */          ,
  /*     3 */              0x0Eu  /* Filter Start Index: Channel: CT_CabSubnet_d2ff0fbe, Init object: 3 */     ,
  /*     4 */              0x12u  /* Filter Start Index: Channel: CT_FMSNet_119a8706, Init object: 4 */        ,
  /*     5 */              0x16u  /* Filter Start Index: Channel: CT_SecuritySubnet_f5346ae6, Init object: 5 */
};
#define CAN_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_InitObject
**********************************************************************************************************************/
/** 
  \var    Can_InitObject
  \brief  This table contains information about the init object: e.g. bustiming register contents.
  \details
  Element     Description
  CBT     
  Control1
*/ 
#define CAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Can_InitObjectType, CAN_CONST) Can_InitObject[6] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    CBT          Control1           Comment */
  { /*     0 */ 0x81212821u, 0x09492002u },  /* ["CT_Backbone1J1939_198bcf1c - CanControllerBaudrateConfig", init object index: 0] */
  { /*     1 */ 0x80822042u, 0x04922000u },  /* ["CT_Backbone2_34cfe263 - CanControllerBaudrateConfig", init object index: 1] */
  { /*     2 */ 0x80E01401u, 0x07012005u },  /* ["CT_CAN6_120de18e - CanControllerBaudrateConfig", init object index: 2] */
  { /*     3 */ 0x80822042u, 0x04922000u },  /* ["CT_CabSubnet_d2ff0fbe - CanControllerBaudrateConfig", init object index: 3] */
  { /*     4 */ 0x81212821u, 0x09492002u },  /* ["CT_FMSNet_119a8706 - CanControllerBaudrateConfig", init object index: 4] */
  { /*     5 */ 0x80822042u, 0x04922000u }   /* ["CT_SecuritySubnet_f5346ae6 - CanControllerBaudrateConfig", init object index: 5] */
};
#define CAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_InitObjectBaudrate
**********************************************************************************************************************/
/** 
  \var    Can_InitObjectBaudrate
  \brief  baudrates ('InitStruct' as index)
*/ 
#define CAN_START_SEC_CONST_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Can_InitObjectBaudrateType, CAN_CONST) Can_InitObjectBaudrate[6] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     InitObjectBaudrate      Comment */
  /*     0 */                250u,  /* [CT_Backbone1J1939_198bcf1c - CanControllerBaudrateConfig] */
  /*     1 */                500u,  /* [CT_Backbone2_34cfe263 - CanControllerBaudrateConfig] */
  /*     2 */                500u,  /* [CT_CAN6_120de18e - CanControllerBaudrateConfig] */
  /*     3 */                500u,  /* [CT_CabSubnet_d2ff0fbe - CanControllerBaudrateConfig] */
  /*     4 */                250u,  /* [CT_FMSNet_119a8706 - CanControllerBaudrateConfig] */
  /*     5 */                500u   /* [CT_SecuritySubnet_f5346ae6 - CanControllerBaudrateConfig] */
};
#define CAN_STOP_SEC_CONST_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_InitObjectFD
**********************************************************************************************************************/
/** 
  \var    Can_InitObjectFD
  \brief  This table contains bittiming register values for the CAN-FD baudrate.
  \details
  Element    Description
  FDCTRL 
  FDCBT  
*/ 
#define CAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Can_InitObjectFDType, CAN_CONST) Can_InitObjectFD[6] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    FDCTRL       FDCBT          Comment */
  { /*     0 */ 0x00000000u, 0x0000u },  /* ["CT_Backbone1J1939_198bcf1c - CanControllerBaudrateConfig", init object index: 0 dummy entry] */
  { /*     1 */ 0x00000000u, 0x0000u },  /* ["CT_Backbone2_34cfe263 - CanControllerBaudrateConfig", init object index: 1 dummy entry] */
  { /*     2 */ 0x80008700u, 0x0C21u },  /* ["CT_CAN6_120de18e - CanControllerBaudrateConfig", init object index: 2] */
  { /*     3 */ 0x00000000u, 0x0000u },  /* ["CT_CabSubnet_d2ff0fbe - CanControllerBaudrateConfig", init object index: 3 dummy entry] */
  { /*     4 */ 0x00000000u, 0x0000u },  /* ["CT_FMSNet_119a8706 - CanControllerBaudrateConfig", init object index: 4 dummy entry] */
  { /*     5 */ 0x00000000u, 0x0000u }   /* ["CT_SecuritySubnet_f5346ae6 - CanControllerBaudrateConfig", init object index: 5 dummy entry] */
};
#define CAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_InitObjectFdBrsConfig
**********************************************************************************************************************/
/** 
  \var    Can_InitObjectFdBrsConfig
  \brief  FD config ('BaudrateObject' as index)
*/ 
#define CAN_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Can_InitObjectFdBrsConfigType, CAN_CONST) Can_InitObjectFdBrsConfig[6] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     InitObjectFdBrsConfig              */
  /*     0 */     CAN_NONE_INITOBJECTFDBRSCONFIG,
  /*     1 */     CAN_NONE_INITOBJECTFDBRSCONFIG,
  /*     2 */  CAN_FD_RXTX_INITOBJECTFDBRSCONFIG,
  /*     3 */     CAN_NONE_INITOBJECTFDBRSCONFIG,
  /*     4 */     CAN_NONE_INITOBJECTFDBRSCONFIG,
  /*     5 */     CAN_NONE_INITOBJECTFDBRSCONFIG
};
#define CAN_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_InitObjectStartIndex
**********************************************************************************************************************/
/** 
  \var    Can_InitObjectStartIndex
  \brief  Start index of 'InitStruct' / baudratesets (controllers as index)
*/ 
#define CAN_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Can_InitObjectStartIndexType, CAN_CONST) Can_InitObjectStartIndex[7] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     InitObjectStartIndex      Comment */
  /*     0 */                    0u,  /* [CT_Backbone1J1939_198bcf1c] */
  /*     1 */                    1u,  /* [CT_Backbone2_34cfe263] */
  /*     2 */                    2u,  /* [CT_CAN6_120de18e] */
  /*     3 */                    3u,  /* [CT_CabSubnet_d2ff0fbe] */
  /*     4 */                    4u,  /* [CT_FMSNet_119a8706] */
  /*     5 */                    5u,  /* [CT_SecuritySubnet_f5346ae6] */
  /*     6 */                    6u   /* [stop index] */
};
#define CAN_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_Mailbox
**********************************************************************************************************************/
/** 
  \var    Can_Mailbox
  \brief  mailbox configuration (over all controllers)
  \details
  Element                Description
  IDValue            
  MemorySectionsIndex
  ActiveSendObject   
  ControllerConfigIdx    the index of the 1:1 relation pointing to Can_ControllerConfig
  HwHandle           
  MailboxSize        
  MailboxType        
  MaxDataLen         
*/ 
#define CAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Can_MailboxType, CAN_CONST) Can_Mailbox[226] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    IDValue      MemorySectionsIndex  ActiveSendObject  ControllerConfigIdx                                                                 HwHandle  MailboxSize  MailboxType                                MaxDataLen        Comment                                                                                       Referable Keys */
  { /*     0 */ 0x80E80000u,                  5u,               0u,                  0u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c */,       5u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_AckmTxPdu_Backbone1J1939_54966c1b]                             */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_AckmTxPdu_Backbone1J1939_54966c1b (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*     1 */ 0x80EC0000u,                  6u,               1u,                  0u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c */,       6u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_FcNPdu_Backbone1J1939_dba64907]                                */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_FcNPdu_Backbone1J1939_dba64907 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*     2 */ 0x94FF4431u,                  7u,               2u,                  0u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c */,       7u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_BB1_01P_oBackbone1J1939_55a00301_Tx]                      */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_BB1_01P_oBackbone1J1939_55a00301_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*     3 */ 0x98EEFF00u,                  8u,               3u,                  0u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c */,       8u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_J1939NmTxPdu_fa509995]                                         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_J1939NmTxPdu_fa509995 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*     4 */ 0x00000000u,                  4u,               4u,                  0u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c */,       4u,          1u, CAN_TX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CN_Backbone1J1939_0b1f4bae_Tx]                                                   */  /* [/ActiveEcuC/Can/CanConfigSet/CN_Backbone1J1939_0b1f4bae_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c, TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*     5 */ 0x00000000u,                  0u,               0u,                  0u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c */,       0u,          2u, CAN_RX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CN_Backbone1J1939_6e8ec372_1_Rx]                                                 */  /* [/ActiveEcuC/Can/CanConfigSet/CN_Backbone1J1939_6e8ec372_1_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c, RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*     6 */ 0x00000000u,                  2u,               0u,                  0u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c */,       2u,          2u, CAN_RX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CN_Backbone1J1939_6e8ec372_Rx]                                                   */  /* [/ActiveEcuC/Can/CanConfigSet/CN_Backbone1J1939_6e8ec372_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c, RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone1J1939_198bcf1c (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*     7 */ 0x8CFF2840u,                153u,               5u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      57u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_BB2_04P_oBackbone2_14239779_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_BB2_04P_oBackbone2_14239779_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*     8 */ 0x90FF2740u,                154u,               6u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      58u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_BB2_02P_oBackbone2_9fdb9738_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_BB2_02P_oBackbone2_9fdb9738_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*     9 */ 0x90FF3040u,                155u,               7u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      59u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_BB2_03P_oBackbone2_f8181638_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_BB2_03P_oBackbone2_f8181638_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    10 */ 0x90FF7740u,                156u,               8u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      60u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_BB2_01P_oBackbone2_379f1438_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_BB2_01P_oBackbone2_379f1438_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    11 */ 0x90FFD440u,                157u,               9u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      61u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_BB2_29P_oBackbone2_bfae97b8_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_BB2_29P_oBackbone2_bfae97b8_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    12 */ 0x93FF2A40u,                158u,              10u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      62u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_BB2_05P_oBackbone2_73e01679_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_BB2_05P_oBackbone2_73e01679_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    13 */ 0x93FF7F40u,                159u,              11u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      63u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_BB2_06P_oBackbone2_dba49579_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_BB2_06P_oBackbone2_dba49579_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    14 */ 0x93FFF040u,                160u,              12u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      64u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_BB2_10P_oBackbone2_503b56b9_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_BB2_10P_oBackbone2_503b56b9_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    15 */ 0x94EFC540u,                161u,              13u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      65u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_BB2_11P_oBackbone2_37f8d7b9_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_BB2_11P_oBackbone2_37f8d7b9_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    16 */ 0x95F0B340u,                162u,              14u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      66u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_BB2_07P_oBackbone2_bc671479_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_BB2_07P_oBackbone2_bc671479_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    17 */ 0x98F33F40u,                163u,              15u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      67u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_BB2_28P_oBackbone2_d86d16b8_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_BB2_28P_oBackbone2_d86d16b8_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    18 */ 0x9CFF9001u,                164u,              16u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      68u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_AnmMsg_CIOM_Backbone2_oBackbone2_d362d40e_Tx]                  */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_AnmMsg_CIOM_Backbone2_oBackbone2_d362d40e_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    19 */ 0x9CFFF240u,                165u,              17u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      69u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_Debug02_CIOM_BB2_oBackbone2_423c0750_Tx]                       */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_Debug02_CIOM_BB2_oBackbone2_423c0750_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    20 */ 0x9CFFF340u,                166u,              18u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      70u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_Debug03_CIOM_BB2_oBackbone2_2d143e88_Tx]                       */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_Debug03_CIOM_BB2_oBackbone2_2d143e88_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    21 */ 0x9CFFF640u,                167u,              19u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      71u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_Debug04_CIOM_BB2_oBackbone2_fbbd95c1_Tx]                       */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_Debug04_CIOM_BB2_oBackbone2_fbbd95c1_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    22 */ 0x9CFFF740u,                168u,              20u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      72u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_Debug05_CIOM_BB2_oBackbone2_9495ac19_Tx]                       */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_Debug05_CIOM_BB2_oBackbone2_9495ac19_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    23 */ 0x9CFFF840u,                169u,              21u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      73u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_Debug06_CIOM_BB2_oBackbone2_25ede671_Tx]                       */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_Debug06_CIOM_BB2_oBackbone2_25ede671_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    24 */ 0x9CFFF940u,                170u,              22u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      74u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_Debug07_CIOM_BB2_oBackbone2_4ac5dfa9_Tx]                       */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_Debug07_CIOM_BB2_oBackbone2_4ac5dfa9_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    25 */ 0x9F40F240u,                171u,              23u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      75u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PhysDiagRespMsg_F2_40_BB2_Tp_oBackbone2_bd4d3e52_Tx]           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PhysDiagRespMsg_F2_40_BB2_Tp_oBackbone2_bd4d3e52_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    26 */ 0x00000000u,                152u,              24u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      56u,          1u, CAN_TX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CN_Backbone2_78967e2c_Tx]                                                        */  /* [/ActiveEcuC/Can/CanConfigSet/CN_Backbone2_78967e2c_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    27 */ 0x9F41FFF4u,                 96u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,       0u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_IntTesterTGW2FuncDiagMsg_BB2_Tp_oBackbone2_c6fcfce9_Rx]        */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_IntTesterTGW2FuncDiagMsg_BB2_Tp_oBackbone2_c6fcfce9_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    28 */ 0x9F41FFF2u,                 97u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,       1u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_TesterFuncDiagMsg_BB2_Tp_oBackbone2_6ec04186_Rx]               */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_TesterFuncDiagMsg_BB2_Tp_oBackbone2_6ec04186_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    29 */ 0x9F40A2F4u,                 98u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,       2u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagReqMsgIntTGW2_A2_F4_BB2_Tp_oBackbone2_8613fab7_Rx]         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagReqMsgIntTGW2_A2_F4_BB2_Tp_oBackbone2_8613fab7_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    30 */ 0x9F40A2F3u,                 99u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,       3u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagReqMsgIntHMIIOM_A2_F3_BB2_Tp_oBackbone2_e882bd41_Rx]       */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagReqMsgIntHMIIOM_A2_F3_BB2_Tp_oBackbone2_e882bd41_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    31 */ 0x9F40A2F2u,                100u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,       4u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PhysDiagReqMsg_A2_F2_BB2_Tp_oBackbone2_e7d85c22_Rx]            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PhysDiagReqMsg_A2_F2_BB2_Tp_oBackbone2_e7d85c22_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    32 */ 0x9F40A1F4u,                101u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,       5u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagReqMsgIntTGW2_A1_F4_BB2_Tp_oBackbone2_91dc1a3b_Rx]         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagReqMsgIntTGW2_A1_F4_BB2_Tp_oBackbone2_91dc1a3b_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    33 */ 0x9F40A1F3u,                102u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,       6u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagReqMsgIntHMIIOM_A1_F3_BB2_Tp_oBackbone2_ba062351_Rx]       */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagReqMsgIntHMIIOM_A1_F3_BB2_Tp_oBackbone2_ba062351_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    34 */ 0x9F40A1F2u,                103u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,       7u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PhysDiagReqMsg_A1_F2_BB2_Tp_oBackbone2_9bf67e6d_Rx]            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PhysDiagReqMsg_A1_F2_BB2_Tp_oBackbone2_9bf67e6d_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    35 */ 0x9F40A0F4u,                104u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,       8u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagReqMsgIntTGW2_A0_F4_BB2_Tp_oBackbone2_9c9945bf_Rx]         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagReqMsgIntTGW2_A0_F4_BB2_Tp_oBackbone2_9c9945bf_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    36 */ 0x9F40A0F3u,                105u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,       9u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagReqMsgIntHMIIOM_A0_F3_BB2_Tp_oBackbone2_3d55549e_Rx]       */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagReqMsgIntHMIIOM_A0_F3_BB2_Tp_oBackbone2_3d55549e_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    37 */ 0x9F40A0F2u,                106u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      10u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PhysDiagReqMsg_A0_F2_BB2_Tp_oBackbone2_b0139fa8_Rx]            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PhysDiagReqMsg_A0_F2_BB2_Tp_oBackbone2_b0139fa8_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    38 */ 0x9F4026F4u,                107u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      11u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagReqMsgIntTGW2_26_F4_BB2_Tp_oBackbone2_ce56ef15_Rx]         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagReqMsgIntTGW2_26_F4_BB2_Tp_oBackbone2_ce56ef15_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    39 */ 0x9F4026F3u,                108u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      12u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagReqMsgIntHMIIOM_26_F3_BB2_Tp_oBackbone2_3a21aae6_Rx]       */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagReqMsgIntHMIIOM_26_F3_BB2_Tp_oBackbone2_3a21aae6_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    40 */ 0x9F4026F2u,                109u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      13u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PhysDiagReqMsg_26_F2_BB2_Tp_oBackbone2_680a80cc_Rx]            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PhysDiagReqMsg_26_F2_BB2_Tp_oBackbone2_680a80cc_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    41 */ 0x9D121140u,                110u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      14u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DebugCtrl1_CIOM_BB2_oBackbone2_ca351e0d_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DebugCtrl1_CIOM_BB2_oBackbone2_ca351e0d_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    42 */ 0x98FEF960u,                111u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      15u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_HMIIOM_BB2_18P_oBackbone2_2f8dfe18_Rx]                         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_HMIIOM_BB2_18P_oBackbone2_2f8dfe18_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    43 */ 0x98FCEF60u,                112u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      16u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_HMIIOM_BB2_15P_oBackbone2_fb4261c4_Rx]                         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_HMIIOM_BB2_15P_oBackbone2_fb4261c4_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    44 */ 0x98FBBF60u,                113u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      17u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_HMIIOM_BB2_38P_oBackbone2_17230dd8_Rx]                         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_HMIIOM_BB2_38P_oBackbone2_17230dd8_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    45 */ 0x98FAE560u,                114u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      18u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_HMIIOM_BB2_12P_oBackbone2_5a09ee73_Rx]                         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_HMIIOM_BB2_12P_oBackbone2_5a09ee73_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    46 */ 0x98FA3760u,                115u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      19u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_HMIIOM_BB2_11P_oBackbone2_17206880_Rx]                         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_HMIIOM_BB2_11P_oBackbone2_17206880_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    47 */ 0x98F77FFFu,                116u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      20u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_TECU_BB2_06S_Tp_oBackbone2_0c47cb8e_Rx]                        */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_TECU_BB2_06S_Tp_oBackbone2_0c47cb8e_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    48 */ 0x98F77F60u,                117u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      21u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_HMIIOM_BB2_27S_Tp_oBackbone2_aeede795_Rx]                      */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_HMIIOM_BB2_27S_Tp_oBackbone2_aeede795_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    49 */ 0x98F77F36u,                118u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      22u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_EMS_BB2_08P_oBackbone2_dd5310ea_Rx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_EMS_BB2_08P_oBackbone2_dd5310ea_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
    /* Index    IDValue      MemorySectionsIndex  ActiveSendObject  ControllerConfigIdx                                                                 HwHandle  MailboxSize  MailboxType                                MaxDataLen        Comment                                                                                       Referable Keys */
  { /*    50 */ 0x98F77F32u,                119u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      23u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_EMS_BB2_06P_oBackbone2_148edfe1_Rx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_EMS_BB2_06P_oBackbone2_148edfe1_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    51 */ 0x98F62860u,                120u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      24u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_HMIIOM_BB2_19P_CIOM_Tp_oBackbone2_470ca910_Rx]                 */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_HMIIOM_BB2_19P_CIOM_Tp_oBackbone2_470ca910_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    52 */ 0x98F36B24u,                121u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      25u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_VMCU_BB2_31S_Tp_oBackbone2_210b49cb_Rx]                        */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_VMCU_BB2_31S_Tp_oBackbone2_210b49cb_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    53 */ 0x98F2BD24u,                122u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      26u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_VMCU_BB2_32S_Tp_oBackbone2_2c992e71_Rx]                        */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_VMCU_BB2_32S_Tp_oBackbone2_2c992e71_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    54 */ 0x98F16110u,                123u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      27u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_EMS_BB2_09S_Tp_oBackbone2_ef0c9958_Rx]                         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_EMS_BB2_09S_Tp_oBackbone2_ef0c9958_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    55 */ 0x97F77F10u,                124u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      28u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_EMS_BB2_04P_oBackbone2_f5214579_Rx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_EMS_BB2_04P_oBackbone2_f5214579_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    56 */ 0x97F5F510u,                125u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      29u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_EMS_BB2_13P_oBackbone2_036dfbaa_Rx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_EMS_BB2_13P_oBackbone2_036dfbaa_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    57 */ 0x97F57510u,                126u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      30u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_EMS_BB2_05P_oBackbone2_85f68835_Rx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_EMS_BB2_05P_oBackbone2_85f68835_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    58 */ 0x97F52A7Eu,                127u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      31u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_HMIIOM_BB2_05P_oBackbone2_e7151824_Rx]                         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_HMIIOM_BB2_05P_oBackbone2_e7151824_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    59 */ 0x97F469E6u,                128u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      32u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_BBM_BB2_03S_CIOM_Tp_oBackbone2_8a8d09f2_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_BBM_BB2_03S_CIOM_Tp_oBackbone2_8a8d09f2_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    60 */ 0x95F4BFFDu,                129u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      33u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_VMCU_BB2_55P_oBackbone2_a30a2c5d_Rx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_VMCU_BB2_55P_oBackbone2_a30a2c5d_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    61 */ 0x95F46760u,                130u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      34u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_HMIIOM_BB2_23P_oBackbone2_45e8e602_Rx]                         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_HMIIOM_BB2_23P_oBackbone2_45e8e602_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    62 */ 0x94FFF0FEu,                131u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      35u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_BB2_22S_FCM_Tp_oBackbone2_7818b16f_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_BB2_22S_FCM_Tp_oBackbone2_7818b16f_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    63 */ 0x94FFF0D1u,                132u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      36u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_HMIIOM_BB2_09P_oBackbone2_08c205a9_Rx]                         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_HMIIOM_BB2_09P_oBackbone2_08c205a9_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    64 */ 0x94FFBF60u,                133u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      37u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_HMIIOM_BB2_34S_oBackbone2_6313bb35_Rx]                         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_HMIIOM_BB2_34S_oBackbone2_6313bb35_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    65 */ 0x94FF7F60u,                134u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      38u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_HMIIOM_BB2_35S_Tp_oBackbone2_61dbf5f7_Rx]                      */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_HMIIOM_BB2_35S_Tp_oBackbone2_61dbf5f7_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    66 */ 0x94FF6328u,                135u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      39u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_HMIIOM_BB2_06S_Tp_oBackbone2_2c2d06cb_Rx]                      */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_HMIIOM_BB2_06S_Tp_oBackbone2_2c2d06cb_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    67 */ 0x94FF2A80u,                136u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      40u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_BB2_30S_FCM_Tp_oBackbone2_d656145d_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_BB2_30S_FCM_Tp_oBackbone2_d656145d_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    68 */ 0x94C06AA9u,                137u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      41u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_VMCU_BB2_54P_oBackbone2_c4c9ad5d_Rx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_VMCU_BB2_54P_oBackbone2_c4c9ad5d_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    69 */ 0x93FF7F7Du,                138u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      42u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_VMCU_BB2_53P_oBackbone2_28f22c1c_Rx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_VMCU_BB2_53P_oBackbone2_28f22c1c_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    70 */ 0x93A74460u,                139u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      43u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_HMIIOM_BB2_33P_oBackbone2_59bf9fe2_Rx]                         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_HMIIOM_BB2_33P_oBackbone2_59bf9fe2_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    71 */ 0x92FFE9BFu,                140u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      44u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_HMIIOM_BB2_03P_oBackbone2_7d4615c2_Rx]                         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_HMIIOM_BB2_03P_oBackbone2_7d4615c2_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    72 */ 0x92FFBF3Fu,                141u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      45u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_VMCU_BB2_52P_oBackbone2_4f31ad1c_Rx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_VMCU_BB2_52P_oBackbone2_4f31ad1c_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    73 */ 0x92FF7FE6u,                142u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      46u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_BBM_BB2_01P_oBackbone2_0057383e_Rx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_BBM_BB2_01P_oBackbone2_0057383e_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    74 */ 0x92FF6324u,                143u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      47u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_VMCU_BB2_05P_oBackbone2_a2f2e1d8_Rx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_VMCU_BB2_05P_oBackbone2_a2f2e1d8_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    75 */ 0x92FF2A24u,                144u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      48u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_VMCU_BB2_04P_oBackbone2_c53160d8_Rx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_VMCU_BB2_04P_oBackbone2_c53160d8_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    76 */ 0x90FFE950u,                145u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      49u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DACU_BB2_02P_oBackbone2_0b8371c6_Rx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DACU_BB2_02P_oBackbone2_0b8371c6_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    77 */ 0x90FF7F24u,                146u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      50u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_VMCU_BB2_82P_oBackbone2_4df77c91_Rx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_VMCU_BB2_82P_oBackbone2_4df77c91_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    78 */ 0x90FF3924u,                147u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      51u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_VMCU_BB2_02P_oBackbone2_4ec96099_Rx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_VMCU_BB2_02P_oBackbone2_4ec96099_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    79 */ 0x00000000u,                148u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      52u,          2u, CAN_RX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_004]                                                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_004 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    80 */ 0x00000000u,                150u,               0u,                  1u  /* /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 */     ,      54u,          2u, CAN_RX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CN_Backbone2_7b3866ec_1_Rx]                                                      */  /* [/ActiveEcuC/Can/CanConfigSet/CN_Backbone2_7b3866ec_1_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263, RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_Backbone2_34cfe263 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    81 */ 0x00000011u,                197u,              25u,                  2u  /* /ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e */          ,       5u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_SCIM_LINtoCAN6_oCAN6_6b855022_Tx]                              */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_SCIM_LINtoCAN6_oCAN6_6b855022_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    82 */ 0x90FF7740u,                198u,              26u,                  2u  /* /ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e */          ,       6u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_SCIM_BB1toCAN6_oCAN6_cb3fc040_Tx]                              */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_SCIM_BB1toCAN6_oCAN6_cb3fc040_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    83 */ 0x90FF7741u,                199u,              27u,                  2u  /* /ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e */          ,       7u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_SCIM_BB2toCAN6_oCAN6_adf5f68f_Tx]                              */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_SCIM_BB2toCAN6_oCAN6_adf5f68f_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    84 */ 0x00000000u,                196u,              28u,                  2u  /* /ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e */          ,       4u,          1u, CAN_TX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CN_CAN6_b040c073_Tx]                                                             */  /* [/ActiveEcuC/Can/CanConfigSet/CN_CAN6_b040c073_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e, TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    85 */ 0x00000000u,                192u,               0u,                  2u  /* /ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e */          ,       0u,          2u, CAN_RX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_003]                                                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_003 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e, RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    86 */ 0x00000000u,                194u,               0u,                  2u  /* /ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e */          ,       2u,          2u, CAN_RX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CN_CAN6_20078678_1_Rx]                                                           */  /* [/ActiveEcuC/Can/CanConfigSet/CN_CAN6_20078678_1_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e, RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CAN6_120de18e (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    87 */ 0x90F17F40u,                345u,              29u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      57u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_02P_oCabSubnet_694518ea_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_02P_oCabSubnet_694518ea_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    88 */ 0x90FF4040u,                346u,              30u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      58u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_32P_oCabSubnet_69ed5c69_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_32P_oCabSubnet_69ed5c69_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    89 */ 0x90FF4C40u,                347u,              31u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      59u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_01P_oCabSubnet_c1019bea_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_01P_oCabSubnet_c1019bea_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    90 */ 0x90FF5140u,                348u,              32u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      60u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_03P_oCabSubnet_0e8699ea_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_03P_oCabSubnet_0e8699ea_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    91 */ 0x90FF9640u,                349u,              33u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      61u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_06P_oCabSubnet_2d3a1aab_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_06P_oCabSubnet_2d3a1aab_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    92 */ 0x90FFC040u,                350u,              34u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      62u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_33P_oCabSubnet_0e2edd69_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_33P_oCabSubnet_0e2edd69_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    93 */ 0x90FFDC40u,                351u,              35u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      63u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_09P_oCabSubnet_49ff9f68_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_09P_oCabSubnet_49ff9f68_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    94 */ 0x94A74440u,                352u,              36u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      64u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_27P_oCabSubnet_4a361ca9_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_27P_oCabSubnet_4a361ca9_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    95 */ 0x94FE1440u,                353u,              37u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      65u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_23P_oCabSubnet_0e491ee8_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_23P_oCabSubnet_0e491ee8_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    96 */ 0x94FF3A40u,                354u,              38u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      66u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_26P_oCabSubnet_2df59da9_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_26P_oCabSubnet_2df59da9_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    97 */ 0x94FF3FBFu,                355u,              39u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      67u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_10P_oCabSubnet_a6a5d96b_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_10P_oCabSubnet_a6a5d96b_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    98 */ 0x95F91140u,                356u,              40u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      68u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_24P_oCabSubnet_e2729fa9_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_24P_oCabSubnet_e2729fa9_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*    99 */ 0x95FA1140u,                357u,              41u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      69u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_31P_oCabSubnet_c1a9df69_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_31P_oCabSubnet_c1a9df69_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
    /* Index    IDValue      MemorySectionsIndex  ActiveSendObject  ControllerConfigIdx                                                                 HwHandle  MailboxSize  MailboxType                                MaxDataLen        Comment                                                                                       Referable Keys */
  { /*   100 */ 0x9CFF9026u,                358u,              42u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      70u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_AnmMsg_CIOM_CabSubnet_oCabSubnet_e295c8bd_Tx]                  */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_AnmMsg_CIOM_CabSubnet_oCabSubnet_e295c8bd_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   101 */ 0x00000000u,                344u,              43u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      56u,          1u, CAN_TX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CN_CabSubnet_9ea693f1_Tx]                                                        */  /* [/ActiveEcuC/Can/CanConfigSet/CN_CabSubnet_9ea693f1_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   102 */ 0x9F42F2A2u,                288u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,       0u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagUUDTRespMsg1_F2_A2_Cab_oCabSubnet_faf51f79_Rx]             */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagUUDTRespMsg1_F2_A2_Cab_oCabSubnet_faf51f79_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   103 */ 0x9F42F298u,                289u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,       1u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagUUDTRespMsg1_F2_98_Cab_oCabSubnet_2175e3c9_Rx]             */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagUUDTRespMsg1_F2_98_Cab_oCabSubnet_2175e3c9_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   104 */ 0x9F42F253u,                290u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,       2u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagUUDTRespMsg1_F2_53_Cab_oCabSubnet_d5110ca2_Rx]             */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagUUDTRespMsg1_F2_53_Cab_oCabSubnet_d5110ca2_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   105 */ 0x9F42F226u,                291u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,       3u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagUUDTRespMsg1_F2_26_Cab_oCabSubnet_8c457eb7_Rx]             */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagUUDTRespMsg1_F2_26_Cab_oCabSubnet_8c457eb7_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   106 */ 0x9F40F4A2u,                292u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,       4u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagRespMsgIntTGW2_F4_A2_Cab_Tp_oCabSubnet_748ab027_Rx]        */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagRespMsgIntTGW2_F4_A2_Cab_Tp_oCabSubnet_748ab027_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   107 */ 0x9F40F498u,                293u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,       5u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagRespMsgIntTGW2_F4_98_Cab_Tp_oCabSubnet_d506a20a_Rx]        */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagRespMsgIntTGW2_F4_98_Cab_Tp_oCabSubnet_d506a20a_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   108 */ 0x9F40F453u,                294u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,       6u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagRespMsgIntTGW2_F4_53_Cab_Tp_oCabSubnet_781e6a27_Rx]        */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagRespMsgIntTGW2_F4_53_Cab_Tp_oCabSubnet_781e6a27_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   109 */ 0x9F40F426u,                295u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,       7u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagRespMsgIntTGW2_F4_26_Cab_Tp_oCabSubnet_0b131bbc_Rx]        */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagRespMsgIntTGW2_F4_26_Cab_Tp_oCabSubnet_0b131bbc_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   110 */ 0x9F40F3A2u,                296u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,       8u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagRespMsgIntHMIIOM_F3_A2_Cab_Tp_oCabSubnet_15c35ee3_Rx]      */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagRespMsgIntHMIIOM_F3_A2_Cab_Tp_oCabSubnet_15c35ee3_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   111 */ 0x9F40F398u,                297u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,       9u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagRespMsgIntHMIIOM_F3_98_Cab_Tp_oCabSubnet_b05496ae_Rx]      */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagRespMsgIntHMIIOM_F3_98_Cab_Tp_oCabSubnet_b05496ae_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   112 */ 0x9F40F353u,                298u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      10u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagRespMsgIntHMIIOM_F3_53_Cab_Tp_oCabSubnet_507b9b04_Rx]      */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagRespMsgIntHMIIOM_F3_53_Cab_Tp_oCabSubnet_507b9b04_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   113 */ 0x9F40F326u,                299u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      11u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagRespMsgIntHMIIOM_F3_26_Cab_Tp_oCabSubnet_68d46304_Rx]      */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagRespMsgIntHMIIOM_F3_26_Cab_Tp_oCabSubnet_68d46304_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   114 */ 0x9F40F2A2u,                300u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      12u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PhysDiagRespMsg_F2_A2_Cab_Tp_oCabSubnet_89846943_Rx]           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PhysDiagRespMsg_F2_A2_Cab_Tp_oCabSubnet_89846943_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   115 */ 0x9F40F298u,                301u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      13u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PhysDiagRespMsg_F2_98_Cab_Tp_oCabSubnet_af4ee02a_Rx]           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PhysDiagRespMsg_F2_98_Cab_Tp_oCabSubnet_af4ee02a_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   116 */ 0x9F40F253u,                302u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      14u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PhysDiagRespMsg_F2_53_Cab_Tp_oCabSubnet_4c48e914_Rx]           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PhysDiagRespMsg_F2_53_Cab_Tp_oCabSubnet_4c48e914_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   117 */ 0x9F40F226u,                303u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      15u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PhysDiagRespMsg_F2_26_Cab_Tp_oCabSubnet_48eedf32_Rx]           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PhysDiagRespMsg_F2_26_Cab_Tp_oCabSubnet_48eedf32_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   118 */ 0x9CFF9050u,                304u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      16u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_AnmMsg_ECUspare6_CabSubnet_oCabSubnet_582cafe8_Rx]             */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_AnmMsg_ECUspare6_CabSubnet_oCabSubnet_582cafe8_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   119 */ 0x9CFF902Bu,                305u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      17u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_AnmMsg_WRCS_CabSubnet_oCabSubnet_62e3be6f_Rx]                  */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_AnmMsg_WRCS_CabSubnet_oCabSubnet_62e3be6f_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   120 */ 0x9CFF902Au,                306u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      18u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_AnmMsg_SRS_CabSubnet_oCabSubnet_690513e9_Rx]                   */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_AnmMsg_SRS_CabSubnet_oCabSubnet_690513e9_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   121 */ 0x9CFF9028u,                307u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      19u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_AnmMsg_LECM1_CabSubnet_oCabSubnet_ba3b1140_Rx]                 */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_AnmMsg_LECM1_CabSubnet_oCabSubnet_ba3b1140_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   122 */ 0x9CFF9025u,                308u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      20u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_AnmMsg_CCM_CabSubnet_oCabSubnet_7e592096_Rx]                   */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_AnmMsg_CCM_CabSubnet_oCabSubnet_7e592096_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   123 */ 0x9902301Fu,                309u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      21u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagFaultStat_WRCS_Cab_oCabSubnet_b86e4ab7_Rx]                 */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagFaultStat_WRCS_Cab_oCabSubnet_b86e4ab7_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   124 */ 0x9902301Eu,                310u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      22u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagFaultStat_SRS_Cab_oCabSubnet_e5f44b73_Rx]                  */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagFaultStat_SRS_Cab_oCabSubnet_e5f44b73_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   125 */ 0x9902301Du,                311u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      23u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagFaultStat_LECM_Cab_oCabSubnet_6e0076c7_Rx]                 */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagFaultStat_LECM_Cab_oCabSubnet_6e0076c7_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   126 */ 0x9902301Cu,                312u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      24u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagFaultStat_CCM_Cab_oCabSubnet_2b754d89_Rx]                  */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagFaultStat_CCM_Cab_oCabSubnet_2b754d89_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   127 */ 0x98FF3FC0u,                313u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      25u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_20S_FCM_Tp_oCabSubnet_88858df9_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_20S_FCM_Tp_oCabSubnet_88858df9_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   128 */ 0x98FEA953u,                314u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      26u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_SRS_Cab_06P_oCabSubnet_bec0c2b2_Rx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_SRS_Cab_06P_oCabSubnet_bec0c2b2_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   129 */ 0x98FEA753u,                315u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      27u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_SRS_Cab_05P_oCabSubnet_2fb89566_Rx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_SRS_Cab_05P_oCabSubnet_2fb89566_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   130 */ 0x98F78141u,                316u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      28u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_34P_FCM_Tp_oCabSubnet_d2bbef9e_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_34P_FCM_Tp_oCabSubnet_d2bbef9e_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   131 */ 0x98F78041u,                317u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      29u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_30S_FCM_Tp_oCabSubnet_ae18d4ab_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_30S_FCM_Tp_oCabSubnet_ae18d4ab_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   132 */ 0x98F77F41u,                318u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      30u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_25S_FCM_Tp_oCabSubnet_063a8d48_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_25S_FCM_Tp_oCabSubnet_063a8d48_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   133 */ 0x98F2B7FFu,                319u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      31u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_LECM1_Cab_03S_oCabSubnet_e20ca5cb_Rx]                          */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_LECM1_Cab_03S_oCabSubnet_e20ca5cb_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   134 */ 0x98F1A800u,                320u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      32u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_13S_FCM_Tp_oCabSubnet_2f98645f_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_13S_FCM_Tp_oCabSubnet_2f98645f_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   135 */ 0x98EF8800u,                321u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      33u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_11S_FCM_Tp_oCabSubnet_a74b983f_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_11S_FCM_Tp_oCabSubnet_a74b983f_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   136 */ 0x98DAF153u,                322u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      34u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PhysDiagRespMsg_F1_53_Cab_Tp_oCabSubnet_1d55a011_Rx]           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PhysDiagRespMsg_F1_53_Cab_Tp_oCabSubnet_1d55a011_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   137 */ 0x95FAA753u,                323u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      35u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_SRS_Cab_04P_oCabSubnet_5f6f582a_Rx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_SRS_Cab_04P_oCabSubnet_5f6f582a_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   138 */ 0x94FFBF3Fu,                324u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      36u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_SRS_Cab_03P_oCabSubnet_d6393c8f_Rx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_SRS_Cab_03P_oCabSubnet_d6393c8f_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   139 */ 0x94FF7FA2u,                325u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      37u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_LECM1_Cab_05S_oCabSubnet_b18bf7f9_Rx]                          */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_LECM1_Cab_05S_oCabSubnet_b18bf7f9_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   140 */ 0x94FF7F26u,                326u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      38u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_WRCS_Cab_03P_oCabSubnet_37a4b7c4_Rx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_WRCS_Cab_03P_oCabSubnet_37a4b7c4_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   141 */ 0x946644A2u,                327u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      39u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_LECM1_Cab_04P_oCabSubnet_b355ee77_Rx]                          */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_LECM1_Cab_04P_oCabSubnet_b355ee77_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   142 */ 0x90FFF3A2u,                328u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      40u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_LECM1_Cab_02P_oCabSubnet_e0d2bc45_Rx]                          */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_LECM1_Cab_02P_oCabSubnet_e0d2bc45_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   143 */ 0x90FFE553u,                329u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      41u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_SRS_Cab_01P_oCabSubnet_3796a617_Rx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_SRS_Cab_01P_oCabSubnet_3796a617_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   144 */ 0x90FFBF41u,                330u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      42u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_29S_FCM_Tp_oCabSubnet_8230888a_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_29S_FCM_Tp_oCabSubnet_8230888a_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   145 */ 0x90FFBF3Fu,                331u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      43u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_WRCS_Cab_02P_oCabSubnet_506736c4_Rx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_WRCS_Cab_02P_oCabSubnet_506736c4_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   146 */ 0x90FF7F41u,                332u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      44u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_05P_FCM_Tp_oCabSubnet_fd75fa58_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_05P_FCM_Tp_oCabSubnet_fd75fa58_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   147 */ 0x90FF6841u,                333u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      45u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Cab_04P_FCM_Tp_oCabSubnet_b91c0468_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Cab_04P_FCM_Tp_oCabSubnet_b91c0468_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   148 */ 0x90FF3978u,                334u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      46u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CCM_Cab_06P_oCabSubnet_ae969834_Rx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CCM_Cab_06P_oCabSubnet_ae969834_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   149 */ 0x90FF3698u,                335u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      47u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CCM_Cab_07P_oCabSubnet_de415578_Rx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CCM_Cab_07P_oCabSubnet_de415578_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
    /* Index    IDValue      MemorySectionsIndex  ActiveSendObject  ControllerConfigIdx                                                                 HwHandle  MailboxSize  MailboxType                                MaxDataLen        Comment                                                                                       Referable Keys */
  { /*   150 */ 0x90FF2698u,                336u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      48u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CCM_Cab_08P_oCabSubnet_674b573f_Rx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CCM_Cab_08P_oCabSubnet_674b573f_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   151 */ 0x90FF2298u,                337u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      49u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CCM_Cab_04P_oCabSubnet_4f3902ac_Rx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CCM_Cab_04P_oCabSubnet_4f3902ac_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   152 */ 0x90FF1998u,                338u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      50u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CCM_Cab_01P_oCabSubnet_27c0fc91_Rx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CCM_Cab_01P_oCabSubnet_27c0fc91_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   153 */ 0x90FF0B98u,                339u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      51u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CCM_Cab_03P_Tp_oCabSubnet_bfb45635_Rx]                         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CCM_Cab_03P_Tp_oCabSubnet_bfb45635_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   154 */ 0x00000000u,                340u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      52u,          2u, CAN_RX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject]                                                               */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   155 */ 0x00000000u,                342u,               0u,                  3u  /* /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe */     ,      54u,          2u, CAN_RX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CN_CabSubnet_eedbadff_1_Rx]                                                      */  /* [/ActiveEcuC/Can/CanConfigSet/CN_CabSubnet_eedbadff_1_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe, RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_CabSubnet_d2ff0fbe (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   156 */ 0x8CF00331u,                389u,              44u,                  4u  /* /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 */        ,       5u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_EEC2_X_CIOMFMS_oFMSNet_d3e60e6a_Tx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_EEC2_X_CIOMFMS_oFMSNet_d3e60e6a_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   157 */ 0x8CF00431u,                390u,              45u,                  4u  /* /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 */        ,       6u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_EEC1_X_CIOMFMS_oFMSNet_954d78de_Tx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_EEC1_X_CIOMFMS_oFMSNet_954d78de_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   158 */ 0x8CFE6C31u,                391u,              46u,                  4u  /* /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 */        ,       7u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_TCO1_X_CIOMFMS_oFMSNet_567baf3d_Tx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_TCO1_X_CIOMFMS_oFMSNet_567baf3d_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   159 */ 0x98F0000Fu,                392u,              47u,                  4u  /* /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 */        ,       8u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_ERC1_x_EMSRetFMS_oFMSNet_d863a207_Tx]                          */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_ERC1_x_EMSRetFMS_oFMSNet_d863a207_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   160 */ 0x98F00010u,                393u,              48u,                  4u  /* /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 */        ,       9u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_ERC1_x_RECUFMS_oFMSNet_338e7918_Tx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_ERC1_x_RECUFMS_oFMSNet_338e7918_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   161 */ 0x98FDA431u,                394u,              49u,                  4u  /* /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 */        ,      10u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PTODE_X_CIOMFMS_oFMSNet_8abc23b8_Tx]                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PTODE_X_CIOMFMS_oFMSNet_8abc23b8_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   162 */ 0x98FEF131u,                395u,              50u,                  4u  /* /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 */        ,      11u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CCVS_X_CIOMFMS_oFMSNet_1dbcfa70_Tx]                            */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CCVS_X_CIOMFMS_oFMSNet_1dbcfa70_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   163 */ 0x98FEF231u,                396u,              51u,                  4u  /* /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 */        ,      12u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_LFE_X_CIOMFMS_oFMSNet_adac24c8_Tx]                             */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_LFE_X_CIOMFMS_oFMSNet_adac24c8_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   164 */ 0x00000000u,                388u,              52u,                  4u  /* /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 */        ,       4u,          1u, CAN_TX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CN_FMSNet_fce1aae5_Tx]                                                           */  /* [/ActiveEcuC/Can/CanConfigSet/CN_FMSNet_fce1aae5_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706, TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   165 */ 0x00000000u,                384u,               0u,                  4u  /* /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 */        ,       0u,          2u, CAN_RX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_002]                                                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_002 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706, RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   166 */ 0x00000000u,                386u,               0u,                  4u  /* /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 */        ,       2u,          2u, CAN_RX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CN_FMSNet_41e51372_1_Rx]                                                         */  /* [/ActiveEcuC/Can/CanConfigSet/CN_FMSNet_41e51372_1_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706, RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_FMSNet_119a8706 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   167 */ 0x90FF2F40u,                523u,              53u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      43u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Sec_01P_oSecuritySubnet_de08418d_Tx]                      */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Sec_01P_oSecuritySubnet_de08418d_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   168 */ 0x90FF7B40u,                524u,              54u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      44u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Sec_04P_oSecuritySubnet_6a707225_Tx]                      */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Sec_04P_oSecuritySubnet_6a707225_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   169 */ 0x94FF3F40u,                525u,              55u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      45u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Sec_10S_Tp_oSecuritySubnet_8c7f36ae_Tx]                   */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Sec_10S_Tp_oSecuritySubnet_8c7f36ae_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   170 */ 0x94FF7240u,                526u,              56u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      46u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Sec_12S_Tp_oSecuritySubnet_2d9a0e12_Tx]                   */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Sec_12S_Tp_oSecuritySubnet_2d9a0e12_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   171 */ 0x98FF9440u,                527u,              57u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      47u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Sec_03P_oSecuritySubnet_4d58f6b7_Tx]                      */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Sec_03P_oSecuritySubnet_4d58f6b7_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   172 */ 0x98FFAA40u,                528u,              58u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      48u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Sec_02P_oSecuritySubnet_04f0ad2a_Tx]                      */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Sec_02P_oSecuritySubnet_04f0ad2a_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   173 */ 0x9CFF9040u,                529u,              59u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      49u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_AnmMsg_CIOM_SecuritySubnet_oSecuritySubnet_d0267e59_Tx]        */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_AnmMsg_CIOM_SecuritySubnet_oSecuritySubnet_d0267e59_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   174 */ 0x9F40A0F2u,                530u,              60u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      50u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PhysDiagReqMsg_A0_F2_Sec_Tp_oSecuritySubnet_cb87d97b_Tx]       */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PhysDiagReqMsg_A0_F2_Sec_Tp_oSecuritySubnet_cb87d97b_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   175 */ 0x9F40A0F3u,                531u,              61u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      51u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagReqMsgIntHMIIOM_A0_F3_Sec_Tp_oSecuritySubnet_09265670_Tx]  */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagReqMsgIntHMIIOM_A0_F3_Sec_Tp_oSecuritySubnet_09265670_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   176 */ 0x9F40A0F4u,                532u,              62u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      52u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagReqMsgIntTGW2_A0_F4_Sec_Tp_oSecuritySubnet_8adef0bc_Tx]    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagReqMsgIntTGW2_A0_F4_Sec_Tp_oSecuritySubnet_8adef0bc_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   177 */ 0x9F40A1F2u,                533u,              63u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      53u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PhysDiagReqMsg_A1_F2_Sec_Tp_oSecuritySubnet_5d2e792a_Tx]       */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PhysDiagReqMsg_A1_F2_Sec_Tp_oSecuritySubnet_5d2e792a_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   178 */ 0x9F40A1F3u,                534u,              64u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      54u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagReqMsgIntHMIIOM_A1_F3_Sec_Tp_oSecuritySubnet_a1b03b66_Tx]  */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagReqMsgIntHMIIOM_A1_F3_Sec_Tp_oSecuritySubnet_a1b03b66_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   179 */ 0x9F40A1F4u,                535u,              65u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      55u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagReqMsgIntTGW2_A1_F4_Sec_Tp_oSecuritySubnet_eb89cc56_Tx]    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagReqMsgIntTGW2_A1_F4_Sec_Tp_oSecuritySubnet_eb89cc56_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   180 */ 0x9F40C0F2u,                536u,              66u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      56u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PhysDiagReqMsg_C0_F2_Sec_Tp_oSecuritySubnet_f27248ff_Tx]       */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PhysDiagReqMsg_C0_F2_Sec_Tp_oSecuritySubnet_f27248ff_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   181 */ 0x9F40C0F3u,                537u,              67u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      57u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagReqMsgIntHMIIOM_C0_F3_Sec_Tp_oSecuritySubnet_3aaf1649_Tx]  */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagReqMsgIntHMIIOM_C0_F3_Sec_Tp_oSecuritySubnet_3aaf1649_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   182 */ 0x9F40C0F4u,                538u,              68u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      58u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagReqMsgIntTGW2_C0_F4_Sec_Tp_oSecuritySubnet_0ba24808_Tx]    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagReqMsgIntTGW2_C0_F4_Sec_Tp_oSecuritySubnet_0ba24808_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   183 */ 0x9F41FFF2u,                539u,              69u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      59u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_TesterFuncDiagMsg_Sec_Tp_oSecuritySubnet_7f5a7347_Tx]          */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_TesterFuncDiagMsg_Sec_Tp_oSecuritySubnet_7f5a7347_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   184 */ 0x9F41FFF4u,                540u,              70u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      60u,          1u,  CAN_TX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_IntTesterTGW2FuncDiagMsg_Sec_Tp_oSecuritySubnet_57d96404_Tx]   */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_IntTesterTGW2FuncDiagMsg_Sec_Tp_oSecuritySubnet_57d96404_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   185 */ 0x00000000u,                522u,              71u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      42u,          1u, CAN_TX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CN_SecuritySubnet_e7a0ee54_Tx]                                                   */  /* [/ActiveEcuC/Can/CanConfigSet/CN_SecuritySubnet_e7a0ee54_Tx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, TX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   186 */ 0x9F42F2C0u,                480u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,       0u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagUUDTRespMsg1_F2_C0_Sec_oSecuritySubnet_d0c97a87_Rx]        */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagUUDTRespMsg1_F2_C0_Sec_oSecuritySubnet_d0c97a87_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   187 */ 0x9F42F2A1u,                481u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,       1u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagUUDTRespMsg1_F2_A1_Sec_oSecuritySubnet_67c5eccf_Rx]        */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagUUDTRespMsg1_F2_A1_Sec_oSecuritySubnet_67c5eccf_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   188 */ 0x9F42F2A0u,                482u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,       2u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagUUDTRespMsg1_F2_A0_Sec_oSecuritySubnet_46825d6a_Rx]        */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagUUDTRespMsg1_F2_A0_Sec_oSecuritySubnet_46825d6a_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   189 */ 0x9F40F4C0u,                483u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,       3u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagRespMsgIntTGW2_F4_C0_Sec_Tp_oSecuritySubnet_7a975d32_Rx]   */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagRespMsgIntTGW2_F4_C0_Sec_Tp_oSecuritySubnet_7a975d32_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   190 */ 0x9F40F4A1u,                484u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,       4u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagRespMsgIntTGW2_F4_A1_Sec_Tp_oSecuritySubnet_efa76b9d_Rx]   */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagRespMsgIntTGW2_F4_A1_Sec_Tp_oSecuritySubnet_efa76b9d_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   191 */ 0x9F40F4A0u,                485u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,       5u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagRespMsgIntTGW2_F4_A0_Sec_Tp_oSecuritySubnet_d9469eac_Rx]   */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagRespMsgIntTGW2_F4_A0_Sec_Tp_oSecuritySubnet_d9469eac_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   192 */ 0x9F40F3C0u,                486u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,       6u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagRespMsgIntHMIIOM_F3_C0_Sec_Tp_oSecuritySubnet_77f0a36b_Rx] */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagRespMsgIntHMIIOM_F3_C0_Sec_Tp_oSecuritySubnet_77f0a36b_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   193 */ 0x9F40F3A1u,                487u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,       7u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagRespMsgIntHMIIOM_F3_A1_Sec_Tp_oSecuritySubnet_052f8796_Rx] */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagRespMsgIntHMIIOM_F3_A1_Sec_Tp_oSecuritySubnet_052f8796_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   194 */ 0x9F40F3A0u,                488u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,       8u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagRespMsgIntHMIIOM_F3_A0_Sec_Tp_oSecuritySubnet_95b29252_Rx] */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagRespMsgIntHMIIOM_F3_A0_Sec_Tp_oSecuritySubnet_95b29252_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   195 */ 0x9F40F2C0u,                489u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,       9u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PhysDiagRespMsg_F2_C0_Sec_Tp_oSecuritySubnet_614a0693_Rx]      */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PhysDiagRespMsg_F2_C0_Sec_Tp_oSecuritySubnet_614a0693_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   196 */ 0x9F40F2A1u,                490u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      10u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PhysDiagRespMsg_F2_A1_Sec_Tp_oSecuritySubnet_d47cc691_Rx]      */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PhysDiagRespMsg_F2_A1_Sec_Tp_oSecuritySubnet_d47cc691_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   197 */ 0x9F40F2A0u,                491u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      11u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PhysDiagRespMsg_F2_A0_Sec_Tp_oSecuritySubnet_207d0e64_Rx]      */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PhysDiagRespMsg_F2_A0_Sec_Tp_oSecuritySubnet_207d0e64_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   198 */ 0x9CFF9049u,                492u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      12u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_AnmMsg_ECUspare5_SecuritySubnet_oSecuritySubnet_25af973c_Rx]   */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_AnmMsg_ECUspare5_SecuritySubnet_oSecuritySubnet_25af973c_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   199 */ 0x9CFF9029u,                493u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      13u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_AnmMsg_PDM_SecuritySubnet_oSecuritySubnet_d922132b_Rx]         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_AnmMsg_PDM_SecuritySubnet_oSecuritySubnet_d922132b_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
    /* Index    IDValue      MemorySectionsIndex  ActiveSendObject  ControllerConfigIdx                                                                 HwHandle  MailboxSize  MailboxType                                MaxDataLen        Comment                                                                                       Referable Keys */
  { /*   200 */ 0x9CFF9027u,                494u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      14u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_AnmMsg_DDM_SecuritySubnet_oSecuritySubnet_669221a0_Rx]         */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_AnmMsg_DDM_SecuritySubnet_oSecuritySubnet_669221a0_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   201 */ 0x9CFF9024u,                495u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      15u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_AnmMsg_Alarm_SecuritySubnet_oSecuritySubnet_38c19358_Rx]       */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_AnmMsg_Alarm_SecuritySubnet_oSecuritySubnet_38c19358_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   202 */ 0x99FFD4A1u,                496u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      16u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagFaultStat_PDM_Sec_oSecuritySubnet_b17b40b8_Rx]             */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagFaultStat_PDM_Sec_oSecuritySubnet_b17b40b8_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   203 */ 0x99FF7FA0u,                497u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      17u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagFaultStat_DDM_Sec_oSecuritySubnet_3b5b41e4_Rx]             */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagFaultStat_DDM_Sec_oSecuritySubnet_3b5b41e4_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   204 */ 0x99FF2AC0u,                498u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      18u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DiagFaultStat_Alarm_Sec_oSecuritySubnet_6cedaf7c_Rx]           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DiagFaultStat_Alarm_Sec_oSecuritySubnet_6cedaf7c_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   205 */ 0x94FFF2A1u,                499u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      19u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PDM_Sec_04S_Tp_oSecuritySubnet_596ba3c3_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PDM_Sec_04S_Tp_oSecuritySubnet_596ba3c3_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   206 */ 0x94FFE9C0u,                500u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      20u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Sec_05S_FCM_Tp_oSecuritySubnet_02ea2344_Rx]               */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Sec_05S_FCM_Tp_oSecuritySubnet_02ea2344_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   207 */ 0x94FFD8A1u,                501u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      21u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PDM_Sec_03S_Tp_oSecuritySubnet_225f8534_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PDM_Sec_03S_Tp_oSecuritySubnet_225f8534_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   208 */ 0x94FFBFA0u,                502u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      22u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DDM_Sec_05S_Tp_oSecuritySubnet_5a1ed97b_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DDM_Sec_05S_Tp_oSecuritySubnet_5a1ed97b_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   209 */ 0x94FFBF3Fu,                503u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      23u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_Alarm_Sec_07S_Tp_oSecuritySubnet_bf7e0710_Rx]                  */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_Alarm_Sec_07S_Tp_oSecuritySubnet_bf7e0710_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   210 */ 0x94FFA5A0u,                504u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      24u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DDM_Sec_04S_Tp_oSecuritySubnet_c3efbadc_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DDM_Sec_04S_Tp_oSecuritySubnet_c3efbadc_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   211 */ 0x94FF94BFu,                505u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      25u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_Alarm_Sec_06S_Tp_oSecuritySubnet_2b24d209_Rx]                  */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_Alarm_Sec_06S_Tp_oSecuritySubnet_2b24d209_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   212 */ 0x94FF8CA0u,                506u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      26u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DDM_Sec_03S_Tp_oSecuritySubnet_b8db9c2b_Rx]                    */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DDM_Sec_03S_Tp_oSecuritySubnet_b8db9c2b_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   213 */ 0x94FF7241u,                507u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      27u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Sec_12S_FCM_Tp_oSecuritySubnet_9cba7397_Rx]               */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Sec_12S_FCM_Tp_oSecuritySubnet_9cba7397_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   214 */ 0x94FF5FA0u,                508u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      28u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Sec_07S_FCM_Tp_oSecuritySubnet_5acc46b8_Rx]               */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Sec_07S_FCM_Tp_oSecuritySubnet_5acc46b8_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   215 */ 0x94FF5941u,                509u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      29u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Sec_11S_FCM_Tp_oSecuritySubnet_e88f2495_Rx]               */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Sec_11S_FCM_Tp_oSecuritySubnet_e88f2495_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   216 */ 0x94FF3F41u,                510u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      30u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Sec_10S_FCM_Tp_oSecuritySubnet_c49c166b_Rx]               */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Sec_10S_FCM_Tp_oSecuritySubnet_c49c166b_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   217 */ 0x94FF2641u,                511u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      31u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Sec_09S_FCM_Tp_oSecuritySubnet_094e790d_Rx]               */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Sec_09S_FCM_Tp_oSecuritySubnet_094e790d_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   218 */ 0x94FF1FE0u,                512u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      32u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Sec_06S_FCM_Tp_oSecuritySubnet_76df7446_Rx]               */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Sec_06S_FCM_Tp_oSecuritySubnet_76df7446_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   219 */ 0x94FF153Fu,                513u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      33u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_Alarm_Sec_03S_Tp_oSecuritySubnet_838659b7_Rx]                  */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_Alarm_Sec_03S_Tp_oSecuritySubnet_838659b7_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   220 */ 0x94FF0C41u,                514u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      34u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_CIOM_Sec_08S_FCM_Tp_oSecuritySubnet_255d4bf3_Rx]               */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_CIOM_Sec_08S_FCM_Tp_oSecuritySubnet_255d4bf3_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   221 */ 0x90FFE9A1u,                515u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      35u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_PDM_Sec_01P_oSecuritySubnet_073fa36a_Rx]                       */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_PDM_Sec_01P_oSecuritySubnet_073fa36a_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   222 */ 0x90FFBFA0u,                516u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      36u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_DDM_Sec_01P_oSecuritySubnet_09e2b30b_Rx]                       */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_DDM_Sec_01P_oSecuritySubnet_09e2b30b_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   223 */ 0x90FF35C0u,                517u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      37u,          1u,  CAN_RX_FULLCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_Alarm_Sec_02P_oSecuritySubnet_a902993f_Rx]                     */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_Alarm_Sec_02P_oSecuritySubnet_a902993f_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_FULLCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   224 */ 0x00000000u,                518u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      38u,          2u, CAN_RX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u },  /* [CanHardwareObject_001]                                                           */  /* [/ActiveEcuC/Can/CanConfigSet/CanHardwareObject_001 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
  { /*   225 */ 0x00000000u,                520u,               0u,                  5u  /* /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 */,      40u,          2u, CAN_RX_BASICCAN_TYPE_MAILBOXTYPEOFMAILBOX,         8u }   /* [CN_SecuritySubnet_3840872e_1_Rx]                                                 */  /* [/ActiveEcuC/Can/CanConfigSet/CN_SecuritySubnet_3840872e_1_Rx (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanHardwareObject), /ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6, RX_BASICCAN_TYPE/ActiveEcuC/Can/CanConfigSet/CT_SecuritySubnet_f5346ae6 (DefRef: /MICROSAR/Can_ImxFlexcan3/Can/CanConfigSet/CanController)] */
};
#define CAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_MemorySectionInfo
**********************************************************************************************************************/
/** 
  \var    Can_MemorySectionInfo
  \brief  Memory section description
  \details
  Element               Description
  MemoryStartAddress
  MemorySectionStart
*/ 
#define CAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Can_MemorySectionInfoType, CAN_CONST) Can_MemorySectionInfo[6] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    MemoryStartAddress  MemorySectionStart        Comment */
  { /*     0 */        0xFFEC8000u,                 0u },  /* [MemorySection: Memory_0] */
  { /*     1 */        0xFBECC000u,                96u },  /* [MemorySection: Memory_1] */
  { /*     2 */        0xFFEC0000u,               192u },  /* [MemorySection: Memory_2] */
  { /*     3 */        0xFFECC000u,               288u },  /* [MemorySection: Memory_3] */
  { /*     4 */        0xFFEC4000u,               384u },  /* [MemorySection: Memory_4] */
  { /*     5 */        0xFBEC0000u,               480u }   /* [MemorySection: Memory_5] */
};
#define CAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_MemorySectionObjects
**********************************************************************************************************************/
/** 
  \var    Can_MemorySectionObjects
  \brief  Memory section objects description
  \details
  Element           Description
  HwHandle      
  MailboxElement
  MailboxHandle 
*/ 
#define CAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Can_MemorySectionObjectsType, CAN_CONST) Can_MemorySectionObjects[576] = {  /* PRQA S 1514, 1533, 0612 */  /* MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_BigStructure */
    /* Index    HwHandle  MailboxElement  MailboxHandle        Comment */
  { /*     0 */       0u,             0u,            5u },  /* [Memory_0 - CN_Backbone1J1939_6e8ec372_1_Rx] */
  { /*     1 */       1u,             1u,            5u },  /* [Memory_0 - CN_Backbone1J1939_6e8ec372_1_Rx] */
  { /*     2 */       2u,             0u,            6u },  /* [Memory_0 - CN_Backbone1J1939_6e8ec372_Rx] */
  { /*     3 */       3u,             1u,            6u },  /* [Memory_0 - CN_Backbone1J1939_6e8ec372_Rx] */
  { /*     4 */       4u,             0u,            4u },  /* [Memory_0 - CN_Backbone1J1939_0b1f4bae_Tx] */
  { /*     5 */       5u,             0u,            0u },  /* [Memory_0 - CanHardwareObject_AckmTxPdu_Backbone1J1939_54966c1b] */
  { /*     6 */       6u,             0u,            1u },  /* [Memory_0 - CanHardwareObject_FcNPdu_Backbone1J1939_dba64907] */
  { /*     7 */       7u,             0u,            2u },  /* [Memory_0 - CanHardwareObject_CIOM_BB1_01P_oBackbone1J1939_55a00301_Tx] */
  { /*     8 */       8u,             0u,            3u },  /* [Memory_0 - CanHardwareObject_J1939NmTxPdu_fa509995] */
  { /*     9 */       9u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    10 */      10u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    11 */      11u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    12 */      12u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    13 */      13u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    14 */      14u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    15 */      15u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    16 */      16u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    17 */      17u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    18 */      18u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    19 */      19u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    20 */      20u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    21 */      21u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    22 */      22u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    23 */      23u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    24 */      24u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    25 */      25u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    26 */      26u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    27 */      27u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    28 */      28u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    29 */      29u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    30 */      30u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    31 */      31u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    32 */      32u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    33 */      33u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    34 */      34u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    35 */      35u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    36 */      36u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    37 */      37u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    38 */      38u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    39 */      39u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    40 */      40u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    41 */      41u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    42 */      42u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    43 */      43u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    44 */      44u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    45 */      45u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    46 */      46u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    47 */      47u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    48 */      48u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    49 */      49u,             0u,            0u },  /* [Memory_0 - Reserved] */
    /* Index    HwHandle  MailboxElement  MailboxHandle        Comment */
  { /*    50 */      50u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    51 */      51u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    52 */      52u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    53 */      53u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    54 */      54u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    55 */      55u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    56 */      56u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    57 */      57u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    58 */      58u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    59 */      59u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    60 */      60u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    61 */      61u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    62 */      62u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    63 */      63u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    64 */      64u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    65 */      65u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    66 */      66u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    67 */      67u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    68 */      68u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    69 */      69u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    70 */      70u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    71 */      71u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    72 */      72u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    73 */      73u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    74 */      74u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    75 */      75u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    76 */      76u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    77 */      77u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    78 */      78u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    79 */      79u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    80 */      80u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    81 */      81u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    82 */      82u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    83 */      83u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    84 */      84u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    85 */      85u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    86 */      86u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    87 */      87u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    88 */      88u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    89 */      89u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    90 */      90u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    91 */      91u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    92 */      92u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    93 */      93u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    94 */      94u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    95 */      95u,             0u,            0u },  /* [Memory_0 - Reserved] */
  { /*    96 */       0u,             0u,           27u },  /* [Memory_1 - CanHardwareObject_IntTesterTGW2FuncDiagMsg_BB2_Tp_oBackbone2_c6fcfce9_Rx] */
  { /*    97 */       1u,             0u,           28u },  /* [Memory_1 - CanHardwareObject_TesterFuncDiagMsg_BB2_Tp_oBackbone2_6ec04186_Rx] */
  { /*    98 */       2u,             0u,           29u },  /* [Memory_1 - CanHardwareObject_DiagReqMsgIntTGW2_A2_F4_BB2_Tp_oBackbone2_8613fab7_Rx] */
  { /*    99 */       3u,             0u,           30u },  /* [Memory_1 - CanHardwareObject_DiagReqMsgIntHMIIOM_A2_F3_BB2_Tp_oBackbone2_e882bd41_Rx] */
    /* Index    HwHandle  MailboxElement  MailboxHandle        Comment */
  { /*   100 */       4u,             0u,           31u },  /* [Memory_1 - CanHardwareObject_PhysDiagReqMsg_A2_F2_BB2_Tp_oBackbone2_e7d85c22_Rx] */
  { /*   101 */       5u,             0u,           32u },  /* [Memory_1 - CanHardwareObject_DiagReqMsgIntTGW2_A1_F4_BB2_Tp_oBackbone2_91dc1a3b_Rx] */
  { /*   102 */       6u,             0u,           33u },  /* [Memory_1 - CanHardwareObject_DiagReqMsgIntHMIIOM_A1_F3_BB2_Tp_oBackbone2_ba062351_Rx] */
  { /*   103 */       7u,             0u,           34u },  /* [Memory_1 - CanHardwareObject_PhysDiagReqMsg_A1_F2_BB2_Tp_oBackbone2_9bf67e6d_Rx] */
  { /*   104 */       8u,             0u,           35u },  /* [Memory_1 - CanHardwareObject_DiagReqMsgIntTGW2_A0_F4_BB2_Tp_oBackbone2_9c9945bf_Rx] */
  { /*   105 */       9u,             0u,           36u },  /* [Memory_1 - CanHardwareObject_DiagReqMsgIntHMIIOM_A0_F3_BB2_Tp_oBackbone2_3d55549e_Rx] */
  { /*   106 */      10u,             0u,           37u },  /* [Memory_1 - CanHardwareObject_PhysDiagReqMsg_A0_F2_BB2_Tp_oBackbone2_b0139fa8_Rx] */
  { /*   107 */      11u,             0u,           38u },  /* [Memory_1 - CanHardwareObject_DiagReqMsgIntTGW2_26_F4_BB2_Tp_oBackbone2_ce56ef15_Rx] */
  { /*   108 */      12u,             0u,           39u },  /* [Memory_1 - CanHardwareObject_DiagReqMsgIntHMIIOM_26_F3_BB2_Tp_oBackbone2_3a21aae6_Rx] */
  { /*   109 */      13u,             0u,           40u },  /* [Memory_1 - CanHardwareObject_PhysDiagReqMsg_26_F2_BB2_Tp_oBackbone2_680a80cc_Rx] */
  { /*   110 */      14u,             0u,           41u },  /* [Memory_1 - CanHardwareObject_DebugCtrl1_CIOM_BB2_oBackbone2_ca351e0d_Rx] */
  { /*   111 */      15u,             0u,           42u },  /* [Memory_1 - CanHardwareObject_HMIIOM_BB2_18P_oBackbone2_2f8dfe18_Rx] */
  { /*   112 */      16u,             0u,           43u },  /* [Memory_1 - CanHardwareObject_HMIIOM_BB2_15P_oBackbone2_fb4261c4_Rx] */
  { /*   113 */      17u,             0u,           44u },  /* [Memory_1 - CanHardwareObject_HMIIOM_BB2_38P_oBackbone2_17230dd8_Rx] */
  { /*   114 */      18u,             0u,           45u },  /* [Memory_1 - CanHardwareObject_HMIIOM_BB2_12P_oBackbone2_5a09ee73_Rx] */
  { /*   115 */      19u,             0u,           46u },  /* [Memory_1 - CanHardwareObject_HMIIOM_BB2_11P_oBackbone2_17206880_Rx] */
  { /*   116 */      20u,             0u,           47u },  /* [Memory_1 - CanHardwareObject_TECU_BB2_06S_Tp_oBackbone2_0c47cb8e_Rx] */
  { /*   117 */      21u,             0u,           48u },  /* [Memory_1 - CanHardwareObject_HMIIOM_BB2_27S_Tp_oBackbone2_aeede795_Rx] */
  { /*   118 */      22u,             0u,           49u },  /* [Memory_1 - CanHardwareObject_EMS_BB2_08P_oBackbone2_dd5310ea_Rx] */
  { /*   119 */      23u,             0u,           50u },  /* [Memory_1 - CanHardwareObject_EMS_BB2_06P_oBackbone2_148edfe1_Rx] */
  { /*   120 */      24u,             0u,           51u },  /* [Memory_1 - CanHardwareObject_HMIIOM_BB2_19P_CIOM_Tp_oBackbone2_470ca910_Rx] */
  { /*   121 */      25u,             0u,           52u },  /* [Memory_1 - CanHardwareObject_VMCU_BB2_31S_Tp_oBackbone2_210b49cb_Rx] */
  { /*   122 */      26u,             0u,           53u },  /* [Memory_1 - CanHardwareObject_VMCU_BB2_32S_Tp_oBackbone2_2c992e71_Rx] */
  { /*   123 */      27u,             0u,           54u },  /* [Memory_1 - CanHardwareObject_EMS_BB2_09S_Tp_oBackbone2_ef0c9958_Rx] */
  { /*   124 */      28u,             0u,           55u },  /* [Memory_1 - CanHardwareObject_EMS_BB2_04P_oBackbone2_f5214579_Rx] */
  { /*   125 */      29u,             0u,           56u },  /* [Memory_1 - CanHardwareObject_EMS_BB2_13P_oBackbone2_036dfbaa_Rx] */
  { /*   126 */      30u,             0u,           57u },  /* [Memory_1 - CanHardwareObject_EMS_BB2_05P_oBackbone2_85f68835_Rx] */
  { /*   127 */      31u,             0u,           58u },  /* [Memory_1 - CanHardwareObject_HMIIOM_BB2_05P_oBackbone2_e7151824_Rx] */
  { /*   128 */      32u,             0u,           59u },  /* [Memory_1 - CanHardwareObject_BBM_BB2_03S_CIOM_Tp_oBackbone2_8a8d09f2_Rx] */
  { /*   129 */      33u,             0u,           60u },  /* [Memory_1 - CanHardwareObject_VMCU_BB2_55P_oBackbone2_a30a2c5d_Rx] */
  { /*   130 */      34u,             0u,           61u },  /* [Memory_1 - CanHardwareObject_HMIIOM_BB2_23P_oBackbone2_45e8e602_Rx] */
  { /*   131 */      35u,             0u,           62u },  /* [Memory_1 - CanHardwareObject_CIOM_BB2_22S_FCM_Tp_oBackbone2_7818b16f_Rx] */
  { /*   132 */      36u,             0u,           63u },  /* [Memory_1 - CanHardwareObject_HMIIOM_BB2_09P_oBackbone2_08c205a9_Rx] */
  { /*   133 */      37u,             0u,           64u },  /* [Memory_1 - CanHardwareObject_HMIIOM_BB2_34S_oBackbone2_6313bb35_Rx] */
  { /*   134 */      38u,             0u,           65u },  /* [Memory_1 - CanHardwareObject_HMIIOM_BB2_35S_Tp_oBackbone2_61dbf5f7_Rx] */
  { /*   135 */      39u,             0u,           66u },  /* [Memory_1 - CanHardwareObject_HMIIOM_BB2_06S_Tp_oBackbone2_2c2d06cb_Rx] */
  { /*   136 */      40u,             0u,           67u },  /* [Memory_1 - CanHardwareObject_CIOM_BB2_30S_FCM_Tp_oBackbone2_d656145d_Rx] */
  { /*   137 */      41u,             0u,           68u },  /* [Memory_1 - CanHardwareObject_VMCU_BB2_54P_oBackbone2_c4c9ad5d_Rx] */
  { /*   138 */      42u,             0u,           69u },  /* [Memory_1 - CanHardwareObject_VMCU_BB2_53P_oBackbone2_28f22c1c_Rx] */
  { /*   139 */      43u,             0u,           70u },  /* [Memory_1 - CanHardwareObject_HMIIOM_BB2_33P_oBackbone2_59bf9fe2_Rx] */
  { /*   140 */      44u,             0u,           71u },  /* [Memory_1 - CanHardwareObject_HMIIOM_BB2_03P_oBackbone2_7d4615c2_Rx] */
  { /*   141 */      45u,             0u,           72u },  /* [Memory_1 - CanHardwareObject_VMCU_BB2_52P_oBackbone2_4f31ad1c_Rx] */
  { /*   142 */      46u,             0u,           73u },  /* [Memory_1 - CanHardwareObject_BBM_BB2_01P_oBackbone2_0057383e_Rx] */
  { /*   143 */      47u,             0u,           74u },  /* [Memory_1 - CanHardwareObject_VMCU_BB2_05P_oBackbone2_a2f2e1d8_Rx] */
  { /*   144 */      48u,             0u,           75u },  /* [Memory_1 - CanHardwareObject_VMCU_BB2_04P_oBackbone2_c53160d8_Rx] */
  { /*   145 */      49u,             0u,           76u },  /* [Memory_1 - CanHardwareObject_DACU_BB2_02P_oBackbone2_0b8371c6_Rx] */
  { /*   146 */      50u,             0u,           77u },  /* [Memory_1 - CanHardwareObject_VMCU_BB2_82P_oBackbone2_4df77c91_Rx] */
  { /*   147 */      51u,             0u,           78u },  /* [Memory_1 - CanHardwareObject_VMCU_BB2_02P_oBackbone2_4ec96099_Rx] */
  { /*   148 */      52u,             0u,           79u },  /* [Memory_1 - CanHardwareObject_004] */
  { /*   149 */      53u,             1u,           79u },  /* [Memory_1 - CanHardwareObject_004] */
    /* Index    HwHandle  MailboxElement  MailboxHandle        Comment */
  { /*   150 */      54u,             0u,           80u },  /* [Memory_1 - CN_Backbone2_7b3866ec_1_Rx] */
  { /*   151 */      55u,             1u,           80u },  /* [Memory_1 - CN_Backbone2_7b3866ec_1_Rx] */
  { /*   152 */      56u,             0u,           26u },  /* [Memory_1 - CN_Backbone2_78967e2c_Tx] */
  { /*   153 */      57u,             0u,            7u },  /* [Memory_1 - CanHardwareObject_CIOM_BB2_04P_oBackbone2_14239779_Tx] */
  { /*   154 */      58u,             0u,            8u },  /* [Memory_1 - CanHardwareObject_CIOM_BB2_02P_oBackbone2_9fdb9738_Tx] */
  { /*   155 */      59u,             0u,            9u },  /* [Memory_1 - CanHardwareObject_CIOM_BB2_03P_oBackbone2_f8181638_Tx] */
  { /*   156 */      60u,             0u,           10u },  /* [Memory_1 - CanHardwareObject_CIOM_BB2_01P_oBackbone2_379f1438_Tx] */
  { /*   157 */      61u,             0u,           11u },  /* [Memory_1 - CanHardwareObject_CIOM_BB2_29P_oBackbone2_bfae97b8_Tx] */
  { /*   158 */      62u,             0u,           12u },  /* [Memory_1 - CanHardwareObject_CIOM_BB2_05P_oBackbone2_73e01679_Tx] */
  { /*   159 */      63u,             0u,           13u },  /* [Memory_1 - CanHardwareObject_CIOM_BB2_06P_oBackbone2_dba49579_Tx] */
  { /*   160 */      64u,             0u,           14u },  /* [Memory_1 - CanHardwareObject_CIOM_BB2_10P_oBackbone2_503b56b9_Tx] */
  { /*   161 */      65u,             0u,           15u },  /* [Memory_1 - CanHardwareObject_CIOM_BB2_11P_oBackbone2_37f8d7b9_Tx] */
  { /*   162 */      66u,             0u,           16u },  /* [Memory_1 - CanHardwareObject_CIOM_BB2_07P_oBackbone2_bc671479_Tx] */
  { /*   163 */      67u,             0u,           17u },  /* [Memory_1 - CanHardwareObject_CIOM_BB2_28P_oBackbone2_d86d16b8_Tx] */
  { /*   164 */      68u,             0u,           18u },  /* [Memory_1 - CanHardwareObject_AnmMsg_CIOM_Backbone2_oBackbone2_d362d40e_Tx] */
  { /*   165 */      69u,             0u,           19u },  /* [Memory_1 - CanHardwareObject_Debug02_CIOM_BB2_oBackbone2_423c0750_Tx] */
  { /*   166 */      70u,             0u,           20u },  /* [Memory_1 - CanHardwareObject_Debug03_CIOM_BB2_oBackbone2_2d143e88_Tx] */
  { /*   167 */      71u,             0u,           21u },  /* [Memory_1 - CanHardwareObject_Debug04_CIOM_BB2_oBackbone2_fbbd95c1_Tx] */
  { /*   168 */      72u,             0u,           22u },  /* [Memory_1 - CanHardwareObject_Debug05_CIOM_BB2_oBackbone2_9495ac19_Tx] */
  { /*   169 */      73u,             0u,           23u },  /* [Memory_1 - CanHardwareObject_Debug06_CIOM_BB2_oBackbone2_25ede671_Tx] */
  { /*   170 */      74u,             0u,           24u },  /* [Memory_1 - CanHardwareObject_Debug07_CIOM_BB2_oBackbone2_4ac5dfa9_Tx] */
  { /*   171 */      75u,             0u,           25u },  /* [Memory_1 - CanHardwareObject_PhysDiagRespMsg_F2_40_BB2_Tp_oBackbone2_bd4d3e52_Tx] */
  { /*   172 */      76u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   173 */      77u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   174 */      78u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   175 */      79u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   176 */      80u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   177 */      81u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   178 */      82u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   179 */      83u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   180 */      84u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   181 */      85u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   182 */      86u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   183 */      87u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   184 */      88u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   185 */      89u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   186 */      90u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   187 */      91u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   188 */      92u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   189 */      93u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   190 */      94u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   191 */      95u,             0u,            0u },  /* [Memory_1 - Reserved] */
  { /*   192 */       0u,             0u,           85u },  /* [Memory_2 - CanHardwareObject_003] */
  { /*   193 */       1u,             1u,           85u },  /* [Memory_2 - CanHardwareObject_003] */
  { /*   194 */       2u,             0u,           86u },  /* [Memory_2 - CN_CAN6_20078678_1_Rx] */
  { /*   195 */       3u,             1u,           86u },  /* [Memory_2 - CN_CAN6_20078678_1_Rx] */
  { /*   196 */       4u,             0u,           84u },  /* [Memory_2 - CN_CAN6_b040c073_Tx] */
  { /*   197 */       5u,             0u,           81u },  /* [Memory_2 - CanHardwareObject_SCIM_LINtoCAN6_oCAN6_6b855022_Tx] */
  { /*   198 */       6u,             0u,           82u },  /* [Memory_2 - CanHardwareObject_SCIM_BB1toCAN6_oCAN6_cb3fc040_Tx] */
  { /*   199 */       7u,             0u,           83u },  /* [Memory_2 - CanHardwareObject_SCIM_BB2toCAN6_oCAN6_adf5f68f_Tx] */
    /* Index    HwHandle  MailboxElement  MailboxHandle        Comment */
  { /*   200 */       8u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   201 */       9u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   202 */      10u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   203 */      11u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   204 */      12u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   205 */      13u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   206 */      14u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   207 */      15u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   208 */      16u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   209 */      17u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   210 */      18u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   211 */      19u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   212 */      20u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   213 */      21u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   214 */      22u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   215 */      23u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   216 */      24u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   217 */      25u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   218 */      26u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   219 */      27u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   220 */      28u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   221 */      29u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   222 */      30u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   223 */      31u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   224 */      32u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   225 */      33u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   226 */      34u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   227 */      35u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   228 */      36u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   229 */      37u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   230 */      38u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   231 */      39u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   232 */      40u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   233 */      41u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   234 */      42u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   235 */      43u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   236 */      44u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   237 */      45u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   238 */      46u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   239 */      47u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   240 */      48u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   241 */      49u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   242 */      50u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   243 */      51u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   244 */      52u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   245 */      53u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   246 */      54u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   247 */      55u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   248 */      56u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   249 */      57u,             0u,            0u },  /* [Memory_2 - Reserved] */
    /* Index    HwHandle  MailboxElement  MailboxHandle        Comment */
  { /*   250 */      58u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   251 */      59u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   252 */      60u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   253 */      61u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   254 */      62u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   255 */      63u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   256 */      64u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   257 */      65u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   258 */      66u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   259 */      67u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   260 */      68u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   261 */      69u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   262 */      70u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   263 */      71u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   264 */      72u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   265 */      73u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   266 */      74u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   267 */      75u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   268 */      76u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   269 */      77u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   270 */      78u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   271 */      79u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   272 */      80u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   273 */      81u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   274 */      82u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   275 */      83u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   276 */      84u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   277 */      85u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   278 */      86u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   279 */      87u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   280 */      88u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   281 */      89u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   282 */      90u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   283 */      91u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   284 */      92u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   285 */      93u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   286 */      94u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   287 */      95u,             0u,            0u },  /* [Memory_2 - Reserved] */
  { /*   288 */       0u,             0u,          102u },  /* [Memory_3 - CanHardwareObject_DiagUUDTRespMsg1_F2_A2_Cab_oCabSubnet_faf51f79_Rx] */
  { /*   289 */       1u,             0u,          103u },  /* [Memory_3 - CanHardwareObject_DiagUUDTRespMsg1_F2_98_Cab_oCabSubnet_2175e3c9_Rx] */
  { /*   290 */       2u,             0u,          104u },  /* [Memory_3 - CanHardwareObject_DiagUUDTRespMsg1_F2_53_Cab_oCabSubnet_d5110ca2_Rx] */
  { /*   291 */       3u,             0u,          105u },  /* [Memory_3 - CanHardwareObject_DiagUUDTRespMsg1_F2_26_Cab_oCabSubnet_8c457eb7_Rx] */
  { /*   292 */       4u,             0u,          106u },  /* [Memory_3 - CanHardwareObject_DiagRespMsgIntTGW2_F4_A2_Cab_Tp_oCabSubnet_748ab027_Rx] */
  { /*   293 */       5u,             0u,          107u },  /* [Memory_3 - CanHardwareObject_DiagRespMsgIntTGW2_F4_98_Cab_Tp_oCabSubnet_d506a20a_Rx] */
  { /*   294 */       6u,             0u,          108u },  /* [Memory_3 - CanHardwareObject_DiagRespMsgIntTGW2_F4_53_Cab_Tp_oCabSubnet_781e6a27_Rx] */
  { /*   295 */       7u,             0u,          109u },  /* [Memory_3 - CanHardwareObject_DiagRespMsgIntTGW2_F4_26_Cab_Tp_oCabSubnet_0b131bbc_Rx] */
  { /*   296 */       8u,             0u,          110u },  /* [Memory_3 - CanHardwareObject_DiagRespMsgIntHMIIOM_F3_A2_Cab_Tp_oCabSubnet_15c35ee3_Rx] */
  { /*   297 */       9u,             0u,          111u },  /* [Memory_3 - CanHardwareObject_DiagRespMsgIntHMIIOM_F3_98_Cab_Tp_oCabSubnet_b05496ae_Rx] */
  { /*   298 */      10u,             0u,          112u },  /* [Memory_3 - CanHardwareObject_DiagRespMsgIntHMIIOM_F3_53_Cab_Tp_oCabSubnet_507b9b04_Rx] */
  { /*   299 */      11u,             0u,          113u },  /* [Memory_3 - CanHardwareObject_DiagRespMsgIntHMIIOM_F3_26_Cab_Tp_oCabSubnet_68d46304_Rx] */
    /* Index    HwHandle  MailboxElement  MailboxHandle        Comment */
  { /*   300 */      12u,             0u,          114u },  /* [Memory_3 - CanHardwareObject_PhysDiagRespMsg_F2_A2_Cab_Tp_oCabSubnet_89846943_Rx] */
  { /*   301 */      13u,             0u,          115u },  /* [Memory_3 - CanHardwareObject_PhysDiagRespMsg_F2_98_Cab_Tp_oCabSubnet_af4ee02a_Rx] */
  { /*   302 */      14u,             0u,          116u },  /* [Memory_3 - CanHardwareObject_PhysDiagRespMsg_F2_53_Cab_Tp_oCabSubnet_4c48e914_Rx] */
  { /*   303 */      15u,             0u,          117u },  /* [Memory_3 - CanHardwareObject_PhysDiagRespMsg_F2_26_Cab_Tp_oCabSubnet_48eedf32_Rx] */
  { /*   304 */      16u,             0u,          118u },  /* [Memory_3 - CanHardwareObject_AnmMsg_ECUspare6_CabSubnet_oCabSubnet_582cafe8_Rx] */
  { /*   305 */      17u,             0u,          119u },  /* [Memory_3 - CanHardwareObject_AnmMsg_WRCS_CabSubnet_oCabSubnet_62e3be6f_Rx] */
  { /*   306 */      18u,             0u,          120u },  /* [Memory_3 - CanHardwareObject_AnmMsg_SRS_CabSubnet_oCabSubnet_690513e9_Rx] */
  { /*   307 */      19u,             0u,          121u },  /* [Memory_3 - CanHardwareObject_AnmMsg_LECM1_CabSubnet_oCabSubnet_ba3b1140_Rx] */
  { /*   308 */      20u,             0u,          122u },  /* [Memory_3 - CanHardwareObject_AnmMsg_CCM_CabSubnet_oCabSubnet_7e592096_Rx] */
  { /*   309 */      21u,             0u,          123u },  /* [Memory_3 - CanHardwareObject_DiagFaultStat_WRCS_Cab_oCabSubnet_b86e4ab7_Rx] */
  { /*   310 */      22u,             0u,          124u },  /* [Memory_3 - CanHardwareObject_DiagFaultStat_SRS_Cab_oCabSubnet_e5f44b73_Rx] */
  { /*   311 */      23u,             0u,          125u },  /* [Memory_3 - CanHardwareObject_DiagFaultStat_LECM_Cab_oCabSubnet_6e0076c7_Rx] */
  { /*   312 */      24u,             0u,          126u },  /* [Memory_3 - CanHardwareObject_DiagFaultStat_CCM_Cab_oCabSubnet_2b754d89_Rx] */
  { /*   313 */      25u,             0u,          127u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_20S_FCM_Tp_oCabSubnet_88858df9_Rx] */
  { /*   314 */      26u,             0u,          128u },  /* [Memory_3 - CanHardwareObject_SRS_Cab_06P_oCabSubnet_bec0c2b2_Rx] */
  { /*   315 */      27u,             0u,          129u },  /* [Memory_3 - CanHardwareObject_SRS_Cab_05P_oCabSubnet_2fb89566_Rx] */
  { /*   316 */      28u,             0u,          130u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_34P_FCM_Tp_oCabSubnet_d2bbef9e_Rx] */
  { /*   317 */      29u,             0u,          131u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_30S_FCM_Tp_oCabSubnet_ae18d4ab_Rx] */
  { /*   318 */      30u,             0u,          132u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_25S_FCM_Tp_oCabSubnet_063a8d48_Rx] */
  { /*   319 */      31u,             0u,          133u },  /* [Memory_3 - CanHardwareObject_LECM1_Cab_03S_oCabSubnet_e20ca5cb_Rx] */
  { /*   320 */      32u,             0u,          134u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_13S_FCM_Tp_oCabSubnet_2f98645f_Rx] */
  { /*   321 */      33u,             0u,          135u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_11S_FCM_Tp_oCabSubnet_a74b983f_Rx] */
  { /*   322 */      34u,             0u,          136u },  /* [Memory_3 - CanHardwareObject_PhysDiagRespMsg_F1_53_Cab_Tp_oCabSubnet_1d55a011_Rx] */
  { /*   323 */      35u,             0u,          137u },  /* [Memory_3 - CanHardwareObject_SRS_Cab_04P_oCabSubnet_5f6f582a_Rx] */
  { /*   324 */      36u,             0u,          138u },  /* [Memory_3 - CanHardwareObject_SRS_Cab_03P_oCabSubnet_d6393c8f_Rx] */
  { /*   325 */      37u,             0u,          139u },  /* [Memory_3 - CanHardwareObject_LECM1_Cab_05S_oCabSubnet_b18bf7f9_Rx] */
  { /*   326 */      38u,             0u,          140u },  /* [Memory_3 - CanHardwareObject_WRCS_Cab_03P_oCabSubnet_37a4b7c4_Rx] */
  { /*   327 */      39u,             0u,          141u },  /* [Memory_3 - CanHardwareObject_LECM1_Cab_04P_oCabSubnet_b355ee77_Rx] */
  { /*   328 */      40u,             0u,          142u },  /* [Memory_3 - CanHardwareObject_LECM1_Cab_02P_oCabSubnet_e0d2bc45_Rx] */
  { /*   329 */      41u,             0u,          143u },  /* [Memory_3 - CanHardwareObject_SRS_Cab_01P_oCabSubnet_3796a617_Rx] */
  { /*   330 */      42u,             0u,          144u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_29S_FCM_Tp_oCabSubnet_8230888a_Rx] */
  { /*   331 */      43u,             0u,          145u },  /* [Memory_3 - CanHardwareObject_WRCS_Cab_02P_oCabSubnet_506736c4_Rx] */
  { /*   332 */      44u,             0u,          146u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_05P_FCM_Tp_oCabSubnet_fd75fa58_Rx] */
  { /*   333 */      45u,             0u,          147u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_04P_FCM_Tp_oCabSubnet_b91c0468_Rx] */
  { /*   334 */      46u,             0u,          148u },  /* [Memory_3 - CanHardwareObject_CCM_Cab_06P_oCabSubnet_ae969834_Rx] */
  { /*   335 */      47u,             0u,          149u },  /* [Memory_3 - CanHardwareObject_CCM_Cab_07P_oCabSubnet_de415578_Rx] */
  { /*   336 */      48u,             0u,          150u },  /* [Memory_3 - CanHardwareObject_CCM_Cab_08P_oCabSubnet_674b573f_Rx] */
  { /*   337 */      49u,             0u,          151u },  /* [Memory_3 - CanHardwareObject_CCM_Cab_04P_oCabSubnet_4f3902ac_Rx] */
  { /*   338 */      50u,             0u,          152u },  /* [Memory_3 - CanHardwareObject_CCM_Cab_01P_oCabSubnet_27c0fc91_Rx] */
  { /*   339 */      51u,             0u,          153u },  /* [Memory_3 - CanHardwareObject_CCM_Cab_03P_Tp_oCabSubnet_bfb45635_Rx] */
  { /*   340 */      52u,             0u,          154u },  /* [Memory_3 - CanHardwareObject] */
  { /*   341 */      53u,             1u,          154u },  /* [Memory_3 - CanHardwareObject] */
  { /*   342 */      54u,             0u,          155u },  /* [Memory_3 - CN_CabSubnet_eedbadff_1_Rx] */
  { /*   343 */      55u,             1u,          155u },  /* [Memory_3 - CN_CabSubnet_eedbadff_1_Rx] */
  { /*   344 */      56u,             0u,          101u },  /* [Memory_3 - CN_CabSubnet_9ea693f1_Tx] */
  { /*   345 */      57u,             0u,           87u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_02P_oCabSubnet_694518ea_Tx] */
  { /*   346 */      58u,             0u,           88u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_32P_oCabSubnet_69ed5c69_Tx] */
  { /*   347 */      59u,             0u,           89u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_01P_oCabSubnet_c1019bea_Tx] */
  { /*   348 */      60u,             0u,           90u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_03P_oCabSubnet_0e8699ea_Tx] */
  { /*   349 */      61u,             0u,           91u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_06P_oCabSubnet_2d3a1aab_Tx] */
    /* Index    HwHandle  MailboxElement  MailboxHandle        Comment */
  { /*   350 */      62u,             0u,           92u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_33P_oCabSubnet_0e2edd69_Tx] */
  { /*   351 */      63u,             0u,           93u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_09P_oCabSubnet_49ff9f68_Tx] */
  { /*   352 */      64u,             0u,           94u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_27P_oCabSubnet_4a361ca9_Tx] */
  { /*   353 */      65u,             0u,           95u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_23P_oCabSubnet_0e491ee8_Tx] */
  { /*   354 */      66u,             0u,           96u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_26P_oCabSubnet_2df59da9_Tx] */
  { /*   355 */      67u,             0u,           97u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_10P_oCabSubnet_a6a5d96b_Tx] */
  { /*   356 */      68u,             0u,           98u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_24P_oCabSubnet_e2729fa9_Tx] */
  { /*   357 */      69u,             0u,           99u },  /* [Memory_3 - CanHardwareObject_CIOM_Cab_31P_oCabSubnet_c1a9df69_Tx] */
  { /*   358 */      70u,             0u,          100u },  /* [Memory_3 - CanHardwareObject_AnmMsg_CIOM_CabSubnet_oCabSubnet_e295c8bd_Tx] */
  { /*   359 */      71u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   360 */      72u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   361 */      73u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   362 */      74u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   363 */      75u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   364 */      76u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   365 */      77u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   366 */      78u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   367 */      79u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   368 */      80u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   369 */      81u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   370 */      82u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   371 */      83u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   372 */      84u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   373 */      85u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   374 */      86u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   375 */      87u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   376 */      88u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   377 */      89u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   378 */      90u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   379 */      91u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   380 */      92u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   381 */      93u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   382 */      94u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   383 */      95u,             0u,            0u },  /* [Memory_3 - Reserved] */
  { /*   384 */       0u,             0u,          165u },  /* [Memory_4 - CanHardwareObject_002] */
  { /*   385 */       1u,             1u,          165u },  /* [Memory_4 - CanHardwareObject_002] */
  { /*   386 */       2u,             0u,          166u },  /* [Memory_4 - CN_FMSNet_41e51372_1_Rx] */
  { /*   387 */       3u,             1u,          166u },  /* [Memory_4 - CN_FMSNet_41e51372_1_Rx] */
  { /*   388 */       4u,             0u,          164u },  /* [Memory_4 - CN_FMSNet_fce1aae5_Tx] */
  { /*   389 */       5u,             0u,          156u },  /* [Memory_4 - CanHardwareObject_EEC2_X_CIOMFMS_oFMSNet_d3e60e6a_Tx] */
  { /*   390 */       6u,             0u,          157u },  /* [Memory_4 - CanHardwareObject_EEC1_X_CIOMFMS_oFMSNet_954d78de_Tx] */
  { /*   391 */       7u,             0u,          158u },  /* [Memory_4 - CanHardwareObject_TCO1_X_CIOMFMS_oFMSNet_567baf3d_Tx] */
  { /*   392 */       8u,             0u,          159u },  /* [Memory_4 - CanHardwareObject_ERC1_x_EMSRetFMS_oFMSNet_d863a207_Tx] */
  { /*   393 */       9u,             0u,          160u },  /* [Memory_4 - CanHardwareObject_ERC1_x_RECUFMS_oFMSNet_338e7918_Tx] */
  { /*   394 */      10u,             0u,          161u },  /* [Memory_4 - CanHardwareObject_PTODE_X_CIOMFMS_oFMSNet_8abc23b8_Tx] */
  { /*   395 */      11u,             0u,          162u },  /* [Memory_4 - CanHardwareObject_CCVS_X_CIOMFMS_oFMSNet_1dbcfa70_Tx] */
  { /*   396 */      12u,             0u,          163u },  /* [Memory_4 - CanHardwareObject_LFE_X_CIOMFMS_oFMSNet_adac24c8_Tx] */
  { /*   397 */      13u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   398 */      14u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   399 */      15u,             0u,            0u },  /* [Memory_4 - Reserved] */
    /* Index    HwHandle  MailboxElement  MailboxHandle        Comment */
  { /*   400 */      16u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   401 */      17u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   402 */      18u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   403 */      19u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   404 */      20u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   405 */      21u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   406 */      22u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   407 */      23u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   408 */      24u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   409 */      25u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   410 */      26u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   411 */      27u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   412 */      28u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   413 */      29u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   414 */      30u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   415 */      31u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   416 */      32u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   417 */      33u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   418 */      34u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   419 */      35u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   420 */      36u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   421 */      37u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   422 */      38u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   423 */      39u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   424 */      40u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   425 */      41u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   426 */      42u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   427 */      43u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   428 */      44u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   429 */      45u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   430 */      46u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   431 */      47u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   432 */      48u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   433 */      49u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   434 */      50u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   435 */      51u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   436 */      52u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   437 */      53u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   438 */      54u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   439 */      55u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   440 */      56u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   441 */      57u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   442 */      58u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   443 */      59u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   444 */      60u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   445 */      61u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   446 */      62u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   447 */      63u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   448 */      64u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   449 */      65u,             0u,            0u },  /* [Memory_4 - Reserved] */
    /* Index    HwHandle  MailboxElement  MailboxHandle        Comment */
  { /*   450 */      66u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   451 */      67u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   452 */      68u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   453 */      69u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   454 */      70u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   455 */      71u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   456 */      72u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   457 */      73u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   458 */      74u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   459 */      75u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   460 */      76u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   461 */      77u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   462 */      78u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   463 */      79u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   464 */      80u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   465 */      81u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   466 */      82u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   467 */      83u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   468 */      84u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   469 */      85u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   470 */      86u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   471 */      87u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   472 */      88u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   473 */      89u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   474 */      90u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   475 */      91u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   476 */      92u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   477 */      93u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   478 */      94u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   479 */      95u,             0u,            0u },  /* [Memory_4 - Reserved] */
  { /*   480 */       0u,             0u,          186u },  /* [Memory_5 - CanHardwareObject_DiagUUDTRespMsg1_F2_C0_Sec_oSecuritySubnet_d0c97a87_Rx] */
  { /*   481 */       1u,             0u,          187u },  /* [Memory_5 - CanHardwareObject_DiagUUDTRespMsg1_F2_A1_Sec_oSecuritySubnet_67c5eccf_Rx] */
  { /*   482 */       2u,             0u,          188u },  /* [Memory_5 - CanHardwareObject_DiagUUDTRespMsg1_F2_A0_Sec_oSecuritySubnet_46825d6a_Rx] */
  { /*   483 */       3u,             0u,          189u },  /* [Memory_5 - CanHardwareObject_DiagRespMsgIntTGW2_F4_C0_Sec_Tp_oSecuritySubnet_7a975d32_Rx] */
  { /*   484 */       4u,             0u,          190u },  /* [Memory_5 - CanHardwareObject_DiagRespMsgIntTGW2_F4_A1_Sec_Tp_oSecuritySubnet_efa76b9d_Rx] */
  { /*   485 */       5u,             0u,          191u },  /* [Memory_5 - CanHardwareObject_DiagRespMsgIntTGW2_F4_A0_Sec_Tp_oSecuritySubnet_d9469eac_Rx] */
  { /*   486 */       6u,             0u,          192u },  /* [Memory_5 - CanHardwareObject_DiagRespMsgIntHMIIOM_F3_C0_Sec_Tp_oSecuritySubnet_77f0a36b_Rx] */
  { /*   487 */       7u,             0u,          193u },  /* [Memory_5 - CanHardwareObject_DiagRespMsgIntHMIIOM_F3_A1_Sec_Tp_oSecuritySubnet_052f8796_Rx] */
  { /*   488 */       8u,             0u,          194u },  /* [Memory_5 - CanHardwareObject_DiagRespMsgIntHMIIOM_F3_A0_Sec_Tp_oSecuritySubnet_95b29252_Rx] */
  { /*   489 */       9u,             0u,          195u },  /* [Memory_5 - CanHardwareObject_PhysDiagRespMsg_F2_C0_Sec_Tp_oSecuritySubnet_614a0693_Rx] */
  { /*   490 */      10u,             0u,          196u },  /* [Memory_5 - CanHardwareObject_PhysDiagRespMsg_F2_A1_Sec_Tp_oSecuritySubnet_d47cc691_Rx] */
  { /*   491 */      11u,             0u,          197u },  /* [Memory_5 - CanHardwareObject_PhysDiagRespMsg_F2_A0_Sec_Tp_oSecuritySubnet_207d0e64_Rx] */
  { /*   492 */      12u,             0u,          198u },  /* [Memory_5 - CanHardwareObject_AnmMsg_ECUspare5_SecuritySubnet_oSecuritySubnet_25af973c_Rx] */
  { /*   493 */      13u,             0u,          199u },  /* [Memory_5 - CanHardwareObject_AnmMsg_PDM_SecuritySubnet_oSecuritySubnet_d922132b_Rx] */
  { /*   494 */      14u,             0u,          200u },  /* [Memory_5 - CanHardwareObject_AnmMsg_DDM_SecuritySubnet_oSecuritySubnet_669221a0_Rx] */
  { /*   495 */      15u,             0u,          201u },  /* [Memory_5 - CanHardwareObject_AnmMsg_Alarm_SecuritySubnet_oSecuritySubnet_38c19358_Rx] */
  { /*   496 */      16u,             0u,          202u },  /* [Memory_5 - CanHardwareObject_DiagFaultStat_PDM_Sec_oSecuritySubnet_b17b40b8_Rx] */
  { /*   497 */      17u,             0u,          203u },  /* [Memory_5 - CanHardwareObject_DiagFaultStat_DDM_Sec_oSecuritySubnet_3b5b41e4_Rx] */
  { /*   498 */      18u,             0u,          204u },  /* [Memory_5 - CanHardwareObject_DiagFaultStat_Alarm_Sec_oSecuritySubnet_6cedaf7c_Rx] */
  { /*   499 */      19u,             0u,          205u },  /* [Memory_5 - CanHardwareObject_PDM_Sec_04S_Tp_oSecuritySubnet_596ba3c3_Rx] */
    /* Index    HwHandle  MailboxElement  MailboxHandle        Comment */
  { /*   500 */      20u,             0u,          206u },  /* [Memory_5 - CanHardwareObject_CIOM_Sec_05S_FCM_Tp_oSecuritySubnet_02ea2344_Rx] */
  { /*   501 */      21u,             0u,          207u },  /* [Memory_5 - CanHardwareObject_PDM_Sec_03S_Tp_oSecuritySubnet_225f8534_Rx] */
  { /*   502 */      22u,             0u,          208u },  /* [Memory_5 - CanHardwareObject_DDM_Sec_05S_Tp_oSecuritySubnet_5a1ed97b_Rx] */
  { /*   503 */      23u,             0u,          209u },  /* [Memory_5 - CanHardwareObject_Alarm_Sec_07S_Tp_oSecuritySubnet_bf7e0710_Rx] */
  { /*   504 */      24u,             0u,          210u },  /* [Memory_5 - CanHardwareObject_DDM_Sec_04S_Tp_oSecuritySubnet_c3efbadc_Rx] */
  { /*   505 */      25u,             0u,          211u },  /* [Memory_5 - CanHardwareObject_Alarm_Sec_06S_Tp_oSecuritySubnet_2b24d209_Rx] */
  { /*   506 */      26u,             0u,          212u },  /* [Memory_5 - CanHardwareObject_DDM_Sec_03S_Tp_oSecuritySubnet_b8db9c2b_Rx] */
  { /*   507 */      27u,             0u,          213u },  /* [Memory_5 - CanHardwareObject_CIOM_Sec_12S_FCM_Tp_oSecuritySubnet_9cba7397_Rx] */
  { /*   508 */      28u,             0u,          214u },  /* [Memory_5 - CanHardwareObject_CIOM_Sec_07S_FCM_Tp_oSecuritySubnet_5acc46b8_Rx] */
  { /*   509 */      29u,             0u,          215u },  /* [Memory_5 - CanHardwareObject_CIOM_Sec_11S_FCM_Tp_oSecuritySubnet_e88f2495_Rx] */
  { /*   510 */      30u,             0u,          216u },  /* [Memory_5 - CanHardwareObject_CIOM_Sec_10S_FCM_Tp_oSecuritySubnet_c49c166b_Rx] */
  { /*   511 */      31u,             0u,          217u },  /* [Memory_5 - CanHardwareObject_CIOM_Sec_09S_FCM_Tp_oSecuritySubnet_094e790d_Rx] */
  { /*   512 */      32u,             0u,          218u },  /* [Memory_5 - CanHardwareObject_CIOM_Sec_06S_FCM_Tp_oSecuritySubnet_76df7446_Rx] */
  { /*   513 */      33u,             0u,          219u },  /* [Memory_5 - CanHardwareObject_Alarm_Sec_03S_Tp_oSecuritySubnet_838659b7_Rx] */
  { /*   514 */      34u,             0u,          220u },  /* [Memory_5 - CanHardwareObject_CIOM_Sec_08S_FCM_Tp_oSecuritySubnet_255d4bf3_Rx] */
  { /*   515 */      35u,             0u,          221u },  /* [Memory_5 - CanHardwareObject_PDM_Sec_01P_oSecuritySubnet_073fa36a_Rx] */
  { /*   516 */      36u,             0u,          222u },  /* [Memory_5 - CanHardwareObject_DDM_Sec_01P_oSecuritySubnet_09e2b30b_Rx] */
  { /*   517 */      37u,             0u,          223u },  /* [Memory_5 - CanHardwareObject_Alarm_Sec_02P_oSecuritySubnet_a902993f_Rx] */
  { /*   518 */      38u,             0u,          224u },  /* [Memory_5 - CanHardwareObject_001] */
  { /*   519 */      39u,             1u,          224u },  /* [Memory_5 - CanHardwareObject_001] */
  { /*   520 */      40u,             0u,          225u },  /* [Memory_5 - CN_SecuritySubnet_3840872e_1_Rx] */
  { /*   521 */      41u,             1u,          225u },  /* [Memory_5 - CN_SecuritySubnet_3840872e_1_Rx] */
  { /*   522 */      42u,             0u,          185u },  /* [Memory_5 - CN_SecuritySubnet_e7a0ee54_Tx] */
  { /*   523 */      43u,             0u,          167u },  /* [Memory_5 - CanHardwareObject_CIOM_Sec_01P_oSecuritySubnet_de08418d_Tx] */
  { /*   524 */      44u,             0u,          168u },  /* [Memory_5 - CanHardwareObject_CIOM_Sec_04P_oSecuritySubnet_6a707225_Tx] */
  { /*   525 */      45u,             0u,          169u },  /* [Memory_5 - CanHardwareObject_CIOM_Sec_10S_Tp_oSecuritySubnet_8c7f36ae_Tx] */
  { /*   526 */      46u,             0u,          170u },  /* [Memory_5 - CanHardwareObject_CIOM_Sec_12S_Tp_oSecuritySubnet_2d9a0e12_Tx] */
  { /*   527 */      47u,             0u,          171u },  /* [Memory_5 - CanHardwareObject_CIOM_Sec_03P_oSecuritySubnet_4d58f6b7_Tx] */
  { /*   528 */      48u,             0u,          172u },  /* [Memory_5 - CanHardwareObject_CIOM_Sec_02P_oSecuritySubnet_04f0ad2a_Tx] */
  { /*   529 */      49u,             0u,          173u },  /* [Memory_5 - CanHardwareObject_AnmMsg_CIOM_SecuritySubnet_oSecuritySubnet_d0267e59_Tx] */
  { /*   530 */      50u,             0u,          174u },  /* [Memory_5 - CanHardwareObject_PhysDiagReqMsg_A0_F2_Sec_Tp_oSecuritySubnet_cb87d97b_Tx] */
  { /*   531 */      51u,             0u,          175u },  /* [Memory_5 - CanHardwareObject_DiagReqMsgIntHMIIOM_A0_F3_Sec_Tp_oSecuritySubnet_09265670_Tx] */
  { /*   532 */      52u,             0u,          176u },  /* [Memory_5 - CanHardwareObject_DiagReqMsgIntTGW2_A0_F4_Sec_Tp_oSecuritySubnet_8adef0bc_Tx] */
  { /*   533 */      53u,             0u,          177u },  /* [Memory_5 - CanHardwareObject_PhysDiagReqMsg_A1_F2_Sec_Tp_oSecuritySubnet_5d2e792a_Tx] */
  { /*   534 */      54u,             0u,          178u },  /* [Memory_5 - CanHardwareObject_DiagReqMsgIntHMIIOM_A1_F3_Sec_Tp_oSecuritySubnet_a1b03b66_Tx] */
  { /*   535 */      55u,             0u,          179u },  /* [Memory_5 - CanHardwareObject_DiagReqMsgIntTGW2_A1_F4_Sec_Tp_oSecuritySubnet_eb89cc56_Tx] */
  { /*   536 */      56u,             0u,          180u },  /* [Memory_5 - CanHardwareObject_PhysDiagReqMsg_C0_F2_Sec_Tp_oSecuritySubnet_f27248ff_Tx] */
  { /*   537 */      57u,             0u,          181u },  /* [Memory_5 - CanHardwareObject_DiagReqMsgIntHMIIOM_C0_F3_Sec_Tp_oSecuritySubnet_3aaf1649_Tx] */
  { /*   538 */      58u,             0u,          182u },  /* [Memory_5 - CanHardwareObject_DiagReqMsgIntTGW2_C0_F4_Sec_Tp_oSecuritySubnet_0ba24808_Tx] */
  { /*   539 */      59u,             0u,          183u },  /* [Memory_5 - CanHardwareObject_TesterFuncDiagMsg_Sec_Tp_oSecuritySubnet_7f5a7347_Tx] */
  { /*   540 */      60u,             0u,          184u },  /* [Memory_5 - CanHardwareObject_IntTesterTGW2FuncDiagMsg_Sec_Tp_oSecuritySubnet_57d96404_Tx] */
  { /*   541 */      61u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   542 */      62u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   543 */      63u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   544 */      64u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   545 */      65u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   546 */      66u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   547 */      67u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   548 */      68u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   549 */      69u,             0u,            0u },  /* [Memory_5 - Reserved] */
    /* Index    HwHandle  MailboxElement  MailboxHandle        Comment */
  { /*   550 */      70u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   551 */      71u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   552 */      72u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   553 */      73u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   554 */      74u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   555 */      75u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   556 */      76u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   557 */      77u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   558 */      78u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   559 */      79u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   560 */      80u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   561 */      81u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   562 */      82u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   563 */      83u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   564 */      84u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   565 */      85u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   566 */      86u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   567 */      87u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   568 */      88u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   569 */      89u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   570 */      90u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   571 */      91u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   572 */      92u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   573 */      93u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   574 */      94u,             0u,            0u },  /* [Memory_5 - Reserved] */
  { /*   575 */      95u,             0u,            0u }   /* [Memory_5 - Reserved] */
};
#define CAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_ActiveSendObject
**********************************************************************************************************************/
/** 
  \var    Can_ActiveSendObject
  \brief  temporary data for TX object
  \details
  Element    Description
  Pdu        buffered PduId for confirmation or cancellation
  State      send state like cancelled or send activ
*/ 
#define CAN_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Can_ActiveSendObjectType, CAN_VAR_NOINIT) Can_ActiveSendObject[72];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define CAN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Can_ControllerData
**********************************************************************************************************************/
/** 
  \var    Can_ControllerData
  \brief  struct for all controller related variable data
  \details
  Element                      Description
  BusOffCounter                This variable stores the busoff recovery timeout counter.
  BusOffTransitionRequest      CAN state request for each controller: ContinueBusOffRecovery=0x00, FinishBusOffRecovery=0x01
  CanInterruptCounter          CAN interrupt disable counter for each controller
  IsBusOff                     CAN state for each controller: busoff occur
  LastInitObject               last set baudrate for reinit
  LogStatus                    CAN state for each controller: UNINIT=0x00, START=0x01, STOP=0x02, INIT=0x04, INCONSISTENT=0x08, WARNING =0x10, PASSIVE=0x20, BUSOFF=0x40, SLEEP=0x80
  ModeTransitionRequest        CAN state request for each controller: INIT=0x00, SLEEP=0x01, WAKEUP=0x02, STOP+INIT=0x03, START=0x04, START+INIT=0x05, NONE=0x06
  RamCheckTransitionRequest    CAN state request for each controller: kCanSuppressRamCheck=0x01, kCanExecuteRamCheck=0x00
  StartModeRequested           This variable stores if the start mode is of a special CAN Controller is already requested in the busoff recovery process.
  CanInterruptOldStatus        last CAN interrupt status for restore interrupt for each controller
  LoopTimeout                  hw loop timeout for each controller
  RxMsgBuffer                  This variable stores received values (ID, DLC, DATA) in the reception process.
*/ 
#define CAN_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Can_ControllerDataType, CAN_VAR_NOINIT) Can_ControllerData[6];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define CAN_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA
**********************************************************************************************************************/




