/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Main.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Lifecycle Header File
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_MAIN_H
# define _RTE_MAIN_H

# include "Rte.h"

# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* BSW Scheduler Life-Cycle API */
FUNC(void, RTE_CODE) SchM_Init(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, RTE_CODE) SchM_Deinit(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

/* RTE Life-Cycle API */
FUNC(Std_ReturnType, RTE_CODE) Rte_Start(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Stop(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

/* Extended Life-Cycle API */
FUNC(void, RTE_CODE) Rte_InitMemory(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */


# ifdef RTE_CORE
/* RTE internal IOC replacements */
FUNC(Std_ReturnType, RTE_CODE) Rte_IocSend_Rte_M_BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(CONST(uint8, RTE_CONST) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocReceive_Rte_M_BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(P2VAR(uint8, AUTOMATIC, RTE_APPL_DATA) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocSend_Rte_M_BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(CONST(uint8, RTE_CONST) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocReceive_Rte_M_BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(P2VAR(uint8, AUTOMATIC, RTE_APPL_DATA) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocSend_Rte_M_BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(CONST(uint8, RTE_CONST) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocReceive_Rte_M_BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(P2VAR(uint8, AUTOMATIC, RTE_APPL_DATA) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocSend_Rte_M_BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(CONST(uint8, RTE_CONST) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocReceive_Rte_M_BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(P2VAR(uint8, AUTOMATIC, RTE_APPL_DATA) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocSend_Rte_M_BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(CONST(uint8, RTE_CONST) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocReceive_Rte_M_BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(P2VAR(uint8, AUTOMATIC, RTE_APPL_DATA) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocSend_Rte_M_BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(CONST(uint8, RTE_CONST) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocReceive_Rte_M_BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(P2VAR(uint8, AUTOMATIC, RTE_APPL_DATA) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocSend_Rte_M_BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(CONST(uint8, RTE_CONST) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocReceive_Rte_M_BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(P2VAR(uint8, AUTOMATIC, RTE_APPL_DATA) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocSend_Rte_M_BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(CONST(uint8, RTE_CONST) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocReceive_Rte_M_BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(P2VAR(uint8, AUTOMATIC, RTE_APPL_DATA) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocSend_Rte_M_BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode(CONST(uint8, RTE_CONST) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocReceive_Rte_M_BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode(P2VAR(uint8, AUTOMATIC, RTE_APPL_DATA) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocSend_Rte_M_Dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl(CONST(uint8, RTE_CONST) data0);
FUNC(Std_ReturnType, RTE_CODE) Rte_IocReceive_Rte_M_Dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl(P2VAR(uint8, AUTOMATIC, RTE_APPL_DATA) data0);
# endif

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#endif /* _RTE_MAIN_H */
