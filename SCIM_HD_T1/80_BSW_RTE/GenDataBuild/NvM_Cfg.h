/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: NvM
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: NvM_Cfg.h
 *   Generation Time: 2020-08-20 13:43:06
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * PROTECTION AGAINST MULTIPLE INCLUSION
 *********************************************************************************************************************/
/* public section - to be used by NvM itself and its users */
#if (!defined NVM_CFG_H_PUBLIC)
#define NVM_CFG_H_PUBLIC

/**********************************************************************************************************************
 * VERSION IDENTIFICATION
 *********************************************************************************************************************/
#define NVM_CFG_MAJOR_VERSION    (5u)
#define NVM_CFG_MINOR_VERSION    (9u)
#define NVM_CFG_PATCH_VERSION    (0u)

/**********************************************************************************************************************
 * NVM API TYPE INCLUDES
 *********************************************************************************************************************/
#include "Rte_NvM_Type.h"

#include "NvM_Types.h"

/**********************************************************************************************************************
 * API CFG TYPE DEFINITIONS
 *********************************************************************************************************************/
/* Type for an the additional published parameter Compiled Configuration ID
 * (see CompiledConfigurationId in NvM.h)
 */
/* Compiled Config Id Type */
/* PRQA S 0750 2 */ /* MD_MSR_18.4 */
typedef union
{
    uint16 Word_u16;
    uint8  Bytes_au8[2u];
} NvM_CompiledConfigIdType;

/**********************************************************************************************************************
 * CFG COMMON PARAMETER
 *********************************************************************************************************************/
/* --------------------  DEVELOPMENT / PRODUCTION MODE -------------------- */
/* switch between Debug- or Production-Mode */
#define NVM_DEV_ERROR_DETECT                  (STD_OFF)

/* Preprocessor switch that is used in NvM_ReadAll() */
#define NVM_DYNAMIC_CONFIGURATION             (STD_OFF)

#define NVM_API_CONFIG_CLASS_1                (1u)
#define NVM_API_CONFIG_CLASS_2                (3u)
#define NVM_API_CONFIG_CLASS_3                (7u)

#define NVM_API_CONFIG_CLASS                  (NVM_API_CONFIG_CLASS_3)

#define NVM_JOB_PRIORISATION                  STD_OFF

/* define compiled Block ID */
#define NVM_COMPILED_CONFIG_ID                (3u)

/* switch for enablinig fast mode during multi block requests */
#define NVM_DRV_MODE_SWITCH                   (STD_ON)

/* switch for enablinig polling mode and disabling notifications */
#define NVM_POLLING_MODE                      (STD_ON)

/* switch for enabling the internal buffer for Crc handling */
#define NVM_CRC_INT_BUFFER                    (STD_ON)

/* number of defined NV blocks */
#define NVM_TOTAL_NUM_OF_NVRAM_BLOCKS         (38uL)

/* internal buffer size */
#define NVM_INTERNAL_BUFFER_LENGTH            876uL

/* version info api switch */
#define NVM_VERSION_INFO_API                  (STD_OFF)

/* switch to enable the ram block status api */
#define NVM_SET_RAM_BLOCK_STATUS_API          (STD_ON)

/* switch that gives the user (EcuM) the possibility to time-out WriteAll cancellation */
#define NVM_KILL_WRITEALL_API                 (STD_ON)

/* enabled or disable the whole repair redundant blocks feature */
#define NVM_REPAIR_REDUNDANT_BLOCKS_API       (STD_OFF)

/* NVM does not need this macro. It is intended for underlying modules,
 * relying on its existence
 */
#define NVM_DATASET_SELECTION_BITS            (6u)

/* block offset for DCM blocks */
#define NVM_DCM_BLOCK_OFFSET                  0x8000u

/* returns corresponding DCM BlockId of an original NVRAM Block */
/* PRQA S 3453 1 */ /* MD_MSR_19.7 */
#define NvM_GetDcmBlockId(MyApplBlockId)      ((MyApplBlockId) | NVM_DCM_BLOCK_OFFSET)

/* BlockId's:
 * Note: The numbers of the following list must meet the configured blocks in the NvM_BlockDescriptorTable_at
 *
 * Alignment of the handles of all blocks
 * Id 0 is reserved for multiblock calls
 * Id 1 is reserved for config ID
 */
#define NvMConf___MultiBlockRequest (0u) 
#define NvMConf_NvMBlockDescriptor_NvMConfigBlock (1uL) 
#define NvMConf_NvMBlockDescriptor_FblVpmValidityFlagsCopy (2uL) 
#define NvMConf_NvMBlockDescriptor_Application_Data_NVM_EngTraceHW_NvM (3uL) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock1 (4uL) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock2 (5uL) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock3 (6uL) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock4 (7uL) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock5 (8uL) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock6 (9uL) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock7 (10uL) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock8 (11uL) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock9 (12uL) 
#define NvMConf_NvMBlockDescriptor_Calibration_Data_NVM_FSP_NVM (13uL) 
#define NvMConf_NvMBlockDescriptor_Application_Data_NVM_DriverAuth2_Ctrl_NVM (14uL) 
#define NvMConf_NvMBlockDescriptor_Application_Data_NVM_VehicleAccess_Ctrl_NVM (15uL) 
#define NvMConf_NvMBlockDescriptor_Application_Data_NVM_InteriorLights_HMICtrl_NVM (16uL) 
#define NvMConf_NvMBlockDescriptor_Application_Data_NVM_MUT_UICtrl_Traction_NVM (17uL) 
#define NvMConf_NvMBlockDescriptor_Application_Data_NVM_RGW_HMICtrl_NVM (18uL) 
#define NvMConf_NvMBlockDescriptor_Application_Data_NVM_SCM_HMICtrl_NVM (19uL) 
#define NvMConf_NvMBlockDescriptor_Application_Data_NVM_VehicleModeDistribution_NVM (20uL) 
#define NvMConf_NvMBlockDescriptor_Application_Data_NVM_PinCode_ctrl_NVM (21uL) 
#define NvMConf_NvMBlockDescriptor_Application_Data_NVM_MUT_UICtrl_Difflock_NVM (22uL) 
#define NvMConf_NvMBlockDescriptor_Keyfob_Radio_Com_NVM_KeyFobNVBlock (23uL) 
#define NvMConf_NvMBlockDescriptor_DemAdminDataBlock (24uL) 
#define NvMConf_NvMBlockDescriptor_DemStatusDataBlock (25uL) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock0 (26uL) 
#define NvMConf_NvMBlockDescriptor_FblVpmState (27uL) 
#define NvMConf_NvMBlockDescriptor_FblVpmPatch (28uL) 
#define NvMConf_NvMBlockDescriptor_FblValidityFlags (29uL) 
#define NvMConf_NvMBlockDescriptor_FblSwFingerprint (30uL) 
#define NvMConf_NvMBlockDescriptor_FblSigValue (31uL) 
#define NvMConf_NvMBlockDescriptor_FblSigSegments (32uL) 
#define NvMConf_NvMBlockDescriptor_FblSigInfo (33uL) 
#define NvMConf_NvMBlockDescriptor_FblResetFlags (34uL) 
#define NvMConf_NvMBlockDescriptor_FblFingerprintNumber (35uL) 
#define NvMConf_NvMBlockDescriptor_FblDataFingerprint (36uL) 
#define NvMConf_NvMBlockDescriptor_FblBootFingerprint (37uL) 


/* CONST_DESCRIPTOR_TABLE contains all block relevant data, including the compiled config ID
 */
#define NVM_START_SEC_CONST_DESCRIPTOR_TABLE
#include "MemMap.h"

/* Additional published parameter because e.g. in case of validate all RAM
 * Blocks it is nice to know the number of Blocks. But take care: this number
 * of Blocks includes Block 0 and Block 1, which are the MultiBlock and the
 * Configuration Block - user Blocks start wiht index 2!
 */
extern CONST(uint16, NVM_PUBLIC_CONST) NvM_NoOfBlockIds_t;

/* Additional published parameter because in case of a clear EEPROM, it is
 * necessary, to write the Configuration Block containing this Compiled
 * Configuration ID to EEPROM
 */
/* Compiled Configuration ID as defined in NvM_Cfg.c */
/* PRQA S 0759 1 */ /* MD_MSR_18.4 */
extern CONST(NvM_CompiledConfigIdType, NVM_PUBLIC_CONST) NvM_CompiledConfigId_t;

#define NVM_STOP_SEC_CONST_DESCRIPTOR_TABLE
#include "MemMap.h"

/* Component define block (available, if EcuC module is active, otherwise only NVM_DUMMY_STATEMENTs are defined*/
#ifndef NVM_USE_DUMMY_STATEMENT
#define NVM_USE_DUMMY_STATEMENT STD_OFF /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef NVM_DUMMY_STATEMENT
#define NVM_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef NVM_DUMMY_STATEMENT_CONST
#define NVM_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef NVM_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define NVM_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef NVM_ATOMIC_VARIABLE_ACCESS
#define NVM_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef NVM_PROCESSOR_MPC5746C
#define NVM_PROCESSOR_MPC5746C
#endif
#ifndef NVM_COMP_DIAB
#define NVM_COMP_DIAB
#endif
#ifndef NVM_GEN_GENERATOR_MSR
#define NVM_GEN_GENERATOR_MSR
#endif
#ifndef NVM_CPUTYPE_BITORDER_MSB2LSB
#define NVM_CPUTYPE_BITORDER_MSB2LSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef NVM_CONFIGURATION_VARIANT_PRECOMPILE
#define NVM_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef NVM_CONFIGURATION_VARIANT_LINKTIME
#define NVM_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef NVM_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define NVM_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef NVM_CONFIGURATION_VARIANT
#define NVM_CONFIGURATION_VARIANT NVM_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef NVM_POSTBUILD_VARIANT_SUPPORT
#define NVM_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif


/* ---- end public configuration section ---------------------------------- */
#endif /* NVM_CFG_H_PUBLIC */

/*---- End of File ---------------------------------------------------------*/

