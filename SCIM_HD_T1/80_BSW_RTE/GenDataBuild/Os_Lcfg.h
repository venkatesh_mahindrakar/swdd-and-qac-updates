/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Lcfg.h
 *   Generation Time: 2020-10-30 14:38:54
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

#ifndef OS_LCFG_H
# define OS_LCFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
# include "Std_Types.h"

/* Os module declarations */
# include "Os_Cfg.h"
# include "Os_Types.h"
# include "Os_Types_Lcfg.h"

/* Os kernel module dependencies */

/* Os hal dependencies */
# include "Os_Hal_Lcfg.h"

/* User file includes */


/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

# define OS_START_SEC_ASW_10ms_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  ASW_10ms_Task()
 *********************************************************************************************************************/
extern FUNC(void, OS_ASW_10MS_TASK_CODE) Os_Task_ASW_10ms_Task(void);

# define OS_STOP_SEC_ASW_10ms_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_ASW_20ms_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  ASW_20ms_Task()
 *********************************************************************************************************************/
extern FUNC(void, OS_ASW_20MS_TASK_CODE) Os_Task_ASW_20ms_Task(void);

# define OS_STOP_SEC_ASW_20ms_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_ASW_Async_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  ASW_Async_Task()
 *********************************************************************************************************************/
extern FUNC(void, OS_ASW_ASYNC_TASK_CODE) Os_Task_ASW_Async_Task(void);

# define OS_STOP_SEC_ASW_Async_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_ASW_Init_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  ASW_Init_Task()
 *********************************************************************************************************************/
extern FUNC(void, OS_ASW_INIT_TASK_CODE) Os_Task_ASW_Init_Task(void);

# define OS_STOP_SEC_ASW_Init_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_BSW_10ms_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  BSW_10ms_Task()
 *********************************************************************************************************************/
extern FUNC(void, OS_BSW_10MS_TASK_CODE) Os_Task_BSW_10ms_Task(void);

# define OS_STOP_SEC_BSW_10ms_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_BSW_5ms_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  BSW_5ms_Task()
 *********************************************************************************************************************/
extern FUNC(void, OS_BSW_5MS_TASK_CODE) Os_Task_BSW_5ms_Task(void);

# define OS_STOP_SEC_BSW_5ms_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_BSW_Async_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  BSW_Async_Task()
 *********************************************************************************************************************/
extern FUNC(void, OS_BSW_ASYNC_TASK_CODE) Os_Task_BSW_Async_Task(void);

# define OS_STOP_SEC_BSW_Async_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_BSW_Diag_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  BSW_Diag_Task()
 *********************************************************************************************************************/
extern FUNC(void, OS_BSW_DIAG_TASK_CODE) Os_Task_BSW_Diag_Task(void);

# define OS_STOP_SEC_BSW_Diag_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_BSW_Lin_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  BSW_Lin_Task()
 *********************************************************************************************************************/
extern FUNC(void, OS_BSW_LIN_TASK_CODE) Os_Task_BSW_Lin_Task(void);

# define OS_STOP_SEC_BSW_Lin_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_CpuLoadIdleTask_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  CpuLoadIdleTask()
 *********************************************************************************************************************/
extern FUNC(void, OS_CPULOADIDLETASK_CODE) Os_Task_CpuLoadIdleTask(void);

# define OS_STOP_SEC_CpuLoadIdleTask_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_Init_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  Init_Task()
 *********************************************************************************************************************/
extern FUNC(void, OS_INIT_TASK_CODE) Os_Task_Init_Task(void);

# define OS_STOP_SEC_Init_Task_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_CanMailboxIsr_0_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  CanMailboxIsr_0()
 *********************************************************************************************************************/
extern FUNC(void, OS_CANMAILBOXISR_0_CODE) Os_Isr_CanMailboxIsr_0(void);

# define OS_STOP_SEC_CanMailboxIsr_0_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_CanMailboxIsr_1_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  CanMailboxIsr_1()
 *********************************************************************************************************************/
extern FUNC(void, OS_CANMAILBOXISR_1_CODE) Os_Isr_CanMailboxIsr_1(void);

# define OS_STOP_SEC_CanMailboxIsr_1_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_CanMailboxIsr_2_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  CanMailboxIsr_2()
 *********************************************************************************************************************/
extern FUNC(void, OS_CANMAILBOXISR_2_CODE) Os_Isr_CanMailboxIsr_2(void);

# define OS_STOP_SEC_CanMailboxIsr_2_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_CanMailboxIsr_4_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  CanMailboxIsr_4()
 *********************************************************************************************************************/
extern FUNC(void, OS_CANMAILBOXISR_4_CODE) Os_Isr_CanMailboxIsr_4(void);

# define OS_STOP_SEC_CanMailboxIsr_4_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_CanMailboxIsr_6_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  CanMailboxIsr_6()
 *********************************************************************************************************************/
extern FUNC(void, OS_CANMAILBOXISR_6_CODE) Os_Isr_CanMailboxIsr_6(void);

# define OS_STOP_SEC_CanMailboxIsr_6_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_CanMailboxIsr_7_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  CanMailboxIsr_7()
 *********************************************************************************************************************/
extern FUNC(void, OS_CANMAILBOXISR_7_CODE) Os_Isr_CanMailboxIsr_7(void);

# define OS_STOP_SEC_CanMailboxIsr_7_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_EMIOS_0_CH_12_CH_13_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  EMIOS_0_CH_12_CH_13_ISR()
 *********************************************************************************************************************/
extern FUNC(void, OS_EMIOS_0_CH_12_CH_13_ISR_CODE) Os_Isr_EMIOS_0_CH_12_CH_13_ISR(void);

# define OS_STOP_SEC_EMIOS_0_CH_12_CH_13_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_EMIOS_0_CH_14_CH_15_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  EMIOS_0_CH_14_CH_15_ISR()
 *********************************************************************************************************************/
extern FUNC(void, OS_EMIOS_0_CH_14_CH_15_ISR_CODE) Os_Isr_EMIOS_0_CH_14_CH_15_ISR(void);

# define OS_STOP_SEC_EMIOS_0_CH_14_CH_15_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_EMIOS_0_CH_2_CH_3_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  EMIOS_0_CH_2_CH_3_ISR()
 *********************************************************************************************************************/
extern FUNC(void, OS_EMIOS_0_CH_2_CH_3_ISR_CODE) Os_Isr_EMIOS_0_CH_2_CH_3_ISR(void);

# define OS_STOP_SEC_EMIOS_0_CH_2_CH_3_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_EMIOS_0_CH_4_CH_5_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  EMIOS_0_CH_4_CH_5_ISR()
 *********************************************************************************************************************/
extern FUNC(void, OS_EMIOS_0_CH_4_CH_5_ISR_CODE) Os_Isr_EMIOS_0_CH_4_CH_5_ISR(void);

# define OS_STOP_SEC_EMIOS_0_CH_4_CH_5_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_EMIOS_0_CH_8_CH_9_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  EMIOS_0_CH_8_CH_9_ISR()
 *********************************************************************************************************************/
extern FUNC(void, OS_EMIOS_0_CH_8_CH_9_ISR_CODE) Os_Isr_EMIOS_0_CH_8_CH_9_ISR(void);

# define OS_STOP_SEC_EMIOS_0_CH_8_CH_9_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_Gpt_PIT_0_TIMER_0_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  Gpt_PIT_0_TIMER_0_ISR()
 *********************************************************************************************************************/
extern FUNC(void, OS_GPT_PIT_0_TIMER_0_ISR_CODE) Os_Isr_Gpt_PIT_0_TIMER_0_ISR(void);

# define OS_STOP_SEC_Gpt_PIT_0_TIMER_0_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_Gpt_PIT_0_TIMER_1_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  Gpt_PIT_0_TIMER_1_ISR()
 *********************************************************************************************************************/
extern FUNC(void, OS_GPT_PIT_0_TIMER_1_ISR_CODE) Os_Isr_Gpt_PIT_0_TIMER_1_ISR(void);

# define OS_STOP_SEC_Gpt_PIT_0_TIMER_1_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_Gpt_PIT_0_TIMER_2_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  Gpt_PIT_0_TIMER_2_ISR()
 *********************************************************************************************************************/
extern FUNC(void, OS_GPT_PIT_0_TIMER_2_ISR_CODE) Os_Isr_Gpt_PIT_0_TIMER_2_ISR(void);

# define OS_STOP_SEC_Gpt_PIT_0_TIMER_2_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_LinIsr_0_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LinIsr_0()
 *********************************************************************************************************************/
extern FUNC(void, OS_LINISR_0_CODE) Os_Isr_LinIsr_0(void);

# define OS_STOP_SEC_LinIsr_0_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_LinIsr_1_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LinIsr_1()
 *********************************************************************************************************************/
extern FUNC(void, OS_LINISR_1_CODE) Os_Isr_LinIsr_1(void);

# define OS_STOP_SEC_LinIsr_1_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_LinIsr_10_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LinIsr_10()
 *********************************************************************************************************************/
extern FUNC(void, OS_LINISR_10_CODE) Os_Isr_LinIsr_10(void);

# define OS_STOP_SEC_LinIsr_10_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_LinIsr_4_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LinIsr_4()
 *********************************************************************************************************************/
extern FUNC(void, OS_LINISR_4_CODE) Os_Isr_LinIsr_4(void);

# define OS_STOP_SEC_LinIsr_4_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_LinIsr_6_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LinIsr_6()
 *********************************************************************************************************************/
extern FUNC(void, OS_LINISR_6_CODE) Os_Isr_LinIsr_6(void);

# define OS_STOP_SEC_LinIsr_6_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_LinIsr_7_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LinIsr_7()
 *********************************************************************************************************************/
extern FUNC(void, OS_LINISR_7_CODE) Os_Isr_LinIsr_7(void);

# define OS_STOP_SEC_LinIsr_7_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_LinIsr_8_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LinIsr_8()
 *********************************************************************************************************************/
extern FUNC(void, OS_LINISR_8_CODE) Os_Isr_LinIsr_8(void);

# define OS_STOP_SEC_LinIsr_8_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_LinIsr_9_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  LinIsr_9()
 *********************************************************************************************************************/
extern FUNC(void, OS_LINISR_9_CODE) Os_Isr_LinIsr_9(void);

# define OS_STOP_SEC_LinIsr_9_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_Mcu_PllDig_Pll0_LossOfLockIsr_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  Mcu_PllDig_Pll0_LossOfLockIsr()
 *********************************************************************************************************************/
extern FUNC(void, OS_MCU_PLLDIG_PLL0_LOSSOFLOCKISR_CODE) Os_Isr_Mcu_PllDig_Pll0_LossOfLockIsr(void);

# define OS_STOP_SEC_Mcu_PllDig_Pll0_LossOfLockIsr_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_WKPU_EXT_IRQ_0_7_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  WKPU_EXT_IRQ_0_7_ISR()
 *********************************************************************************************************************/
extern FUNC(void, OS_WKPU_EXT_IRQ_0_7_ISR_CODE) Os_Isr_WKPU_EXT_IRQ_0_7_ISR(void);

# define OS_STOP_SEC_WKPU_EXT_IRQ_0_7_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_WKPU_EXT_IRQ_16_23_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  WKPU_EXT_IRQ_16_23_ISR()
 *********************************************************************************************************************/
extern FUNC(void, OS_WKPU_EXT_IRQ_16_23_ISR_CODE) Os_Isr_WKPU_EXT_IRQ_16_23_ISR(void);

# define OS_STOP_SEC_WKPU_EXT_IRQ_16_23_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_WKPU_EXT_IRQ_24_31_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  WKPU_EXT_IRQ_24_31_ISR()
 *********************************************************************************************************************/
extern FUNC(void, OS_WKPU_EXT_IRQ_24_31_ISR_CODE) Os_Isr_WKPU_EXT_IRQ_24_31_ISR(void);

# define OS_STOP_SEC_WKPU_EXT_IRQ_24_31_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_WKPU_EXT_IRQ_8_15_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  WKPU_EXT_IRQ_8_15_ISR()
 *********************************************************************************************************************/
extern FUNC(void, OS_WKPU_EXT_IRQ_8_15_ISR_CODE) Os_Isr_WKPU_EXT_IRQ_8_15_ISR(void);

# define OS_STOP_SEC_WKPU_EXT_IRQ_8_15_ISR_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#endif /* OS_LCFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_Lcfg.h
 *********************************************************************************************************************/
