/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: LinTrcv
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: LinTrcv_GeneralTypes.h
 *   Generation Time: 2020-08-20 13:43:07
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/



#ifndef __LINTRCV_GENERALTYPES_H
#define __LINTRCV_GENERALTYPES_H

typedef enum LinTrcv_TrcvModeTypeTag 
{
  LINTRCV_TRCV_MODE_NORMAL  = 0u,
  LINTRCV_TRCV_MODE_STANDBY = 1u,
  LINTRCV_TRCV_MODE_SLEEP   = 2u
} LinTrcv_TrcvModeType;


typedef enum LinTrcv_TrcvWakeupModeTypeTag
{
  LINTRCV_WUMODE_ENABLE  = 0u,  
  LINTRCV_WUMODE_DISABLE = 1u,
  LINTRCV_WUMODE_CLEAR   = 2u
} LinTrcv_TrcvWakeupModeType;

typedef enum LinTrcv_TrcvWakeupReasonTypeTag
{
  LINTRCV_WU_ERROR = 0u,
  LINTRCV_WU_NOT_SUPPORTED = 1u,
  LINTRCV_WU_BY_BUS = 2u,
  LINTRCV_WU_BY_PIN = 3u,
  LINTRCV_WU_INTERNALLY = 4u,
  LINTRCV_WU_RESET = 5u,
  LINTRCV_WU_POWER_ON = 6u  
} LinTrcv_TrcvWakeupReasonType;




#endif

