/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: PduR
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: PduR_Lcfg.h
 *   Generation Time: 2020-11-05 18:26:04
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/


#if !defined (PDUR_LCFG_H)
# define PDUR_LCFG_H

/**********************************************************************************************************************
 * MISRA JUSTIFICATION
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
# include "PduR_Types.h"
# include "PduR_PBcfg.h"

/* include headers with symbolic name values */
/* \trace SPEC-2020167 */

#include "LinIf.h"
#include "CanIf.h"
#include "CanTp.h"
#include "Com_Cbk.h"
#include "Dcm_Cbk.h"
#include "J1939Tp.h"
#include "J1939Rm_Cbk.h"
#include "Cdd_Cbk.h"


/**********************************************************************************************************************
 * GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  PduRPCDataSwitches  PduR Data Switches  (PRE_COMPILE)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define PDUR_BMTXBUFFERARRAYRAM                                                                     STD_ON
#define PDUR_BMTXBUFFERINDROM                                                                       STD_ON
#define PDUR_BMTXBUFFERROMIDXOFBMTXBUFFERINDROM                                                     STD_ON
#define PDUR_BMTXBUFFERINSTANCERAM                                                                  STD_ON
#define PDUR_BMTXBUFFERARRAYRAMREADIDXOFBMTXBUFFERINSTANCERAM                                       STD_ON
#define PDUR_BMTXBUFFERARRAYRAMWRITEIDXOFBMTXBUFFERINSTANCERAM                                      STD_ON
#define PDUR_PDURBUFFERSTATEOFBMTXBUFFERINSTANCERAM                                                 STD_ON
#define PDUR_TXBUFFERUSEDOFBMTXBUFFERINSTANCERAM                                                    STD_ON
#define PDUR_BMTXBUFFERINSTANCEROM                                                                  STD_ON
#define PDUR_BMTXBUFFERROMIDXOFBMTXBUFFERINSTANCEROM                                                STD_ON
#define PDUR_BMTXBUFFERRAM                                                                          STD_ON
#define PDUR_ALLOCATEDOFBMTXBUFFERRAM                                                               STD_ON
#define PDUR_BMTXBUFFERARRAYRAMINSTANCESTOPIDXOFBMTXBUFFERRAM                                       STD_ON
#define PDUR_BMTXBUFFERARRAYRAMREADIDXOFBMTXBUFFERRAM                                               STD_ON
#define PDUR_BMTXBUFFERARRAYRAMWRITEIDXOFBMTXBUFFERRAM                                              STD_ON
#define PDUR_PDURBUFFERSTATEOFBMTXBUFFERRAM                                                         STD_ON
#define PDUR_RXLENGTHOFBMTXBUFFERRAM                                                                STD_ON
#define PDUR_BMTXBUFFERROM                                                                          STD_ON
#define PDUR_BMTXBUFFERARRAYRAMENDIDXOFBMTXBUFFERROM                                                STD_ON
#define PDUR_BMTXBUFFERARRAYRAMLENGTHOFBMTXBUFFERROM                                                STD_ON
#define PDUR_BMTXBUFFERARRAYRAMSTARTIDXOFBMTXBUFFERROM                                              STD_ON
#define PDUR_BMTXBUFFERINSTANCEROMENDIDXOFBMTXBUFFERROM                                             STD_ON
#define PDUR_BMTXBUFFERINSTANCEROMSTARTIDXOFBMTXBUFFERROM                                           STD_ON
#define PDUR_CONFIGID                                                                               STD_ON
#define PDUR_COREID2COREMANAGERROM                                                                  STD_OFF  /**< Deactivateable: 'PduR_CoreId2CoreManagerRom' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define PDUR_COREMANAGERROMIDXOFCOREID2COREMANAGERROM                                               STD_OFF  /**< Deactivateable: 'PduR_CoreId2CoreManagerRom.CoreManagerRomIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_COREMANAGERROMUSEDOFCOREID2COREMANAGERROM                                              STD_OFF  /**< Deactivateable: 'PduR_CoreId2CoreManagerRom.CoreManagerRomUsed' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_INVALIDHNDOFCOREID2COREMANAGERROM                                                      STD_OFF  /**< Deactivateable: 'PduR_CoreId2CoreManagerRom.InvalidHnd' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_COREMANAGERROM                                                                         STD_ON
#define PDUR_MMROMINDENDIDXOFCOREMANAGERROM                                                         STD_ON
#define PDUR_MMROMINDSTARTIDXOFCOREMANAGERROM                                                       STD_ON
#define PDUR_MMROMINDUSEDOFCOREMANAGERROM                                                           STD_ON
#define PDUR_SRCCOREROMENDIDXOFCOREMANAGERROM                                                       STD_OFF  /**< Deactivateable: 'PduR_CoreManagerRom.SrcCoreRomEndIdx' Reason: 'All indirection targets are deactivated in all variants.' */
#define PDUR_SRCCOREROMSTARTIDXOFCOREMANAGERROM                                                     STD_OFF  /**< Deactivateable: 'PduR_CoreManagerRom.SrcCoreRomStartIdx' Reason: 'All indirection targets are deactivated in all variants.' */
#define PDUR_DEFERREDEVENTCACHEARRAYRAM                                                             STD_OFF  /**< Deactivateable: 'PduR_DeferredEventCacheArrayRam' Reason: 'No Deferred Event Cache is configured' */
#define PDUR_RMGDESTROMIDXOFDEFERREDEVENTCACHEARRAYRAM                                              STD_OFF  /**< Deactivateable: 'PduR_DeferredEventCacheArrayRam.RmGDestRomIdx' Reason: 'No Deferred Event Cache is configured' */
#define PDUR_DEFERREDEVENTCACHERAM                                                                  STD_OFF  /**< Deactivateable: 'PduR_DeferredEventCacheRam' Reason: 'No Deferred Event Cache is configured' */
#define PDUR_DEFERREDEVENTCACHEARRAYRAMREADIDXOFDEFERREDEVENTCACHERAM                               STD_OFF  /**< Deactivateable: 'PduR_DeferredEventCacheRam.DeferredEventCacheArrayRamReadIdx' Reason: 'No Deferred Event Cache is configured' */
#define PDUR_DEFERREDEVENTCACHEARRAYRAMWRITEIDXOFDEFERREDEVENTCACHERAM                              STD_OFF  /**< Deactivateable: 'PduR_DeferredEventCacheRam.DeferredEventCacheArrayRamWriteIdx' Reason: 'No Deferred Event Cache is configured' */
#define PDUR_DEFERREDEVENTCACHEROM                                                                  STD_OFF  /**< Deactivateable: 'PduR_DeferredEventCacheRom' Reason: 'No Deferred Event Cache is configured' */
#define PDUR_DEFERREDEVENTCACHEARRAYRAMENDIDXOFDEFERREDEVENTCACHEROM                                STD_OFF  /**< Deactivateable: 'PduR_DeferredEventCacheRom.DeferredEventCacheArrayRamEndIdx' Reason: 'No Deferred Event Cache is configured' */
#define PDUR_DEFERREDEVENTCACHEARRAYRAMLENGTHOFDEFERREDEVENTCACHEROM                                STD_OFF  /**< Deactivateable: 'PduR_DeferredEventCacheRom.DeferredEventCacheArrayRamLength' Reason: 'No Deferred Event Cache is configured' */
#define PDUR_DEFERREDEVENTCACHEARRAYRAMSTARTIDXOFDEFERREDEVENTCACHEROM                              STD_OFF  /**< Deactivateable: 'PduR_DeferredEventCacheRom.DeferredEventCacheArrayRamStartIdx' Reason: 'No Deferred Event Cache is configured' */
#define PDUR_EXCLUSIVEAREAROM                                                                       STD_ON
#define PDUR_LOCKOFEXCLUSIVEAREAROM                                                                 STD_ON
#define PDUR_UNLOCKOFEXCLUSIVEAREAROM                                                               STD_ON
#define PDUR_FINALMAGICNUMBER                                                                       STD_OFF  /**< Deactivateable: 'PduR_FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define PDUR_FMFIFOELEMENTRAM                                                                       STD_ON
#define PDUR_BMTXBUFFERROMIDXOFFMFIFOELEMENTRAM                                                     STD_ON
#define PDUR_DEDICATEDTXBUFFEROFFMFIFOELEMENTRAM                                                    STD_ON
#define PDUR_RMDESTROMIDXOFFMFIFOELEMENTRAM                                                         STD_ON
#define PDUR_STATEOFFMFIFOELEMENTRAM                                                                STD_ON
#define PDUR_FMFIFOINSTANCERAM                                                                      STD_ON
#define PDUR_BMTXBUFFERINSTANCEROMIDXOFFMFIFOINSTANCERAM                                            STD_ON
#define PDUR_FMFIFOINSTANCEROM                                                                      STD_ON
#define PDUR_FMFIFOROMIDXOFFMFIFOINSTANCEROM                                                        STD_ON
#define PDUR_FMFIFORAM                                                                              STD_ON
#define PDUR_FILLLEVELOFFMFIFORAM                                                                   STD_ON
#define PDUR_FMFIFOELEMENTRAMREADIDXOFFMFIFORAM                                                     STD_ON
#define PDUR_FMFIFOELEMENTRAMWRITEIDXOFFMFIFORAM                                                    STD_ON
#define PDUR_PENDINGCONFIRMATIONSOFFMFIFORAM                                                        STD_ON
#define PDUR_TPTXSMSTATEOFFMFIFORAM                                                                 STD_ON
#define PDUR_FMFIFOROM                                                                              STD_ON
#define PDUR_BMTXBUFFERINDROMENDIDXOFFMFIFOROM                                                      STD_ON
#define PDUR_BMTXBUFFERINDROMLENGTHOFFMFIFOROM                                                      STD_ON
#define PDUR_BMTXBUFFERINDROMSTARTIDXOFFMFIFOROM                                                    STD_ON
#define PDUR_FMFIFOELEMENTRAMENDIDXOFFMFIFOROM                                                      STD_ON
#define PDUR_FMFIFOELEMENTRAMLENGTHOFFMFIFOROM                                                      STD_ON
#define PDUR_FMFIFOELEMENTRAMSTARTIDXOFFMFIFOROM                                                    STD_ON
#define PDUR_LOCKROMIDXOFFMFIFOROM                                                                  STD_OFF  /**< Deactivateable: 'PduR_FmFifoRom.LockRomIdx' Reason: 'the optional indirection is deactivated because LockRomUsedOfFmFifoRom is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define PDUR_LOCKROMUSEDOFFMFIFOROM                                                                 STD_OFF  /**< Deactivateable: 'PduR_FmFifoRom.LockRomUsed' Reason: 'the optional indirection is deactivated because LockRomUsedOfFmFifoRom is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define PDUR_FM_ACTIVATENEXT_FMSMSTATEHANDLER                                                       STD_ON
#define PDUR_FCTPTROFFM_ACTIVATENEXT_FMSMSTATEHANDLER                                               STD_ON
#define PDUR_FM_ACTIVATEREAD_FMSMSTATEHANDLER                                                       STD_ON
#define PDUR_FCTPTROFFM_ACTIVATEREAD_FMSMSTATEHANDLER                                               STD_ON
#define PDUR_FM_ACTIVATEWRITE_FMSMSTATEHANDLER                                                      STD_ON
#define PDUR_FCTPTROFFM_ACTIVATEWRITE_FMSMSTATEHANDLER                                              STD_ON
#define PDUR_FM_FINISHREAD_FMSMSTATEHANDLER                                                         STD_ON
#define PDUR_FCTPTROFFM_FINISHREAD_FMSMSTATEHANDLER                                                 STD_ON
#define PDUR_FM_FINISHWRITE_FMSMSTATEHANDLER                                                        STD_ON
#define PDUR_FCTPTROFFM_FINISHWRITE_FMSMSTATEHANDLER                                                STD_ON
#define PDUR_FM_TPDISABLEROUTING_FMSMSTATEHANDLER                                                   STD_OFF  /**< Deactivateable: 'PduR_Fm_TpDisableRouting_FmSmStateHandler' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define PDUR_FCTPTROFFM_TPDISABLEROUTING_FMSMSTATEHANDLER                                           STD_OFF  /**< Deactivateable: 'PduR_Fm_TpDisableRouting_FmSmStateHandler.FctPtr' Reason: 'No routing path groups present.' */
#define PDUR_GENERALPROPERTIESROM                                                                   STD_ON
#define PDUR_HASIFTXFIFOOFGENERALPROPERTIESROM                                                      STD_OFF  /**< Deactivateable: 'PduR_GeneralPropertiesRom.hasIfTxFifo' Reason: 'the value of PduR_hasIfTxFifoOfGeneralPropertiesRom is always 'false' due to this, the array is deactivated.' */
#define PDUR_HASTPTXBUFFEREDFORWARDINGOFGENERALPROPERTIESROM                                        STD_ON
#define PDUR_INITDATAHASHCODE                                                                       STD_OFF  /**< Deactivateable: 'PduR_InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define PDUR_INITIALIZED                                                                            STD_ON
#define PDUR_INTERFACEFIFOQUEUEARRAYRAM                                                             STD_OFF  /**< Deactivateable: 'PduR_InterfaceFifoQueueArrayRam' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_INTERFACEFIFOQUEUEELEMENTRAM                                                           STD_OFF  /**< Deactivateable: 'PduR_InterfaceFifoQueueElementRam' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_ACTUALLENGTHOFINTERFACEFIFOQUEUEELEMENTRAM                                             STD_OFF  /**< Deactivateable: 'PduR_InterfaceFifoQueueElementRam.ActualLength' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_INTERFACEFIFOQUEUERAM                                                                  STD_OFF  /**< Deactivateable: 'PduR_InterfaceFifoQueueRam' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_FIFOFULLOFINTERFACEFIFOQUEUERAM                                                        STD_OFF  /**< Deactivateable: 'PduR_InterfaceFifoQueueRam.FifoFull' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_INTERFACEFIFOQUEUEELEMENTRAMREADIDXOFINTERFACEFIFOQUEUERAM                             STD_OFF  /**< Deactivateable: 'PduR_InterfaceFifoQueueRam.InterfaceFifoQueueElementRamReadIdx' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_INTERFACEFIFOQUEUEELEMENTRAMWRITEIDXOFINTERFACEFIFOQUEUERAM                            STD_OFF  /**< Deactivateable: 'PduR_InterfaceFifoQueueRam.InterfaceFifoQueueElementRamWriteIdx' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_INTERFACEFIFOQUEUEROM                                                                  STD_OFF  /**< Deactivateable: 'PduR_InterfaceFifoQueueRom' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define PDUR_INTERFACEFIFOQUEUEARRAYRAMENDIDXOFINTERFACEFIFOQUEUEROM                                STD_OFF  /**< Deactivateable: 'PduR_InterfaceFifoQueueRom.InterfaceFifoQueueArrayRamEndIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_INTERFACEFIFOQUEUEARRAYRAMSTARTIDXOFINTERFACEFIFOQUEUEROM                              STD_OFF  /**< Deactivateable: 'PduR_InterfaceFifoQueueRom.InterfaceFifoQueueArrayRamStartIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_INTERFACEFIFOQUEUEELEMENTRAMENDIDXOFINTERFACEFIFOQUEUEROM                              STD_OFF  /**< Deactivateable: 'PduR_InterfaceFifoQueueRom.InterfaceFifoQueueElementRamEndIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_INTERFACEFIFOQUEUEELEMENTRAMSTARTIDXOFINTERFACEFIFOQUEUEROM                            STD_OFF  /**< Deactivateable: 'PduR_InterfaceFifoQueueRom.InterfaceFifoQueueElementRamStartIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_MAXPDULENGTHOFINTERFACEFIFOQUEUEROM                                                    STD_OFF  /**< Deactivateable: 'PduR_InterfaceFifoQueueRom.MaxPduLength' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_RMDESTROMIDXOFINTERFACEFIFOQUEUEROM                                                    STD_OFF  /**< Deactivateable: 'PduR_InterfaceFifoQueueRom.RmDestRomIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_LOCKROM                                                                                STD_ON
#define PDUR_EXCLUSIVEAREAROMIDXOFLOCKROM                                                           STD_ON
#define PDUR_EXCLUSIVEAREAROMUSEDOFLOCKROM                                                          STD_ON
#define PDUR_SPINLOCKRAMIDXOFLOCKROM                                                                STD_OFF  /**< Deactivateable: 'PduR_LockRom.SpinlockRamIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_SPINLOCKRAMUSEDOFLOCKROM                                                               STD_OFF  /**< Deactivateable: 'PduR_LockRom.SpinlockRamUsed' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_MCQBUFFERARRAYRAM                                                                      STD_OFF  /**< Deactivateable: 'PduR_McQBufferArrayRam' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_MCQBUFFERRAM                                                                           STD_OFF  /**< Deactivateable: 'PduR_McQBufferRam' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_MCQBUFFERARRAYRAMPENDINGREADIDXOFMCQBUFFERRAM                                          STD_OFF  /**< Deactivateable: 'PduR_McQBufferRam.McQBufferArrayRamPendingReadIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_MCQBUFFERARRAYRAMPENDINGWRITEIDXOFMCQBUFFERRAM                                         STD_OFF  /**< Deactivateable: 'PduR_McQBufferRam.McQBufferArrayRamPendingWriteIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_MCQBUFFERARRAYRAMREADIDXOFMCQBUFFERRAM                                                 STD_OFF  /**< Deactivateable: 'PduR_McQBufferRam.McQBufferArrayRamReadIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_MCQBUFFERARRAYRAMWRITEIDXOFMCQBUFFERRAM                                                STD_OFF  /**< Deactivateable: 'PduR_McQBufferRam.McQBufferArrayRamWriteIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_MCQBUFFERROM                                                                           STD_OFF  /**< Deactivateable: 'PduR_McQBufferRom' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_MCQBUFFERARRAYRAMENDIDXOFMCQBUFFERROM                                                  STD_OFF  /**< Deactivateable: 'PduR_McQBufferRom.McQBufferArrayRamEndIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_MCQBUFFERARRAYRAMLENGTHOFMCQBUFFERROM                                                  STD_OFF  /**< Deactivateable: 'PduR_McQBufferRom.McQBufferArrayRamLength' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_MCQBUFFERARRAYRAMSTARTIDXOFMCQBUFFERROM                                                STD_OFF  /**< Deactivateable: 'PduR_McQBufferRom.McQBufferArrayRamStartIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_MMROM                                                                                  STD_ON
#define PDUR_CANCELRECEIVESUPPORTEDOFMMROM                                                          STD_OFF  /**< Deactivateable: 'PduR_MmRom.CancelReceiveSupported' Reason: 'the value of PduR_CancelReceiveSupportedOfMmRom is always 'false' due to this, the array is deactivated.' */
#define PDUR_CHANGEPARAMETERSUPPORTEDOFMMROM                                                        STD_OFF  /**< Deactivateable: 'PduR_MmRom.ChangeParameterSupported' Reason: 'the value of PduR_ChangeParameterSupportedOfMmRom is always 'false' due to this, the array is deactivated.' */
#define PDUR_COREMANAGERROMIDXOFMMROM                                                               STD_ON
#define PDUR_IFCANCELTRANSMITSUPPORTEDOFMMROM                                                       STD_ON
#define PDUR_LOIFCANCELTRANSMITFCTPTROFMMROM                                                        STD_ON
#define PDUR_LOIFOFMMROM                                                                            STD_ON
#define PDUR_LOIFTRANSMITFCTPTROFMMROM                                                              STD_ON
#define PDUR_LOTPCANCELRECEIVEFCTPTROFMMROM                                                         STD_OFF  /**< Deactivateable: 'PduR_MmRom.LoTpCancelReceiveFctPtr' Reason: 'the value of PduR_LoTpCancelReceiveFctPtrOfMmRom is always 'NULL_PTR' due to this, the array is deactivated.' */
#define PDUR_LOTPCANCELTRANSMITFCTPTROFMMROM                                                        STD_ON
#define PDUR_LOTPCHANGEPARAMETERFCTPTROFMMROM                                                       STD_OFF  /**< Deactivateable: 'PduR_MmRom.LoTpChangeParameterFctPtr' Reason: 'the value of PduR_LoTpChangeParameterFctPtrOfMmRom is always 'NULL_PTR' due to this, the array is deactivated.' */
#define PDUR_LOTPOFMMROM                                                                            STD_ON
#define PDUR_LOTPTRANSMITFCTPTROFMMROM                                                              STD_ON
#define PDUR_MASKEDBITSOFMMROM                                                                      STD_ON
#define PDUR_RMGDESTROMENDIDXOFMMROM                                                                STD_ON
#define PDUR_RMGDESTROMSTARTIDXOFMMROM                                                              STD_ON
#define PDUR_RMGDESTROMUSEDOFMMROM                                                                  STD_ON
#define PDUR_TPCANCELTRANSMITSUPPORTEDOFMMROM                                                       STD_ON
#define PDUR_UPIFOFMMROM                                                                            STD_ON
#define PDUR_UPIFRXINDICATIONFCTPTROFMMROM                                                          STD_ON
#define PDUR_UPIFTRIGGERTRANSMITFCTPTROFMMROM                                                       STD_ON
#define PDUR_UPIFTXCONFIRMATIONFCTPTROFMMROM                                                        STD_ON
#define PDUR_UPTPCOPYRXDATAFCTPTROFMMROM                                                            STD_ON
#define PDUR_UPTPCOPYTXDATAFCTPTROFMMROM                                                            STD_ON
#define PDUR_UPTPOFMMROM                                                                            STD_ON
#define PDUR_UPTPSTARTOFRECEPTIONFCTPTROFMMROM                                                      STD_ON
#define PDUR_UPTPTPRXINDICATIONFCTPTROFMMROM                                                        STD_ON
#define PDUR_UPTPTPTXCONFIRMATIONFCTPTROFMMROM                                                      STD_ON
#define PDUR_MMROMIND                                                                               STD_ON
#define PDUR_QUEUEFCTPTRROM                                                                         STD_OFF  /**< Deactivateable: 'PduR_QueueFctPtrRom' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define PDUR_FLUSHFCTPTROFQUEUEFCTPTRROM                                                            STD_OFF  /**< Deactivateable: 'PduR_QueueFctPtrRom.FlushFctPtr' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_GETFCTPTROFQUEUEFCTPTRROM                                                              STD_OFF  /**< Deactivateable: 'PduR_QueueFctPtrRom.GetFctPtr' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_GETFILLLEVELFCTPTROFQUEUEFCTPTRROM                                                     STD_OFF  /**< Deactivateable: 'PduR_QueueFctPtrRom.GetFillLevelFctPtr' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_PUTFCTPTROFQUEUEFCTPTRROM                                                              STD_OFF  /**< Deactivateable: 'PduR_QueueFctPtrRom.PutFctPtr' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_REMOVEFCTPTROFQUEUEFCTPTRROM                                                           STD_OFF  /**< Deactivateable: 'PduR_QueueFctPtrRom.RemoveFctPtr' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_RMBUFFEREDIFPROPERTIESRAM                                                              STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRam' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_IFSMSTATEOFRMBUFFEREDIFPROPERTIESRAM                                                   STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRam.IfSmState' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_RMBUFFEREDIFPROPERTIESROM                                                              STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define PDUR_DEDICATEDTXBUFFEROFRMBUFFEREDIFPROPERTIESROM                                           STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.DedicatedTxBuffer' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_IMPLEMENTATIONTYPEOFRMBUFFEREDIFPROPERTIESROM                                          STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.ImplementationType' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_INTERFACEFIFOQUEUEROMIDXOFRMBUFFEREDIFPROPERTIESROM                                    STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.InterfaceFifoQueueRomIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_INTERFACEFIFOQUEUEROMUSEDOFRMBUFFEREDIFPROPERTIESROM                                   STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.InterfaceFifoQueueRomUsed' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_MAXPDULENGTHOFRMBUFFEREDIFPROPERTIESROM                                                STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.MaxPduLength' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_QUEUEDATAPROVISIONTYPEOFRMBUFFEREDIFPROPERTIESROM                                      STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.QueueDataProvisionType' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_QUEUEFCTPTRROMIDXOFRMBUFFEREDIFPROPERTIESROM                                           STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.QueueFctPtrRomIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_QUEUETYPEOFRMBUFFEREDIFPROPERTIESROM                                                   STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.QueueType' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_SINGLEBUFFERROMIDXOFRMBUFFEREDIFPROPERTIESROM                                          STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.SingleBufferRomIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_SINGLEBUFFERROMUSEDOFRMBUFFEREDIFPROPERTIESROM                                         STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.SingleBufferRomUsed' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_RMBUFFEREDTPPROPERTIESRAM                                                              STD_ON
#define PDUR_FMFIFOELEMENTRAMIDXOFRMBUFFEREDTPPROPERTIESRAM                                         STD_ON
#define PDUR_TPRXSMSTATEOFRMBUFFEREDTPPROPERTIESRAM                                                 STD_ON
#define PDUR_RMBUFFEREDTPPROPERTIESROM                                                              STD_ON
#define PDUR_DEDICATEDTXBUFFEROFRMBUFFEREDTPPROPERTIESROM                                           STD_ON
#define PDUR_FMFIFOROMIDXOFRMBUFFEREDTPPROPERTIESROM                                                STD_ON
#define PDUR_METADATALENGTHOFRMBUFFEREDTPPROPERTIESROM                                              STD_OFF  /**< Deactivateable: 'PduR_RmBufferedTpPropertiesRom.MetaDataLength' Reason: 'Meta Data Support is not active' */
#define PDUR_METADATALENGTHUSEDOFRMBUFFEREDTPPROPERTIESROM                                          STD_OFF  /**< Deactivateable: 'PduR_RmBufferedTpPropertiesRom.MetaDataLengthUsed' Reason: 'the value of PduR_MetaDataLengthUsedOfRmBufferedTpPropertiesRom is always 'false' due to this, the array is deactivated.' */
#define PDUR_QUEUEDDESTCNTOFRMBUFFEREDTPPROPERTIESROM                                               STD_ON
#define PDUR_TPTHRESHOLDOFRMBUFFEREDTPPROPERTIESROM                                                 STD_ON
#define PDUR_RMDESTROM                                                                              STD_ON
#define PDUR_PDULENGTHHANDLINGSTRATEGYOFRMDESTROM                                                   STD_OFF  /**< Deactivateable: 'PduR_RmDestRom.PduLengthHandlingStrategy' Reason: 'the value of PduR_PduLengthHandlingStrategyOfRmDestRom is always '0' due to this, the array is deactivated.' */
#define PDUR_RMDESTRPGROMIDXOFRMDESTROM                                                             STD_OFF  /**< Deactivateable: 'PduR_RmDestRom.RmDestRpgRomIdx' Reason: 'the optional indirection is deactivated because RmDestRpgRomUsedOfRmDestRom is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define PDUR_RMDESTRPGROMUSEDOFRMDESTROM                                                            STD_OFF  /**< Deactivateable: 'PduR_RmDestRom.RmDestRpgRomUsed' Reason: 'the optional indirection is deactivated because RmDestRpgRomUsedOfRmDestRom is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define PDUR_RMGDESTROMIDXOFRMDESTROM                                                               STD_ON
#define PDUR_RMSRCROMIDXOFRMDESTROM                                                                 STD_ON
#define PDUR_ROUTINGTYPEOFRMDESTROM                                                                 STD_ON
#define PDUR_RMDESTRPGRAM                                                                           STD_OFF  /**< Deactivateable: 'PduR_RmDestRpgRam' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_ENABLEDCNTOFRMDESTRPGRAM                                                               STD_OFF  /**< Deactivateable: 'PduR_RmDestRpgRam.EnabledCnt' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_RMDESTRPGROM                                                                           STD_OFF  /**< Deactivateable: 'PduR_RmDestRpgRom' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define PDUR_INITIALENABLEDCNTOFRMDESTRPGROM                                                        STD_OFF  /**< Deactivateable: 'PduR_RmDestRpgRom.InitialEnabledCnt' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_RMDESTROMIDXOFRMDESTRPGROM                                                             STD_OFF  /**< Deactivateable: 'PduR_RmDestRpgRom.RmDestRomIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_RMDESTROMUSEDOFRMDESTRPGROM                                                            STD_OFF  /**< Deactivateable: 'PduR_RmDestRpgRom.RmDestRomUsed' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_RMDESTRPGROMIND                                                                        STD_OFF  /**< Deactivateable: 'PduR_RmDestRpgRomInd' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_RMGDESTNTO1INFORAM                                                                     STD_OFF  /**< Deactivateable: 'PduR_RmGDestNto1InfoRam' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_RMDESTROMIDXOFRMGDESTNTO1INFORAM                                                       STD_OFF  /**< Deactivateable: 'PduR_RmGDestNto1InfoRam.RmDestRomIdx' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_TRANSMISSIONACTIVEOFRMGDESTNTO1INFORAM                                                 STD_OFF  /**< Deactivateable: 'PduR_RmGDestNto1InfoRam.TransmissionActive' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_RMGDESTROM                                                                             STD_ON
#define PDUR_DESTHNDOFRMGDESTROM                                                                    STD_ON
#define PDUR_DIRECTIONOFRMGDESTROM                                                                  STD_ON
#define PDUR_FMFIFOINSTANCEROMIDXOFRMGDESTROM                                                       STD_ON
#define PDUR_FMFIFOINSTANCEROMUSEDOFRMGDESTROM                                                      STD_ON
#define PDUR_LOCKROMIDXOFRMGDESTROM                                                                 STD_ON
#define PDUR_MASKEDBITSOFRMGDESTROM                                                                 STD_ON
#define PDUR_MMROMIDXOFRMGDESTROM                                                                   STD_ON
#define PDUR_PDURDESTPDUPROCESSINGOFRMGDESTROM                                                      STD_ON
#define PDUR_RMBUFFEREDIFPROPERTIESROMIDXOFRMGDESTROM                                               STD_OFF  /**< Deactivateable: 'PduR_RmGDestRom.RmBufferedIfPropertiesRomIdx' Reason: 'the optional indirection is deactivated because RmBufferedIfPropertiesRomUsedOfRmGDestRom is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define PDUR_RMBUFFEREDIFPROPERTIESROMUSEDOFRMGDESTROM                                              STD_OFF  /**< Deactivateable: 'PduR_RmGDestRom.RmBufferedIfPropertiesRomUsed' Reason: 'the optional indirection is deactivated because RmBufferedIfPropertiesRomUsedOfRmGDestRom is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define PDUR_RMDESTROMIDXOFRMGDESTROM                                                               STD_ON
#define PDUR_RMDESTROMUSEDOFRMGDESTROM                                                              STD_ON
#define PDUR_RMGDESTNTO1INFORAMIDXOFRMGDESTROM                                                      STD_OFF  /**< Deactivateable: 'PduR_RmGDestRom.RmGDestNto1InfoRamIdx' Reason: 'the optional indirection is deactivated because RmGDestNto1InfoRamUsedOfRmGDestRom is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define PDUR_RMGDESTNTO1INFORAMUSEDOFRMGDESTROM                                                     STD_OFF  /**< Deactivateable: 'PduR_RmGDestRom.RmGDestNto1InfoRamUsed' Reason: 'the optional indirection is deactivated because RmGDestNto1InfoRamUsedOfRmGDestRom is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define PDUR_RMGDESTTPTXSTATERAMIDXOFRMGDESTROM                                                     STD_ON
#define PDUR_RMGDESTTPTXSTATERAMUSEDOFRMGDESTROM                                                    STD_ON
#define PDUR_SMGDESTROMIDXOFRMGDESTROM                                                              STD_OFF  /**< Deactivateable: 'PduR_RmGDestRom.SmGDestRomIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMGDESTROMUSEDOFRMGDESTROM                                                             STD_OFF  /**< Deactivateable: 'PduR_RmGDestRom.SmGDestRomUsed' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_RMGDESTTPTXSTATERAM                                                                    STD_ON
#define PDUR_TPTXINSTSMSTATEOFRMGDESTTPTXSTATERAM                                                   STD_ON
#define PDUR_RMIF_TXCONFIRMATION_STATEHANDLER                                                       STD_OFF  /**< Deactivateable: 'PduR_RmIf_TxConfirmation_StateHandler' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define PDUR_FCTPTROFRMIF_TXCONFIRMATION_STATEHANDLER                                               STD_OFF  /**< Deactivateable: 'PduR_RmIf_TxConfirmation_StateHandler.FctPtr' Reason: 'No Communication Interface Gateway Routing with TriggerTx-with-multiple-buffers, or DirectTx-with-single-buffer available' */
#define PDUR_RMSRCROM                                                                               STD_ON
#define PDUR_LOCKROMIDXOFRMSRCROM                                                                   STD_ON
#define PDUR_MASKEDBITSOFRMSRCROM                                                                   STD_ON
#define PDUR_MMROMIDXOFRMSRCROM                                                                     STD_ON
#define PDUR_RMBUFFEREDTPPROPERTIESROMIDXOFRMSRCROM                                                 STD_ON
#define PDUR_RMBUFFEREDTPPROPERTIESROMUSEDOFRMSRCROM                                                STD_ON
#define PDUR_RMDESTROMENDIDXOFRMSRCROM                                                              STD_ON
#define PDUR_RMDESTROMLENGTHOFRMSRCROM                                                              STD_ON
#define PDUR_RMDESTROMSTARTIDXOFRMSRCROM                                                            STD_ON
#define PDUR_SMSRCROMIDXOFRMSRCROM                                                                  STD_OFF  /**< Deactivateable: 'PduR_RmSrcRom.SmSrcRomIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMSRCROMUSEDOFRMSRCROM                                                                 STD_OFF  /**< Deactivateable: 'PduR_RmSrcRom.SmSrcRomUsed' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SRCHNDOFRMSRCROM                                                                       STD_ON
#define PDUR_TRIGGERTRANSMITSUPPORTEDOFRMSRCROM                                                     STD_ON
#define PDUR_TXCONFIRMATIONSUPPORTEDOFRMSRCROM                                                      STD_ON
#define PDUR_RMTP_CANCELRECEIVE_TPRXSMSTATEHANDLER                                                  STD_ON
#define PDUR_FCTPTROFRMTP_CANCELRECEIVE_TPRXSMSTATEHANDLER                                          STD_ON
#define PDUR_RMTP_CHECKREADY2TRANSMIT_TPTXSMSTATEHANDLER                                            STD_ON
#define PDUR_FCTPTROFRMTP_CHECKREADY2TRANSMIT_TPTXSMSTATEHANDLER                                    STD_ON
#define PDUR_RMTP_COPYRXDATA_TPRXSMSTATEHANDLER                                                     STD_ON
#define PDUR_FCTPTROFRMTP_COPYRXDATA_TPRXSMSTATEHANDLER                                             STD_ON
#define PDUR_RMTP_FINISHRECEPTION_TPTXSMSTATEHANDLER                                                STD_ON
#define PDUR_FCTPTROFRMTP_FINISHRECEPTION_TPTXSMSTATEHANDLER                                        STD_ON
#define PDUR_RMTP_FINISHTRANSMISSION_TPTXSMSTATEHANDLER                                             STD_ON
#define PDUR_FCTPTROFRMTP_FINISHTRANSMISSION_TPTXSMSTATEHANDLER                                     STD_ON
#define PDUR_RMTP_STARTOFRECEPTION_TPRXSMSTATEHANDLER                                               STD_ON
#define PDUR_FCTPTROFRMTP_STARTOFRECEPTION_TPRXSMSTATEHANDLER                                       STD_ON
#define PDUR_RMTP_TPRXINDICATION_TPRXSMSTATEHANDLER                                                 STD_ON
#define PDUR_FCTPTROFRMTP_TPRXINDICATION_TPRXSMSTATEHANDLER                                         STD_ON
#define PDUR_RMTRANSMITFCTPTR                                                                       STD_ON
#define PDUR_RMTXINSTSMROM                                                                          STD_ON
#define PDUR_PDUR_RMTP_TXINST_CANCELTRANSMITOFRMTXINSTSMROM                                         STD_ON
#define PDUR_PDUR_RMTP_TXINST_COPYTXDATAOFRMTXINSTSMROM                                             STD_ON
#define PDUR_PDUR_RMTP_TXINST_TRIGGERTRANSMITOFRMTXINSTSMROM                                        STD_ON
#define PDUR_PDUR_RMTP_TXINST_TXCONFIRMATIONOFRMTXINSTSMROM                                         STD_ON
#define PDUR_RPGRAM                                                                                 STD_OFF  /**< Deactivateable: 'PduR_RpgRam' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_ENABLEDOFRPGRAM                                                                        STD_OFF  /**< Deactivateable: 'PduR_RpgRam.Enabled' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_RPGROM                                                                                 STD_OFF  /**< Deactivateable: 'PduR_RpgRom' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define PDUR_ENABLEDATINITOFRPGROM                                                                  STD_OFF  /**< Deactivateable: 'PduR_RpgRom.EnabledAtInit' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_INVALIDHNDOFRPGROM                                                                     STD_OFF  /**< Deactivateable: 'PduR_RpgRom.InvalidHnd' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_RMDESTRPGROMINDENDIDXOFRPGROM                                                          STD_OFF  /**< Deactivateable: 'PduR_RpgRom.RmDestRpgRomIndEndIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_RMDESTRPGROMINDSTARTIDXOFRPGROM                                                        STD_OFF  /**< Deactivateable: 'PduR_RpgRom.RmDestRpgRomIndStartIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_RMDESTRPGROMINDUSEDOFRPGROM                                                            STD_OFF  /**< Deactivateable: 'PduR_RpgRom.RmDestRpgRomIndUsed' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_RXIF2DEST                                                                              STD_ON
#define PDUR_BSWMPDURRXINDICATIONCALLBACKOFRXIF2DEST                                                STD_OFF  /**< Deactivateable: 'PduR_RxIf2Dest.BswMPduRRxIndicationCallback' Reason: 'Callback Support is not active' */
#define PDUR_INVALIDHNDOFRXIF2DEST                                                                  STD_OFF  /**< Deactivateable: 'PduR_RxIf2Dest.InvalidHnd' Reason: 'the value of PduR_InvalidHndOfRxIf2Dest is always 'false' due to this, the array is deactivated.' */
#define PDUR_RMSRCROMIDXOFRXIF2DEST                                                                 STD_ON
#define PDUR_RXTP2DEST                                                                              STD_ON
#define PDUR_BSWMPDURTPRXINDICATIONCALLBACKOFRXTP2DEST                                              STD_OFF  /**< Deactivateable: 'PduR_RxTp2Dest.BswMPduRTpRxIndicationCallback' Reason: 'Callback Support is not active' */
#define PDUR_BSWMPDURTPSTARTOFRECEPTIONCALLBACKOFRXTP2DEST                                          STD_OFF  /**< Deactivateable: 'PduR_RxTp2Dest.BswMPduRTpStartOfReceptionCallback' Reason: 'Callback Support is not active' */
#define PDUR_INVALIDHNDOFRXTP2DEST                                                                  STD_OFF  /**< Deactivateable: 'PduR_RxTp2Dest.InvalidHnd' Reason: 'the value of PduR_InvalidHndOfRxTp2Dest is always 'false' due to this, the array is deactivated.' */
#define PDUR_RMSRCROMIDXOFRXTP2DEST                                                                 STD_ON
#define PDUR_RMSRCROMUSEDOFRXTP2DEST                                                                STD_ON
#define PDUR_RXTP2SRC                                                                               STD_OFF  /**< Deactivateable: 'PduR_RxTp2Src' Reason: '(No PduRBswModule configured which uses the CancelReceive API.. Evaluated DefinitionRef: /MICROSAR/PduR/PduRBswModules/PduRCancelReceive) && (No PduRBswModule configured which uses the ChangeParameter API.. Evaluated DefinitionRef: /MICROSAR/PduR/PduRBswModules/PduRChangeParameterRequestApi)' */
#define PDUR_INVALIDHNDOFRXTP2SRC                                                                   STD_OFF  /**< Deactivateable: 'PduR_RxTp2Src.InvalidHnd' Reason: '(No PduRBswModule configured which uses the CancelReceive API.. Evaluated DefinitionRef: /MICROSAR/PduR/PduRBswModules/PduRCancelReceive) && (No PduRBswModule configured which uses the ChangeParameter API.. Evaluated DefinitionRef: /MICROSAR/PduR/PduRBswModules/PduRChangeParameterRequestApi)' */
#define PDUR_RMDESTROMIDXOFRXTP2SRC                                                                 STD_OFF  /**< Deactivateable: 'PduR_RxTp2Src.RmDestRomIdx' Reason: '(No PduRBswModule configured which uses the CancelReceive API.. Evaluated DefinitionRef: /MICROSAR/PduR/PduRBswModules/PduRCancelReceive) && (No PduRBswModule configured which uses the ChangeParameter API.. Evaluated DefinitionRef: /MICROSAR/PduR/PduRBswModules/PduRChangeParameterRequestApi)' */
#define PDUR_SINGLEBUFFERARRAYRAM                                                                   STD_OFF  /**< Deactivateable: 'PduR_SingleBufferArrayRam' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_SINGLEBUFFERINITVALUESROM                                                              STD_OFF  /**< Deactivateable: 'PduR_SingleBufferInitValuesRom' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_SINGLEBUFFERRAM                                                                        STD_OFF  /**< Deactivateable: 'PduR_SingleBufferRam' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_ACTUALLENGTHOFSINGLEBUFFERRAM                                                          STD_OFF  /**< Deactivateable: 'PduR_SingleBufferRam.ActualLength' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define PDUR_SINGLEBUFFERROM                                                                        STD_OFF  /**< Deactivateable: 'PduR_SingleBufferRom' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define PDUR_RMDESTROMIDXOFSINGLEBUFFERROM                                                          STD_OFF  /**< Deactivateable: 'PduR_SingleBufferRom.RmDestRomIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_SINGLEBUFFERARRAYRAMENDIDXOFSINGLEBUFFERROM                                            STD_OFF  /**< Deactivateable: 'PduR_SingleBufferRom.SingleBufferArrayRamEndIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_SINGLEBUFFERARRAYRAMLENGTHOFSINGLEBUFFERROM                                            STD_OFF  /**< Deactivateable: 'PduR_SingleBufferRom.SingleBufferArrayRamLength' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_SINGLEBUFFERARRAYRAMSTARTIDXOFSINGLEBUFFERROM                                          STD_OFF  /**< Deactivateable: 'PduR_SingleBufferRom.SingleBufferArrayRamStartIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_SINGLEBUFFERINITVALUESROMENDIDXOFSINGLEBUFFERROM                                       STD_OFF  /**< Deactivateable: 'PduR_SingleBufferRom.SingleBufferInitValuesRomEndIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_SINGLEBUFFERINITVALUESROMLENGTHOFSINGLEBUFFERROM                                       STD_OFF  /**< Deactivateable: 'PduR_SingleBufferRom.SingleBufferInitValuesRomLength' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_SINGLEBUFFERINITVALUESROMSTARTIDXOFSINGLEBUFFERROM                                     STD_OFF  /**< Deactivateable: 'PduR_SingleBufferRom.SingleBufferInitValuesRomStartIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_SIZEOFBMTXBUFFERARRAYRAM                                                               STD_ON
#define PDUR_SIZEOFBMTXBUFFERINDROM                                                                 STD_ON
#define PDUR_SIZEOFBMTXBUFFERINSTANCERAM                                                            STD_ON
#define PDUR_SIZEOFBMTXBUFFERINSTANCEROM                                                            STD_ON
#define PDUR_SIZEOFBMTXBUFFERRAM                                                                    STD_ON
#define PDUR_SIZEOFBMTXBUFFERROM                                                                    STD_ON
#define PDUR_SIZEOFCOREMANAGERROM                                                                   STD_ON
#define PDUR_SIZEOFEXCLUSIVEAREAROM                                                                 STD_ON
#define PDUR_SIZEOFFMFIFOELEMENTRAM                                                                 STD_ON
#define PDUR_SIZEOFFMFIFOINSTANCERAM                                                                STD_ON
#define PDUR_SIZEOFFMFIFOINSTANCEROM                                                                STD_ON
#define PDUR_SIZEOFFMFIFORAM                                                                        STD_ON
#define PDUR_SIZEOFFMFIFOROM                                                                        STD_ON
#define PDUR_SIZEOFFM_ACTIVATENEXT_FMSMSTATEHANDLER                                                 STD_ON
#define PDUR_SIZEOFFM_ACTIVATEREAD_FMSMSTATEHANDLER                                                 STD_ON
#define PDUR_SIZEOFFM_ACTIVATEWRITE_FMSMSTATEHANDLER                                                STD_ON
#define PDUR_SIZEOFFM_FINISHREAD_FMSMSTATEHANDLER                                                   STD_ON
#define PDUR_SIZEOFFM_FINISHWRITE_FMSMSTATEHANDLER                                                  STD_ON
#define PDUR_SIZEOFGENERALPROPERTIESROM                                                             STD_ON
#define PDUR_SIZEOFLOCKROM                                                                          STD_ON
#define PDUR_SIZEOFMMROM                                                                            STD_ON
#define PDUR_SIZEOFMMROMIND                                                                         STD_ON
#define PDUR_SIZEOFRMBUFFEREDTPPROPERTIESRAM                                                        STD_ON
#define PDUR_SIZEOFRMBUFFEREDTPPROPERTIESROM                                                        STD_ON
#define PDUR_SIZEOFRMDESTROM                                                                        STD_ON
#define PDUR_SIZEOFRMGDESTROM                                                                       STD_ON
#define PDUR_SIZEOFRMGDESTTPTXSTATERAM                                                              STD_ON
#define PDUR_SIZEOFRMSRCROM                                                                         STD_ON
#define PDUR_SIZEOFRMTP_CANCELRECEIVE_TPRXSMSTATEHANDLER                                            STD_ON
#define PDUR_SIZEOFRMTP_CHECKREADY2TRANSMIT_TPTXSMSTATEHANDLER                                      STD_ON
#define PDUR_SIZEOFRMTP_COPYRXDATA_TPRXSMSTATEHANDLER                                               STD_ON
#define PDUR_SIZEOFRMTP_FINISHRECEPTION_TPTXSMSTATEHANDLER                                          STD_ON
#define PDUR_SIZEOFRMTP_FINISHTRANSMISSION_TPTXSMSTATEHANDLER                                       STD_ON
#define PDUR_SIZEOFRMTP_STARTOFRECEPTION_TPRXSMSTATEHANDLER                                         STD_ON
#define PDUR_SIZEOFRMTP_TPRXINDICATION_TPRXSMSTATEHANDLER                                           STD_ON
#define PDUR_SIZEOFRMTRANSMITFCTPTR                                                                 STD_ON
#define PDUR_SIZEOFRMTXINSTSMROM                                                                    STD_ON
#define PDUR_SIZEOFRXIF2DEST                                                                        STD_ON
#define PDUR_SIZEOFRXTP2DEST                                                                        STD_ON
#define PDUR_SIZEOFTX2LO                                                                            STD_ON
#define PDUR_SIZEOFTXIF2UP                                                                          STD_ON
#define PDUR_SIZEOFTXTP2SRC                                                                         STD_ON
#define PDUR_SMDATAPLANEROM                                                                         STD_OFF  /**< Deactivateable: 'PduR_SmDataPlaneRom' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMFIBRAMENDIDXOFSMDATAPLANEROM                                                         STD_OFF  /**< Deactivateable: 'PduR_SmDataPlaneRom.SmFibRamEndIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMFIBRAMSTARTIDXOFSMDATAPLANEROM                                                       STD_OFF  /**< Deactivateable: 'PduR_SmDataPlaneRom.SmFibRamStartIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMFIBRAMUSEDOFSMDATAPLANEROM                                                           STD_OFF  /**< Deactivateable: 'PduR_SmDataPlaneRom.SmFibRamUsed' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMLINEARTATOSACALCULATIONSTRATEGYROMIDXOFSMDATAPLANEROM                                STD_OFF  /**< Deactivateable: 'PduR_SmDataPlaneRom.SmLinearTaToSaCalculationStrategyRomIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMLINEARTATOSACALCULATIONSTRATEGYROMUSEDOFSMDATAPLANEROM                               STD_OFF  /**< Deactivateable: 'PduR_SmDataPlaneRom.SmLinearTaToSaCalculationStrategyRomUsed' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMSATAFROMMETADATACALCULATIONSTRATEGYROMIDXOFSMDATAPLANEROM                            STD_OFF  /**< Deactivateable: 'PduR_SmDataPlaneRom.SmSaTaFromMetaDataCalculationStrategyRomIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMSATAFROMMETADATACALCULATIONSTRATEGYROMUSEDOFSMDATAPLANEROM                           STD_OFF  /**< Deactivateable: 'PduR_SmDataPlaneRom.SmSaTaFromMetaDataCalculationStrategyRomUsed' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMFIBRAM                                                                               STD_OFF  /**< Deactivateable: 'PduR_SmFibRam' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_LEARNEDCONNECTIONIDOFSMFIBRAM                                                          STD_OFF  /**< Deactivateable: 'PduR_SmFibRam.LearnedConnectionId' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMGDESTFILTERFCTPTR                                                                    STD_OFF  /**< Deactivateable: 'PduR_SmGDestFilterFctPtr' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMGDESTROM                                                                             STD_OFF  /**< Deactivateable: 'PduR_SmGDestRom' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_CONNECTIONIDOFSMGDESTROM                                                               STD_OFF  /**< Deactivateable: 'PduR_SmGDestRom.ConnectionId' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_METADATALENGTHOFSMGDESTROM                                                             STD_OFF  /**< Deactivateable: 'PduR_SmGDestRom.MetaDataLength' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMDATAPLANEROMIDXOFSMGDESTROM                                                          STD_OFF  /**< Deactivateable: 'PduR_SmGDestRom.SmDataPlaneRomIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMGDESTFILTERFCTPTRIDXOFSMGDESTROM                                                     STD_OFF  /**< Deactivateable: 'PduR_SmGDestRom.SmGDestFilterFctPtrIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMLINEARTATOSACALCULATIONSTRATEGYGETSAFCTPTR                                           STD_OFF  /**< Deactivateable: 'PduR_SmLinearTaToSaCalculationStrategyGetSaFctPtr' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMLINEARTATOSACALCULATIONSTRATEGYROM                                                   STD_OFF  /**< Deactivateable: 'PduR_SmLinearTaToSaCalculationStrategyRom' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_MASKOFSMLINEARTATOSACALCULATIONSTRATEGYROM                                             STD_OFF  /**< Deactivateable: 'PduR_SmLinearTaToSaCalculationStrategyRom.Mask' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_OFFSETOFSMLINEARTATOSACALCULATIONSTRATEGYROM                                           STD_OFF  /**< Deactivateable: 'PduR_SmLinearTaToSaCalculationStrategyRom.Offset' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMSATAFROMMETADATACALCULATIONSTRATEGYROM                                               STD_OFF  /**< Deactivateable: 'PduR_SmSaTaFromMetaDataCalculationStrategyRom' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SAMASKOFSMSATAFROMMETADATACALCULATIONSTRATEGYROM                                       STD_OFF  /**< Deactivateable: 'PduR_SmSaTaFromMetaDataCalculationStrategyRom.SaMask' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SASTARTBITOFSMSATAFROMMETADATACALCULATIONSTRATEGYROM                                   STD_OFF  /**< Deactivateable: 'PduR_SmSaTaFromMetaDataCalculationStrategyRom.SaStartBit' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_TAMASKOFSMSATAFROMMETADATACALCULATIONSTRATEGYROM                                       STD_OFF  /**< Deactivateable: 'PduR_SmSaTaFromMetaDataCalculationStrategyRom.TaMask' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_TASTARTBITOFSMSATAFROMMETADATACALCULATIONSTRATEGYROM                                   STD_OFF  /**< Deactivateable: 'PduR_SmSaTaFromMetaDataCalculationStrategyRom.TaStartBit' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMSRCFILTERFCTPTR                                                                      STD_OFF  /**< Deactivateable: 'PduR_SmSrcFilterFctPtr' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMSRCROM                                                                               STD_OFF  /**< Deactivateable: 'PduR_SmSrcRom' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_CONNECTIONIDOFSMSRCROM                                                                 STD_OFF  /**< Deactivateable: 'PduR_SmSrcRom.ConnectionId' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_METADATALENGTHOFSMSRCROM                                                               STD_OFF  /**< Deactivateable: 'PduR_SmSrcRom.MetaDataLength' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMDATAPLANEROMIDXOFSMSRCROM                                                            STD_OFF  /**< Deactivateable: 'PduR_SmSrcRom.SmDataPlaneRomIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMLINEARTATOSACALCULATIONSTRATEGYGETSAFCTPTRIDXOFSMSRCROM                              STD_OFF  /**< Deactivateable: 'PduR_SmSrcRom.SmLinearTaToSaCalculationStrategyGetSaFctPtrIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMLINEARTATOSACALCULATIONSTRATEGYGETSAFCTPTRUSEDOFSMSRCROM                             STD_OFF  /**< Deactivateable: 'PduR_SmSrcRom.SmLinearTaToSaCalculationStrategyGetSaFctPtrUsed' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SMSRCFILTERFCTPTRIDXOFSMSRCROM                                                         STD_OFF  /**< Deactivateable: 'PduR_SmSrcRom.SmSrcFilterFctPtrIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSwitching] is configured to 'false'' */
#define PDUR_SPINLOCKRAM                                                                            STD_OFF  /**< Deactivateable: 'PduR_SpinlockRam' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_COUNTEROFSPINLOCKRAM                                                                   STD_OFF  /**< Deactivateable: 'PduR_SpinlockRam.Counter' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_LOCKVARIABLEOFSPINLOCKRAM                                                              STD_OFF  /**< Deactivateable: 'PduR_SpinlockRam.LockVariable' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_SRCCOREROM                                                                             STD_OFF  /**< Deactivateable: 'PduR_SrcCoreRom' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define PDUR_DEFERREDEVENTCACHEROMIDXOFSRCCOREROM                                                   STD_OFF  /**< Deactivateable: 'PduR_SrcCoreRom.DeferredEventCacheRomIdx' Reason: 'No Deferred Event Cache is configured' */
#define PDUR_DEFERREDEVENTCACHEROMUSEDOFSRCCOREROM                                                  STD_OFF  /**< Deactivateable: 'PduR_SrcCoreRom.DeferredEventCacheRomUsed' Reason: 'No Deferred Event Cache is configured' */
#define PDUR_MCQBUFFERROMIDXOFSRCCOREROM                                                            STD_OFF  /**< Deactivateable: 'PduR_SrcCoreRom.McQBufferRomIdx' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_MCQBUFFERROMUSEDOFSRCCOREROM                                                           STD_OFF  /**< Deactivateable: 'PduR_SrcCoreRom.McQBufferRomUsed' Reason: '/ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] is configured to 'false'' */
#define PDUR_TX2LO                                                                                  STD_ON
#define PDUR_BSWMPDURTRANSMITCALLBACKOFTX2LO                                                        STD_OFF  /**< Deactivateable: 'PduR_Tx2Lo.BswMPduRTransmitCallback' Reason: 'Callback Support is not active' */
#define PDUR_CANCELTRANSMITUSEDOFTX2LO                                                              STD_ON
#define PDUR_INVALIDHNDOFTX2LO                                                                      STD_OFF  /**< Deactivateable: 'PduR_Tx2Lo.InvalidHnd' Reason: 'the value of PduR_InvalidHndOfTx2Lo is always 'false' due to this, the array is deactivated.' */
#define PDUR_MASKEDBITSOFTX2LO                                                                      STD_ON
#define PDUR_RMSRCROMIDXOFTX2LO                                                                     STD_ON
#define PDUR_RMSRCROMUSEDOFTX2LO                                                                    STD_ON
#define PDUR_RMTRANSMITFCTPTRIDXOFTX2LO                                                             STD_ON
#define PDUR_TXIF2UP                                                                                STD_ON
#define PDUR_BSWMPDURTXCONFIRMATIONCALLBACKOFTXIF2UP                                                STD_OFF  /**< Deactivateable: 'PduR_TxIf2Up.BswMPduRTxConfirmationCallback' Reason: 'Callback Support is not active' */
#define PDUR_INVALIDHNDOFTXIF2UP                                                                    STD_OFF  /**< Deactivateable: 'PduR_TxIf2Up.InvalidHnd' Reason: 'the value of PduR_InvalidHndOfTxIf2Up is always 'false' due to this, the array is deactivated.' */
#define PDUR_RMGDESTROMIDXOFTXIF2UP                                                                 STD_ON
#define PDUR_TXCONFIRMATIONUSEDOFTXIF2UP                                                            STD_ON
#define PDUR_TXTP2SRC                                                                               STD_ON
#define PDUR_BSWMPDURTPTXCONFIRMATIONCALLBACKOFTXTP2SRC                                             STD_OFF  /**< Deactivateable: 'PduR_TxTp2Src.BswMPduRTpTxConfirmationCallback' Reason: 'Callback Support is not active' */
#define PDUR_INVALIDHNDOFTXTP2SRC                                                                   STD_OFF  /**< Deactivateable: 'PduR_TxTp2Src.InvalidHnd' Reason: 'the value of PduR_InvalidHndOfTxTp2Src is always 'false' due to this, the array is deactivated.' */
#define PDUR_RMGDESTROMIDXOFTXTP2SRC                                                                STD_ON
#define PDUR_PCCONFIG                                                                               STD_ON
#define PDUR_BMTXBUFFERARRAYRAMOFPCCONFIG                                                           STD_ON
#define PDUR_BMTXBUFFERINDROMOFPCCONFIG                                                             STD_ON
#define PDUR_BMTXBUFFERINSTANCERAMOFPCCONFIG                                                        STD_ON
#define PDUR_BMTXBUFFERINSTANCEROMOFPCCONFIG                                                        STD_ON
#define PDUR_BMTXBUFFERRAMOFPCCONFIG                                                                STD_ON
#define PDUR_BMTXBUFFERROMOFPCCONFIG                                                                STD_ON
#define PDUR_CONFIGIDOFPCCONFIG                                                                     STD_ON
#define PDUR_COREMANAGERROMOFPCCONFIG                                                               STD_ON
#define PDUR_EXCLUSIVEAREAROMOFPCCONFIG                                                             STD_ON
#define PDUR_FINALMAGICNUMBEROFPCCONFIG                                                             STD_OFF  /**< Deactivateable: 'PduR_PCConfig.FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define PDUR_FMFIFOELEMENTRAMOFPCCONFIG                                                             STD_ON
#define PDUR_FMFIFOINSTANCERAMOFPCCONFIG                                                            STD_ON
#define PDUR_FMFIFOINSTANCEROMOFPCCONFIG                                                            STD_ON
#define PDUR_FMFIFORAMOFPCCONFIG                                                                    STD_ON
#define PDUR_FMFIFOROMOFPCCONFIG                                                                    STD_ON
#define PDUR_FM_ACTIVATENEXT_FMSMSTATEHANDLEROFPCCONFIG                                             STD_ON
#define PDUR_FM_ACTIVATEREAD_FMSMSTATEHANDLEROFPCCONFIG                                             STD_ON
#define PDUR_FM_ACTIVATEWRITE_FMSMSTATEHANDLEROFPCCONFIG                                            STD_ON
#define PDUR_FM_FINISHREAD_FMSMSTATEHANDLEROFPCCONFIG                                               STD_ON
#define PDUR_FM_FINISHWRITE_FMSMSTATEHANDLEROFPCCONFIG                                              STD_ON
#define PDUR_GENERALPROPERTIESROMOFPCCONFIG                                                         STD_ON
#define PDUR_INITDATAHASHCODEOFPCCONFIG                                                             STD_OFF  /**< Deactivateable: 'PduR_PCConfig.InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define PDUR_INITIALIZEDOFPCCONFIG                                                                  STD_ON
#define PDUR_LOCKROMOFPCCONFIG                                                                      STD_ON
#define PDUR_MMROMINDOFPCCONFIG                                                                     STD_ON
#define PDUR_MMROMOFPCCONFIG                                                                        STD_ON
#define PDUR_RMBUFFEREDTPPROPERTIESRAMOFPCCONFIG                                                    STD_ON
#define PDUR_RMBUFFEREDTPPROPERTIESROMOFPCCONFIG                                                    STD_ON
#define PDUR_RMDESTROMOFPCCONFIG                                                                    STD_ON
#define PDUR_RMGDESTROMOFPCCONFIG                                                                   STD_ON
#define PDUR_RMGDESTTPTXSTATERAMOFPCCONFIG                                                          STD_ON
#define PDUR_RMSRCROMOFPCCONFIG                                                                     STD_ON
#define PDUR_RMTP_CANCELRECEIVE_TPRXSMSTATEHANDLEROFPCCONFIG                                        STD_ON
#define PDUR_RMTP_CHECKREADY2TRANSMIT_TPTXSMSTATEHANDLEROFPCCONFIG                                  STD_ON
#define PDUR_RMTP_COPYRXDATA_TPRXSMSTATEHANDLEROFPCCONFIG                                           STD_ON
#define PDUR_RMTP_FINISHRECEPTION_TPTXSMSTATEHANDLEROFPCCONFIG                                      STD_ON
#define PDUR_RMTP_FINISHTRANSMISSION_TPTXSMSTATEHANDLEROFPCCONFIG                                   STD_ON
#define PDUR_RMTP_STARTOFRECEPTION_TPRXSMSTATEHANDLEROFPCCONFIG                                     STD_ON
#define PDUR_RMTP_TPRXINDICATION_TPRXSMSTATEHANDLEROFPCCONFIG                                       STD_ON
#define PDUR_RMTRANSMITFCTPTROFPCCONFIG                                                             STD_ON
#define PDUR_RMTXINSTSMROMOFPCCONFIG                                                                STD_ON
#define PDUR_RXIF2DESTOFPCCONFIG                                                                    STD_ON
#define PDUR_RXTP2DESTOFPCCONFIG                                                                    STD_ON
#define PDUR_SIZEOFBMTXBUFFERARRAYRAMOFPCCONFIG                                                     STD_ON
#define PDUR_SIZEOFBMTXBUFFERINDROMOFPCCONFIG                                                       STD_ON
#define PDUR_SIZEOFBMTXBUFFERINSTANCERAMOFPCCONFIG                                                  STD_ON
#define PDUR_SIZEOFBMTXBUFFERINSTANCEROMOFPCCONFIG                                                  STD_ON
#define PDUR_SIZEOFBMTXBUFFERRAMOFPCCONFIG                                                          STD_ON
#define PDUR_SIZEOFBMTXBUFFERROMOFPCCONFIG                                                          STD_ON
#define PDUR_SIZEOFCOREMANAGERROMOFPCCONFIG                                                         STD_ON
#define PDUR_SIZEOFEXCLUSIVEAREAROMOFPCCONFIG                                                       STD_ON
#define PDUR_SIZEOFFMFIFOELEMENTRAMOFPCCONFIG                                                       STD_ON
#define PDUR_SIZEOFFMFIFOINSTANCERAMOFPCCONFIG                                                      STD_ON
#define PDUR_SIZEOFFMFIFOINSTANCEROMOFPCCONFIG                                                      STD_ON
#define PDUR_SIZEOFFMFIFORAMOFPCCONFIG                                                              STD_ON
#define PDUR_SIZEOFFMFIFOROMOFPCCONFIG                                                              STD_ON
#define PDUR_SIZEOFFM_ACTIVATENEXT_FMSMSTATEHANDLEROFPCCONFIG                                       STD_ON
#define PDUR_SIZEOFFM_ACTIVATEREAD_FMSMSTATEHANDLEROFPCCONFIG                                       STD_ON
#define PDUR_SIZEOFFM_ACTIVATEWRITE_FMSMSTATEHANDLEROFPCCONFIG                                      STD_ON
#define PDUR_SIZEOFFM_FINISHREAD_FMSMSTATEHANDLEROFPCCONFIG                                         STD_ON
#define PDUR_SIZEOFFM_FINISHWRITE_FMSMSTATEHANDLEROFPCCONFIG                                        STD_ON
#define PDUR_SIZEOFGENERALPROPERTIESROMOFPCCONFIG                                                   STD_ON
#define PDUR_SIZEOFLOCKROMOFPCCONFIG                                                                STD_ON
#define PDUR_SIZEOFMMROMINDOFPCCONFIG                                                               STD_ON
#define PDUR_SIZEOFMMROMOFPCCONFIG                                                                  STD_ON
#define PDUR_SIZEOFRMBUFFEREDTPPROPERTIESRAMOFPCCONFIG                                              STD_ON
#define PDUR_SIZEOFRMBUFFEREDTPPROPERTIESROMOFPCCONFIG                                              STD_ON
#define PDUR_SIZEOFRMDESTROMOFPCCONFIG                                                              STD_ON
#define PDUR_SIZEOFRMGDESTROMOFPCCONFIG                                                             STD_ON
#define PDUR_SIZEOFRMGDESTTPTXSTATERAMOFPCCONFIG                                                    STD_ON
#define PDUR_SIZEOFRMSRCROMOFPCCONFIG                                                               STD_ON
#define PDUR_SIZEOFRMTP_CANCELRECEIVE_TPRXSMSTATEHANDLEROFPCCONFIG                                  STD_ON
#define PDUR_SIZEOFRMTP_CHECKREADY2TRANSMIT_TPTXSMSTATEHANDLEROFPCCONFIG                            STD_ON
#define PDUR_SIZEOFRMTP_COPYRXDATA_TPRXSMSTATEHANDLEROFPCCONFIG                                     STD_ON
#define PDUR_SIZEOFRMTP_FINISHRECEPTION_TPTXSMSTATEHANDLEROFPCCONFIG                                STD_ON
#define PDUR_SIZEOFRMTP_FINISHTRANSMISSION_TPTXSMSTATEHANDLEROFPCCONFIG                             STD_ON
#define PDUR_SIZEOFRMTP_STARTOFRECEPTION_TPRXSMSTATEHANDLEROFPCCONFIG                               STD_ON
#define PDUR_SIZEOFRMTP_TPRXINDICATION_TPRXSMSTATEHANDLEROFPCCONFIG                                 STD_ON
#define PDUR_SIZEOFRMTRANSMITFCTPTROFPCCONFIG                                                       STD_ON
#define PDUR_SIZEOFRMTXINSTSMROMOFPCCONFIG                                                          STD_ON
#define PDUR_SIZEOFRXIF2DESTOFPCCONFIG                                                              STD_ON
#define PDUR_SIZEOFRXTP2DESTOFPCCONFIG                                                              STD_ON
#define PDUR_SIZEOFTX2LOOFPCCONFIG                                                                  STD_ON
#define PDUR_SIZEOFTXIF2UPOFPCCONFIG                                                                STD_ON
#define PDUR_SIZEOFTXTP2SRCOFPCCONFIG                                                               STD_ON
#define PDUR_TX2LOOFPCCONFIG                                                                        STD_ON
#define PDUR_TXIF2UPOFPCCONFIG                                                                      STD_ON
#define PDUR_TXTP2SRCOFPCCONFIG                                                                     STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCMinNumericValueDefines  PduR Min Numeric Value Defines (PRE_COMPILE)
  \brief  These defines are used to implement against the minimum value in numerical based data.
  \{
*/ 
#define PDUR_MIN_BMTXBUFFERARRAYRAM                                                                 0u
#define PDUR_MIN_BMTXBUFFERARRAYRAMREADIDXOFBMTXBUFFERINSTANCERAM                                   0u
#define PDUR_MIN_BMTXBUFFERARRAYRAMWRITEIDXOFBMTXBUFFERINSTANCERAM                                  0u
#define PDUR_MIN_BMTXBUFFERARRAYRAMINSTANCESTOPIDXOFBMTXBUFFERRAM                                   0u
#define PDUR_MIN_BMTXBUFFERARRAYRAMREADIDXOFBMTXBUFFERRAM                                           0u
#define PDUR_MIN_BMTXBUFFERARRAYRAMWRITEIDXOFBMTXBUFFERRAM                                          0u
#define PDUR_MIN_RXLENGTHOFBMTXBUFFERRAM                                                            0u
#define PDUR_MIN_BMTXBUFFERROMIDXOFFMFIFOELEMENTRAM                                                 0u
#define PDUR_MIN_RMDESTROMIDXOFFMFIFOELEMENTRAM                                                     0u
#define PDUR_MIN_BMTXBUFFERINSTANCEROMIDXOFFMFIFOINSTANCERAM                                        0u
#define PDUR_MIN_FILLLEVELOFFMFIFORAM                                                               0u
#define PDUR_MIN_FMFIFOELEMENTRAMREADIDXOFFMFIFORAM                                                 0u
#define PDUR_MIN_FMFIFOELEMENTRAMWRITEIDXOFFMFIFORAM                                                0u
#define PDUR_MIN_FMFIFOELEMENTRAMIDXOFRMBUFFEREDTPPROPERTIESRAM                                     0u
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCMaxNumericValueDefines  PduR Max Numeric Value Defines (PRE_COMPILE)
  \brief  These defines are used to implement against the maximum value in numerical based data.
  \{
*/ 
#define PDUR_MAX_BMTXBUFFERARRAYRAM                                                                 255u
#define PDUR_MAX_BMTXBUFFERARRAYRAMREADIDXOFBMTXBUFFERINSTANCERAM                                   4294967295u
#define PDUR_MAX_BMTXBUFFERARRAYRAMWRITEIDXOFBMTXBUFFERINSTANCERAM                                  4294967295u
#define PDUR_MAX_BMTXBUFFERARRAYRAMINSTANCESTOPIDXOFBMTXBUFFERRAM                                   4294967295u
#define PDUR_MAX_BMTXBUFFERARRAYRAMREADIDXOFBMTXBUFFERRAM                                           4294967295u
#define PDUR_MAX_BMTXBUFFERARRAYRAMWRITEIDXOFBMTXBUFFERRAM                                          4294967295u
#define PDUR_MAX_RXLENGTHOFBMTXBUFFERRAM                                                            65535u
#define PDUR_MAX_BMTXBUFFERROMIDXOFFMFIFOELEMENTRAM                                                 255u
#define PDUR_MAX_RMDESTROMIDXOFFMFIFOELEMENTRAM                                                     65535u
#define PDUR_MAX_BMTXBUFFERINSTANCEROMIDXOFFMFIFOINSTANCERAM                                        255u
#define PDUR_MAX_FILLLEVELOFFMFIFORAM                                                               65535u
#define PDUR_MAX_FMFIFOELEMENTRAMREADIDXOFFMFIFORAM                                                 255u
#define PDUR_MAX_FMFIFOELEMENTRAMWRITEIDXOFFMFIFORAM                                                255u
#define PDUR_MAX_FMFIFOELEMENTRAMIDXOFRMBUFFEREDTPPROPERTIESRAM                                     255u
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCNoReferenceDefines  PduR No Reference Defines (PRE_COMPILE)
  \brief  These defines are used to indicate unused indexes in data relations.
  \{
*/ 
#define PDUR_NO_MMROMINDENDIDXOFCOREMANAGERROM                                                      255u
#define PDUR_NO_MMROMINDSTARTIDXOFCOREMANAGERROM                                                    255u
#define PDUR_NO_BMTXBUFFERROMIDXOFFMFIFOELEMENTRAM                                                  255u
#define PDUR_NO_RMDESTROMIDXOFFMFIFOELEMENTRAM                                                      65535u
#define PDUR_NO_BMTXBUFFERINSTANCEROMIDXOFFMFIFOINSTANCERAM                                         255u
#define PDUR_NO_FMFIFOELEMENTRAMREADIDXOFFMFIFORAM                                                  255u
#define PDUR_NO_FMFIFOELEMENTRAMWRITEIDXOFFMFIFORAM                                                 255u
#define PDUR_NO_EXCLUSIVEAREAROMIDXOFLOCKROM                                                        255u
#define PDUR_NO_RMGDESTROMENDIDXOFMMROM                                                             65535u
#define PDUR_NO_RMGDESTROMSTARTIDXOFMMROM                                                           65535u
#define PDUR_NO_FMFIFOELEMENTRAMIDXOFRMBUFFEREDTPPROPERTIESRAM                                      255u
#define PDUR_NO_DESTHNDOFRMGDESTROM                                                                 255u
#define PDUR_NO_FMFIFOINSTANCEROMIDXOFRMGDESTROM                                                    255u
#define PDUR_NO_RMDESTROMIDXOFRMGDESTROM                                                            65535u
#define PDUR_NO_RMGDESTTPTXSTATERAMIDXOFRMGDESTROM                                                  255u
#define PDUR_NO_RMBUFFEREDTPPROPERTIESROMIDXOFRMSRCROM                                              255u
#define PDUR_NO_SRCHNDOFRMSRCROM                                                                    255u
#define PDUR_NO_RMSRCROMIDXOFRXTP2DEST                                                              65535u
#define PDUR_NO_RMSRCROMIDXOFTX2LO                                                                  65535u
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCEnumExistsDefines  PduR Enum Exists Defines (PRE_COMPILE)
  \brief  These defines can be used to deactivate enumeration based code sequences if the enumeration value does not exist in the configuration data.
  \{
*/ 
#define PDUR_EXISTS_SINGLE_BUFFER_QUEUE_IMPLEMENTATIONTYPEOFRMBUFFEREDIFPROPERTIESROM               STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.ImplementationType' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_EXISTS_SIMPLE_FIFO_QUEUE_IMPLEMENTATIONTYPEOFRMBUFFEREDIFPROPERTIESROM                 STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.ImplementationType' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_EXISTS_SHARED_FIFO_QUEUE_IMPLEMENTATIONTYPEOFRMBUFFEREDIFPROPERTIESROM                 STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.ImplementationType' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_EXISTS_DIRECT_QUEUEDATAPROVISIONTYPEOFRMBUFFEREDIFPROPERTIESROM                        STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.QueueDataProvisionType' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_EXISTS_TRIGGER_TRANSMIT_QUEUEDATAPROVISIONTYPEOFRMBUFFEREDIFPROPERTIESROM              STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.QueueDataProvisionType' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_EXISTS_SINGLE_BUFFER_QUEUETYPEOFRMBUFFEREDIFPROPERTIESROM                              STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.QueueType' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_EXISTS_FIFO_QUEUETYPEOFRMBUFFEREDIFPROPERTIESROM                                       STD_OFF  /**< Deactivateable: 'PduR_RmBufferedIfPropertiesRom.QueueType' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define PDUR_EXISTS_SHORTEN_PDULENGTHHANDLINGSTRATEGYOFRMDESTROM                                    STD_OFF  /**< Deactivateable: 'PduR_RmDestRom.PduLengthHandlingStrategy' Reason: 'the value of PduR_PduLengthHandlingStrategyOfRmDestRom is always '0' due to this, the array is deactivated.' */
#define PDUR_EXISTS_DISCARD_PDULENGTHHANDLINGSTRATEGYOFRMDESTROM                                    STD_OFF  /**< Deactivateable: 'PduR_RmDestRom.PduLengthHandlingStrategy' Reason: 'the value of PduR_PduLengthHandlingStrategyOfRmDestRom is always '0' due to this, the array is deactivated.' */
#define PDUR_EXISTS_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM                                 STD_ON
#define PDUR_EXISTS_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM                                 STD_ON
#define PDUR_EXISTS_IF_NOBUFFER_GATEWAY_ROUTINGTYPEOFRMDESTROM                                      STD_ON
#define PDUR_EXISTS_IF_BUFFERED_ROUTINGTYPEOFRMDESTROM                                              STD_OFF
#define PDUR_EXISTS_TP_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM                                 STD_ON
#define PDUR_EXISTS_TP_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM                                 STD_ON
#define PDUR_EXISTS_TP_BUFFERED_ROUTINGTYPEOFRMDESTROM                                              STD_ON
#define PDUR_EXISTS_RX_DIRECTIONOFRMGDESTROM                                                        STD_ON
#define PDUR_EXISTS_TX_DIRECTIONOFRMGDESTROM                                                        STD_ON
#define PDUR_EXISTS_DEFERRED_PDURDESTPDUPROCESSINGOFRMGDESTROM                                      STD_OFF  /**< /ActiveEcuC/PduR/PduRGeneral[0:PduREnableDeferredReception] and /ActiveEcuC/PduR/PduRGeneral[0:PduREnableDeferredTransmission] are both disabled. */
#define PDUR_EXISTS_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM                                     STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCEnumDefines  PduR Enum Defines (PRE_COMPILE)
  \brief  These defines are the enumeration values of enumeration based CONST and VAR data.
  \{
*/ 
#define PDUR_BUFFER_EMPTY_PDURBUFFERSTATEOFBMTXBUFFERINSTANCERAM                                    0x00u
#define PDUR_BUFFER_WRITE4READ_PDURBUFFERSTATEOFBMTXBUFFERINSTANCERAM                               0x01u
#define PDUR_BUFFER_READ4WRITE_PDURBUFFERSTATEOFBMTXBUFFERINSTANCERAM                               0x02u
#define PDUR_BUFFER_FULL_PDURBUFFERSTATEOFBMTXBUFFERINSTANCERAM                                     0x03u
#define PDUR_BUFFER_EMPTY_PDURBUFFERSTATEOFBMTXBUFFERRAM                                            0x00u
#define PDUR_BUFFER_WRITE4READ_PDURBUFFERSTATEOFBMTXBUFFERRAM                                       0x01u
#define PDUR_BUFFER_READ4WRITE_PDURBUFFERSTATEOFBMTXBUFFERRAM                                       0x02u
#define PDUR_BUFFER_FULL_PDURBUFFERSTATEOFBMTXBUFFERRAM                                             0x03u
#define PDUR_FM_IDLE_STATEOFFMFIFOELEMENTRAM                                                        0x00u
#define PDUR_FM_ALLOCATED_STATEOFFMFIFOELEMENTRAM                                                   0x01u
#define PDUR_FM_ALLOCATED_WITH_BUFFER_STATEOFFMFIFOELEMENTRAM                                       0x02u
#define PDUR_FM_WRITE_ACTIVE_STATEOFFMFIFOELEMENTRAM                                                0x03u
#define PDUR_FM_WRITE_FINISHED_READ_PENDING_STATEOFFMFIFOELEMENTRAM                                 0x04u
#define PDUR_FM_WRITE_READ_ACTIVE_STATEOFFMFIFOELEMENTRAM                                           0x05u
#define PDUR_FM_WRITE_FINISHED_READ_ACTIVE_STATEOFFMFIFOELEMENTRAM                                  0x06u
#define PDUR_FM_READ_FINISHED_STATEOFFMFIFOELEMENTRAM                                               0x07u
#define PDUR_RM_TX_IDLE_TPTXSMSTATEOFFMFIFORAM                                                      0x00u
#define PDUR_RM_TX_RECEPTION_ACTIVE_TPTXSMSTATEOFFMFIFORAM                                          0x01u
#define PDUR_RM_TX_TRANSMISSION_FINISHED_TPTXSMSTATEOFFMFIFORAM                                     0x02u
#define PDUR_RM_TX_RECEPTION_TRANSMISSION_ACTIVE_TPTXSMSTATEOFFMFIFORAM                             0x03u
#define PDUR_RM_TX_RECEPTION_FINISHED_TRANSMISSION_ACTIVE_TPTXSMSTATEOFFMFIFORAM                    0x04u
#define PDUR_RM_RX_IDLE_TPRXSMSTATEOFRMBUFFEREDTPPROPERTIESRAM                                      0x00u
#define PDUR_RM_RX_ACTIVE_TPRXSMSTATEOFRMBUFFEREDTPPROPERTIESRAM                                    0x01u
#define PDUR_RM_RX_ABORTED_TPRXSMSTATEOFRMBUFFEREDTPPROPERTIESRAM                                   0x02u
#define PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM                                        0x00u
#define PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM                                        0x01u
#define PDUR_IF_NOBUFFER_GATEWAY_ROUTINGTYPEOFRMDESTROM                                             0x02u
#define PDUR_TP_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM                                        0x04u
#define PDUR_TP_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM                                        0x05u
#define PDUR_TP_BUFFERED_ROUTINGTYPEOFRMDESTROM                                                     0x06u
#define PDUR_RX_DIRECTIONOFRMGDESTROM                                                               0x00u
#define PDUR_TX_DIRECTIONOFRMGDESTROM                                                               0x01u
#define PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM                                            0x01u
#define PDUR_RM_TXINST_IDLE_TPTXINSTSMSTATEOFRMGDESTTPTXSTATERAM                                    0x00u
#define PDUR_RM_TXINST_ACTIVE_TPTXINSTSMSTATEOFRMGDESTTPTXSTATERAM                                  0x01u
#define PDUR_RM_TXINST_WAITING_TPTXINSTSMSTATEOFRMGDESTTPTXSTATERAM                                 0x02u
#define PDUR_RM_TXINST_ABORTED_TPTXINSTSMSTATEOFRMGDESTTPTXSTATERAM                                 0x03u
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCMaskedBitDefines  PduR Masked Bit Defines (PRE_COMPILE)
  \brief  These defines are masks to extract packed boolean data.
  \{
*/ 
#define PDUR_IFCANCELTRANSMITSUPPORTEDOFMMROM_MASK                                                  0x40u
#define PDUR_LOIFOFMMROM_MASK                                                                       0x20u
#define PDUR_LOTPOFMMROM_MASK                                                                       0x10u
#define PDUR_RMGDESTROMUSEDOFMMROM_MASK                                                             0x08u
#define PDUR_TPCANCELTRANSMITSUPPORTEDOFMMROM_MASK                                                  0x04u
#define PDUR_UPIFOFMMROM_MASK                                                                       0x02u
#define PDUR_UPTPOFMMROM_MASK                                                                       0x01u
#define PDUR_FMFIFOINSTANCEROMUSEDOFRMGDESTROM_MASK                                                 0x04u
#define PDUR_RMDESTROMUSEDOFRMGDESTROM_MASK                                                         0x02u
#define PDUR_RMGDESTTPTXSTATERAMUSEDOFRMGDESTROM_MASK                                               0x01u
#define PDUR_RMBUFFEREDTPPROPERTIESROMUSEDOFRMSRCROM_MASK                                           0x04u
#define PDUR_TRIGGERTRANSMITSUPPORTEDOFRMSRCROM_MASK                                                0x02u
#define PDUR_TXCONFIRMATIONSUPPORTEDOFRMSRCROM_MASK                                                 0x01u
#define PDUR_CANCELTRANSMITUSEDOFTX2LO_MASK                                                         0x02u
#define PDUR_RMSRCROMUSEDOFTX2LO_MASK                                                               0x01u
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCIsReducedToDefineDefines  PduR Is Reduced To Define Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define is STD_ON else STD_OFF.
  \{
*/ 
#define PDUR_ISDEF_BMTXBUFFERROMIDXOFBMTXBUFFERINDROM                                               STD_OFF
#define PDUR_ISDEF_BMTXBUFFERROMIDXOFBMTXBUFFERINSTANCEROM                                          STD_OFF
#define PDUR_ISDEF_BMTXBUFFERARRAYRAMENDIDXOFBMTXBUFFERROM                                          STD_OFF
#define PDUR_ISDEF_BMTXBUFFERARRAYRAMLENGTHOFBMTXBUFFERROM                                          STD_OFF
#define PDUR_ISDEF_BMTXBUFFERARRAYRAMSTARTIDXOFBMTXBUFFERROM                                        STD_OFF
#define PDUR_ISDEF_BMTXBUFFERINSTANCEROMENDIDXOFBMTXBUFFERROM                                       STD_OFF
#define PDUR_ISDEF_BMTXBUFFERINSTANCEROMSTARTIDXOFBMTXBUFFERROM                                     STD_OFF
#define PDUR_ISDEF_MMROMINDENDIDXOFCOREMANAGERROM                                                   STD_OFF
#define PDUR_ISDEF_MMROMINDSTARTIDXOFCOREMANAGERROM                                                 STD_OFF
#define PDUR_ISDEF_MMROMINDUSEDOFCOREMANAGERROM                                                     STD_OFF
#define PDUR_ISDEF_LOCKOFEXCLUSIVEAREAROM                                                           STD_OFF
#define PDUR_ISDEF_UNLOCKOFEXCLUSIVEAREAROM                                                         STD_OFF
#define PDUR_ISDEF_FMFIFOROMIDXOFFMFIFOINSTANCEROM                                                  STD_OFF
#define PDUR_ISDEF_BMTXBUFFERINDROMENDIDXOFFMFIFOROM                                                STD_OFF
#define PDUR_ISDEF_BMTXBUFFERINDROMLENGTHOFFMFIFOROM                                                STD_OFF
#define PDUR_ISDEF_BMTXBUFFERINDROMSTARTIDXOFFMFIFOROM                                              STD_OFF
#define PDUR_ISDEF_FMFIFOELEMENTRAMENDIDXOFFMFIFOROM                                                STD_OFF
#define PDUR_ISDEF_FMFIFOELEMENTRAMLENGTHOFFMFIFOROM                                                STD_OFF
#define PDUR_ISDEF_FMFIFOELEMENTRAMSTARTIDXOFFMFIFOROM                                              STD_OFF
#define PDUR_ISDEF_FCTPTROFFM_ACTIVATENEXT_FMSMSTATEHANDLER                                         STD_OFF
#define PDUR_ISDEF_FCTPTROFFM_ACTIVATEREAD_FMSMSTATEHANDLER                                         STD_OFF
#define PDUR_ISDEF_FCTPTROFFM_ACTIVATEWRITE_FMSMSTATEHANDLER                                        STD_OFF
#define PDUR_ISDEF_FCTPTROFFM_FINISHREAD_FMSMSTATEHANDLER                                           STD_OFF
#define PDUR_ISDEF_FCTPTROFFM_FINISHWRITE_FMSMSTATEHANDLER                                          STD_OFF
#define PDUR_ISDEF_HASTPTXBUFFEREDFORWARDINGOFGENERALPROPERTIESROM                                  STD_OFF
#define PDUR_ISDEF_EXCLUSIVEAREAROMIDXOFLOCKROM                                                     STD_OFF
#define PDUR_ISDEF_EXCLUSIVEAREAROMUSEDOFLOCKROM                                                    STD_OFF
#define PDUR_ISDEF_COREMANAGERROMIDXOFMMROM                                                         STD_OFF
#define PDUR_ISDEF_IFCANCELTRANSMITSUPPORTEDOFMMROM                                                 STD_OFF
#define PDUR_ISDEF_LOIFCANCELTRANSMITFCTPTROFMMROM                                                  STD_OFF
#define PDUR_ISDEF_LOIFOFMMROM                                                                      STD_OFF
#define PDUR_ISDEF_LOIFTRANSMITFCTPTROFMMROM                                                        STD_OFF
#define PDUR_ISDEF_LOTPCANCELTRANSMITFCTPTROFMMROM                                                  STD_OFF
#define PDUR_ISDEF_LOTPOFMMROM                                                                      STD_OFF
#define PDUR_ISDEF_LOTPTRANSMITFCTPTROFMMROM                                                        STD_OFF
#define PDUR_ISDEF_MASKEDBITSOFMMROM                                                                STD_OFF
#define PDUR_ISDEF_RMGDESTROMENDIDXOFMMROM                                                          STD_OFF
#define PDUR_ISDEF_RMGDESTROMSTARTIDXOFMMROM                                                        STD_OFF
#define PDUR_ISDEF_RMGDESTROMUSEDOFMMROM                                                            STD_OFF
#define PDUR_ISDEF_TPCANCELTRANSMITSUPPORTEDOFMMROM                                                 STD_OFF
#define PDUR_ISDEF_UPIFOFMMROM                                                                      STD_OFF
#define PDUR_ISDEF_UPIFRXINDICATIONFCTPTROFMMROM                                                    STD_OFF
#define PDUR_ISDEF_UPIFTRIGGERTRANSMITFCTPTROFMMROM                                                 STD_OFF
#define PDUR_ISDEF_UPIFTXCONFIRMATIONFCTPTROFMMROM                                                  STD_OFF
#define PDUR_ISDEF_UPTPCOPYRXDATAFCTPTROFMMROM                                                      STD_OFF
#define PDUR_ISDEF_UPTPCOPYTXDATAFCTPTROFMMROM                                                      STD_OFF
#define PDUR_ISDEF_UPTPOFMMROM                                                                      STD_OFF
#define PDUR_ISDEF_UPTPSTARTOFRECEPTIONFCTPTROFMMROM                                                STD_OFF
#define PDUR_ISDEF_UPTPTPRXINDICATIONFCTPTROFMMROM                                                  STD_OFF
#define PDUR_ISDEF_UPTPTPTXCONFIRMATIONFCTPTROFMMROM                                                STD_OFF
#define PDUR_ISDEF_MMROMIND                                                                         STD_OFF
#define PDUR_ISDEF_DEDICATEDTXBUFFEROFRMBUFFEREDTPPROPERTIESROM                                     STD_OFF
#define PDUR_ISDEF_FMFIFOROMIDXOFRMBUFFEREDTPPROPERTIESROM                                          STD_OFF
#define PDUR_ISDEF_QUEUEDDESTCNTOFRMBUFFEREDTPPROPERTIESROM                                         STD_OFF
#define PDUR_ISDEF_TPTHRESHOLDOFRMBUFFEREDTPPROPERTIESROM                                           STD_OFF
#define PDUR_ISDEF_RMGDESTROMIDXOFRMDESTROM                                                         STD_OFF
#define PDUR_ISDEF_RMSRCROMIDXOFRMDESTROM                                                           STD_OFF
#define PDUR_ISDEF_ROUTINGTYPEOFRMDESTROM                                                           STD_OFF
#define PDUR_ISDEF_DESTHNDOFRMGDESTROM                                                              STD_OFF
#define PDUR_ISDEF_DIRECTIONOFRMGDESTROM                                                            STD_OFF
#define PDUR_ISDEF_FMFIFOINSTANCEROMIDXOFRMGDESTROM                                                 STD_OFF
#define PDUR_ISDEF_FMFIFOINSTANCEROMUSEDOFRMGDESTROM                                                STD_OFF
#define PDUR_ISDEF_LOCKROMIDXOFRMGDESTROM                                                           STD_OFF
#define PDUR_ISDEF_MASKEDBITSOFRMGDESTROM                                                           STD_OFF
#define PDUR_ISDEF_MMROMIDXOFRMGDESTROM                                                             STD_OFF
#define PDUR_ISDEF_PDURDESTPDUPROCESSINGOFRMGDESTROM                                                STD_OFF
#define PDUR_ISDEF_RMDESTROMIDXOFRMGDESTROM                                                         STD_OFF
#define PDUR_ISDEF_RMDESTROMUSEDOFRMGDESTROM                                                        STD_OFF
#define PDUR_ISDEF_RMGDESTTPTXSTATERAMIDXOFRMGDESTROM                                               STD_OFF
#define PDUR_ISDEF_RMGDESTTPTXSTATERAMUSEDOFRMGDESTROM                                              STD_OFF
#define PDUR_ISDEF_LOCKROMIDXOFRMSRCROM                                                             STD_OFF
#define PDUR_ISDEF_MASKEDBITSOFRMSRCROM                                                             STD_OFF
#define PDUR_ISDEF_MMROMIDXOFRMSRCROM                                                               STD_OFF
#define PDUR_ISDEF_RMBUFFEREDTPPROPERTIESROMIDXOFRMSRCROM                                           STD_OFF
#define PDUR_ISDEF_RMBUFFEREDTPPROPERTIESROMUSEDOFRMSRCROM                                          STD_OFF
#define PDUR_ISDEF_RMDESTROMENDIDXOFRMSRCROM                                                        STD_OFF
#define PDUR_ISDEF_RMDESTROMLENGTHOFRMSRCROM                                                        STD_OFF
#define PDUR_ISDEF_RMDESTROMSTARTIDXOFRMSRCROM                                                      STD_OFF
#define PDUR_ISDEF_SRCHNDOFRMSRCROM                                                                 STD_OFF
#define PDUR_ISDEF_TRIGGERTRANSMITSUPPORTEDOFRMSRCROM                                               STD_OFF
#define PDUR_ISDEF_TXCONFIRMATIONSUPPORTEDOFRMSRCROM                                                STD_OFF
#define PDUR_ISDEF_FCTPTROFRMTP_CANCELRECEIVE_TPRXSMSTATEHANDLER                                    STD_OFF
#define PDUR_ISDEF_FCTPTROFRMTP_CHECKREADY2TRANSMIT_TPTXSMSTATEHANDLER                              STD_OFF
#define PDUR_ISDEF_FCTPTROFRMTP_COPYRXDATA_TPRXSMSTATEHANDLER                                       STD_OFF
#define PDUR_ISDEF_FCTPTROFRMTP_FINISHRECEPTION_TPTXSMSTATEHANDLER                                  STD_OFF
#define PDUR_ISDEF_FCTPTROFRMTP_FINISHTRANSMISSION_TPTXSMSTATEHANDLER                               STD_OFF
#define PDUR_ISDEF_FCTPTROFRMTP_STARTOFRECEPTION_TPRXSMSTATEHANDLER                                 STD_OFF
#define PDUR_ISDEF_FCTPTROFRMTP_TPRXINDICATION_TPRXSMSTATEHANDLER                                   STD_OFF
#define PDUR_ISDEF_RMTRANSMITFCTPTR                                                                 STD_OFF
#define PDUR_ISDEF_PDUR_RMTP_TXINST_CANCELTRANSMITOFRMTXINSTSMROM                                   STD_OFF
#define PDUR_ISDEF_PDUR_RMTP_TXINST_COPYTXDATAOFRMTXINSTSMROM                                       STD_OFF
#define PDUR_ISDEF_PDUR_RMTP_TXINST_TRIGGERTRANSMITOFRMTXINSTSMROM                                  STD_OFF
#define PDUR_ISDEF_PDUR_RMTP_TXINST_TXCONFIRMATIONOFRMTXINSTSMROM                                   STD_OFF
#define PDUR_ISDEF_RMSRCROMIDXOFRXIF2DEST                                                           STD_OFF
#define PDUR_ISDEF_RMSRCROMIDXOFRXTP2DEST                                                           STD_OFF
#define PDUR_ISDEF_RMSRCROMUSEDOFRXTP2DEST                                                          STD_OFF
#define PDUR_ISDEF_CANCELTRANSMITUSEDOFTX2LO                                                        STD_OFF
#define PDUR_ISDEF_MASKEDBITSOFTX2LO                                                                STD_OFF
#define PDUR_ISDEF_RMSRCROMIDXOFTX2LO                                                               STD_OFF
#define PDUR_ISDEF_RMSRCROMUSEDOFTX2LO                                                              STD_OFF
#define PDUR_ISDEF_RMTRANSMITFCTPTRIDXOFTX2LO                                                       STD_OFF
#define PDUR_ISDEF_RMGDESTROMIDXOFTXIF2UP                                                           STD_OFF
#define PDUR_ISDEF_TXCONFIRMATIONUSEDOFTXIF2UP                                                      STD_OFF
#define PDUR_ISDEF_RMGDESTROMIDXOFTXTP2SRC                                                          STD_OFF
#define PDUR_ISDEF_BMTXBUFFERARRAYRAMOFPCCONFIG                                                     STD_ON
#define PDUR_ISDEF_BMTXBUFFERINDROMOFPCCONFIG                                                       STD_ON
#define PDUR_ISDEF_BMTXBUFFERINSTANCERAMOFPCCONFIG                                                  STD_ON
#define PDUR_ISDEF_BMTXBUFFERINSTANCEROMOFPCCONFIG                                                  STD_ON
#define PDUR_ISDEF_BMTXBUFFERRAMOFPCCONFIG                                                          STD_ON
#define PDUR_ISDEF_BMTXBUFFERROMOFPCCONFIG                                                          STD_ON
#define PDUR_ISDEF_COREMANAGERROMOFPCCONFIG                                                         STD_ON
#define PDUR_ISDEF_EXCLUSIVEAREAROMOFPCCONFIG                                                       STD_ON
#define PDUR_ISDEF_FMFIFOELEMENTRAMOFPCCONFIG                                                       STD_ON
#define PDUR_ISDEF_FMFIFOINSTANCERAMOFPCCONFIG                                                      STD_ON
#define PDUR_ISDEF_FMFIFOINSTANCEROMOFPCCONFIG                                                      STD_ON
#define PDUR_ISDEF_FMFIFORAMOFPCCONFIG                                                              STD_ON
#define PDUR_ISDEF_FMFIFOROMOFPCCONFIG                                                              STD_ON
#define PDUR_ISDEF_FM_ACTIVATENEXT_FMSMSTATEHANDLEROFPCCONFIG                                       STD_ON
#define PDUR_ISDEF_FM_ACTIVATEREAD_FMSMSTATEHANDLEROFPCCONFIG                                       STD_ON
#define PDUR_ISDEF_FM_ACTIVATEWRITE_FMSMSTATEHANDLEROFPCCONFIG                                      STD_ON
#define PDUR_ISDEF_FM_FINISHREAD_FMSMSTATEHANDLEROFPCCONFIG                                         STD_ON
#define PDUR_ISDEF_FM_FINISHWRITE_FMSMSTATEHANDLEROFPCCONFIG                                        STD_ON
#define PDUR_ISDEF_GENERALPROPERTIESROMOFPCCONFIG                                                   STD_ON
#define PDUR_ISDEF_INITIALIZEDOFPCCONFIG                                                            STD_ON
#define PDUR_ISDEF_LOCKROMOFPCCONFIG                                                                STD_ON
#define PDUR_ISDEF_MMROMINDOFPCCONFIG                                                               STD_ON
#define PDUR_ISDEF_MMROMOFPCCONFIG                                                                  STD_ON
#define PDUR_ISDEF_RMBUFFEREDTPPROPERTIESRAMOFPCCONFIG                                              STD_ON
#define PDUR_ISDEF_RMBUFFEREDTPPROPERTIESROMOFPCCONFIG                                              STD_ON
#define PDUR_ISDEF_RMDESTROMOFPCCONFIG                                                              STD_ON
#define PDUR_ISDEF_RMGDESTROMOFPCCONFIG                                                             STD_ON
#define PDUR_ISDEF_RMGDESTTPTXSTATERAMOFPCCONFIG                                                    STD_ON
#define PDUR_ISDEF_RMSRCROMOFPCCONFIG                                                               STD_ON
#define PDUR_ISDEF_RMTP_CANCELRECEIVE_TPRXSMSTATEHANDLEROFPCCONFIG                                  STD_ON
#define PDUR_ISDEF_RMTP_CHECKREADY2TRANSMIT_TPTXSMSTATEHANDLEROFPCCONFIG                            STD_ON
#define PDUR_ISDEF_RMTP_COPYRXDATA_TPRXSMSTATEHANDLEROFPCCONFIG                                     STD_ON
#define PDUR_ISDEF_RMTP_FINISHRECEPTION_TPTXSMSTATEHANDLEROFPCCONFIG                                STD_ON
#define PDUR_ISDEF_RMTP_FINISHTRANSMISSION_TPTXSMSTATEHANDLEROFPCCONFIG                             STD_ON
#define PDUR_ISDEF_RMTP_STARTOFRECEPTION_TPRXSMSTATEHANDLEROFPCCONFIG                               STD_ON
#define PDUR_ISDEF_RMTP_TPRXINDICATION_TPRXSMSTATEHANDLEROFPCCONFIG                                 STD_ON
#define PDUR_ISDEF_RMTRANSMITFCTPTROFPCCONFIG                                                       STD_ON
#define PDUR_ISDEF_RMTXINSTSMROMOFPCCONFIG                                                          STD_ON
#define PDUR_ISDEF_RXIF2DESTOFPCCONFIG                                                              STD_ON
#define PDUR_ISDEF_RXTP2DESTOFPCCONFIG                                                              STD_ON
#define PDUR_ISDEF_TX2LOOFPCCONFIG                                                                  STD_ON
#define PDUR_ISDEF_TXIF2UPOFPCCONFIG                                                                STD_ON
#define PDUR_ISDEF_TXTP2SRCOFPCCONFIG                                                               STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCEqualsAlwaysToDefines  PduR Equals Always To Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define contains the always equals value.
  \{
*/ 
#define PDUR_EQ2_BMTXBUFFERROMIDXOFBMTXBUFFERINDROM                                                 
#define PDUR_EQ2_BMTXBUFFERROMIDXOFBMTXBUFFERINSTANCEROM                                            
#define PDUR_EQ2_BMTXBUFFERARRAYRAMENDIDXOFBMTXBUFFERROM                                            
#define PDUR_EQ2_BMTXBUFFERARRAYRAMLENGTHOFBMTXBUFFERROM                                            
#define PDUR_EQ2_BMTXBUFFERARRAYRAMSTARTIDXOFBMTXBUFFERROM                                          
#define PDUR_EQ2_BMTXBUFFERINSTANCEROMENDIDXOFBMTXBUFFERROM                                         
#define PDUR_EQ2_BMTXBUFFERINSTANCEROMSTARTIDXOFBMTXBUFFERROM                                       
#define PDUR_EQ2_MMROMINDENDIDXOFCOREMANAGERROM                                                     
#define PDUR_EQ2_MMROMINDSTARTIDXOFCOREMANAGERROM                                                   
#define PDUR_EQ2_MMROMINDUSEDOFCOREMANAGERROM                                                       
#define PDUR_EQ2_LOCKOFEXCLUSIVEAREAROM                                                             
#define PDUR_EQ2_UNLOCKOFEXCLUSIVEAREAROM                                                           
#define PDUR_EQ2_FMFIFOROMIDXOFFMFIFOINSTANCEROM                                                    
#define PDUR_EQ2_BMTXBUFFERINDROMENDIDXOFFMFIFOROM                                                  
#define PDUR_EQ2_BMTXBUFFERINDROMLENGTHOFFMFIFOROM                                                  
#define PDUR_EQ2_BMTXBUFFERINDROMSTARTIDXOFFMFIFOROM                                                
#define PDUR_EQ2_FMFIFOELEMENTRAMENDIDXOFFMFIFOROM                                                  
#define PDUR_EQ2_FMFIFOELEMENTRAMLENGTHOFFMFIFOROM                                                  
#define PDUR_EQ2_FMFIFOELEMENTRAMSTARTIDXOFFMFIFOROM                                                
#define PDUR_EQ2_FCTPTROFFM_ACTIVATENEXT_FMSMSTATEHANDLER                                           
#define PDUR_EQ2_FCTPTROFFM_ACTIVATEREAD_FMSMSTATEHANDLER                                           
#define PDUR_EQ2_FCTPTROFFM_ACTIVATEWRITE_FMSMSTATEHANDLER                                          
#define PDUR_EQ2_FCTPTROFFM_FINISHREAD_FMSMSTATEHANDLER                                             
#define PDUR_EQ2_FCTPTROFFM_FINISHWRITE_FMSMSTATEHANDLER                                            
#define PDUR_EQ2_HASTPTXBUFFEREDFORWARDINGOFGENERALPROPERTIESROM                                    
#define PDUR_EQ2_EXCLUSIVEAREAROMIDXOFLOCKROM                                                       
#define PDUR_EQ2_EXCLUSIVEAREAROMUSEDOFLOCKROM                                                      
#define PDUR_EQ2_COREMANAGERROMIDXOFMMROM                                                           
#define PDUR_EQ2_IFCANCELTRANSMITSUPPORTEDOFMMROM                                                   
#define PDUR_EQ2_LOIFCANCELTRANSMITFCTPTROFMMROM                                                    
#define PDUR_EQ2_LOIFOFMMROM                                                                        
#define PDUR_EQ2_LOIFTRANSMITFCTPTROFMMROM                                                          
#define PDUR_EQ2_LOTPCANCELTRANSMITFCTPTROFMMROM                                                    
#define PDUR_EQ2_LOTPOFMMROM                                                                        
#define PDUR_EQ2_LOTPTRANSMITFCTPTROFMMROM                                                          
#define PDUR_EQ2_MASKEDBITSOFMMROM                                                                  
#define PDUR_EQ2_RMGDESTROMENDIDXOFMMROM                                                            
#define PDUR_EQ2_RMGDESTROMSTARTIDXOFMMROM                                                          
#define PDUR_EQ2_RMGDESTROMUSEDOFMMROM                                                              
#define PDUR_EQ2_TPCANCELTRANSMITSUPPORTEDOFMMROM                                                   
#define PDUR_EQ2_UPIFOFMMROM                                                                        
#define PDUR_EQ2_UPIFRXINDICATIONFCTPTROFMMROM                                                      
#define PDUR_EQ2_UPIFTRIGGERTRANSMITFCTPTROFMMROM                                                   
#define PDUR_EQ2_UPIFTXCONFIRMATIONFCTPTROFMMROM                                                    
#define PDUR_EQ2_UPTPCOPYRXDATAFCTPTROFMMROM                                                        
#define PDUR_EQ2_UPTPCOPYTXDATAFCTPTROFMMROM                                                        
#define PDUR_EQ2_UPTPOFMMROM                                                                        
#define PDUR_EQ2_UPTPSTARTOFRECEPTIONFCTPTROFMMROM                                                  
#define PDUR_EQ2_UPTPTPRXINDICATIONFCTPTROFMMROM                                                    
#define PDUR_EQ2_UPTPTPTXCONFIRMATIONFCTPTROFMMROM                                                  
#define PDUR_EQ2_MMROMIND                                                                           
#define PDUR_EQ2_DEDICATEDTXBUFFEROFRMBUFFEREDTPPROPERTIESROM                                       
#define PDUR_EQ2_FMFIFOROMIDXOFRMBUFFEREDTPPROPERTIESROM                                            
#define PDUR_EQ2_QUEUEDDESTCNTOFRMBUFFEREDTPPROPERTIESROM                                           
#define PDUR_EQ2_TPTHRESHOLDOFRMBUFFEREDTPPROPERTIESROM                                             
#define PDUR_EQ2_RMGDESTROMIDXOFRMDESTROM                                                           
#define PDUR_EQ2_RMSRCROMIDXOFRMDESTROM                                                             
#define PDUR_EQ2_ROUTINGTYPEOFRMDESTROM                                                             
#define PDUR_EQ2_DESTHNDOFRMGDESTROM                                                                
#define PDUR_EQ2_DIRECTIONOFRMGDESTROM                                                              
#define PDUR_EQ2_FMFIFOINSTANCEROMIDXOFRMGDESTROM                                                   
#define PDUR_EQ2_FMFIFOINSTANCEROMUSEDOFRMGDESTROM                                                  
#define PDUR_EQ2_LOCKROMIDXOFRMGDESTROM                                                             
#define PDUR_EQ2_MASKEDBITSOFRMGDESTROM                                                             
#define PDUR_EQ2_MMROMIDXOFRMGDESTROM                                                               
#define PDUR_EQ2_PDURDESTPDUPROCESSINGOFRMGDESTROM                                                  
#define PDUR_EQ2_RMDESTROMIDXOFRMGDESTROM                                                           
#define PDUR_EQ2_RMDESTROMUSEDOFRMGDESTROM                                                          
#define PDUR_EQ2_RMGDESTTPTXSTATERAMIDXOFRMGDESTROM                                                 
#define PDUR_EQ2_RMGDESTTPTXSTATERAMUSEDOFRMGDESTROM                                                
#define PDUR_EQ2_LOCKROMIDXOFRMSRCROM                                                               
#define PDUR_EQ2_MASKEDBITSOFRMSRCROM                                                               
#define PDUR_EQ2_MMROMIDXOFRMSRCROM                                                                 
#define PDUR_EQ2_RMBUFFEREDTPPROPERTIESROMIDXOFRMSRCROM                                             
#define PDUR_EQ2_RMBUFFEREDTPPROPERTIESROMUSEDOFRMSRCROM                                            
#define PDUR_EQ2_RMDESTROMENDIDXOFRMSRCROM                                                          
#define PDUR_EQ2_RMDESTROMLENGTHOFRMSRCROM                                                          
#define PDUR_EQ2_RMDESTROMSTARTIDXOFRMSRCROM                                                        
#define PDUR_EQ2_SRCHNDOFRMSRCROM                                                                   
#define PDUR_EQ2_TRIGGERTRANSMITSUPPORTEDOFRMSRCROM                                                 
#define PDUR_EQ2_TXCONFIRMATIONSUPPORTEDOFRMSRCROM                                                  
#define PDUR_EQ2_FCTPTROFRMTP_CANCELRECEIVE_TPRXSMSTATEHANDLER                                      
#define PDUR_EQ2_FCTPTROFRMTP_CHECKREADY2TRANSMIT_TPTXSMSTATEHANDLER                                
#define PDUR_EQ2_FCTPTROFRMTP_COPYRXDATA_TPRXSMSTATEHANDLER                                         
#define PDUR_EQ2_FCTPTROFRMTP_FINISHRECEPTION_TPTXSMSTATEHANDLER                                    
#define PDUR_EQ2_FCTPTROFRMTP_FINISHTRANSMISSION_TPTXSMSTATEHANDLER                                 
#define PDUR_EQ2_FCTPTROFRMTP_STARTOFRECEPTION_TPRXSMSTATEHANDLER                                   
#define PDUR_EQ2_FCTPTROFRMTP_TPRXINDICATION_TPRXSMSTATEHANDLER                                     
#define PDUR_EQ2_RMTRANSMITFCTPTR                                                                   
#define PDUR_EQ2_PDUR_RMTP_TXINST_CANCELTRANSMITOFRMTXINSTSMROM                                     
#define PDUR_EQ2_PDUR_RMTP_TXINST_COPYTXDATAOFRMTXINSTSMROM                                         
#define PDUR_EQ2_PDUR_RMTP_TXINST_TRIGGERTRANSMITOFRMTXINSTSMROM                                    
#define PDUR_EQ2_PDUR_RMTP_TXINST_TXCONFIRMATIONOFRMTXINSTSMROM                                     
#define PDUR_EQ2_RMSRCROMIDXOFRXIF2DEST                                                             
#define PDUR_EQ2_RMSRCROMIDXOFRXTP2DEST                                                             
#define PDUR_EQ2_RMSRCROMUSEDOFRXTP2DEST                                                            
#define PDUR_EQ2_CANCELTRANSMITUSEDOFTX2LO                                                          
#define PDUR_EQ2_MASKEDBITSOFTX2LO                                                                  
#define PDUR_EQ2_RMSRCROMIDXOFTX2LO                                                                 
#define PDUR_EQ2_RMSRCROMUSEDOFTX2LO                                                                
#define PDUR_EQ2_RMTRANSMITFCTPTRIDXOFTX2LO                                                         
#define PDUR_EQ2_RMGDESTROMIDXOFTXIF2UP                                                             
#define PDUR_EQ2_TXCONFIRMATIONUSEDOFTXIF2UP                                                        
#define PDUR_EQ2_RMGDESTROMIDXOFTXTP2SRC                                                            
#define PDUR_EQ2_BMTXBUFFERARRAYRAMOFPCCONFIG                                                       PduR_BmTxBufferArrayRam.raw
#define PDUR_EQ2_BMTXBUFFERINDROMOFPCCONFIG                                                         PduR_BmTxBufferIndRom
#define PDUR_EQ2_BMTXBUFFERINSTANCERAMOFPCCONFIG                                                    PduR_BmTxBufferInstanceRam.raw
#define PDUR_EQ2_BMTXBUFFERINSTANCEROMOFPCCONFIG                                                    PduR_BmTxBufferInstanceRom
#define PDUR_EQ2_BMTXBUFFERRAMOFPCCONFIG                                                            PduR_BmTxBufferRam.raw
#define PDUR_EQ2_BMTXBUFFERROMOFPCCONFIG                                                            PduR_BmTxBufferRom
#define PDUR_EQ2_COREMANAGERROMOFPCCONFIG                                                           PduR_CoreManagerRom
#define PDUR_EQ2_EXCLUSIVEAREAROMOFPCCONFIG                                                         PduR_ExclusiveAreaRom
#define PDUR_EQ2_FMFIFOELEMENTRAMOFPCCONFIG                                                         PduR_FmFifoElementRam.raw
#define PDUR_EQ2_FMFIFOINSTANCERAMOFPCCONFIG                                                        PduR_FmFifoInstanceRam.raw
#define PDUR_EQ2_FMFIFOINSTANCEROMOFPCCONFIG                                                        PduR_FmFifoInstanceRom
#define PDUR_EQ2_FMFIFORAMOFPCCONFIG                                                                PduR_FmFifoRam.raw
#define PDUR_EQ2_FMFIFOROMOFPCCONFIG                                                                PduR_FmFifoRom
#define PDUR_EQ2_FM_ACTIVATENEXT_FMSMSTATEHANDLEROFPCCONFIG                                         PduR_Fm_ActivateNext_FmSmStateHandler
#define PDUR_EQ2_FM_ACTIVATEREAD_FMSMSTATEHANDLEROFPCCONFIG                                         PduR_Fm_ActivateRead_FmSmStateHandler
#define PDUR_EQ2_FM_ACTIVATEWRITE_FMSMSTATEHANDLEROFPCCONFIG                                        PduR_Fm_ActivateWrite_FmSmStateHandler
#define PDUR_EQ2_FM_FINISHREAD_FMSMSTATEHANDLEROFPCCONFIG                                           PduR_Fm_FinishRead_FmSmStateHandler
#define PDUR_EQ2_FM_FINISHWRITE_FMSMSTATEHANDLEROFPCCONFIG                                          PduR_Fm_FinishWrite_FmSmStateHandler
#define PDUR_EQ2_GENERALPROPERTIESROMOFPCCONFIG                                                     PduR_GeneralPropertiesRom
#define PDUR_EQ2_INITIALIZEDOFPCCONFIG                                                              (&(PduR_Initialized))
#define PDUR_EQ2_LOCKROMOFPCCONFIG                                                                  PduR_LockRom
#define PDUR_EQ2_MMROMINDOFPCCONFIG                                                                 PduR_MmRomInd
#define PDUR_EQ2_MMROMOFPCCONFIG                                                                    PduR_MmRom
#define PDUR_EQ2_RMBUFFEREDTPPROPERTIESRAMOFPCCONFIG                                                PduR_RmBufferedTpPropertiesRam.raw
#define PDUR_EQ2_RMBUFFEREDTPPROPERTIESROMOFPCCONFIG                                                PduR_RmBufferedTpPropertiesRom
#define PDUR_EQ2_RMDESTROMOFPCCONFIG                                                                PduR_RmDestRom
#define PDUR_EQ2_RMGDESTROMOFPCCONFIG                                                               PduR_RmGDestRom
#define PDUR_EQ2_RMGDESTTPTXSTATERAMOFPCCONFIG                                                      PduR_RmGDestTpTxStateRam.raw
#define PDUR_EQ2_RMSRCROMOFPCCONFIG                                                                 PduR_RmSrcRom
#define PDUR_EQ2_RMTP_CANCELRECEIVE_TPRXSMSTATEHANDLEROFPCCONFIG                                    PduR_RmTp_CancelReceive_TpRxSmStateHandler
#define PDUR_EQ2_RMTP_CHECKREADY2TRANSMIT_TPTXSMSTATEHANDLEROFPCCONFIG                              PduR_RmTp_CheckReady2Transmit_TpTxSmStateHandler
#define PDUR_EQ2_RMTP_COPYRXDATA_TPRXSMSTATEHANDLEROFPCCONFIG                                       PduR_RmTp_CopyRxData_TpRxSmStateHandler
#define PDUR_EQ2_RMTP_FINISHRECEPTION_TPTXSMSTATEHANDLEROFPCCONFIG                                  PduR_RmTp_FinishReception_TpTxSmStateHandler
#define PDUR_EQ2_RMTP_FINISHTRANSMISSION_TPTXSMSTATEHANDLEROFPCCONFIG                               PduR_RmTp_FinishTransmission_TpTxSmStateHandler
#define PDUR_EQ2_RMTP_STARTOFRECEPTION_TPRXSMSTATEHANDLEROFPCCONFIG                                 PduR_RmTp_StartOfReception_TpRxSmStateHandler
#define PDUR_EQ2_RMTP_TPRXINDICATION_TPRXSMSTATEHANDLEROFPCCONFIG                                   PduR_RmTp_TpRxIndication_TpRxSmStateHandler
#define PDUR_EQ2_RMTRANSMITFCTPTROFPCCONFIG                                                         PduR_RmTransmitFctPtr
#define PDUR_EQ2_RMTXINSTSMROMOFPCCONFIG                                                            PduR_RmTxInstSmRom
#define PDUR_EQ2_RXIF2DESTOFPCCONFIG                                                                PduR_RxIf2Dest
#define PDUR_EQ2_RXTP2DESTOFPCCONFIG                                                                PduR_RxTp2Dest
#define PDUR_EQ2_TX2LOOFPCCONFIG                                                                    PduR_Tx2Lo
#define PDUR_EQ2_TXIF2UPOFPCCONFIG                                                                  PduR_TxIf2Up
#define PDUR_EQ2_TXTP2SRCOFPCCONFIG                                                                 PduR_TxTp2Src
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCSymbolicInitializationPointers  PduR Symbolic Initialization Pointers (PRE_COMPILE)
  \brief  Symbolic initialization pointers to be used in the call of a preinit or init function.
  \{
*/ 
#define PduR_Config_Ptr                                                                             NULL_PTR  /**< symbolic identifier which shall be used to initialize 'PduR' */
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCInitializationSymbols  PduR Initialization Symbols (PRE_COMPILE)
  \brief  Symbolic initialization pointers which may be used in the call of a preinit or init function. Please note, that the defined value can be a 'NULL_PTR' and the address operator is not usable.
  \{
*/ 
#define PduR_Config                                                                                 NULL_PTR  /**< symbolic identifier which could be used to initialize 'PduR */
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCGeneral  PduR General (PRE_COMPILE)
  \brief  General constant defines not associated with a group of defines.
  \{
*/ 
#define PDUR_CHECK_INIT_POINTER                                                                     STD_OFF  /**< STD_ON if the init pointer shall not be used as NULL_PTR and a check shall validate this. */
#define PDUR_FINAL_MAGIC_NUMBER                                                                     0x331Eu  /**< the precompile constant to validate the size of the initialization structure at initialization time of PduR */
#define PDUR_INDIVIDUAL_POSTBUILD                                                                   STD_OFF  /**< the precompile constant to check, that the module is individual postbuildable. The module 'PduR' is not configured to be postbuild capable. */
#define PDUR_INIT_DATA                                                                              PDUR_CONST  /**< CompilerMemClassDefine for the initialization data. */
#define PDUR_INIT_DATA_HASH_CODE                                                                    -863935689  /**< the precompile constant to validate the initialization structure at initialization time of PduR with a hashcode. The seed value is '0x331Eu' */
#define PDUR_USE_ECUM_BSW_ERROR_HOOK                                                                STD_OFF  /**< STD_ON if the EcuM_BswErrorHook shall be called in the ConfigPtr check. */
#define PDUR_USE_INIT_POINTER                                                                       STD_OFF  /**< STD_ON if the init pointer PduR shall be used. */
/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  PduRLTDataSwitches  PduR Data Switches  (LINK)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define PDUR_LTCONFIG                                                                               STD_OFF  /**< Deactivateable: 'PduR_LTConfig' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
/** 
  \}
*/ 



/** BroadcastDummy */

#define PDUR_MAX_TRIGGER_TRANSMIT_PDU_SIZE 8


#if (PDUR_FMFIFOELEMENTRAM == STD_OFF)
  #define PduR_FmFifoElementRamIterType uint16
#endif

/**********************************************************************************************************************
 * GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/
/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
/** 
  \defgroup  PduRPCGetConstantDuplicatedRootDataMacros  PduR Get Constant Duplicated Root Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated by constance root data elements.
  \{
*/ 
#define PduR_GetBmTxBufferArrayRamOfPCConfig()                                                      PduR_BmTxBufferArrayRam.raw  /**< the pointer to PduR_BmTxBufferArrayRam */
#define PduR_GetBmTxBufferIndRomOfPCConfig()                                                        PduR_BmTxBufferIndRom  /**< the pointer to PduR_BmTxBufferIndRom */
#define PduR_GetBmTxBufferInstanceRamOfPCConfig()                                                   PduR_BmTxBufferInstanceRam.raw  /**< the pointer to PduR_BmTxBufferInstanceRam */
#define PduR_GetBmTxBufferInstanceRomOfPCConfig()                                                   PduR_BmTxBufferInstanceRom  /**< the pointer to PduR_BmTxBufferInstanceRom */
#define PduR_GetBmTxBufferRamOfPCConfig()                                                           PduR_BmTxBufferRam.raw  /**< the pointer to PduR_BmTxBufferRam */
#define PduR_GetBmTxBufferRomOfPCConfig()                                                           PduR_BmTxBufferRom  /**< the pointer to PduR_BmTxBufferRom */
#define PduR_GetConfigIdOfPCConfig()                                                                0u  /**< DefinitionRef: /MICROSAR/PduR/PduRRoutingTables/PduRConfigurationId */
#define PduR_GetCoreManagerRomOfPCConfig()                                                          PduR_CoreManagerRom  /**< the pointer to PduR_CoreManagerRom */
#define PduR_GetExclusiveAreaRomOfPCConfig()                                                        PduR_ExclusiveAreaRom  /**< the pointer to PduR_ExclusiveAreaRom */
#define PduR_GetFmFifoElementRamOfPCConfig()                                                        PduR_FmFifoElementRam.raw  /**< the pointer to PduR_FmFifoElementRam */
#define PduR_GetFmFifoInstanceRamOfPCConfig()                                                       PduR_FmFifoInstanceRam.raw  /**< the pointer to PduR_FmFifoInstanceRam */
#define PduR_GetFmFifoInstanceRomOfPCConfig()                                                       PduR_FmFifoInstanceRom  /**< the pointer to PduR_FmFifoInstanceRom */
#define PduR_GetFmFifoRamOfPCConfig()                                                               PduR_FmFifoRam.raw  /**< the pointer to PduR_FmFifoRam */
#define PduR_GetFmFifoRomOfPCConfig()                                                               PduR_FmFifoRom  /**< the pointer to PduR_FmFifoRom */
#define PduR_GetFm_ActivateNext_FmSmStateHandlerOfPCConfig()                                        PduR_Fm_ActivateNext_FmSmStateHandler  /**< the pointer to PduR_Fm_ActivateNext_FmSmStateHandler */
#define PduR_GetFm_ActivateRead_FmSmStateHandlerOfPCConfig()                                        PduR_Fm_ActivateRead_FmSmStateHandler  /**< the pointer to PduR_Fm_ActivateRead_FmSmStateHandler */
#define PduR_GetFm_ActivateWrite_FmSmStateHandlerOfPCConfig()                                       PduR_Fm_ActivateWrite_FmSmStateHandler  /**< the pointer to PduR_Fm_ActivateWrite_FmSmStateHandler */
#define PduR_GetFm_FinishRead_FmSmStateHandlerOfPCConfig()                                          PduR_Fm_FinishRead_FmSmStateHandler  /**< the pointer to PduR_Fm_FinishRead_FmSmStateHandler */
#define PduR_GetFm_FinishWrite_FmSmStateHandlerOfPCConfig()                                         PduR_Fm_FinishWrite_FmSmStateHandler  /**< the pointer to PduR_Fm_FinishWrite_FmSmStateHandler */
#define PduR_GetGeneralPropertiesRomOfPCConfig()                                                    PduR_GeneralPropertiesRom  /**< the pointer to PduR_GeneralPropertiesRom */
#define PduR_GetInitializedOfPCConfig()                                                             (&(PduR_Initialized))  /**< the pointer to PduR_Initialized */
#define PduR_GetLockRomOfPCConfig()                                                                 PduR_LockRom  /**< the pointer to PduR_LockRom */
#define PduR_GetMmRomIndOfPCConfig()                                                                PduR_MmRomInd  /**< the pointer to PduR_MmRomInd */
#define PduR_GetMmRomOfPCConfig()                                                                   PduR_MmRom  /**< the pointer to PduR_MmRom */
#define PduR_GetRmBufferedTpPropertiesRamOfPCConfig()                                               PduR_RmBufferedTpPropertiesRam.raw  /**< the pointer to PduR_RmBufferedTpPropertiesRam */
#define PduR_GetRmBufferedTpPropertiesRomOfPCConfig()                                               PduR_RmBufferedTpPropertiesRom  /**< the pointer to PduR_RmBufferedTpPropertiesRom */
#define PduR_GetRmDestRomOfPCConfig()                                                               PduR_RmDestRom  /**< the pointer to PduR_RmDestRom */
#define PduR_GetRmGDestRomOfPCConfig()                                                              PduR_RmGDestRom  /**< the pointer to PduR_RmGDestRom */
#define PduR_GetRmGDestTpTxStateRamOfPCConfig()                                                     PduR_RmGDestTpTxStateRam.raw  /**< the pointer to PduR_RmGDestTpTxStateRam */
#define PduR_GetRmSrcRomOfPCConfig()                                                                PduR_RmSrcRom  /**< the pointer to PduR_RmSrcRom */
#define PduR_GetRmTp_CancelReceive_TpRxSmStateHandlerOfPCConfig()                                   PduR_RmTp_CancelReceive_TpRxSmStateHandler  /**< the pointer to PduR_RmTp_CancelReceive_TpRxSmStateHandler */
#define PduR_GetRmTp_CheckReady2Transmit_TpTxSmStateHandlerOfPCConfig()                             PduR_RmTp_CheckReady2Transmit_TpTxSmStateHandler  /**< the pointer to PduR_RmTp_CheckReady2Transmit_TpTxSmStateHandler */
#define PduR_GetRmTp_CopyRxData_TpRxSmStateHandlerOfPCConfig()                                      PduR_RmTp_CopyRxData_TpRxSmStateHandler  /**< the pointer to PduR_RmTp_CopyRxData_TpRxSmStateHandler */
#define PduR_GetRmTp_FinishReception_TpTxSmStateHandlerOfPCConfig()                                 PduR_RmTp_FinishReception_TpTxSmStateHandler  /**< the pointer to PduR_RmTp_FinishReception_TpTxSmStateHandler */
#define PduR_GetRmTp_FinishTransmission_TpTxSmStateHandlerOfPCConfig()                              PduR_RmTp_FinishTransmission_TpTxSmStateHandler  /**< the pointer to PduR_RmTp_FinishTransmission_TpTxSmStateHandler */
#define PduR_GetRmTp_StartOfReception_TpRxSmStateHandlerOfPCConfig()                                PduR_RmTp_StartOfReception_TpRxSmStateHandler  /**< the pointer to PduR_RmTp_StartOfReception_TpRxSmStateHandler */
#define PduR_GetRmTp_TpRxIndication_TpRxSmStateHandlerOfPCConfig()                                  PduR_RmTp_TpRxIndication_TpRxSmStateHandler  /**< the pointer to PduR_RmTp_TpRxIndication_TpRxSmStateHandler */
#define PduR_GetRmTransmitFctPtrOfPCConfig()                                                        PduR_RmTransmitFctPtr  /**< the pointer to PduR_RmTransmitFctPtr */
#define PduR_GetRmTxInstSmRomOfPCConfig()                                                           PduR_RmTxInstSmRom  /**< the pointer to PduR_RmTxInstSmRom */
#define PduR_GetRxIf2DestOfPCConfig()                                                               PduR_RxIf2Dest  /**< the pointer to PduR_RxIf2Dest */
#define PduR_GetRxTp2DestOfPCConfig()                                                               PduR_RxTp2Dest  /**< the pointer to PduR_RxTp2Dest */
#define PduR_GetSizeOfBmTxBufferArrayRamOfPCConfig()                                                7106u  /**< the number of accomplishable value elements in PduR_BmTxBufferArrayRam */
#define PduR_GetSizeOfBmTxBufferIndRomOfPCConfig()                                                  77u  /**< the number of accomplishable value elements in PduR_BmTxBufferIndRom */
#define PduR_GetSizeOfBmTxBufferInstanceRomOfPCConfig()                                             81u  /**< the number of accomplishable value elements in PduR_BmTxBufferInstanceRom */
#define PduR_GetSizeOfBmTxBufferRomOfPCConfig()                                                     77u  /**< the number of accomplishable value elements in PduR_BmTxBufferRom */
#define PduR_GetSizeOfCoreManagerRomOfPCConfig()                                                    1u  /**< the number of accomplishable value elements in PduR_CoreManagerRom */
#define PduR_GetSizeOfExclusiveAreaRomOfPCConfig()                                                  1u  /**< the number of accomplishable value elements in PduR_ExclusiveAreaRom */
#define PduR_GetSizeOfFmFifoElementRamOfPCConfig()                                                  151u  /**< the number of accomplishable value elements in PduR_FmFifoElementRam */
#define PduR_GetSizeOfFmFifoInstanceRomOfPCConfig()                                                 97u  /**< the number of accomplishable value elements in PduR_FmFifoInstanceRom */
#define PduR_GetSizeOfFmFifoRomOfPCConfig()                                                         95u  /**< the number of accomplishable value elements in PduR_FmFifoRom */
#define PduR_GetSizeOfFm_ActivateNext_FmSmStateHandlerOfPCConfig()                                  8u  /**< the number of accomplishable value elements in PduR_Fm_ActivateNext_FmSmStateHandler */
#define PduR_GetSizeOfFm_ActivateRead_FmSmStateHandlerOfPCConfig()                                  8u  /**< the number of accomplishable value elements in PduR_Fm_ActivateRead_FmSmStateHandler */
#define PduR_GetSizeOfFm_ActivateWrite_FmSmStateHandlerOfPCConfig()                                 8u  /**< the number of accomplishable value elements in PduR_Fm_ActivateWrite_FmSmStateHandler */
#define PduR_GetSizeOfFm_FinishRead_FmSmStateHandlerOfPCConfig()                                    8u  /**< the number of accomplishable value elements in PduR_Fm_FinishRead_FmSmStateHandler */
#define PduR_GetSizeOfFm_FinishWrite_FmSmStateHandlerOfPCConfig()                                   8u  /**< the number of accomplishable value elements in PduR_Fm_FinishWrite_FmSmStateHandler */
#define PduR_GetSizeOfGeneralPropertiesRomOfPCConfig()                                              1u  /**< the number of accomplishable value elements in PduR_GeneralPropertiesRom */
#define PduR_GetSizeOfLockRomOfPCConfig()                                                           1u  /**< the number of accomplishable value elements in PduR_LockRom */
#define PduR_GetSizeOfMmRomIndOfPCConfig()                                                          9u  /**< the number of accomplishable value elements in PduR_MmRomInd */
#define PduR_GetSizeOfMmRomOfPCConfig()                                                             9u  /**< the number of accomplishable value elements in PduR_MmRom */
#define PduR_GetSizeOfRmBufferedTpPropertiesRomOfPCConfig()                                         95u  /**< the number of accomplishable value elements in PduR_RmBufferedTpPropertiesRom */
#define PduR_GetSizeOfRmDestRomOfPCConfig()                                                         653u  /**< the number of accomplishable value elements in PduR_RmDestRom */
#define PduR_GetSizeOfRmGDestRomOfPCConfig()                                                        653u  /**< the number of accomplishable value elements in PduR_RmGDestRom */
#define PduR_GetSizeOfRmGDestTpTxStateRamOfPCConfig()                                               99u  /**< the number of accomplishable value elements in PduR_RmGDestTpTxStateRam */
#define PduR_GetSizeOfRmSrcRomOfPCConfig()                                                          649u  /**< the number of accomplishable value elements in PduR_RmSrcRom */
#define PduR_GetSizeOfRmTp_CancelReceive_TpRxSmStateHandlerOfPCConfig()                             3u  /**< the number of accomplishable value elements in PduR_RmTp_CancelReceive_TpRxSmStateHandler */
#define PduR_GetSizeOfRmTp_CheckReady2Transmit_TpTxSmStateHandlerOfPCConfig()                       5u  /**< the number of accomplishable value elements in PduR_RmTp_CheckReady2Transmit_TpTxSmStateHandler */
#define PduR_GetSizeOfRmTp_CopyRxData_TpRxSmStateHandlerOfPCConfig()                                3u  /**< the number of accomplishable value elements in PduR_RmTp_CopyRxData_TpRxSmStateHandler */
#define PduR_GetSizeOfRmTp_FinishReception_TpTxSmStateHandlerOfPCConfig()                           5u  /**< the number of accomplishable value elements in PduR_RmTp_FinishReception_TpTxSmStateHandler */
#define PduR_GetSizeOfRmTp_FinishTransmission_TpTxSmStateHandlerOfPCConfig()                        5u  /**< the number of accomplishable value elements in PduR_RmTp_FinishTransmission_TpTxSmStateHandler */
#define PduR_GetSizeOfRmTp_StartOfReception_TpRxSmStateHandlerOfPCConfig()                          3u  /**< the number of accomplishable value elements in PduR_RmTp_StartOfReception_TpRxSmStateHandler */
#define PduR_GetSizeOfRmTp_TpRxIndication_TpRxSmStateHandlerOfPCConfig()                            3u  /**< the number of accomplishable value elements in PduR_RmTp_TpRxIndication_TpRxSmStateHandler */
#define PduR_GetSizeOfRmTransmitFctPtrOfPCConfig()                                                  3u  /**< the number of accomplishable value elements in PduR_RmTransmitFctPtr */
#define PduR_GetSizeOfRmTxInstSmRomOfPCConfig()                                                     4u  /**< the number of accomplishable value elements in PduR_RmTxInstSmRom */
#define PduR_GetSizeOfRxIf2DestOfPCConfig()                                                         173u  /**< the number of accomplishable value elements in PduR_RxIf2Dest */
#define PduR_GetSizeOfRxTp2DestOfPCConfig()                                                         201u  /**< the number of accomplishable value elements in PduR_RxTp2Dest */
#define PduR_GetSizeOfTx2LoOfPCConfig()                                                             275u  /**< the number of accomplishable value elements in PduR_Tx2Lo */
#define PduR_GetSizeOfTxIf2UpOfPCConfig()                                                           127u  /**< the number of accomplishable value elements in PduR_TxIf2Up */
#define PduR_GetSizeOfTxTp2SrcOfPCConfig()                                                          203u  /**< the number of accomplishable value elements in PduR_TxTp2Src */
#define PduR_GetTx2LoOfPCConfig()                                                                   PduR_Tx2Lo  /**< the pointer to PduR_Tx2Lo */
#define PduR_GetTxIf2UpOfPCConfig()                                                                 PduR_TxIf2Up  /**< the pointer to PduR_TxIf2Up */
#define PduR_GetTxTp2SrcOfPCConfig()                                                                PduR_TxTp2Src  /**< the pointer to PduR_TxTp2Src */
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCGetDuplicatedRootDataMacros  PduR Get Duplicated Root Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated root data elements.
  \{
*/ 
#define PduR_GetSizeOfBmTxBufferInstanceRamOfPCConfig()                                             PduR_GetSizeOfBmTxBufferInstanceRomOfPCConfig()  /**< the number of accomplishable value elements in PduR_BmTxBufferInstanceRam */
#define PduR_GetSizeOfBmTxBufferRamOfPCConfig()                                                     PduR_GetSizeOfBmTxBufferRomOfPCConfig()  /**< the number of accomplishable value elements in PduR_BmTxBufferRam */
#define PduR_GetSizeOfFmFifoInstanceRamOfPCConfig()                                                 PduR_GetSizeOfFmFifoInstanceRomOfPCConfig()  /**< the number of accomplishable value elements in PduR_FmFifoInstanceRam */
#define PduR_GetSizeOfFmFifoRamOfPCConfig()                                                         PduR_GetSizeOfFmFifoRomOfPCConfig()  /**< the number of accomplishable value elements in PduR_FmFifoRam */
#define PduR_GetSizeOfRmBufferedTpPropertiesRamOfPCConfig()                                         PduR_GetSizeOfRmBufferedTpPropertiesRomOfPCConfig()  /**< the number of accomplishable value elements in PduR_RmBufferedTpPropertiesRam */
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCGetDataMacros  PduR Get Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read CONST and VAR data.
  \{
*/ 
#define PduR_GetBmTxBufferArrayRam(Index)                                                           (PduR_GetBmTxBufferArrayRamOfPCConfig()[(Index)])
#define PduR_GetBmTxBufferRomIdxOfBmTxBufferIndRom(Index)                                           (PduR_GetBmTxBufferIndRomOfPCConfig()[(Index)].BmTxBufferRomIdxOfBmTxBufferIndRom)
#define PduR_GetBmTxBufferArrayRamReadIdxOfBmTxBufferInstanceRam(Index)                             (PduR_GetBmTxBufferInstanceRamOfPCConfig()[(Index)].BmTxBufferArrayRamReadIdxOfBmTxBufferInstanceRam)
#define PduR_GetBmTxBufferArrayRamWriteIdxOfBmTxBufferInstanceRam(Index)                            (PduR_GetBmTxBufferInstanceRamOfPCConfig()[(Index)].BmTxBufferArrayRamWriteIdxOfBmTxBufferInstanceRam)
#define PduR_GetPduRBufferStateOfBmTxBufferInstanceRam(Index)                                       (PduR_GetBmTxBufferInstanceRamOfPCConfig()[(Index)].PduRBufferStateOfBmTxBufferInstanceRam)
#define PduR_IsTxBufferUsedOfBmTxBufferInstanceRam(Index)                                           ((PduR_GetBmTxBufferInstanceRamOfPCConfig()[(Index)].TxBufferUsedOfBmTxBufferInstanceRam) != FALSE)
#define PduR_GetBmTxBufferRomIdxOfBmTxBufferInstanceRom(Index)                                      (PduR_GetBmTxBufferInstanceRomOfPCConfig()[(Index)].BmTxBufferRomIdxOfBmTxBufferInstanceRom)
#define PduR_IsAllocatedOfBmTxBufferRam(Index)                                                      ((PduR_GetBmTxBufferRamOfPCConfig()[(Index)].AllocatedOfBmTxBufferRam) != FALSE)
#define PduR_GetBmTxBufferArrayRamInstanceStopIdxOfBmTxBufferRam(Index)                             (PduR_GetBmTxBufferRamOfPCConfig()[(Index)].BmTxBufferArrayRamInstanceStopIdxOfBmTxBufferRam)
#define PduR_GetBmTxBufferArrayRamReadIdxOfBmTxBufferRam(Index)                                     (PduR_GetBmTxBufferRamOfPCConfig()[(Index)].BmTxBufferArrayRamReadIdxOfBmTxBufferRam)
#define PduR_GetBmTxBufferArrayRamWriteIdxOfBmTxBufferRam(Index)                                    (PduR_GetBmTxBufferRamOfPCConfig()[(Index)].BmTxBufferArrayRamWriteIdxOfBmTxBufferRam)
#define PduR_GetPduRBufferStateOfBmTxBufferRam(Index)                                               (PduR_GetBmTxBufferRamOfPCConfig()[(Index)].PduRBufferStateOfBmTxBufferRam)
#define PduR_GetRxLengthOfBmTxBufferRam(Index)                                                      (PduR_GetBmTxBufferRamOfPCConfig()[(Index)].RxLengthOfBmTxBufferRam)
#define PduR_GetBmTxBufferArrayRamEndIdxOfBmTxBufferRom(Index)                                      (PduR_GetBmTxBufferRomOfPCConfig()[(Index)].BmTxBufferArrayRamEndIdxOfBmTxBufferRom)
#define PduR_GetBmTxBufferArrayRamStartIdxOfBmTxBufferRom(Index)                                    (PduR_GetBmTxBufferRomOfPCConfig()[(Index)].BmTxBufferArrayRamStartIdxOfBmTxBufferRom)
#define PduR_GetBmTxBufferInstanceRomEndIdxOfBmTxBufferRom(Index)                                   (PduR_GetBmTxBufferRomOfPCConfig()[(Index)].BmTxBufferInstanceRomEndIdxOfBmTxBufferRom)
#define PduR_GetBmTxBufferInstanceRomStartIdxOfBmTxBufferRom(Index)                                 (PduR_GetBmTxBufferRomOfPCConfig()[(Index)].BmTxBufferInstanceRomStartIdxOfBmTxBufferRom)
#define PduR_GetLockOfExclusiveAreaRom(Index)                                                       (PduR_GetExclusiveAreaRomOfPCConfig()[(Index)].LockOfExclusiveAreaRom)
#define PduR_GetUnlockOfExclusiveAreaRom(Index)                                                     (PduR_GetExclusiveAreaRomOfPCConfig()[(Index)].UnlockOfExclusiveAreaRom)
#define PduR_GetBmTxBufferRomIdxOfFmFifoElementRam(Index)                                           (PduR_GetFmFifoElementRamOfPCConfig()[(Index)].BmTxBufferRomIdxOfFmFifoElementRam)
#define PduR_IsDedicatedTxBufferOfFmFifoElementRam(Index)                                           ((PduR_GetFmFifoElementRamOfPCConfig()[(Index)].DedicatedTxBufferOfFmFifoElementRam) != FALSE)
#define PduR_GetRmDestRomIdxOfFmFifoElementRam(Index)                                               (PduR_GetFmFifoElementRamOfPCConfig()[(Index)].RmDestRomIdxOfFmFifoElementRam)
#define PduR_GetStateOfFmFifoElementRam(Index)                                                      (PduR_GetFmFifoElementRamOfPCConfig()[(Index)].StateOfFmFifoElementRam)
#define PduR_GetBmTxBufferInstanceRomIdxOfFmFifoInstanceRam(Index)                                  (PduR_GetFmFifoInstanceRamOfPCConfig()[(Index)].BmTxBufferInstanceRomIdxOfFmFifoInstanceRam)
#define PduR_GetFmFifoRomIdxOfFmFifoInstanceRom(Index)                                              (PduR_GetFmFifoInstanceRomOfPCConfig()[(Index)].FmFifoRomIdxOfFmFifoInstanceRom)
#define PduR_GetFillLevelOfFmFifoRam(Index)                                                         (PduR_GetFmFifoRamOfPCConfig()[(Index)].FillLevelOfFmFifoRam)
#define PduR_GetFmFifoElementRamReadIdxOfFmFifoRam(Index)                                           (PduR_GetFmFifoRamOfPCConfig()[(Index)].FmFifoElementRamReadIdxOfFmFifoRam)
#define PduR_GetFmFifoElementRamWriteIdxOfFmFifoRam(Index)                                          (PduR_GetFmFifoRamOfPCConfig()[(Index)].FmFifoElementRamWriteIdxOfFmFifoRam)
#define PduR_GetPendingConfirmationsOfFmFifoRam(Index)                                              (PduR_GetFmFifoRamOfPCConfig()[(Index)].PendingConfirmationsOfFmFifoRam)
#define PduR_GetTpTxSmStateOfFmFifoRam(Index)                                                       (PduR_GetFmFifoRamOfPCConfig()[(Index)].TpTxSmStateOfFmFifoRam)
#define PduR_GetBmTxBufferIndRomEndIdxOfFmFifoRom(Index)                                            (PduR_GetFmFifoRomOfPCConfig()[(Index)].BmTxBufferIndRomEndIdxOfFmFifoRom)
#define PduR_GetBmTxBufferIndRomStartIdxOfFmFifoRom(Index)                                          (PduR_GetFmFifoRomOfPCConfig()[(Index)].BmTxBufferIndRomStartIdxOfFmFifoRom)
#define PduR_GetFmFifoElementRamEndIdxOfFmFifoRom(Index)                                            (PduR_GetFmFifoRomOfPCConfig()[(Index)].FmFifoElementRamEndIdxOfFmFifoRom)
#define PduR_GetFmFifoElementRamStartIdxOfFmFifoRom(Index)                                          (PduR_GetFmFifoRomOfPCConfig()[(Index)].FmFifoElementRamStartIdxOfFmFifoRom)
#define PduR_GetFctPtrOfFm_ActivateNext_FmSmStateHandler(Index)                                     (PduR_GetFm_ActivateNext_FmSmStateHandlerOfPCConfig()[(Index)].FctPtrOfFm_ActivateNext_FmSmStateHandler)
#define PduR_GetFctPtrOfFm_ActivateRead_FmSmStateHandler(Index)                                     (PduR_GetFm_ActivateRead_FmSmStateHandlerOfPCConfig()[(Index)].FctPtrOfFm_ActivateRead_FmSmStateHandler)
#define PduR_GetFctPtrOfFm_ActivateWrite_FmSmStateHandler(Index)                                    (PduR_GetFm_ActivateWrite_FmSmStateHandlerOfPCConfig()[(Index)].FctPtrOfFm_ActivateWrite_FmSmStateHandler)
#define PduR_GetFctPtrOfFm_FinishRead_FmSmStateHandler(Index)                                       (PduR_GetFm_FinishRead_FmSmStateHandlerOfPCConfig()[(Index)].FctPtrOfFm_FinishRead_FmSmStateHandler)
#define PduR_GetFctPtrOfFm_FinishWrite_FmSmStateHandler(Index)                                      (PduR_GetFm_FinishWrite_FmSmStateHandlerOfPCConfig()[(Index)].FctPtrOfFm_FinishWrite_FmSmStateHandler)
#define PduR_IshasTpTxBufferedForwardingOfGeneralPropertiesRom(Index)                               ((PduR_GetGeneralPropertiesRomOfPCConfig()[(Index)].hasTpTxBufferedForwardingOfGeneralPropertiesRom) != FALSE)
#define PduR_IsInitialized()                                                                        (((*(PduR_GetInitializedOfPCConfig()))) != FALSE)
#define PduR_GetCoreManagerRomIdxOfMmRom(Index)                                                     (PduR_GetMmRomOfPCConfig()[(Index)].CoreManagerRomIdxOfMmRom)
#define PduR_GetLoIfCancelTransmitFctPtrOfMmRom(Index)                                              (PduR_GetMmRomOfPCConfig()[(Index)].LoIfCancelTransmitFctPtrOfMmRom)
#define PduR_GetLoIfTransmitFctPtrOfMmRom(Index)                                                    (PduR_GetMmRomOfPCConfig()[(Index)].LoIfTransmitFctPtrOfMmRom)
#define PduR_GetLoTpCancelTransmitFctPtrOfMmRom(Index)                                              (PduR_GetMmRomOfPCConfig()[(Index)].LoTpCancelTransmitFctPtrOfMmRom)
#define PduR_GetLoTpTransmitFctPtrOfMmRom(Index)                                                    (PduR_GetMmRomOfPCConfig()[(Index)].LoTpTransmitFctPtrOfMmRom)
#define PduR_GetMaskedBitsOfMmRom(Index)                                                            (PduR_GetMmRomOfPCConfig()[(Index)].MaskedBitsOfMmRom)
#define PduR_GetRmGDestRomEndIdxOfMmRom(Index)                                                      (PduR_GetMmRomOfPCConfig()[(Index)].RmGDestRomEndIdxOfMmRom)
#define PduR_GetRmGDestRomStartIdxOfMmRom(Index)                                                    (PduR_GetMmRomOfPCConfig()[(Index)].RmGDestRomStartIdxOfMmRom)
#define PduR_GetUpIfRxIndicationFctPtrOfMmRom(Index)                                                (PduR_GetMmRomOfPCConfig()[(Index)].UpIfRxIndicationFctPtrOfMmRom)
#define PduR_GetUpIfTriggerTransmitFctPtrOfMmRom(Index)                                             (PduR_GetMmRomOfPCConfig()[(Index)].UpIfTriggerTransmitFctPtrOfMmRom)
#define PduR_GetUpIfTxConfirmationFctPtrOfMmRom(Index)                                              (PduR_GetMmRomOfPCConfig()[(Index)].UpIfTxConfirmationFctPtrOfMmRom)
#define PduR_GetUpTpCopyRxDataFctPtrOfMmRom(Index)                                                  (PduR_GetMmRomOfPCConfig()[(Index)].UpTpCopyRxDataFctPtrOfMmRom)
#define PduR_GetUpTpCopyTxDataFctPtrOfMmRom(Index)                                                  (PduR_GetMmRomOfPCConfig()[(Index)].UpTpCopyTxDataFctPtrOfMmRom)
#define PduR_GetUpTpStartOfReceptionFctPtrOfMmRom(Index)                                            (PduR_GetMmRomOfPCConfig()[(Index)].UpTpStartOfReceptionFctPtrOfMmRom)
#define PduR_GetUpTpTpRxIndicationFctPtrOfMmRom(Index)                                              (PduR_GetMmRomOfPCConfig()[(Index)].UpTpTpRxIndicationFctPtrOfMmRom)
#define PduR_GetUpTpTpTxConfirmationFctPtrOfMmRom(Index)                                            (PduR_GetMmRomOfPCConfig()[(Index)].UpTpTpTxConfirmationFctPtrOfMmRom)
#define PduR_GetFmFifoElementRamIdxOfRmBufferedTpPropertiesRam(Index)                               (PduR_GetRmBufferedTpPropertiesRamOfPCConfig()[(Index)].FmFifoElementRamIdxOfRmBufferedTpPropertiesRam)
#define PduR_GetTpRxSmStateOfRmBufferedTpPropertiesRam(Index)                                       (PduR_GetRmBufferedTpPropertiesRamOfPCConfig()[(Index)].TpRxSmStateOfRmBufferedTpPropertiesRam)
#define PduR_IsDedicatedTxBufferOfRmBufferedTpPropertiesRom(Index)                                  ((PduR_GetRmBufferedTpPropertiesRomOfPCConfig()[(Index)].DedicatedTxBufferOfRmBufferedTpPropertiesRom) != FALSE)
#define PduR_GetFmFifoRomIdxOfRmBufferedTpPropertiesRom(Index)                                      (PduR_GetRmBufferedTpPropertiesRomOfPCConfig()[(Index)].FmFifoRomIdxOfRmBufferedTpPropertiesRom)
#define PduR_GetQueuedDestCntOfRmBufferedTpPropertiesRom(Index)                                     (PduR_GetRmBufferedTpPropertiesRomOfPCConfig()[(Index)].QueuedDestCntOfRmBufferedTpPropertiesRom)
#define PduR_GetTpThresholdOfRmBufferedTpPropertiesRom(Index)                                       (PduR_GetRmBufferedTpPropertiesRomOfPCConfig()[(Index)].TpThresholdOfRmBufferedTpPropertiesRom)
#define PduR_GetRmGDestRomIdxOfRmDestRom(Index)                                                     (PduR_GetRmDestRomOfPCConfig()[(Index)].RmGDestRomIdxOfRmDestRom)
#define PduR_GetRmSrcRomIdxOfRmDestRom(Index)                                                       (PduR_GetRmDestRomOfPCConfig()[(Index)].RmSrcRomIdxOfRmDestRom)
#define PduR_GetRoutingTypeOfRmDestRom(Index)                                                       (PduR_GetRmDestRomOfPCConfig()[(Index)].RoutingTypeOfRmDestRom)
#define PduR_GetDestHndOfRmGDestRom(Index)                                                          (PduR_GetRmGDestRomOfPCConfig()[(Index)].DestHndOfRmGDestRom)
#define PduR_GetDirectionOfRmGDestRom(Index)                                                        (PduR_GetRmGDestRomOfPCConfig()[(Index)].DirectionOfRmGDestRom)
#define PduR_GetFmFifoInstanceRomIdxOfRmGDestRom(Index)                                             (PduR_GetRmGDestRomOfPCConfig()[(Index)].FmFifoInstanceRomIdxOfRmGDestRom)
#define PduR_GetMaskedBitsOfRmGDestRom(Index)                                                       (PduR_GetRmGDestRomOfPCConfig()[(Index)].MaskedBitsOfRmGDestRom)
#define PduR_GetMmRomIdxOfRmGDestRom(Index)                                                         (PduR_GetRmGDestRomOfPCConfig()[(Index)].MmRomIdxOfRmGDestRom)
#define PduR_GetPduRDestPduProcessingOfRmGDestRom(Index)                                            (PduR_GetRmGDestRomOfPCConfig()[(Index)].PduRDestPduProcessingOfRmGDestRom)
#define PduR_GetRmDestRomIdxOfRmGDestRom(Index)                                                     (PduR_GetRmGDestRomOfPCConfig()[(Index)].RmDestRomIdxOfRmGDestRom)
#define PduR_GetRmGDestTpTxStateRamIdxOfRmGDestRom(Index)                                           (PduR_GetRmGDestRomOfPCConfig()[(Index)].RmGDestTpTxStateRamIdxOfRmGDestRom)
#define PduR_GetTpTxInstSmStateOfRmGDestTpTxStateRam(Index)                                         (PduR_GetRmGDestTpTxStateRamOfPCConfig()[(Index)].TpTxInstSmStateOfRmGDestTpTxStateRam)
#define PduR_GetLockRomIdxOfRmSrcRom(Index)                                                         (PduR_GetRmSrcRomOfPCConfig()[(Index)].LockRomIdxOfRmSrcRom)
#define PduR_GetMaskedBitsOfRmSrcRom(Index)                                                         (PduR_GetRmSrcRomOfPCConfig()[(Index)].MaskedBitsOfRmSrcRom)
#define PduR_GetMmRomIdxOfRmSrcRom(Index)                                                           (PduR_GetRmSrcRomOfPCConfig()[(Index)].MmRomIdxOfRmSrcRom)
#define PduR_GetRmBufferedTpPropertiesRomIdxOfRmSrcRom(Index)                                       (PduR_GetRmSrcRomOfPCConfig()[(Index)].RmBufferedTpPropertiesRomIdxOfRmSrcRom)
#define PduR_GetRmDestRomEndIdxOfRmSrcRom(Index)                                                    (PduR_GetRmSrcRomOfPCConfig()[(Index)].RmDestRomEndIdxOfRmSrcRom)
#define PduR_GetRmDestRomStartIdxOfRmSrcRom(Index)                                                  (PduR_GetRmSrcRomOfPCConfig()[(Index)].RmDestRomStartIdxOfRmSrcRom)
#define PduR_GetSrcHndOfRmSrcRom(Index)                                                             (PduR_GetRmSrcRomOfPCConfig()[(Index)].SrcHndOfRmSrcRom)
#define PduR_GetFctPtrOfRmTp_CancelReceive_TpRxSmStateHandler(Index)                                (PduR_GetRmTp_CancelReceive_TpRxSmStateHandlerOfPCConfig()[(Index)].FctPtrOfRmTp_CancelReceive_TpRxSmStateHandler)
#define PduR_GetFctPtrOfRmTp_CheckReady2Transmit_TpTxSmStateHandler(Index)                          (PduR_GetRmTp_CheckReady2Transmit_TpTxSmStateHandlerOfPCConfig()[(Index)].FctPtrOfRmTp_CheckReady2Transmit_TpTxSmStateHandler)
#define PduR_GetFctPtrOfRmTp_CopyRxData_TpRxSmStateHandler(Index)                                   (PduR_GetRmTp_CopyRxData_TpRxSmStateHandlerOfPCConfig()[(Index)].FctPtrOfRmTp_CopyRxData_TpRxSmStateHandler)
#define PduR_GetFctPtrOfRmTp_FinishReception_TpTxSmStateHandler(Index)                              (PduR_GetRmTp_FinishReception_TpTxSmStateHandlerOfPCConfig()[(Index)].FctPtrOfRmTp_FinishReception_TpTxSmStateHandler)
#define PduR_GetFctPtrOfRmTp_FinishTransmission_TpTxSmStateHandler(Index)                           (PduR_GetRmTp_FinishTransmission_TpTxSmStateHandlerOfPCConfig()[(Index)].FctPtrOfRmTp_FinishTransmission_TpTxSmStateHandler)
#define PduR_GetFctPtrOfRmTp_StartOfReception_TpRxSmStateHandler(Index)                             (PduR_GetRmTp_StartOfReception_TpRxSmStateHandlerOfPCConfig()[(Index)].FctPtrOfRmTp_StartOfReception_TpRxSmStateHandler)
#define PduR_GetFctPtrOfRmTp_TpRxIndication_TpRxSmStateHandler(Index)                               (PduR_GetRmTp_TpRxIndication_TpRxSmStateHandlerOfPCConfig()[(Index)].FctPtrOfRmTp_TpRxIndication_TpRxSmStateHandler)
#define PduR_GetRmTransmitFctPtr(Index)                                                             (PduR_GetRmTransmitFctPtrOfPCConfig()[(Index)])
#define PduR_GetPduR_RmTp_TxInst_CancelTransmitOfRmTxInstSmRom(Index)                               (PduR_GetRmTxInstSmRomOfPCConfig()[(Index)].PduR_RmTp_TxInst_CancelTransmitOfRmTxInstSmRom)
#define PduR_GetPduR_RmTp_TxInst_CopyTxDataOfRmTxInstSmRom(Index)                                   (PduR_GetRmTxInstSmRomOfPCConfig()[(Index)].PduR_RmTp_TxInst_CopyTxDataOfRmTxInstSmRom)
#define PduR_GetPduR_RmTp_TxInst_TriggerTransmitOfRmTxInstSmRom(Index)                              (PduR_GetRmTxInstSmRomOfPCConfig()[(Index)].PduR_RmTp_TxInst_TriggerTransmitOfRmTxInstSmRom)
#define PduR_GetPduR_RmTp_TxInst_TxConfirmationOfRmTxInstSmRom(Index)                               (PduR_GetRmTxInstSmRomOfPCConfig()[(Index)].PduR_RmTp_TxInst_TxConfirmationOfRmTxInstSmRom)
#define PduR_GetMaskedBitsOfTx2Lo(Index)                                                            (PduR_GetTx2LoOfPCConfig()[(Index)].MaskedBitsOfTx2Lo)
#define PduR_GetRmTransmitFctPtrIdxOfTx2Lo(Index)                                                   (PduR_GetTx2LoOfPCConfig()[(Index)].RmTransmitFctPtrIdxOfTx2Lo)
#define PduR_GetRmGDestRomIdxOfTxIf2Up(Index)                                                       (PduR_GetTxIf2UpOfPCConfig()[(Index)].RmGDestRomIdxOfTxIf2Up)
#define PduR_IsTxConfirmationUsedOfTxIf2Up(Index)                                                   ((PduR_GetTxIf2UpOfPCConfig()[(Index)].TxConfirmationUsedOfTxIf2Up) != FALSE)
#define PduR_GetRmGDestRomIdxOfTxTp2Src(Index)                                                      (PduR_GetTxTp2SrcOfPCConfig()[(Index)].RmGDestRomIdxOfTxTp2Src)
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCGetBitDataMacros  PduR Get Bit Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read bitcoded data elements.
  \{
*/ 
#define PduR_IsLoTpOfMmRom(Index)                                                                   (PDUR_LOTPOFMMROM_MASK == (PduR_GetMaskedBitsOfMmRom(Index) & PDUR_LOTPOFMMROM_MASK))  /**< Is the module a lower transport protocol module. */
#define PduR_IsUpIfOfMmRom(Index)                                                                   (PDUR_UPIFOFMMROM_MASK == (PduR_GetMaskedBitsOfMmRom(Index) & PDUR_UPIFOFMMROM_MASK))  /**< Is the module a upper communication interface module. */
#define PduR_IsUpTpOfMmRom(Index)                                                                   (PDUR_UPTPOFMMROM_MASK == (PduR_GetMaskedBitsOfMmRom(Index) & PDUR_UPTPOFMMROM_MASK))  /**< Is the module a upper transport protocol module. */
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCGetDeduplicatedDataMacros  PduR Get Deduplicated Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated data elements.
  \{
*/ 
#define PduR_GetBmTxBufferArrayRamLengthOfBmTxBufferRom(Index)                                      ((PduR_BmTxBufferArrayRamLengthOfBmTxBufferRomType)((PduR_GetBmTxBufferArrayRamEndIdxOfBmTxBufferRom(Index) - PduR_GetBmTxBufferArrayRamStartIdxOfBmTxBufferRom(Index))))  /**< the number of relations pointing to PduR_BmTxBufferArrayRam */
#define PduR_GetConfigId()                                                                          PduR_GetConfigIdOfPCConfig()
#define PduR_GetMmRomIndEndIdxOfCoreManagerRom(Index)                                               ((PduR_MmRomIndEndIdxOfCoreManagerRomType)((((PduR_MmRomIndEndIdxOfCoreManagerRomType)(Index)) + 9u)))  /**< the end index of the 0:n relation pointing to PduR_MmRomInd */
#define PduR_GetMmRomIndStartIdxOfCoreManagerRom(Index)                                             ((PduR_MmRomIndStartIdxOfCoreManagerRomType)((Index)))  /**< the start index of the 0:n relation pointing to PduR_MmRomInd */
#define PduR_IsMmRomIndUsedOfCoreManagerRom(Index)                                                  (((boolean)(PduR_GetMmRomIndStartIdxOfCoreManagerRom(Index) != PDUR_NO_MMROMINDSTARTIDXOFCOREMANAGERROM)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to PduR_MmRomInd */
#define PduR_GetBmTxBufferIndRomLengthOfFmFifoRom(Index)                                            ((PduR_BmTxBufferIndRomLengthOfFmFifoRomType)((PduR_GetBmTxBufferIndRomEndIdxOfFmFifoRom(Index) - PduR_GetBmTxBufferIndRomStartIdxOfFmFifoRom(Index))))  /**< the number of relations pointing to PduR_BmTxBufferIndRom */
#define PduR_GetFmFifoElementRamLengthOfFmFifoRom(Index)                                            ((PduR_FmFifoElementRamLengthOfFmFifoRomType)((PduR_GetFmFifoElementRamEndIdxOfFmFifoRom(Index) - PduR_GetFmFifoElementRamStartIdxOfFmFifoRom(Index))))  /**< the number of relations pointing to PduR_FmFifoElementRam */
#define PduR_GetExclusiveAreaRomIdxOfLockRom(Index)                                                 ((PduR_ExclusiveAreaRomIdxOfLockRomType)((Index)))  /**< the index of the 0:1 relation pointing to PduR_ExclusiveAreaRom */
#define PduR_IsExclusiveAreaRomUsedOfLockRom(Index)                                                 (((boolean)(PduR_GetExclusiveAreaRomIdxOfLockRom(Index) != PDUR_NO_EXCLUSIVEAREAROMIDXOFLOCKROM)) != FALSE)  /**< TRUE, if the 0:1 relation has minimum 1 relation pointing to PduR_ExclusiveAreaRom */
#define PduR_IsIfCancelTransmitSupportedOfMmRom(Index)                                              (((boolean)(PduR_GetMaskedBitsOfMmRom(Index) == 0x68u)) != FALSE)  /**< Does the module support the Communication Interface CancelTransmit API. */
#define PduR_IsLoIfOfMmRom(Index)                                                                   (((boolean)(PduR_GetMaskedBitsOfMmRom(Index) >= 0x1Du)) != FALSE)  /**< Is the module a lower communication interface module. */
#define PduR_IsRmGDestRomUsedOfMmRom(Index)                                                         (((boolean)(PduR_GetRmGDestRomStartIdxOfMmRom(Index) != PDUR_NO_RMGDESTROMSTARTIDXOFMMROM)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to PduR_RmGDestRom */
#define PduR_IsTpCancelTransmitSupportedOfMmRom(Index)                                              (((boolean)(PduR_GetMaskedBitsOfMmRom(Index) == 0x1Cu)) != FALSE)  /**< Does the module support the transport protocol CancelTransmit API. */
#define PduR_GetMmRomInd(Index)                                                                     ((PduR_MmRomIndType)((Index)))  /**< the indexes of the 1:1 sorted relation pointing to PduR_MmRom */
#define PduR_IsFmFifoInstanceRomUsedOfRmGDestRom(Index)                                             (((boolean)(PduR_GetFmFifoInstanceRomIdxOfRmGDestRom(Index) != PDUR_NO_FMFIFOINSTANCEROMIDXOFRMGDESTROM)) != FALSE)  /**< TRUE, if the 0:1 relation has minimum 1 relation pointing to PduR_FmFifoInstanceRom */
#define PduR_GetLockRomIdxOfRmGDestRom(Index)                                                       ((PduR_LockRomIdxOfRmGDestRomType)((PduR_GetPduRDestPduProcessingOfRmGDestRom(Index) - 0x01u)))  /**< the index of the 1:1 relation pointing to PduR_LockRom */
#define PduR_IsRmDestRomUsedOfRmGDestRom(Index)                                                     (((boolean)(PduR_GetRmDestRomIdxOfRmGDestRom(Index) != PDUR_NO_RMDESTROMIDXOFRMGDESTROM)) != FALSE)  /**< TRUE, if the 0:1 relation has minimum 1 relation pointing to PduR_RmDestRom */
#define PduR_IsRmGDestTpTxStateRamUsedOfRmGDestRom(Index)                                           (((boolean)(PduR_GetRmGDestTpTxStateRamIdxOfRmGDestRom(Index) != PDUR_NO_RMGDESTTPTXSTATERAMIDXOFRMGDESTROM)) != FALSE)  /**< TRUE, if the 0:1 relation has minimum 1 relation pointing to PduR_RmGDestTpTxStateRam */
#define PduR_IsRmBufferedTpPropertiesRomUsedOfRmSrcRom(Index)                                       (((boolean)(PduR_GetRmBufferedTpPropertiesRomIdxOfRmSrcRom(Index) != PDUR_NO_RMBUFFEREDTPPROPERTIESROMIDXOFRMSRCROM)) != FALSE)  /**< TRUE, if the 0:1 relation has minimum 1 relation pointing to PduR_RmBufferedTpPropertiesRom */
#define PduR_GetRmDestRomLengthOfRmSrcRom(Index)                                                    ((PduR_RmDestRomLengthOfRmSrcRomType)((PduR_GetRmDestRomEndIdxOfRmSrcRom(Index) - PduR_GetRmDestRomStartIdxOfRmSrcRom(Index))))  /**< the number of relations pointing to PduR_RmDestRom */
#define PduR_IsTriggerTransmitSupportedOfRmSrcRom(Index)                                            (((boolean)(PduR_GetMaskedBitsOfRmSrcRom(Index) == 0x02u)) != FALSE)
#define PduR_IsTxConfirmationSupportedOfRmSrcRom(Index)                                             (((boolean)(PduR_GetMaskedBitsOfRmSrcRom(Index) == 0x01u)) != FALSE)
#define PduR_GetRmSrcRomIdxOfRxIf2Dest(Index)                                                       ((PduR_RmSrcRomIdxOfRxIf2DestType)((Index)))  /**< the index of the 1:1 relation pointing to PduR_RmSrcRom */
#define PduR_GetRmSrcRomIdxOfRxTp2Dest(Index)                                                       ((PduR_RmSrcRomIdxOfRxTp2DestType)((((PduR_RmSrcRomIdxOfRxTp2DestType)(Index)) + 173u)))  /**< the index of the 0:1 relation pointing to PduR_RmSrcRom */
#define PduR_IsRmSrcRomUsedOfRxTp2Dest(Index)                                                       (((boolean)(PduR_GetRmSrcRomIdxOfRxTp2Dest(Index) != PDUR_NO_RMSRCROMIDXOFRXTP2DEST)) != FALSE)  /**< TRUE, if the 0:1 relation has minimum 1 relation pointing to PduR_RmSrcRom */
#define PduR_GetSizeOfBmTxBufferArrayRam()                                                          PduR_GetSizeOfBmTxBufferArrayRamOfPCConfig()
#define PduR_GetSizeOfBmTxBufferIndRom()                                                            PduR_GetSizeOfBmTxBufferIndRomOfPCConfig()
#define PduR_GetSizeOfBmTxBufferInstanceRam()                                                       PduR_GetSizeOfBmTxBufferInstanceRamOfPCConfig()
#define PduR_GetSizeOfBmTxBufferInstanceRom()                                                       PduR_GetSizeOfBmTxBufferInstanceRomOfPCConfig()
#define PduR_GetSizeOfBmTxBufferRam()                                                               PduR_GetSizeOfBmTxBufferRamOfPCConfig()
#define PduR_GetSizeOfBmTxBufferRom()                                                               PduR_GetSizeOfBmTxBufferRomOfPCConfig()
#define PduR_GetSizeOfCoreManagerRom()                                                              PduR_GetSizeOfCoreManagerRomOfPCConfig()
#define PduR_GetSizeOfExclusiveAreaRom()                                                            PduR_GetSizeOfExclusiveAreaRomOfPCConfig()
#define PduR_GetSizeOfFmFifoElementRam()                                                            PduR_GetSizeOfFmFifoElementRamOfPCConfig()
#define PduR_GetSizeOfFmFifoInstanceRam()                                                           PduR_GetSizeOfFmFifoInstanceRamOfPCConfig()
#define PduR_GetSizeOfFmFifoInstanceRom()                                                           PduR_GetSizeOfFmFifoInstanceRomOfPCConfig()
#define PduR_GetSizeOfFmFifoRam()                                                                   PduR_GetSizeOfFmFifoRamOfPCConfig()
#define PduR_GetSizeOfFmFifoRom()                                                                   PduR_GetSizeOfFmFifoRomOfPCConfig()
#define PduR_GetSizeOfFm_ActivateNext_FmSmStateHandler()                                            PduR_GetSizeOfFm_ActivateNext_FmSmStateHandlerOfPCConfig()
#define PduR_GetSizeOfFm_ActivateRead_FmSmStateHandler()                                            PduR_GetSizeOfFm_ActivateRead_FmSmStateHandlerOfPCConfig()
#define PduR_GetSizeOfFm_ActivateWrite_FmSmStateHandler()                                           PduR_GetSizeOfFm_ActivateWrite_FmSmStateHandlerOfPCConfig()
#define PduR_GetSizeOfFm_FinishRead_FmSmStateHandler()                                              PduR_GetSizeOfFm_FinishRead_FmSmStateHandlerOfPCConfig()
#define PduR_GetSizeOfFm_FinishWrite_FmSmStateHandler()                                             PduR_GetSizeOfFm_FinishWrite_FmSmStateHandlerOfPCConfig()
#define PduR_GetSizeOfGeneralPropertiesRom()                                                        PduR_GetSizeOfGeneralPropertiesRomOfPCConfig()
#define PduR_GetSizeOfLockRom()                                                                     PduR_GetSizeOfLockRomOfPCConfig()
#define PduR_GetSizeOfMmRom()                                                                       PduR_GetSizeOfMmRomOfPCConfig()
#define PduR_GetSizeOfMmRomInd()                                                                    PduR_GetSizeOfMmRomIndOfPCConfig()
#define PduR_GetSizeOfRmBufferedTpPropertiesRam()                                                   PduR_GetSizeOfRmBufferedTpPropertiesRamOfPCConfig()
#define PduR_GetSizeOfRmBufferedTpPropertiesRom()                                                   PduR_GetSizeOfRmBufferedTpPropertiesRomOfPCConfig()
#define PduR_GetSizeOfRmDestRom()                                                                   PduR_GetSizeOfRmDestRomOfPCConfig()
#define PduR_GetSizeOfRmGDestRom()                                                                  PduR_GetSizeOfRmGDestRomOfPCConfig()
#define PduR_GetSizeOfRmGDestTpTxStateRam()                                                         PduR_GetSizeOfRmGDestTpTxStateRamOfPCConfig()
#define PduR_GetSizeOfRmSrcRom()                                                                    PduR_GetSizeOfRmSrcRomOfPCConfig()
#define PduR_GetSizeOfRmTp_CancelReceive_TpRxSmStateHandler()                                       PduR_GetSizeOfRmTp_CancelReceive_TpRxSmStateHandlerOfPCConfig()
#define PduR_GetSizeOfRmTp_CheckReady2Transmit_TpTxSmStateHandler()                                 PduR_GetSizeOfRmTp_CheckReady2Transmit_TpTxSmStateHandlerOfPCConfig()
#define PduR_GetSizeOfRmTp_CopyRxData_TpRxSmStateHandler()                                          PduR_GetSizeOfRmTp_CopyRxData_TpRxSmStateHandlerOfPCConfig()
#define PduR_GetSizeOfRmTp_FinishReception_TpTxSmStateHandler()                                     PduR_GetSizeOfRmTp_FinishReception_TpTxSmStateHandlerOfPCConfig()
#define PduR_GetSizeOfRmTp_FinishTransmission_TpTxSmStateHandler()                                  PduR_GetSizeOfRmTp_FinishTransmission_TpTxSmStateHandlerOfPCConfig()
#define PduR_GetSizeOfRmTp_StartOfReception_TpRxSmStateHandler()                                    PduR_GetSizeOfRmTp_StartOfReception_TpRxSmStateHandlerOfPCConfig()
#define PduR_GetSizeOfRmTp_TpRxIndication_TpRxSmStateHandler()                                      PduR_GetSizeOfRmTp_TpRxIndication_TpRxSmStateHandlerOfPCConfig()
#define PduR_GetSizeOfRmTransmitFctPtr()                                                            PduR_GetSizeOfRmTransmitFctPtrOfPCConfig()
#define PduR_GetSizeOfRmTxInstSmRom()                                                               PduR_GetSizeOfRmTxInstSmRomOfPCConfig()
#define PduR_GetSizeOfRxIf2Dest()                                                                   PduR_GetSizeOfRxIf2DestOfPCConfig()
#define PduR_GetSizeOfRxTp2Dest()                                                                   PduR_GetSizeOfRxTp2DestOfPCConfig()
#define PduR_GetSizeOfTx2Lo()                                                                       PduR_GetSizeOfTx2LoOfPCConfig()
#define PduR_GetSizeOfTxIf2Up()                                                                     PduR_GetSizeOfTxIf2UpOfPCConfig()
#define PduR_GetSizeOfTxTp2Src()                                                                    PduR_GetSizeOfTxTp2SrcOfPCConfig()
#define PduR_IsCancelTransmitUsedOfTx2Lo(Index)                                                     (((boolean)(PduR_GetMaskedBitsOfTx2Lo(Index) == 0x03u)) != FALSE)  /**< TRUE if the routing can use the CancelTransmit API */
#define PduR_GetRmSrcRomIdxOfTx2Lo(Index)                                                           ((PduR_RmSrcRomIdxOfTx2LoType)((((PduR_RmSrcRomIdxOfTx2LoType)(Index)) + 374u)))  /**< the index of the 0:1 relation pointing to PduR_RmSrcRom */
#define PduR_IsRmSrcRomUsedOfTx2Lo(Index)                                                           (((boolean)(PduR_GetRmSrcRomIdxOfTx2Lo(Index) != PDUR_NO_RMSRCROMIDXOFTX2LO)) != FALSE)  /**< TRUE, if the 0:1 relation has minimum 1 relation pointing to PduR_RmSrcRom */
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCSetDataMacros  PduR Set Data Macros (PRE_COMPILE)
  \brief  These macros can be used to write data.
  \{
*/ 
#define PduR_SetBmTxBufferArrayRam(Index, Value)                                                    PduR_GetBmTxBufferArrayRamOfPCConfig()[(Index)] = (Value)
#define PduR_SetBmTxBufferArrayRamReadIdxOfBmTxBufferInstanceRam(Index, Value)                      PduR_GetBmTxBufferInstanceRamOfPCConfig()[(Index)].BmTxBufferArrayRamReadIdxOfBmTxBufferInstanceRam = (Value)
#define PduR_SetBmTxBufferArrayRamWriteIdxOfBmTxBufferInstanceRam(Index, Value)                     PduR_GetBmTxBufferInstanceRamOfPCConfig()[(Index)].BmTxBufferArrayRamWriteIdxOfBmTxBufferInstanceRam = (Value)
#define PduR_SetPduRBufferStateOfBmTxBufferInstanceRam(Index, Value)                                PduR_GetBmTxBufferInstanceRamOfPCConfig()[(Index)].PduRBufferStateOfBmTxBufferInstanceRam = (Value)
#define PduR_SetTxBufferUsedOfBmTxBufferInstanceRam(Index, Value)                                   PduR_GetBmTxBufferInstanceRamOfPCConfig()[(Index)].TxBufferUsedOfBmTxBufferInstanceRam = (Value)
#define PduR_SetAllocatedOfBmTxBufferRam(Index, Value)                                              PduR_GetBmTxBufferRamOfPCConfig()[(Index)].AllocatedOfBmTxBufferRam = (Value)
#define PduR_SetBmTxBufferArrayRamInstanceStopIdxOfBmTxBufferRam(Index, Value)                      PduR_GetBmTxBufferRamOfPCConfig()[(Index)].BmTxBufferArrayRamInstanceStopIdxOfBmTxBufferRam = (Value)
#define PduR_SetBmTxBufferArrayRamReadIdxOfBmTxBufferRam(Index, Value)                              PduR_GetBmTxBufferRamOfPCConfig()[(Index)].BmTxBufferArrayRamReadIdxOfBmTxBufferRam = (Value)
#define PduR_SetBmTxBufferArrayRamWriteIdxOfBmTxBufferRam(Index, Value)                             PduR_GetBmTxBufferRamOfPCConfig()[(Index)].BmTxBufferArrayRamWriteIdxOfBmTxBufferRam = (Value)
#define PduR_SetPduRBufferStateOfBmTxBufferRam(Index, Value)                                        PduR_GetBmTxBufferRamOfPCConfig()[(Index)].PduRBufferStateOfBmTxBufferRam = (Value)
#define PduR_SetRxLengthOfBmTxBufferRam(Index, Value)                                               PduR_GetBmTxBufferRamOfPCConfig()[(Index)].RxLengthOfBmTxBufferRam = (Value)
#define PduR_SetBmTxBufferRomIdxOfFmFifoElementRam(Index, Value)                                    PduR_GetFmFifoElementRamOfPCConfig()[(Index)].BmTxBufferRomIdxOfFmFifoElementRam = (Value)
#define PduR_SetDedicatedTxBufferOfFmFifoElementRam(Index, Value)                                   PduR_GetFmFifoElementRamOfPCConfig()[(Index)].DedicatedTxBufferOfFmFifoElementRam = (Value)
#define PduR_SetRmDestRomIdxOfFmFifoElementRam(Index, Value)                                        PduR_GetFmFifoElementRamOfPCConfig()[(Index)].RmDestRomIdxOfFmFifoElementRam = (Value)
#define PduR_SetStateOfFmFifoElementRam(Index, Value)                                               PduR_GetFmFifoElementRamOfPCConfig()[(Index)].StateOfFmFifoElementRam = (Value)
#define PduR_SetBmTxBufferInstanceRomIdxOfFmFifoInstanceRam(Index, Value)                           PduR_GetFmFifoInstanceRamOfPCConfig()[(Index)].BmTxBufferInstanceRomIdxOfFmFifoInstanceRam = (Value)
#define PduR_SetFillLevelOfFmFifoRam(Index, Value)                                                  PduR_GetFmFifoRamOfPCConfig()[(Index)].FillLevelOfFmFifoRam = (Value)
#define PduR_SetFmFifoElementRamReadIdxOfFmFifoRam(Index, Value)                                    PduR_GetFmFifoRamOfPCConfig()[(Index)].FmFifoElementRamReadIdxOfFmFifoRam = (Value)
#define PduR_SetFmFifoElementRamWriteIdxOfFmFifoRam(Index, Value)                                   PduR_GetFmFifoRamOfPCConfig()[(Index)].FmFifoElementRamWriteIdxOfFmFifoRam = (Value)
#define PduR_SetPendingConfirmationsOfFmFifoRam(Index, Value)                                       PduR_GetFmFifoRamOfPCConfig()[(Index)].PendingConfirmationsOfFmFifoRam = (Value)
#define PduR_SetTpTxSmStateOfFmFifoRam(Index, Value)                                                PduR_GetFmFifoRamOfPCConfig()[(Index)].TpTxSmStateOfFmFifoRam = (Value)
#define PduR_SetInitialized(Value)                                                                  (*(PduR_GetInitializedOfPCConfig())) = (Value)
#define PduR_SetFmFifoElementRamIdxOfRmBufferedTpPropertiesRam(Index, Value)                        PduR_GetRmBufferedTpPropertiesRamOfPCConfig()[(Index)].FmFifoElementRamIdxOfRmBufferedTpPropertiesRam = (Value)
#define PduR_SetTpRxSmStateOfRmBufferedTpPropertiesRam(Index, Value)                                PduR_GetRmBufferedTpPropertiesRamOfPCConfig()[(Index)].TpRxSmStateOfRmBufferedTpPropertiesRam = (Value)
#define PduR_SetTpTxInstSmStateOfRmGDestTpTxStateRam(Index, Value)                                  PduR_GetRmGDestTpTxStateRamOfPCConfig()[(Index)].TpTxInstSmStateOfRmGDestTpTxStateRam = (Value)
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCGetAddressOfDataMacros  PduR Get Address Of Data Macros (PRE_COMPILE)
  \brief  These macros can be used to get the data by the address operator.
  \{
*/ 
#define PduR_GetAddrBmTxBufferArrayRam(Index)                                                       (&PduR_GetBmTxBufferArrayRam(Index))
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCHasMacros  PduR Has Macros (PRE_COMPILE)
  \brief  These macros can be used to detect at runtime a deactivated piece of information. TRUE in the CONFIGURATION_VARIANT PRE-COMPILE, TRUE or FALSE in the CONFIGURATION_VARIANT POST-BUILD.
  \{
*/ 
#define PduR_HasBmTxBufferArrayRam()                                                                (TRUE != FALSE)
#define PduR_HasBmTxBufferIndRom()                                                                  (TRUE != FALSE)
#define PduR_HasBmTxBufferRomIdxOfBmTxBufferIndRom()                                                (TRUE != FALSE)
#define PduR_HasBmTxBufferInstanceRam()                                                             (TRUE != FALSE)
#define PduR_HasBmTxBufferArrayRamReadIdxOfBmTxBufferInstanceRam()                                  (TRUE != FALSE)
#define PduR_HasBmTxBufferArrayRamWriteIdxOfBmTxBufferInstanceRam()                                 (TRUE != FALSE)
#define PduR_HasPduRBufferStateOfBmTxBufferInstanceRam()                                            (TRUE != FALSE)
#define PduR_HasTxBufferUsedOfBmTxBufferInstanceRam()                                               (TRUE != FALSE)
#define PduR_HasBmTxBufferInstanceRom()                                                             (TRUE != FALSE)
#define PduR_HasBmTxBufferRomIdxOfBmTxBufferInstanceRom()                                           (TRUE != FALSE)
#define PduR_HasBmTxBufferRam()                                                                     (TRUE != FALSE)
#define PduR_HasAllocatedOfBmTxBufferRam()                                                          (TRUE != FALSE)
#define PduR_HasBmTxBufferArrayRamInstanceStopIdxOfBmTxBufferRam()                                  (TRUE != FALSE)
#define PduR_HasBmTxBufferArrayRamReadIdxOfBmTxBufferRam()                                          (TRUE != FALSE)
#define PduR_HasBmTxBufferArrayRamWriteIdxOfBmTxBufferRam()                                         (TRUE != FALSE)
#define PduR_HasPduRBufferStateOfBmTxBufferRam()                                                    (TRUE != FALSE)
#define PduR_HasRxLengthOfBmTxBufferRam()                                                           (TRUE != FALSE)
#define PduR_HasBmTxBufferRom()                                                                     (TRUE != FALSE)
#define PduR_HasBmTxBufferArrayRamEndIdxOfBmTxBufferRom()                                           (TRUE != FALSE)
#define PduR_HasBmTxBufferArrayRamLengthOfBmTxBufferRom()                                           (TRUE != FALSE)
#define PduR_HasBmTxBufferArrayRamStartIdxOfBmTxBufferRom()                                         (TRUE != FALSE)
#define PduR_HasBmTxBufferInstanceRomEndIdxOfBmTxBufferRom()                                        (TRUE != FALSE)
#define PduR_HasBmTxBufferInstanceRomStartIdxOfBmTxBufferRom()                                      (TRUE != FALSE)
#define PduR_HasConfigId()                                                                          (TRUE != FALSE)
#define PduR_HasCoreManagerRom()                                                                    (TRUE != FALSE)
#define PduR_HasMmRomIndEndIdxOfCoreManagerRom()                                                    (TRUE != FALSE)
#define PduR_HasMmRomIndStartIdxOfCoreManagerRom()                                                  (TRUE != FALSE)
#define PduR_HasMmRomIndUsedOfCoreManagerRom()                                                      (TRUE != FALSE)
#define PduR_HasExclusiveAreaRom()                                                                  (TRUE != FALSE)
#define PduR_HasLockOfExclusiveAreaRom()                                                            (TRUE != FALSE)
#define PduR_HasUnlockOfExclusiveAreaRom()                                                          (TRUE != FALSE)
#define PduR_HasFmFifoElementRam()                                                                  (TRUE != FALSE)
#define PduR_HasBmTxBufferRomIdxOfFmFifoElementRam()                                                (TRUE != FALSE)
#define PduR_HasDedicatedTxBufferOfFmFifoElementRam()                                               (TRUE != FALSE)
#define PduR_HasRmDestRomIdxOfFmFifoElementRam()                                                    (TRUE != FALSE)
#define PduR_HasStateOfFmFifoElementRam()                                                           (TRUE != FALSE)
#define PduR_HasFmFifoInstanceRam()                                                                 (TRUE != FALSE)
#define PduR_HasBmTxBufferInstanceRomIdxOfFmFifoInstanceRam()                                       (TRUE != FALSE)
#define PduR_HasFmFifoInstanceRom()                                                                 (TRUE != FALSE)
#define PduR_HasFmFifoRomIdxOfFmFifoInstanceRom()                                                   (TRUE != FALSE)
#define PduR_HasFmFifoRam()                                                                         (TRUE != FALSE)
#define PduR_HasFillLevelOfFmFifoRam()                                                              (TRUE != FALSE)
#define PduR_HasFmFifoElementRamReadIdxOfFmFifoRam()                                                (TRUE != FALSE)
#define PduR_HasFmFifoElementRamWriteIdxOfFmFifoRam()                                               (TRUE != FALSE)
#define PduR_HasPendingConfirmationsOfFmFifoRam()                                                   (TRUE != FALSE)
#define PduR_HasTpTxSmStateOfFmFifoRam()                                                            (TRUE != FALSE)
#define PduR_HasFmFifoRom()                                                                         (TRUE != FALSE)
#define PduR_HasBmTxBufferIndRomEndIdxOfFmFifoRom()                                                 (TRUE != FALSE)
#define PduR_HasBmTxBufferIndRomLengthOfFmFifoRom()                                                 (TRUE != FALSE)
#define PduR_HasBmTxBufferIndRomStartIdxOfFmFifoRom()                                               (TRUE != FALSE)
#define PduR_HasFmFifoElementRamEndIdxOfFmFifoRom()                                                 (TRUE != FALSE)
#define PduR_HasFmFifoElementRamLengthOfFmFifoRom()                                                 (TRUE != FALSE)
#define PduR_HasFmFifoElementRamStartIdxOfFmFifoRom()                                               (TRUE != FALSE)
#define PduR_HasFm_ActivateNext_FmSmStateHandler()                                                  (TRUE != FALSE)
#define PduR_HasFctPtrOfFm_ActivateNext_FmSmStateHandler()                                          (TRUE != FALSE)
#define PduR_HasFm_ActivateRead_FmSmStateHandler()                                                  (TRUE != FALSE)
#define PduR_HasFctPtrOfFm_ActivateRead_FmSmStateHandler()                                          (TRUE != FALSE)
#define PduR_HasFm_ActivateWrite_FmSmStateHandler()                                                 (TRUE != FALSE)
#define PduR_HasFctPtrOfFm_ActivateWrite_FmSmStateHandler()                                         (TRUE != FALSE)
#define PduR_HasFm_FinishRead_FmSmStateHandler()                                                    (TRUE != FALSE)
#define PduR_HasFctPtrOfFm_FinishRead_FmSmStateHandler()                                            (TRUE != FALSE)
#define PduR_HasFm_FinishWrite_FmSmStateHandler()                                                   (TRUE != FALSE)
#define PduR_HasFctPtrOfFm_FinishWrite_FmSmStateHandler()                                           (TRUE != FALSE)
#define PduR_HasGeneralPropertiesRom()                                                              (TRUE != FALSE)
#define PduR_HashasTpTxBufferedForwardingOfGeneralPropertiesRom()                                   (TRUE != FALSE)
#define PduR_HasInitialized()                                                                       (TRUE != FALSE)
#define PduR_HasLockRom()                                                                           (TRUE != FALSE)
#define PduR_HasExclusiveAreaRomIdxOfLockRom()                                                      (TRUE != FALSE)
#define PduR_HasExclusiveAreaRomUsedOfLockRom()                                                     (TRUE != FALSE)
#define PduR_HasMmRom()                                                                             (TRUE != FALSE)
#define PduR_HasCoreManagerRomIdxOfMmRom()                                                          (TRUE != FALSE)
#define PduR_HasIfCancelTransmitSupportedOfMmRom()                                                  (TRUE != FALSE)
#define PduR_HasLoIfCancelTransmitFctPtrOfMmRom()                                                   (TRUE != FALSE)
#define PduR_HasLoIfOfMmRom()                                                                       (TRUE != FALSE)
#define PduR_HasLoIfTransmitFctPtrOfMmRom()                                                         (TRUE != FALSE)
#define PduR_HasLoTpCancelTransmitFctPtrOfMmRom()                                                   (TRUE != FALSE)
#define PduR_HasLoTpOfMmRom()                                                                       (TRUE != FALSE)
#define PduR_HasLoTpTransmitFctPtrOfMmRom()                                                         (TRUE != FALSE)
#define PduR_HasMaskedBitsOfMmRom()                                                                 (TRUE != FALSE)
#define PduR_HasRmGDestRomEndIdxOfMmRom()                                                           (TRUE != FALSE)
#define PduR_HasRmGDestRomStartIdxOfMmRom()                                                         (TRUE != FALSE)
#define PduR_HasRmGDestRomUsedOfMmRom()                                                             (TRUE != FALSE)
#define PduR_HasTpCancelTransmitSupportedOfMmRom()                                                  (TRUE != FALSE)
#define PduR_HasUpIfOfMmRom()                                                                       (TRUE != FALSE)
#define PduR_HasUpIfRxIndicationFctPtrOfMmRom()                                                     (TRUE != FALSE)
#define PduR_HasUpIfTriggerTransmitFctPtrOfMmRom()                                                  (TRUE != FALSE)
#define PduR_HasUpIfTxConfirmationFctPtrOfMmRom()                                                   (TRUE != FALSE)
#define PduR_HasUpTpCopyRxDataFctPtrOfMmRom()                                                       (TRUE != FALSE)
#define PduR_HasUpTpCopyTxDataFctPtrOfMmRom()                                                       (TRUE != FALSE)
#define PduR_HasUpTpOfMmRom()                                                                       (TRUE != FALSE)
#define PduR_HasUpTpStartOfReceptionFctPtrOfMmRom()                                                 (TRUE != FALSE)
#define PduR_HasUpTpTpRxIndicationFctPtrOfMmRom()                                                   (TRUE != FALSE)
#define PduR_HasUpTpTpTxConfirmationFctPtrOfMmRom()                                                 (TRUE != FALSE)
#define PduR_HasMmRomInd()                                                                          (TRUE != FALSE)
#define PduR_HasRmBufferedTpPropertiesRam()                                                         (TRUE != FALSE)
#define PduR_HasFmFifoElementRamIdxOfRmBufferedTpPropertiesRam()                                    (TRUE != FALSE)
#define PduR_HasTpRxSmStateOfRmBufferedTpPropertiesRam()                                            (TRUE != FALSE)
#define PduR_HasRmBufferedTpPropertiesRom()                                                         (TRUE != FALSE)
#define PduR_HasDedicatedTxBufferOfRmBufferedTpPropertiesRom()                                      (TRUE != FALSE)
#define PduR_HasFmFifoRomIdxOfRmBufferedTpPropertiesRom()                                           (TRUE != FALSE)
#define PduR_HasQueuedDestCntOfRmBufferedTpPropertiesRom()                                          (TRUE != FALSE)
#define PduR_HasTpThresholdOfRmBufferedTpPropertiesRom()                                            (TRUE != FALSE)
#define PduR_HasRmDestRom()                                                                         (TRUE != FALSE)
#define PduR_HasRmGDestRomIdxOfRmDestRom()                                                          (TRUE != FALSE)
#define PduR_HasRmSrcRomIdxOfRmDestRom()                                                            (TRUE != FALSE)
#define PduR_HasRoutingTypeOfRmDestRom()                                                            (TRUE != FALSE)
#define PduR_HasRmGDestRom()                                                                        (TRUE != FALSE)
#define PduR_HasDestHndOfRmGDestRom()                                                               (TRUE != FALSE)
#define PduR_HasDirectionOfRmGDestRom()                                                             (TRUE != FALSE)
#define PduR_HasFmFifoInstanceRomIdxOfRmGDestRom()                                                  (TRUE != FALSE)
#define PduR_HasFmFifoInstanceRomUsedOfRmGDestRom()                                                 (TRUE != FALSE)
#define PduR_HasLockRomIdxOfRmGDestRom()                                                            (TRUE != FALSE)
#define PduR_HasMaskedBitsOfRmGDestRom()                                                            (TRUE != FALSE)
#define PduR_HasMmRomIdxOfRmGDestRom()                                                              (TRUE != FALSE)
#define PduR_HasPduRDestPduProcessingOfRmGDestRom()                                                 (TRUE != FALSE)
#define PduR_HasRmDestRomIdxOfRmGDestRom()                                                          (TRUE != FALSE)
#define PduR_HasRmDestRomUsedOfRmGDestRom()                                                         (TRUE != FALSE)
#define PduR_HasRmGDestTpTxStateRamIdxOfRmGDestRom()                                                (TRUE != FALSE)
#define PduR_HasRmGDestTpTxStateRamUsedOfRmGDestRom()                                               (TRUE != FALSE)
#define PduR_HasRmGDestTpTxStateRam()                                                               (TRUE != FALSE)
#define PduR_HasTpTxInstSmStateOfRmGDestTpTxStateRam()                                              (TRUE != FALSE)
#define PduR_HasRmSrcRom()                                                                          (TRUE != FALSE)
#define PduR_HasLockRomIdxOfRmSrcRom()                                                              (TRUE != FALSE)
#define PduR_HasMaskedBitsOfRmSrcRom()                                                              (TRUE != FALSE)
#define PduR_HasMmRomIdxOfRmSrcRom()                                                                (TRUE != FALSE)
#define PduR_HasRmBufferedTpPropertiesRomIdxOfRmSrcRom()                                            (TRUE != FALSE)
#define PduR_HasRmBufferedTpPropertiesRomUsedOfRmSrcRom()                                           (TRUE != FALSE)
#define PduR_HasRmDestRomEndIdxOfRmSrcRom()                                                         (TRUE != FALSE)
#define PduR_HasRmDestRomLengthOfRmSrcRom()                                                         (TRUE != FALSE)
#define PduR_HasRmDestRomStartIdxOfRmSrcRom()                                                       (TRUE != FALSE)
#define PduR_HasSrcHndOfRmSrcRom()                                                                  (TRUE != FALSE)
#define PduR_HasTriggerTransmitSupportedOfRmSrcRom()                                                (TRUE != FALSE)
#define PduR_HasTxConfirmationSupportedOfRmSrcRom()                                                 (TRUE != FALSE)
#define PduR_HasRmTp_CancelReceive_TpRxSmStateHandler()                                             (TRUE != FALSE)
#define PduR_HasFctPtrOfRmTp_CancelReceive_TpRxSmStateHandler()                                     (TRUE != FALSE)
#define PduR_HasRmTp_CheckReady2Transmit_TpTxSmStateHandler()                                       (TRUE != FALSE)
#define PduR_HasFctPtrOfRmTp_CheckReady2Transmit_TpTxSmStateHandler()                               (TRUE != FALSE)
#define PduR_HasRmTp_CopyRxData_TpRxSmStateHandler()                                                (TRUE != FALSE)
#define PduR_HasFctPtrOfRmTp_CopyRxData_TpRxSmStateHandler()                                        (TRUE != FALSE)
#define PduR_HasRmTp_FinishReception_TpTxSmStateHandler()                                           (TRUE != FALSE)
#define PduR_HasFctPtrOfRmTp_FinishReception_TpTxSmStateHandler()                                   (TRUE != FALSE)
#define PduR_HasRmTp_FinishTransmission_TpTxSmStateHandler()                                        (TRUE != FALSE)
#define PduR_HasFctPtrOfRmTp_FinishTransmission_TpTxSmStateHandler()                                (TRUE != FALSE)
#define PduR_HasRmTp_StartOfReception_TpRxSmStateHandler()                                          (TRUE != FALSE)
#define PduR_HasFctPtrOfRmTp_StartOfReception_TpRxSmStateHandler()                                  (TRUE != FALSE)
#define PduR_HasRmTp_TpRxIndication_TpRxSmStateHandler()                                            (TRUE != FALSE)
#define PduR_HasFctPtrOfRmTp_TpRxIndication_TpRxSmStateHandler()                                    (TRUE != FALSE)
#define PduR_HasRmTransmitFctPtr()                                                                  (TRUE != FALSE)
#define PduR_HasRmTxInstSmRom()                                                                     (TRUE != FALSE)
#define PduR_HasPduR_RmTp_TxInst_CancelTransmitOfRmTxInstSmRom()                                    (TRUE != FALSE)
#define PduR_HasPduR_RmTp_TxInst_CopyTxDataOfRmTxInstSmRom()                                        (TRUE != FALSE)
#define PduR_HasPduR_RmTp_TxInst_TriggerTransmitOfRmTxInstSmRom()                                   (TRUE != FALSE)
#define PduR_HasPduR_RmTp_TxInst_TxConfirmationOfRmTxInstSmRom()                                    (TRUE != FALSE)
#define PduR_HasRxIf2Dest()                                                                         (TRUE != FALSE)
#define PduR_HasRmSrcRomIdxOfRxIf2Dest()                                                            (TRUE != FALSE)
#define PduR_HasRxTp2Dest()                                                                         (TRUE != FALSE)
#define PduR_HasRmSrcRomIdxOfRxTp2Dest()                                                            (TRUE != FALSE)
#define PduR_HasRmSrcRomUsedOfRxTp2Dest()                                                           (TRUE != FALSE)
#define PduR_HasSizeOfBmTxBufferArrayRam()                                                          (TRUE != FALSE)
#define PduR_HasSizeOfBmTxBufferIndRom()                                                            (TRUE != FALSE)
#define PduR_HasSizeOfBmTxBufferInstanceRam()                                                       (TRUE != FALSE)
#define PduR_HasSizeOfBmTxBufferInstanceRom()                                                       (TRUE != FALSE)
#define PduR_HasSizeOfBmTxBufferRam()                                                               (TRUE != FALSE)
#define PduR_HasSizeOfBmTxBufferRom()                                                               (TRUE != FALSE)
#define PduR_HasSizeOfCoreManagerRom()                                                              (TRUE != FALSE)
#define PduR_HasSizeOfExclusiveAreaRom()                                                            (TRUE != FALSE)
#define PduR_HasSizeOfFmFifoElementRam()                                                            (TRUE != FALSE)
#define PduR_HasSizeOfFmFifoInstanceRam()                                                           (TRUE != FALSE)
#define PduR_HasSizeOfFmFifoInstanceRom()                                                           (TRUE != FALSE)
#define PduR_HasSizeOfFmFifoRam()                                                                   (TRUE != FALSE)
#define PduR_HasSizeOfFmFifoRom()                                                                   (TRUE != FALSE)
#define PduR_HasSizeOfFm_ActivateNext_FmSmStateHandler()                                            (TRUE != FALSE)
#define PduR_HasSizeOfFm_ActivateRead_FmSmStateHandler()                                            (TRUE != FALSE)
#define PduR_HasSizeOfFm_ActivateWrite_FmSmStateHandler()                                           (TRUE != FALSE)
#define PduR_HasSizeOfFm_FinishRead_FmSmStateHandler()                                              (TRUE != FALSE)
#define PduR_HasSizeOfFm_FinishWrite_FmSmStateHandler()                                             (TRUE != FALSE)
#define PduR_HasSizeOfGeneralPropertiesRom()                                                        (TRUE != FALSE)
#define PduR_HasSizeOfLockRom()                                                                     (TRUE != FALSE)
#define PduR_HasSizeOfMmRom()                                                                       (TRUE != FALSE)
#define PduR_HasSizeOfMmRomInd()                                                                    (TRUE != FALSE)
#define PduR_HasSizeOfRmBufferedTpPropertiesRam()                                                   (TRUE != FALSE)
#define PduR_HasSizeOfRmBufferedTpPropertiesRom()                                                   (TRUE != FALSE)
#define PduR_HasSizeOfRmDestRom()                                                                   (TRUE != FALSE)
#define PduR_HasSizeOfRmGDestRom()                                                                  (TRUE != FALSE)
#define PduR_HasSizeOfRmGDestTpTxStateRam()                                                         (TRUE != FALSE)
#define PduR_HasSizeOfRmSrcRom()                                                                    (TRUE != FALSE)
#define PduR_HasSizeOfRmTp_CancelReceive_TpRxSmStateHandler()                                       (TRUE != FALSE)
#define PduR_HasSizeOfRmTp_CheckReady2Transmit_TpTxSmStateHandler()                                 (TRUE != FALSE)
#define PduR_HasSizeOfRmTp_CopyRxData_TpRxSmStateHandler()                                          (TRUE != FALSE)
#define PduR_HasSizeOfRmTp_FinishReception_TpTxSmStateHandler()                                     (TRUE != FALSE)
#define PduR_HasSizeOfRmTp_FinishTransmission_TpTxSmStateHandler()                                  (TRUE != FALSE)
#define PduR_HasSizeOfRmTp_StartOfReception_TpRxSmStateHandler()                                    (TRUE != FALSE)
#define PduR_HasSizeOfRmTp_TpRxIndication_TpRxSmStateHandler()                                      (TRUE != FALSE)
#define PduR_HasSizeOfRmTransmitFctPtr()                                                            (TRUE != FALSE)
#define PduR_HasSizeOfRmTxInstSmRom()                                                               (TRUE != FALSE)
#define PduR_HasSizeOfRxIf2Dest()                                                                   (TRUE != FALSE)
#define PduR_HasSizeOfRxTp2Dest()                                                                   (TRUE != FALSE)
#define PduR_HasSizeOfTx2Lo()                                                                       (TRUE != FALSE)
#define PduR_HasSizeOfTxIf2Up()                                                                     (TRUE != FALSE)
#define PduR_HasSizeOfTxTp2Src()                                                                    (TRUE != FALSE)
#define PduR_HasTx2Lo()                                                                             (TRUE != FALSE)
#define PduR_HasCancelTransmitUsedOfTx2Lo()                                                         (TRUE != FALSE)
#define PduR_HasMaskedBitsOfTx2Lo()                                                                 (TRUE != FALSE)
#define PduR_HasRmSrcRomIdxOfTx2Lo()                                                                (TRUE != FALSE)
#define PduR_HasRmSrcRomUsedOfTx2Lo()                                                               (TRUE != FALSE)
#define PduR_HasRmTransmitFctPtrIdxOfTx2Lo()                                                        (TRUE != FALSE)
#define PduR_HasTxIf2Up()                                                                           (TRUE != FALSE)
#define PduR_HasRmGDestRomIdxOfTxIf2Up()                                                            (TRUE != FALSE)
#define PduR_HasTxConfirmationUsedOfTxIf2Up()                                                       (TRUE != FALSE)
#define PduR_HasTxTp2Src()                                                                          (TRUE != FALSE)
#define PduR_HasRmGDestRomIdxOfTxTp2Src()                                                           (TRUE != FALSE)
#define PduR_HasPCConfig()                                                                          (TRUE != FALSE)
#define PduR_HasBmTxBufferArrayRamOfPCConfig()                                                      (TRUE != FALSE)
#define PduR_HasBmTxBufferIndRomOfPCConfig()                                                        (TRUE != FALSE)
#define PduR_HasBmTxBufferInstanceRamOfPCConfig()                                                   (TRUE != FALSE)
#define PduR_HasBmTxBufferInstanceRomOfPCConfig()                                                   (TRUE != FALSE)
#define PduR_HasBmTxBufferRamOfPCConfig()                                                           (TRUE != FALSE)
#define PduR_HasBmTxBufferRomOfPCConfig()                                                           (TRUE != FALSE)
#define PduR_HasConfigIdOfPCConfig()                                                                (TRUE != FALSE)
#define PduR_HasCoreManagerRomOfPCConfig()                                                          (TRUE != FALSE)
#define PduR_HasExclusiveAreaRomOfPCConfig()                                                        (TRUE != FALSE)
#define PduR_HasFmFifoElementRamOfPCConfig()                                                        (TRUE != FALSE)
#define PduR_HasFmFifoInstanceRamOfPCConfig()                                                       (TRUE != FALSE)
#define PduR_HasFmFifoInstanceRomOfPCConfig()                                                       (TRUE != FALSE)
#define PduR_HasFmFifoRamOfPCConfig()                                                               (TRUE != FALSE)
#define PduR_HasFmFifoRomOfPCConfig()                                                               (TRUE != FALSE)
#define PduR_HasFm_ActivateNext_FmSmStateHandlerOfPCConfig()                                        (TRUE != FALSE)
#define PduR_HasFm_ActivateRead_FmSmStateHandlerOfPCConfig()                                        (TRUE != FALSE)
#define PduR_HasFm_ActivateWrite_FmSmStateHandlerOfPCConfig()                                       (TRUE != FALSE)
#define PduR_HasFm_FinishRead_FmSmStateHandlerOfPCConfig()                                          (TRUE != FALSE)
#define PduR_HasFm_FinishWrite_FmSmStateHandlerOfPCConfig()                                         (TRUE != FALSE)
#define PduR_HasGeneralPropertiesRomOfPCConfig()                                                    (TRUE != FALSE)
#define PduR_HasInitializedOfPCConfig()                                                             (TRUE != FALSE)
#define PduR_HasLockRomOfPCConfig()                                                                 (TRUE != FALSE)
#define PduR_HasMmRomIndOfPCConfig()                                                                (TRUE != FALSE)
#define PduR_HasMmRomOfPCConfig()                                                                   (TRUE != FALSE)
#define PduR_HasRmBufferedTpPropertiesRamOfPCConfig()                                               (TRUE != FALSE)
#define PduR_HasRmBufferedTpPropertiesRomOfPCConfig()                                               (TRUE != FALSE)
#define PduR_HasRmDestRomOfPCConfig()                                                               (TRUE != FALSE)
#define PduR_HasRmGDestRomOfPCConfig()                                                              (TRUE != FALSE)
#define PduR_HasRmGDestTpTxStateRamOfPCConfig()                                                     (TRUE != FALSE)
#define PduR_HasRmSrcRomOfPCConfig()                                                                (TRUE != FALSE)
#define PduR_HasRmTp_CancelReceive_TpRxSmStateHandlerOfPCConfig()                                   (TRUE != FALSE)
#define PduR_HasRmTp_CheckReady2Transmit_TpTxSmStateHandlerOfPCConfig()                             (TRUE != FALSE)
#define PduR_HasRmTp_CopyRxData_TpRxSmStateHandlerOfPCConfig()                                      (TRUE != FALSE)
#define PduR_HasRmTp_FinishReception_TpTxSmStateHandlerOfPCConfig()                                 (TRUE != FALSE)
#define PduR_HasRmTp_FinishTransmission_TpTxSmStateHandlerOfPCConfig()                              (TRUE != FALSE)
#define PduR_HasRmTp_StartOfReception_TpRxSmStateHandlerOfPCConfig()                                (TRUE != FALSE)
#define PduR_HasRmTp_TpRxIndication_TpRxSmStateHandlerOfPCConfig()                                  (TRUE != FALSE)
#define PduR_HasRmTransmitFctPtrOfPCConfig()                                                        (TRUE != FALSE)
#define PduR_HasRmTxInstSmRomOfPCConfig()                                                           (TRUE != FALSE)
#define PduR_HasRxIf2DestOfPCConfig()                                                               (TRUE != FALSE)
#define PduR_HasRxTp2DestOfPCConfig()                                                               (TRUE != FALSE)
#define PduR_HasSizeOfBmTxBufferArrayRamOfPCConfig()                                                (TRUE != FALSE)
#define PduR_HasSizeOfBmTxBufferIndRomOfPCConfig()                                                  (TRUE != FALSE)
#define PduR_HasSizeOfBmTxBufferInstanceRamOfPCConfig()                                             (TRUE != FALSE)
#define PduR_HasSizeOfBmTxBufferInstanceRomOfPCConfig()                                             (TRUE != FALSE)
#define PduR_HasSizeOfBmTxBufferRamOfPCConfig()                                                     (TRUE != FALSE)
#define PduR_HasSizeOfBmTxBufferRomOfPCConfig()                                                     (TRUE != FALSE)
#define PduR_HasSizeOfCoreManagerRomOfPCConfig()                                                    (TRUE != FALSE)
#define PduR_HasSizeOfExclusiveAreaRomOfPCConfig()                                                  (TRUE != FALSE)
#define PduR_HasSizeOfFmFifoElementRamOfPCConfig()                                                  (TRUE != FALSE)
#define PduR_HasSizeOfFmFifoInstanceRamOfPCConfig()                                                 (TRUE != FALSE)
#define PduR_HasSizeOfFmFifoInstanceRomOfPCConfig()                                                 (TRUE != FALSE)
#define PduR_HasSizeOfFmFifoRamOfPCConfig()                                                         (TRUE != FALSE)
#define PduR_HasSizeOfFmFifoRomOfPCConfig()                                                         (TRUE != FALSE)
#define PduR_HasSizeOfFm_ActivateNext_FmSmStateHandlerOfPCConfig()                                  (TRUE != FALSE)
#define PduR_HasSizeOfFm_ActivateRead_FmSmStateHandlerOfPCConfig()                                  (TRUE != FALSE)
#define PduR_HasSizeOfFm_ActivateWrite_FmSmStateHandlerOfPCConfig()                                 (TRUE != FALSE)
#define PduR_HasSizeOfFm_FinishRead_FmSmStateHandlerOfPCConfig()                                    (TRUE != FALSE)
#define PduR_HasSizeOfFm_FinishWrite_FmSmStateHandlerOfPCConfig()                                   (TRUE != FALSE)
#define PduR_HasSizeOfGeneralPropertiesRomOfPCConfig()                                              (TRUE != FALSE)
#define PduR_HasSizeOfLockRomOfPCConfig()                                                           (TRUE != FALSE)
#define PduR_HasSizeOfMmRomIndOfPCConfig()                                                          (TRUE != FALSE)
#define PduR_HasSizeOfMmRomOfPCConfig()                                                             (TRUE != FALSE)
#define PduR_HasSizeOfRmBufferedTpPropertiesRamOfPCConfig()                                         (TRUE != FALSE)
#define PduR_HasSizeOfRmBufferedTpPropertiesRomOfPCConfig()                                         (TRUE != FALSE)
#define PduR_HasSizeOfRmDestRomOfPCConfig()                                                         (TRUE != FALSE)
#define PduR_HasSizeOfRmGDestRomOfPCConfig()                                                        (TRUE != FALSE)
#define PduR_HasSizeOfRmGDestTpTxStateRamOfPCConfig()                                               (TRUE != FALSE)
#define PduR_HasSizeOfRmSrcRomOfPCConfig()                                                          (TRUE != FALSE)
#define PduR_HasSizeOfRmTp_CancelReceive_TpRxSmStateHandlerOfPCConfig()                             (TRUE != FALSE)
#define PduR_HasSizeOfRmTp_CheckReady2Transmit_TpTxSmStateHandlerOfPCConfig()                       (TRUE != FALSE)
#define PduR_HasSizeOfRmTp_CopyRxData_TpRxSmStateHandlerOfPCConfig()                                (TRUE != FALSE)
#define PduR_HasSizeOfRmTp_FinishReception_TpTxSmStateHandlerOfPCConfig()                           (TRUE != FALSE)
#define PduR_HasSizeOfRmTp_FinishTransmission_TpTxSmStateHandlerOfPCConfig()                        (TRUE != FALSE)
#define PduR_HasSizeOfRmTp_StartOfReception_TpRxSmStateHandlerOfPCConfig()                          (TRUE != FALSE)
#define PduR_HasSizeOfRmTp_TpRxIndication_TpRxSmStateHandlerOfPCConfig()                            (TRUE != FALSE)
#define PduR_HasSizeOfRmTransmitFctPtrOfPCConfig()                                                  (TRUE != FALSE)
#define PduR_HasSizeOfRmTxInstSmRomOfPCConfig()                                                     (TRUE != FALSE)
#define PduR_HasSizeOfRxIf2DestOfPCConfig()                                                         (TRUE != FALSE)
#define PduR_HasSizeOfRxTp2DestOfPCConfig()                                                         (TRUE != FALSE)
#define PduR_HasSizeOfTx2LoOfPCConfig()                                                             (TRUE != FALSE)
#define PduR_HasSizeOfTxIf2UpOfPCConfig()                                                           (TRUE != FALSE)
#define PduR_HasSizeOfTxTp2SrcOfPCConfig()                                                          (TRUE != FALSE)
#define PduR_HasTx2LoOfPCConfig()                                                                   (TRUE != FALSE)
#define PduR_HasTxIf2UpOfPCConfig()                                                                 (TRUE != FALSE)
#define PduR_HasTxTp2SrcOfPCConfig()                                                                (TRUE != FALSE)
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCIncrementDataMacros  PduR Increment Data Macros (PRE_COMPILE)
  \brief  These macros can be used to increment VAR data with numerical nature.
  \{
*/ 
#define PduR_IncBmTxBufferArrayRam(Index)                                                           PduR_GetBmTxBufferArrayRam(Index)++
#define PduR_IncBmTxBufferArrayRamReadIdxOfBmTxBufferInstanceRam(Index)                             PduR_GetBmTxBufferArrayRamReadIdxOfBmTxBufferInstanceRam(Index)++
#define PduR_IncBmTxBufferArrayRamWriteIdxOfBmTxBufferInstanceRam(Index)                            PduR_GetBmTxBufferArrayRamWriteIdxOfBmTxBufferInstanceRam(Index)++
#define PduR_IncBmTxBufferArrayRamInstanceStopIdxOfBmTxBufferRam(Index)                             PduR_GetBmTxBufferArrayRamInstanceStopIdxOfBmTxBufferRam(Index)++
#define PduR_IncBmTxBufferArrayRamReadIdxOfBmTxBufferRam(Index)                                     PduR_GetBmTxBufferArrayRamReadIdxOfBmTxBufferRam(Index)++
#define PduR_IncBmTxBufferArrayRamWriteIdxOfBmTxBufferRam(Index)                                    PduR_GetBmTxBufferArrayRamWriteIdxOfBmTxBufferRam(Index)++
#define PduR_IncRxLengthOfBmTxBufferRam(Index)                                                      PduR_GetRxLengthOfBmTxBufferRam(Index)++
#define PduR_IncBmTxBufferRomIdxOfFmFifoElementRam(Index)                                           PduR_GetBmTxBufferRomIdxOfFmFifoElementRam(Index)++
#define PduR_IncRmDestRomIdxOfFmFifoElementRam(Index)                                               PduR_GetRmDestRomIdxOfFmFifoElementRam(Index)++
#define PduR_IncBmTxBufferInstanceRomIdxOfFmFifoInstanceRam(Index)                                  PduR_GetBmTxBufferInstanceRomIdxOfFmFifoInstanceRam(Index)++
#define PduR_IncFillLevelOfFmFifoRam(Index)                                                         PduR_GetFillLevelOfFmFifoRam(Index)++
#define PduR_IncFmFifoElementRamReadIdxOfFmFifoRam(Index)                                           PduR_GetFmFifoElementRamReadIdxOfFmFifoRam(Index)++
#define PduR_IncFmFifoElementRamWriteIdxOfFmFifoRam(Index)                                          PduR_GetFmFifoElementRamWriteIdxOfFmFifoRam(Index)++
#define PduR_IncPendingConfirmationsOfFmFifoRam(Index)                                              PduR_GetPendingConfirmationsOfFmFifoRam(Index)++
#define PduR_IncFmFifoElementRamIdxOfRmBufferedTpPropertiesRam(Index)                               PduR_GetFmFifoElementRamIdxOfRmBufferedTpPropertiesRam(Index)++
/** 
  \}
*/ 

/** 
  \defgroup  PduRPCDecrementDataMacros  PduR Decrement Data Macros (PRE_COMPILE)
  \brief  These macros can be used to decrement VAR data with numerical nature.
  \{
*/ 
#define PduR_DecBmTxBufferArrayRam(Index)                                                           PduR_GetBmTxBufferArrayRam(Index)--
#define PduR_DecBmTxBufferArrayRamReadIdxOfBmTxBufferInstanceRam(Index)                             PduR_GetBmTxBufferArrayRamReadIdxOfBmTxBufferInstanceRam(Index)--
#define PduR_DecBmTxBufferArrayRamWriteIdxOfBmTxBufferInstanceRam(Index)                            PduR_GetBmTxBufferArrayRamWriteIdxOfBmTxBufferInstanceRam(Index)--
#define PduR_DecBmTxBufferArrayRamInstanceStopIdxOfBmTxBufferRam(Index)                             PduR_GetBmTxBufferArrayRamInstanceStopIdxOfBmTxBufferRam(Index)--
#define PduR_DecBmTxBufferArrayRamReadIdxOfBmTxBufferRam(Index)                                     PduR_GetBmTxBufferArrayRamReadIdxOfBmTxBufferRam(Index)--
#define PduR_DecBmTxBufferArrayRamWriteIdxOfBmTxBufferRam(Index)                                    PduR_GetBmTxBufferArrayRamWriteIdxOfBmTxBufferRam(Index)--
#define PduR_DecRxLengthOfBmTxBufferRam(Index)                                                      PduR_GetRxLengthOfBmTxBufferRam(Index)--
#define PduR_DecBmTxBufferRomIdxOfFmFifoElementRam(Index)                                           PduR_GetBmTxBufferRomIdxOfFmFifoElementRam(Index)--
#define PduR_DecRmDestRomIdxOfFmFifoElementRam(Index)                                               PduR_GetRmDestRomIdxOfFmFifoElementRam(Index)--
#define PduR_DecBmTxBufferInstanceRomIdxOfFmFifoInstanceRam(Index)                                  PduR_GetBmTxBufferInstanceRomIdxOfFmFifoInstanceRam(Index)--
#define PduR_DecFillLevelOfFmFifoRam(Index)                                                         PduR_GetFillLevelOfFmFifoRam(Index)--
#define PduR_DecFmFifoElementRamReadIdxOfFmFifoRam(Index)                                           PduR_GetFmFifoElementRamReadIdxOfFmFifoRam(Index)--
#define PduR_DecFmFifoElementRamWriteIdxOfFmFifoRam(Index)                                          PduR_GetFmFifoElementRamWriteIdxOfFmFifoRam(Index)--
#define PduR_DecPendingConfirmationsOfFmFifoRam(Index)                                              PduR_GetPendingConfirmationsOfFmFifoRam(Index)--
#define PduR_DecFmFifoElementRamIdxOfRmBufferedTpPropertiesRam(Index)                               PduR_GetFmFifoElementRamIdxOfRmBufferedTpPropertiesRam(Index)--
/** 
  \}
*/ 

  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/

/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/


/**********************************************************************************************************************
 * GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

typedef P2FUNC(void, PDUR_CODE, PduR_LockFunctionType) (void);		/* PRQA S 1336 */ /* MD_PduR_1336 */

/* Communication Interface APIs */

typedef P2FUNC(Std_ReturnType, PDUR_APPL_CODE, PduR_TransmitFctPtrType) (PduIdType, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA));		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(Std_ReturnType, PDUR_APPL_CODE, PduR_TriggerTransmitFctPtrType) (PduIdType, P2VAR(PduInfoType, AUTOMATIC, PDUR_APPL_DATA));		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(void, PDUR_APPL_CODE, PduR_IfRxIndicationType) (PduIdType, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA));		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(void, PDUR_APPL_CODE, PduR_IfTxConfirmationFctPtrType) (PduIdType);		/* PRQA S 1336 */ /* MD_PduR_1336 */

/* Transport Protocol APIs */
typedef P2FUNC(BufReq_ReturnType, PDUR_APPL_CODE, PduR_StartOfReceptionFctPtrType) (PduIdType, P2VAR(PduInfoType, AUTOMATIC, PDUR_APPL_DATA), PduLengthType, P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA));		/* PRQA S 1336 */ /* MD_PduR_1336 */

typedef P2FUNC(BufReq_ReturnType, PDUR_APPL_CODE, PduR_CopyRxDataFctPtrType) (PduIdType, P2VAR(PduInfoType, AUTOMATIC, PDUR_APPL_DATA), P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA));		/* PRQA S 1336 */ /* MD_PduR_1336 */

typedef P2FUNC(BufReq_ReturnType, PDUR_APPL_CODE, PduR_CopyTxDataFctPtrType) (PduIdType, P2VAR(PduInfoType, AUTOMATIC, PDUR_APPL_DATA), P2VAR(RetryInfoType, AUTOMATIC, PDUR_APPL_DATA), P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA));		/* PRQA S 1336 */ /* MD_PduR_1336 */

typedef P2FUNC(void, PDUR_APPL_CODE, PduR_TpRxIndicationFctPtrType) (PduIdType, Std_ReturnType);		/* PRQA S 1336 */ /* MD_PduR_1336 */

typedef P2FUNC(void, PDUR_APPL_CODE, PduR_TpTxConfirmationFctPtrType) (PduIdType, Std_ReturnType);		/* PRQA S 1336 */ /* MD_PduR_1336 */


#if ((PDUR_IFCANCELTRANSMITSUPPORTEDOFMMROM == STD_ON) || (PDUR_TPCANCELTRANSMITSUPPORTEDOFMMROM == STD_ON))
typedef P2FUNC(Std_ReturnType, PDUR_APPL_CODE, PduR_CancelTransmitFctPtrType)(PduIdType);		/* PRQA S 1336 */ /* MD_PduR_1336 */
#endif
#if (PDUR_CANCELRECEIVESUPPORTEDOFMMROM == STD_ON)
typedef P2FUNC(Std_ReturnType, PDUR_APPL_CODE, PduR_CancelReceiveFctPtrType) (PduIdType);		/* PRQA S 1336 */ /* MD_PduR_1336 */
#endif
#if (PDUR_CHANGEPARAMETERSUPPORTEDOFMMROM == STD_ON)
typedef P2FUNC(Std_ReturnType, PDUR_APPL_CODE, PduR_ChangeParameterFctPtrType) (PduIdType, TPParameterType, uint16);		/* PRQA S 1336 */ /* MD_PduR_1336 */
#endif

typedef P2FUNC(void, PDUR_CODE, PduR_DisableRoutingFctPtrType) (PduR_RoutingPathGroupIdType);		/* PRQA S 1336 */ /* MD_PduR_1336 */


/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  PduRPCIterableTypes  PduR Iterable Types (PRE_COMPILE)
  \brief  These type definitions are used to iterate over an array with least processor cycles for variable access as possible.
  \{
*/ 
/**   \brief  type used to iterate PduR_BmTxBufferArrayRam */
typedef uint32_least PduR_BmTxBufferArrayRamIterType;

/**   \brief  type used to iterate PduR_BmTxBufferIndRom */
typedef uint32_least PduR_BmTxBufferIndRomIterType;

/**   \brief  type used to iterate PduR_BmTxBufferInstanceRom */
typedef uint8_least PduR_BmTxBufferInstanceRomIterType;

/**   \brief  type used to iterate PduR_BmTxBufferRom */
typedef uint8_least PduR_BmTxBufferRomIterType;

/**   \brief  type used to iterate PduR_CoreManagerRom */
typedef uint8_least PduR_CoreManagerRomIterType;

/**   \brief  type used to iterate PduR_ExclusiveAreaRom */
typedef uint8_least PduR_ExclusiveAreaRomIterType;

/**   \brief  type used to iterate PduR_FmFifoElementRam */
typedef uint8_least PduR_FmFifoElementRamIterType;

/**   \brief  type used to iterate PduR_FmFifoInstanceRom */
typedef uint8_least PduR_FmFifoInstanceRomIterType;

/**   \brief  type used to iterate PduR_FmFifoRom */
typedef uint8_least PduR_FmFifoRomIterType;

/**   \brief  type used to iterate PduR_Fm_ActivateNext_FmSmStateHandler */
typedef uint8_least PduR_Fm_ActivateNext_FmSmStateHandlerIterType;

/**   \brief  type used to iterate PduR_Fm_ActivateRead_FmSmStateHandler */
typedef uint8_least PduR_Fm_ActivateRead_FmSmStateHandlerIterType;

/**   \brief  type used to iterate PduR_Fm_ActivateWrite_FmSmStateHandler */
typedef uint8_least PduR_Fm_ActivateWrite_FmSmStateHandlerIterType;

/**   \brief  type used to iterate PduR_Fm_FinishRead_FmSmStateHandler */
typedef uint8_least PduR_Fm_FinishRead_FmSmStateHandlerIterType;

/**   \brief  type used to iterate PduR_Fm_FinishWrite_FmSmStateHandler */
typedef uint8_least PduR_Fm_FinishWrite_FmSmStateHandlerIterType;

/**   \brief  type used to iterate PduR_GeneralPropertiesRom */
typedef uint8_least PduR_GeneralPropertiesRomIterType;

/**   \brief  type used to iterate PduR_LockRom */
typedef uint8_least PduR_LockRomIterType;

/**   \brief  type used to iterate PduR_MmRom */
typedef uint8_least PduR_MmRomIterType;

/**   \brief  type used to iterate PduR_MmRomInd */
typedef uint8_least PduR_MmRomIndIterType;

/**   \brief  type used to iterate PduR_RmBufferedTpPropertiesRom */
typedef uint8_least PduR_RmBufferedTpPropertiesRomIterType;

/**   \brief  type used to iterate PduR_RmDestRom */
typedef uint16_least PduR_RmDestRomIterType;

/**   \brief  type used to iterate PduR_RmGDestRom */
typedef uint16_least PduR_RmGDestRomIterType;

/**   \brief  type used to iterate PduR_RmGDestTpTxStateRam */
typedef uint8_least PduR_RmGDestTpTxStateRamIterType;

/**   \brief  type used to iterate PduR_RmSrcRom */
typedef uint16_least PduR_RmSrcRomIterType;

/**   \brief  type used to iterate PduR_RmTp_CancelReceive_TpRxSmStateHandler */
typedef uint8_least PduR_RmTp_CancelReceive_TpRxSmStateHandlerIterType;

/**   \brief  type used to iterate PduR_RmTp_CheckReady2Transmit_TpTxSmStateHandler */
typedef uint8_least PduR_RmTp_CheckReady2Transmit_TpTxSmStateHandlerIterType;

/**   \brief  type used to iterate PduR_RmTp_CopyRxData_TpRxSmStateHandler */
typedef uint8_least PduR_RmTp_CopyRxData_TpRxSmStateHandlerIterType;

/**   \brief  type used to iterate PduR_RmTp_FinishReception_TpTxSmStateHandler */
typedef uint8_least PduR_RmTp_FinishReception_TpTxSmStateHandlerIterType;

/**   \brief  type used to iterate PduR_RmTp_FinishTransmission_TpTxSmStateHandler */
typedef uint8_least PduR_RmTp_FinishTransmission_TpTxSmStateHandlerIterType;

/**   \brief  type used to iterate PduR_RmTp_StartOfReception_TpRxSmStateHandler */
typedef uint8_least PduR_RmTp_StartOfReception_TpRxSmStateHandlerIterType;

/**   \brief  type used to iterate PduR_RmTp_TpRxIndication_TpRxSmStateHandler */
typedef uint8_least PduR_RmTp_TpRxIndication_TpRxSmStateHandlerIterType;

/**   \brief  type used to iterate PduR_RmTransmitFctPtr */
typedef uint8_least PduR_RmTransmitFctPtrIterType;

/**   \brief  type used to iterate PduR_RmTxInstSmRom */
typedef uint8_least PduR_RmTxInstSmRomIterType;

/**   \brief  type used to iterate PduR_RxIf2Dest */
typedef uint8_least PduR_RxIf2DestIterType;

/**   \brief  type used to iterate PduR_RxTp2Dest */
typedef uint8_least PduR_RxTp2DestIterType;

/**   \brief  type used to iterate PduR_Tx2Lo */
typedef uint16_least PduR_Tx2LoIterType;

/**   \brief  type used to iterate PduR_TxIf2Up */
typedef uint8_least PduR_TxIf2UpIterType;

/**   \brief  type used to iterate PduR_TxTp2Src */
typedef uint8_least PduR_TxTp2SrcIterType;

/** 
  \}
*/ 

/** 
  \defgroup  PduRPCIterableTypesWithSizeRelations  PduR Iterable Types With Size Relations (PRE_COMPILE)
  \brief  These type definitions are used to iterate over a VAR based array with the same iterator as the related CONST array.
  \{
*/ 
/**   \brief  type used to iterate PduR_BmTxBufferInstanceRam */
typedef PduR_BmTxBufferInstanceRomIterType PduR_BmTxBufferInstanceRamIterType;

/**   \brief  type used to iterate PduR_BmTxBufferRam */
typedef PduR_BmTxBufferRomIterType PduR_BmTxBufferRamIterType;

/**   \brief  type used to iterate PduR_FmFifoInstanceRam */
typedef PduR_FmFifoInstanceRomIterType PduR_FmFifoInstanceRamIterType;

/**   \brief  type used to iterate PduR_FmFifoRam */
typedef PduR_FmFifoRomIterType PduR_FmFifoRamIterType;

/**   \brief  type used to iterate PduR_RmBufferedTpPropertiesRam */
typedef PduR_RmBufferedTpPropertiesRomIterType PduR_RmBufferedTpPropertiesRamIterType;

/** 
  \}
*/ 

/** 
  \defgroup  PduRPCValueTypes  PduR Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value based data representations.
  \{
*/ 
/**   \brief  value based type definition for PduR_BmTxBufferArrayRam */
typedef uint8 PduR_BmTxBufferArrayRamType;

/**   \brief  value based type definition for PduR_BmTxBufferRomIdxOfBmTxBufferIndRom */
typedef uint8 PduR_BmTxBufferRomIdxOfBmTxBufferIndRomType;

/**   \brief  value based type definition for PduR_BmTxBufferArrayRamReadIdxOfBmTxBufferInstanceRam */
typedef uint32 PduR_BmTxBufferArrayRamReadIdxOfBmTxBufferInstanceRamType;

/**   \brief  value based type definition for PduR_BmTxBufferArrayRamWriteIdxOfBmTxBufferInstanceRam */
typedef uint32 PduR_BmTxBufferArrayRamWriteIdxOfBmTxBufferInstanceRamType;

/**   \brief  value based type definition for PduR_PduRBufferStateOfBmTxBufferInstanceRam */
typedef uint8 PduR_PduRBufferStateOfBmTxBufferInstanceRamType;

/**   \brief  value based type definition for PduR_TxBufferUsedOfBmTxBufferInstanceRam */
typedef boolean PduR_TxBufferUsedOfBmTxBufferInstanceRamType;

/**   \brief  value based type definition for PduR_BmTxBufferRomIdxOfBmTxBufferInstanceRom */
typedef uint8 PduR_BmTxBufferRomIdxOfBmTxBufferInstanceRomType;

/**   \brief  value based type definition for PduR_AllocatedOfBmTxBufferRam */
typedef boolean PduR_AllocatedOfBmTxBufferRamType;

/**   \brief  value based type definition for PduR_BmTxBufferArrayRamInstanceStopIdxOfBmTxBufferRam */
typedef uint32 PduR_BmTxBufferArrayRamInstanceStopIdxOfBmTxBufferRamType;

/**   \brief  value based type definition for PduR_BmTxBufferArrayRamReadIdxOfBmTxBufferRam */
typedef uint32 PduR_BmTxBufferArrayRamReadIdxOfBmTxBufferRamType;

/**   \brief  value based type definition for PduR_BmTxBufferArrayRamWriteIdxOfBmTxBufferRam */
typedef uint32 PduR_BmTxBufferArrayRamWriteIdxOfBmTxBufferRamType;

/**   \brief  value based type definition for PduR_PduRBufferStateOfBmTxBufferRam */
typedef uint8 PduR_PduRBufferStateOfBmTxBufferRamType;

/**   \brief  value based type definition for PduR_RxLengthOfBmTxBufferRam */
typedef PduLengthType PduR_RxLengthOfBmTxBufferRamType;

/**   \brief  value based type definition for PduR_BmTxBufferArrayRamEndIdxOfBmTxBufferRom */
typedef uint16 PduR_BmTxBufferArrayRamEndIdxOfBmTxBufferRomType;

/**   \brief  value based type definition for PduR_BmTxBufferArrayRamLengthOfBmTxBufferRom */
typedef uint16 PduR_BmTxBufferArrayRamLengthOfBmTxBufferRomType;

/**   \brief  value based type definition for PduR_BmTxBufferArrayRamStartIdxOfBmTxBufferRom */
typedef uint16 PduR_BmTxBufferArrayRamStartIdxOfBmTxBufferRomType;

/**   \brief  value based type definition for PduR_BmTxBufferInstanceRomEndIdxOfBmTxBufferRom */
typedef uint8 PduR_BmTxBufferInstanceRomEndIdxOfBmTxBufferRomType;

/**   \brief  value based type definition for PduR_BmTxBufferInstanceRomStartIdxOfBmTxBufferRom */
typedef uint8 PduR_BmTxBufferInstanceRomStartIdxOfBmTxBufferRomType;

/**   \brief  value based type definition for PduR_ConfigId */
typedef uint8 PduR_ConfigIdType;

/**   \brief  value based type definition for PduR_MmRomIndEndIdxOfCoreManagerRom */
typedef uint8 PduR_MmRomIndEndIdxOfCoreManagerRomType;

/**   \brief  value based type definition for PduR_MmRomIndStartIdxOfCoreManagerRom */
typedef uint8 PduR_MmRomIndStartIdxOfCoreManagerRomType;

/**   \brief  value based type definition for PduR_MmRomIndUsedOfCoreManagerRom */
typedef boolean PduR_MmRomIndUsedOfCoreManagerRomType;

/**   \brief  value based type definition for PduR_BmTxBufferRomIdxOfFmFifoElementRam */
typedef uint8 PduR_BmTxBufferRomIdxOfFmFifoElementRamType;

/**   \brief  value based type definition for PduR_DedicatedTxBufferOfFmFifoElementRam */
typedef boolean PduR_DedicatedTxBufferOfFmFifoElementRamType;

/**   \brief  value based type definition for PduR_RmDestRomIdxOfFmFifoElementRam */
typedef uint16 PduR_RmDestRomIdxOfFmFifoElementRamType;

/**   \brief  value based type definition for PduR_StateOfFmFifoElementRam */
typedef uint8 PduR_StateOfFmFifoElementRamType;

/**   \brief  value based type definition for PduR_BmTxBufferInstanceRomIdxOfFmFifoInstanceRam */
typedef uint8 PduR_BmTxBufferInstanceRomIdxOfFmFifoInstanceRamType;

/**   \brief  value based type definition for PduR_FmFifoRomIdxOfFmFifoInstanceRom */
typedef uint8 PduR_FmFifoRomIdxOfFmFifoInstanceRomType;

/**   \brief  value based type definition for PduR_FillLevelOfFmFifoRam */
typedef uint16 PduR_FillLevelOfFmFifoRamType;

/**   \brief  value based type definition for PduR_FmFifoElementRamReadIdxOfFmFifoRam */
typedef uint8 PduR_FmFifoElementRamReadIdxOfFmFifoRamType;

/**   \brief  value based type definition for PduR_FmFifoElementRamWriteIdxOfFmFifoRam */
typedef uint8 PduR_FmFifoElementRamWriteIdxOfFmFifoRamType;

/**   \brief  value based type definition for PduR_TpTxSmStateOfFmFifoRam */
typedef uint8 PduR_TpTxSmStateOfFmFifoRamType;

/**   \brief  value based type definition for PduR_BmTxBufferIndRomEndIdxOfFmFifoRom */
typedef uint8 PduR_BmTxBufferIndRomEndIdxOfFmFifoRomType;

/**   \brief  value based type definition for PduR_BmTxBufferIndRomLengthOfFmFifoRom */
typedef uint8 PduR_BmTxBufferIndRomLengthOfFmFifoRomType;

/**   \brief  value based type definition for PduR_BmTxBufferIndRomStartIdxOfFmFifoRom */
typedef uint8 PduR_BmTxBufferIndRomStartIdxOfFmFifoRomType;

/**   \brief  value based type definition for PduR_FmFifoElementRamEndIdxOfFmFifoRom */
typedef uint8 PduR_FmFifoElementRamEndIdxOfFmFifoRomType;

/**   \brief  value based type definition for PduR_FmFifoElementRamLengthOfFmFifoRom */
typedef uint8 PduR_FmFifoElementRamLengthOfFmFifoRomType;

/**   \brief  value based type definition for PduR_FmFifoElementRamStartIdxOfFmFifoRom */
typedef uint8 PduR_FmFifoElementRamStartIdxOfFmFifoRomType;

/**   \brief  value based type definition for PduR_hasTpTxBufferedForwardingOfGeneralPropertiesRom */
typedef boolean PduR_hasTpTxBufferedForwardingOfGeneralPropertiesRomType;

/**   \brief  value based type definition for PduR_Initialized */
typedef boolean PduR_InitializedType;

/**   \brief  value based type definition for PduR_ExclusiveAreaRomIdxOfLockRom */
typedef uint8 PduR_ExclusiveAreaRomIdxOfLockRomType;

/**   \brief  value based type definition for PduR_ExclusiveAreaRomUsedOfLockRom */
typedef boolean PduR_ExclusiveAreaRomUsedOfLockRomType;

/**   \brief  value based type definition for PduR_CoreManagerRomIdxOfMmRom */
typedef uint8 PduR_CoreManagerRomIdxOfMmRomType;

/**   \brief  value based type definition for PduR_IfCancelTransmitSupportedOfMmRom */
typedef boolean PduR_IfCancelTransmitSupportedOfMmRomType;

/**   \brief  value based type definition for PduR_LoIfOfMmRom */
typedef boolean PduR_LoIfOfMmRomType;

/**   \brief  value based type definition for PduR_LoTpOfMmRom */
typedef boolean PduR_LoTpOfMmRomType;

/**   \brief  value based type definition for PduR_MaskedBitsOfMmRom */
typedef uint8 PduR_MaskedBitsOfMmRomType;

/**   \brief  value based type definition for PduR_RmGDestRomEndIdxOfMmRom */
typedef uint16 PduR_RmGDestRomEndIdxOfMmRomType;

/**   \brief  value based type definition for PduR_RmGDestRomStartIdxOfMmRom */
typedef uint16 PduR_RmGDestRomStartIdxOfMmRomType;

/**   \brief  value based type definition for PduR_RmGDestRomUsedOfMmRom */
typedef boolean PduR_RmGDestRomUsedOfMmRomType;

/**   \brief  value based type definition for PduR_TpCancelTransmitSupportedOfMmRom */
typedef boolean PduR_TpCancelTransmitSupportedOfMmRomType;

/**   \brief  value based type definition for PduR_UpIfOfMmRom */
typedef boolean PduR_UpIfOfMmRomType;

/**   \brief  value based type definition for PduR_UpTpOfMmRom */
typedef boolean PduR_UpTpOfMmRomType;

/**   \brief  value based type definition for PduR_MmRomInd */
typedef uint8 PduR_MmRomIndType;

/**   \brief  value based type definition for PduR_FmFifoElementRamIdxOfRmBufferedTpPropertiesRam */
typedef uint8 PduR_FmFifoElementRamIdxOfRmBufferedTpPropertiesRamType;

/**   \brief  value based type definition for PduR_TpRxSmStateOfRmBufferedTpPropertiesRam */
typedef uint8 PduR_TpRxSmStateOfRmBufferedTpPropertiesRamType;

/**   \brief  value based type definition for PduR_DedicatedTxBufferOfRmBufferedTpPropertiesRom */
typedef boolean PduR_DedicatedTxBufferOfRmBufferedTpPropertiesRomType;

/**   \brief  value based type definition for PduR_FmFifoRomIdxOfRmBufferedTpPropertiesRom */
typedef uint8 PduR_FmFifoRomIdxOfRmBufferedTpPropertiesRomType;

/**   \brief  value based type definition for PduR_QueuedDestCntOfRmBufferedTpPropertiesRom */
typedef uint8 PduR_QueuedDestCntOfRmBufferedTpPropertiesRomType;

/**   \brief  value based type definition for PduR_TpThresholdOfRmBufferedTpPropertiesRom */
typedef uint8 PduR_TpThresholdOfRmBufferedTpPropertiesRomType;

/**   \brief  value based type definition for PduR_RmGDestRomIdxOfRmDestRom */
typedef uint16 PduR_RmGDestRomIdxOfRmDestRomType;

/**   \brief  value based type definition for PduR_RmSrcRomIdxOfRmDestRom */
typedef uint16 PduR_RmSrcRomIdxOfRmDestRomType;

/**   \brief  value based type definition for PduR_RoutingTypeOfRmDestRom */
typedef uint8 PduR_RoutingTypeOfRmDestRomType;

/**   \brief  value based type definition for PduR_DestHndOfRmGDestRom */
typedef uint8 PduR_DestHndOfRmGDestRomType;

/**   \brief  value based type definition for PduR_DirectionOfRmGDestRom */
typedef uint8 PduR_DirectionOfRmGDestRomType;

/**   \brief  value based type definition for PduR_FmFifoInstanceRomIdxOfRmGDestRom */
typedef uint8 PduR_FmFifoInstanceRomIdxOfRmGDestRomType;

/**   \brief  value based type definition for PduR_FmFifoInstanceRomUsedOfRmGDestRom */
typedef boolean PduR_FmFifoInstanceRomUsedOfRmGDestRomType;

/**   \brief  value based type definition for PduR_LockRomIdxOfRmGDestRom */
typedef uint8 PduR_LockRomIdxOfRmGDestRomType;

/**   \brief  value based type definition for PduR_MaskedBitsOfRmGDestRom */
typedef uint8 PduR_MaskedBitsOfRmGDestRomType;

/**   \brief  value based type definition for PduR_MmRomIdxOfRmGDestRom */
typedef uint8 PduR_MmRomIdxOfRmGDestRomType;

/**   \brief  value based type definition for PduR_PduRDestPduProcessingOfRmGDestRom */
typedef uint8 PduR_PduRDestPduProcessingOfRmGDestRomType;

/**   \brief  value based type definition for PduR_RmDestRomIdxOfRmGDestRom */
typedef uint16 PduR_RmDestRomIdxOfRmGDestRomType;

/**   \brief  value based type definition for PduR_RmDestRomUsedOfRmGDestRom */
typedef boolean PduR_RmDestRomUsedOfRmGDestRomType;

/**   \brief  value based type definition for PduR_RmGDestTpTxStateRamIdxOfRmGDestRom */
typedef uint8 PduR_RmGDestTpTxStateRamIdxOfRmGDestRomType;

/**   \brief  value based type definition for PduR_RmGDestTpTxStateRamUsedOfRmGDestRom */
typedef boolean PduR_RmGDestTpTxStateRamUsedOfRmGDestRomType;

/**   \brief  value based type definition for PduR_TpTxInstSmStateOfRmGDestTpTxStateRam */
typedef uint8 PduR_TpTxInstSmStateOfRmGDestTpTxStateRamType;

/**   \brief  value based type definition for PduR_LockRomIdxOfRmSrcRom */
typedef uint8 PduR_LockRomIdxOfRmSrcRomType;

/**   \brief  value based type definition for PduR_MaskedBitsOfRmSrcRom */
typedef uint8 PduR_MaskedBitsOfRmSrcRomType;

/**   \brief  value based type definition for PduR_MmRomIdxOfRmSrcRom */
typedef uint8 PduR_MmRomIdxOfRmSrcRomType;

/**   \brief  value based type definition for PduR_RmBufferedTpPropertiesRomIdxOfRmSrcRom */
typedef uint8 PduR_RmBufferedTpPropertiesRomIdxOfRmSrcRomType;

/**   \brief  value based type definition for PduR_RmBufferedTpPropertiesRomUsedOfRmSrcRom */
typedef boolean PduR_RmBufferedTpPropertiesRomUsedOfRmSrcRomType;

/**   \brief  value based type definition for PduR_RmDestRomEndIdxOfRmSrcRom */
typedef uint16 PduR_RmDestRomEndIdxOfRmSrcRomType;

/**   \brief  value based type definition for PduR_RmDestRomLengthOfRmSrcRom */
typedef uint8 PduR_RmDestRomLengthOfRmSrcRomType;

/**   \brief  value based type definition for PduR_RmDestRomStartIdxOfRmSrcRom */
typedef uint16 PduR_RmDestRomStartIdxOfRmSrcRomType;

/**   \brief  value based type definition for PduR_SrcHndOfRmSrcRom */
typedef uint8 PduR_SrcHndOfRmSrcRomType;

/**   \brief  value based type definition for PduR_TriggerTransmitSupportedOfRmSrcRom */
typedef boolean PduR_TriggerTransmitSupportedOfRmSrcRomType;

/**   \brief  value based type definition for PduR_TxConfirmationSupportedOfRmSrcRom */
typedef boolean PduR_TxConfirmationSupportedOfRmSrcRomType;

/**   \brief  value based type definition for PduR_RmSrcRomIdxOfRxIf2Dest */
typedef uint8 PduR_RmSrcRomIdxOfRxIf2DestType;

/**   \brief  value based type definition for PduR_RmSrcRomIdxOfRxTp2Dest */
typedef uint16 PduR_RmSrcRomIdxOfRxTp2DestType;

/**   \brief  value based type definition for PduR_RmSrcRomUsedOfRxTp2Dest */
typedef boolean PduR_RmSrcRomUsedOfRxTp2DestType;

/**   \brief  value based type definition for PduR_SizeOfBmTxBufferArrayRam */
typedef uint16 PduR_SizeOfBmTxBufferArrayRamType;

/**   \brief  value based type definition for PduR_SizeOfBmTxBufferIndRom */
typedef uint8 PduR_SizeOfBmTxBufferIndRomType;

/**   \brief  value based type definition for PduR_SizeOfBmTxBufferInstanceRam */
typedef uint8 PduR_SizeOfBmTxBufferInstanceRamType;

/**   \brief  value based type definition for PduR_SizeOfBmTxBufferInstanceRom */
typedef uint8 PduR_SizeOfBmTxBufferInstanceRomType;

/**   \brief  value based type definition for PduR_SizeOfBmTxBufferRam */
typedef uint8 PduR_SizeOfBmTxBufferRamType;

/**   \brief  value based type definition for PduR_SizeOfBmTxBufferRom */
typedef uint8 PduR_SizeOfBmTxBufferRomType;

/**   \brief  value based type definition for PduR_SizeOfCoreManagerRom */
typedef uint8 PduR_SizeOfCoreManagerRomType;

/**   \brief  value based type definition for PduR_SizeOfExclusiveAreaRom */
typedef uint8 PduR_SizeOfExclusiveAreaRomType;

/**   \brief  value based type definition for PduR_SizeOfFmFifoElementRam */
typedef uint8 PduR_SizeOfFmFifoElementRamType;

/**   \brief  value based type definition for PduR_SizeOfFmFifoInstanceRam */
typedef uint8 PduR_SizeOfFmFifoInstanceRamType;

/**   \brief  value based type definition for PduR_SizeOfFmFifoInstanceRom */
typedef uint8 PduR_SizeOfFmFifoInstanceRomType;

/**   \brief  value based type definition for PduR_SizeOfFmFifoRam */
typedef uint8 PduR_SizeOfFmFifoRamType;

/**   \brief  value based type definition for PduR_SizeOfFmFifoRom */
typedef uint8 PduR_SizeOfFmFifoRomType;

/**   \brief  value based type definition for PduR_SizeOfFm_ActivateNext_FmSmStateHandler */
typedef uint8 PduR_SizeOfFm_ActivateNext_FmSmStateHandlerType;

/**   \brief  value based type definition for PduR_SizeOfFm_ActivateRead_FmSmStateHandler */
typedef uint8 PduR_SizeOfFm_ActivateRead_FmSmStateHandlerType;

/**   \brief  value based type definition for PduR_SizeOfFm_ActivateWrite_FmSmStateHandler */
typedef uint8 PduR_SizeOfFm_ActivateWrite_FmSmStateHandlerType;

/**   \brief  value based type definition for PduR_SizeOfFm_FinishRead_FmSmStateHandler */
typedef uint8 PduR_SizeOfFm_FinishRead_FmSmStateHandlerType;

/**   \brief  value based type definition for PduR_SizeOfFm_FinishWrite_FmSmStateHandler */
typedef uint8 PduR_SizeOfFm_FinishWrite_FmSmStateHandlerType;

/**   \brief  value based type definition for PduR_SizeOfGeneralPropertiesRom */
typedef uint8 PduR_SizeOfGeneralPropertiesRomType;

/**   \brief  value based type definition for PduR_SizeOfLockRom */
typedef uint8 PduR_SizeOfLockRomType;

/**   \brief  value based type definition for PduR_SizeOfMmRom */
typedef uint8 PduR_SizeOfMmRomType;

/**   \brief  value based type definition for PduR_SizeOfMmRomInd */
typedef uint8 PduR_SizeOfMmRomIndType;

/**   \brief  value based type definition for PduR_SizeOfRmBufferedTpPropertiesRam */
typedef uint8 PduR_SizeOfRmBufferedTpPropertiesRamType;

/**   \brief  value based type definition for PduR_SizeOfRmBufferedTpPropertiesRom */
typedef uint8 PduR_SizeOfRmBufferedTpPropertiesRomType;

/**   \brief  value based type definition for PduR_SizeOfRmDestRom */
typedef uint16 PduR_SizeOfRmDestRomType;

/**   \brief  value based type definition for PduR_SizeOfRmGDestRom */
typedef uint16 PduR_SizeOfRmGDestRomType;

/**   \brief  value based type definition for PduR_SizeOfRmGDestTpTxStateRam */
typedef uint8 PduR_SizeOfRmGDestTpTxStateRamType;

/**   \brief  value based type definition for PduR_SizeOfRmSrcRom */
typedef uint16 PduR_SizeOfRmSrcRomType;

/**   \brief  value based type definition for PduR_SizeOfRmTp_CancelReceive_TpRxSmStateHandler */
typedef uint8 PduR_SizeOfRmTp_CancelReceive_TpRxSmStateHandlerType;

/**   \brief  value based type definition for PduR_SizeOfRmTp_CheckReady2Transmit_TpTxSmStateHandler */
typedef uint8 PduR_SizeOfRmTp_CheckReady2Transmit_TpTxSmStateHandlerType;

/**   \brief  value based type definition for PduR_SizeOfRmTp_CopyRxData_TpRxSmStateHandler */
typedef uint8 PduR_SizeOfRmTp_CopyRxData_TpRxSmStateHandlerType;

/**   \brief  value based type definition for PduR_SizeOfRmTp_FinishReception_TpTxSmStateHandler */
typedef uint8 PduR_SizeOfRmTp_FinishReception_TpTxSmStateHandlerType;

/**   \brief  value based type definition for PduR_SizeOfRmTp_FinishTransmission_TpTxSmStateHandler */
typedef uint8 PduR_SizeOfRmTp_FinishTransmission_TpTxSmStateHandlerType;

/**   \brief  value based type definition for PduR_SizeOfRmTp_StartOfReception_TpRxSmStateHandler */
typedef uint8 PduR_SizeOfRmTp_StartOfReception_TpRxSmStateHandlerType;

/**   \brief  value based type definition for PduR_SizeOfRmTp_TpRxIndication_TpRxSmStateHandler */
typedef uint8 PduR_SizeOfRmTp_TpRxIndication_TpRxSmStateHandlerType;

/**   \brief  value based type definition for PduR_SizeOfRmTransmitFctPtr */
typedef uint8 PduR_SizeOfRmTransmitFctPtrType;

/**   \brief  value based type definition for PduR_SizeOfRmTxInstSmRom */
typedef uint8 PduR_SizeOfRmTxInstSmRomType;

/**   \brief  value based type definition for PduR_SizeOfRxIf2Dest */
typedef uint8 PduR_SizeOfRxIf2DestType;

/**   \brief  value based type definition for PduR_SizeOfRxTp2Dest */
typedef uint8 PduR_SizeOfRxTp2DestType;

/**   \brief  value based type definition for PduR_SizeOfTx2Lo */
typedef uint16 PduR_SizeOfTx2LoType;

/**   \brief  value based type definition for PduR_SizeOfTxIf2Up */
typedef uint8 PduR_SizeOfTxIf2UpType;

/**   \brief  value based type definition for PduR_SizeOfTxTp2Src */
typedef uint8 PduR_SizeOfTxTp2SrcType;

/**   \brief  value based type definition for PduR_CancelTransmitUsedOfTx2Lo */
typedef boolean PduR_CancelTransmitUsedOfTx2LoType;

/**   \brief  value based type definition for PduR_MaskedBitsOfTx2Lo */
typedef uint8 PduR_MaskedBitsOfTx2LoType;

/**   \brief  value based type definition for PduR_RmSrcRomIdxOfTx2Lo */
typedef uint16 PduR_RmSrcRomIdxOfTx2LoType;

/**   \brief  value based type definition for PduR_RmSrcRomUsedOfTx2Lo */
typedef boolean PduR_RmSrcRomUsedOfTx2LoType;

/**   \brief  value based type definition for PduR_RmTransmitFctPtrIdxOfTx2Lo */
typedef uint8 PduR_RmTransmitFctPtrIdxOfTx2LoType;

/**   \brief  value based type definition for PduR_RmGDestRomIdxOfTxIf2Up */
typedef uint16 PduR_RmGDestRomIdxOfTxIf2UpType;

/**   \brief  value based type definition for PduR_TxConfirmationUsedOfTxIf2Up */
typedef boolean PduR_TxConfirmationUsedOfTxIf2UpType;

/**   \brief  value based type definition for PduR_RmGDestRomIdxOfTxTp2Src */
typedef uint16 PduR_RmGDestRomIdxOfTxTp2SrcType;

/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

#if(PDUR_RMSRCROM == STD_ON)
typedef P2FUNC(Std_ReturnType, PDUR_APPL_CODE, PduR_RmTransmitFctPtrType) (PduR_RmSrcRomIterType, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA));		/* PRQA S 1336 */ /* MD_PduR_1336 */
#endif
#if(PDUR_RMDESTROM == STD_ON)
typedef P2FUNC(void, PDUR_APPL_CODE, PduR_RmIfRxIndicationType) (PduR_RmDestRomIterType, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA));		/* PRQA S 1336 */ /* MD_PduR_1336 */
#endif

/* Queue abstraction layer */
typedef P2FUNC(Std_ReturnType, PDUR_APPL_CODE, PduR_QAL_PutFctPtrType)          (PduR_RmGDestRomIterType, PduR_RmDestRomIterType, PduLengthType, SduDataPtrType);    /* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(Std_ReturnType, PDUR_APPL_CODE, PduR_QAL_GetFctPtrType)          (PduR_RmGDestRomIterType, P2VAR(PduR_RmDestRomIterType, AUTOMATIC, PDUR_APPL_DATA), P2VAR(PduInfoType, AUTOMATIC, PDUR_APPL_DATA));    /* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(void,           PDUR_APPL_CODE, PduR_QAL_RemoveFctPtrType)       (PduR_RmGDestRomIterType);    /* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(uint16,         PDUR_APPL_CODE, PduR_QAL_GetFillLevelFctPtrType) (PduR_RmGDestRomIterType);    /* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(void,           PDUR_APPL_CODE, PduR_QAL_FlushFctPtrType)        (PduR_RmGDestRomIterType);    /* PRQA S 1336 */ /* MD_PduR_1336 */

typedef P2FUNC(void, PDUR_CODE, PduR_RmIf_TxConfirmation_StateHandler_FctPtrType) (PduR_RmGDestRomIterType);		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(void, PDUR_CODE, PduR_RmIf_FifoHandling_StateHandler_FctPtrType) (PduR_RmDestRomIterType, PduR_RmGDestRomIdxOfRmDestRomType, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA));		/* PRQA S 1336 */ /* MD_PduR_1336 */

#if(PDUR_FMFIFORAM == STD_ON)
typedef P2FUNC(BufReq_ReturnType, PDUR_APPL_CODE, PduR_StartOfReceptionFifoSmFctPtrType) (PduR_RmSrcRomIterType, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA), PduLengthType, P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA));		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(BufReq_ReturnType, PDUR_APPL_CODE, PduR_CopyRxDataFifoSmFctPtrType) (PduR_RmSrcRomIterType, PduLengthType, SduDataPtrType, P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA));		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(void, PDUR_APPL_CODE, PduR_TpRxIndicationFifoSmFctPtrType) (PduR_RmSrcRomIterType, Std_ReturnType);		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(void, PDUR_APPL_CODE, PduR_TpCancelReceiveFctPtrType) (PduR_RmSrcRomIterType);		/* PRQA S 1336 */ /* MD_PduR_1336 */

typedef P2FUNC(BufReq_ReturnType, PDUR_APPL_CODE, PduR_CopyTxDataFifoSmFctPtrType) (PduR_RmGDestRomIterType, PduLengthType, SduDataPtrType, P2CONST(RetryInfoType, AUTOMATIC, PDUR_APPL_DATA), P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA));		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef PduR_CopyTxDataFifoSmFctPtrType PduR_CopyTxDataFifoSmFctPtrArrayType[2];		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(Std_ReturnType, PDUR_APPL_CODE, PduR_TpTriggerTransmitFctPtrType) (PduR_RmDestRomIterType, PduR_RmGDestRomIterType);		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef PduR_TpTriggerTransmitFctPtrType PduR_TpTriggerTransmitFctPtrArrayType[2];		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(void, PDUR_APPL_CODE, PduR_TpTxConfirmationFifoSmFctPtrType) (PduR_RmGDestRomIterType, Std_ReturnType);		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef PduR_TpTxConfirmationFifoSmFctPtrType PduR_TpTxConfirmationFifoSmFctPtrArrayType[2];		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(Std_ReturnType, PDUR_APPL_CODE, PduR_TpCancelTransmitFctPtrType)(PduR_RmDestRomIterType);		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef PduR_TpCancelTransmitFctPtrType PduR_TpCancelTransmitFctPtrArrayType[2];		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(void, PDUR_CODE, PduR_TpDisableRoutingFctPtrType) (PduR_RmGDestRomIterType);		/* PRQA S 1336 */ /* MD_PduR_1336 */

typedef P2FUNC(Std_ReturnType, PDUR_APPL_CODE, PduR_CheckReady2TransmitFctPtrType) (PduR_RmSrcRomIterType);		/* PRQA S 1336 */ /* MD_PduR_1336 */

typedef P2FUNC(void, PDUR_APPL_CODE, PduR_FinishReceptionFctPtrType) (PduR_RmSrcRomIterType, Std_ReturnType);		/* PRQA S 1336 */ /* MD_PduR_1336 */

typedef P2FUNC(void, PDUR_APPL_CODE, PduR_FinishTransmissionFctPtrType) (PduR_RmDestRomIterType, Std_ReturnType);		/* PRQA S 1336 */ /* MD_PduR_1336 */

typedef P2FUNC(void, PDUR_APPL_CODE, PduR_ActivateNextFctPtrType) (PduR_FmFifoElementRamIterType, Std_ReturnType);		/* PRQA S 1336 */ /* MD_PduR_1336 */
#endif

#if((PDUR_EXISTS_TP_BUFFERED_ROUTINGTYPEOFRMDESTROM  == STD_ON) || (PDUR_EXISTS_IF_BUFFERED_ROUTINGTYPEOFRMDESTROM  == STD_ON))
typedef P2FUNC(void, PDUR_APPL_CODE, PduR_FinishReadFctPtrType) (PduR_FmFifoElementRamIterType, Std_ReturnType);		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(void, PDUR_APPL_CODE, PduR_FinishWriteFctPtrType) (PduR_FmFifoElementRamIterType, Std_ReturnType);		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(void, PDUR_APPL_CODE, PduR_ActivateReadFctPtrType) (PduR_FmFifoElementRamIterType);		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(void, PDUR_APPL_CODE, PduR_ActivateWriteFctPtrType) (PduR_FmFifoElementRamIterType);		/* PRQA S 1336 */ /* MD_PduR_1336 */
#endif

#if (PDUR_EXISTS_IF_BUFFERED_ROUTINGTYPEOFRMDESTROM == STD_ON)
typedef P2FUNC(void, PDUR_APPL_CODE, PduR_Fm_FlushFiFoElementPtrType) (PduR_FmFifoElementRamIterType);		/* PRQA S 1336 */ /* MD_PduR_1336 */
#endif

#if(PDUR_SMDATAPLANEROM == STD_ON)
typedef uint8 PduR_FilterReturnType;
# define PDUR_FILTER_PASS  0u
# define PDUR_FILTER_BLOCK 1u

typedef uint32 PduR_SmSaType;
typedef uint32 PduR_SmTaType;

#define PDUR_SM_UNLEARNED_CONNECTION 0u
typedef P2FUNC(PduR_SmSaType, PDUR_CODE, PduR_Sm_LinearTaToSaCalculationStrategy_GetSaFctPtrType) (PduR_SmLinearTaToSaCalculationStrategyRomIterType, uint32);		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(void, PDUR_CODE, PduR_Sm_SrcFilterFctPtrType) (PduR_SmSrcRomIdxOfRmSrcRomType, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA));		/* PRQA S 1336 */ /* MD_PduR_1336 */
typedef P2FUNC(PduR_FilterReturnType, PDUR_CODE, PduR_Sm_DestFilterFctPtrType) (PduR_SmGDestRomIdxOfRmGDestRomType, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA));		/* PRQA S 1336 */ /* MD_PduR_1336 */
#endif

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  PduRPCStructTypes  PduR Struct Types (PRE_COMPILE)
  \brief  These type definitions are used for structured data representations.
  \{
*/ 
/**   \brief  type used in PduR_BmTxBufferIndRom */
typedef struct sPduR_BmTxBufferIndRomType
{
  PduR_BmTxBufferRomIdxOfBmTxBufferIndRomType BmTxBufferRomIdxOfBmTxBufferIndRom;  /**< the index of the 1:1 relation pointing to PduR_BmTxBufferRom */
} PduR_BmTxBufferIndRomType;

/**   \brief  type used in PduR_BmTxBufferInstanceRam */
typedef struct sPduR_BmTxBufferInstanceRamType
{
  PduR_BmTxBufferArrayRamReadIdxOfBmTxBufferInstanceRamType BmTxBufferArrayRamReadIdxOfBmTxBufferInstanceRam;  /**< the index of the 1:1 relation pointing to PduR_BmTxBufferArrayRam */
  PduR_BmTxBufferArrayRamWriteIdxOfBmTxBufferInstanceRamType BmTxBufferArrayRamWriteIdxOfBmTxBufferInstanceRam;  /**< the index of the 1:1 relation pointing to PduR_BmTxBufferArrayRam */
  PduR_PduRBufferStateOfBmTxBufferInstanceRamType PduRBufferStateOfBmTxBufferInstanceRam;  /**< PduRBufferState represents the buffer state (READ4WRITE, WRITE4READ, FULL, EMPTY) */
  PduR_TxBufferUsedOfBmTxBufferInstanceRamType TxBufferUsedOfBmTxBufferInstanceRam;  /**< TRUE if a destination of an 1:N routing use the TxBuffer element. */
} PduR_BmTxBufferInstanceRamType;

/**   \brief  type used in PduR_BmTxBufferInstanceRom */
typedef struct sPduR_BmTxBufferInstanceRomType
{
  PduR_BmTxBufferRomIdxOfBmTxBufferInstanceRomType BmTxBufferRomIdxOfBmTxBufferInstanceRom;  /**< the index of the 1:1 relation pointing to PduR_BmTxBufferRom */
} PduR_BmTxBufferInstanceRomType;

/**   \brief  type used in PduR_BmTxBufferRam */
typedef struct sPduR_BmTxBufferRamType
{
  PduR_BmTxBufferArrayRamInstanceStopIdxOfBmTxBufferRamType BmTxBufferArrayRamInstanceStopIdxOfBmTxBufferRam;  /**< the index of the 1:1 relation pointing to PduR_BmTxBufferArrayRam */
  PduR_BmTxBufferArrayRamReadIdxOfBmTxBufferRamType BmTxBufferArrayRamReadIdxOfBmTxBufferRam;  /**< the index of the 1:1 relation pointing to PduR_BmTxBufferArrayRam */
  PduR_BmTxBufferArrayRamWriteIdxOfBmTxBufferRamType BmTxBufferArrayRamWriteIdxOfBmTxBufferRam;  /**< the index of the 1:1 relation pointing to PduR_BmTxBufferArrayRam */
  PduR_RxLengthOfBmTxBufferRamType RxLengthOfBmTxBufferRam;  /**< Rx Pdu Sdu length */
  PduR_AllocatedOfBmTxBufferRamType AllocatedOfBmTxBufferRam;  /**< Is true, if the buffer is allocated by a routing. */
  PduR_PduRBufferStateOfBmTxBufferRamType PduRBufferStateOfBmTxBufferRam;  /**< PduRBufferState represents the buffer state (READ4WRITE, WRITE4READ, FULL, EMPTY) */
} PduR_BmTxBufferRamType;

/**   \brief  type used in PduR_BmTxBufferRom */
typedef struct sPduR_BmTxBufferRomType
{
  PduR_BmTxBufferArrayRamEndIdxOfBmTxBufferRomType BmTxBufferArrayRamEndIdxOfBmTxBufferRom;  /**< the end index of the 1:n relation pointing to PduR_BmTxBufferArrayRam */
  PduR_BmTxBufferArrayRamStartIdxOfBmTxBufferRomType BmTxBufferArrayRamStartIdxOfBmTxBufferRom;  /**< the start index of the 1:n relation pointing to PduR_BmTxBufferArrayRam */
  PduR_BmTxBufferInstanceRomEndIdxOfBmTxBufferRomType BmTxBufferInstanceRomEndIdxOfBmTxBufferRom;  /**< the end index of the 1:n relation pointing to PduR_BmTxBufferInstanceRom */
  PduR_BmTxBufferInstanceRomStartIdxOfBmTxBufferRomType BmTxBufferInstanceRomStartIdxOfBmTxBufferRom;  /**< the start index of the 1:n relation pointing to PduR_BmTxBufferInstanceRom */
} PduR_BmTxBufferRomType;

/**   \brief  type used in PduR_CoreManagerRom */
typedef struct sPduR_CoreManagerRomType
{
  uint8 PduR_CoreManagerRomNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} PduR_CoreManagerRomType;

/**   \brief  type used in PduR_ExclusiveAreaRom */
typedef struct sPduR_ExclusiveAreaRomType
{
  PduR_LockFunctionType LockOfExclusiveAreaRom;  /**< Lock function */
  PduR_LockFunctionType UnlockOfExclusiveAreaRom;  /**< Unlock function */
} PduR_ExclusiveAreaRomType;

/**   \brief  type used in PduR_FmFifoElementRam */
typedef struct sPduR_FmFifoElementRamType
{
  PduR_RmDestRomIdxOfFmFifoElementRamType RmDestRomIdxOfFmFifoElementRam;  /**< the index of the 0:1 relation pointing to PduR_RmDestRom */
  PduR_BmTxBufferRomIdxOfFmFifoElementRamType BmTxBufferRomIdxOfFmFifoElementRam;  /**< the index of the 0:1 relation pointing to PduR_BmTxBufferRom */
  PduR_DedicatedTxBufferOfFmFifoElementRamType DedicatedTxBufferOfFmFifoElementRam;  /**< TRUE if a routing has a dedicate Tx Buffer and the Tx Buffer is not shared with an other routing */
  PduR_StateOfFmFifoElementRamType StateOfFmFifoElementRam;  /**< Fifo Manager state machine state */
} PduR_FmFifoElementRamType;

/**   \brief  type used in PduR_FmFifoInstanceRam */
typedef struct sPduR_FmFifoInstanceRamType
{
  PduR_BmTxBufferInstanceRomIdxOfFmFifoInstanceRamType BmTxBufferInstanceRomIdxOfFmFifoInstanceRam;  /**< the index of the 0:1 relation pointing to PduR_BmTxBufferInstanceRom */
} PduR_FmFifoInstanceRamType;

/**   \brief  type used in PduR_FmFifoInstanceRom */
typedef struct sPduR_FmFifoInstanceRomType
{
  PduR_FmFifoRomIdxOfFmFifoInstanceRomType FmFifoRomIdxOfFmFifoInstanceRom;  /**< the index of the 1:1 relation pointing to PduR_FmFifoRom */
} PduR_FmFifoInstanceRomType;

/**   \brief  type used in PduR_FmFifoRam */
typedef struct sPduR_FmFifoRamType
{
  PduR_FillLevelOfFmFifoRamType FillLevelOfFmFifoRam;  /**< Fill level of the FIFO queue */
  PduR_FmFifoElementRamReadIdxOfFmFifoRamType FmFifoElementRamReadIdxOfFmFifoRam;  /**< the index of the 0:1 relation pointing to PduR_FmFifoElementRam */
  PduR_FmFifoElementRamWriteIdxOfFmFifoRamType FmFifoElementRamWriteIdxOfFmFifoRam;  /**< the index of the 0:1 relation pointing to PduR_FmFifoElementRam */
  PduR_TpTxSmStateOfFmFifoRamType TpTxSmStateOfFmFifoRam;  /**< Tp Tx state */
  PduR_RmDestRomLengthOfRmSrcRomType PendingConfirmationsOfFmFifoRam;  /**< Number of pending Tx Confirmations of all possible destinations. */
} PduR_FmFifoRamType;

/**   \brief  type used in PduR_FmFifoRom */
typedef struct sPduR_FmFifoRomType
{
  PduR_BmTxBufferIndRomEndIdxOfFmFifoRomType BmTxBufferIndRomEndIdxOfFmFifoRom;  /**< the end index of the 1:n relation pointing to PduR_BmTxBufferIndRom */
  PduR_BmTxBufferIndRomStartIdxOfFmFifoRomType BmTxBufferIndRomStartIdxOfFmFifoRom;  /**< the start index of the 1:n relation pointing to PduR_BmTxBufferIndRom */
  PduR_FmFifoElementRamEndIdxOfFmFifoRomType FmFifoElementRamEndIdxOfFmFifoRom;  /**< the end index of the 1:n relation pointing to PduR_FmFifoElementRam */
  PduR_FmFifoElementRamStartIdxOfFmFifoRomType FmFifoElementRamStartIdxOfFmFifoRom;  /**< the start index of the 1:n relation pointing to PduR_FmFifoElementRam */
} PduR_FmFifoRomType;

/**   \brief  type used in PduR_Fm_ActivateNext_FmSmStateHandler */
typedef struct sPduR_Fm_ActivateNext_FmSmStateHandlerType
{
  PduR_ActivateNextFctPtrType FctPtrOfFm_ActivateNext_FmSmStateHandler;
} PduR_Fm_ActivateNext_FmSmStateHandlerType;

/**   \brief  type used in PduR_Fm_ActivateRead_FmSmStateHandler */
typedef struct sPduR_Fm_ActivateRead_FmSmStateHandlerType
{
  PduR_ActivateReadFctPtrType FctPtrOfFm_ActivateRead_FmSmStateHandler;
} PduR_Fm_ActivateRead_FmSmStateHandlerType;

/**   \brief  type used in PduR_Fm_ActivateWrite_FmSmStateHandler */
typedef struct sPduR_Fm_ActivateWrite_FmSmStateHandlerType
{
  PduR_ActivateWriteFctPtrType FctPtrOfFm_ActivateWrite_FmSmStateHandler;
} PduR_Fm_ActivateWrite_FmSmStateHandlerType;

/**   \brief  type used in PduR_Fm_FinishRead_FmSmStateHandler */
typedef struct sPduR_Fm_FinishRead_FmSmStateHandlerType
{
  PduR_FinishReadFctPtrType FctPtrOfFm_FinishRead_FmSmStateHandler;
} PduR_Fm_FinishRead_FmSmStateHandlerType;

/**   \brief  type used in PduR_Fm_FinishWrite_FmSmStateHandler */
typedef struct sPduR_Fm_FinishWrite_FmSmStateHandlerType
{
  PduR_FinishWriteFctPtrType FctPtrOfFm_FinishWrite_FmSmStateHandler;
} PduR_Fm_FinishWrite_FmSmStateHandlerType;

/**   \brief  type used in PduR_GeneralPropertiesRom */
typedef struct sPduR_GeneralPropertiesRomType
{
  PduR_hasTpTxBufferedForwardingOfGeneralPropertiesRomType hasTpTxBufferedForwardingOfGeneralPropertiesRom;
} PduR_GeneralPropertiesRomType;

/**   \brief  type used in PduR_LockRom */
typedef struct sPduR_LockRomType
{
  uint8 PduR_LockRomNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} PduR_LockRomType;

/**   \brief  type used in PduR_MmRom */
typedef struct sPduR_MmRomType
{
  PduR_RmGDestRomEndIdxOfMmRomType RmGDestRomEndIdxOfMmRom;  /**< the end index of the 0:n relation pointing to PduR_RmGDestRom */
  PduR_RmGDestRomStartIdxOfMmRomType RmGDestRomStartIdxOfMmRom;  /**< the start index of the 0:n relation pointing to PduR_RmGDestRom */
  PduR_CoreManagerRomIdxOfMmRomType CoreManagerRomIdxOfMmRom;  /**< the index of the 1:1 relation pointing to PduR_CoreManagerRom */
  PduR_MaskedBitsOfMmRomType MaskedBitsOfMmRom;  /**< contains bitcoded the boolean data of PduR_IfCancelTransmitSupportedOfMmRom, PduR_LoIfOfMmRom, PduR_LoTpOfMmRom, PduR_RmGDestRomUsedOfMmRom, PduR_TpCancelTransmitSupportedOfMmRom, PduR_UpIfOfMmRom, PduR_UpTpOfMmRom */
  PduR_CancelTransmitFctPtrType LoIfCancelTransmitFctPtrOfMmRom;  /**< Lower layer cancel transmit function pointers. */
  PduR_CancelTransmitFctPtrType LoTpCancelTransmitFctPtrOfMmRom;  /**< Lower layer cancel transmit function pointers. */
  PduR_CopyRxDataFctPtrType UpTpCopyRxDataFctPtrOfMmRom;  /**< Transport protocol CopyRxData function pointers */
  PduR_CopyTxDataFctPtrType UpTpCopyTxDataFctPtrOfMmRom;  /**< Transport protocol CopyTxData function pointers */
  PduR_IfRxIndicationType UpIfRxIndicationFctPtrOfMmRom;  /**< Upper layer communication interface Rx indication function pointers. */
  PduR_IfTxConfirmationFctPtrType UpIfTxConfirmationFctPtrOfMmRom;  /**< Upper layer communication interface Tx confimation function pointers */
  PduR_StartOfReceptionFctPtrType UpTpStartOfReceptionFctPtrOfMmRom;  /**< Transport protocol StartOfReception function pointers */
  PduR_TpRxIndicationFctPtrType UpTpTpRxIndicationFctPtrOfMmRom;  /**< Transport protocol TpRxIndication function pointers */
  PduR_TpTxConfirmationFctPtrType UpTpTpTxConfirmationFctPtrOfMmRom;  /**< Transport protocol TpTxConfimation function pointers */
  PduR_TransmitFctPtrType LoIfTransmitFctPtrOfMmRom;  /**< Lower layer If transmit function pointers */
  PduR_TransmitFctPtrType LoTpTransmitFctPtrOfMmRom;  /**< Lower layer Tp transmit function pointers */
  PduR_TriggerTransmitFctPtrType UpIfTriggerTransmitFctPtrOfMmRom;  /**< Upper layer trigger transmit function pointers */
} PduR_MmRomType;

/**   \brief  type used in PduR_RmBufferedTpPropertiesRam */
typedef struct sPduR_RmBufferedTpPropertiesRamType
{
  PduR_FmFifoElementRamIdxOfRmBufferedTpPropertiesRamType FmFifoElementRamIdxOfRmBufferedTpPropertiesRam;  /**< the index of the 0:1 relation pointing to PduR_FmFifoElementRam */
  PduR_TpRxSmStateOfRmBufferedTpPropertiesRamType TpRxSmStateOfRmBufferedTpPropertiesRam;  /**< Tp source instance state */
} PduR_RmBufferedTpPropertiesRamType;

/**   \brief  type used in PduR_RmBufferedTpPropertiesRom */
typedef struct sPduR_RmBufferedTpPropertiesRomType
{
  PduR_DedicatedTxBufferOfRmBufferedTpPropertiesRomType DedicatedTxBufferOfRmBufferedTpPropertiesRom;  /**< True, if the PduRSrcPdu has a dedicated TxBuffer */
  PduR_FmFifoRomIdxOfRmBufferedTpPropertiesRomType FmFifoRomIdxOfRmBufferedTpPropertiesRom;  /**< the index of the 1:1 relation pointing to PduR_FmFifoRom */
  PduR_QueuedDestCntOfRmBufferedTpPropertiesRomType QueuedDestCntOfRmBufferedTpPropertiesRom;  /**< Number of queued TP destinations. */
  PduR_TpThresholdOfRmBufferedTpPropertiesRomType TpThresholdOfRmBufferedTpPropertiesRom;  /**< TP threshold value */
} PduR_RmBufferedTpPropertiesRomType;

/**   \brief  type used in PduR_RmDestRom */
typedef struct sPduR_RmDestRomType
{
  PduR_RmGDestRomIdxOfRmDestRomType RmGDestRomIdxOfRmDestRom;  /**< the index of the 1:1 relation pointing to PduR_RmGDestRom */
  PduR_RmSrcRomIdxOfRmDestRomType RmSrcRomIdxOfRmDestRom;  /**< the index of the 1:1 relation pointing to PduR_RmSrcRom */
  PduR_RoutingTypeOfRmDestRomType RoutingTypeOfRmDestRom;  /**< Type of the Routing (API Forwarding, Gateway). */
} PduR_RmDestRomType;

/**   \brief  type used in PduR_RmGDestRom */
typedef struct sPduR_RmGDestRomType
{
  PduR_RmDestRomIdxOfRmGDestRomType RmDestRomIdxOfRmGDestRom;  /**< the index of the 0:1 relation pointing to PduR_RmDestRom */
  PduR_DestHndOfRmGDestRomType DestHndOfRmGDestRom;  /**< handle to be used as parameter for the StartOfReception, CopyRxData, Transmit or RxIndication function call. */
  PduR_DirectionOfRmGDestRomType DirectionOfRmGDestRom;  /**< Direction of this Pdu: Rx or Tx */
  PduR_FmFifoInstanceRomIdxOfRmGDestRomType FmFifoInstanceRomIdxOfRmGDestRom;  /**< the index of the 0:1 relation pointing to PduR_FmFifoInstanceRom */
  PduR_MaskedBitsOfRmGDestRomType MaskedBitsOfRmGDestRom;  /**< contains bitcoded the boolean data of PduR_FmFifoInstanceRomUsedOfRmGDestRom, PduR_RmDestRomUsedOfRmGDestRom, PduR_RmGDestTpTxStateRamUsedOfRmGDestRom */
  PduR_MmRomIdxOfRmGDestRomType MmRomIdxOfRmGDestRom;  /**< the index of the 1:1 relation pointing to PduR_MmRom */
  PduR_PduRDestPduProcessingOfRmGDestRomType PduRDestPduProcessingOfRmGDestRom;  /**< Is Processing Type of destination PDU. */
  PduR_RmGDestTpTxStateRamIdxOfRmGDestRomType RmGDestTpTxStateRamIdxOfRmGDestRom;  /**< the index of the 0:1 relation pointing to PduR_RmGDestTpTxStateRam */
} PduR_RmGDestRomType;

/**   \brief  type used in PduR_RmGDestTpTxStateRam */
typedef struct sPduR_RmGDestTpTxStateRamType
{
  PduR_TpTxInstSmStateOfRmGDestTpTxStateRamType TpTxInstSmStateOfRmGDestTpTxStateRam;  /**< Tp dest instance state */
} PduR_RmGDestTpTxStateRamType;

/**   \brief  type used in PduR_RmSrcRom */
typedef struct sPduR_RmSrcRomType
{
  PduR_RmDestRomEndIdxOfRmSrcRomType RmDestRomEndIdxOfRmSrcRom;  /**< the end index of the 1:n relation pointing to PduR_RmDestRom */
  PduR_RmDestRomStartIdxOfRmSrcRomType RmDestRomStartIdxOfRmSrcRom;  /**< the start index of the 1:n relation pointing to PduR_RmDestRom */
  PduR_LockRomIdxOfRmSrcRomType LockRomIdxOfRmSrcRom;  /**< the index of the 1:1 relation pointing to PduR_LockRom */
  PduR_MaskedBitsOfRmSrcRomType MaskedBitsOfRmSrcRom;  /**< contains bitcoded the boolean data of PduR_RmBufferedTpPropertiesRomUsedOfRmSrcRom, PduR_TriggerTransmitSupportedOfRmSrcRom, PduR_TxConfirmationSupportedOfRmSrcRom */
  PduR_MmRomIdxOfRmSrcRomType MmRomIdxOfRmSrcRom;  /**< the index of the 1:1 relation pointing to PduR_MmRom */
  PduR_RmBufferedTpPropertiesRomIdxOfRmSrcRomType RmBufferedTpPropertiesRomIdxOfRmSrcRom;  /**< the index of the 0:1 relation pointing to PduR_RmBufferedTpPropertiesRom */
  PduR_SrcHndOfRmSrcRomType SrcHndOfRmSrcRom;  /**< handle to be used as parameter for the TxConfirmation or TriggerTransmit function call. */
} PduR_RmSrcRomType;

/**   \brief  type used in PduR_RmTp_CancelReceive_TpRxSmStateHandler */
typedef struct sPduR_RmTp_CancelReceive_TpRxSmStateHandlerType
{
  PduR_TpCancelReceiveFctPtrType FctPtrOfRmTp_CancelReceive_TpRxSmStateHandler;
} PduR_RmTp_CancelReceive_TpRxSmStateHandlerType;

/**   \brief  type used in PduR_RmTp_CheckReady2Transmit_TpTxSmStateHandler */
typedef struct sPduR_RmTp_CheckReady2Transmit_TpTxSmStateHandlerType
{
  PduR_CheckReady2TransmitFctPtrType FctPtrOfRmTp_CheckReady2Transmit_TpTxSmStateHandler;
} PduR_RmTp_CheckReady2Transmit_TpTxSmStateHandlerType;

/**   \brief  type used in PduR_RmTp_CopyRxData_TpRxSmStateHandler */
typedef struct sPduR_RmTp_CopyRxData_TpRxSmStateHandlerType
{
  PduR_CopyRxDataFifoSmFctPtrType FctPtrOfRmTp_CopyRxData_TpRxSmStateHandler;
} PduR_RmTp_CopyRxData_TpRxSmStateHandlerType;

/**   \brief  type used in PduR_RmTp_FinishReception_TpTxSmStateHandler */
typedef struct sPduR_RmTp_FinishReception_TpTxSmStateHandlerType
{
  PduR_FinishReceptionFctPtrType FctPtrOfRmTp_FinishReception_TpTxSmStateHandler;
} PduR_RmTp_FinishReception_TpTxSmStateHandlerType;

/**   \brief  type used in PduR_RmTp_FinishTransmission_TpTxSmStateHandler */
typedef struct sPduR_RmTp_FinishTransmission_TpTxSmStateHandlerType
{
  PduR_FinishTransmissionFctPtrType FctPtrOfRmTp_FinishTransmission_TpTxSmStateHandler;
} PduR_RmTp_FinishTransmission_TpTxSmStateHandlerType;

/**   \brief  type used in PduR_RmTp_StartOfReception_TpRxSmStateHandler */
typedef struct sPduR_RmTp_StartOfReception_TpRxSmStateHandlerType
{
  PduR_StartOfReceptionFifoSmFctPtrType FctPtrOfRmTp_StartOfReception_TpRxSmStateHandler;
} PduR_RmTp_StartOfReception_TpRxSmStateHandlerType;

/**   \brief  type used in PduR_RmTp_TpRxIndication_TpRxSmStateHandler */
typedef struct sPduR_RmTp_TpRxIndication_TpRxSmStateHandlerType
{
  PduR_TpRxIndicationFifoSmFctPtrType FctPtrOfRmTp_TpRxIndication_TpRxSmStateHandler;
} PduR_RmTp_TpRxIndication_TpRxSmStateHandlerType;

/**   \brief  type used in PduR_RmTxInstSmRom */
typedef struct sPduR_RmTxInstSmRomType
{
  PduR_CopyTxDataFifoSmFctPtrArrayType PduR_RmTp_TxInst_CopyTxDataOfRmTxInstSmRom;
  PduR_TpCancelTransmitFctPtrArrayType PduR_RmTp_TxInst_CancelTransmitOfRmTxInstSmRom;
  PduR_TpTriggerTransmitFctPtrArrayType PduR_RmTp_TxInst_TriggerTransmitOfRmTxInstSmRom;
  PduR_TpTxConfirmationFifoSmFctPtrArrayType PduR_RmTp_TxInst_TxConfirmationOfRmTxInstSmRom;
} PduR_RmTxInstSmRomType;

/**   \brief  type used in PduR_RxIf2Dest */
typedef struct sPduR_RxIf2DestType
{
  uint8 PduR_RxIf2DestNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} PduR_RxIf2DestType;

/**   \brief  type used in PduR_RxTp2Dest */
typedef struct sPduR_RxTp2DestType
{
  uint8 PduR_RxTp2DestNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} PduR_RxTp2DestType;

/**   \brief  type used in PduR_Tx2Lo */
typedef struct sPduR_Tx2LoType
{
  PduR_MaskedBitsOfTx2LoType MaskedBitsOfTx2Lo;  /**< contains bitcoded the boolean data of PduR_CancelTransmitUsedOfTx2Lo, PduR_RmSrcRomUsedOfTx2Lo */
  PduR_RmTransmitFctPtrIdxOfTx2LoType RmTransmitFctPtrIdxOfTx2Lo;  /**< the index of the 1:1 relation pointing to PduR_RmTransmitFctPtr */
} PduR_Tx2LoType;

/**   \brief  type used in PduR_TxIf2Up */
typedef struct sPduR_TxIf2UpType
{
  PduR_RmGDestRomIdxOfTxIf2UpType RmGDestRomIdxOfTxIf2Up;  /**< the index of the 1:1 relation pointing to PduR_RmGDestRom */
  PduR_TxConfirmationUsedOfTxIf2UpType TxConfirmationUsedOfTxIf2Up;  /**< True, if any of the source PduRDestPdus uses a TxConfirmation. */
} PduR_TxIf2UpType;

/**   \brief  type used in PduR_TxTp2Src */
typedef struct sPduR_TxTp2SrcType
{
  PduR_RmGDestRomIdxOfTxTp2SrcType RmGDestRomIdxOfTxTp2Src;  /**< the index of the 1:1 relation pointing to PduR_RmGDestRom */
} PduR_TxTp2SrcType;

/** 
  \}
*/ 

/** 
  \defgroup  PduRPCSymbolicStructTypes  PduR Symbolic Struct Types (PRE_COMPILE)
  \brief  These structs are used in unions to have a symbol based data representation style.
  \{
*/ 
/**   \brief  type to be used as symbolic data element access to PduR_BmTxBufferArrayRam */
typedef struct PduR_BmTxBufferArrayRamStructSTag
{
  PduR_BmTxBufferArrayRamType PduRTxBuffer_FuncDiagTx_F2[8];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_FuncDiagTx_F4[8];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagReq_26_SingleFrame[8];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagReq_53_SingleFrame[8];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagReq_98_SingleFrame[8];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagReq_A0_SingleFrame[8];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagReq_A1_SingleFrame[8];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagReq_A2_SingleFrame[8];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagReq_C0_SingleFrame[8];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagReq_SingleFrame[8];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagResp_98_SingleFrame[8];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagResp_A0_SingleFrame[8];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagResp_A1_SingleFrame[8];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagResp_C0_SingleFrame[8];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Tx_CIOM_BB2_22S[10];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Cab_Rx_CCM_Cab_03P[10];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_EMS_BB2_09S[11];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_04S[11];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_20S[11];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_TECU_BB2_06S[11];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_VMCU_BB2_31S[11];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_VMCU_BB2_32S[11];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_04S[11];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_11S[11];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_06S[13];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_27S[13];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_05S[13];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_25S[13];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_VMCU_BB2_57P[15];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_EMS_BB2_11S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_TECU_BB2_05S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_VMCU_BB2_34S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Tx_CIOM_BB2_12S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Tx_CIOM_BB2_13S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Tx_CIOM_BB2_30S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Sec_Tx_Alarm_Sec_03S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Sec_Tx_Alarm_Sec_06S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Sec_Tx_Alarm_Sec_07S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_05S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_06S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_07S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_08S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_09S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_10S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_11S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_12S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Sec_Tx_DDM_Sec_03S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Sec_Tx_DDM_Sec_04S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Sec_Tx_DDM_Sec_05S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Sec_Tx_PDM_Sec_03S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Sec_Tx_PDM_Sec_04S[16];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_19P[21];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_34P[21];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB1_Rx_PropTCO2_X_TACHO[23];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagReq_LongFrame[32];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagResp_LongFrame[32];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_21S[36];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_36S[36];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_20S[36];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_30S[36];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB1_Rx_DI_X_TACHO[42];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_35S[98];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_29S[98];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagReq_Pool_Short[100];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagResp_26[100];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagResp_53[100];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagResp_Pool_Short[100];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_BB2_Rx_BBM_BB2_03S_CIOM[128];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_13S[128];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagResp_A0[130];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagResp_A1[130];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagResp_A2[130];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagResp_C0[130];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagResp_98[180];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiagResp_Pool_Long[200];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiag_TransferData_2[200];
  PduR_BmTxBufferArrayRamType PduRTxBuffer_PhysDiag_TransferData_1[4200];
} PduR_BmTxBufferArrayRamStructSType;

/**   \brief  type to be used as symbolic data element access to PduR_BmTxBufferInstanceRam */
typedef struct PduR_BmTxBufferInstanceRamStructSTag
{
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_FuncDiagTx_F2[3];
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_FuncDiagTx_F4[3];
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagReq_26_SingleFrame;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagReq_53_SingleFrame;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagReq_98_SingleFrame;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagReq_A0_SingleFrame;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagReq_A1_SingleFrame;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagReq_A2_SingleFrame;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagReq_C0_SingleFrame;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagReq_SingleFrame;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagResp_98_SingleFrame;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagResp_A0_SingleFrame;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagResp_A1_SingleFrame;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagResp_C0_SingleFrame;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Tx_CIOM_BB2_22S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Cab_Rx_CCM_Cab_03P;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_EMS_BB2_09S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_04S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_20S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_TECU_BB2_06S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_VMCU_BB2_31S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_VMCU_BB2_32S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_04S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_11S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_06S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_27S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_05S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_25S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_VMCU_BB2_57P;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_EMS_BB2_11S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_TECU_BB2_05S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_VMCU_BB2_34S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Tx_CIOM_BB2_12S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Tx_CIOM_BB2_13S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Tx_CIOM_BB2_30S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Sec_Tx_Alarm_Sec_03S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Sec_Tx_Alarm_Sec_06S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Sec_Tx_Alarm_Sec_07S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_05S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_06S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_07S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_08S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_09S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_10S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_11S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_12S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Sec_Tx_DDM_Sec_03S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Sec_Tx_DDM_Sec_04S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Sec_Tx_DDM_Sec_05S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Sec_Tx_PDM_Sec_03S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Sec_Tx_PDM_Sec_04S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_19P;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_34P;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB1_Rx_PropTCO2_X_TACHO;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagReq_LongFrame;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagResp_LongFrame;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_21S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_36S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_20S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_30S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB1_Rx_DI_X_TACHO;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_35S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_29S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagReq_Pool_Short;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagResp_26;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagResp_53;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagResp_Pool_Short;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_BB2_Rx_BBM_BB2_03S_CIOM;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_13S;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagResp_A0;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagResp_A1;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagResp_A2;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagResp_C0;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagResp_98;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiagResp_Pool_Long;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiag_TransferData_2;
  PduR_BmTxBufferInstanceRamType PduRTxBuffer_PhysDiag_TransferData_1;
} PduR_BmTxBufferInstanceRamStructSType;

/**   \brief  type to be used as symbolic data element access to PduR_BmTxBufferRam */
typedef struct PduR_BmTxBufferRamStructSTag
{
  PduR_BmTxBufferRamType PduRTxBuffer_FuncDiagTx_F2;
  PduR_BmTxBufferRamType PduRTxBuffer_FuncDiagTx_F4;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagReq_26_SingleFrame;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagReq_53_SingleFrame;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagReq_98_SingleFrame;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagReq_A0_SingleFrame;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagReq_A1_SingleFrame;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagReq_A2_SingleFrame;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagReq_C0_SingleFrame;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagReq_SingleFrame;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagResp_98_SingleFrame;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagResp_A0_SingleFrame;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagResp_A1_SingleFrame;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagResp_C0_SingleFrame;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Tx_CIOM_BB2_22S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Cab_Rx_CCM_Cab_03P;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_EMS_BB2_09S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_04S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_20S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_TECU_BB2_06S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_VMCU_BB2_31S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_VMCU_BB2_32S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_04S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_11S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_06S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_27S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_05S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_25S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_VMCU_BB2_57P;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_EMS_BB2_11S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_TECU_BB2_05S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_VMCU_BB2_34S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Tx_CIOM_BB2_12S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Tx_CIOM_BB2_13S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Tx_CIOM_BB2_30S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Sec_Tx_Alarm_Sec_03S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Sec_Tx_Alarm_Sec_06S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Sec_Tx_Alarm_Sec_07S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_05S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_06S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_07S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_08S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_09S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_10S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_11S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Sec_Tx_CIOM_Sec_12S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Sec_Tx_DDM_Sec_03S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Sec_Tx_DDM_Sec_04S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Sec_Tx_DDM_Sec_05S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Sec_Tx_PDM_Sec_03S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Sec_Tx_PDM_Sec_04S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_19P;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_34P;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB1_Rx_PropTCO2_X_TACHO;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagReq_LongFrame;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagResp_LongFrame;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_21S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_36S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_20S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_30S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB1_Rx_DI_X_TACHO;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_HMIIOM_BB2_35S;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_29S;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagReq_Pool_Short;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagResp_26;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagResp_53;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagResp_Pool_Short;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_BB2_Rx_BBM_BB2_03S_CIOM;
  PduR_BmTxBufferRamType PduRTxBuffer_CanTp_Cab_Tx_CIOM_Cab_13S;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagResp_A0;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagResp_A1;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagResp_A2;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagResp_C0;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagResp_98;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiagResp_Pool_Long;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiag_TransferData_2;
  PduR_BmTxBufferRamType PduRTxBuffer_PhysDiag_TransferData_1;
} PduR_BmTxBufferRamStructSType;

/**   \brief  type to be used as symbolic data element access to PduR_FmFifoElementRam */
typedef struct PduR_FmFifoElementRamStructSTag
{
  PduR_FmFifoElementRamType TpSharedBufferQueue_Sec_Tx_Alarm_Sec_03S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_PhysDiagRespMsg_F2_A2_BB2[5];
  PduR_FmFifoElementRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_11S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagReqMsgIntHMIIOM_98_F3_Cab;
  PduR_FmFifoElementRamType TpSharedBufferQueue_IntTesterTGW2FuncDiagMsg;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_34P;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Tx_CIOM_BB2_13S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_HMIIOM_BB2_35S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_PhysDiagRespMsg_F2_98_BB2[5];
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Tx_CIOM_BB2_30S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagRespMsgIntHMIIOM_F3_C0_BB2;
  PduR_FmFifoElementRamType TpSharedBufferQueue_PhysDiagRespMsg_F1_53_BB2[5];
  PduR_FmFifoElementRamType TpSharedBufferQueue_PhysDiagReqMsg_A1_F2_Sec[4];
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagReqMsgIntHMIIOM_A1_F3_Sec;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagRespMsgIntTGW2_F4_A2_BB2;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB_Rx_DI_X_TACHO;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagRespMsgIntTGW2_F4_C0_BB2;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_HMIIOM_BB2_20S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_PhysDiagRespMsg_F2_26_BB2[5];
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Tx_CIOM_BB2_22S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagReqMsgIntTGW2_98_F4_Cab;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB1_Rx_PropTCO2_X_TACHO;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_20S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Sec_Tx_CIOM_Sec_09S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_25S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagReqMsgIntHMIIOM_A2_F3_Cab;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_29S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_VMCU_BB2_34S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_PhysDiagReqMsg_53_F1_Cab[4];
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_TECU_BB2_05S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_PhysDiagReqMsg_A2_F2_Cab[4];
  PduR_FmFifoElementRamType TpSharedBufferQueue_FuncDiagTx_F2;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Tx_CIOM_BB2_12S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagRespMsgIntTGW2_F4_A0_BB2;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagReqMsgIntTGW2_53_F4_Cab;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagRespMsgIntHMIIOM_F3_A2_BB2;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagRespMsgIntHMIIOM_F3_A1_BB2;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagRespMsgIntHMIIOM_F3_53_BB2;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagReqMsgIntTGW2_26_F4_Cab;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Sec_Tx_Alarm_Sec_07S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_PhysDiagReqMsg_A0_F2_Sec[4];
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagReqMsgIntTGW2_A0_F4_Sec;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagReqMsgIntHMIIOM_C0_F3_Sec;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_EMS_BB2_09S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_05S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagReqMsgIntTGW2_C0_F4_Sec;
  PduR_FmFifoElementRamType TpSharedBufferQueue_PhysDiagReqMsg_C0_F2_Sec[4];
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagRespMsgIntTGW2_F4_98_BB2;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagReqMsgIntHMIIOM_53_F3_Cab;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagReqMsgIntTGW2_A1_F4_Sec;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Sec_Tx_DDM_Sec_04S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagRespMsgIntTGW2_F4_53_BB2;
  PduR_FmFifoElementRamType TpSharedBufferQueue_PhysDiagReqMsg_26_F2_Cab[4];
  PduR_FmFifoElementRamType TpSharedBufferQueue_PhysDiagRespMsg_F2_A0_BB2[5];
  PduR_FmFifoElementRamType TpSharedBufferQueue_PhysDiagReqMsg_53_F2_Cab[4];
  PduR_FmFifoElementRamType TpSharedBufferQueue_Sec_Tx_CIOM_Sec_07S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Sec_Tx_DDM_Sec_03S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_HMIIOM_BB2_36S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagReqMsgIntHMIIOM_A0_F3_Sec;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagRespMsgIntHMIIOM_F3_A0_BB2;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_HMIIOM_BB2_21S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_VMCU_BB2_31S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Sec_Tx_CIOM_Sec_11S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagReqMsgIntHMIIOM_26_F3_Cab;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_04S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_HMIIOM_BB2_19P;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Sec_Tx_CIOM_Sec_05S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_PhysDiagRespMsg_F2_53_BB2[5];
  PduR_FmFifoElementRamType TpSharedBufferQueue_Sec_Tx_CIOM_Sec_06S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Sec_Tx_DDM_Sec_05S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagRespMsgIntTGW2_F4_A1_BB2;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Cab_Rx_CCM_Cab_03P;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagRespMsgIntHMIIOM_F3_26_BB2;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagReqMsgIntTGW2_A2_F4_Cab;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_VMCU_BB2_32S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_HMIIOM_BB2_27S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_BBM_BB2_03S_CIOM;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Sec_Tx_CIOM_Sec_10S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagRespMsgIntHMIIOM_F3_98_BB2;
  PduR_FmFifoElementRamType TpSharedBufferQueue_DiagRespMsgIntTGW2_F4_26_BB2;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Sec_Tx_CIOM_Sec_08S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Sec_Tx_CIOM_Sec_12S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_PhysDiagRespMsg_F2_A1_BB2[5];
  PduR_FmFifoElementRamType TpSharedBufferQueue_Sec_Tx_Alarm_Sec_06S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_HMIIOM_BB2_06S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_PhysDiagRespMsg_F2_C0_BB2[5];
  PduR_FmFifoElementRamType TpSharedBufferQueue_Sec_Tx_PDM_Sec_03S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_HMIIOM_BB2_04S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_TECU_BB2_06S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_13S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_30S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_EMS_BB2_11S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_PhysDiagReqMsg_98_F2_Cab[4];
  PduR_FmFifoElementRamType TpSharedBufferQueue_Sec_Tx_PDM_Sec_04S;
  PduR_FmFifoElementRamType TpSharedBufferQueue_BB2_Rx_VMCU_BB2_57P;
} PduR_FmFifoElementRamStructSType;

/**   \brief  type to be used as symbolic data element access to PduR_FmFifoInstanceRam */
typedef struct PduR_FmFifoInstanceRamStructSTag
{
  PduR_FmFifoInstanceRamType Alarm_Sec_03S_oSecuritySubnet_7f4118ec_Rx;
  PduR_FmFifoInstanceRamType PhysDiagRespMsg_F2_A2_BB2_oBackbone2_3cccad73_Tx;
  PduR_FmFifoInstanceRamType CIOM_Cab_11S_oCabSubnet_c1389abd_Tx;
  PduR_FmFifoInstanceRamType DiagReqMsgIntHMIIOM_98_F3_Cab_oCabSubnet_4bc610a1_Tx;
  PduR_FmFifoInstanceRamType IntTesterTGW2FuncDiagMsg_Cab_oCabSubnet_302595ad_Tx;
  PduR_FmFifoInstanceRamType IntTesterTGW2FuncDiagMsg_Sec_oSecuritySubnet_04594da9_Tx;
  PduR_FmFifoInstanceRamType CIOM_Cab_34P_oCabSubnet_3fa59da8_Tx;
  PduR_FmFifoInstanceRamType CIOM_BB2_13S_oBackbone2_0fe85b2c_Tx;
  PduR_FmFifoInstanceRamType HMIIOM_BB2_35S_oBackbone2_a0ca074b_Rx;
  PduR_FmFifoInstanceRamType PhysDiagRespMsg_F2_98_BB2_oBackbone2_0931b1de_Tx;
  PduR_FmFifoInstanceRamType CIOM_BB2_30S_oBackbone2_ab127275_Tx;
  PduR_FmFifoInstanceRamType DiagRespMsgIntHMIIOM_F3_C0_BB2_oBackbone2_6082985a_Tx;
  PduR_FmFifoInstanceRamType PhysDiagRespMsg_F1_53_BB2_oBackbone2_4dbfc71c_Tx;
  PduR_FmFifoInstanceRamType PhysDiagReqMsg_A1_F2_Sec_oSecuritySubnet_bfad192c_Tx;
  PduR_FmFifoInstanceRamType DiagReqMsgIntHMIIOM_A1_F3_Sec_oSecuritySubnet_06e2b6e6_Tx;
  PduR_FmFifoInstanceRamType DiagRespMsgIntTGW2_F4_A2_BB2_oBackbone2_194aa58a_Tx;
  PduR_FmFifoInstanceRamType DI_X_TACHO_oBackbone1J1939_0dca0ef5_Rx;
  PduR_FmFifoInstanceRamType DiagRespMsgIntTGW2_F4_C0_BB2_oBackbone2_ac972330_Tx;
  PduR_FmFifoInstanceRamType HMIIOM_BB2_20S_oBackbone2_20ea552e_Rx;
  PduR_FmFifoInstanceRamType PhysDiagRespMsg_F2_26_BB2_oBackbone2_1be3a078_Tx;
  PduR_FmFifoInstanceRamType CIOM_BB2_22S_oBackbone2_a525ef93_Tx;
  PduR_FmFifoInstanceRamType DiagReqMsgIntTGW2_98_F4_Cab_oCabSubnet_8583943c_Tx;
  PduR_FmFifoInstanceRamType PropTCO2_X_TACHO_oBackbone1J1939_d2ebdcdb_Rx;
  PduR_FmFifoInstanceRamType CIOM_Cab_20S_oCabSubnet_6bf52e02_Tx;
  PduR_FmFifoInstanceRamType CIOM_Sec_09S_oSecuritySubnet_9aeecef3_Tx;
  PduR_FmFifoInstanceRamType CIOM_Cab_25S_oCabSubnet_eba53182_Tx;
  PduR_FmFifoInstanceRamType DiagReqMsgIntHMIIOM_A2_F3_Cab_oCabSubnet_bf1d0d5b_Tx;
  PduR_FmFifoInstanceRamType CIOM_Cab_29S_oCabSubnet_793b1ffc_Tx;
  PduR_FmFifoInstanceRamType VMCU_BB2_34S_oBackbone2_9c0e9ff2_Rx;
  PduR_FmFifoInstanceRamType PhysDiagReqMsg_53_F1_Cab_oCabSubnet_0dfb69e8_Tx;
  PduR_FmFifoInstanceRamType TECU_BB2_05S_oBackbone2_692c9645_Rx;
  PduR_FmFifoInstanceRamType PhysDiagReqMsg_A2_F2_Cab_oCabSubnet_70f4cf94_Tx;
  PduR_FmFifoInstanceRamType TesterFuncDiagMsg_Cab_oCabSubnet_766a0d5a_Tx;
  PduR_FmFifoInstanceRamType TesterFuncDiagMsg_Sec_oSecuritySubnet_50e2aa6d_Tx;
  PduR_FmFifoInstanceRamType CIOM_BB2_12S_oBackbone2_fe325e86_Tx;
  PduR_FmFifoInstanceRamType DiagRespMsgIntTGW2_F4_A0_BB2_oBackbone2_5d078c5b_Tx;
  PduR_FmFifoInstanceRamType DiagReqMsgIntTGW2_53_F4_Cab_oCabSubnet_878411a3_Tx;
  PduR_FmFifoInstanceRamType DiagRespMsgIntHMIIOM_F3_A2_BB2_oBackbone2_d55f1ee0_Tx;
  PduR_FmFifoInstanceRamType DiagRespMsgIntHMIIOM_F3_A1_BB2_oBackbone2_5e8c20f9_Tx;
  PduR_FmFifoInstanceRamType DiagRespMsgIntHMIIOM_F3_53_BB2_oBackbone2_e9c474e8_Tx;
  PduR_FmFifoInstanceRamType DiagReqMsgIntTGW2_26_F4_Cab_oCabSubnet_63722514_Tx;
  PduR_FmFifoInstanceRamType Alarm_Sec_07S_oSecuritySubnet_74e9cfea_Rx;
  PduR_FmFifoInstanceRamType PhysDiagReqMsg_A0_F2_Sec_oSecuritySubnet_821cf5f0_Tx;
  PduR_FmFifoInstanceRamType DiagReqMsgIntTGW2_A0_F4_Sec_oSecuritySubnet_c9e0207f_Tx;
  PduR_FmFifoInstanceRamType DiagReqMsgIntHMIIOM_C0_F3_Sec_oSecuritySubnet_fe92025d_Tx;
  PduR_FmFifoInstanceRamType EMS_BB2_09S_oBackbone2_f00cfd06_Rx;
  PduR_FmFifoInstanceRamType CIOM_Cab_05S_oCabSubnet_86401064_Tx;
  PduR_FmFifoInstanceRamType DiagReqMsgIntTGW2_C0_F4_Sec_oSecuritySubnet_0c217818_Tx;
  PduR_FmFifoInstanceRamType PhysDiagReqMsg_C0_F2_Sec_oSecuritySubnet_47ddad97_Tx;
  PduR_FmFifoInstanceRamType DiagRespMsgIntTGW2_F4_98_BB2_oBackbone2_2cb7b927_Tx;
  PduR_FmFifoInstanceRamType DiagReqMsgIntHMIIOM_53_F3_Cab_oCabSubnet_49c1953e_Tx;
  PduR_FmFifoInstanceRamType DiagReqMsgIntTGW2_A1_F4_Sec_oSecuritySubnet_f451cca3_Tx;
  PduR_FmFifoInstanceRamType DDM_Sec_04S_oSecuritySubnet_f7af7427_Rx;
  PduR_FmFifoInstanceRamType DiagRespMsgIntTGW2_F4_53_BB2_oBackbone2_25d1cf82_Tx;
  PduR_FmFifoInstanceRamType PhysDiagReqMsg_26_F2_Cab_oCabSubnet_62de6346_Tx;
  PduR_FmFifoInstanceRamType PhysDiagRespMsg_F2_A0_BB2_oBackbone2_788184a2_Tx;
  PduR_FmFifoInstanceRamType PhysDiagReqMsg_53_F2_Cab_oCabSubnet_862857f1_Tx;
  PduR_FmFifoInstanceRamType CIOM_Sec_07S_oSecuritySubnet_e302eca2_Tx;
  PduR_FmFifoInstanceRamType DDM_Sec_03S_oSecuritySubnet_16817e43_Rx;
  PduR_FmFifoInstanceRamType HMIIOM_BB2_36S_oBackbone2_1d006b85_Rx;
  PduR_FmFifoInstanceRamType DiagReqMsgIntHMIIOM_A0_F3_Sec_oSecuritySubnet_3b535a3a_Tx;
  PduR_FmFifoInstanceRamType DiagRespMsgIntHMIIOM_F3_A0_BB2_oBackbone2_91123731_Tx;
  PduR_FmFifoInstanceRamType HMIIOM_BB2_21S_oBackbone2_fd7c8cab_Rx;
  PduR_FmFifoInstanceRamType VMCU_BB2_31S_oBackbone2_81212ce1_Rx;
  PduR_FmFifoInstanceRamType CIOM_Sec_11S_oSecuritySubnet_d4660cbf_Tx;
  PduR_FmFifoInstanceRamType DiagReqMsgIntHMIIOM_26_F3_Cab_oCabSubnet_ad37a189_Tx;
  PduR_FmFifoInstanceRamType CIOM_Cab_04S_oCabSubnet_779a15ce_Tx;
  PduR_FmFifoInstanceRamType HMIIOM_BB2_19P_CIOM_oBackbone2_da8f8d8b_Rx;
  PduR_FmFifoInstanceRamType CIOM_Sec_05S_oSecuritySubnet_7a00e33f_Tx;
  PduR_FmFifoInstanceRamType PhysDiagRespMsg_F2_53_BB2_oBackbone2_0057c77b_Tx;
  PduR_FmFifoInstanceRamType CIOM_Sec_06S_oSecuritySubnet_423b684c_Tx;
  PduR_FmFifoInstanceRamType DDM_Sec_05S_oSecuritySubnet_18fdc2c6_Rx;
  PduR_FmFifoInstanceRamType DiagRespMsgIntTGW2_F4_A1_BB2_oBackbone2_92999b93_Tx;
  PduR_FmFifoInstanceRamType CCM_Cab_03P_oCabSubnet_59cf0584_Rx;
  PduR_FmFifoInstanceRamType DiagRespMsgIntHMIIOM_F3_26_BB2_oBackbone2_f27013eb_Tx;
  PduR_FmFifoInstanceRamType DiagReqMsgIntTGW2_A2_F4_Cab_oCabSubnet_715889c6_Tx;
  PduR_FmFifoInstanceRamType VMCU_BB2_32S_oBackbone2_3ceb402f_Rx;
  PduR_FmFifoInstanceRamType HMIIOM_BB2_27S_oBackbone2_5d995376_Rx;
  PduR_FmFifoInstanceRamType BBM_BB2_03S_CIOM_oBackbone2_d5047985_Rx;
  PduR_FmFifoInstanceRamType CIOM_Sec_10S_oSecuritySubnet_755f8851_Tx;
  PduR_FmFifoInstanceRamType DiagRespMsgIntHMIIOM_F3_98_BB2_oBackbone2_e0a2024d_Tx;
  PduR_FmFifoInstanceRamType DiagRespMsgIntTGW2_F4_26_BB2_oBackbone2_3e65a881_Tx;
  PduR_FmFifoInstanceRamType CIOM_Sec_08S_oSecuritySubnet_3bd74a1d_Tx;
  PduR_FmFifoInstanceRamType CIOM_Sec_12S_oSecuritySubnet_ec5d87cc_Tx;
  PduR_FmFifoInstanceRamType PhysDiagRespMsg_F2_A1_BB2_oBackbone2_b71f936a_Tx;
  PduR_FmFifoInstanceRamType Alarm_Sec_06S_oSecuritySubnet_9bbb790b_Rx;
  PduR_FmFifoInstanceRamType HMIIOM_BB2_06S_oBackbone2_61614e5e_Rx;
  PduR_FmFifoInstanceRamType PhysDiagRespMsg_F2_C0_BB2_oBackbone2_89112bc9_Tx;
  PduR_FmFifoInstanceRamType PDM_Sec_03S_oSecuritySubnet_29b4be53_Rx;
  PduR_FmFifoInstanceRamType HMIIOM_BB2_04S_oBackbone2_013dfb15_Rx;
  PduR_FmFifoInstanceRamType TECU_BB2_06S_oBackbone2_d4e6fa8b_Rx;
  PduR_FmFifoInstanceRamType CIOM_Cab_13S_oCabSubnet_f9fd97a8_Tx;
  PduR_FmFifoInstanceRamType CIOM_Cab_30S_oCabSubnet_5d07bef1_Tx;
  PduR_FmFifoInstanceRamType EMS_BB2_11S_oBackbone2_3700cf1d_Rx;
  PduR_FmFifoInstanceRamType PhysDiagReqMsg_98_F2_Cab_oCabSubnet_842fd26e_Tx;
  PduR_FmFifoInstanceRamType PDM_Sec_04S_oSecuritySubnet_c89ab437_Rx;
  PduR_FmFifoInstanceRamType VMCU_BB2_57P_oBackbone2_ae986a7a_Rx;
} PduR_FmFifoInstanceRamStructSType;

/**   \brief  type to be used as symbolic data element access to PduR_FmFifoRam */
typedef struct PduR_FmFifoRamStructSTag
{
  PduR_FmFifoRamType TpSharedBufferQueue_Sec_Tx_Alarm_Sec_03S;
  PduR_FmFifoRamType TpSharedBufferQueue_PhysDiagRespMsg_F2_A2_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_11S;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagReqMsgIntHMIIOM_98_F3_Cab;
  PduR_FmFifoRamType TpSharedBufferQueue_IntTesterTGW2FuncDiagMsg;
  PduR_FmFifoRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_34P;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Tx_CIOM_BB2_13S;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_HMIIOM_BB2_35S;
  PduR_FmFifoRamType TpSharedBufferQueue_PhysDiagRespMsg_F2_98_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Tx_CIOM_BB2_30S;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagRespMsgIntHMIIOM_F3_C0_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_PhysDiagRespMsg_F1_53_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_PhysDiagReqMsg_A1_F2_Sec;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagReqMsgIntHMIIOM_A1_F3_Sec;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagRespMsgIntTGW2_F4_A2_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_BB_Rx_DI_X_TACHO;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagRespMsgIntTGW2_F4_C0_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_HMIIOM_BB2_20S;
  PduR_FmFifoRamType TpSharedBufferQueue_PhysDiagRespMsg_F2_26_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Tx_CIOM_BB2_22S;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagReqMsgIntTGW2_98_F4_Cab;
  PduR_FmFifoRamType TpSharedBufferQueue_BB1_Rx_PropTCO2_X_TACHO;
  PduR_FmFifoRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_20S;
  PduR_FmFifoRamType TpSharedBufferQueue_Sec_Tx_CIOM_Sec_09S;
  PduR_FmFifoRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_25S;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagReqMsgIntHMIIOM_A2_F3_Cab;
  PduR_FmFifoRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_29S;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_VMCU_BB2_34S;
  PduR_FmFifoRamType TpSharedBufferQueue_PhysDiagReqMsg_53_F1_Cab;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_TECU_BB2_05S;
  PduR_FmFifoRamType TpSharedBufferQueue_PhysDiagReqMsg_A2_F2_Cab;
  PduR_FmFifoRamType TpSharedBufferQueue_FuncDiagTx_F2;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Tx_CIOM_BB2_12S;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagRespMsgIntTGW2_F4_A0_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagReqMsgIntTGW2_53_F4_Cab;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagRespMsgIntHMIIOM_F3_A2_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagRespMsgIntHMIIOM_F3_A1_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagRespMsgIntHMIIOM_F3_53_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagReqMsgIntTGW2_26_F4_Cab;
  PduR_FmFifoRamType TpSharedBufferQueue_Sec_Tx_Alarm_Sec_07S;
  PduR_FmFifoRamType TpSharedBufferQueue_PhysDiagReqMsg_A0_F2_Sec;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagReqMsgIntTGW2_A0_F4_Sec;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagReqMsgIntHMIIOM_C0_F3_Sec;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_EMS_BB2_09S;
  PduR_FmFifoRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_05S;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagReqMsgIntTGW2_C0_F4_Sec;
  PduR_FmFifoRamType TpSharedBufferQueue_PhysDiagReqMsg_C0_F2_Sec;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagRespMsgIntTGW2_F4_98_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagReqMsgIntHMIIOM_53_F3_Cab;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagReqMsgIntTGW2_A1_F4_Sec;
  PduR_FmFifoRamType TpSharedBufferQueue_Sec_Tx_DDM_Sec_04S;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagRespMsgIntTGW2_F4_53_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_PhysDiagReqMsg_26_F2_Cab;
  PduR_FmFifoRamType TpSharedBufferQueue_PhysDiagRespMsg_F2_A0_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_PhysDiagReqMsg_53_F2_Cab;
  PduR_FmFifoRamType TpSharedBufferQueue_Sec_Tx_CIOM_Sec_07S;
  PduR_FmFifoRamType TpSharedBufferQueue_Sec_Tx_DDM_Sec_03S;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_HMIIOM_BB2_36S;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagReqMsgIntHMIIOM_A0_F3_Sec;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagRespMsgIntHMIIOM_F3_A0_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_HMIIOM_BB2_21S;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_VMCU_BB2_31S;
  PduR_FmFifoRamType TpSharedBufferQueue_Sec_Tx_CIOM_Sec_11S;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagReqMsgIntHMIIOM_26_F3_Cab;
  PduR_FmFifoRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_04S;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_HMIIOM_BB2_19P;
  PduR_FmFifoRamType TpSharedBufferQueue_Sec_Tx_CIOM_Sec_05S;
  PduR_FmFifoRamType TpSharedBufferQueue_PhysDiagRespMsg_F2_53_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_Sec_Tx_CIOM_Sec_06S;
  PduR_FmFifoRamType TpSharedBufferQueue_Sec_Tx_DDM_Sec_05S;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagRespMsgIntTGW2_F4_A1_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_Cab_Rx_CCM_Cab_03P;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagRespMsgIntHMIIOM_F3_26_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagReqMsgIntTGW2_A2_F4_Cab;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_VMCU_BB2_32S;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_HMIIOM_BB2_27S;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_BBM_BB2_03S_CIOM;
  PduR_FmFifoRamType TpSharedBufferQueue_Sec_Tx_CIOM_Sec_10S;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagRespMsgIntHMIIOM_F3_98_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_DiagRespMsgIntTGW2_F4_26_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_Sec_Tx_CIOM_Sec_08S;
  PduR_FmFifoRamType TpSharedBufferQueue_Sec_Tx_CIOM_Sec_12S;
  PduR_FmFifoRamType TpSharedBufferQueue_PhysDiagRespMsg_F2_A1_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_Sec_Tx_Alarm_Sec_06S;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_HMIIOM_BB2_06S;
  PduR_FmFifoRamType TpSharedBufferQueue_PhysDiagRespMsg_F2_C0_BB2;
  PduR_FmFifoRamType TpSharedBufferQueue_Sec_Tx_PDM_Sec_03S;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_HMIIOM_BB2_04S;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_TECU_BB2_06S;
  PduR_FmFifoRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_13S;
  PduR_FmFifoRamType TpSharedBufferQueue_Cab_Tx_CIOM_Cab_30S;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_EMS_BB2_11S;
  PduR_FmFifoRamType TpSharedBufferQueue_PhysDiagReqMsg_98_F2_Cab;
  PduR_FmFifoRamType TpSharedBufferQueue_Sec_Tx_PDM_Sec_04S;
  PduR_FmFifoRamType TpSharedBufferQueue_BB2_Rx_VMCU_BB2_57P;
} PduR_FmFifoRamStructSType;

/**   \brief  type to be used as symbolic data element access to PduR_RmBufferedTpPropertiesRam */
typedef struct PduR_RmBufferedTpPropertiesRamStructSTag
{
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_01528206;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_017c679d;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_05d2c983;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_064dc9ab;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_06bdd5c2;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_06f4c24e;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_0948f4eb;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_0aeda82a;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_187ffacf;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_1ac6f203;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_1afac4d5;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_1b947dce;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_1c80e599;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_246f1e3b;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_25a59123;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_266542d7;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_26a1cbb9;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_26b85268;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_270fccd3;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_2b407fd5;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_2e6f76ae;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_2f32e214;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_304197c1;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_304f257a;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_30b4cd32;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_39fe25b5;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_3c553d77;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_408ae78e;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_446f53c1;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_44bef04e;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_46a397e3;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_4a8c70b8;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_5cdf6731;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_5cfa406e;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_5f2267d8;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_62ae1653;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_63947842;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_656dbd2a;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_6ea67099;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_7040bac2;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_71b786d1;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_75cfe088;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_7ab1f990;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_8259d96a;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_86e04d33;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_89263a8e;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_89789648;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_8a32f8f6;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_8c488735;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_8c4f8f58;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_918dff20;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_962aaefb;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_973cde10;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_99763ffd;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_99e30330;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_9d957b7c;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_9e5d6760;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_a40032de;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_ab3d4554;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_ac4babc0;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_b18fc6c3;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_b4b812c0;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_b9d033d4;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_bbc023c9;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_bc75d45f;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_bd0d068c;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_bd1ce89a;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_be2dd0ee;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_bfa5c35c;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_bfda77cf;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_c050dec8;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_c0583aba;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_c0680d6c;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_c28e0d5d;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_c4655c6f;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_c6acd63d;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_ccea7767;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_cd1487f2;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_ce8b3ea0;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_d6988d26;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_d71ba6b1;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_d91d7573;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_d9d30309;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_da0a8dca;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_dbfd746c;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_df90de79;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_df9a30f3;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_dfacef7f;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_ea4e2140;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_eb982057;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_f1553bf5;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_f4695c57;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_fdae10f0;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_fed5ac9b;
  PduR_RmBufferedTpPropertiesRamType PduRSrcPdu_ff6a6468;
} PduR_RmBufferedTpPropertiesRamStructSType;

/**   \brief  type to be used as symbolic data element access to PduR_RmGDestTpTxStateRam */
typedef struct PduR_RmGDestTpTxStateRamStructSTag
{
  PduR_RmGDestTpTxStateRamType Alarm_Sec_03S_oSecuritySubnet_7f4118ec_Rx;
  PduR_RmGDestTpTxStateRamType Alarm_Sec_06S_oSecuritySubnet_9bbb790b_Rx;
  PduR_RmGDestTpTxStateRamType Alarm_Sec_07S_oSecuritySubnet_74e9cfea_Rx;
  PduR_RmGDestTpTxStateRamType BBM_BB2_03S_CIOM_oBackbone2_d5047985_Rx;
  PduR_RmGDestTpTxStateRamType CCM_Cab_03P_oCabSubnet_59cf0584_Rx;
  PduR_RmGDestTpTxStateRamType CIOM_BB2_12S_oBackbone2_fe325e86_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_BB2_13S_oBackbone2_0fe85b2c_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_BB2_22S_oBackbone2_a525ef93_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_BB2_30S_oBackbone2_ab127275_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Cab_04S_oCabSubnet_779a15ce_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Cab_05S_oCabSubnet_86401064_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Cab_11S_oCabSubnet_c1389abd_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Cab_13S_oCabSubnet_f9fd97a8_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Cab_20S_oCabSubnet_6bf52e02_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Cab_25S_oCabSubnet_eba53182_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Cab_29S_oCabSubnet_793b1ffc_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Cab_30S_oCabSubnet_5d07bef1_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Cab_34P_oCabSubnet_3fa59da8_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Sec_05S_oSecuritySubnet_7a00e33f_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Sec_06S_oSecuritySubnet_423b684c_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Sec_07S_oSecuritySubnet_e302eca2_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Sec_08S_oSecuritySubnet_3bd74a1d_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Sec_09S_oSecuritySubnet_9aeecef3_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Sec_10S_oSecuritySubnet_755f8851_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Sec_11S_oSecuritySubnet_d4660cbf_Tx;
  PduR_RmGDestTpTxStateRamType CIOM_Sec_12S_oSecuritySubnet_ec5d87cc_Tx;
  PduR_RmGDestTpTxStateRamType DDM_Sec_03S_oSecuritySubnet_16817e43_Rx;
  PduR_RmGDestTpTxStateRamType DDM_Sec_04S_oSecuritySubnet_f7af7427_Rx;
  PduR_RmGDestTpTxStateRamType DDM_Sec_05S_oSecuritySubnet_18fdc2c6_Rx;
  PduR_RmGDestTpTxStateRamType DI_X_TACHO_oBackbone1J1939_0dca0ef5_Rx;
  PduR_RmGDestTpTxStateRamType DiagReqMsgIntHMIIOM_26_F3_Cab_oCabSubnet_ad37a189_Tx;
  PduR_RmGDestTpTxStateRamType DiagReqMsgIntHMIIOM_53_F3_Cab_oCabSubnet_49c1953e_Tx;
  PduR_RmGDestTpTxStateRamType DiagReqMsgIntHMIIOM_98_F3_Cab_oCabSubnet_4bc610a1_Tx;
  PduR_RmGDestTpTxStateRamType DiagReqMsgIntHMIIOM_A0_F3_Sec_oSecuritySubnet_3b535a3a_Tx;
  PduR_RmGDestTpTxStateRamType DiagReqMsgIntHMIIOM_A1_F3_Sec_oSecuritySubnet_06e2b6e6_Tx;
  PduR_RmGDestTpTxStateRamType DiagReqMsgIntHMIIOM_A2_F3_Cab_oCabSubnet_bf1d0d5b_Tx;
  PduR_RmGDestTpTxStateRamType DiagReqMsgIntHMIIOM_C0_F3_Sec_oSecuritySubnet_fe92025d_Tx;
  PduR_RmGDestTpTxStateRamType DiagReqMsgIntTGW2_26_F4_Cab_oCabSubnet_63722514_Tx;
  PduR_RmGDestTpTxStateRamType DiagReqMsgIntTGW2_53_F4_Cab_oCabSubnet_878411a3_Tx;
  PduR_RmGDestTpTxStateRamType DiagReqMsgIntTGW2_98_F4_Cab_oCabSubnet_8583943c_Tx;
  PduR_RmGDestTpTxStateRamType DiagReqMsgIntTGW2_A0_F4_Sec_oSecuritySubnet_c9e0207f_Tx;
  PduR_RmGDestTpTxStateRamType DiagReqMsgIntTGW2_A1_F4_Sec_oSecuritySubnet_f451cca3_Tx;
  PduR_RmGDestTpTxStateRamType DiagReqMsgIntTGW2_A2_F4_Cab_oCabSubnet_715889c6_Tx;
  PduR_RmGDestTpTxStateRamType DiagReqMsgIntTGW2_C0_F4_Sec_oSecuritySubnet_0c217818_Tx;
  PduR_RmGDestTpTxStateRamType DiagRespMsgIntHMIIOM_F3_26_BB2_oBackbone2_f27013eb_Tx;
  PduR_RmGDestTpTxStateRamType DiagRespMsgIntHMIIOM_F3_53_BB2_oBackbone2_e9c474e8_Tx;
  PduR_RmGDestTpTxStateRamType DiagRespMsgIntHMIIOM_F3_98_BB2_oBackbone2_e0a2024d_Tx;
  PduR_RmGDestTpTxStateRamType DiagRespMsgIntHMIIOM_F3_A0_BB2_oBackbone2_91123731_Tx;
  PduR_RmGDestTpTxStateRamType DiagRespMsgIntHMIIOM_F3_A1_BB2_oBackbone2_5e8c20f9_Tx;
  PduR_RmGDestTpTxStateRamType DiagRespMsgIntHMIIOM_F3_A2_BB2_oBackbone2_d55f1ee0_Tx;
  PduR_RmGDestTpTxStateRamType DiagRespMsgIntHMIIOM_F3_C0_BB2_oBackbone2_6082985a_Tx;
  PduR_RmGDestTpTxStateRamType DiagRespMsgIntTGW2_F4_26_BB2_oBackbone2_3e65a881_Tx;
  PduR_RmGDestTpTxStateRamType DiagRespMsgIntTGW2_F4_53_BB2_oBackbone2_25d1cf82_Tx;
  PduR_RmGDestTpTxStateRamType DiagRespMsgIntTGW2_F4_98_BB2_oBackbone2_2cb7b927_Tx;
  PduR_RmGDestTpTxStateRamType DiagRespMsgIntTGW2_F4_A0_BB2_oBackbone2_5d078c5b_Tx;
  PduR_RmGDestTpTxStateRamType DiagRespMsgIntTGW2_F4_A1_BB2_oBackbone2_92999b93_Tx;
  PduR_RmGDestTpTxStateRamType DiagRespMsgIntTGW2_F4_A2_BB2_oBackbone2_194aa58a_Tx;
  PduR_RmGDestTpTxStateRamType DiagRespMsgIntTGW2_F4_C0_BB2_oBackbone2_ac972330_Tx;
  PduR_RmGDestTpTxStateRamType EMS_BB2_09S_oBackbone2_f00cfd06_Rx;
  PduR_RmGDestTpTxStateRamType EMS_BB2_11S_oBackbone2_3700cf1d_Rx;
  PduR_RmGDestTpTxStateRamType HMIIOM_BB2_04S_oBackbone2_013dfb15_Rx;
  PduR_RmGDestTpTxStateRamType HMIIOM_BB2_06S_oBackbone2_61614e5e_Rx;
  PduR_RmGDestTpTxStateRamType HMIIOM_BB2_19P_CIOM_oBackbone2_da8f8d8b_Rx;
  PduR_RmGDestTpTxStateRamType HMIIOM_BB2_20S_oBackbone2_20ea552e_Rx;
  PduR_RmGDestTpTxStateRamType HMIIOM_BB2_21S_oBackbone2_fd7c8cab_Rx;
  PduR_RmGDestTpTxStateRamType HMIIOM_BB2_27S_oBackbone2_5d995376_Rx;
  PduR_RmGDestTpTxStateRamType HMIIOM_BB2_35S_oBackbone2_a0ca074b_Rx;
  PduR_RmGDestTpTxStateRamType HMIIOM_BB2_36S_oBackbone2_1d006b85_Rx;
  PduR_RmGDestTpTxStateRamType IntTesterTGW2FuncDiagMsg_BB2_oBackbone2_5e7bda8e_Rx;
  PduR_RmGDestTpTxStateRamType IntTesterTGW2FuncDiagMsg_Cab_oCabSubnet_302595ad_Tx;
  PduR_RmGDestTpTxStateRamType IntTesterTGW2FuncDiagMsg_Sec_oSecuritySubnet_04594da9_Tx;
  PduR_RmGDestTpTxStateRamType PDM_Sec_03S_oSecuritySubnet_29b4be53_Rx;
  PduR_RmGDestTpTxStateRamType PDM_Sec_04S_oSecuritySubnet_c89ab437_Rx;
  PduR_RmGDestTpTxStateRamType PhysDiagReqMsg_26_F2_Cab_oCabSubnet_62de6346_Tx;
  PduR_RmGDestTpTxStateRamType PhysDiagReqMsg_53_F1_Cab_oCabSubnet_0dfb69e8_Tx;
  PduR_RmGDestTpTxStateRamType PhysDiagReqMsg_53_F2_Cab_oCabSubnet_862857f1_Tx;
  PduR_RmGDestTpTxStateRamType PhysDiagReqMsg_98_F2_Cab_oCabSubnet_842fd26e_Tx;
  PduR_RmGDestTpTxStateRamType PhysDiagReqMsg_A0_F2_Sec_oSecuritySubnet_821cf5f0_Tx;
  PduR_RmGDestTpTxStateRamType PhysDiagReqMsg_A1_F2_Sec_oSecuritySubnet_bfad192c_Tx;
  PduR_RmGDestTpTxStateRamType PhysDiagReqMsg_A2_F2_Cab_oCabSubnet_70f4cf94_Tx;
  PduR_RmGDestTpTxStateRamType PhysDiagReqMsg_C0_F2_Sec_oSecuritySubnet_47ddad97_Tx;
  PduR_RmGDestTpTxStateRamType PhysDiagRespMsg_F1_53_BB2_oBackbone2_4dbfc71c_Tx;
  PduR_RmGDestTpTxStateRamType PhysDiagRespMsg_F2_26_BB2_oBackbone2_1be3a078_Tx;
  PduR_RmGDestTpTxStateRamType PhysDiagRespMsg_F2_53_BB2_oBackbone2_0057c77b_Tx;
  PduR_RmGDestTpTxStateRamType PhysDiagRespMsg_F2_98_BB2_oBackbone2_0931b1de_Tx;
  PduR_RmGDestTpTxStateRamType PhysDiagRespMsg_F2_A0_BB2_oBackbone2_788184a2_Tx;
  PduR_RmGDestTpTxStateRamType PhysDiagRespMsg_F2_A1_BB2_oBackbone2_b71f936a_Tx;
  PduR_RmGDestTpTxStateRamType PhysDiagRespMsg_F2_A2_BB2_oBackbone2_3cccad73_Tx;
  PduR_RmGDestTpTxStateRamType PhysDiagRespMsg_F2_C0_BB2_oBackbone2_89112bc9_Tx;
  PduR_RmGDestTpTxStateRamType PropTCO2_X_TACHO_oBackbone1J1939_d2ebdcdb_Rx;
  PduR_RmGDestTpTxStateRamType TECU_BB2_05S_oBackbone2_692c9645_Rx;
  PduR_RmGDestTpTxStateRamType TECU_BB2_06S_oBackbone2_d4e6fa8b_Rx;
  PduR_RmGDestTpTxStateRamType TesterFuncDiagMsg_BB2_oBackbone2_7fc3ea4b_Rx;
  PduR_RmGDestTpTxStateRamType TesterFuncDiagMsg_Cab_oCabSubnet_766a0d5a_Tx;
  PduR_RmGDestTpTxStateRamType TesterFuncDiagMsg_Sec_oSecuritySubnet_50e2aa6d_Tx;
  PduR_RmGDestTpTxStateRamType VMCU_BB2_31S_oBackbone2_81212ce1_Rx;
  PduR_RmGDestTpTxStateRamType VMCU_BB2_32S_oBackbone2_3ceb402f_Rx;
  PduR_RmGDestTpTxStateRamType VMCU_BB2_34S_oBackbone2_9c0e9ff2_Rx;
  PduR_RmGDestTpTxStateRamType VMCU_BB2_57P_oBackbone2_ae986a7a_Rx;
} PduR_RmGDestTpTxStateRamStructSType;

/** 
  \}
*/ 

/** 
  \defgroup  PduRPCUnionIndexAndSymbolTypes  PduR Union Index And Symbol Types (PRE_COMPILE)
  \brief  These unions are used to access arrays in an index and symbol based style.
  \{
*/ 
/**   \brief  type to access PduR_BmTxBufferArrayRam in an index and symbol based style. */
typedef union PduR_BmTxBufferArrayRamUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  PduR_BmTxBufferArrayRamType raw[7106];
  PduR_BmTxBufferArrayRamStructSType str;
} PduR_BmTxBufferArrayRamUType;

/**   \brief  type to access PduR_BmTxBufferInstanceRam in an index and symbol based style. */
typedef union PduR_BmTxBufferInstanceRamUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  PduR_BmTxBufferInstanceRamType raw[81];
  PduR_BmTxBufferInstanceRamStructSType str;
} PduR_BmTxBufferInstanceRamUType;

/**   \brief  type to access PduR_BmTxBufferRam in an index and symbol based style. */
typedef union PduR_BmTxBufferRamUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  PduR_BmTxBufferRamType raw[77];
  PduR_BmTxBufferRamStructSType str;
} PduR_BmTxBufferRamUType;

/**   \brief  type to access PduR_FmFifoElementRam in an index and symbol based style. */
typedef union PduR_FmFifoElementRamUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  PduR_FmFifoElementRamType raw[151];
  PduR_FmFifoElementRamStructSType str;
} PduR_FmFifoElementRamUType;

/**   \brief  type to access PduR_FmFifoInstanceRam in an index and symbol based style. */
typedef union PduR_FmFifoInstanceRamUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  PduR_FmFifoInstanceRamType raw[97];
  PduR_FmFifoInstanceRamStructSType str;
} PduR_FmFifoInstanceRamUType;

/**   \brief  type to access PduR_FmFifoRam in an index and symbol based style. */
typedef union PduR_FmFifoRamUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  PduR_FmFifoRamType raw[95];
  PduR_FmFifoRamStructSType str;
} PduR_FmFifoRamUType;

/**   \brief  type to access PduR_RmBufferedTpPropertiesRam in an index and symbol based style. */
typedef union PduR_RmBufferedTpPropertiesRamUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  PduR_RmBufferedTpPropertiesRamType raw[95];
  PduR_RmBufferedTpPropertiesRamStructSType str;
} PduR_RmBufferedTpPropertiesRamUType;

/**   \brief  type to access PduR_RmGDestTpTxStateRam in an index and symbol based style. */
typedef union PduR_RmGDestTpTxStateRamUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  PduR_RmGDestTpTxStateRamType raw[99];
  PduR_RmGDestTpTxStateRamStructSType str;
} PduR_RmGDestTpTxStateRamUType;

/** 
  \}
*/ 

/** 
  \defgroup  PduRPCRootPointerTypes  PduR Root Pointer Types (PRE_COMPILE)
  \brief  These type definitions are used to point from the config root to symbol instances.
  \{
*/ 
/**   \brief  type used to point to PduR_BmTxBufferArrayRam */
typedef P2VAR(PduR_BmTxBufferArrayRamType, TYPEDEF, PDUR_VAR_NOINIT) PduR_BmTxBufferArrayRamPtrType;

/**   \brief  type used to point to PduR_BmTxBufferIndRom */
typedef P2CONST(PduR_BmTxBufferIndRomType, TYPEDEF, PDUR_CONST) PduR_BmTxBufferIndRomPtrType;

/**   \brief  type used to point to PduR_BmTxBufferInstanceRam */
typedef P2VAR(PduR_BmTxBufferInstanceRamType, TYPEDEF, PDUR_VAR_NOINIT) PduR_BmTxBufferInstanceRamPtrType;

/**   \brief  type used to point to PduR_BmTxBufferInstanceRom */
typedef P2CONST(PduR_BmTxBufferInstanceRomType, TYPEDEF, PDUR_CONST) PduR_BmTxBufferInstanceRomPtrType;

/**   \brief  type used to point to PduR_BmTxBufferRam */
typedef P2VAR(PduR_BmTxBufferRamType, TYPEDEF, PDUR_VAR_NOINIT) PduR_BmTxBufferRamPtrType;

/**   \brief  type used to point to PduR_BmTxBufferRom */
typedef P2CONST(PduR_BmTxBufferRomType, TYPEDEF, PDUR_CONST) PduR_BmTxBufferRomPtrType;

/**   \brief  type used to point to PduR_CoreManagerRom */
typedef P2CONST(PduR_CoreManagerRomType, TYPEDEF, PDUR_CONST) PduR_CoreManagerRomPtrType;

/**   \brief  type used to point to PduR_ExclusiveAreaRom */
typedef P2CONST(PduR_ExclusiveAreaRomType, TYPEDEF, PDUR_CONST) PduR_ExclusiveAreaRomPtrType;

/**   \brief  type used to point to PduR_FmFifoElementRam */
typedef P2VAR(PduR_FmFifoElementRamType, TYPEDEF, PDUR_VAR_NOINIT) PduR_FmFifoElementRamPtrType;

/**   \brief  type used to point to PduR_FmFifoInstanceRam */
typedef P2VAR(PduR_FmFifoInstanceRamType, TYPEDEF, PDUR_VAR_NOINIT) PduR_FmFifoInstanceRamPtrType;

/**   \brief  type used to point to PduR_FmFifoInstanceRom */
typedef P2CONST(PduR_FmFifoInstanceRomType, TYPEDEF, PDUR_CONST) PduR_FmFifoInstanceRomPtrType;

/**   \brief  type used to point to PduR_FmFifoRam */
typedef P2VAR(PduR_FmFifoRamType, TYPEDEF, PDUR_VAR_NOINIT) PduR_FmFifoRamPtrType;

/**   \brief  type used to point to PduR_FmFifoRom */
typedef P2CONST(PduR_FmFifoRomType, TYPEDEF, PDUR_CONST) PduR_FmFifoRomPtrType;

/**   \brief  type used to point to PduR_Fm_ActivateNext_FmSmStateHandler */
typedef P2CONST(PduR_Fm_ActivateNext_FmSmStateHandlerType, TYPEDEF, PDUR_CONST) PduR_Fm_ActivateNext_FmSmStateHandlerPtrType;

/**   \brief  type used to point to PduR_Fm_ActivateRead_FmSmStateHandler */
typedef P2CONST(PduR_Fm_ActivateRead_FmSmStateHandlerType, TYPEDEF, PDUR_CONST) PduR_Fm_ActivateRead_FmSmStateHandlerPtrType;

/**   \brief  type used to point to PduR_Fm_ActivateWrite_FmSmStateHandler */
typedef P2CONST(PduR_Fm_ActivateWrite_FmSmStateHandlerType, TYPEDEF, PDUR_CONST) PduR_Fm_ActivateWrite_FmSmStateHandlerPtrType;

/**   \brief  type used to point to PduR_Fm_FinishRead_FmSmStateHandler */
typedef P2CONST(PduR_Fm_FinishRead_FmSmStateHandlerType, TYPEDEF, PDUR_CONST) PduR_Fm_FinishRead_FmSmStateHandlerPtrType;

/**   \brief  type used to point to PduR_Fm_FinishWrite_FmSmStateHandler */
typedef P2CONST(PduR_Fm_FinishWrite_FmSmStateHandlerType, TYPEDEF, PDUR_CONST) PduR_Fm_FinishWrite_FmSmStateHandlerPtrType;

/**   \brief  type used to point to PduR_GeneralPropertiesRom */
typedef P2CONST(PduR_GeneralPropertiesRomType, TYPEDEF, PDUR_CONST) PduR_GeneralPropertiesRomPtrType;

/**   \brief  type used to point to PduR_Initialized */
typedef P2VAR(PduR_InitializedType, TYPEDEF, PDUR_VAR_ZERO_INIT) PduR_InitializedPtrType;

/**   \brief  type used to point to PduR_LockRom */
typedef P2CONST(PduR_LockRomType, TYPEDEF, PDUR_CONST) PduR_LockRomPtrType;

/**   \brief  type used to point to PduR_MmRom */
typedef P2CONST(PduR_MmRomType, TYPEDEF, PDUR_CONST) PduR_MmRomPtrType;

/**   \brief  type used to point to PduR_MmRomInd */
typedef P2CONST(PduR_MmRomIndType, TYPEDEF, PDUR_CONST) PduR_MmRomIndPtrType;

/**   \brief  type used to point to PduR_RmBufferedTpPropertiesRam */
typedef P2VAR(PduR_RmBufferedTpPropertiesRamType, TYPEDEF, PDUR_VAR_NOINIT) PduR_RmBufferedTpPropertiesRamPtrType;

/**   \brief  type used to point to PduR_RmBufferedTpPropertiesRom */
typedef P2CONST(PduR_RmBufferedTpPropertiesRomType, TYPEDEF, PDUR_CONST) PduR_RmBufferedTpPropertiesRomPtrType;

/**   \brief  type used to point to PduR_RmDestRom */
typedef P2CONST(PduR_RmDestRomType, TYPEDEF, PDUR_CONST) PduR_RmDestRomPtrType;

/**   \brief  type used to point to PduR_RmGDestRom */
typedef P2CONST(PduR_RmGDestRomType, TYPEDEF, PDUR_CONST) PduR_RmGDestRomPtrType;

/**   \brief  type used to point to PduR_RmGDestTpTxStateRam */
typedef P2VAR(PduR_RmGDestTpTxStateRamType, TYPEDEF, PDUR_VAR_NOINIT) PduR_RmGDestTpTxStateRamPtrType;

/**   \brief  type used to point to PduR_RmSrcRom */
typedef P2CONST(PduR_RmSrcRomType, TYPEDEF, PDUR_CONST) PduR_RmSrcRomPtrType;

/**   \brief  type used to point to PduR_RmTp_CancelReceive_TpRxSmStateHandler */
typedef P2CONST(PduR_RmTp_CancelReceive_TpRxSmStateHandlerType, TYPEDEF, PDUR_CONST) PduR_RmTp_CancelReceive_TpRxSmStateHandlerPtrType;

/**   \brief  type used to point to PduR_RmTp_CheckReady2Transmit_TpTxSmStateHandler */
typedef P2CONST(PduR_RmTp_CheckReady2Transmit_TpTxSmStateHandlerType, TYPEDEF, PDUR_CONST) PduR_RmTp_CheckReady2Transmit_TpTxSmStateHandlerPtrType;

/**   \brief  type used to point to PduR_RmTp_CopyRxData_TpRxSmStateHandler */
typedef P2CONST(PduR_RmTp_CopyRxData_TpRxSmStateHandlerType, TYPEDEF, PDUR_CONST) PduR_RmTp_CopyRxData_TpRxSmStateHandlerPtrType;

/**   \brief  type used to point to PduR_RmTp_FinishReception_TpTxSmStateHandler */
typedef P2CONST(PduR_RmTp_FinishReception_TpTxSmStateHandlerType, TYPEDEF, PDUR_CONST) PduR_RmTp_FinishReception_TpTxSmStateHandlerPtrType;

/**   \brief  type used to point to PduR_RmTp_FinishTransmission_TpTxSmStateHandler */
typedef P2CONST(PduR_RmTp_FinishTransmission_TpTxSmStateHandlerType, TYPEDEF, PDUR_CONST) PduR_RmTp_FinishTransmission_TpTxSmStateHandlerPtrType;

/**   \brief  type used to point to PduR_RmTp_StartOfReception_TpRxSmStateHandler */
typedef P2CONST(PduR_RmTp_StartOfReception_TpRxSmStateHandlerType, TYPEDEF, PDUR_CONST) PduR_RmTp_StartOfReception_TpRxSmStateHandlerPtrType;

/**   \brief  type used to point to PduR_RmTp_TpRxIndication_TpRxSmStateHandler */
typedef P2CONST(PduR_RmTp_TpRxIndication_TpRxSmStateHandlerType, TYPEDEF, PDUR_CONST) PduR_RmTp_TpRxIndication_TpRxSmStateHandlerPtrType;

/**   \brief  type used to point to PduR_RmTransmitFctPtr */
typedef P2CONST(PduR_RmTransmitFctPtrType, TYPEDEF, PDUR_CONST) PduR_RmTransmitFctPtrPtrType;

/**   \brief  type used to point to PduR_RmTxInstSmRom */
typedef P2CONST(PduR_RmTxInstSmRomType, TYPEDEF, PDUR_CONST) PduR_RmTxInstSmRomPtrType;

/**   \brief  type used to point to PduR_RxIf2Dest */
typedef P2CONST(PduR_RxIf2DestType, TYPEDEF, PDUR_CONST) PduR_RxIf2DestPtrType;

/**   \brief  type used to point to PduR_RxTp2Dest */
typedef P2CONST(PduR_RxTp2DestType, TYPEDEF, PDUR_CONST) PduR_RxTp2DestPtrType;

/**   \brief  type used to point to PduR_Tx2Lo */
typedef P2CONST(PduR_Tx2LoType, TYPEDEF, PDUR_CONST) PduR_Tx2LoPtrType;

/**   \brief  type used to point to PduR_TxIf2Up */
typedef P2CONST(PduR_TxIf2UpType, TYPEDEF, PDUR_CONST) PduR_TxIf2UpPtrType;

/**   \brief  type used to point to PduR_TxTp2Src */
typedef P2CONST(PduR_TxTp2SrcType, TYPEDEF, PDUR_CONST) PduR_TxTp2SrcPtrType;

/** 
  \}
*/ 

/** 
  \defgroup  PduRPCRootValueTypes  PduR Root Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value representations in root arrays.
  \{
*/ 
/**   \brief  type used in PduR_PCConfig */
typedef struct sPduR_PCConfigType
{
  uint8 PduR_PCConfigNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} PduR_PCConfigType;

typedef PduR_PCConfigType PduR_PBConfigType;  /**< A structure type is present for data in each configuration class. This typedef redefines the probably different name to the specified one. */

/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/


/**********************************************************************************************************************
 * GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  PduR_BmTxBufferIndRom
**********************************************************************************************************************/
/** 
  \var    PduR_BmTxBufferIndRom
  \brief  PduR BufferManager TxBuffer Indirection Table
  \details
  Element             Description
  BmTxBufferRomIdx    the index of the 1:1 relation pointing to PduR_BmTxBufferRom
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_BmTxBufferIndRomType, PDUR_CONST) PduR_BmTxBufferIndRom[77];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_BmTxBufferInstanceRom
**********************************************************************************************************************/
/** 
  \var    PduR_BmTxBufferInstanceRom
  \brief  PduR BufferManager TxBufferInstance Table
  \details
  Element             Description
  BmTxBufferRomIdx    the index of the 1:1 relation pointing to PduR_BmTxBufferRom
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_BmTxBufferInstanceRomType, PDUR_CONST) PduR_BmTxBufferInstanceRom[81];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_BmTxBufferRom
**********************************************************************************************************************/
/** 
  \var    PduR_BmTxBufferRom
  \brief  PduR BufferManager TxBuffer Table
  \details
  Element                          Description
  BmTxBufferArrayRamEndIdx         the end index of the 1:n relation pointing to PduR_BmTxBufferArrayRam
  BmTxBufferArrayRamStartIdx       the start index of the 1:n relation pointing to PduR_BmTxBufferArrayRam
  BmTxBufferInstanceRomEndIdx      the end index of the 1:n relation pointing to PduR_BmTxBufferInstanceRom
  BmTxBufferInstanceRomStartIdx    the start index of the 1:n relation pointing to PduR_BmTxBufferInstanceRom
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_BmTxBufferRomType, PDUR_CONST) PduR_BmTxBufferRom[77];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_ExclusiveAreaRom
**********************************************************************************************************************/
/** 
  \var    PduR_ExclusiveAreaRom
  \brief  PduR Exclusive Area Locks
  \details
  Element    Description
  Lock       Lock function
  Unlock     Unlock function
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_ExclusiveAreaRomType, PDUR_CONST) PduR_ExclusiveAreaRom[1];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_FmFifoInstanceRom
**********************************************************************************************************************/
/** 
  \var    PduR_FmFifoInstanceRom
  \brief  Instance of the PduRDestPdus using a single Fifo
  \details
  Element         Description
  FmFifoRomIdx    the index of the 1:1 relation pointing to PduR_FmFifoRom
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_FmFifoInstanceRomType, PDUR_CONST) PduR_FmFifoInstanceRom[97];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_FmFifoRom
**********************************************************************************************************************/
/** 
  \var    PduR_FmFifoRom
  \brief  PduR FiFoManager Fifo Table
  \details
  Element                     Description
  BmTxBufferIndRomEndIdx      the end index of the 1:n relation pointing to PduR_BmTxBufferIndRom
  BmTxBufferIndRomStartIdx    the start index of the 1:n relation pointing to PduR_BmTxBufferIndRom
  FmFifoElementRamEndIdx      the end index of the 1:n relation pointing to PduR_FmFifoElementRam
  FmFifoElementRamStartIdx    the start index of the 1:n relation pointing to PduR_FmFifoElementRam
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_FmFifoRomType, PDUR_CONST) PduR_FmFifoRom[95];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_Fm_ActivateNext_FmSmStateHandler
**********************************************************************************************************************/
/** 
  \var    PduR_Fm_ActivateNext_FmSmStateHandler
  \details
  Element    Description
  FctPtr 
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_Fm_ActivateNext_FmSmStateHandlerType, PDUR_CONST) PduR_Fm_ActivateNext_FmSmStateHandler[8];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_Fm_ActivateRead_FmSmStateHandler
**********************************************************************************************************************/
/** 
  \var    PduR_Fm_ActivateRead_FmSmStateHandler
  \details
  Element    Description
  FctPtr 
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_Fm_ActivateRead_FmSmStateHandlerType, PDUR_CONST) PduR_Fm_ActivateRead_FmSmStateHandler[8];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_Fm_ActivateWrite_FmSmStateHandler
**********************************************************************************************************************/
/** 
  \var    PduR_Fm_ActivateWrite_FmSmStateHandler
  \details
  Element    Description
  FctPtr 
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_Fm_ActivateWrite_FmSmStateHandlerType, PDUR_CONST) PduR_Fm_ActivateWrite_FmSmStateHandler[8];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_Fm_FinishRead_FmSmStateHandler
**********************************************************************************************************************/
/** 
  \var    PduR_Fm_FinishRead_FmSmStateHandler
  \details
  Element    Description
  FctPtr 
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_Fm_FinishRead_FmSmStateHandlerType, PDUR_CONST) PduR_Fm_FinishRead_FmSmStateHandler[8];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_Fm_FinishWrite_FmSmStateHandler
**********************************************************************************************************************/
/** 
  \var    PduR_Fm_FinishWrite_FmSmStateHandler
  \details
  Element    Description
  FctPtr 
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_Fm_FinishWrite_FmSmStateHandlerType, PDUR_CONST) PduR_Fm_FinishWrite_FmSmStateHandler[8];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_GeneralPropertiesRom
**********************************************************************************************************************/
/** 
  \var    PduR_GeneralPropertiesRom
  \brief  General Properties Switches of the PduR.
  \details
  Element                      Description
  hasTpTxBufferedForwarding
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_GeneralPropertiesRomType, PDUR_CONST) PduR_GeneralPropertiesRom[1];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_MmRom
**********************************************************************************************************************/
/** 
  \var    PduR_MmRom
  \brief  Module manager: Contains all function pointers of the bordering modules.
  \details
  Element                       Description
  RmGDestRomEndIdx              the end index of the 0:n relation pointing to PduR_RmGDestRom
  RmGDestRomStartIdx            the start index of the 0:n relation pointing to PduR_RmGDestRom
  CoreManagerRomIdx             the index of the 1:1 relation pointing to PduR_CoreManagerRom
  MaskedBits                    contains bitcoded the boolean data of PduR_IfCancelTransmitSupportedOfMmRom, PduR_LoIfOfMmRom, PduR_LoTpOfMmRom, PduR_RmGDestRomUsedOfMmRom, PduR_TpCancelTransmitSupportedOfMmRom, PduR_UpIfOfMmRom, PduR_UpTpOfMmRom
  LoIfCancelTransmitFctPtr      Lower layer cancel transmit function pointers.
  LoTpCancelTransmitFctPtr      Lower layer cancel transmit function pointers.
  UpTpCopyRxDataFctPtr          Transport protocol CopyRxData function pointers
  UpTpCopyTxDataFctPtr          Transport protocol CopyTxData function pointers
  UpIfRxIndicationFctPtr        Upper layer communication interface Rx indication function pointers.
  UpIfTxConfirmationFctPtr      Upper layer communication interface Tx confimation function pointers
  UpTpStartOfReceptionFctPtr    Transport protocol StartOfReception function pointers
  UpTpTpRxIndicationFctPtr      Transport protocol TpRxIndication function pointers
  UpTpTpTxConfirmationFctPtr    Transport protocol TpTxConfimation function pointers
  LoIfTransmitFctPtr            Lower layer If transmit function pointers
  LoTpTransmitFctPtr            Lower layer Tp transmit function pointers
  UpIfTriggerTransmitFctPtr     Upper layer trigger transmit function pointers
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_MmRomType, PDUR_CONST) PduR_MmRom[9];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmBufferedTpPropertiesRom
**********************************************************************************************************************/
/** 
  \var    PduR_RmBufferedTpPropertiesRom
  \brief  PduR RoutiongManager Properties of buffered Tp routing paths.
  \details
  Element              Description
  DedicatedTxBuffer    True, if the PduRSrcPdu has a dedicated TxBuffer
  FmFifoRomIdx         the index of the 1:1 relation pointing to PduR_FmFifoRom
  QueuedDestCnt        Number of queued TP destinations.
  TpThreshold          TP threshold value
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_RmBufferedTpPropertiesRomType, PDUR_CONST) PduR_RmBufferedTpPropertiesRom[95];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmDestRom
**********************************************************************************************************************/
/** 
  \var    PduR_RmDestRom
  \brief  PduR RoutiongPathManager destPdu Table
  \details
  Element          Description
  RmGDestRomIdx    the index of the 1:1 relation pointing to PduR_RmGDestRom
  RmSrcRomIdx      the index of the 1:1 relation pointing to PduR_RmSrcRom
  RoutingType      Type of the Routing (API Forwarding, Gateway).
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_RmDestRomType, PDUR_CONST) PduR_RmDestRom[653];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmGDestRom
**********************************************************************************************************************/
/** 
  \var    PduR_RmGDestRom
  \brief  PduR RoutiongPathManager global destPdu Table
  \details
  Element                   Description
  RmDestRomIdx              the index of the 0:1 relation pointing to PduR_RmDestRom
  DestHnd                   handle to be used as parameter for the StartOfReception, CopyRxData, Transmit or RxIndication function call.
  Direction                 Direction of this Pdu: Rx or Tx
  FmFifoInstanceRomIdx      the index of the 0:1 relation pointing to PduR_FmFifoInstanceRom
  MaskedBits                contains bitcoded the boolean data of PduR_FmFifoInstanceRomUsedOfRmGDestRom, PduR_RmDestRomUsedOfRmGDestRom, PduR_RmGDestTpTxStateRamUsedOfRmGDestRom
  MmRomIdx                  the index of the 1:1 relation pointing to PduR_MmRom
  PduRDestPduProcessing     Is Processing Type of destination PDU.
  RmGDestTpTxStateRamIdx    the index of the 0:1 relation pointing to PduR_RmGDestTpTxStateRam
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_RmGDestRomType, PDUR_CONST) PduR_RmGDestRom[653];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmSrcRom
**********************************************************************************************************************/
/** 
  \var    PduR_RmSrcRom
  \brief  PduR RoutiongManager SrcPdu Table
  \details
  Element                         Description
  RmDestRomEndIdx                 the end index of the 1:n relation pointing to PduR_RmDestRom
  RmDestRomStartIdx               the start index of the 1:n relation pointing to PduR_RmDestRom
  LockRomIdx                      the index of the 1:1 relation pointing to PduR_LockRom
  MaskedBits                      contains bitcoded the boolean data of PduR_RmBufferedTpPropertiesRomUsedOfRmSrcRom, PduR_TriggerTransmitSupportedOfRmSrcRom, PduR_TxConfirmationSupportedOfRmSrcRom
  MmRomIdx                        the index of the 1:1 relation pointing to PduR_MmRom
  RmBufferedTpPropertiesRomIdx    the index of the 0:1 relation pointing to PduR_RmBufferedTpPropertiesRom
  SrcHnd                          handle to be used as parameter for the TxConfirmation or TriggerTransmit function call.
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_RmSrcRomType, PDUR_CONST) PduR_RmSrcRom[649];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmTp_CancelReceive_TpRxSmStateHandler
**********************************************************************************************************************/
/** 
  \var    PduR_RmTp_CancelReceive_TpRxSmStateHandler
  \details
  Element    Description
  FctPtr 
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_RmTp_CancelReceive_TpRxSmStateHandlerType, PDUR_CONST) PduR_RmTp_CancelReceive_TpRxSmStateHandler[3];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmTp_CheckReady2Transmit_TpTxSmStateHandler
**********************************************************************************************************************/
/** 
  \var    PduR_RmTp_CheckReady2Transmit_TpTxSmStateHandler
  \details
  Element    Description
  FctPtr 
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_RmTp_CheckReady2Transmit_TpTxSmStateHandlerType, PDUR_CONST) PduR_RmTp_CheckReady2Transmit_TpTxSmStateHandler[5];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmTp_CopyRxData_TpRxSmStateHandler
**********************************************************************************************************************/
/** 
  \var    PduR_RmTp_CopyRxData_TpRxSmStateHandler
  \details
  Element    Description
  FctPtr 
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_RmTp_CopyRxData_TpRxSmStateHandlerType, PDUR_CONST) PduR_RmTp_CopyRxData_TpRxSmStateHandler[3];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmTp_FinishReception_TpTxSmStateHandler
**********************************************************************************************************************/
/** 
  \var    PduR_RmTp_FinishReception_TpTxSmStateHandler
  \details
  Element    Description
  FctPtr 
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_RmTp_FinishReception_TpTxSmStateHandlerType, PDUR_CONST) PduR_RmTp_FinishReception_TpTxSmStateHandler[5];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmTp_FinishTransmission_TpTxSmStateHandler
**********************************************************************************************************************/
/** 
  \var    PduR_RmTp_FinishTransmission_TpTxSmStateHandler
  \details
  Element    Description
  FctPtr 
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_RmTp_FinishTransmission_TpTxSmStateHandlerType, PDUR_CONST) PduR_RmTp_FinishTransmission_TpTxSmStateHandler[5];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmTp_StartOfReception_TpRxSmStateHandler
**********************************************************************************************************************/
/** 
  \var    PduR_RmTp_StartOfReception_TpRxSmStateHandler
  \details
  Element    Description
  FctPtr 
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_RmTp_StartOfReception_TpRxSmStateHandlerType, PDUR_CONST) PduR_RmTp_StartOfReception_TpRxSmStateHandler[3];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmTp_TpRxIndication_TpRxSmStateHandler
**********************************************************************************************************************/
/** 
  \var    PduR_RmTp_TpRxIndication_TpRxSmStateHandler
  \details
  Element    Description
  FctPtr 
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_RmTp_TpRxIndication_TpRxSmStateHandlerType, PDUR_CONST) PduR_RmTp_TpRxIndication_TpRxSmStateHandler[3];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmTransmitFctPtr
**********************************************************************************************************************/
/** 
  \var    PduR_RmTransmitFctPtr
  \brief  Internal routing manager Transmit functions.
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_RmTransmitFctPtrType, PDUR_CONST) PduR_RmTransmitFctPtr[3];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmTxInstSmRom
**********************************************************************************************************************/
/** 
  \var    PduR_RmTxInstSmRom
  \brief  Contains all function pointers for the TxInst StateMachine.
  \details
  Element                             Description
  PduR_RmTp_TxInst_CopyTxData     
  PduR_RmTp_TxInst_CancelTransmit 
  PduR_RmTp_TxInst_TriggerTransmit
  PduR_RmTp_TxInst_TxConfirmation 
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_RmTxInstSmRomType, PDUR_CONST) PduR_RmTxInstSmRom[4];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_Tx2Lo
**********************************************************************************************************************/
/** 
  \var    PduR_Tx2Lo
  \brief  Contains all informations to route a Pdu from a upper layer to a lower layer module, or to cancel a transmission
  \details
  Element                Description
  MaskedBits             contains bitcoded the boolean data of PduR_CancelTransmitUsedOfTx2Lo, PduR_RmSrcRomUsedOfTx2Lo
  RmTransmitFctPtrIdx    the index of the 1:1 relation pointing to PduR_RmTransmitFctPtr
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_Tx2LoType, PDUR_CONST) PduR_Tx2Lo[275];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_TxIf2Up
**********************************************************************************************************************/
/** 
  \var    PduR_TxIf2Up
  \brief  This table contains all routing information to perform the Tx handling of an interface routing. Used in the &lt;LLIf&gt;_TriggerTransmit and &lt;LLIf&gt;_TxConfirmation
  \details
  Element               Description
  RmGDestRomIdx         the index of the 1:1 relation pointing to PduR_RmGDestRom
  TxConfirmationUsed    True, if any of the source PduRDestPdus uses a TxConfirmation.
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_TxIf2UpType, PDUR_CONST) PduR_TxIf2Up[127];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_TxTp2Src
**********************************************************************************************************************/
/** 
  \var    PduR_TxTp2Src
  \brief  This table contains all routing information to perform the Tx handling of a transport protocol routing, Used in the &lt;LoTp&gt;_CopyTxData and &lt;LoTp&gt;_TxConfirmation
  \details
  Element          Description
  RmGDestRomIdx    the index of the 1:1 relation pointing to PduR_RmGDestRom
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(PduR_TxTp2SrcType, PDUR_CONST) PduR_TxTp2Src[203];
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_BmTxBufferArrayRam
**********************************************************************************************************************/
/** 
  \var    PduR_BmTxBufferArrayRam
  \brief  PduR BufferManagere TxBufferArray Table
*/ 
#define PDUR_START_SEC_BUFFER_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(PduR_BmTxBufferArrayRamUType, PDUR_VAR_NOINIT) PduR_BmTxBufferArrayRam;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define PDUR_STOP_SEC_BUFFER_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_BmTxBufferInstanceRam
**********************************************************************************************************************/
/** 
  \var    PduR_BmTxBufferInstanceRam
  \brief  PduR BufferManager TxBufferInstance Table
  \details
  Element                       Description
  BmTxBufferArrayRamReadIdx     the index of the 1:1 relation pointing to PduR_BmTxBufferArrayRam
  BmTxBufferArrayRamWriteIdx    the index of the 1:1 relation pointing to PduR_BmTxBufferArrayRam
  PduRBufferState               PduRBufferState represents the buffer state (READ4WRITE, WRITE4READ, FULL, EMPTY)
  TxBufferUsed                  TRUE if a destination of an 1:N routing use the TxBuffer element.
*/ 
#define PDUR_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(PduR_BmTxBufferInstanceRamUType, PDUR_VAR_NOINIT) PduR_BmTxBufferInstanceRam;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define PDUR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_BmTxBufferRam
**********************************************************************************************************************/
/** 
  \var    PduR_BmTxBufferRam
  \brief  PduR BufferManager TxBuffer Table
  \details
  Element                              Description
  BmTxBufferArrayRamInstanceStopIdx    the index of the 1:1 relation pointing to PduR_BmTxBufferArrayRam
  BmTxBufferArrayRamReadIdx            the index of the 1:1 relation pointing to PduR_BmTxBufferArrayRam
  BmTxBufferArrayRamWriteIdx           the index of the 1:1 relation pointing to PduR_BmTxBufferArrayRam
  RxLength                             Rx Pdu Sdu length
  Allocated                            Is true, if the buffer is allocated by a routing.
  PduRBufferState                      PduRBufferState represents the buffer state (READ4WRITE, WRITE4READ, FULL, EMPTY)
*/ 
#define PDUR_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(PduR_BmTxBufferRamUType, PDUR_VAR_NOINIT) PduR_BmTxBufferRam;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define PDUR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_FmFifoElementRam
**********************************************************************************************************************/
/** 
  \var    PduR_FmFifoElementRam
  \brief  PduR FiFoManager FIFO element table
  \details
  Element              Description
  RmDestRomIdx         the index of the 0:1 relation pointing to PduR_RmDestRom
  BmTxBufferRomIdx     the index of the 0:1 relation pointing to PduR_BmTxBufferRom
  DedicatedTxBuffer    TRUE if a routing has a dedicate Tx Buffer and the Tx Buffer is not shared with an other routing
  State                Fifo Manager state machine state
*/ 
#define PDUR_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(PduR_FmFifoElementRamUType, PDUR_VAR_NOINIT) PduR_FmFifoElementRam;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define PDUR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_FmFifoInstanceRam
**********************************************************************************************************************/
/** 
  \var    PduR_FmFifoInstanceRam
  \brief  Instance of the PduRDestPdus using a single Fifo
  \details
  Element                     Description
  BmTxBufferInstanceRomIdx    the index of the 0:1 relation pointing to PduR_BmTxBufferInstanceRom
*/ 
#define PDUR_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(PduR_FmFifoInstanceRamUType, PDUR_VAR_NOINIT) PduR_FmFifoInstanceRam;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define PDUR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_FmFifoRam
**********************************************************************************************************************/
/** 
  \var    PduR_FmFifoRam
  \brief  PduR FiFoManager Fifo Table
  \details
  Element                     Description
  FillLevel                   Fill level of the FIFO queue
  FmFifoElementRamReadIdx     the index of the 0:1 relation pointing to PduR_FmFifoElementRam
  FmFifoElementRamWriteIdx    the index of the 0:1 relation pointing to PduR_FmFifoElementRam
  TpTxSmState                 Tp Tx state
  PendingConfirmations        Number of pending Tx Confirmations of all possible destinations.
*/ 
#define PDUR_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(PduR_FmFifoRamUType, PDUR_VAR_NOINIT) PduR_FmFifoRam;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define PDUR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_Initialized
**********************************************************************************************************************/
/** 
  \var    PduR_Initialized
  \brief  Initialization state of PduR. TRUE, if PduR_Init() has been called, else FALSE
*/ 
#define PDUR_START_SEC_VAR_ZERO_INIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(PduR_InitializedType, PDUR_VAR_ZERO_INIT) PduR_Initialized;
#define PDUR_STOP_SEC_VAR_ZERO_INIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmBufferedTpPropertiesRam
**********************************************************************************************************************/
/** 
  \var    PduR_RmBufferedTpPropertiesRam
  \brief  PduR RoutiongManager Properties of buffered Tp routing paths.
  \details
  Element                Description
  FmFifoElementRamIdx    the index of the 0:1 relation pointing to PduR_FmFifoElementRam
  TpRxSmState            Tp source instance state
*/ 
#define PDUR_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(PduR_RmBufferedTpPropertiesRamUType, PDUR_VAR_NOINIT) PduR_RmBufferedTpPropertiesRam;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define PDUR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmGDestTpTxStateRam
**********************************************************************************************************************/
/** 
  \var    PduR_RmGDestTpTxStateRam
  \brief  PduR RoutiongPathManager global destPdu Tp Tx State
  \details
  Element            Description
  TpTxInstSmState    Tp dest instance state
*/ 
#define PDUR_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(PduR_RmGDestTpTxStateRamUType, PDUR_VAR_NOINIT) PduR_RmGDestTpTxStateRam;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define PDUR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/


#define PDUR_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "MemMap.h"    /* PRQA S 5087 */        /* MD_MSR_MemMap */

#if(PDUR_USE_INIT_POINTER == STD_ON)
extern P2CONST(PduR_PBConfigType, PDUR_VAR_ZERO_INIT, PDUR_PBCFG) PduR_ConfigDataPtr;
#endif

#define PDUR_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "MemMap.h"    /* PRQA S 5087 */        /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/


/**********************************************************************************************************************
 * LOCAL GEN FUNCTION PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * LOCAL GEN FUNCTIONS
 *********************************************************************************************************************/

#endif  /* PDUR_LCFG_H */
/**********************************************************************************************************************
 * END OF FILE: PduR_Lcfg.h
 *********************************************************************************************************************/

