/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Types_Lcfg.h
 *   Generation Time: 2020-10-30 14:38:57
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

#ifndef OS_TYPES_LCFG_H
# define OS_TYPES_LCFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
# include "Std_Types.h"

/* Os module declarations */

/* Os kernel module dependencies */

/* Os hal dependencies */

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* OS-Application identifiers. */
#define OsApplication_Trusted_Core0 OsApplication_Trusted_Core0
#define OsApplication_Untrusted_Core0 OsApplication_Untrusted_Core0
#define SystemApplication_OsCore0 SystemApplication_OsCore0

/* Trusted function identifiers. */

/* Non-trusted function identifiers. */

/* Fast trusted function identifiers. */

/* Task identifiers. */
#define ASW_10ms_Task ASW_10ms_Task
#define ASW_20ms_Task ASW_20ms_Task
#define ASW_Async_Task ASW_Async_Task
#define ASW_Init_Task ASW_Init_Task
#define BSW_10ms_Task BSW_10ms_Task
#define BSW_5ms_Task BSW_5ms_Task
#define BSW_Async_Task BSW_Async_Task
#define BSW_Diag_Task BSW_Diag_Task
#define BSW_Lin_Task BSW_Lin_Task
#define CpuLoadIdleTask CpuLoadIdleTask
#define IdleTask_OsCore0 IdleTask_OsCore0
#define Init_Task Init_Task

/* Category 2 ISR identifiers. */
#define CanIsr_0_MB00To03 CanIsr_0_MB00To03
#define CanIsr_0_MB04To07 CanIsr_0_MB04To07
#define CanIsr_0_MB08To11 CanIsr_0_MB08To11
#define CanIsr_0_MB12To15 CanIsr_0_MB12To15
#define CanIsr_0_MB16To31 CanIsr_0_MB16To31
#define CanIsr_0_MB32To63 CanIsr_0_MB32To63
#define CanIsr_0_MB64To95 CanIsr_0_MB64To95
#define CanIsr_1_MB00To03 CanIsr_1_MB00To03
#define CanIsr_1_MB04To07 CanIsr_1_MB04To07
#define CanIsr_1_MB08To11 CanIsr_1_MB08To11
#define CanIsr_1_MB12To15 CanIsr_1_MB12To15
#define CanIsr_1_MB16To31 CanIsr_1_MB16To31
#define CanIsr_1_MB32To63 CanIsr_1_MB32To63
#define CanIsr_1_MB64To95 CanIsr_1_MB64To95
#define CanIsr_2_MB00To03 CanIsr_2_MB00To03
#define CanIsr_2_MB04To07 CanIsr_2_MB04To07
#define CanIsr_2_MB08To11 CanIsr_2_MB08To11
#define CanIsr_2_MB12To15 CanIsr_2_MB12To15
#define CanIsr_2_MB16To31 CanIsr_2_MB16To31
#define CanIsr_2_MB32To63 CanIsr_2_MB32To63
#define CanIsr_2_MB64To95 CanIsr_2_MB64To95
#define CanIsr_4_MB00To03 CanIsr_4_MB00To03
#define CanIsr_4_MB04To07 CanIsr_4_MB04To07
#define CanIsr_4_MB08To11 CanIsr_4_MB08To11
#define CanIsr_4_MB12To15 CanIsr_4_MB12To15
#define CanIsr_4_MB16To31 CanIsr_4_MB16To31
#define CanIsr_4_MB32To63 CanIsr_4_MB32To63
#define CanIsr_4_MB64To95 CanIsr_4_MB64To95
#define CanIsr_6_MB00To03 CanIsr_6_MB00To03
#define CanIsr_6_MB04To07 CanIsr_6_MB04To07
#define CanIsr_6_MB08To11 CanIsr_6_MB08To11
#define CanIsr_6_MB12To15 CanIsr_6_MB12To15
#define CanIsr_6_MB16To31 CanIsr_6_MB16To31
#define CanIsr_6_MB32To63 CanIsr_6_MB32To63
#define CanIsr_6_MB64To95 CanIsr_6_MB64To95
#define CanIsr_7_MB00To03 CanIsr_7_MB00To03
#define CanIsr_7_MB04To07 CanIsr_7_MB04To07
#define CanIsr_7_MB08To11 CanIsr_7_MB08To11
#define CanIsr_7_MB12To15 CanIsr_7_MB12To15
#define CanIsr_7_MB16To31 CanIsr_7_MB16To31
#define CanIsr_7_MB32To63 CanIsr_7_MB32To63
#define CanIsr_7_MB64To95 CanIsr_7_MB64To95
#define CounterIsr_SystemTimer CounterIsr_SystemTimer
#define DOWHS1_EMIOS0_CH3_ISR DOWHS1_EMIOS0_CH3_ISR
#define DOWHS2_EMIOS0_CH5_ISR DOWHS2_EMIOS0_CH5_ISR
#define DOWLS2_EMIOS0_CH13_ISR DOWLS2_EMIOS0_CH13_ISR
#define DOWLS2_EMIOS0_CH9_ISR DOWLS2_EMIOS0_CH9_ISR
#define DOWLS3_EMIOS0_CH14_ISR DOWLS3_EMIOS0_CH14_ISR
#define Gpt_PIT_0_TIMER_0_ISR Gpt_PIT_0_TIMER_0_ISR
#define Gpt_PIT_0_TIMER_1_ISR Gpt_PIT_0_TIMER_1_ISR
#define Gpt_PIT_0_TIMER_2_ISR Gpt_PIT_0_TIMER_2_ISR
#define Lin_Channel_0_ERR Lin_Channel_0_ERR
#define Lin_Channel_0_RXI Lin_Channel_0_RXI
#define Lin_Channel_0_TXI Lin_Channel_0_TXI
#define Lin_Channel_10_ERR Lin_Channel_10_ERR
#define Lin_Channel_10_RXI Lin_Channel_10_RXI
#define Lin_Channel_10_TXI Lin_Channel_10_TXI
#define Lin_Channel_1_ERR Lin_Channel_1_ERR
#define Lin_Channel_1_RXI Lin_Channel_1_RXI
#define Lin_Channel_1_TXI Lin_Channel_1_TXI
#define Lin_Channel_4_ERR Lin_Channel_4_ERR
#define Lin_Channel_4_RXI Lin_Channel_4_RXI
#define Lin_Channel_4_TXI Lin_Channel_4_TXI
#define Lin_Channel_6_ERR Lin_Channel_6_ERR
#define Lin_Channel_6_RXI Lin_Channel_6_RXI
#define Lin_Channel_6_TXI Lin_Channel_6_TXI
#define Lin_Channel_7_ERR Lin_Channel_7_ERR
#define Lin_Channel_7_RXI Lin_Channel_7_RXI
#define Lin_Channel_7_TXI Lin_Channel_7_TXI
#define Lin_Channel_8_ERR Lin_Channel_8_ERR
#define Lin_Channel_8_RXI Lin_Channel_8_RXI
#define Lin_Channel_8_TXI Lin_Channel_8_TXI
#define Lin_Channel_9_ERR Lin_Channel_9_ERR
#define Lin_Channel_9_RXI Lin_Channel_9_RXI
#define Lin_Channel_9_TXI Lin_Channel_9_TXI
#define MCU_PLL_LossOfLock MCU_PLL_LossOfLock
#define WKUP_IRQ0 WKUP_IRQ0
#define WKUP_IRQ1 WKUP_IRQ1
#define WKUP_IRQ2 WKUP_IRQ2
#define WKUP_IRQ3 WKUP_IRQ3

/* Alarm identifiers. */
#define Rte_Al_TE2_BSW_10ms_Task_0_10ms Rte_Al_TE2_BSW_10ms_Task_0_10ms
#define Rte_Al_TE2_BSW_5ms_Task_0_5ms Rte_Al_TE2_BSW_5ms_Task_0_5ms
#define Rte_Al_TE2_BSW_Diag_Task_0_10ms Rte_Al_TE2_BSW_Diag_Task_0_10ms
#define Rte_Al_TE2_BSW_Diag_Task_0_5ms Rte_Al_TE2_BSW_Diag_Task_0_5ms
#define Rte_Al_TE_ASW_10ms_Task_0_10ms Rte_Al_TE_ASW_10ms_Task_0_10ms
#define Rte_Al_TE_ASW_20ms_Task_0_100ms Rte_Al_TE_ASW_20ms_Task_0_100ms
#define Rte_Al_TE_ASW_20ms_Task_0_20ms Rte_Al_TE_ASW_20ms_Task_0_20ms
#define Rte_Al_TE_Application_Data_NVM_Application_Data_NVM_1s_runnable Rte_Al_TE_Application_Data_NVM_Application_Data_NVM_1s_runnable
#define Rte_Al_TE_DiagnosticComponent_LocalTimeDistribution_200ms_Runnable Rte_Al_TE_DiagnosticComponent_LocalTimeDistribution_200ms_Runnable

/* Counter identifiers. */
#define SystemTimer SystemTimer

/* ScheduleTable identifiers. */

/* Resource identifiers. */
#define OsResource OsResource
#define OsResource_ASW OsResource_ASW
#define OsResource_BSW OsResource_BSW

/* Spinlock identifiers. */

/* Peripheral identifiers. */

/* Barrier identifiers. */

/* Trace thread identifiers (Tasks and ISRs inclusive system objects). */
#define Os_TraceId_ASW_10ms_Task Os_TraceId_ASW_10ms_Task
#define Os_TraceId_ASW_20ms_Task Os_TraceId_ASW_20ms_Task
#define Os_TraceId_ASW_Async_Task Os_TraceId_ASW_Async_Task
#define Os_TraceId_ASW_Init_Task Os_TraceId_ASW_Init_Task
#define Os_TraceId_BSW_10ms_Task Os_TraceId_BSW_10ms_Task
#define Os_TraceId_BSW_5ms_Task Os_TraceId_BSW_5ms_Task
#define Os_TraceId_BSW_Async_Task Os_TraceId_BSW_Async_Task
#define Os_TraceId_BSW_Diag_Task Os_TraceId_BSW_Diag_Task
#define Os_TraceId_BSW_Lin_Task Os_TraceId_BSW_Lin_Task
#define Os_TraceId_CpuLoadIdleTask Os_TraceId_CpuLoadIdleTask
#define Os_TraceId_IdleTask_OsCore0 Os_TraceId_IdleTask_OsCore0
#define Os_TraceId_Init_Task Os_TraceId_Init_Task
#define Os_TraceId_CanIsr_0_MB00To03 Os_TraceId_CanIsr_0_MB00To03
#define Os_TraceId_CanIsr_0_MB04To07 Os_TraceId_CanIsr_0_MB04To07
#define Os_TraceId_CanIsr_0_MB08To11 Os_TraceId_CanIsr_0_MB08To11
#define Os_TraceId_CanIsr_0_MB12To15 Os_TraceId_CanIsr_0_MB12To15
#define Os_TraceId_CanIsr_0_MB16To31 Os_TraceId_CanIsr_0_MB16To31
#define Os_TraceId_CanIsr_0_MB32To63 Os_TraceId_CanIsr_0_MB32To63
#define Os_TraceId_CanIsr_0_MB64To95 Os_TraceId_CanIsr_0_MB64To95
#define Os_TraceId_CanIsr_1_MB00To03 Os_TraceId_CanIsr_1_MB00To03
#define Os_TraceId_CanIsr_1_MB04To07 Os_TraceId_CanIsr_1_MB04To07
#define Os_TraceId_CanIsr_1_MB08To11 Os_TraceId_CanIsr_1_MB08To11
#define Os_TraceId_CanIsr_1_MB12To15 Os_TraceId_CanIsr_1_MB12To15
#define Os_TraceId_CanIsr_1_MB16To31 Os_TraceId_CanIsr_1_MB16To31
#define Os_TraceId_CanIsr_1_MB32To63 Os_TraceId_CanIsr_1_MB32To63
#define Os_TraceId_CanIsr_1_MB64To95 Os_TraceId_CanIsr_1_MB64To95
#define Os_TraceId_CanIsr_2_MB00To03 Os_TraceId_CanIsr_2_MB00To03
#define Os_TraceId_CanIsr_2_MB04To07 Os_TraceId_CanIsr_2_MB04To07
#define Os_TraceId_CanIsr_2_MB08To11 Os_TraceId_CanIsr_2_MB08To11
#define Os_TraceId_CanIsr_2_MB12To15 Os_TraceId_CanIsr_2_MB12To15
#define Os_TraceId_CanIsr_2_MB16To31 Os_TraceId_CanIsr_2_MB16To31
#define Os_TraceId_CanIsr_2_MB32To63 Os_TraceId_CanIsr_2_MB32To63
#define Os_TraceId_CanIsr_2_MB64To95 Os_TraceId_CanIsr_2_MB64To95
#define Os_TraceId_CanIsr_4_MB00To03 Os_TraceId_CanIsr_4_MB00To03
#define Os_TraceId_CanIsr_4_MB04To07 Os_TraceId_CanIsr_4_MB04To07
#define Os_TraceId_CanIsr_4_MB08To11 Os_TraceId_CanIsr_4_MB08To11
#define Os_TraceId_CanIsr_4_MB12To15 Os_TraceId_CanIsr_4_MB12To15
#define Os_TraceId_CanIsr_4_MB16To31 Os_TraceId_CanIsr_4_MB16To31
#define Os_TraceId_CanIsr_4_MB32To63 Os_TraceId_CanIsr_4_MB32To63
#define Os_TraceId_CanIsr_4_MB64To95 Os_TraceId_CanIsr_4_MB64To95
#define Os_TraceId_CanIsr_6_MB00To03 Os_TraceId_CanIsr_6_MB00To03
#define Os_TraceId_CanIsr_6_MB04To07 Os_TraceId_CanIsr_6_MB04To07
#define Os_TraceId_CanIsr_6_MB08To11 Os_TraceId_CanIsr_6_MB08To11
#define Os_TraceId_CanIsr_6_MB12To15 Os_TraceId_CanIsr_6_MB12To15
#define Os_TraceId_CanIsr_6_MB16To31 Os_TraceId_CanIsr_6_MB16To31
#define Os_TraceId_CanIsr_6_MB32To63 Os_TraceId_CanIsr_6_MB32To63
#define Os_TraceId_CanIsr_6_MB64To95 Os_TraceId_CanIsr_6_MB64To95
#define Os_TraceId_CanIsr_7_MB00To03 Os_TraceId_CanIsr_7_MB00To03
#define Os_TraceId_CanIsr_7_MB04To07 Os_TraceId_CanIsr_7_MB04To07
#define Os_TraceId_CanIsr_7_MB08To11 Os_TraceId_CanIsr_7_MB08To11
#define Os_TraceId_CanIsr_7_MB12To15 Os_TraceId_CanIsr_7_MB12To15
#define Os_TraceId_CanIsr_7_MB16To31 Os_TraceId_CanIsr_7_MB16To31
#define Os_TraceId_CanIsr_7_MB32To63 Os_TraceId_CanIsr_7_MB32To63
#define Os_TraceId_CanIsr_7_MB64To95 Os_TraceId_CanIsr_7_MB64To95
#define Os_TraceId_CounterIsr_SystemTimer Os_TraceId_CounterIsr_SystemTimer
#define Os_TraceId_DOWHS1_EMIOS0_CH3_ISR Os_TraceId_DOWHS1_EMIOS0_CH3_ISR
#define Os_TraceId_DOWHS2_EMIOS0_CH5_ISR Os_TraceId_DOWHS2_EMIOS0_CH5_ISR
#define Os_TraceId_DOWLS2_EMIOS0_CH13_ISR Os_TraceId_DOWLS2_EMIOS0_CH13_ISR
#define Os_TraceId_DOWLS2_EMIOS0_CH9_ISR Os_TraceId_DOWLS2_EMIOS0_CH9_ISR
#define Os_TraceId_DOWLS3_EMIOS0_CH14_ISR Os_TraceId_DOWLS3_EMIOS0_CH14_ISR
#define Os_TraceId_Gpt_PIT_0_TIMER_0_ISR Os_TraceId_Gpt_PIT_0_TIMER_0_ISR
#define Os_TraceId_Gpt_PIT_0_TIMER_1_ISR Os_TraceId_Gpt_PIT_0_TIMER_1_ISR
#define Os_TraceId_Gpt_PIT_0_TIMER_2_ISR Os_TraceId_Gpt_PIT_0_TIMER_2_ISR
#define Os_TraceId_Lin_Channel_0_ERR Os_TraceId_Lin_Channel_0_ERR
#define Os_TraceId_Lin_Channel_0_RXI Os_TraceId_Lin_Channel_0_RXI
#define Os_TraceId_Lin_Channel_0_TXI Os_TraceId_Lin_Channel_0_TXI
#define Os_TraceId_Lin_Channel_10_ERR Os_TraceId_Lin_Channel_10_ERR
#define Os_TraceId_Lin_Channel_10_RXI Os_TraceId_Lin_Channel_10_RXI
#define Os_TraceId_Lin_Channel_10_TXI Os_TraceId_Lin_Channel_10_TXI
#define Os_TraceId_Lin_Channel_1_ERR Os_TraceId_Lin_Channel_1_ERR
#define Os_TraceId_Lin_Channel_1_RXI Os_TraceId_Lin_Channel_1_RXI
#define Os_TraceId_Lin_Channel_1_TXI Os_TraceId_Lin_Channel_1_TXI
#define Os_TraceId_Lin_Channel_4_ERR Os_TraceId_Lin_Channel_4_ERR
#define Os_TraceId_Lin_Channel_4_RXI Os_TraceId_Lin_Channel_4_RXI
#define Os_TraceId_Lin_Channel_4_TXI Os_TraceId_Lin_Channel_4_TXI
#define Os_TraceId_Lin_Channel_6_ERR Os_TraceId_Lin_Channel_6_ERR
#define Os_TraceId_Lin_Channel_6_RXI Os_TraceId_Lin_Channel_6_RXI
#define Os_TraceId_Lin_Channel_6_TXI Os_TraceId_Lin_Channel_6_TXI
#define Os_TraceId_Lin_Channel_7_ERR Os_TraceId_Lin_Channel_7_ERR
#define Os_TraceId_Lin_Channel_7_RXI Os_TraceId_Lin_Channel_7_RXI
#define Os_TraceId_Lin_Channel_7_TXI Os_TraceId_Lin_Channel_7_TXI
#define Os_TraceId_Lin_Channel_8_ERR Os_TraceId_Lin_Channel_8_ERR
#define Os_TraceId_Lin_Channel_8_RXI Os_TraceId_Lin_Channel_8_RXI
#define Os_TraceId_Lin_Channel_8_TXI Os_TraceId_Lin_Channel_8_TXI
#define Os_TraceId_Lin_Channel_9_ERR Os_TraceId_Lin_Channel_9_ERR
#define Os_TraceId_Lin_Channel_9_RXI Os_TraceId_Lin_Channel_9_RXI
#define Os_TraceId_Lin_Channel_9_TXI Os_TraceId_Lin_Channel_9_TXI
#define Os_TraceId_MCU_PLL_LossOfLock Os_TraceId_MCU_PLL_LossOfLock
#define Os_TraceId_WKUP_IRQ0 Os_TraceId_WKUP_IRQ0
#define Os_TraceId_WKUP_IRQ1 Os_TraceId_WKUP_IRQ1
#define Os_TraceId_WKUP_IRQ2 Os_TraceId_WKUP_IRQ2
#define Os_TraceId_WKUP_IRQ3 Os_TraceId_WKUP_IRQ3

/* Trace spinlock identifiers (All spinlocks inclusive system objects). */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/*! OS-Application identifiers. */
typedef enum
{
  OsApplication_Trusted_Core0 = 0, /* 0x00000001 */  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OsApplication_Untrusted_Core0 = 1, /* 0x00000002 */  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  SystemApplication_OsCore0 = 2, /* 0x00000004 */  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OS_APPID_COUNT = 3,
  INVALID_OSAPPLICATION = OS_APPID_COUNT
} ApplicationType;

/*! Trusted function identifiers. */
typedef enum
{
  OS_TRUSTEDFUNCTIONID_COUNT = 0
} TrustedFunctionIndexType;

/*! Non-trusted function identifiers. */
typedef enum
{
  OS_NONTRUSTEDFUNCTIONID_COUNT = 0
} Os_NonTrustedFunctionIndexType;

/*! Fast trusted function identifiers. */
typedef enum
{
  OS_FASTTRUSTEDFUNCTIONID_COUNT = 0
} Os_FastTrustedFunctionIndexType;

/*! Task identifiers. */
typedef enum
{
  ASW_10ms_Task = 0,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  ASW_20ms_Task = 1,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  ASW_Async_Task = 2,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  ASW_Init_Task = 3,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  BSW_10ms_Task = 4,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  BSW_5ms_Task = 5,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  BSW_Async_Task = 6,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  BSW_Diag_Task = 7,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  BSW_Lin_Task = 8,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CpuLoadIdleTask = 9,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  IdleTask_OsCore0 = 10,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Init_Task = 11,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OS_TASKID_COUNT = 12,
  INVALID_TASK = OS_TASKID_COUNT
} TaskType;

/*! Category 2 ISR identifiers. */
typedef enum
{
  CanIsr_0_MB00To03 = 0,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_0_MB04To07 = 1,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_0_MB08To11 = 2,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_0_MB12To15 = 3,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_0_MB16To31 = 4,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_0_MB32To63 = 5,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_0_MB64To95 = 6,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_1_MB00To03 = 7,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_1_MB04To07 = 8,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_1_MB08To11 = 9,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_1_MB12To15 = 10,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_1_MB16To31 = 11,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_1_MB32To63 = 12,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_1_MB64To95 = 13,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_2_MB00To03 = 14,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_2_MB04To07 = 15,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_2_MB08To11 = 16,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_2_MB12To15 = 17,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_2_MB16To31 = 18,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_2_MB32To63 = 19,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_2_MB64To95 = 20,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_4_MB00To03 = 21,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_4_MB04To07 = 22,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_4_MB08To11 = 23,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_4_MB12To15 = 24,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_4_MB16To31 = 25,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_4_MB32To63 = 26,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_4_MB64To95 = 27,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_6_MB00To03 = 28,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_6_MB04To07 = 29,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_6_MB08To11 = 30,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_6_MB12To15 = 31,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_6_MB16To31 = 32,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_6_MB32To63 = 33,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_6_MB64To95 = 34,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_7_MB00To03 = 35,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_7_MB04To07 = 36,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_7_MB08To11 = 37,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_7_MB12To15 = 38,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_7_MB16To31 = 39,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_7_MB32To63 = 40,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_7_MB64To95 = 41,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CounterIsr_SystemTimer = 42,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  DOWHS1_EMIOS0_CH3_ISR = 43,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  DOWHS2_EMIOS0_CH5_ISR = 44,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  DOWLS2_EMIOS0_CH13_ISR = 45,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  DOWLS2_EMIOS0_CH9_ISR = 46,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  DOWLS3_EMIOS0_CH14_ISR = 47,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Gpt_PIT_0_TIMER_0_ISR = 48,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Gpt_PIT_0_TIMER_1_ISR = 49,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Gpt_PIT_0_TIMER_2_ISR = 50,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_0_ERR = 51,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_0_RXI = 52,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_0_TXI = 53,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_10_ERR = 54,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_10_RXI = 55,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_10_TXI = 56,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_1_ERR = 57,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_1_RXI = 58,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_1_TXI = 59,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_4_ERR = 60,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_4_RXI = 61,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_4_TXI = 62,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_6_ERR = 63,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_6_RXI = 64,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_6_TXI = 65,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_7_ERR = 66,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_7_RXI = 67,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_7_TXI = 68,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_8_ERR = 69,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_8_RXI = 70,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_8_TXI = 71,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_9_ERR = 72,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_9_RXI = 73,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Lin_Channel_9_TXI = 74,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  MCU_PLL_LossOfLock = 75,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  WKUP_IRQ0 = 76,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  WKUP_IRQ1 = 77,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  WKUP_IRQ2 = 78,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  WKUP_IRQ3 = 79,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OS_ISRID_COUNT = 80,
  INVALID_ISR = OS_ISRID_COUNT
} ISRType;

/*! Alarm identifiers. */
typedef enum
{
  Rte_Al_TE2_BSW_10ms_Task_0_10ms = 0,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE2_BSW_5ms_Task_0_5ms = 1,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE2_BSW_Diag_Task_0_10ms = 2,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE2_BSW_Diag_Task_0_5ms = 3,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_ASW_10ms_Task_0_10ms = 4,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_ASW_20ms_Task_0_100ms = 5,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_ASW_20ms_Task_0_20ms = 6,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_Application_Data_NVM_Application_Data_NVM_1s_runnable = 7,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_DiagnosticComponent_LocalTimeDistribution_200ms_Runnable = 8,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OS_ALARMID_COUNT = 9
} AlarmType;

/*! Counter identifiers. */
typedef enum
{
  SystemTimer = 0,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OS_COUNTERID_COUNT = 1
} CounterType;

/*! ScheduleTable identifiers. */
typedef enum
{
  OS_SCHTID_COUNT = 0
} ScheduleTableType;

/*! Resource identifiers. */
typedef enum
{
  OsResource = 0,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OsResource_ASW = 1,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OsResource_BSW = 2,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OS_RESOURCEID_COUNT = 3
} ResourceType;

/*! Spinlock identifiers. */
typedef enum
{
  OS_SPINLOCKID_COUNT = 0,
  INVALID_SPINLOCK = OS_SPINLOCKID_COUNT
} SpinlockIdType;

/*! Peripheral identifiers. */
typedef enum
{
  OS_PERIPHERALID_COUNT = 0
} Os_PeripheralIdType;

/*! Barrier identifiers. */
typedef enum
{
  OS_BARRIERID_COUNT = 0
} Os_BarrierIdType;

/*! Trace thread identifiers (Tasks and ISRs inclusive system objects). */
typedef enum
{
  Os_TraceId_ASW_10ms_Task = 0,
  Os_TraceId_ASW_20ms_Task = 1,
  Os_TraceId_ASW_Async_Task = 2,
  Os_TraceId_ASW_Init_Task = 3,
  Os_TraceId_BSW_10ms_Task = 4,
  Os_TraceId_BSW_5ms_Task = 5,
  Os_TraceId_BSW_Async_Task = 6,
  Os_TraceId_BSW_Diag_Task = 7,
  Os_TraceId_BSW_Lin_Task = 8,
  Os_TraceId_CpuLoadIdleTask = 9,
  Os_TraceId_IdleTask_OsCore0 = 10,
  Os_TraceId_Init_Task = 11,
  Os_TraceId_CanIsr_0_MB00To03 = 12,
  Os_TraceId_CanIsr_0_MB04To07 = 13,
  Os_TraceId_CanIsr_0_MB08To11 = 14,
  Os_TraceId_CanIsr_0_MB12To15 = 15,
  Os_TraceId_CanIsr_0_MB16To31 = 16,
  Os_TraceId_CanIsr_0_MB32To63 = 17,
  Os_TraceId_CanIsr_0_MB64To95 = 18,
  Os_TraceId_CanIsr_1_MB00To03 = 19,
  Os_TraceId_CanIsr_1_MB04To07 = 20,
  Os_TraceId_CanIsr_1_MB08To11 = 21,
  Os_TraceId_CanIsr_1_MB12To15 = 22,
  Os_TraceId_CanIsr_1_MB16To31 = 23,
  Os_TraceId_CanIsr_1_MB32To63 = 24,
  Os_TraceId_CanIsr_1_MB64To95 = 25,
  Os_TraceId_CanIsr_2_MB00To03 = 26,
  Os_TraceId_CanIsr_2_MB04To07 = 27,
  Os_TraceId_CanIsr_2_MB08To11 = 28,
  Os_TraceId_CanIsr_2_MB12To15 = 29,
  Os_TraceId_CanIsr_2_MB16To31 = 30,
  Os_TraceId_CanIsr_2_MB32To63 = 31,
  Os_TraceId_CanIsr_2_MB64To95 = 32,
  Os_TraceId_CanIsr_4_MB00To03 = 33,
  Os_TraceId_CanIsr_4_MB04To07 = 34,
  Os_TraceId_CanIsr_4_MB08To11 = 35,
  Os_TraceId_CanIsr_4_MB12To15 = 36,
  Os_TraceId_CanIsr_4_MB16To31 = 37,
  Os_TraceId_CanIsr_4_MB32To63 = 38,
  Os_TraceId_CanIsr_4_MB64To95 = 39,
  Os_TraceId_CanIsr_6_MB00To03 = 40,
  Os_TraceId_CanIsr_6_MB04To07 = 41,
  Os_TraceId_CanIsr_6_MB08To11 = 42,
  Os_TraceId_CanIsr_6_MB12To15 = 43,
  Os_TraceId_CanIsr_6_MB16To31 = 44,
  Os_TraceId_CanIsr_6_MB32To63 = 45,
  Os_TraceId_CanIsr_6_MB64To95 = 46,
  Os_TraceId_CanIsr_7_MB00To03 = 47,
  Os_TraceId_CanIsr_7_MB04To07 = 48,
  Os_TraceId_CanIsr_7_MB08To11 = 49,
  Os_TraceId_CanIsr_7_MB12To15 = 50,
  Os_TraceId_CanIsr_7_MB16To31 = 51,
  Os_TraceId_CanIsr_7_MB32To63 = 52,
  Os_TraceId_CanIsr_7_MB64To95 = 53,
  Os_TraceId_CounterIsr_SystemTimer = 54,
  Os_TraceId_DOWHS1_EMIOS0_CH3_ISR = 55,
  Os_TraceId_DOWHS2_EMIOS0_CH5_ISR = 56,
  Os_TraceId_DOWLS2_EMIOS0_CH13_ISR = 57,
  Os_TraceId_DOWLS2_EMIOS0_CH9_ISR = 58,
  Os_TraceId_DOWLS3_EMIOS0_CH14_ISR = 59,
  Os_TraceId_Gpt_PIT_0_TIMER_0_ISR = 60,
  Os_TraceId_Gpt_PIT_0_TIMER_1_ISR = 61,
  Os_TraceId_Gpt_PIT_0_TIMER_2_ISR = 62,
  Os_TraceId_Lin_Channel_0_ERR = 63,
  Os_TraceId_Lin_Channel_0_RXI = 64,
  Os_TraceId_Lin_Channel_0_TXI = 65,
  Os_TraceId_Lin_Channel_10_ERR = 66,
  Os_TraceId_Lin_Channel_10_RXI = 67,
  Os_TraceId_Lin_Channel_10_TXI = 68,
  Os_TraceId_Lin_Channel_1_ERR = 69,
  Os_TraceId_Lin_Channel_1_RXI = 70,
  Os_TraceId_Lin_Channel_1_TXI = 71,
  Os_TraceId_Lin_Channel_4_ERR = 72,
  Os_TraceId_Lin_Channel_4_RXI = 73,
  Os_TraceId_Lin_Channel_4_TXI = 74,
  Os_TraceId_Lin_Channel_6_ERR = 75,
  Os_TraceId_Lin_Channel_6_RXI = 76,
  Os_TraceId_Lin_Channel_6_TXI = 77,
  Os_TraceId_Lin_Channel_7_ERR = 78,
  Os_TraceId_Lin_Channel_7_RXI = 79,
  Os_TraceId_Lin_Channel_7_TXI = 80,
  Os_TraceId_Lin_Channel_8_ERR = 81,
  Os_TraceId_Lin_Channel_8_RXI = 82,
  Os_TraceId_Lin_Channel_8_TXI = 83,
  Os_TraceId_Lin_Channel_9_ERR = 84,
  Os_TraceId_Lin_Channel_9_RXI = 85,
  Os_TraceId_Lin_Channel_9_TXI = 86,
  Os_TraceId_MCU_PLL_LossOfLock = 87,
  Os_TraceId_WKUP_IRQ0 = 88,
  Os_TraceId_WKUP_IRQ1 = 89,
  Os_TraceId_WKUP_IRQ2 = 90,
  Os_TraceId_WKUP_IRQ3 = 91,
  OS_TRACE_THREADID_COUNT = 92,
  OS_TRACE_INVALID_THREAD = OS_TRACE_THREADID_COUNT + 1
} Os_TraceThreadIdType;

/*! Trace spinlock identifiers (All spinlocks inclusive system objects). */
typedef enum
{
  OS_TRACE_NUMBER_OF_CONFIGURED_SPINLOCKS = OS_SPINLOCKID_COUNT,
  OS_TRACE_NUMBER_OF_ALL_SPINLOCKS = OS_SPINLOCKID_COUNT + 0u,  /* PRQA S 4521 */ /* MD_Os_Rule10.1_4521 */
  OS_TRACE_INVALID_SPINLOCK = OS_TRACE_NUMBER_OF_ALL_SPINLOCKS + 1
} Os_TraceSpinlockIdType;

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/


#endif /* OS_TYPES_LCFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_Types_Lcfg.h
 *********************************************************************************************************************/
