/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte.oil
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  OIL-File containing project specific OS definitions for the MICROSAR RTE
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *                                              P L E A S E   N O T E
 *
 * The attributes in this file must not be changed. Missing mandatory attributes must be set in the including file.
 * They are presented as a comment in the corresponding object definition containing a list of all legal values.
 *********************************************************************************************************************/

   TASK ASW_10ms_Task {
      ACTIVATION = 1:"@RO@";
      PRIORITY = 10:"@RO@";
      SCHEDULE = FULL:"@RO@";
// TIMING_PROTECTION = FALSE|TRUE; (AUTOSAR OS only)
      AUTOSTART = FALSE:"@RO@";
      EVENT = Rte_Ev_Cyclic_ASW_10ms_Task_0_10ms:"@RO@";
      EVENT = Rte_Ev_Run_SCIM_Manager_SCIM_Manager_EnterRun:"@RO@";
   }:"@RO@@NR@";

   ALARM Rte_Al_TE_ASW_10ms_Task_0_10ms {
      COUNTER = SystemTimer;
      ACTION = SETEVENT
      {
         TASK = ASW_10ms_Task;
         EVENT = Rte_Ev_Cyclic_ASW_10ms_Task_0_10ms;
      }:"@ROC@";
      AUTOSTART = FALSE:"@RO@";
   }:"@RO@@NR@";

   TASK ASW_20ms_Task {
      ACTIVATION = 1:"@RO@";
      PRIORITY = 30:"@RO@";
      SCHEDULE = FULL:"@RO@";
// TIMING_PROTECTION = FALSE|TRUE; (AUTOSAR OS only)
      AUTOSTART = FALSE:"@RO@";
      EVENT = Rte_Ev_Cyclic_ASW_20ms_Task_0_100ms:"@RO@";
      EVENT = Rte_Ev_Cyclic_ASW_20ms_Task_0_20ms:"@RO@";
      EVENT = Rte_Ev_Run_DiagnosticComponent_LocalTimeDistribution_200ms_Runnable:"@RO@";
   }:"@RO@@NR@";

   ALARM Rte_Al_TE_ASW_20ms_Task_0_100ms {
      COUNTER = SystemTimer;
      ACTION = SETEVENT
      {
         TASK = ASW_20ms_Task;
         EVENT = Rte_Ev_Cyclic_ASW_20ms_Task_0_100ms;
      }:"@ROC@";
      AUTOSTART = FALSE:"@RO@";
   }:"@RO@@NR@";

   ALARM Rte_Al_TE_ASW_20ms_Task_0_20ms {
      COUNTER = SystemTimer;
      ACTION = SETEVENT
      {
         TASK = ASW_20ms_Task;
         EVENT = Rte_Ev_Cyclic_ASW_20ms_Task_0_20ms;
      }:"@ROC@";
      AUTOSTART = FALSE:"@RO@";
   }:"@RO@@NR@";

   ALARM Rte_Al_TE_DiagnosticComponent_LocalTimeDistribution_200ms_Runnable {
      COUNTER = SystemTimer;
      ACTION = SETEVENT
      {
         TASK = ASW_20ms_Task;
         EVENT = Rte_Ev_Run_DiagnosticComponent_LocalTimeDistribution_200ms_Runnable;
      }:"@ROC@";
      AUTOSTART = FALSE:"@RO@";
   }:"@RO@@NR@";

   TASK ASW_Async_Task {
      ACTIVATION = 1:"@RO@";
      PRIORITY = 59:"@RO@";
      SCHEDULE = FULL:"@RO@";
// TIMING_PROTECTION = FALSE|TRUE; (AUTOSAR OS only)
      AUTOSTART = FALSE:"@RO@";
      EVENT = Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif:"@RO@";
      EVENT = Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif:"@RO@";
      EVENT = Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif:"@RO@";
      EVENT = Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif:"@RO@";
      EVENT = Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif:"@RO@";
      EVENT = Rte_Ev_Run1_DiagnosticComponent_DiagnosticComponent_Dcm_ActivateIss:"@RO@";
      EVENT = Rte_Ev_Run1_DiagnosticComponent_DiagnosticComponent_Dcm_DeactivateIss:"@RO@";
      EVENT = Rte_Ev_Run1_LINMgr_SCIM_LINMgr_LIN6SchEndNotif:"@RO@";
      EVENT = Rte_Ev_Run1_LINMgr_SCIM_LINMgr_LIN7SchEndNotif:"@RO@";
      EVENT = Rte_Ev_Run1_LINMgr_SCIM_LINMgr_LIN8SchEndNotif:"@RO@";
      EVENT = Rte_Ev_Run_Application_Data_NVM_Application_Data_NVM_1s_runnable:"@RO@";
      EVENT = Rte_Ev_Run_Calibration_Data_NVM_FspNVDataRxEventRunnable:"@RO@";
      EVENT = Rte_Ev_Run_CryptoDriverDoorLatch_Tx_VEC_CryptoProxySenderConfirmation:"@RO@";
      EVENT = Rte_Ev_Run_CryptoDriverDoorLatch_Tx_VEC_CryptoProxySenderReception:"@RO@";
      EVENT = Rte_Ev_Run_CryptoDriverKeyCyl_Rx_VEC_CryptoProxyReceiverReception:"@RO@";
      EVENT = Rte_Ev_Run_CryptoEngineStart_Tx_VEC_CryptoProxySenderConfirmation:"@RO@";
      EVENT = Rte_Ev_Run_CryptoEngineStart_Tx_VEC_CryptoProxySenderReception:"@RO@";
      EVENT = Rte_Ev_Run_CryptoGearboxLock_Tx_VEC_CryptoProxySenderConfirmation:"@RO@";
      EVENT = Rte_Ev_Run_CryptoGearboxLock_Tx_VEC_CryptoProxySenderReception:"@RO@";
      EVENT = Rte_Ev_Run_CryptoKeyAuth_Tx_VEC_CryptoProxySenderConfirmation:"@RO@";
      EVENT = Rte_Ev_Run_CryptoKeyAuth_Tx_VEC_CryptoProxySenderReception:"@RO@";
      EVENT = Rte_Ev_Run_CryptoLockingSwitch_Rx_VEC_CryptoProxyReceiverReception:"@RO@";
      EVENT = Rte_Ev_Run_CryptoLuggageCompartment_Tx_VEC_CryptoProxySenderConfirmation:"@RO@";
      EVENT = Rte_Ev_Run_CryptoLuggageCompartment_Tx_VEC_CryptoProxySenderReception:"@RO@";
      EVENT = Rte_Ev_Run_CryptoPsngDoorLatch_Tx_VEC_CryptoProxySenderConfirmation:"@RO@";
      EVENT = Rte_Ev_Run_CryptoPsngDoorLatch_Tx_VEC_CryptoProxySenderReception:"@RO@";
      EVENT = Rte_Ev_Run_CryptoPsngKeyCyl_Rx_VEC_CryptoProxyReceiverReception:"@RO@";
      EVENT = Rte_Ev_Run_CryptoReducedSetMode_Tx_VEC_CryptoProxySenderConfirmation:"@RO@";
      EVENT = Rte_Ev_Run_CryptoReducedSetMode_Tx_VEC_CryptoProxySenderReception:"@RO@";
      EVENT = Rte_Ev_Run_CryptoTheftAlarmActivation_Tx_VEC_CryptoProxySenderConfirmation:"@RO@";
      EVENT = Rte_Ev_Run_CryptoTheftAlarmActivation_Tx_VEC_CryptoProxySenderReception:"@RO@";
      EVENT = Rte_Ev_Run_Keyfob_Radio_Com_NVM_KeyFobNVDataRxEventRunnable:"@RO@";
   }:"@RO@@NR@";

   ALARM Rte_Al_TE_Application_Data_NVM_Application_Data_NVM_1s_runnable {
      COUNTER = SystemTimer;
      ACTION = SETEVENT
      {
         TASK = ASW_Async_Task;
         EVENT = Rte_Ev_Run_Application_Data_NVM_Application_Data_NVM_1s_runnable;
      }:"@ROC@";
      AUTOSTART = FALSE:"@RO@";
   }:"@RO@@NR@";

   TASK ASW_Init_Task {
      ACTIVATION = 1:"@RO@";
      PRIORITY = 200:"@RO@";
      SCHEDULE = NON:"@RO@";
// TIMING_PROTECTION = FALSE|TRUE; (AUTOSAR OS only)
      AUTOSTART = FALSE:"@RO@";
   }:"@RO@@NR@";

   TASK BSW_10ms_Task {
      ACTIVATION = 1:"@RO@";
      PRIORITY = 130:"@RO@";
      SCHEDULE = FULL:"@RO@";
// TIMING_PROTECTION = FALSE|TRUE; (AUTOSAR OS only)
      AUTOSTART = FALSE:"@RO@";
   }:"@RO@@NR@";

   ALARM Rte_Al_TE2_BSW_10ms_Task_0_10ms {
      COUNTER = SystemTimer;
      ACTION = ACTIVATETASK
      {
         TASK = BSW_10ms_Task;
      }:"@ROC@";
      AUTOSTART = FALSE:"@RO@";
   }:"@RO@@NR@";

   TASK BSW_5ms_Task {
      ACTIVATION = 1:"@RO@";
      PRIORITY = 140:"@RO@";
      SCHEDULE = FULL:"@RO@";
// TIMING_PROTECTION = FALSE|TRUE; (AUTOSAR OS only)
      AUTOSTART = FALSE:"@RO@";
   }:"@RO@@NR@";

   ALARM Rte_Al_TE2_BSW_5ms_Task_0_5ms {
      COUNTER = SystemTimer;
      ACTION = ACTIVATETASK
      {
         TASK = BSW_5ms_Task;
      }:"@ROC@";
      AUTOSTART = FALSE:"@RO@";
   }:"@RO@@NR@";

   TASK BSW_Async_Task {
      ACTIVATION = 1:"@RO@";
      PRIORITY = 120:"@RO@";
      SCHEDULE = FULL:"@RO@";
// TIMING_PROTECTION = FALSE|TRUE; (AUTOSAR OS only)
      AUTOSTART = FALSE:"@RO@";
   }:"@RO@@NR@";

   TASK BSW_Diag_Task {
      ACTIVATION = 1:"@RO@";
      PRIORITY = 101:"@RO@";
      SCHEDULE = FULL:"@RO@";
// TIMING_PROTECTION = FALSE|TRUE; (AUTOSAR OS only)
      AUTOSTART = FALSE:"@RO@";
      EVENT = Rte_Ev_Cyclic2_BSW_Diag_Task_0_10ms:"@RO@";
      EVENT = Rte_Ev_Cyclic2_BSW_Diag_Task_0_5ms:"@RO@";
   }:"@RO@@NR@";

   ALARM Rte_Al_TE2_BSW_Diag_Task_0_10ms {
      COUNTER = SystemTimer;
      ACTION = SETEVENT
      {
         TASK = BSW_Diag_Task;
         EVENT = Rte_Ev_Cyclic2_BSW_Diag_Task_0_10ms;
      }:"@ROC@";
      AUTOSTART = FALSE:"@RO@";
   }:"@RO@@NR@";

   ALARM Rte_Al_TE2_BSW_Diag_Task_0_5ms {
      COUNTER = SystemTimer;
      ACTION = SETEVENT
      {
         TASK = BSW_Diag_Task;
         EVENT = Rte_Ev_Cyclic2_BSW_Diag_Task_0_5ms;
      }:"@ROC@";
      AUTOSTART = FALSE:"@RO@";
   }:"@RO@@NR@";

   TASK BSW_Lin_Task {
      ACTIVATION = 1:"@RO@";
      PRIORITY = 110:"@RO@";
      SCHEDULE = FULL:"@RO@";
// TIMING_PROTECTION = FALSE|TRUE; (AUTOSAR OS only)
      AUTOSTART = FALSE:"@RO@";
      EVENT = Rte_Ev_Run_BswM_BswM_Read_LIN1_ScheduleTableRequestMode:"@RO@";
      EVENT = Rte_Ev_Run_BswM_BswM_Read_LIN2_ScheduleTableRequestMode:"@RO@";
      EVENT = Rte_Ev_Run_BswM_BswM_Read_LIN3_ScheduleTableRequestMode:"@RO@";
      EVENT = Rte_Ev_Run_BswM_BswM_Read_LIN4_ScheduleTableRequestMode:"@RO@";
      EVENT = Rte_Ev_Run_BswM_BswM_Read_LIN5_ScheduleTableRequestMode:"@RO@";
      EVENT = Rte_Ev_Run_BswM_BswM_Read_LIN6_ScheduleTableRequestMode:"@RO@";
      EVENT = Rte_Ev_Run_BswM_BswM_Read_LIN7_ScheduleTableRequestMode:"@RO@";
      EVENT = Rte_Ev_Run_BswM_BswM_Read_LIN8_ScheduleTableRequestMode:"@RO@";
   }:"@RO@@NR@";

   EVENT Rte_Ev_Cyclic2_BSW_Diag_Task_0_10ms {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Cyclic2_BSW_Diag_Task_0_5ms {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Cyclic_ASW_10ms_Task_0_10ms {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Cyclic_ASW_20ms_Task_0_100ms {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Cyclic_ASW_20ms_Task_0_20ms {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run1_DiagnosticComponent_DiagnosticComponent_Dcm_ActivateIss {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run1_DiagnosticComponent_DiagnosticComponent_Dcm_DeactivateIss {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run1_LINMgr_SCIM_LINMgr_LIN6SchEndNotif {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run1_LINMgr_SCIM_LINMgr_LIN7SchEndNotif {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run1_LINMgr_SCIM_LINMgr_LIN8SchEndNotif {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_Application_Data_NVM_Application_Data_NVM_1s_runnable {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_BswM_BswM_Read_LIN1_ScheduleTableRequestMode {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_BswM_BswM_Read_LIN2_ScheduleTableRequestMode {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_BswM_BswM_Read_LIN3_ScheduleTableRequestMode {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_BswM_BswM_Read_LIN4_ScheduleTableRequestMode {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_BswM_BswM_Read_LIN5_ScheduleTableRequestMode {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_BswM_BswM_Read_LIN6_ScheduleTableRequestMode {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_BswM_BswM_Read_LIN7_ScheduleTableRequestMode {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_BswM_BswM_Read_LIN8_ScheduleTableRequestMode {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_Calibration_Data_NVM_FspNVDataRxEventRunnable {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoDriverDoorLatch_Tx_VEC_CryptoProxySenderConfirmation {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoDriverDoorLatch_Tx_VEC_CryptoProxySenderReception {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoDriverKeyCyl_Rx_VEC_CryptoProxyReceiverReception {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoEngineStart_Tx_VEC_CryptoProxySenderConfirmation {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoEngineStart_Tx_VEC_CryptoProxySenderReception {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoGearboxLock_Tx_VEC_CryptoProxySenderConfirmation {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoGearboxLock_Tx_VEC_CryptoProxySenderReception {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoKeyAuth_Tx_VEC_CryptoProxySenderConfirmation {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoKeyAuth_Tx_VEC_CryptoProxySenderReception {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoLockingSwitch_Rx_VEC_CryptoProxyReceiverReception {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoLuggageCompartment_Tx_VEC_CryptoProxySenderConfirmation {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoLuggageCompartment_Tx_VEC_CryptoProxySenderReception {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoPsngDoorLatch_Tx_VEC_CryptoProxySenderConfirmation {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoPsngDoorLatch_Tx_VEC_CryptoProxySenderReception {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoPsngKeyCyl_Rx_VEC_CryptoProxyReceiverReception {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoReducedSetMode_Tx_VEC_CryptoProxySenderConfirmation {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoReducedSetMode_Tx_VEC_CryptoProxySenderReception {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoTheftAlarmActivation_Tx_VEC_CryptoProxySenderConfirmation {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_CryptoTheftAlarmActivation_Tx_VEC_CryptoProxySenderReception {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_DiagnosticComponent_LocalTimeDistribution_200ms_Runnable {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_Keyfob_Radio_Com_NVM_KeyFobNVDataRxEventRunnable {
      MASK = AUTO;
   }:"@ROC@@NR@";

   EVENT Rte_Ev_Run_SCIM_Manager_SCIM_Manager_EnterRun {
      MASK = AUTO;
   }:"@ROC@@NR@";
