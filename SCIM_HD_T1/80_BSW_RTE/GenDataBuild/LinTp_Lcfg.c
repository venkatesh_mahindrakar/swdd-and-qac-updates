/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: LinTp
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: LinTp_Lcfg.c
 *   Generation Time: 2020-08-20 13:43:06
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/


#define LINTP_LCFG_SOURCE

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  Includes
 *********************************************************************************************************************/
#include "LinIf.h"
#include "PduR_LinTp.h"

/**********************************************************************************************************************
  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/



/**********************************************************************************************************************
  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA
**********************************************************************************************************************/



/**********************************************************************************************************************
  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  LinTp_ChannelConfig
**********************************************************************************************************************/
/** 
  \var    LinTp_ChannelConfig
  \details
  Element                    Description
  LinTp_SchedChangeNotify
  LinTp_StrictNADCheck       My comment /ActiveEcuC/LinTp/LinTpGlobalConfig/CHNL_5864b8ff
*/ 
#define LINTP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinTp_ChannelConfigType, LINTP_CONST) LinTp_ChannelConfig[8] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    LinTp_SchedChangeNotify  LinTp_StrictNADCheck        Referable Keys */
  { /*     0 */                   FALSE,                FALSE },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/CHNL_45618847] */
  { /*     1 */                   FALSE,                FALSE },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/CHNL_8e3d5be2] */
  { /*     2 */                   FALSE,                FALSE },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/CHNL_08a9294c] */
  { /*     3 */                   FALSE,                FALSE },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/CHNL_c3f5fae9] */
  { /*     4 */                   FALSE,                FALSE },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/CHNL_def0ca51] */
  { /*     5 */                   FALSE,                FALSE },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/CHNL_15ac19f4] */
  { /*     6 */                   FALSE,                FALSE },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/CHNL_93386b5a] */
  { /*     7 */                   FALSE,                FALSE }   /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/CHNL_5864b8ff] */
};
#define LINTP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinTp_LinIfToLinTpChannel
**********************************************************************************************************************/
/** 
  \var    LinTp_LinIfToLinTpChannel
  \details
  Element    Description
  Channel
*/ 
#define LINTP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinTp_LinIfToLinTpChannelType, LINTP_CONST) LinTp_LinIfToLinTpChannel[8] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    Channel                              */
  { /*     0 */ LinIfConf_LinIfChannel_CHNL_45618847 },
  { /*     1 */ LinIfConf_LinIfChannel_CHNL_8e3d5be2 },
  { /*     2 */ LinIfConf_LinIfChannel_CHNL_08a9294c },
  { /*     3 */ LinIfConf_LinIfChannel_CHNL_c3f5fae9 },
  { /*     4 */ LinIfConf_LinIfChannel_CHNL_def0ca51 },
  { /*     5 */ LinIfConf_LinIfChannel_CHNL_15ac19f4 },
  { /*     6 */ LinIfConf_LinIfChannel_CHNL_93386b5a },
  { /*     7 */ LinIfConf_LinIfChannel_CHNL_5864b8ff }
};
#define LINTP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinTp_RxNSdu
**********************************************************************************************************************/
/** 
  \var    LinTp_RxNSdu
  \brief  List of all LinTp RxNsdus sorted by their PduId
  \details
  Element            Description
  UpperLayerPduId    RxNSdu external ID (SNV)
  CtrlIdx            the index of the 1:1 relation pointing to LinTp_Ctrl
  NAD                NAD
  Ncr                Ncr timeout in ticks
*/ 
#define LINTP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinTp_RxNSduType, LINTP_CONST) LinTp_RxNSdu[124] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    UpperLayerPduId                          CtrlIdx  NAD    Ncr          Referable Keys */
  { /*     0 */ PduRConf_PduRSrcPdu_PduRSrcPdu_5b587d0d,      3u, 0x13u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_CCFW_oLIN03_d7124ce9_Rx] */
  { /*     1 */ PduRConf_PduRSrcPdu_PduRSrcPdu_f46eac1f,      3u, 0x12u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_DLFW_oLIN03_0f2202e7_Rx] */
  { /*     2 */ PduRConf_PduRSrcPdu_PduRSrcPdu_903f7ca8,      3u, 0x0Eu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_ELCP1_oLIN03_8611f0b3_Rx] */
  { /*     3 */ PduRConf_PduRSrcPdu_PduRSrcPdu_1e24ebac,      3u, 0x23u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_ELCP2_oLIN03_089ef750_Rx] */
  { /*     4 */ PduRConf_PduRSrcPdu_PduRSrcPdu_78104876,      0u, 0x4Au, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_4A_oLIN00_8520c1c5_Rx] */
  { /*     5 */ PduRConf_PduRSrcPdu_PduRSrcPdu_95de453f,      0u, 0x4Bu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_4B_oLIN00_0bafc626_Rx] */
  { /*     6 */ PduRConf_PduRSrcPdu_PduRSrcPdu_ee5fa3bc,      0u, 0x4Cu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_4C_oLIN00_c705c6b8_Rx] */
  { /*     7 */ PduRConf_PduRSrcPdu_PduRSrcPdu_0a403810,      0u, 0x4Du, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_4D_oLIN00_cdc0cfa1_Rx] */
  { /*     8 */ PduRConf_PduRSrcPdu_PduRSrcPdu_bcced411,      0u, 0x4Eu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_4E_oLIN00_016acf3f_Rx] */
  { /*     9 */ PduRConf_PduRSrcPdu_PduRSrcPdu_3b1029aa,      0u, 0x4Fu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_4F_oLIN00_8fe5c8dc_Rx] */
  { /*    10 */ PduRConf_PduRSrcPdu_PduRSrcPdu_f0ad078f,      0u, 0x5Au, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_5A_oLIN00_925bd586_Rx] */
  { /*    11 */ PduRConf_PduRSrcPdu_PduRSrcPdu_f600435a,      0u, 0x5Bu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_5B_oLIN00_1cd4d265_Rx] */
  { /*    12 */ PduRConf_PduRSrcPdu_PduRSrcPdu_1ec07ae6,      0u, 0x5Cu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_5C_oLIN00_d07ed2fb_Rx] */
  { /*    13 */ PduRConf_PduRSrcPdu_PduRSrcPdu_6150cbe1,      0u, 0x5Du, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_5D_oLIN00_dabbdbe2_Rx] */
  { /*    14 */ PduRConf_PduRSrcPdu_PduRSrcPdu_e19a2a43,      0u, 0x5Eu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_5E_oLIN00_1611db7c_Rx] */
  { /*    15 */ PduRConf_PduRSrcPdu_PduRSrcPdu_3574755d,      0u, 0x5Fu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_5F_oLIN00_989edc9f_Rx] */
  { /*    16 */ PduRConf_PduRSrcPdu_PduRSrcPdu_829de581,      0u, 0x40u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_40_oLIN00_e5da50cb_Rx] */
  { /*    17 */ PduRConf_PduRSrcPdu_PduRSrcPdu_5fe7b952,      0u, 0x41u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_41_oLIN00_29705055_Rx] */
  { /*    18 */ PduRConf_PduRSrcPdu_PduRSrcPdu_f13661f0,      0u, 0x42u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_42_oLIN00_a7ff57b6_Rx] */
  { /*    19 */ PduRConf_PduRSrcPdu_PduRSrcPdu_91c4d480,      0u, 0x43u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_43_oLIN00_6b555728_Rx] */
  { /*    20 */ PduRConf_PduRSrcPdu_PduRSrcPdu_ea896e64,      0u, 0x44u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_44_oLIN00_61905e31_Rx] */
  { /*    21 */ PduRConf_PduRSrcPdu_PduRSrcPdu_209270f6,      0u, 0x45u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_45_oLIN00_ad3a5eaf_Rx] */
  { /*    22 */ PduRConf_PduRSrcPdu_PduRSrcPdu_fcc4f722,      0u, 0x46u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_46_oLIN00_23b5594c_Rx] */
  { /*    23 */ PduRConf_PduRSrcPdu_PduRSrcPdu_cd0c7f78,      0u, 0x47u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_47_oLIN00_ef1f59d2_Rx] */
  { /*    24 */ PduRConf_PduRSrcPdu_PduRSrcPdu_8972d072,      0u, 0x48u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_48_oLIN00_363f4b7e_Rx] */
  { /*    25 */ PduRConf_PduRSrcPdu_PduRSrcPdu_cdbffe3a,      0u, 0x49u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_49_oLIN00_fa954be0_Rx] */
  { /*    26 */ PduRConf_PduRSrcPdu_PduRSrcPdu_599d0d59,      0u, 0x50u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_50_oLIN00_f2a14488_Rx] */
  { /*    27 */ PduRConf_PduRSrcPdu_PduRSrcPdu_ea09514d,      0u, 0x51u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_51_oLIN00_3e0b4416_Rx] */
  { /*    28 */ PduRConf_PduRSrcPdu_PduRSrcPdu_fee126de,      0u, 0x52u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_52_oLIN00_b08443f5_Rx] */
  { /*    29 */ PduRConf_PduRSrcPdu_PduRSrcPdu_1895a63c,      0u, 0x53u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_53_oLIN00_7c2e436b_Rx] */
  { /*    30 */ PduRConf_PduRSrcPdu_PduRSrcPdu_d72a79d3,      0u, 0x54u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_54_oLIN00_76eb4a72_Rx] */
  { /*    31 */ PduRConf_PduRSrcPdu_PduRSrcPdu_a1c49f75,      0u, 0x55u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_55_oLIN00_ba414aec_Rx] */
  { /*    32 */ PduRConf_PduRSrcPdu_PduRSrcPdu_26e14a14,      0u, 0x56u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_56_oLIN00_34ce4d0f_Rx] */
  { /*    33 */ PduRConf_PduRSrcPdu_PduRSrcPdu_797d2ca8,      0u, 0x57u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_57_oLIN00_f8644d91_Rx] */
  { /*    34 */ PduRConf_PduRSrcPdu_PduRSrcPdu_f5e79a62,      0u, 0x58u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_58_oLIN00_21445f3d_Rx] */
  { /*    35 */ PduRConf_PduRSrcPdu_PduRSrcPdu_52c84a11,      0u, 0x59u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_59_oLIN00_edee5fa3_Rx] */
  { /*    36 */ PduRConf_PduRSrcPdu_PduRSrcPdu_f1ad5858,      0u, 0x60u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_60_oLIN00_cb2c784d_Rx] */
  { /*    37 */ PduRConf_PduRSrcPdu_PduRSrcPdu_99747a0b,      0u, 0x02u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_L1_oLIN00_620b3198_Rx] */
  { /*    38 */ PduRConf_PduRSrcPdu_PduRSrcPdu_afe1a735,      1u, 0x02u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_L2_oLIN01_9b8306ed_Rx] */
  { /*    39 */ PduRConf_PduRSrcPdu_PduRSrcPdu_55c6efb1,      2u, 0x02u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_L3_oLIN02_ce2057c9_Rx] */
  { /*    40 */ PduRConf_PduRSrcPdu_PduRSrcPdu_fc6b6d63,      3u, 0x02u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_L4_oLIN03_b3e26e46_Rx] */
  { /*    41 */ PduRConf_PduRSrcPdu_PduRSrcPdu_377e0be2,      4u, 0x02u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP1_L5_oLIN04_e12cfb7b_Rx] */
  { /*    42 */ PduRConf_PduRSrcPdu_PduRSrcPdu_8b61dcaf,      1u, 0x4Au, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_4A_oLIN01_6bc59752_Rx] */
  { /*    43 */ PduRConf_PduRSrcPdu_PduRSrcPdu_9fc9d0ca,      1u, 0x4Bu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_4B_oLIN01_e54a90b1_Rx] */
  { /*    44 */ PduRConf_PduRSrcPdu_PduRSrcPdu_37e39e86,      1u, 0x4Cu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_4C_oLIN01_29e0902f_Rx] */
  { /*    45 */ PduRConf_PduRSrcPdu_PduRSrcPdu_8e21deb6,      1u, 0x4Du, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_4D_oLIN01_23259936_Rx] */
  { /*    46 */ PduRConf_PduRSrcPdu_PduRSrcPdu_09578dd1,      1u, 0x4Eu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_4E_oLIN01_ef8f99a8_Rx] */
  { /*    47 */ PduRConf_PduRSrcPdu_PduRSrcPdu_b8d73109,      1u, 0x4Fu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_4F_oLIN01_61009e4b_Rx] */
  { /*    48 */ PduRConf_PduRSrcPdu_PduRSrcPdu_e3e49c92,      1u, 0x5Au, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_5A_oLIN01_7cbe8311_Rx] */
  { /*    49 */ PduRConf_PduRSrcPdu_PduRSrcPdu_128e965d,      1u, 0x5Bu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_5B_oLIN01_f23184f2_Rx] */
    /* Index    UpperLayerPduId                          CtrlIdx  NAD    Ncr          Referable Keys */
  { /*    50 */ PduRConf_PduRSrcPdu_PduRSrcPdu_0ff2af9d,      1u, 0x5Cu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_5C_oLIN01_3e9b846c_Rx] */
  { /*    51 */ PduRConf_PduRSrcPdu_PduRSrcPdu_388f0802,      1u, 0x5Du, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_5D_oLIN01_345e8d75_Rx] */
  { /*    52 */ PduRConf_PduRSrcPdu_PduRSrcPdu_6288cdc0,      1u, 0x5Eu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_5E_oLIN01_f8f48deb_Rx] */
  { /*    53 */ PduRConf_PduRSrcPdu_PduRSrcPdu_34787a47,      1u, 0x5Fu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_5F_oLIN01_767b8a08_Rx] */
  { /*    54 */ PduRConf_PduRSrcPdu_PduRSrcPdu_adc354c3,      1u, 0x40u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_40_oLIN01_0b3f065c_Rx] */
  { /*    55 */ PduRConf_PduRSrcPdu_PduRSrcPdu_78cc87f9,      1u, 0x41u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_41_oLIN01_c79506c2_Rx] */
  { /*    56 */ PduRConf_PduRSrcPdu_PduRSrcPdu_8a6e9cc9,      1u, 0x42u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_42_oLIN01_491a0121_Rx] */
  { /*    57 */ PduRConf_PduRSrcPdu_PduRSrcPdu_671943d6,      1u, 0x43u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_43_oLIN01_85b001bf_Rx] */
  { /*    58 */ PduRConf_PduRSrcPdu_PduRSrcPdu_790bf261,      1u, 0x44u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_44_oLIN01_8f7508a6_Rx] */
  { /*    59 */ PduRConf_PduRSrcPdu_PduRSrcPdu_30aaf785,      1u, 0x45u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_45_oLIN01_43df0838_Rx] */
  { /*    60 */ PduRConf_PduRSrcPdu_PduRSrcPdu_03c5baed,      1u, 0x46u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_46_oLIN01_cd500fdb_Rx] */
  { /*    61 */ PduRConf_PduRSrcPdu_PduRSrcPdu_fd40f52b,      1u, 0x47u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_47_oLIN01_01fa0f45_Rx] */
  { /*    62 */ PduRConf_PduRSrcPdu_PduRSrcPdu_1d064c43,      1u, 0x48u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_48_oLIN01_d8da1de9_Rx] */
  { /*    63 */ PduRConf_PduRSrcPdu_PduRSrcPdu_cb7f46b3,      1u, 0x49u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_49_oLIN01_14701d77_Rx] */
  { /*    64 */ PduRConf_PduRSrcPdu_PduRSrcPdu_146a20f0,      1u, 0x50u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_50_oLIN01_1c44121f_Rx] */
  { /*    65 */ PduRConf_PduRSrcPdu_PduRSrcPdu_3758b7ab,      1u, 0x51u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_51_oLIN01_d0ee1281_Rx] */
  { /*    66 */ PduRConf_PduRSrcPdu_PduRSrcPdu_44c35ff0,      1u, 0x52u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_52_oLIN01_5e611562_Rx] */
  { /*    67 */ PduRConf_PduRSrcPdu_PduRSrcPdu_6af528f4,      1u, 0x53u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_53_oLIN01_92cb15fc_Rx] */
  { /*    68 */ PduRConf_PduRSrcPdu_PduRSrcPdu_da4d01a7,      1u, 0x54u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_54_oLIN01_980e1ce5_Rx] */
  { /*    69 */ PduRConf_PduRSrcPdu_PduRSrcPdu_a7fc524b,      1u, 0x55u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_55_oLIN01_54a41c7b_Rx] */
  { /*    70 */ PduRConf_PduRSrcPdu_PduRSrcPdu_4e01f700,      1u, 0x56u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_56_oLIN01_da2b1b98_Rx] */
  { /*    71 */ PduRConf_PduRSrcPdu_PduRSrcPdu_8b212133,      1u, 0x57u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_57_oLIN01_16811b06_Rx] */
  { /*    72 */ PduRConf_PduRSrcPdu_PduRSrcPdu_82a01191,      1u, 0x58u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_58_oLIN01_cfa109aa_Rx] */
  { /*    73 */ PduRConf_PduRSrcPdu_PduRSrcPdu_bff68e2d,      1u, 0x59u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_59_oLIN01_030b0934_Rx] */
  { /*    74 */ PduRConf_PduRSrcPdu_PduRSrcPdu_328650f4,      1u, 0x60u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_60_oLIN01_25c92eda_Rx] */
  { /*    75 */ PduRConf_PduRSrcPdu_PduRSrcPdu_f7959d8f,      0u, 0x03u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_L1_oLIN00_fbe95799_Rx] */
  { /*    76 */ PduRConf_PduRSrcPdu_PduRSrcPdu_e28df3db,      1u, 0x03u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_L2_oLIN01_026160ec_Rx] */
  { /*    77 */ PduRConf_PduRSrcPdu_PduRSrcPdu_1b1ab2ff,      2u, 0x03u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP2_L3_oLIN02_57c231c8_Rx] */
  { /*    78 */ PduRConf_PduRSrcPdu_PduRSrcPdu_63202e6b,      2u, 0x4Au, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_4A_oLIN02_33421928_Rx] */
  { /*    79 */ PduRConf_PduRSrcPdu_PduRSrcPdu_1c94c14e,      2u, 0x4Bu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_4B_oLIN02_bdcd1ecb_Rx] */
  { /*    80 */ PduRConf_PduRSrcPdu_PduRSrcPdu_b395104c,      2u, 0x4Cu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_4C_oLIN02_71671e55_Rx] */
  { /*    81 */ PduRConf_PduRSrcPdu_PduRSrcPdu_a0dc157f,      2u, 0x4Du, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_4D_oLIN02_7ba2174c_Rx] */
  { /*    82 */ PduRConf_PduRSrcPdu_PduRSrcPdu_72de301a,      2u, 0x4Eu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_4E_oLIN02_b70817d2_Rx] */
  { /*    83 */ PduRConf_PduRSrcPdu_PduRSrcPdu_862b3ebb,      2u, 0x4Fu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_4F_oLIN02_39871031_Rx] */
  { /*    84 */ PduRConf_PduRSrcPdu_PduRSrcPdu_17f98beb,      2u, 0x5Au, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_5A_oLIN02_24390d6b_Rx] */
  { /*    85 */ PduRConf_PduRSrcPdu_PduRSrcPdu_3ad59601,      2u, 0x5Bu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_5B_oLIN02_aab60a88_Rx] */
  { /*    86 */ PduRConf_PduRSrcPdu_PduRSrcPdu_fad5e462,      2u, 0x5Cu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_5C_oLIN02_661c0a16_Rx] */
  { /*    87 */ PduRConf_PduRSrcPdu_PduRSrcPdu_8e9adf0f,      2u, 0x5Du, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_5D_oLIN02_6cd9030f_Rx] */
  { /*    88 */ PduRConf_PduRSrcPdu_PduRSrcPdu_50f38210,      2u, 0x5Eu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_5E_oLIN02_a0730391_Rx] */
  { /*    89 */ PduRConf_PduRSrcPdu_PduRSrcPdu_9894765a,      2u, 0x5Fu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_5F_oLIN02_2efc0472_Rx] */
  { /*    90 */ PduRConf_PduRSrcPdu_PduRSrcPdu_b3908af8,      2u, 0x40u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_40_oLIN02_53b88826_Rx] */
  { /*    91 */ PduRConf_PduRSrcPdu_PduRSrcPdu_731eaf66,      2u, 0x41u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_41_oLIN02_9f1288b8_Rx] */
  { /*    92 */ PduRConf_PduRSrcPdu_PduRSrcPdu_0a252841,      2u, 0x42u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_42_oLIN02_119d8f5b_Rx] */
  { /*    93 */ PduRConf_PduRSrcPdu_PduRSrcPdu_3b58f9ef,      2u, 0x43u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_43_oLIN02_dd378fc5_Rx] */
  { /*    94 */ PduRConf_PduRSrcPdu_PduRSrcPdu_2681dcb1,      2u, 0x44u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_44_oLIN02_d7f286dc_Rx] */
  { /*    95 */ PduRConf_PduRSrcPdu_PduRSrcPdu_54004ea5,      2u, 0x45u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_45_oLIN02_1b588642_Rx] */
  { /*    96 */ PduRConf_PduRSrcPdu_PduRSrcPdu_e7f4913f,      2u, 0x46u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_46_oLIN02_95d781a1_Rx] */
  { /*    97 */ PduRConf_PduRSrcPdu_PduRSrcPdu_bdab974a,      2u, 0x47u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_47_oLIN02_597d813f_Rx] */
  { /*    98 */ PduRConf_PduRSrcPdu_PduRSrcPdu_ccd695fa,      2u, 0x48u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_48_oLIN02_805d9393_Rx] */
  { /*    99 */ PduRConf_PduRSrcPdu_PduRSrcPdu_2ae49e9d,      2u, 0x49u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_49_oLIN02_4cf7930d_Rx] */
    /* Index    UpperLayerPduId                          CtrlIdx  NAD    Ncr          Referable Keys */
  { /*   100 */ PduRConf_PduRSrcPdu_PduRSrcPdu_943de0f2,      2u, 0x50u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_50_oLIN02_44c39c65_Rx] */
  { /*   101 */ PduRConf_PduRSrcPdu_PduRSrcPdu_0072b072,      2u, 0x51u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_51_oLIN02_88699cfb_Rx] */
  { /*   102 */ PduRConf_PduRSrcPdu_PduRSrcPdu_7a8b2337,      2u, 0x52u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_52_oLIN02_06e69b18_Rx] */
  { /*   103 */ PduRConf_PduRSrcPdu_PduRSrcPdu_f75fb2d2,      2u, 0x53u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_53_oLIN02_ca4c9b86_Rx] */
  { /*   104 */ PduRConf_PduRSrcPdu_PduRSrcPdu_74a4056f,      2u, 0x54u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_54_oLIN02_c089929f_Rx] */
  { /*   105 */ PduRConf_PduRSrcPdu_PduRSrcPdu_46901311,      2u, 0x55u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_55_oLIN02_0c239201_Rx] */
  { /*   106 */ PduRConf_PduRSrcPdu_PduRSrcPdu_c1b5c670,      2u, 0x56u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_56_oLIN02_82ac95e2_Rx] */
  { /*   107 */ PduRConf_PduRSrcPdu_PduRSrcPdu_09fe52bd,      2u, 0x57u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_57_oLIN02_4e06957c_Rx] */
  { /*   108 */ PduRConf_PduRSrcPdu_PduRSrcPdu_b98ce9b7,      2u, 0x58u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_58_oLIN02_972687d0_Rx] */
  { /*   109 */ PduRConf_PduRSrcPdu_PduRSrcPdu_401c60b5,      2u, 0x59u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_59_oLIN02_5b8c874e_Rx] */
  { /*   110 */ PduRConf_PduRSrcPdu_PduRSrcPdu_3f09cf26,      2u, 0x60u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_60_oLIN02_7d4ea0a0_Rx] */
  { /*   111 */ PduRConf_PduRSrcPdu_PduRSrcPdu_6b1d1eb8,      1u, 0x04u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP3_L2_oLIN01_c3efbf2c_Rx] */
  { /*   112 */ PduRConf_PduRSrcPdu_PduRSrcPdu_c33e4609,      3u, 0x40u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP4_40_oLIN03_0d84ad33_Rx] */
  { /*   113 */ PduRConf_PduRSrcPdu_PduRSrcPdu_87b10390,      1u, 0x05u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP4_L2_oLIN01_ead4aaaf_Rx] */
  { /*   114 */ PduRConf_PduRSrcPdu_PduRSrcPdu_660d977b,      4u, 0x40u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_FSP5_40_oLIN04_526ee750_Rx] */
  { /*   115 */ PduRConf_PduRSrcPdu_PduRSrcPdu_a0f8dbfb,      0u, 0x0Bu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_ILCP1_oLIN00_1a12ec88_Rx] */
  { /*   116 */ PduRConf_PduRSrcPdu_PduRSrcPdu_95103cdf,      3u, 0x20u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_ILCP2_oLIN03_0d94bad1_Rx] */
  { /*   117 */ PduRConf_PduRSrcPdu_PduRSrcPdu_8389a02c,      0u, 0x1Du, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_LECM2_oLIN00_2afc07d8_Rx] */
  { /*   118 */ PduRConf_PduRSrcPdu_PduRSrcPdu_3b8e8c69,      0u, 0x1Eu, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_LECMBasic_oLIN00_029997c6_Rx] */
  { /*   119 */ PduRConf_PduRSrcPdu_PduRSrcPdu_0e5523b8,      4u, 0x10u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_RCECS_oLIN04_f9903c90_Rx] */
  { /*   120 */ PduRConf_PduRSrcPdu_PduRSrcPdu_68bca152,      2u, 0x39u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_TCP_oLIN02_b3851a34_Rx] */
  { /*   121 */ PduRConf_PduRSrcPdu_PduRSrcPdu_1c42292e,      5u, 0xD0u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_LinSlave_L8_oLIN05_ae44c72a_Rx] */
  { /*   122 */ PduRConf_PduRSrcPdu_PduRSrcPdu_74040ff9,      6u, 0xD0u, 0x1Fu },  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_LinSlave_L8_oLIN06_374d9690_Rx] */
  { /*   123 */ PduRConf_PduRSrcPdu_PduRSrcPdu_f7f89fa3,      7u, 0xD0u, 0x1Fu }   /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/SlaveResp_LinSlave_L8_oLIN07_404aa606_Rx] */
};
#define LINTP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinTp_TxNSdu
**********************************************************************************************************************/
/** 
  \var    LinTp_TxNSdu
  \brief  List of all LinTp TxNsdus sorted by their PduId
  \details
  Element               Description
  UpperLayerPduId       TxNSdu external ID (SNV)
  AssociatedRxNSduId
  CtrlIdx               the index of the 1:1 relation pointing to LinTp_Ctrl
  NAD                   NAD
  Nas                   Nas timeout in ticks
  Ncs                   Ncs timeout in ticks
*/ 
#define LINTP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinTp_TxNSduType, LINTP_CONST) LinTp_TxNSdu[129] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    UpperLayerPduId                                              AssociatedRxNSduId  CtrlIdx  NAD    Nas    Ncs          Comment                                                                    Referable Keys */
  { /*     0 */ PduRConf_PduRDestPdu_MasterReq_CCFW_oLIN03_fcabea98_Tx     ,                 0u,      3u, 0x13u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_CCFW_oLIN03_d7124ce9_Rx]      */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_CCFW_oLIN03_98357989_Tx] */
  { /*     1 */ PduRConf_PduRDestPdu_MasterReq_DLFW_oLIN03_c633fea1_Tx     ,                 1u,      3u, 0x12u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_DLFW_oLIN03_0f2202e7_Rx]      */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_DLFW_oLIN03_40053787_Tx] */
  { /*     2 */ PduRConf_PduRDestPdu_MasterReq_ELCP1_oLIN03_18fd3d27_Tx    ,                 2u,      3u, 0x0Eu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_ELCP1_oLIN03_8611f0b3_Rx]     */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_ELCP1_oLIN03_cbecb6de_Tx] */
  { /*     3 */ PduRConf_PduRDestPdu_MasterReq_ELCP2_oLIN03_6e18041a_Tx    ,                 3u,      3u, 0x23u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_ELCP2_oLIN03_089ef750_Rx]     */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_ELCP2_oLIN03_4563b13d_Tx] */
  { /*     4 */ PduRConf_PduRDestPdu_MasterReq_FSP1_4A_oLIN00_94ed91b3_Tx  ,                 4u,      0u, 0x4Au, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_4A_oLIN00_8520c1c5_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_4A_oLIN00_cacc7d77_Tx] */
  { /*     5 */ PduRConf_PduRDestPdu_MasterReq_FSP1_4B_oLIN00_e208a88e_Tx  ,                 5u,      0u, 0x4Bu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_4B_oLIN00_0bafc626_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_4B_oLIN00_44437a94_Tx] */
  { /*     6 */ PduRConf_PduRDestPdu_MasterReq_FSP1_4C_oLIN00_797b425a_Tx  ,                 6u,      0u, 0x4Cu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_4C_oLIN00_c705c6b8_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_4C_oLIN00_88e97a0a_Tx] */
  { /*     7 */ PduRConf_PduRDestPdu_MasterReq_FSP1_4D_oLIN00_0fc2daf4_Tx  ,                 7u,      0u, 0x4Du, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_4D_oLIN00_cdc0cfa1_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_4D_oLIN00_822c7313_Tx] */
  { /*     8 */ PduRConf_PduRDestPdu_MasterReq_FSP1_4E_oLIN00_94b13020_Tx  ,                 8u,      0u, 0x4Eu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_4E_oLIN00_016acf3f_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_4E_oLIN00_4e86738d_Tx] */
  { /*     9 */ PduRConf_PduRDestPdu_MasterReq_FSP1_4F_oLIN00_e254091d_Tx  ,                 9u,      0u, 0x4Fu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_4F_oLIN00_8fe5c8dc_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_4F_oLIN00_c009746e_Tx] */
  { /*    10 */ PduRConf_PduRDestPdu_MasterReq_FSP1_5A_oLIN00_15c8f494_Tx  ,                10u,      0u, 0x5Au, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_5A_oLIN00_925bd586_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_5A_oLIN00_ddb76934_Tx] */
  { /*    11 */ PduRConf_PduRDestPdu_MasterReq_FSP1_5B_oLIN00_632dcda9_Tx  ,                11u,      0u, 0x5Bu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_5B_oLIN00_1cd4d265_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_5B_oLIN00_53386ed7_Tx] */
  { /*    12 */ PduRConf_PduRDestPdu_MasterReq_FSP1_5C_oLIN00_f85e277d_Tx  ,                12u,      0u, 0x5Cu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_5C_oLIN00_d07ed2fb_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_5C_oLIN00_9f926e49_Tx] */
  { /*    13 */ PduRConf_PduRDestPdu_MasterReq_FSP1_5D_oLIN00_8ee7bfd3_Tx  ,                13u,      0u, 0x5Du, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_5D_oLIN00_dabbdbe2_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_5D_oLIN00_95576750_Tx] */
  { /*    14 */ PduRConf_PduRDestPdu_MasterReq_FSP1_5E_oLIN00_15945507_Tx  ,                14u,      0u, 0x5Eu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_5E_oLIN00_1611db7c_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_5E_oLIN00_59fd67ce_Tx] */
  { /*    15 */ PduRConf_PduRDestPdu_MasterReq_FSP1_5F_oLIN00_63716c3a_Tx  ,                15u,      0u, 0x5Fu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_5F_oLIN00_989edc9f_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_5F_oLIN00_d772602d_Tx] */
  { /*    16 */ PduRConf_PduRDestPdu_MasterReq_FSP1_40_oLIN00_09c3e883_Tx  ,                16u,      0u, 0x40u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_40_oLIN00_e5da50cb_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_40_oLIN00_aa36ec79_Tx] */
  { /*    17 */ PduRConf_PduRDestPdu_MasterReq_FSP1_41_oLIN00_92b00257_Tx  ,                17u,      0u, 0x41u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_41_oLIN00_29705055_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_41_oLIN00_669cece7_Tx] */
  { /*    18 */ PduRConf_PduRDestPdu_MasterReq_FSP1_42_oLIN00_e4553b6a_Tx  ,                18u,      0u, 0x42u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_42_oLIN00_a7ff57b6_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_42_oLIN00_e813eb04_Tx] */
  { /*    19 */ PduRConf_PduRDestPdu_MasterReq_FSP1_43_oLIN00_7f26d1be_Tx  ,                19u,      0u, 0x43u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_43_oLIN00_6b555728_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_43_oLIN00_24b9eb9a_Tx] */
  { /*    20 */ PduRConf_PduRDestPdu_MasterReq_FSP1_44_oLIN00_099f4910_Tx  ,                20u,      0u, 0x44u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_44_oLIN00_61905e31_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_44_oLIN00_2e7ce283_Tx] */
  { /*    21 */ PduRConf_PduRDestPdu_MasterReq_FSP1_45_oLIN00_92eca3c4_Tx  ,                21u,      0u, 0x45u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_45_oLIN00_ad3a5eaf_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_45_oLIN00_e2d6e21d_Tx] */
  { /*    22 */ PduRConf_PduRDestPdu_MasterReq_FSP1_46_oLIN00_e4099af9_Tx  ,                22u,      0u, 0x46u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_46_oLIN00_23b5594c_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_46_oLIN00_6c59e5fe_Tx] */
  { /*    23 */ PduRConf_PduRDestPdu_MasterReq_FSP1_47_oLIN00_7f7a702d_Tx  ,                23u,      0u, 0x47u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_47_oLIN00_ef1f59d2_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_47_oLIN00_a0f3e560_Tx] */
  { /*    24 */ PduRConf_PduRDestPdu_MasterReq_FSP1_48_oLIN00_097aaba5_Tx  ,                24u,      0u, 0x48u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_48_oLIN00_363f4b7e_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_48_oLIN00_79d3f7cc_Tx] */
  { /*    25 */ PduRConf_PduRDestPdu_MasterReq_FSP1_49_oLIN00_92094171_Tx  ,                25u,      0u, 0x49u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_49_oLIN00_fa954be0_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_49_oLIN00_b579f752_Tx] */
  { /*    26 */ PduRConf_PduRDestPdu_MasterReq_FSP1_50_oLIN00_88e68da4_Tx  ,                26u,      0u, 0x50u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_50_oLIN00_f2a14488_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_50_oLIN00_bd4df83a_Tx] */
  { /*    27 */ PduRConf_PduRDestPdu_MasterReq_FSP1_51_oLIN00_13956770_Tx  ,                27u,      0u, 0x51u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_51_oLIN00_3e0b4416_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_51_oLIN00_71e7f8a4_Tx] */
  { /*    28 */ PduRConf_PduRDestPdu_MasterReq_FSP1_52_oLIN00_65705e4d_Tx  ,                28u,      0u, 0x52u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_52_oLIN00_b08443f5_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_52_oLIN00_ff68ff47_Tx] */
  { /*    29 */ PduRConf_PduRDestPdu_MasterReq_FSP1_53_oLIN00_fe03b499_Tx  ,                29u,      0u, 0x53u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_53_oLIN00_7c2e436b_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_53_oLIN00_33c2ffd9_Tx] */
  { /*    30 */ PduRConf_PduRDestPdu_MasterReq_FSP1_54_oLIN00_88ba2c37_Tx  ,                30u,      0u, 0x54u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_54_oLIN00_76eb4a72_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_54_oLIN00_3907f6c0_Tx] */
  { /*    31 */ PduRConf_PduRDestPdu_MasterReq_FSP1_55_oLIN00_13c9c6e3_Tx  ,                31u,      0u, 0x55u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_55_oLIN00_ba414aec_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_55_oLIN00_f5adf65e_Tx] */
  { /*    32 */ PduRConf_PduRDestPdu_MasterReq_FSP1_56_oLIN00_652cffde_Tx  ,                32u,      0u, 0x56u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_56_oLIN00_34ce4d0f_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_56_oLIN00_7b22f1bd_Tx] */
  { /*    33 */ PduRConf_PduRDestPdu_MasterReq_FSP1_57_oLIN00_fe5f150a_Tx  ,                33u,      0u, 0x57u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_57_oLIN00_f8644d91_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_57_oLIN00_b788f123_Tx] */
  { /*    34 */ PduRConf_PduRDestPdu_MasterReq_FSP1_58_oLIN00_885fce82_Tx  ,                34u,      0u, 0x58u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_58_oLIN00_21445f3d_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_58_oLIN00_6ea8e38f_Tx] */
  { /*    35 */ PduRConf_PduRDestPdu_MasterReq_FSP1_59_oLIN00_132c2456_Tx  ,                35u,      0u, 0x59u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_59_oLIN00_edee5fa3_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_59_oLIN00_a202e311_Tx] */
  { /*    36 */ PduRConf_PduRDestPdu_MasterReq_FSP1_60_oLIN00_d0f8248c_Tx  ,                36u,      0u, 0x60u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_60_oLIN00_cb2c784d_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_60_oLIN00_84c0c4ff_Tx] */
  { /*    37 */ PduRConf_PduRDestPdu_MasterReq_FSP1_L1_oLIN00_e7dc451b_Tx  ,                37u,      0u, 0x02u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_L1_oLIN00_620b3198_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_L1_oLIN00_2de78d2a_Tx] */
  { /*    38 */ PduRConf_PduRDestPdu_MasterReq_FSP1_L2_oLIN01_4856dd21_Tx  ,                38u,      1u, 0x02u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_L2_oLIN01_9b8306ed_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_L2_oLIN01_d46fba5f_Tx] */
  { /*    39 */ PduRConf_PduRDestPdu_MasterReq_FSP1_L3_oLIN02_63e4d2bd_Tx  ,                39u,      2u, 0x02u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_L3_oLIN02_ce2057c9_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_L3_oLIN02_81cceb7b_Tx] */
  { /*    40 */ PduRConf_PduRDestPdu_MasterReq_FSP1_L4_oLIN03_cc32eb14_Tx  ,                40u,      3u, 0x02u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_L4_oLIN03_b3e26e46_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_L4_oLIN03_fc0ed2f4_Tx] */
  { /*    41 */ PduRConf_PduRDestPdu_MasterReq_FSP1_L5_oLIN04_34dc6c16_Tx  ,                41u,      4u, 0x02u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP1_L5_oLIN04_e12cfb7b_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP1_L5_oLIN04_aec047c9_Tx] */
  { /*    42 */ PduRConf_PduRDestPdu_MasterReq_FSP2_4A_oLIN01_bfd5dd4c_Tx  ,                42u,      1u, 0x4Au, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_4A_oLIN01_6bc59752_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_4A_oLIN01_24292be0_Tx] */
  { /*    43 */ PduRConf_PduRDestPdu_MasterReq_FSP2_4B_oLIN01_c930e471_Tx  ,                43u,      1u, 0x4Bu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_4B_oLIN01_e54a90b1_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_4B_oLIN01_aaa62c03_Tx] */
  { /*    44 */ PduRConf_PduRDestPdu_MasterReq_FSP2_4C_oLIN01_52430ea5_Tx  ,                44u,      1u, 0x4Cu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_4C_oLIN01_29e0902f_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_4C_oLIN01_660c2c9d_Tx] */
  { /*    45 */ PduRConf_PduRDestPdu_MasterReq_FSP2_4D_oLIN01_24fa960b_Tx  ,                45u,      1u, 0x4Du, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_4D_oLIN01_23259936_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_4D_oLIN01_6cc92584_Tx] */
  { /*    46 */ PduRConf_PduRDestPdu_MasterReq_FSP2_4E_oLIN01_bf897cdf_Tx  ,                46u,      1u, 0x4Eu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_4E_oLIN01_ef8f99a8_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_4E_oLIN01_a063251a_Tx] */
  { /*    47 */ PduRConf_PduRDestPdu_MasterReq_FSP2_4F_oLIN01_c96c45e2_Tx  ,                47u,      1u, 0x4Fu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_4F_oLIN01_61009e4b_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_4F_oLIN01_2eec22f9_Tx] */
  { /*    48 */ PduRConf_PduRDestPdu_MasterReq_FSP2_5A_oLIN01_3ef0b86b_Tx  ,                48u,      1u, 0x5Au, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_5A_oLIN01_7cbe8311_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_5A_oLIN01_33523fa3_Tx] */
  { /*    49 */ PduRConf_PduRDestPdu_MasterReq_FSP2_5B_oLIN01_48158156_Tx  ,                49u,      1u, 0x5Bu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_5B_oLIN01_f23184f2_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_5B_oLIN01_bddd3840_Tx] */
    /* Index    UpperLayerPduId                                              AssociatedRxNSduId  CtrlIdx  NAD    Nas    Ncs          Comment                                                                    Referable Keys */
  { /*    50 */ PduRConf_PduRDestPdu_MasterReq_FSP2_5C_oLIN01_d3666b82_Tx  ,                50u,      1u, 0x5Cu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_5C_oLIN01_3e9b846c_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_5C_oLIN01_717738de_Tx] */
  { /*    51 */ PduRConf_PduRDestPdu_MasterReq_FSP2_5D_oLIN01_a5dff32c_Tx  ,                51u,      1u, 0x5Du, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_5D_oLIN01_345e8d75_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_5D_oLIN01_7bb231c7_Tx] */
  { /*    52 */ PduRConf_PduRDestPdu_MasterReq_FSP2_5E_oLIN01_3eac19f8_Tx  ,                52u,      1u, 0x5Eu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_5E_oLIN01_f8f48deb_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_5E_oLIN01_b7183159_Tx] */
  { /*    53 */ PduRConf_PduRDestPdu_MasterReq_FSP2_5F_oLIN01_484920c5_Tx  ,                53u,      1u, 0x5Fu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_5F_oLIN01_767b8a08_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_5F_oLIN01_399736ba_Tx] */
  { /*    54 */ PduRConf_PduRDestPdu_MasterReq_FSP2_40_oLIN01_22fba47c_Tx  ,                54u,      1u, 0x40u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_40_oLIN01_0b3f065c_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_40_oLIN01_44d3baee_Tx] */
  { /*    55 */ PduRConf_PduRDestPdu_MasterReq_FSP2_41_oLIN01_b9884ea8_Tx  ,                55u,      1u, 0x41u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_41_oLIN01_c79506c2_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_41_oLIN01_8879ba70_Tx] */
  { /*    56 */ PduRConf_PduRDestPdu_MasterReq_FSP2_42_oLIN01_cf6d7795_Tx  ,                56u,      1u, 0x42u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_42_oLIN01_491a0121_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_42_oLIN01_06f6bd93_Tx] */
  { /*    57 */ PduRConf_PduRDestPdu_MasterReq_FSP2_43_oLIN01_541e9d41_Tx  ,                57u,      1u, 0x43u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_43_oLIN01_85b001bf_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_43_oLIN01_ca5cbd0d_Tx] */
  { /*    58 */ PduRConf_PduRDestPdu_MasterReq_FSP2_44_oLIN01_22a705ef_Tx  ,                58u,      1u, 0x44u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_44_oLIN01_8f7508a6_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_44_oLIN01_c099b414_Tx] */
  { /*    59 */ PduRConf_PduRDestPdu_MasterReq_FSP2_45_oLIN01_b9d4ef3b_Tx  ,                59u,      1u, 0x45u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_45_oLIN01_43df0838_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_45_oLIN01_0c33b48a_Tx] */
  { /*    60 */ PduRConf_PduRDestPdu_MasterReq_FSP2_46_oLIN01_cf31d606_Tx  ,                60u,      1u, 0x46u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_46_oLIN01_cd500fdb_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_46_oLIN01_82bcb369_Tx] */
  { /*    61 */ PduRConf_PduRDestPdu_MasterReq_FSP2_47_oLIN01_54423cd2_Tx  ,                61u,      1u, 0x47u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_47_oLIN01_01fa0f45_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_47_oLIN01_4e16b3f7_Tx] */
  { /*    62 */ PduRConf_PduRDestPdu_MasterReq_FSP2_48_oLIN01_2242e75a_Tx  ,                62u,      1u, 0x48u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_48_oLIN01_d8da1de9_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_48_oLIN01_9736a15b_Tx] */
  { /*    63 */ PduRConf_PduRDestPdu_MasterReq_FSP2_49_oLIN01_b9310d8e_Tx  ,                63u,      1u, 0x49u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_49_oLIN01_14701d77_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_49_oLIN01_5b9ca1c5_Tx] */
  { /*    64 */ PduRConf_PduRDestPdu_MasterReq_FSP2_50_oLIN01_a3dec15b_Tx  ,                64u,      1u, 0x50u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_50_oLIN01_1c44121f_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_50_oLIN01_53a8aead_Tx] */
  { /*    65 */ PduRConf_PduRDestPdu_MasterReq_FSP2_51_oLIN01_38ad2b8f_Tx  ,                65u,      1u, 0x51u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_51_oLIN01_d0ee1281_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_51_oLIN01_9f02ae33_Tx] */
  { /*    66 */ PduRConf_PduRDestPdu_MasterReq_FSP2_52_oLIN01_4e4812b2_Tx  ,                66u,      1u, 0x52u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_52_oLIN01_5e611562_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_52_oLIN01_118da9d0_Tx] */
  { /*    67 */ PduRConf_PduRDestPdu_MasterReq_FSP2_53_oLIN01_d53bf866_Tx  ,                67u,      1u, 0x53u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_53_oLIN01_92cb15fc_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_53_oLIN01_dd27a94e_Tx] */
  { /*    68 */ PduRConf_PduRDestPdu_MasterReq_FSP2_54_oLIN01_a38260c8_Tx  ,                68u,      1u, 0x54u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_54_oLIN01_980e1ce5_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_54_oLIN01_d7e2a057_Tx] */
  { /*    69 */ PduRConf_PduRDestPdu_MasterReq_FSP2_55_oLIN01_38f18a1c_Tx  ,                69u,      1u, 0x55u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_55_oLIN01_54a41c7b_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_55_oLIN01_1b48a0c9_Tx] */
  { /*    70 */ PduRConf_PduRDestPdu_MasterReq_FSP2_56_oLIN01_4e14b321_Tx  ,                70u,      1u, 0x56u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_56_oLIN01_da2b1b98_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_56_oLIN01_95c7a72a_Tx] */
  { /*    71 */ PduRConf_PduRDestPdu_MasterReq_FSP2_57_oLIN01_d56759f5_Tx  ,                71u,      1u, 0x57u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_57_oLIN01_16811b06_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_57_oLIN01_596da7b4_Tx] */
  { /*    72 */ PduRConf_PduRDestPdu_MasterReq_FSP2_58_oLIN01_a367827d_Tx  ,                72u,      1u, 0x58u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_58_oLIN01_cfa109aa_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_58_oLIN01_804db518_Tx] */
  { /*    73 */ PduRConf_PduRDestPdu_MasterReq_FSP2_59_oLIN01_381468a9_Tx  ,                73u,      1u, 0x59u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_59_oLIN01_030b0934_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_59_oLIN01_4ce7b586_Tx] */
  { /*    74 */ PduRConf_PduRDestPdu_MasterReq_FSP2_60_oLIN01_fbc06873_Tx  ,                74u,      1u, 0x60u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_60_oLIN01_25c92eda_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_60_oLIN01_6a259268_Tx] */
  { /*    75 */ PduRConf_PduRDestPdu_MasterReq_FSP2_L1_oLIN00_158ba8e3_Tx  ,                75u,      0u, 0x03u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_L1_oLIN00_fbe95799_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_L1_oLIN00_b405eb2b_Tx] */
  { /*    76 */ PduRConf_PduRDestPdu_MasterReq_FSP2_L2_oLIN01_ba0130d9_Tx  ,                76u,      1u, 0x03u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_L2_oLIN01_026160ec_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_L2_oLIN01_4d8ddc5e_Tx] */
  { /*    77 */ PduRConf_PduRDestPdu_MasterReq_FSP2_L3_oLIN02_91b33f45_Tx  ,                77u,      2u, 0x03u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP2_L3_oLIN02_57c231c8_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP2_L3_oLIN02_182e8d7a_Tx] */
  { /*    78 */ PduRConf_PduRDestPdu_MasterReq_FSP3_4A_oLIN02_e8099e93_Tx  ,                78u,      2u, 0x4Au, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_4A_oLIN02_33421928_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_4A_oLIN02_7caea59a_Tx] */
  { /*    79 */ PduRConf_PduRDestPdu_MasterReq_FSP3_4B_oLIN02_9eeca7ae_Tx  ,                79u,      2u, 0x4Bu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_4B_oLIN02_bdcd1ecb_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_4B_oLIN02_f221a279_Tx] */
  { /*    80 */ PduRConf_PduRDestPdu_MasterReq_FSP3_4C_oLIN02_059f4d7a_Tx  ,                80u,      2u, 0x4Cu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_4C_oLIN02_71671e55_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_4C_oLIN02_3e8ba2e7_Tx] */
  { /*    81 */ PduRConf_PduRDestPdu_MasterReq_FSP3_4D_oLIN02_7326d5d4_Tx  ,                81u,      2u, 0x4Du, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_4D_oLIN02_7ba2174c_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_4D_oLIN02_344eabfe_Tx] */
  { /*    82 */ PduRConf_PduRDestPdu_MasterReq_FSP3_4E_oLIN02_e8553f00_Tx  ,                82u,      2u, 0x4Eu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_4E_oLIN02_b70817d2_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_4E_oLIN02_f8e4ab60_Tx] */
  { /*    83 */ PduRConf_PduRDestPdu_MasterReq_FSP3_4F_oLIN02_9eb0063d_Tx  ,                83u,      2u, 0x4Fu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_4F_oLIN02_39871031_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_4F_oLIN02_766bac83_Tx] */
  { /*    84 */ PduRConf_PduRDestPdu_MasterReq_FSP3_5A_oLIN02_692cfbb4_Tx  ,                84u,      2u, 0x5Au, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_5A_oLIN02_24390d6b_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_5A_oLIN02_6bd5b1d9_Tx] */
  { /*    85 */ PduRConf_PduRDestPdu_MasterReq_FSP3_5B_oLIN02_1fc9c289_Tx  ,                85u,      2u, 0x5Bu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_5B_oLIN02_aab60a88_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_5B_oLIN02_e55ab63a_Tx] */
  { /*    86 */ PduRConf_PduRDestPdu_MasterReq_FSP3_5C_oLIN02_84ba285d_Tx  ,                86u,      2u, 0x5Cu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_5C_oLIN02_661c0a16_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_5C_oLIN02_29f0b6a4_Tx] */
  { /*    87 */ PduRConf_PduRDestPdu_MasterReq_FSP3_5D_oLIN02_f203b0f3_Tx  ,                87u,      2u, 0x5Du, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_5D_oLIN02_6cd9030f_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_5D_oLIN02_2335bfbd_Tx] */
  { /*    88 */ PduRConf_PduRDestPdu_MasterReq_FSP3_5E_oLIN02_69705a27_Tx  ,                88u,      2u, 0x5Eu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_5E_oLIN02_a0730391_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_5E_oLIN02_ef9fbf23_Tx] */
  { /*    89 */ PduRConf_PduRDestPdu_MasterReq_FSP3_5F_oLIN02_1f95631a_Tx  ,                89u,      2u, 0x5Fu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_5F_oLIN02_2efc0472_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_5F_oLIN02_6110b8c0_Tx] */
  { /*    90 */ PduRConf_PduRDestPdu_MasterReq_FSP3_40_oLIN02_7527e7a3_Tx  ,                90u,      2u, 0x40u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_40_oLIN02_53b88826_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_40_oLIN02_1c543494_Tx] */
  { /*    91 */ PduRConf_PduRDestPdu_MasterReq_FSP3_41_oLIN02_ee540d77_Tx  ,                91u,      2u, 0x41u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_41_oLIN02_9f1288b8_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_41_oLIN02_d0fe340a_Tx] */
  { /*    92 */ PduRConf_PduRDestPdu_MasterReq_FSP3_42_oLIN02_98b1344a_Tx  ,                92u,      2u, 0x42u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_42_oLIN02_119d8f5b_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_42_oLIN02_5e7133e9_Tx] */
  { /*    93 */ PduRConf_PduRDestPdu_MasterReq_FSP3_43_oLIN02_03c2de9e_Tx  ,                93u,      2u, 0x43u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_43_oLIN02_dd378fc5_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_43_oLIN02_92db3377_Tx] */
  { /*    94 */ PduRConf_PduRDestPdu_MasterReq_FSP3_44_oLIN02_757b4630_Tx  ,                94u,      2u, 0x44u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_44_oLIN02_d7f286dc_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_44_oLIN02_981e3a6e_Tx] */
  { /*    95 */ PduRConf_PduRDestPdu_MasterReq_FSP3_45_oLIN02_ee08ace4_Tx  ,                95u,      2u, 0x45u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_45_oLIN02_1b588642_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_45_oLIN02_54b43af0_Tx] */
  { /*    96 */ PduRConf_PduRDestPdu_MasterReq_FSP3_46_oLIN02_98ed95d9_Tx  ,                96u,      2u, 0x46u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_46_oLIN02_95d781a1_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_46_oLIN02_da3b3d13_Tx] */
  { /*    97 */ PduRConf_PduRDestPdu_MasterReq_FSP3_47_oLIN02_039e7f0d_Tx  ,                97u,      2u, 0x47u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_47_oLIN02_597d813f_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_47_oLIN02_16913d8d_Tx] */
  { /*    98 */ PduRConf_PduRDestPdu_MasterReq_FSP3_48_oLIN02_759ea485_Tx  ,                98u,      2u, 0x48u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_48_oLIN02_805d9393_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_48_oLIN02_cfb12f21_Tx] */
  { /*    99 */ PduRConf_PduRDestPdu_MasterReq_FSP3_49_oLIN02_eeed4e51_Tx  ,                99u,      2u, 0x49u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_49_oLIN02_4cf7930d_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_49_oLIN02_031b2fbf_Tx] */
    /* Index    UpperLayerPduId                                              AssociatedRxNSduId  CtrlIdx  NAD    Nas    Ncs          Comment                                                                    Referable Keys */
  { /*   100 */ PduRConf_PduRDestPdu_MasterReq_FSP3_50_oLIN02_f4028284_Tx  ,               100u,      2u, 0x50u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_50_oLIN02_44c39c65_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_50_oLIN02_0b2f20d7_Tx] */
  { /*   101 */ PduRConf_PduRDestPdu_MasterReq_FSP3_51_oLIN02_6f716850_Tx  ,               101u,      2u, 0x51u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_51_oLIN02_88699cfb_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_51_oLIN02_c7852049_Tx] */
  { /*   102 */ PduRConf_PduRDestPdu_MasterReq_FSP3_52_oLIN02_1994516d_Tx  ,               102u,      2u, 0x52u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_52_oLIN02_06e69b18_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_52_oLIN02_490a27aa_Tx] */
  { /*   103 */ PduRConf_PduRDestPdu_MasterReq_FSP3_53_oLIN02_82e7bbb9_Tx  ,               103u,      2u, 0x53u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_53_oLIN02_ca4c9b86_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_53_oLIN02_85a02734_Tx] */
  { /*   104 */ PduRConf_PduRDestPdu_MasterReq_FSP3_54_oLIN02_f45e2317_Tx  ,               104u,      2u, 0x54u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_54_oLIN02_c089929f_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_54_oLIN02_8f652e2d_Tx] */
  { /*   105 */ PduRConf_PduRDestPdu_MasterReq_FSP3_55_oLIN02_6f2dc9c3_Tx  ,               105u,      2u, 0x55u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_55_oLIN02_0c239201_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_55_oLIN02_43cf2eb3_Tx] */
  { /*   106 */ PduRConf_PduRDestPdu_MasterReq_FSP3_56_oLIN02_19c8f0fe_Tx  ,               106u,      2u, 0x56u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_56_oLIN02_82ac95e2_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_56_oLIN02_cd402950_Tx] */
  { /*   107 */ PduRConf_PduRDestPdu_MasterReq_FSP3_57_oLIN02_82bb1a2a_Tx  ,               107u,      2u, 0x57u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_57_oLIN02_4e06957c_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_57_oLIN02_01ea29ce_Tx] */
  { /*   108 */ PduRConf_PduRDestPdu_MasterReq_FSP3_58_oLIN02_f4bbc1a2_Tx  ,               108u,      2u, 0x58u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_58_oLIN02_972687d0_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_58_oLIN02_d8ca3b62_Tx] */
  { /*   109 */ PduRConf_PduRDestPdu_MasterReq_FSP3_59_oLIN02_6fc82b76_Tx  ,               109u,      2u, 0x59u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_59_oLIN02_5b8c874e_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_59_oLIN02_14603bfc_Tx] */
  { /*   110 */ PduRConf_PduRDestPdu_MasterReq_FSP3_60_oLIN02_ac1c2bac_Tx  ,               110u,      2u, 0x60u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_60_oLIN02_7d4ea0a0_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_60_oLIN02_32a21c12_Tx] */
  { /*   111 */ PduRConf_PduRDestPdu_MasterReq_FSP3_L2_oLIN01_5d1c964e_Tx  ,               111u,      1u, 0x04u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP3_L2_oLIN01_c3efbf2c_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP3_L2_oLIN01_8c03039e_Tx] */
  { /*   112 */ PduRConf_PduRDestPdu_MasterReq_FSP4_40_oLIN03_748b3d82_Tx  ,               112u,      3u, 0x40u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP4_40_oLIN03_0d84ad33_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP4_40_oLIN03_42681181_Tx] */
  { /*   113 */ PduRConf_PduRDestPdu_MasterReq_FSP4_L2_oLIN01_85dfed68_Tx  ,               113u,      1u, 0x05u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP4_L2_oLIN01_ead4aaaf_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP4_L2_oLIN01_a538161d_Tx] */
  { /*   114 */ PduRConf_PduRDestPdu_MasterReq_FSP5_40_oLIN04_f00bf6c3_Tx  ,               114u,      4u, 0x40u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_FSP5_40_oLIN04_526ee750_Rx]   */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_FSP5_40_oLIN04_1d825be2_Tx] */
  { /*   115 */ PduRConf_PduRDestPdu_MasterReq_ILCP1_oLIN00_0bff65a0_Tx    ,               115u,      0u, 0x0Bu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_ILCP1_oLIN00_1a12ec88_Rx]     */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_ILCP1_oLIN00_57efaae5_Tx] */
  { /*   116 */ PduRConf_PduRDestPdu_MasterReq_ILCP2_oLIN03_cddbb9d5_Tx    ,               116u,      3u, 0x20u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_ILCP2_oLIN03_0d94bad1_Rx]     */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_ILCP2_oLIN03_4069fcbc_Tx] */
  { /*   117 */ PduRConf_PduRDestPdu_MasterReq_LECM2_oLIN00_85a61f49_Tx    ,               117u,      0u, 0x1Du, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_LECM2_oLIN00_2afc07d8_Rx]     */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_LECM2_oLIN00_670141b5_Tx] */
  { /*   118 */ PduRConf_PduRDestPdu_MasterReq_LECMBasic_oLIN00_33465f21_Tx,               118u,      0u, 0x1Eu, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_LECMBasic_oLIN00_029997c6_Rx] */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_LECMBasic_oLIN00_16bdebb7_Tx] */
  { /*   119 */ PduRConf_PduRDestPdu_MasterReq_RCECS_oLIN04_dc3c974a_Tx    ,               119u,      4u, 0x10u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_RCECS_oLIN04_f9903c90_Rx]     */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_RCECS_oLIN04_b46d7afd_Tx] */
  { /*   120 */ PduRConf_PduRDestPdu_MasterReq_TCP_oLIN02_741cea1f_Tx      ,               120u,      2u, 0x39u, 0x15u, 0x1Fu },  /* [LinTpConf_LinTpRxNSdu_SlaveResp_TCP_oLIN02_b3851a34_Rx]       */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_TCP_oLIN02_4b422897_Tx] */
  { /*   121 */ PduRConf_PduRDestPdu_MasterReq_oLIN00_3234fe1b_Tx          ,               255u,      0u, 0x7Fu, 0x15u, 0x1Fu },  /* [No referenced RxNSdu]                                         */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_oLIN00_4a2bb011_Tx] */
  { /*   122 */ PduRConf_PduRDestPdu_MasterReq_oLIN01_eb5b5f1c_Tx          ,               255u,      1u, 0x7Fu, 0x15u, 0x1Fu },  /* [No referenced RxNSdu]                                         */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_oLIN01_3d2c8087_Tx] */
  { /*   123 */ PduRConf_PduRDestPdu_MasterReq_oLIN02_5b9aba54_Tx          ,               255u,      2u, 0x7Fu, 0x15u, 0x1Fu },  /* [No referenced RxNSdu]                                         */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_oLIN02_a425d13d_Tx] */
  { /*   124 */ PduRConf_PduRDestPdu_MasterReq_oLIN03_82f51b53_Tx          ,               255u,      3u, 0x7Fu, 0x15u, 0x1Fu },  /* [No referenced RxNSdu]                                         */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_oLIN03_d322e1ab_Tx] */
  { /*   125 */ PduRConf_PduRDestPdu_MasterReq_oLIN04_e1687685_Tx          ,               255u,      4u, 0x7Fu, 0x15u, 0x1Fu },  /* [No referenced RxNSdu]                                         */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_oLIN04_4d467408_Tx] */
  { /*   126 */ PduRConf_PduRDestPdu_MasterReq_oLIN05_3807d782_Tx          ,               255u,      5u, 0x7Eu, 0x15u, 0x1Fu },  /* [No referenced RxNSdu]                                         */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_oLIN05_3a41449e_Tx] */
  { /*   127 */ PduRConf_PduRDestPdu_MasterReq_oLIN06_88c632ca_Tx          ,               255u,      6u, 0x7Eu, 0x15u, 0x1Fu },  /* [No referenced RxNSdu]                                         */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_oLIN06_a3481524_Tx] */
  { /*   128 */ PduRConf_PduRDestPdu_MasterReq_oLIN07_51a993cd_Tx          ,               255u,      7u, 0x7Eu, 0x15u, 0x1Fu }   /* [No referenced RxNSdu]                                         */  /* [/ActiveEcuC/LinTp/LinTpGlobalConfig/MasterReq_oLIN07_d44f25b2_Tx] */
};
#define LINTP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinTp_Ctrl
**********************************************************************************************************************/
#define LINTP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(LinTp_CtrlUType, LINTP_VAR_NOINIT) LinTp_Ctrl;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */  /* Data structure per LinIf channel */
  /* Index        Referable Keys */
  /*     0 */  /* [CHNL_45618847] */
  /*     1 */  /* [CHNL_8e3d5be2] */
  /*     2 */  /* [CHNL_08a9294c] */
  /*     3 */  /* [CHNL_c3f5fae9] */
  /*     4 */  /* [CHNL_def0ca51] */
  /*     5 */  /* [CHNL_15ac19f4] */
  /*     6 */  /* [CHNL_93386b5a] */
  /*     7 */  /* [CHNL_5864b8ff] */

#define LINTP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA
**********************************************************************************************************************/





