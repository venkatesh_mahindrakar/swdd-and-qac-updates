/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: RamTst
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: RamTst_PrivateCfg.h
 *   Generation Time: 2020-08-20 13:43:06
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  MISRA DEVIATIONS
 * -------------------------------------------------------------------------------------------------------------------
 * MD_RamTst_3453:
 *   Reason:
 *   Risk:       No type checking in function like macros
 *   Prevention: Component test
 *********************************************************************************************************************/

#if !defined(RAMTST_PRIVATE_CFG_H)
#define RAMTST_PRIVATE_CFG_H


/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/



/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/



/**********************************************************************************************************************
 *  VERSION IDENTIFICATION
 *********************************************************************************************************************/

#define RAMTST_PRIVATE_CFG_MAJOR_VERSION	(4u) 
#define RAMTST_PRIVATE_CFG_MINOR_VERSION	(00u) 
#define RAMTST_PRIVATE_CFG_PATCH_VERSION	(00u) 


#ifndef RAMTST_PRIVATE_CFG_INCLUDE
  #error "RamTst_PrivateCfg.h must not be included outside of RamTst scope"
#endif


/* ----- Include file for development error reporting --------------------- */
/* Hint: 
 * To include a DET-Header-File both switches 'RamTstDevErrorDetect' and 'RamTstDevErrorReporting' must be enabled 
 */ 


/* ----- Development error reporting function ----------------------------- */
/* Hint: 
 * No Development Mode enabled 
 */ 


/* ----- Include file for production error reporting ---------------------- */
#include "Dem.h"

/* ----- DEM Error Codes -------------------------------------------------- */
#if(defined DEM_AR_RELEASE_MAJOR_VERSION && DEM_AR_RELEASE_MAJOR_VERSION == (0x04))
  #if(DEM_AR_RELEASE_MINOR_VERSION == 0 && DEM_AR_RELEASE_REVISION_VERSION < (0x03))
    /* Prefixing for AUTOSAR versions 4.0 Rev 1 and 4.0 Rev 2 */
    #define RAMTST_E_RAM_FAILURE                    (Dem_AutoCreatedDemEvent_RamTstDemEventParameterRefs_RAMTST_E_RAM_FAILURE)
  #else
    /* Prefixing for AUTOSAR versions 4.0 Rev 3 and later */
    #define RAMTST_E_RAM_FAILURE                    (DemConf_DemEventParameter_AutoCreatedDemEvent_RamTstDemEventParameterRefs_RAMTST_E_RAM_FAILURE)
  #endif
#else
  /* Prefixing for AUTOSAR version 3.2 Rev 1 */
  #define RAMTST_E_RAM_FAILURE                      (Dem_RAMTST_E_RAM_FAILURE)
#endif
/* ----- Production error reporting function ------------------------------ */
#define RamTst_DemReportError(EventId, EventStatus)\
  ( (void) Dem_ReportErrorStatus(EventId, EventStatus)) /* PRQA S 3453 */ /* MD_RamTst_3453 */


/* ----- Include file(s) for interrupt locking ---------------------------- */
#include "SchM_RamTst.h"


/* ----- Interrupt locking macros ----------------------------------------- */
#define RamTst_EnterCritical()  SchM_Enter_RamTst_RAMTST_EXCLUSIVE_AREA_0()
#define RamTst_ExitCritical()   SchM_Exit_RamTst_RAMTST_EXCLUSIVE_AREA_0()


#endif  /* RAMTST__PRIVATE_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: RamTst_PrivateCfg.h
 *********************************************************************************************************************/

