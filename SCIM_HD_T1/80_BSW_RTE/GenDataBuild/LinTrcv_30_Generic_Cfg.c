/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: LinTrcv
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: LinTrcv_30_Generic_Cfg.c
 *   Generation Time: 2020-08-20 13:43:07
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/

 
 
 /**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/
/* Disable the following MISRA warnings because they would appear too often for individual suppression.
   See justification at the end of file */

/* PRQA S 0850 EOF */ /* MD_MSR_19.8 */


/* -----------------------------------------------------------------------------
    &&&~ Includes
 ----------------------------------------------------------------------------- */

#include "LinTrcv_30_Generic.h"


/* -----------------------------------------------------------------------------
    &&&~ VersionDefines
 ----------------------------------------------------------------------------- */
#if (DRVTRANS_LIN_30_GENERIC_GENTOOL_CFG5_MAJOR_VERSION != 0x06u)
# error "LinTrcv_30_Generic_Cfg.c : Incompatible DRVTRANS_30_GENERIC_GENTOOL_CFG5_MAJOR_VERSION in generated File!"
#endif

#if (DRVTRANS_LIN_30_GENERIC_GENTOOL_CFG5_MINOR_VERSION != 0x01u)
# error "LinTrcv_30_Generic_Cfg.c : Incompatible DRVTRANS_30_GENERIC_GENTOOL_CFG5_MINOR_VERSIONin generated File!"
#endif

#if (DRVTRANS_LIN_30_GENERIC_GENTOOL_CFG5_PATCH_VERSION != 0x01u)
# error "LinTrcv_30_Generic_Cfg.c : Incompatible DRVTRANS_30_GENERIC_GENTOOL_CFG5_PATCH_VERSION in generated File!"
#endif

/**********************************************************************************************************************
  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/



/**********************************************************************************************************************
  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA
**********************************************************************************************************************/



/**********************************************************************************************************************
  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  LinTrcv_30_Generic_Channel
**********************************************************************************************************************/
/** 
  \var    LinTrcv_30_Generic_Channel
  \details
  Element        Description
  ChannelUsed
  InitState  
*/ 
#define LINTRCV_30_GENERIC_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(LinTrcv_30_Generic_ChannelType, LINTRCV_30_GENERIC_CONST) LinTrcv_30_Generic_Channel[5] = {
    /* Index    ChannelUsed  InitState               */
  { /*     0 */        TRUE, LINTRCV_TRCV_MODE_SLEEP },
  { /*     1 */        TRUE, LINTRCV_TRCV_MODE_SLEEP },
  { /*     2 */        TRUE, LINTRCV_TRCV_MODE_SLEEP },
  { /*     3 */        TRUE, LINTRCV_TRCV_MODE_SLEEP },
  { /*     4 */        TRUE, LINTRCV_TRCV_MODE_SLEEP }
};
#define LINTRCV_30_GENERIC_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  LinTrcv_30_Generic_DioChannel
**********************************************************************************************************************/
/** 
  \var    LinTrcv_30_Generic_DioChannel
  \details
  Element     Description
  LinDioEn
*/ 
#define LINTRCV_30_GENERIC_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(LinTrcv_30_Generic_DioChannelType, LINTRCV_30_GENERIC_CONST) LinTrcv_30_Generic_DioChannel[5] = {
    /* Index    LinDioEn                   */
  { /*     0 */ DioConf_DioChannel_LIN1_EN },
  { /*     1 */ DioConf_DioChannel_LIN2_EN },
  { /*     2 */ DioConf_DioChannel_LIN3_EN },
  { /*     3 */ DioConf_DioChannel_LIN4_EN },
  { /*     4 */ DioConf_DioChannel_LIN5_EN }
};
#define LINTRCV_30_GENERIC_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA
**********************************************************************************************************************/


/* -----------------------------------------------------------------------------
    &&&~ Constants
 ----------------------------------------------------------------------------- */

#define LINTRCV_30_GENERIC_START_SEC_CONST_8BIT
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */

CONST(uint8, LINTRCV_30_GENERIC_CONST) LinTrcv_30_Generic_NrOfChannels = LINTRCV_30_GENERIC_NROFCHANNELS;
CONST(uint8, LINTRCV_30_GENERIC_CONST) LinTrcv_30_Generic_InstanceId = LINTRCV_30_GENERIC_INSTANCE_ID;

#define LINTRCV_30_GENERIC_STOP_SEC_CONST_8BIT
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define LINTRCV_30_GENERIC_START_SEC_CONST_32BIT
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */

CONST(uint32, LINTRCV_30_GENERIC_CONST) LinTrcv_30_Generic_WaitCount = LINTRCV_30_GENERIC_WAIT_COUNT;

#define LINTRCV_30_GENERIC_STOP_SEC_CONST_32BIT
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* -----------------------------------------------------------------------------
    &&&~ Variables
 ----------------------------------------------------------------------------- */

#define LINTRCV_30_GENERIC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */

VAR(LinTrcv_30_Generic_ChannelDataType, LINTRCV_30_GENERIC_VAR_NOINIT) LinTrcv_30_Generic_ChannelData[LINTRCV_30_GENERIC_NROFCHANNELS];

#define LINTRCV_30_GENERIC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */












