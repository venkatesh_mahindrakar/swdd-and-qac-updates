/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Rtm
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Rtm_Cfg.c
 *   Generation Time: 2020-08-20 13:43:07
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/

/* PRQA S 857 EOF */ /* MD_MSR_1.1_857 */
#include "Rtm.h"

/*lint -e546 */ /* Suppress ID546 because & is required for function pointer access */

#if (RTM_TIME_MEASUREMENT == STD_ON)
# define RTM_START_SEC_VAR_INIT_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_19.1 */

VAR(sint8, RTM_VAR_INIT) Rtm_MeasurementNestingCtr[RTM_MEAS_SECTION_COUNT] = 
{
  0x00 /* Measurement Id: 0 */, 
  0x00 /* Measurement Id: 1 */, 
  0x00 /* Measurement Id: 36 */
};

VAR(uint8, RTM_VAR_INIT) Rtm_Ctrl[RTM_CTRL_VECTOR_LEN] = 
{
  0x00u
};

VAR(uint8, RTM_VAR_INIT) Rtm_MeasurementConfig[RTM_CTRL_VECTOR_LEN] = 
{
  0x00
};

VAR(Rtm_MeasurementTimestampType, RTM_VAR_INIT) Rtm_StartTimeStamps[RTM_MEAS_SECTION_COUNT] = 
{
  (Rtm_MeasurementTimestampType) 0x00 /* Measurement Id: 0 */, 
  (Rtm_MeasurementTimestampType) 0x00 /* Measurement Id: 1 */, 
  (Rtm_MeasurementTimestampType) 0x00 /* Measurement Id: 36 */
};

VAR(Rtm_DataSet, RTM_VAR_INIT) Rtm_Results[RTM_MEAS_SECTION_COUNT] = 
{
  {0u, 0u, (Rtm_MeasurementTimestampType) 0xFFFFFFFFu, 0u} /* Measurement Id: 0, Measurement Name: Rtm_Overhead_RespTime_EnableISRs */, 
  {0u, 0u, (Rtm_MeasurementTimestampType) 0xFFFFFFFFu, 0u} /* Measurement Id: 1, Measurement Name: Rtm_Overhead_RespTime_DisableISRs */, 
  {0u, 0u, (Rtm_MeasurementTimestampType) 0xFFFFFFFFu, 0u} /* Measurement Id: 36, Measurement Name: Rtm_CpuLoadMeasurement */
};


# define RTM_STOP_SEC_VAR_INIT_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_19.1 */

# define RTM_START_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_19.1 */

CONST(uint16, RTM_CONST) Rtm_ThresholdTimes[RTM_MEAS_SECTION_COUNT] = 
{
  0x00000000uL /* Measurement Id: 0 */, 
  0x00000000uL /* Measurement Id: 1 */, 
  0x00000000uL /* Measurement Id: 36 */
};

CONST(Rtm_ThresholdCbkFctType, RTM_CONST) Rtm_ThresholdCbkFctArr[RTM_MEAS_SECTION_COUNT] =
{
  NULL_PTR /* Measurement Id: 0 */, 
  NULL_PTR /* Measurement Id: 1 */, 
  NULL_PTR /* Measurement Id: 36 */
};

CONST(uint8, RTM_CONST) Rtm_CtrlConfig[RTM_CTRL_VECTOR_LEN] = 
{
  0x00u
};


# define RTM_STOP_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_19.1 */




#endif /* RTM_TIME_MEASUREMENT == STD_ON */


