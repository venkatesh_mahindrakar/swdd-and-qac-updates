/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Cfg.h
 *   Generation Time: 2020-09-28 09:11:21
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/

#ifndef OS_CFG_H
# define OS_CFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */

/* Os module declarations */

/* Os kernel module dependencies */

/* Os hal dependencies */
# include "Os_Hal_Cfg.h"


/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/*! Configuration major version identification. */
# define OS_CFG_MAJOR_VERSION                    (2u)

/*! Configuration minor version identification. */
# define OS_CFG_MINOR_VERSION                    (33u)

/*! Defines which platform is used. */
# define OS_CFG_PLATFORM_POWERPC     (STD_ON)

/*! Defines which derivative group is configured. */
# define OS_CFG_DERIVATIVEGROUP_MPC574XC1     (STD_ON)

/*! Defines which derivative is configured. */
# define OS_CFG_DERIVATIVE_MPC5746C     (STD_ON)

/*! Defines which compiler is configured. */
# define OS_CFG_COMPILER_DIAB     (STD_ON)

/*! Defines whether access macros to get context related information in the error hook are enabled (STD_ON) or not (STD_OFF). */
# define OS_CFG_ERR_PARAMETERACCESS              (STD_OFF)

/*! Defines whether access macros to get service ID information in the error hook are enabled (STD_ON) or not (STD_OFF). */
# define OS_CFG_ERR_GETSERVICEID                 (STD_OFF)

/*! Defines whether the pre-task hook is active (STD_ON) or not (STD_OFF). */
# define OS_CFG_PRETASKHOOK                      (STD_OFF)

/*! Defines whether the post-task hook is active (STD_ON) or not (STD_OFF). */
# define OS_CFG_POSTTASKHOOK                     (STD_OFF)

/*! Defines whether the OS shall call the panic hook (STD_ON) or not (STD_OFF). */
# define OS_CFG_PANICHOOK                        (STD_OFF)

/*! Defines whether the system startup hook is configured (STD_ON) or not (STD_OFF). */
# define OS_CFG_STARTUPHOOK_SYSTEM               (STD_OFF)

/*! Defines whether the system shutdown hook is configured (STD_ON) or not (STD_OFF). */
# define OS_CFG_SHUTDOWNHOOK_SYSTEM              (STD_ON)

/*! Defines whether the system error hook is configured (STD_ON) or not (STD_OFF). */
# define OS_CFG_ERRORHOOK_SYSTEM                 (STD_ON)

/*! Defines whether the system protection hook is configured (STD_ON) or not (STD_OFF). */
# define OS_CFG_PROTECTIONHOOK_SYSTEM            (STD_OFF)

/*! Defines whether backward compatibility defines are needed (STD_ON) or not (STD_OFF). */
# define OS_CFG_PERIPHERAL_COMPATIBILITY         (STD_OFF)

/* OS application modes */
# define DONOTCARE     ((AppModeType)0)
# define OS_APPMODE_NONE     ((AppModeType)0)
# define OSDEFAULTAPPMODE     ((AppModeType)1)
# define OS_APPMODE_ANY     ((AppModeType)255)

/*! Defines whether EVENT is active (STD_ON) or not (STD_OFF). */
# define OS_CFG_EVENT                            (STD_ON)

/* Event masks */
# define Rte_Ev_Cyclic2_BSW_Diag_Task_0_10ms     ((EventMaskType)1uLL)
# define Rte_Ev_Cyclic2_BSW_Diag_Task_0_5ms     ((EventMaskType)2uLL)
# define Rte_Ev_Cyclic_ASW_10ms_Task_0_10ms     ((EventMaskType)1uLL)
# define Rte_Ev_Cyclic_ASW_20ms_Task_0_100ms     ((EventMaskType)1uLL)
# define Rte_Ev_Cyclic_ASW_20ms_Task_0_20ms     ((EventMaskType)2uLL)
# define Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif     ((EventMaskType)1uLL)
# define Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif     ((EventMaskType)2uLL)
# define Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif     ((EventMaskType)4uLL)
# define Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif     ((EventMaskType)8uLL)
# define Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif     ((EventMaskType)16uLL)
# define Rte_Ev_Run1_DiagnosticComponent_DiagnosticComponent_Dcm_ActivateIss     ((EventMaskType)32uLL)
# define Rte_Ev_Run1_DiagnosticComponent_DiagnosticComponent_Dcm_DeactivateIss     ((EventMaskType)64uLL)
# define Rte_Ev_Run1_LINMgr_SCIM_LINMgr_LIN6SchEndNotif     ((EventMaskType)128uLL)
# define Rte_Ev_Run1_LINMgr_SCIM_LINMgr_LIN7SchEndNotif     ((EventMaskType)256uLL)
# define Rte_Ev_Run1_LINMgr_SCIM_LINMgr_LIN8SchEndNotif     ((EventMaskType)512uLL)
# define Rte_Ev_Run_Application_Data_NVM_Application_Data_NVM_1s_runnable     ((EventMaskType)1024uLL)
# define Rte_Ev_Run_BswM_BswM_Read_LIN1_ScheduleTableRequestMode     ((EventMaskType)1uLL)
# define Rte_Ev_Run_BswM_BswM_Read_LIN2_ScheduleTableRequestMode     ((EventMaskType)2uLL)
# define Rte_Ev_Run_BswM_BswM_Read_LIN3_ScheduleTableRequestMode     ((EventMaskType)4uLL)
# define Rte_Ev_Run_BswM_BswM_Read_LIN4_ScheduleTableRequestMode     ((EventMaskType)8uLL)
# define Rte_Ev_Run_BswM_BswM_Read_LIN5_ScheduleTableRequestMode     ((EventMaskType)16uLL)
# define Rte_Ev_Run_BswM_BswM_Read_LIN6_ScheduleTableRequestMode     ((EventMaskType)32uLL)
# define Rte_Ev_Run_BswM_BswM_Read_LIN7_ScheduleTableRequestMode     ((EventMaskType)64uLL)
# define Rte_Ev_Run_BswM_BswM_Read_LIN8_ScheduleTableRequestMode     ((EventMaskType)128uLL)
# define Rte_Ev_Run_Calibration_Data_NVM_FspNVDataRxEventRunnable     ((EventMaskType)2048uLL)
# define Rte_Ev_Run_CryptoDriverDoorLatch_Tx_VEC_CryptoProxySenderConfirmation     ((EventMaskType)4096uLL)
# define Rte_Ev_Run_CryptoDriverDoorLatch_Tx_VEC_CryptoProxySenderReception     ((EventMaskType)8192uLL)
# define Rte_Ev_Run_CryptoDriverKeyCyl_Rx_VEC_CryptoProxyReceiverReception     ((EventMaskType)16384uLL)
# define Rte_Ev_Run_CryptoEngineStart_Tx_VEC_CryptoProxySenderConfirmation     ((EventMaskType)32768uLL)
# define Rte_Ev_Run_CryptoEngineStart_Tx_VEC_CryptoProxySenderReception     ((EventMaskType)65536uLL)
# define Rte_Ev_Run_CryptoGearboxLock_Tx_VEC_CryptoProxySenderConfirmation     ((EventMaskType)131072uLL)
# define Rte_Ev_Run_CryptoGearboxLock_Tx_VEC_CryptoProxySenderReception     ((EventMaskType)262144uLL)
# define Rte_Ev_Run_CryptoKeyAuth_Tx_VEC_CryptoProxySenderConfirmation     ((EventMaskType)524288uLL)
# define Rte_Ev_Run_CryptoKeyAuth_Tx_VEC_CryptoProxySenderReception     ((EventMaskType)1048576uLL)
# define Rte_Ev_Run_CryptoLockingSwitch_Rx_VEC_CryptoProxyReceiverReception     ((EventMaskType)2097152uLL)
# define Rte_Ev_Run_CryptoLuggageCompartment_Tx_VEC_CryptoProxySenderConfirmation     ((EventMaskType)4194304uLL)
# define Rte_Ev_Run_CryptoLuggageCompartment_Tx_VEC_CryptoProxySenderReception     ((EventMaskType)8388608uLL)
# define Rte_Ev_Run_CryptoPsngDoorLatch_Tx_VEC_CryptoProxySenderConfirmation     ((EventMaskType)16777216uLL)
# define Rte_Ev_Run_CryptoPsngDoorLatch_Tx_VEC_CryptoProxySenderReception     ((EventMaskType)33554432uLL)
# define Rte_Ev_Run_CryptoPsngKeyCyl_Rx_VEC_CryptoProxyReceiverReception     ((EventMaskType)67108864uLL)
# define Rte_Ev_Run_CryptoReducedSetMode_Tx_VEC_CryptoProxySenderConfirmation     ((EventMaskType)134217728uLL)
# define Rte_Ev_Run_CryptoReducedSetMode_Tx_VEC_CryptoProxySenderReception     ((EventMaskType)268435456uLL)
# define Rte_Ev_Run_CryptoTheftAlarmActivation_Tx_VEC_CryptoProxySenderConfirmation     ((EventMaskType)536870912uLL)
# define Rte_Ev_Run_CryptoTheftAlarmActivation_Tx_VEC_CryptoProxySenderReception     ((EventMaskType)1073741824uLL)
# define Rte_Ev_Run_DiagnosticComponent_LocalTimeDistribution_200ms_Runnable     ((EventMaskType)4uLL)
# define Rte_Ev_Run_Keyfob_Radio_Com_NVM_KeyFobNVDataRxEventRunnable     ((EventMaskType)2147483648uLL)
# define Rte_Ev_Run_SCIM_Manager_SCIM_Manager_EnterRun     ((EventMaskType)2uLL)

/* Software counter timing macros */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/


#endif /* OS_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_Cfg.h
 *********************************************************************************************************************/

