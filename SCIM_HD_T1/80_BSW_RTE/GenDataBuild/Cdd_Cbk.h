/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Cdd
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Cdd_Cbk.h
 *   Generation Time: 2020-08-20 13:43:06
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/



#if !defined (CDD_CBK_H)
# define CDD_CBK_H

/**********************************************************************************************************************
  INCLUDES
**********************************************************************************************************************/
# include "ComStack_Types.h"



/**********************************************************************************************************************
  GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
#ifndef CDD_USE_DUMMY_STATEMENT
#define CDD_USE_DUMMY_STATEMENT STD_OFF /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef CDD_DUMMY_STATEMENT
#define CDD_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef CDD_DUMMY_STATEMENT_CONST
#define CDD_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef CDD_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define CDD_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef CDD_ATOMIC_VARIABLE_ACCESS
#define CDD_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef CDD_PROCESSOR_MPC5746C
#define CDD_PROCESSOR_MPC5746C
#endif
#ifndef CDD_COMP_DIAB
#define CDD_COMP_DIAB
#endif
#ifndef CDD_GEN_GENERATOR_MSR
#define CDD_GEN_GENERATOR_MSR
#endif
#ifndef CDD_CPUTYPE_BITORDER_MSB2LSB
#define CDD_CPUTYPE_BITORDER_MSB2LSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef CDD_CONFIGURATION_VARIANT_PRECOMPILE
#define CDD_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef CDD_CONFIGURATION_VARIANT_LINKTIME
#define CDD_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef CDD_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define CDD_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef CDD_CONFIGURATION_VARIANT
#define CDD_CONFIGURATION_VARIANT CDD_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef CDD_POSTBUILD_VARIANT_SUPPORT
#define CDD_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif


#define CDD_COMIF  STD_OFF
#define CDD_COMIF_RX  STD_OFF
#define CDD_COMIF_TX  STD_OFF
#define CDD_COMIF_TRIGGERTRANSMIT  STD_OFF

#define CDD_PDUR_UL_COMIF  STD_OFF
#define CDD_PDUR_UL_COMIF_TRIGGERTRANSMIT  STD_OFF

#define CDD_PDUR_UL_COMTP  STD_ON

#define CDD_PDUR_LL_COMIF  STD_OFF
#define CDD_PDUR_LL_COMIF_TRIGGERTRANSMIT  STD_OFF

#define CDD_PDUR_LL_COMTP  STD_OFF

#define CDD_SOADUL_COMIF_RX  STD_OFF
#define CDD_SOADUL_COMIF_TRIGGERTRANSMIT  STD_OFF
#define CDD_SOADUL_COMIF_TXCONFIRMATION   STD_OFF

#define CDD_SOADUL_COMTP_RX  STD_OFF
#define CDD_SOADUL_COMTP_TX  STD_OFF





/**
 * \defgroup CddHandleIdsPduRUpperLayerRx Handle IDs of handle space PduRUpperLayerRx.
 * \brief CddPduRUpperLayerContribution Rx PDUs
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_CCFW_oLIN03_d7124ce9_Rx 0
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_DLFW_oLIN03_0f2202e7_Rx 1
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_ELCP1_oLIN03_8611f0b3_Rx 2
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_ELCP2_oLIN03_089ef750_Rx 3
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_4A_oLIN00_8520c1c5_Rx 20
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_4B_oLIN00_0bafc626_Rx 21
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_4C_oLIN00_c705c6b8_Rx 22
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_4D_oLIN00_cdc0cfa1_Rx 23
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_4E_oLIN00_016acf3f_Rx 24
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_4F_oLIN00_8fe5c8dc_Rx 25
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_5A_oLIN00_925bd586_Rx 26
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_5B_oLIN00_1cd4d265_Rx 27
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_5C_oLIN00_d07ed2fb_Rx 28
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_5D_oLIN00_dabbdbe2_Rx 29
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_5E_oLIN00_1611db7c_Rx 30
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_5F_oLIN00_989edc9f_Rx 31
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_40_oLIN00_e5da50cb_Rx 32
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_41_oLIN00_29705055_Rx 33
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_42_oLIN00_a7ff57b6_Rx 34
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_43_oLIN00_6b555728_Rx 35
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_44_oLIN00_61905e31_Rx 36
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_45_oLIN00_ad3a5eaf_Rx 37
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_46_oLIN00_23b5594c_Rx 38
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_47_oLIN00_ef1f59d2_Rx 39
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_48_oLIN00_363f4b7e_Rx 40
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_49_oLIN00_fa954be0_Rx 41
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_50_oLIN00_f2a14488_Rx 42
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_51_oLIN00_3e0b4416_Rx 43
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_52_oLIN00_b08443f5_Rx 44
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_53_oLIN00_7c2e436b_Rx 45
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_54_oLIN00_76eb4a72_Rx 46
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_55_oLIN00_ba414aec_Rx 47
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_56_oLIN00_34ce4d0f_Rx 48
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_57_oLIN00_f8644d91_Rx 49
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_58_oLIN00_21445f3d_Rx 50
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_59_oLIN00_edee5fa3_Rx 51
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_60_oLIN00_cb2c784d_Rx 52
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_L1_oLIN00_620b3198_Rx 10
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_L2_oLIN01_9b8306ed_Rx 11
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_L3_oLIN02_ce2057c9_Rx 12
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_L4_oLIN03_b3e26e46_Rx 13
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP1_L5_oLIN04_e12cfb7b_Rx 14
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_4A_oLIN01_6bc59752_Rx 53
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_4B_oLIN01_e54a90b1_Rx 54
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_4C_oLIN01_29e0902f_Rx 55
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_4D_oLIN01_23259936_Rx 56
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_4E_oLIN01_ef8f99a8_Rx 57
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_4F_oLIN01_61009e4b_Rx 58
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_5A_oLIN01_7cbe8311_Rx 59
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_5B_oLIN01_f23184f2_Rx 60
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_5C_oLIN01_3e9b846c_Rx 61
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_5D_oLIN01_345e8d75_Rx 62
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_5E_oLIN01_f8f48deb_Rx 63
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_5F_oLIN01_767b8a08_Rx 64
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_40_oLIN01_0b3f065c_Rx 65
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_41_oLIN01_c79506c2_Rx 66
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_42_oLIN01_491a0121_Rx 67
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_43_oLIN01_85b001bf_Rx 68
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_44_oLIN01_8f7508a6_Rx 69
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_45_oLIN01_43df0838_Rx 70
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_46_oLIN01_cd500fdb_Rx 71
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_47_oLIN01_01fa0f45_Rx 72
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_48_oLIN01_d8da1de9_Rx 73
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_49_oLIN01_14701d77_Rx 74
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_50_oLIN01_1c44121f_Rx 75
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_51_oLIN01_d0ee1281_Rx 76
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_52_oLIN01_5e611562_Rx 77
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_53_oLIN01_92cb15fc_Rx 78
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_54_oLIN01_980e1ce5_Rx 79
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_55_oLIN01_54a41c7b_Rx 80
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_56_oLIN01_da2b1b98_Rx 81
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_57_oLIN01_16811b06_Rx 82
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_58_oLIN01_cfa109aa_Rx 83
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_59_oLIN01_030b0934_Rx 84
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_60_oLIN01_25c92eda_Rx 85
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_L1_oLIN00_fbe95799_Rx 15
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_L2_oLIN01_026160ec_Rx 16
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP2_L3_oLIN02_57c231c8_Rx 17
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_4A_oLIN02_33421928_Rx 86
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_4B_oLIN02_bdcd1ecb_Rx 87
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_4C_oLIN02_71671e55_Rx 88
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_4D_oLIN02_7ba2174c_Rx 89
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_4E_oLIN02_b70817d2_Rx 90
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_4F_oLIN02_39871031_Rx 91
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_5A_oLIN02_24390d6b_Rx 92
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_5B_oLIN02_aab60a88_Rx 93
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_5C_oLIN02_661c0a16_Rx 94
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_5D_oLIN02_6cd9030f_Rx 95
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_5E_oLIN02_a0730391_Rx 96
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_5F_oLIN02_2efc0472_Rx 97
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_40_oLIN02_53b88826_Rx 98
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_41_oLIN02_9f1288b8_Rx 99
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_42_oLIN02_119d8f5b_Rx 100
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_43_oLIN02_dd378fc5_Rx 101
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_44_oLIN02_d7f286dc_Rx 102
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_45_oLIN02_1b588642_Rx 103
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_46_oLIN02_95d781a1_Rx 104
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_47_oLIN02_597d813f_Rx 105
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_48_oLIN02_805d9393_Rx 106
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_49_oLIN02_4cf7930d_Rx 107
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_50_oLIN02_44c39c65_Rx 108
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_51_oLIN02_88699cfb_Rx 109
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_52_oLIN02_06e69b18_Rx 110
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_53_oLIN02_ca4c9b86_Rx 111
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_54_oLIN02_c089929f_Rx 112
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_55_oLIN02_0c239201_Rx 113
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_56_oLIN02_82ac95e2_Rx 114
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_57_oLIN02_4e06957c_Rx 115
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_58_oLIN02_972687d0_Rx 116
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_59_oLIN02_5b8c874e_Rx 117
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_60_oLIN02_7d4ea0a0_Rx 118
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP3_L2_oLIN01_c3efbf2c_Rx 18
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP4_40_oLIN03_0d84ad33_Rx 119
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP4_L2_oLIN01_ead4aaaf_Rx 19
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_FSP5_40_oLIN04_526ee750_Rx 120
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_ILCP1_oLIN00_1a12ec88_Rx 4
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_ILCP2_oLIN03_0d94bad1_Rx 5
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_LECM2_oLIN00_2afc07d8_Rx 6
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_LECMBasic_oLIN00_029997c6_Rx 7
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_LinSlave_L8_oLIN05_ae44c72a_Rx 121
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_LinSlave_L8_oLIN06_374d9690_Rx 122
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_LinSlave_L8_oLIN07_404aa606_Rx 123
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_RCECS_oLIN04_f9903c90_Rx 8
#define CddConf_CddPduRUpperLayerRxPdu_SlaveResp_TCP_oLIN02_b3851a34_Rx 9
/**\} */

/**
 * \defgroup CddHandleIdsPduRUpperLayerTx Handle IDs of handle space PduRUpperLayerTx.
 * \brief CddPduRUpperLayerContribution Tx PDUs
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_CCFW_oLIN03_98357989_Tx 0
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_DLFW_oLIN03_40053787_Tx 1
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_ELCP1_oLIN03_cbecb6de_Tx 2
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_ELCP2_oLIN03_4563b13d_Tx 3
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4A_oLIN00_cacc7d77_Tx 25
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4B_oLIN00_44437a94_Tx 26
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4C_oLIN00_88e97a0a_Tx 27
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4D_oLIN00_822c7313_Tx 28
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4E_oLIN00_4e86738d_Tx 29
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_4F_oLIN00_c009746e_Tx 30
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5A_oLIN00_ddb76934_Tx 31
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5B_oLIN00_53386ed7_Tx 32
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5C_oLIN00_9f926e49_Tx 33
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5D_oLIN00_95576750_Tx 34
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5E_oLIN00_59fd67ce_Tx 35
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_5F_oLIN00_d772602d_Tx 36
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_40_oLIN00_aa36ec79_Tx 37
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_41_oLIN00_669cece7_Tx 38
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_42_oLIN00_e813eb04_Tx 39
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_43_oLIN00_24b9eb9a_Tx 40
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_44_oLIN00_2e7ce283_Tx 41
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_45_oLIN00_e2d6e21d_Tx 42
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_46_oLIN00_6c59e5fe_Tx 43
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_47_oLIN00_a0f3e560_Tx 44
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_48_oLIN00_79d3f7cc_Tx 45
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_49_oLIN00_b579f752_Tx 46
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_50_oLIN00_bd4df83a_Tx 47
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_51_oLIN00_71e7f8a4_Tx 48
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_52_oLIN00_ff68ff47_Tx 49
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_53_oLIN00_33c2ffd9_Tx 50
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_54_oLIN00_3907f6c0_Tx 51
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_55_oLIN00_f5adf65e_Tx 52
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_56_oLIN00_7b22f1bd_Tx 53
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_57_oLIN00_b788f123_Tx 54
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_58_oLIN00_6ea8e38f_Tx 55
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_59_oLIN00_a202e311_Tx 56
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_60_oLIN00_84c0c4ff_Tx 57
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L1_oLIN00_2de78d2a_Tx 10
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L2_oLIN01_d46fba5f_Tx 11
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L3_oLIN02_81cceb7b_Tx 12
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L4_oLIN03_fc0ed2f4_Tx 13
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP1_L5_oLIN04_aec047c9_Tx 14
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4A_oLIN01_24292be0_Tx 58
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4B_oLIN01_aaa62c03_Tx 59
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4C_oLIN01_660c2c9d_Tx 60
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4D_oLIN01_6cc92584_Tx 61
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4E_oLIN01_a063251a_Tx 62
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_4F_oLIN01_2eec22f9_Tx 63
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5A_oLIN01_33523fa3_Tx 64
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5B_oLIN01_bddd3840_Tx 65
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5C_oLIN01_717738de_Tx 66
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5D_oLIN01_7bb231c7_Tx 67
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5E_oLIN01_b7183159_Tx 68
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_5F_oLIN01_399736ba_Tx 69
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_40_oLIN01_44d3baee_Tx 70
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_41_oLIN01_8879ba70_Tx 71
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_42_oLIN01_06f6bd93_Tx 72
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_43_oLIN01_ca5cbd0d_Tx 73
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_44_oLIN01_c099b414_Tx 74
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_45_oLIN01_0c33b48a_Tx 75
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_46_oLIN01_82bcb369_Tx 76
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_47_oLIN01_4e16b3f7_Tx 77
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_48_oLIN01_9736a15b_Tx 78
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_49_oLIN01_5b9ca1c5_Tx 79
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_50_oLIN01_53a8aead_Tx 80
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_51_oLIN01_9f02ae33_Tx 81
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_52_oLIN01_118da9d0_Tx 82
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_53_oLIN01_dd27a94e_Tx 83
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_54_oLIN01_d7e2a057_Tx 84
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_55_oLIN01_1b48a0c9_Tx 85
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_56_oLIN01_95c7a72a_Tx 86
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_57_oLIN01_596da7b4_Tx 87
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_58_oLIN01_804db518_Tx 88
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_59_oLIN01_4ce7b586_Tx 89
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_60_oLIN01_6a259268_Tx 90
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L1_oLIN00_b405eb2b_Tx 15
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L2_oLIN01_4d8ddc5e_Tx 16
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP2_L3_oLIN02_182e8d7a_Tx 17
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4A_oLIN02_7caea59a_Tx 91
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4B_oLIN02_f221a279_Tx 92
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4C_oLIN02_3e8ba2e7_Tx 93
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4D_oLIN02_344eabfe_Tx 94
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4E_oLIN02_f8e4ab60_Tx 95
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_4F_oLIN02_766bac83_Tx 96
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5A_oLIN02_6bd5b1d9_Tx 97
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5B_oLIN02_e55ab63a_Tx 98
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5C_oLIN02_29f0b6a4_Tx 99
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5D_oLIN02_2335bfbd_Tx 100
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5E_oLIN02_ef9fbf23_Tx 101
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_5F_oLIN02_6110b8c0_Tx 102
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_40_oLIN02_1c543494_Tx 103
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_41_oLIN02_d0fe340a_Tx 104
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_42_oLIN02_5e7133e9_Tx 105
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_43_oLIN02_92db3377_Tx 106
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_44_oLIN02_981e3a6e_Tx 107
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_45_oLIN02_54b43af0_Tx 108
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_46_oLIN02_da3b3d13_Tx 109
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_47_oLIN02_16913d8d_Tx 110
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_48_oLIN02_cfb12f21_Tx 111
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_49_oLIN02_031b2fbf_Tx 112
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_50_oLIN02_0b2f20d7_Tx 113
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_51_oLIN02_c7852049_Tx 114
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_52_oLIN02_490a27aa_Tx 115
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_53_oLIN02_85a02734_Tx 116
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_54_oLIN02_8f652e2d_Tx 117
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_55_oLIN02_43cf2eb3_Tx 118
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_56_oLIN02_cd402950_Tx 119
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_57_oLIN02_01ea29ce_Tx 120
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_58_oLIN02_d8ca3b62_Tx 121
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_59_oLIN02_14603bfc_Tx 122
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_60_oLIN02_32a21c12_Tx 123
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP3_L2_oLIN01_8c03039e_Tx 18
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP4_40_oLIN03_42681181_Tx 124
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP4_L2_oLIN01_a538161d_Tx 19
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_FSP5_40_oLIN04_1d825be2_Tx 125
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_ILCP1_oLIN00_57efaae5_Tx 4
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_ILCP2_oLIN03_4069fcbc_Tx 5
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_LECM2_oLIN00_670141b5_Tx 6
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_LECMBasic_oLIN00_16bdebb7_Tx 7
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_RCECS_oLIN04_b46d7afd_Tx 8
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_TCP_oLIN02_4b422897_Tx 9
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN00_4a2bb011_Tx   20
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN01_3d2c8087_Tx   21
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN02_a425d13d_Tx   22
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN03_d322e1ab_Tx   23
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN04_4d467408_Tx   24
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN05_3a41449e_Tx   126
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN06_a3481524_Tx   127
#define CddConf_CddPduRUpperLayerTxPdu_MasterReq_oLIN07_d44f25b2_Tx   128
/**\} */

/**********************************************************************************************************************
  GLOBAL FUNCTION MACROS
**********************************************************************************************************************/

/**********************************************************************************************************************
  GLOBAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/
# define CDD_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
# include "MemMap.h"    /* PRQA S 5087 */       /* MD_MSR_MemMap */
/*lint -restore */





/*! \defgroup ProvidedCddCddTransportProtocol Provided Cdd transport protocol interface to PduR
    \brief    These services have to be provided by the Cdd if the CDD is an upper layer for the PduR. */
/*\{*/

/**********************************************************************************************************************
  Cdd_StartOfReception
**********************************************************************************************************************/
/*! \brief       The function call indicates the reception start of a segmented PDU.
    \param[in]   id             id of the TP CddPduRUpperLayerRxPdu.
    \param[in]   info           Pointer to a PduInfoType structure containing the payload data
    \param[in]   TpSduLength    length of the entire the TP SDU that will be received.
    \param[out]  bufferSizePtr  length of the available receive buffer in Cdd.\n
                                This parameter is used e.g. in CanTp to calculate the Block Size (BS).
    \return      a BufReq_ReturnType constant of ComStackTypes.h.
    \pre         The Cdd is initialized and active.
    \context     This function can be called on interrupt and task level and has not to be interrupted by other\n
                 Cdd_StartOfReception calls for the same id.
    \note        The function is called by the PduR.
**********************************************************************************************************************/
FUNC(BufReq_ReturnType, CDD_CODE) Cdd_StartOfReception(PduIdType id, P2VAR(PduInfoType, AUTOMATIC, CDD_APPL_DATA) info, PduLengthType TpSduLength, P2VAR(PduLengthType, AUTOMATIC, CDD_APPL_DATA) bufferSizePtr);

/**********************************************************************************************************************
  Cdd_CopyRxData
**********************************************************************************************************************/
/*! \brief       This function is called to trigger the copy process of a segmented PDU.\n
                 The function can be called several times and\n
                 each call to this function copies parts of the received data.\n
    \param[in]   id             id of the TP CddPduRUpperLayerRxPdu.
    \param[in]   info           a PduInfoType pointing to the data to be copied in the Cdd data buffer.
    \param[out]  bufferSizePtr  available receive buffer after data has been copied.
    \return      a BufReq_ReturnType constant of ComStackTypes.h.      
    \pre         The Cdd is initialized and active.
    \context     This function can be called on interrupt and task level and has not to be interrupted by other\n
                 Cdd_CopyRxData calls for the same id.
    \note        The function is called by the PduR.
**********************************************************************************************************************/
FUNC(BufReq_ReturnType, CDD_CODE) Cdd_CopyRxData(PduIdType id, P2VAR(PduInfoType, AUTOMATIC, CDD_APPL_DATA) info, P2VAR(PduLengthType, AUTOMATIC, CDD_APPL_DATA) bufferSizePtr);

/**********************************************************************************************************************
  Cdd_TpRxIndication
**********************************************************************************************************************/
/*! \brief       The function is called to indicate the complete receiption of a Cdd TP SDU
                 or to report an error that occurred during reception.
    \param[in]   id             id of the TP CddPduRUpperLayerRxPdu.
    \param[in]   result         a Std_ReturnType to indicate the result of the reception.
    \return      none
    \pre         The Cdd is initialized and active.
    \context     This function can be called on interrupt and task level and has not to be interrupted by other\n
                 Cdd_TpRxIndication calls for the same id.
    \note        The function is called by the PduR.
**********************************************************************************************************************/
FUNC(void, CDD_CODE) Cdd_TpRxIndication(PduIdType id, Std_ReturnType result);

/**********************************************************************************************************************
  Cdd_CopyTxData
**********************************************************************************************************************/
/*! \brief       This function is called to request transmit data of a TP CddPduRUpperLayerTxPdu\n
                 The function can be called several times and\n
                 each call to this function copies the next part of the data to be transmitted.\n
    \param[in]   id             id of the TP CddPduRUpperLayerTxPdu.
    \param[in]   info           a PduInfoType pointing to the destination buffer.
    \param[in]   retry          NULL_PTR to indicate a successful copy process\n
                                or a RetryInfoType containing a TpDataStateType constant of ComStackTypes.h.
    \param       availableDataPtr   Indicates the remaining number of bytes that are available in the TX buffer.\n
                                availableDataPtr can be used by TP modules that support dynamic payload lengths\n
                                (e.g. Iso FrTp) to determine the size of the following CFs.
    \return      a BufReq_ReturnType constant of ComStackTypes.h.
    \pre         The Cdd is initialized and active.
    \context     This function can be called on interrupt and task level and has not to be interrupted by other\n
                 Cdd_CopyTxData calls for the same id.
    \note        The function is called by the PduR.
**********************************************************************************************************************/
FUNC(BufReq_ReturnType, CDD_CODE) Cdd_CopyTxData(PduIdType id, P2VAR(PduInfoType, AUTOMATIC, CDD_APPL_DATA) info, P2VAR(RetryInfoType, AUTOMATIC, CDD_APPL_DATA) retry, P2VAR(PduLengthType, AUTOMATIC, CDD_APPL_DATA) availableDataPtr);

/**********************************************************************************************************************
  Cdd_TpTxConfirmation
**********************************************************************************************************************/
/*! \brief       The function is called to confirm a successful transmission of a TP CddPduRUpperLayerTxPdu\n
                 or to report an error that occurred during transmission.
    \param[in]   id             id of the TP CddPduRUpperLayerTxPdu.
    \param[in]   result         a Std_ReturnType to indicate the result of the transmission.
    \return      none
    \pre         The Cdd is initialized and active.
    \context     This function can be called on interrupt and task level and has not to be interrupted by other\n
                 Cdd_TpTxConfirmation calls for the same id.
    \note        The function is called by the PduR.
**********************************************************************************************************************/
FUNC(void, CDD_CODE) Cdd_TpTxConfirmation(PduIdType id, Std_ReturnType result);

/*\}*/


# define CDD_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
# include "MemMap.h"    /* PRQA S 5087 */       /* MD_MSR_MemMap */
/*lint -restore */

#endif  /* CDD_CBK_H */
/**********************************************************************************************************************
  END OF FILE: Cdd_Cbk.h
**********************************************************************************************************************/


