/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_ServiceBraking_HMICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <ServiceBraking_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_SERVICEBRAKING_HMICTRL_TYPE_H
# define _RTE_SERVICEBRAKING_HMICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef InactiveActive_Inactive
#   define InactiveActive_Inactive (0U)
#  endif

#  ifndef InactiveActive_Active
#   define InactiveActive_Active (1U)
#  endif

#  ifndef InactiveActive_Error
#   define InactiveActive_Error (2U)
#  endif

#  ifndef InactiveActive_NotAvailable
#   define InactiveActive_NotAvailable (3U)
#  endif

#  ifndef Inhibit_NoInhibit
#   define Inhibit_NoInhibit (0U)
#  endif

#  ifndef Inhibit_InhibitActive
#   define Inhibit_InhibitActive (1U)
#  endif

#  ifndef Inhibit_Error
#   define Inhibit_Error (2U)
#  endif

#  ifndef Inhibit_NotAvailable
#   define Inhibit_NotAvailable (3U)
#  endif

#  ifndef OffOn_Off
#   define OffOn_Off (0U)
#  endif

#  ifndef OffOn_On
#   define OffOn_On (1U)
#  endif

#  ifndef OffOn_Error
#   define OffOn_Error (2U)
#  endif

#  ifndef OffOn_NotAvailable
#   define OffOn_NotAvailable (3U)
#  endif

#  ifndef PassiveActive_Passive
#   define PassiveActive_Passive (0U)
#  endif

#  ifndef PassiveActive_Active
#   define PassiveActive_Active (1U)
#  endif

#  ifndef PassiveActive_Error
#   define PassiveActive_Error (2U)
#  endif

#  ifndef PassiveActive_NotAvailable
#   define PassiveActive_NotAvailable (3U)
#  endif

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_SERVICEBRAKING_HMICTRL_TYPE_H */
