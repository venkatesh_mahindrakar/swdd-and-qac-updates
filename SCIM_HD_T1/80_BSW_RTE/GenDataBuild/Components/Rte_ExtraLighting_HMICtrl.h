/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_ExtraLighting_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <ExtraLighting_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_EXTRALIGHTING_HMICTRL_H
# define _RTE_EXTRALIGHTING_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_ExtraLighting_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_BeaconSRocker_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_Beacon_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_CabWorkingLight_DevInd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_EquipmentLightInd_cmd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_FifthWheelLightInd_cmd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_LEDVega_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_PloughtLights_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_RearWorkProjector_Indication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_WorkingLight_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_BeaconSRocker_DeviceEvent_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Beacon_DeviceEven_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_CabWorkLight_ButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EquipmentLight_DeviceEvent_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FifthWheelLight_DeviceEvent_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LEDVega_DeviceEvent_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_PloughLight_DeviceEvent_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_PloughtLightsPushButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ExteriorLightPanel_2_LINMastCtrl_RearWorkProjector_ButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_SpotlightFront_DeviceEvent_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_SpotlightRoof_DeviceEvent_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Parked_Parked; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleMode_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_WorkLight_ButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_BBNetwBeaconLight_stat_BBNetwBeaconLight_stat (3U)
#  define Rte_InitValue_BBNetwBodyOrCabWrknLight_stat_BBNetwBodyOrCabWrknLight_stat (3U)
#  define Rte_InitValue_BBNetwTrailerBodyLighting_rqst_BBNetwTrailerBodyLighting_rqst (3U)
#  define Rte_InitValue_BBNetwWrknLightChassis_stat_BBNetwWrknLightChassis_stat (3U)
#  define Rte_InitValue_BeaconSRocker_DeviceEvent_A2PosSwitchStatus (3U)
#  define Rte_InitValue_BeaconSRocker_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_Beacon_DeviceEven_PushButtonStatus (3U)
#  define Rte_InitValue_Beacon_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_BodyOrCabWorkingLightFdbk_stat_BodyOrCabWorkingLightFdbk_stat (3U)
#  define Rte_InitValue_BodyOrCabWorkingLight_rqst_BodyOrCabWorkingLight_rqst (3U)
#  define Rte_InitValue_CabBeaconLightFeedback_Status_CabBeaconLightFeedback_Status (3U)
#  define Rte_InitValue_CabBeaconLight_rqst_CabBeaconLight_rqst (3U)
#  define Rte_InitValue_CabFrontSpotFeedback_Status_CabFrontSpotFeedback_Status (3U)
#  define Rte_InitValue_CabFrontSpot_rqst_CabFrontSpot_rqst (3U)
#  define Rte_InitValue_CabPlowLight_rqst_CabPlowLight_rqst (3U)
#  define Rte_InitValue_CabRoofSignLightFeedback_stat_CabRoofSignLightFeedback_stat (3U)
#  define Rte_InitValue_CabRoofSignOrVegaLight_rqst_CabRoofSignOrVegaLight_rqst (3U)
#  define Rte_InitValue_CabRoofSpotFeedback_Status_CabRoofSpotFeedback_Status (3U)
#  define Rte_InitValue_CabRoofSpot_rqst_CabRoofSpot_rqst (3U)
#  define Rte_InitValue_CabTrailerBodyLgthnFdbk_stat_CabTrailerBodyLgthnFdbk_stat (3U)
#  define Rte_InitValue_CabTrailerBodyLighting_rqst_CabTrailerBodyLighting_rqst (3U)
#  define Rte_InitValue_CabWorkLight_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_CabWorkingLightChassis_rqst_CabWorkingLightChassis_rqst (3U)
#  define Rte_InitValue_CabWorkingLight_DevInd_DeviceIndication (0U)
#  define Rte_InitValue_CabWrknLightChassisFdbk_stat_CabWrknLightChassisFdbk_stat (3U)
#  define Rte_InitValue_EquipmentLightInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_EquipmentLight_DeviceEvent_PushButtonStatus (3U)
#  define Rte_InitValue_ExtraSideMarkers_DeviceEvent_PushButtonStatus (3U)
#  define Rte_InitValue_ExtraSideMarkers_DeviceInd_DeviceIndication (3U)
#  define Rte_InitValue_ExtraSideMarkers_FunctStat_ExtraSideMarkers_FunctStat (3U)
#  define Rte_InitValue_ExtraSideMarkers_Rqst_ExtraSideMarkers_Rqst (3U)
#  define Rte_InitValue_FifthWheelLightInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_FifthWheelLight_DeviceEvent_PushButtonStatus (3U)
#  define Rte_InitValue_LEDVega_DeviceEvent_PushButtonStatus (3U)
#  define Rte_InitValue_LEDVega_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_PloughLampModeStatus_PloughLampModeStatus (3U)
#  define Rte_InitValue_PloughLight_DeviceEvent_PushButtonStatus (3U)
#  define Rte_InitValue_PloughtLightsPushButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_PloughtLights_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_RearWorkProjector_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_RearWorkProjector_Indication_DeviceIndication (3U)
#  define Rte_InitValue_RoofSignLight_DeviceEvent_PushButtonStatus (3U)
#  define Rte_InitValue_SpotlightFront_DeviceEvent_PushButtonStatus (3U)
#  define Rte_InitValue_SpotlightRoof_DeviceEvent_PushButtonStatus (3U)
#  define Rte_InitValue_SwcActivation_Parked_Parked (1U)
#  define Rte_InitValue_TrailerBodyLampDI_stat_TrailerBodyLampDI_stat (3U)
#  define Rte_InitValue_TrailerBodyLampFdbk_stat_TrailerBodyLampFdbk_stat (3U)
#  define Rte_InitValue_TrailerBodyLamp_rqst_TrailerBodyLamp_rqst (3U)
#  define Rte_InitValue_VehicleModeInternal_VehicleMode (15U)
#  define Rte_InitValue_WRC5thWheelRequest_WRC5thWheelRequest (3U)
#  define Rte_InitValue_WRCBeaconRequest_WRCBeaconRequest (3U)
#  define Rte_InitValue_WRCCabBodyWLightsRqst_WRCCabBodyWLightsRqst (3U)
#  define Rte_InitValue_WorkLight_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_WorkingLight_DeviceIndication_DeviceIndication (3U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraLighting_HMICtrl_BBNetwBeaconLight_stat_BBNetwBeaconLight_stat(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_EXTRALIGHTING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraLighting_HMICtrl_BBNetwBodyOrCabWrknLight_stat_BBNetwBodyOrCabWrknLight_stat(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_EXTRALIGHTING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraLighting_HMICtrl_BBNetwWrknLightChassis_stat_BBNetwWrknLightChassis_stat(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_EXTRALIGHTING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraLighting_HMICtrl_BodyOrCabWorkingLightFdbk_stat_BodyOrCabWorkingLightFdbk_stat(P2VAR(OffOn_T, AUTOMATIC, RTE_EXTRALIGHTING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraLighting_HMICtrl_CabBeaconLightFeedback_Status_CabBeaconLightFeedback_Status(P2VAR(OffOn_T, AUTOMATIC, RTE_EXTRALIGHTING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraLighting_HMICtrl_CabRoofSignLightFeedback_stat_CabRoofSignLightFeedback_stat(P2VAR(OffOn_T, AUTOMATIC, RTE_EXTRALIGHTING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraLighting_HMICtrl_CabTrailerBodyLgthnFdbk_stat_CabTrailerBodyLgthnFdbk_stat(P2VAR(OffOn_T, AUTOMATIC, RTE_EXTRALIGHTING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraLighting_HMICtrl_PloughLampModeStatus_PloughLampModeStatus(P2VAR(InactiveActive_T, AUTOMATIC, RTE_EXTRALIGHTING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraLighting_HMICtrl_TrailerBodyLampDI_stat_TrailerBodyLampDI_stat(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_EXTRALIGHTING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraLighting_HMICtrl_TrailerBodyLampFdbk_stat_TrailerBodyLampFdbk_stat(P2VAR(OffOn_T, AUTOMATIC, RTE_EXTRALIGHTING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraLighting_HMICtrl_WRC5thWheelRequest_WRC5thWheelRequest(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_EXTRALIGHTING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraLighting_HMICtrl_WRCBeaconRequest_WRCBeaconRequest(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_EXTRALIGHTING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraLighting_HMICtrl_WRCCabBodyWLightsRqst_WRCCabBodyWLightsRqst(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_EXTRALIGHTING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExtraLighting_HMICtrl_BodyOrCabWorkingLight_rqst_BodyOrCabWorkingLight_rqst(Request_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExtraLighting_HMICtrl_CabBeaconLight_rqst_CabBeaconLight_rqst(Request_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExtraLighting_HMICtrl_CabFrontSpot_rqst_CabFrontSpot_rqst(Request_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExtraLighting_HMICtrl_CabPlowLight_rqst_CabPlowLight_rqst(Request_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExtraLighting_HMICtrl_CabRoofSignOrVegaLight_rqst_CabRoofSignOrVegaLight_rqst(Request_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExtraLighting_HMICtrl_CabRoofSpot_rqst_CabRoofSpot_rqst(Request_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExtraLighting_HMICtrl_CabTrailerBodyLighting_rqst_CabTrailerBodyLighting_rqst(Request_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExtraLighting_HMICtrl_CabWorkingLightChassis_rqst_CabWorkingLightChassis_rqst(Request_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExtraLighting_HMICtrl_TrailerBodyLamp_rqst_TrailerBodyLamp_rqst(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_BBNetwBeaconLight_stat_BBNetwBeaconLight_stat Rte_Read_ExtraLighting_HMICtrl_BBNetwBeaconLight_stat_BBNetwBeaconLight_stat
#  define Rte_Read_BBNetwBodyOrCabWrknLight_stat_BBNetwBodyOrCabWrknLight_stat Rte_Read_ExtraLighting_HMICtrl_BBNetwBodyOrCabWrknLight_stat_BBNetwBodyOrCabWrknLight_stat
#  define Rte_Read_BBNetwTrailerBodyLighting_rqst_BBNetwTrailerBodyLighting_rqst Rte_Read_ExtraLighting_HMICtrl_BBNetwTrailerBodyLighting_rqst_BBNetwTrailerBodyLighting_rqst
#  define Rte_Read_ExtraLighting_HMICtrl_BBNetwTrailerBodyLighting_rqst_BBNetwTrailerBodyLighting_rqst(data) (Com_ReceiveSignal(ComConf_ComSignal_BBNetwTrailerBodyLighting_rqst_oBBM_BB2_02P_oBackbone2_a59c16c2_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_BBNetwWrknLightChassis_stat_BBNetwWrknLightChassis_stat Rte_Read_ExtraLighting_HMICtrl_BBNetwWrknLightChassis_stat_BBNetwWrknLightChassis_stat
#  define Rte_Read_BeaconSRocker_DeviceEvent_A2PosSwitchStatus Rte_Read_ExtraLighting_HMICtrl_BeaconSRocker_DeviceEvent_A2PosSwitchStatus
#  define Rte_Read_ExtraLighting_HMICtrl_BeaconSRocker_DeviceEvent_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_BeaconSRocker_DeviceEvent_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Beacon_DeviceEven_PushButtonStatus Rte_Read_ExtraLighting_HMICtrl_Beacon_DeviceEven_PushButtonStatus
#  define Rte_Read_ExtraLighting_HMICtrl_Beacon_DeviceEven_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_Beacon_DeviceEven_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_BodyOrCabWorkingLightFdbk_stat_BodyOrCabWorkingLightFdbk_stat Rte_Read_ExtraLighting_HMICtrl_BodyOrCabWorkingLightFdbk_stat_BodyOrCabWorkingLightFdbk_stat
#  define Rte_Read_CabBeaconLightFeedback_Status_CabBeaconLightFeedback_Status Rte_Read_ExtraLighting_HMICtrl_CabBeaconLightFeedback_Status_CabBeaconLightFeedback_Status
#  define Rte_Read_CabRoofSignLightFeedback_stat_CabRoofSignLightFeedback_stat Rte_Read_ExtraLighting_HMICtrl_CabRoofSignLightFeedback_stat_CabRoofSignLightFeedback_stat
#  define Rte_Read_CabTrailerBodyLgthnFdbk_stat_CabTrailerBodyLgthnFdbk_stat Rte_Read_ExtraLighting_HMICtrl_CabTrailerBodyLgthnFdbk_stat_CabTrailerBodyLgthnFdbk_stat
#  define Rte_Read_CabWorkLight_ButtonStatus_PushButtonStatus Rte_Read_ExtraLighting_HMICtrl_CabWorkLight_ButtonStatus_PushButtonStatus
#  define Rte_Read_ExtraLighting_HMICtrl_CabWorkLight_ButtonStatus_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_CabWorkLight_ButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_CabWrknLightChassisFdbk_stat_CabWrknLightChassisFdbk_stat Rte_Read_ExtraLighting_HMICtrl_CabWrknLightChassisFdbk_stat_CabWrknLightChassisFdbk_stat
#  define Rte_Read_ExtraLighting_HMICtrl_CabWrknLightChassisFdbk_stat_CabWrknLightChassisFdbk_stat(data) (Com_ReceiveSignal(ComConf_ComSignal_CabWrknLightChassisFdbk_stat_ISig_4_oVMCU_BB2_08P_oBackbone2_2aa5c4e4_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_EquipmentLight_DeviceEvent_PushButtonStatus Rte_Read_ExtraLighting_HMICtrl_EquipmentLight_DeviceEvent_PushButtonStatus
#  define Rte_Read_ExtraLighting_HMICtrl_EquipmentLight_DeviceEvent_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_EquipmentLight_DeviceEvent_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FifthWheelLight_DeviceEvent_PushButtonStatus Rte_Read_ExtraLighting_HMICtrl_FifthWheelLight_DeviceEvent_PushButtonStatus
#  define Rte_Read_ExtraLighting_HMICtrl_FifthWheelLight_DeviceEvent_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_FifthWheelLight_DeviceEvent_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LEDVega_DeviceEvent_PushButtonStatus Rte_Read_ExtraLighting_HMICtrl_LEDVega_DeviceEvent_PushButtonStatus
#  define Rte_Read_ExtraLighting_HMICtrl_LEDVega_DeviceEvent_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_LEDVega_DeviceEvent_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_PloughLampModeStatus_PloughLampModeStatus Rte_Read_ExtraLighting_HMICtrl_PloughLampModeStatus_PloughLampModeStatus
#  define Rte_Read_PloughLight_DeviceEvent_PushButtonStatus Rte_Read_ExtraLighting_HMICtrl_PloughLight_DeviceEvent_PushButtonStatus
#  define Rte_Read_ExtraLighting_HMICtrl_PloughLight_DeviceEvent_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_PloughLight_DeviceEvent_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_PloughtLightsPushButtonStatus_PushButtonStatus Rte_Read_ExtraLighting_HMICtrl_PloughtLightsPushButtonStatus_PushButtonStatus
#  define Rte_Read_ExtraLighting_HMICtrl_PloughtLightsPushButtonStatus_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_PloughtLightsPushButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RearWorkProjector_ButtonStatus_PushButtonStatus Rte_Read_ExtraLighting_HMICtrl_RearWorkProjector_ButtonStatus_PushButtonStatus
#  define Rte_Read_ExtraLighting_HMICtrl_RearWorkProjector_ButtonStatus_PushButtonStatus(data) (*(data) = Rte_ExteriorLightPanel_2_LINMastCtrl_RearWorkProjector_ButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SpotlightFront_DeviceEvent_PushButtonStatus Rte_Read_ExtraLighting_HMICtrl_SpotlightFront_DeviceEvent_PushButtonStatus
#  define Rte_Read_ExtraLighting_HMICtrl_SpotlightFront_DeviceEvent_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_SpotlightFront_DeviceEvent_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SpotlightRoof_DeviceEvent_PushButtonStatus Rte_Read_ExtraLighting_HMICtrl_SpotlightRoof_DeviceEvent_PushButtonStatus
#  define Rte_Read_ExtraLighting_HMICtrl_SpotlightRoof_DeviceEvent_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_SpotlightRoof_DeviceEvent_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_Parked_Parked Rte_Read_ExtraLighting_HMICtrl_SwcActivation_Parked_Parked
#  define Rte_Read_ExtraLighting_HMICtrl_SwcActivation_Parked_Parked(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Parked_Parked, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_TrailerBodyLampDI_stat_TrailerBodyLampDI_stat Rte_Read_ExtraLighting_HMICtrl_TrailerBodyLampDI_stat_TrailerBodyLampDI_stat
#  define Rte_Read_TrailerBodyLampFdbk_stat_TrailerBodyLampFdbk_stat Rte_Read_ExtraLighting_HMICtrl_TrailerBodyLampFdbk_stat_TrailerBodyLampFdbk_stat
#  define Rte_Read_VehicleModeInternal_VehicleMode Rte_Read_ExtraLighting_HMICtrl_VehicleModeInternal_VehicleMode
#  define Rte_Read_ExtraLighting_HMICtrl_VehicleModeInternal_VehicleMode(data) (*(data) = Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WRC5thWheelRequest_WRC5thWheelRequest Rte_Read_ExtraLighting_HMICtrl_WRC5thWheelRequest_WRC5thWheelRequest
#  define Rte_Read_WRCBeaconRequest_WRCBeaconRequest Rte_Read_ExtraLighting_HMICtrl_WRCBeaconRequest_WRCBeaconRequest
#  define Rte_Read_WRCCabBodyWLightsRqst_WRCCabBodyWLightsRqst Rte_Read_ExtraLighting_HMICtrl_WRCCabBodyWLightsRqst_WRCCabBodyWLightsRqst
#  define Rte_Read_WorkLight_ButtonStatus_PushButtonStatus Rte_Read_ExtraLighting_HMICtrl_WorkLight_ButtonStatus_PushButtonStatus
#  define Rte_Read_ExtraLighting_HMICtrl_WorkLight_ButtonStatus_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_WorkLight_ButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_BeaconSRocker_DeviceIndication_DeviceIndication Rte_Write_ExtraLighting_HMICtrl_BeaconSRocker_DeviceIndication_DeviceIndication
#  define Rte_Write_ExtraLighting_HMICtrl_BeaconSRocker_DeviceIndication_DeviceIndication(data) (Rte_ExtraLighting_HMICtrl_BeaconSRocker_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Beacon_DeviceIndication_DeviceIndication Rte_Write_ExtraLighting_HMICtrl_Beacon_DeviceIndication_DeviceIndication
#  define Rte_Write_ExtraLighting_HMICtrl_Beacon_DeviceIndication_DeviceIndication(data) (Rte_ExtraLighting_HMICtrl_Beacon_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_BodyOrCabWorkingLight_rqst_BodyOrCabWorkingLight_rqst Rte_Write_ExtraLighting_HMICtrl_BodyOrCabWorkingLight_rqst_BodyOrCabWorkingLight_rqst
#  define Rte_Write_CabBeaconLight_rqst_CabBeaconLight_rqst Rte_Write_ExtraLighting_HMICtrl_CabBeaconLight_rqst_CabBeaconLight_rqst
#  define Rte_Write_CabFrontSpot_rqst_CabFrontSpot_rqst Rte_Write_ExtraLighting_HMICtrl_CabFrontSpot_rqst_CabFrontSpot_rqst
#  define Rte_Write_CabPlowLight_rqst_CabPlowLight_rqst Rte_Write_ExtraLighting_HMICtrl_CabPlowLight_rqst_CabPlowLight_rqst
#  define Rte_Write_CabRoofSignOrVegaLight_rqst_CabRoofSignOrVegaLight_rqst Rte_Write_ExtraLighting_HMICtrl_CabRoofSignOrVegaLight_rqst_CabRoofSignOrVegaLight_rqst
#  define Rte_Write_CabRoofSpot_rqst_CabRoofSpot_rqst Rte_Write_ExtraLighting_HMICtrl_CabRoofSpot_rqst_CabRoofSpot_rqst
#  define Rte_Write_CabTrailerBodyLighting_rqst_CabTrailerBodyLighting_rqst Rte_Write_ExtraLighting_HMICtrl_CabTrailerBodyLighting_rqst_CabTrailerBodyLighting_rqst
#  define Rte_Write_CabWorkingLightChassis_rqst_CabWorkingLightChassis_rqst Rte_Write_ExtraLighting_HMICtrl_CabWorkingLightChassis_rqst_CabWorkingLightChassis_rqst
#  define Rte_Write_CabWorkingLight_DevInd_DeviceIndication Rte_Write_ExtraLighting_HMICtrl_CabWorkingLight_DevInd_DeviceIndication
#  define Rte_Write_ExtraLighting_HMICtrl_CabWorkingLight_DevInd_DeviceIndication(data) (Rte_ExtraLighting_HMICtrl_CabWorkingLight_DevInd_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_EquipmentLightInd_cmd_DeviceIndication Rte_Write_ExtraLighting_HMICtrl_EquipmentLightInd_cmd_DeviceIndication
#  define Rte_Write_ExtraLighting_HMICtrl_EquipmentLightInd_cmd_DeviceIndication(data) (Rte_ExtraLighting_HMICtrl_EquipmentLightInd_cmd_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_FifthWheelLightInd_cmd_DeviceIndication Rte_Write_ExtraLighting_HMICtrl_FifthWheelLightInd_cmd_DeviceIndication
#  define Rte_Write_ExtraLighting_HMICtrl_FifthWheelLightInd_cmd_DeviceIndication(data) (Rte_ExtraLighting_HMICtrl_FifthWheelLightInd_cmd_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LEDVega_DeviceIndication_DeviceIndication Rte_Write_ExtraLighting_HMICtrl_LEDVega_DeviceIndication_DeviceIndication
#  define Rte_Write_ExtraLighting_HMICtrl_LEDVega_DeviceIndication_DeviceIndication(data) (Rte_ExtraLighting_HMICtrl_LEDVega_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_PloughtLights_DeviceIndication_DeviceIndication Rte_Write_ExtraLighting_HMICtrl_PloughtLights_DeviceIndication_DeviceIndication
#  define Rte_Write_ExtraLighting_HMICtrl_PloughtLights_DeviceIndication_DeviceIndication(data) (Rte_ExtraLighting_HMICtrl_PloughtLights_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_RearWorkProjector_Indication_DeviceIndication Rte_Write_ExtraLighting_HMICtrl_RearWorkProjector_Indication_DeviceIndication
#  define Rte_Write_ExtraLighting_HMICtrl_RearWorkProjector_Indication_DeviceIndication(data) (Rte_ExtraLighting_HMICtrl_RearWorkProjector_Indication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_TrailerBodyLamp_rqst_TrailerBodyLamp_rqst Rte_Write_ExtraLighting_HMICtrl_TrailerBodyLamp_rqst_TrailerBodyLamp_rqst
#  define Rte_Write_WorkingLight_DeviceIndication_DeviceIndication Rte_Write_ExtraLighting_HMICtrl_WorkingLight_DeviceIndication_DeviceIndication
#  define Rte_Write_ExtraLighting_HMICtrl_WorkingLight_DeviceIndication_DeviceIndication(data) (Rte_ExtraLighting_HMICtrl_WorkingLight_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


# endif /* !defined(RTE_CORE) */


# define ExtraLighting_HMICtrl_START_SEC_CODE
# include "ExtraLighting_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_BeaconWarningLogic_20ms_runnable BeaconWarningLogic_20ms_runnable
#  define RTE_RUNNABLE_BodyOrCabWorkingLight_20ms_runnable BodyOrCabWorkingLight_20ms_runnable
#  define RTE_RUNNABLE_ChassisWorkingLightLogic_20ms_runnable ChassisWorkingLightLogic_20ms_runnable
#  define RTE_RUNNABLE_EquipmentLightsLogic_20ms_runnable EquipmentLightsLogic_20ms_runnable
#  define RTE_RUNNABLE_ExtraLighting_HMICtrl_init ExtraLighting_HMICtrl_init
#  define RTE_RUNNABLE_ExtraSpotsLogic_20ms_runnable ExtraSpotsLogic_20ms_runnable
#  define RTE_RUNNABLE_PloughLampsLogic_20ms_runnable PloughLampsLogic_20ms_runnable
#  define RTE_RUNNABLE_VegaLedsLogic_20ms_runnable VegaLedsLogic_20ms_runnable
# endif

FUNC(void, ExtraLighting_HMICtrl_CODE) BeaconWarningLogic_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, ExtraLighting_HMICtrl_CODE) BodyOrCabWorkingLight_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, ExtraLighting_HMICtrl_CODE) ChassisWorkingLightLogic_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, ExtraLighting_HMICtrl_CODE) EquipmentLightsLogic_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, ExtraLighting_HMICtrl_CODE) ExtraLighting_HMICtrl_init(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, ExtraLighting_HMICtrl_CODE) ExtraSpotsLogic_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, ExtraLighting_HMICtrl_CODE) PloughLampsLogic_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, ExtraLighting_HMICtrl_CODE) VegaLedsLogic_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define ExtraLighting_HMICtrl_STOP_SEC_CODE
# include "ExtraLighting_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_EXTRALIGHTING_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
