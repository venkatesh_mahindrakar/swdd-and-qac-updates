/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_VEC_CryptoProxyReceiverSwc_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <VEC_CryptoProxyReceiverSwc>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_VEC_CRYPTOPROXYRECEIVERSWC_TYPE_H
# define _RTE_VEC_CRYPTOPROXYRECEIVERSWC_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef VEC_CryptoProxy_Idle
#   define VEC_CryptoProxy_Idle (0U)
#  endif

#  ifndef VEC_CryptoProxy_TransmitNewIdentification
#   define VEC_CryptoProxy_TransmitNewIdentification (1U)
#  endif

#  ifndef VEC_CryptoProxy_IdentificationTransmitted
#   define VEC_CryptoProxy_IdentificationTransmitted (2U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_VEC_CRYPTOPROXYRECEIVERSWC_TYPE_H */
