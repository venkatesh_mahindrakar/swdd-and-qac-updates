/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CityHorn_Input_Hdlr_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <CityHorn_Input_Hdlr>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_CITYHORN_INPUT_HDLR_TYPE_H
# define _RTE_CITYHORN_INPUT_HDLR_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef DEM_EVENT_STATUS_PASSED
#   define DEM_EVENT_STATUS_PASSED (0U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED
#   define DEM_EVENT_STATUS_FAILED (1U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED
#   define DEM_EVENT_STATUS_PREPASSED (2U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED
#   define DEM_EVENT_STATUS_PREFAILED (3U)
#  endif

#  ifndef DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED
#   define DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
#  endif

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

#  ifndef TestNotRun
#   define TestNotRun (0U)
#  endif

#  ifndef OffState_NoFaultDetected
#   define OffState_NoFaultDetected (16U)
#  endif

#  ifndef OffState_FaultDetected_STG
#   define OffState_FaultDetected_STG (17U)
#  endif

#  ifndef OffState_FaultDetected_STB
#   define OffState_FaultDetected_STB (18U)
#  endif

#  ifndef OffState_FaultDetected_OC
#   define OffState_FaultDetected_OC (19U)
#  endif

#  ifndef OffState_FaultDetected_VBT
#   define OffState_FaultDetected_VBT (22U)
#  endif

#  ifndef OffState_FaultDetected_VAT
#   define OffState_FaultDetected_VAT (23U)
#  endif

#  ifndef OnState_NoFaultDetected
#   define OnState_NoFaultDetected (32U)
#  endif

#  ifndef OnState_FaultDetected_STG
#   define OnState_FaultDetected_STG (33U)
#  endif

#  ifndef OnState_FaultDetected_STB
#   define OnState_FaultDetected_STB (34U)
#  endif

#  ifndef OnState_FaultDetected_OC
#   define OnState_FaultDetected_OC (35U)
#  endif

#  ifndef OnState_FaultDetected_VBT
#   define OnState_FaultDetected_VBT (38U)
#  endif

#  ifndef OnState_FaultDetected_VAT
#   define OnState_FaultDetected_VAT (39U)
#  endif

#  ifndef OnState_FaultDetected_VOR
#   define OnState_FaultDetected_VOR (41U)
#  endif

#  ifndef OnState_FaultDetected_CAT
#   define OnState_FaultDetected_CAT (44U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_CITYHORN_INPUT_HDLR_TYPE_H */
