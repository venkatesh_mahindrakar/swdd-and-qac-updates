/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_SCIM_PVTPT_IO.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <SCIM_PVTPT_IO>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_SCIM_PVTPT_IO_H
# define _RTE_SCIM_PVTPT_IO_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_SCIM_PVTPT_IO_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(BswM_BswMRteMDG_PvtReport_X1C14, RTE_VAR_NOINIT) Rte_SCIM_PVTPT_IO_Request_SwcModeRequest_PvtReportCtrl_requestedMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(uint8, RTE_VAR_NOINIT) Rte_SCIM_PVTPT_IO_ScimPvtControl_P_Status; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_Debug_PVT_ADI_ReportRequest_Debug_PVT_ADI_ReportRequest (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_FlexDataRequest (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VDCDC (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VLiving (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VParked (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS1 (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS2 (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS3 (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS4 (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BLS1 (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_DAIPullUp (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_Generic1 (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS1 (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS2 (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS2 (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS3 (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS1 (FALSE)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS2 (FALSE)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS2 (FALSE)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS3 (FALSE)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Duty (0U)
#  define Rte_InitValue_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Freq (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_ADI_ReportGroup (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_DOWHS1_ReportedValue (FALSE)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_DOWHS2_ReportedValue (FALSE)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_DOWLS2_ReportedValue (FALSE)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_DOWLS3_ReportedValue (FALSE)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCFault (FALSE)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCVolt (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingFault (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingVolt (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedFault (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedVolt (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI01_7 (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI02_8 (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI03_9 (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI04_10 (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI05_11 (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI06_12 (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Fault (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Volt (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Fault (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Volt (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Fault (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Volt (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Fault (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Volt (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Fault (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Volt (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_DAI1_2 (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT_Fault (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Fault (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Freq (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_VD (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Fault (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Freq (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_VD (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Fault (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Freq (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_VD (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Fault (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Freq (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_VD (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Long (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Short (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSinceWkUp_Short (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_SCIM_RD_Generic1 (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_SCIM_RD_Generic2 (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_SCIM_RD_Generic3 (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_SCIM_RD_Generic4 (0U)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_SCIM_RD_Generic5 (FALSE)
#  define Rte_InitValue_Debug_PVT_ReportGroup_Debug_SCIM_RD_Generic6 (0U)
#  define Rte_InitValue_Request_SwcModeRequest_PvtReportCtrl_requestedMode (0U)
#  define Rte_InitValue_ScimPvtControl_P_Status (0U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_ADI_ReportGroup(Debug_PVT_ADI_ReportGroup data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_DOWHS1_ReportedValue(Debug_PVT_DOWHS1_ReportedValue data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_DOWHS2_ReportedValue(Debug_PVT_DOWHS2_ReportedValue data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_DOWLS2_ReportedValue(Debug_PVT_DOWLS2_ReportedValue data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_DOWLS3_ReportedValue(Debug_PVT_DOWLS3_ReportedValue data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCFault(Debug_PVT_SCIM_RD_12VDCDCFault data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCVolt(Debug_PVT_SCIM_RD_12VDCDCVolt data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingFault(Debug_PVT_SCIM_RD_12VLivingFault data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingVolt(Debug_PVT_SCIM_RD_12VLivingVolt data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedFault(Debug_PVT_SCIM_RD_12VParkedFault data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedVolt(Debug_PVT_SCIM_RD_12VParkedVolt data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI01_7(Debug_PVT_SCIM_RD_ADI01_7 data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI02_8(Debug_PVT_SCIM_RD_ADI02_8 data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI03_9(Debug_PVT_SCIM_RD_ADI03_9 data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI04_10(Debug_PVT_SCIM_RD_ADI04_10 data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI05_11(Debug_PVT_SCIM_RD_ADI05_11 data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI06_12(Debug_PVT_SCIM_RD_ADI06_12 data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Fault(Debug_PVT_SCIM_RD_BHS1_Fault data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Volt(Debug_PVT_SCIM_RD_BHS1_Volt data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Fault(Debug_PVT_SCIM_RD_BHS2_Fault data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Volt(Debug_PVT_SCIM_RD_BHS2_Volt data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Fault(Debug_PVT_SCIM_RD_BHS3_Fault data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Volt(Debug_PVT_SCIM_RD_BHS3_Volt data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Fault(Debug_PVT_SCIM_RD_BHS4_Fault data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Volt(Debug_PVT_SCIM_RD_BHS4_Volt data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Fault(Debug_PVT_SCIM_RD_BLS1_Fault data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Volt(Debug_PVT_SCIM_RD_BLS1_Volt data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_DAI1_2(Debug_PVT_SCIM_RD_DAI1_2 data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT(Debug_PVT_SCIM_RD_VBAT data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT_Fault(Debug_PVT_SCIM_RD_VBAT_Fault data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Fault(Debug_PVT_SCIM_RD_WHS1_Fault data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Freq(Debug_PVT_SCIM_RD_WHS1_Freq data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_VD(Debug_PVT_SCIM_RD_WHS1_VD data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Fault(Debug_PVT_SCIM_RD_WHS2_Fault data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Freq(Debug_PVT_SCIM_RD_WHS2_Freq data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_VD(Debug_PVT_SCIM_RD_WHS2_VD data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Fault(Debug_PVT_SCIM_RD_WLS2_Fault data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Freq(Debug_PVT_SCIM_RD_WLS2_Freq data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_VD(Debug_PVT_SCIM_RD_WLS2_VD data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Fault(Debug_PVT_SCIM_RD_WLS3_Fault data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Freq(Debug_PVT_SCIM_RD_WLS3_Freq data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_VD(Debug_PVT_SCIM_RD_WLS3_VD data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Long(Debug_PVT_SCIM_TSincePwrOn_Long data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Short(Debug_PVT_SCIM_TSincePwrOn_Short data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSinceWkUp_Short(Debug_PVT_SCIM_TSinceWkUp_Short data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_Debug_PVT_ADI_ReportRequest_Debug_PVT_ADI_ReportRequest Rte_Read_SCIM_PVTPT_IO_Debug_PVT_ADI_ReportRequest_Debug_PVT_ADI_ReportRequest
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_ADI_ReportRequest_Debug_PVT_ADI_ReportRequest(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_ADI_ReportRequest_oDebugCtrl1_CIOM_BB2_oBackbone2_54366174_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VDCDC Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VDCDC
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VDCDC(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_SCIM_Ctrl_12VDCDC_oDebugCtrl1_CIOM_BB2_oBackbone2_a3dd4461_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VLiving Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VLiving
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VLiving(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_SCIM_Ctrl_12VLiving_oDebugCtrl1_CIOM_BB2_oBackbone2_ba000e16_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VParked Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VParked
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VParked(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_SCIM_Ctrl_12VParked_oDebugCtrl1_CIOM_BB2_oBackbone2_8a2aeb20_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS1 Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS1
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS1(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_SCIM_Ctrl_BHS1_oDebugCtrl1_CIOM_BB2_oBackbone2_4fee59ef_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS2 Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS2
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS2(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_SCIM_Ctrl_BHS2_oDebugCtrl1_CIOM_BB2_oBackbone2_14f9e8fa_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS3 Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS3
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS3(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_SCIM_Ctrl_BHS3_oDebugCtrl1_CIOM_BB2_oBackbone2_220b7809_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS4 Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS4
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS4(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_SCIM_Ctrl_BHS4_oDebugCtrl1_CIOM_BB2_oBackbone2_a2d68ad0_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BLS1 Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BLS1
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BLS1(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_SCIM_Ctrl_BLS1_oDebugCtrl1_CIOM_BB2_oBackbone2_c7740a4d_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_DAIPullUp Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_DAIPullUp
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_DAIPullUp(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_SCIM_Ctrl_DAIPullUp_oDebugCtrl1_CIOM_BB2_oBackbone2_81c2cf10_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_Generic1 Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_Generic1
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_Generic1(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_SCIM_Ctrl_Generic1_oDebugCtrl1_CIOM_BB2_oBackbone2_36282b0c_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS1 Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS1
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS1(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_SCIM_Ctrl_WHS1_oDebugCtrl1_CIOM_BB2_oBackbone2_038fb7b1_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS2 Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS2
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS2(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_SCIM_Ctrl_WHS2_oDebugCtrl1_CIOM_BB2_oBackbone2_589806a4_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS2 Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS2
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS2(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_SCIM_Ctrl_WLS2_oDebugCtrl1_CIOM_BB2_oBackbone2_d0025506_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS3 Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS3
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS3(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_SCIM_Ctrl_WLS3_oDebugCtrl1_CIOM_BB2_oBackbone2_e6f0c5f5_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS1 Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS1
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS1(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_ScimHwSelect_WHS1_oDebugCtrl1_CIOM_BB2_oBackbone2_71c2c8da_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS2 Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS2
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS2(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_ScimHwSelect_WHS2_oDebugCtrl1_CIOM_BB2_oBackbone2_2ad579cf_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS2 Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS2
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS2(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_ScimHwSelect_WLS2_oDebugCtrl1_CIOM_BB2_oBackbone2_a24f2a6d_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS3 Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS3
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS3(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_ScimHwSelect_WLS3_oDebugCtrl1_CIOM_BB2_oBackbone2_94bdba9e_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Duty Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Duty
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Duty(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_ScimHw_W_Duty_oDebugCtrl1_CIOM_BB2_oBackbone2_eae64c94_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Freq Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Freq
#  define Rte_Read_SCIM_PVTPT_IO_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Freq(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_ScimHw_W_Freq_oDebugCtrl1_CIOM_BB2_oBackbone2_75ee4708_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_ADI_ReportGroup Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_ADI_ReportGroup
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWHS1_ReportedValue Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_DOWHS1_ReportedValue
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWHS2_ReportedValue Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_DOWHS2_ReportedValue
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWLS2_ReportedValue Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_DOWLS2_ReportedValue
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWLS3_ReportedValue Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_DOWLS3_ReportedValue
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCFault Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCFault
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCVolt Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCVolt
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingFault Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingFault
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingVolt Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingVolt
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedFault Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedFault
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedVolt Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedVolt
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI01_7 Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI01_7
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI02_8 Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI02_8
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI03_9 Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI03_9
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI04_10 Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI04_10
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI05_11 Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI05_11
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI06_12 Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI06_12
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Fault Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Fault
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Volt Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Volt
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Fault Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Fault
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Volt Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Volt
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Fault Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Fault
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Volt Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Volt
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Fault Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Fault
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Volt Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Volt
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Fault Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Fault
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Volt Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Volt
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_DAI1_2 Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_DAI1_2
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT_Fault Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT_Fault
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Fault Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Fault
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Freq Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Freq
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_VD Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_VD
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Fault Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Fault
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Freq Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Freq
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_VD Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_VD
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Fault Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Fault
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Freq Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Freq
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_VD Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_VD
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Fault Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Fault
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Freq Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Freq
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_VD Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_VD
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Long Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Long
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Short Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Short
#  define Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSinceWkUp_Short Rte_Write_SCIM_PVTPT_IO_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSinceWkUp_Short
#  define Rte_Write_Request_SwcModeRequest_PvtReportCtrl_requestedMode Rte_Write_SCIM_PVTPT_IO_Request_SwcModeRequest_PvtReportCtrl_requestedMode
#  define Rte_Write_SCIM_PVTPT_IO_Request_SwcModeRequest_PvtReportCtrl_requestedMode(data) (Rte_SCIM_PVTPT_IO_Request_SwcModeRequest_PvtReportCtrl_requestedMode = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ScimPvtControl_P_Status Rte_Write_SCIM_PVTPT_IO_ScimPvtControl_P_Status
#  define Rte_Write_SCIM_PVTPT_IO_ScimPvtControl_P_Status(data) (Rte_SCIM_PVTPT_IO_ScimPvtControl_P_Status = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) AdiPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_AdiInterface_P_GetAdiPinState_CS AdiInterface_P_GetAdiPinState_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) AdiInterface_P_GetPullUpState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) isPullUpActive_Strong, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) isPullUpActive_Weak, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) isPullUpActive_DAI); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_AdiInterface_P_GetPullUpState_CS AdiInterface_P_GetPullUpState_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) AdiInterface_P_SetPullUp_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL ActivateStrongPullUp, IOHWAB_BOOL ActivateWeakPullUp, IOHWAB_BOOL ActivateDAIPullUp); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_AdiInterface_P_SetPullUp_CS AdiInterface_P_SetPullUp_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) Do12VInterface_P_GetDcdc12VState_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DcDc12vRefVoltage, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDcDc12vActivated, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Do12VInterface_P_GetDcdc12VState_CS Do12VInterface_P_GetDcdc12VState_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDo12VActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Do12VPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS Do12VInterface_P_GetDo12VPinsState_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS Do12VInterface_P_SetDcdc12VActive_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS Do12VInterface_P_SetDo12VLivingActive_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS Do12VInterface_P_SetDo12VParkedActive_CS
#  define RTE_START_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_DOBHS_APPL_CODE) DobhsCtrlInterface_P_1_GetDobhsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DobhsCtrlInterface_P_1_GetDobhsPinState_CS DobhsCtrlInterface_P_1_GetDobhsPinState_CS
#  define RTE_START_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_DOBHS_APPL_CODE) DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DobhsCtrlInterface_P_1_SetDobhsActive_CS DobhsCtrlInterface_P_1_SetDobhsActive_CS
#  define RTE_START_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_DOBHS_APPL_CODE) DobhsCtrlInterface_P_2_GetDobhsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DobhsCtrlInterface_P_2_GetDobhsPinState_CS DobhsCtrlInterface_P_2_GetDobhsPinState_CS
#  define RTE_START_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_DOBHS_APPL_CODE) DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DobhsCtrlInterface_P_2_SetDobhsActive_CS DobhsCtrlInterface_P_2_SetDobhsActive_CS
#  define RTE_START_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_DOBHS_APPL_CODE) DobhsCtrlInterface_P_3_GetDobhsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DobhsCtrlInterface_P_3_GetDobhsPinState_CS DobhsCtrlInterface_P_3_GetDobhsPinState_CS
#  define RTE_START_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_DOBHS_APPL_CODE) DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DobhsCtrlInterface_P_3_SetDobhsActive_CS DobhsCtrlInterface_P_3_SetDobhsActive_CS
#  define RTE_START_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_DOBHS_APPL_CODE) DobhsCtrlInterface_P_4_GetDobhsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DobhsCtrlInterface_P_4_GetDobhsPinState_CS DobhsCtrlInterface_P_4_GetDobhsPinState_CS
#  define RTE_START_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_DOBHS_APPL_CODE) DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DobhsCtrlInterface_P_4_SetDobhsActive_CS DobhsCtrlInterface_P_4_SetDobhsActive_CS
#  define RTE_START_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_DOBHS_APPL_CODE) DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) isDioActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS DobhsDiagInterface_P_GetDobhsPinState_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) DoblsCtrlInterface_P_GetDoblsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS DoblsCtrlInterface_P_GetDoblsPinState_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DoblsCtrlInterface_P_SetDoblsActive_CS DoblsCtrlInterface_P_SetDoblsActive_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPwmDutycycle, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DutyCycle, P2VAR(VGTT_EcuPwmPeriod, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Period, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DiagStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS DowhsInterface_P_GetDoPinStateOne_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DowhsInterface_P_SetDowActive_CS DowhsInterface_P_SetDowActive_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPwmDutycycle, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DutyCycle, P2VAR(VGTT_EcuPwmPeriod, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Period, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DiagStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS DowlsInterface_P_GetDoPinStateOne_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DowlsInterface_P_SetDowActive_CS DowlsInterface_P_SetDowActive_CS
#  define RTE_START_SEC_IOHWAB_ASIL_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_CORE_APPL_CODE) VbatInterface_P_GetVbatVoltage_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_VbatInterface_P_GetVbatVoltage_CS VbatInterface_P_GetVbatVoltage_CS


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_Pvt_ActivateReporting_X1C14_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1C14_Pvt_ActivateReporting_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_X1C14_Pvt_ActivateReporting_v() (Rte_AddrPar_0x29_X1C14_Pvt_ActivateReporting_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define SCIM_PVTPT_IO_START_SEC_CODE
# include "SCIM_PVTPT_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_Runnable_PVTPT_CtrlIo Runnable_PVTPT_CtrlIo
#  define RTE_RUNNABLE_Runnable_PVTPT_ReadIo Runnable_PVTPT_ReadIo
# endif

FUNC(void, SCIM_PVTPT_IO_CODE) Runnable_PVTPT_CtrlIo(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, SCIM_PVTPT_IO_CODE) Runnable_PVTPT_ReadIo(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define SCIM_PVTPT_IO_STOP_SEC_CODE
# include "SCIM_PVTPT_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_AdiInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_Do12VInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_DowxsInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_VbatInterface_I_AdcInFailure (2U)

#  define RTE_E_VbatInterface_I_IoHwAbApplicationError (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_SCIM_PVTPT_IO_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
