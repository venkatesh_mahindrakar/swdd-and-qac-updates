/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_MirrorFolding_HMICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <MirrorFolding_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_MIRRORFOLDING_HMICTRL_TYPE_H
# define _RTE_MIRRORFOLDING_HMICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef A3PosSwitchStatus_Middle
#   define A3PosSwitchStatus_Middle (0U)
#  endif

#  ifndef A3PosSwitchStatus_Lower
#   define A3PosSwitchStatus_Lower (1U)
#  endif

#  ifndef A3PosSwitchStatus_Upper
#   define A3PosSwitchStatus_Upper (2U)
#  endif

#  ifndef A3PosSwitchStatus_Spare
#   define A3PosSwitchStatus_Spare (3U)
#  endif

#  ifndef A3PosSwitchStatus_Spare_01
#   define A3PosSwitchStatus_Spare_01 (4U)
#  endif

#  ifndef A3PosSwitchStatus_Spare_02
#   define A3PosSwitchStatus_Spare_02 (5U)
#  endif

#  ifndef A3PosSwitchStatus_Error
#   define A3PosSwitchStatus_Error (6U)
#  endif

#  ifndef A3PosSwitchStatus_NotAvailable
#   define A3PosSwitchStatus_NotAvailable (7U)
#  endif

#  ifndef MirrorFoldingRequest_NoRequest
#   define MirrorFoldingRequest_NoRequest (0U)
#  endif

#  ifndef MirrorFoldingRequest_UnfoldingRequest
#   define MirrorFoldingRequest_UnfoldingRequest (1U)
#  endif

#  ifndef MirrorFoldingRequest_FoldingRequest
#   define MirrorFoldingRequest_FoldingRequest (2U)
#  endif

#  ifndef MirrorFoldingRequest_Spare01
#   define MirrorFoldingRequest_Spare01 (3U)
#  endif

#  ifndef MirrorFoldingRequest_Spare02
#   define MirrorFoldingRequest_Spare02 (4U)
#  endif

#  ifndef MirrorFoldingRequest_Spare03
#   define MirrorFoldingRequest_Spare03 (5U)
#  endif

#  ifndef MirrorFoldingRequest_Error
#   define MirrorFoldingRequest_Error (6U)
#  endif

#  ifndef MirrorFoldingRequest_NotAvailable
#   define MirrorFoldingRequest_NotAvailable (7U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_MIRRORFOLDING_HMICTRL_TYPE_H */
