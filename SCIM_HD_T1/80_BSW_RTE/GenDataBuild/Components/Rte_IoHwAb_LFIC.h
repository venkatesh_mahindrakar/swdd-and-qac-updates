/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_IoHwAb_LFIC.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <IoHwAb_LFIC>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_IOHWAB_LFIC_H
# define _RTE_IOHWAB_LFIC_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_IoHwAb_LFIC_Type.h"
# include "Rte_DataHandleType.h"


# define IoHwAb_LFIC_START_SEC_CODE
# include "IoHwAb_LFIC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_Get_LFSearchCompleteFlag Get_LFSearchCompleteFlag
#  define RTE_RUNNABLE_IoHwAb_LFIC_10ms_runnable IoHwAb_LFIC_10ms_runnable
#  define RTE_RUNNABLE_LFAntennaDiagnostic_P_GetDiagnosticResult LFAntennaDiagnostic_P_GetDiagnosticResult
#  define RTE_RUNNABLE_RE_IoHwAb_IcuNotification_LFIC_IRQ IoHwAb_IcuNotification_LFIC_IRQ
#  define RTE_RUNNABLE_RE_IohwAb_LFControl_Gpt_Notification_Cbk IohwAb_LFControl_Gpt_Notification_Cbk
#  define RTE_RUNNABLE_RE_LficInit RE_LficInit
#  define RTE_RUNNABLE_RE_SetupDstTelegram RE_SetupDstTelegram
#  define RTE_RUNNABLE_RE_SetupLfPeriodicTelegram RE_SetupLfPeriodicTelegram
#  define RTE_RUNNABLE_RE_SetupLfTelegram RE_SetupLfTelegram
#  define RTE_RUNNABLE_RE_StopLfPeriodicTelegram RE_StopLfPeriodicTelegram
#  define RTE_RUNNABLE_RE_TimeoutTxTelegram RE_TimeoutTxTelegram
#  define RTE_RUNNABLE_Set_LFSearchCompleteFlag Set_LFSearchCompleteFlag
# endif

FUNC(void, IoHwAb_LFIC_CODE) Get_LFSearchCompleteFlag(P2VAR(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) LFSearchCompleteFlag_PEPS); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, IoHwAb_LFIC_CODE) IoHwAb_LFIC_10ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, IoHwAb_LFIC_CODE) LFAntennaDiagnostic_P_GetDiagnosticResult(P2VAR(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) ValidFlag, P2VAR(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) STGStatus, P2VAR(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) STBStatus, P2VAR(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) OCStatus, P2VAR(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) OTStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, IoHwAb_LFIC_CODE) LFAntennaDiagnostic_P_GetDiagnosticResult(P2VAR(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) ValidFlag, P2VAR(DataArrayType_uint8_5, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) STGStatus, P2VAR(DataArrayType_uint8_5, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) STBStatus, P2VAR(DataArrayType_uint8_5, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) OCStatus, P2VAR(DataArrayType_uint8_5, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) OTStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, IoHwAb_LFIC_CODE) IoHwAb_IcuNotification_LFIC_IRQ(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, IoHwAb_LFIC_CODE) IohwAb_LFControl_Gpt_Notification_Cbk(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, IoHwAb_LFIC_CODE) RE_LficInit(uint8 Gain_vehicleOption); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, IoHwAb_LFIC_CODE) RE_SetupDstTelegram(uint8 Dst_Order); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, IoHwAb_LFIC_CODE) RE_SetupLfPeriodicTelegram(uint8 b_AntennaIndex, uint8 b_SearchPattern, uint16 Counter_LFRawDataBit, P2CONST(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_DATA) Buffer_LFRawData, uint16 PeriodicTime); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, IoHwAb_LFIC_CODE) RE_SetupLfPeriodicTelegram(uint8 b_AntennaIndex, uint8 b_SearchPattern, uint16 Counter_LFRawDataBit, P2CONST(ArrayByteSize32, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_DATA) Buffer_LFRawData, uint16 PeriodicTime); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, IoHwAb_LFIC_CODE) RE_SetupLfTelegram(uint8 b_AntennaIndex, uint8 b_SearchPattern, uint16 Counter_LFRawDataBit, P2CONST(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_DATA) Buffer_LFRawData); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, IoHwAb_LFIC_CODE) RE_SetupLfTelegram(uint8 b_AntennaIndex, uint8 b_SearchPattern, uint16 Counter_LFRawDataBit, P2CONST(ArrayByteSize32, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_DATA) Buffer_LFRawData); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, IoHwAb_LFIC_CODE) RE_StopLfPeriodicTelegram(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, IoHwAb_LFIC_CODE) RE_TimeoutTxTelegram(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, IoHwAb_LFIC_CODE) Set_LFSearchCompleteFlag(uint8 LFSearchCompleteFlag_PEPS); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define IoHwAb_LFIC_STOP_SEC_CODE
# include "IoHwAb_LFIC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_IOHWAB_LFIC_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
