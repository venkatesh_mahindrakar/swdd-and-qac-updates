/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_BunkUserInterfaceHigh2_LINMaCtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <BunkUserInterfaceHigh2_LINMaCtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_TYPE_H
# define _RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef AlarmClkID_NotUsed
#   define AlarmClkID_NotUsed (0U)
#  endif

#  ifndef AlarmClkID_Alarm1
#   define AlarmClkID_Alarm1 (1U)
#  endif

#  ifndef AlarmClkID_Alarm2
#   define AlarmClkID_Alarm2 (2U)
#  endif

#  ifndef AlarmClkID_Alarm3
#   define AlarmClkID_Alarm3 (3U)
#  endif

#  ifndef AlarmClkID_Alarm4
#   define AlarmClkID_Alarm4 (4U)
#  endif

#  ifndef AlarmClkID_Alarm5
#   define AlarmClkID_Alarm5 (5U)
#  endif

#  ifndef AlarmClkID_Alarm6
#   define AlarmClkID_Alarm6 (6U)
#  endif

#  ifndef AlarmClkID_Alarm7
#   define AlarmClkID_Alarm7 (7U)
#  endif

#  ifndef AlarmClkID_Alarm8
#   define AlarmClkID_Alarm8 (8U)
#  endif

#  ifndef AlarmClkID_Alarm9
#   define AlarmClkID_Alarm9 (9U)
#  endif

#  ifndef AlarmClkID_Alarm10
#   define AlarmClkID_Alarm10 (10U)
#  endif

#  ifndef AlarmClkID_Error
#   define AlarmClkID_Error (126U)
#  endif

#  ifndef AlarmClkID_NotAvailable
#   define AlarmClkID_NotAvailable (127U)
#  endif

#  ifndef AlarmClkStat_Inactive
#   define AlarmClkStat_Inactive (0U)
#  endif

#  ifndef AlarmClkStat_Active
#   define AlarmClkStat_Active (1U)
#  endif

#  ifndef AlarmClkStat_NoUsed
#   define AlarmClkStat_NoUsed (2U)
#  endif

#  ifndef AlarmClkStat_Spare
#   define AlarmClkStat_Spare (3U)
#  endif

#  ifndef AlarmClkType_NoAudibleNotification
#   define AlarmClkType_NoAudibleNotification (0U)
#  endif

#  ifndef AlarmClkType_Buzzer
#   define AlarmClkType_Buzzer (1U)
#  endif

#  ifndef AlarmClkType_Radio
#   define AlarmClkType_Radio (2U)
#  endif

#  ifndef AlarmClkType_Reserved
#   define AlarmClkType_Reserved (3U)
#  endif

#  ifndef AlarmClkType_Reserved_01
#   define AlarmClkType_Reserved_01 (4U)
#  endif

#  ifndef AlarmClkType_Reserved_02
#   define AlarmClkType_Reserved_02 (5U)
#  endif

#  ifndef AlarmClkType_Error
#   define AlarmClkType_Error (6U)
#  endif

#  ifndef AlarmClkType_NotAvailable
#   define AlarmClkType_NotAvailable (7U)
#  endif

#  ifndef BTStatus_BTOff
#   define BTStatus_BTOff (0U)
#  endif

#  ifndef BTStatus_BTOnAndNoDeviceConnected
#   define BTStatus_BTOnAndNoDeviceConnected (1U)
#  endif

#  ifndef BTStatus_BTOnAndDeviceConnected
#   define BTStatus_BTOnAndDeviceConnected (2U)
#  endif

#  ifndef BTStatus_NotAvailable
#   define BTStatus_NotAvailable (3U)
#  endif

#  ifndef Inactive
#   define Inactive (0U)
#  endif

#  ifndef Diagnostic
#   define Diagnostic (1U)
#  endif

#  ifndef SwitchDetection
#   define SwitchDetection (2U)
#  endif

#  ifndef ApplicationMonitoring
#   define ApplicationMonitoring (3U)
#  endif

#  ifndef Calibration
#   define Calibration (4U)
#  endif

#  ifndef Spare1
#   define Spare1 (5U)
#  endif

#  ifndef Error
#   define Error (6U)
#  endif

#  ifndef NotAvailable
#   define NotAvailable (7U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED
#   define DEM_EVENT_STATUS_PASSED (0U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED
#   define DEM_EVENT_STATUS_FAILED (1U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED
#   define DEM_EVENT_STATUS_PREPASSED (2U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED
#   define DEM_EVENT_STATUS_PREFAILED (3U)
#  endif

#  ifndef DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED
#   define DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
#  endif

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef IL_Mode_OFF
#   define IL_Mode_OFF (0U)
#  endif

#  ifndef IL_Mode_NightDriving
#   define IL_Mode_NightDriving (1U)
#  endif

#  ifndef IL_Mode_Resting
#   define IL_Mode_Resting (2U)
#  endif

#  ifndef IL_Mode_Max
#   define IL_Mode_Max (3U)
#  endif

#  ifndef IL_Mode_spare_1
#   define IL_Mode_spare_1 (4U)
#  endif

#  ifndef IL_Mode_spare_2
#   define IL_Mode_spare_2 (5U)
#  endif

#  ifndef IL_Mode_ErrorIndicator
#   define IL_Mode_ErrorIndicator (6U)
#  endif

#  ifndef IL_Mode_NotAvailable
#   define IL_Mode_NotAvailable (7U)
#  endif

#  ifndef OffOn_Off
#   define OffOn_Off (0U)
#  endif

#  ifndef OffOn_On
#   define OffOn_On (1U)
#  endif

#  ifndef OffOn_Error
#   define OffOn_Error (2U)
#  endif

#  ifndef OffOn_NotAvailable
#   define OffOn_NotAvailable (3U)
#  endif

#  ifndef ParkHeaterTimer_cmd_NoAction
#   define ParkHeaterTimer_cmd_NoAction (0U)
#  endif

#  ifndef ParkHeaterTimer_cmd_TimerEnable
#   define ParkHeaterTimer_cmd_TimerEnable (1U)
#  endif

#  ifndef ParkHeaterTimer_cmd_TimerDisable
#   define ParkHeaterTimer_cmd_TimerDisable (2U)
#  endif

#  ifndef ParkHeaterTimer_cmd_Spare
#   define ParkHeaterTimer_cmd_Spare (3U)
#  endif

#  ifndef ParkHeaterTimer_cmd_Spare_01
#   define ParkHeaterTimer_cmd_Spare_01 (4U)
#  endif

#  ifndef ParkHeaterTimer_cmd_Spare_02
#   define ParkHeaterTimer_cmd_Spare_02 (5U)
#  endif

#  ifndef ParkHeaterTimer_cmd_Error
#   define ParkHeaterTimer_cmd_Error (6U)
#  endif

#  ifndef ParkHeaterTimer_cmd_NotAvailable
#   define ParkHeaterTimer_cmd_NotAvailable (7U)
#  endif

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_TYPE_H */
