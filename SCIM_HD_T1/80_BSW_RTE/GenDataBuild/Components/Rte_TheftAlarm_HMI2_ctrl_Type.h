/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_TheftAlarm_HMI2_ctrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <TheftAlarm_HMI2_ctrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_THEFTALARM_HMI2_CTRL_TYPE_H
# define _RTE_THEFTALARM_HMI2_CTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef AlarmStatus_stat_Idle
#   define AlarmStatus_stat_Idle (0U)
#  endif

#  ifndef AlarmStatus_stat_SetMode
#   define AlarmStatus_stat_SetMode (1U)
#  endif

#  ifndef AlarmStatus_stat_ReduceSetMode
#   define AlarmStatus_stat_ReduceSetMode (2U)
#  endif

#  ifndef AlarmStatus_stat_UnsetMode
#   define AlarmStatus_stat_UnsetMode (3U)
#  endif

#  ifndef AlarmStatus_stat_PanicMode
#   define AlarmStatus_stat_PanicMode (4U)
#  endif

#  ifndef AlarmStatus_stat_AlarmMode
#   define AlarmStatus_stat_AlarmMode (5U)
#  endif

#  ifndef AlarmStatus_stat_ServiceMode
#   define AlarmStatus_stat_ServiceMode (6U)
#  endif

#  ifndef AlarmStatus_stat_TamperMode
#   define AlarmStatus_stat_TamperMode (7U)
#  endif

#  ifndef AlarmStatus_stat_Spare_01
#   define AlarmStatus_stat_Spare_01 (8U)
#  endif

#  ifndef AlarmStatus_stat_Spare_02
#   define AlarmStatus_stat_Spare_02 (9U)
#  endif

#  ifndef AlarmStatus_stat_Spare_03
#   define AlarmStatus_stat_Spare_03 (10U)
#  endif

#  ifndef AlarmStatus_stat_Spare_04
#   define AlarmStatus_stat_Spare_04 (11U)
#  endif

#  ifndef AlarmStatus_stat_Spare_05
#   define AlarmStatus_stat_Spare_05 (12U)
#  endif

#  ifndef AlarmStatus_stat_Spare_06
#   define AlarmStatus_stat_Spare_06 (13U)
#  endif

#  ifndef AlarmStatus_stat_Error
#   define AlarmStatus_stat_Error (14U)
#  endif

#  ifndef AlarmStatus_stat_NotAvailable
#   define AlarmStatus_stat_NotAvailable (15U)
#  endif

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef KeyAuthentication_stat_decrypt_KeyNotAuthenticated
#   define KeyAuthentication_stat_decrypt_KeyNotAuthenticated (0U)
#  endif

#  ifndef KeyAuthentication_stat_decrypt_KeyAuthenticated
#   define KeyAuthentication_stat_decrypt_KeyAuthenticated (1U)
#  endif

#  ifndef KeyAuthentication_stat_decrypt_Spare1
#   define KeyAuthentication_stat_decrypt_Spare1 (2U)
#  endif

#  ifndef KeyAuthentication_stat_decrypt_Spare2
#   define KeyAuthentication_stat_decrypt_Spare2 (3U)
#  endif

#  ifndef KeyAuthentication_stat_decrypt_Spare3
#   define KeyAuthentication_stat_decrypt_Spare3 (4U)
#  endif

#  ifndef KeyAuthentication_stat_decrypt_Spare4
#   define KeyAuthentication_stat_decrypt_Spare4 (5U)
#  endif

#  ifndef KeyAuthentication_stat_decrypt_Error
#   define KeyAuthentication_stat_decrypt_Error (6U)
#  endif

#  ifndef KeyAuthentication_stat_decrypt_NotAvailable
#   define KeyAuthentication_stat_decrypt_NotAvailable (7U)
#  endif

#  ifndef KeyPosition_KeyOut
#   define KeyPosition_KeyOut (0U)
#  endif

#  ifndef KeyPosition_IgnitionKeyInOffPosition
#   define KeyPosition_IgnitionKeyInOffPosition (1U)
#  endif

#  ifndef KeyPosition_IgnitionKeyInAccessoryPosition
#   define KeyPosition_IgnitionKeyInAccessoryPosition (2U)
#  endif

#  ifndef KeyPosition_IgnitionKeyIn15PositionNormalDrivingPosition
#   define KeyPosition_IgnitionKeyIn15PositionNormalDrivingPosition (3U)
#  endif

#  ifndef KeyPosition_IgnitionKeyInPreheatPosition
#   define KeyPosition_IgnitionKeyInPreheatPosition (4U)
#  endif

#  ifndef KeyPosition_IgnitionKeyInCrankPosition
#   define KeyPosition_IgnitionKeyInCrankPosition (5U)
#  endif

#  ifndef KeyPosition_ErrorIndicator
#   define KeyPosition_ErrorIndicator (6U)
#  endif

#  ifndef KeyPosition_NotAvailable
#   define KeyPosition_NotAvailable (7U)
#  endif

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

#  ifndef ReducedSetMode_rqst_decrypt_Idle
#   define ReducedSetMode_rqst_decrypt_Idle (0U)
#  endif

#  ifndef ReducedSetMode_rqst_decrypt_ReducedSetModeRequested
#   define ReducedSetMode_rqst_decrypt_ReducedSetModeRequested (1U)
#  endif

#  ifndef ReducedSetMode_rqst_decrypt_ReducedSetModeNOT_Requested
#   define ReducedSetMode_rqst_decrypt_ReducedSetModeNOT_Requested (2U)
#  endif

#  ifndef ReducedSetMode_rqst_decrypt_Spare1
#   define ReducedSetMode_rqst_decrypt_Spare1 (3U)
#  endif

#  ifndef ReducedSetMode_rqst_decrypt_Spare2
#   define ReducedSetMode_rqst_decrypt_Spare2 (4U)
#  endif

#  ifndef ReducedSetMode_rqst_decrypt_Spare3
#   define ReducedSetMode_rqst_decrypt_Spare3 (5U)
#  endif

#  ifndef ReducedSetMode_rqst_decrypt_Error
#   define ReducedSetMode_rqst_decrypt_Error (6U)
#  endif

#  ifndef ReducedSetMode_rqst_decrypt_NotAvailable
#   define ReducedSetMode_rqst_decrypt_NotAvailable (7U)
#  endif

#  ifndef TheftAlarmAct_rqst_decrypt_Idle
#   define TheftAlarmAct_rqst_decrypt_Idle (0U)
#  endif

#  ifndef TheftAlarmAct_rqst_decrypt_TheftAlarmActReqstFromKeyfob
#   define TheftAlarmAct_rqst_decrypt_TheftAlarmActReqstFromKeyfob (1U)
#  endif

#  ifndef TheftAlarmAct_rqst_decrypt_TheftAlarmDeactReqstFromKeyfob
#   define TheftAlarmAct_rqst_decrypt_TheftAlarmDeactReqstFromKeyfob (2U)
#  endif

#  ifndef TheftAlarmAct_rqst_decrypt_TheftAlarmActivationRequestFromOtherSource
#   define TheftAlarmAct_rqst_decrypt_TheftAlarmActivationRequestFromOtherSource (3U)
#  endif

#  ifndef TheftAlarmAct_rqst_decrypt_TheftAlarmDeactReqstFromOtherSource
#   define TheftAlarmAct_rqst_decrypt_TheftAlarmDeactReqstFromOtherSource (4U)
#  endif

#  ifndef TheftAlarmAct_rqst_decrypt_Spare
#   define TheftAlarmAct_rqst_decrypt_Spare (5U)
#  endif

#  ifndef TheftAlarmAct_rqst_decrypt_Error
#   define TheftAlarmAct_rqst_decrypt_Error (6U)
#  endif

#  ifndef TheftAlarmAct_rqst_decrypt_NotAvailable
#   define TheftAlarmAct_rqst_decrypt_NotAvailable (7U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

#  ifndef VehicleMode_Hibernate
#   define VehicleMode_Hibernate (0U)
#  endif

#  ifndef VehicleMode_Parked
#   define VehicleMode_Parked (1U)
#  endif

#  ifndef VehicleMode_Living
#   define VehicleMode_Living (2U)
#  endif

#  ifndef VehicleMode_Accessory
#   define VehicleMode_Accessory (3U)
#  endif

#  ifndef VehicleMode_PreRunning
#   define VehicleMode_PreRunning (4U)
#  endif

#  ifndef VehicleMode_Cranking
#   define VehicleMode_Cranking (5U)
#  endif

#  ifndef VehicleMode_Running
#   define VehicleMode_Running (6U)
#  endif

#  ifndef VehicleMode_Spare_1
#   define VehicleMode_Spare_1 (7U)
#  endif

#  ifndef VehicleMode_Spare_2
#   define VehicleMode_Spare_2 (8U)
#  endif

#  ifndef VehicleMode_Spare_3
#   define VehicleMode_Spare_3 (9U)
#  endif

#  ifndef VehicleMode_Spare_4
#   define VehicleMode_Spare_4 (10U)
#  endif

#  ifndef VehicleMode_Spare_5
#   define VehicleMode_Spare_5 (11U)
#  endif

#  ifndef VehicleMode_Spare_6
#   define VehicleMode_Spare_6 (12U)
#  endif

#  ifndef VehicleMode_Spare_7
#   define VehicleMode_Spare_7 (13U)
#  endif

#  ifndef VehicleMode_Error
#   define VehicleMode_Error (14U)
#  endif

#  ifndef VehicleMode_NotAvailable
#   define VehicleMode_NotAvailable (15U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_THEFTALARM_HMI2_CTRL_TYPE_H */
