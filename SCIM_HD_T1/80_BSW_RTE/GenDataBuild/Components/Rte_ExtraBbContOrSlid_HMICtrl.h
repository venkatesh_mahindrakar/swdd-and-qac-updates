/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_ExtraBbContOrSlid_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <ExtraBbContOrSlid_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_EXTRABBCONTORSLID_HMICTRL_H
# define _RTE_EXTRABBCONTORSLID_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_ExtraBbContOrSlid_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraBbContOrSlid_HMICtrl_ContUnlock_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraBbContOrSlid_HMICtrl_Slid5thWheel_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ContUnlockSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Slid5thWheelSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_BBContainerUnlockRequest_BBContainerUnlockRequest (3U)
#  define Rte_InitValue_BBSlidable5thWheelRequest_BBSlidable5thWheelRequest (3U)
#  define Rte_InitValue_ContUnlockSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_ContUnlock_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_ExtraBBContainerUnlockStatus_ExtraBBContainerUnlockStatus (3U)
#  define Rte_InitValue_ExtraBBSlidable5thWheelStatus_ExtraBBSlidable5thWheelStatus (3U)
#  define Rte_InitValue_Slid5thWheelSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_Slid5thWheel_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_SwcActivation_IgnitionOn_IgnitionOn (1U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraBbContOrSlid_HMICtrl_ExtraBBContainerUnlockStatus_ExtraBBContainerUnlockStatus(P2VAR(InactiveActive_T, AUTOMATIC, RTE_EXTRABBCONTORSLID_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraBbContOrSlid_HMICtrl_ExtraBBSlidable5thWheelStatus_ExtraBBSlidable5thWheelStatus(P2VAR(InactiveActive_T, AUTOMATIC, RTE_EXTRABBCONTORSLID_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExtraBbContOrSlid_HMICtrl_BBContainerUnlockRequest_BBContainerUnlockRequest(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExtraBbContOrSlid_HMICtrl_BBSlidable5thWheelRequest_BBSlidable5thWheelRequest(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ContUnlockSwitchStatus_A2PosSwitchStatus Rte_Read_ExtraBbContOrSlid_HMICtrl_ContUnlockSwitchStatus_A2PosSwitchStatus
#  define Rte_Read_ExtraBbContOrSlid_HMICtrl_ContUnlockSwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_ContUnlockSwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ExtraBBContainerUnlockStatus_ExtraBBContainerUnlockStatus Rte_Read_ExtraBbContOrSlid_HMICtrl_ExtraBBContainerUnlockStatus_ExtraBBContainerUnlockStatus
#  define Rte_Read_ExtraBBSlidable5thWheelStatus_ExtraBBSlidable5thWheelStatus Rte_Read_ExtraBbContOrSlid_HMICtrl_ExtraBBSlidable5thWheelStatus_ExtraBBSlidable5thWheelStatus
#  define Rte_Read_Slid5thWheelSwitchStatus_A2PosSwitchStatus Rte_Read_ExtraBbContOrSlid_HMICtrl_Slid5thWheelSwitchStatus_A2PosSwitchStatus
#  define Rte_Read_ExtraBbContOrSlid_HMICtrl_Slid5thWheelSwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_Slid5thWheelSwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_IgnitionOn_IgnitionOn Rte_Read_ExtraBbContOrSlid_HMICtrl_SwcActivation_IgnitionOn_IgnitionOn
#  define Rte_Read_ExtraBbContOrSlid_HMICtrl_SwcActivation_IgnitionOn_IgnitionOn(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_BBContainerUnlockRequest_BBContainerUnlockRequest Rte_Write_ExtraBbContOrSlid_HMICtrl_BBContainerUnlockRequest_BBContainerUnlockRequest
#  define Rte_Write_BBSlidable5thWheelRequest_BBSlidable5thWheelRequest Rte_Write_ExtraBbContOrSlid_HMICtrl_BBSlidable5thWheelRequest_BBSlidable5thWheelRequest
#  define Rte_Write_ContUnlock_DeviceIndication_DeviceIndication Rte_Write_ExtraBbContOrSlid_HMICtrl_ContUnlock_DeviceIndication_DeviceIndication
#  define Rte_Write_ExtraBbContOrSlid_HMICtrl_ContUnlock_DeviceIndication_DeviceIndication(data) (Rte_ExtraBbContOrSlid_HMICtrl_ContUnlock_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Slid5thWheel_DeviceIndication_DeviceIndication Rte_Write_ExtraBbContOrSlid_HMICtrl_Slid5thWheel_DeviceIndication_DeviceIndication
#  define Rte_Write_ExtraBbContOrSlid_HMICtrl_Slid5thWheel_DeviceIndication_DeviceIndication(data) (Rte_ExtraBbContOrSlid_HMICtrl_Slid5thWheel_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_ContainerUnlockHMIDeviceType_P1CXO_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CXO_ContainerUnlockHMIDeviceType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Slidable5thWheelHMIDeviceType_P1CXP_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CXP_Slidable5thWheelHMIDeviceType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Slid5thWheelTimeoutForReq_P1DV9_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DV9_Slid5thWheelTimeoutForReq_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1CXO_ContainerUnlockHMIDeviceType_v() (Rte_AddrPar_0x2B_P1CXO_ContainerUnlockHMIDeviceType_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CXP_Slidable5thWheelHMIDeviceType_v() (Rte_AddrPar_0x2B_P1CXP_Slidable5thWheelHMIDeviceType_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DV9_Slid5thWheelTimeoutForReq_v() (Rte_AddrPar_0x2B_P1DV9_Slid5thWheelTimeoutForReq_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define ExtraBbContOrSlid_HMICtrl_START_SEC_CODE
# include "ExtraBbContOrSlid_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_ExtraBbContOrSlid_HMICtrl_20ms_runnable ExtraBbContOrSlid_HMICtrl_20ms_runnable
#  define RTE_RUNNABLE_ExtraBbContOrSlid_HMICtrl_init ExtraBbContOrSlid_HMICtrl_init
# endif

FUNC(void, ExtraBbContOrSlid_HMICtrl_CODE) ExtraBbContOrSlid_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, ExtraBbContOrSlid_HMICtrl_CODE) ExtraBbContOrSlid_HMICtrl_init(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define ExtraBbContOrSlid_HMICtrl_STOP_SEC_CODE
# include "ExtraBbContOrSlid_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_EXTRABBCONTORSLID_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
