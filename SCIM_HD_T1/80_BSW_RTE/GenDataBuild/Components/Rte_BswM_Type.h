/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_BswM_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <BswM>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_BSWM_TYPE_H
# define _RTE_BSWM_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef CAN_NormalCom
#   define CAN_NormalCom (0U)
#  endif

#  ifndef CAN_BusOff
#   define CAN_BusOff (1U)
#  endif

#  ifndef LIN1_Table1
#   define LIN1_Table1 (1U)
#  endif

#  ifndef LIN1_Table2
#   define LIN1_Table2 (2U)
#  endif

#  ifndef LIN1_Table_E
#   define LIN1_Table_E (3U)
#  endif

#  ifndef LIN1_MasterReq_SlaveResp_Table1
#   define LIN1_MasterReq_SlaveResp_Table1 (4U)
#  endif

#  ifndef LIN1_MasterReq_SlaveResp_Table2
#   define LIN1_MasterReq_SlaveResp_Table2 (5U)
#  endif

#  ifndef LIN1_NULL
#   define LIN1_NULL (0U)
#  endif

#  ifndef LIN1_MasterReq_SlaveResp
#   define LIN1_MasterReq_SlaveResp (6U)
#  endif

#  ifndef LIN2_NULL
#   define LIN2_NULL (0U)
#  endif

#  ifndef LIN2_TABLE0
#   define LIN2_TABLE0 (1U)
#  endif

#  ifndef LIN2_TABLE_E
#   define LIN2_TABLE_E (2U)
#  endif

#  ifndef LIN2_MasterReq_SlaveResp_TABLE0
#   define LIN2_MasterReq_SlaveResp_TABLE0 (3U)
#  endif

#  ifndef LIN2_MasterReq_SlaveResp
#   define LIN2_MasterReq_SlaveResp (4U)
#  endif

#  ifndef LIN3_NULL
#   define LIN3_NULL (0U)
#  endif

#  ifndef LIN3_TABLE1
#   define LIN3_TABLE1 (1U)
#  endif

#  ifndef LIN3_TABLE2
#   define LIN3_TABLE2 (2U)
#  endif

#  ifndef LIN3_TABLE_E
#   define LIN3_TABLE_E (3U)
#  endif

#  ifndef LIN3_MasterReq_SlaveResp_Table1
#   define LIN3_MasterReq_SlaveResp_Table1 (4U)
#  endif

#  ifndef LIN3_MasterReq_SlaveResp_Table2
#   define LIN3_MasterReq_SlaveResp_Table2 (5U)
#  endif

#  ifndef LIN3_MasterReq_SlaveResp
#   define LIN3_MasterReq_SlaveResp (6U)
#  endif

#  ifndef LIN4_NULL
#   define LIN4_NULL (0U)
#  endif

#  ifndef LIN4_MasterReq_SlaveResp_Table1
#   define LIN4_MasterReq_SlaveResp_Table1 (4U)
#  endif

#  ifndef LIN4_TABLE1
#   define LIN4_TABLE1 (1U)
#  endif

#  ifndef LIN4_TABLE2
#   define LIN4_TABLE2 (2U)
#  endif

#  ifndef LIN4_TABLE_E
#   define LIN4_TABLE_E (3U)
#  endif

#  ifndef LIN4_MasterReq_SlaveResp_Table2
#   define LIN4_MasterReq_SlaveResp_Table2 (5U)
#  endif

#  ifndef LIN4_MasterReq_SlaveResp
#   define LIN4_MasterReq_SlaveResp (6U)
#  endif

#  ifndef LIN5_NULL
#   define LIN5_NULL (0U)
#  endif

#  ifndef LIN5_TABLE1
#   define LIN5_TABLE1 (1U)
#  endif

#  ifndef LIN5_MasterReq_SlaveResp_Table1
#   define LIN5_MasterReq_SlaveResp_Table1 (4U)
#  endif

#  ifndef LIN5_MasterReq_SlaveResp_Table2
#   define LIN5_MasterReq_SlaveResp_Table2 (5U)
#  endif

#  ifndef LIN5_MasterReq_SlaveResp
#   define LIN5_MasterReq_SlaveResp (6U)
#  endif

#  ifndef LIN5_TABLE2
#   define LIN5_TABLE2 (2U)
#  endif

#  ifndef LIN5_TABLE_E
#   define LIN5_TABLE_E (3U)
#  endif

#  ifndef LIN6_NULL
#   define LIN6_NULL (0U)
#  endif

#  ifndef LIN6_TABLE0
#   define LIN6_TABLE0 (1U)
#  endif

#  ifndef LIN6_MasterReq_SlaveResp_Table0
#   define LIN6_MasterReq_SlaveResp_Table0 (3U)
#  endif

#  ifndef LIN6_MasterReq_SlaveResp
#   define LIN6_MasterReq_SlaveResp (2U)
#  endif

#  ifndef LIN7_NULL
#   define LIN7_NULL (0U)
#  endif

#  ifndef LIN7_TABLE0
#   define LIN7_TABLE0 (1U)
#  endif

#  ifndef LIN7_MasterReq_SlaveResp_Table0
#   define LIN7_MasterReq_SlaveResp_Table0 (3U)
#  endif

#  ifndef LIN7_MasterReq_SlaveResp
#   define LIN7_MasterReq_SlaveResp (2U)
#  endif

#  ifndef LIN8_NULL
#   define LIN8_NULL (0U)
#  endif

#  ifndef LIN8_TABLE0
#   define LIN8_TABLE0 (1U)
#  endif

#  ifndef LIN8_MasterReq_SlaveResp_Table0
#   define LIN8_MasterReq_SlaveResp_Table0 (3U)
#  endif

#  ifndef LIN8_MasterReq_SlaveResp
#   define LIN8_MasterReq_SlaveResp (2U)
#  endif

#  ifndef LIN_SchTable_INIT
#   define LIN_SchTable_INIT (0U)
#  endif

#  ifndef LIN_SchTable_EndOfNotification
#   define LIN_SchTable_EndOfNotification (1U)
#  endif

#  ifndef LIN_SchTable_StartOfIndication
#   define LIN_SchTable_StartOfIndication (2U)
#  endif

#  ifndef NvmWriteAll_NoRequest
#   define NvmWriteAll_NoRequest (0U)
#  endif

#  ifndef NvmWriteAll_Request
#   define NvmWriteAll_Request (1U)
#  endif

#  ifndef PvtReport_Enabled
#   define PvtReport_Enabled (0U)
#  endif

#  ifndef PvtReport_Disabled
#   define PvtReport_Disabled (1U)
#  endif

#  ifndef ResetProcess_Inprogress
#   define ResetProcess_Inprogress (2U)
#  endif

#  ifndef ResetProcess_Completed
#   define ResetProcess_Completed (3U)
#  endif

#  ifndef ResetProcess_Started
#   define ResetProcess_Started (1U)
#  endif

#  ifndef ResetProcess_Idle
#   define ResetProcess_Idle (0U)
#  endif

#  ifndef STARTUP
#   define STARTUP (0U)
#  endif

#  ifndef RUN
#   define RUN (1U)
#  endif

#  ifndef POSTRUN
#   define POSTRUN (2U)
#  endif

#  ifndef WAKEUP
#   define WAKEUP (3U)
#  endif

#  ifndef SHUTDOWN
#   define SHUTDOWN (4U)
#  endif

#  ifndef DCM_ENUM_NONE
#   define DCM_ENUM_NONE (0U)
#  endif

#  ifndef DCM_ENUM_HARD
#   define DCM_ENUM_HARD (1U)
#  endif

#  ifndef DCM_ENUM_KEYONOFF
#   define DCM_ENUM_KEYONOFF (2U)
#  endif

#  ifndef DCM_ENUM_SOFT
#   define DCM_ENUM_SOFT (3U)
#  endif

#  ifndef DCM_ENUM_JUMPTOBOOTLOADER
#   define DCM_ENUM_JUMPTOBOOTLOADER (4U)
#  endif

#  ifndef DCM_ENUM_JUMPTOSYSSUPPLIERBOOTLOADER
#   define DCM_ENUM_JUMPTOSYSSUPPLIERBOOTLOADER (5U)
#  endif

#  ifndef DCM_ENUM_EXECUTE
#   define DCM_ENUM_EXECUTE (6U)
#  endif

# endif /* RTE_CORE */


/**********************************************************************************************************************
 * Definitions for Mode Management
 *********************************************************************************************************************/
# ifndef RTE_MODETYPE_BswMRteMDG_CanBusOff
#  define RTE_MODETYPE_BswMRteMDG_CanBusOff
typedef BswM_BswMRteMDG_CanBusOff Rte_ModeType_BswMRteMDG_CanBusOff;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN1Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN1Schedule
typedef BswM_BswMRteMDG_LIN1Schedule Rte_ModeType_BswMRteMDG_LIN1Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN2Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN2Schedule
typedef BswM_BswMRteMDG_LIN2Schedule Rte_ModeType_BswMRteMDG_LIN2Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN3Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN3Schedule
typedef BswM_BswMRteMDG_LIN3Schedule Rte_ModeType_BswMRteMDG_LIN3Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN4Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN4Schedule
typedef BswM_BswMRteMDG_LIN4Schedule Rte_ModeType_BswMRteMDG_LIN4Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN5Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN5Schedule
typedef BswM_BswMRteMDG_LIN5Schedule Rte_ModeType_BswMRteMDG_LIN5Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN6Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN6Schedule
typedef BswM_BswMRteMDG_LIN6Schedule Rte_ModeType_BswMRteMDG_LIN6Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN7Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN7Schedule
typedef BswM_BswMRteMDG_LIN7Schedule Rte_ModeType_BswMRteMDG_LIN7Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN8Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN8Schedule
typedef BswM_BswMRteMDG_LIN8Schedule Rte_ModeType_BswMRteMDG_LIN8Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LINSchTableState
#  define RTE_MODETYPE_BswMRteMDG_LINSchTableState
typedef BswM_BswMRteMDG_LINSchTableState Rte_ModeType_BswMRteMDG_LINSchTableState;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_NvmWriteAllRequest
#  define RTE_MODETYPE_BswMRteMDG_NvmWriteAllRequest
typedef BswM_BswMRteMDG_NvmWriteAllRequest Rte_ModeType_BswMRteMDG_NvmWriteAllRequest;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_PvtReport_X1C14
#  define RTE_MODETYPE_BswMRteMDG_PvtReport_X1C14
typedef BswM_BswMRteMDG_PvtReport_X1C14 Rte_ModeType_BswMRteMDG_PvtReport_X1C14;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_ResetProcess
#  define RTE_MODETYPE_BswMRteMDG_ResetProcess
typedef BswM_BswMRteMDG_ResetProcess Rte_ModeType_BswMRteMDG_ResetProcess;
# endif
# ifndef RTE_MODETYPE_DcmEcuReset
#  define RTE_MODETYPE_DcmEcuReset
typedef Dcm_EcuResetType Rte_ModeType_DcmEcuReset;
# endif
# ifndef RTE_MODETYPE_ESH_Mode
#  define RTE_MODETYPE_ESH_Mode
typedef BswM_ESH_Mode Rte_ModeType_ESH_Mode;
# endif

# define RTE_MODE_BswM_BswMRteMDG_CanBusOff_CAN_BusOff (0U)
# ifndef RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff
#  define RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff (0U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_CanBusOff_CAN_NormalCom (1U)
# ifndef RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom
#  define RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom (1U)
# endif
# define RTE_TRANSITION_BswM_BswMRteMDG_CanBusOff (2U)
# ifndef RTE_TRANSITION_BswMRteMDG_CanBusOff
#  define RTE_TRANSITION_BswMRteMDG_CanBusOff (2U)
# endif

# define RTE_MODE_BswM_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1 (1U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2 (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2 (2U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN1Schedule_LIN1_NULL (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_NULL
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_NULL (3U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN1Schedule_LIN1_Table1 (4U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table1
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table1 (4U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN1Schedule_LIN1_Table2 (5U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table2
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table2 (5U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN1Schedule_LIN1_Table_E (6U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table_E
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table_E (6U)
# endif
# define RTE_TRANSITION_BswM_BswMRteMDG_LIN1Schedule (7U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN1Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN1Schedule (7U)
# endif

# define RTE_MODE_BswM_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0
#  define RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0 (1U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN2Schedule_LIN2_NULL (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_NULL
#  define RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_NULL (2U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN2Schedule_LIN2_TABLE0 (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE0
#  define RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE0 (3U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E (4U)
# ifndef RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E
#  define RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E (4U)
# endif
# define RTE_TRANSITION_BswM_BswMRteMDG_LIN2Schedule (5U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN2Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN2Schedule (5U)
# endif

# define RTE_MODE_BswM_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1 (1U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2 (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2 (2U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN3Schedule_LIN3_NULL (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_NULL
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_NULL (3U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN3Schedule_LIN3_TABLE1 (4U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE1
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE1 (4U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN3Schedule_LIN3_TABLE2 (5U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE2
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE2 (5U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E (6U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E (6U)
# endif
# define RTE_TRANSITION_BswM_BswMRteMDG_LIN3Schedule (7U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN3Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN3Schedule (7U)
# endif

# define RTE_MODE_BswM_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1 (1U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2 (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2 (2U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN4Schedule_LIN4_NULL (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_NULL
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_NULL (3U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN4Schedule_LIN4_TABLE1 (4U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE1
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE1 (4U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN4Schedule_LIN4_TABLE2 (5U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE2
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE2 (5U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E (6U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E (6U)
# endif
# define RTE_TRANSITION_BswM_BswMRteMDG_LIN4Schedule (7U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN4Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN4Schedule (7U)
# endif

# define RTE_MODE_BswM_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1 (1U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2 (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2 (2U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN5Schedule_LIN5_NULL (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_NULL
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_NULL (3U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN5Schedule_LIN5_TABLE1 (4U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE1
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE1 (4U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN5Schedule_LIN5_TABLE2 (5U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE2
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE2 (5U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E (6U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E (6U)
# endif
# define RTE_TRANSITION_BswM_BswMRteMDG_LIN5Schedule (7U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN5Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN5Schedule (7U)
# endif

# define RTE_MODE_BswM_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp_Table0 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp_Table0
#  define RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp_Table0 (1U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN6Schedule_LIN6_NULL (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_NULL
#  define RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_NULL (2U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN6Schedule_LIN6_TABLE0 (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_TABLE0
#  define RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_TABLE0 (3U)
# endif
# define RTE_TRANSITION_BswM_BswMRteMDG_LIN6Schedule (4U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN6Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN6Schedule (4U)
# endif

# define RTE_MODE_BswM_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp_Table0 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp_Table0
#  define RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp_Table0 (1U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN7Schedule_LIN7_NULL (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_NULL
#  define RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_NULL (2U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN7Schedule_LIN7_TABLE0 (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_TABLE0
#  define RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_TABLE0 (3U)
# endif
# define RTE_TRANSITION_BswM_BswMRteMDG_LIN7Schedule (4U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN7Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN7Schedule (4U)
# endif

# define RTE_MODE_BswM_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp_Table0 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp_Table0
#  define RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp_Table0 (1U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN8Schedule_LIN8_NULL (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_NULL
#  define RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_NULL (2U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LIN8Schedule_LIN8_TABLE0 (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_TABLE0
#  define RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_TABLE0 (3U)
# endif
# define RTE_TRANSITION_BswM_BswMRteMDG_LIN8Schedule (4U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN8Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN8Schedule (4U)
# endif

# define RTE_MODE_BswM_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification (0U)
# ifndef RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification
#  define RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification (0U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT (1U)
# ifndef RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT
#  define RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT (1U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication (2U)
# ifndef RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication
#  define RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication (2U)
# endif
# define RTE_TRANSITION_BswM_BswMRteMDG_LINSchTableState (3U)
# ifndef RTE_TRANSITION_BswMRteMDG_LINSchTableState
#  define RTE_TRANSITION_BswMRteMDG_LINSchTableState (3U)
# endif

# define RTE_MODE_BswM_BswMRteMDG_NvmWriteAllRequest_NvmWriteAll_NoRequest (0U)
# ifndef RTE_MODE_BswMRteMDG_NvmWriteAllRequest_NvmWriteAll_NoRequest
#  define RTE_MODE_BswMRteMDG_NvmWriteAllRequest_NvmWriteAll_NoRequest (0U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_NvmWriteAllRequest_NvmWriteAll_Request (1U)
# ifndef RTE_MODE_BswMRteMDG_NvmWriteAllRequest_NvmWriteAll_Request
#  define RTE_MODE_BswMRteMDG_NvmWriteAllRequest_NvmWriteAll_Request (1U)
# endif
# define RTE_TRANSITION_BswM_BswMRteMDG_NvmWriteAllRequest (2U)
# ifndef RTE_TRANSITION_BswMRteMDG_NvmWriteAllRequest
#  define RTE_TRANSITION_BswMRteMDG_NvmWriteAllRequest (2U)
# endif

# define RTE_MODE_BswM_BswMRteMDG_PvtReport_X1C14_PvtReport_Disabled (0U)
# ifndef RTE_MODE_BswMRteMDG_PvtReport_X1C14_PvtReport_Disabled
#  define RTE_MODE_BswMRteMDG_PvtReport_X1C14_PvtReport_Disabled (0U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_PvtReport_X1C14_PvtReport_Enabled (1U)
# ifndef RTE_MODE_BswMRteMDG_PvtReport_X1C14_PvtReport_Enabled
#  define RTE_MODE_BswMRteMDG_PvtReport_X1C14_PvtReport_Enabled (1U)
# endif
# define RTE_TRANSITION_BswM_BswMRteMDG_PvtReport_X1C14 (2U)
# ifndef RTE_TRANSITION_BswMRteMDG_PvtReport_X1C14
#  define RTE_TRANSITION_BswMRteMDG_PvtReport_X1C14 (2U)
# endif

# define RTE_MODE_BswM_BswMRteMDG_ResetProcess_ResetProcess_Completed (0U)
# ifndef RTE_MODE_BswMRteMDG_ResetProcess_ResetProcess_Completed
#  define RTE_MODE_BswMRteMDG_ResetProcess_ResetProcess_Completed (0U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_ResetProcess_ResetProcess_Idle (1U)
# ifndef RTE_MODE_BswMRteMDG_ResetProcess_ResetProcess_Idle
#  define RTE_MODE_BswMRteMDG_ResetProcess_ResetProcess_Idle (1U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_ResetProcess_ResetProcess_Inprogress (2U)
# ifndef RTE_MODE_BswMRteMDG_ResetProcess_ResetProcess_Inprogress
#  define RTE_MODE_BswMRteMDG_ResetProcess_ResetProcess_Inprogress (2U)
# endif
# define RTE_MODE_BswM_BswMRteMDG_ResetProcess_ResetProcess_Started (3U)
# ifndef RTE_MODE_BswMRteMDG_ResetProcess_ResetProcess_Started
#  define RTE_MODE_BswMRteMDG_ResetProcess_ResetProcess_Started (3U)
# endif
# define RTE_TRANSITION_BswM_BswMRteMDG_ResetProcess (4U)
# ifndef RTE_TRANSITION_BswMRteMDG_ResetProcess
#  define RTE_TRANSITION_BswMRteMDG_ResetProcess (4U)
# endif

# define RTE_MODE_BswM_DcmEcuReset_NONE (0U)
# ifndef RTE_MODE_DcmEcuReset_NONE
#  define RTE_MODE_DcmEcuReset_NONE (0U)
# endif
# define RTE_MODE_BswM_DcmEcuReset_HARD (1U)
# ifndef RTE_MODE_DcmEcuReset_HARD
#  define RTE_MODE_DcmEcuReset_HARD (1U)
# endif
# define RTE_MODE_BswM_DcmEcuReset_KEYONOFF (2U)
# ifndef RTE_MODE_DcmEcuReset_KEYONOFF
#  define RTE_MODE_DcmEcuReset_KEYONOFF (2U)
# endif
# define RTE_MODE_BswM_DcmEcuReset_SOFT (3U)
# ifndef RTE_MODE_DcmEcuReset_SOFT
#  define RTE_MODE_DcmEcuReset_SOFT (3U)
# endif
# define RTE_MODE_BswM_DcmEcuReset_JUMPTOBOOTLOADER (4U)
# ifndef RTE_MODE_DcmEcuReset_JUMPTOBOOTLOADER
#  define RTE_MODE_DcmEcuReset_JUMPTOBOOTLOADER (4U)
# endif
# define RTE_MODE_BswM_DcmEcuReset_JUMPTOSYSSUPPLIERBOOTLOADER (5U)
# ifndef RTE_MODE_DcmEcuReset_JUMPTOSYSSUPPLIERBOOTLOADER
#  define RTE_MODE_DcmEcuReset_JUMPTOSYSSUPPLIERBOOTLOADER (5U)
# endif
# define RTE_MODE_BswM_DcmEcuReset_EXECUTE (6U)
# ifndef RTE_MODE_DcmEcuReset_EXECUTE
#  define RTE_MODE_DcmEcuReset_EXECUTE (6U)
# endif
# define RTE_TRANSITION_BswM_DcmEcuReset (7U)
# ifndef RTE_TRANSITION_DcmEcuReset
#  define RTE_TRANSITION_DcmEcuReset (7U)
# endif

# define RTE_MODE_BswM_ESH_Mode_POSTRUN (0U)
# ifndef RTE_MODE_ESH_Mode_POSTRUN
#  define RTE_MODE_ESH_Mode_POSTRUN (0U)
# endif
# define RTE_MODE_BswM_ESH_Mode_RUN (1U)
# ifndef RTE_MODE_ESH_Mode_RUN
#  define RTE_MODE_ESH_Mode_RUN (1U)
# endif
# define RTE_MODE_BswM_ESH_Mode_SHUTDOWN (2U)
# ifndef RTE_MODE_ESH_Mode_SHUTDOWN
#  define RTE_MODE_ESH_Mode_SHUTDOWN (2U)
# endif
# define RTE_MODE_BswM_ESH_Mode_STARTUP (3U)
# ifndef RTE_MODE_ESH_Mode_STARTUP
#  define RTE_MODE_ESH_Mode_STARTUP (3U)
# endif
# define RTE_MODE_BswM_ESH_Mode_WAKEUP (4U)
# ifndef RTE_MODE_ESH_Mode_WAKEUP
#  define RTE_MODE_ESH_Mode_WAKEUP (4U)
# endif
# define RTE_TRANSITION_BswM_ESH_Mode (5U)
# ifndef RTE_TRANSITION_ESH_Mode
#  define RTE_TRANSITION_ESH_Mode (5U)
# endif

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_BSWM_TYPE_H */
