/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_InteriorLightPanel_2_LINMastCtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <InteriorLightPanel_2_LINMastCtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_INTERIORLIGHTPANEL_2_LINMASTCTRL_TYPE_H
# define _RTE_INTERIORLIGHTPANEL_2_LINMASTCTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef Inactive
#   define Inactive (0U)
#  endif

#  ifndef Diagnostic
#   define Diagnostic (1U)
#  endif

#  ifndef SwitchDetection
#   define SwitchDetection (2U)
#  endif

#  ifndef ApplicationMonitoring
#   define ApplicationMonitoring (3U)
#  endif

#  ifndef Calibration
#   define Calibration (4U)
#  endif

#  ifndef Spare1
#   define Spare1 (5U)
#  endif

#  ifndef Error
#   define Error (6U)
#  endif

#  ifndef NotAvailable
#   define NotAvailable (7U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED
#   define DEM_EVENT_STATUS_PASSED (0U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED
#   define DEM_EVENT_STATUS_FAILED (1U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED
#   define DEM_EVENT_STATUS_PREPASSED (2U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED
#   define DEM_EVENT_STATUS_PREFAILED (3U)
#  endif

#  ifndef DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED
#   define DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
#  endif

#  ifndef FreeWheel_Status_NoMovement
#   define FreeWheel_Status_NoMovement (0U)
#  endif

#  ifndef FreeWheel_Status_1StepClockwise
#   define FreeWheel_Status_1StepClockwise (1U)
#  endif

#  ifndef FreeWheel_Status_2StepsClockwise
#   define FreeWheel_Status_2StepsClockwise (2U)
#  endif

#  ifndef FreeWheel_Status_3StepsClockwise
#   define FreeWheel_Status_3StepsClockwise (3U)
#  endif

#  ifndef FreeWheel_Status_4StepsClockwise
#   define FreeWheel_Status_4StepsClockwise (4U)
#  endif

#  ifndef FreeWheel_Status_5StepsClockwise
#   define FreeWheel_Status_5StepsClockwise (5U)
#  endif

#  ifndef FreeWheel_Status_6StepsClockwise
#   define FreeWheel_Status_6StepsClockwise (6U)
#  endif

#  ifndef FreeWheel_Status_1StepCounterClockwise
#   define FreeWheel_Status_1StepCounterClockwise (7U)
#  endif

#  ifndef FreeWheel_Status_2StepsCounterClockwise
#   define FreeWheel_Status_2StepsCounterClockwise (8U)
#  endif

#  ifndef FreeWheel_Status_3StepsCounterClockwise
#   define FreeWheel_Status_3StepsCounterClockwise (9U)
#  endif

#  ifndef FreeWheel_Status_4StepsCounterClockwise
#   define FreeWheel_Status_4StepsCounterClockwise (10U)
#  endif

#  ifndef FreeWheel_Status_5StepsCounterClockwise
#   define FreeWheel_Status_5StepsCounterClockwise (11U)
#  endif

#  ifndef FreeWheel_Status_6StepsCounterClockwise
#   define FreeWheel_Status_6StepsCounterClockwise (12U)
#  endif

#  ifndef FreeWheel_Status_Spare
#   define FreeWheel_Status_Spare (13U)
#  endif

#  ifndef FreeWheel_Status_Error
#   define FreeWheel_Status_Error (14U)
#  endif

#  ifndef FreeWheel_Status_NotAvailable
#   define FreeWheel_Status_NotAvailable (15U)
#  endif

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_INTERIORLIGHTPANEL_2_LINMASTCTRL_TYPE_H */
