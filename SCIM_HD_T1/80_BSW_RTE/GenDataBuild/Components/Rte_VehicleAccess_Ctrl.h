/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_VehicleAccess_Ctrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <VehicleAccess_Ctrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_VEHICLEACCESS_CTRL_H
# define _RTE_VEHICLEACCESS_CTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_VehicleAccess_Ctrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_DashboardLockSwitch_Devic_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_DriverDoorLatch_rqst_CryptTrig_CryptoTrigger; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DoorLatch_rqst_decrypt_T, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FrontLidLatch_cmd_T, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_FrontLidLatch_cmd_FrontLidLatch_cmd; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(KeyfobLocation_rqst_T, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_KeyfobLocation_rqst_KeyfobLocation_rqst; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DoorLatch_rqst_decrypt_T, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_PsngrDoorLatch_rqst_CryptTrig_CryptoTrigger; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DoorAjar_stat_T, RTE_VAR_NOINIT) Rte_InternalDoorsAjar_Hdlr_DriverDoorAjarInternal_stat_DoorAjar_stat; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DoorLatch_stat_T, RTE_VAR_NOINIT) Rte_CentralDoorsLatch_Hdlr_DriverDoorLatchInternal_stat_DoorLatch_stat; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FrontLidLatch_stat_T, RTE_VAR_NOINIT) Rte_FrontLidLatchSensor_hdlr_FrontLidLatch_stat_FrontLidLatch_stat; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DoorLockUnlock_T, RTE_VAR_NOINIT) Rte_InCabLock_HMICtrl_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(KeyfobInCabLocation_stat_T, RTE_VAR_NOINIT) Rte_KeyfobInCabLocation_stat_oCIOM_BB2_01P_oBackbone2_69de562e_Tx; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_Keyfob_UICtrl_KeyfobLockButton_Status_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(KeyfobOutsideLocation_stat_T, RTE_VAR_NOINIT) Rte_Keyfob_Mgr_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_Keyfob_UICtrl_KeyfobSuperLockButton_Sta_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_KeyfobUnlockButton_Status_oCIOM_BB2_02P_oBackbone2_7b99e4e7_Tx; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_PassiveDoorButton_hdlr_LeftDoorButton_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DoorLatch_stat_T, RTE_VAR_NOINIT) Rte_CentralDoorsLatch_Hdlr_PsngDoorLatchInternal_stat_DoorLatch_stat; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DoorAjar_stat_T, RTE_VAR_NOINIT) Rte_InternalDoorsAjar_Hdlr_PsngrDoorAjarInternal_stat_DoorAjar_stat; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_PassiveDoorButton_hdlr_RightDoorButton_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleMode_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_AutoRelock_rqst_AutoRelock_rqst (3U)
#  define Rte_InitValue_AutorelockingMovements_stat_AutorelockingMovements_stat (7U)
#  define Rte_InitValue_DashboardLockSwitch_Devic_DeviceIndication (0U)
#  define Rte_InitValue_DoorLock_stat_DoorLock_stat (15U)
#  define Rte_InitValue_DoorsAjar_stat_DoorsAjar_stat (7U)
#  define Rte_InitValue_DriverDoorAjarInternal_stat_DoorAjar_stat (7U)
#  define Rte_InitValue_DriverDoorAjar_stat_DoorAjar_stat (7U)
#  define Rte_InitValue_DriverDoorLatchInternal_stat_DoorLatch_stat (7U)
#  define Rte_InitValue_DriverDoorLatch_rqst_CryptTrig_CryptoTrigger (FALSE)
#  define Rte_InitValue_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt (7U)
#  define Rte_InitValue_DriverDoorLatch_stat_DoorLatch_stat (7U)
#  define Rte_InitValue_EmergencyDoorsUnlock_rqst_EmergencyDoorsUnlock_rqst (7U)
#  define Rte_InitValue_FrontLidLatch_cmd_FrontLidLatch_cmd (7U)
#  define Rte_InitValue_FrontLidLatch_stat_FrontLidLatch_stat (7U)
#  define Rte_InitValue_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst (7U)
#  define Rte_InitValue_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat (0U)
#  define Rte_InitValue_KeyfobLocation_rqst_KeyfobLocation_rqst (3U)
#  define Rte_InitValue_KeyfobLockButton_Status_PushButtonStatus (3U)
#  define Rte_InitValue_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle (7U)
#  define Rte_InitValue_KeyfobSuperLockButton_Sta_PushButtonStatus (3U)
#  define Rte_InitValue_KeyfobUnlockButton_Status_PushButtonStatus (3U)
#  define Rte_InitValue_LeftDoorButton_stat_PushButtonStatus (3U)
#  define Rte_InitValue_LockingIndication_rqst_LockingIndication_rqst (7U)
#  define Rte_InitValue_PassengerDoorAjar_stat_DoorAjar_stat (7U)
#  define Rte_InitValue_PassengerDoorLatch_stat_DoorLatch_stat (7U)
#  define Rte_InitValue_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt (7U)
#  define Rte_InitValue_PsngDoorLatchInternal_stat_DoorLatch_stat (7U)
#  define Rte_InitValue_PsngrDoorAjarInternal_stat_DoorAjar_stat (7U)
#  define Rte_InitValue_PsngrDoorLatch_rqst_CryptTrig_CryptoTrigger (FALSE)
#  define Rte_InitValue_RightDoorButton_stat_PushButtonStatus (3U)
#  define Rte_InitValue_SpeedLockingInhibition_stat_SpeedLockingInhibition_stat (7U)
#  define Rte_InitValue_SwcActivation_Security_SwcActivation_Security (1U)
#  define Rte_InitValue_Synch_Unsynch_Mode_stat_Synch_Unsynch_Mode_stat (7U)
#  define Rte_InitValue_VehicleModeInternal_VehicleMode (15U)
#  define Rte_InitValue_WRCLockButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_WRCUnlockButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed (65535U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleAccess_Ctrl_AutorelockingMovements_stat_AutorelockingMovements_stat(P2VAR(AutorelockingMovements_stat_T, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleAccess_Ctrl_DriverDoorAjar_stat_DoorAjar_stat(P2VAR(DoorAjar_stat_T, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleAccess_Ctrl_DriverDoorLatch_stat_DoorLatch_stat(P2VAR(DoorLatch_stat_T, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleAccess_Ctrl_DrivrDrKeyCylTrn_st_serialized_Crypto_Function_serialized(P2VAR(uint8, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleAccess_Ctrl_DrivrDrKeyCylTrn_st_serialized_Crypto_Function_serialized(P2VAR(Crypto_Function_serialized_T, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleAccess_Ctrl_EmergencyDoorsUnlock_rqst_EmergencyDoorsUnlock_rqst(P2VAR(EmergencyDoorsUnlock_rqst_T, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleAccess_Ctrl_PassengerDoorAjar_stat_DoorAjar_stat(P2VAR(DoorAjar_stat_T, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleAccess_Ctrl_PassengerDoorLatch_stat_DoorLatch_stat(P2VAR(DoorLatch_stat_T, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleAccess_Ctrl_PsgDoorKeyCylTrn_st_serialized_Crypto_Function_serialized(P2VAR(uint8, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleAccess_Ctrl_PsgDoorKeyCylTrn_st_serialized_Crypto_Function_serialized(P2VAR(Crypto_Function_serialized_T, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleAccess_Ctrl_SpeedLockingInhibition_stat_SpeedLockingInhibition_stat(P2VAR(SpeedLockingInhibition_stat_T, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleAccess_Ctrl_Synch_Unsynch_Mode_stat_Synch_Unsynch_Mode_stat(P2VAR(Synch_Unsynch_Mode_stat_T, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleAccess_Ctrl_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I(P2VAR(uint8, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleAccess_Ctrl_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I(P2VAR(StandardNVM_T, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleAccess_Ctrl_WRCLockButtonStatus_PushButtonStatus(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleAccess_Ctrl_WRCUnlockButtonStatus_PushButtonStatus(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_VehicleAccess_Ctrl_AutoRelock_rqst_AutoRelock_rqst(AutoRelock_rqst_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_VehicleAccess_Ctrl_DoorLock_stat_DoorLock_stat(DoorLock_stat_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_VehicleAccess_Ctrl_DoorsAjar_stat_DoorsAjar_stat(DoorsAjar_stat_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_VehicleAccess_Ctrl_DriverDoorLatch_rqt_serialized_Crypto_Function_serialized(P2CONST(uint8, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_VehicleAccess_Ctrl_DriverDoorLatch_rqt_serialized_Crypto_Function_serialized(P2CONST(Crypto_Function_serialized_T, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_VehicleAccess_Ctrl_LockingIndication_rqst_LockingIndication_rqst(LockingIndication_rqst_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_VehicleAccess_Ctrl_PsngrDoorLatch_rqst_serialized_Crypto_Function_serialized(P2CONST(uint8, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_VehicleAccess_Ctrl_PsngrDoorLatch_rqst_serialized_Crypto_Function_serialized(P2CONST(Crypto_Function_serialized_T, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_VehicleAccess_Ctrl_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I(P2CONST(uint8, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_VehicleAccess_Ctrl_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I(P2CONST(StandardNVM_T, AUTOMATIC, RTE_VEHICLEACCESS_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_AutorelockingMovements_stat_AutorelockingMovements_stat Rte_Read_VehicleAccess_Ctrl_AutorelockingMovements_stat_AutorelockingMovements_stat
#  define Rte_Read_DriverDoorAjarInternal_stat_DoorAjar_stat Rte_Read_VehicleAccess_Ctrl_DriverDoorAjarInternal_stat_DoorAjar_stat
#  define Rte_Read_VehicleAccess_Ctrl_DriverDoorAjarInternal_stat_DoorAjar_stat(data) (*(data) = Rte_InternalDoorsAjar_Hdlr_DriverDoorAjarInternal_stat_DoorAjar_stat, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DriverDoorAjar_stat_DoorAjar_stat Rte_Read_VehicleAccess_Ctrl_DriverDoorAjar_stat_DoorAjar_stat
#  define Rte_Read_DriverDoorLatchInternal_stat_DoorLatch_stat Rte_Read_VehicleAccess_Ctrl_DriverDoorLatchInternal_stat_DoorLatch_stat
#  define Rte_Read_VehicleAccess_Ctrl_DriverDoorLatchInternal_stat_DoorLatch_stat(data) (*(data) = Rte_CentralDoorsLatch_Hdlr_DriverDoorLatchInternal_stat_DoorLatch_stat, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DriverDoorLatch_stat_DoorLatch_stat Rte_Read_VehicleAccess_Ctrl_DriverDoorLatch_stat_DoorLatch_stat
#  define Rte_Read_DrivrDrKeyCylTrn_st_serialized_Crypto_Function_serialized Rte_Read_VehicleAccess_Ctrl_DrivrDrKeyCylTrn_st_serialized_Crypto_Function_serialized
#  define Rte_Read_EmergencyDoorsUnlock_rqst_EmergencyDoorsUnlock_rqst Rte_Read_VehicleAccess_Ctrl_EmergencyDoorsUnlock_rqst_EmergencyDoorsUnlock_rqst
#  define Rte_Read_FrontLidLatch_stat_FrontLidLatch_stat Rte_Read_VehicleAccess_Ctrl_FrontLidLatch_stat_FrontLidLatch_stat
#  define Rte_Read_VehicleAccess_Ctrl_FrontLidLatch_stat_FrontLidLatch_stat(data) (*(data) = Rte_FrontLidLatchSensor_hdlr_FrontLidLatch_stat_FrontLidLatch_stat, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst Rte_Read_VehicleAccess_Ctrl_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst
#  define Rte_Read_VehicleAccess_Ctrl_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst(data) (*(data) = Rte_InCabLock_HMICtrl_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat Rte_Read_VehicleAccess_Ctrl_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat
#  define Rte_Read_VehicleAccess_Ctrl_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat(data) (*(data) = Rte_KeyfobInCabLocation_stat_oCIOM_BB2_01P_oBackbone2_69de562e_Tx, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_KeyfobLockButton_Status_PushButtonStatus Rte_Read_VehicleAccess_Ctrl_KeyfobLockButton_Status_PushButtonStatus
#  define Rte_Read_VehicleAccess_Ctrl_KeyfobLockButton_Status_PushButtonStatus(data) (*(data) = Rte_Keyfob_UICtrl_KeyfobLockButton_Status_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle Rte_Read_VehicleAccess_Ctrl_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle
#  define Rte_Read_VehicleAccess_Ctrl_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle(data) (*(data) = Rte_Keyfob_Mgr_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_KeyfobSuperLockButton_Sta_PushButtonStatus Rte_Read_VehicleAccess_Ctrl_KeyfobSuperLockButton_Sta_PushButtonStatus
#  define Rte_Read_VehicleAccess_Ctrl_KeyfobSuperLockButton_Sta_PushButtonStatus(data) (*(data) = Rte_Keyfob_UICtrl_KeyfobSuperLockButton_Sta_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_KeyfobUnlockButton_Status_PushButtonStatus Rte_Read_VehicleAccess_Ctrl_KeyfobUnlockButton_Status_PushButtonStatus
#  define Rte_Read_VehicleAccess_Ctrl_KeyfobUnlockButton_Status_PushButtonStatus(data) (*(data) = Rte_KeyfobUnlockButton_Status_oCIOM_BB2_02P_oBackbone2_7b99e4e7_Tx, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LeftDoorButton_stat_PushButtonStatus Rte_Read_VehicleAccess_Ctrl_LeftDoorButton_stat_PushButtonStatus
#  define Rte_Read_VehicleAccess_Ctrl_LeftDoorButton_stat_PushButtonStatus(data) (*(data) = Rte_PassiveDoorButton_hdlr_LeftDoorButton_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_PassengerDoorAjar_stat_DoorAjar_stat Rte_Read_VehicleAccess_Ctrl_PassengerDoorAjar_stat_DoorAjar_stat
#  define Rte_Read_PassengerDoorLatch_stat_DoorLatch_stat Rte_Read_VehicleAccess_Ctrl_PassengerDoorLatch_stat_DoorLatch_stat
#  define Rte_Read_PsgDoorKeyCylTrn_st_serialized_Crypto_Function_serialized Rte_Read_VehicleAccess_Ctrl_PsgDoorKeyCylTrn_st_serialized_Crypto_Function_serialized
#  define Rte_Read_PsngDoorLatchInternal_stat_DoorLatch_stat Rte_Read_VehicleAccess_Ctrl_PsngDoorLatchInternal_stat_DoorLatch_stat
#  define Rte_Read_VehicleAccess_Ctrl_PsngDoorLatchInternal_stat_DoorLatch_stat(data) (*(data) = Rte_CentralDoorsLatch_Hdlr_PsngDoorLatchInternal_stat_DoorLatch_stat, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_PsngrDoorAjarInternal_stat_DoorAjar_stat Rte_Read_VehicleAccess_Ctrl_PsngrDoorAjarInternal_stat_DoorAjar_stat
#  define Rte_Read_VehicleAccess_Ctrl_PsngrDoorAjarInternal_stat_DoorAjar_stat(data) (*(data) = Rte_InternalDoorsAjar_Hdlr_PsngrDoorAjarInternal_stat_DoorAjar_stat, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RightDoorButton_stat_PushButtonStatus Rte_Read_VehicleAccess_Ctrl_RightDoorButton_stat_PushButtonStatus
#  define Rte_Read_VehicleAccess_Ctrl_RightDoorButton_stat_PushButtonStatus(data) (*(data) = Rte_PassiveDoorButton_hdlr_RightDoorButton_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SpeedLockingInhibition_stat_SpeedLockingInhibition_stat Rte_Read_VehicleAccess_Ctrl_SpeedLockingInhibition_stat_SpeedLockingInhibition_stat
#  define Rte_Read_SwcActivation_Security_SwcActivation_Security Rte_Read_VehicleAccess_Ctrl_SwcActivation_Security_SwcActivation_Security
#  define Rte_Read_VehicleAccess_Ctrl_SwcActivation_Security_SwcActivation_Security(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Synch_Unsynch_Mode_stat_Synch_Unsynch_Mode_stat Rte_Read_VehicleAccess_Ctrl_Synch_Unsynch_Mode_stat_Synch_Unsynch_Mode_stat
#  define Rte_Read_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I Rte_Read_VehicleAccess_Ctrl_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I
#  define Rte_Read_VehicleModeInternal_VehicleMode Rte_Read_VehicleAccess_Ctrl_VehicleModeInternal_VehicleMode
#  define Rte_Read_VehicleAccess_Ctrl_VehicleModeInternal_VehicleMode(data) (*(data) = Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WRCLockButtonStatus_PushButtonStatus Rte_Read_VehicleAccess_Ctrl_WRCLockButtonStatus_PushButtonStatus
#  define Rte_Read_WRCUnlockButtonStatus_PushButtonStatus Rte_Read_VehicleAccess_Ctrl_WRCUnlockButtonStatus_PushButtonStatus
#  define Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed Rte_Read_VehicleAccess_Ctrl_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed
#  define Rte_Read_VehicleAccess_Ctrl_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(data) (Com_ReceiveSignal(ComConf_ComSignal_WheelBasedVehicleSpeed_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_e3ff6d4c_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_AutoRelock_rqst_AutoRelock_rqst Rte_Write_VehicleAccess_Ctrl_AutoRelock_rqst_AutoRelock_rqst
#  define Rte_Write_DashboardLockSwitch_Devic_DeviceIndication Rte_Write_VehicleAccess_Ctrl_DashboardLockSwitch_Devic_DeviceIndication
#  define Rte_Write_VehicleAccess_Ctrl_DashboardLockSwitch_Devic_DeviceIndication(data) (Rte_VehicleAccess_Ctrl_DashboardLockSwitch_Devic_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_DoorLock_stat_DoorLock_stat Rte_Write_VehicleAccess_Ctrl_DoorLock_stat_DoorLock_stat
#  define Rte_Write_DoorsAjar_stat_DoorsAjar_stat Rte_Write_VehicleAccess_Ctrl_DoorsAjar_stat_DoorsAjar_stat
#  define Rte_Write_DriverDoorLatch_rqst_CryptTrig_CryptoTrigger Rte_Write_VehicleAccess_Ctrl_DriverDoorLatch_rqst_CryptTrig_CryptoTrigger
#  define Rte_Write_VehicleAccess_Ctrl_DriverDoorLatch_rqst_CryptTrig_CryptoTrigger(data) (Rte_VehicleAccess_Ctrl_DriverDoorLatch_rqst_CryptTrig_CryptoTrigger = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt Rte_Write_VehicleAccess_Ctrl_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt
#  define Rte_Write_VehicleAccess_Ctrl_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(data) (Rte_VehicleAccess_Ctrl_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_DriverDoorLatch_rqt_serialized_Crypto_Function_serialized Rte_Write_VehicleAccess_Ctrl_DriverDoorLatch_rqt_serialized_Crypto_Function_serialized
#  define Rte_Write_FrontLidLatch_cmd_FrontLidLatch_cmd Rte_Write_VehicleAccess_Ctrl_FrontLidLatch_cmd_FrontLidLatch_cmd
#  define Rte_Write_VehicleAccess_Ctrl_FrontLidLatch_cmd_FrontLidLatch_cmd(data) (Rte_VehicleAccess_Ctrl_FrontLidLatch_cmd_FrontLidLatch_cmd = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_KeyfobLocation_rqst_KeyfobLocation_rqst Rte_Write_VehicleAccess_Ctrl_KeyfobLocation_rqst_KeyfobLocation_rqst
#  define Rte_Write_VehicleAccess_Ctrl_KeyfobLocation_rqst_KeyfobLocation_rqst(data) (Rte_VehicleAccess_Ctrl_KeyfobLocation_rqst_KeyfobLocation_rqst = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LockingIndication_rqst_LockingIndication_rqst Rte_Write_VehicleAccess_Ctrl_LockingIndication_rqst_LockingIndication_rqst
#  define Rte_Write_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt Rte_Write_VehicleAccess_Ctrl_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt
#  define Rte_Write_VehicleAccess_Ctrl_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(data) (Rte_VehicleAccess_Ctrl_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_PsngrDoorLatch_rqst_CryptTrig_CryptoTrigger Rte_Write_VehicleAccess_Ctrl_PsngrDoorLatch_rqst_CryptTrig_CryptoTrigger
#  define Rte_Write_VehicleAccess_Ctrl_PsngrDoorLatch_rqst_CryptTrig_CryptoTrigger(data) (Rte_VehicleAccess_Ctrl_PsngrDoorLatch_rqst_CryptTrig_CryptoTrigger = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_PsngrDoorLatch_rqst_serialized_Crypto_Function_serialized Rte_Write_VehicleAccess_Ctrl_PsngrDoorLatch_rqst_serialized_Crypto_Function_serialized
#  define Rte_Write_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I Rte_Write_VehicleAccess_Ctrl_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_ActivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_LockControlActDeactivation_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)29)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_DeactivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_LockControlActDeactivation_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)29)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_GetIssState(Issm_UserHandleType parg0, P2VAR(Issm_IssStateType, AUTOMATIC, RTE_ISSM_APPL_VAR) issState); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_LockControlActDeactivation_GetIssState(arg1) (Issm_GetIssState((Issm_UserHandleType)29, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_LockControlCabRqst2_Pending_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)50)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_LockControlCabRqst2_Pending_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)50)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_DoorLockingFailureTimeout_X1CX9_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX9_DoorLockingFailureTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AlarmAutoRelockRequestDuration_X1CYA_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CYA_AlarmAutoRelockRequestDuration_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_X1CX9_DoorLockingFailureTimeout_v() (Rte_AddrPar_0x29_X1CX9_DoorLockingFailureTimeout_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_X1CYA_AlarmAutoRelockRequestDuration_v() (Rte_AddrPar_0x29_X1CYA_AlarmAutoRelockRequestDuration_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1O8Q_DoorLockHazardIndicationRqstDelay_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DoorAutoLockingSpeed_P1B2Q_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2Q_DoorAutoLockingSpeed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AutoAlarmReactivationTimeout_P1B2S_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2S_AutoAlarmReactivationTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DoorLatchProtectionTimeWindow_P1DW8_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DW8_DoorLatchProtectionTimeWindow_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DoorLatchProtectionRestingTime_P1DW9_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DW9_DoorLatchProtectionRestingTime_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DoorIndicationReqDuration_P1DWP_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWP_DoorIndicationReqDuration_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DoorLatchProtectMaxOperation_P1DXA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DXA_DoorLatchProtectMaxOperation_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_SpeedRelockingReinitThreshold_P1H55_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1H55_SpeedRelockingReinitThreshold_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DashboardLedTimeout_P1IZ4_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1IZ4_DashboardLedTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_LockFunctionHardwareInterface_P1MXZ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1MXZ_LockFunctionHardwareInterface_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PassiveEntryFunction_Type_P1VKF_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VKF_PassiveEntryFunction_Type_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NE9_KeyInsertDetection_Enabled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NQE_LockModeHandling_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VKI_PassiveStart_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1O8Q_DoorLockHazardIndicationRqstDelay_v() (Rte_AddrPar_0x2B_P1O8Q_DoorLockHazardIndicationRqstDelay_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1B2Q_DoorAutoLockingSpeed_v() (Rte_AddrPar_0x2B_P1B2Q_DoorAutoLockingSpeed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1B2S_AutoAlarmReactivationTimeout_v() (Rte_AddrPar_0x2B_P1B2S_AutoAlarmReactivationTimeout_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DW8_DoorLatchProtectionTimeWindow_v() (Rte_AddrPar_0x2B_P1DW8_DoorLatchProtectionTimeWindow_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DW9_DoorLatchProtectionRestingTime_v() (Rte_AddrPar_0x2B_P1DW9_DoorLatchProtectionRestingTime_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DWP_DoorIndicationReqDuration_v() (Rte_AddrPar_0x2B_P1DWP_DoorIndicationReqDuration_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DXA_DoorLatchProtectMaxOperation_v() (Rte_AddrPar_0x2B_P1DXA_DoorLatchProtectMaxOperation_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1H55_SpeedRelockingReinitThreshold_v() (Rte_AddrPar_0x2B_P1H55_SpeedRelockingReinitThreshold_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1IZ4_DashboardLedTimeout_v() (Rte_AddrPar_0x2B_P1IZ4_DashboardLedTimeout_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1MXZ_LockFunctionHardwareInterface_v() (Rte_AddrPar_0x2B_P1MXZ_LockFunctionHardwareInterface_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1VKF_PassiveEntryFunction_Type_v() (Rte_AddrPar_0x2B_P1VKF_PassiveEntryFunction_Type_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1NE9_KeyInsertDetection_Enabled_v() (Rte_AddrPar_0x2B_P1NE9_KeyInsertDetection_Enabled_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1NQE_LockModeHandling_v() (Rte_AddrPar_0x2B_P1NQE_LockModeHandling_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1VKI_PassiveStart_Installed_v() (Rte_AddrPar_0x2B_P1VKI_PassiveStart_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define VehicleAccess_Ctrl_START_SEC_CODE
# include "VehicleAccess_Ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_VehicleAccess_Ctrl_20ms_runnable VehicleAccess_Ctrl_20ms_runnable
#  define RTE_RUNNABLE_VehicleAccess_Ctrl_Init VehicleAccess_Ctrl_Init
# endif

FUNC(void, VehicleAccess_Ctrl_CODE) VehicleAccess_Ctrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, VehicleAccess_Ctrl_CODE) VehicleAccess_Ctrl_Init(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define VehicleAccess_Ctrl_STOP_SEC_CODE
# include "VehicleAccess_Ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_Issm_IssRequest_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_VEHICLEACCESS_CTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
