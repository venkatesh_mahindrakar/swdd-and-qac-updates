/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_VEC_CryptoProxyReceiverSwc.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <VEC_CryptoProxyReceiverSwc>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_VEC_CRYPTOPROXYRECEIVERSWC_H
# define _RTE_VEC_CRYPTOPROXYRECEIVERSWC_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# ifndef RTE_CORE
#  define RTE_MULTI_INST_API
# endif

/* include files */

# include "Rte_VEC_CryptoProxyReceiverSwc_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE
typedef P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, TYPEDEF, RTE_CONST) Rte_Instance; /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/

# ifndef RTE_CORE
# endif /* !defined(RTE_CORE) */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Receive_<p>_<d> (explicit S/R communication with isQueued = true)
 *********************************************************************************************************************/
#  define Rte_Receive_VEC_EncryptedSignal_EncryptedSignal(self, data) ((self)->VEC_EncryptedSignal.Receive_EncryptedSignal(data)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Send_<p>_<d> (explicit S/R communication with isQueued = true)
 *********************************************************************************************************************/
#  define Rte_Send_VEC_CryptoIdKey_CryptoIdKey(self, data) ((self)->VEC_CryptoIdKey.Send_CryptoIdKey(data)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_VEC_CryptoProxySerializedData_Crypto_Function_serialized(self, data) ((self)->VEC_CryptoProxySerializedData.Write_Crypto_Function_serialized(data)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Feedback_<p>_<d> (explicit S/R communication status handling)
 *********************************************************************************************************************/
#  define Rte_Feedback_VEC_CryptoIdKey_CryptoIdKey(self) ((self)->VEC_CryptoIdKey.Feedback_CryptoIdKey()) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define Rte_Call_CsmSymDecrypt_SymDecryptFinish(self, arg1, arg2) ((self)->CsmSymDecrypt.Call_SymDecryptFinish(arg1, arg2)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_CsmSymDecrypt_SymDecryptStart(self, arg1, arg2, arg3) ((self)->CsmSymDecrypt.Call_SymDecryptStart(arg1, arg2, arg3)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_CsmSymDecrypt_SymDecryptUpdate(self, arg1, arg2, arg3, arg4) ((self)->CsmSymDecrypt.Call_SymDecryptUpdate(arg1, arg2, arg3, arg4)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_CsmSymEncrypt_SymEncryptFinish(self, arg1, arg2) ((self)->CsmSymEncrypt.Call_SymEncryptFinish(arg1, arg2)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_CsmSymEncrypt_SymEncryptStart(self, arg1, arg2, arg3) ((self)->CsmSymEncrypt.Call_SymEncryptStart(arg1, arg2, arg3)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_CsmSymEncrypt_SymEncryptUpdate(self, arg1, arg2, arg3, arg4) ((self)->CsmSymEncrypt.Call_SymEncryptUpdate(arg1, arg2, arg3, arg4)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_VEC_IdentificationKeyInit_IdentificationKeyInit(self, arg1) ((self)->VEC_IdentificationKeyInit.Call_IdentificationKeyInit(arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber(self) \
  ((self)->IrvRead_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber())
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState(self) \
  ((self)->IrvRead_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState())
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber(self, data) \
  ((self)->IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber(data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState(self, data) \
  ((self)->IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState(data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_NewReceivedIdentificationNumber(self, data) \
  ((self)->IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_NewReceivedIdentificationNumber(data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber(self) \
  ((self)->IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber())
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState(self) \
  ((self)->IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState())
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_NewReceivedIdentificationNumber(self) \
  ((self)->IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_NewReceivedIdentificationNumber())
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber(self, data) \
  ((self)->IrvWrite_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber(data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState(self, data) \
  ((self)->IrvWrite_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState(data))
/* PRQA L:L1 */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  define Rte_CData_VEC_CryptoProxyTimeFactorResentIdentification(self) ((self)->CData_VEC_CryptoProxyTimeFactorResentIdentification()) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_CData_VEC_CryptoProxyTimeFactorTillNewIdentificationKey(self) ((self)->CData_VEC_CryptoProxyTimeFactorTillNewIdentificationKey()) /* PRQA S 3453 */ /* MD_MSR_19.7 */

/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  define Rte_Prm_ComCryptoKey_P1DLX_v(self) ((self)->ComCryptoKey_P1DLX.Prm_v()) /* PRQA S 3453 */ /* MD_MSR_19.7 */

/**********************************************************************************************************************
 * Per-Instance Memory User Types
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  define Rte_Pim_VEC_CryptoProxy_DelayTimer(self) \
  ((self)->Pim_VEC_CryptoProxy_DelayTimer) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Pim_VEC_CryptoProxy_ResentTimer(self) \
  ((self)->Pim_VEC_CryptoProxy_ResentTimer) /* PRQA S 3453 */ /* MD_MSR_19.7 */


# endif /* !defined(RTE_CORE) */


# define VEC_CryptoProxyReceiverSwc_START_SEC_CODE
# include "VEC_CryptoProxyReceiverSwc_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_VEC_CryptoProxyReceiverMainFunction VEC_CryptoProxyReceiverMainFunction
#  define RTE_RUNNABLE_VEC_CryptoProxyReceiverReception VEC_CryptoProxyReceiverReception
#  define RTE_RUNNABLE_VEC_CryptoProxyReceiverSwc_Init VEC_CryptoProxyReceiverSwc_Init
# endif

FUNC(void, VEC_CryptoProxyReceiverSwc_CODE) VEC_CryptoProxyReceiverMainFunction(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, VEC_CryptoProxyReceiverSwc_CODE) VEC_CryptoProxyReceiverReception(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, VEC_CryptoProxyReceiverSwc_CODE) VEC_CryptoProxyReceiverSwc_Init(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define VEC_CryptoProxyReceiverSwc_STOP_SEC_CODE
# include "VEC_CryptoProxyReceiverSwc_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_CsmSymDecrypt_CSM_E_BUSY (2U)

#  define RTE_E_CsmSymDecrypt_CSM_E_NOT_OK (1U)

#  define RTE_E_CsmSymDecrypt_CSM_E_SMALL_BUFFER (3U)

#  define RTE_E_CsmSymEncrypt_CSM_E_BUSY (2U)

#  define RTE_E_CsmSymEncrypt_CSM_E_NOT_OK (1U)

#  define RTE_E_CsmSymEncrypt_CSM_E_SMALL_BUFFER (3U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_VEC_CRYPTOPROXYRECEIVERSWC_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
