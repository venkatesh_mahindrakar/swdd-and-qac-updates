/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_SCIM_PVTPT_IO_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <SCIM_PVTPT_IO>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_SCIM_PVTPT_IO_TYPE_H
# define _RTE_SCIM_PVTPT_IO_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef PvtReport_Enabled
#   define PvtReport_Enabled (0U)
#  endif

#  ifndef PvtReport_Disabled
#   define PvtReport_Disabled (1U)
#  endif

#  ifndef Cx0_Ctrl_CyclicReport
#   define Cx0_Ctrl_CyclicReport (0U)
#  endif

#  ifndef Cx1_ADI_01To06_DAI1
#   define Cx1_ADI_01To06_DAI1 (1U)
#  endif

#  ifndef Cx2_ADI_07To12_DAI2
#   define Cx2_ADI_07To12_DAI2 (2U)
#  endif

#  ifndef Cx3_ADI_13To16_DAI
#   define Cx3_ADI_13To16_DAI (3U)
#  endif

#  ifndef Cx0_Ctr_CyclicReport
#   define Cx0_Ctr_CyclicReport (0U)
#  endif

#  ifndef Cx1_Universal_debug_trace
#   define Cx1_Universal_debug_trace (1U)
#  endif

#  ifndef Cx2_Keyfob_RF_data1
#   define Cx2_Keyfob_RF_data1 (2U)
#  endif

#  ifndef Cx3_Keyfob_RF_data2
#   define Cx3_Keyfob_RF_data2 (3U)
#  endif

#  ifndef Cx4_Keyfob_LF_data1
#   define Cx4_Keyfob_LF_data1 (4U)
#  endif

#  ifndef Cx5_Keyfob_LF_data2
#   define Cx5_Keyfob_LF_data2 (5U)
#  endif

#  ifndef Cx6_SW_execution_statistics
#   define Cx6_SW_execution_statistics (6U)
#  endif

#  ifndef Cx7_SW_state_statistics
#   define Cx7_SW_state_statistics (7U)
#  endif

#  ifndef Cx0_Idle
#   define Cx0_Idle (0U)
#  endif

#  ifndef Cx1_Deactivate
#   define Cx1_Deactivate (1U)
#  endif

#  ifndef Cx2_Activate
#   define Cx2_Activate (2U)
#  endif

#  ifndef Cx0_None
#   define Cx0_None (0U)
#  endif

#  ifndef Cx1_Short_To_Ground
#   define Cx1_Short_To_Ground (1U)
#  endif

#  ifndef Cx2_Short_To_Battery
#   define Cx2_Short_To_Battery (2U)
#  endif

#  ifndef Cx3_Open_Circuit
#   define Cx3_Open_Circuit (3U)
#  endif

#  ifndef Cx4_Reserved
#   define Cx4_Reserved (4U)
#  endif

#  ifndef Cx5_Reserved
#   define Cx5_Reserved (5U)
#  endif

#  ifndef Cx6_Voltage_Too_Low
#   define Cx6_Voltage_Too_Low (6U)
#  endif

#  ifndef Cx7_Voltage_Too_High
#   define Cx7_Voltage_Too_High (7U)
#  endif

#  ifndef Cx4_PWM_failure
#   define Cx4_PWM_failure (4U)
#  endif

#  ifndef IOCtrl_AppRequest
#   define IOCtrl_AppRequest (0U)
#  endif

#  ifndef IOCtrl_DiagReturnCtrlToApp
#   define IOCtrl_DiagReturnCtrlToApp (1U)
#  endif

#  ifndef IOCtrl_DiagShortTermAdjust
#   define IOCtrl_DiagShortTermAdjust (2U)
#  endif

#  ifndef TestNotRun
#   define TestNotRun (0U)
#  endif

#  ifndef OffState_NoFaultDetected
#   define OffState_NoFaultDetected (16U)
#  endif

#  ifndef OffState_FaultDetected_STG
#   define OffState_FaultDetected_STG (17U)
#  endif

#  ifndef OffState_FaultDetected_STB
#   define OffState_FaultDetected_STB (18U)
#  endif

#  ifndef OffState_FaultDetected_OC
#   define OffState_FaultDetected_OC (19U)
#  endif

#  ifndef OffState_FaultDetected_VBT
#   define OffState_FaultDetected_VBT (22U)
#  endif

#  ifndef OffState_FaultDetected_VAT
#   define OffState_FaultDetected_VAT (23U)
#  endif

#  ifndef OnState_NoFaultDetected
#   define OnState_NoFaultDetected (32U)
#  endif

#  ifndef OnState_FaultDetected_STG
#   define OnState_FaultDetected_STG (33U)
#  endif

#  ifndef OnState_FaultDetected_STB
#   define OnState_FaultDetected_STB (34U)
#  endif

#  ifndef OnState_FaultDetected_OC
#   define OnState_FaultDetected_OC (35U)
#  endif

#  ifndef OnState_FaultDetected_VBT
#   define OnState_FaultDetected_VBT (38U)
#  endif

#  ifndef OnState_FaultDetected_VAT
#   define OnState_FaultDetected_VAT (39U)
#  endif

#  ifndef OnState_FaultDetected_VOR
#   define OnState_FaultDetected_VOR (41U)
#  endif

#  ifndef OnState_FaultDetected_CAT
#   define OnState_FaultDetected_CAT (44U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_SCIM_PVTPT_IO_TYPE_H */
