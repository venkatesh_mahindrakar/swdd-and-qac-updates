/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_SCIM_PVTPT_FlexData.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <SCIM_PVTPT_FlexData>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_SCIM_PVTPT_FLEXDATA_H
# define _RTE_SCIM_PVTPT_FLEXDATA_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_SCIM_PVTPT_FlexData_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_Debug_PVT_LF_Trig_Debug_PVT_LF_Trig (0U)
#  define Rte_InitValue_Debug_PVT_SCIM_FlexArrayData1_Debug_PVT_SCIM_FlexArrayData1 (0U)
#  define Rte_InitValue_Debug_PVT_SCIM_FlexArrayDataId_Debug_PVT_SCIM_FlexArrayDataId (0U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_FlexData_Debug_PVT_SCIM_FlexArrayData_Debug_PVT_SCIM_FlexArrayData(P2CONST(uint8, AUTOMATIC, RTE_SCIM_PVTPT_FLEXDATA_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_FlexData_Debug_PVT_SCIM_FlexArrayData_Debug_PVT_SCIM_FlexArrayData(P2CONST(Debug_PVT_SCIM_FlexArrayData, AUTOMATIC, RTE_SCIM_PVTPT_FLEXDATA_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_FlexData_Debug_PVT_SCIM_FlexArrayData1_Debug_PVT_SCIM_FlexArrayData1(Debug_PVT_SCIM_FlexArrayData1 data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SCIM_PVTPT_FlexData_Debug_PVT_SCIM_FlexArrayDataId_Debug_PVT_SCIM_FlexArrayDataId(Debug_PVT_SCIM_FlexArrayDataId data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_Debug_PVT_Flex_Request_Debug_PVT_FlexDataRequest Rte_Read_SCIM_PVTPT_FlexData_Debug_PVT_Flex_Request_Debug_PVT_FlexDataRequest
#  define Rte_Read_SCIM_PVTPT_FlexData_Debug_PVT_Flex_Request_Debug_PVT_FlexDataRequest(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_FlexDataRequest_oDebugCtrl1_CIOM_BB2_oBackbone2_52a16a4c_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Debug_PVT_LF_Trig_Debug_PVT_LF_Trig Rte_Read_SCIM_PVTPT_FlexData_Debug_PVT_LF_Trig_Debug_PVT_LF_Trig
#  define Rte_Read_SCIM_PVTPT_FlexData_Debug_PVT_LF_Trig_Debug_PVT_LF_Trig(data) (Com_ReceiveSignal(ComConf_ComSignal_Debug_PVT_LF_Trig_oDebugCtrl1_CIOM_BB2_oBackbone2_c300b3c6_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_Debug_PVT_SCIM_FlexArrayData_Debug_PVT_SCIM_FlexArrayData Rte_Write_SCIM_PVTPT_FlexData_Debug_PVT_SCIM_FlexArrayData_Debug_PVT_SCIM_FlexArrayData
#  define Rte_Write_Debug_PVT_SCIM_FlexArrayData1_Debug_PVT_SCIM_FlexArrayData1 Rte_Write_SCIM_PVTPT_FlexData_Debug_PVT_SCIM_FlexArrayData1_Debug_PVT_SCIM_FlexArrayData1
#  define Rte_Write_Debug_PVT_SCIM_FlexArrayDataId_Debug_PVT_SCIM_FlexArrayDataId Rte_Write_SCIM_PVTPT_FlexData_Debug_PVT_SCIM_FlexArrayDataId_Debug_PVT_SCIM_FlexArrayDataId


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_PEPS_APPL_CODE) RE_GetLfAntState(P2VAR(LfRssi, AUTOMATIC, RTE_PEPS_APPL_VAR) LfRssiStatus, P2VAR(uint8, AUTOMATIC, RTE_PEPS_APPL_VAR) FobFound, P2VAR(uint8, AUTOMATIC, RTE_PEPS_APPL_VAR) FobLocation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_LfInterface_P_GetLfAntState RE_GetLfAntState
#  define RTE_START_SEC_RKE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_RKE_APPL_CODE) RE_GetFobRkeState(P2VAR(ButtonStatus, AUTOMATIC, RTE_RKE_APPL_VAR) RkeButton, P2VAR(uint16, AUTOMATIC, RTE_RKE_APPL_VAR) FobID, P2VAR(uint32, AUTOMATIC, RTE_RKE_APPL_VAR) FobSN, P2VAR(uint16, AUTOMATIC, RTE_RKE_APPL_VAR) FobRollingCounter, P2VAR(uint16, AUTOMATIC, RTE_RKE_APPL_VAR) ScimRollingCounter, P2VAR(uint8, AUTOMATIC, RTE_RKE_APPL_VAR) FobBattery); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_RKE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RkeInterface_P_GetFobRkeState RE_GetFobRkeState


# endif /* !defined(RTE_CORE) */


# define SCIM_PVTPT_FlexData_START_SEC_CODE
# include "SCIM_PVTPT_FlexData_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_Runnable_PVTPT_Flex_Report Runnable_PVTPT_Flex_Report
#  define RTE_RUNNABLE_Runnable_PVTPT_Flex_Request Runnable_PVTPT_Flex_Request
# endif

FUNC(void, SCIM_PVTPT_FlexData_CODE) Runnable_PVTPT_Flex_Report(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, SCIM_PVTPT_FlexData_CODE) Runnable_PVTPT_Flex_Request(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define SCIM_PVTPT_FlexData_STOP_SEC_CODE
# include "SCIM_PVTPT_FlexData_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_LfInterface_I_RadioApplicationError (1U)

#  define RTE_E_RkeInterface_I_RadioApplicationError (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_SCIM_PVTPT_FLEXDATA_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
