/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_ReverseGearWarning_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <ReverseGearWarning_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_REVERSEGEARWARNING_HMICTRL_H
# define _RTE_REVERSEGEARWARNING_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_ReverseGearWarning_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ReverseGearWarning_HMICtrl_ReverseWarningInd_cmd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ReverseGearWarningBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ReverseGearWarningSw_stat_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ReverseGearWarningBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_ReverseGearWarningSw_stat_A2PosSwitchStatus (3U)
#  define Rte_InitValue_ReverseWarningInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_ReverseWarning_rqst_ReverseWarning_rqst (3U)
#  define Rte_InitValue_SwcActivation_IgnitionOn_IgnitionOn (1U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ReverseGearWarning_HMICtrl_RGW_HMICtrl_NVM_I_RGW_HMICtrl_NVM_I(P2VAR(uint8, AUTOMATIC, RTE_REVERSEGEARWARNING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ReverseGearWarning_HMICtrl_RGW_HMICtrl_NVM_I_RGW_HMICtrl_NVM_I(P2VAR(StandardNVM_T, AUTOMATIC, RTE_REVERSEGEARWARNING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ReverseGearWarning_HMICtrl_RGW_HMICtrl_NVM_I_RGW_HMICtrl_NVM_I(P2CONST(uint8, AUTOMATIC, RTE_REVERSEGEARWARNING_HMICTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ReverseGearWarning_HMICtrl_RGW_HMICtrl_NVM_I_RGW_HMICtrl_NVM_I(P2CONST(StandardNVM_T, AUTOMATIC, RTE_REVERSEGEARWARNING_HMICTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ReverseGearWarning_HMICtrl_ReverseWarning_rqst_ReverseWarning_rqst(ReverseWarning_rqst_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_RGW_HMICtrl_NVM_I_RGW_HMICtrl_NVM_I Rte_Read_ReverseGearWarning_HMICtrl_RGW_HMICtrl_NVM_I_RGW_HMICtrl_NVM_I
#  define Rte_Read_ReverseGearWarningBtn_stat_PushButtonStatus Rte_Read_ReverseGearWarning_HMICtrl_ReverseGearWarningBtn_stat_PushButtonStatus
#  define Rte_Read_ReverseGearWarning_HMICtrl_ReverseGearWarningBtn_stat_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_ReverseGearWarningBtn_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ReverseGearWarningSw_stat_A2PosSwitchStatus Rte_Read_ReverseGearWarning_HMICtrl_ReverseGearWarningSw_stat_A2PosSwitchStatus
#  define Rte_Read_ReverseGearWarning_HMICtrl_ReverseGearWarningSw_stat_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_ReverseGearWarningSw_stat_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_IgnitionOn_IgnitionOn Rte_Read_ReverseGearWarning_HMICtrl_SwcActivation_IgnitionOn_IgnitionOn
#  define Rte_Read_ReverseGearWarning_HMICtrl_SwcActivation_IgnitionOn_IgnitionOn(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_RGW_HMICtrl_NVM_I_RGW_HMICtrl_NVM_I Rte_Write_ReverseGearWarning_HMICtrl_RGW_HMICtrl_NVM_I_RGW_HMICtrl_NVM_I
#  define Rte_Write_ReverseWarningInd_cmd_DeviceIndication Rte_Write_ReverseGearWarning_HMICtrl_ReverseWarningInd_cmd_DeviceIndication
#  define Rte_Write_ReverseGearWarning_HMICtrl_ReverseWarningInd_cmd_DeviceIndication(data) (Rte_ReverseGearWarning_HMICtrl_ReverseWarningInd_cmd_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ReverseWarning_rqst_ReverseWarning_rqst Rte_Write_ReverseGearWarning_HMICtrl_ReverseWarning_rqst_ReverseWarning_rqst


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BXH_ReverseWarning_SwType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1BXH_ReverseWarning_SwType_v() (Rte_AddrPar_0x2B_P1BXH_ReverseWarning_SwType_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AJJ_ReverseWarning_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1AJJ_ReverseWarning_Act_v() (Rte_AddrPar_0x2B_and_0x37_P1AJJ_ReverseWarning_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define ReverseGearWarning_HMICtrl_START_SEC_CODE
# include "ReverseGearWarning_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_ReverseGearWarning_HMICtrl_20ms_runnable ReverseGearWarning_HMICtrl_20ms_runnable
# endif

FUNC(void, ReverseGearWarning_HMICtrl_CODE) ReverseGearWarning_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define ReverseGearWarning_HMICtrl_STOP_SEC_CODE
# include "ReverseGearWarning_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_REVERSEGEARWARNING_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
