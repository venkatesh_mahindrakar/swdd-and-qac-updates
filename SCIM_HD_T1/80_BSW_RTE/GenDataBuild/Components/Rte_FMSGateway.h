/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_FMSGateway.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <FMSGateway>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_FMSGATEWAY_H
# define _RTE_FMSGATEWAY_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_FMSGateway_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_AcceleratorPedalPosition1_AcceleratorPedalPosition1 (255U)
#  define Rte_InitValue_AcceleratorPedalPosition1_fms_AcceleratorPedalPosition1 (255U)
#  define Rte_InitValue_ActualDrvlnRetdrPctTrq_fms_ActualDrvlnRetdrPercentTorque (255U)
#  define Rte_InitValue_ActualDrvlnRetdrPercentTorque_ActualDrvlnRetdrPercentTorque (255U)
#  define Rte_InitValue_ActualEngRetdrPercentTrq_fms_ActualEngineRetarderPercentTrq (255U)
#  define Rte_InitValue_ActualEnginePercentTorque_ActualEnginePercentTorque (255U)
#  define Rte_InitValue_ActualEnginePercentTorque_fms_ActualEnginePercentTorque (255U)
#  define Rte_InitValue_ActualEngineRetarderPercentTrq_ActualEngineRetarderPercentTrq (255U)
#  define Rte_InitValue_AmbientAirTemperature_AmbientAirTemperature (65535U)
#  define Rte_InitValue_AmbientAirTemperature_fms_AmbientAirTemperature (65535U)
#  define Rte_InitValue_BrakeSwitch_BrakeSwitch (3U)
#  define Rte_InitValue_BrakeSwitch_fms_BrakeSwitch (3U)
#  define Rte_InitValue_CCActive_CCActive (3U)
#  define Rte_InitValue_CCActive_fms_CCActive (3U)
#  define Rte_InitValue_CatalystTankLevel_CatalystTankLevel (255U)
#  define Rte_InitValue_CatalystTankLevel_fms_CatalystTankLevel (255U)
#  define Rte_InitValue_ClutchSwitch_ClutchSwitch (3U)
#  define Rte_InitValue_ClutchSwitch_fms_ClutchSwitch (3U)
#  define Rte_InitValue_DirectionIndicator_fms_DirectionIndicator (3U)
#  define Rte_InitValue_Driver1TimeRelatedStates_Driver1TimeRelatedStates (15U)
#  define Rte_InitValue_Driver1TimeRelatedStates_fms_Driver1TimeRelatedStates (15U)
#  define Rte_InitValue_Driver1WorkingState_Driver1WorkingState (7U)
#  define Rte_InitValue_Driver1WorkingState_fms_Driver1WorkingState (7U)
#  define Rte_InitValue_Driver2TimeRelatedStates_Driver2TimeRelatedStates (15U)
#  define Rte_InitValue_Driver2TimeRelatedStates_fms_Driver2TimeRelatedStates (15U)
#  define Rte_InitValue_Driver2WorkingState_Driver2WorkingState (7U)
#  define Rte_InitValue_Driver2WorkingState_fms_Driver2WorkingState (7U)
#  define Rte_InitValue_DriverCardDriver1_DriverCardDriver1 (3U)
#  define Rte_InitValue_DriverCardDriver1_fms_DriverCardDriver1 (3U)
#  define Rte_InitValue_DriverCardDriver2_DriverCardDriver2 (3U)
#  define Rte_InitValue_DriverCardDriver2_fms_DriverCardDriver2 (3U)
#  define Rte_InitValue_EngineCoolantTemp_stat_EngineCoolantTemp_stat (255U)
#  define Rte_InitValue_EngineCoolantTemp_stat_fms_EngineCoolantTemp_stat (255U)
#  define Rte_InitValue_EngineFuelRate_EngineFuelRate (65535U)
#  define Rte_InitValue_EngineFuelRate_fms_EngineFuelRate (65535U)
#  define Rte_InitValue_EngineGasRate_EngineGasRate (65535U)
#  define Rte_InitValue_EnginePctLoadAtCurrentSpd_fms_EnginePercentLoadAtCurrentSpd (255U)
#  define Rte_InitValue_EnginePercentLoadAtCurrentSpd_EnginePercentLoadAtCurrentSpd (255U)
#  define Rte_InitValue_EngineRetarderTorqueMode_EngineRetarderTorqueMode (15U)
#  define Rte_InitValue_EngineRetarderTorqueMode_fms_EngineRetarderTorqueMode (15U)
#  define Rte_InitValue_EngineRunningTime_EngineRunningTime (4294967295U)
#  define Rte_InitValue_EngineSpeed_EngineSpeed (65535U)
#  define Rte_InitValue_EngineSpeed_fms_EngineSpeed (65535U)
#  define Rte_InitValue_EngineTotalFuelConsumed_EngineTotalFuelConsumed (4294967295U)
#  define Rte_InitValue_EngineTotalLngConsumed_EngineTotalLngConsumed (4294967295U)
#  define Rte_InitValue_FMSAtLeastOnePTOEngaged_AtLeastOnePTOEngaged (3U)
#  define Rte_InitValue_FMSAxleLocation_AxleLocation (255U)
#  define Rte_InitValue_FMSAxleWeight_AxleWeight (65535U)
#  define Rte_InitValue_FMSDiagnosisSupported_FMSDiagnosisSupported (3U)
#  define Rte_InitValue_FMSEngineTotHoursOfOp_EngineTotalHoursOfOperation (4294967295U)
#  define Rte_InitValue_FMSEngineTotalFuelUsed_FMSEngineTotalFuelUsed (4294967295U)
#  define Rte_InitValue_FMSHiResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed (4294967295U)
#  define Rte_InitValue_FMSInstantFuelEconomy_FMSInstantFuelEconomy (65535U)
#  define Rte_InitValue_FMSPtoState_PtoState (31U)
#  define Rte_InitValue_FMSRequestsSupported_FMSRequestsSupported (3U)
#  define Rte_InitValue_FMSServiceDistance_FMSServiceDistance (65535U)
#  define Rte_InitValue_FuelLevel_FuelLevel (255U)
#  define Rte_InitValue_FuelLevel_fms_FuelLevel (255U)
#  define Rte_InitValue_FuelType_FuelType (255U)
#  define Rte_InitValue_GrossCombVehicleWeight_fms_GrossCombinationVehicleWeight (65535U)
#  define Rte_InitValue_GrossCombinationVehicleWeight_GrossCombinationVehicleWeight (65535U)
#  define Rte_InitValue_HandlingInformation_HandlingInformation (3U)
#  define Rte_InitValue_HandlingInformation_fms_HandlingInformation (3U)
#  define Rte_InitValue_HighResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed (4294967295U)
#  define Rte_InitValue_InstantaneousFuelEconomy_FMSInstantFuelEconomy (65535U)
#  define Rte_InitValue_LngTank1RemainingGasVolume_LngTank1RemainingGasVolume (255U)
#  define Rte_InitValue_LngTank2RemainingGasVolume_LngTank2RemainingGasVolume (255U)
#  define Rte_InitValue_PGNReq_Tacho_CIOM_PGNRequest (65131U)
#  define Rte_InitValue_PtosStatus_PtosStatus (3U)
#  define Rte_InitValue_RetarderTorqueMode_RetarderTorqueMode (15U)
#  define Rte_InitValue_RetarderTorqueMode_fms_RetarderTorqueMode (15U)
#  define Rte_InitValue_ReverseGearEngaged_ReverseGearEngaged (3U)
#  define Rte_InitValue_ServiceBrakeAirPrsCircuit1_ServiceBrakeAirPrsCircuit1 (255U)
#  define Rte_InitValue_ServiceBrakeAirPrsCircuit1_fms_ServiceBrakeAirPrsCircuit1 (255U)
#  define Rte_InitValue_ServiceBrakeAirPrsCircuit2_ServiceBrakeAirPrsCircuit2 (255U)
#  define Rte_InitValue_ServiceBrakeAirPrsCircuit2_fms_ServiceBrakeAirPrsCircuit2 (255U)
#  define Rte_InitValue_SwcActivation_IgnitionOn_IgnitionOn (1U)
#  define Rte_InitValue_SystemEvent_SystemEvent (3U)
#  define Rte_InitValue_SystemEvent_fms_SystemEvent (3U)
#  define Rte_InitValue_TachographPerformance_TachographPerformance (3U)
#  define Rte_InitValue_TachographPerformance_fms_TachographPerformance (3U)
#  define Rte_InitValue_TachographVehicleSpeed_TachographVehicleSpeed (65535U)
#  define Rte_InitValue_TachographVehicleSpeed_fms_TachographVehicleSpeed (65535U)
#  define Rte_InitValue_TotalVehicleDistanceHiRes_fms_TotalVehicleDistanceHighRes (4294967295U)
#  define Rte_InitValue_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes (4294967295U)
#  define Rte_InitValue_VehFrontAxle1CalibratedLoad_VehicleAxleCalibratedLoad (65535U)
#  define Rte_InitValue_VehRearAxle1CalibratedLoad_VehicleAxleCalibratedLoad (65535U)
#  define Rte_InitValue_VehRearAxle2CalibratedLoad_VehicleAxleCalibratedLoad (65535U)
#  define Rte_InitValue_VehRearAxle3CalibratedLoad_VehicleAxleCalibratedLoad (65535U)
#  define Rte_InitValue_VehicleMotion_VehicleMotion (3U)
#  define Rte_InitValue_VehicleMotion_fms_VehicleMotion (3U)
#  define Rte_InitValue_VehicleOverspeed_VehicleOverspeed (3U)
#  define Rte_InitValue_VehicleOverspeed_fms_VehicleOverspeed (3U)
#  define Rte_InitValue_WeightClass_WeightClass (15U)
#  define Rte_InitValue_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed (65535U)
#  define Rte_InitValue_WheelBasedVehicleSpeed_fms_WheelBasedVehicleSpeed (65535U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Receive_FMSGateway_Driver1Identification_Driver1Identification(P2VAR(uint8, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Receive_FMSGateway_Driver1Identification_Driver1Identification(P2VAR(Driver1Identification_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Receive_FMSGateway_DriversIdentifications_DriversIdentifications(P2VAR(uint8, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data, P2VAR(uint16, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) length); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Receive_FMSGateway_DriversIdentifications_DriversIdentifications(P2VAR(DriversIdentifications_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data, P2VAR(uint16, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) length); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Receive_FMSGateway_MaintService_MaintService(P2VAR(MaintService_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_AcceleratorPedalPosition1_AcceleratorPedalPosition1(P2VAR(Percent8bitFactor04_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_ActualDrvlnRetdrPercentTorque_ActualDrvlnRetdrPercentTorque(P2VAR(Percent8bit125NegOffset_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_ActualEnginePercentTorque_ActualEnginePercentTorque(P2VAR(Percent8bit125NegOffset_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_ActualEngineRetarderPercentTrq_ActualEngineRetarderPercentTrq(P2VAR(Percent8bit125NegOffset_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_AmbientAirTemperature_AmbientAirTemperature(P2VAR(Temperature16bit_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_BrakeSwitch_BrakeSwitch(P2VAR(BrakeSwitch_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_CCActive_CCActive(P2VAR(OffOn_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_CatalystTankLevel_CatalystTankLevel(P2VAR(Percent8bitFactor04_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_ClutchSwitch_ClutchSwitch(P2VAR(ClutchSwitch_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_Driver1TimeRelatedStates_Driver1TimeRelatedStates(P2VAR(Driver1TimeRelatedStates_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_Driver1WorkingState_Driver1WorkingState(P2VAR(DriverWorkingState_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_Driver2TimeRelatedStates_Driver2TimeRelatedStates(P2VAR(DriverTimeRelatedStates_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_Driver2WorkingState_Driver2WorkingState(P2VAR(DriverWorkingState_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_DriverCardDriver1_DriverCardDriver1(P2VAR(NotPresentPresent_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_DriverCardDriver2_DriverCardDriver2(P2VAR(NotPresentPresent_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_EngineCoolantTemp_stat_EngineCoolantTemp_stat(P2VAR(EngineTemp_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_EngineFuelRate_EngineFuelRate(P2VAR(FuelRate_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_EngineGasRate_EngineGasRate(P2VAR(GasRate_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_EnginePercentLoadAtCurrentSpd_EnginePercentLoadAtCurrentSpd(P2VAR(Percent8bitNoOffset_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_EngineRetarderTorqueMode_EngineRetarderTorqueMode(P2VAR(EngineRetarderTorqueMode_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_EngineRunningTime_EngineRunningTime(P2VAR(Seconds32bitExtra_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_EngineSpeed_EngineSpeed(P2VAR(SpeedRpm16bit_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_EngineTotalFuelConsumed_EngineTotalFuelConsumed(P2VAR(Volume32bit_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_EngineTotalLngConsumed_EngineTotalLngConsumed(P2VAR(TotalLngConsumed_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_FMS1_FMS1(P2VAR(FMS1_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_FuelLevel_FuelLevel(P2VAR(Percent8bitFactor04_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_GrossCombinationVehicleWeight_GrossCombinationVehicleWeight(P2VAR(VehicleWeight16bit_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_HandlingInformation_HandlingInformation(P2VAR(HandlingInformation_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_HighResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed(P2VAR(Volume32BitFact001_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_InstantaneousFuelEconomy_FMSInstantFuelEconomy(P2VAR(InstantFuelEconomy_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_LngTank1RemainingGasVolume_LngTank1RemainingGasVolume(P2VAR(Percent8bitFactor04_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_LngTank2RemainingGasVolume_LngTank2RemainingGasVolume(P2VAR(Percent8bitFactor04_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_OilPrediction_OilPrediction(P2VAR(OilPrediction_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_PtosStatus_PtosStatus(P2VAR(PtosStatus_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_RetarderTorqueMode_RetarderTorqueMode(P2VAR(RetarderTorqueMode_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_ReverseGearEngaged_ReverseGearEngaged(P2VAR(ReverseGearEngaged_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_ServiceBrakeAirPrsCircuit1_ServiceBrakeAirPrsCircuit1(P2VAR(PressureFactor8_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_ServiceBrakeAirPrsCircuit2_ServiceBrakeAirPrsCircuit2(P2VAR(PressureFactor8_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_SystemEvent_SystemEvent(P2VAR(SystemEvent_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_TachographPerformance_TachographPerformance(P2VAR(TachographPerformance_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_TachographVehicleSpeed_TachographVehicleSpeed(P2VAR(Speed16bit_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes(P2VAR(Distance32bit_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_VehFrontAxle1CalibratedLoad_VehicleAxleCalibratedLoad(P2VAR(AxleLoad_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_VehRearAxle1CalibratedLoad_VehicleAxleCalibratedLoad(P2VAR(AxleLoad_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_VehRearAxle2CalibratedLoad_VehicleAxleCalibratedLoad(P2VAR(AxleLoad_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_VehRearAxle3CalibratedLoad_VehicleAxleCalibratedLoad(P2VAR(AxleLoad_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_VehicleMotion_VehicleMotion(P2VAR(NotDetected_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_VehicleOverspeed_VehicleOverspeed(P2VAR(VehicleOverspeed_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FMSGateway_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(P2VAR(Speed16bit_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Send_FMSGateway_FMSDriversIdentifications_DriversIdentifications(P2CONST(uint8, AUTOMATIC, RTE_FMSGATEWAY_APPL_DATA) data, uint16 length); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Send_FMSGateway_FMSDriversIdentifications_DriversIdentifications(P2CONST(DriversIdentifications_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_DATA) data, uint16 length); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Send_FMSGateway_FMSVehicleIdentNumber_VehicleIdentNumber(P2CONST(uint8, AUTOMATIC, RTE_FMSGATEWAY_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Send_FMSGateway_FMSVehicleIdentNumber_VehicleIdentNumber(P2CONST(VehicleIdentNumber_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_AcceleratorPedalPosition1_fms_AcceleratorPedalPosition1(Percent8bitFactor04_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_ActualDrvlnRetdrPctTrq_fms_ActualDrvlnRetdrPercentTorque(Percent8bit125NegOffset_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_ActualEngRetdrPercentTrq_fms_ActualEngineRetarderPercentTrq(Percent8bit125NegOffset_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_ActualEnginePercentTorque_fms_ActualEnginePercentTorque(Percent8bit125NegOffset_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_AmbientAirTemperature_fms_AmbientAirTemperature(Temperature16bit_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_BrakeSwitch_fms_BrakeSwitch(BrakeSwitch_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_CCActive_fms_CCActive(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_CatalystTankLevel_fms_CatalystTankLevel(Percent8bitFactor04_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_ClutchSwitch_fms_ClutchSwitch(ClutchSwitch_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_DirectionIndicator_fms_DirectionIndicator(DirectionIndicator_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_Driver1TimeRelatedStates_fms_Driver1TimeRelatedStates(Driver1TimeRelatedStates_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_Driver1WorkingState_fms_Driver1WorkingState(DriverWorkingState_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_Driver2TimeRelatedStates_fms_Driver2TimeRelatedStates(DriverTimeRelatedStates_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_Driver2WorkingState_fms_Driver2WorkingState(DriverWorkingState_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_DriverCardDriver1_fms_DriverCardDriver1(NotPresentPresent_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_DriverCardDriver2_fms_DriverCardDriver2(NotPresentPresent_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_EngineCoolantTemp_stat_fms_EngineCoolantTemp_stat(EngineTemp_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_EngineFuelRate_fms_EngineFuelRate(FuelRate_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_EnginePctLoadAtCurrentSpd_fms_EnginePercentLoadAtCurrentSpd(Percent8bitNoOffset_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_EngineRetarderTorqueMode_fms_EngineRetarderTorqueMode(EngineRetarderTorqueMode_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_EngineSpeed_fms_EngineSpeed(SpeedRpm16bit_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_FMS1_fms_FMS1(P2CONST(FMS1_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_FMSAtLeastOnePTOEngaged_AtLeastOnePTOEngaged(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_FMSAxleLocation_AxleLocation(Int8Bit_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_FMSAxleWeight_AxleWeight(AxleLoad_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_FMSDiagnosisSupported_FMSDiagnosisSupported(Supported_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_FMSEngineTotHoursOfOp_EngineTotalHoursOfOperation(EngineTotalHoursOfOperation_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_FMSEngineTotalFuelUsed_FMSEngineTotalFuelUsed(Volume32bitFact05_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_FMSHiResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed(Volume32BitFact001_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_FMSInstantFuelEconomy_FMSInstantFuelEconomy(InstantFuelEconomy_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_FMSPtoState_PtoState(PtoState_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_FMSRequestsSupported_FMSRequestsSupported(Supported_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_FMSServiceDistance_FMSServiceDistance(ServiceDistance16BitFact5Offset_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_FMSSoftwareVersionSupported_FMSSoftwareVersionSupported(P2CONST(uint8, AUTOMATIC, RTE_FMSGATEWAY_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_FMSSoftwareVersionSupported_FMSSoftwareVersionSupported(P2CONST(SoftwareVersionSupported_T, AUTOMATIC, RTE_FMSGATEWAY_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_FuelLevel_fms_FuelLevel(Percent8bitFactor04_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_FuelType_FuelType(FuelType_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_GrossCombVehicleWeight_fms_GrossCombinationVehicleWeight(VehicleWeight16bit_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_HandlingInformation_fms_HandlingInformation(HandlingInformation_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_PGNReq_Tacho_CIOM_PGNRequest(PGNRequest_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_RetarderTorqueMode_fms_RetarderTorqueMode(RetarderTorqueMode_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_ServiceBrakeAirPrsCircuit1_fms_ServiceBrakeAirPrsCircuit1(PressureFactor8_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_ServiceBrakeAirPrsCircuit2_fms_ServiceBrakeAirPrsCircuit2(PressureFactor8_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_SystemEvent_fms_SystemEvent(SystemEvent_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_TachographPerformance_fms_TachographPerformance(TachographPerformance_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_TachographVehicleSpeed_fms_TachographVehicleSpeed(Speed16bit_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_TotalVehicleDistanceHiRes_fms_TotalVehicleDistanceHighRes(Distance32bit_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_VehicleMotion_fms_VehicleMotion(NotDetected_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_VehicleOverspeed_fms_VehicleOverspeed(VehicleOverspeed_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_WeightClass_WeightClass(WeightClass_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FMSGateway_WheelBasedVehicleSpeed_fms_WheelBasedVehicleSpeed(Speed16bit_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Receive_<p>_<d> (explicit S/R communication with isQueued = true)
 *********************************************************************************************************************/
#  define Rte_Receive_Driver1Identification_Driver1Identification Rte_Receive_FMSGateway_Driver1Identification_Driver1Identification
#  define Rte_Receive_DriversIdentifications_DriversIdentifications Rte_Receive_FMSGateway_DriversIdentifications_DriversIdentifications
#  define Rte_Receive_MaintService_MaintService Rte_Receive_FMSGateway_MaintService_MaintService


/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_AcceleratorPedalPosition1_AcceleratorPedalPosition1 Rte_Read_FMSGateway_AcceleratorPedalPosition1_AcceleratorPedalPosition1
#  define Rte_Read_ActualDrvlnRetdrPercentTorque_ActualDrvlnRetdrPercentTorque Rte_Read_FMSGateway_ActualDrvlnRetdrPercentTorque_ActualDrvlnRetdrPercentTorque
#  define Rte_Read_ActualEnginePercentTorque_ActualEnginePercentTorque Rte_Read_FMSGateway_ActualEnginePercentTorque_ActualEnginePercentTorque
#  define Rte_Read_ActualEngineRetarderPercentTrq_ActualEngineRetarderPercentTrq Rte_Read_FMSGateway_ActualEngineRetarderPercentTrq_ActualEngineRetarderPercentTrq
#  define Rte_Read_AmbientAirTemperature_AmbientAirTemperature Rte_Read_FMSGateway_AmbientAirTemperature_AmbientAirTemperature
#  define Rte_Read_BrakeSwitch_BrakeSwitch Rte_Read_FMSGateway_BrakeSwitch_BrakeSwitch
#  define Rte_Read_CCActive_CCActive Rte_Read_FMSGateway_CCActive_CCActive
#  define Rte_Read_CatalystTankLevel_CatalystTankLevel Rte_Read_FMSGateway_CatalystTankLevel_CatalystTankLevel
#  define Rte_Read_ClutchSwitch_ClutchSwitch Rte_Read_FMSGateway_ClutchSwitch_ClutchSwitch
#  define Rte_Read_Driver1TimeRelatedStates_Driver1TimeRelatedStates Rte_Read_FMSGateway_Driver1TimeRelatedStates_Driver1TimeRelatedStates
#  define Rte_Read_Driver1WorkingState_Driver1WorkingState Rte_Read_FMSGateway_Driver1WorkingState_Driver1WorkingState
#  define Rte_Read_Driver2TimeRelatedStates_Driver2TimeRelatedStates Rte_Read_FMSGateway_Driver2TimeRelatedStates_Driver2TimeRelatedStates
#  define Rte_Read_Driver2WorkingState_Driver2WorkingState Rte_Read_FMSGateway_Driver2WorkingState_Driver2WorkingState
#  define Rte_Read_DriverCardDriver1_DriverCardDriver1 Rte_Read_FMSGateway_DriverCardDriver1_DriverCardDriver1
#  define Rte_Read_DriverCardDriver2_DriverCardDriver2 Rte_Read_FMSGateway_DriverCardDriver2_DriverCardDriver2
#  define Rte_Read_EngineCoolantTemp_stat_EngineCoolantTemp_stat Rte_Read_FMSGateway_EngineCoolantTemp_stat_EngineCoolantTemp_stat
#  define Rte_Read_EngineFuelRate_EngineFuelRate Rte_Read_FMSGateway_EngineFuelRate_EngineFuelRate
#  define Rte_Read_EngineGasRate_EngineGasRate Rte_Read_FMSGateway_EngineGasRate_EngineGasRate
#  define Rte_Read_EnginePercentLoadAtCurrentSpd_EnginePercentLoadAtCurrentSpd Rte_Read_FMSGateway_EnginePercentLoadAtCurrentSpd_EnginePercentLoadAtCurrentSpd
#  define Rte_Read_EngineRetarderTorqueMode_EngineRetarderTorqueMode Rte_Read_FMSGateway_EngineRetarderTorqueMode_EngineRetarderTorqueMode
#  define Rte_Read_EngineRunningTime_EngineRunningTime Rte_Read_FMSGateway_EngineRunningTime_EngineRunningTime
#  define Rte_Read_EngineSpeed_EngineSpeed Rte_Read_FMSGateway_EngineSpeed_EngineSpeed
#  define Rte_Read_EngineTotalFuelConsumed_EngineTotalFuelConsumed Rte_Read_FMSGateway_EngineTotalFuelConsumed_EngineTotalFuelConsumed
#  define Rte_Read_EngineTotalLngConsumed_EngineTotalLngConsumed Rte_Read_FMSGateway_EngineTotalLngConsumed_EngineTotalLngConsumed
#  define Rte_Read_FMS1_FMS1 Rte_Read_FMSGateway_FMS1_FMS1
#  define Rte_Read_FuelLevel_FuelLevel Rte_Read_FMSGateway_FuelLevel_FuelLevel
#  define Rte_Read_GrossCombinationVehicleWeight_GrossCombinationVehicleWeight Rte_Read_FMSGateway_GrossCombinationVehicleWeight_GrossCombinationVehicleWeight
#  define Rte_Read_HandlingInformation_HandlingInformation Rte_Read_FMSGateway_HandlingInformation_HandlingInformation
#  define Rte_Read_HighResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed Rte_Read_FMSGateway_HighResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed
#  define Rte_Read_InstantaneousFuelEconomy_FMSInstantFuelEconomy Rte_Read_FMSGateway_InstantaneousFuelEconomy_FMSInstantFuelEconomy
#  define Rte_Read_LngTank1RemainingGasVolume_LngTank1RemainingGasVolume Rte_Read_FMSGateway_LngTank1RemainingGasVolume_LngTank1RemainingGasVolume
#  define Rte_Read_LngTank2RemainingGasVolume_LngTank2RemainingGasVolume Rte_Read_FMSGateway_LngTank2RemainingGasVolume_LngTank2RemainingGasVolume
#  define Rte_Read_OilPrediction_OilPrediction Rte_Read_FMSGateway_OilPrediction_OilPrediction
#  define Rte_Read_PtosStatus_PtosStatus Rte_Read_FMSGateway_PtosStatus_PtosStatus
#  define Rte_Read_RetarderTorqueMode_RetarderTorqueMode Rte_Read_FMSGateway_RetarderTorqueMode_RetarderTorqueMode
#  define Rte_Read_ReverseGearEngaged_ReverseGearEngaged Rte_Read_FMSGateway_ReverseGearEngaged_ReverseGearEngaged
#  define Rte_Read_ServiceBrakeAirPrsCircuit1_ServiceBrakeAirPrsCircuit1 Rte_Read_FMSGateway_ServiceBrakeAirPrsCircuit1_ServiceBrakeAirPrsCircuit1
#  define Rte_Read_ServiceBrakeAirPrsCircuit2_ServiceBrakeAirPrsCircuit2 Rte_Read_FMSGateway_ServiceBrakeAirPrsCircuit2_ServiceBrakeAirPrsCircuit2
#  define Rte_Read_SwcActivation_IgnitionOn_IgnitionOn Rte_Read_FMSGateway_SwcActivation_IgnitionOn_IgnitionOn
#  define Rte_Read_FMSGateway_SwcActivation_IgnitionOn_IgnitionOn(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SystemEvent_SystemEvent Rte_Read_FMSGateway_SystemEvent_SystemEvent
#  define Rte_Read_TachographPerformance_TachographPerformance Rte_Read_FMSGateway_TachographPerformance_TachographPerformance
#  define Rte_Read_TachographVehicleSpeed_TachographVehicleSpeed Rte_Read_FMSGateway_TachographVehicleSpeed_TachographVehicleSpeed
#  define Rte_Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes Rte_Read_FMSGateway_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes
#  define Rte_Read_VehFrontAxle1CalibratedLoad_VehicleAxleCalibratedLoad Rte_Read_FMSGateway_VehFrontAxle1CalibratedLoad_VehicleAxleCalibratedLoad
#  define Rte_Read_VehRearAxle1CalibratedLoad_VehicleAxleCalibratedLoad Rte_Read_FMSGateway_VehRearAxle1CalibratedLoad_VehicleAxleCalibratedLoad
#  define Rte_Read_VehRearAxle2CalibratedLoad_VehicleAxleCalibratedLoad Rte_Read_FMSGateway_VehRearAxle2CalibratedLoad_VehicleAxleCalibratedLoad
#  define Rte_Read_VehRearAxle3CalibratedLoad_VehicleAxleCalibratedLoad Rte_Read_FMSGateway_VehRearAxle3CalibratedLoad_VehicleAxleCalibratedLoad
#  define Rte_Read_VehicleMotion_VehicleMotion Rte_Read_FMSGateway_VehicleMotion_VehicleMotion
#  define Rte_Read_VehicleOverspeed_VehicleOverspeed Rte_Read_FMSGateway_VehicleOverspeed_VehicleOverspeed
#  define Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed Rte_Read_FMSGateway_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed


/**********************************************************************************************************************
 * Rte_Send_<p>_<d> (explicit S/R communication with isQueued = true)
 *********************************************************************************************************************/
#  define Rte_Send_FMSDriversIdentifications_DriversIdentifications Rte_Send_FMSGateway_FMSDriversIdentifications_DriversIdentifications
#  define Rte_Send_FMSVehicleIdentNumber_VehicleIdentNumber Rte_Send_FMSGateway_FMSVehicleIdentNumber_VehicleIdentNumber


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_AcceleratorPedalPosition1_fms_AcceleratorPedalPosition1 Rte_Write_FMSGateway_AcceleratorPedalPosition1_fms_AcceleratorPedalPosition1
#  define Rte_Write_ActualDrvlnRetdrPctTrq_fms_ActualDrvlnRetdrPercentTorque Rte_Write_FMSGateway_ActualDrvlnRetdrPctTrq_fms_ActualDrvlnRetdrPercentTorque
#  define Rte_Write_ActualEngRetdrPercentTrq_fms_ActualEngineRetarderPercentTrq Rte_Write_FMSGateway_ActualEngRetdrPercentTrq_fms_ActualEngineRetarderPercentTrq
#  define Rte_Write_ActualEnginePercentTorque_fms_ActualEnginePercentTorque Rte_Write_FMSGateway_ActualEnginePercentTorque_fms_ActualEnginePercentTorque
#  define Rte_Write_AmbientAirTemperature_fms_AmbientAirTemperature Rte_Write_FMSGateway_AmbientAirTemperature_fms_AmbientAirTemperature
#  define Rte_Write_BrakeSwitch_fms_BrakeSwitch Rte_Write_FMSGateway_BrakeSwitch_fms_BrakeSwitch
#  define Rte_Write_CCActive_fms_CCActive Rte_Write_FMSGateway_CCActive_fms_CCActive
#  define Rte_Write_CatalystTankLevel_fms_CatalystTankLevel Rte_Write_FMSGateway_CatalystTankLevel_fms_CatalystTankLevel
#  define Rte_Write_ClutchSwitch_fms_ClutchSwitch Rte_Write_FMSGateway_ClutchSwitch_fms_ClutchSwitch
#  define Rte_Write_DirectionIndicator_fms_DirectionIndicator Rte_Write_FMSGateway_DirectionIndicator_fms_DirectionIndicator
#  define Rte_Write_Driver1TimeRelatedStates_fms_Driver1TimeRelatedStates Rte_Write_FMSGateway_Driver1TimeRelatedStates_fms_Driver1TimeRelatedStates
#  define Rte_Write_Driver1WorkingState_fms_Driver1WorkingState Rte_Write_FMSGateway_Driver1WorkingState_fms_Driver1WorkingState
#  define Rte_Write_Driver2TimeRelatedStates_fms_Driver2TimeRelatedStates Rte_Write_FMSGateway_Driver2TimeRelatedStates_fms_Driver2TimeRelatedStates
#  define Rte_Write_Driver2WorkingState_fms_Driver2WorkingState Rte_Write_FMSGateway_Driver2WorkingState_fms_Driver2WorkingState
#  define Rte_Write_DriverCardDriver1_fms_DriverCardDriver1 Rte_Write_FMSGateway_DriverCardDriver1_fms_DriverCardDriver1
#  define Rte_Write_DriverCardDriver2_fms_DriverCardDriver2 Rte_Write_FMSGateway_DriverCardDriver2_fms_DriverCardDriver2
#  define Rte_Write_EngineCoolantTemp_stat_fms_EngineCoolantTemp_stat Rte_Write_FMSGateway_EngineCoolantTemp_stat_fms_EngineCoolantTemp_stat
#  define Rte_Write_EngineFuelRate_fms_EngineFuelRate Rte_Write_FMSGateway_EngineFuelRate_fms_EngineFuelRate
#  define Rte_Write_EnginePctLoadAtCurrentSpd_fms_EnginePercentLoadAtCurrentSpd Rte_Write_FMSGateway_EnginePctLoadAtCurrentSpd_fms_EnginePercentLoadAtCurrentSpd
#  define Rte_Write_EngineRetarderTorqueMode_fms_EngineRetarderTorqueMode Rte_Write_FMSGateway_EngineRetarderTorqueMode_fms_EngineRetarderTorqueMode
#  define Rte_Write_EngineSpeed_fms_EngineSpeed Rte_Write_FMSGateway_EngineSpeed_fms_EngineSpeed
#  define Rte_Write_FMS1_fms_FMS1 Rte_Write_FMSGateway_FMS1_fms_FMS1
#  define Rte_Write_FMSAtLeastOnePTOEngaged_AtLeastOnePTOEngaged Rte_Write_FMSGateway_FMSAtLeastOnePTOEngaged_AtLeastOnePTOEngaged
#  define Rte_Write_FMSAxleLocation_AxleLocation Rte_Write_FMSGateway_FMSAxleLocation_AxleLocation
#  define Rte_Write_FMSAxleWeight_AxleWeight Rte_Write_FMSGateway_FMSAxleWeight_AxleWeight
#  define Rte_Write_FMSDiagnosisSupported_FMSDiagnosisSupported Rte_Write_FMSGateway_FMSDiagnosisSupported_FMSDiagnosisSupported
#  define Rte_Write_FMSEngineTotHoursOfOp_EngineTotalHoursOfOperation Rte_Write_FMSGateway_FMSEngineTotHoursOfOp_EngineTotalHoursOfOperation
#  define Rte_Write_FMSEngineTotalFuelUsed_FMSEngineTotalFuelUsed Rte_Write_FMSGateway_FMSEngineTotalFuelUsed_FMSEngineTotalFuelUsed
#  define Rte_Write_FMSHiResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed Rte_Write_FMSGateway_FMSHiResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed
#  define Rte_Write_FMSInstantFuelEconomy_FMSInstantFuelEconomy Rte_Write_FMSGateway_FMSInstantFuelEconomy_FMSInstantFuelEconomy
#  define Rte_Write_FMSPtoState_PtoState Rte_Write_FMSGateway_FMSPtoState_PtoState
#  define Rte_Write_FMSRequestsSupported_FMSRequestsSupported Rte_Write_FMSGateway_FMSRequestsSupported_FMSRequestsSupported
#  define Rte_Write_FMSServiceDistance_FMSServiceDistance Rte_Write_FMSGateway_FMSServiceDistance_FMSServiceDistance
#  define Rte_Write_FMSSoftwareVersionSupported_FMSSoftwareVersionSupported Rte_Write_FMSGateway_FMSSoftwareVersionSupported_FMSSoftwareVersionSupported
#  define Rte_Write_FuelLevel_fms_FuelLevel Rte_Write_FMSGateway_FuelLevel_fms_FuelLevel
#  define Rte_Write_FuelType_FuelType Rte_Write_FMSGateway_FuelType_FuelType
#  define Rte_Write_GrossCombVehicleWeight_fms_GrossCombinationVehicleWeight Rte_Write_FMSGateway_GrossCombVehicleWeight_fms_GrossCombinationVehicleWeight
#  define Rte_Write_HandlingInformation_fms_HandlingInformation Rte_Write_FMSGateway_HandlingInformation_fms_HandlingInformation
#  define Rte_Write_PGNReq_Tacho_CIOM_PGNRequest Rte_Write_FMSGateway_PGNReq_Tacho_CIOM_PGNRequest
#  define Rte_Write_RetarderTorqueMode_fms_RetarderTorqueMode Rte_Write_FMSGateway_RetarderTorqueMode_fms_RetarderTorqueMode
#  define Rte_Write_ServiceBrakeAirPrsCircuit1_fms_ServiceBrakeAirPrsCircuit1 Rte_Write_FMSGateway_ServiceBrakeAirPrsCircuit1_fms_ServiceBrakeAirPrsCircuit1
#  define Rte_Write_ServiceBrakeAirPrsCircuit2_fms_ServiceBrakeAirPrsCircuit2 Rte_Write_FMSGateway_ServiceBrakeAirPrsCircuit2_fms_ServiceBrakeAirPrsCircuit2
#  define Rte_Write_SystemEvent_fms_SystemEvent Rte_Write_FMSGateway_SystemEvent_fms_SystemEvent
#  define Rte_Write_TachographPerformance_fms_TachographPerformance Rte_Write_FMSGateway_TachographPerformance_fms_TachographPerformance
#  define Rte_Write_TachographVehicleSpeed_fms_TachographVehicleSpeed Rte_Write_FMSGateway_TachographVehicleSpeed_fms_TachographVehicleSpeed
#  define Rte_Write_TotalVehicleDistanceHiRes_fms_TotalVehicleDistanceHighRes Rte_Write_FMSGateway_TotalVehicleDistanceHiRes_fms_TotalVehicleDistanceHighRes
#  define Rte_Write_VehicleMotion_fms_VehicleMotion Rte_Write_FMSGateway_VehicleMotion_fms_VehicleMotion
#  define Rte_Write_VehicleOverspeed_fms_VehicleOverspeed Rte_Write_FMSGateway_VehicleOverspeed_fms_VehicleOverspeed
#  define Rte_Write_WeightClass_WeightClass Rte_Write_FMSGateway_WeightClass_WeightClass
#  define Rte_Write_WheelBasedVehicleSpeed_fms_WheelBasedVehicleSpeed Rte_Write_FMSGateway_WheelBasedVehicleSpeed_fms_WheelBasedVehicleSpeed


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_LNGTank1Volume_P1LX9_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LX9_LNGTank1Volume_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_LNGTank2Volume_P1LYA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LYA_LNGTank2Volume_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_WeightClassInformation_P1M7S_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1M7S_WeightClassInformation_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FuelTypeInformation_P1M7T_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1M7T_FuelTypeInformation_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_CabHeightValue_P1R0P_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1R0P_CabHeightValue_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DXX_FMSgateway_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LX8_EngineFuelTypeLNG_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1M7Q_EraGlonassUnitInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1LX9_LNGTank1Volume_v() (Rte_AddrPar_0x2B_P1LX9_LNGTank1Volume_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1LYA_LNGTank2Volume_v() (Rte_AddrPar_0x2B_P1LYA_LNGTank2Volume_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1M7S_WeightClassInformation_v() (Rte_AddrPar_0x2B_P1M7S_WeightClassInformation_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1M7T_FuelTypeInformation_v() (Rte_AddrPar_0x2B_P1M7T_FuelTypeInformation_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1R0P_CabHeightValue_v() (Rte_AddrPar_0x2B_P1R0P_CabHeightValue_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DXX_FMSgateway_Act_v() (Rte_AddrPar_0x2B_P1DXX_FMSgateway_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1LX8_EngineFuelTypeLNG_v() (Rte_AddrPar_0x2B_P1LX8_EngineFuelTypeLNG_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1M7Q_EraGlonassUnitInstalled_v() (Rte_AddrPar_0x2B_P1M7Q_EraGlonassUnitInstalled_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_VIN_VINNO_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_VINNO_VIN_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_VINNO_VIN_v() (&(Rte_AddrPar_0x2F_VINNO_VIN_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_VINNO_VIN_v() (&Rte_AddrPar_0x2F_VINNO_VIN_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

# endif /* !defined(RTE_CORE) */


# define FMSGateway_START_SEC_CODE
# include "FMSGateway_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_FMSGateway_20ms_runnable FMSGateway_20ms_runnable
# endif

FUNC(void, FMSGateway_CODE) FMSGateway_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define FMSGateway_STOP_SEC_CODE
# include "FMSGateway_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_FMSGATEWAY_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
