/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_SpeedControlFreeWheel_LINMasCtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <SpeedControlFreeWheel_LINMasCtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_H
# define _RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_SpeedControlFreeWheel_LINMasCtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_SpeedControlFreeWheel_LINMasCtrl_FCWPushButton_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_SpeedControlFreeWheel_LINMasCtrl_LKSPushButton_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_SpeedControlFreeWheel_LINMasCtrl_SpeedControlModeButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FreeWheel_Status_T, RTE_VAR_NOINIT) Rte_SpeedControlFreeWheel_LINMasCtrl_SpeedControlModeWheelStatus_FreeWheel_Status; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_SpeedControlMode_HMICtrl_ACCOrCCIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_SpeedControlMode_HMICtrl_ASLIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN4_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DiagActiveState_T, RTE_VAR_NOINIT) Rte_DiagnosticComponent_DiagActiveState_isDiagActive; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_CollisionMitigation_HMICtrl_FCW_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_LKS_HMICtrl_LKS_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ACCOrCCIndication_DeviceIndication (3U)
#  define Rte_InitValue_ASLIndication_DeviceIndication (3U)
#  define Rte_InitValue_ComMode_LIN4_ComMode_LIN (7U)
#  define Rte_InitValue_DiagActiveState_isDiagActive (0U)
#  define Rte_InitValue_DiagInfoCCFW_DiagInfo (0U)
#  define Rte_InitValue_FCWPushButton_PushButtonStatus (3U)
#  define Rte_InitValue_FCW_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_LIN_ACCOrCCIndication_DeviceIndication (0U)
#  define Rte_InitValue_LIN_ASLIndication_DeviceIndication (0U)
#  define Rte_InitValue_LIN_FCWPushButton_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_FCW_DeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_LIN_LKSPushButton_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_LKS_DeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_LIN_SpeedControlModeButtonStat_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_SpeedControlModeWheelStat_FreeWheel_Status (15U)
#  define Rte_InitValue_LKSPushButton_PushButtonStatus (3U)
#  define Rte_InitValue_LKS_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_ResponseErrorCCFW_ResponseErrorCCFW (FALSE)
#  define Rte_InitValue_SpeedControlModeButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_SpeedControlModeWheelStatus_FreeWheel_Status (15U)
# endif


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Buffers for inter-runnable variables
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_SpeedControlFreeWheel_LINMasCtrl_Irv_IOCTL_CCFWLinCtrl; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
# endif /* !defined(RTE_CORE) */


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_SpeedControlFreeWheel_LINMasCtrl_ResponseErrorCCFW_ResponseErrorCCFW(P2VAR(ResponseErrorCCFW, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SpeedControlFreeWheel_LINMasCtrl_LIN_ACCOrCCIndication_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SpeedControlFreeWheel_LINMasCtrl_LIN_ASLIndication_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SpeedControlFreeWheel_LINMasCtrl_LIN_FCW_DeviceIndication_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SpeedControlFreeWheel_LINMasCtrl_LIN_LKS_DeviceIndication_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ACCOrCCIndication_DeviceIndication Rte_Read_SpeedControlFreeWheel_LINMasCtrl_ACCOrCCIndication_DeviceIndication
#  define Rte_Read_SpeedControlFreeWheel_LINMasCtrl_ACCOrCCIndication_DeviceIndication(data) (*(data) = Rte_SpeedControlMode_HMICtrl_ACCOrCCIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ASLIndication_DeviceIndication Rte_Read_SpeedControlFreeWheel_LINMasCtrl_ASLIndication_DeviceIndication
#  define Rte_Read_SpeedControlFreeWheel_LINMasCtrl_ASLIndication_DeviceIndication(data) (*(data) = Rte_SpeedControlMode_HMICtrl_ASLIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ComMode_LIN4_ComMode_LIN Rte_Read_SpeedControlFreeWheel_LINMasCtrl_ComMode_LIN4_ComMode_LIN
#  define Rte_Read_SpeedControlFreeWheel_LINMasCtrl_ComMode_LIN4_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN4_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DiagActiveState_isDiagActive Rte_Read_SpeedControlFreeWheel_LINMasCtrl_DiagActiveState_isDiagActive
#  define Rte_Read_SpeedControlFreeWheel_LINMasCtrl_DiagActiveState_isDiagActive(data) (*(data) = Rte_DiagnosticComponent_DiagActiveState_isDiagActive, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DiagInfoCCFW_DiagInfo Rte_Read_SpeedControlFreeWheel_LINMasCtrl_DiagInfoCCFW_DiagInfo
#  define Rte_Read_SpeedControlFreeWheel_LINMasCtrl_DiagInfoCCFW_DiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_DiagInfoCCFW_oCCFWtoCIOM_L4_oLIN03_69083ad8_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FCW_DeviceIndication_DeviceIndication Rte_Read_SpeedControlFreeWheel_LINMasCtrl_FCW_DeviceIndication_DeviceIndication
#  define Rte_Read_SpeedControlFreeWheel_LINMasCtrl_FCW_DeviceIndication_DeviceIndication(data) (*(data) = Rte_CollisionMitigation_HMICtrl_FCW_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_FCWPushButton_PushButtonStatus Rte_Read_SpeedControlFreeWheel_LINMasCtrl_LIN_FCWPushButton_PushButtonStatus
#  define Rte_Read_SpeedControlFreeWheel_LINMasCtrl_LIN_FCWPushButton_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_FCWPushButton_oCCFWtoCIOM_L4_oLIN03_242f5462_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_LKSPushButton_PushButtonStatus Rte_Read_SpeedControlFreeWheel_LINMasCtrl_LIN_LKSPushButton_PushButtonStatus
#  define Rte_Read_SpeedControlFreeWheel_LINMasCtrl_LIN_LKSPushButton_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_LKSPushButton_oCCFWtoCIOM_L4_oLIN03_8e8e108b_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_SpeedControlModeButtonStat_PushButtonStatus Rte_Read_SpeedControlFreeWheel_LINMasCtrl_LIN_SpeedControlModeButtonStat_PushButtonStatus
#  define Rte_Read_SpeedControlFreeWheel_LINMasCtrl_LIN_SpeedControlModeButtonStat_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_SpeedControlModeButtonStat_oCCFWtoCIOM_L4_oLIN03_0f6013d7_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_SpeedControlModeWheelStat_FreeWheel_Status Rte_Read_SpeedControlFreeWheel_LINMasCtrl_LIN_SpeedControlModeWheelStat_FreeWheel_Status
#  define Rte_Read_SpeedControlFreeWheel_LINMasCtrl_LIN_SpeedControlModeWheelStat_FreeWheel_Status(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_SpeedControlModeWheelStat_oCCFWtoCIOM_L4_oLIN03_cc9e59ce_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LKS_DeviceIndication_DeviceIndication Rte_Read_SpeedControlFreeWheel_LINMasCtrl_LKS_DeviceIndication_DeviceIndication
#  define Rte_Read_SpeedControlFreeWheel_LINMasCtrl_LKS_DeviceIndication_DeviceIndication(data) (*(data) = Rte_LKS_HMICtrl_LKS_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ResponseErrorCCFW_ResponseErrorCCFW Rte_Read_SpeedControlFreeWheel_LINMasCtrl_ResponseErrorCCFW_ResponseErrorCCFW


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_FCWPushButton_PushButtonStatus Rte_Write_SpeedControlFreeWheel_LINMasCtrl_FCWPushButton_PushButtonStatus
#  define Rte_Write_SpeedControlFreeWheel_LINMasCtrl_FCWPushButton_PushButtonStatus(data) (Rte_SpeedControlFreeWheel_LINMasCtrl_FCWPushButton_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LIN_ACCOrCCIndication_DeviceIndication Rte_Write_SpeedControlFreeWheel_LINMasCtrl_LIN_ACCOrCCIndication_DeviceIndication
#  define Rte_Write_LIN_ASLIndication_DeviceIndication Rte_Write_SpeedControlFreeWheel_LINMasCtrl_LIN_ASLIndication_DeviceIndication
#  define Rte_Write_LIN_FCW_DeviceIndication_DeviceIndication Rte_Write_SpeedControlFreeWheel_LINMasCtrl_LIN_FCW_DeviceIndication_DeviceIndication
#  define Rte_Write_LIN_LKS_DeviceIndication_DeviceIndication Rte_Write_SpeedControlFreeWheel_LINMasCtrl_LIN_LKS_DeviceIndication_DeviceIndication
#  define Rte_Write_LKSPushButton_PushButtonStatus Rte_Write_SpeedControlFreeWheel_LINMasCtrl_LKSPushButton_PushButtonStatus
#  define Rte_Write_SpeedControlFreeWheel_LINMasCtrl_LKSPushButton_PushButtonStatus(data) (Rte_SpeedControlFreeWheel_LINMasCtrl_LKSPushButton_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_SpeedControlModeButtonStatus_PushButtonStatus Rte_Write_SpeedControlFreeWheel_LINMasCtrl_SpeedControlModeButtonStatus_PushButtonStatus
#  define Rte_Write_SpeedControlFreeWheel_LINMasCtrl_SpeedControlModeButtonStatus_PushButtonStatus(data) (Rte_SpeedControlFreeWheel_LINMasCtrl_SpeedControlModeButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_SpeedControlModeWheelStatus_FreeWheel_Status Rte_Write_SpeedControlFreeWheel_LINMasCtrl_SpeedControlModeWheelStatus_FreeWheel_Status
#  define Rte_Write_SpeedControlFreeWheel_LINMasCtrl_SpeedControlModeWheelStatus_FreeWheel_Status(data) (Rte_SpeedControlFreeWheel_LINMasCtrl_SpeedControlModeWheelStatus_FreeWheel_Status = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_ResetEventStatus(Dem_EventIdType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1BKB_87_CCFWLink_NoResp_ResetEventStatus() (Dem_ResetEventStatus((Dem_EventIdType)203)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1BKB_87_CCFWLink_NoResp_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)203, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN1_16_CCFW_VBT_ResetEventStatus() (Dem_ResetEventStatus((Dem_EventIdType)209)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN1_16_CCFW_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)209, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN1_17_CCFW_VAT_ResetEventStatus() (Dem_ResetEventStatus((Dem_EventIdType)210)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN1_17_CCFW_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)210, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN1_44_CCFW_RAM_ResetEventStatus() (Dem_ResetEventStatus((Dem_EventIdType)211)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN1_44_CCFW_RAM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)211, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN1_45_CCFW_FLASH_ResetEventStatus() (Dem_ResetEventStatus((Dem_EventIdType)212)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN1_45_CCFW_FLASH_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)212, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN1_46_CCFW_EEPROM_ResetEventStatus() (Dem_ResetEventStatus((Dem_EventIdType)213)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN1_46_CCFW_EEPROM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)213, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_ResetEventStatus() (Dem_ResetEventStatus((Dem_EventIdType)214)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)214, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_FreezeCurrentState_Irv_IOCTL_CCFWLinCtrl(data) \
  (Rte_Irv_SpeedControlFreeWheel_LINMasCtrl_Irv_IOCTL_CCFWLinCtrl = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData_Irv_IOCTL_CCFWLinCtrl() \
  Rte_Irv_SpeedControlFreeWheel_LINMasCtrl_Irv_IOCTL_CCFWLinCtrl
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_CCFWLinCtrl(data) \
  (Rte_Irv_SpeedControlFreeWheel_LINMasCtrl_Irv_IOCTL_CCFWLinCtrl = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_CCFWLinCtrl(data) \
  (Rte_Irv_SpeedControlFreeWheel_LINMasCtrl_Irv_IOCTL_CCFWLinCtrl = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_SpeedControlFreeWheel_LINMasCtrl_20ms_runnable_Irv_IOCTL_CCFWLinCtrl() \
  Rte_Irv_SpeedControlFreeWheel_LINMasCtrl_Irv_IOCTL_CCFWLinCtrl
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_SpeedControlFreeWheel_LINMasCtrl_20ms_runnable_Irv_IOCTL_CCFWLinCtrl(data) \
  (Rte_Irv_SpeedControlFreeWheel_LINMasCtrl_Irv_IOCTL_CCFWLinCtrl = (data))
/* PRQA L:L1 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2C_CCFW_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1B2C_CCFW_Installed_v() (Rte_AddrPar_0x2B_P1B2C_CCFW_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define SpeedControlFreeWheel_LINMasCtrl_START_SEC_CODE
# include "SpeedControlFreeWheel_LINMasCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_FreezeCurrentState DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData
#  define RTE_RUNNABLE_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment
#  define RTE_RUNNABLE_SpeedControlFreeWheel_LINMasCtrl_20ms_runnable SpeedControlFreeWheel_LINMasCtrl_20ms_runnable
# endif

FUNC(Std_ReturnType, SpeedControlFreeWheel_LINMasCtrl_CODE) DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, SpeedControlFreeWheel_LINMasCtrl_CODE) DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, SpeedControlFreeWheel_LINMasCtrl_CODE) DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, SpeedControlFreeWheel_LINMasCtrl_CODE) DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, SpeedControlFreeWheel_LINMasCtrl_CODE) DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, SpeedControlFreeWheel_LINMasCtrl_CODE) DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, SpeedControlFreeWheel_LINMasCtrl_CODE) SpeedControlFreeWheel_LINMasCtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define SpeedControlFreeWheel_LINMasCtrl_STOP_SEC_CODE
# include "SpeedControlFreeWheel_LINMasCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_P1EOQ_Data_P1EOQ_E_NOT_OK (1U)

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
