/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Keyfob_Mgr_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <Keyfob_Mgr>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_KEYFOB_MGR_TYPE_H
# define _RTE_KEYFOB_MGR_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef DCM_E_POSITIVERESPONSE
#   define DCM_E_POSITIVERESPONSE (0U)
#  endif

#  ifndef DCM_E_GENERALREJECT
#   define DCM_E_GENERALREJECT (16U)
#  endif

#  ifndef DCM_E_SERVICENOTSUPPORTED
#   define DCM_E_SERVICENOTSUPPORTED (17U)
#  endif

#  ifndef DCM_E_SUBFUNCTIONNOTSUPPORTED
#   define DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
#  endif

#  ifndef DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT
#   define DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
#  endif

#  ifndef DCM_E_RESPONSETOOLONG
#   define DCM_E_RESPONSETOOLONG (20U)
#  endif

#  ifndef DCM_E_BUSYREPEATREQUEST
#   define DCM_E_BUSYREPEATREQUEST (33U)
#  endif

#  ifndef DCM_E_CONDITIONSNOTCORRECT
#   define DCM_E_CONDITIONSNOTCORRECT (34U)
#  endif

#  ifndef DCM_E_REQUESTSEQUENCEERROR
#   define DCM_E_REQUESTSEQUENCEERROR (36U)
#  endif

#  ifndef DCM_E_NORESPONSEFROMSUBNETCOMPONENT
#   define DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
#  endif

#  ifndef DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION
#   define DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
#  endif

#  ifndef DCM_E_REQUESTOUTOFRANGE
#   define DCM_E_REQUESTOUTOFRANGE (49U)
#  endif

#  ifndef DCM_E_SECURITYACCESSDENIED
#   define DCM_E_SECURITYACCESSDENIED (51U)
#  endif

#  ifndef DCM_E_INVALIDKEY
#   define DCM_E_INVALIDKEY (53U)
#  endif

#  ifndef DCM_E_EXCEEDNUMBEROFATTEMPTS
#   define DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
#  endif

#  ifndef DCM_E_REQUIREDTIMEDELAYNOTEXPIRED
#   define DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
#  endif

#  ifndef DCM_E_UPLOADDOWNLOADNOTACCEPTED
#   define DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
#  endif

#  ifndef DCM_E_TRANSFERDATASUSPENDED
#   define DCM_E_TRANSFERDATASUSPENDED (113U)
#  endif

#  ifndef DCM_E_GENERALPROGRAMMINGFAILURE
#   define DCM_E_GENERALPROGRAMMINGFAILURE (114U)
#  endif

#  ifndef DCM_E_WRONGBLOCKSEQUENCECOUNTER
#   define DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
#  endif

#  ifndef DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING
#   define DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
#  endif

#  ifndef DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION
#   define DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
#  endif

#  ifndef DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION
#   define DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
#  endif

#  ifndef DCM_E_RPMTOOHIGH
#   define DCM_E_RPMTOOHIGH (129U)
#  endif

#  ifndef DCM_E_RPMTOOLOW
#   define DCM_E_RPMTOOLOW (130U)
#  endif

#  ifndef DCM_E_ENGINEISRUNNING
#   define DCM_E_ENGINEISRUNNING (131U)
#  endif

#  ifndef DCM_E_ENGINEISNOTRUNNING
#   define DCM_E_ENGINEISNOTRUNNING (132U)
#  endif

#  ifndef DCM_E_ENGINERUNTIMETOOLOW
#   define DCM_E_ENGINERUNTIMETOOLOW (133U)
#  endif

#  ifndef DCM_E_TEMPERATURETOOHIGH
#   define DCM_E_TEMPERATURETOOHIGH (134U)
#  endif

#  ifndef DCM_E_TEMPERATURETOOLOW
#   define DCM_E_TEMPERATURETOOLOW (135U)
#  endif

#  ifndef DCM_E_VEHICLESPEEDTOOHIGH
#   define DCM_E_VEHICLESPEEDTOOHIGH (136U)
#  endif

#  ifndef DCM_E_VEHICLESPEEDTOOLOW
#   define DCM_E_VEHICLESPEEDTOOLOW (137U)
#  endif

#  ifndef DCM_E_THROTTLE_PEDALTOOHIGH
#   define DCM_E_THROTTLE_PEDALTOOHIGH (138U)
#  endif

#  ifndef DCM_E_THROTTLE_PEDALTOOLOW
#   define DCM_E_THROTTLE_PEDALTOOLOW (139U)
#  endif

#  ifndef DCM_E_TRANSMISSIONRANGENOTINNEUTRAL
#   define DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
#  endif

#  ifndef DCM_E_TRANSMISSIONRANGENOTINGEAR
#   define DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
#  endif

#  ifndef DCM_E_BRAKESWITCH_NOTCLOSED
#   define DCM_E_BRAKESWITCH_NOTCLOSED (143U)
#  endif

#  ifndef DCM_E_SHIFTERLEVERNOTINPARK
#   define DCM_E_SHIFTERLEVERNOTINPARK (144U)
#  endif

#  ifndef DCM_E_TORQUECONVERTERCLUTCHLOCKED
#   define DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
#  endif

#  ifndef DCM_E_VOLTAGETOOHIGH
#   define DCM_E_VOLTAGETOOHIGH (146U)
#  endif

#  ifndef DCM_E_VOLTAGETOOLOW
#   define DCM_E_VOLTAGETOOLOW (147U)
#  endif

#  ifndef DCM_E_VMSCNC_0
#   define DCM_E_VMSCNC_0 (240U)
#  endif

#  ifndef DCM_E_VMSCNC_1
#   define DCM_E_VMSCNC_1 (241U)
#  endif

#  ifndef DCM_E_VMSCNC_2
#   define DCM_E_VMSCNC_2 (242U)
#  endif

#  ifndef DCM_E_VMSCNC_3
#   define DCM_E_VMSCNC_3 (243U)
#  endif

#  ifndef DCM_E_VMSCNC_4
#   define DCM_E_VMSCNC_4 (244U)
#  endif

#  ifndef DCM_E_VMSCNC_5
#   define DCM_E_VMSCNC_5 (245U)
#  endif

#  ifndef DCM_E_VMSCNC_6
#   define DCM_E_VMSCNC_6 (246U)
#  endif

#  ifndef DCM_E_VMSCNC_7
#   define DCM_E_VMSCNC_7 (247U)
#  endif

#  ifndef DCM_E_VMSCNC_8
#   define DCM_E_VMSCNC_8 (248U)
#  endif

#  ifndef DCM_E_VMSCNC_9
#   define DCM_E_VMSCNC_9 (249U)
#  endif

#  ifndef DCM_E_VMSCNC_A
#   define DCM_E_VMSCNC_A (250U)
#  endif

#  ifndef DCM_E_VMSCNC_B
#   define DCM_E_VMSCNC_B (251U)
#  endif

#  ifndef DCM_E_VMSCNC_C
#   define DCM_E_VMSCNC_C (252U)
#  endif

#  ifndef DCM_E_VMSCNC_D
#   define DCM_E_VMSCNC_D (253U)
#  endif

#  ifndef DCM_E_VMSCNC_E
#   define DCM_E_VMSCNC_E (254U)
#  endif

#  ifndef DCM_INITIAL
#   define DCM_INITIAL (0U)
#  endif

#  ifndef DCM_PENDING
#   define DCM_PENDING (1U)
#  endif

#  ifndef DCM_CANCEL
#   define DCM_CANCEL (2U)
#  endif

#  ifndef DCM_FORCE_RCRRP_OK
#   define DCM_FORCE_RCRRP_OK (3U)
#  endif

#  ifndef DCM_FORCE_RCRRP_NOT_OK
#   define DCM_FORCE_RCRRP_NOT_OK (64U)
#  endif

#  ifndef Diag_Active_FALSE
#   define Diag_Active_FALSE (0U)
#  endif

#  ifndef Diag_Active_TRUE
#   define Diag_Active_TRUE (1U)
#  endif

#  ifndef DriverAuthDeviceMatching_Idle
#   define DriverAuthDeviceMatching_Idle (0U)
#  endif

#  ifndef DriverAuthDeviceMatching_DeviceToMatchIsPresent
#   define DriverAuthDeviceMatching_DeviceToMatchIsPresent (1U)
#  endif

#  ifndef DriverAuthDeviceMatching_Error
#   define DriverAuthDeviceMatching_Error (2U)
#  endif

#  ifndef DriverAuthDeviceMatching_NotAvailable
#   define DriverAuthDeviceMatching_NotAvailable (3U)
#  endif

#  ifndef KeyfobAuth_rqst_Idle
#   define KeyfobAuth_rqst_Idle (0U)
#  endif

#  ifndef KeyfobAuth_rqst_RequestByPassiveMechanism
#   define KeyfobAuth_rqst_RequestByPassiveMechanism (1U)
#  endif

#  ifndef KeyfobAuth_rqst_RequestByImmobilizerMechanism
#   define KeyfobAuth_rqst_RequestByImmobilizerMechanism (2U)
#  endif

#  ifndef KeyfobAuth_rqst_Spare1
#   define KeyfobAuth_rqst_Spare1 (3U)
#  endif

#  ifndef KeyfobAuth_rqst_Spare2
#   define KeyfobAuth_rqst_Spare2 (4U)
#  endif

#  ifndef KeyfobAuth_rqst_Spare3
#   define KeyfobAuth_rqst_Spare3 (5U)
#  endif

#  ifndef KeyfobAuth_rqst_Error
#   define KeyfobAuth_rqst_Error (6U)
#  endif

#  ifndef KeyfobAuth_rqst_NotAavailable
#   define KeyfobAuth_rqst_NotAavailable (7U)
#  endif

#  ifndef KeyfobAuth_stat_Idle
#   define KeyfobAuth_stat_Idle (0U)
#  endif

#  ifndef KeyfobAuth_stat_NokeyfobAuthenticated
#   define KeyfobAuth_stat_NokeyfobAuthenticated (1U)
#  endif

#  ifndef KeyfobAuth_stat_KeyfobAuthenticated
#   define KeyfobAuth_stat_KeyfobAuthenticated (2U)
#  endif

#  ifndef KeyfobAuth_stat_Spare1
#   define KeyfobAuth_stat_Spare1 (3U)
#  endif

#  ifndef KeyfobAuth_stat_Spare2
#   define KeyfobAuth_stat_Spare2 (4U)
#  endif

#  ifndef KeyfobAuth_stat_Spare3
#   define KeyfobAuth_stat_Spare3 (5U)
#  endif

#  ifndef KeyfobAuth_stat_Error
#   define KeyfobAuth_stat_Error (6U)
#  endif

#  ifndef KeyfobAuth_stat_NotAvailable
#   define KeyfobAuth_stat_NotAvailable (7U)
#  endif

#  ifndef KeyfobInCabLocation_stat_Idle
#   define KeyfobInCabLocation_stat_Idle (0U)
#  endif

#  ifndef KeyfobInCabLocation_stat_NotDetected_Incab
#   define KeyfobInCabLocation_stat_NotDetected_Incab (1U)
#  endif

#  ifndef KeyfobInCabLocation_stat_Detected_Incab
#   define KeyfobInCabLocation_stat_Detected_Incab (2U)
#  endif

#  ifndef KeyfobInCabLocation_stat_Spare1
#   define KeyfobInCabLocation_stat_Spare1 (3U)
#  endif

#  ifndef KeyfobInCabLocation_stat_Spare2
#   define KeyfobInCabLocation_stat_Spare2 (4U)
#  endif

#  ifndef KeyfobInCabLocation_stat_Spare3
#   define KeyfobInCabLocation_stat_Spare3 (5U)
#  endif

#  ifndef KeyfobInCabLocation_stat_Error
#   define KeyfobInCabLocation_stat_Error (6U)
#  endif

#  ifndef KeyfobInCabLocation_stat_NotAvailable
#   define KeyfobInCabLocation_stat_NotAvailable (7U)
#  endif

#  ifndef KeyfobInCabPresencePS_rqst_NoRequest
#   define KeyfobInCabPresencePS_rqst_NoRequest (0U)
#  endif

#  ifndef KeyfobInCabPresencePS_rqst_RequestDetectionInCab
#   define KeyfobInCabPresencePS_rqst_RequestDetectionInCab (1U)
#  endif

#  ifndef KeyfobInCabPresencePS_rqst_Error
#   define KeyfobInCabPresencePS_rqst_Error (2U)
#  endif

#  ifndef KeyfobInCabPresencePS_rqst_NotAvailable
#   define KeyfobInCabPresencePS_rqst_NotAvailable (3U)
#  endif

#  ifndef KeyfobLocation_rqst_NoRequest
#   define KeyfobLocation_rqst_NoRequest (0U)
#  endif

#  ifndef KeyfobLocation_rqst_Request
#   define KeyfobLocation_rqst_Request (1U)
#  endif

#  ifndef KeyfobLocation_rqst_Error
#   define KeyfobLocation_rqst_Error (2U)
#  endif

#  ifndef KeyfobLocation_rqst_NotAvailable
#   define KeyfobLocation_rqst_NotAvailable (3U)
#  endif

#  ifndef KeyfobOutsideLocation_stat_Idle
#   define KeyfobOutsideLocation_stat_Idle (0U)
#  endif

#  ifndef KeyfobOutsideLocation_stat_NotDetectedOutside
#   define KeyfobOutsideLocation_stat_NotDetectedOutside (1U)
#  endif

#  ifndef KeyfobOutsideLocation_stat_DetectedOnRightSide
#   define KeyfobOutsideLocation_stat_DetectedOnRightSide (2U)
#  endif

#  ifndef KeyfobOutsideLocation_stat_DetectedOnLeftSide
#   define KeyfobOutsideLocation_stat_DetectedOnLeftSide (3U)
#  endif

#  ifndef KeyfobOutsideLocation_stat_DetectedOnBothSides
#   define KeyfobOutsideLocation_stat_DetectedOnBothSides (4U)
#  endif

#  ifndef KeyfobOutsideLocation_stat_DetectedOutOfDoorAreas
#   define KeyfobOutsideLocation_stat_DetectedOutOfDoorAreas (5U)
#  endif

#  ifndef KeyfobOutsideLocation_stat_Error
#   define KeyfobOutsideLocation_stat_Error (6U)
#  endif

#  ifndef KeyfobOutsideLocation_stat_NotAvailable
#   define KeyfobOutsideLocation_stat_NotAvailable (7U)
#  endif

#  ifndef ImmoDriver_ProcessingStatus_Idle
#   define ImmoDriver_ProcessingStatus_Idle (0U)
#  endif

#  ifndef ImmoDriver_ProcessingStatus_Ongoing
#   define ImmoDriver_ProcessingStatus_Ongoing (1U)
#  endif

#  ifndef ImmoDriver_ProcessingStatus_LfSent
#   define ImmoDriver_ProcessingStatus_LfSent (2U)
#  endif

#  ifndef ImmoDriver_ProcessingStatus_DecryptedInList
#   define ImmoDriver_ProcessingStatus_DecryptedInList (3U)
#  endif

#  ifndef ImmoDriver_ProcessingStatus_DecryptedNotInList
#   define ImmoDriver_ProcessingStatus_DecryptedNotInList (4U)
#  endif

#  ifndef ImmoDriver_ProcessingStatus_NotDecrypted
#   define ImmoDriver_ProcessingStatus_NotDecrypted (5U)
#  endif

#  ifndef ImmoDriver_ProcessingStatus_HwError
#   define ImmoDriver_ProcessingStatus_HwError (6U)
#  endif

#  ifndef ImmoType_ATA5702
#   define ImmoType_ATA5702 (0U)
#  endif

#  ifndef ImmoType_ATA5577
#   define ImmoType_ATA5577 (1U)
#  endif

#  ifndef PassiveDriver_ProcessingStatus_Idle
#   define PassiveDriver_ProcessingStatus_Idle (0U)
#  endif

#  ifndef PassiveDriver_ProcessingStatus_Ongoing
#   define PassiveDriver_ProcessingStatus_Ongoing (1U)
#  endif

#  ifndef PassiveDriver_ProcessingStatus_LfSent
#   define PassiveDriver_ProcessingStatus_LfSent (2U)
#  endif

#  ifndef PassiveDriver_ProcessingStatus_Detected
#   define PassiveDriver_ProcessingStatus_Detected (3U)
#  endif

#  ifndef PassiveDriver_ProcessingStatus_NotDetected
#   define PassiveDriver_ProcessingStatus_NotDetected (4U)
#  endif

#  ifndef PassiveDriver_ProcessingStatus_HwError
#   define PassiveDriver_ProcessingStatus_HwError (5U)
#  endif

#  ifndef PassiveSearchCoverage_PassiveStart2Ant
#   define PassiveSearchCoverage_PassiveStart2Ant (0U)
#  endif

#  ifndef PassiveSearchCoverage_PassiveStart1Ant
#   define PassiveSearchCoverage_PassiveStart1Ant (1U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_KEYFOB_MGR_TYPE_H */
