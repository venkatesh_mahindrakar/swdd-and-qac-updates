/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Cdd_LinDiagnostics_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <Cdd_LinDiagnostics>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_CDD_LINDIAGNOSTICS_TYPE_H
# define _RTE_CDD_LINDIAGNOSTICS_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef CDD_LIN_NOT_OK
#   define CDD_LIN_NOT_OK (11U)
#  endif

#  ifndef CDD_LIN_TX_OK
#   define CDD_LIN_TX_OK (0U)
#  endif

#  ifndef CDD_LIN_TX_BUSY
#   define CDD_LIN_TX_BUSY (1U)
#  endif

#  ifndef CDD_LIN_TX_HEADER_ERROR
#   define CDD_LIN_TX_HEADER_ERROR (2U)
#  endif

#  ifndef CDD_LIN_TX_ERROR
#   define CDD_LIN_TX_ERROR (3U)
#  endif

#  ifndef CDD_LIN_RX_OK
#   define CDD_LIN_RX_OK (4U)
#  endif

#  ifndef CDD_LIN_RX_BUSY
#   define CDD_LIN_RX_BUSY (5U)
#  endif

#  ifndef CDD_LIN_RX_ERROR
#   define CDD_LIN_RX_ERROR (6U)
#  endif

#  ifndef CDD_LIN_RX_NO_RESPONSE
#   define CDD_LIN_RX_NO_RESPONSE (7U)
#  endif

#  ifndef CDD_LIN_NONE
#   define CDD_LIN_NONE (9U)
#  endif

#  ifndef Inactive
#   define Inactive (0U)
#  endif

#  ifndef Diagnostic
#   define Diagnostic (1U)
#  endif

#  ifndef SwitchDetection
#   define SwitchDetection (2U)
#  endif

#  ifndef ApplicationMonitoring
#   define ApplicationMonitoring (3U)
#  endif

#  ifndef Calibration
#   define Calibration (4U)
#  endif

#  ifndef Spare1
#   define Spare1 (5U)
#  endif

#  ifndef Error
#   define Error (6U)
#  endif

#  ifndef NotAvailable
#   define NotAvailable (7U)
#  endif

#  ifndef Diag_Active_FALSE
#   define Diag_Active_FALSE (0U)
#  endif

#  ifndef Diag_Active_TRUE
#   define Diag_Active_TRUE (1U)
#  endif

#  ifndef None
#   define None (0U)
#  endif

#  ifndef LinDiag_BUS1
#   define LinDiag_BUS1 (1U)
#  endif

#  ifndef LinDiag_BUS2
#   define LinDiag_BUS2 (2U)
#  endif

#  ifndef LinDiag_BUS3
#   define LinDiag_BUS3 (3U)
#  endif

#  ifndef LinDiag_BUS4
#   define LinDiag_BUS4 (4U)
#  endif

#  ifndef LinDiag_BUS5
#   define LinDiag_BUS5 (5U)
#  endif

#  ifndef LinDiag_SpareBUS1
#   define LinDiag_SpareBUS1 (6U)
#  endif

#  ifndef LinDiag_SpareBUS2
#   define LinDiag_SpareBUS2 (7U)
#  endif

#  ifndef LinDiag_ALL_BUSSES
#   define LinDiag_ALL_BUSSES (8U)
#  endif

#  ifndef NO_OPEARATION
#   define NO_OPEARATION (0U)
#  endif

#  ifndef START_OPEARATION
#   define START_OPEARATION (1U)
#  endif

#  ifndef STOP_OPERATION
#   define STOP_OPERATION (2U)
#  endif

#  ifndef LinDiagService_None
#   define LinDiagService_None (0U)
#  endif

#  ifndef LinDiagService_Pending
#   define LinDiagService_Pending (1U)
#  endif

#  ifndef LinDiagService_Completed
#   define LinDiagService_Completed (2U)
#  endif

#  ifndef LinDiagService_Error
#   define LinDiagService_Error (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_CDD_LINDIAGNOSTICS_TYPE_H */
