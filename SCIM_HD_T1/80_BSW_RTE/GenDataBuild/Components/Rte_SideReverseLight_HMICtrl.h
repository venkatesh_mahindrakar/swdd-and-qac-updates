/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_SideReverseLight_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <SideReverseLight_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_SIDEREVERSELIGHT_HMICTRL_H
# define _RTE_SIDEREVERSELIGHT_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# ifndef RTE_CORE
#  define RTE_MULTI_INST_API
# endif

/* include files */

# include "Rte_SideReverseLight_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE
typedef uint8 Rte_Instance; /* PRQA S 1508 */ /* MD_Rte_1508 */
# endif


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_BBNetwExtraSWrknLight_stat_BBNetwExtraSWrknLight_stat (7U)
#  define Rte_InitValue_CabExtraSideWorkingLight_rqst_CabExtraSideWorkingLight_rqst (7U)
#  define Rte_InitValue_CabExtraSideWrknLightFdbk_stat_CabExtraSideWrknLightFdbk_stat (3U)
#  define Rte_InitValue_SideReverseLightInd_cmd_DualDeviceIndication (15U)
#  define Rte_InitValue_SideReverseLight_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_SideReverseLight_SwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_SideWheelLightInd_cmd_DeviceIndication (0U)
#  define Rte_InitValue_WRCSideReverseWorkingRequest_WRCSideReverseWorkingRequest (3U)
# endif

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_SIDEREVERSELIGHT_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_1508:  MISRA rule: 5.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk.
     Prevention: Not required.

*/
