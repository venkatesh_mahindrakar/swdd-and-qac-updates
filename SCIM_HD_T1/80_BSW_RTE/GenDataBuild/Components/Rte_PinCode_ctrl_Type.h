/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_PinCode_ctrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <PinCode_ctrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_PINCODE_CTRL_TYPE_H
# define _RTE_PINCODE_CTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef DynamicCode_rqst_Idle
#   define DynamicCode_rqst_Idle (0U)
#  endif

#  ifndef DynamicCode_rqst_DynamicCodeRequest
#   define DynamicCode_rqst_DynamicCodeRequest (1U)
#  endif

#  ifndef DynamicCode_rqst_Error
#   define DynamicCode_rqst_Error (6U)
#  endif

#  ifndef DynamicCode_rqst_NotAvailable
#   define DynamicCode_rqst_NotAvailable (7U)
#  endif

#  ifndef PinCode_rqst_Idle
#   define PinCode_rqst_Idle (0U)
#  endif

#  ifndef PinCode_rqst_PINCodeNotNeeded
#   define PinCode_rqst_PINCodeNotNeeded (1U)
#  endif

#  ifndef PinCode_rqst_StatusOfThePinCodeRequested
#   define PinCode_rqst_StatusOfThePinCodeRequested (2U)
#  endif

#  ifndef PinCode_rqst_PinCodeNeeded
#   define PinCode_rqst_PinCodeNeeded (3U)
#  endif

#  ifndef PinCode_rqst_ResetPinCodeStatus
#   define PinCode_rqst_ResetPinCodeStatus (4U)
#  endif

#  ifndef PinCode_rqst_Spare
#   define PinCode_rqst_Spare (5U)
#  endif

#  ifndef PinCode_rqst_Error
#   define PinCode_rqst_Error (6U)
#  endif

#  ifndef PinCode_rqst_NotAvailable
#   define PinCode_rqst_NotAvailable (7U)
#  endif

#  ifndef PinCode_stat_Idle
#   define PinCode_stat_Idle (0U)
#  endif

#  ifndef PinCode_stat_NoPinCodeEntered
#   define PinCode_stat_NoPinCodeEntered (1U)
#  endif

#  ifndef PinCode_stat_WrongPinCode
#   define PinCode_stat_WrongPinCode (2U)
#  endif

#  ifndef PinCode_stat_GoodPinCode
#   define PinCode_stat_GoodPinCode (3U)
#  endif

#  ifndef PinCode_stat_Error
#   define PinCode_stat_Error (6U)
#  endif

#  ifndef PinCode_stat_NotAvailable
#   define PinCode_stat_NotAvailable (7U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_PINCODE_CTRL_TYPE_H */
