/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_SCIM_PVTPT_FlexData_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <SCIM_PVTPT_FlexData>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_SCIM_PVTPT_FLEXDATA_TYPE_H
# define _RTE_SCIM_PVTPT_FLEXDATA_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef Cx0_Ctr_CyclicReport
#   define Cx0_Ctr_CyclicReport (0U)
#  endif

#  ifndef Cx1_Universal_debug_trace
#   define Cx1_Universal_debug_trace (1U)
#  endif

#  ifndef Cx2_Keyfob_RF_data1
#   define Cx2_Keyfob_RF_data1 (2U)
#  endif

#  ifndef Cx3_Keyfob_RF_data2
#   define Cx3_Keyfob_RF_data2 (3U)
#  endif

#  ifndef Cx4_Keyfob_LF_data1
#   define Cx4_Keyfob_LF_data1 (4U)
#  endif

#  ifndef Cx5_Keyfob_LF_data2
#   define Cx5_Keyfob_LF_data2 (5U)
#  endif

#  ifndef Cx6_SW_execution_statistics
#   define Cx6_SW_execution_statistics (6U)
#  endif

#  ifndef Cx7_SW_state_statistics
#   define Cx7_SW_state_statistics (7U)
#  endif

#  ifndef Cx0_Idle
#   define Cx0_Idle (0U)
#  endif

#  ifndef Cx1_P1
#   define Cx1_P1 (1U)
#  endif

#  ifndef Cx2_P2
#   define Cx2_P2 (2U)
#  endif

#  ifndef Cx3_P3
#   define Cx3_P3 (3U)
#  endif

#  ifndef Cx0_Predefined_Cyclic_Report
#   define Cx0_Predefined_Cyclic_Report (0U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_SCIM_PVTPT_FLEXDATA_TYPE_H */
