/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_VehicleAccess_Ctrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <VehicleAccess_Ctrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_VEHICLEACCESS_CTRL_TYPE_H
# define _RTE_VEHICLEACCESS_CTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef AutoRelock_rqst_Idle
#   define AutoRelock_rqst_Idle (0U)
#  endif

#  ifndef AutoRelock_rqst_AutoRelockActivated
#   define AutoRelock_rqst_AutoRelockActivated (1U)
#  endif

#  ifndef AutoRelock_rqst_Error
#   define AutoRelock_rqst_Error (2U)
#  endif

#  ifndef AutoRelock_rqst_NotAvailable
#   define AutoRelock_rqst_NotAvailable (3U)
#  endif

#  ifndef AutorelockingMovements_stat_Idle
#   define AutorelockingMovements_stat_Idle (0U)
#  endif

#  ifndef AutorelockingMovements_stat_NoMovementHasBeenDetected
#   define AutorelockingMovements_stat_NoMovementHasBeenDetected (1U)
#  endif

#  ifndef AutorelockingMovements_stat_MovementHasBeenDetected
#   define AutorelockingMovements_stat_MovementHasBeenDetected (2U)
#  endif

#  ifndef AutorelockingMovements_stat_Spare
#   define AutorelockingMovements_stat_Spare (3U)
#  endif

#  ifndef AutorelockingMovements_stat_Spare_01
#   define AutorelockingMovements_stat_Spare_01 (4U)
#  endif

#  ifndef AutorelockingMovements_stat_Spare_02
#   define AutorelockingMovements_stat_Spare_02 (5U)
#  endif

#  ifndef AutorelockingMovements_stat_Error
#   define AutorelockingMovements_stat_Error (6U)
#  endif

#  ifndef AutorelockingMovements_stat_NotAvailable
#   define AutorelockingMovements_stat_NotAvailable (7U)
#  endif

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef DoorAjar_stat_Idle
#   define DoorAjar_stat_Idle (0U)
#  endif

#  ifndef DoorAjar_stat_DoorClosed
#   define DoorAjar_stat_DoorClosed (1U)
#  endif

#  ifndef DoorAjar_stat_DoorOpen
#   define DoorAjar_stat_DoorOpen (2U)
#  endif

#  ifndef DoorAjar_stat_Error
#   define DoorAjar_stat_Error (6U)
#  endif

#  ifndef DoorAjar_stat_NotAvailable
#   define DoorAjar_stat_NotAvailable (7U)
#  endif

#  ifndef DoorLatch_rqst_decrypt_Idle
#   define DoorLatch_rqst_decrypt_Idle (0U)
#  endif

#  ifndef DoorLatch_rqst_decrypt_LockDoorCommand
#   define DoorLatch_rqst_decrypt_LockDoorCommand (1U)
#  endif

#  ifndef DoorLatch_rqst_decrypt_UnlockDoorCommand
#   define DoorLatch_rqst_decrypt_UnlockDoorCommand (2U)
#  endif

#  ifndef DoorLatch_rqst_decrypt_EmergencyUnlockRequest
#   define DoorLatch_rqst_decrypt_EmergencyUnlockRequest (3U)
#  endif

#  ifndef DoorLatch_rqst_decrypt_Spare1
#   define DoorLatch_rqst_decrypt_Spare1 (4U)
#  endif

#  ifndef DoorLatch_rqst_decrypt_Spare2
#   define DoorLatch_rqst_decrypt_Spare2 (5U)
#  endif

#  ifndef DoorLatch_rqst_decrypt_Error
#   define DoorLatch_rqst_decrypt_Error (6U)
#  endif

#  ifndef DoorLatch_rqst_decrypt_NotAvailable
#   define DoorLatch_rqst_decrypt_NotAvailable (7U)
#  endif

#  ifndef DoorLatch_stat_Idle
#   define DoorLatch_stat_Idle (0U)
#  endif

#  ifndef DoorLatch_stat_LatchUnlockedFiltered
#   define DoorLatch_stat_LatchUnlockedFiltered (1U)
#  endif

#  ifndef DoorLatch_stat_LatchLockedFiltered
#   define DoorLatch_stat_LatchLockedFiltered (2U)
#  endif

#  ifndef DoorLatch_stat_Error
#   define DoorLatch_stat_Error (6U)
#  endif

#  ifndef DoorLatch_stat_NotAvailable
#   define DoorLatch_stat_NotAvailable (7U)
#  endif

#  ifndef DoorLockUnlock_Idle
#   define DoorLockUnlock_Idle (0U)
#  endif

#  ifndef DoorLockUnlock_Unlock
#   define DoorLockUnlock_Unlock (1U)
#  endif

#  ifndef DoorLockUnlock_Lock
#   define DoorLockUnlock_Lock (2U)
#  endif

#  ifndef DoorLockUnlock_MonoLockUnlock
#   define DoorLockUnlock_MonoLockUnlock (3U)
#  endif

#  ifndef DoorLockUnlock_Spare1
#   define DoorLockUnlock_Spare1 (4U)
#  endif

#  ifndef DoorLockUnlock_Spare2
#   define DoorLockUnlock_Spare2 (5U)
#  endif

#  ifndef DoorLockUnlock_Error
#   define DoorLockUnlock_Error (6U)
#  endif

#  ifndef DoorLockUnlock_NotAvailable
#   define DoorLockUnlock_NotAvailable (7U)
#  endif

#  ifndef DoorLock_stat_Idle
#   define DoorLock_stat_Idle (0U)
#  endif

#  ifndef DoorLock_stat_BothDoorsAreUnlocked
#   define DoorLock_stat_BothDoorsAreUnlocked (1U)
#  endif

#  ifndef DoorLock_stat_DriverDoorIsUnlocked
#   define DoorLock_stat_DriverDoorIsUnlocked (2U)
#  endif

#  ifndef DoorLock_stat_PassengerDoorIsUnlocked
#   define DoorLock_stat_PassengerDoorIsUnlocked (3U)
#  endif

#  ifndef DoorLock_stat_BothDoorsAreLocked
#   define DoorLock_stat_BothDoorsAreLocked (4U)
#  endif

#  ifndef DoorLock_stat_Error
#   define DoorLock_stat_Error (14U)
#  endif

#  ifndef DoorLock_stat_NotAvailable
#   define DoorLock_stat_NotAvailable (15U)
#  endif

#  ifndef DoorsAjar_stat_Idle
#   define DoorsAjar_stat_Idle (0U)
#  endif

#  ifndef DoorsAjar_stat_BothDoorsAreClosed
#   define DoorsAjar_stat_BothDoorsAreClosed (1U)
#  endif

#  ifndef DoorsAjar_stat_DriverDoorIsOpen
#   define DoorsAjar_stat_DriverDoorIsOpen (2U)
#  endif

#  ifndef DoorsAjar_stat_PassengerDoorIsOpen
#   define DoorsAjar_stat_PassengerDoorIsOpen (3U)
#  endif

#  ifndef DoorsAjar_stat_BothDoorsAreOpen
#   define DoorsAjar_stat_BothDoorsAreOpen (4U)
#  endif

#  ifndef DoorsAjar_stat_Spare
#   define DoorsAjar_stat_Spare (5U)
#  endif

#  ifndef DoorsAjar_stat_Error
#   define DoorsAjar_stat_Error (6U)
#  endif

#  ifndef DoorsAjar_stat_NotAvailable
#   define DoorsAjar_stat_NotAvailable (7U)
#  endif

#  ifndef EmergencyDoorsUnlock_rqst_Idle
#   define EmergencyDoorsUnlock_rqst_Idle (0U)
#  endif

#  ifndef EmergencyDoorsUnlock_rqst_Spare
#   define EmergencyDoorsUnlock_rqst_Spare (1U)
#  endif

#  ifndef EmergencyDoorsUnlock_rqst_EmergencyUnlockRequest
#   define EmergencyDoorsUnlock_rqst_EmergencyUnlockRequest (2U)
#  endif

#  ifndef EmergencyDoorsUnlock_rqst_Error
#   define EmergencyDoorsUnlock_rqst_Error (6U)
#  endif

#  ifndef EmergencyDoorsUnlock_rqst_NotAvailable
#   define EmergencyDoorsUnlock_rqst_NotAvailable (7U)
#  endif

#  ifndef FrontLidLatch_cmd_Idle
#   define FrontLidLatch_cmd_Idle (0U)
#  endif

#  ifndef FrontLidLatch_cmd_LockFrontLidCommand
#   define FrontLidLatch_cmd_LockFrontLidCommand (1U)
#  endif

#  ifndef FrontLidLatch_cmd_UnlockFrontLidCommand
#   define FrontLidLatch_cmd_UnlockFrontLidCommand (2U)
#  endif

#  ifndef FrontLidLatch_cmd_Error
#   define FrontLidLatch_cmd_Error (6U)
#  endif

#  ifndef FrontLidLatch_cmd_NotAvailable
#   define FrontLidLatch_cmd_NotAvailable (7U)
#  endif

#  ifndef FrontLidLatch_stat_Idle
#   define FrontLidLatch_stat_Idle (0U)
#  endif

#  ifndef FrontLidLatch_stat_LatchUnlocked
#   define FrontLidLatch_stat_LatchUnlocked (1U)
#  endif

#  ifndef FrontLidLatch_stat_LatchLocked
#   define FrontLidLatch_stat_LatchLocked (2U)
#  endif

#  ifndef FrontLidLatch_stat_Error
#   define FrontLidLatch_stat_Error (6U)
#  endif

#  ifndef FrontLidLatch_stat_NotAvailable
#   define FrontLidLatch_stat_NotAvailable (7U)
#  endif

#  ifndef ISSM_STATE_INACTIVE
#   define ISSM_STATE_INACTIVE (0U)
#  endif

#  ifndef ISSM_STATE_PENDING
#   define ISSM_STATE_PENDING (1U)
#  endif

#  ifndef ISSM_STATE_ACTIVE
#   define ISSM_STATE_ACTIVE (2U)
#  endif

#  ifndef KeyfobInCabLocation_stat_Idle
#   define KeyfobInCabLocation_stat_Idle (0U)
#  endif

#  ifndef KeyfobInCabLocation_stat_NotDetected_Incab
#   define KeyfobInCabLocation_stat_NotDetected_Incab (1U)
#  endif

#  ifndef KeyfobInCabLocation_stat_Detected_Incab
#   define KeyfobInCabLocation_stat_Detected_Incab (2U)
#  endif

#  ifndef KeyfobInCabLocation_stat_Spare1
#   define KeyfobInCabLocation_stat_Spare1 (3U)
#  endif

#  ifndef KeyfobInCabLocation_stat_Spare2
#   define KeyfobInCabLocation_stat_Spare2 (4U)
#  endif

#  ifndef KeyfobInCabLocation_stat_Spare3
#   define KeyfobInCabLocation_stat_Spare3 (5U)
#  endif

#  ifndef KeyfobInCabLocation_stat_Error
#   define KeyfobInCabLocation_stat_Error (6U)
#  endif

#  ifndef KeyfobInCabLocation_stat_NotAvailable
#   define KeyfobInCabLocation_stat_NotAvailable (7U)
#  endif

#  ifndef KeyfobLocation_rqst_NoRequest
#   define KeyfobLocation_rqst_NoRequest (0U)
#  endif

#  ifndef KeyfobLocation_rqst_Request
#   define KeyfobLocation_rqst_Request (1U)
#  endif

#  ifndef KeyfobLocation_rqst_Error
#   define KeyfobLocation_rqst_Error (2U)
#  endif

#  ifndef KeyfobLocation_rqst_NotAvailable
#   define KeyfobLocation_rqst_NotAvailable (3U)
#  endif

#  ifndef KeyfobOutsideLocation_stat_Idle
#   define KeyfobOutsideLocation_stat_Idle (0U)
#  endif

#  ifndef KeyfobOutsideLocation_stat_NotDetectedOutside
#   define KeyfobOutsideLocation_stat_NotDetectedOutside (1U)
#  endif

#  ifndef KeyfobOutsideLocation_stat_DetectedOnRightSide
#   define KeyfobOutsideLocation_stat_DetectedOnRightSide (2U)
#  endif

#  ifndef KeyfobOutsideLocation_stat_DetectedOnLeftSide
#   define KeyfobOutsideLocation_stat_DetectedOnLeftSide (3U)
#  endif

#  ifndef KeyfobOutsideLocation_stat_DetectedOnBothSides
#   define KeyfobOutsideLocation_stat_DetectedOnBothSides (4U)
#  endif

#  ifndef KeyfobOutsideLocation_stat_DetectedOutOfDoorAreas
#   define KeyfobOutsideLocation_stat_DetectedOutOfDoorAreas (5U)
#  endif

#  ifndef KeyfobOutsideLocation_stat_Error
#   define KeyfobOutsideLocation_stat_Error (6U)
#  endif

#  ifndef KeyfobOutsideLocation_stat_NotAvailable
#   define KeyfobOutsideLocation_stat_NotAvailable (7U)
#  endif

#  ifndef LockingIndication_rqst_Idle
#   define LockingIndication_rqst_Idle (0U)
#  endif

#  ifndef LockingIndication_rqst_Lock
#   define LockingIndication_rqst_Lock (1U)
#  endif

#  ifndef LockingIndication_rqst_Unlock
#   define LockingIndication_rqst_Unlock (2U)
#  endif

#  ifndef LockingIndication_rqst_Spare
#   define LockingIndication_rqst_Spare (3U)
#  endif

#  ifndef LockingIndication_rqst_Spare01
#   define LockingIndication_rqst_Spare01 (4U)
#  endif

#  ifndef LockingIndication_rqst_Spare02
#   define LockingIndication_rqst_Spare02 (5U)
#  endif

#  ifndef LockingIndication_rqst_Error
#   define LockingIndication_rqst_Error (6U)
#  endif

#  ifndef LockingIndication_rqst_NotAvailable
#   define LockingIndication_rqst_NotAvailable (7U)
#  endif

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

#  ifndef SpeedLockingInhibition_stat_Idle
#   define SpeedLockingInhibition_stat_Idle (0U)
#  endif

#  ifndef SpeedLockingInhibition_stat_SpeedLockingActivate
#   define SpeedLockingInhibition_stat_SpeedLockingActivate (1U)
#  endif

#  ifndef SpeedLockingInhibition_stat_SpeedLockingDeactivate
#   define SpeedLockingInhibition_stat_SpeedLockingDeactivate (2U)
#  endif

#  ifndef SpeedLockingInhibition_stat_Error
#   define SpeedLockingInhibition_stat_Error (6U)
#  endif

#  ifndef SpeedLockingInhibition_stat_NotAvailable
#   define SpeedLockingInhibition_stat_NotAvailable (7U)
#  endif

#  ifndef Synch_Unsynch_Mode_stat_Idle
#   define Synch_Unsynch_Mode_stat_Idle (0U)
#  endif

#  ifndef Synch_Unsynch_Mode_stat_SynchronizedMode
#   define Synch_Unsynch_Mode_stat_SynchronizedMode (1U)
#  endif

#  ifndef Synch_Unsynch_Mode_stat_UnsynchronizedMode
#   define Synch_Unsynch_Mode_stat_UnsynchronizedMode (2U)
#  endif

#  ifndef Synch_Unsynch_Mode_stat_Spare
#   define Synch_Unsynch_Mode_stat_Spare (3U)
#  endif

#  ifndef Synch_Unsynch_Mode_stat_Spare_01
#   define Synch_Unsynch_Mode_stat_Spare_01 (4U)
#  endif

#  ifndef Synch_Unsynch_Mode_stat_Spare_02
#   define Synch_Unsynch_Mode_stat_Spare_02 (5U)
#  endif

#  ifndef Synch_Unsynch_Mode_stat_Error
#   define Synch_Unsynch_Mode_stat_Error (6U)
#  endif

#  ifndef Synch_Unsynch_Mode_stat_NotAvailable
#   define Synch_Unsynch_Mode_stat_NotAvailable (7U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

#  ifndef VehicleMode_Hibernate
#   define VehicleMode_Hibernate (0U)
#  endif

#  ifndef VehicleMode_Parked
#   define VehicleMode_Parked (1U)
#  endif

#  ifndef VehicleMode_Living
#   define VehicleMode_Living (2U)
#  endif

#  ifndef VehicleMode_Accessory
#   define VehicleMode_Accessory (3U)
#  endif

#  ifndef VehicleMode_PreRunning
#   define VehicleMode_PreRunning (4U)
#  endif

#  ifndef VehicleMode_Cranking
#   define VehicleMode_Cranking (5U)
#  endif

#  ifndef VehicleMode_Running
#   define VehicleMode_Running (6U)
#  endif

#  ifndef VehicleMode_Spare_1
#   define VehicleMode_Spare_1 (7U)
#  endif

#  ifndef VehicleMode_Spare_2
#   define VehicleMode_Spare_2 (8U)
#  endif

#  ifndef VehicleMode_Spare_3
#   define VehicleMode_Spare_3 (9U)
#  endif

#  ifndef VehicleMode_Spare_4
#   define VehicleMode_Spare_4 (10U)
#  endif

#  ifndef VehicleMode_Spare_5
#   define VehicleMode_Spare_5 (11U)
#  endif

#  ifndef VehicleMode_Spare_6
#   define VehicleMode_Spare_6 (12U)
#  endif

#  ifndef VehicleMode_Spare_7
#   define VehicleMode_Spare_7 (13U)
#  endif

#  ifndef VehicleMode_Error
#   define VehicleMode_Error (14U)
#  endif

#  ifndef VehicleMode_NotAvailable
#   define VehicleMode_NotAvailable (15U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_VEHICLEACCESS_CTRL_TYPE_H */
