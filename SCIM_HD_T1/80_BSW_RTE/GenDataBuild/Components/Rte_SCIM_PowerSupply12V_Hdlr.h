/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_SCIM_PowerSupply12V_Hdlr.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <SCIM_PowerSupply12V_Hdlr>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_SCIM_POWERSUPPLY12V_HDLR_H
# define _RTE_SCIM_POWERSUPPLY12V_HDLR_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_SCIM_PowerSupply12V_Hdlr_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Living12VPowerStability, RTE_VAR_NOINIT) Rte_SCIM_PowerSupply12V_Hdlr_Living12VPowerStability_Living12VPowerStability; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_Living12VResetRequest_Living12VResetRequest; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleMode_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_Living12VPowerStability_Living12VPowerStability (0U)
#  define Rte_InitValue_Living12VResetRequest_Living12VResetRequest (FALSE)
#  define Rte_InitValue_VehicleModeInternal_VehicleMode (15U)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_Living12VResetRequest_Living12VResetRequest Rte_Read_SCIM_PowerSupply12V_Hdlr_Living12VResetRequest_Living12VResetRequest
#  define Rte_Read_SCIM_PowerSupply12V_Hdlr_Living12VResetRequest_Living12VResetRequest(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_Living12VResetRequest_Living12VResetRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_VehicleModeInternal_VehicleMode Rte_Read_SCIM_PowerSupply12V_Hdlr_VehicleModeInternal_VehicleMode
#  define Rte_Read_SCIM_PowerSupply12V_Hdlr_VehicleModeInternal_VehicleMode(data) (*(data) = Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_Living12VPowerStability_Living12VPowerStability Rte_Write_SCIM_PowerSupply12V_Hdlr_Living12VPowerStability_Living12VPowerStability
#  define Rte_Write_SCIM_PowerSupply12V_Hdlr_Living12VPowerStability_Living12VPowerStability(data) (Rte_SCIM_PowerSupply12V_Hdlr_Living12VPowerStability_Living12VPowerStability = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) Do12VInterface_P_GetDcdc12VState_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DcDc12vRefVoltage, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDcDc12vActivated, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Do12VInterface_P_GetDcdc12VState_CS Do12VInterface_P_GetDcdc12VState_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDo12VActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Do12VPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS Do12VInterface_P_GetDo12VPinsState_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS Do12VInterface_P_SetDcdc12VActive_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS Do12VInterface_P_SetDo12VLivingActive_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS Do12VInterface_P_SetDo12VParkedActive_CS
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_GetAllActiveIss(P2VAR(uint32, AUTOMATIC, RTE_ISSM_APPL_VAR) activeIssField); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Issm_GetAllActiveIss_GetAllActiveIss Issm_GetAllActiveIss


# endif /* !defined(RTE_CORE) */


# define SCIM_PowerSupply12V_Hdlr_START_SEC_CODE
# include "SCIM_PowerSupply12V_Hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_SCIM_PowerSupply12V_Hdlr_10ms_runnable SCIM_PowerSupply12V_Hdlr_10ms_runnable
# endif

FUNC(void, SCIM_PowerSupply12V_Hdlr_CODE) SCIM_PowerSupply12V_Hdlr_10ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define SCIM_PowerSupply12V_Hdlr_STOP_SEC_CODE
# include "SCIM_PowerSupply12V_Hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_Do12VInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_Issm_GetAllActiveIss_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_SCIM_POWERSUPPLY12V_HDLR_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
