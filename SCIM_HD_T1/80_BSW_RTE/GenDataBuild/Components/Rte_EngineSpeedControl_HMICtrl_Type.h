/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_EngineSpeedControl_HMICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <EngineSpeedControl_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_ENGINESPEEDCONTROL_HMICTRL_TYPE_H
# define _RTE_ENGINESPEEDCONTROL_HMICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef A3PosSwitchStatus_Middle
#   define A3PosSwitchStatus_Middle (0U)
#  endif

#  ifndef A3PosSwitchStatus_Lower
#   define A3PosSwitchStatus_Lower (1U)
#  endif

#  ifndef A3PosSwitchStatus_Upper
#   define A3PosSwitchStatus_Upper (2U)
#  endif

#  ifndef A3PosSwitchStatus_Spare
#   define A3PosSwitchStatus_Spare (3U)
#  endif

#  ifndef A3PosSwitchStatus_Spare_01
#   define A3PosSwitchStatus_Spare_01 (4U)
#  endif

#  ifndef A3PosSwitchStatus_Spare_02
#   define A3PosSwitchStatus_Spare_02 (5U)
#  endif

#  ifndef A3PosSwitchStatus_Error
#   define A3PosSwitchStatus_Error (6U)
#  endif

#  ifndef A3PosSwitchStatus_NotAvailable
#   define A3PosSwitchStatus_NotAvailable (7U)
#  endif

#  ifndef AcceleratorPedalStatus_SecPedNotActPrimPedAct
#   define AcceleratorPedalStatus_SecPedNotActPrimPedAct (0U)
#  endif

#  ifndef AcceleratorPedalStatus_SecPedActPrimPedNotAct
#   define AcceleratorPedalStatus_SecPedActPrimPedNotAct (1U)
#  endif

#  ifndef AcceleratorPedalStatus_PrimAndSecPedalInhibited
#   define AcceleratorPedalStatus_PrimAndSecPedalInhibited (2U)
#  endif

#  ifndef AcceleratorPedalStatus_Spare1
#   define AcceleratorPedalStatus_Spare1 (3U)
#  endif

#  ifndef AcceleratorPedalStatus_Spare2
#   define AcceleratorPedalStatus_Spare2 (4U)
#  endif

#  ifndef AcceleratorPedalStatus_Spare3
#   define AcceleratorPedalStatus_Spare3 (5U)
#  endif

#  ifndef AcceleratorPedalStatus_Error
#   define AcceleratorPedalStatus_Error (6U)
#  endif

#  ifndef AcceleratorPedalStatus_NotAvailable
#   define AcceleratorPedalStatus_NotAvailable (7U)
#  endif

#  ifndef ButtonStatus_Idle
#   define ButtonStatus_Idle (0U)
#  endif

#  ifndef ButtonStatus_Pressed
#   define ButtonStatus_Pressed (1U)
#  endif

#  ifndef ButtonStatus_Error
#   define ButtonStatus_Error (2U)
#  endif

#  ifndef ButtonStatus_NotAvailable
#   define ButtonStatus_NotAvailable (3U)
#  endif

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef DisableEnable_Disable
#   define DisableEnable_Disable (0U)
#  endif

#  ifndef DisableEnable_Enable
#   define DisableEnable_Enable (1U)
#  endif

#  ifndef DisableEnable_Error
#   define DisableEnable_Error (2U)
#  endif

#  ifndef DisableEnable_NotAvailable
#   define DisableEnable_NotAvailable (3U)
#  endif

#  ifndef EngineSpeedControlStatus_Inactive
#   define EngineSpeedControlStatus_Inactive (0U)
#  endif

#  ifndef EngineSpeedControlStatus_ActiveManual
#   define EngineSpeedControlStatus_ActiveManual (1U)
#  endif

#  ifndef EngineSpeedControlStatus_ActiveExternalSource
#   define EngineSpeedControlStatus_ActiveExternalSource (2U)
#  endif

#  ifndef EngineSpeedControlStatus_CrossCountryEngineSpeedControlActive
#   define EngineSpeedControlStatus_CrossCountryEngineSpeedControlActive (3U)
#  endif

#  ifndef EngineSpeedControlStatus_AutoPtoMin_AutoNeutral
#   define EngineSpeedControlStatus_AutoPtoMin_AutoNeutral (4U)
#  endif

#  ifndef EngineSpeedControlStatus_Spare_01
#   define EngineSpeedControlStatus_Spare_01 (5U)
#  endif

#  ifndef EngineSpeedControlStatus_Error
#   define EngineSpeedControlStatus_Error (6U)
#  endif

#  ifndef EngineSpeedControlStatus_NotAvailable
#   define EngineSpeedControlStatus_NotAvailable (7U)
#  endif

#  ifndef EngineSpeedRequest_NoRequest
#   define EngineSpeedRequest_NoRequest (0U)
#  endif

#  ifndef EngineSpeedRequest_Off
#   define EngineSpeedRequest_Off (1U)
#  endif

#  ifndef EngineSpeedRequest_Activate
#   define EngineSpeedRequest_Activate (2U)
#  endif

#  ifndef EngineSpeedRequest_Increase
#   define EngineSpeedRequest_Increase (3U)
#  endif

#  ifndef EngineSpeedRequest_Decrease
#   define EngineSpeedRequest_Decrease (4U)
#  endif

#  ifndef EngineSpeedRequest_Spare
#   define EngineSpeedRequest_Spare (5U)
#  endif

#  ifndef EngineSpeedRequest_Error
#   define EngineSpeedRequest_Error (6U)
#  endif

#  ifndef EngineSpeedRequest_NotAvailable
#   define EngineSpeedRequest_NotAvailable (7U)
#  endif

#  ifndef EscActionRequest_NoRequest
#   define EscActionRequest_NoRequest (0U)
#  endif

#  ifndef EscActionRequest_Resume
#   define EscActionRequest_Resume (1U)
#  endif

#  ifndef EscActionRequest_Increase
#   define EscActionRequest_Increase (2U)
#  endif

#  ifndef EscActionRequest_Decrease
#   define EscActionRequest_Decrease (3U)
#  endif

#  ifndef EscActionRequest_Set
#   define EscActionRequest_Set (4U)
#  endif

#  ifndef EscActionRequest_Spare
#   define EscActionRequest_Spare (5U)
#  endif

#  ifndef EscActionRequest_Error
#   define EscActionRequest_Error (6U)
#  endif

#  ifndef EscActionRequest_NotAvailable
#   define EscActionRequest_NotAvailable (7U)
#  endif

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

#  ifndef SWSpdCtrlButtonsStatus1_None
#   define SWSpdCtrlButtonsStatus1_None (0U)
#  endif

#  ifndef SWSpdCtrlButtonsStatus1_PauseOff
#   define SWSpdCtrlButtonsStatus1_PauseOff (1U)
#  endif

#  ifndef SWSpdCtrlButtonsStatus1_Resume
#   define SWSpdCtrlButtonsStatus1_Resume (2U)
#  endif

#  ifndef SWSpdCtrlButtonsStatus1_Increase
#   define SWSpdCtrlButtonsStatus1_Increase (3U)
#  endif

#  ifndef SWSpdCtrlButtonsStatus1_Decrease
#   define SWSpdCtrlButtonsStatus1_Decrease (4U)
#  endif

#  ifndef SWSpdCtrlButtonsStatus1_Enter
#   define SWSpdCtrlButtonsStatus1_Enter (5U)
#  endif

#  ifndef SWSpdCtrlButtonsStatus1_CC
#   define SWSpdCtrlButtonsStatus1_CC (6U)
#  endif

#  ifndef SWSpdCtrlButtonsStatus1_Overspeed
#   define SWSpdCtrlButtonsStatus1_Overspeed (7U)
#  endif

#  ifndef SWSpdCtrlButtonsStatus1_ACC
#   define SWSpdCtrlButtonsStatus1_ACC (8U)
#  endif

#  ifndef SWSpdCtrlButtonsStatus1_TimeGap
#   define SWSpdCtrlButtonsStatus1_TimeGap (9U)
#  endif

#  ifndef SWSpdCtrlButtonsStatus1_Spare
#   define SWSpdCtrlButtonsStatus1_Spare (10U)
#  endif

#  ifndef SWSpdCtrlButtonsStatus1_Spare_01
#   define SWSpdCtrlButtonsStatus1_Spare_01 (11U)
#  endif

#  ifndef SWSpdCtrlButtonsStatus1_Spare_02
#   define SWSpdCtrlButtonsStatus1_Spare_02 (12U)
#  endif

#  ifndef SWSpdCtrlButtonsStatus1_Spare_03
#   define SWSpdCtrlButtonsStatus1_Spare_03 (13U)
#  endif

#  ifndef SWSpdCtrlButtonsStatus1_Error
#   define SWSpdCtrlButtonsStatus1_Error (14U)
#  endif

#  ifndef SWSpdCtrlButtonsStatus1_NotAvaillable
#   define SWSpdCtrlButtonsStatus1_NotAvaillable (15U)
#  endif

#  ifndef SWSpeedControlAdjustMode_AdjustSpeed
#   define SWSpeedControlAdjustMode_AdjustSpeed (0U)
#  endif

#  ifndef SWSpeedControlAdjustMode_AdjustOverspeed
#   define SWSpeedControlAdjustMode_AdjustOverspeed (1U)
#  endif

#  ifndef SWSpeedControlAdjustMode_AdjustTimeGap
#   define SWSpeedControlAdjustMode_AdjustTimeGap (2U)
#  endif

#  ifndef SWSpeedControlAdjustMode_Spare
#   define SWSpeedControlAdjustMode_Spare (3U)
#  endif

#  ifndef SWSpeedControlAdjustMode_Spare_01
#   define SWSpeedControlAdjustMode_Spare_01 (4U)
#  endif

#  ifndef SWSpeedControlAdjustMode_Spare_02
#   define SWSpeedControlAdjustMode_Spare_02 (5U)
#  endif

#  ifndef SWSpeedControlAdjustMode_Spare_03
#   define SWSpeedControlAdjustMode_Spare_03 (6U)
#  endif

#  ifndef SWSpeedControlAdjustMode_Spare_04
#   define SWSpeedControlAdjustMode_Spare_04 (7U)
#  endif

#  ifndef SWSpeedControlAdjustMode_Spare_05
#   define SWSpeedControlAdjustMode_Spare_05 (8U)
#  endif

#  ifndef SWSpeedControlAdjustMode_Spare_06
#   define SWSpeedControlAdjustMode_Spare_06 (9U)
#  endif

#  ifndef SWSpeedControlAdjustMode_Spare_07
#   define SWSpeedControlAdjustMode_Spare_07 (10U)
#  endif

#  ifndef SWSpeedControlAdjustMode_Spare_08
#   define SWSpeedControlAdjustMode_Spare_08 (11U)
#  endif

#  ifndef SWSpeedControlAdjustMode_Spare_09
#   define SWSpeedControlAdjustMode_Spare_09 (12U)
#  endif

#  ifndef SWSpeedControlAdjustMode_Spare_10
#   define SWSpeedControlAdjustMode_Spare_10 (13U)
#  endif

#  ifndef SWSpeedControlAdjustMode_Error
#   define SWSpeedControlAdjustMode_Error (14U)
#  endif

#  ifndef SWSpeedControlAdjustMode_NotAvailable
#   define SWSpeedControlAdjustMode_NotAvailable (15U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_ENGINESPEEDCONTROL_HMICTRL_TYPE_H */
