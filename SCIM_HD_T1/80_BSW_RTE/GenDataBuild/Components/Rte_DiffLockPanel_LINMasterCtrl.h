/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_DiffLockPanel_LINMasterCtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <DiffLockPanel_LINMasterCtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_DIFFLOCKPANEL_LINMASTERCTRL_H
# define _RTE_DIFFLOCKPANEL_LINMASTERCTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_DiffLockPanel_LINMasterCtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_DiffLockPanel_LINMasterCtrl_DifflockDeactivationBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FreeWheel_Status_T, RTE_VAR_NOINIT) Rte_DiffLockPanel_LINMasterCtrl_DifflockMode_Wheelstatus_FreeWheel_Status; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_DiffLockPanel_LINMasterCtrl_EscButtonMuddySiteStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_DiffLockPanel_LINMasterCtrl_Offroad_ButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN4_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DiagActiveState_T, RTE_VAR_NOINIT) Rte_DiagnosticComponent_DiagActiveState_isDiagActive; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MovingUnitTraction_UICtrl_DifflockOnOff_Indication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_EngineSpeedControl_HMICtrl_EscButtonMuddySiteDeviceInd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MovingUnitTraction_UICtrl_Offroad_Indication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ComMode_LIN4_ComMode_LIN (7U)
#  define Rte_InitValue_DiagActiveState_isDiagActive (0U)
#  define Rte_InitValue_DiagInfoDLFW_DiagInfo (0U)
#  define Rte_InitValue_DifflockDeactivationBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_DifflockMode_Wheelstatus_FreeWheel_Status (15U)
#  define Rte_InitValue_DifflockOnOff_Indication_DeviceIndication (3U)
#  define Rte_InitValue_EscButtonMuddySiteDeviceInd_DeviceIndication (3U)
#  define Rte_InitValue_EscButtonMuddySiteStatus_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_DifflockDeactivationBtn_st_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_DifflockMode_Wheelstatus_FreeWheel_Status (15U)
#  define Rte_InitValue_LIN_DifflockOnOff_Indication_DeviceIndication (0U)
#  define Rte_InitValue_LIN_EscButtonMuddySiteDeviceIn_DeviceIndication (0U)
#  define Rte_InitValue_LIN_EscButtonMuddySiteStatus_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_Offroad_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_Offroad_Indication_DeviceIndication (0U)
#  define Rte_InitValue_Offroad_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_Offroad_Indication_DeviceIndication (3U)
#  define Rte_InitValue_ResponseErrorDLFW_ResponseErrorDLFW (FALSE)
# endif


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Buffers for inter-runnable variables
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_DiffLockPanel_LINMasterCtrl_Irv_IOCTL_DiffLockLinCtrl; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
# endif /* !defined(RTE_CORE) */


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_DiffLockPanel_LINMasterCtrl_ResponseErrorDLFW_ResponseErrorDLFW(P2VAR(ResponseErrorDLFW_T, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DiffLockPanel_LINMasterCtrl_LIN_DifflockOnOff_Indication_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DiffLockPanel_LINMasterCtrl_LIN_EscButtonMuddySiteDeviceIn_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DiffLockPanel_LINMasterCtrl_LIN_Offroad_Indication_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ComMode_LIN4_ComMode_LIN Rte_Read_DiffLockPanel_LINMasterCtrl_ComMode_LIN4_ComMode_LIN
#  define Rte_Read_DiffLockPanel_LINMasterCtrl_ComMode_LIN4_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN4_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DiagActiveState_isDiagActive Rte_Read_DiffLockPanel_LINMasterCtrl_DiagActiveState_isDiagActive
#  define Rte_Read_DiffLockPanel_LINMasterCtrl_DiagActiveState_isDiagActive(data) (*(data) = Rte_DiagnosticComponent_DiagActiveState_isDiagActive, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DiagInfoDLFW_DiagInfo Rte_Read_DiffLockPanel_LINMasterCtrl_DiagInfoDLFW_DiagInfo
#  define Rte_Read_DiffLockPanel_LINMasterCtrl_DiagInfoDLFW_DiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_DiagInfoDLFW_oDLFWtoCIOM_L4_oLIN03_f14b1ebc_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DifflockOnOff_Indication_DeviceIndication Rte_Read_DiffLockPanel_LINMasterCtrl_DifflockOnOff_Indication_DeviceIndication
#  define Rte_Read_DiffLockPanel_LINMasterCtrl_DifflockOnOff_Indication_DeviceIndication(data) (*(data) = Rte_MovingUnitTraction_UICtrl_DifflockOnOff_Indication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_EscButtonMuddySiteDeviceInd_DeviceIndication Rte_Read_DiffLockPanel_LINMasterCtrl_EscButtonMuddySiteDeviceInd_DeviceIndication
#  define Rte_Read_DiffLockPanel_LINMasterCtrl_EscButtonMuddySiteDeviceInd_DeviceIndication(data) (*(data) = Rte_EngineSpeedControl_HMICtrl_EscButtonMuddySiteDeviceInd_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_DifflockDeactivationBtn_st_PushButtonStatus Rte_Read_DiffLockPanel_LINMasterCtrl_LIN_DifflockDeactivationBtn_st_PushButtonStatus
#  define Rte_Read_DiffLockPanel_LINMasterCtrl_LIN_DifflockDeactivationBtn_st_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_DifflockDeactivationBtn_st_oDLFWtoCIOM_L4_oLIN03_7700113d_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_DifflockMode_Wheelstatus_FreeWheel_Status Rte_Read_DiffLockPanel_LINMasterCtrl_LIN_DifflockMode_Wheelstatus_FreeWheel_Status
#  define Rte_Read_DiffLockPanel_LINMasterCtrl_LIN_DifflockMode_Wheelstatus_FreeWheel_Status(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_DifflockMode_Wheelstatus_oDLFWtoCIOM_L4_oLIN03_80a746c7_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_EscButtonMuddySiteStatus_PushButtonStatus Rte_Read_DiffLockPanel_LINMasterCtrl_LIN_EscButtonMuddySiteStatus_PushButtonStatus
#  define Rte_Read_DiffLockPanel_LINMasterCtrl_LIN_EscButtonMuddySiteStatus_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_EscButtonMuddySiteStatus_oDLFWtoCIOM_L4_oLIN03_ac37f393_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_Offroad_ButtonStatus_PushButtonStatus Rte_Read_DiffLockPanel_LINMasterCtrl_LIN_Offroad_ButtonStatus_PushButtonStatus
#  define Rte_Read_DiffLockPanel_LINMasterCtrl_LIN_Offroad_ButtonStatus_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_Offroad_ButtonStatus_oDLFWtoCIOM_L4_oLIN03_2199360f_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Offroad_Indication_DeviceIndication Rte_Read_DiffLockPanel_LINMasterCtrl_Offroad_Indication_DeviceIndication
#  define Rte_Read_DiffLockPanel_LINMasterCtrl_Offroad_Indication_DeviceIndication(data) (*(data) = Rte_MovingUnitTraction_UICtrl_Offroad_Indication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ResponseErrorDLFW_ResponseErrorDLFW Rte_Read_DiffLockPanel_LINMasterCtrl_ResponseErrorDLFW_ResponseErrorDLFW


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_DifflockDeactivationBtn_stat_PushButtonStatus Rte_Write_DiffLockPanel_LINMasterCtrl_DifflockDeactivationBtn_stat_PushButtonStatus
#  define Rte_Write_DiffLockPanel_LINMasterCtrl_DifflockDeactivationBtn_stat_PushButtonStatus(data) (Rte_DiffLockPanel_LINMasterCtrl_DifflockDeactivationBtn_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_DifflockMode_Wheelstatus_FreeWheel_Status Rte_Write_DiffLockPanel_LINMasterCtrl_DifflockMode_Wheelstatus_FreeWheel_Status
#  define Rte_Write_DiffLockPanel_LINMasterCtrl_DifflockMode_Wheelstatus_FreeWheel_Status(data) (Rte_DiffLockPanel_LINMasterCtrl_DifflockMode_Wheelstatus_FreeWheel_Status = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_EscButtonMuddySiteStatus_PushButtonStatus Rte_Write_DiffLockPanel_LINMasterCtrl_EscButtonMuddySiteStatus_PushButtonStatus
#  define Rte_Write_DiffLockPanel_LINMasterCtrl_EscButtonMuddySiteStatus_PushButtonStatus(data) (Rte_DiffLockPanel_LINMasterCtrl_EscButtonMuddySiteStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LIN_DifflockOnOff_Indication_DeviceIndication Rte_Write_DiffLockPanel_LINMasterCtrl_LIN_DifflockOnOff_Indication_DeviceIndication
#  define Rte_Write_LIN_EscButtonMuddySiteDeviceIn_DeviceIndication Rte_Write_DiffLockPanel_LINMasterCtrl_LIN_EscButtonMuddySiteDeviceIn_DeviceIndication
#  define Rte_Write_LIN_Offroad_Indication_DeviceIndication Rte_Write_DiffLockPanel_LINMasterCtrl_LIN_Offroad_Indication_DeviceIndication
#  define Rte_Write_Offroad_ButtonStatus_PushButtonStatus Rte_Write_DiffLockPanel_LINMasterCtrl_Offroad_ButtonStatus_PushButtonStatus
#  define Rte_Write_DiffLockPanel_LINMasterCtrl_Offroad_ButtonStatus_PushButtonStatus(data) (Rte_DiffLockPanel_LINMasterCtrl_Offroad_ButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1BKH_87_DLFWLink_NoResp_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)208, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN2_16_DLFW_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)215, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN2_17_DLFW_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)216, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN2_44_DLFW_RAM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)217, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN2_45_DLFW_FLASH_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)218, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN2_46_DLFW_EEPROM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)219, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN2_94_DLFW_SWFAIL_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)220, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData_Irv_IOCTL_DiffLockLinCtrl() \
  Rte_Irv_DiffLockPanel_LINMasterCtrl_Irv_IOCTL_DiffLockLinCtrl
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_DiffLockLinCtrl(data) \
  (Rte_Irv_DiffLockPanel_LINMasterCtrl_Irv_IOCTL_DiffLockLinCtrl = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_DiffLockLinCtrl(data) \
  (Rte_Irv_DiffLockPanel_LINMasterCtrl_Irv_IOCTL_DiffLockLinCtrl = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DiffLockPanel_LINMasterCtrl_20ms_runnable_Irv_IOCTL_DiffLockLinCtrl() \
  Rte_Irv_DiffLockPanel_LINMasterCtrl_Irv_IOCTL_DiffLockLinCtrl
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DiffLockPanel_LINMasterCtrl_20ms_runnable_Irv_IOCTL_DiffLockLinCtrl(data) \
  (Rte_Irv_DiffLockPanel_LINMasterCtrl_Irv_IOCTL_DiffLockLinCtrl = (data))
/* PRQA L:L1 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B04_DLFW_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1B04_DLFW_Installed_v() (Rte_AddrPar_0x2B_P1B04_DLFW_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define DiffLockPanel_LINMasterCtrl_START_SEC_CODE
# include "DiffLockPanel_LINMasterCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_FreezeCurrentState DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData
#  define RTE_RUNNABLE_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment
#  define RTE_RUNNABLE_DiffLockPanel_LINMasterCtrl_20ms_runnable DiffLockPanel_LINMasterCtrl_20ms_runnable
# endif

FUNC(Std_ReturnType, DiffLockPanel_LINMasterCtrl_CODE) DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiffLockPanel_LINMasterCtrl_CODE) DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiffLockPanel_LINMasterCtrl_CODE) DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiffLockPanel_LINMasterCtrl_CODE) DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiffLockPanel_LINMasterCtrl_CODE) DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiffLockPanel_LINMasterCtrl_CODE) DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, DiffLockPanel_LINMasterCtrl_CODE) DiffLockPanel_LINMasterCtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define DiffLockPanel_LINMasterCtrl_STOP_SEC_CODE
# include "DiffLockPanel_LINMasterCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_P1EOR_Data_P1EOR_E_NOT_OK (1U)

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_DIFFLOCKPANEL_LINMASTERCTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
