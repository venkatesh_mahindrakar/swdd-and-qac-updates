/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_ECSWiredRemote_LINMasterCtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <ECSWiredRemote_LINMasterCtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_ECSWIREDREMOTE_LINMASTERCTRL_H
# define _RTE_ECSWIREDREMOTE_LINMASTERCTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_ECSWiredRemote_LINMasterCtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_AdjustButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_BackButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_MemButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_SelectButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_StopButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(EvalButtonRequest_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_WRDownButtonStatus_EvalButtonRequest; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(EvalButtonRequest_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_WRUpButtonStatus_EvalButtonRequest; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_Adjust_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN5_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DiagActiveState_T, RTE_VAR_NOINIT) Rte_DiagnosticComponent_DiagActiveState_isDiagActive; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_Down_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_M1_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_M2_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_M3_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ShortPulseMaxLength_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_ShortPulseMaxLength_ShortPulseMaxLength; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_Up_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_AdjustButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_Adjust_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_BackButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ComMode_LIN5_ComMode_LIN (7U)
#  define Rte_InitValue_DiagActiveState_isDiagActive (0U)
#  define Rte_InitValue_DiagInfoRCECS_DiagInfo (0U)
#  define Rte_InitValue_Down_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_LIN_AdjustButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_Adjust_DeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_LIN_BackButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_Down_DeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_LIN_M1_DeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_LIN_M2_DeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_LIN_M3_DeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_LIN_MemButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_SelectButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_ShortPulseMaxLength_ShortPulseMaxLength (0U)
#  define Rte_InitValue_LIN_StopButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_Up_DeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_LIN_WRDownButtonStatus_EvalButtonRequest (7U)
#  define Rte_InitValue_LIN_WRUpButtonStatus_EvalButtonRequest (7U)
#  define Rte_InitValue_M1_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_M2_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_M3_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_MemButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ResponseErrorRCECS_ResponseErrorRCECS (FALSE)
#  define Rte_InitValue_SelectButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ShortPulseMaxLength_ShortPulseMaxLength (15U)
#  define Rte_InitValue_StopButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_Up_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_WRDownButtonStatus_EvalButtonRequest (7U)
#  define Rte_InitValue_WRUpButtonStatus_EvalButtonRequest (7U)
# endif


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Buffers for inter-runnable variables
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_ECSWiredRemote_LINMasterCtrl_Irv_IOCTL_RCECSLinCtrl; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
# endif /* !defined(RTE_CORE) */


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ECSWiredRemote_LINMasterCtrl_ResponseErrorRCECS_ResponseErrorRCECS(P2VAR(ResponseErrorRCECS, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ECSWiredRemote_LINMasterCtrl_LIN_Adjust_DeviceIndication_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ECSWiredRemote_LINMasterCtrl_LIN_Down_DeviceIndication_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ECSWiredRemote_LINMasterCtrl_LIN_M1_DeviceIndication_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ECSWiredRemote_LINMasterCtrl_LIN_M2_DeviceIndication_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ECSWiredRemote_LINMasterCtrl_LIN_M3_DeviceIndication_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ECSWiredRemote_LINMasterCtrl_LIN_ShortPulseMaxLength_ShortPulseMaxLength(ShortPulseMaxLength_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ECSWiredRemote_LINMasterCtrl_LIN_Up_DeviceIndication_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_Adjust_DeviceIndication_DeviceIndication Rte_Read_ECSWiredRemote_LINMasterCtrl_Adjust_DeviceIndication_DeviceIndication
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_Adjust_DeviceIndication_DeviceIndication(data) (*(data) = Rte_WiredControlBox_HMICtrl_Adjust_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ComMode_LIN5_ComMode_LIN Rte_Read_ECSWiredRemote_LINMasterCtrl_ComMode_LIN5_ComMode_LIN
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_ComMode_LIN5_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN5_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DiagActiveState_isDiagActive Rte_Read_ECSWiredRemote_LINMasterCtrl_DiagActiveState_isDiagActive
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_DiagActiveState_isDiagActive(data) (*(data) = Rte_DiagnosticComponent_DiagActiveState_isDiagActive, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DiagInfoRCECS_DiagInfo Rte_Read_ECSWiredRemote_LINMasterCtrl_DiagInfoRCECS_DiagInfo
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_DiagInfoRCECS_DiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_DiagInfoRCECS_oRCECStoCIOM_L5_oLIN04_46e60a03_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Down_DeviceIndication_DeviceIndication Rte_Read_ECSWiredRemote_LINMasterCtrl_Down_DeviceIndication_DeviceIndication
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_Down_DeviceIndication_DeviceIndication(data) (*(data) = Rte_WiredControlBox_HMICtrl_Down_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_AdjustButtonStatus_PushButtonStatus Rte_Read_ECSWiredRemote_LINMasterCtrl_LIN_AdjustButtonStatus_PushButtonStatus
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_LIN_AdjustButtonStatus_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_AdjustButtonStatus_oRCECStoCIOM_L5_oLIN04_52fa0034_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BackButtonStatus_PushButtonStatus Rte_Read_ECSWiredRemote_LINMasterCtrl_LIN_BackButtonStatus_PushButtonStatus
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_LIN_BackButtonStatus_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BackButtonStatus_oRCECStoCIOM_L5_oLIN04_91249976_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_MemButtonStatus_PushButtonStatus Rte_Read_ECSWiredRemote_LINMasterCtrl_LIN_MemButtonStatus_PushButtonStatus
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_LIN_MemButtonStatus_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_MemButtonStatus_oRCECStoCIOM_L5_oLIN04_0975e3a6_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_SelectButtonStatus_PushButtonStatus Rte_Read_ECSWiredRemote_LINMasterCtrl_LIN_SelectButtonStatus_PushButtonStatus
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_LIN_SelectButtonStatus_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_SelectButtonStatus_oRCECStoCIOM_L5_oLIN04_07d81d0f_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_StopButtonStatus_PushButtonStatus Rte_Read_ECSWiredRemote_LINMasterCtrl_LIN_StopButtonStatus_PushButtonStatus
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_LIN_StopButtonStatus_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_StopButtonStatus_oRCECStoCIOM_L5_oLIN04_c8659d40_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_WRDownButtonStatus_EvalButtonRequest Rte_Read_ECSWiredRemote_LINMasterCtrl_LIN_WRDownButtonStatus_EvalButtonRequest
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_LIN_WRDownButtonStatus_EvalButtonRequest(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_WRDownButtonStatus_oRCECStoCIOM_L5_oLIN04_48a8dab2_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_WRUpButtonStatus_EvalButtonRequest Rte_Read_ECSWiredRemote_LINMasterCtrl_LIN_WRUpButtonStatus_EvalButtonRequest
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_LIN_WRUpButtonStatus_EvalButtonRequest(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_WRUpButtonStatus_oRCECStoCIOM_L5_oLIN04_a2873536_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_M1_DeviceIndication_DeviceIndication Rte_Read_ECSWiredRemote_LINMasterCtrl_M1_DeviceIndication_DeviceIndication
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_M1_DeviceIndication_DeviceIndication(data) (*(data) = Rte_WiredControlBox_HMICtrl_M1_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_M2_DeviceIndication_DeviceIndication Rte_Read_ECSWiredRemote_LINMasterCtrl_M2_DeviceIndication_DeviceIndication
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_M2_DeviceIndication_DeviceIndication(data) (*(data) = Rte_WiredControlBox_HMICtrl_M2_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_M3_DeviceIndication_DeviceIndication Rte_Read_ECSWiredRemote_LINMasterCtrl_M3_DeviceIndication_DeviceIndication
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_M3_DeviceIndication_DeviceIndication(data) (*(data) = Rte_WiredControlBox_HMICtrl_M3_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ResponseErrorRCECS_ResponseErrorRCECS Rte_Read_ECSWiredRemote_LINMasterCtrl_ResponseErrorRCECS_ResponseErrorRCECS
#  define Rte_Read_ShortPulseMaxLength_ShortPulseMaxLength Rte_Read_ECSWiredRemote_LINMasterCtrl_ShortPulseMaxLength_ShortPulseMaxLength
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_ShortPulseMaxLength_ShortPulseMaxLength(data) (*(data) = Rte_WiredControlBox_HMICtrl_ShortPulseMaxLength_ShortPulseMaxLength, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Up_DeviceIndication_DeviceIndication Rte_Read_ECSWiredRemote_LINMasterCtrl_Up_DeviceIndication_DeviceIndication
#  define Rte_Read_ECSWiredRemote_LINMasterCtrl_Up_DeviceIndication_DeviceIndication(data) (*(data) = Rte_WiredControlBox_HMICtrl_Up_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_AdjustButtonStatus_PushButtonStatus Rte_Write_ECSWiredRemote_LINMasterCtrl_AdjustButtonStatus_PushButtonStatus
#  define Rte_Write_ECSWiredRemote_LINMasterCtrl_AdjustButtonStatus_PushButtonStatus(data) (Rte_ECSWiredRemote_LINMasterCtrl_AdjustButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_BackButtonStatus_PushButtonStatus Rte_Write_ECSWiredRemote_LINMasterCtrl_BackButtonStatus_PushButtonStatus
#  define Rte_Write_ECSWiredRemote_LINMasterCtrl_BackButtonStatus_PushButtonStatus(data) (Rte_ECSWiredRemote_LINMasterCtrl_BackButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LIN_Adjust_DeviceIndication_DeviceIndication Rte_Write_ECSWiredRemote_LINMasterCtrl_LIN_Adjust_DeviceIndication_DeviceIndication
#  define Rte_Write_LIN_Down_DeviceIndication_DeviceIndication Rte_Write_ECSWiredRemote_LINMasterCtrl_LIN_Down_DeviceIndication_DeviceIndication
#  define Rte_Write_LIN_M1_DeviceIndication_DeviceIndication Rte_Write_ECSWiredRemote_LINMasterCtrl_LIN_M1_DeviceIndication_DeviceIndication
#  define Rte_Write_LIN_M2_DeviceIndication_DeviceIndication Rte_Write_ECSWiredRemote_LINMasterCtrl_LIN_M2_DeviceIndication_DeviceIndication
#  define Rte_Write_LIN_M3_DeviceIndication_DeviceIndication Rte_Write_ECSWiredRemote_LINMasterCtrl_LIN_M3_DeviceIndication_DeviceIndication
#  define Rte_Write_LIN_ShortPulseMaxLength_ShortPulseMaxLength Rte_Write_ECSWiredRemote_LINMasterCtrl_LIN_ShortPulseMaxLength_ShortPulseMaxLength
#  define Rte_Write_LIN_Up_DeviceIndication_DeviceIndication Rte_Write_ECSWiredRemote_LINMasterCtrl_LIN_Up_DeviceIndication_DeviceIndication
#  define Rte_Write_MemButtonStatus_PushButtonStatus Rte_Write_ECSWiredRemote_LINMasterCtrl_MemButtonStatus_PushButtonStatus
#  define Rte_Write_ECSWiredRemote_LINMasterCtrl_MemButtonStatus_PushButtonStatus(data) (Rte_ECSWiredRemote_LINMasterCtrl_MemButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_SelectButtonStatus_PushButtonStatus Rte_Write_ECSWiredRemote_LINMasterCtrl_SelectButtonStatus_PushButtonStatus
#  define Rte_Write_ECSWiredRemote_LINMasterCtrl_SelectButtonStatus_PushButtonStatus(data) (Rte_ECSWiredRemote_LINMasterCtrl_SelectButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_StopButtonStatus_PushButtonStatus Rte_Write_ECSWiredRemote_LINMasterCtrl_StopButtonStatus_PushButtonStatus
#  define Rte_Write_ECSWiredRemote_LINMasterCtrl_StopButtonStatus_PushButtonStatus(data) (Rte_ECSWiredRemote_LINMasterCtrl_StopButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_WRDownButtonStatus_EvalButtonRequest Rte_Write_ECSWiredRemote_LINMasterCtrl_WRDownButtonStatus_EvalButtonRequest
#  define Rte_Write_ECSWiredRemote_LINMasterCtrl_WRDownButtonStatus_EvalButtonRequest(data) (Rte_ECSWiredRemote_LINMasterCtrl_WRDownButtonStatus_EvalButtonRequest = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_WRUpButtonStatus_EvalButtonRequest Rte_Write_ECSWiredRemote_LINMasterCtrl_WRUpButtonStatus_EvalButtonRequest
#  define Rte_Write_ECSWiredRemote_LINMasterCtrl_WRUpButtonStatus_EvalButtonRequest(data) (Rte_ECSWiredRemote_LINMasterCtrl_WRUpButtonStatus_EvalButtonRequest = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1BKE_87_RCECSLink_NoResp_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)206, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOI_16_RCECS_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)246, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOI_17_RCECS_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)247, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOI_46_RCECS_EEPROM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)248, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOI_94_RCECS_SWFAIL_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)250, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_ActivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_ECSStandByTrigger1_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)20)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_DeactivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_ECSStandByTrigger1_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)20)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData_Irv_IOCTL_RCECSLinCtrl() \
  Rte_Irv_ECSWiredRemote_LINMasterCtrl_Irv_IOCTL_RCECSLinCtrl
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_RCECSLinCtrl(data) \
  (Rte_Irv_ECSWiredRemote_LINMasterCtrl_Irv_IOCTL_RCECSLinCtrl = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_RCECSLinCtrl(data) \
  (Rte_Irv_ECSWiredRemote_LINMasterCtrl_Irv_IOCTL_RCECSLinCtrl = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_ECSWiredRemote_LINMasterCtrl_20ms_runnable_Irv_IOCTL_RCECSLinCtrl() \
  Rte_Irv_ECSWiredRemote_LINMasterCtrl_Irv_IOCTL_RCECSLinCtrl
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_ECSWiredRemote_LINMasterCtrl_20ms_runnable_Irv_IOCTL_RCECSLinCtrl(data) \
  (Rte_Irv_ECSWiredRemote_LINMasterCtrl_Irv_IOCTL_RCECSLinCtrl = (data))
/* PRQA L:L1 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1ALT_ECS_PartialAirSystem_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1ALU_ECS_FullAirSystem_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1ALT_ECS_PartialAirSystem_v() (Rte_AddrPar_0x2B_and_0x37_P1ALT_ECS_PartialAirSystem_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1ALU_ECS_FullAirSystem_v() (Rte_AddrPar_0x2B_and_0x37_P1ALU_ECS_FullAirSystem_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define ECSWiredRemote_LINMasterCtrl_START_SEC_CODE
# include "ECSWiredRemote_LINMasterCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_FreezeCurrentState DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData
#  define RTE_RUNNABLE_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReturnControlToECU DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ShortTermAdjustment DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ShortTermAdjustment
#  define RTE_RUNNABLE_ECSWiredRemote_LINMasterCtrl_20ms_runnable ECSWiredRemote_LINMasterCtrl_20ms_runnable
# endif

FUNC(Std_ReturnType, ECSWiredRemote_LINMasterCtrl_CODE) DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, ECSWiredRemote_LINMasterCtrl_CODE) DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, ECSWiredRemote_LINMasterCtrl_CODE) DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, ECSWiredRemote_LINMasterCtrl_CODE) DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, ECSWiredRemote_LINMasterCtrl_CODE) DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, ECSWiredRemote_LINMasterCtrl_CODE) DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, ECSWiredRemote_LINMasterCtrl_CODE) ECSWiredRemote_LINMasterCtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define ECSWiredRemote_LINMasterCtrl_STOP_SEC_CODE
# include "ECSWiredRemote_LINMasterCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_P1EOV_Data_P1EOV_E_NOT_OK (1U)

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)

#  define RTE_E_Issm_IssRequest_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_ECSWIREDREMOTE_LINMASTERCTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
