/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_DiagnosticMonitor_Appl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <DiagnosticMonitor_Appl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_DIAGNOSTICMONITOR_APPL_H
# define _RTE_DIAGNOSTICMONITOR_APPL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_DiagnosticMonitor_Appl_Type.h"
# include "Rte_DataHandleType.h"


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_LIN_topology_P1AJR_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1AJR_LIN_topology_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DXX_FMSgateway_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_KeyfobEncryptCode_P1DS4_a_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DS4_KeyfobEncryptCode_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1AJR_LIN_topology_v() (Rte_AddrPar_0x2B_P1AJR_LIN_topology_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DXX_FMSgateway_Act_v() (Rte_AddrPar_0x2B_P1DXX_FMSgateway_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_P1DS4_KeyfobEncryptCode_v() (&(Rte_AddrPar_0x2B_P1DS4_KeyfobEncryptCode_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_P1DS4_KeyfobEncryptCode_v() (&Rte_AddrPar_0x2B_P1DS4_KeyfobEncryptCode_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AW6_IsEcuAvailableDDM_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AWY_IsEcuAvailableAlarm_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AXE_IsEcuAvailablePDM_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1AW6_IsEcuAvailableDDM_v() (Rte_AddrPar_0x2B_and_0x37_P1AW6_IsEcuAvailableDDM_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1AWY_IsEcuAvailableAlarm_v() (Rte_AddrPar_0x2B_and_0x37_P1AWY_IsEcuAvailableAlarm_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1AXE_IsEcuAvailablePDM_v() (Rte_AddrPar_0x2B_and_0x37_P1AXE_IsEcuAvailablePDM_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2D_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2D) Rte_AddrPar_0x2D_P1TTA_GearBoxLockActivation_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2D_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1TTA_GearBoxLockActivation_v() (Rte_AddrPar_0x2D_P1TTA_GearBoxLockActivation_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2D_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_CrankingLockActivation_P1DS3_T, RTE_CONST_SA_lvl_0x2D_and_0x37) Rte_AddrPar_0x2D_and_0x37_P1DS3_CrankingLockActivation_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2D_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1DS3_CrankingLockActivation_v() (Rte_AddrPar_0x2D_and_0x37_P1DS3_CrankingLockActivation_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1C54_FactoryModeActive_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ComCryptoKey_P1DLX_a_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1DLX_ComCryptoKey_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1C54_FactoryModeActive_v() (Rte_AddrPar_0x2F_P1C54_FactoryModeActive_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_P1DLX_ComCryptoKey_v() (&(Rte_AddrPar_0x2F_P1DLX_ComCryptoKey_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_P1DLX_ComCryptoKey_v() (&Rte_AddrPar_0x2F_P1DLX_ComCryptoKey_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

# endif /* !defined(RTE_CORE) */


# define DiagnosticMonitor_Appl_START_SEC_CODE
# include "DiagnosticMonitor_Appl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_P1QXU_Data_P1QXU_EcuNotConfiguredFailureCause_ReadData DataServices_P1QXU_Data_P1QXU_EcuNotConfiguredFailureCause_ReadData
# endif

# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_Appl_CODE) DataServices_P1QXU_Data_P1QXU_EcuNotConfiguredFailureCause_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_APPL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_Appl_CODE) DataServices_P1QXU_Data_P1QXU_EcuNotConfiguredFailureCause_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_APPL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define DiagnosticMonitor_Appl_STOP_SEC_CODE
# include "DiagnosticMonitor_Appl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_P1QXU_Data_P1QXU_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_DIAGNOSTICMONITOR_APPL_H */
