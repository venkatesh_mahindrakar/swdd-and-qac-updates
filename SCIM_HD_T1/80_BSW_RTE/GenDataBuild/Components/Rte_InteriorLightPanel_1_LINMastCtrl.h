/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_InteriorLightPanel_1_LINMastCtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <InteriorLightPanel_1_LINMastCtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_H
# define _RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_InteriorLightPanel_1_LINMastCtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_1_LINMastCtrl_DoorAutoFuncBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_1_LINMastCtrl_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_1_LINMastCtrl_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FreeWheel_Status_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_1_LINMastCtrl_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN1_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DiagActiveState_T, RTE_VAR_NOINIT) Rte_DiagnosticComponent_DiagActiveState_isDiagActive; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_DoorAutoFuncInd_cmd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_IntLghtOffModeInd_cmd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_IntLightMaxModeInd_cmd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_IntLightNightModeInd_cmd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_IntLightRestingModeInd_cmd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ComMode_LIN1_ComMode_LIN (7U)
#  define Rte_InitValue_DiagActiveState_isDiagActive (0U)
#  define Rte_InitValue_DoorAutoFuncBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_DoorAutoFuncInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status (15U)
#  define Rte_InitValue_IntLghtOffModeInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_IntLightMaxModeInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_IntLightNightModeInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_IntLightRestingModeInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_LIN_DoorAutoFuncBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_DoorAutoFuncInd_cmd_DeviceIndication (0U)
#  define Rte_InitValue_LIN_IntLghtDimmingLvlDecBtn_s_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_IntLghtDimmingLvlIncBtn_s_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_IntLghtModeSelrFreeWheel_s_FreeWheel_Status (15U)
#  define Rte_InitValue_LIN_IntLghtOffModeInd_cmd_DeviceIndication (0U)
#  define Rte_InitValue_LIN_IntLightMaxModeInd_cmd_DeviceIndication (0U)
#  define Rte_InitValue_LIN_IntLightNightModeInd_cmd_DeviceIndication (0U)
#  define Rte_InitValue_LIN_IntLightRestingModeInd_cmd_DeviceIndication (0U)
#  define Rte_InitValue_ResponseErrorILCP1_ResponseErrorILCP1 (FALSE)
# endif


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Buffers for inter-runnable variables
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_InteriorLightPanel_1_LINMastCtrl_Irv_IOCTL_ILCP1Lin; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
# endif /* !defined(RTE_CORE) */


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InteriorLightPanel_1_LINMastCtrl_LIN_IntLghtModeSelrFreeWheel_s_FreeWheel_Status(P2VAR(FreeWheel_Status_T, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InteriorLightPanel_1_LINMastCtrl_ResponseErrorILCP1_ResponseErrorILCP1(P2VAR(ResponseErrorILCP1_T, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_InteriorLightPanel_1_LINMastCtrl_LIN_DoorAutoFuncInd_cmd_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_InteriorLightPanel_1_LINMastCtrl_LIN_IntLghtOffModeInd_cmd_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_InteriorLightPanel_1_LINMastCtrl_LIN_IntLightMaxModeInd_cmd_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_InteriorLightPanel_1_LINMastCtrl_LIN_IntLightNightModeInd_cmd_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_InteriorLightPanel_1_LINMastCtrl_LIN_IntLightRestingModeInd_cmd_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ComMode_LIN1_ComMode_LIN Rte_Read_InteriorLightPanel_1_LINMastCtrl_ComMode_LIN1_ComMode_LIN
#  define Rte_Read_InteriorLightPanel_1_LINMastCtrl_ComMode_LIN1_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN1_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DiagActiveState_isDiagActive Rte_Read_InteriorLightPanel_1_LINMastCtrl_DiagActiveState_isDiagActive
#  define Rte_Read_InteriorLightPanel_1_LINMastCtrl_DiagActiveState_isDiagActive(data) (*(data) = Rte_DiagnosticComponent_DiagActiveState_isDiagActive, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DoorAutoFuncInd_cmd_DeviceIndication Rte_Read_InteriorLightPanel_1_LINMastCtrl_DoorAutoFuncInd_cmd_DeviceIndication
#  define Rte_Read_InteriorLightPanel_1_LINMastCtrl_DoorAutoFuncInd_cmd_DeviceIndication(data) (*(data) = Rte_InteriorLights_HMICtrl_DoorAutoFuncInd_cmd_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLghtOffModeInd_cmd_DeviceIndication Rte_Read_InteriorLightPanel_1_LINMastCtrl_IntLghtOffModeInd_cmd_DeviceIndication
#  define Rte_Read_InteriorLightPanel_1_LINMastCtrl_IntLghtOffModeInd_cmd_DeviceIndication(data) (*(data) = Rte_InteriorLights_HMICtrl_IntLghtOffModeInd_cmd_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLightMaxModeInd_cmd_DeviceIndication Rte_Read_InteriorLightPanel_1_LINMastCtrl_IntLightMaxModeInd_cmd_DeviceIndication
#  define Rte_Read_InteriorLightPanel_1_LINMastCtrl_IntLightMaxModeInd_cmd_DeviceIndication(data) (*(data) = Rte_InteriorLights_HMICtrl_IntLightMaxModeInd_cmd_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLightNightModeInd_cmd_DeviceIndication Rte_Read_InteriorLightPanel_1_LINMastCtrl_IntLightNightModeInd_cmd_DeviceIndication
#  define Rte_Read_InteriorLightPanel_1_LINMastCtrl_IntLightNightModeInd_cmd_DeviceIndication(data) (*(data) = Rte_InteriorLights_HMICtrl_IntLightNightModeInd_cmd_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLightRestingModeInd_cmd_DeviceIndication Rte_Read_InteriorLightPanel_1_LINMastCtrl_IntLightRestingModeInd_cmd_DeviceIndication
#  define Rte_Read_InteriorLightPanel_1_LINMastCtrl_IntLightRestingModeInd_cmd_DeviceIndication(data) (*(data) = Rte_InteriorLights_HMICtrl_IntLightRestingModeInd_cmd_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_DoorAutoFuncBtn_stat_PushButtonStatus Rte_Read_InteriorLightPanel_1_LINMastCtrl_LIN_DoorAutoFuncBtn_stat_PushButtonStatus
#  define Rte_Read_InteriorLightPanel_1_LINMastCtrl_LIN_DoorAutoFuncBtn_stat_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_DoorAutoFuncBtn_stat_oILCP1toCIOM_L1_oLIN00_0dc2e67f_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_IntLghtDimmingLvlDecBtn_s_PushButtonStatus Rte_Read_InteriorLightPanel_1_LINMastCtrl_LIN_IntLghtDimmingLvlDecBtn_s_PushButtonStatus
#  define Rte_Read_InteriorLightPanel_1_LINMastCtrl_LIN_IntLghtDimmingLvlDecBtn_s_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_IntLghtDimmingLvlDecBtn_s_oILCP1toCIOM_L1_oLIN00_0a50c243_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_IntLghtDimmingLvlIncBtn_s_PushButtonStatus Rte_Read_InteriorLightPanel_1_LINMastCtrl_LIN_IntLghtDimmingLvlIncBtn_s_PushButtonStatus
#  define Rte_Read_InteriorLightPanel_1_LINMastCtrl_LIN_IntLghtDimmingLvlIncBtn_s_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_IntLghtDimmingLvlIncBtn_s_oILCP1toCIOM_L1_oLIN00_34c0f447_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_IntLghtModeSelrFreeWheel_s_FreeWheel_Status Rte_Read_InteriorLightPanel_1_LINMastCtrl_LIN_IntLghtModeSelrFreeWheel_s_FreeWheel_Status
#  define Rte_Read_ResponseErrorILCP1_ResponseErrorILCP1 Rte_Read_InteriorLightPanel_1_LINMastCtrl_ResponseErrorILCP1_ResponseErrorILCP1


/**********************************************************************************************************************
 * Rte_IsUpdated_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_IsUpdated_LIN_IntLghtModeSelrFreeWheel_s_FreeWheel_Status Rte_IsUpdated_InteriorLightPanel_1_LINMastCtrl_LIN_IntLghtModeSelrFreeWheel_s_FreeWheel_Status
#  define Rte_IsUpdated_InteriorLightPanel_1_LINMastCtrl_LIN_IntLghtModeSelrFreeWheel_s_FreeWheel_Status() ((Rte_RxUpdateFlags.Rte_RxUpdate_InteriorLightPanel_1_LINMastCtrl_LIN_IntLghtModeSelrFreeWheel_s_FreeWheel_Status == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_DoorAutoFuncBtn_stat_PushButtonStatus Rte_Write_InteriorLightPanel_1_LINMastCtrl_DoorAutoFuncBtn_stat_PushButtonStatus
#  define Rte_Write_InteriorLightPanel_1_LINMastCtrl_DoorAutoFuncBtn_stat_PushButtonStatus(data) (Rte_InteriorLightPanel_1_LINMastCtrl_DoorAutoFuncBtn_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus Rte_Write_InteriorLightPanel_1_LINMastCtrl_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus
#  define Rte_Write_InteriorLightPanel_1_LINMastCtrl_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus(data) (Rte_InteriorLightPanel_1_LINMastCtrl_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus Rte_Write_InteriorLightPanel_1_LINMastCtrl_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus
#  define Rte_Write_InteriorLightPanel_1_LINMastCtrl_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus(data) (Rte_InteriorLightPanel_1_LINMastCtrl_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status Rte_Write_InteriorLightPanel_1_LINMastCtrl_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status
#  define Rte_Write_InteriorLightPanel_1_LINMastCtrl_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status(data) (Rte_InteriorLightPanel_1_LINMastCtrl_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LIN_DoorAutoFuncInd_cmd_DeviceIndication Rte_Write_InteriorLightPanel_1_LINMastCtrl_LIN_DoorAutoFuncInd_cmd_DeviceIndication
#  define Rte_Write_LIN_IntLghtOffModeInd_cmd_DeviceIndication Rte_Write_InteriorLightPanel_1_LINMastCtrl_LIN_IntLghtOffModeInd_cmd_DeviceIndication
#  define Rte_Write_LIN_IntLightMaxModeInd_cmd_DeviceIndication Rte_Write_InteriorLightPanel_1_LINMastCtrl_LIN_IntLightMaxModeInd_cmd_DeviceIndication
#  define Rte_Write_LIN_IntLightNightModeInd_cmd_DeviceIndication Rte_Write_InteriorLightPanel_1_LINMastCtrl_LIN_IntLightNightModeInd_cmd_DeviceIndication
#  define Rte_Write_LIN_IntLightRestingModeInd_cmd_DeviceIndication Rte_Write_InteriorLightPanel_1_LINMastCtrl_LIN_IntLightRestingModeInd_cmd_DeviceIndication


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1BKD_87_ILCP1Link_NoResp_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)205, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_ActivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_OtherInteriorLights2_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)33)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_DeactivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_OtherInteriorLights2_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)33)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData_Irv_IOCTL_ILCP1Lin() \
  Rte_Irv_InteriorLightPanel_1_LINMastCtrl_Irv_IOCTL_ILCP1Lin
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ILCP1Lin(data) \
  (Rte_Irv_InteriorLightPanel_1_LINMastCtrl_Irv_IOCTL_ILCP1Lin = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_ILCP1Lin(data) \
  (Rte_Irv_InteriorLightPanel_1_LINMastCtrl_Irv_IOCTL_ILCP1Lin = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_InteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ILCP1Lin() \
  Rte_Irv_InteriorLightPanel_1_LINMastCtrl_Irv_IOCTL_ILCP1Lin
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_InteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ILCP1Lin(data) \
  (Rte_Irv_InteriorLightPanel_1_LINMastCtrl_Irv_IOCTL_ILCP1Lin = (data))
/* PRQA L:L1 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VR1_ILCP1_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1VR1_ILCP1_Installed_v() (Rte_AddrPar_0x2B_P1VR1_ILCP1_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define InteriorLightPanel_1_LINMastCtrl_START_SEC_CODE
# include "InteriorLightPanel_1_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_FreezeCurrentState DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData
#  define RTE_RUNNABLE_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment
#  define RTE_RUNNABLE_InteriorLightPanel_1_LINMastCtrl_20ms_runnable InteriorLightPanel_1_LINMastCtrl_20ms_runnable
# endif

FUNC(Std_ReturnType, InteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, InteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, InteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, InteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, InteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, InteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, InteriorLightPanel_1_LINMastCtrl_CODE) InteriorLightPanel_1_LINMastCtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define InteriorLightPanel_1_LINMastCtrl_STOP_SEC_CODE
# include "InteriorLightPanel_1_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_P1EOU_Data_P1EOU_E_NOT_OK (1U)

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)

#  define RTE_E_Issm_IssRequest_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
