/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_SwivelSeat_ctrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <SwivelSeat_ctrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_SWIVELSEAT_CTRL_H
# define _RTE_SWIVELSEAT_CTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_SwivelSeat_ctrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(SeatSwivelStatus_T, RTE_VAR_NOINIT) Rte_SwivelSeatSwitch_hdlr_SeatSwivelStatus_SeatSwivelStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_EngineRun_EngineRun; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_SeatSwivelStatus_SeatSwivelStatus (3U)
#  define Rte_InitValue_SeatSwivelWarning_rqst_SeatSwivelWarning_rqst (3U)
#  define Rte_InitValue_SwcActivation_EngineRun_EngineRun (1U)
#  define Rte_InitValue_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed (65535U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SwivelSeat_ctrl_SeatSwivelWarning_rqst_SeatSwivelWarning_rqst(InactiveActive_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_SeatSwivelStatus_SeatSwivelStatus Rte_Read_SwivelSeat_ctrl_SeatSwivelStatus_SeatSwivelStatus
#  define Rte_Read_SwivelSeat_ctrl_SeatSwivelStatus_SeatSwivelStatus(data) (*(data) = Rte_SwivelSeatSwitch_hdlr_SeatSwivelStatus_SeatSwivelStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_EngineRun_EngineRun Rte_Read_SwivelSeat_ctrl_SwcActivation_EngineRun_EngineRun
#  define Rte_Read_SwivelSeat_ctrl_SwcActivation_EngineRun_EngineRun(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_EngineRun_EngineRun, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed Rte_Read_SwivelSeat_ctrl_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed
#  define Rte_Read_SwivelSeat_ctrl_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(data) (Com_ReceiveSignal(ComConf_ComSignal_WheelBasedVehicleSpeed_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_e3ff6d4c_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_SeatSwivelWarning_rqst_SeatSwivelWarning_rqst Rte_Write_SwivelSeat_ctrl_SeatSwivelWarning_rqst_SeatSwivelWarning_rqst


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWQ_SwivelSeatCheck_SetSpeed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1DWQ_SwivelSeatCheck_SetSpeed_v() (Rte_AddrPar_0x2B_P1DWQ_SwivelSeatCheck_SetSpeed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1CUD_SwivelSeatWarning_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1CUD_SwivelSeatWarning_Act_v() (Rte_AddrPar_0x2B_and_0x37_P1CUD_SwivelSeatWarning_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define SwivelSeat_ctrl_START_SEC_CODE
# include "SwivelSeat_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_SwivelSeat_ctrl_20ms_runnable SwivelSeat_ctrl_20ms_runnable
# endif

FUNC(void, SwivelSeat_ctrl_CODE) SwivelSeat_ctrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define SwivelSeat_ctrl_STOP_SEC_CODE
# include "SwivelSeat_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_SWIVELSEAT_CTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
