/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_IoHwAb_ASIL_Dobhs.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <IoHwAb_ASIL_Dobhs>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_IOHWAB_ASIL_DOBHS_H
# define _RTE_IOHWAB_ASIL_DOBHS_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_IoHwAb_ASIL_Dobhs_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DiagActiveState_T, RTE_VAR_NOINIT) Rte_DiagnosticComponent_DiagActiveState_isDiagActive; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Fsc_OperationalMode_T, RTE_VAR_NOINIT) Rte_IoHwAb_ASIL_Core_Fsc_OperationalMode_P_Fsc_OperationalMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(uint8, RTE_VAR_NOINIT) Rte_SCIM_PVTPT_IO_ScimPvtControl_P_Status; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_DiagActiveState_P_isDiagActive (0U)
#  define Rte_InitValue_Fsc_OperationalMode_P_Fsc_OperationalMode (6U)
#  define Rte_InitValue_ScimPvtControl_P_Status (0U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuFaultStatus(P2VAR(Rte_DT_EcuHwFaultValues_T_0, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuFaultStatus(P2VAR(EcuHwFaultValues_T, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(P2VAR(Rte_DT_EcuHwDioCtrlArray_T_0, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(P2VAR(EcuHwDioCtrlArray_T, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuFaultStatus(P2VAR(Rte_DT_EcuHwFaultValues_T_0, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuFaultStatus(P2VAR(EcuHwFaultValues_T, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(P2VAR(Rte_DT_EcuHwDioCtrlArray_T_0, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(P2VAR(EcuHwDioCtrlArray_T, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuFaultStatus(P2VAR(Rte_DT_EcuHwFaultValues_T_0, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuFaultStatus(P2VAR(EcuHwFaultValues_T, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(P2VAR(Rte_DT_EcuHwDioCtrlArray_T_0, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(P2VAR(EcuHwDioCtrlArray_T, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuFaultStatus(P2VAR(Rte_DT_EcuHwFaultValues_T_0, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuFaultStatus(P2VAR(EcuHwFaultValues_T, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(P2VAR(Rte_DT_EcuHwDioCtrlArray_T_0, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(P2VAR(EcuHwDioCtrlArray_T, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuFaultStatus(P2VAR(Rte_DT_EcuHwFaultValues_T_0, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuFaultStatus(P2VAR(EcuHwFaultValues_T, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(P2VAR(Rte_DT_EcuHwDioCtrlArray_T_0, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(P2VAR(EcuHwDioCtrlArray_T, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(P2VAR(Rte_DT_EcuHwFaultValues_T_0, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(P2VAR(EcuHwFaultValues_T, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(P2VAR(Rte_DT_EcuHwDioCtrlArray_T_0, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Dobhs_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(P2VAR(EcuHwDioCtrlArray_T, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvWrite_IoHwAb_ASIL_Dobhs_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(P2CONST(Rte_DT_EcuHwFaultValues_T_0, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvWrite_IoHwAb_ASIL_Dobhs_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(P2CONST(EcuHwFaultValues_T, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvWrite_IoHwAb_ASIL_Dobhs_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(P2CONST(Rte_DT_EcuHwDioCtrlArray_T_0, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvWrite_IoHwAb_ASIL_Dobhs_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(P2CONST(EcuHwDioCtrlArray_T, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_DiagActiveState_P_isDiagActive Rte_Read_IoHwAb_ASIL_Dobhs_DiagActiveState_P_isDiagActive
#  define Rte_Read_IoHwAb_ASIL_Dobhs_DiagActiveState_P_isDiagActive(data) (*(data) = Rte_DiagnosticComponent_DiagActiveState_isDiagActive, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode Rte_Read_IoHwAb_ASIL_Dobhs_Fsc_OperationalMode_P_Fsc_OperationalMode
#  define Rte_Read_IoHwAb_ASIL_Dobhs_Fsc_OperationalMode_P_Fsc_OperationalMode(data) (*(data) = Rte_IoHwAb_ASIL_Core_Fsc_OperationalMode_P_Fsc_OperationalMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ScimPvtControl_P_Status Rte_Read_IoHwAb_ASIL_Dobhs_ScimPvtControl_P_Status
#  define Rte_Read_IoHwAb_ASIL_Dobhs_ScimPvtControl_P_Status(data) (*(data) = Rte_SCIM_PVTPT_IO_ScimPvtControl_P_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_IOHWAB_ASIL_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_CORE_APPL_CODE) EcuHwState_P_GetEcuVoltages_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) EcuVoltageValues); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  else
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_CORE_APPL_CODE) EcuHwState_P_GetEcuVoltages_CS(P2VAR(EcuHwVoltageValues_T, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) EcuVoltageValues); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  endif
#  define RTE_STOP_SEC_IOHWAB_ASIL_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_EcuHwState_P_GetEcuVoltages_CS EcuHwState_P_GetEcuVoltages_CS
#  define RTE_START_SEC_IOHWAB_ASIL_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_CORE_APPL_CODE) VbatInterface_P_GetVbatVoltage_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_VbatInterface_P_GetVbatVoltage_CS VbatInterface_P_GetVbatVoltage_CS


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuFaultStatus(data) \
  Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuFaultStatus(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(data) \
  Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuFaultStatus(data) \
  Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuFaultStatus(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(data) \
  Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuFaultStatus(data) \
  Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuFaultStatus(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(data) \
  Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuFaultStatus(data) \
  Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuFaultStatus(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(data) \
  Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuFaultStatus(data) \
  Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuFaultStatus(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(data) \
  Rte_IrvRead_IoHwAb_ASIL_Dobhs_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(data) \
  Rte_IrvRead_IoHwAb_ASIL_Dobhs_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(data) \
  Rte_IrvRead_IoHwAb_ASIL_Dobhs_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(data) \
  Rte_IrvWrite_IoHwAb_ASIL_Dobhs_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(data) \
  Rte_IrvWrite_IoHwAb_ASIL_Dobhs_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(data)
/* PRQA L:L1 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_HwToleranceThreshold_X1C04_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1C04_HwToleranceThreshold_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_DOBHS_X1CXX_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXX_PcbConfig_DOBHS_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CY1_DigitalBiLevelVoltageConfig_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_X1C04_HwToleranceThreshold_v() (Rte_AddrPar_0x29_X1C04_HwToleranceThreshold_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CXX_PcbConfig_DOBHS_v() (&(Rte_AddrPar_0x29_X1CXX_PcbConfig_DOBHS_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CXX_PcbConfig_DOBHS_v() (&Rte_AddrPar_0x29_X1CXX_PcbConfig_DOBHS_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  define Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v() (&Rte_AddrPar_0x29_X1CY1_DigitalBiLevelVoltageConfig_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define IoHwAb_ASIL_Dobhs_START_SEC_CODE
# include "IoHwAb_ASIL_Dobhs_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DobhsCtrlInterface_P_1_GetDobhsPinState_CS DobhsCtrlInterface_P_1_GetDobhsPinState_CS
#  define RTE_RUNNABLE_DobhsCtrlInterface_P_1_SetDobhsActive_CS DobhsCtrlInterface_P_1_SetDobhsActive_CS
#  define RTE_RUNNABLE_DobhsCtrlInterface_P_2_GetDobhsPinState_CS DobhsCtrlInterface_P_2_GetDobhsPinState_CS
#  define RTE_RUNNABLE_DobhsCtrlInterface_P_2_SetDobhsActive_CS DobhsCtrlInterface_P_2_SetDobhsActive_CS
#  define RTE_RUNNABLE_DobhsCtrlInterface_P_3_GetDobhsPinState_CS DobhsCtrlInterface_P_3_GetDobhsPinState_CS
#  define RTE_RUNNABLE_DobhsCtrlInterface_P_3_SetDobhsActive_CS DobhsCtrlInterface_P_3_SetDobhsActive_CS
#  define RTE_RUNNABLE_DobhsCtrlInterface_P_4_GetDobhsPinState_CS DobhsCtrlInterface_P_4_GetDobhsPinState_CS
#  define RTE_RUNNABLE_DobhsCtrlInterface_P_4_SetDobhsActive_CS DobhsCtrlInterface_P_4_SetDobhsActive_CS
#  define RTE_RUNNABLE_DobhsDiagInterface_P_GetDobhsPinState_CS DobhsDiagInterface_P_GetDobhsPinState_CS
#  define RTE_RUNNABLE_IoHwAb_ASIL_Dobhs_10ms_runnable IoHwAb_ASIL_Dobhs_10ms_runnable
# endif

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_1_GetDobhsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_2_GetDobhsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_3_GetDobhsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_4_GetDobhsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) isDioActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, IoHwAb_ASIL_Dobhs_CODE) IoHwAb_ASIL_Dobhs_10ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define IoHwAb_ASIL_Dobhs_STOP_SEC_CODE
# include "IoHwAb_ASIL_Dobhs_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_EcuHwState_I_AdcInFailure (1U)

#  define RTE_E_VbatInterface_I_AdcInFailure (2U)

#  define RTE_E_VbatInterface_I_IoHwAbApplicationError (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_IOHWAB_ASIL_DOBHS_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
