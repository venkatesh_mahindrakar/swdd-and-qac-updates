/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_ServiceBraking_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <ServiceBraking_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_SERVICEBRAKING_HMICTRL_H
# define _RTE_SERVICEBRAKING_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_ServiceBraking_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ServiceBraking_HMICtrl_HillStartAid_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ABSInhibitSwitchStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_HillStartAidButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ABSInhibitSwitchStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ABSInhibitionRequest_ABSInhibitionRequest (3U)
#  define Rte_InitValue_ABSInhibitionStatus_ABSInhibitionStatus (3U)
#  define Rte_InitValue_ASRHillHolderSwitch_ASRHillHolderSwitch (3U)
#  define Rte_InitValue_HSADriverRequest_HSADriverRequest (3U)
#  define Rte_InitValue_HillStartAidButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_HillStartAid_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_SwcActivation_IgnitionOn_IgnitionOn (1U)
#  define Rte_InitValue_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed (65535U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ServiceBraking_HMICtrl_ABSInhibitionStatus_ABSInhibitionStatus(P2VAR(Inhibit_T, AUTOMATIC, RTE_SERVICEBRAKING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ServiceBraking_HMICtrl_ASRHillHolderSwitch_ASRHillHolderSwitch(P2VAR(PassiveActive_T, AUTOMATIC, RTE_SERVICEBRAKING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ServiceBraking_HMICtrl_ABSInhibitionRequest_ABSInhibitionRequest(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ServiceBraking_HMICtrl_HSADriverRequest_HSADriverRequest(InactiveActive_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ABSInhibitSwitchStatus_PushButtonStatus Rte_Read_ServiceBraking_HMICtrl_ABSInhibitSwitchStatus_PushButtonStatus
#  define Rte_Read_ServiceBraking_HMICtrl_ABSInhibitSwitchStatus_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_ABSInhibitSwitchStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ABSInhibitionStatus_ABSInhibitionStatus Rte_Read_ServiceBraking_HMICtrl_ABSInhibitionStatus_ABSInhibitionStatus
#  define Rte_Read_ASRHillHolderSwitch_ASRHillHolderSwitch Rte_Read_ServiceBraking_HMICtrl_ASRHillHolderSwitch_ASRHillHolderSwitch
#  define Rte_Read_HillStartAidButtonStatus_PushButtonStatus Rte_Read_ServiceBraking_HMICtrl_HillStartAidButtonStatus_PushButtonStatus
#  define Rte_Read_ServiceBraking_HMICtrl_HillStartAidButtonStatus_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_HillStartAidButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_IgnitionOn_IgnitionOn Rte_Read_ServiceBraking_HMICtrl_SwcActivation_IgnitionOn_IgnitionOn
#  define Rte_Read_ServiceBraking_HMICtrl_SwcActivation_IgnitionOn_IgnitionOn(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed Rte_Read_ServiceBraking_HMICtrl_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed
#  define Rte_Read_ServiceBraking_HMICtrl_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(data) (Com_ReceiveSignal(ComConf_ComSignal_WheelBasedVehicleSpeed_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_e3ff6d4c_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_ABSInhibitionRequest_ABSInhibitionRequest Rte_Write_ServiceBraking_HMICtrl_ABSInhibitionRequest_ABSInhibitionRequest
#  define Rte_Write_HSADriverRequest_HSADriverRequest Rte_Write_ServiceBraking_HMICtrl_HSADriverRequest_HSADriverRequest
#  define Rte_Write_HillStartAid_DeviceIndication_DeviceIndication Rte_Write_ServiceBraking_HMICtrl_HillStartAid_DeviceIndication_DeviceIndication
#  define Rte_Write_ServiceBraking_HMICtrl_HillStartAid_DeviceIndication_DeviceIndication(data) (Rte_ServiceBraking_HMICtrl_HillStartAid_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_ABS_Inhibit_SwType_P1SY6_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1SY6_ABS_Inhibit_SwType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1A1R_HSA_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NTV_HSA_DefaultConfig_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1SY4_ABS_Inhibit_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1SY6_ABS_Inhibit_SwType_v() (Rte_AddrPar_0x2B_P1SY6_ABS_Inhibit_SwType_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1A1R_HSA_Installed_v() (Rte_AddrPar_0x2B_P1A1R_HSA_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1NTV_HSA_DefaultConfig_v() (Rte_AddrPar_0x2B_P1NTV_HSA_DefaultConfig_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1SY4_ABS_Inhibit_Installed_v() (Rte_AddrPar_0x2B_P1SY4_ABS_Inhibit_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define ServiceBraking_HMICtrl_START_SEC_CODE
# include "ServiceBraking_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_SB_ABS_Inhibit_20ms_runnable SB_ABS_Inhibit_20ms_runnable
#  define RTE_RUNNABLE_SB_HSA_20ms_runnable SB_HSA_20ms_runnable
#  define RTE_RUNNABLE_ServiceBraking_HMICtrl_20ms_runnable ServiceBraking_HMICtrl_20ms_runnable
#  define RTE_RUNNABLE_ServiceBraking_HMICtrl_Init ServiceBraking_HMICtrl_Init
# endif

FUNC(void, ServiceBraking_HMICtrl_CODE) SB_ABS_Inhibit_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, ServiceBraking_HMICtrl_CODE) SB_HSA_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, ServiceBraking_HMICtrl_CODE) ServiceBraking_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, ServiceBraking_HMICtrl_CODE) ServiceBraking_HMICtrl_Init(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define ServiceBraking_HMICtrl_STOP_SEC_CODE
# include "ServiceBraking_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_SERVICEBRAKING_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
