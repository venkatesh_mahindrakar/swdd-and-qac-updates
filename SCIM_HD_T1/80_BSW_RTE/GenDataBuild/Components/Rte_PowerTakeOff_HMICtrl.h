/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_PowerTakeOff_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <PowerTakeOff_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_POWERTAKEOFF_HMICTRL_H
# define _RTE_POWERTAKEOFF_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_PowerTakeOff_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_PowerTakeOff_HMICtrl_PTO1_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_PowerTakeOff_HMICtrl_PTO2_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_PowerTakeOff_HMICtrl_PTO3_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_PowerTakeOff_HMICtrl_PTO4_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Pto1SwitchStatus_Pto1SwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Pto2SwitchStatus_Pto2SwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Pto3SwitchStatus_Pto3SwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Pto4SwitchStatus_Pto4SwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PTO1_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_PTO2_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_PTO3_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_PTO4_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_Pto1CabRequest_Pto1CabRequest (3U)
#  define Rte_InitValue_Pto1Indication_Pto1Indication (3U)
#  define Rte_InitValue_Pto1Status_Pto1Status (3U)
#  define Rte_InitValue_Pto1SwitchStatus_Pto1SwitchStatus (3U)
#  define Rte_InitValue_Pto2CabRequest_Pto2CabRequest (3U)
#  define Rte_InitValue_Pto2Indication_Pto2Indication (3U)
#  define Rte_InitValue_Pto2Status_Pto2Status (3U)
#  define Rte_InitValue_Pto2SwitchStatus_Pto2SwitchStatus (3U)
#  define Rte_InitValue_Pto3CabRequest_Pto3CabRequest (3U)
#  define Rte_InitValue_Pto3Indication_Pto3Indication (3U)
#  define Rte_InitValue_Pto3Status_Pto3Status (3U)
#  define Rte_InitValue_Pto3SwitchStatus_Pto3SwitchStatus (3U)
#  define Rte_InitValue_Pto4CabRequest_Pto4CabRequest (3U)
#  define Rte_InitValue_Pto4Indication_Pto4Indication (3U)
#  define Rte_InitValue_Pto4Status_Pto4Status (3U)
#  define Rte_InitValue_Pto4SwitchStatus_Pto4SwitchStatus (3U)
#  define Rte_InitValue_SwcActivation_IgnitionOn_IgnitionOn (1U)
#  define Rte_InitValue_WrcPto1ButtonStatus_WrcPto1ButtonStatus (3U)
#  define Rte_InitValue_WrcPto2ButtonStatus_WrcPto2ButtonStatus (3U)
#  define Rte_InitValue_WrcPto3ButtonStatus_WrcPto3ButtonStatus (3U)
#  define Rte_InitValue_WrcPto4ButtonStatus_WrcPto4ButtonStatus (3U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_PowerTakeOff_HMICtrl_Pto1Status_Pto1Status(P2VAR(InactiveActive_T, AUTOMATIC, RTE_POWERTAKEOFF_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_PowerTakeOff_HMICtrl_Pto2Indication_Pto2Indication(P2VAR(Request_T, AUTOMATIC, RTE_POWERTAKEOFF_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_PowerTakeOff_HMICtrl_Pto2Status_Pto2Status(P2VAR(InactiveActive_T, AUTOMATIC, RTE_POWERTAKEOFF_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_PowerTakeOff_HMICtrl_Pto4Status_Pto4Status(P2VAR(InactiveActive_T, AUTOMATIC, RTE_POWERTAKEOFF_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_PowerTakeOff_HMICtrl_WrcPto1ButtonStatus_WrcPto1ButtonStatus(P2VAR(ButtonStatus_T, AUTOMATIC, RTE_POWERTAKEOFF_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_PowerTakeOff_HMICtrl_WrcPto2ButtonStatus_WrcPto2ButtonStatus(P2VAR(ButtonStatus_T, AUTOMATIC, RTE_POWERTAKEOFF_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_PowerTakeOff_HMICtrl_WrcPto3ButtonStatus_WrcPto3ButtonStatus(P2VAR(ButtonStatus_T, AUTOMATIC, RTE_POWERTAKEOFF_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_PowerTakeOff_HMICtrl_WrcPto4ButtonStatus_WrcPto4ButtonStatus(P2VAR(ButtonStatus_T, AUTOMATIC, RTE_POWERTAKEOFF_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_PowerTakeOff_HMICtrl_Pto1CabRequest_Pto1CabRequest(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_PowerTakeOff_HMICtrl_Pto2CabRequest_Pto2CabRequest(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_PowerTakeOff_HMICtrl_Pto3CabRequest_Pto3CabRequest(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_PowerTakeOff_HMICtrl_Pto4CabRequest_Pto4CabRequest(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_Pto1Indication_Pto1Indication Rte_Read_PowerTakeOff_HMICtrl_Pto1Indication_Pto1Indication
#  define Rte_Read_PowerTakeOff_HMICtrl_Pto1Indication_Pto1Indication(data) (Com_ReceiveSignal(ComConf_ComSignal_Pto1Indication_oVMCU_BB2_07P_oBackbone2_4298758a_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Pto1Status_Pto1Status Rte_Read_PowerTakeOff_HMICtrl_Pto1Status_Pto1Status
#  define Rte_Read_Pto1SwitchStatus_Pto1SwitchStatus Rte_Read_PowerTakeOff_HMICtrl_Pto1SwitchStatus_Pto1SwitchStatus
#  define Rte_Read_PowerTakeOff_HMICtrl_Pto1SwitchStatus_Pto1SwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_Pto1SwitchStatus_Pto1SwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Pto2Indication_Pto2Indication Rte_Read_PowerTakeOff_HMICtrl_Pto2Indication_Pto2Indication
#  define Rte_Read_Pto2Status_Pto2Status Rte_Read_PowerTakeOff_HMICtrl_Pto2Status_Pto2Status
#  define Rte_Read_Pto2SwitchStatus_Pto2SwitchStatus Rte_Read_PowerTakeOff_HMICtrl_Pto2SwitchStatus_Pto2SwitchStatus
#  define Rte_Read_PowerTakeOff_HMICtrl_Pto2SwitchStatus_Pto2SwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_Pto2SwitchStatus_Pto2SwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Pto3Indication_Pto3Indication Rte_Read_PowerTakeOff_HMICtrl_Pto3Indication_Pto3Indication
#  define Rte_Read_PowerTakeOff_HMICtrl_Pto3Indication_Pto3Indication(data) (Com_ReceiveSignal(ComConf_ComSignal_Pto3Indication_oVMCU_BB2_07P_oBackbone2_b308dae1_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Pto3Status_Pto3Status Rte_Read_PowerTakeOff_HMICtrl_Pto3Status_Pto3Status
#  define Rte_Read_PowerTakeOff_HMICtrl_Pto3Status_Pto3Status(data) (Com_ReceiveSignal(ComConf_ComSignal_Pto3Status_ISig_4_oVMCU_BB2_07P_oBackbone2_5abeb512_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Pto3SwitchStatus_Pto3SwitchStatus Rte_Read_PowerTakeOff_HMICtrl_Pto3SwitchStatus_Pto3SwitchStatus
#  define Rte_Read_PowerTakeOff_HMICtrl_Pto3SwitchStatus_Pto3SwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_Pto3SwitchStatus_Pto3SwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Pto4Indication_Pto4Indication Rte_Read_PowerTakeOff_HMICtrl_Pto4Indication_Pto4Indication
#  define Rte_Read_PowerTakeOff_HMICtrl_Pto4Indication_Pto4Indication(data) (Com_ReceiveSignal(ComConf_ComSignal_Pto4Indication_oVMCU_BB2_07P_oBackbone2_efb8f988_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Pto4Status_Pto4Status Rte_Read_PowerTakeOff_HMICtrl_Pto4Status_Pto4Status
#  define Rte_Read_Pto4SwitchStatus_Pto4SwitchStatus Rte_Read_PowerTakeOff_HMICtrl_Pto4SwitchStatus_Pto4SwitchStatus
#  define Rte_Read_PowerTakeOff_HMICtrl_Pto4SwitchStatus_Pto4SwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_Pto4SwitchStatus_Pto4SwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_IgnitionOn_IgnitionOn Rte_Read_PowerTakeOff_HMICtrl_SwcActivation_IgnitionOn_IgnitionOn
#  define Rte_Read_PowerTakeOff_HMICtrl_SwcActivation_IgnitionOn_IgnitionOn(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WrcPto1ButtonStatus_WrcPto1ButtonStatus Rte_Read_PowerTakeOff_HMICtrl_WrcPto1ButtonStatus_WrcPto1ButtonStatus
#  define Rte_Read_WrcPto2ButtonStatus_WrcPto2ButtonStatus Rte_Read_PowerTakeOff_HMICtrl_WrcPto2ButtonStatus_WrcPto2ButtonStatus
#  define Rte_Read_WrcPto3ButtonStatus_WrcPto3ButtonStatus Rte_Read_PowerTakeOff_HMICtrl_WrcPto3ButtonStatus_WrcPto3ButtonStatus
#  define Rte_Read_WrcPto4ButtonStatus_WrcPto4ButtonStatus Rte_Read_PowerTakeOff_HMICtrl_WrcPto4ButtonStatus_WrcPto4ButtonStatus


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PTO1_DeviceIndication_DeviceIndication Rte_Write_PowerTakeOff_HMICtrl_PTO1_DeviceIndication_DeviceIndication
#  define Rte_Write_PowerTakeOff_HMICtrl_PTO1_DeviceIndication_DeviceIndication(data) (Rte_PowerTakeOff_HMICtrl_PTO1_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_PTO2_DeviceIndication_DeviceIndication Rte_Write_PowerTakeOff_HMICtrl_PTO2_DeviceIndication_DeviceIndication
#  define Rte_Write_PowerTakeOff_HMICtrl_PTO2_DeviceIndication_DeviceIndication(data) (Rte_PowerTakeOff_HMICtrl_PTO2_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_PTO3_DeviceIndication_DeviceIndication Rte_Write_PowerTakeOff_HMICtrl_PTO3_DeviceIndication_DeviceIndication
#  define Rte_Write_PowerTakeOff_HMICtrl_PTO3_DeviceIndication_DeviceIndication(data) (Rte_PowerTakeOff_HMICtrl_PTO3_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_PTO4_DeviceIndication_DeviceIndication Rte_Write_PowerTakeOff_HMICtrl_PTO4_DeviceIndication_DeviceIndication
#  define Rte_Write_PowerTakeOff_HMICtrl_PTO4_DeviceIndication_DeviceIndication(data) (Rte_PowerTakeOff_HMICtrl_PTO4_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Pto1CabRequest_Pto1CabRequest Rte_Write_PowerTakeOff_HMICtrl_Pto1CabRequest_Pto1CabRequest
#  define Rte_Write_Pto2CabRequest_Pto2CabRequest Rte_Write_PowerTakeOff_HMICtrl_Pto2CabRequest_Pto2CabRequest
#  define Rte_Write_Pto3CabRequest_Pto3CabRequest Rte_Write_PowerTakeOff_HMICtrl_Pto3CabRequest_Pto3CabRequest
#  define Rte_Write_Pto4CabRequest_Pto4CabRequest Rte_Write_PowerTakeOff_HMICtrl_Pto4CabRequest_Pto4CabRequest


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BD2_PTO_EmergencyDeactivationTimer_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PTO_EmergencyFilteringTimer_P1BD3_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BD3_PTO_EmergencyFilteringTimer_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PTO_RequestFilteringTimer_P1BD4_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BD4_PTO_RequestFilteringTimer_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1BD2_PTO_EmergencyDeactivationTimer_v() (Rte_AddrPar_0x2B_P1BD2_PTO_EmergencyDeactivationTimer_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1BD3_PTO_EmergencyFilteringTimer_v() (Rte_AddrPar_0x2B_P1BD3_PTO_EmergencyFilteringTimer_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1BD4_PTO_RequestFilteringTimer_v() (Rte_AddrPar_0x2B_P1BD4_PTO_RequestFilteringTimer_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AJL_PTO1_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AJM_PTO2_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1B9X_WirelessRC_Enable_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1BBG_PTO3_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1BBH_PTO4_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1AJL_PTO1_Act_v() (Rte_AddrPar_0x2B_and_0x37_P1AJL_PTO1_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1AJM_PTO2_Act_v() (Rte_AddrPar_0x2B_and_0x37_P1AJM_PTO2_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1B9X_WirelessRC_Enable_v() (Rte_AddrPar_0x2B_and_0x37_P1B9X_WirelessRC_Enable_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1BBG_PTO3_Act_v() (Rte_AddrPar_0x2B_and_0x37_P1BBG_PTO3_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1BBH_PTO4_Act_v() (Rte_AddrPar_0x2B_and_0x37_P1BBH_PTO4_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define PowerTakeOff_HMICtrl_START_SEC_CODE
# include "PowerTakeOff_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_PowerTakeOff_HMICtrl_20ms_runnable PowerTakeOff_HMICtrl_20ms_runnable
#  define RTE_RUNNABLE_PowerTakeOff_HMICtrl_init PowerTakeOff_HMICtrl_init
# endif

FUNC(void, PowerTakeOff_HMICtrl_CODE) PowerTakeOff_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, PowerTakeOff_HMICtrl_CODE) PowerTakeOff_HMICtrl_init(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define PowerTakeOff_HMICtrl_STOP_SEC_CODE
# include "PowerTakeOff_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_POWERTAKEOFF_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
