/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_LINMgr.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <LINMgr>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_LINMGR_H
# define _RTE_LINMGR_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_LINMgr_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN1_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN2_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN3_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN4_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN5_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(uint8, RTE_VAR_NOINIT) Rte_Cdd_LinDiagnostics_LinDiagRequestFlag_CCNADRequest; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(uint8, RTE_VAR_NOINIT) Rte_Cdd_LinDiagnostics_LinDiagRequestFlag_PNSNRequest; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_LIN_SwcActivation_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleMode_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ComMode_LIN1_ComMode_LIN (7U)
#  define Rte_InitValue_ComMode_LIN2_ComMode_LIN (7U)
#  define Rte_InitValue_ComMode_LIN3_ComMode_LIN (7U)
#  define Rte_InitValue_ComMode_LIN4_ComMode_LIN (7U)
#  define Rte_InitValue_ComMode_LIN5_ComMode_LIN (7U)
#  define Rte_InitValue_ComMode_LIN6_ComMode_LIN (7U)
#  define Rte_InitValue_ComMode_LIN7_ComMode_LIN (7U)
#  define Rte_InitValue_ComMode_LIN8_ComMode_LIN (7U)
#  define Rte_InitValue_DiagInfoILCP1_DiagInfo (0U)
#  define Rte_InitValue_DiagInfoTCP_DiagInfo (0U)
#  define Rte_InitValue_LinDiagRequestFlag_CCNADRequest (0U)
#  define Rte_InitValue_LinDiagRequestFlag_PNSNRequest (0U)
#  define Rte_InitValue_Request_LIN6_ScheduleTableRequestMode_requestedMode (0U)
#  define Rte_InitValue_Request_LIN7_ScheduleTableRequestMode_requestedMode (0U)
#  define Rte_InitValue_Request_LIN8_ScheduleTableRequestMode_requestedMode (0U)
#  define Rte_InitValue_SwcActivation_LIN_SwcActivation_LIN (1U)
#  define Rte_InitValue_VehicleModeInternal_VehicleMode (15U)
#  define Rte_InitValue_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted (FALSE)
#  define Rte_InitValue_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type (FALSE)
# endif


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Buffers for inter-runnable variables
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_LINMgr_Irv_DID_FCI; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_Irv_LINMgr_Irv_RID_SwitchDetectionTrig; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
# endif /* !defined(RTE_CORE) */


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LINMgr_Request_LIN1_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN1Schedule data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LINMgr_Request_LIN2_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN2Schedule data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LINMgr_Request_LIN3_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN3Schedule data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LINMgr_Request_LIN4_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN4Schedule data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LINMgr_Request_LIN5_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN5Schedule data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LINMgr_Request_LIN6_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN6Schedule data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LINMgr_Request_LIN7_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN7Schedule data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LINMgr_Request_LIN8_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN8Schedule data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_LINMgr_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_LINMgr_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_LINMgr_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_LINMgr_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_LINMgr_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_LINMgr_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_LINMgr_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_LINMgr_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_LinDiagRequestFlag_CCNADRequest Rte_Read_LINMgr_LinDiagRequestFlag_CCNADRequest
#  define Rte_Read_LINMgr_LinDiagRequestFlag_CCNADRequest(data) (*(data) = Rte_Cdd_LinDiagnostics_LinDiagRequestFlag_CCNADRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LinDiagRequestFlag_PNSNRequest Rte_Read_LINMgr_LinDiagRequestFlag_PNSNRequest
#  define Rte_Read_LINMgr_LinDiagRequestFlag_PNSNRequest(data) (*(data) = Rte_Cdd_LinDiagnostics_LinDiagRequestFlag_PNSNRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_LIN_SwcActivation_LIN Rte_Read_LINMgr_SwcActivation_LIN_SwcActivation_LIN
#  define Rte_Read_LINMgr_SwcActivation_LIN_SwcActivation_LIN(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_LIN_SwcActivation_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_VehicleModeInternal_VehicleMode Rte_Read_LINMgr_VehicleModeInternal_VehicleMode
#  define Rte_Read_LINMgr_VehicleModeInternal_VehicleMode(data) (*(data) = Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted Rte_Read_LINMgr_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted
#  define Rte_Read_LINMgr_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type Rte_Read_LINMgr_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type
#  define Rte_Read_LINMgr_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_ComMode_LIN1_ComMode_LIN Rte_Write_LINMgr_ComMode_LIN1_ComMode_LIN
#  define Rte_Write_LINMgr_ComMode_LIN1_ComMode_LIN(data) (Rte_LINMgr_ComMode_LIN1_ComMode_LIN = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ComMode_LIN2_ComMode_LIN Rte_Write_LINMgr_ComMode_LIN2_ComMode_LIN
#  define Rte_Write_LINMgr_ComMode_LIN2_ComMode_LIN(data) (Rte_LINMgr_ComMode_LIN2_ComMode_LIN = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ComMode_LIN3_ComMode_LIN Rte_Write_LINMgr_ComMode_LIN3_ComMode_LIN
#  define Rte_Write_LINMgr_ComMode_LIN3_ComMode_LIN(data) (Rte_LINMgr_ComMode_LIN3_ComMode_LIN = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ComMode_LIN4_ComMode_LIN Rte_Write_LINMgr_ComMode_LIN4_ComMode_LIN
#  define Rte_Write_LINMgr_ComMode_LIN4_ComMode_LIN(data) (Rte_LINMgr_ComMode_LIN4_ComMode_LIN = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ComMode_LIN5_ComMode_LIN Rte_Write_LINMgr_ComMode_LIN5_ComMode_LIN
#  define Rte_Write_LINMgr_ComMode_LIN5_ComMode_LIN(data) (Rte_LINMgr_ComMode_LIN5_ComMode_LIN = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ComMode_LIN6_ComMode_LIN Rte_Write_LINMgr_ComMode_LIN6_ComMode_LIN
#  define Rte_Write_LINMgr_ComMode_LIN6_ComMode_LIN(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ComMode_LIN7_ComMode_LIN Rte_Write_LINMgr_ComMode_LIN7_ComMode_LIN
#  define Rte_Write_LINMgr_ComMode_LIN7_ComMode_LIN(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ComMode_LIN8_ComMode_LIN Rte_Write_LINMgr_ComMode_LIN8_ComMode_LIN
#  define Rte_Write_LINMgr_ComMode_LIN8_ComMode_LIN(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Request_LIN1_ScheduleTableRequestMode_requestedMode Rte_Write_LINMgr_Request_LIN1_ScheduleTableRequestMode_requestedMode
#  define Rte_Write_Request_LIN2_ScheduleTableRequestMode_requestedMode Rte_Write_LINMgr_Request_LIN2_ScheduleTableRequestMode_requestedMode
#  define Rte_Write_Request_LIN3_ScheduleTableRequestMode_requestedMode Rte_Write_LINMgr_Request_LIN3_ScheduleTableRequestMode_requestedMode
#  define Rte_Write_Request_LIN4_ScheduleTableRequestMode_requestedMode Rte_Write_LINMgr_Request_LIN4_ScheduleTableRequestMode_requestedMode
#  define Rte_Write_Request_LIN5_ScheduleTableRequestMode_requestedMode Rte_Write_LINMgr_Request_LIN5_ScheduleTableRequestMode_requestedMode
#  define Rte_Write_Request_LIN6_ScheduleTableRequestMode_requestedMode Rte_Write_LINMgr_Request_LIN6_ScheduleTableRequestMode_requestedMode
#  define Rte_Write_Request_LIN7_ScheduleTableRequestMode_requestedMode Rte_Write_LINMgr_Request_LIN7_ScheduleTableRequestMode_requestedMode
#  define Rte_Write_Request_LIN8_ScheduleTableRequestMode_requestedMode Rte_Write_LINMgr_Request_LIN8_ScheduleTableRequestMode_requestedMode


/**********************************************************************************************************************
 * Rte_Mode_<p>_<m>
 *********************************************************************************************************************/
#  define Rte_Mode_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule Rte_Mode_LINMgr_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule
#  define Rte_Mode_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule Rte_Mode_LINMgr_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule
#  define Rte_Mode_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule Rte_Mode_LINMgr_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule
#  define Rte_Mode_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule Rte_Mode_LINMgr_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule
#  define Rte_Mode_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule Rte_Mode_LINMgr_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule
#  define Rte_Mode_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule Rte_Mode_LINMgr_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule
#  define Rte_Mode_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule Rte_Mode_LINMgr_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule
#  define Rte_Mode_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule Rte_Mode_LINMgr_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDo12VActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Do12VPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS Do12VInterface_P_GetDo12VPinsState_CS
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1BJO_88_LIN1busCIOM_BusOff_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)198, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BJP_88_LIN2busCIOM_BusOff_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)199, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BJQ_88_LIN3busCIOM_BusOff_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)200, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BJR_88_LIN4busCIOM_BusOff_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)201, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BJS_88_LIN5busCIOM_BusOff_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)202, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_COMM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_COMM_APPL_CODE) ComM_GetCurrentComMode(ComM_UserHandleType parg0, P2VAR(ComM_ModeType, AUTOMATIC, RTE_COMM_APPL_VAR) ComMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_COMM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_CN_LIN00_ace1a6ba_GetCurrentComMode(arg1) (ComM_GetCurrentComMode((ComM_UserHandleType)6, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_CN_LIN01_4323cd84_GetCurrentComMode(arg1) (ComM_GetCurrentComMode((ComM_UserHandleType)7, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_CN_LIN02_a8147687_GetCurrentComMode(arg1) (ComM_GetCurrentComMode((ComM_UserHandleType)8, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_CN_LIN03_47d61db9_GetCurrentComMode(arg1) (ComM_GetCurrentComMode((ComM_UserHandleType)9, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_CN_LIN04_a50a06c0_GetCurrentComMode(arg1) (ComM_GetCurrentComMode((ComM_UserHandleType)10, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_CN_LIN05_4ac86dfe_GetCurrentComMode(arg1) (ComM_GetCurrentComMode((ComM_UserHandleType)11, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_CN_LIN06_a1ffd6fd_GetCurrentComMode(arg1) (ComM_GetCurrentComMode((ComM_UserHandleType)12, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_CN_LIN07_4e3dbdc3_GetCurrentComMode(arg1) (ComM_GetCurrentComMode((ComM_UserHandleType)13, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DataServices_P1CXF_Data_P1CXF_FCI_ReadData_Irv_DID_FCI() \
  Rte_Irv_LINMgr_Irv_DID_FCI
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_RoutineServices_R1AAJ_FlexibleSwitchDetection_Start_Irv_RID_SwitchDetectionTrig() \
  Rte_Irv_LINMgr_Irv_RID_SwitchDetectionTrig
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_RoutineServices_R1AAJ_FlexibleSwitchDetection_Start_Irv_RID_SwitchDetectionTrig(data) \
  (Rte_Irv_LINMgr_Irv_RID_SwitchDetectionTrig = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_SCIM_LINMgr_20ms_runnable_Irv_DID_FCI() \
  Rte_Irv_LINMgr_Irv_DID_FCI
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_SCIM_LINMgr_20ms_runnable_Irv_RID_SwitchDetectionTrig() \
  Rte_Irv_LINMgr_Irv_RID_SwitchDetectionTrig
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_SCIM_LINMgr_20ms_runnable_Irv_DID_FCI(data) \
  (Rte_Irv_LINMgr_Irv_DID_FCI = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_SCIM_LINMgr_20ms_runnable_Irv_RID_SwitchDetectionTrig(data) \
  (Rte_Irv_LINMgr_Irv_RID_SwitchDetectionTrig = (data))
/* PRQA L:L1 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX0_PcbConfig_LinInterfaces_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v() (&(Rte_AddrPar_0x29_X1CX0_PcbConfig_LinInterfaces_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v() (&Rte_AddrPar_0x29_X1CX0_PcbConfig_LinInterfaces_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_LIN_topology_P1AJR_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1AJR_LIN_topology_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1AJR_LIN_topology_v() (Rte_AddrPar_0x2B_P1AJR_LIN_topology_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1WPP_isSecurityLinActive_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1WPP_isSecurityLinActive_v() (Rte_AddrPar_0x2F_P1WPP_isSecurityLinActive_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define LINMgr_START_SEC_CODE
# include "LINMgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_P1CXF_Data_P1CXF_FCI_ReadData DataServices_P1CXF_Data_P1CXF_FCI_ReadData
#  define RTE_RUNNABLE_Issm_IssStateChange_Issm_IssActivation Issm_IssStateChange_Issm_IssActivation
#  define RTE_RUNNABLE_Issm_IssStateChange_Issm_IssDeactivation Issm_IssStateChange_Issm_IssDeactivation
#  define RTE_RUNNABLE_RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults
#  define RTE_RUNNABLE_RoutineServices_R1AAJ_FlexibleSwitchDetection_Start RoutineServices_R1AAJ_FlexibleSwitchDetection_Start
#  define RTE_RUNNABLE_RoutineServices_R1AAJ_FlexibleSwitchDetection_Stop RoutineServices_R1AAJ_FlexibleSwitchDetection_Stop
#  define RTE_RUNNABLE_SCIM_LINMgr_20ms_runnable SCIM_LINMgr_20ms_runnable
#  define RTE_RUNNABLE_SCIM_LINMgr_LIN1SchEndNotif SCIM_LINMgr_LIN1SchEndNotif
#  define RTE_RUNNABLE_SCIM_LINMgr_LIN2SchEndNotif SCIM_LINMgr_LIN2SchEndNotif
#  define RTE_RUNNABLE_SCIM_LINMgr_LIN3SchEndNotif SCIM_LINMgr_LIN3SchEndNotif
#  define RTE_RUNNABLE_SCIM_LINMgr_LIN4SchEndNotif SCIM_LINMgr_LIN4SchEndNotif
#  define RTE_RUNNABLE_SCIM_LINMgr_LIN5SchEndNotif SCIM_LINMgr_LIN5SchEndNotif
#  define RTE_RUNNABLE_SCIM_LINMgr_LIN6SchEndNotif SCIM_LINMgr_LIN6SchEndNotif
#  define RTE_RUNNABLE_SCIM_LINMgr_LIN7SchEndNotif SCIM_LINMgr_LIN7SchEndNotif
#  define RTE_RUNNABLE_SCIM_LINMgr_LIN8SchEndNotif SCIM_LINMgr_LIN8SchEndNotif
# endif

# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, LINMgr_CODE) DataServices_P1CXF_Data_P1CXF_FCI_ReadData(P2VAR(uint8, AUTOMATIC, RTE_LINMGR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, LINMgr_CODE) DataServices_P1CXF_Data_P1CXF_FCI_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_LINMGR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, LINMgr_CODE) Issm_IssStateChange_Issm_IssActivation(Issm_IssHandleType issID); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, LINMgr_CODE) Issm_IssStateChange_Issm_IssDeactivation(Issm_IssHandleType issID); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, LINMgr_CODE) RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_LINMGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_LINMGR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, LINMgr_CODE) RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_LINMGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_LINMGR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, LINMgr_CODE) RoutineServices_R1AAJ_FlexibleSwitchDetection_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_LINMGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_LINMGR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, LINMgr_CODE) RoutineServices_R1AAJ_FlexibleSwitchDetection_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_LINMGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_LINMGR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, LINMgr_CODE) RoutineServices_R1AAJ_FlexibleSwitchDetection_Stop(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_LINMGR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, LINMgr_CODE) SCIM_LINMgr_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN1SchEndNotif(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN2SchEndNotif(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN3SchEndNotif(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN4SchEndNotif(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN5SchEndNotif(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN6SchEndNotif(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN7SchEndNotif(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN8SchEndNotif(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define LINMgr_STOP_SEC_CODE
# include "LINMgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_ComM_UserRequest_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1CXF_Data_P1CXF_E_NOT_OK (1U)

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)

#  define RTE_E_Do12VInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_RoutineServices_R1AAJ_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_R1AAJ_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_R1AAJ_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_LINMGR_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
