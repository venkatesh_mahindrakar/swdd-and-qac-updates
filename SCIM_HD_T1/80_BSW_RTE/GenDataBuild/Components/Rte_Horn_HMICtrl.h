/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Horn_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <Horn_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_HORN_HMICTRL_H
# define _RTE_HORN_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_Horn_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_Horn_HMICtrl_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_Horn_HMICtrl_CityHorn_HMI_rqst_CityHorn_HMI_rqst; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_AuxHorn_Input_Hdlr_AH_PushButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_CityHorn_Input_Hdlr_CH_PushButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Living_Living; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_AH_PushButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst (3U)
#  define Rte_InitValue_CH_PushButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_CityHorn_HMI_rqst_CityHorn_HMI_rqst (3U)
#  define Rte_InitValue_SwcActivation_Living_Living (1U)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_AH_PushButtonStatus_PushButtonStatus Rte_Read_Horn_HMICtrl_AH_PushButtonStatus_PushButtonStatus
#  define Rte_Read_Horn_HMICtrl_AH_PushButtonStatus_PushButtonStatus(data) (*(data) = Rte_AuxHorn_Input_Hdlr_AH_PushButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_CH_PushButtonStatus_PushButtonStatus Rte_Read_Horn_HMICtrl_CH_PushButtonStatus_PushButtonStatus
#  define Rte_Read_Horn_HMICtrl_CH_PushButtonStatus_PushButtonStatus(data) (*(data) = Rte_CityHorn_Input_Hdlr_CH_PushButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_Living_Living Rte_Read_Horn_HMICtrl_SwcActivation_Living_Living
#  define Rte_Read_Horn_HMICtrl_SwcActivation_Living_Living(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Living_Living, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst Rte_Write_Horn_HMICtrl_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst
#  define Rte_Write_Horn_HMICtrl_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst(data) (Rte_Horn_HMICtrl_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_CityHorn_HMI_rqst_CityHorn_HMI_rqst Rte_Write_Horn_HMICtrl_CityHorn_HMI_rqst_CityHorn_HMI_rqst
#  define Rte_Write_Horn_HMICtrl_CityHorn_HMI_rqst_CityHorn_HMI_rqst(data) (Rte_Horn_HMICtrl_CityHorn_HMI_rqst_CityHorn_HMI_rqst = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1A1T_CityHorn_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1A1U_AuxiliaryHorn_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1A1V_HornLivingMode_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1A1T_CityHorn_Act_v() (Rte_AddrPar_0x2B_and_0x37_P1A1T_CityHorn_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1A1U_AuxiliaryHorn_Act_v() (Rte_AddrPar_0x2B_and_0x37_P1A1U_AuxiliaryHorn_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1A1V_HornLivingMode_Act_v() (Rte_AddrPar_0x2B_and_0x37_P1A1V_HornLivingMode_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define Horn_HMICtrl_START_SEC_CODE
# include "Horn_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_Horn_HMICtrl_20ms_runnable Horn_HMICtrl_20ms_runnable
# endif

FUNC(void, Horn_HMICtrl_CODE) Horn_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define Horn_HMICtrl_STOP_SEC_CODE
# include "Horn_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_HORN_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
