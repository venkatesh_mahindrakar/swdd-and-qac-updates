/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_DriverAuthentication2_Ctrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <DriverAuthentication2_Ctrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_DRIVERAUTHENTICATION2_CTRL_H
# define _RTE_DRIVERAUTHENTICATION2_CTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_DriverAuthentication2_Ctrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Boolean, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_EngineStartAuth_stat_CryptTrig_CryptoTrigger; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_GrbxUnlockAuth_stat_CryptTrig_CryptoTrigger; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_KeyAuth_stat_CryptTrig_CryptoTrigger; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(KeyAuthentication_stat_decrypt_T, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(KeyfobAuth_rqst_T, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_KeyfobAuth_rqst_KeyfobAuth_rqst; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PinCode_rqst_T, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_PinCode_rqst_PinCode_rqst; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceAuthentication_rqst_T, RTE_VAR_NOINIT) Rte_AuthenticationDevice_UICtrl_DeviceAuthentication_rqst_DeviceAuthentication_rqst; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DoorsAjar_stat_T, RTE_VAR_NOINIT) Rte_DoorsAjar_stat_ISig_4_oCIOM_BB2_06P_oBackbone2_8810fc26_Tx; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(KeyfobAuth_stat_T, RTE_VAR_NOINIT) Rte_Keyfob_Mgr_KeyfobAuth_stat_KeyfobAuth_stat; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PinCode_stat_T, RTE_VAR_NOINIT) Rte_PinCode_ctrl_PinCode_stat_PinCode_stat; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleMode_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_DeviceAuthentication_rqst_DeviceAuthentication_rqst (7U)
#  define Rte_InitValue_DeviceInCab_stat_DeviceInCab_stat (3U)
#  define Rte_InitValue_DoorsAjar_stat_DoorsAjar_stat (7U)
#  define Rte_InitValue_EngineStartAuth_rqst_EngineStartAuth_rqst (7U)
#  define Rte_InitValue_EngineStartAuth_stat_CryptTrig_CryptoTrigger (FALSE)
#  define Rte_InitValue_EngineStartAuth_stat_decrypt_EngineStartAuth_stat_decrypt (7U)
#  define Rte_InitValue_GearBoxUnlockAuth_rqst_GearBoxUnlockAuth_rqst (3U)
#  define Rte_InitValue_GearboxUnlockAuth_stat_decrypt_GearboxUnlockAuth_stat_decrypt (3U)
#  define Rte_InitValue_GrbxUnlockAuth_stat_CryptTrig_CryptoTrigger (FALSE)
#  define Rte_InitValue_KeyAuth_stat_CryptTrig_CryptoTrigger (FALSE)
#  define Rte_InitValue_KeyAuthentication_rqst_KeyAuthentication_rqst (7U)
#  define Rte_InitValue_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt (7U)
#  define Rte_InitValue_KeyNotValid_KeyNotValid (7U)
#  define Rte_InitValue_KeyfobAuth_rqst_KeyfobAuth_rqst (7U)
#  define Rte_InitValue_KeyfobAuth_stat_KeyfobAuth_stat (7U)
#  define Rte_InitValue_PinCode_rqst_PinCode_rqst (7U)
#  define Rte_InitValue_PinCode_stat_PinCode_stat (7U)
#  define Rte_InitValue_SwcActivation_Security_SwcActivation_Security (1U)
#  define Rte_InitValue_VIN_rqst_VIN_rqst (FALSE)
#  define Rte_InitValue_VehicleModeInternal_VehicleMode (15U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Receive_DriverAuthentication2_Ctrl_ECU1VIN_stat_VIN_stat(P2VAR(uint8, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Receive_DriverAuthentication2_Ctrl_ECU1VIN_stat_VIN_stat(P2VAR(VIN_stat_T, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Receive_DriverAuthentication2_Ctrl_ECU2VIN_stat_VIN_stat(P2VAR(uint8, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Receive_DriverAuthentication2_Ctrl_ECU2VIN_stat_VIN_stat(P2VAR(VIN_stat_T, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Receive_DriverAuthentication2_Ctrl_ECU3VIN_stat_VIN_stat(P2VAR(uint8, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Receive_DriverAuthentication2_Ctrl_ECU3VIN_stat_VIN_stat(P2VAR(VIN_stat_T, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Receive_DriverAuthentication2_Ctrl_ECU4VIN_stat_VIN_stat(P2VAR(uint8, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Receive_DriverAuthentication2_Ctrl_ECU4VIN_stat_VIN_stat(P2VAR(VIN_stat_T, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Receive_DriverAuthentication2_Ctrl_ECU5VIN_stat_VIN_stat(P2VAR(uint8, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Receive_DriverAuthentication2_Ctrl_ECU5VIN_stat_VIN_stat(P2VAR(VIN_stat_T, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_DriverAuthentication2_Ctrl_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM(P2VAR(uint8, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_DriverAuthentication2_Ctrl_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM(P2VAR(StandardNVM_T, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_DriverAuthentication2_Ctrl_EngineStartAuth_rqst_EngineStartAuth_rqst(P2VAR(EngineStartAuth_rqst_T, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_DriverAuthentication2_Ctrl_GearBoxUnlockAuth_rqst_GearBoxUnlockAuth_rqst(P2VAR(GearBoxUnlockAuth_rqst_T, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_DriverAuthentication2_Ctrl_KeyAuthentication_rqst_KeyAuthentication_rqst(P2VAR(KeyAuthentication_rqst_T, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DriverAuthentication2_Ctrl_DeviceInCab_stat_DeviceInCab_stat(DeviceInCab_stat_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DriverAuthentication2_Ctrl_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM(P2CONST(uint8, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DriverAuthentication2_Ctrl_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM(P2CONST(StandardNVM_T, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DriverAuthentication2_Ctrl_EngineStartAuth_st_serialized_Crypto_Function_serialized(P2CONST(uint8, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DriverAuthentication2_Ctrl_EngineStartAuth_st_serialized_Crypto_Function_serialized(P2CONST(Crypto_Function_serialized_T, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DriverAuthentication2_Ctrl_GrbxUnlockAuth_stat_serialized_Crypto_Function_serialized(P2CONST(uint8, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DriverAuthentication2_Ctrl_GrbxUnlockAuth_stat_serialized_Crypto_Function_serialized(P2CONST(Crypto_Function_serialized_T, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DriverAuthentication2_Ctrl_KeyAuth_stat_serialized_Crypto_Function_serialized(P2CONST(uint8, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DriverAuthentication2_Ctrl_KeyAuth_stat_serialized_Crypto_Function_serialized(P2CONST(Crypto_Function_serialized_T, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DriverAuthentication2_Ctrl_KeyNotValid_KeyNotValid(KeyNotValid_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DriverAuthentication2_Ctrl_VIN_rqst_VIN_rqst(VIN_rqst_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, RTE_CODE) Rte_IrvRead_DriverAuthentication2_Ctrl_DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData_Vincheckstatus(P2VAR(VINCheckStatus_T, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, RTE_CODE) Rte_IrvWrite_DriverAuthentication2_Ctrl_DriverAuthentication2_Ctrl_20ms_runnable_Vincheckstatus(P2CONST(VINCheckStatus_T, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Receive_<p>_<d> (explicit S/R communication with isQueued = true)
 *********************************************************************************************************************/
#  define Rte_Receive_ECU1VIN_stat_VIN_stat Rte_Receive_DriverAuthentication2_Ctrl_ECU1VIN_stat_VIN_stat
#  define Rte_Receive_ECU2VIN_stat_VIN_stat Rte_Receive_DriverAuthentication2_Ctrl_ECU2VIN_stat_VIN_stat
#  define Rte_Receive_ECU3VIN_stat_VIN_stat Rte_Receive_DriverAuthentication2_Ctrl_ECU3VIN_stat_VIN_stat
#  define Rte_Receive_ECU4VIN_stat_VIN_stat Rte_Receive_DriverAuthentication2_Ctrl_ECU4VIN_stat_VIN_stat
#  define Rte_Receive_ECU5VIN_stat_VIN_stat Rte_Receive_DriverAuthentication2_Ctrl_ECU5VIN_stat_VIN_stat


/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_DeviceAuthentication_rqst_DeviceAuthentication_rqst Rte_Read_DriverAuthentication2_Ctrl_DeviceAuthentication_rqst_DeviceAuthentication_rqst
#  define Rte_Read_DriverAuthentication2_Ctrl_DeviceAuthentication_rqst_DeviceAuthentication_rqst(data) (*(data) = Rte_AuthenticationDevice_UICtrl_DeviceAuthentication_rqst_DeviceAuthentication_rqst, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DoorsAjar_stat_DoorsAjar_stat Rte_Read_DriverAuthentication2_Ctrl_DoorsAjar_stat_DoorsAjar_stat
#  define Rte_Read_DriverAuthentication2_Ctrl_DoorsAjar_stat_DoorsAjar_stat(data) (*(data) = Rte_DoorsAjar_stat_ISig_4_oCIOM_BB2_06P_oBackbone2_8810fc26_Tx, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM Rte_Read_DriverAuthentication2_Ctrl_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM
#  define Rte_Read_EngineStartAuth_rqst_EngineStartAuth_rqst Rte_Read_DriverAuthentication2_Ctrl_EngineStartAuth_rqst_EngineStartAuth_rqst
#  define Rte_Read_GearBoxUnlockAuth_rqst_GearBoxUnlockAuth_rqst Rte_Read_DriverAuthentication2_Ctrl_GearBoxUnlockAuth_rqst_GearBoxUnlockAuth_rqst
#  define Rte_Read_KeyAuthentication_rqst_KeyAuthentication_rqst Rte_Read_DriverAuthentication2_Ctrl_KeyAuthentication_rqst_KeyAuthentication_rqst
#  define Rte_Read_KeyfobAuth_stat_KeyfobAuth_stat Rte_Read_DriverAuthentication2_Ctrl_KeyfobAuth_stat_KeyfobAuth_stat
#  define Rte_Read_DriverAuthentication2_Ctrl_KeyfobAuth_stat_KeyfobAuth_stat(data) (*(data) = Rte_Keyfob_Mgr_KeyfobAuth_stat_KeyfobAuth_stat, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_PinCode_stat_PinCode_stat Rte_Read_DriverAuthentication2_Ctrl_PinCode_stat_PinCode_stat
#  define Rte_Read_DriverAuthentication2_Ctrl_PinCode_stat_PinCode_stat(data) (*(data) = Rte_PinCode_ctrl_PinCode_stat_PinCode_stat, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_Security_SwcActivation_Security Rte_Read_DriverAuthentication2_Ctrl_SwcActivation_Security_SwcActivation_Security
#  define Rte_Read_DriverAuthentication2_Ctrl_SwcActivation_Security_SwcActivation_Security(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_VehicleModeInternal_VehicleMode Rte_Read_DriverAuthentication2_Ctrl_VehicleModeInternal_VehicleMode
#  define Rte_Read_DriverAuthentication2_Ctrl_VehicleModeInternal_VehicleMode(data) (*(data) = Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_DeviceInCab_stat_DeviceInCab_stat Rte_Write_DriverAuthentication2_Ctrl_DeviceInCab_stat_DeviceInCab_stat
#  define Rte_Write_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM Rte_Write_DriverAuthentication2_Ctrl_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM
#  define Rte_Write_EngineStartAuth_st_serialized_Crypto_Function_serialized Rte_Write_DriverAuthentication2_Ctrl_EngineStartAuth_st_serialized_Crypto_Function_serialized
#  define Rte_Write_EngineStartAuth_stat_CryptTrig_CryptoTrigger Rte_Write_DriverAuthentication2_Ctrl_EngineStartAuth_stat_CryptTrig_CryptoTrigger
#  define Rte_Write_DriverAuthentication2_Ctrl_EngineStartAuth_stat_CryptTrig_CryptoTrigger(data) (Rte_DriverAuthentication2_Ctrl_EngineStartAuth_stat_CryptTrig_CryptoTrigger = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_EngineStartAuth_stat_decrypt_EngineStartAuth_stat_decrypt Rte_Write_DriverAuthentication2_Ctrl_EngineStartAuth_stat_decrypt_EngineStartAuth_stat_decrypt
#  define Rte_Write_DriverAuthentication2_Ctrl_EngineStartAuth_stat_decrypt_EngineStartAuth_stat_decrypt(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_GearboxUnlockAuth_stat_decrypt_GearboxUnlockAuth_stat_decrypt Rte_Write_DriverAuthentication2_Ctrl_GearboxUnlockAuth_stat_decrypt_GearboxUnlockAuth_stat_decrypt
#  define Rte_Write_DriverAuthentication2_Ctrl_GearboxUnlockAuth_stat_decrypt_GearboxUnlockAuth_stat_decrypt(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_GrbxUnlockAuth_stat_CryptTrig_CryptoTrigger Rte_Write_DriverAuthentication2_Ctrl_GrbxUnlockAuth_stat_CryptTrig_CryptoTrigger
#  define Rte_Write_DriverAuthentication2_Ctrl_GrbxUnlockAuth_stat_CryptTrig_CryptoTrigger(data) (Rte_DriverAuthentication2_Ctrl_GrbxUnlockAuth_stat_CryptTrig_CryptoTrigger = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_GrbxUnlockAuth_stat_serialized_Crypto_Function_serialized Rte_Write_DriverAuthentication2_Ctrl_GrbxUnlockAuth_stat_serialized_Crypto_Function_serialized
#  define Rte_Write_KeyAuth_stat_CryptTrig_CryptoTrigger Rte_Write_DriverAuthentication2_Ctrl_KeyAuth_stat_CryptTrig_CryptoTrigger
#  define Rte_Write_DriverAuthentication2_Ctrl_KeyAuth_stat_CryptTrig_CryptoTrigger(data) (Rte_DriverAuthentication2_Ctrl_KeyAuth_stat_CryptTrig_CryptoTrigger = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_KeyAuth_stat_serialized_Crypto_Function_serialized Rte_Write_DriverAuthentication2_Ctrl_KeyAuth_stat_serialized_Crypto_Function_serialized
#  define Rte_Write_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt Rte_Write_DriverAuthentication2_Ctrl_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt
#  define Rte_Write_DriverAuthentication2_Ctrl_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt(data) (Rte_DriverAuthentication2_Ctrl_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_KeyNotValid_KeyNotValid Rte_Write_DriverAuthentication2_Ctrl_KeyNotValid_KeyNotValid
#  define Rte_Write_KeyfobAuth_rqst_KeyfobAuth_rqst Rte_Write_DriverAuthentication2_Ctrl_KeyfobAuth_rqst_KeyfobAuth_rqst
#  define Rte_Write_DriverAuthentication2_Ctrl_KeyfobAuth_rqst_KeyfobAuth_rqst(data) (Rte_DriverAuthentication2_Ctrl_KeyfobAuth_rqst_KeyfobAuth_rqst = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_PinCode_rqst_PinCode_rqst Rte_Write_DriverAuthentication2_Ctrl_PinCode_rqst_PinCode_rqst
#  define Rte_Write_DriverAuthentication2_Ctrl_PinCode_rqst_PinCode_rqst(data) (Rte_DriverAuthentication2_Ctrl_PinCode_rqst_PinCode_rqst = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_VIN_rqst_VIN_rqst Rte_Write_DriverAuthentication2_Ctrl_VIN_rqst_VIN_rqst


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_ActivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_ImmobilizerPINCode_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)26)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_DeactivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_ImmobilizerPINCode_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)26)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData_Vincheckstatus(data) \
  Rte_IrvRead_DriverAuthentication2_Ctrl_DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData_Vincheckstatus(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DriverAuthentication2_Ctrl_20ms_runnable_Vincheckstatus(data) \
  Rte_IrvWrite_DriverAuthentication2_Ctrl_DriverAuthentication2_Ctrl_20ms_runnable_Vincheckstatus(data)
/* PRQA L:L1 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VKI_PassiveStart_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_VINCheckProcessing_P1VKG_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VKG_VINCheckProcessing_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1VKI_PassiveStart_Installed_v() (Rte_AddrPar_0x2B_P1VKI_PassiveStart_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1VKG_VINCheckProcessing_v() (&Rte_AddrPar_0x2B_P1VKG_VINCheckProcessing_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2D_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2D) Rte_AddrPar_0x2D_P1TTA_GearBoxLockActivation_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2D_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1TTA_GearBoxLockActivation_v() (Rte_AddrPar_0x2D_P1TTA_GearBoxLockActivation_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2D_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_CrankingLockActivation_P1DS3_T, RTE_CONST_SA_lvl_0x2D_and_0x37) Rte_AddrPar_0x2D_and_0x37_P1DS3_CrankingLockActivation_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2D_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1DS3_CrankingLockActivation_v() (Rte_AddrPar_0x2D_and_0x37_P1DS3_CrankingLockActivation_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_ChassisId_CHANO_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_CHANO_ChassisId_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_CHANO_ChassisId_v() (&(Rte_AddrPar_0x2F_CHANO_ChassisId_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_CHANO_ChassisId_v() (&Rte_AddrPar_0x2F_CHANO_ChassisId_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

# endif /* !defined(RTE_CORE) */


# define DriverAuthentication2_Ctrl_START_SEC_CODE
# include "DriverAuthentication2_Ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData
#  define RTE_RUNNABLE_DriverAuthentication2_Ctrl_20ms_runnable DriverAuthentication2_Ctrl_20ms_runnable
# endif

# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DriverAuthentication2_Ctrl_CODE) DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DriverAuthentication2_Ctrl_CODE) DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData(P2VAR(Dcm_Data5ByteType, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, DriverAuthentication2_Ctrl_CODE) DriverAuthentication2_Ctrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define DriverAuthentication2_Ctrl_STOP_SEC_CODE
# include "DriverAuthentication2_Ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_P1VKH_Data_P1VKH_E_NOT_OK (1U)

#  define RTE_E_Issm_IssRequest_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_DRIVERAUTHENTICATION2_CTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
