/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_RoofHatch_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <RoofHatch_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_ROOFHATCH_HMICTRL_H
# define _RTE_ROOFHATCH_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_RoofHatch_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_DoubleSwitch_HMICtrl_SwitchStatus_combined_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Living_Living; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_BunkH1RoofhatchCloseBtn_Stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH1RoofhatchOpenBtn_Stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus (3U)
#  define Rte_InitValue_RoofHatch_HMI_rqst_RoofHatch_HMI_rqst (7U)
#  define Rte_InitValue_RoofHatch_SwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_SwcActivation_Living_Living (1U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_RoofHatch_HMICtrl_BunkH1RoofhatchCloseBtn_Stat_PushButtonStatus(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_ROOFHATCH_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_RoofHatch_HMICtrl_BunkH1RoofhatchOpenBtn_Stat_PushButtonStatus(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_ROOFHATCH_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_RoofHatch_HMICtrl_RoofHatch_HMI_rqst_RoofHatch_HMI_rqst(RoofHatch_HMI_rqst_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_BunkH1RoofhatchCloseBtn_Stat_PushButtonStatus Rte_Read_RoofHatch_HMICtrl_BunkH1RoofhatchCloseBtn_Stat_PushButtonStatus
#  define Rte_Read_BunkH1RoofhatchOpenBtn_Stat_PushButtonStatus Rte_Read_RoofHatch_HMICtrl_BunkH1RoofhatchOpenBtn_Stat_PushButtonStatus
#  define Rte_Read_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus Rte_Read_RoofHatch_HMICtrl_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus
#  define Rte_Read_RoofHatch_HMICtrl_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus(data) (*(data) = Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus Rte_Read_RoofHatch_HMICtrl_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus
#  define Rte_Read_RoofHatch_HMICtrl_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus(data) (*(data) = Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RoofHatch_SwitchStatus_A3PosSwitchStatus Rte_Read_RoofHatch_HMICtrl_RoofHatch_SwitchStatus_A3PosSwitchStatus
#  define Rte_Read_RoofHatch_HMICtrl_RoofHatch_SwitchStatus_A3PosSwitchStatus(data) (*(data) = Rte_DoubleSwitch_HMICtrl_SwitchStatus_combined_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_Living_Living Rte_Read_RoofHatch_HMICtrl_SwcActivation_Living_Living
#  define Rte_Read_RoofHatch_HMICtrl_SwcActivation_Living_Living(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Living_Living, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_RoofHatch_HMI_rqst_RoofHatch_HMI_rqst Rte_Write_RoofHatch_HMICtrl_RoofHatch_HMI_rqst_RoofHatch_HMI_rqst


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1GCD_RoofHatch_FlexibleSwitchLogic_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CW8_RoofHatchCtrl_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1GCD_RoofHatch_FlexibleSwitchLogic_v() (Rte_AddrPar_0x2B_P1GCD_RoofHatch_FlexibleSwitchLogic_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CW8_RoofHatchCtrl_Act_v() (Rte_AddrPar_0x2B_P1CW8_RoofHatchCtrl_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define RoofHatch_HMICtrl_START_SEC_CODE
# include "RoofHatch_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RoofHatch_HMICtrl_20ms_runnable RoofHatch_HMICtrl_20ms_runnable
#  define RTE_RUNNABLE_RoofHatch_HMICtrl_Init RoofHatch_HMICtrl_Init
# endif

FUNC(void, RoofHatch_HMICtrl_CODE) RoofHatch_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, RoofHatch_HMICtrl_CODE) RoofHatch_HMICtrl_Init(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define RoofHatch_HMICtrl_STOP_SEC_CODE
# include "RoofHatch_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_ROOFHATCH_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
