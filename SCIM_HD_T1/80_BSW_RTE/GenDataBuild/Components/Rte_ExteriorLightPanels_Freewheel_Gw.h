/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_ExteriorLightPanels_Freewheel_Gw.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <ExteriorLightPanels_Freewheel_Gw>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_EXTERIORLIGHTPANELS_FREEWHEEL_GW_H
# define _RTE_EXTERIORLIGHTPANELS_FREEWHEEL_GW_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_ExteriorLightPanels_Freewheel_Gw_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Living_Living; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_LightMode_Status_1_FreeWheel_Status (15U)
#  define Rte_InitValue_LightMode_Status_2_FreeWheel_Status (15U)
#  define Rte_InitValue_LightMode_Status_Ctr_1_Freewheel_Status_Ctr (15U)
#  define Rte_InitValue_LightMode_Status_Ctr_2_Freewheel_Status_Ctr (15U)
#  define Rte_InitValue_SwcActivation_Living_Living (1U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExteriorLightPanels_Freewheel_Gw_LightMode_Status_1_FreeWheel_Status(P2VAR(FreeWheel_Status_T, AUTOMATIC, RTE_EXTERIORLIGHTPANELS_FREEWHEEL_GW_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExteriorLightPanels_Freewheel_Gw_LightMode_Status_2_FreeWheel_Status(P2VAR(FreeWheel_Status_T, AUTOMATIC, RTE_EXTERIORLIGHTPANELS_FREEWHEEL_GW_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanels_Freewheel_Gw_LightMode_Status_Ctr_1_Freewheel_Status_Ctr(Freewheel_Status_Ctr_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanels_Freewheel_Gw_LightMode_Status_Ctr_2_Freewheel_Status_Ctr(Freewheel_Status_Ctr_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_LightMode_Status_1_FreeWheel_Status Rte_Read_ExteriorLightPanels_Freewheel_Gw_LightMode_Status_1_FreeWheel_Status
#  define Rte_Read_LightMode_Status_2_FreeWheel_Status Rte_Read_ExteriorLightPanels_Freewheel_Gw_LightMode_Status_2_FreeWheel_Status
#  define Rte_Read_SwcActivation_Living_Living Rte_Read_ExteriorLightPanels_Freewheel_Gw_SwcActivation_Living_Living
#  define Rte_Read_ExteriorLightPanels_Freewheel_Gw_SwcActivation_Living_Living(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Living_Living, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_IsUpdated_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_IsUpdated_LightMode_Status_1_FreeWheel_Status Rte_IsUpdated_ExteriorLightPanels_Freewheel_Gw_LightMode_Status_1_FreeWheel_Status
#  define Rte_IsUpdated_ExteriorLightPanels_Freewheel_Gw_LightMode_Status_1_FreeWheel_Status() ((Rte_RxUpdateFlags.Rte_RxUpdate_ExteriorLightPanels_Freewheel_Gw_LightMode_Status_1_FreeWheel_Status == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_IsUpdated_LightMode_Status_2_FreeWheel_Status Rte_IsUpdated_ExteriorLightPanels_Freewheel_Gw_LightMode_Status_2_FreeWheel_Status
#  define Rte_IsUpdated_ExteriorLightPanels_Freewheel_Gw_LightMode_Status_2_FreeWheel_Status() ((Rte_RxUpdateFlags.Rte_RxUpdate_ExteriorLightPanels_Freewheel_Gw_LightMode_Status_2_FreeWheel_Status == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_LightMode_Status_Ctr_1_Freewheel_Status_Ctr Rte_Write_ExteriorLightPanels_Freewheel_Gw_LightMode_Status_Ctr_1_Freewheel_Status_Ctr
#  define Rte_Write_LightMode_Status_Ctr_2_Freewheel_Status_Ctr Rte_Write_ExteriorLightPanels_Freewheel_Gw_LightMode_Status_Ctr_2_Freewheel_Status_Ctr


# endif /* !defined(RTE_CORE) */


# define ExteriorLightPanels_Freewheel_Gw_START_SEC_CODE
# include "ExteriorLightPanels_Freewheel_Gw_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_ExteriorLightPanels_Freewheel_Gw_20ms_runnable ExteriorLightPanels_Freewheel_Gw_20ms_runnable
# endif

FUNC(void, ExteriorLightPanels_Freewheel_Gw_CODE) ExteriorLightPanels_Freewheel_Gw_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define ExteriorLightPanels_Freewheel_Gw_STOP_SEC_CODE
# include "ExteriorLightPanels_Freewheel_Gw_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_EXTERIORLIGHTPANELS_FREEWHEEL_GW_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
