/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_RearAxleSteering_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <RearAxleSteering_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_REARAXLESTEERING_HMICTRL_H
# define _RTE_REARAXLESTEERING_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_RearAxleSteering_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_RearAxleSteering_HMICtrl_RearAxleSteeringDeviceInd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_RearAxleSteering_DeviceEvent_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_EngineRun_EngineRun; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ExtraAxleSteeringFunctionStat_RearAxleSteeringFunctionStatus (7U)
#  define Rte_InitValue_RearAxleSteeringDeviceInd_DeviceIndication (3U)
#  define Rte_InitValue_RearAxleSteeringFunctionDsbl_RearAxleSteeringFunctionDsbl (3U)
#  define Rte_InitValue_RearAxleSteeringFunctionStatus_RearAxleSteeringFunctionStatus (7U)
#  define Rte_InitValue_RearAxleSteering_DeviceEvent_A2PosSwitchStatus (3U)
#  define Rte_InitValue_SwcActivation_EngineRun_EngineRun (1U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_RearAxleSteering_HMICtrl_ExtraAxleSteeringFunctionStat_RearAxleSteeringFunctionStatus(P2VAR(RearAxleSteeringFunctionStatus_T, AUTOMATIC, RTE_REARAXLESTEERING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_RearAxleSteering_HMICtrl_RearAxleSteeringFunctionStatus_RearAxleSteeringFunctionStatus(P2VAR(RearAxleSteeringFunctionStatus_T, AUTOMATIC, RTE_REARAXLESTEERING_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_RearAxleSteering_HMICtrl_RearAxleSteeringFunctionDsbl_RearAxleSteeringFunctionDsbl(RearAxleSteeringFunctionDsbl_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ExtraAxleSteeringFunctionStat_RearAxleSteeringFunctionStatus Rte_Read_RearAxleSteering_HMICtrl_ExtraAxleSteeringFunctionStat_RearAxleSteeringFunctionStatus
#  define Rte_Read_RearAxleSteeringFunctionStatus_RearAxleSteeringFunctionStatus Rte_Read_RearAxleSteering_HMICtrl_RearAxleSteeringFunctionStatus_RearAxleSteeringFunctionStatus
#  define Rte_Read_RearAxleSteering_DeviceEvent_A2PosSwitchStatus Rte_Read_RearAxleSteering_HMICtrl_RearAxleSteering_DeviceEvent_A2PosSwitchStatus
#  define Rte_Read_RearAxleSteering_HMICtrl_RearAxleSteering_DeviceEvent_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_RearAxleSteering_DeviceEvent_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_EngineRun_EngineRun Rte_Read_RearAxleSteering_HMICtrl_SwcActivation_EngineRun_EngineRun
#  define Rte_Read_RearAxleSteering_HMICtrl_SwcActivation_EngineRun_EngineRun(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_EngineRun_EngineRun, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_RearAxleSteeringDeviceInd_DeviceIndication Rte_Write_RearAxleSteering_HMICtrl_RearAxleSteeringDeviceInd_DeviceIndication
#  define Rte_Write_RearAxleSteering_HMICtrl_RearAxleSteeringDeviceInd_DeviceIndication(data) (Rte_RearAxleSteering_HMICtrl_RearAxleSteeringDeviceInd_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_RearAxleSteeringFunctionDsbl_RearAxleSteeringFunctionDsbl Rte_Write_RearAxleSteering_HMICtrl_RearAxleSteeringFunctionDsbl_RearAxleSteeringFunctionDsbl


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_RAS_LEDFeedbackIndication_P1GCC_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1GCC_RAS_LEDFeedbackIndication_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1GBT_RAS_SwitchInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1RRH_RAS_ToggleButton_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1GCC_RAS_LEDFeedbackIndication_v() (Rte_AddrPar_0x2B_P1GCC_RAS_LEDFeedbackIndication_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1GBT_RAS_SwitchInstalled_v() (Rte_AddrPar_0x2B_P1GBT_RAS_SwitchInstalled_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1RRH_RAS_ToggleButton_v() (Rte_AddrPar_0x2B_P1RRH_RAS_ToggleButton_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define RearAxleSteering_HMICtrl_START_SEC_CODE
# include "RearAxleSteering_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RearAxleSteering_HMICtrl_20ms_runnable RearAxleSteering_HMICtrl_20ms_runnable
# endif

FUNC(void, RearAxleSteering_HMICtrl_CODE) RearAxleSteering_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define RearAxleSteering_HMICtrl_STOP_SEC_CODE
# include "RearAxleSteering_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_REARAXLESTEERING_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
