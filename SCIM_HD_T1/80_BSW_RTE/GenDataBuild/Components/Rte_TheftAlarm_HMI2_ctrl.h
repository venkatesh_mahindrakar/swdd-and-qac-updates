/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_TheftAlarm_HMI2_ctrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <TheftAlarm_HMI2_ctrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_THEFTALARM_HMI2_CTRL_H
# define _RTE_THEFTALARM_HMI2_CTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_TheftAlarm_HMI2_ctrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_TheftAlarm_HMI2_ctrl_ReducedSetMode_DevInd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_TheftAlarm_HMI2_ctrl_ReducedSetMode_rqst_CryptTrig_CryptoTrigger; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_TheftAlarm_HMI2_ctrl_TheftAlarmAct_rqst_CryptTrig_CryptoTrigger; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(KeyAuthentication_stat_decrypt_T, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_Keyfob_UICtrl_KeyfobLockButton_Status_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_Keyfob_UICtrl_KeyfobSuperLockButton_Sta_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_KeyfobUnlockButton_Status_oCIOM_BB2_02P_oBackbone2_7b99e4e7_Tx; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ReducedSetModeButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Parked_Parked; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleMode_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_AlarmStatus_stat_AlarmStatus_stat (15U)
#  define Rte_InitValue_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt (7U)
#  define Rte_InitValue_KeyPosition_KeyPosition (7U)
#  define Rte_InitValue_KeyfobLockButton_Status_PushButtonStatus (3U)
#  define Rte_InitValue_KeyfobSuperLockButton_Status_PushButtonStatus (3U)
#  define Rte_InitValue_KeyfobUnlockButton_Status_PushButtonStatus (3U)
#  define Rte_InitValue_ReducedSetModeButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ReducedSetMode_DevInd_DeviceIndication (0U)
#  define Rte_InitValue_ReducedSetMode_rqst_CryptTrig_CryptoTrigger (FALSE)
#  define Rte_InitValue_ReducedSetMode_rqst_decrypt_ReducedSetMode_rqst_decrypt (7U)
#  define Rte_InitValue_SwcActivation_Parked_Parked (1U)
#  define Rte_InitValue_TheftAlarmAct_rqst_CryptTrig_CryptoTrigger (FALSE)
#  define Rte_InitValue_TheftAlarmAct_rqst_decrypt_TheftAlarmAct_rqst_decrypt (7U)
#  define Rte_InitValue_VehicleModeInternal_VehicleMode (15U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_TheftAlarm_HMI2_ctrl_KeyPosition_KeyPosition(P2VAR(KeyPosition_T, AUTOMATIC, RTE_THEFTALARM_HMI2_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_TheftAlarm_HMI2_ctrl_ReducedSetMode_rqst_serialized_Crypto_Function_serialized(P2CONST(uint8, AUTOMATIC, RTE_THEFTALARM_HMI2_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_TheftAlarm_HMI2_ctrl_ReducedSetMode_rqst_serialized_Crypto_Function_serialized(P2CONST(Crypto_Function_serialized_T, AUTOMATIC, RTE_THEFTALARM_HMI2_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_TheftAlarm_HMI2_ctrl_TheftAlarmAct_rqst_serialized_Crypto_Function_serialized(P2CONST(uint8, AUTOMATIC, RTE_THEFTALARM_HMI2_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_TheftAlarm_HMI2_ctrl_TheftAlarmAct_rqst_serialized_Crypto_Function_serialized(P2CONST(Crypto_Function_serialized_T, AUTOMATIC, RTE_THEFTALARM_HMI2_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_AlarmStatus_stat_AlarmStatus_stat Rte_Read_TheftAlarm_HMI2_ctrl_AlarmStatus_stat_AlarmStatus_stat
#  define Rte_Read_TheftAlarm_HMI2_ctrl_AlarmStatus_stat_AlarmStatus_stat(data) (Com_ReceiveSignal(ComConf_ComSignal_AlarmStatus_stat_ISig_10_oAlarm_Sec_02P_oSecuritySubnet_14de6466_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt Rte_Read_TheftAlarm_HMI2_ctrl_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt
#  define Rte_Read_TheftAlarm_HMI2_ctrl_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt(data) (*(data) = Rte_DriverAuthentication2_Ctrl_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_KeyPosition_KeyPosition Rte_Read_TheftAlarm_HMI2_ctrl_KeyPosition_KeyPosition
#  define Rte_Read_KeyfobLockButton_Status_PushButtonStatus Rte_Read_TheftAlarm_HMI2_ctrl_KeyfobLockButton_Status_PushButtonStatus
#  define Rte_Read_TheftAlarm_HMI2_ctrl_KeyfobLockButton_Status_PushButtonStatus(data) (*(data) = Rte_Keyfob_UICtrl_KeyfobLockButton_Status_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_KeyfobSuperLockButton_Status_PushButtonStatus Rte_Read_TheftAlarm_HMI2_ctrl_KeyfobSuperLockButton_Status_PushButtonStatus
#  define Rte_Read_TheftAlarm_HMI2_ctrl_KeyfobSuperLockButton_Status_PushButtonStatus(data) (*(data) = Rte_Keyfob_UICtrl_KeyfobSuperLockButton_Sta_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_KeyfobUnlockButton_Status_PushButtonStatus Rte_Read_TheftAlarm_HMI2_ctrl_KeyfobUnlockButton_Status_PushButtonStatus
#  define Rte_Read_TheftAlarm_HMI2_ctrl_KeyfobUnlockButton_Status_PushButtonStatus(data) (*(data) = Rte_KeyfobUnlockButton_Status_oCIOM_BB2_02P_oBackbone2_7b99e4e7_Tx, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ReducedSetModeButtonStatus_PushButtonStatus Rte_Read_TheftAlarm_HMI2_ctrl_ReducedSetModeButtonStatus_PushButtonStatus
#  define Rte_Read_TheftAlarm_HMI2_ctrl_ReducedSetModeButtonStatus_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_ReducedSetModeButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_Parked_Parked Rte_Read_TheftAlarm_HMI2_ctrl_SwcActivation_Parked_Parked
#  define Rte_Read_TheftAlarm_HMI2_ctrl_SwcActivation_Parked_Parked(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Parked_Parked, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_VehicleModeInternal_VehicleMode Rte_Read_TheftAlarm_HMI2_ctrl_VehicleModeInternal_VehicleMode
#  define Rte_Read_TheftAlarm_HMI2_ctrl_VehicleModeInternal_VehicleMode(data) (*(data) = Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_ReducedSetMode_DevInd_DeviceIndication Rte_Write_TheftAlarm_HMI2_ctrl_ReducedSetMode_DevInd_DeviceIndication
#  define Rte_Write_TheftAlarm_HMI2_ctrl_ReducedSetMode_DevInd_DeviceIndication(data) (Rte_TheftAlarm_HMI2_ctrl_ReducedSetMode_DevInd_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ReducedSetMode_rqst_CryptTrig_CryptoTrigger Rte_Write_TheftAlarm_HMI2_ctrl_ReducedSetMode_rqst_CryptTrig_CryptoTrigger
#  define Rte_Write_TheftAlarm_HMI2_ctrl_ReducedSetMode_rqst_CryptTrig_CryptoTrigger(data) (Rte_TheftAlarm_HMI2_ctrl_ReducedSetMode_rqst_CryptTrig_CryptoTrigger = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ReducedSetMode_rqst_decrypt_ReducedSetMode_rqst_decrypt Rte_Write_TheftAlarm_HMI2_ctrl_ReducedSetMode_rqst_decrypt_ReducedSetMode_rqst_decrypt
#  define Rte_Write_TheftAlarm_HMI2_ctrl_ReducedSetMode_rqst_decrypt_ReducedSetMode_rqst_decrypt(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ReducedSetMode_rqst_serialized_Crypto_Function_serialized Rte_Write_TheftAlarm_HMI2_ctrl_ReducedSetMode_rqst_serialized_Crypto_Function_serialized
#  define Rte_Write_TheftAlarmAct_rqst_CryptTrig_CryptoTrigger Rte_Write_TheftAlarm_HMI2_ctrl_TheftAlarmAct_rqst_CryptTrig_CryptoTrigger
#  define Rte_Write_TheftAlarm_HMI2_ctrl_TheftAlarmAct_rqst_CryptTrig_CryptoTrigger(data) (Rte_TheftAlarm_HMI2_ctrl_TheftAlarmAct_rqst_CryptTrig_CryptoTrigger = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_TheftAlarmAct_rqst_decrypt_TheftAlarmAct_rqst_decrypt Rte_Write_TheftAlarm_HMI2_ctrl_TheftAlarmAct_rqst_decrypt_TheftAlarmAct_rqst_decrypt
#  define Rte_Write_TheftAlarm_HMI2_ctrl_TheftAlarmAct_rqst_decrypt_TheftAlarmAct_rqst_decrypt(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_TheftAlarmAct_rqst_serialized_Crypto_Function_serialized Rte_Write_TheftAlarm_HMI2_ctrl_TheftAlarmAct_rqst_serialized_Crypto_Function_serialized


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_TheftAlarmRequestDuration_X1CY8_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CY8_TheftAlarmRequestDuration_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_X1CY8_TheftAlarmRequestDuration_v() (Rte_AddrPar_0x29_X1CY8_TheftAlarmRequestDuration_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2T_AlarmInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1B2T_AlarmInstalled_v() (Rte_AddrPar_0x2B_P1B2T_AlarmInstalled_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define TheftAlarm_HMI2_ctrl_START_SEC_CODE
# include "TheftAlarm_HMI2_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_TheftAlarm_HMI2_ctrl_20ms_runnable TheftAlarm_HMI2_ctrl_20ms_runnable
# endif

FUNC(void, TheftAlarm_HMI2_ctrl_CODE) TheftAlarm_HMI2_ctrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define TheftAlarm_HMI2_ctrl_STOP_SEC_CODE
# include "TheftAlarm_HMI2_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_THEFTALARM_HMI2_CTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
