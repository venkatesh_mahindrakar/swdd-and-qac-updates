/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_BswM.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <BswM>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_BSWM_H
# define _RTE_BSWM_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_BswM_Type.h"
# include "Rte_DataHandleType.h"


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_Request_LIN1_ScheduleTableRequestMode_requestedMode (0U)
#  define Rte_InitValue_Request_LIN2_ScheduleTableRequestMode_requestedMode (0U)
#  define Rte_InitValue_Request_LIN3_ScheduleTableRequestMode_requestedMode (0U)
#  define Rte_InitValue_Request_LIN4_ScheduleTableRequestMode_requestedMode (0U)
#  define Rte_InitValue_Request_LIN5_ScheduleTableRequestMode_requestedMode (0U)
#  define Rte_InitValue_Request_LIN6_ScheduleTableRequestMode_requestedMode (0U)
#  define Rte_InitValue_Request_LIN7_ScheduleTableRequestMode_requestedMode (0U)
#  define Rte_InitValue_Request_LIN8_ScheduleTableRequestMode_requestedMode (0U)
#  define Rte_InitValue_Request_SwcModeRequest_NvmWriteAllRequest_requestedMode (0U)
#  define Rte_InitValue_Request_SwcModeRequest_PvtReportCtrl_requestedMode (0U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BswM_Request_LIN1_ScheduleTableRequestMode_requestedMode(P2VAR(BswM_BswMRteMDG_LIN1Schedule, AUTOMATIC, RTE_BSWM_APPL_VAR) data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BswM_Request_LIN2_ScheduleTableRequestMode_requestedMode(P2VAR(BswM_BswMRteMDG_LIN2Schedule, AUTOMATIC, RTE_BSWM_APPL_VAR) data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BswM_Request_LIN3_ScheduleTableRequestMode_requestedMode(P2VAR(BswM_BswMRteMDG_LIN3Schedule, AUTOMATIC, RTE_BSWM_APPL_VAR) data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BswM_Request_LIN4_ScheduleTableRequestMode_requestedMode(P2VAR(BswM_BswMRteMDG_LIN4Schedule, AUTOMATIC, RTE_BSWM_APPL_VAR) data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BswM_Request_LIN5_ScheduleTableRequestMode_requestedMode(P2VAR(BswM_BswMRteMDG_LIN5Schedule, AUTOMATIC, RTE_BSWM_APPL_VAR) data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BswM_Request_LIN6_ScheduleTableRequestMode_requestedMode(P2VAR(BswM_BswMRteMDG_LIN6Schedule, AUTOMATIC, RTE_BSWM_APPL_VAR) data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BswM_Request_LIN7_ScheduleTableRequestMode_requestedMode(P2VAR(BswM_BswMRteMDG_LIN7Schedule, AUTOMATIC, RTE_BSWM_APPL_VAR) data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BswM_Request_LIN8_ScheduleTableRequestMode_requestedMode(P2VAR(BswM_BswMRteMDG_LIN8Schedule, AUTOMATIC, RTE_BSWM_APPL_VAR) data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BswM_Request_SwcModeRequest_NvmWriteAllRequest_requestedMode(P2VAR(BswM_BswMRteMDG_NvmWriteAllRequest, AUTOMATIC, RTE_BSWM_APPL_VAR) data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BswM_Request_SwcModeRequest_PvtReportCtrl_requestedMode(P2VAR(BswM_BswMRteMDG_PvtReport_X1C14, AUTOMATIC, RTE_BSWM_APPL_VAR) data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(BswM_ESH_Mode, RTE_CODE) Rte_Mode_BswM_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Dcm_EcuResetType, RTE_CODE) Rte_Mode_BswM_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule(BswM_BswMRteMDG_LIN1Schedule nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule(BswM_BswMRteMDG_LIN2Schedule nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule(BswM_BswMRteMDG_LIN3Schedule nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule(BswM_BswMRteMDG_LIN4Schedule nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule(BswM_BswMRteMDG_LIN5Schedule nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule(BswM_BswMRteMDG_LIN6Schedule nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule(BswM_BswMRteMDG_LIN7Schedule nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule(BswM_BswMRteMDG_LIN8Schedule nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode(BswM_ESH_Mode nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_Request_LIN1_ScheduleTableRequestMode_requestedMode Rte_Read_BswM_Request_LIN1_ScheduleTableRequestMode_requestedMode
#  define Rte_Read_Request_LIN2_ScheduleTableRequestMode_requestedMode Rte_Read_BswM_Request_LIN2_ScheduleTableRequestMode_requestedMode
#  define Rte_Read_Request_LIN3_ScheduleTableRequestMode_requestedMode Rte_Read_BswM_Request_LIN3_ScheduleTableRequestMode_requestedMode
#  define Rte_Read_Request_LIN4_ScheduleTableRequestMode_requestedMode Rte_Read_BswM_Request_LIN4_ScheduleTableRequestMode_requestedMode
#  define Rte_Read_Request_LIN5_ScheduleTableRequestMode_requestedMode Rte_Read_BswM_Request_LIN5_ScheduleTableRequestMode_requestedMode
#  define Rte_Read_Request_LIN6_ScheduleTableRequestMode_requestedMode Rte_Read_BswM_Request_LIN6_ScheduleTableRequestMode_requestedMode
#  define Rte_Read_Request_LIN7_ScheduleTableRequestMode_requestedMode Rte_Read_BswM_Request_LIN7_ScheduleTableRequestMode_requestedMode
#  define Rte_Read_Request_LIN8_ScheduleTableRequestMode_requestedMode Rte_Read_BswM_Request_LIN8_ScheduleTableRequestMode_requestedMode
#  define Rte_Read_Request_SwcModeRequest_NvmWriteAllRequest_requestedMode Rte_Read_BswM_Request_SwcModeRequest_NvmWriteAllRequest_requestedMode
#  define Rte_Read_Request_SwcModeRequest_PvtReportCtrl_requestedMode Rte_Read_BswM_Request_SwcModeRequest_PvtReportCtrl_requestedMode


/**********************************************************************************************************************
 * Rte_Mode_<p>_<m>
 *********************************************************************************************************************/
#  define Rte_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode Rte_Mode_BswM_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode
#  define Rte_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset Rte_Mode_BswM_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset


/**********************************************************************************************************************
 * Rte_Switch_<p>_<m>
 *********************************************************************************************************************/
#  define Rte_Switch_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff Rte_Switch_BswM_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff
#  define Rte_Switch_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff Rte_Switch_BswM_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff
#  define Rte_Switch_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff Rte_Switch_BswM_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff
#  define Rte_Switch_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff Rte_Switch_BswM_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff
#  define Rte_Switch_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff Rte_Switch_BswM_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff
#  define Rte_Switch_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule Rte_Switch_BswM_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule
#  define Rte_Switch_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState Rte_Switch_BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState
#  define Rte_Switch_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule Rte_Switch_BswM_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule
#  define Rte_Switch_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState Rte_Switch_BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState
#  define Rte_Switch_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule Rte_Switch_BswM_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule
#  define Rte_Switch_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState Rte_Switch_BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState
#  define Rte_Switch_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule Rte_Switch_BswM_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule
#  define Rte_Switch_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState Rte_Switch_BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState
#  define Rte_Switch_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule Rte_Switch_BswM_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule
#  define Rte_Switch_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState Rte_Switch_BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState
#  define Rte_Switch_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule Rte_Switch_BswM_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule
#  define Rte_Switch_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState Rte_Switch_BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState
#  define Rte_Switch_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule Rte_Switch_BswM_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule
#  define Rte_Switch_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState Rte_Switch_BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState
#  define Rte_Switch_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule Rte_Switch_BswM_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule
#  define Rte_Switch_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState Rte_Switch_BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState
#  define Rte_Switch_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff Rte_Switch_BswM_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff
#  define Rte_Switch_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode Rte_Switch_BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode


# endif /* !defined(RTE_CORE) */


# define BswM_START_SEC_CODE
# include "BswM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_BswM_MainFunction BswM_MainFunction
#  define RTE_RUNNABLE_BswM_Read_LIN1_ScheduleTableRequestMode BswM_Read_LIN1_ScheduleTableRequestMode
#  define RTE_RUNNABLE_BswM_Read_LIN2_ScheduleTableRequestMode BswM_Read_LIN2_ScheduleTableRequestMode
#  define RTE_RUNNABLE_BswM_Read_LIN3_ScheduleTableRequestMode BswM_Read_LIN3_ScheduleTableRequestMode
#  define RTE_RUNNABLE_BswM_Read_LIN4_ScheduleTableRequestMode BswM_Read_LIN4_ScheduleTableRequestMode
#  define RTE_RUNNABLE_BswM_Read_LIN5_ScheduleTableRequestMode BswM_Read_LIN5_ScheduleTableRequestMode
#  define RTE_RUNNABLE_BswM_Read_LIN6_ScheduleTableRequestMode BswM_Read_LIN6_ScheduleTableRequestMode
#  define RTE_RUNNABLE_BswM_Read_LIN7_ScheduleTableRequestMode BswM_Read_LIN7_ScheduleTableRequestMode
#  define RTE_RUNNABLE_BswM_Read_LIN8_ScheduleTableRequestMode BswM_Read_LIN8_ScheduleTableRequestMode
# endif

FUNC(void, BswM_CODE) BswM_MainFunction(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, BswM_CODE) BswM_Read_LIN1_ScheduleTableRequestMode(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, BswM_CODE) BswM_Read_LIN2_ScheduleTableRequestMode(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, BswM_CODE) BswM_Read_LIN3_ScheduleTableRequestMode(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, BswM_CODE) BswM_Read_LIN4_ScheduleTableRequestMode(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, BswM_CODE) BswM_Read_LIN5_ScheduleTableRequestMode(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, BswM_CODE) BswM_Read_LIN6_ScheduleTableRequestMode(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, BswM_CODE) BswM_Read_LIN7_ScheduleTableRequestMode(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, BswM_CODE) BswM_Read_LIN8_ScheduleTableRequestMode(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define BswM_STOP_SEC_CODE
# include "BswM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_BSWM_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
