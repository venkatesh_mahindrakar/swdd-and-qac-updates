/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_LevelControl_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <LevelControl_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_LEVELCONTROL_HMICTRL_H
# define _RTE_LEVELCONTROL_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_LevelControl_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(FalseTrue_T, RTE_VAR_NOINIT) Rte_LevelControl_HMICtrl_BlinkECSWiredLEDs_BlinkECSWiredLEDs; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_LevelControl_HMICtrl_FPBR_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_LevelControl_HMICtrl_FerryFunction_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_LevelControl_HMICtrl_KneelDeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AlternativeDriveLevelSw_stat_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ECSStandByReq_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_ECSStandByReqRCECS_ECSStandByReqRCECS; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FPBRSwitchStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FerryFunctionSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_KneelSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LoadingLevelSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Living_Living; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleMode_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FalseTrue_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredAirSuspensionStopRequest_AirSuspensionStopRequest; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(LevelAdjustmentAction_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredLevelAdjustmentAction_LevelAdjustmentAction; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(LevelAdjustmentAxles_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredLevelAdjustmentAxles_LevelAdjustmentAxles; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(LevelAdjustmentStroke_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredLevelAdjustmentStroke_LevelAdjustmentStroke; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(WiredLevelUserMemory_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredLevelUserMemory_WiredLevelUserMemory; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(LevelUserMemoryAction_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredLevelUserMemoryAction_LevelUserMemoryAction; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_AlternativeDriveLevelSw_stat_A3PosSwitchStatus (7U)
#  define Rte_InitValue_BackToDriveReq_BackToDriveReq (3U)
#  define Rte_InitValue_BackToDriveReqACK_BackToDriveReqACK (3U)
#  define Rte_InitValue_BlinkECSWiredLEDs_BlinkECSWiredLEDs (3U)
#  define Rte_InitValue_ChangeKneelACK_ChangeKneelACK (3U)
#  define Rte_InitValue_ECSStandByReqRCECS_ECSStandByReqRCECS (7U)
#  define Rte_InitValue_ECSStandByReqWRC_ECSStandByReqWRC (7U)
#  define Rte_InitValue_ECSStandByRequest_ECSStandByRequest (7U)
#  define Rte_InitValue_ECSStandbyActive_ECSStandbyActive (3U)
#  define Rte_InitValue_ECSStandbyAllowed_ECSStandbyAllowed (3U)
#  define Rte_InitValue_ElectricalLoadReduction_rqst_ElectricalLoadReduction_rqst (7U)
#  define Rte_InitValue_FPBRChangeReq_FPBRChangeReq (3U)
#  define Rte_InitValue_FPBRSwitchStatus_PushButtonStatus (3U)
#  define Rte_InitValue_FPBR_DeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_FerryFunctionRequest_FerryFunctionRequest (3U)
#  define Rte_InitValue_FerryFunctionStatus_FerryFunctionStatus (7U)
#  define Rte_InitValue_FerryFunctionSwitchChangeACK_FerryFunctionSwitchChangeACK (3U)
#  define Rte_InitValue_FerryFunctionSwitchChangeReq_FerryFunctionSwitchChangeReq (3U)
#  define Rte_InitValue_FerryFunctionSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_FerryFunction_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_HeightAdjustmentAllowed_HeightAdjustmentAllowed (3U)
#  define Rte_InitValue_InhibitWRCECSMenuCmd_InhibitWRCECSMenuCmd (3U)
#  define Rte_InitValue_KneelDeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_KneelSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_KneelingChangeRequest_KneelingChangeRequest (3U)
#  define Rte_InitValue_KneelingStatusHMI_KneelingStatusHMI (3U)
#  define Rte_InitValue_LevelStrokeRequest_LevelStrokeRequest (3U)
#  define Rte_InitValue_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LoadingLevelSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_RampLevelRequest_RampLevelRequest (15U)
#  define Rte_InitValue_RampLevelRequestACK_RampLevelRequestACK (3U)
#  define Rte_InitValue_RampLevelStorageAck_RampLevelStorageAck (3U)
#  define Rte_InitValue_RampLevelStorageRequest_RampLevelStorageRequest (15U)
#  define Rte_InitValue_RideHeightFunctionRequest_RideHeightFunctionRequest (15U)
#  define Rte_InitValue_RideHeightStorageAck_RideHeightStorageAck (3U)
#  define Rte_InitValue_RideHeightStorageRequest_RideHeightStorageRequest (15U)
#  define Rte_InitValue_StopLevelChangeAck_StopLevelChangeStatus (3U)
#  define Rte_InitValue_StopLevelChangeRequest_StopLevelChangeRequest (3U)
#  define Rte_InitValue_SwcActivation_Living_Living (1U)
#  define Rte_InitValue_VehicleModeInternal_VehicleMode (15U)
#  define Rte_InitValue_WRCAirSuspensionStopRequest_AirSuspensionStopRequest (3U)
#  define Rte_InitValue_WRCLevelAdjustmentAction_LevelAdjustmentAction (15U)
#  define Rte_InitValue_WRCLevelAdjustmentAxles_LevelAdjustmentAxles (7U)
#  define Rte_InitValue_WRCLevelAdjustmentStroke_LevelAdjustmentStroke (7U)
#  define Rte_InitValue_WRCLevelUserMemory_LevelUserMemory (15U)
#  define Rte_InitValue_WRCLevelUserMemoryAction_LevelUserMemoryAction (7U)
#  define Rte_InitValue_WRCRollRequest_WRCRollRequest (7U)
#  define Rte_InitValue_WiredAirSuspensionStopRequest_AirSuspensionStopRequest (3U)
#  define Rte_InitValue_WiredLevelAdjustmentAction_LevelAdjustmentAction (15U)
#  define Rte_InitValue_WiredLevelAdjustmentAxles_LevelAdjustmentAxles (7U)
#  define Rte_InitValue_WiredLevelAdjustmentStroke_LevelAdjustmentStroke (7U)
#  define Rte_InitValue_WiredLevelUserMemory_WiredLevelUserMemory (15U)
#  define Rte_InitValue_WiredLevelUserMemoryAction_LevelUserMemoryAction (7U)
# endif


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Buffers for inter-runnable variables
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint32, RTE_VAR_NOINIT) Rte_Irv_LevelControl_HMICtrl_IRV_ECS_StandbyTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
# endif /* !defined(RTE_CORE) */


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_BackToDriveReqACK_BackToDriveReqACK(P2VAR(BackToDriveReqACK_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_ChangeKneelACK_ChangeKneelACK(P2VAR(ChangeKneelACK_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_ECSStandByReqWRC_ECSStandByReqWRC(P2VAR(ECSStandByReq_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_ECSStandbyAllowed_ECSStandbyAllowed(P2VAR(FalseTrue_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_ElectricalLoadReduction_rqst_ElectricalLoadReduction_rqst(P2VAR(ElectricalLoadReduction_rqst_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_FPBRMMIStat_FPBRMMIStat(P2VAR(FPBRMMIStat_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_FerryFunctionStatus_FerryFunctionStatus(P2VAR(FerryFunctionStatus_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_FerryFunctionSwitchChangeACK_FerryFunctionSwitchChangeACK(P2VAR(Ack2Bit_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_HeightAdjustmentAllowed_HeightAdjustmentAllowed(P2VAR(FalseTrue_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_KneelingStatusHMI_KneelingStatusHMI(P2VAR(KneelingStatusHMI_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_RampLevelRequestACK_RampLevelRequestACK(P2VAR(ChangeRequest2Bit_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_RampLevelStorageAck_RampLevelStorageAck(P2VAR(StorageAck_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_RideHeightStorageAck_RideHeightStorageAck(P2VAR(StorageAck_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_StopLevelChangeAck_StopLevelChangeStatus(P2VAR(StopLevelChangeStatus_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_WRCAirSuspensionStopRequest_AirSuspensionStopRequest(P2VAR(FalseTrue_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_WRCLevelAdjustmentAction_LevelAdjustmentAction(P2VAR(LevelAdjustmentAction_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_WRCLevelAdjustmentAxles_LevelAdjustmentAxles(P2VAR(LevelAdjustmentAxles_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_WRCLevelAdjustmentStroke_LevelAdjustmentStroke(P2VAR(LevelAdjustmentStroke_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_WRCLevelUserMemory_LevelUserMemory(P2VAR(LevelUserMemory_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_WRCLevelUserMemoryAction_LevelUserMemoryAction(P2VAR(LevelUserMemoryAction_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_LevelControl_HMICtrl_WRCRollRequest_WRCRollRequest(P2VAR(RollRequest_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LevelControl_HMICtrl_BackToDriveReq_BackToDriveReq(BackToDriveReq_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LevelControl_HMICtrl_ECSStandByRequest_ECSStandByRequest(ECSStandByRequest_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LevelControl_HMICtrl_ECSStandbyActive_ECSStandbyActive(FalseTrue_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LevelControl_HMICtrl_FPBRChangeReq_FPBRChangeReq(FPBRChangeReq_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LevelControl_HMICtrl_FerryFunctionRequest_FerryFunctionRequest(Request_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LevelControl_HMICtrl_FerryFunctionSwitchChangeReq_FerryFunctionSwitchChangeReq(ChangeRequest2Bit_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LevelControl_HMICtrl_InhibitWRCECSMenuCmd_InhibitWRCECSMenuCmd(InactiveActive_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LevelControl_HMICtrl_KneelingChangeRequest_KneelingChangeRequest(KneelingChangeRequest_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LevelControl_HMICtrl_LevelRequest_LevelRequest(P2CONST(LevelRequest_T, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LevelControl_HMICtrl_LevelStrokeRequest_LevelStrokeRequest(LevelStrokeRequest_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LevelControl_HMICtrl_RampLevelRequest_RampLevelRequest(RampLevelRequest_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LevelControl_HMICtrl_RampLevelStorageRequest_RampLevelStorageRequest(RampLevelRequest_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LevelControl_HMICtrl_RideHeightFunctionRequest_RideHeightFunctionRequest(RideHeightFunction_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LevelControl_HMICtrl_RideHeightStorageRequest_RideHeightStorageRequest(RideHeightStorageRequest_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_LevelControl_HMICtrl_StopLevelChangeRequest_StopLevelChangeRequest(Request_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_AlternativeDriveLevelSw_stat_A3PosSwitchStatus Rte_Read_LevelControl_HMICtrl_AlternativeDriveLevelSw_stat_A3PosSwitchStatus
#  define Rte_Read_LevelControl_HMICtrl_AlternativeDriveLevelSw_stat_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_AlternativeDriveLevelSw_stat_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_BackToDriveReqACK_BackToDriveReqACK Rte_Read_LevelControl_HMICtrl_BackToDriveReqACK_BackToDriveReqACK
#  define Rte_Read_ChangeKneelACK_ChangeKneelACK Rte_Read_LevelControl_HMICtrl_ChangeKneelACK_ChangeKneelACK
#  define Rte_Read_ECSStandByReqRCECS_ECSStandByReqRCECS Rte_Read_LevelControl_HMICtrl_ECSStandByReqRCECS_ECSStandByReqRCECS
#  define Rte_Read_LevelControl_HMICtrl_ECSStandByReqRCECS_ECSStandByReqRCECS(data) (*(data) = Rte_WiredControlBox_HMICtrl_ECSStandByReqRCECS_ECSStandByReqRCECS, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ECSStandByReqWRC_ECSStandByReqWRC Rte_Read_LevelControl_HMICtrl_ECSStandByReqWRC_ECSStandByReqWRC
#  define Rte_Read_ECSStandbyAllowed_ECSStandbyAllowed Rte_Read_LevelControl_HMICtrl_ECSStandbyAllowed_ECSStandbyAllowed
#  define Rte_Read_ElectricalLoadReduction_rqst_ElectricalLoadReduction_rqst Rte_Read_LevelControl_HMICtrl_ElectricalLoadReduction_rqst_ElectricalLoadReduction_rqst
#  define Rte_Read_FPBRMMIStat_FPBRMMIStat Rte_Read_LevelControl_HMICtrl_FPBRMMIStat_FPBRMMIStat
#  define Rte_Read_FPBRSwitchStatus_PushButtonStatus Rte_Read_LevelControl_HMICtrl_FPBRSwitchStatus_PushButtonStatus
#  define Rte_Read_LevelControl_HMICtrl_FPBRSwitchStatus_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_FPBRSwitchStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FerryFunctionStatus_FerryFunctionStatus Rte_Read_LevelControl_HMICtrl_FerryFunctionStatus_FerryFunctionStatus
#  define Rte_Read_FerryFunctionSwitchChangeACK_FerryFunctionSwitchChangeACK Rte_Read_LevelControl_HMICtrl_FerryFunctionSwitchChangeACK_FerryFunctionSwitchChangeACK
#  define Rte_Read_FerryFunctionSwitchStatus_A2PosSwitchStatus Rte_Read_LevelControl_HMICtrl_FerryFunctionSwitchStatus_A2PosSwitchStatus
#  define Rte_Read_LevelControl_HMICtrl_FerryFunctionSwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_FerryFunctionSwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed Rte_Read_LevelControl_HMICtrl_HeightAdjustmentAllowed_HeightAdjustmentAllowed
#  define Rte_Read_KneelSwitchStatus_A2PosSwitchStatus Rte_Read_LevelControl_HMICtrl_KneelSwitchStatus_A2PosSwitchStatus
#  define Rte_Read_LevelControl_HMICtrl_KneelSwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_KneelSwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_KneelingStatusHMI_KneelingStatusHMI Rte_Read_LevelControl_HMICtrl_KneelingStatusHMI_KneelingStatusHMI
#  define Rte_Read_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus Rte_Read_LevelControl_HMICtrl_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus
#  define Rte_Read_LevelControl_HMICtrl_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LoadingLevelSwitchStatus_A3PosSwitchStatus Rte_Read_LevelControl_HMICtrl_LoadingLevelSwitchStatus_A3PosSwitchStatus
#  define Rte_Read_LevelControl_HMICtrl_LoadingLevelSwitchStatus_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_LoadingLevelSwitchStatus_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RampLevelRequestACK_RampLevelRequestACK Rte_Read_LevelControl_HMICtrl_RampLevelRequestACK_RampLevelRequestACK
#  define Rte_Read_RampLevelStorageAck_RampLevelStorageAck Rte_Read_LevelControl_HMICtrl_RampLevelStorageAck_RampLevelStorageAck
#  define Rte_Read_RideHeightStorageAck_RideHeightStorageAck Rte_Read_LevelControl_HMICtrl_RideHeightStorageAck_RideHeightStorageAck
#  define Rte_Read_StopLevelChangeAck_StopLevelChangeStatus Rte_Read_LevelControl_HMICtrl_StopLevelChangeAck_StopLevelChangeStatus
#  define Rte_Read_SwcActivation_Living_Living Rte_Read_LevelControl_HMICtrl_SwcActivation_Living_Living
#  define Rte_Read_LevelControl_HMICtrl_SwcActivation_Living_Living(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Living_Living, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_VehicleModeInternal_VehicleMode Rte_Read_LevelControl_HMICtrl_VehicleModeInternal_VehicleMode
#  define Rte_Read_LevelControl_HMICtrl_VehicleModeInternal_VehicleMode(data) (*(data) = Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WRCAirSuspensionStopRequest_AirSuspensionStopRequest Rte_Read_LevelControl_HMICtrl_WRCAirSuspensionStopRequest_AirSuspensionStopRequest
#  define Rte_Read_WRCLevelAdjustmentAction_LevelAdjustmentAction Rte_Read_LevelControl_HMICtrl_WRCLevelAdjustmentAction_LevelAdjustmentAction
#  define Rte_Read_WRCLevelAdjustmentAxles_LevelAdjustmentAxles Rte_Read_LevelControl_HMICtrl_WRCLevelAdjustmentAxles_LevelAdjustmentAxles
#  define Rte_Read_WRCLevelAdjustmentStroke_LevelAdjustmentStroke Rte_Read_LevelControl_HMICtrl_WRCLevelAdjustmentStroke_LevelAdjustmentStroke
#  define Rte_Read_WRCLevelUserMemory_LevelUserMemory Rte_Read_LevelControl_HMICtrl_WRCLevelUserMemory_LevelUserMemory
#  define Rte_Read_WRCLevelUserMemoryAction_LevelUserMemoryAction Rte_Read_LevelControl_HMICtrl_WRCLevelUserMemoryAction_LevelUserMemoryAction
#  define Rte_Read_WRCRollRequest_WRCRollRequest Rte_Read_LevelControl_HMICtrl_WRCRollRequest_WRCRollRequest
#  define Rte_Read_WiredAirSuspensionStopRequest_AirSuspensionStopRequest Rte_Read_LevelControl_HMICtrl_WiredAirSuspensionStopRequest_AirSuspensionStopRequest
#  define Rte_Read_LevelControl_HMICtrl_WiredAirSuspensionStopRequest_AirSuspensionStopRequest(data) (*(data) = Rte_WiredControlBox_HMICtrl_WiredAirSuspensionStopRequest_AirSuspensionStopRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WiredLevelAdjustmentAction_LevelAdjustmentAction Rte_Read_LevelControl_HMICtrl_WiredLevelAdjustmentAction_LevelAdjustmentAction
#  define Rte_Read_LevelControl_HMICtrl_WiredLevelAdjustmentAction_LevelAdjustmentAction(data) (*(data) = Rte_WiredControlBox_HMICtrl_WiredLevelAdjustmentAction_LevelAdjustmentAction, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WiredLevelAdjustmentAxles_LevelAdjustmentAxles Rte_Read_LevelControl_HMICtrl_WiredLevelAdjustmentAxles_LevelAdjustmentAxles
#  define Rte_Read_LevelControl_HMICtrl_WiredLevelAdjustmentAxles_LevelAdjustmentAxles(data) (*(data) = Rte_WiredControlBox_HMICtrl_WiredLevelAdjustmentAxles_LevelAdjustmentAxles, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WiredLevelAdjustmentStroke_LevelAdjustmentStroke Rte_Read_LevelControl_HMICtrl_WiredLevelAdjustmentStroke_LevelAdjustmentStroke
#  define Rte_Read_LevelControl_HMICtrl_WiredLevelAdjustmentStroke_LevelAdjustmentStroke(data) (*(data) = Rte_WiredControlBox_HMICtrl_WiredLevelAdjustmentStroke_LevelAdjustmentStroke, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WiredLevelUserMemory_WiredLevelUserMemory Rte_Read_LevelControl_HMICtrl_WiredLevelUserMemory_WiredLevelUserMemory
#  define Rte_Read_LevelControl_HMICtrl_WiredLevelUserMemory_WiredLevelUserMemory(data) (*(data) = Rte_WiredControlBox_HMICtrl_WiredLevelUserMemory_WiredLevelUserMemory, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WiredLevelUserMemoryAction_LevelUserMemoryAction Rte_Read_LevelControl_HMICtrl_WiredLevelUserMemoryAction_LevelUserMemoryAction
#  define Rte_Read_LevelControl_HMICtrl_WiredLevelUserMemoryAction_LevelUserMemoryAction(data) (*(data) = Rte_WiredControlBox_HMICtrl_WiredLevelUserMemoryAction_LevelUserMemoryAction, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_BackToDriveReq_BackToDriveReq Rte_Write_LevelControl_HMICtrl_BackToDriveReq_BackToDriveReq
#  define Rte_Write_BlinkECSWiredLEDs_BlinkECSWiredLEDs Rte_Write_LevelControl_HMICtrl_BlinkECSWiredLEDs_BlinkECSWiredLEDs
#  define Rte_Write_LevelControl_HMICtrl_BlinkECSWiredLEDs_BlinkECSWiredLEDs(data) (Rte_LevelControl_HMICtrl_BlinkECSWiredLEDs_BlinkECSWiredLEDs = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ECSStandByRequest_ECSStandByRequest Rte_Write_LevelControl_HMICtrl_ECSStandByRequest_ECSStandByRequest
#  define Rte_Write_ECSStandbyActive_ECSStandbyActive Rte_Write_LevelControl_HMICtrl_ECSStandbyActive_ECSStandbyActive
#  define Rte_Write_FPBRChangeReq_FPBRChangeReq Rte_Write_LevelControl_HMICtrl_FPBRChangeReq_FPBRChangeReq
#  define Rte_Write_FPBR_DeviceIndication_DeviceIndication Rte_Write_LevelControl_HMICtrl_FPBR_DeviceIndication_DeviceIndication
#  define Rte_Write_LevelControl_HMICtrl_FPBR_DeviceIndication_DeviceIndication(data) (Rte_LevelControl_HMICtrl_FPBR_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_FerryFunctionRequest_FerryFunctionRequest Rte_Write_LevelControl_HMICtrl_FerryFunctionRequest_FerryFunctionRequest
#  define Rte_Write_FerryFunctionSwitchChangeReq_FerryFunctionSwitchChangeReq Rte_Write_LevelControl_HMICtrl_FerryFunctionSwitchChangeReq_FerryFunctionSwitchChangeReq
#  define Rte_Write_FerryFunction_DeviceIndication_DeviceIndication Rte_Write_LevelControl_HMICtrl_FerryFunction_DeviceIndication_DeviceIndication
#  define Rte_Write_LevelControl_HMICtrl_FerryFunction_DeviceIndication_DeviceIndication(data) (Rte_LevelControl_HMICtrl_FerryFunction_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_InhibitWRCECSMenuCmd_InhibitWRCECSMenuCmd Rte_Write_LevelControl_HMICtrl_InhibitWRCECSMenuCmd_InhibitWRCECSMenuCmd
#  define Rte_Write_KneelDeviceIndication_DeviceIndication Rte_Write_LevelControl_HMICtrl_KneelDeviceIndication_DeviceIndication
#  define Rte_Write_LevelControl_HMICtrl_KneelDeviceIndication_DeviceIndication(data) (Rte_LevelControl_HMICtrl_KneelDeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_KneelingChangeRequest_KneelingChangeRequest Rte_Write_LevelControl_HMICtrl_KneelingChangeRequest_KneelingChangeRequest
#  define Rte_Write_LevelRequest_LevelRequest Rte_Write_LevelControl_HMICtrl_LevelRequest_LevelRequest
#  define Rte_Write_LevelStrokeRequest_LevelStrokeRequest Rte_Write_LevelControl_HMICtrl_LevelStrokeRequest_LevelStrokeRequest
#  define Rte_Write_RampLevelRequest_RampLevelRequest Rte_Write_LevelControl_HMICtrl_RampLevelRequest_RampLevelRequest
#  define Rte_Write_RampLevelStorageRequest_RampLevelStorageRequest Rte_Write_LevelControl_HMICtrl_RampLevelStorageRequest_RampLevelStorageRequest
#  define Rte_Write_RideHeightFunctionRequest_RideHeightFunctionRequest Rte_Write_LevelControl_HMICtrl_RideHeightFunctionRequest_RideHeightFunctionRequest
#  define Rte_Write_RideHeightStorageRequest_RideHeightStorageRequest Rte_Write_LevelControl_HMICtrl_RideHeightStorageRequest_RideHeightStorageRequest
#  define Rte_Write_StopLevelChangeRequest_StopLevelChangeRequest Rte_Write_LevelControl_HMICtrl_StopLevelChangeRequest_StopLevelChangeRequest


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1BUO_63_LoadingLevelSw_stuck_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)252, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1CXA_63_FerryFlexibleSwitchStuck_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)253, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1CXB_63_KneelingFlexibleSwitchStuck_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)254, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1CXC_63_LoadingLevelAdjSwitchStuck_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)255, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1DOO_63_FPBRSwitchStuck_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)256, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_ActivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_ECSStandByActive_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)19)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_DeactivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_ECSStandByActive_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)19)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData_IRV_ECS_StandbyTimer() \
  Rte_Irv_LevelControl_HMICtrl_IRV_ECS_StandbyTimer
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_LevelControl_HMICtrl_20ms_runnable_IRV_ECS_StandbyTimer(data) \
  (Rte_Irv_LevelControl_HMICtrl_IRV_ECS_StandbyTimer = (data))
/* PRQA L:L1 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_ECSStandbyActivationTimeout_P1CUA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CUA_ECSStandbyActivationTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ECSStandbyExtendedActTimeout_P1CUB_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CUB_ECSStandbyExtendedActTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ECSActiveStateTimeout_P1CUE_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CUE_ECSActiveStateTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CUF_LoadingLevelSwStuckedTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_KneelButtonStuckedTimeout_P1DWD_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWD_KneelButtonStuckedTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FerryFuncSwStuckedTimeout_P1EXK_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EXK_FerryFuncSwStuckedTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ECS_StandbyBlinkTime_P1GCL_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1GCL_ECS_StandbyBlinkTime_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1IZ2_LoadingLevelAdjSwStuckTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FrontSuspensionType_P1JBR_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1JBR_FrontSuspensionType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LXQ_FPBRSwitchStuckedTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FPBRSwitchRequestACKTime_P1LXR_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LXR_FPBRSwitchRequestACKTime_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1A12_ADL_Sw_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CT4_FerrySw_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CT9_LoadingLevelSw_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EXH_KneelingSwitchInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1IZ1_LoadingLevelAdjSwitchInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LXP_FPBRSwitchInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1CUA_ECSStandbyActivationTimeout_v() (Rte_AddrPar_0x2B_P1CUA_ECSStandbyActivationTimeout_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CUB_ECSStandbyExtendedActTimeout_v() (Rte_AddrPar_0x2B_P1CUB_ECSStandbyExtendedActTimeout_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CUE_ECSActiveStateTimeout_v() (Rte_AddrPar_0x2B_P1CUE_ECSActiveStateTimeout_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CUF_LoadingLevelSwStuckedTimeout_v() (Rte_AddrPar_0x2B_P1CUF_LoadingLevelSwStuckedTimeout_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DWD_KneelButtonStuckedTimeout_v() (Rte_AddrPar_0x2B_P1DWD_KneelButtonStuckedTimeout_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EXK_FerryFuncSwStuckedTimeout_v() (Rte_AddrPar_0x2B_P1EXK_FerryFuncSwStuckedTimeout_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1GCL_ECS_StandbyBlinkTime_v() (Rte_AddrPar_0x2B_P1GCL_ECS_StandbyBlinkTime_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1IZ2_LoadingLevelAdjSwStuckTimeout_v() (Rte_AddrPar_0x2B_P1IZ2_LoadingLevelAdjSwStuckTimeout_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1JBR_FrontSuspensionType_v() (Rte_AddrPar_0x2B_P1JBR_FrontSuspensionType_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1LXQ_FPBRSwitchStuckedTimeout_v() (Rte_AddrPar_0x2B_P1LXQ_FPBRSwitchStuckedTimeout_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1LXR_FPBRSwitchRequestACKTime_v() (Rte_AddrPar_0x2B_P1LXR_FPBRSwitchRequestACKTime_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1A12_ADL_Sw_v() (Rte_AddrPar_0x2B_P1A12_ADL_Sw_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CT4_FerrySw_Installed_v() (Rte_AddrPar_0x2B_P1CT4_FerrySw_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CT9_LoadingLevelSw_Installed_v() (Rte_AddrPar_0x2B_P1CT9_LoadingLevelSw_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EXH_KneelingSwitchInstalled_v() (Rte_AddrPar_0x2B_P1EXH_KneelingSwitchInstalled_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1IZ1_LoadingLevelAdjSwitchInstalled_v() (Rte_AddrPar_0x2B_P1IZ1_LoadingLevelAdjSwitchInstalled_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1LXP_FPBRSwitchInstalled_v() (Rte_AddrPar_0x2B_P1LXP_FPBRSwitchInstalled_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1ALT_ECS_PartialAirSystem_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1ALU_ECS_FullAirSystem_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1B9X_WirelessRC_Enable_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1ALT_ECS_PartialAirSystem_v() (Rte_AddrPar_0x2B_and_0x37_P1ALT_ECS_PartialAirSystem_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1ALU_ECS_FullAirSystem_v() (Rte_AddrPar_0x2B_and_0x37_P1ALU_ECS_FullAirSystem_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1B9X_WirelessRC_Enable_v() (Rte_AddrPar_0x2B_and_0x37_P1B9X_WirelessRC_Enable_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define LevelControl_HMICtrl_START_SEC_CODE
# include "LevelControl_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData
#  define RTE_RUNNABLE_LevelControl_HMICtrl_20ms_runnable LevelControl_HMICtrl_20ms_runnable
#  define RTE_RUNNABLE_LevelControl_HMICtrl_Init LevelControl_HMICtrl_Init
# endif

# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, LevelControl_HMICtrl_CODE) DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData(P2VAR(uint8, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, LevelControl_HMICtrl_CODE) DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData(P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, LevelControl_HMICtrl_CODE) LevelControl_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, LevelControl_HMICtrl_CODE) LevelControl_HMICtrl_Init(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define LevelControl_HMICtrl_STOP_SEC_CODE
# include "LevelControl_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_P1DVZ_Data_P1DVZ_E_NOT_OK (1U)

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)

#  define RTE_E_Issm_IssRequest_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_LEVELCONTROL_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
