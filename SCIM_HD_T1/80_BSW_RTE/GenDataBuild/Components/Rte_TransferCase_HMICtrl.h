/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_TransferCase_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <TransferCase_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_TRANSFERCASE_HMICTRL_H
# define _RTE_TRANSFERCASE_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_TransferCase_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_TransferCase_HMICtrl_TransferCaseNeutral_DevInd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_TransferCaseNeutral_SwitchStat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_SwcActivation_IgnitionOn_IgnitionOn (1U)
#  define Rte_InitValue_TransferCaseNeutral_Ack_TransferCaseNeutral_Ack (3U)
#  define Rte_InitValue_TransferCaseNeutral_DevInd_DeviceIndication (3U)
#  define Rte_InitValue_TransferCaseNeutral_Req_TransferCaseNeutral_Req (7U)
#  define Rte_InitValue_TransferCaseNeutral_SwitchStat_PushButtonStatus (3U)
#  define Rte_InitValue_TransferCaseNeutral_status_TransferCaseNeutral_status (3U)
#  define Rte_InitValue_TransferCasePTOEngaged_TransferCasePTOEngaged (3U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_TransferCase_HMICtrl_TransferCaseNeutral_Ack_TransferCaseNeutral_Ack(P2VAR(TransferCaseNeutral_T, AUTOMATIC, RTE_TRANSFERCASE_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_TransferCase_HMICtrl_TransferCaseNeutral_status_TransferCaseNeutral_status(P2VAR(TransferCaseNeutral_status_T, AUTOMATIC, RTE_TRANSFERCASE_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_TransferCase_HMICtrl_TransferCasePTOEngaged_TransferCasePTOEngaged(P2VAR(NotEngagedEngaged_T, AUTOMATIC, RTE_TRANSFERCASE_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_TransferCase_HMICtrl_TransferCaseNeutral_Req_TransferCaseNeutral_Req(TransferCaseNeutral_Req2_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_SwcActivation_IgnitionOn_IgnitionOn Rte_Read_TransferCase_HMICtrl_SwcActivation_IgnitionOn_IgnitionOn
#  define Rte_Read_TransferCase_HMICtrl_SwcActivation_IgnitionOn_IgnitionOn(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_TransferCaseNeutral_Ack_TransferCaseNeutral_Ack Rte_Read_TransferCase_HMICtrl_TransferCaseNeutral_Ack_TransferCaseNeutral_Ack
#  define Rte_Read_TransferCaseNeutral_SwitchStat_PushButtonStatus Rte_Read_TransferCase_HMICtrl_TransferCaseNeutral_SwitchStat_PushButtonStatus
#  define Rte_Read_TransferCase_HMICtrl_TransferCaseNeutral_SwitchStat_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_TransferCaseNeutral_SwitchStat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_TransferCaseNeutral_status_TransferCaseNeutral_status Rte_Read_TransferCase_HMICtrl_TransferCaseNeutral_status_TransferCaseNeutral_status
#  define Rte_Read_TransferCasePTOEngaged_TransferCasePTOEngaged Rte_Read_TransferCase_HMICtrl_TransferCasePTOEngaged_TransferCasePTOEngaged


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_TransferCaseNeutral_DevInd_DeviceIndication Rte_Write_TransferCase_HMICtrl_TransferCaseNeutral_DevInd_DeviceIndication
#  define Rte_Write_TransferCase_HMICtrl_TransferCaseNeutral_DevInd_DeviceIndication(data) (Rte_TransferCase_HMICtrl_TransferCaseNeutral_DevInd_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_TransferCaseNeutral_Req_TransferCaseNeutral_Req Rte_Write_TransferCase_HMICtrl_TransferCaseNeutral_Req_TransferCaseNeutral_Req


# endif /* !defined(RTE_CORE) */


# define TransferCase_HMICtrl_START_SEC_CODE
# include "TransferCase_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_TransferCase_HMICtrl_20ms_runnable TransferCase_HMICtrl_20ms_runnable
# endif

FUNC(void, TransferCase_HMICtrl_CODE) TransferCase_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define TransferCase_HMICtrl_STOP_SEC_CODE
# include "TransferCase_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_TRANSFERCASE_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
