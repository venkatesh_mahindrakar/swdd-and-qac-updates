/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_ExtraBbTailLiftCrane_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <ExtraBbTailLiftCrane_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_EXTRABBTAILLIFTCRANE_HMICTRL_H
# define _RTE_EXTRABBTAILLIFTCRANE_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_ExtraBbTailLiftCrane_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraBbTailLiftCrane_HMICtrl_CraneSupply_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraBbTailLiftCrane_HMICtrl_TailLift_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_CranePushButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_CraneSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Living_Living; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_TailLiftPushButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_TailLiftSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_BBCraneRequest_BBCraneRequest (3U)
#  define Rte_InitValue_BBTailLiftRequest_BBTailLiftRequest (3U)
#  define Rte_InitValue_CranePushButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_CraneSupply_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_CraneSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_ExtraBBCraneStatus_ExtraBBCraneStatus (7U)
#  define Rte_InitValue_ExtraBBTailLiftStatus_ExtraBBTailLiftStatus (3U)
#  define Rte_InitValue_SwcActivation_Living_Living (1U)
#  define Rte_InitValue_TailLiftPushButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_TailLiftSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_TailLift_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_WRCCraneRequest_WRCCraneRequest (3U)
#  define Rte_InitValue_WRCTailLiftRequest_WRCTailLiftRequest (3U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraBbTailLiftCrane_HMICtrl_ExtraBBCraneStatus_ExtraBBCraneStatus(P2VAR(ExtraBBCraneStatus_T, AUTOMATIC, RTE_EXTRABBTAILLIFTCRANE_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraBbTailLiftCrane_HMICtrl_ExtraBBTailLiftStatus_ExtraBBTailLiftStatus(P2VAR(InactiveActive_T, AUTOMATIC, RTE_EXTRABBTAILLIFTCRANE_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraBbTailLiftCrane_HMICtrl_WRCCraneRequest_WRCCraneRequest(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_EXTRABBTAILLIFTCRANE_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExtraBbTailLiftCrane_HMICtrl_WRCTailLiftRequest_WRCTailLiftRequest(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_EXTRABBTAILLIFTCRANE_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExtraBbTailLiftCrane_HMICtrl_BBCraneRequest_BBCraneRequest(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExtraBbTailLiftCrane_HMICtrl_BBTailLiftRequest_BBTailLiftRequest(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_CranePushButtonStatus_PushButtonStatus Rte_Read_ExtraBbTailLiftCrane_HMICtrl_CranePushButtonStatus_PushButtonStatus
#  define Rte_Read_ExtraBbTailLiftCrane_HMICtrl_CranePushButtonStatus_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_CranePushButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_CraneSwitchStatus_A2PosSwitchStatus Rte_Read_ExtraBbTailLiftCrane_HMICtrl_CraneSwitchStatus_A2PosSwitchStatus
#  define Rte_Read_ExtraBbTailLiftCrane_HMICtrl_CraneSwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_CraneSwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ExtraBBCraneStatus_ExtraBBCraneStatus Rte_Read_ExtraBbTailLiftCrane_HMICtrl_ExtraBBCraneStatus_ExtraBBCraneStatus
#  define Rte_Read_ExtraBBTailLiftStatus_ExtraBBTailLiftStatus Rte_Read_ExtraBbTailLiftCrane_HMICtrl_ExtraBBTailLiftStatus_ExtraBBTailLiftStatus
#  define Rte_Read_SwcActivation_Living_Living Rte_Read_ExtraBbTailLiftCrane_HMICtrl_SwcActivation_Living_Living
#  define Rte_Read_ExtraBbTailLiftCrane_HMICtrl_SwcActivation_Living_Living(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Living_Living, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_TailLiftPushButtonStatus_PushButtonStatus Rte_Read_ExtraBbTailLiftCrane_HMICtrl_TailLiftPushButtonStatus_PushButtonStatus
#  define Rte_Read_ExtraBbTailLiftCrane_HMICtrl_TailLiftPushButtonStatus_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_TailLiftPushButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_TailLiftSwitchStatus_A2PosSwitchStatus Rte_Read_ExtraBbTailLiftCrane_HMICtrl_TailLiftSwitchStatus_A2PosSwitchStatus
#  define Rte_Read_ExtraBbTailLiftCrane_HMICtrl_TailLiftSwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_TailLiftSwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WRCCraneRequest_WRCCraneRequest Rte_Read_ExtraBbTailLiftCrane_HMICtrl_WRCCraneRequest_WRCCraneRequest
#  define Rte_Read_WRCTailLiftRequest_WRCTailLiftRequest Rte_Read_ExtraBbTailLiftCrane_HMICtrl_WRCTailLiftRequest_WRCTailLiftRequest


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_BBCraneRequest_BBCraneRequest Rte_Write_ExtraBbTailLiftCrane_HMICtrl_BBCraneRequest_BBCraneRequest
#  define Rte_Write_BBTailLiftRequest_BBTailLiftRequest Rte_Write_ExtraBbTailLiftCrane_HMICtrl_BBTailLiftRequest_BBTailLiftRequest
#  define Rte_Write_CraneSupply_DeviceIndication_DeviceIndication Rte_Write_ExtraBbTailLiftCrane_HMICtrl_CraneSupply_DeviceIndication_DeviceIndication
#  define Rte_Write_ExtraBbTailLiftCrane_HMICtrl_CraneSupply_DeviceIndication_DeviceIndication(data) (Rte_ExtraBbTailLiftCrane_HMICtrl_CraneSupply_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_TailLift_DeviceIndication_DeviceIndication Rte_Write_ExtraBbTailLiftCrane_HMICtrl_TailLift_DeviceIndication_DeviceIndication
#  define Rte_Write_ExtraBbTailLiftCrane_HMICtrl_TailLift_DeviceIndication_DeviceIndication(data) (Rte_ExtraBbTailLiftCrane_HMICtrl_TailLift_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_TailLiftHMIDeviceType_P1CW9_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CW9_TailLiftHMIDeviceType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_CraneHMIDeviceType_P1CXA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CXA_CraneHMIDeviceType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_TailLift_Crane_Act_P1CXB_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CXB_TailLift_Crane_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_CraneSwIndicationType_P1CXC_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CXC_CraneSwIndicationType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_TailLiftTimeoutForRequest_P1DWA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWA_TailLiftTimeoutForRequest_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1CW9_TailLiftHMIDeviceType_v() (Rte_AddrPar_0x2B_P1CW9_TailLiftHMIDeviceType_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CXA_CraneHMIDeviceType_v() (Rte_AddrPar_0x2B_P1CXA_CraneHMIDeviceType_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CXB_TailLift_Crane_Act_v() (Rte_AddrPar_0x2B_P1CXB_TailLift_Crane_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CXC_CraneSwIndicationType_v() (Rte_AddrPar_0x2B_P1CXC_CraneSwIndicationType_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DWA_TailLiftTimeoutForRequest_v() (Rte_AddrPar_0x2B_P1DWA_TailLiftTimeoutForRequest_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1B9X_WirelessRC_Enable_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1B9X_WirelessRC_Enable_v() (Rte_AddrPar_0x2B_and_0x37_P1B9X_WirelessRC_Enable_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define ExtraBbTailLiftCrane_HMICtrl_START_SEC_CODE
# include "ExtraBbTailLiftCrane_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_ExtraBbTailLiftCrane_HMICtrl_Crane_20ms_runnable ExtraBbTailLiftCrane_HMICtrl_Crane_20ms_runnable
#  define RTE_RUNNABLE_ExtraBbTailLiftCrane_HMICtrl_TailLift_20ms_runnable ExtraBbTailLiftCrane_HMICtrl_TailLift_20ms_runnable
# endif

FUNC(void, ExtraBbTailLiftCrane_HMICtrl_CODE) ExtraBbTailLiftCrane_HMICtrl_Crane_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, ExtraBbTailLiftCrane_HMICtrl_CODE) ExtraBbTailLiftCrane_HMICtrl_TailLift_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define ExtraBbTailLiftCrane_HMICtrl_STOP_SEC_CODE
# include "ExtraBbTailLiftCrane_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_EXTRABBTAILLIFTCRANE_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
