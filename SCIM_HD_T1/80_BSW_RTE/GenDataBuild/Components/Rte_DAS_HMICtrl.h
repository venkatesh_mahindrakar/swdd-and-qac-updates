/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_DAS_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <DAS_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_DAS_HMICTRL_H
# define _RTE_DAS_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_DAS_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_DAS_HMICtrl_DAS_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_DAS_SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_DASActivation_rqst_DASActivation_rqst (3U)
#  define Rte_InitValue_DASApplicationStatus_DASApplicationStatus (7U)
#  define Rte_InitValue_DAS_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_DAS_SwitchStatus_A2PosSwitchStatus (3U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_DAS_HMICtrl_DASApplicationStatus_DASApplicationStatus(P2VAR(DASSystemStatus_T, AUTOMATIC, RTE_DAS_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DAS_HMICtrl_DASActivation_rqst_DASActivation_rqst(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_DASApplicationStatus_DASApplicationStatus Rte_Read_DAS_HMICtrl_DASApplicationStatus_DASApplicationStatus
#  define Rte_Read_DAS_SwitchStatus_A2PosSwitchStatus Rte_Read_DAS_HMICtrl_DAS_SwitchStatus_A2PosSwitchStatus
#  define Rte_Read_DAS_HMICtrl_DAS_SwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_DAS_SwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_DASActivation_rqst_DASActivation_rqst Rte_Write_DAS_HMICtrl_DASActivation_rqst_DASActivation_rqst
#  define Rte_Write_DAS_DeviceIndication_DeviceIndication Rte_Write_DAS_HMICtrl_DAS_DeviceIndication_DeviceIndication
#  define Rte_Write_DAS_HMICtrl_DAS_DeviceIndication_DeviceIndication(data) (Rte_DAS_HMICtrl_DAS_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


# endif /* !defined(RTE_CORE) */


# define DAS_HMICtrl_START_SEC_CODE
# include "DAS_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DAS_HMICtrl_20ms_Runnable DAS_HMICtrl_20ms_Runnable
# endif

FUNC(void, DAS_HMICtrl_CODE) DAS_HMICtrl_20ms_Runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define DAS_HMICtrl_STOP_SEC_CODE
# include "DAS_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_DAS_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
