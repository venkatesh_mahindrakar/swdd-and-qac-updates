/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_VEC_RomTest.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <VEC_RomTest>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_VEC_ROMTEST_H
# define _RTE_VEC_ROMTEST_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_VEC_RomTest_Type.h"
# include "Rte_DataHandleType.h"

# include "Rte_Hook.h"


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/


# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CSM_APPL_CODE) Csm_SignatureVerifyFinish(Csm_ConfigIdType parg0, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) signatureBuffer, uint32 signatureLength, P2VAR(Csm_VerifyResultType, AUTOMATIC, RTE_CSM_APPL_VAR) resultBuffer); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  else
FUNC(Std_ReturnType, RTE_CSM_APPL_CODE) Csm_SignatureVerifyFinish(Csm_ConfigIdType parg0, P2CONST(SignatureVerifyDataBuffer, AUTOMATIC, RTE_CSM_APPL_DATA) signatureBuffer, uint32 signatureLength, P2VAR(Csm_VerifyResultType, AUTOMATIC, RTE_CSM_APPL_VAR) resultBuffer); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  endif
#  define RTE_STOP_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CsmSignatureVerify_SignatureVerifyFinish(arg1, arg2, arg3) (Csm_SignatureVerifyFinish((Csm_ConfigIdType)3, arg1, arg2, arg3)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_CSM_APPL_CODE) Csm_SignatureVerifyStart(Csm_ConfigIdType parg0, P2CONST(AsymPublicKeyType, AUTOMATIC, RTE_CSM_APPL_DATA) key); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CsmSignatureVerify_SignatureVerifyStart(arg1) (Csm_SignatureVerifyStart((Csm_ConfigIdType)3, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CSM_APPL_CODE) Csm_SignatureVerifyUpdate(Csm_ConfigIdType parg0, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) dataBuffer, uint32 dataLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  else
FUNC(Std_ReturnType, RTE_CSM_APPL_CODE) Csm_SignatureVerifyUpdate(Csm_ConfigIdType parg0, P2CONST(SignatureVerifyDataBuffer, AUTOMATIC, RTE_CSM_APPL_DATA) dataBuffer, uint32 dataLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  endif
#  define RTE_STOP_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CsmSignatureVerify_SignatureVerifyUpdate(arg1, arg2) (Csm_SignatureVerifyUpdate((Csm_ConfigIdType)3, arg1, arg2)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Exclusive Areas
 *********************************************************************************************************************/

#  define Rte_Enter_VEC_ExclusiveArea() SuspendAllInterrupts()  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */

#  define Rte_Exit_VEC_ExclusiveArea() ResumeAllInterrupts()  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */


# endif /* !defined(RTE_CORE) */


# define VEC_RomTest_START_SEC_CODE
# include "VEC_RomTest_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_VEC_CallbackNotification VEC_CallbackNotification
#  define RTE_RUNNABLE_VEC_GetBlockStatus VEC_GetBlockStatus
#  define RTE_RUNNABLE_VEC_RomTest_Init VEC_RomTest_Init
#  define RTE_RUNNABLE_VEC_RomTest_MainFunction VEC_RomTest_MainFunction
# endif

FUNC(Std_ReturnType, VEC_RomTest_CODE) VEC_CallbackNotification(Csm_ReturnType retVal); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, VEC_RomTest_CODE) VEC_GetBlockStatus(UInt8 blockNr, P2VAR(tVecRomTestBlockStatus, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) status); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, VEC_RomTest_CODE) VEC_RomTest_Init(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, VEC_RomTest_CODE) VEC_RomTest_MainFunction(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define VEC_RomTest_STOP_SEC_CODE
# include "VEC_RomTest_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_CsmCallback_CSM_E_NOT_OK (1U)

#  define RTE_E_CsmSignatureVerify_CSM_E_BUSY (2U)

#  define RTE_E_CsmSignatureVerify_CSM_E_NOT_OK (1U)

#  define RTE_E_CsmSignatureVerify_CSM_E_SMALL_BUFFER (3U)

#  define RTE_E_VEC_RomTestStatus_RTE_E_VEC_BlockStatus_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_VEC_ROMTEST_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
