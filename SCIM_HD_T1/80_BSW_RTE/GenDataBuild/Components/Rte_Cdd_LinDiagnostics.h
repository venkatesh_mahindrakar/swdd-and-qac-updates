/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Cdd_LinDiagnostics.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <Cdd_LinDiagnostics>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_CDD_LINDIAGNOSTICS_H
# define _RTE_CDD_LINDIAGNOSTICS_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_Cdd_LinDiagnostics_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Cdd_LinDiagnostics_LinDiagRequestFlag_CCNADRequest; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(uint8, RTE_VAR_NOINIT) Rte_Cdd_LinDiagnostics_LinDiagRequestFlag_PNSNRequest; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN1_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN2_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN3_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN4_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN5_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DiagActiveState_T, RTE_VAR_NOINIT) Rte_DiagnosticComponent_DiagActiveState_isDiagActive; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ComMode_LIN1_ComMode_LIN (0U)
#  define Rte_InitValue_ComMode_LIN2_ComMode_LIN (0U)
#  define Rte_InitValue_ComMode_LIN3_ComMode_LIN (0U)
#  define Rte_InitValue_ComMode_LIN4_ComMode_LIN (0U)
#  define Rte_InitValue_ComMode_LIN5_ComMode_LIN (0U)
#  define Rte_InitValue_DiagActiveState_P_isDiagActive (0U)
#  define Rte_InitValue_LinDiagRequestFlag_CCNADRequest (0U)
#  define Rte_InitValue_LinDiagRequestFlag_PNSNRequest (0U)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ComMode_LIN1_ComMode_LIN Rte_Read_Cdd_LinDiagnostics_ComMode_LIN1_ComMode_LIN
#  define Rte_Read_Cdd_LinDiagnostics_ComMode_LIN1_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN1_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ComMode_LIN2_ComMode_LIN Rte_Read_Cdd_LinDiagnostics_ComMode_LIN2_ComMode_LIN
#  define Rte_Read_Cdd_LinDiagnostics_ComMode_LIN2_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN2_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ComMode_LIN3_ComMode_LIN Rte_Read_Cdd_LinDiagnostics_ComMode_LIN3_ComMode_LIN
#  define Rte_Read_Cdd_LinDiagnostics_ComMode_LIN3_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN3_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ComMode_LIN4_ComMode_LIN Rte_Read_Cdd_LinDiagnostics_ComMode_LIN4_ComMode_LIN
#  define Rte_Read_Cdd_LinDiagnostics_ComMode_LIN4_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN4_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ComMode_LIN5_ComMode_LIN Rte_Read_Cdd_LinDiagnostics_ComMode_LIN5_ComMode_LIN
#  define Rte_Read_Cdd_LinDiagnostics_ComMode_LIN5_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN5_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DiagActiveState_P_isDiagActive Rte_Read_Cdd_LinDiagnostics_DiagActiveState_P_isDiagActive
#  define Rte_Read_Cdd_LinDiagnostics_DiagActiveState_P_isDiagActive(data) (*(data) = Rte_DiagnosticComponent_DiagActiveState_isDiagActive, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_LinDiagRequestFlag_CCNADRequest Rte_Write_Cdd_LinDiagnostics_LinDiagRequestFlag_CCNADRequest
#  define Rte_Write_Cdd_LinDiagnostics_LinDiagRequestFlag_CCNADRequest(data) (Rte_Cdd_LinDiagnostics_LinDiagRequestFlag_CCNADRequest = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LinDiagRequestFlag_PNSNRequest Rte_Write_Cdd_LinDiagnostics_LinDiagRequestFlag_PNSNRequest
#  define Rte_Write_Cdd_LinDiagnostics_LinDiagRequestFlag_PNSNRequest(data) (Rte_Cdd_LinDiagnostics_LinDiagRequestFlag_PNSNRequest = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CDD_LINTPHANDLING_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_CDD_LINTPHANDLING_APPL_CODE) Cdd_LinTpClearRequest(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_CDD_LINTPHANDLING_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CddLinClearRequest_ClearRequest() (Cdd_LinTpClearRequest(), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_CDD_LINTPHANDLING_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_CDD_LINTPHANDLING_APPL_CODE) Cdd_LinTpTransmit(uint8 TxId, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINTPHANDLING_APPL_VAR) TxData, uint8 Length); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_CDD_LINTPHANDLING_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CddLinTxHandling_Transmit(arg1, arg2, arg3) (Cdd_LinTpTransmit(arg1, arg2, arg3), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_LIN_topology_P1AJR_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1AJR_LIN_topology_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1AJR_LIN_topology_v() (Rte_AddrPar_0x2B_P1AJR_LIN_topology_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define Cdd_LinDiagnostics_START_SEC_CODE
# include "Cdd_LinDiagnostics_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_CddLinDiagServices_FSPAssignReq CddLinDiagServices_FSPAssignReq
#  define RTE_RUNNABLE_CddLinDiagServices_FSPAssignResp CddLinDiagServices_FSPAssignResp
#  define RTE_RUNNABLE_CddLinDiagServices_SlaveNodePnSnReq CddLinDiagServices_SlaveNodePnSnReq
#  define RTE_RUNNABLE_CddLinDiagServices_SlaveNodePnSnResp CddLinDiagServices_SlaveNodePnSnResp
#  define RTE_RUNNABLE_CddLinRxHandling_ReceiveIndication CddLinRxHandling_ReceiveIndication
#  define RTE_RUNNABLE_Cdd_LinDiagnostics_10ms_Runnable Cdd_LinDiagnostics_10ms_Runnable
# endif

FUNC(void, Cdd_LinDiagnostics_CODE) CddLinDiagServices_FSPAssignReq(LinDiagBusInfo LinBusInfo, LinDiagRequest_T RequestType); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, Cdd_LinDiagnostics_CODE) CddLinDiagServices_FSPAssignResp(P2VAR(LinDiagServiceStatus, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) pDiagServiceStatus, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) pAvailableFSPCount, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) pFspErrorStatus, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) pFspNvData); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, Cdd_LinDiagnostics_CODE) CddLinDiagServices_SlaveNodePnSnReq(LinDiagBusInfo LinBusInfo); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, Cdd_LinDiagnostics_CODE) CddLinDiagServices_SlaveNodePnSnResp(P2VAR(LinDiagServiceStatus, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) DiagServiceStatus, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) NoOfLinSlaves, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) LinDiagRespPNSN); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, Cdd_LinDiagnostics_CODE) CddLinRxHandling_ReceiveIndication(uint8 TxId, uint8 SubServiceId, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) RxData, uint8 Length, CddLinTp_Status Status); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, Cdd_LinDiagnostics_CODE) Cdd_LinDiagnostics_10ms_Runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define Cdd_LinDiagnostics_STOP_SEC_CODE
# include "Cdd_LinDiagnostics_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_CDD_LINDIAGNOSTICS_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
