/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_ExteriorLightPanel_2_LINMastCtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <ExteriorLightPanel_2_LINMastCtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_H
# define _RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_ExteriorLightPanel_2_LINMastCtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ExteriorLightPanel_2_LINMastCtrl_RearWorkProjector_ButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN4_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DiagActiveState_T, RTE_VAR_NOINIT) Rte_DiagnosticComponent_DiagActiveState_isDiagActive; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_RearWorkProjector_Indication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ComMode_LIN4_ComMode_LIN (7U)
#  define Rte_InitValue_DRL_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_DiagActiveState_isDiagActive (0U)
#  define Rte_InitValue_DiagInfoELCP2_DiagInfo (0U)
#  define Rte_InitValue_FogLightFront_ButtonStatus_2_PushButtonStatus (3U)
#  define Rte_InitValue_FogLightRear_ButtonStatus_2_PushButtonStatus (3U)
#  define Rte_InitValue_HeadLampUpDown_SwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LIN_DRL_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_FogLightFront_ButtonStat_2_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_FogLightRear_ButtonStat_2_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_HeadLampUpDown_SwitchStatu_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LIN_LightMode_Status_2_FreeWheel_Status (15U)
#  define Rte_InitValue_LIN_RearWorkProjector_BtnStat_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_RearWorkProjector_Indicati_DeviceIndication (0U)
#  define Rte_InitValue_LightMode_Status_2_FreeWheel_Status (15U)
#  define Rte_InitValue_RearWorkProjector_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_RearWorkProjector_Indication_DeviceIndication (3U)
#  define Rte_InitValue_ResponseErrorELCP2_ResponseErrorELCP2 (FALSE)
# endif


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Buffers for inter-runnable variables
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_ExteriorLightPanel_2_LINMastCtrl_Irv_IOCTL_ELCP2LinCtrl; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
# endif /* !defined(RTE_CORE) */


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExteriorLightPanel_2_LINMastCtrl_LIN_LightMode_Status_2_FreeWheel_Status(P2VAR(FreeWheel_Status_T, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExteriorLightPanel_2_LINMastCtrl_ResponseErrorELCP2_ResponseErrorELCP2(P2VAR(ResponseErrorELCP2_T, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_2_LINMastCtrl_DRL_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_2_LINMastCtrl_FogLightFront_ButtonStatus_2_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_2_LINMastCtrl_FogLightRear_ButtonStatus_2_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_2_LINMastCtrl_HeadLampUpDown_SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_2_LINMastCtrl_LIN_RearWorkProjector_Indicati_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_2_LINMastCtrl_LightMode_Status_2_FreeWheel_Status(FreeWheel_Status_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ComMode_LIN4_ComMode_LIN Rte_Read_ExteriorLightPanel_2_LINMastCtrl_ComMode_LIN4_ComMode_LIN
#  define Rte_Read_ExteriorLightPanel_2_LINMastCtrl_ComMode_LIN4_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN4_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DiagActiveState_isDiagActive Rte_Read_ExteriorLightPanel_2_LINMastCtrl_DiagActiveState_isDiagActive
#  define Rte_Read_ExteriorLightPanel_2_LINMastCtrl_DiagActiveState_isDiagActive(data) (*(data) = Rte_DiagnosticComponent_DiagActiveState_isDiagActive, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DiagInfoELCP2_DiagInfo Rte_Read_ExteriorLightPanel_2_LINMastCtrl_DiagInfoELCP2_DiagInfo
#  define Rte_Read_ExteriorLightPanel_2_LINMastCtrl_DiagInfoELCP2_DiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_DiagInfoELCP2_oELCP2toCIOM_L4_oLIN03_e304f264_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_DRL_ButtonStatus_PushButtonStatus Rte_Read_ExteriorLightPanel_2_LINMastCtrl_LIN_DRL_ButtonStatus_PushButtonStatus
#  define Rte_Read_ExteriorLightPanel_2_LINMastCtrl_LIN_DRL_ButtonStatus_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_DRL_ButtonStatus_oELCP2toCIOM_L4_oLIN03_e8cb5735_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_FogLightFront_ButtonStat_2_PushButtonStatus Rte_Read_ExteriorLightPanel_2_LINMastCtrl_LIN_FogLightFront_ButtonStat_2_PushButtonStatus
#  define Rte_Read_ExteriorLightPanel_2_LINMastCtrl_LIN_FogLightFront_ButtonStat_2_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_FogLightFront_ButtonStat_2_oELCP2toCIOM_L4_oLIN03_ea214d6e_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_FogLightRear_ButtonStat_2_PushButtonStatus Rte_Read_ExteriorLightPanel_2_LINMastCtrl_LIN_FogLightRear_ButtonStat_2_PushButtonStatus
#  define Rte_Read_ExteriorLightPanel_2_LINMastCtrl_LIN_FogLightRear_ButtonStat_2_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_FogLightRear_ButtonStat_2_oELCP2toCIOM_L4_oLIN03_1f3e5f46_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_HeadLampUpDown_SwitchStatu_A3PosSwitchStatus Rte_Read_ExteriorLightPanel_2_LINMastCtrl_LIN_HeadLampUpDown_SwitchStatu_A3PosSwitchStatus
#  define Rte_Read_ExteriorLightPanel_2_LINMastCtrl_LIN_HeadLampUpDown_SwitchStatu_A3PosSwitchStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_HeadLampUpDown_SwitchStatu_oELCP2toCIOM_L4_oLIN03_ea48cea1_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_LightMode_Status_2_FreeWheel_Status Rte_Read_ExteriorLightPanel_2_LINMastCtrl_LIN_LightMode_Status_2_FreeWheel_Status
#  define Rte_Read_LIN_RearWorkProjector_BtnStat_PushButtonStatus Rte_Read_ExteriorLightPanel_2_LINMastCtrl_LIN_RearWorkProjector_BtnStat_PushButtonStatus
#  define Rte_Read_ExteriorLightPanel_2_LINMastCtrl_LIN_RearWorkProjector_BtnStat_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_RearWorkProjector_BtnStat_oELCP2toCIOM_L4_oLIN03_bb9bb1f8_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RearWorkProjector_Indication_DeviceIndication Rte_Read_ExteriorLightPanel_2_LINMastCtrl_RearWorkProjector_Indication_DeviceIndication
#  define Rte_Read_ExteriorLightPanel_2_LINMastCtrl_RearWorkProjector_Indication_DeviceIndication(data) (*(data) = Rte_ExtraLighting_HMICtrl_RearWorkProjector_Indication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ResponseErrorELCP2_ResponseErrorELCP2 Rte_Read_ExteriorLightPanel_2_LINMastCtrl_ResponseErrorELCP2_ResponseErrorELCP2


/**********************************************************************************************************************
 * Rte_IsUpdated_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_IsUpdated_LIN_LightMode_Status_2_FreeWheel_Status Rte_IsUpdated_ExteriorLightPanel_2_LINMastCtrl_LIN_LightMode_Status_2_FreeWheel_Status
#  define Rte_IsUpdated_ExteriorLightPanel_2_LINMastCtrl_LIN_LightMode_Status_2_FreeWheel_Status() ((Rte_RxUpdateFlags.Rte_RxUpdate_ExteriorLightPanel_2_LINMastCtrl_LIN_LightMode_Status_2_FreeWheel_Status == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_DRL_ButtonStatus_PushButtonStatus Rte_Write_ExteriorLightPanel_2_LINMastCtrl_DRL_ButtonStatus_PushButtonStatus
#  define Rte_Write_FogLightFront_ButtonStatus_2_PushButtonStatus Rte_Write_ExteriorLightPanel_2_LINMastCtrl_FogLightFront_ButtonStatus_2_PushButtonStatus
#  define Rte_Write_FogLightRear_ButtonStatus_2_PushButtonStatus Rte_Write_ExteriorLightPanel_2_LINMastCtrl_FogLightRear_ButtonStatus_2_PushButtonStatus
#  define Rte_Write_HeadLampUpDown_SwitchStatus_A3PosSwitchStatus Rte_Write_ExteriorLightPanel_2_LINMastCtrl_HeadLampUpDown_SwitchStatus_A3PosSwitchStatus
#  define Rte_Write_LIN_RearWorkProjector_Indicati_DeviceIndication Rte_Write_ExteriorLightPanel_2_LINMastCtrl_LIN_RearWorkProjector_Indicati_DeviceIndication
#  define Rte_Write_LightMode_Status_2_FreeWheel_Status Rte_Write_ExteriorLightPanel_2_LINMastCtrl_LightMode_Status_2_FreeWheel_Status
#  define Rte_Write_RearWorkProjector_ButtonStatus_PushButtonStatus Rte_Write_ExteriorLightPanel_2_LINMastCtrl_RearWorkProjector_ButtonStatus_PushButtonStatus
#  define Rte_Write_ExteriorLightPanel_2_LINMastCtrl_RearWorkProjector_ButtonStatus_PushButtonStatus(data) (Rte_ExteriorLightPanel_2_LINMastCtrl_RearWorkProjector_ButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1F0B_16_ELCP2_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)269, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1F0B_17_ELCP__VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)270, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1F0B_44_ELCP2_RAM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)271, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1F0B_45_ELCP_FLASH_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)272, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1F0B_46_ELCP_EEPROM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)273, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1F0B_49_ELCP2_HWFAIL_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)274, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1F0B_94_ELCP2_SWFAIL_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)275, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1F0C_87_ELCP2Link_NoResp_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)276, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_ActivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_ExteriorLightsRequest2_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)22)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_DeactivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_ExteriorLightsRequest2_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)22)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_GetIssState(Issm_UserHandleType parg0, P2VAR(Issm_IssStateType, AUTOMATIC, RTE_ISSM_APPL_VAR) issState); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_ExteriorLightsRequest2_GetIssState(arg1) (Issm_GetIssState((Issm_UserHandleType)22, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_WLight_InputELCP_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)48)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_WLight_InputELCP_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)48)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_WLight_InputELCP_GetIssState(arg1) (Issm_GetIssState((Issm_UserHandleType)48, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP2LinCtrl() \
  Rte_Irv_ExteriorLightPanel_2_LINMastCtrl_Irv_IOCTL_ELCP2LinCtrl
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ELCP2LinCtrl(data) \
  (Rte_Irv_ExteriorLightPanel_2_LINMastCtrl_Irv_IOCTL_ELCP2LinCtrl = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_ELCP2LinCtrl(data) \
  (Rte_Irv_ExteriorLightPanel_2_LINMastCtrl_Irv_IOCTL_ELCP2LinCtrl = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_ExteriorLightPanel_2_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP2LinCtrl() \
  Rte_Irv_ExteriorLightPanel_2_LINMastCtrl_Irv_IOCTL_ELCP2LinCtrl
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_ExteriorLightPanel_2_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP2LinCtrl(data) \
  (Rte_Irv_ExteriorLightPanel_2_LINMastCtrl_Irv_IOCTL_ELCP2LinCtrl = (data))
/* PRQA L:L1 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VR5_ELCP2_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1VR5_ELCP2_Installed_v() (Rte_AddrPar_0x2B_P1VR5_ELCP2_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define ExteriorLightPanel_2_LINMastCtrl_START_SEC_CODE
# include "ExteriorLightPanel_2_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_FreezeCurrentState DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData
#  define RTE_RUNNABLE_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReturnControlToECU DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment
#  define RTE_RUNNABLE_ExteriorLightPanel_2_LINMastCtrl_20ms_runnable ExteriorLightPanel_2_LINMastCtrl_20ms_runnable
# endif

FUNC(Std_ReturnType, ExteriorLightPanel_2_LINMastCtrl_CODE) DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, ExteriorLightPanel_2_LINMastCtrl_CODE) DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, ExteriorLightPanel_2_LINMastCtrl_CODE) DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, ExteriorLightPanel_2_LINMastCtrl_CODE) DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, ExteriorLightPanel_2_LINMastCtrl_CODE) DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, ExteriorLightPanel_2_LINMastCtrl_CODE) DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, ExteriorLightPanel_2_LINMastCtrl_CODE) ExteriorLightPanel_2_LINMastCtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define ExteriorLightPanel_2_LINMastCtrl_STOP_SEC_CODE
# include "ExteriorLightPanel_2_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_P1EOT_Data_P1EOT_E_NOT_OK (1U)

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)

#  define RTE_E_Issm_IssRequest_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
