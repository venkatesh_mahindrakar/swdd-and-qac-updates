/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_BunkUserInterfaceHigh2_LINMaCtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <BunkUserInterfaceHigh2_LINMaCtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_H
# define _RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_BunkUserInterfaceHigh2_LINMaCtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightActvnBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2LockButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN1_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(IntLghtLvlIndScaled_cmd_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_AudioSystemStatus_AudioSystemStatus (3U)
#  define Rte_InitValue_AudioVolumeIndicationCmd_VolumeValueType (0U)
#  define Rte_InitValue_BTStatus_BTStatus (3U)
#  define Rte_InitValue_BunkH2AudioOnOff_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2Fade_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2IntLightActvnBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2LockButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2OnOFF_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2ParkHeater_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2Phone_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2TempDec_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2TempInc_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2VolumeDown_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2VolumeUp_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ComMode_LIN1_ComMode_LIN (7U)
#  define Rte_InitValue_DiagInfoLECM2_DiagInfo (0U)
#  define Rte_InitValue_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd (15U)
#  define Rte_InitValue_LIN_AudioSystemStatus_AudioSystemStatus (3U)
#  define Rte_InitValue_LIN_AudioVolumeIndicationCmd_VolumeValueType (0U)
#  define Rte_InitValue_LIN_BTStatus_BTStatus (3U)
#  define Rte_InitValue_LIN_BunkH2AudioOnOff_ButtonSta_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2Fade_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2IntLightActvnBtn_sta_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2IntLightDecBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2IntLightIncBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2LockButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2OnOFF_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2ParkHeater_ButtonSta_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2Phone_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2PowerWinCloseDSBtn_s_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2PowerWinClosePSBtn_s_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2PowerWinOpenDSBtn_st_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2PowerWinOpenPSBtn_st_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2RoofhatchCloseBtn_St_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2RoofhatchOpenBtn_Sta_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2TempDec_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2TempInc_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2VolumeDown_ButtonSta_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkH2VolumeUp_ButtonStatu_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd (0U)
#  define Rte_InitValue_LIN_PhoneButtonIndication_cmd_DeviceIndication (0U)
#  define Rte_InitValue_PhoneButtonIndication_cmd_DeviceIndication (3U)
#  define Rte_InitValue_ResponseErrorLECM2_ResponseErrorLECM2 (FALSE)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat(P2VAR(AlmClkCurAlarm_stat_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_AudioSystemStatus_AudioSystemStatus(P2VAR(OffOn_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_AudioVolumeIndicationCmd_VolumeValueType(P2VAR(VolumeValueType_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_BTStatus_BTStatus(P2VAR(BTStatus_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_IntLghtModeInd_cmd_InteriorLightMode(P2VAR(InteriorLightMode_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst(P2VAR(AlmClkSetCurAlm_rqst_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2PHTi_rqs_SetParkHtrTmr_rqst(P2VAR(SetParkHtrTmr_rqst_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2PowerWinCloseDSBtn_s_PushButtonStatus(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2PowerWinClosePSBtn_s_PushButtonStatus(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2PowerWinOpenDSBtn_st_PushButtonStatus(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2PowerWinOpenPSBtn_st_PushButtonStatus(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_PhoneButtonIndication_cmd_DeviceIndication(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_ResponseErrorLECM2_ResponseErrorLECM2(P2VAR(ResponseErrorLECM2_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst(P2CONST(AlmClkSetCurAlm_rqst_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2AudioOnOff_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2Fade_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OnOFF_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2PHTimer_rqst_SetParkHtrTmr_rqst(P2CONST(SetParkHtrTmr_rqst_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2ParkHeater_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2Phone_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2TempDec_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2TempInc_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2VolumeDown_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2VolumeUp_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_LIN_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat(P2CONST(AlmClkCurAlarm_stat_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_LIN_AudioSystemStatus_AudioSystemStatus(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_LIN_AudioVolumeIndicationCmd_VolumeValueType(VolumeValueType_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BTStatus_BTStatus(BTStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_LIN_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(IntLghtLvlIndScaled_cmd_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_LIN_IntLghtModeInd_cmd_InteriorLightMode(P2CONST(InteriorLightMode_T, AUTOMATIC, RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_LIN_PhoneButtonIndication_cmd_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat
#  define Rte_Read_AudioSystemStatus_AudioSystemStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_AudioSystemStatus_AudioSystemStatus
#  define Rte_Read_AudioVolumeIndicationCmd_VolumeValueType Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_AudioVolumeIndicationCmd_VolumeValueType
#  define Rte_Read_BTStatus_BTStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_BTStatus_BTStatus
#  define Rte_Read_ComMode_LIN1_ComMode_LIN Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_ComMode_LIN1_ComMode_LIN
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_ComMode_LIN1_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN1_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DiagInfoLECM2_DiagInfo Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_DiagInfoLECM2_DiagInfo
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_DiagInfoLECM2_DiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_DiagInfoLECM2_oLECM2toCIOM_FR1_L1_oLIN00_427132ed_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(data) (*(data) = Rte_InteriorLights_HMICtrl_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLghtModeInd_cmd_InteriorLightMode Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_IntLghtModeInd_cmd_InteriorLightMode
#  define Rte_Read_LIN_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst
#  define Rte_Read_LIN_BunkH2AudioOnOff_ButtonSta_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2AudioOnOff_ButtonSta_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2AudioOnOff_ButtonSta_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkH2AudioOnOff_ButtonSta_oLECM2toCIOM_FR1_L1_oLIN00_8085e670_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkH2Fade_ButtonStatus_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2Fade_ButtonStatus_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2Fade_ButtonStatus_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkH2Fade_ButtonStatus_oLECM2toCIOM_FR1_L1_oLIN00_64ae31ec_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkH2IntLightActvnBtn_sta_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2IntLightActvnBtn_sta_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2IntLightActvnBtn_sta_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkH2IntLightActvnBtn_sta_oLECM2toCIOM_FR1_L1_oLIN00_7ae32ea6_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkH2IntLightDecBtn_stat_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2IntLightDecBtn_stat_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2IntLightDecBtn_stat_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkH2IntLightDecBtn_stat_oLECM2toCIOM_FR1_L1_oLIN00_445fc32a_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkH2IntLightIncBtn_stat_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2IntLightIncBtn_stat_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2IntLightIncBtn_stat_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkH2IntLightIncBtn_stat_oLECM2toCIOM_FR1_L1_oLIN00_a3c1ab0b_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkH2LockButtonStatus_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2LockButtonStatus_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2LockButtonStatus_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkH2LockButtonStatus_oLECM2toCIOM_FR1_L1_oLIN00_d71d2857_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkH2OnOFF_ButtonStatus_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2OnOFF_ButtonStatus_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2OnOFF_ButtonStatus_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkH2OnOFF_ButtonStatus_oLECM2toCIOM_FR1_L1_oLIN00_6a259b90_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkH2PHTi_rqs_SetParkHtrTmr_rqst Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2PHTi_rqs_SetParkHtrTmr_rqst
#  define Rte_Read_LIN_BunkH2ParkHeater_ButtonSta_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2ParkHeater_ButtonSta_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2ParkHeater_ButtonSta_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkH2ParkHeater_ButtonSta_oLECM2toCIOM_FR1_L1_oLIN00_2be63ba3_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkH2Phone_ButtonStatus_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2Phone_ButtonStatus_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2Phone_ButtonStatus_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkH2Phone_ButtonStatus_oLECM2toCIOM_FR1_L1_oLIN00_99e0fe87_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkH2PowerWinCloseDSBtn_s_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2PowerWinCloseDSBtn_s_PushButtonStatus
#  define Rte_Read_LIN_BunkH2PowerWinClosePSBtn_s_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2PowerWinClosePSBtn_s_PushButtonStatus
#  define Rte_Read_LIN_BunkH2PowerWinOpenDSBtn_st_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2PowerWinOpenDSBtn_st_PushButtonStatus
#  define Rte_Read_LIN_BunkH2PowerWinOpenPSBtn_st_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2PowerWinOpenPSBtn_st_PushButtonStatus
#  define Rte_Read_LIN_BunkH2RoofhatchCloseBtn_St_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2RoofhatchCloseBtn_St_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2RoofhatchCloseBtn_St_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkH2RoofhatchCloseBtn_St_oLECM2toCIOM_FR1_L1_oLIN00_c184d263_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkH2RoofhatchOpenBtn_Sta_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2RoofhatchOpenBtn_Sta_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2RoofhatchOpenBtn_Sta_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkH2RoofhatchOpenBtn_Sta_oLECM2toCIOM_FR1_L1_oLIN00_dd2458bc_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkH2TempDec_ButtonStatus_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2TempDec_ButtonStatus_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2TempDec_ButtonStatus_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkH2TempDec_ButtonStatus_oLECM2toCIOM_FR1_L1_oLIN00_857664ae_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkH2TempInc_ButtonStatus_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2TempInc_ButtonStatus_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2TempInc_ButtonStatus_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkH2TempInc_ButtonStatus_oLECM2toCIOM_FR1_L1_oLIN00_61f74d27_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkH2VolumeDown_ButtonSta_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2VolumeDown_ButtonSta_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2VolumeDown_ButtonSta_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkH2VolumeDown_ButtonSta_oLECM2toCIOM_FR1_L1_oLIN00_e04f5721_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkH2VolumeUp_ButtonStatu_PushButtonStatus Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2VolumeUp_ButtonStatu_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BunkH2VolumeUp_ButtonStatu_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkH2VolumeUp_ButtonStatu_oLECM2toCIOM_FR1_L1_oLIN00_92cefa8d_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_PhoneButtonIndication_cmd_DeviceIndication Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_PhoneButtonIndication_cmd_DeviceIndication
#  define Rte_Read_ResponseErrorLECM2_ResponseErrorLECM2 Rte_Read_BunkUserInterfaceHigh2_LINMaCtrl_ResponseErrorLECM2_ResponseErrorLECM2


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst
#  define Rte_Write_BunkH2AudioOnOff_ButtonStatus_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2AudioOnOff_ButtonStatus_PushButtonStatus
#  define Rte_Write_BunkH2Fade_ButtonStatus_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2Fade_ButtonStatus_PushButtonStatus
#  define Rte_Write_BunkH2IntLightActvnBtn_stat_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightActvnBtn_stat_PushButtonStatus
#  define Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightActvnBtn_stat_PushButtonStatus(data) (Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightActvnBtn_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus
#  define Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus(data) (Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus
#  define Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus(data) (Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_BunkH2LockButtonStatus_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2LockButtonStatus_PushButtonStatus
#  define Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2LockButtonStatus_PushButtonStatus(data) (Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2LockButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_BunkH2OnOFF_ButtonStatus_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OnOFF_ButtonStatus_PushButtonStatus
#  define Rte_Write_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus
#  define Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus(data) (Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus
#  define Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus(data) (Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus
#  define Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus(data) (Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus
#  define Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus(data) (Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_BunkH2PHTimer_rqst_SetParkHtrTmr_rqst Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2PHTimer_rqst_SetParkHtrTmr_rqst
#  define Rte_Write_BunkH2ParkHeater_ButtonStatus_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2ParkHeater_ButtonStatus_PushButtonStatus
#  define Rte_Write_BunkH2Phone_ButtonStatus_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2Phone_ButtonStatus_PushButtonStatus
#  define Rte_Write_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus
#  define Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus(data) (Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus
#  define Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus(data) (Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_BunkH2TempDec_ButtonStatus_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2TempDec_ButtonStatus_PushButtonStatus
#  define Rte_Write_BunkH2TempInc_ButtonStatus_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2TempInc_ButtonStatus_PushButtonStatus
#  define Rte_Write_BunkH2VolumeDown_ButtonStatus_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2VolumeDown_ButtonStatus_PushButtonStatus
#  define Rte_Write_BunkH2VolumeUp_ButtonStatus_PushButtonStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2VolumeUp_ButtonStatus_PushButtonStatus
#  define Rte_Write_LIN_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_LIN_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat
#  define Rte_Write_LIN_AudioSystemStatus_AudioSystemStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_LIN_AudioSystemStatus_AudioSystemStatus
#  define Rte_Write_LIN_AudioVolumeIndicationCmd_VolumeValueType Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_LIN_AudioVolumeIndicationCmd_VolumeValueType
#  define Rte_Write_LIN_BTStatus_BTStatus Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_LIN_BTStatus_BTStatus
#  define Rte_Write_LIN_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_LIN_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd
#  define Rte_Write_LIN_IntLghtModeInd_cmd_InteriorLightMode Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_LIN_IntLghtModeInd_cmd_InteriorLightMode
#  define Rte_Write_LIN_PhoneButtonIndication_cmd_DeviceIndication Rte_Write_BunkUserInterfaceHigh2_LINMaCtrl_LIN_PhoneButtonIndication_cmd_DeviceIndication


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1BKF_87_LECMHighLink_NoResp_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)207, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOH_16_LECMHigh_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)239, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOH_17_LECMHigh_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)240, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOH_44_LECMHigh_RAM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)241, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOH_45_LECMHigh_FLASH_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)242, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOH_46_LECMHigh_EEPROM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)243, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOH_49_LECMHigh_HWFAIL_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)244, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOH_94_LECMHigh_SWFAIL_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)245, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_ActivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_AudioRadio2_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)5)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_DeactivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_AudioRadio2_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)5)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_ClimPHTimerSettings3_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)12)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_ClimPHTimerSettings3_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)12)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_LockControlCabRqst2_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)31)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_LockControlCabRqst2_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)31)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_OtherInteriorLights4_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)35)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_OtherInteriorLights4_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)35)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_PHActMaintainLiving6_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)39)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_PHActMaintainLiving6_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)39)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_PowerWindowsActivate3_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)43)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_PowerWindowsActivate3_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)43)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_RoofHatchRequest3_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)45)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_RoofHatchRequest3_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)45)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2G_LECMH_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1B2G_LECMH_Installed_v() (Rte_AddrPar_0x2B_P1B2G_LECMH_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define BunkUserInterfaceHigh2_LINMaCtrl_START_SEC_CODE
# include "BunkUserInterfaceHigh2_LINMaCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_BunkUserInterfaceHigh2_LINMaCtrl_10ms_runnable BunkUserInterfaceHigh2_LINMaCtrl_10ms_runnable
# endif

FUNC(void, BunkUserInterfaceHigh2_LINMaCtrl_CODE) BunkUserInterfaceHigh2_LINMaCtrl_10ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define BunkUserInterfaceHigh2_LINMaCtrl_STOP_SEC_CODE
# include "BunkUserInterfaceHigh2_LINMaCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)

#  define RTE_E_Issm_IssRequest_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_BUNKUSERINTERFACEHIGH2_LINMACTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
