/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_FlexibleSwitchesRouter_Ctrl_LINMstr.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <FlexibleSwitchesRouter_Ctrl_LINMstr>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_H
# define _RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Boolean, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_Living12VResetRequest_Living12VResetRequest; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN1_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN2_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN3_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN4_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN5_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DiagActiveState_T, RTE_VAR_NOINIT) Rte_DiagnosticComponent_DiagActiveState_isDiagActive; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ComMode_LIN1_ComMode_LIN (7U)
#  define Rte_InitValue_ComMode_LIN2_ComMode_LIN (7U)
#  define Rte_InitValue_ComMode_LIN3_ComMode_LIN (7U)
#  define Rte_InitValue_ComMode_LIN4_ComMode_LIN (7U)
#  define Rte_InitValue_ComMode_LIN5_ComMode_LIN (7U)
#  define Rte_InitValue_DiagActiveState_isDiagActive (0U)
#  define Rte_InitValue_FSP1DiagInfoL1_FSPDiagInfo (0U)
#  define Rte_InitValue_FSP1DiagInfoL2_FSPDiagInfo (0U)
#  define Rte_InitValue_FSP1DiagInfoL3_FSPDiagInfo (0U)
#  define Rte_InitValue_FSP1DiagInfoL4_FSPDiagInfo (0U)
#  define Rte_InitValue_FSP1DiagInfoL5_FSPDiagInfo (0U)
#  define Rte_InitValue_FSP1IndicationCmdL1_FSPIndicationCmd (0U)
#  define Rte_InitValue_FSP1IndicationCmdL2_FSPIndicationCmd (0U)
#  define Rte_InitValue_FSP1IndicationCmdL3_FSPIndicationCmd (0U)
#  define Rte_InitValue_FSP1IndicationCmdL4_FSPIndicationCmd (0U)
#  define Rte_InitValue_FSP1IndicationCmdL5_FSPIndicationCmd (0U)
#  define Rte_InitValue_FSP1ResponseErrorL1_FSP1ResponseErrorL1 (FALSE)
#  define Rte_InitValue_FSP1ResponseErrorL2_FSP1ResponseErrorL2 (FALSE)
#  define Rte_InitValue_FSP1ResponseErrorL3_FSP1ResponseErrorL3 (FALSE)
#  define Rte_InitValue_FSP1ResponseErrorL4_FSP1ResponseErrorL4 (FALSE)
#  define Rte_InitValue_FSP1ResponseErrorL5_FSP1ResponseErrorL5 (FALSE)
#  define Rte_InitValue_FSP1SwitchStatusL1_FSPSwitchStatus (0U)
#  define Rte_InitValue_FSP1SwitchStatusL2_FSPSwitchStatus (0U)
#  define Rte_InitValue_FSP1SwitchStatusL3_FSPSwitchStatus (0U)
#  define Rte_InitValue_FSP1SwitchStatusL4_FSPSwitchStatus (0U)
#  define Rte_InitValue_FSP1SwitchStatusL5_FSPSwitchStatus (0U)
#  define Rte_InitValue_FSP2DiagInfoL1_FSPDiagInfo (0U)
#  define Rte_InitValue_FSP2DiagInfoL2_FSPDiagInfo (0U)
#  define Rte_InitValue_FSP2DiagInfoL3_FSPDiagInfo (0U)
#  define Rte_InitValue_FSP2IndicationCmdL1_FSPIndicationCmd (0U)
#  define Rte_InitValue_FSP2IndicationCmdL2_FSPIndicationCmd (0U)
#  define Rte_InitValue_FSP2IndicationCmdL3_FSPIndicationCmd (0U)
#  define Rte_InitValue_FSP2ResponseErrorL1_FSP2ResponseErrorL1 (FALSE)
#  define Rte_InitValue_FSP2ResponseErrorL2_FSP2ResponseErrorL2 (FALSE)
#  define Rte_InitValue_FSP2ResponseErrorL3_FSP2ResponseErrorL3 (FALSE)
#  define Rte_InitValue_FSP2SwitchStatusL1_FSPSwitchStatus (0U)
#  define Rte_InitValue_FSP2SwitchStatusL2_FSPSwitchStatus (0U)
#  define Rte_InitValue_FSP2SwitchStatusL3_FSPSwitchStatus (0U)
#  define Rte_InitValue_FSP3DiagInfoL2_FSPDiagInfo (0U)
#  define Rte_InitValue_FSP3IndicationCmdL2_FSPIndicationCmd (0U)
#  define Rte_InitValue_FSP3ResponseErrorL2_FSP3ResponseErrorL2 (FALSE)
#  define Rte_InitValue_FSP3SwitchStatusL2_FSPSwitchStatus (0U)
#  define Rte_InitValue_FSP4DiagInfoL2_FSPDiagInfo (0U)
#  define Rte_InitValue_FSP4IndicationCmdL2_FSPIndicationCmd (0U)
#  define Rte_InitValue_FSP4ResponseErrorL2_FSP4ResponseErrorL2 (FALSE)
#  define Rte_InitValue_FSP4SwitchStatusL2_FSPSwitchStatus (0U)
#  define Rte_InitValue_Living12VResetRequest_Living12VResetRequest (FALSE)
#  define Rte_InitValue_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted (FALSE)
# endif


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Buffers for inter-runnable variables
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_FlexibleSwitchesRouter_Ctrl_LINMstr_Irv_IOCTL_FspLinCtrl; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
# endif /* !defined(RTE_CORE) */


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1IndicationCmd_FSPIndicationCmdArray(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1IndicationCmd_FSPIndicationCmdArray(P2VAR(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1ResponseErrorL1_FSP1ResponseErrorL1(P2VAR(FSP1ResponseErrorL1_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1ResponseErrorL2_FSP1ResponseErrorL2(P2VAR(FSP1ResponseErrorL2_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1ResponseErrorL3_FSP1ResponseErrorL3(P2VAR(FSP1ResponseErrorL3_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1ResponseErrorL4_FSP1ResponseErrorL4(P2VAR(FSP1ResponseErrorL4_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1ResponseErrorL5_FSP1ResponseErrorL5(P2VAR(FSP1ResponseErrorL5_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1SwitchStatusL1_FSPSwitchStatus(P2VAR(FSPSwitchStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1SwitchStatusL2_FSPSwitchStatus(P2VAR(FSPSwitchStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1SwitchStatusL3_FSPSwitchStatus(P2VAR(FSPSwitchStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1SwitchStatusL4_FSPSwitchStatus(P2VAR(FSPSwitchStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1SwitchStatusL5_FSPSwitchStatus(P2VAR(FSPSwitchStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2IndicationCmd_FSPIndicationCmdArray(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2IndicationCmd_FSPIndicationCmdArray(P2VAR(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2ResponseErrorL1_FSP2ResponseErrorL1(P2VAR(FSP2ResponseErrorL1_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2ResponseErrorL2_FSP2ResponseErrorL2(P2VAR(FSP2ResponseErrorL2_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2ResponseErrorL3_FSP2ResponseErrorL3(P2VAR(FSP2ResponseErrorL3_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2SwitchStatusL1_FSPSwitchStatus(P2VAR(FSPSwitchStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2SwitchStatusL2_FSPSwitchStatus(P2VAR(FSPSwitchStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2SwitchStatusL3_FSPSwitchStatus(P2VAR(FSPSwitchStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP3IndicationCmd_FSPIndicationCmdArray(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP3IndicationCmd_FSPIndicationCmdArray(P2VAR(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP3ResponseErrorL2_FSP3ResponseErrorL2(P2VAR(FSP3ResponseErrorL2_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP3SwitchStatusL2_FSPSwitchStatus(P2VAR(FSPSwitchStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP4IndicationCmd_FSPIndicationCmdArray(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP4IndicationCmd_FSPIndicationCmdArray(P2VAR(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP4ResponseErrorL2_FSP4ResponseErrorL2(P2VAR(FSP4ResponseErrorL2_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP4SwitchStatusL2_FSPSwitchStatus(P2VAR(FSPSwitchStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP5IndicationCmd_FSPIndicationCmdArray(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP5IndicationCmd_FSPIndicationCmdArray(P2VAR(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP6IndicationCmd_FSPIndicationCmdArray(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP6IndicationCmd_FSPIndicationCmdArray(P2VAR(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP7IndicationCmd_FSPIndicationCmdArray(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP7IndicationCmd_FSPIndicationCmdArray(P2VAR(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP8IndicationCmd_FSPIndicationCmdArray(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP8IndicationCmd_FSPIndicationCmdArray(P2VAR(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP9IndicationCmd_FSPIndicationCmdArray(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP9IndicationCmd_FSPIndicationCmdArray(P2VAR(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP_BIndicationCmd_FSPIndicationCmdArray(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP_BIndicationCmd_FSPIndicationCmdArray(P2VAR(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FspNV_PR_FspNV(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FspNV_PR_FspNV(P2VAR(FspNVM_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1IndicationCmdL1_FSPIndicationCmd(FSPIndicationCmd_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1IndicationCmdL3_FSPIndicationCmd(FSPIndicationCmd_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1IndicationCmdL4_FSPIndicationCmd(FSPIndicationCmd_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1IndicationCmdL5_FSPIndicationCmd(FSPIndicationCmd_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1SwitchStatus_FSPSwitchStatusArray(P2CONST(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1SwitchStatus_FSPSwitchStatusArray(P2CONST(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2IndicationCmdL1_FSPIndicationCmd(FSPIndicationCmd_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2IndicationCmdL3_FSPIndicationCmd(FSPIndicationCmd_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2SwitchStatus_FSPSwitchStatusArray(P2CONST(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2SwitchStatus_FSPSwitchStatusArray(P2CONST(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP3IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP3SwitchStatus_FSPSwitchStatusArray(P2CONST(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP3SwitchStatus_FSPSwitchStatusArray(P2CONST(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP4IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP4SwitchStatus_FSPSwitchStatusArray(P2CONST(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP4SwitchStatus_FSPSwitchStatusArray(P2CONST(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP5SwitchStatus_FSPSwitchStatusArray(P2CONST(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP5SwitchStatus_FSPSwitchStatusArray(P2CONST(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP6SwitchStatus_FSPSwitchStatusArray(P2CONST(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP6SwitchStatus_FSPSwitchStatusArray(P2CONST(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP7SwitchStatus_FSPSwitchStatusArray(P2CONST(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP7SwitchStatus_FSPSwitchStatusArray(P2CONST(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP8SwitchStatus_FSPSwitchStatusArray(P2CONST(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP8SwitchStatus_FSPSwitchStatusArray(P2CONST(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP9SwitchStatus_FSPSwitchStatusArray(P2CONST(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP9SwitchStatus_FSPSwitchStatusArray(P2CONST(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP_BSwitchStatus_FSPSwitchStatusArray(P2CONST(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP_BSwitchStatus_FSPSwitchStatusArray(P2CONST(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FspNV_PR_FspNV(P2CONST(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FspNV_PR_FspNV(P2CONST(FspNVM_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_FlexibleSwitchesRouter_Ctrl_LINMstr_DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData_Irv_NbOfFspByCCNAD(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_FlexibleSwitchesRouter_Ctrl_LINMstr_DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData_Irv_NbOfFspByCCNAD(P2VAR(FSP_Array5, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_FlexibleSwitchesRouter_Ctrl_LINMstr_DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData_Irv_NbOfFspInFailure(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_FlexibleSwitchesRouter_Ctrl_LINMstr_DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData_Irv_NbOfFspInFailure(P2VAR(FSP_Array5, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_FlexibleSwitchesRouter_Ctrl_LINMstr_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_FlexibleSwitchesRouter_Ctrl_LINMstr_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(P2VAR(FSP_Array5, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_FlexibleSwitchesRouter_Ctrl_LINMstr_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_FlexibleSwitchesRouter_Ctrl_LINMstr_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(P2VAR(FSP_Array5, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvWrite_FlexibleSwitchesRouter_Ctrl_LINMstr_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(P2CONST(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvWrite_FlexibleSwitchesRouter_Ctrl_LINMstr_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(P2CONST(FSP_Array5, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvWrite_FlexibleSwitchesRouter_Ctrl_LINMstr_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(P2CONST(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvWrite_FlexibleSwitchesRouter_Ctrl_LINMstr_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(P2CONST(FSP_Array5, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ComMode_LIN1_ComMode_LIN Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_ComMode_LIN1_ComMode_LIN
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_ComMode_LIN1_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN1_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ComMode_LIN2_ComMode_LIN Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_ComMode_LIN2_ComMode_LIN
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_ComMode_LIN2_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN2_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ComMode_LIN3_ComMode_LIN Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_ComMode_LIN3_ComMode_LIN
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_ComMode_LIN3_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN3_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ComMode_LIN4_ComMode_LIN Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_ComMode_LIN4_ComMode_LIN
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_ComMode_LIN4_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN4_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ComMode_LIN5_ComMode_LIN Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_ComMode_LIN5_ComMode_LIN
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_ComMode_LIN5_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN5_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DiagActiveState_isDiagActive Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_DiagActiveState_isDiagActive
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_DiagActiveState_isDiagActive(data) (*(data) = Rte_DiagnosticComponent_DiagActiveState_isDiagActive, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FSP1DiagInfoL1_FSPDiagInfo Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1DiagInfoL1_FSPDiagInfo
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1DiagInfoL1_FSPDiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_FSP1DiagInfoL1_oFSP1_Frame_L1_oLIN00_f04712d6_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FSP1DiagInfoL2_FSPDiagInfo Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1DiagInfoL2_FSPDiagInfo
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1DiagInfoL2_FSPDiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_FSP1DiagInfoL2_oFSP1_Frame_L2_oLIN01_7f2a1c9e_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FSP1DiagInfoL3_FSPDiagInfo Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1DiagInfoL3_FSPDiagInfo
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1DiagInfoL3_FSPDiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_FSP1DiagInfoL3_oFSP1_Frame_L3_oLIN02_b1faa76e_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FSP1DiagInfoL4_FSPDiagInfo Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1DiagInfoL4_FSPDiagInfo
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1DiagInfoL4_FSPDiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_FSP1DiagInfoL4_oFSP1_Frame_L4_oLIN03_ba81064f_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FSP1DiagInfoL5_FSPDiagInfo Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1DiagInfoL5_FSPDiagInfo
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1DiagInfoL5_FSPDiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_FSP1DiagInfoL5_oFSP1_Frame_L5_oLIN04_733c79a6_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FSP1IndicationCmd_FSPIndicationCmdArray Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1IndicationCmd_FSPIndicationCmdArray
#  define Rte_Read_FSP1ResponseErrorL1_FSP1ResponseErrorL1 Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1ResponseErrorL1_FSP1ResponseErrorL1
#  define Rte_Read_FSP1ResponseErrorL2_FSP1ResponseErrorL2 Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1ResponseErrorL2_FSP1ResponseErrorL2
#  define Rte_Read_FSP1ResponseErrorL3_FSP1ResponseErrorL3 Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1ResponseErrorL3_FSP1ResponseErrorL3
#  define Rte_Read_FSP1ResponseErrorL4_FSP1ResponseErrorL4 Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1ResponseErrorL4_FSP1ResponseErrorL4
#  define Rte_Read_FSP1ResponseErrorL5_FSP1ResponseErrorL5 Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1ResponseErrorL5_FSP1ResponseErrorL5
#  define Rte_Read_FSP1SwitchStatusL1_FSPSwitchStatus Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1SwitchStatusL1_FSPSwitchStatus
#  define Rte_Read_FSP1SwitchStatusL2_FSPSwitchStatus Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1SwitchStatusL2_FSPSwitchStatus
#  define Rte_Read_FSP1SwitchStatusL3_FSPSwitchStatus Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1SwitchStatusL3_FSPSwitchStatus
#  define Rte_Read_FSP1SwitchStatusL4_FSPSwitchStatus Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1SwitchStatusL4_FSPSwitchStatus
#  define Rte_Read_FSP1SwitchStatusL5_FSPSwitchStatus Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1SwitchStatusL5_FSPSwitchStatus
#  define Rte_Read_FSP2DiagInfoL1_FSPDiagInfo Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2DiagInfoL1_FSPDiagInfo
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2DiagInfoL1_FSPDiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_FSP2DiagInfoL1_oFSP2_Frame_L1_oLIN00_f8caf847_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FSP2DiagInfoL2_FSPDiagInfo Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2DiagInfoL2_FSPDiagInfo
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2DiagInfoL2_FSPDiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_FSP2DiagInfoL2_oFSP2_Frame_L2_oLIN01_77a7f60f_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FSP2DiagInfoL3_FSPDiagInfo Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2DiagInfoL3_FSPDiagInfo
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2DiagInfoL3_FSPDiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_FSP2DiagInfoL3_oFSP2_Frame_L3_oLIN02_b9774dff_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FSP2IndicationCmd_FSPIndicationCmdArray Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2IndicationCmd_FSPIndicationCmdArray
#  define Rte_Read_FSP2ResponseErrorL1_FSP2ResponseErrorL1 Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2ResponseErrorL1_FSP2ResponseErrorL1
#  define Rte_Read_FSP2ResponseErrorL2_FSP2ResponseErrorL2 Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2ResponseErrorL2_FSP2ResponseErrorL2
#  define Rte_Read_FSP2ResponseErrorL3_FSP2ResponseErrorL3 Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2ResponseErrorL3_FSP2ResponseErrorL3
#  define Rte_Read_FSP2SwitchStatusL1_FSPSwitchStatus Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2SwitchStatusL1_FSPSwitchStatus
#  define Rte_Read_FSP2SwitchStatusL2_FSPSwitchStatus Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2SwitchStatusL2_FSPSwitchStatus
#  define Rte_Read_FSP2SwitchStatusL3_FSPSwitchStatus Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2SwitchStatusL3_FSPSwitchStatus
#  define Rte_Read_FSP3DiagInfoL2_FSPDiagInfo Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP3DiagInfoL2_FSPDiagInfo
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP3DiagInfoL2_FSPDiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_FSP3DiagInfoL2_oFSP3_Frame_L2_oLIN01_c6f3adbf_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FSP3IndicationCmd_FSPIndicationCmdArray Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP3IndicationCmd_FSPIndicationCmdArray
#  define Rte_Read_FSP3ResponseErrorL2_FSP3ResponseErrorL2 Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP3ResponseErrorL2_FSP3ResponseErrorL2
#  define Rte_Read_FSP3SwitchStatusL2_FSPSwitchStatus Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP3SwitchStatusL2_FSPSwitchStatus
#  define Rte_Read_FSP4DiagInfoL2_FSPDiagInfo Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP4DiagInfoL2_FSPDiagInfo
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP4DiagInfoL2_FSPDiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_FSP4DiagInfoL2_oFSP4_Frame_L2_oLIN01_66bc232d_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FSP4IndicationCmd_FSPIndicationCmdArray Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP4IndicationCmd_FSPIndicationCmdArray
#  define Rte_Read_FSP4ResponseErrorL2_FSP4ResponseErrorL2 Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP4ResponseErrorL2_FSP4ResponseErrorL2
#  define Rte_Read_FSP4SwitchStatusL2_FSPSwitchStatus Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP4SwitchStatusL2_FSPSwitchStatus
#  define Rte_Read_FSP5IndicationCmd_FSPIndicationCmdArray Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP5IndicationCmd_FSPIndicationCmdArray
#  define Rte_Read_FSP6IndicationCmd_FSPIndicationCmdArray Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP6IndicationCmd_FSPIndicationCmdArray
#  define Rte_Read_FSP7IndicationCmd_FSPIndicationCmdArray Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP7IndicationCmd_FSPIndicationCmdArray
#  define Rte_Read_FSP8IndicationCmd_FSPIndicationCmdArray Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP8IndicationCmd_FSPIndicationCmdArray
#  define Rte_Read_FSP9IndicationCmd_FSPIndicationCmdArray Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP9IndicationCmd_FSPIndicationCmdArray
#  define Rte_Read_FSP_BIndicationCmd_FSPIndicationCmdArray Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP_BIndicationCmd_FSPIndicationCmdArray
#  define Rte_Read_FspNV_PR_FspNV Rte_Read_FlexibleSwitchesRouter_Ctrl_LINMstr_FspNV_PR_FspNV


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_FSP1IndicationCmdL1_FSPIndicationCmd Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1IndicationCmdL1_FSPIndicationCmd
#  define Rte_Write_FSP1IndicationCmdL2_FSPIndicationCmd Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1IndicationCmdL2_FSPIndicationCmd
#  define Rte_Write_FSP1IndicationCmdL3_FSPIndicationCmd Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1IndicationCmdL3_FSPIndicationCmd
#  define Rte_Write_FSP1IndicationCmdL4_FSPIndicationCmd Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1IndicationCmdL4_FSPIndicationCmd
#  define Rte_Write_FSP1IndicationCmdL5_FSPIndicationCmd Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1IndicationCmdL5_FSPIndicationCmd
#  define Rte_Write_FSP1SwitchStatus_FSPSwitchStatusArray Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1SwitchStatus_FSPSwitchStatusArray
#  define Rte_Write_FSP2IndicationCmdL1_FSPIndicationCmd Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2IndicationCmdL1_FSPIndicationCmd
#  define Rte_Write_FSP2IndicationCmdL2_FSPIndicationCmd Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2IndicationCmdL2_FSPIndicationCmd
#  define Rte_Write_FSP2IndicationCmdL3_FSPIndicationCmd Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2IndicationCmdL3_FSPIndicationCmd
#  define Rte_Write_FSP2SwitchStatus_FSPSwitchStatusArray Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2SwitchStatus_FSPSwitchStatusArray
#  define Rte_Write_FSP3IndicationCmdL2_FSPIndicationCmd Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP3IndicationCmdL2_FSPIndicationCmd
#  define Rte_Write_FSP3SwitchStatus_FSPSwitchStatusArray Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP3SwitchStatus_FSPSwitchStatusArray
#  define Rte_Write_FSP4IndicationCmdL2_FSPIndicationCmd Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP4IndicationCmdL2_FSPIndicationCmd
#  define Rte_Write_FSP4SwitchStatus_FSPSwitchStatusArray Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP4SwitchStatus_FSPSwitchStatusArray
#  define Rte_Write_FSP5SwitchStatus_FSPSwitchStatusArray Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP5SwitchStatus_FSPSwitchStatusArray
#  define Rte_Write_FSP6SwitchStatus_FSPSwitchStatusArray Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP6SwitchStatus_FSPSwitchStatusArray
#  define Rte_Write_FSP7SwitchStatus_FSPSwitchStatusArray Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP7SwitchStatus_FSPSwitchStatusArray
#  define Rte_Write_FSP8SwitchStatus_FSPSwitchStatusArray Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP8SwitchStatus_FSPSwitchStatusArray
#  define Rte_Write_FSP9SwitchStatus_FSPSwitchStatusArray Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP9SwitchStatus_FSPSwitchStatusArray
#  define Rte_Write_FSP_BSwitchStatus_FSPSwitchStatusArray Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP_BSwitchStatus_FSPSwitchStatusArray
#  define Rte_Write_FspNV_PR_FspNV Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_FspNV_PR_FspNV
#  define Rte_Write_Living12VResetRequest_Living12VResetRequest Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_Living12VResetRequest_Living12VResetRequest
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_Living12VResetRequest_Living12VResetRequest(data) (Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_Living12VResetRequest_Living12VResetRequest = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_LINMstr_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted(data) (Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CDD_LINDIAGNOSTICS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_CDD_LINDIAGNOSTICS_APPL_CODE) CddLinDiagServices_FSPAssignReq(LinDiagBusInfo LinBusInfo, LinDiagRequest_T RequestType); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_CDD_LINDIAGNOSTICS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CddLinDiagServices_FSPAssignReq(arg1, arg2) (CddLinDiagServices_FSPAssignReq(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_CDD_LINDIAGNOSTICS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_CDD_LINDIAGNOSTICS_APPL_CODE) CddLinDiagServices_FSPAssignResp(P2VAR(LinDiagServiceStatus, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) pDiagServiceStatus, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) pAvailableFSPCount, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) pFspErrorStatus, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) pFspNvData); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_CDD_LINDIAGNOSTICS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CddLinDiagServices_FSPAssignResp(arg1, arg2, arg3, arg4) (CddLinDiagServices_FSPAssignResp(arg1, arg2, arg3, arg4), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1BK9_87_FSP_NoResp_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)257, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN8_16_FSP_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)259, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN8_17_FSP_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)260, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN8_44_FSP_MemoryFailure_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)261, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN8_79_FlexSwLostFCI_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)262, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData_Irv_NbOfFspByCCNAD(data) \
  Rte_IrvRead_FlexibleSwitchesRouter_Ctrl_LINMstr_DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData_Irv_NbOfFspByCCNAD(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData_Irv_NbOfFspInFailure(data) \
  Rte_IrvRead_FlexibleSwitchesRouter_Ctrl_LINMstr_DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData_Irv_NbOfFspInFailure(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData_Irv_IOCTL_FspLinCtrl() \
  Rte_Irv_FlexibleSwitchesRouter_Ctrl_LINMstr_Irv_IOCTL_FspLinCtrl
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_FspLinCtrl(data) \
  (Rte_Irv_FlexibleSwitchesRouter_Ctrl_LINMstr_Irv_IOCTL_FspLinCtrl = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_FspLinCtrl(data) \
  (Rte_Irv_FlexibleSwitchesRouter_Ctrl_LINMstr_Irv_IOCTL_FspLinCtrl = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl() \
  Rte_Irv_FlexibleSwitchesRouter_Ctrl_LINMstr_Irv_IOCTL_FspLinCtrl
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(data) \
  Rte_IrvRead_FlexibleSwitchesRouter_Ctrl_LINMstr_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(data) \
  Rte_IrvRead_FlexibleSwitchesRouter_Ctrl_LINMstr_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl(data) \
  (Rte_Irv_FlexibleSwitchesRouter_Ctrl_LINMstr_Irv_IOCTL_FspLinCtrl = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(data) \
  Rte_IrvWrite_FlexibleSwitchesRouter_Ctrl_LINMstr_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(data) \
  Rte_IrvWrite_FlexibleSwitchesRouter_Ctrl_LINMstr_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(data)
/* PRQA L:L1 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_FSPConfigSettingsLIN2_P1EW0_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EW0_FSPConfigSettingsLIN2_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FSPConfigSettingsLIN3_P1EW1_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EW1_FSPConfigSettingsLIN3_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FSPConfigSettingsLIN4_P1EW2_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EW2_FSPConfigSettingsLIN4_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FSPConfigSettingsLIN5_P1EW3_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EW3_FSPConfigSettingsLIN5_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FSPConfigSettingsLIN1_P1EWZ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EWZ_FSPConfigSettingsLIN1_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v() (Rte_AddrPar_0x2B_P1EW0_FSPConfigSettingsLIN2_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v() (Rte_AddrPar_0x2B_P1EW1_FSPConfigSettingsLIN3_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v() (Rte_AddrPar_0x2B_P1EW2_FSPConfigSettingsLIN4_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v() (Rte_AddrPar_0x2B_P1EW3_FSPConfigSettingsLIN5_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v() (Rte_AddrPar_0x2B_P1EWZ_FSPConfigSettingsLIN1_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define FlexibleSwitchesRouter_Ctrl_LINMstr_START_SEC_CODE
# include "FlexibleSwitchesRouter_Ctrl_LINMstr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent
#  define RTE_RUNNABLE_DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData
#  define RTE_RUNNABLE_DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData
#  define RTE_RUNNABLE_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData
#  define RTE_RUNNABLE_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment
#  define RTE_RUNNABLE_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable FlexibleSwitchPanel_LINMastCtrl_20ms_runnable
#  define RTE_RUNNABLE_RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults
#  define RTE_RUNNABLE_RoutineServices_R1AAI_FspAssignmentRoutine_Start RoutineServices_R1AAI_FspAssignmentRoutine_Start
#  define RTE_RUNNABLE_RoutineServices_R1AAI_FspAssignmentRoutine_Stop RoutineServices_R1AAI_FspAssignmentRoutine_Stop
# endif

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData(P2VAR(Dcm_Data6ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData(P2VAR(Dcm_Data5ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) FlexibleSwitchPanel_LINMastCtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data7ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_Start(uint8 In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_Start(uint8 In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_Stop(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define FlexibleSwitchesRouter_Ctrl_LINMstr_STOP_SEC_CODE
# include "FlexibleSwitchesRouter_Ctrl_LINMstr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_CallbackInitMonitorForEvent_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1C1R_Data_P1C1R_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1DCU_Data_P1DCU_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK (1U)

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_R1AAI_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_R1AAI_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
