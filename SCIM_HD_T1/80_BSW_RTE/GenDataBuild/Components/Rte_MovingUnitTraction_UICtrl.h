/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_MovingUnitTraction_UICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <MovingUnitTraction_UICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_MOVINGUNITTRACTION_UICTRL_H
# define _RTE_MOVINGUNITTRACTION_UICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_MovingUnitTraction_UICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MovingUnitTraction_UICtrl_ASROff_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MovingUnitTraction_UICtrl_ConstructionSwitch_DeviceInd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MovingUnitTraction_UICtrl_DifflockOnOff_Indication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MovingUnitTraction_UICtrl_Offroad_Indication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MovingUnitTraction_UICtrl_RearDiffLock_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ASROffButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Construction_SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_DiffLockPanel_LINMasterCtrl_DifflockDeactivationBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FreeWheel_Status_T, RTE_VAR_NOINIT) Rte_DiffLockPanel_LINMasterCtrl_DifflockMode_Wheelstatus_FreeWheel_Status; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_DiffLockPanel_LINMasterCtrl_Offroad_ButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_RearAxleDiffLock_ButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ASROffButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ASROff_DeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_AutoFrontWheelDriveDrvReq_AutoFrontWheelDriveDrvReq (3U)
#  define Rte_InitValue_ConstructionSwitch_DeviceInd_DeviceIndication (3U)
#  define Rte_InitValue_ConstructionSwitch_stat_ConstructionSwitch_stat (3U)
#  define Rte_InitValue_Construction_SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_DifflockDeactivationBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_DifflockMode_Wheelstatus_FreeWheel_Status (15U)
#  define Rte_InitValue_DifflockOnOff_Indication_DeviceIndication (3U)
#  define Rte_InitValue_FrAxleDiffLockActvnDriverRqst_FrAxleDiffLockActvnDriverRqst (3U)
#  define Rte_InitValue_FrtAxleHydroActive_Status_FrtAxleHydroActive_Status (3U)
#  define Rte_InitValue_Offroad_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_Offroad_Indication_DeviceIndication (3U)
#  define Rte_InitValue_RearAxleDiffLockActvnDrvrReq_RearAxleDiffLockActvnDrvrReq (3U)
#  define Rte_InitValue_RearAxleDiffLock_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_RearDiffLock_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_RrInterAxlDiffLockActvnDrvrReq_RrInterAxlDiffLockActvnDrvrReq (3U)
#  define Rte_InitValue_SplitBoxDifflockActvnDrvrRqst_TransferBDifflockActvnDrvrRqst (3U)
#  define Rte_InitValue_SwcActivation_IgnitionOn_IgnitionOn (1U)
#  define Rte_InitValue_TractionControlDriverRqst_TractionControlDriverRqst (7U)
#  define Rte_InitValue_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed (65535U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_MovingUnitTraction_UICtrl_FrtAxleHydroActive_Status_FrtAxleHydroActive_Status(P2VAR(DeactivateActivate_T, AUTOMATIC, RTE_MOVINGUNITTRACTION_UICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_MovingUnitTraction_UICtrl_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I(P2VAR(uint8, AUTOMATIC, RTE_MOVINGUNITTRACTION_UICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_MovingUnitTraction_UICtrl_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I(P2VAR(StandardNVM_T, AUTOMATIC, RTE_MOVINGUNITTRACTION_UICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_MovingUnitTraction_UICtrl_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I(P2VAR(uint8, AUTOMATIC, RTE_MOVINGUNITTRACTION_UICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_MovingUnitTraction_UICtrl_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I(P2VAR(StandardNVM_T, AUTOMATIC, RTE_MOVINGUNITTRACTION_UICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_MovingUnitTraction_UICtrl_AutoFrontWheelDriveDrvReq_AutoFrontWheelDriveDrvReq(DisengageEngage_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_MovingUnitTraction_UICtrl_ConstructionSwitch_stat_ConstructionSwitch_stat(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_MovingUnitTraction_UICtrl_FrAxleDiffLockActvnDriverRqst_FrAxleDiffLockActvnDriverRqst(DisengageEngage_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_MovingUnitTraction_UICtrl_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I(P2CONST(uint8, AUTOMATIC, RTE_MOVINGUNITTRACTION_UICTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_MovingUnitTraction_UICtrl_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I(P2CONST(StandardNVM_T, AUTOMATIC, RTE_MOVINGUNITTRACTION_UICTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_MovingUnitTraction_UICtrl_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I(P2CONST(uint8, AUTOMATIC, RTE_MOVINGUNITTRACTION_UICTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_MovingUnitTraction_UICtrl_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I(P2CONST(StandardNVM_T, AUTOMATIC, RTE_MOVINGUNITTRACTION_UICTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_MovingUnitTraction_UICtrl_RearAxleDiffLockActvnDrvrReq_RearAxleDiffLockActvnDrvrReq(DisengageEngage_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_MovingUnitTraction_UICtrl_RrInterAxlDiffLockActvnDrvrReq_RrInterAxlDiffLockActvnDrvrReq(DisengageEngage_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_MovingUnitTraction_UICtrl_SplitBoxDifflockActvnDrvrRqst_TransferBDifflockActvnDrvrRqst(DisengageEngage_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_MovingUnitTraction_UICtrl_TractionControlDriverRqst_TractionControlDriverRqst(TractionControlDriverRqst_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ASROffButtonStatus_PushButtonStatus Rte_Read_MovingUnitTraction_UICtrl_ASROffButtonStatus_PushButtonStatus
#  define Rte_Read_MovingUnitTraction_UICtrl_ASROffButtonStatus_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_ASROffButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Construction_SwitchStatus_A2PosSwitchStatus Rte_Read_MovingUnitTraction_UICtrl_Construction_SwitchStatus_A2PosSwitchStatus
#  define Rte_Read_MovingUnitTraction_UICtrl_Construction_SwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_Construction_SwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DifflockDeactivationBtn_stat_PushButtonStatus Rte_Read_MovingUnitTraction_UICtrl_DifflockDeactivationBtn_stat_PushButtonStatus
#  define Rte_Read_MovingUnitTraction_UICtrl_DifflockDeactivationBtn_stat_PushButtonStatus(data) (*(data) = Rte_DiffLockPanel_LINMasterCtrl_DifflockDeactivationBtn_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DifflockMode_Wheelstatus_FreeWheel_Status Rte_Read_MovingUnitTraction_UICtrl_DifflockMode_Wheelstatus_FreeWheel_Status
#  define Rte_Read_MovingUnitTraction_UICtrl_DifflockMode_Wheelstatus_FreeWheel_Status(data) (*(data) = Rte_DiffLockPanel_LINMasterCtrl_DifflockMode_Wheelstatus_FreeWheel_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FrtAxleHydroActive_Status_FrtAxleHydroActive_Status Rte_Read_MovingUnitTraction_UICtrl_FrtAxleHydroActive_Status_FrtAxleHydroActive_Status
#  define Rte_Read_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I Rte_Read_MovingUnitTraction_UICtrl_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I
#  define Rte_Read_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I Rte_Read_MovingUnitTraction_UICtrl_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I
#  define Rte_Read_Offroad_ButtonStatus_PushButtonStatus Rte_Read_MovingUnitTraction_UICtrl_Offroad_ButtonStatus_PushButtonStatus
#  define Rte_Read_MovingUnitTraction_UICtrl_Offroad_ButtonStatus_PushButtonStatus(data) (*(data) = Rte_DiffLockPanel_LINMasterCtrl_Offroad_ButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RearAxleDiffLock_ButtonStatus_PushButtonStatus Rte_Read_MovingUnitTraction_UICtrl_RearAxleDiffLock_ButtonStatus_PushButtonStatus
#  define Rte_Read_MovingUnitTraction_UICtrl_RearAxleDiffLock_ButtonStatus_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_RearAxleDiffLock_ButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_IgnitionOn_IgnitionOn Rte_Read_MovingUnitTraction_UICtrl_SwcActivation_IgnitionOn_IgnitionOn
#  define Rte_Read_MovingUnitTraction_UICtrl_SwcActivation_IgnitionOn_IgnitionOn(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed Rte_Read_MovingUnitTraction_UICtrl_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed
#  define Rte_Read_MovingUnitTraction_UICtrl_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(data) (Com_ReceiveSignal(ComConf_ComSignal_WheelBasedVehicleSpeed_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_e3ff6d4c_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_ASROff_DeviceIndication_DeviceIndication Rte_Write_MovingUnitTraction_UICtrl_ASROff_DeviceIndication_DeviceIndication
#  define Rte_Write_MovingUnitTraction_UICtrl_ASROff_DeviceIndication_DeviceIndication(data) (Rte_MovingUnitTraction_UICtrl_ASROff_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AutoFrontWheelDriveDrvReq_AutoFrontWheelDriveDrvReq Rte_Write_MovingUnitTraction_UICtrl_AutoFrontWheelDriveDrvReq_AutoFrontWheelDriveDrvReq
#  define Rte_Write_ConstructionSwitch_DeviceInd_DeviceIndication Rte_Write_MovingUnitTraction_UICtrl_ConstructionSwitch_DeviceInd_DeviceIndication
#  define Rte_Write_MovingUnitTraction_UICtrl_ConstructionSwitch_DeviceInd_DeviceIndication(data) (Rte_MovingUnitTraction_UICtrl_ConstructionSwitch_DeviceInd_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ConstructionSwitch_stat_ConstructionSwitch_stat Rte_Write_MovingUnitTraction_UICtrl_ConstructionSwitch_stat_ConstructionSwitch_stat
#  define Rte_Write_DifflockOnOff_Indication_DeviceIndication Rte_Write_MovingUnitTraction_UICtrl_DifflockOnOff_Indication_DeviceIndication
#  define Rte_Write_MovingUnitTraction_UICtrl_DifflockOnOff_Indication_DeviceIndication(data) (Rte_MovingUnitTraction_UICtrl_DifflockOnOff_Indication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_FrAxleDiffLockActvnDriverRqst_FrAxleDiffLockActvnDriverRqst Rte_Write_MovingUnitTraction_UICtrl_FrAxleDiffLockActvnDriverRqst_FrAxleDiffLockActvnDriverRqst
#  define Rte_Write_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I Rte_Write_MovingUnitTraction_UICtrl_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I
#  define Rte_Write_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I Rte_Write_MovingUnitTraction_UICtrl_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I
#  define Rte_Write_Offroad_Indication_DeviceIndication Rte_Write_MovingUnitTraction_UICtrl_Offroad_Indication_DeviceIndication
#  define Rte_Write_MovingUnitTraction_UICtrl_Offroad_Indication_DeviceIndication(data) (Rte_MovingUnitTraction_UICtrl_Offroad_Indication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_RearAxleDiffLockActvnDrvrReq_RearAxleDiffLockActvnDrvrReq Rte_Write_MovingUnitTraction_UICtrl_RearAxleDiffLockActvnDrvrReq_RearAxleDiffLockActvnDrvrReq
#  define Rte_Write_RearDiffLock_DeviceIndication_DeviceIndication Rte_Write_MovingUnitTraction_UICtrl_RearDiffLock_DeviceIndication_DeviceIndication
#  define Rte_Write_MovingUnitTraction_UICtrl_RearDiffLock_DeviceIndication_DeviceIndication(data) (Rte_MovingUnitTraction_UICtrl_RearDiffLock_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_RrInterAxlDiffLockActvnDrvrReq_RrInterAxlDiffLockActvnDrvrReq Rte_Write_MovingUnitTraction_UICtrl_RrInterAxlDiffLockActvnDrvrReq_RrInterAxlDiffLockActvnDrvrReq
#  define Rte_Write_SplitBoxDifflockActvnDrvrRqst_TransferBDifflockActvnDrvrRqst Rte_Write_MovingUnitTraction_UICtrl_SplitBoxDifflockActvnDrvrRqst_TransferBDifflockActvnDrvrRqst
#  define Rte_Write_TractionControlDriverRqst_TractionControlDriverRqst Rte_Write_MovingUnitTraction_UICtrl_TractionControlDriverRqst_TractionControlDriverRqst


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define Rte_Call_Event_D1BOW_63_TractionSw_Stuck_SetEventStatus(arg1) (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_AxleConfiguration_P1B16_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B16_AxleConfiguration_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FrontAxleArrangement_P1CSH_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CSH_FrontAxleArrangement_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DiffLockSinglePushSwitch_LogicSelection_P1NAK_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NAK_DiffLockSinglePushSwitch_LogicSelection_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_WheelDifferentialLockPushButtonType_P1UG1_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1UG1_WheelDifferentialLockPushButtonType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_RearAxleDifflockDisregardRequestVehicleSpeed_P1WEB_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WEB_RearAxleDifflockDisregardRequestVehicleSpeed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B03_RearWheelDiffLockPushSw_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B04_DLFW_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CUC_ConstructionSw_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1F7J_ASROffRoadFullVersion_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1FNW_ASROffButtonInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NQA_AutomaticFrontWheelDrive_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1SDA_OptitrackSystemInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1B16_AxleConfiguration_v() (Rte_AddrPar_0x2B_P1B16_AxleConfiguration_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CSH_FrontAxleArrangement_v() (Rte_AddrPar_0x2B_P1CSH_FrontAxleArrangement_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1NAK_DiffLockSinglePushSwitch_LogicSelection_v() (Rte_AddrPar_0x2B_P1NAK_DiffLockSinglePushSwitch_LogicSelection_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1UG1_WheelDifferentialLockPushButtonType_v() (Rte_AddrPar_0x2B_P1UG1_WheelDifferentialLockPushButtonType_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1WEB_RearAxleDifflockDisregardRequestVehicleSpeed_v() (Rte_AddrPar_0x2B_P1WEB_RearAxleDifflockDisregardRequestVehicleSpeed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1B03_RearWheelDiffLockPushSw_v() (Rte_AddrPar_0x2B_P1B03_RearWheelDiffLockPushSw_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1B04_DLFW_Installed_v() (Rte_AddrPar_0x2B_P1B04_DLFW_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CUC_ConstructionSw_Act_v() (Rte_AddrPar_0x2B_P1CUC_ConstructionSw_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1F7J_ASROffRoadFullVersion_v() (Rte_AddrPar_0x2B_P1F7J_ASROffRoadFullVersion_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1FNW_ASROffButtonInstalled_v() (Rte_AddrPar_0x2B_P1FNW_ASROffButtonInstalled_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1NQA_AutomaticFrontWheelDrive_Act_v() (Rte_AddrPar_0x2B_P1NQA_AutomaticFrontWheelDrive_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1SDA_OptitrackSystemInstalled_v() (Rte_AddrPar_0x2B_P1SDA_OptitrackSystemInstalled_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define MovingUnitTraction_UICtrl_START_SEC_CODE
# include "MovingUnitTraction_UICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_MovingUnitTraction_UICtrl_Difflock_20ms_runnable MovingUnitTraction_UICtrl_Difflock_20ms_runnable
#  define RTE_RUNNABLE_MovingUnitTraction_UICtrl_Traction_20ms_runnable MovingUnitTraction_UICtrl_Traction_20ms_runnable
# endif

FUNC(void, MovingUnitTraction_UICtrl_CODE) MovingUnitTraction_UICtrl_Difflock_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, MovingUnitTraction_UICtrl_CODE) MovingUnitTraction_UICtrl_Traction_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define MovingUnitTraction_UICtrl_STOP_SEC_CODE
# include "MovingUnitTraction_UICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_MOVINGUNITTRACTION_UICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
