/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_InCabLock_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <InCabLock_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_INCABLOCK_HMICTRL_H
# define _RTE_INCABLOCK_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_InCabLock_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DoorLockUnlock_T, RTE_VAR_NOINIT) Rte_InCabLock_HMICtrl_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2LockButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_DashboardLockButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Living_Living; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_BunkH1LockButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH1UnlockButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2LockButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_DashboardLockButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst (7U)
#  define Rte_InitValue_SwcActivation_Living_Living (1U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InCabLock_HMICtrl_BunkH1LockButtonStatus_PushButtonStatus(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_INCABLOCK_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InCabLock_HMICtrl_BunkH1UnlockButtonStatus_PushButtonStatus(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_INCABLOCK_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InCabLock_HMICtrl_Locking_Switch_stat_serialized_Crypto_Function_serialized(P2VAR(uint8, AUTOMATIC, RTE_INCABLOCK_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InCabLock_HMICtrl_Locking_Switch_stat_serialized_Crypto_Function_serialized(P2VAR(Crypto_Function_serialized_T, AUTOMATIC, RTE_INCABLOCK_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_BunkH1LockButtonStatus_PushButtonStatus Rte_Read_InCabLock_HMICtrl_BunkH1LockButtonStatus_PushButtonStatus
#  define Rte_Read_BunkH1UnlockButtonStatus_PushButtonStatus Rte_Read_InCabLock_HMICtrl_BunkH1UnlockButtonStatus_PushButtonStatus
#  define Rte_Read_BunkH2LockButtonStatus_PushButtonStatus Rte_Read_InCabLock_HMICtrl_BunkH2LockButtonStatus_PushButtonStatus
#  define Rte_Read_InCabLock_HMICtrl_BunkH2LockButtonStatus_PushButtonStatus(data) (*(data) = Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2LockButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DashboardLockButtonStatus_PushButtonStatus Rte_Read_InCabLock_HMICtrl_DashboardLockButtonStatus_PushButtonStatus
#  define Rte_Read_InCabLock_HMICtrl_DashboardLockButtonStatus_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_DashboardLockButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Locking_Switch_stat_serialized_Crypto_Function_serialized Rte_Read_InCabLock_HMICtrl_Locking_Switch_stat_serialized_Crypto_Function_serialized
#  define Rte_Read_SwcActivation_Living_Living Rte_Read_InCabLock_HMICtrl_SwcActivation_Living_Living
#  define Rte_Read_InCabLock_HMICtrl_SwcActivation_Living_Living(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Living_Living, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst Rte_Write_InCabLock_HMICtrl_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst
#  define Rte_Write_InCabLock_HMICtrl_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst(data) (Rte_InCabLock_HMICtrl_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


# endif /* !defined(RTE_CORE) */


# define InCabLock_HMICtrl_START_SEC_CODE
# include "InCabLock_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_InCabLock_HMICtrl_20ms_runnable InCabLock_HMICtrl_20ms_runnable
# endif

FUNC(void, InCabLock_HMICtrl_CODE) InCabLock_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define InCabLock_HMICtrl_STOP_SEC_CODE
# include "InCabLock_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_INCABLOCK_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
