/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_LINMgr_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <LINMgr>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_LINMGR_TYPE_H
# define _RTE_LINMGR_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef LIN1_Table1
#   define LIN1_Table1 (1U)
#  endif

#  ifndef LIN1_Table2
#   define LIN1_Table2 (2U)
#  endif

#  ifndef LIN1_Table_E
#   define LIN1_Table_E (3U)
#  endif

#  ifndef LIN1_MasterReq_SlaveResp_Table1
#   define LIN1_MasterReq_SlaveResp_Table1 (4U)
#  endif

#  ifndef LIN1_MasterReq_SlaveResp_Table2
#   define LIN1_MasterReq_SlaveResp_Table2 (5U)
#  endif

#  ifndef LIN1_NULL
#   define LIN1_NULL (0U)
#  endif

#  ifndef LIN1_MasterReq_SlaveResp
#   define LIN1_MasterReq_SlaveResp (6U)
#  endif

#  ifndef LIN2_NULL
#   define LIN2_NULL (0U)
#  endif

#  ifndef LIN2_TABLE0
#   define LIN2_TABLE0 (1U)
#  endif

#  ifndef LIN2_TABLE_E
#   define LIN2_TABLE_E (2U)
#  endif

#  ifndef LIN2_MasterReq_SlaveResp_TABLE0
#   define LIN2_MasterReq_SlaveResp_TABLE0 (3U)
#  endif

#  ifndef LIN2_MasterReq_SlaveResp
#   define LIN2_MasterReq_SlaveResp (4U)
#  endif

#  ifndef LIN3_NULL
#   define LIN3_NULL (0U)
#  endif

#  ifndef LIN3_TABLE1
#   define LIN3_TABLE1 (1U)
#  endif

#  ifndef LIN3_TABLE2
#   define LIN3_TABLE2 (2U)
#  endif

#  ifndef LIN3_TABLE_E
#   define LIN3_TABLE_E (3U)
#  endif

#  ifndef LIN3_MasterReq_SlaveResp_Table1
#   define LIN3_MasterReq_SlaveResp_Table1 (4U)
#  endif

#  ifndef LIN3_MasterReq_SlaveResp_Table2
#   define LIN3_MasterReq_SlaveResp_Table2 (5U)
#  endif

#  ifndef LIN3_MasterReq_SlaveResp
#   define LIN3_MasterReq_SlaveResp (6U)
#  endif

#  ifndef LIN4_NULL
#   define LIN4_NULL (0U)
#  endif

#  ifndef LIN4_MasterReq_SlaveResp_Table1
#   define LIN4_MasterReq_SlaveResp_Table1 (4U)
#  endif

#  ifndef LIN4_TABLE1
#   define LIN4_TABLE1 (1U)
#  endif

#  ifndef LIN4_TABLE2
#   define LIN4_TABLE2 (2U)
#  endif

#  ifndef LIN4_TABLE_E
#   define LIN4_TABLE_E (3U)
#  endif

#  ifndef LIN4_MasterReq_SlaveResp_Table2
#   define LIN4_MasterReq_SlaveResp_Table2 (5U)
#  endif

#  ifndef LIN4_MasterReq_SlaveResp
#   define LIN4_MasterReq_SlaveResp (6U)
#  endif

#  ifndef LIN5_NULL
#   define LIN5_NULL (0U)
#  endif

#  ifndef LIN5_TABLE1
#   define LIN5_TABLE1 (1U)
#  endif

#  ifndef LIN5_MasterReq_SlaveResp_Table1
#   define LIN5_MasterReq_SlaveResp_Table1 (4U)
#  endif

#  ifndef LIN5_MasterReq_SlaveResp_Table2
#   define LIN5_MasterReq_SlaveResp_Table2 (5U)
#  endif

#  ifndef LIN5_MasterReq_SlaveResp
#   define LIN5_MasterReq_SlaveResp (6U)
#  endif

#  ifndef LIN5_TABLE2
#   define LIN5_TABLE2 (2U)
#  endif

#  ifndef LIN5_TABLE_E
#   define LIN5_TABLE_E (3U)
#  endif

#  ifndef LIN6_NULL
#   define LIN6_NULL (0U)
#  endif

#  ifndef LIN6_TABLE0
#   define LIN6_TABLE0 (1U)
#  endif

#  ifndef LIN6_MasterReq_SlaveResp_Table0
#   define LIN6_MasterReq_SlaveResp_Table0 (3U)
#  endif

#  ifndef LIN6_MasterReq_SlaveResp
#   define LIN6_MasterReq_SlaveResp (2U)
#  endif

#  ifndef LIN7_NULL
#   define LIN7_NULL (0U)
#  endif

#  ifndef LIN7_TABLE0
#   define LIN7_TABLE0 (1U)
#  endif

#  ifndef LIN7_MasterReq_SlaveResp_Table0
#   define LIN7_MasterReq_SlaveResp_Table0 (3U)
#  endif

#  ifndef LIN7_MasterReq_SlaveResp
#   define LIN7_MasterReq_SlaveResp (2U)
#  endif

#  ifndef LIN8_NULL
#   define LIN8_NULL (0U)
#  endif

#  ifndef LIN8_TABLE0
#   define LIN8_TABLE0 (1U)
#  endif

#  ifndef LIN8_MasterReq_SlaveResp_Table0
#   define LIN8_MasterReq_SlaveResp_Table0 (3U)
#  endif

#  ifndef LIN8_MasterReq_SlaveResp
#   define LIN8_MasterReq_SlaveResp (2U)
#  endif

#  ifndef COMM_NO_COMMUNICATION
#   define COMM_NO_COMMUNICATION (0U)
#  endif

#  ifndef COMM_SILENT_COMMUNICATION
#   define COMM_SILENT_COMMUNICATION (1U)
#  endif

#  ifndef COMM_FULL_COMMUNICATION
#   define COMM_FULL_COMMUNICATION (2U)
#  endif

#  ifndef Inactive
#   define Inactive (0U)
#  endif

#  ifndef Diagnostic
#   define Diagnostic (1U)
#  endif

#  ifndef SwitchDetection
#   define SwitchDetection (2U)
#  endif

#  ifndef ApplicationMonitoring
#   define ApplicationMonitoring (3U)
#  endif

#  ifndef Calibration
#   define Calibration (4U)
#  endif

#  ifndef Spare1
#   define Spare1 (5U)
#  endif

#  ifndef Error
#   define Error (6U)
#  endif

#  ifndef NotAvailable
#   define NotAvailable (7U)
#  endif

#  ifndef DCM_E_POSITIVERESPONSE
#   define DCM_E_POSITIVERESPONSE (0U)
#  endif

#  ifndef DCM_E_GENERALREJECT
#   define DCM_E_GENERALREJECT (16U)
#  endif

#  ifndef DCM_E_SERVICENOTSUPPORTED
#   define DCM_E_SERVICENOTSUPPORTED (17U)
#  endif

#  ifndef DCM_E_SUBFUNCTIONNOTSUPPORTED
#   define DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
#  endif

#  ifndef DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT
#   define DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
#  endif

#  ifndef DCM_E_RESPONSETOOLONG
#   define DCM_E_RESPONSETOOLONG (20U)
#  endif

#  ifndef DCM_E_BUSYREPEATREQUEST
#   define DCM_E_BUSYREPEATREQUEST (33U)
#  endif

#  ifndef DCM_E_CONDITIONSNOTCORRECT
#   define DCM_E_CONDITIONSNOTCORRECT (34U)
#  endif

#  ifndef DCM_E_REQUESTSEQUENCEERROR
#   define DCM_E_REQUESTSEQUENCEERROR (36U)
#  endif

#  ifndef DCM_E_NORESPONSEFROMSUBNETCOMPONENT
#   define DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
#  endif

#  ifndef DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION
#   define DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
#  endif

#  ifndef DCM_E_REQUESTOUTOFRANGE
#   define DCM_E_REQUESTOUTOFRANGE (49U)
#  endif

#  ifndef DCM_E_SECURITYACCESSDENIED
#   define DCM_E_SECURITYACCESSDENIED (51U)
#  endif

#  ifndef DCM_E_INVALIDKEY
#   define DCM_E_INVALIDKEY (53U)
#  endif

#  ifndef DCM_E_EXCEEDNUMBEROFATTEMPTS
#   define DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
#  endif

#  ifndef DCM_E_REQUIREDTIMEDELAYNOTEXPIRED
#   define DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
#  endif

#  ifndef DCM_E_UPLOADDOWNLOADNOTACCEPTED
#   define DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
#  endif

#  ifndef DCM_E_TRANSFERDATASUSPENDED
#   define DCM_E_TRANSFERDATASUSPENDED (113U)
#  endif

#  ifndef DCM_E_GENERALPROGRAMMINGFAILURE
#   define DCM_E_GENERALPROGRAMMINGFAILURE (114U)
#  endif

#  ifndef DCM_E_WRONGBLOCKSEQUENCECOUNTER
#   define DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
#  endif

#  ifndef DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING
#   define DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
#  endif

#  ifndef DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION
#   define DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
#  endif

#  ifndef DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION
#   define DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
#  endif

#  ifndef DCM_E_RPMTOOHIGH
#   define DCM_E_RPMTOOHIGH (129U)
#  endif

#  ifndef DCM_E_RPMTOOLOW
#   define DCM_E_RPMTOOLOW (130U)
#  endif

#  ifndef DCM_E_ENGINEISRUNNING
#   define DCM_E_ENGINEISRUNNING (131U)
#  endif

#  ifndef DCM_E_ENGINEISNOTRUNNING
#   define DCM_E_ENGINEISNOTRUNNING (132U)
#  endif

#  ifndef DCM_E_ENGINERUNTIMETOOLOW
#   define DCM_E_ENGINERUNTIMETOOLOW (133U)
#  endif

#  ifndef DCM_E_TEMPERATURETOOHIGH
#   define DCM_E_TEMPERATURETOOHIGH (134U)
#  endif

#  ifndef DCM_E_TEMPERATURETOOLOW
#   define DCM_E_TEMPERATURETOOLOW (135U)
#  endif

#  ifndef DCM_E_VEHICLESPEEDTOOHIGH
#   define DCM_E_VEHICLESPEEDTOOHIGH (136U)
#  endif

#  ifndef DCM_E_VEHICLESPEEDTOOLOW
#   define DCM_E_VEHICLESPEEDTOOLOW (137U)
#  endif

#  ifndef DCM_E_THROTTLE_PEDALTOOHIGH
#   define DCM_E_THROTTLE_PEDALTOOHIGH (138U)
#  endif

#  ifndef DCM_E_THROTTLE_PEDALTOOLOW
#   define DCM_E_THROTTLE_PEDALTOOLOW (139U)
#  endif

#  ifndef DCM_E_TRANSMISSIONRANGENOTINNEUTRAL
#   define DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
#  endif

#  ifndef DCM_E_TRANSMISSIONRANGENOTINGEAR
#   define DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
#  endif

#  ifndef DCM_E_BRAKESWITCH_NOTCLOSED
#   define DCM_E_BRAKESWITCH_NOTCLOSED (143U)
#  endif

#  ifndef DCM_E_SHIFTERLEVERNOTINPARK
#   define DCM_E_SHIFTERLEVERNOTINPARK (144U)
#  endif

#  ifndef DCM_E_TORQUECONVERTERCLUTCHLOCKED
#   define DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
#  endif

#  ifndef DCM_E_VOLTAGETOOHIGH
#   define DCM_E_VOLTAGETOOHIGH (146U)
#  endif

#  ifndef DCM_E_VOLTAGETOOLOW
#   define DCM_E_VOLTAGETOOLOW (147U)
#  endif

#  ifndef DCM_E_VMSCNC_0
#   define DCM_E_VMSCNC_0 (240U)
#  endif

#  ifndef DCM_E_VMSCNC_1
#   define DCM_E_VMSCNC_1 (241U)
#  endif

#  ifndef DCM_E_VMSCNC_2
#   define DCM_E_VMSCNC_2 (242U)
#  endif

#  ifndef DCM_E_VMSCNC_3
#   define DCM_E_VMSCNC_3 (243U)
#  endif

#  ifndef DCM_E_VMSCNC_4
#   define DCM_E_VMSCNC_4 (244U)
#  endif

#  ifndef DCM_E_VMSCNC_5
#   define DCM_E_VMSCNC_5 (245U)
#  endif

#  ifndef DCM_E_VMSCNC_6
#   define DCM_E_VMSCNC_6 (246U)
#  endif

#  ifndef DCM_E_VMSCNC_7
#   define DCM_E_VMSCNC_7 (247U)
#  endif

#  ifndef DCM_E_VMSCNC_8
#   define DCM_E_VMSCNC_8 (248U)
#  endif

#  ifndef DCM_E_VMSCNC_9
#   define DCM_E_VMSCNC_9 (249U)
#  endif

#  ifndef DCM_E_VMSCNC_A
#   define DCM_E_VMSCNC_A (250U)
#  endif

#  ifndef DCM_E_VMSCNC_B
#   define DCM_E_VMSCNC_B (251U)
#  endif

#  ifndef DCM_E_VMSCNC_C
#   define DCM_E_VMSCNC_C (252U)
#  endif

#  ifndef DCM_E_VMSCNC_D
#   define DCM_E_VMSCNC_D (253U)
#  endif

#  ifndef DCM_E_VMSCNC_E
#   define DCM_E_VMSCNC_E (254U)
#  endif

#  ifndef DCM_INITIAL
#   define DCM_INITIAL (0U)
#  endif

#  ifndef DCM_PENDING
#   define DCM_PENDING (1U)
#  endif

#  ifndef DCM_CANCEL
#   define DCM_CANCEL (2U)
#  endif

#  ifndef DCM_FORCE_RCRRP_OK
#   define DCM_FORCE_RCRRP_OK (3U)
#  endif

#  ifndef DCM_FORCE_RCRRP_NOT_OK
#   define DCM_FORCE_RCRRP_NOT_OK (64U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED
#   define DEM_EVENT_STATUS_PASSED (0U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED
#   define DEM_EVENT_STATUS_FAILED (1U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED
#   define DEM_EVENT_STATUS_PREPASSED (2U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED
#   define DEM_EVENT_STATUS_PREFAILED (3U)
#  endif

#  ifndef DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED
#   define DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
#  endif

#  ifndef SEWS_PcbConfig_LinInterfaces_X1CX0_T_NotPopulated
#   define SEWS_PcbConfig_LinInterfaces_X1CX0_T_NotPopulated (0U)
#  endif

#  ifndef SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated
#   define SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated (1U)
#  endif

#  ifndef TestNotRun
#   define TestNotRun (0U)
#  endif

#  ifndef OffState_NoFaultDetected
#   define OffState_NoFaultDetected (16U)
#  endif

#  ifndef OffState_FaultDetected_STG
#   define OffState_FaultDetected_STG (17U)
#  endif

#  ifndef OffState_FaultDetected_STB
#   define OffState_FaultDetected_STB (18U)
#  endif

#  ifndef OffState_FaultDetected_OC
#   define OffState_FaultDetected_OC (19U)
#  endif

#  ifndef OffState_FaultDetected_VBT
#   define OffState_FaultDetected_VBT (22U)
#  endif

#  ifndef OffState_FaultDetected_VAT
#   define OffState_FaultDetected_VAT (23U)
#  endif

#  ifndef OnState_NoFaultDetected
#   define OnState_NoFaultDetected (32U)
#  endif

#  ifndef OnState_FaultDetected_STG
#   define OnState_FaultDetected_STG (33U)
#  endif

#  ifndef OnState_FaultDetected_STB
#   define OnState_FaultDetected_STB (34U)
#  endif

#  ifndef OnState_FaultDetected_OC
#   define OnState_FaultDetected_OC (35U)
#  endif

#  ifndef OnState_FaultDetected_VBT
#   define OnState_FaultDetected_VBT (38U)
#  endif

#  ifndef OnState_FaultDetected_VAT
#   define OnState_FaultDetected_VAT (39U)
#  endif

#  ifndef OnState_FaultDetected_VOR
#   define OnState_FaultDetected_VOR (41U)
#  endif

#  ifndef OnState_FaultDetected_CAT
#   define OnState_FaultDetected_CAT (44U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

#  ifndef VehicleMode_Hibernate
#   define VehicleMode_Hibernate (0U)
#  endif

#  ifndef VehicleMode_Parked
#   define VehicleMode_Parked (1U)
#  endif

#  ifndef VehicleMode_Living
#   define VehicleMode_Living (2U)
#  endif

#  ifndef VehicleMode_Accessory
#   define VehicleMode_Accessory (3U)
#  endif

#  ifndef VehicleMode_PreRunning
#   define VehicleMode_PreRunning (4U)
#  endif

#  ifndef VehicleMode_Cranking
#   define VehicleMode_Cranking (5U)
#  endif

#  ifndef VehicleMode_Running
#   define VehicleMode_Running (6U)
#  endif

#  ifndef VehicleMode_Spare_1
#   define VehicleMode_Spare_1 (7U)
#  endif

#  ifndef VehicleMode_Spare_2
#   define VehicleMode_Spare_2 (8U)
#  endif

#  ifndef VehicleMode_Spare_3
#   define VehicleMode_Spare_3 (9U)
#  endif

#  ifndef VehicleMode_Spare_4
#   define VehicleMode_Spare_4 (10U)
#  endif

#  ifndef VehicleMode_Spare_5
#   define VehicleMode_Spare_5 (11U)
#  endif

#  ifndef VehicleMode_Spare_6
#   define VehicleMode_Spare_6 (12U)
#  endif

#  ifndef VehicleMode_Spare_7
#   define VehicleMode_Spare_7 (13U)
#  endif

#  ifndef VehicleMode_Error
#   define VehicleMode_Error (14U)
#  endif

#  ifndef VehicleMode_NotAvailable
#   define VehicleMode_NotAvailable (15U)
#  endif

# endif /* RTE_CORE */


/**********************************************************************************************************************
 * Definitions for Mode Management
 *********************************************************************************************************************/
# ifndef RTE_MODETYPE_BswMRteMDG_LIN1Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN1Schedule
typedef uint8 Rte_ModeType_BswMRteMDG_LIN1Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN2Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN2Schedule
typedef uint8 Rte_ModeType_BswMRteMDG_LIN2Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN3Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN3Schedule
typedef uint8 Rte_ModeType_BswMRteMDG_LIN3Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN4Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN4Schedule
typedef uint8 Rte_ModeType_BswMRteMDG_LIN4Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN5Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN5Schedule
typedef uint8 Rte_ModeType_BswMRteMDG_LIN5Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN6Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN6Schedule
typedef uint8 Rte_ModeType_BswMRteMDG_LIN6Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN7Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN7Schedule
typedef uint8 Rte_ModeType_BswMRteMDG_LIN7Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN8Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN8Schedule
typedef uint8 Rte_ModeType_BswMRteMDG_LIN8Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LINSchTableState
#  define RTE_MODETYPE_BswMRteMDG_LINSchTableState
typedef uint8 Rte_ModeType_BswMRteMDG_LINSchTableState;
# endif

# define RTE_MODE_LINMgr_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1 (1U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2 (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2 (2U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN1Schedule_LIN1_NULL (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_NULL
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_NULL (3U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN1Schedule_LIN1_Table1 (4U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table1
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table1 (4U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN1Schedule_LIN1_Table2 (5U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table2
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table2 (5U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN1Schedule_LIN1_Table_E (6U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table_E
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table_E (6U)
# endif
# define RTE_TRANSITION_LINMgr_BswMRteMDG_LIN1Schedule (7U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN1Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN1Schedule (7U)
# endif

# define RTE_MODE_LINMgr_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0
#  define RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0 (1U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN2Schedule_LIN2_NULL (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_NULL
#  define RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_NULL (2U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN2Schedule_LIN2_TABLE0 (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE0
#  define RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE0 (3U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E (4U)
# ifndef RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E
#  define RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E (4U)
# endif
# define RTE_TRANSITION_LINMgr_BswMRteMDG_LIN2Schedule (5U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN2Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN2Schedule (5U)
# endif

# define RTE_MODE_LINMgr_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1 (1U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2 (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2 (2U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN3Schedule_LIN3_NULL (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_NULL
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_NULL (3U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN3Schedule_LIN3_TABLE1 (4U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE1
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE1 (4U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN3Schedule_LIN3_TABLE2 (5U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE2
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE2 (5U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E (6U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E (6U)
# endif
# define RTE_TRANSITION_LINMgr_BswMRteMDG_LIN3Schedule (7U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN3Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN3Schedule (7U)
# endif

# define RTE_MODE_LINMgr_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1 (1U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2 (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2 (2U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN4Schedule_LIN4_NULL (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_NULL
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_NULL (3U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN4Schedule_LIN4_TABLE1 (4U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE1
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE1 (4U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN4Schedule_LIN4_TABLE2 (5U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE2
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE2 (5U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E (6U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E (6U)
# endif
# define RTE_TRANSITION_LINMgr_BswMRteMDG_LIN4Schedule (7U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN4Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN4Schedule (7U)
# endif

# define RTE_MODE_LINMgr_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1 (1U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2 (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2 (2U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN5Schedule_LIN5_NULL (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_NULL
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_NULL (3U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN5Schedule_LIN5_TABLE1 (4U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE1
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE1 (4U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN5Schedule_LIN5_TABLE2 (5U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE2
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE2 (5U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E (6U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E (6U)
# endif
# define RTE_TRANSITION_LINMgr_BswMRteMDG_LIN5Schedule (7U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN5Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN5Schedule (7U)
# endif

# define RTE_MODE_LINMgr_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp_Table0 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp_Table0
#  define RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp_Table0 (1U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN6Schedule_LIN6_NULL (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_NULL
#  define RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_NULL (2U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN6Schedule_LIN6_TABLE0 (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_TABLE0
#  define RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_TABLE0 (3U)
# endif
# define RTE_TRANSITION_LINMgr_BswMRteMDG_LIN6Schedule (4U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN6Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN6Schedule (4U)
# endif

# define RTE_MODE_LINMgr_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp_Table0 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp_Table0
#  define RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp_Table0 (1U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN7Schedule_LIN7_NULL (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_NULL
#  define RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_NULL (2U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN7Schedule_LIN7_TABLE0 (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_TABLE0
#  define RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_TABLE0 (3U)
# endif
# define RTE_TRANSITION_LINMgr_BswMRteMDG_LIN7Schedule (4U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN7Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN7Schedule (4U)
# endif

# define RTE_MODE_LINMgr_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp_Table0 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp_Table0
#  define RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp_Table0 (1U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN8Schedule_LIN8_NULL (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_NULL
#  define RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_NULL (2U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LIN8Schedule_LIN8_TABLE0 (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_TABLE0
#  define RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_TABLE0 (3U)
# endif
# define RTE_TRANSITION_LINMgr_BswMRteMDG_LIN8Schedule (4U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN8Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN8Schedule (4U)
# endif

# define RTE_MODE_LINMgr_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification (0U)
# ifndef RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification
#  define RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification (0U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT (1U)
# ifndef RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT
#  define RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT (1U)
# endif
# define RTE_MODE_LINMgr_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication (2U)
# ifndef RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication
#  define RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication (2U)
# endif
# define RTE_TRANSITION_LINMgr_BswMRteMDG_LINSchTableState (3U)
# ifndef RTE_TRANSITION_BswMRteMDG_LINSchTableState
#  define RTE_TRANSITION_BswMRteMDG_LINSchTableState (3U)
# endif

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_LINMGR_TYPE_H */
