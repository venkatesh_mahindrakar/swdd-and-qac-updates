/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CollisionMitigation_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <CollisionMitigation_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_COLLISIONMITIGATION_HMICTRL_H
# define _RTE_COLLISIONMITIGATION_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CollisionMitigation_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_CollisionMitigation_HMICtrl_FCW_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AEBS_ButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_SpeedControlFreeWheel_LINMasCtrl_FCWPushButton_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FCW_SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_EngineRun_EngineRun; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_AEBS_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_CM_Status_CM_Status (7U)
#  define Rte_InitValue_CollSituationHMICtrlRequestVM_CollSituationHMICtrlRequestVM (7U)
#  define Rte_InitValue_FCWPushButton_PushButtonStatus (3U)
#  define Rte_InitValue_FCW_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_FCW_Enable_FCW_Enable (3U)
#  define Rte_InitValue_FCW_Status_FCW_Status (3U)
#  define Rte_InitValue_FCW_SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_SetCMOperation_SetCMOperation (7U)
#  define Rte_InitValue_SetFCWOperation_SetFCWOperation (7U)
#  define Rte_InitValue_SwcActivation_EngineRun_EngineRun (1U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_CollisionMitigation_HMICtrl_CM_Status_CM_Status(P2VAR(CM_Status_T, AUTOMATIC, RTE_COLLISIONMITIGATION_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_CollisionMitigation_HMICtrl_FCW_Status_FCW_Status(P2VAR(DisableEnable_T, AUTOMATIC, RTE_COLLISIONMITIGATION_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CollisionMitigation_HMICtrl_CollSituationHMICtrlRequestVM_CollSituationHMICtrlRequestVM(CollSituationHMICtrlRequestVM_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CollisionMitigation_HMICtrl_FCW_Enable_FCW_Enable(DisableEnable_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CollisionMitigation_HMICtrl_SetCMOperation_SetCMOperation(SetCMOperation_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CollisionMitigation_HMICtrl_SetFCWOperation_SetFCWOperation(SetFCWOperation_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_AEBS_ButtonStatus_PushButtonStatus Rte_Read_CollisionMitigation_HMICtrl_AEBS_ButtonStatus_PushButtonStatus
#  define Rte_Read_CollisionMitigation_HMICtrl_AEBS_ButtonStatus_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_AEBS_ButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_CM_Status_CM_Status Rte_Read_CollisionMitigation_HMICtrl_CM_Status_CM_Status
#  define Rte_Read_FCWPushButton_PushButtonStatus Rte_Read_CollisionMitigation_HMICtrl_FCWPushButton_PushButtonStatus
#  define Rte_Read_CollisionMitigation_HMICtrl_FCWPushButton_PushButtonStatus(data) (*(data) = Rte_SpeedControlFreeWheel_LINMasCtrl_FCWPushButton_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FCW_Status_FCW_Status Rte_Read_CollisionMitigation_HMICtrl_FCW_Status_FCW_Status
#  define Rte_Read_FCW_SwitchStatus_A2PosSwitchStatus Rte_Read_CollisionMitigation_HMICtrl_FCW_SwitchStatus_A2PosSwitchStatus
#  define Rte_Read_CollisionMitigation_HMICtrl_FCW_SwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_FCW_SwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_EngineRun_EngineRun Rte_Read_CollisionMitigation_HMICtrl_SwcActivation_EngineRun_EngineRun
#  define Rte_Read_CollisionMitigation_HMICtrl_SwcActivation_EngineRun_EngineRun(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_EngineRun_EngineRun, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_CollSituationHMICtrlRequestVM_CollSituationHMICtrlRequestVM Rte_Write_CollisionMitigation_HMICtrl_CollSituationHMICtrlRequestVM_CollSituationHMICtrlRequestVM
#  define Rte_Write_FCW_DeviceIndication_DeviceIndication Rte_Write_CollisionMitigation_HMICtrl_FCW_DeviceIndication_DeviceIndication
#  define Rte_Write_CollisionMitigation_HMICtrl_FCW_DeviceIndication_DeviceIndication(data) (Rte_CollisionMitigation_HMICtrl_FCW_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_FCW_Enable_FCW_Enable Rte_Write_CollisionMitigation_HMICtrl_FCW_Enable_FCW_Enable
#  define Rte_Write_SetCMOperation_SetCMOperation Rte_Write_CollisionMitigation_HMICtrl_SetCMOperation_SetCMOperation
#  define Rte_Write_SetFCWOperation_SetFCWOperation Rte_Write_CollisionMitigation_HMICtrl_SetFCWOperation_SetFCWOperation


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_HeadwaySupport_P1BEX_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BEX_HeadwaySupport_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FCW_LedLogic_P1LG1_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LG1_FCW_LedLogic_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_CM_Configuration_P1LGD_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LGD_CM_Configuration_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FCW_SwPushThreshold_P1LGE_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LGE_FCW_SwPushThreshold_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FCW_ConfirmTimeout_P1LGF_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LGF_FCW_ConfirmTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FCW_SwStuckTimeout_P1LGG_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LGG_FCW_SwStuckTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1MOT_CollSituationHMICtrlRequestVM_Time_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NT1_CM_DeviceType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1BEX_HeadwaySupport_v() (Rte_AddrPar_0x2B_P1BEX_HeadwaySupport_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1LG1_FCW_LedLogic_v() (Rte_AddrPar_0x2B_P1LG1_FCW_LedLogic_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1LGD_CM_Configuration_v() (Rte_AddrPar_0x2B_P1LGD_CM_Configuration_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1LGE_FCW_SwPushThreshold_v() (Rte_AddrPar_0x2B_P1LGE_FCW_SwPushThreshold_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1LGF_FCW_ConfirmTimeout_v() (Rte_AddrPar_0x2B_P1LGF_FCW_ConfirmTimeout_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1LGG_FCW_SwStuckTimeout_v() (Rte_AddrPar_0x2B_P1LGG_FCW_SwStuckTimeout_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1MOT_CollSituationHMICtrlRequestVM_Time_v() (Rte_AddrPar_0x2B_P1MOT_CollSituationHMICtrlRequestVM_Time_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1NT1_CM_DeviceType_v() (Rte_AddrPar_0x2B_P1NT1_CM_DeviceType_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define CollisionMitigation_HMICtrl_START_SEC_CODE
# include "CollisionMitigation_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_CollisionMitigation_HMICtrl_20ms_Runnable CollisionMitigation_HMICtrl_20ms_Runnable
# endif

FUNC(void, CollisionMitigation_HMICtrl_CODE) CollisionMitigation_HMICtrl_20ms_Runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define CollisionMitigation_HMICtrl_STOP_SEC_CODE
# include "CollisionMitigation_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_COLLISIONMITIGATION_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
