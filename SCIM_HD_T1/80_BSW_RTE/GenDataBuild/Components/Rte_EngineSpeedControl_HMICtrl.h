/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_EngineSpeedControl_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <EngineSpeedControl_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_ENGINESPEEDCONTROL_HMICTRL_H
# define _RTE_ENGINESPEEDCONTROL_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_EngineSpeedControl_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_EngineSpeedControl_HMICtrl_EscButtonMuddySiteDeviceInd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_EngineSpeedControl_HMICtrl_EscSwitchEnableDeviceInd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_EngineSpeedControl_HMICtrl_EscSwitchMuddySiteDeviceInd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_DiffLockPanel_LINMasterCtrl_EscButtonMuddySiteStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchEnableStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchIncDecStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchMuddySiteStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchResumeStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ACCEnableRqst_ACCEnableRqst (3U)
#  define Rte_InitValue_AcceleratorPedalStatus_AcceleratorPedalStatus (7U)
#  define Rte_InitValue_AdjustRequestForIdle_AdjustRequestForIdle (7U)
#  define Rte_InitValue_CCEnableRequest_CCEnableRequest (3U)
#  define Rte_InitValue_EngineSpeedControlStatus_EngineSpeedControlStatus (7U)
#  define Rte_InitValue_EscButtonMuddySiteDeviceInd_DeviceIndication (3U)
#  define Rte_InitValue_EscButtonMuddySiteStatus_PushButtonStatus (3U)
#  define Rte_InitValue_EscCabActionRequest_EscCabActionRequest (7U)
#  define Rte_InitValue_EscCabEnable_EscCabEnable (3U)
#  define Rte_InitValue_EscCrossCountryActivation_rqst_EscCrossCountryActivation_rqst (3U)
#  define Rte_InitValue_EscSwitchEnableDeviceInd_DeviceIndication (3U)
#  define Rte_InitValue_EscSwitchEnableStatus_PushButtonStatus (3U)
#  define Rte_InitValue_EscSwitchIncDecStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_EscSwitchMuddySiteDeviceInd_DeviceIndication (3U)
#  define Rte_InitValue_EscSwitchMuddySiteStatus_PushButtonStatus (3U)
#  define Rte_InitValue_EscSwitchResumeStatus_PushButtonStatus (3U)
#  define Rte_InitValue_SWSpdCtrlButtonsStatus1_SWSpdCtrlButtonsStatus1 (15U)
#  define Rte_InitValue_SWSpeedControlAdjustMode_SWSpeedControlAdjustMode (15U)
#  define Rte_InitValue_SwcActivation_IgnitionOn_IgnitionOn (1U)
#  define Rte_InitValue_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed (65535U)
#  define Rte_InitValue_WrcEngSpdCtrlDecreaseButtStat_WrcEngSpdCtrlDecreaseButtStat (3U)
#  define Rte_InitValue_WrcEngSpdCtrlEnableStatus_WrcEngSpdCtrlEnableStatus (3U)
#  define Rte_InitValue_WrcEngSpdCtrlIncreaseButtStat_WrcEngSpdCtrlIncreaseButtStat (3U)
#  define Rte_InitValue_WrcEngSpdCtrlResumeButtonStat_WrcEngSpdCtrlResumeButtonStat (3U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_EngineSpeedControl_HMICtrl_ACCEnableRqst_ACCEnableRqst(P2VAR(DisableEnable_T, AUTOMATIC, RTE_ENGINESPEEDCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_EngineSpeedControl_HMICtrl_AcceleratorPedalStatus_AcceleratorPedalStatus(P2VAR(AcceleratorPedalStatus_T, AUTOMATIC, RTE_ENGINESPEEDCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_EngineSpeedControl_HMICtrl_CCEnableRequest_CCEnableRequest(P2VAR(DisableEnable_T, AUTOMATIC, RTE_ENGINESPEEDCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_EngineSpeedControl_HMICtrl_EngineSpeedControlStatus_EngineSpeedControlStatus(P2VAR(EngineSpeedControlStatus_T, AUTOMATIC, RTE_ENGINESPEEDCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_EngineSpeedControl_HMICtrl_SWSpdCtrlButtonsStatus1_SWSpdCtrlButtonsStatus1(P2VAR(SWSpdCtrlButtonsStatus1_T, AUTOMATIC, RTE_ENGINESPEEDCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_EngineSpeedControl_HMICtrl_SWSpeedControlAdjustMode_SWSpeedControlAdjustMode(P2VAR(SWSpeedControlAdjustMode_T, AUTOMATIC, RTE_ENGINESPEEDCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_EngineSpeedControl_HMICtrl_WrcEngSpdCtrlDecreaseButtStat_WrcEngSpdCtrlDecreaseButtStat(P2VAR(ButtonStatus_T, AUTOMATIC, RTE_ENGINESPEEDCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_EngineSpeedControl_HMICtrl_WrcEngSpdCtrlEnableStatus_WrcEngSpdCtrlEnableStatus(P2VAR(DisableEnable_T, AUTOMATIC, RTE_ENGINESPEEDCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_EngineSpeedControl_HMICtrl_WrcEngSpdCtrlIncreaseButtStat_WrcEngSpdCtrlIncreaseButtStat(P2VAR(ButtonStatus_T, AUTOMATIC, RTE_ENGINESPEEDCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_EngineSpeedControl_HMICtrl_WrcEngSpdCtrlResumeButtonStat_WrcEngSpdCtrlResumeButtonStat(P2VAR(ButtonStatus_T, AUTOMATIC, RTE_ENGINESPEEDCONTROL_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_EngineSpeedControl_HMICtrl_AdjustRequestForIdle_AdjustRequestForIdle(EngineSpeedRequest_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_EngineSpeedControl_HMICtrl_EscCabActionRequest_EscCabActionRequest(EscActionRequest_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_EngineSpeedControl_HMICtrl_EscCabEnable_EscCabEnable(DisableEnable_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_EngineSpeedControl_HMICtrl_EscCrossCountryActivation_rqst_EscCrossCountryActivation_rqst(DisableEnable_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ACCEnableRqst_ACCEnableRqst Rte_Read_EngineSpeedControl_HMICtrl_ACCEnableRqst_ACCEnableRqst
#  define Rte_Read_AcceleratorPedalStatus_AcceleratorPedalStatus Rte_Read_EngineSpeedControl_HMICtrl_AcceleratorPedalStatus_AcceleratorPedalStatus
#  define Rte_Read_CCEnableRequest_CCEnableRequest Rte_Read_EngineSpeedControl_HMICtrl_CCEnableRequest_CCEnableRequest
#  define Rte_Read_EngineSpeedControlStatus_EngineSpeedControlStatus Rte_Read_EngineSpeedControl_HMICtrl_EngineSpeedControlStatus_EngineSpeedControlStatus
#  define Rte_Read_EscButtonMuddySiteStatus_PushButtonStatus Rte_Read_EngineSpeedControl_HMICtrl_EscButtonMuddySiteStatus_PushButtonStatus
#  define Rte_Read_EngineSpeedControl_HMICtrl_EscButtonMuddySiteStatus_PushButtonStatus(data) (*(data) = Rte_DiffLockPanel_LINMasterCtrl_EscButtonMuddySiteStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_EscSwitchEnableStatus_PushButtonStatus Rte_Read_EngineSpeedControl_HMICtrl_EscSwitchEnableStatus_PushButtonStatus
#  define Rte_Read_EngineSpeedControl_HMICtrl_EscSwitchEnableStatus_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchEnableStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_EscSwitchIncDecStatus_A3PosSwitchStatus Rte_Read_EngineSpeedControl_HMICtrl_EscSwitchIncDecStatus_A3PosSwitchStatus
#  define Rte_Read_EngineSpeedControl_HMICtrl_EscSwitchIncDecStatus_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchIncDecStatus_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_EscSwitchMuddySiteStatus_PushButtonStatus Rte_Read_EngineSpeedControl_HMICtrl_EscSwitchMuddySiteStatus_PushButtonStatus
#  define Rte_Read_EngineSpeedControl_HMICtrl_EscSwitchMuddySiteStatus_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchMuddySiteStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_EscSwitchResumeStatus_PushButtonStatus Rte_Read_EngineSpeedControl_HMICtrl_EscSwitchResumeStatus_PushButtonStatus
#  define Rte_Read_EngineSpeedControl_HMICtrl_EscSwitchResumeStatus_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchResumeStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SWSpdCtrlButtonsStatus1_SWSpdCtrlButtonsStatus1 Rte_Read_EngineSpeedControl_HMICtrl_SWSpdCtrlButtonsStatus1_SWSpdCtrlButtonsStatus1
#  define Rte_Read_SWSpeedControlAdjustMode_SWSpeedControlAdjustMode Rte_Read_EngineSpeedControl_HMICtrl_SWSpeedControlAdjustMode_SWSpeedControlAdjustMode
#  define Rte_Read_SwcActivation_IgnitionOn_IgnitionOn Rte_Read_EngineSpeedControl_HMICtrl_SwcActivation_IgnitionOn_IgnitionOn
#  define Rte_Read_EngineSpeedControl_HMICtrl_SwcActivation_IgnitionOn_IgnitionOn(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed Rte_Read_EngineSpeedControl_HMICtrl_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed
#  define Rte_Read_EngineSpeedControl_HMICtrl_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(data) (Com_ReceiveSignal(ComConf_ComSignal_WheelBasedVehicleSpeed_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_e3ff6d4c_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WrcEngSpdCtrlDecreaseButtStat_WrcEngSpdCtrlDecreaseButtStat Rte_Read_EngineSpeedControl_HMICtrl_WrcEngSpdCtrlDecreaseButtStat_WrcEngSpdCtrlDecreaseButtStat
#  define Rte_Read_WrcEngSpdCtrlEnableStatus_WrcEngSpdCtrlEnableStatus Rte_Read_EngineSpeedControl_HMICtrl_WrcEngSpdCtrlEnableStatus_WrcEngSpdCtrlEnableStatus
#  define Rte_Read_WrcEngSpdCtrlIncreaseButtStat_WrcEngSpdCtrlIncreaseButtStat Rte_Read_EngineSpeedControl_HMICtrl_WrcEngSpdCtrlIncreaseButtStat_WrcEngSpdCtrlIncreaseButtStat
#  define Rte_Read_WrcEngSpdCtrlResumeButtonStat_WrcEngSpdCtrlResumeButtonStat Rte_Read_EngineSpeedControl_HMICtrl_WrcEngSpdCtrlResumeButtonStat_WrcEngSpdCtrlResumeButtonStat


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_AdjustRequestForIdle_AdjustRequestForIdle Rte_Write_EngineSpeedControl_HMICtrl_AdjustRequestForIdle_AdjustRequestForIdle
#  define Rte_Write_EscButtonMuddySiteDeviceInd_DeviceIndication Rte_Write_EngineSpeedControl_HMICtrl_EscButtonMuddySiteDeviceInd_DeviceIndication
#  define Rte_Write_EngineSpeedControl_HMICtrl_EscButtonMuddySiteDeviceInd_DeviceIndication(data) (Rte_EngineSpeedControl_HMICtrl_EscButtonMuddySiteDeviceInd_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_EscCabActionRequest_EscCabActionRequest Rte_Write_EngineSpeedControl_HMICtrl_EscCabActionRequest_EscCabActionRequest
#  define Rte_Write_EscCabEnable_EscCabEnable Rte_Write_EngineSpeedControl_HMICtrl_EscCabEnable_EscCabEnable
#  define Rte_Write_EscCrossCountryActivation_rqst_EscCrossCountryActivation_rqst Rte_Write_EngineSpeedControl_HMICtrl_EscCrossCountryActivation_rqst_EscCrossCountryActivation_rqst
#  define Rte_Write_EscSwitchEnableDeviceInd_DeviceIndication Rte_Write_EngineSpeedControl_HMICtrl_EscSwitchEnableDeviceInd_DeviceIndication
#  define Rte_Write_EngineSpeedControl_HMICtrl_EscSwitchEnableDeviceInd_DeviceIndication(data) (Rte_EngineSpeedControl_HMICtrl_EscSwitchEnableDeviceInd_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_EscSwitchMuddySiteDeviceInd_DeviceIndication Rte_Write_EngineSpeedControl_HMICtrl_EscSwitchMuddySiteDeviceInd_DeviceIndication
#  define Rte_Write_EngineSpeedControl_HMICtrl_EscSwitchMuddySiteDeviceInd_DeviceIndication(data) (Rte_EngineSpeedControl_HMICtrl_EscSwitchMuddySiteDeviceInd_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1IZ3_ESC_InhibitionByPrimaryPedal_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B0X_EngineSpeedControlSw_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1IZ3_ESC_InhibitionByPrimaryPedal_v() (Rte_AddrPar_0x2B_P1IZ3_ESC_InhibitionByPrimaryPedal_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1B0X_EngineSpeedControlSw_v() (Rte_AddrPar_0x2B_P1B0X_EngineSpeedControlSw_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_BodybuilderAccessToAccelPedal_P1B72_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1B72_BodybuilderAccessToAccelPedal_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1B0W_CrossCountryCC_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1B72_BodybuilderAccessToAccelPedal_v() (Rte_AddrPar_0x2B_and_0x37_P1B72_BodybuilderAccessToAccelPedal_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1B0W_CrossCountryCC_Act_v() (Rte_AddrPar_0x2B_and_0x37_P1B0W_CrossCountryCC_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define EngineSpeedControl_HMICtrl_START_SEC_CODE
# include "EngineSpeedControl_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_EngineSpeedControl_HMICtrl_20ms_runnable EngineSpeedControl_HMICtrl_20ms_runnable
#  define RTE_RUNNABLE_EngineSpeedControl_HMICtrl_init EngineSpeedControl_HMICtrl_init
# endif

FUNC(void, EngineSpeedControl_HMICtrl_CODE) EngineSpeedControl_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, EngineSpeedControl_HMICtrl_CODE) EngineSpeedControl_HMICtrl_init(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define EngineSpeedControl_HMICtrl_STOP_SEC_CODE
# include "EngineSpeedControl_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_ENGINESPEEDCONTROL_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
