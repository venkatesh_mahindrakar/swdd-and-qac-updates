/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_PEPS.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <PEPS>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_PEPS_H
# define _RTE_PEPS_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_PEPS_Type.h"
# include "Rte_DataHandleType.h"


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_IOHWAB_LFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_IOHWAB_LFIC_APPL_CODE) Get_LFSearchCompleteFlag(P2VAR(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) LFSearchCompleteFlag_PEPS); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_LFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_LFSearchCompleteFlag_Get_LFSearchCompleteFlag(arg1) (Get_LFSearchCompleteFlag(arg1), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_IOHWAB_LFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_IOHWAB_LFIC_APPL_CODE) Set_LFSearchCompleteFlag(uint8 LFSearchCompleteFlag_PEPS); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_LFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_LFSearchCompleteFlag_Set_LFSearchCompleteFlag(arg1) (Set_LFSearchCompleteFlag(arg1), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_IOHWAB_LFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_IOHWAB_LFIC_APPL_CODE) RE_LficInit(uint8 Gain_vehicleOption); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_LFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_LfICInit_CS(arg1) (RE_LficInit(arg1), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_IOHWAB_RFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_RFIC_APPL_CODE) RE_RficDioInterface_Read(P2VAR(IOHWAB_UINT8, AUTOMATIC, RTE_IOHWAB_RFIC_APPL_VAR) ReadValue); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_RFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RficDioInterface_P_Read RE_RficDioInterface_Read
#  define RTE_START_SEC_IOHWAB_RFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_IOHWAB_RFIC_APPL_CODE) RE_rfic_IRQ_ActiveCheck(uint8 kb_TerminalControlState); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_RFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RficIRQActiveCheck_CS(arg1) (RE_rfic_IRQ_ActiveCheck(arg1), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_IOHWAB_RFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_IOHWAB_RFIC_APPL_CODE) RE_rfic_init(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_RFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RficInit_CS() (RE_rfic_init(), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_IOHWAB_RFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_IOHWAB_RFIC_APPL_CODE) RE_SearchSysmode(uint8 RficSystemMode, uint8 RFIC_ReqCmd); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_RFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SearchSysMode_CS(arg1, arg2) (RE_SearchSysmode(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_IOHWAB_LFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_IOHWAB_LFIC_APPL_CODE) RE_SetupLfTelegram(uint8 b_AntennaIndex, uint8 b_SearchPattern, uint16 Counter_LFRawDataBit, P2CONST(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_DATA) Buffer_LFRawData); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  else
FUNC(void, RTE_IOHWAB_LFIC_APPL_CODE) RE_SetupLfTelegram(uint8 b_AntennaIndex, uint8 b_SearchPattern, uint16 Counter_LFRawDataBit, P2CONST(ArrayByteSize32, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_DATA) Buffer_LFRawData); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  endif
#  define RTE_STOP_SEC_IOHWAB_LFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SetupLfTelegram_CS(arg1, arg2, arg3, arg4) (RE_SetupLfTelegram(arg1, arg2, arg3, arg4), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_IOHWAB_LFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_IOHWAB_LFIC_APPL_CODE) RE_TimeoutTxTelegram(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_LFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_TimeoutTxTelegram_CS() (RE_TimeoutTxTelegram(), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2D_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_AntMappingConfig_Gain_X1C03_a_T, RTE_CONST_SA_lvl_0x2D) Rte_AddrPar_0x2D_X1C03_AntMappingConfig_Gain_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AntMappingConfig_Multi_X1CY3_a_T, RTE_CONST_SA_lvl_0x2D) Rte_AddrPar_0x2D_X1CY3_AntMappingConfig_Multi_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AntMappingConfig_Single_X1CY5_s_T, RTE_CONST_SA_lvl_0x2D) Rte_AddrPar_0x2D_X1CY5_AntMappingConfig_Single_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2D_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1C03_AntMappingConfig_Gain_v() (&(Rte_AddrPar_0x2D_X1C03_AntMappingConfig_Gain_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1C03_AntMappingConfig_Gain_v() (&Rte_AddrPar_0x2D_X1C03_AntMappingConfig_Gain_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CY3_AntMappingConfig_Multi_v() ((P2CONST(SEWS_AntMappingConfig_Multi_X1CY3a_T, AUTOMATIC, RTE_VAR_NOINIT)) &(Rte_AddrPar_0x2D_X1CY3_AntMappingConfig_Multi_v[0][0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CY3_AntMappingConfig_Multi_v() (&Rte_AddrPar_0x2D_X1CY3_AntMappingConfig_Multi_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  define Rte_Prm_X1CY5_AntMappingConfig_Single_v() (&Rte_AddrPar_0x2D_X1CY5_AntMappingConfig_Single_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_KeyfobDetectionMappingSelection_P1WIP_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1WIP_KeyfobDetectionMappingSelection_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_KeyfobDetectionMappingConfig_P1WIR_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1WIR_KeyfobDetectionMappingConfig_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AuxPassiveAntennasActivation_P1WIQ_s_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1WIQ_AuxPassiveAntennasActivation_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1WIP_KeyfobDetectionMappingSelection_v() (Rte_AddrPar_0x2F_P1WIP_KeyfobDetectionMappingSelection_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1WIR_KeyfobDetectionMappingConfig_v() (Rte_AddrPar_0x2F_P1WIR_KeyfobDetectionMappingConfig_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1WIQ_AuxPassiveAntennasActivation_v() (&Rte_AddrPar_0x2F_P1WIQ_AuxPassiveAntennasActivation_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define PEPS_START_SEC_CODE
# include "PEPS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_ReadData DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_ReadData
#  define RTE_RUNNABLE_DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_WriteData DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_WriteData
#  define RTE_RUNNABLE_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult GetKeyfobPassiveSearchResult
#  define RTE_RUNNABLE_KeyfobPassiveSearchRqst_KeyfobPassiveSearch KeyfobPassiveSearch
#  define RTE_RUNNABLE_PEPS_10ms_runnable PEPS_10ms_runnable
#  define RTE_RUNNABLE_RE_Check_Passive_Encryption RE_Check_Passive_Encryption
#  define RTE_RUNNABLE_RE_Clear_HighFixCheckTimer RE_Clear_HighFixCheckTimer
#  define RTE_RUNNABLE_RE_Generate_Telegram_EncryptionKey RE_Generate_Telegram_EncryptionKey
#  define RTE_RUNNABLE_RE_GetLfAntState RE_GetLfAntState
#  define RTE_RUNNABLE_RE_PassiveEncryptionManager RE_PassiveEncryptionManager
#  define RTE_RUNNABLE_RE_Request_Antena_Check RE_Request_Antena_Check
#  define RTE_RUNNABLE_RE_RficSysModeConfirm_PEPS RE_RficSysModeConfirm_PEPS
#  define RTE_RUNNABLE_RE_Select_RF_WaitTime RE_Select_RF_WaitTime
#  define RTE_RUNNABLE_RE_Set_ValidFobFoundResult RE_Set_ValidFobFoundResult
#  define RTE_RUNNABLE_RoutineServices_Y1ABE_AntennaMappingByDidActivation_Start RoutineServices_Y1ABE_AntennaMappingByDidActivation_Start
# endif

# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, PEPS_CODE) DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_ReadData(P2VAR(uint8, AUTOMATIC, RTE_PEPS_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, PEPS_CODE) DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_ReadData(P2VAR(Dcm_Data40ByteType, AUTOMATIC, RTE_PEPS_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, PEPS_CODE) DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_WriteData(P2CONST(uint8, AUTOMATIC, RTE_PEPS_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_PEPS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, PEPS_CODE) DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_WriteData(P2CONST(Dcm_Data40ByteType, AUTOMATIC, RTE_PEPS_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_PEPS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, PEPS_CODE) GetKeyfobPassiveSearchResult(P2VAR(SCIM_PassiveDriver_ProcessingStatus_T, AUTOMATIC, RTE_PEPS_APPL_VAR) ProcessingStatus, P2VAR(KeyfobInCabLocation_stat_T, AUTOMATIC, RTE_PEPS_APPL_VAR) KeyfobLocationbyPassive_Incab, P2VAR(KeyfobOutsideLocation_stat_T, AUTOMATIC, RTE_PEPS_APPL_VAR) KeyfobLocationbyPassive_Outcab); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, PEPS_CODE) KeyfobPassiveSearch(SCIM_PassiveSearchCoverage_T PassiveSearchCoverage, boolean SearchRequestedbyPassive); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, PEPS_CODE) PEPS_10ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, PEPS_CODE) RE_Check_Passive_Encryption(P2VAR(uint8, AUTOMATIC, RTE_PEPS_APPL_VAR) RcvData); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, PEPS_CODE) RE_Clear_HighFixCheckTimer(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, PEPS_CODE) RE_Generate_Telegram_EncryptionKey(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, PEPS_CODE) RE_GetLfAntState(P2VAR(LfRssi, AUTOMATIC, RTE_PEPS_APPL_VAR) LfRssiStatus, P2VAR(uint8, AUTOMATIC, RTE_PEPS_APPL_VAR) FobFound, P2VAR(uint8, AUTOMATIC, RTE_PEPS_APPL_VAR) FobLocation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, PEPS_CODE) RE_PassiveEncryptionManager(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, PEPS_CODE) RE_Request_Antena_Check(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, PEPS_CODE) RE_RficSysModeConfirm_PEPS(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, PEPS_CODE) RE_Select_RF_WaitTime(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, PEPS_CODE) RE_Set_ValidFobFoundResult(uint8 fobnum); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, PEPS_CODE) RoutineServices_Y1ABE_AntennaMappingByDidActivation_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_PEPS_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_PEPS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, PEPS_CODE) RoutineServices_Y1ABE_AntennaMappingByDidActivation_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_PEPS_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_PEPS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define PEPS_STOP_SEC_CODE
# include "PEPS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_X1CY4_Data_X1CY4_E_NOT_OK (1U)

#  define RTE_E_Dio_I_IoHwAbApplicationError (1U)

#  define RTE_E_LfInterface_I_RadioApplicationError (1U)

#  define RTE_E_RoutineServices_Y1ABE_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Y1ABE_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Y1ABE_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_PEPS_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
