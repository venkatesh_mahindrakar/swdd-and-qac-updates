/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_FMSGateway_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <FMSGateway>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_FMSGATEWAY_TYPE_H
# define _RTE_FMSGATEWAY_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef BrakeSwitch_BrakePedalReleased
#   define BrakeSwitch_BrakePedalReleased (0U)
#  endif

#  ifndef BrakeSwitch_BrakePedalDepressed
#   define BrakeSwitch_BrakePedalDepressed (1U)
#  endif

#  ifndef BrakeSwitch_Error
#   define BrakeSwitch_Error (2U)
#  endif

#  ifndef BrakeSwitch_NotAvailable
#   define BrakeSwitch_NotAvailable (3U)
#  endif

#  ifndef ClutchSwitch_ClutchPedalReleased
#   define ClutchSwitch_ClutchPedalReleased (0U)
#  endif

#  ifndef ClutchSwitch_ClutchPedalDepressed
#   define ClutchSwitch_ClutchPedalDepressed (1U)
#  endif

#  ifndef ClutchSwitch_Error
#   define ClutchSwitch_Error (2U)
#  endif

#  ifndef ClutchSwitch_NotAvailable
#   define ClutchSwitch_NotAvailable (3U)
#  endif

#  ifndef DirectionIndicator_Forward
#   define DirectionIndicator_Forward (0U)
#  endif

#  ifndef DirectionIndicator_Reverse
#   define DirectionIndicator_Reverse (1U)
#  endif

#  ifndef DirectionIndicator_Error
#   define DirectionIndicator_Error (2U)
#  endif

#  ifndef DirectionIndicator_NotAvailable
#   define DirectionIndicator_NotAvailable (3U)
#  endif

#  ifndef Driver1TimeRelatedStates_NormalNoLimitsReached
#   define Driver1TimeRelatedStates_NormalNoLimitsReached (0U)
#  endif

#  ifndef Driver1TimeRelatedStates_15minBefore4h30min
#   define Driver1TimeRelatedStates_15minBefore4h30min (1U)
#  endif

#  ifndef Driver1TimeRelatedStates_4h30minReached
#   define Driver1TimeRelatedStates_4h30minReached (2U)
#  endif

#  ifndef Driver1TimeRelatedStates_DailyDrivingTimePreWarning
#   define Driver1TimeRelatedStates_DailyDrivingTimePreWarning (3U)
#  endif

#  ifndef Driver1TimeRelatedStates_DailyDrivingTimeWarning
#   define Driver1TimeRelatedStates_DailyDrivingTimeWarning (4U)
#  endif

#  ifndef Driver1TimeRelatedStates_DailyWeeklyRestPreWarning
#   define Driver1TimeRelatedStates_DailyWeeklyRestPreWarning (5U)
#  endif

#  ifndef Driver1TimeRelatedStates_DailyWeeklyRestWarning
#   define Driver1TimeRelatedStates_DailyWeeklyRestWarning (6U)
#  endif

#  ifndef Driver1TimeRelatedStates_WeeklyDrivingTimePreWarning
#   define Driver1TimeRelatedStates_WeeklyDrivingTimePreWarning (7U)
#  endif

#  ifndef Driver1TimeRelatedStates_WeeklyDrivingTimeWarning
#   define Driver1TimeRelatedStates_WeeklyDrivingTimeWarning (8U)
#  endif

#  ifndef Driver1TimeRelatedStates_2WeekDrivingTimePreWarning
#   define Driver1TimeRelatedStates_2WeekDrivingTimePreWarning (9U)
#  endif

#  ifndef Driver1TimeRelatedStates_2WeekDrivingTimeWarning
#   define Driver1TimeRelatedStates_2WeekDrivingTimeWarning (10U)
#  endif

#  ifndef Driver1TimeRelatedStates_Driver1CardExpiryWarning
#   define Driver1TimeRelatedStates_Driver1CardExpiryWarning (11U)
#  endif

#  ifndef Driver1TimeRelatedStates_NextMandatoryDriver1CardDownload
#   define Driver1TimeRelatedStates_NextMandatoryDriver1CardDownload (12U)
#  endif

#  ifndef Driver1TimeRelatedStates_Other
#   define Driver1TimeRelatedStates_Other (13U)
#  endif

#  ifndef Driver1TimeRelatedStates_Error
#   define Driver1TimeRelatedStates_Error (14U)
#  endif

#  ifndef Driver1TimeRelatedStates_NotAvailable
#   define Driver1TimeRelatedStates_NotAvailable (15U)
#  endif

#  ifndef DriverTimeRelatedStates_NormalNoLimitsReached
#   define DriverTimeRelatedStates_NormalNoLimitsReached (0U)
#  endif

#  ifndef DriverTimeRelatedStates_Limit1_15minBefore4h30min
#   define DriverTimeRelatedStates_Limit1_15minBefore4h30min (1U)
#  endif

#  ifndef DriverTimeRelatedStates_Limit2_4h30minReached
#   define DriverTimeRelatedStates_Limit2_4h30minReached (2U)
#  endif

#  ifndef DriverTimeRelatedStates_Limit3_15MinutesBefore9h
#   define DriverTimeRelatedStates_Limit3_15MinutesBefore9h (3U)
#  endif

#  ifndef DriverTimeRelatedStates_Limit4_9hReached
#   define DriverTimeRelatedStates_Limit4_9hReached (4U)
#  endif

#  ifndef DriverTimeRelatedStates_Limit5_15MinutesBefore16h
#   define DriverTimeRelatedStates_Limit5_15MinutesBefore16h (5U)
#  endif

#  ifndef DriverTimeRelatedStates_Limit6_16hReached
#   define DriverTimeRelatedStates_Limit6_16hReached (6U)
#  endif

#  ifndef DriverTimeRelatedStates_Reserved
#   define DriverTimeRelatedStates_Reserved (7U)
#  endif

#  ifndef DriverTimeRelatedStates_Reserved_01
#   define DriverTimeRelatedStates_Reserved_01 (8U)
#  endif

#  ifndef DriverTimeRelatedStates_Reserved_02
#   define DriverTimeRelatedStates_Reserved_02 (9U)
#  endif

#  ifndef DriverTimeRelatedStates_Reserved_03
#   define DriverTimeRelatedStates_Reserved_03 (10U)
#  endif

#  ifndef DriverTimeRelatedStates_Reserved_04
#   define DriverTimeRelatedStates_Reserved_04 (11U)
#  endif

#  ifndef DriverTimeRelatedStates_Reserved_05
#   define DriverTimeRelatedStates_Reserved_05 (12U)
#  endif

#  ifndef DriverTimeRelatedStates_Other
#   define DriverTimeRelatedStates_Other (13U)
#  endif

#  ifndef DriverTimeRelatedStates_Error
#   define DriverTimeRelatedStates_Error (14U)
#  endif

#  ifndef DriverTimeRelatedStates_NotAvailable
#   define DriverTimeRelatedStates_NotAvailable (15U)
#  endif

#  ifndef DriverWorkingState_RestSleeping
#   define DriverWorkingState_RestSleeping (0U)
#  endif

#  ifndef DriverWorkingState_DriverAvailableShortBreak
#   define DriverWorkingState_DriverAvailableShortBreak (1U)
#  endif

#  ifndef DriverWorkingState_WorkLoadingUnloadingWorkingInAnOffice
#   define DriverWorkingState_WorkLoadingUnloadingWorkingInAnOffice (2U)
#  endif

#  ifndef DriverWorkingState_DriveBehindWheel
#   define DriverWorkingState_DriveBehindWheel (3U)
#  endif

#  ifndef DriverWorkingState_Reserved
#   define DriverWorkingState_Reserved (4U)
#  endif

#  ifndef DriverWorkingState_Reserved_01
#   define DriverWorkingState_Reserved_01 (5U)
#  endif

#  ifndef DriverWorkingState_ErrorIndicator
#   define DriverWorkingState_ErrorIndicator (6U)
#  endif

#  ifndef DriverWorkingState_NotAvailable
#   define DriverWorkingState_NotAvailable (7U)
#  endif

#  ifndef EngineRetarderTorqueMode_LowIdleGovernorNoRequestDefaultMode
#   define EngineRetarderTorqueMode_LowIdleGovernorNoRequestDefaultMode (0U)
#  endif

#  ifndef EngineRetarderTorqueMode_AcceleratorPedalOperatorSelection
#   define EngineRetarderTorqueMode_AcceleratorPedalOperatorSelection (1U)
#  endif

#  ifndef EngineRetarderTorqueMode_CruiseControl
#   define EngineRetarderTorqueMode_CruiseControl (2U)
#  endif

#  ifndef EngineRetarderTorqueMode_PTOGovernor
#   define EngineRetarderTorqueMode_PTOGovernor (3U)
#  endif

#  ifndef EngineRetarderTorqueMode_RoadSpeedGovernor
#   define EngineRetarderTorqueMode_RoadSpeedGovernor (4U)
#  endif

#  ifndef EngineRetarderTorqueMode_ASRControl
#   define EngineRetarderTorqueMode_ASRControl (5U)
#  endif

#  ifndef EngineRetarderTorqueMode_TransmissionControl
#   define EngineRetarderTorqueMode_TransmissionControl (6U)
#  endif

#  ifndef EngineRetarderTorqueMode_ABSControl
#   define EngineRetarderTorqueMode_ABSControl (7U)
#  endif

#  ifndef EngineRetarderTorqueMode_TorqueLimiting
#   define EngineRetarderTorqueMode_TorqueLimiting (8U)
#  endif

#  ifndef EngineRetarderTorqueMode_HighSpeedGovernor
#   define EngineRetarderTorqueMode_HighSpeedGovernor (9U)
#  endif

#  ifndef EngineRetarderTorqueMode_BrakingSystem
#   define EngineRetarderTorqueMode_BrakingSystem (10U)
#  endif

#  ifndef EngineRetarderTorqueMode_RemoteAccelerator
#   define EngineRetarderTorqueMode_RemoteAccelerator (11U)
#  endif

#  ifndef EngineRetarderTorqueMode_ServiceProcedure
#   define EngineRetarderTorqueMode_ServiceProcedure (12U)
#  endif

#  ifndef EngineRetarderTorqueMode_NotDefined
#   define EngineRetarderTorqueMode_NotDefined (13U)
#  endif

#  ifndef EngineRetarderTorqueMode_Other
#   define EngineRetarderTorqueMode_Other (14U)
#  endif

#  ifndef EngineRetarderTorqueMode_NotAvailable
#   define EngineRetarderTorqueMode_NotAvailable (15U)
#  endif

#  ifndef FuelType_NotAvailable_NONE
#   define FuelType_NotAvailable_NONE (0U)
#  endif

#  ifndef FuelType_GasolinePetrol_GAS
#   define FuelType_GasolinePetrol_GAS (1U)
#  endif

#  ifndef FuelType_Methanol_METH
#   define FuelType_Methanol_METH (2U)
#  endif

#  ifndef FuelType_Ethanol_ETH
#   define FuelType_Ethanol_ETH (3U)
#  endif

#  ifndef FuelType_Diesel_DSL
#   define FuelType_Diesel_DSL (4U)
#  endif

#  ifndef FuelType_LiquefiedPetroleumGas_LPG
#   define FuelType_LiquefiedPetroleumGas_LPG (5U)
#  endif

#  ifndef FuelType_CompressedNaturalGas_CNG
#   define FuelType_CompressedNaturalGas_CNG (6U)
#  endif

#  ifndef FuelType_Propane_PROP
#   define FuelType_Propane_PROP (7U)
#  endif

#  ifndef FuelType_BatteryElectric_ELEC
#   define FuelType_BatteryElectric_ELEC (8U)
#  endif

#  ifndef FuelType_BifuelVehicleGasoline_BI_GAS
#   define FuelType_BifuelVehicleGasoline_BI_GAS (9U)
#  endif

#  ifndef FuelType_BifuelVehicleMethanol_BI_METH
#   define FuelType_BifuelVehicleMethanol_BI_METH (10U)
#  endif

#  ifndef FuelType_BifuelVehicleEthanol_BI_ETH
#   define FuelType_BifuelVehicleEthanol_BI_ETH (11U)
#  endif

#  ifndef FuelType_BifuelVehicleLPG_BI_LPG
#   define FuelType_BifuelVehicleLPG_BI_LPG (12U)
#  endif

#  ifndef FuelType_BifuelVehicleCNG_BI_CNG
#   define FuelType_BifuelVehicleCNG_BI_CNG (13U)
#  endif

#  ifndef FuelType_BifuelVehiclePropane_BI_PROP
#   define FuelType_BifuelVehiclePropane_BI_PROP (14U)
#  endif

#  ifndef FuelType_BifuelVehicleBattery_BI_ELEC
#   define FuelType_BifuelVehicleBattery_BI_ELEC (15U)
#  endif

#  ifndef FuelType_BifuelVehicleBatteryCombustion_BI_MIX
#   define FuelType_BifuelVehicleBatteryCombustion_BI_MIX (16U)
#  endif

#  ifndef FuelType_HybridVehicleGasoline_HYB_GAS
#   define FuelType_HybridVehicleGasoline_HYB_GAS (17U)
#  endif

#  ifndef FuelType_HybridVehicleEthanol_HYB_ETH
#   define FuelType_HybridVehicleEthanol_HYB_ETH (18U)
#  endif

#  ifndef FuelType_HybridVehicleDiesel_HYB_DSL
#   define FuelType_HybridVehicleDiesel_HYB_DSL (19U)
#  endif

#  ifndef FuelType_HybridVehicleBattery_HYB_ELEC
#   define FuelType_HybridVehicleBattery_HYB_ELEC (20U)
#  endif

#  ifndef FuelType_HybridVehicleBatteryAndCombustion_HYB_MIX
#   define FuelType_HybridVehicleBatteryAndCombustion_HYB_MIX (21U)
#  endif

#  ifndef FuelType_HybridVehicleRegenerationMode_HYB_REG
#   define FuelType_HybridVehicleRegenerationMode_HYB_REG (22U)
#  endif

#  ifndef FuelType_NaturalGas_NG
#   define FuelType_NaturalGas_NG (23U)
#  endif

#  ifndef FuelType_BifuelVehicleNG_BI_NG
#   define FuelType_BifuelVehicleNG_BI_NG (24U)
#  endif

#  ifndef FuelType_BifuelDiesel
#   define FuelType_BifuelDiesel (25U)
#  endif

#  ifndef FuelType_NaturalGasCompressedOrLiquefied
#   define FuelType_NaturalGasCompressedOrLiquefied (26U)
#  endif

#  ifndef FuelType_DualFuel_DieselAndCNG
#   define FuelType_DualFuel_DieselAndCNG (27U)
#  endif

#  ifndef FuelType_DualFuel_DieselAndLNG
#   define FuelType_DualFuel_DieselAndLNG (28U)
#  endif

#  ifndef FuelType_Error
#   define FuelType_Error (254U)
#  endif

#  ifndef FuelType_NotAvailable
#   define FuelType_NotAvailable (255U)
#  endif

#  ifndef HandlingInformation_NoHandlingInformation
#   define HandlingInformation_NoHandlingInformation (0U)
#  endif

#  ifndef HandlingInformation_HandlingInformation
#   define HandlingInformation_HandlingInformation (1U)
#  endif

#  ifndef HandlingInformation_Error
#   define HandlingInformation_Error (2U)
#  endif

#  ifndef HandlingInformation_NotAvailable
#   define HandlingInformation_NotAvailable (3U)
#  endif

#  ifndef NotDetected_NotDetected
#   define NotDetected_NotDetected (0U)
#  endif

#  ifndef NotDetected_Detected
#   define NotDetected_Detected (1U)
#  endif

#  ifndef NotDetected_Error
#   define NotDetected_Error (2U)
#  endif

#  ifndef NotDetected_NotAvaliable
#   define NotDetected_NotAvaliable (3U)
#  endif

#  ifndef NotPresentPresent_NotPresent
#   define NotPresentPresent_NotPresent (0U)
#  endif

#  ifndef NotPresentPresent_Present
#   define NotPresentPresent_Present (1U)
#  endif

#  ifndef NotPresentPresent_ErrorIndicator
#   define NotPresentPresent_ErrorIndicator (2U)
#  endif

#  ifndef NotPresentPresent_NotAvailable
#   define NotPresentPresent_NotAvailable (3U)
#  endif

#  ifndef OffOn_Off
#   define OffOn_Off (0U)
#  endif

#  ifndef OffOn_On
#   define OffOn_On (1U)
#  endif

#  ifndef OffOn_Error
#   define OffOn_Error (2U)
#  endif

#  ifndef OffOn_NotAvailable
#   define OffOn_NotAvailable (3U)
#  endif

#  ifndef OilQuality_NonVDS_VDS4WithFixedInterval
#   define OilQuality_NonVDS_VDS4WithFixedInterval (0U)
#  endif

#  ifndef OilQuality_VDSVDS3WithFixedInterval
#   define OilQuality_VDSVDS3WithFixedInterval (1U)
#  endif

#  ifndef OilQuality_VDS5
#   define OilQuality_VDS5 (2U)
#  endif

#  ifndef OilQuality_VDS3
#   define OilQuality_VDS3 (3U)
#  endif

#  ifndef OilQuality_VDS4
#   define OilQuality_VDS4 (4U)
#  endif

#  ifndef OilQuality_NoSelectionMade
#   define OilQuality_NoSelectionMade (5U)
#  endif

#  ifndef OilQuality_Error
#   define OilQuality_Error (6U)
#  endif

#  ifndef OilQuality_NotAvailable
#   define OilQuality_NotAvailable (7U)
#  endif

#  ifndef OilStatus_NotActive
#   define OilStatus_NotActive (0U)
#  endif

#  ifndef OilStatus_ServiceOverDue
#   define OilStatus_ServiceOverDue (1U)
#  endif

#  ifndef OilStatus_ServiceDue
#   define OilStatus_ServiceDue (2U)
#  endif

#  ifndef OilStatus_ServiceNearDue
#   define OilStatus_ServiceNearDue (3U)
#  endif

#  ifndef OilStatus_Ok
#   define OilStatus_Ok (4U)
#  endif

#  ifndef OilStatus_Spare
#   define OilStatus_Spare (5U)
#  endif

#  ifndef OilStatus_Error
#   define OilStatus_Error (6U)
#  endif

#  ifndef OilStatus_NotAvailable
#   define OilStatus_NotAvailable (7U)
#  endif

#  ifndef PtoState_OffDisabled
#   define PtoState_OffDisabled (0U)
#  endif

#  ifndef PtoState_Hold
#   define PtoState_Hold (1U)
#  endif

#  ifndef PtoState_RemoteHold
#   define PtoState_RemoteHold (2U)
#  endif

#  ifndef PtoState_Standby
#   define PtoState_Standby (3U)
#  endif

#  ifndef PtoState_RemoteStandby
#   define PtoState_RemoteStandby (4U)
#  endif

#  ifndef PtoState_Set
#   define PtoState_Set (5U)
#  endif

#  ifndef PtoState_DecelerateCoast
#   define PtoState_DecelerateCoast (6U)
#  endif

#  ifndef PtoState_Resume
#   define PtoState_Resume (7U)
#  endif

#  ifndef PtoState_Accelerate
#   define PtoState_Accelerate (8U)
#  endif

#  ifndef PtoState_AcceleratorOverride
#   define PtoState_AcceleratorOverride (9U)
#  endif

#  ifndef PtoState_PreprogramSetSpeed1
#   define PtoState_PreprogramSetSpeed1 (10U)
#  endif

#  ifndef PtoState_PreprogramSetSpeed2
#   define PtoState_PreprogramSetSpeed2 (11U)
#  endif

#  ifndef PtoState_PreprogramSetSpeed3
#   define PtoState_PreprogramSetSpeed3 (12U)
#  endif

#  ifndef PtoState_PreprogramSetSpeed4
#   define PtoState_PreprogramSetSpeed4 (13U)
#  endif

#  ifndef PtoState_PreprogramSetSpeed5
#   define PtoState_PreprogramSetSpeed5 (14U)
#  endif

#  ifndef PtoState_PreprogramSetSpeed6
#   define PtoState_PreprogramSetSpeed6 (15U)
#  endif

#  ifndef PtoState_PreprogramSetSpeed7
#   define PtoState_PreprogramSetSpeed7 (16U)
#  endif

#  ifndef PtoState_PreprogramSetSpeed8
#   define PtoState_PreprogramSetSpeed8 (17U)
#  endif

#  ifndef PtoState_PTOSetSpeedMemory1
#   define PtoState_PTOSetSpeedMemory1 (18U)
#  endif

#  ifndef PtoState_PTOSetSpeedMemory2
#   define PtoState_PTOSetSpeedMemory2 (19U)
#  endif

#  ifndef PtoState_NotDefined
#   define PtoState_NotDefined (20U)
#  endif

#  ifndef PtoState_NotDefined01
#   define PtoState_NotDefined01 (21U)
#  endif

#  ifndef PtoState_NotDefined02
#   define PtoState_NotDefined02 (22U)
#  endif

#  ifndef PtoState_NotDefined03
#   define PtoState_NotDefined03 (23U)
#  endif

#  ifndef PtoState_NotDefined04
#   define PtoState_NotDefined04 (24U)
#  endif

#  ifndef PtoState_NotDefined05
#   define PtoState_NotDefined05 (25U)
#  endif

#  ifndef PtoState_NotDefined06
#   define PtoState_NotDefined06 (26U)
#  endif

#  ifndef PtoState_NotDefined07
#   define PtoState_NotDefined07 (27U)
#  endif

#  ifndef PtoState_NotDefined08
#   define PtoState_NotDefined08 (28U)
#  endif

#  ifndef PtoState_NotDefined09
#   define PtoState_NotDefined09 (29U)
#  endif

#  ifndef PtoState_NotDefined10
#   define PtoState_NotDefined10 (30U)
#  endif

#  ifndef PtoState_NotAvailable
#   define PtoState_NotAvailable (31U)
#  endif

#  ifndef PtosStatus_NoPTOEngaged
#   define PtosStatus_NoPTOEngaged (0U)
#  endif

#  ifndef PtosStatus_OneOrMorePTOEngaged
#   define PtosStatus_OneOrMorePTOEngaged (1U)
#  endif

#  ifndef PtosStatus_Error
#   define PtosStatus_Error (2U)
#  endif

#  ifndef PtosStatus_NotAvailable
#   define PtosStatus_NotAvailable (3U)
#  endif

#  ifndef RetarderTorqueMode_LowIdleGovernorNoRequestDefaultMode
#   define RetarderTorqueMode_LowIdleGovernorNoRequestDefaultMode (0U)
#  endif

#  ifndef RetarderTorqueMode_AcceleratorPedalOperatorSelection
#   define RetarderTorqueMode_AcceleratorPedalOperatorSelection (1U)
#  endif

#  ifndef RetarderTorqueMode_CruiseControl
#   define RetarderTorqueMode_CruiseControl (2U)
#  endif

#  ifndef RetarderTorqueMode_PTOGovernor
#   define RetarderTorqueMode_PTOGovernor (3U)
#  endif

#  ifndef RetarderTorqueMode_RoadSpeedGovernor
#   define RetarderTorqueMode_RoadSpeedGovernor (4U)
#  endif

#  ifndef RetarderTorqueMode_ASRControl
#   define RetarderTorqueMode_ASRControl (5U)
#  endif

#  ifndef RetarderTorqueMode_TransmissionControl
#   define RetarderTorqueMode_TransmissionControl (6U)
#  endif

#  ifndef RetarderTorqueMode_ABSControl
#   define RetarderTorqueMode_ABSControl (7U)
#  endif

#  ifndef RetarderTorqueMode_TorqueLimiting
#   define RetarderTorqueMode_TorqueLimiting (8U)
#  endif

#  ifndef RetarderTorqueMode_HighSpeedGovernor
#   define RetarderTorqueMode_HighSpeedGovernor (9U)
#  endif

#  ifndef RetarderTorqueMode_BrakingSystem
#   define RetarderTorqueMode_BrakingSystem (10U)
#  endif

#  ifndef RetarderTorqueMode_RemoteAccelerator
#   define RetarderTorqueMode_RemoteAccelerator (11U)
#  endif

#  ifndef RetarderTorqueMode_ServiceProcedure
#   define RetarderTorqueMode_ServiceProcedure (12U)
#  endif

#  ifndef RetarderTorqueMode_NotDefined
#   define RetarderTorqueMode_NotDefined (13U)
#  endif

#  ifndef RetarderTorqueMode_Other
#   define RetarderTorqueMode_Other (14U)
#  endif

#  ifndef RetarderTorqueMode_NotAvailable
#   define RetarderTorqueMode_NotAvailable (15U)
#  endif

#  ifndef ReverseGearEngaged_ReverseGearNotEngaged
#   define ReverseGearEngaged_ReverseGearNotEngaged (0U)
#  endif

#  ifndef ReverseGearEngaged_ReverseGearEngaged
#   define ReverseGearEngaged_ReverseGearEngaged (1U)
#  endif

#  ifndef ReverseGearEngaged_Error
#   define ReverseGearEngaged_Error (2U)
#  endif

#  ifndef ReverseGearEngaged_NotAvailable
#   define ReverseGearEngaged_NotAvailable (3U)
#  endif

#  ifndef Supported_NotSupported
#   define Supported_NotSupported (0U)
#  endif

#  ifndef Supported_Supported
#   define Supported_Supported (1U)
#  endif

#  ifndef Supported_Reserved
#   define Supported_Reserved (2U)
#  endif

#  ifndef Supported_DontCare
#   define Supported_DontCare (3U)
#  endif

#  ifndef SystemEvent_NoTachographEvent
#   define SystemEvent_NoTachographEvent (0U)
#  endif

#  ifndef SystemEvent_TachographEvent
#   define SystemEvent_TachographEvent (1U)
#  endif

#  ifndef SystemEvent_Error
#   define SystemEvent_Error (2U)
#  endif

#  ifndef SystemEvent_NotAvailable
#   define SystemEvent_NotAvailable (3U)
#  endif

#  ifndef TachographPerformance_NormalPerformance
#   define TachographPerformance_NormalPerformance (0U)
#  endif

#  ifndef TachographPerformance_PerformanceAnalysis
#   define TachographPerformance_PerformanceAnalysis (1U)
#  endif

#  ifndef TachographPerformance_Error
#   define TachographPerformance_Error (2U)
#  endif

#  ifndef TachographPerformance_NotAvailable
#   define TachographPerformance_NotAvailable (3U)
#  endif

#  ifndef TellTaleStatus_Off
#   define TellTaleStatus_Off (0U)
#  endif

#  ifndef TellTaleStatus_Red
#   define TellTaleStatus_Red (1U)
#  endif

#  ifndef TellTaleStatus_Yellow
#   define TellTaleStatus_Yellow (2U)
#  endif

#  ifndef TellTaleStatus_Info
#   define TellTaleStatus_Info (3U)
#  endif

#  ifndef TellTaleStatus_Reserved1
#   define TellTaleStatus_Reserved1 (4U)
#  endif

#  ifndef TellTaleStatus_Reserved2
#   define TellTaleStatus_Reserved2 (5U)
#  endif

#  ifndef TellTaleStatus_Reserved3
#   define TellTaleStatus_Reserved3 (6U)
#  endif

#  ifndef TellTaleStatus_NotAvailable
#   define TellTaleStatus_NotAvailable (7U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

#  ifndef VehicleOverspeed_NoOverspeed
#   define VehicleOverspeed_NoOverspeed (0U)
#  endif

#  ifndef VehicleOverspeed_Overspeed
#   define VehicleOverspeed_Overspeed (1U)
#  endif

#  ifndef VehicleOverspeed_PreOverspeed
#   define VehicleOverspeed_PreOverspeed (2U)
#  endif

#  ifndef VehicleOverspeed_NotAvailable
#   define VehicleOverspeed_NotAvailable (3U)
#  endif

#  ifndef WeightClass_Spare0
#   define WeightClass_Spare0 (0U)
#  endif

#  ifndef WeightClass_PassengerVehicleClassM1
#   define WeightClass_PassengerVehicleClassM1 (1U)
#  endif

#  ifndef WeightClass_PassengerVehicleClassM2
#   define WeightClass_PassengerVehicleClassM2 (2U)
#  endif

#  ifndef WeightClass_PassengerVehicleClassM3
#   define WeightClass_PassengerVehicleClassM3 (3U)
#  endif

#  ifndef WeightClass_LightCommercialVehiclesClassN1
#   define WeightClass_LightCommercialVehiclesClassN1 (4U)
#  endif

#  ifndef WeightClass_HeavyDutyVehiclesClassN2
#   define WeightClass_HeavyDutyVehiclesClassN2 (5U)
#  endif

#  ifndef WeightClass_HeavyDutyVehiclesClassN3
#   define WeightClass_HeavyDutyVehiclesClassN3 (6U)
#  endif

#  ifndef WeightClass_Spare1
#   define WeightClass_Spare1 (7U)
#  endif

#  ifndef WeightClass_Spare2
#   define WeightClass_Spare2 (8U)
#  endif

#  ifndef WeightClass_Spare3
#   define WeightClass_Spare3 (9U)
#  endif

#  ifndef WeightClass_Spare4
#   define WeightClass_Spare4 (10U)
#  endif

#  ifndef WeightClass_Spare5
#   define WeightClass_Spare5 (11U)
#  endif

#  ifndef WeightClass_Spare6
#   define WeightClass_Spare6 (12U)
#  endif

#  ifndef WeightClass_Spare7
#   define WeightClass_Spare7 (13U)
#  endif

#  ifndef WeightClass_Error
#   define WeightClass_Error (14U)
#  endif

#  ifndef WeightClass_NotAvailable
#   define WeightClass_NotAvailable (15U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_FMSGATEWAY_TYPE_H */
