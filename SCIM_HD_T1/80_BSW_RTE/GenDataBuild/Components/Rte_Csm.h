/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Csm.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <Csm>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_CSM_H
# define _RTE_CSM_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_Csm_Type.h"
# include "Rte_DataHandleType.h"


# define Csm_START_SEC_CODE
# include "Csm_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_AsymDecryptFinish Csm_AsymDecryptFinish
#  define RTE_RUNNABLE_AsymDecryptStart Csm_AsymDecryptStart
#  define RTE_RUNNABLE_AsymDecryptUpdate Csm_AsymDecryptUpdate
#  define RTE_RUNNABLE_Csm_MainFunction Csm_MainFunction
#  define RTE_RUNNABLE_RandomGenerate Csm_RandomGenerate
#  define RTE_RUNNABLE_RandomSeedFinish Csm_RandomSeedFinish
#  define RTE_RUNNABLE_RandomSeedStart Csm_RandomSeedStart
#  define RTE_RUNNABLE_RandomSeedUpdate Csm_RandomSeedUpdate
#  define RTE_RUNNABLE_SignatureVerifyFinish Csm_SignatureVerifyFinish
#  define RTE_RUNNABLE_SignatureVerifyStart Csm_SignatureVerifyStart
#  define RTE_RUNNABLE_SignatureVerifyUpdate Csm_SignatureVerifyUpdate
#  define RTE_RUNNABLE_SymDecryptFinish Csm_SymDecryptFinish
#  define RTE_RUNNABLE_SymDecryptStart Csm_SymDecryptStart
#  define RTE_RUNNABLE_SymDecryptUpdate Csm_SymDecryptUpdate
#  define RTE_RUNNABLE_SymEncryptFinish Csm_SymEncryptFinish
#  define RTE_RUNNABLE_SymEncryptStart Csm_SymEncryptStart
#  define RTE_RUNNABLE_SymEncryptUpdate Csm_SymEncryptUpdate
# endif

# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Csm_CODE) Csm_AsymDecryptFinish(Csm_ConfigIdType parg0, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextBuffer, P2VAR(AsymDecryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# else
FUNC(Std_ReturnType, Csm_CODE) Csm_AsymDecryptFinish(Csm_ConfigIdType parg0, P2VAR(AsymDecryptResultBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextBuffer, P2VAR(AsymDecryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# endif
FUNC(Std_ReturnType, Csm_CODE) Csm_AsymDecryptStart(Csm_ConfigIdType parg0, P2CONST(AsymPrivateKeyType, AUTOMATIC, RTE_CSM_APPL_DATA) key); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Csm_CODE) Csm_AsymDecryptUpdate(Csm_ConfigIdType parg0, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) cipherTextBuffer, uint32 cipherTextLength, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextBuffer, P2VAR(AsymDecryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# else
FUNC(Std_ReturnType, Csm_CODE) Csm_AsymDecryptUpdate(Csm_ConfigIdType parg0, P2CONST(AsymDecryptDataBuffer, AUTOMATIC, RTE_CSM_APPL_DATA) cipherTextBuffer, uint32 cipherTextLength, P2VAR(AsymDecryptResultBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextBuffer, P2VAR(AsymDecryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# endif
FUNC(void, Csm_CODE) Csm_MainFunction(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Csm_CODE) Csm_RandomGenerate(Csm_ConfigIdType parg0, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) resultBuffer, uint32 resultLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# else
FUNC(Std_ReturnType, Csm_CODE) Csm_RandomGenerate(Csm_ConfigIdType parg0, P2VAR(RandomGenerateResultBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) resultBuffer, uint32 resultLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# endif
FUNC(Std_ReturnType, Csm_CODE) Csm_RandomSeedFinish(Csm_ConfigIdType parg0); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
FUNC(Std_ReturnType, Csm_CODE) Csm_RandomSeedStart(Csm_ConfigIdType parg0); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Csm_CODE) Csm_RandomSeedUpdate(Csm_ConfigIdType parg0, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) seedBuffer, uint32 seedLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# else
FUNC(Std_ReturnType, Csm_CODE) Csm_RandomSeedUpdate(Csm_ConfigIdType parg0, P2CONST(RandomSeedDataBuffer, AUTOMATIC, RTE_CSM_APPL_DATA) seedBuffer, uint32 seedLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Csm_CODE) Csm_SignatureVerifyFinish(Csm_ConfigIdType parg0, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) signatureBuffer, uint32 signatureLength, P2VAR(Csm_VerifyResultType, AUTOMATIC, RTE_CSM_APPL_VAR) resultBuffer); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# else
FUNC(Std_ReturnType, Csm_CODE) Csm_SignatureVerifyFinish(Csm_ConfigIdType parg0, P2CONST(SignatureVerifyDataBuffer, AUTOMATIC, RTE_CSM_APPL_DATA) signatureBuffer, uint32 signatureLength, P2VAR(Csm_VerifyResultType, AUTOMATIC, RTE_CSM_APPL_VAR) resultBuffer); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# endif
FUNC(Std_ReturnType, Csm_CODE) Csm_SignatureVerifyStart(Csm_ConfigIdType parg0, P2CONST(AsymPublicKeyType, AUTOMATIC, RTE_CSM_APPL_DATA) key); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Csm_CODE) Csm_SignatureVerifyUpdate(Csm_ConfigIdType parg0, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) dataBuffer, uint32 dataLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# else
FUNC(Std_ReturnType, Csm_CODE) Csm_SignatureVerifyUpdate(Csm_ConfigIdType parg0, P2CONST(SignatureVerifyDataBuffer, AUTOMATIC, RTE_CSM_APPL_DATA) dataBuffer, uint32 dataLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Csm_CODE) Csm_SymDecryptFinish(Csm_ConfigIdType parg0, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextBuffer, P2VAR(SymDecryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# else
FUNC(Std_ReturnType, Csm_CODE) Csm_SymDecryptFinish(Csm_ConfigIdType parg0, P2VAR(SymDecryptResultBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextBuffer, P2VAR(SymDecryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Csm_CODE) Csm_SymDecryptStart(Csm_ConfigIdType parg0, P2CONST(SymKeyType, AUTOMATIC, RTE_CSM_APPL_DATA) key, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) InitVectorBuffer, uint32 InitVectorLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# else
FUNC(Std_ReturnType, Csm_CODE) Csm_SymDecryptStart(Csm_ConfigIdType parg0, P2CONST(SymKeyType, AUTOMATIC, RTE_CSM_APPL_DATA) key, P2CONST(SymDecryptDataBuffer, AUTOMATIC, RTE_CSM_APPL_DATA) InitVectorBuffer, uint32 InitVectorLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Csm_CODE) Csm_SymDecryptUpdate(Csm_ConfigIdType parg0, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) cipherTextBuffer, uint32 cipherTextLength, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextBuffer, P2VAR(SymDecryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# else
FUNC(Std_ReturnType, Csm_CODE) Csm_SymDecryptUpdate(Csm_ConfigIdType parg0, P2CONST(SymDecryptDataBuffer, AUTOMATIC, RTE_CSM_APPL_DATA) cipherTextBuffer, uint32 cipherTextLength, P2VAR(SymDecryptResultBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextBuffer, P2VAR(SymDecryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Csm_CODE) Csm_SymEncryptFinish(Csm_ConfigIdType parg0, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) cipherTextBuffer, P2VAR(SymEncryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) cipherTextLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# else
FUNC(Std_ReturnType, Csm_CODE) Csm_SymEncryptFinish(Csm_ConfigIdType parg0, P2VAR(SymEncryptResultBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) cipherTextBuffer, P2VAR(SymEncryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) cipherTextLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Csm_CODE) Csm_SymEncryptStart(Csm_ConfigIdType parg0, P2CONST(SymKeyType, AUTOMATIC, RTE_CSM_APPL_DATA) key, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) InitVectorBuffer, uint32 InitVectorLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# else
FUNC(Std_ReturnType, Csm_CODE) Csm_SymEncryptStart(Csm_ConfigIdType parg0, P2CONST(SymKeyType, AUTOMATIC, RTE_CSM_APPL_DATA) key, P2CONST(SymEncryptDataBuffer, AUTOMATIC, RTE_CSM_APPL_DATA) InitVectorBuffer, uint32 InitVectorLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Csm_CODE) Csm_SymEncryptUpdate(Csm_ConfigIdType parg0, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) plainTextBuffer, uint32 plainTextLength, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) cipherTextBuffer, P2VAR(SymEncryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) cipherTextLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# else
FUNC(Std_ReturnType, Csm_CODE) Csm_SymEncryptUpdate(Csm_ConfigIdType parg0, P2CONST(SymEncryptDataBuffer, AUTOMATIC, RTE_CSM_APPL_DATA) plainTextBuffer, uint32 plainTextLength, P2VAR(SymEncryptResultBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) cipherTextBuffer, P2VAR(SymEncryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) cipherTextLength); /* PRQA S 0850, 1330, 3451 */ /* MD_MSR_19.8, MD_Rte_1330, MD_Rte_3451 */
# endif

# define Csm_STOP_SEC_CODE
# include "Csm_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_CsmAsymDecrypt_CSM_E_BUSY (2U)

#  define RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK (1U)

#  define RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER (3U)

#  define RTE_E_CsmRandomGenerate_CSM_E_BUSY (2U)

#  define RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION (4U)

#  define RTE_E_CsmRandomGenerate_CSM_E_NOT_OK (1U)

#  define RTE_E_CsmRandomSeed_CSM_E_BUSY (2U)

#  define RTE_E_CsmRandomSeed_CSM_E_NOT_OK (1U)

#  define RTE_E_CsmRandomSeed_CSM_E_SMALL_BUFFER (3U)

#  define RTE_E_CsmSignatureVerify_CSM_E_BUSY (2U)

#  define RTE_E_CsmSignatureVerify_CSM_E_NOT_OK (1U)

#  define RTE_E_CsmSignatureVerify_CSM_E_SMALL_BUFFER (3U)

#  define RTE_E_CsmSymDecrypt_CSM_E_BUSY (2U)

#  define RTE_E_CsmSymDecrypt_CSM_E_NOT_OK (1U)

#  define RTE_E_CsmSymDecrypt_CSM_E_SMALL_BUFFER (3U)

#  define RTE_E_CsmSymEncrypt_CSM_E_BUSY (2U)

#  define RTE_E_CsmSymEncrypt_CSM_E_NOT_OK (1U)

#  define RTE_E_CsmSymEncrypt_CSM_E_SMALL_BUFFER (3U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_CSM_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_1330:  MISRA rule: 16.4
     Reason:     The RTE Generator uses default names for parameter identifiers of port defined arguments of service modules.
                 Therefore the parameter identifiers in the function declaration differs from those of the implementation of the BSW module.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
