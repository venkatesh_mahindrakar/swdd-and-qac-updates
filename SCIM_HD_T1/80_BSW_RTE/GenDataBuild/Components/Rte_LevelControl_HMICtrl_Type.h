/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_LevelControl_HMICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <LevelControl_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_LEVELCONTROL_HMICTRL_TYPE_H
# define _RTE_LEVELCONTROL_HMICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef A2PosSwitchStatus_Off
#   define A2PosSwitchStatus_Off (0U)
#  endif

#  ifndef A2PosSwitchStatus_On
#   define A2PosSwitchStatus_On (1U)
#  endif

#  ifndef A2PosSwitchStatus_Error
#   define A2PosSwitchStatus_Error (2U)
#  endif

#  ifndef A2PosSwitchStatus_NotAvailable
#   define A2PosSwitchStatus_NotAvailable (3U)
#  endif

#  ifndef A3PosSwitchStatus_Middle
#   define A3PosSwitchStatus_Middle (0U)
#  endif

#  ifndef A3PosSwitchStatus_Lower
#   define A3PosSwitchStatus_Lower (1U)
#  endif

#  ifndef A3PosSwitchStatus_Upper
#   define A3PosSwitchStatus_Upper (2U)
#  endif

#  ifndef A3PosSwitchStatus_Spare
#   define A3PosSwitchStatus_Spare (3U)
#  endif

#  ifndef A3PosSwitchStatus_Spare_01
#   define A3PosSwitchStatus_Spare_01 (4U)
#  endif

#  ifndef A3PosSwitchStatus_Spare_02
#   define A3PosSwitchStatus_Spare_02 (5U)
#  endif

#  ifndef A3PosSwitchStatus_Error
#   define A3PosSwitchStatus_Error (6U)
#  endif

#  ifndef A3PosSwitchStatus_NotAvailable
#   define A3PosSwitchStatus_NotAvailable (7U)
#  endif

#  ifndef Ack2Bit_NoAction
#   define Ack2Bit_NoAction (0U)
#  endif

#  ifndef Ack2Bit_ChangeAcknowledged
#   define Ack2Bit_ChangeAcknowledged (1U)
#  endif

#  ifndef Ack2Bit_Error
#   define Ack2Bit_Error (2U)
#  endif

#  ifndef Ack2Bit_NotAvailable
#   define Ack2Bit_NotAvailable (3U)
#  endif

#  ifndef BackToDriveReqACK_TakNoAction
#   define BackToDriveReqACK_TakNoAction (0U)
#  endif

#  ifndef BackToDriveReqACK_ChangeAcknowledged
#   define BackToDriveReqACK_ChangeAcknowledged (1U)
#  endif

#  ifndef BackToDriveReqACK_Error
#   define BackToDriveReqACK_Error (2U)
#  endif

#  ifndef BackToDriveReqACK_NotAvailable
#   define BackToDriveReqACK_NotAvailable (3U)
#  endif

#  ifndef BackToDriveReq_Idle
#   define BackToDriveReq_Idle (0U)
#  endif

#  ifndef BackToDriveReq_B2DRequested
#   define BackToDriveReq_B2DRequested (1U)
#  endif

#  ifndef Requested_Error
#   define Requested_Error (2U)
#  endif

#  ifndef Requested_NotAvailable
#   define Requested_NotAvailable (3U)
#  endif

#  ifndef ChangeKneelACK_NoAction
#   define ChangeKneelACK_NoAction (0U)
#  endif

#  ifndef ChangeKneelACK_ChangeAcknowledged
#   define ChangeKneelACK_ChangeAcknowledged (1U)
#  endif

#  ifndef ChangeKneelACK_Error
#   define ChangeKneelACK_Error (2U)
#  endif

#  ifndef ChangeKneelACK_NotAvailable
#   define ChangeKneelACK_NotAvailable (3U)
#  endif

#  ifndef ChangeRequest2Bit_TakeNoAction
#   define ChangeRequest2Bit_TakeNoAction (0U)
#  endif

#  ifndef ChangeRequest2Bit_Change
#   define ChangeRequest2Bit_Change (1U)
#  endif

#  ifndef ChangeRequest2Bit_Error
#   define ChangeRequest2Bit_Error (2U)
#  endif

#  ifndef ChangeRequest2Bit_NotAvailable
#   define ChangeRequest2Bit_NotAvailable (3U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED
#   define DEM_EVENT_STATUS_PASSED (0U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED
#   define DEM_EVENT_STATUS_FAILED (1U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED
#   define DEM_EVENT_STATUS_PREPASSED (2U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED
#   define DEM_EVENT_STATUS_PREFAILED (3U)
#  endif

#  ifndef DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED
#   define DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
#  endif

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef ECSStandByReq_Idle
#   define ECSStandByReq_Idle (0U)
#  endif

#  ifndef ECSStandByReq_StandbyRequested
#   define ECSStandByReq_StandbyRequested (1U)
#  endif

#  ifndef ECSStandByReq_StopStandby
#   define ECSStandByReq_StopStandby (2U)
#  endif

#  ifndef ECSStandByReq_Reserved
#   define ECSStandByReq_Reserved (3U)
#  endif

#  ifndef ECSStandByReq_Reserved_01
#   define ECSStandByReq_Reserved_01 (4U)
#  endif

#  ifndef ECSStandByReq_Reserved_02
#   define ECSStandByReq_Reserved_02 (5U)
#  endif

#  ifndef ECSStandByReq_Error
#   define ECSStandByReq_Error (6U)
#  endif

#  ifndef ECSStandByReq_NotAvailable
#   define ECSStandByReq_NotAvailable (7U)
#  endif

#  ifndef ECSStandByRequest_NoRequest
#   define ECSStandByRequest_NoRequest (0U)
#  endif

#  ifndef ECSStandByRequest_Initiate
#   define ECSStandByRequest_Initiate (1U)
#  endif

#  ifndef ECSStandByRequest_StandbyRequestedRCECS
#   define ECSStandByRequest_StandbyRequestedRCECS (2U)
#  endif

#  ifndef ECSStandByRequest_StandbyRequestedWRC
#   define ECSStandByRequest_StandbyRequestedWRC (3U)
#  endif

#  ifndef ECSStandByRequest_Reserved
#   define ECSStandByRequest_Reserved (4U)
#  endif

#  ifndef ECSStandByRequest_Reserved_01
#   define ECSStandByRequest_Reserved_01 (5U)
#  endif

#  ifndef ECSStandByRequest_Error
#   define ECSStandByRequest_Error (6U)
#  endif

#  ifndef ECSStandByRequest_NotAvailable
#   define ECSStandByRequest_NotAvailable (7U)
#  endif

#  ifndef ElectricalLoadReduction_rqst_NoRequest
#   define ElectricalLoadReduction_rqst_NoRequest (0U)
#  endif

#  ifndef ElectricalLoadReduction_rqst_Level1Request
#   define ElectricalLoadReduction_rqst_Level1Request (1U)
#  endif

#  ifndef ElectricalLoadReduction_rqst_Level2Request
#   define ElectricalLoadReduction_rqst_Level2Request (2U)
#  endif

#  ifndef ElectricalLoadReduction_rqst_SpareValue
#   define ElectricalLoadReduction_rqst_SpareValue (3U)
#  endif

#  ifndef ElectricalLoadReduction_rqst_SpareValue_01
#   define ElectricalLoadReduction_rqst_SpareValue_01 (4U)
#  endif

#  ifndef ElectricalLoadReduction_rqst_SpareValue_02
#   define ElectricalLoadReduction_rqst_SpareValue_02 (5U)
#  endif

#  ifndef ElectricalLoadReduction_rqst_Error
#   define ElectricalLoadReduction_rqst_Error (6U)
#  endif

#  ifndef ElectricalLoadReduction_rqst_NotAvailable
#   define ElectricalLoadReduction_rqst_NotAvailable (7U)
#  endif

#  ifndef FPBRChangeReq_TakeNoAction
#   define FPBRChangeReq_TakeNoAction (0U)
#  endif

#  ifndef FPBRChangeReq_ChangeFPBRFunction
#   define FPBRChangeReq_ChangeFPBRFunction (1U)
#  endif

#  ifndef FPBRChangeReq_Error
#   define FPBRChangeReq_Error (2U)
#  endif

#  ifndef FPBRChangeReq_NotAvailable
#   define FPBRChangeReq_NotAvailable (3U)
#  endif

#  ifndef FPBRStatusInd_Off
#   define FPBRStatusInd_Off (0U)
#  endif

#  ifndef FPBRStatusInd_On
#   define FPBRStatusInd_On (1U)
#  endif

#  ifndef FPBRStatusInd_Changing
#   define FPBRStatusInd_Changing (2U)
#  endif

#  ifndef FPBRStatusInd_NotAvailable
#   define FPBRStatusInd_NotAvailable (3U)
#  endif

#  ifndef FalseTrue_False
#   define FalseTrue_False (0U)
#  endif

#  ifndef FalseTrue_True
#   define FalseTrue_True (1U)
#  endif

#  ifndef FalseTrue_Error
#   define FalseTrue_Error (2U)
#  endif

#  ifndef FalseTrue_NotAvaiable
#   define FalseTrue_NotAvaiable (3U)
#  endif

#  ifndef FerryFunctionStatus_FerryFunctionNotActive
#   define FerryFunctionStatus_FerryFunctionNotActive (0U)
#  endif

#  ifndef FerryFunctionStatus_FerryFunctionActiveLevelReached
#   define FerryFunctionStatus_FerryFunctionActiveLevelReached (1U)
#  endif

#  ifndef FerryFunctionStatus_FerryFunctionActiveLeveNotlReached
#   define FerryFunctionStatus_FerryFunctionActiveLeveNotlReached (2U)
#  endif

#  ifndef FerryFunctionStatus_Spare_01
#   define FerryFunctionStatus_Spare_01 (3U)
#  endif

#  ifndef FerryFunctionStatus_Spare_02
#   define FerryFunctionStatus_Spare_02 (4U)
#  endif

#  ifndef FerryFunctionStatus_Spare_03
#   define FerryFunctionStatus_Spare_03 (5U)
#  endif

#  ifndef FerryFunctionStatus_Error
#   define FerryFunctionStatus_Error (6U)
#  endif

#  ifndef FerryFunctionStatus_NotAvailable
#   define FerryFunctionStatus_NotAvailable (7U)
#  endif

#  ifndef InactiveActive_Inactive
#   define InactiveActive_Inactive (0U)
#  endif

#  ifndef InactiveActive_Active
#   define InactiveActive_Active (1U)
#  endif

#  ifndef InactiveActive_Error
#   define InactiveActive_Error (2U)
#  endif

#  ifndef InactiveActive_NotAvailable
#   define InactiveActive_NotAvailable (3U)
#  endif

#  ifndef KneelingChangeRequest_TakeNoAction
#   define KneelingChangeRequest_TakeNoAction (0U)
#  endif

#  ifndef KneelingChangeRequest_ChangeKneelFunction
#   define KneelingChangeRequest_ChangeKneelFunction (1U)
#  endif

#  ifndef KneelingChangeRequest_Error
#   define KneelingChangeRequest_Error (2U)
#  endif

#  ifndef KneelingChangeRequest_NotAvailable
#   define KneelingChangeRequest_NotAvailable (3U)
#  endif

#  ifndef KneelingStatusHMI_Inactive
#   define KneelingStatusHMI_Inactive (0U)
#  endif

#  ifndef KneelingStatusHMI_Active
#   define KneelingStatusHMI_Active (1U)
#  endif

#  ifndef KneelingStatusHMI_Error
#   define KneelingStatusHMI_Error (2U)
#  endif

#  ifndef KneelingStatusHMI_NotAvailable
#   define KneelingStatusHMI_NotAvailable (3U)
#  endif

#  ifndef LevelAdjustmentAction_Idle
#   define LevelAdjustmentAction_Idle (0U)
#  endif

#  ifndef LevelAdjustmentAction_UpBasic
#   define LevelAdjustmentAction_UpBasic (1U)
#  endif

#  ifndef LevelAdjustmentAction_DownBasic
#   define LevelAdjustmentAction_DownBasic (2U)
#  endif

#  ifndef LevelAdjustmentAction_UpShortMovement
#   define LevelAdjustmentAction_UpShortMovement (3U)
#  endif

#  ifndef LevelAdjustmentAction_DownShortMovement
#   define LevelAdjustmentAction_DownShortMovement (4U)
#  endif

#  ifndef LevelAdjustmentAction_Reserved
#   define LevelAdjustmentAction_Reserved (5U)
#  endif

#  ifndef LevelAdjustmentAction_GotoDriveLevel
#   define LevelAdjustmentAction_GotoDriveLevel (6U)
#  endif

#  ifndef LevelAdjustmentAction_Reserved_01
#   define LevelAdjustmentAction_Reserved_01 (7U)
#  endif

#  ifndef LevelAdjustmentAction_Ferry
#   define LevelAdjustmentAction_Ferry (8U)
#  endif

#  ifndef LevelAdjustmentAction_Reserved_02
#   define LevelAdjustmentAction_Reserved_02 (9U)
#  endif

#  ifndef LevelAdjustmentAction_Reserved_03
#   define LevelAdjustmentAction_Reserved_03 (10U)
#  endif

#  ifndef LevelAdjustmentAction_Reserved_04
#   define LevelAdjustmentAction_Reserved_04 (11U)
#  endif

#  ifndef LevelAdjustmentAction_Reserved_05
#   define LevelAdjustmentAction_Reserved_05 (12U)
#  endif

#  ifndef LevelAdjustmentAction_Reserved_06
#   define LevelAdjustmentAction_Reserved_06 (13U)
#  endif

#  ifndef LevelAdjustmentAction_Error
#   define LevelAdjustmentAction_Error (14U)
#  endif

#  ifndef LevelAdjustmentAction_NotAvailable
#   define LevelAdjustmentAction_NotAvailable (15U)
#  endif

#  ifndef LevelAdjustmentAxles_Rear
#   define LevelAdjustmentAxles_Rear (0U)
#  endif

#  ifndef LevelAdjustmentAxles_Front
#   define LevelAdjustmentAxles_Front (1U)
#  endif

#  ifndef LevelAdjustmentAxles_Parallel
#   define LevelAdjustmentAxles_Parallel (2U)
#  endif

#  ifndef LevelAdjustmentAxles_Reserved
#   define LevelAdjustmentAxles_Reserved (3U)
#  endif

#  ifndef LevelAdjustmentAxles_Reserved_01
#   define LevelAdjustmentAxles_Reserved_01 (4U)
#  endif

#  ifndef LevelAdjustmentAxles_Reserved_02
#   define LevelAdjustmentAxles_Reserved_02 (5U)
#  endif

#  ifndef LevelAdjustmentAxles_Error
#   define LevelAdjustmentAxles_Error (6U)
#  endif

#  ifndef LevelAdjustmentAxles_NotAvailable
#   define LevelAdjustmentAxles_NotAvailable (7U)
#  endif

#  ifndef LevelAdjustmentStroke_DriveStroke
#   define LevelAdjustmentStroke_DriveStroke (0U)
#  endif

#  ifndef LevelAdjustmentStroke_DockingStroke
#   define LevelAdjustmentStroke_DockingStroke (1U)
#  endif

#  ifndef LevelAdjustmentStroke_Reserved
#   define LevelAdjustmentStroke_Reserved (2U)
#  endif

#  ifndef LevelAdjustmentStroke_Reserved_01
#   define LevelAdjustmentStroke_Reserved_01 (3U)
#  endif

#  ifndef LevelAdjustmentStroke_Reserved_02
#   define LevelAdjustmentStroke_Reserved_02 (4U)
#  endif

#  ifndef LevelAdjustmentStroke_Reserved_03
#   define LevelAdjustmentStroke_Reserved_03 (5U)
#  endif

#  ifndef LevelAdjustmentStroke_Error
#   define LevelAdjustmentStroke_Error (6U)
#  endif

#  ifndef LevelAdjustmentStroke_NotAvailable
#   define LevelAdjustmentStroke_NotAvailable (7U)
#  endif

#  ifndef LevelChangeRequest_TakeNoAction
#   define LevelChangeRequest_TakeNoAction (0U)
#  endif

#  ifndef LevelChangeRequest_VehicleBodyUpLifting
#   define LevelChangeRequest_VehicleBodyUpLifting (1U)
#  endif

#  ifndef LevelChangeRequest_VehicleBodyDownLowering
#   define LevelChangeRequest_VehicleBodyDownLowering (2U)
#  endif

#  ifndef LevelChangeRequest_VehicleBodyUpMinimumMovementLifting
#   define LevelChangeRequest_VehicleBodyUpMinimumMovementLifting (3U)
#  endif

#  ifndef LevelChangeRequest_VehicleBodyDownMinimumMovementLowering
#   define LevelChangeRequest_VehicleBodyDownMinimumMovementLowering (4U)
#  endif

#  ifndef LevelChangeRequest_NotDefined
#   define LevelChangeRequest_NotDefined (5U)
#  endif

#  ifndef LevelChangeRequest_ErrorIndicator
#   define LevelChangeRequest_ErrorIndicator (6U)
#  endif

#  ifndef LevelChangeRequest_NotAvailable
#   define LevelChangeRequest_NotAvailable (7U)
#  endif

#  ifndef LevelStrokeRequest_DockingLevelControlStroke
#   define LevelStrokeRequest_DockingLevelControlStroke (0U)
#  endif

#  ifndef LevelStrokeRequest_DriveLevelControlStroke
#   define LevelStrokeRequest_DriveLevelControlStroke (1U)
#  endif

#  ifndef LevelStrokeRequest_ErrorIndicator
#   define LevelStrokeRequest_ErrorIndicator (2U)
#  endif

#  ifndef LevelStrokeRequest_NotAvailable
#   define LevelStrokeRequest_NotAvailable (3U)
#  endif

#  ifndef LevelUserMemoryAction_Inactive
#   define LevelUserMemoryAction_Inactive (0U)
#  endif

#  ifndef LevelUserMemoryAction_Recall
#   define LevelUserMemoryAction_Recall (1U)
#  endif

#  ifndef LevelUserMemoryAction_Store
#   define LevelUserMemoryAction_Store (2U)
#  endif

#  ifndef LevelUserMemoryAction_Default
#   define LevelUserMemoryAction_Default (3U)
#  endif

#  ifndef LevelUserMemoryAction_Reserved
#   define LevelUserMemoryAction_Reserved (4U)
#  endif

#  ifndef LevelUserMemoryAction_Reserved_01
#   define LevelUserMemoryAction_Reserved_01 (5U)
#  endif

#  ifndef LevelUserMemoryAction_Error
#   define LevelUserMemoryAction_Error (6U)
#  endif

#  ifndef LevelUserMemoryAction_NotAvailable
#   define LevelUserMemoryAction_NotAvailable (7U)
#  endif

#  ifndef LevelUserMemory_NotUsed
#   define LevelUserMemory_NotUsed (0U)
#  endif

#  ifndef LevelUserMemory_M1
#   define LevelUserMemory_M1 (1U)
#  endif

#  ifndef LevelUserMemory_M2
#   define LevelUserMemory_M2 (2U)
#  endif

#  ifndef LevelUserMemory_M3
#   define LevelUserMemory_M3 (3U)
#  endif

#  ifndef LevelUserMemory_M4
#   define LevelUserMemory_M4 (4U)
#  endif

#  ifndef LevelUserMemory_M5
#   define LevelUserMemory_M5 (5U)
#  endif

#  ifndef LevelUserMemory_M6
#   define LevelUserMemory_M6 (6U)
#  endif

#  ifndef LevelUserMemory_M7
#   define LevelUserMemory_M7 (7U)
#  endif

#  ifndef LevelUserMemory_Spare
#   define LevelUserMemory_Spare (8U)
#  endif

#  ifndef LevelUserMemory_Spare01
#   define LevelUserMemory_Spare01 (9U)
#  endif

#  ifndef LevelUserMemory_Spare02
#   define LevelUserMemory_Spare02 (10U)
#  endif

#  ifndef LevelUserMemory_Spare03
#   define LevelUserMemory_Spare03 (11U)
#  endif

#  ifndef LevelUserMemory_Spare04
#   define LevelUserMemory_Spare04 (12U)
#  endif

#  ifndef LevelUserMemory_Spare05
#   define LevelUserMemory_Spare05 (13U)
#  endif

#  ifndef LevelUserMemory_Error
#   define LevelUserMemory_Error (14U)
#  endif

#  ifndef LevelUserMemory_NotAvailable
#   define LevelUserMemory_NotAvailable (15U)
#  endif

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

#  ifndef RampLevelRequest_TakeNoAction
#   define RampLevelRequest_TakeNoAction (0U)
#  endif

#  ifndef RampLevelRequest_RampLevelM1
#   define RampLevelRequest_RampLevelM1 (1U)
#  endif

#  ifndef RampLevelRequest_RampLevelM2
#   define RampLevelRequest_RampLevelM2 (2U)
#  endif

#  ifndef RampLevelRequest_RampLevelM3
#   define RampLevelRequest_RampLevelM3 (3U)
#  endif

#  ifndef RampLevelRequest_RampLevelM4
#   define RampLevelRequest_RampLevelM4 (4U)
#  endif

#  ifndef RampLevelRequest_RampLevelM5
#   define RampLevelRequest_RampLevelM5 (5U)
#  endif

#  ifndef RampLevelRequest_RampLevelM6
#   define RampLevelRequest_RampLevelM6 (6U)
#  endif

#  ifndef RampLevelRequest_RampLevelM7
#   define RampLevelRequest_RampLevelM7 (7U)
#  endif

#  ifndef RampLevelRequest_NotDefined_04
#   define RampLevelRequest_NotDefined_04 (8U)
#  endif

#  ifndef RampLevelRequest_NotDefined_05
#   define RampLevelRequest_NotDefined_05 (9U)
#  endif

#  ifndef RampLevelRequest_NotDefined_06
#   define RampLevelRequest_NotDefined_06 (10U)
#  endif

#  ifndef RampLevelRequest_NotDefined_07
#   define RampLevelRequest_NotDefined_07 (11U)
#  endif

#  ifndef RampLevelRequest_NotDefined_08
#   define RampLevelRequest_NotDefined_08 (12U)
#  endif

#  ifndef RampLevelRequest_NotDefined_09
#   define RampLevelRequest_NotDefined_09 (13U)
#  endif

#  ifndef RampLevelRequest_ErrorIndicator
#   define RampLevelRequest_ErrorIndicator (14U)
#  endif

#  ifndef RampLevelRequest_NotAvailable
#   define RampLevelRequest_NotAvailable (15U)
#  endif

#  ifndef Request_NotRequested
#   define Request_NotRequested (0U)
#  endif

#  ifndef Request_RequestActive
#   define Request_RequestActive (1U)
#  endif

#  ifndef Request_Error
#   define Request_Error (2U)
#  endif

#  ifndef Request_NotAvailable
#   define Request_NotAvailable (3U)
#  endif

#  ifndef RideHeightFunction_Inactive
#   define RideHeightFunction_Inactive (0U)
#  endif

#  ifndef RideHeightFunction_StandardDrivePosition
#   define RideHeightFunction_StandardDrivePosition (1U)
#  endif

#  ifndef RideHeightFunction_AlternativeDrivePosition
#   define RideHeightFunction_AlternativeDrivePosition (2U)
#  endif

#  ifndef RideHeightFunction_SpeedDependentDrivePosition
#   define RideHeightFunction_SpeedDependentDrivePosition (3U)
#  endif

#  ifndef RideHeightFunction_HighDrivePosition
#   define RideHeightFunction_HighDrivePosition (4U)
#  endif

#  ifndef RideHeightFunction_LowDrivePosition
#   define RideHeightFunction_LowDrivePosition (5U)
#  endif

#  ifndef RideHeightFunction_AlternativeDrivePosition1
#   define RideHeightFunction_AlternativeDrivePosition1 (6U)
#  endif

#  ifndef RideHeightFunction_AlternativeDrivePosition2
#   define RideHeightFunction_AlternativeDrivePosition2 (7U)
#  endif

#  ifndef RideHeightFunction_NotDefined
#   define RideHeightFunction_NotDefined (8U)
#  endif

#  ifndef RideHeightFunction_NotDefined_01
#   define RideHeightFunction_NotDefined_01 (9U)
#  endif

#  ifndef RideHeightFunction_NotDefined_02
#   define RideHeightFunction_NotDefined_02 (10U)
#  endif

#  ifndef RideHeightFunction_NotDefined_03
#   define RideHeightFunction_NotDefined_03 (11U)
#  endif

#  ifndef RideHeightFunction_NotDefined_04
#   define RideHeightFunction_NotDefined_04 (12U)
#  endif

#  ifndef RideHeightFunction_NotDefined_05
#   define RideHeightFunction_NotDefined_05 (13U)
#  endif

#  ifndef RideHeightFunction_ErrorIndicator
#   define RideHeightFunction_ErrorIndicator (14U)
#  endif

#  ifndef RideHeightFunction_NotAvailable
#   define RideHeightFunction_NotAvailable (15U)
#  endif

#  ifndef RideHeightStorageRequest_TakeNoAction
#   define RideHeightStorageRequest_TakeNoAction (0U)
#  endif

#  ifndef RideHeightStorageRequest_ResetToStoreFactoryDrivePosition
#   define RideHeightStorageRequest_ResetToStoreFactoryDrivePosition (1U)
#  endif

#  ifndef RideHeightStorageRequest_StoreStandardUserDrivePosition
#   define RideHeightStorageRequest_StoreStandardUserDrivePosition (2U)
#  endif

#  ifndef RideHeightStorageRequest_StoreLowDrivePosition
#   define RideHeightStorageRequest_StoreLowDrivePosition (3U)
#  endif

#  ifndef RideHeightStorageRequest_StoreHighDrivePosition
#   define RideHeightStorageRequest_StoreHighDrivePosition (4U)
#  endif

#  ifndef RideHeightStorageRequest_ResetLowDrivePosition
#   define RideHeightStorageRequest_ResetLowDrivePosition (5U)
#  endif

#  ifndef RideHeightStorageRequest_ResetHighDrivePosition
#   define RideHeightStorageRequest_ResetHighDrivePosition (6U)
#  endif

#  ifndef RideHeightStorageRequest_NotDefined
#   define RideHeightStorageRequest_NotDefined (7U)
#  endif

#  ifndef RideHeightStorageRequest_NotDefined_01
#   define RideHeightStorageRequest_NotDefined_01 (8U)
#  endif

#  ifndef RideHeightStorageRequest_NotDefined_02
#   define RideHeightStorageRequest_NotDefined_02 (9U)
#  endif

#  ifndef RideHeightStorageRequest_NotDefined_03
#   define RideHeightStorageRequest_NotDefined_03 (10U)
#  endif

#  ifndef RideHeightStorageRequest_NotDefined_04
#   define RideHeightStorageRequest_NotDefined_04 (11U)
#  endif

#  ifndef RideHeightStorageRequest_NotDefined_05
#   define RideHeightStorageRequest_NotDefined_05 (12U)
#  endif

#  ifndef RideHeightStorageRequest_NotDefined_06
#   define RideHeightStorageRequest_NotDefined_06 (13U)
#  endif

#  ifndef RideHeightStorageRequest_ErrorIndicator
#   define RideHeightStorageRequest_ErrorIndicator (14U)
#  endif

#  ifndef RideHeightStorageRequest_NotAvailable
#   define RideHeightStorageRequest_NotAvailable (15U)
#  endif

#  ifndef RollRequest_Idle
#   define RollRequest_Idle (0U)
#  endif

#  ifndef RollRequest_Clockwise
#   define RollRequest_Clockwise (1U)
#  endif

#  ifndef RollRequest_CounterClockwise
#   define RollRequest_CounterClockwise (2U)
#  endif

#  ifndef RollRequest_ClockwiseShort
#   define RollRequest_ClockwiseShort (3U)
#  endif

#  ifndef RollRequest_CounterClockwiseShort
#   define RollRequest_CounterClockwiseShort (4U)
#  endif

#  ifndef RollRequest_Reserved
#   define RollRequest_Reserved (5U)
#  endif

#  ifndef RollRequest_Error
#   define RollRequest_Error (6U)
#  endif

#  ifndef RollRequest_NotAvailable
#   define RollRequest_NotAvailable (7U)
#  endif

#  ifndef StopLevelChangeStatus_NoStopRequest
#   define StopLevelChangeStatus_NoStopRequest (0U)
#  endif

#  ifndef StopLevelChangeStatus_LevelChangedStopped
#   define StopLevelChangeStatus_LevelChangedStopped (1U)
#  endif

#  ifndef StopLevelChangeStatus_ErrorIndicator
#   define StopLevelChangeStatus_ErrorIndicator (2U)
#  endif

#  ifndef StopLevelChangeStatus_NotAvailable
#   define StopLevelChangeStatus_NotAvailable (3U)
#  endif

#  ifndef StorageAck_TakeNoAction
#   define StorageAck_TakeNoAction (0U)
#  endif

#  ifndef StorageAck_ChangeAcknowledged
#   define StorageAck_ChangeAcknowledged (1U)
#  endif

#  ifndef StorageAck_Error
#   define StorageAck_Error (2U)
#  endif

#  ifndef StorageAck_NotAvaiable
#   define StorageAck_NotAvaiable (3U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

#  ifndef VehicleMode_Hibernate
#   define VehicleMode_Hibernate (0U)
#  endif

#  ifndef VehicleMode_Parked
#   define VehicleMode_Parked (1U)
#  endif

#  ifndef VehicleMode_Living
#   define VehicleMode_Living (2U)
#  endif

#  ifndef VehicleMode_Accessory
#   define VehicleMode_Accessory (3U)
#  endif

#  ifndef VehicleMode_PreRunning
#   define VehicleMode_PreRunning (4U)
#  endif

#  ifndef VehicleMode_Cranking
#   define VehicleMode_Cranking (5U)
#  endif

#  ifndef VehicleMode_Running
#   define VehicleMode_Running (6U)
#  endif

#  ifndef VehicleMode_Spare_1
#   define VehicleMode_Spare_1 (7U)
#  endif

#  ifndef VehicleMode_Spare_2
#   define VehicleMode_Spare_2 (8U)
#  endif

#  ifndef VehicleMode_Spare_3
#   define VehicleMode_Spare_3 (9U)
#  endif

#  ifndef VehicleMode_Spare_4
#   define VehicleMode_Spare_4 (10U)
#  endif

#  ifndef VehicleMode_Spare_5
#   define VehicleMode_Spare_5 (11U)
#  endif

#  ifndef VehicleMode_Spare_6
#   define VehicleMode_Spare_6 (12U)
#  endif

#  ifndef VehicleMode_Spare_7
#   define VehicleMode_Spare_7 (13U)
#  endif

#  ifndef VehicleMode_Error
#   define VehicleMode_Error (14U)
#  endif

#  ifndef VehicleMode_NotAvailable
#   define VehicleMode_NotAvailable (15U)
#  endif

#  ifndef WiredLevelUserMemory_MemOff
#   define WiredLevelUserMemory_MemOff (0U)
#  endif

#  ifndef WiredLevelUserMemory_M1
#   define WiredLevelUserMemory_M1 (1U)
#  endif

#  ifndef WiredLevelUserMemory_M2
#   define WiredLevelUserMemory_M2 (2U)
#  endif

#  ifndef WiredLevelUserMemory_M3
#   define WiredLevelUserMemory_M3 (3U)
#  endif

#  ifndef WiredLevelUserMemory_M4
#   define WiredLevelUserMemory_M4 (4U)
#  endif

#  ifndef WiredLevelUserMemory_M5
#   define WiredLevelUserMemory_M5 (5U)
#  endif

#  ifndef WiredLevelUserMemory_M6
#   define WiredLevelUserMemory_M6 (6U)
#  endif

#  ifndef WiredLevelUserMemory_M7
#   define WiredLevelUserMemory_M7 (7U)
#  endif

#  ifndef WiredLevelUserMemory_Spare
#   define WiredLevelUserMemory_Spare (8U)
#  endif

#  ifndef WiredLevelUserMemory_Spare01
#   define WiredLevelUserMemory_Spare01 (9U)
#  endif

#  ifndef WiredLevelUserMemory_Spare02
#   define WiredLevelUserMemory_Spare02 (10U)
#  endif

#  ifndef WiredLevelUserMemory_Spare03
#   define WiredLevelUserMemory_Spare03 (11U)
#  endif

#  ifndef WiredLevelUserMemory_Spare04
#   define WiredLevelUserMemory_Spare04 (12U)
#  endif

#  ifndef WiredLevelUserMemory_Spare05
#   define WiredLevelUserMemory_Spare05 (13U)
#  endif

#  ifndef WiredLevelUserMemory_Error
#   define WiredLevelUserMemory_Error (14U)
#  endif

#  ifndef WiredLevelUserMemory_NotAvailable
#   define WiredLevelUserMemory_NotAvailable (15U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_LEVELCONTROL_HMICTRL_TYPE_H */
