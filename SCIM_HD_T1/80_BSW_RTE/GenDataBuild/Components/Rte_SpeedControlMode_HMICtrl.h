/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_SpeedControlMode_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <SpeedControlMode_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_SPEEDCONTROLMODE_HMICTRL_H
# define _RTE_SPEEDCONTROLMODE_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_SpeedControlMode_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_SpeedControlMode_HMICtrl_ACCOrCCIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_SpeedControlMode_HMICtrl_ASLIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_SpeedControlFreeWheel_LINMasCtrl_SpeedControlModeButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FreeWheel_Status_T, RTE_VAR_NOINIT) Rte_SpeedControlFreeWheel_LINMasCtrl_SpeedControlModeWheelStatus_FreeWheel_Status; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ACCOrCCIndication_DeviceIndication (3U)
#  define Rte_InitValue_ASLIndication_DeviceIndication (3U)
#  define Rte_InitValue_CCStates_CCStates (15U)
#  define Rte_InitValue_DriverMemory_rqst_DriverMemory_rqst (31U)
#  define Rte_InitValue_FWSelectedACCMode_CCIM_ACC (7U)
#  define Rte_InitValue_FWSelectedSpeedControlMode_FWSelectedSpeedControlMode (7U)
#  define Rte_InitValue_FWSpeedControlEndStopEvent_FWSpeedControlEndStopEvent (3U)
#  define Rte_InitValue_SpeedControlModeButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_SpeedControlModeWheelStatus_FreeWheel_Status (15U)
#  define Rte_InitValue_SwcActivation_IgnitionOn_IgnitionOn (1U)
#  define Rte_InitValue_XRSLStates_XRSLStates (15U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_SpeedControlMode_HMICtrl_CCStates_CCStates(P2VAR(CCStates_T, AUTOMATIC, RTE_SPEEDCONTROLMODE_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_SpeedControlMode_HMICtrl_DriverMemory_rqst_DriverMemory_rqst(P2VAR(DriverMemory_rqst_T, AUTOMATIC, RTE_SPEEDCONTROLMODE_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_SpeedControlMode_HMICtrl_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I(P2VAR(uint8, AUTOMATIC, RTE_SPEEDCONTROLMODE_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_SpeedControlMode_HMICtrl_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I(P2VAR(SpeedControl_NVM_T, AUTOMATIC, RTE_SPEEDCONTROLMODE_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_SpeedControlMode_HMICtrl_XRSLStates_XRSLStates(P2VAR(XRSLStates_T, AUTOMATIC, RTE_SPEEDCONTROLMODE_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SpeedControlMode_HMICtrl_FWSelectedACCMode_CCIM_ACC(CCIM_ACC_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SpeedControlMode_HMICtrl_FWSelectedSpeedControlMode_FWSelectedSpeedControlMode(FWSelectedSpeedControlMode_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SpeedControlMode_HMICtrl_FWSpeedControlEndStopEvent_FWSpeedControlEndStopEvent(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SpeedControlMode_HMICtrl_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I(P2CONST(uint8, AUTOMATIC, RTE_SPEEDCONTROLMODE_HMICTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_SpeedControlMode_HMICtrl_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I(P2CONST(SpeedControl_NVM_T, AUTOMATIC, RTE_SPEEDCONTROLMODE_HMICTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_CCStates_CCStates Rte_Read_SpeedControlMode_HMICtrl_CCStates_CCStates
#  define Rte_Read_DriverMemory_rqst_DriverMemory_rqst Rte_Read_SpeedControlMode_HMICtrl_DriverMemory_rqst_DriverMemory_rqst
#  define Rte_Read_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I Rte_Read_SpeedControlMode_HMICtrl_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I
#  define Rte_Read_SpeedControlModeButtonStatus_PushButtonStatus Rte_Read_SpeedControlMode_HMICtrl_SpeedControlModeButtonStatus_PushButtonStatus
#  define Rte_Read_SpeedControlMode_HMICtrl_SpeedControlModeButtonStatus_PushButtonStatus(data) (*(data) = Rte_SpeedControlFreeWheel_LINMasCtrl_SpeedControlModeButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SpeedControlModeWheelStatus_FreeWheel_Status Rte_Read_SpeedControlMode_HMICtrl_SpeedControlModeWheelStatus_FreeWheel_Status
#  define Rte_Read_SpeedControlMode_HMICtrl_SpeedControlModeWheelStatus_FreeWheel_Status(data) (*(data) = Rte_SpeedControlFreeWheel_LINMasCtrl_SpeedControlModeWheelStatus_FreeWheel_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_IgnitionOn_IgnitionOn Rte_Read_SpeedControlMode_HMICtrl_SwcActivation_IgnitionOn_IgnitionOn
#  define Rte_Read_SpeedControlMode_HMICtrl_SwcActivation_IgnitionOn_IgnitionOn(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_XRSLStates_XRSLStates Rte_Read_SpeedControlMode_HMICtrl_XRSLStates_XRSLStates


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_ACCOrCCIndication_DeviceIndication Rte_Write_SpeedControlMode_HMICtrl_ACCOrCCIndication_DeviceIndication
#  define Rte_Write_SpeedControlMode_HMICtrl_ACCOrCCIndication_DeviceIndication(data) (Rte_SpeedControlMode_HMICtrl_ACCOrCCIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ASLIndication_DeviceIndication Rte_Write_SpeedControlMode_HMICtrl_ASLIndication_DeviceIndication
#  define Rte_Write_SpeedControlMode_HMICtrl_ASLIndication_DeviceIndication(data) (Rte_SpeedControlMode_HMICtrl_ASLIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_FWSelectedACCMode_CCIM_ACC Rte_Write_SpeedControlMode_HMICtrl_FWSelectedACCMode_CCIM_ACC
#  define Rte_Write_FWSelectedSpeedControlMode_FWSelectedSpeedControlMode Rte_Write_SpeedControlMode_HMICtrl_FWSelectedSpeedControlMode_FWSelectedSpeedControlMode
#  define Rte_Write_FWSpeedControlEndStopEvent_FWSpeedControlEndStopEvent Rte_Write_SpeedControlMode_HMICtrl_FWSpeedControlEndStopEvent_FWSpeedControlEndStopEvent
#  define Rte_Write_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I Rte_Write_SpeedControlMode_HMICtrl_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_HeadwaySupport_P1BEX_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BEX_HeadwaySupport_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2C_CCFW_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EXP_PersonalSettingsForCC_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1BEX_HeadwaySupport_v() (Rte_AddrPar_0x2B_P1BEX_HeadwaySupport_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1B2C_CCFW_Installed_v() (Rte_AddrPar_0x2B_P1B2C_CCFW_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EXP_PersonalSettingsForCC_v() (Rte_AddrPar_0x2B_P1EXP_PersonalSettingsForCC_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define SpeedControlMode_HMICtrl_START_SEC_CODE
# include "SpeedControlMode_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_SpeedControlMode_HMICtrl_20ms_runnable SpeedControlMode_HMICtrl_20ms_runnable
#  define RTE_RUNNABLE_SpeedControlMode_HMICtrl_Init SpeedControlMode_HMICtrl_Init
# endif

FUNC(void, SpeedControlMode_HMICtrl_CODE) SpeedControlMode_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, SpeedControlMode_HMICtrl_CODE) SpeedControlMode_HMICtrl_Init(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define SpeedControlMode_HMICtrl_STOP_SEC_CODE
# include "SpeedControlMode_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_SPEEDCONTROLMODE_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
