/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_DiagnosticMonitor_IO.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <DiagnosticMonitor_IO>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_DIAGNOSTICMONITOR_IO_H
# define _RTE_DIAGNOSTICMONITOR_IO_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_DiagnosticMonitor_IO_Type.h"
# include "Rte_DataHandleType.h"


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) AdiPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_AdiInterface_P_GetAdiPinState_CS AdiInterface_P_GetAdiPinState_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) AdiInterface_P_GetPullUpState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) isPullUpActive_Strong, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) isPullUpActive_Weak, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) isPullUpActive_DAI); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_AdiInterface_P_GetPullUpState_CS AdiInterface_P_GetPullUpState_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) Do12VInterface_P_GetDcdc12VState_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DcDc12vRefVoltage, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDcDc12vActivated, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Do12VInterface_P_GetDcdc12VState_CS Do12VInterface_P_GetDcdc12VState_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDo12VActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Do12VPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS Do12VInterface_P_GetDo12VPinsState_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS Do12VInterface_P_SetDcdc12VActive_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS Do12VInterface_P_SetDo12VLivingActive_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS Do12VInterface_P_SetDo12VParkedActive_CS
#  define RTE_START_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_DOBHS_APPL_CODE) DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DobhsCtrlInterface_P_1_SetDobhsActive_CS DobhsCtrlInterface_P_1_SetDobhsActive_CS
#  define RTE_START_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_DOBHS_APPL_CODE) DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DobhsCtrlInterface_P_2_SetDobhsActive_CS DobhsCtrlInterface_P_2_SetDobhsActive_CS
#  define RTE_START_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_DOBHS_APPL_CODE) DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DobhsCtrlInterface_P_3_SetDobhsActive_CS DobhsCtrlInterface_P_3_SetDobhsActive_CS
#  define RTE_START_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_DOBHS_APPL_CODE) DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DobhsCtrlInterface_P_4_SetDobhsActive_CS DobhsCtrlInterface_P_4_SetDobhsActive_CS
#  define RTE_START_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_DOBHS_APPL_CODE) DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) isDioActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_DOBHS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS DobhsDiagInterface_P_GetDobhsPinState_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) DoblsCtrlInterface_P_GetDoblsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS DoblsCtrlInterface_P_GetDoblsPinState_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DoblsCtrlInterface_P_SetDoblsActive_CS DoblsCtrlInterface_P_SetDoblsActive_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPwmDutycycle, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DutyCycle, P2VAR(VGTT_EcuPwmPeriod, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Period, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DiagStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS DowhsInterface_P_GetDoPinStateOne_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DowhsInterface_P_SetDowActive_CS DowhsInterface_P_SetDowActive_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPwmDutycycle, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DutyCycle, P2VAR(VGTT_EcuPwmPeriod, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Period, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DiagStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS DowlsInterface_P_GetDoPinStateOne_CS
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DowlsInterface_P_SetDowActive_CS DowlsInterface_P_SetDowActive_CS
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1AD0_1C_DcDc12v_VOR_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)416, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_29_ECU_SignalInvalid_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)289, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_2F_ECU_SignalErratic_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)286, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD9_16_PWR24V_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)299, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD9_17_PWR24V_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)300, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD9_1C_PWR24V_VOR_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)298, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E0K_11_DOWHS1_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)304, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E0K_12_DOWHS1_STB_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)305, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E0K_13_DOWHS1_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)306, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E0K_16_DOWHS1_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)307, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E0K_17_DOWHS1_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)308, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E0K_38_DOWHS1_PWM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)418, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E10_11_ADI4_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)366, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E10_13_ADI4_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)367, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E11_11_ADI5_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)368, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E11_13_ADI5_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)369, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E12_11_ADI6_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)370, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E12_13_ADI6_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)371, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E13_11_ADI7_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)372, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E13_13_ADI7_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)373, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E14_11_ADI8_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)374, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E14_13_ADI8_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)375, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E15_11_ADI9_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)376, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E15_13_ADI9_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)377, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E16_11_ADI10_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)378, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E16_13_ADI10_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)379, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E17_11_ADI11_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)380, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E17_13_ADI11_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)381, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E18_11_ADI12_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)382, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E18_13_ADI12_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)383, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E19_11_ADI13_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)384, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E19_13_ADI13_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)385, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1Q_11_AO12L_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)309, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1Q_12_AO12L_STB_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)310, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1Q_16_AO12L_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)311, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1Q_17_AO12L_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)312, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1Q_19_AO12L_CAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)419, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1R_11_AO12P_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)313, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1R_12_AO12P_STB_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)314, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1R_16_AO12P_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)315, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1R_17_AO12P_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)316, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1R_19_AO12P_CAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)420, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1S_11_DOWHS2_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)317, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1S_12_DOWHS2_STB_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)318, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1S_13_DOWHS2_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)319, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1S_16_DOWHS2_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)320, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1S_17_DOWHS2_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)321, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1S_38_DOWHS2_PWM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)421, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1T_11_DOBHS1_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)322, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1T_12_DOBHS1_STB_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)323, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1T_13_DOBHS1_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)324, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1T_16_DOBHS1_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)325, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1T_17_DOBHS1_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)326, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1U_11_DOBHS2_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)327, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1U_12_DOBHS2_STB_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)328, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1U_13_DOBHS2_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)329, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1U_16_DOBHS2_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)330, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1U_17_DOBHS2_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)331, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1V_11_DOBHS3_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)332, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1V_12_DOBHS3_STB_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)333, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1V_13_DOBHS3_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)334, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1V_16_DOBHS3_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)335, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1V_17_DOBHS3_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)336, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1W_11_DOBHS4_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)337, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1W_12_DOBHS4_STB_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)338, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1W_13_DOBHS4_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)339, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1W_16_DOBHS4_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)340, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1W_17_DOBHS4_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)341, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1X_11_ADI1_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)342, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1X_13_ADI1_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)343, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1Y_11_ADI2_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)344, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1Y_13_ADI2_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)345, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1Z_11_ADI3_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)346, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E1Z_13_ADI3_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)347, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2A_11_ADI14_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)348, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2A_13_ADI14_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)349, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2B_11_ADI15_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)350, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2B_13_ADI15_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)351, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2C_11_ADI16_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)352, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2C_13_ADI16_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)353, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2G_12_DOBLS1_STB_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)354, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2G_14_DOBLS1_STGOC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)424, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2G_16_DOBLS1_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)355, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2G_17_DOBLS1_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)356, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2H_12_DOWLS2_STB_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)357, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2H_14_DOWLS2_STGOC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)425, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2H_16_DOWLS2_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)358, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2H_17_DOWLS2_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)359, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2H_38_DOWLS2_PWM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)422, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2I_12_DOWLS3_STB_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)360, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2I_14_DOWLS3_STGOC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)426, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2I_16_DOWLS3_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)361, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2I_17_DOWLS3_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)362, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2I_38_DOWLS3_PWM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)423, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2L_29_DAI1_SI_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)363, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E2M_29_DAI2_SI_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)364, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1F1A_11_ADI17_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)386, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1F1A_13_ADI17_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)387, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1F1B_11_ADI18_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)388, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1F1B_13_ADI18_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)389, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1F1C_11_ADI19_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)390, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1F1C_13_ADI19_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)391, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FM3_11_LFPi_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)393, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FM3_12_LFPi_STB_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)394, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FM3_13_LFPi_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)395, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FM3_98_LFPi_OverTemperature_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)396, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FM4_11_LFP4_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)397, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FM4_12_LFP4_STB_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)398, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FM4_13_LFP4_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)399, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FM5_11_LFP1_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)400, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FM5_12_LFP1_STB_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)401, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FM5_13_LFP1_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)402, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FM6_11_LFP2_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)403, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FM6_12_LFP2_STB_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)404, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FM6_13_LFP2_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)405, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FM7_11_LFP3_STG_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)406, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FM7_12_LFP3_STB_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)407, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FM7_13_LFP3_OC_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)408, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_IOHWAB_LFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_IOHWAB_LFIC_APPL_CODE) LFAntennaDiagnostic_P_GetDiagnosticResult(P2VAR(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) ValidFlag, P2VAR(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) STGStatus, P2VAR(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) STBStatus, P2VAR(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) OCStatus, P2VAR(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) OTStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  else
FUNC(void, RTE_IOHWAB_LFIC_APPL_CODE) LFAntennaDiagnostic_P_GetDiagnosticResult(P2VAR(uint8, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) ValidFlag, P2VAR(DataArrayType_uint8_5, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) STGStatus, P2VAR(DataArrayType_uint8_5, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) STBStatus, P2VAR(DataArrayType_uint8_5, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) OCStatus, P2VAR(DataArrayType_uint8_5, AUTOMATIC, RTE_IOHWAB_LFIC_APPL_VAR) OTStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  endif
#  define RTE_STOP_SEC_IOHWAB_LFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_LFAntennaDiagnostic_P_GetDiagnosticResult(arg1, arg2, arg3, arg4, arg5) (LFAntennaDiagnostic_P_GetDiagnosticResult(arg1, arg2, arg3, arg4, arg5), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_IOHWAB_ASIL_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_CORE_APPL_CODE) VbatInterface_P_GetVbatVoltage_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_VbatInterface_I_GetVbatVoltage_CS VbatInterface_P_GetVbatVoltage_CS


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_HwToleranceThreshold_X1C04_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1C04_HwToleranceThreshold_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1C05_DtcActivationVbatEnable_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_X1C04_HwToleranceThreshold_v() (Rte_AddrPar_0x29_X1C04_HwToleranceThreshold_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_X1C05_DtcActivationVbatEnable_v() (Rte_AddrPar_0x29_X1C05_DtcActivationVbatEnable_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_Diag_Act_DOWHS01_P1V6O_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6O_Diag_Act_DOWHS01_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Diag_Act_DOWHS02_P1V6P_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6P_Diag_Act_DOWHS02_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Diag_Act_DOWLS02_P1V7E_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7E_Diag_Act_DOWLS02_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Diag_Act_DOWLS03_P1V7F_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7F_Diag_Act_DOWLS03_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6I_Diag_Act_AO12_P_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6K_Diag_Act_AO12_L_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6Q_Diag_Act_DOBHS01_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6R_Diag_Act_DOBHS02_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6S_Diag_Act_DOBHS03_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6T_Diag_Act_DOBHS04_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7D_Diag_Act_DOBLS01_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7G_Diag_Act_DAI01_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7H_Diag_Act_DAI02_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8E_Diag_Act_12VDCDC_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Diag_Act_LF_P_P1V79_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V79_Diag_Act_LF_P_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1V6O_Diag_Act_DOWHS01_v() (Rte_AddrPar_0x2B_P1V6O_Diag_Act_DOWHS01_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V6P_Diag_Act_DOWHS02_v() (Rte_AddrPar_0x2B_P1V6P_Diag_Act_DOWHS02_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V7E_Diag_Act_DOWLS02_v() (Rte_AddrPar_0x2B_P1V7E_Diag_Act_DOWLS02_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V7F_Diag_Act_DOWLS03_v() (Rte_AddrPar_0x2B_P1V7F_Diag_Act_DOWLS03_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V6I_Diag_Act_AO12_P_v() (Rte_AddrPar_0x2B_P1V6I_Diag_Act_AO12_P_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V6K_Diag_Act_AO12_L_v() (Rte_AddrPar_0x2B_P1V6K_Diag_Act_AO12_L_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v() (Rte_AddrPar_0x2B_P1V6Q_Diag_Act_DOBHS01_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V6R_Diag_Act_DOBHS02_v() (Rte_AddrPar_0x2B_P1V6R_Diag_Act_DOBHS02_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V6S_Diag_Act_DOBHS03_v() (Rte_AddrPar_0x2B_P1V6S_Diag_Act_DOBHS03_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V6T_Diag_Act_DOBHS04_v() (Rte_AddrPar_0x2B_P1V6T_Diag_Act_DOBHS04_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V7D_Diag_Act_DOBLS01_v() (Rte_AddrPar_0x2B_P1V7D_Diag_Act_DOBLS01_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V7G_Diag_Act_DAI01_v() (Rte_AddrPar_0x2B_P1V7G_Diag_Act_DAI01_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V7H_Diag_Act_DAI02_v() (Rte_AddrPar_0x2B_P1V7H_Diag_Act_DAI02_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V8E_Diag_Act_12VDCDC_v() (Rte_AddrPar_0x2B_P1V8E_Diag_Act_12VDCDC_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V79_Diag_Act_LF_P_v() (&Rte_AddrPar_0x2B_P1V79_Diag_Act_LF_P_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define DiagnosticMonitor_IO_START_SEC_CODE
# include "DiagnosticMonitor_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData
#  define RTE_RUNNABLE_DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData
#  define RTE_RUNNABLE_DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData
#  define RTE_RUNNABLE_DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData
#  define RTE_RUNNABLE_DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData
#  define RTE_RUNNABLE_DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData
#  define RTE_RUNNABLE_DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData
#  define RTE_RUNNABLE_DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData
#  define RTE_RUNNABLE_DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData
#  define RTE_RUNNABLE_DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData
#  define RTE_RUNNABLE_DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_FreezeCurrentState DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData
#  define RTE_RUNNABLE_DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReturnControlToECU DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment
#  define RTE_RUNNABLE_DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_FreezeCurrentState DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData
#  define RTE_RUNNABLE_DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReturnControlToECU DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment
#  define RTE_RUNNABLE_DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_FreezeCurrentState DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData
#  define RTE_RUNNABLE_DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReturnControlToECU DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment
#  define RTE_RUNNABLE_DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_FreezeCurrentState DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData
#  define RTE_RUNNABLE_DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReturnControlToECU DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment
#  define RTE_RUNNABLE_DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_FreezeCurrentState DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData
#  define RTE_RUNNABLE_DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReturnControlToECU DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment
#  define RTE_RUNNABLE_DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_FreezeCurrentState DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData
#  define RTE_RUNNABLE_DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReturnControlToECU DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment
#  define RTE_RUNNABLE_DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_FreezeCurrentState DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData
#  define RTE_RUNNABLE_DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment
#  define RTE_RUNNABLE_DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_FreezeCurrentState DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData
#  define RTE_RUNNABLE_DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReturnControlToECU DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment
#  define RTE_RUNNABLE_DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_FreezeCurrentState DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData
#  define RTE_RUNNABLE_DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReturnControlToECU DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment
#  define RTE_RUNNABLE_DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_FreezeCurrentState DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData
#  define RTE_RUNNABLE_DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReturnControlToECU DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment
#  define RTE_RUNNABLE_DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_FreezeCurrentState DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData
#  define RTE_RUNNABLE_DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment
#  define RTE_RUNNABLE_DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData
#  define RTE_RUNNABLE_DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_FreezeCurrentState DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData
#  define RTE_RUNNABLE_DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReturnControlToECU DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment
#  define RTE_RUNNABLE_DiagMonitor_IO_10ms_runnable DiagMonitor_IO_10ms_runnable
# endif

# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment(P2CONST(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment(P2CONST(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment(P2CONST(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment(P2CONST(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment(P2CONST(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, DiagnosticMonitor_IO_CODE) DiagMonitor_IO_10ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define DiagnosticMonitor_IO_STOP_SEC_CODE
# include "DiagnosticMonitor_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_AdiInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_DataServices_P1QXI_Data_P1QXI_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ1_Data_P1VQ1_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ2_Data_P1VQ2_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ3_Data_P1VQ3_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ4_Data_P1VQ4_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ5_Data_P1VQ5_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ6_Data_P1VQ6_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ7_Data_P1VQ7_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ8_Data_P1VQ8_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ9_Data_P1VQ9_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRA_Data_P1VRA_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRB_Data_P1VRB_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRC_Data_P1VRC_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRD_Data_P1VRD_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRE_Data_P1VRE_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRF_Data_P1VRF_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRG_Data_P1VRG_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRH_Data_P1VRH_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRI_Data_P1VRI_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRJ_Data_P1VRJ_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRK_Data_P1VRK_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRL_Data_P1VRL_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRM_Data_P1VRM_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRN_Data_P1VRN_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRO_Data_P1VRO_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRS_Data_P1VRS_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRT_Data_P1VRT_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRU_Data_P1VRU_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRV_Data_P1VRV_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRW_Data_P1VRW_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK (1U)

#  define RTE_E_DataServices_X1CV5_Data_X1CV5_E_NOT_OK (1U)

#  define RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK (1U)

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)

#  define RTE_E_Do12VInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_DowxsInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_VbatInterface_I_AdcInFailure (2U)

#  define RTE_E_VbatInterface_I_IoHwAbApplicationError (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_DIAGNOSTICMONITOR_IO_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
