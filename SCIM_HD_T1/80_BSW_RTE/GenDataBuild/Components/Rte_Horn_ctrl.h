/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Horn_ctrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <Horn_ctrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_HORN_CTRL_H
# define _RTE_HORN_CTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_Horn_ctrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_Horn_HMICtrl_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_Horn_HMICtrl_CityHorn_HMI_rqst_CityHorn_HMI_rqst; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Parked_Parked; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst (3U)
#  define Rte_InitValue_AuxiliaryHorn_cmd_AuxiliaryHorn_cmd (3U)
#  define Rte_InitValue_CityHorn_HMI_rqst_CityHorn_HMI_rqst (3U)
#  define Rte_InitValue_CityHorn_cmd_CityHorn_cmd (3U)
#  define Rte_InitValue_CtaBB_Horn_rqst_CtaBB_Horn_rqst (3U)
#  define Rte_InitValue_PanicAlarmFromKeyfob_rqst_PanicAlarmFromKeyfob_rqst (3U)
#  define Rte_InitValue_SwcActivation_Parked_Parked (1U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_Horn_ctrl_CtaBB_Horn_rqst_CtaBB_Horn_rqst(P2VAR(OffOn_T, AUTOMATIC, RTE_HORN_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_Horn_ctrl_AuxiliaryHorn_cmd_AuxiliaryHorn_cmd(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_Horn_ctrl_CityHorn_cmd_CityHorn_cmd(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst Rte_Read_Horn_ctrl_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst
#  define Rte_Read_Horn_ctrl_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst(data) (*(data) = Rte_Horn_HMICtrl_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_CityHorn_HMI_rqst_CityHorn_HMI_rqst Rte_Read_Horn_ctrl_CityHorn_HMI_rqst_CityHorn_HMI_rqst
#  define Rte_Read_Horn_ctrl_CityHorn_HMI_rqst_CityHorn_HMI_rqst(data) (*(data) = Rte_Horn_HMICtrl_CityHorn_HMI_rqst_CityHorn_HMI_rqst, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_CtaBB_Horn_rqst_CtaBB_Horn_rqst Rte_Read_Horn_ctrl_CtaBB_Horn_rqst_CtaBB_Horn_rqst
#  define Rte_Read_PanicAlarmFromKeyfob_rqst_PanicAlarmFromKeyfob_rqst Rte_Read_Horn_ctrl_PanicAlarmFromKeyfob_rqst_PanicAlarmFromKeyfob_rqst
#  define Rte_Read_Horn_ctrl_PanicAlarmFromKeyfob_rqst_PanicAlarmFromKeyfob_rqst(data) (*(data) = 3U, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_Parked_Parked Rte_Read_Horn_ctrl_SwcActivation_Parked_Parked
#  define Rte_Read_Horn_ctrl_SwcActivation_Parked_Parked(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Parked_Parked, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_AuxiliaryHorn_cmd_AuxiliaryHorn_cmd Rte_Write_Horn_ctrl_AuxiliaryHorn_cmd_AuxiliaryHorn_cmd
#  define Rte_Write_CityHorn_cmd_CityHorn_cmd Rte_Write_Horn_ctrl_CityHorn_cmd_CityHorn_cmd


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1A1T_CityHorn_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1A1T_CityHorn_Act_v() (Rte_AddrPar_0x2B_and_0x37_P1A1T_CityHorn_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define Horn_ctrl_START_SEC_CODE
# include "Horn_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_Horn_ctrl_20ms_runnable Horn_ctrl_20ms_runnable
# endif

FUNC(void, Horn_ctrl_CODE) Horn_ctrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define Horn_ctrl_STOP_SEC_CODE
# include "Horn_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_HORN_CTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
