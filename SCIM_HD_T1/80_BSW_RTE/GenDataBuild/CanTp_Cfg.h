/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: CanTp
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: CanTp_Cfg.h
 *   Generation Time: 2020-09-07 16:09:39
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/

#if !defined(CANTP_CFG_H)
#define CANTP_CFG_H

/* -----------------------------------------------------------------------------
    &&&~ Include files
 ----------------------------------------------------------------------------- */

#include "ComStack_Types.h"

/* Definition of feature switches in the library delivered */
#ifndef CANTP_USE_DUMMY_STATEMENT
#define CANTP_USE_DUMMY_STATEMENT STD_OFF /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef CANTP_DUMMY_STATEMENT
#define CANTP_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef CANTP_DUMMY_STATEMENT_CONST
#define CANTP_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef CANTP_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define CANTP_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef CANTP_ATOMIC_VARIABLE_ACCESS
#define CANTP_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef CANTP_PROCESSOR_MPC5746C
#define CANTP_PROCESSOR_MPC5746C
#endif
#ifndef CANTP_COMP_DIAB
#define CANTP_COMP_DIAB
#endif
#ifndef CANTP_GEN_GENERATOR_MSR
#define CANTP_GEN_GENERATOR_MSR
#endif
#ifndef CANTP_CPUTYPE_BITORDER_MSB2LSB
#define CANTP_CPUTYPE_BITORDER_MSB2LSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef CANTP_CONFIGURATION_VARIANT_PRECOMPILE
#define CANTP_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef CANTP_CONFIGURATION_VARIANT_LINKTIME
#define CANTP_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef CANTP_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define CANTP_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef CANTP_CONFIGURATION_VARIANT
#define CANTP_CONFIGURATION_VARIANT CANTP_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef CANTP_POSTBUILD_VARIANT_SUPPORT
#define CANTP_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif


/* PRQA S 3453 MACROS_3453 */  /* MD_CSL_3453 */
 /*  Version information  */ 
#define CANTP_CFG_GEN_MAJOR_VERSION                                            4u 
#define CANTP_CFG_GEN_MINOR_VERSION                                            4u 
#define CANTP_CFG_GEN_PATCH_VERSION                                            0u 

 /*  Global defines  */ 
#define CANTP_VERSION_INFO_API                                                 STD_OFF 
#define CANTP_DEV_ERROR_DETECT                                                 STD_OFF 
#define CANTP_DEV_ERROR_REPORT                                                 STD_OFF 
#define CANTP_TC                                                               STD_ON 
#define CANTP_RC                                                               STD_OFF 
#define CANTP_INVALID_HDL                                                      ((PduIdType) 65535u) 
#define CANTP_TASK_CYCLE                                                       5u 

 /*  Global constant defines  */ 
#define CANTP_MAX_FRAME_LENGTH                                                 8u 
#define CANTP_MAX_PAYLOAD_LENGTH                                               7u 
#define CANTP_MAX_PDU_METADATA_LENGTH                                          0u 
#define CANTP_MAX_SDU_METADATA_LENGTH                                          0u 
#define CANTP_NUM_RX_CHANNELS                                                  CanTp_GetSizeOfRxState() 
#define CANTP_NUM_RX_SDUS                                                      75 
#define CANTP_NUM_TX_CHANNELS                                                  CanTp_GetSizeOfTxState() 
#define CANTP_NUM_TX_SDUS                                                      72 
#define CANTP_SIZEOF_PDULENGTHTYPE                                             2 

 /*  Communication Type  */ 
#define CANTP_MODE_FULL_DUPLEX                                                 1u 
#define CANTP_MODE_HALF_DUPLEX                                                 0u 
#define CANTP_TATYPE_FUNCTIONAL                                                1u 
#define CANTP_TATYPE_PHYSICAL                                                  0u 

 /*  CanTp Adress Format  */ 
#define CANTP_ADDRESS_FORMAT_EXTENDED                                          1u 
#define CANTP_ADDRESS_FORMAT_MIXED11                                           2u 
#define CANTP_ADDRESS_FORMAT_MIXED29                                           4u 
#define CANTP_ADDRESS_FORMAT_NORMALFIXED                                       3u 
#define CANTP_ADDRESS_FORMAT_STANDARD                                          0u 

 /*  Can Type  */ 
#define CANTP_CANTYPE_CAN20                                                    0u 
#define CANTP_CANTYPE_CANFD                                                    1u 

 /*  Vector-CanTp extended capabilities  */ 
#define CANTP_GENERATOR_COMPATIBILITY_VERSION                                  0x0005D047uL                                                   /* Extended error detection capabilities */ 
#define CANTP_CAN20_PADDING_ACTIVE                                             STD_ON 
#define CANTP_HAVE_PADDING_BYTE                                                STD_OFF 
#define CANTP_PADDING_PATTERN                                                  0xCCu                                                          /* Padding byte to be used for the Tx side if CANTP_HAVE_PADDING_BYTE is ON */ 
#define CANTP_DYN_CHANNEL_ASSIGNMENT                                           STD_OFF 
#define CANTP_STANDARD_ADDRESSING                                              STD_ON 
#define CANTP_EXTENDED_ADDRESSING                                              STD_ON 
#define CANTP_MIXED11_ADDRESSING                                               STD_ON 
#define CANTP_MIXED29_ADDRESSING                                               STD_OFF 
#define CANTP_NORMALFIXED_ADDRESSING                                           STD_OFF 
#define CANTP_SINGLE_RX_BUFFER_OPTIMIZED                                       STD_OFF 
#define CANTP_RXTX_MAINFUNCTION_API                                            STD_ON 
#define CANTP_CONSTANT_BS                                                      STD_ON 
#define CANTP_REJECT_FC_WITH_RES_STMIN                                         STD_OFF 
#define CANTP_USE_ONLY_FIRST_FC                                                STD_OFF 
#define CANTP_ENABLE_CHANGE_PARAM                                              STD_OFF 
#define CANTP_ENABLE_READ_PARAM                                                STD_OFF 
#define CANTP_ONLY_NOTIFY_INFORMED_APPL                                        STD_ON 
#define CANTP_TRANSMIT_QUEUE                                                   STD_OFF 
#define CANTP_SUPPORT_LONG_FF                                                  STD_OFF 
#define CANTP_PDUR_API_AR_VERSION                                              0x412 
#define CANTP_SYNC_TRANSMIT                                                    STD_ON 
#define CANTP_SUPPORT_CANFD                                                    STD_OFF 
#define CANTP_STMIN_BY_APPL                                                    STD_OFF 
#define CANTP_DCM_REQUEST_DETECT                                               STD_OFF 
#define CANTP_GENERIC_CONNECTIONS                                              STD_OFF 
#define CanTp_ApplStartSeparationTime                                           /*  mapping of user defined function to internal name  */  

 /*  CanTp Lower Layer  */ 
#define CANTP_LOLAYER_CANIF                                                    STD_ON 
#define CANTP_LOLAYER_TC                                                       STD_ON 
#define CanTp_CanIfCancelTransmit(Dir, SduId, PduId)                           ((void)CanIf_CancelTransmit((PduId))) 
#define CanTp_CanIfTransmit(Dir, SduId, PduId, Data)                           (CanIf_Transmit((PduId),(Data))) 


/* PRQA L:MACROS_3453 */



/* -----------------------------------------------------------------------------
    &&&~ Pre-Compile optimized configuration
 ----------------------------------------------------------------------------- */
 




/* -----------------------------------------------------------------------------
    &&&~ Rx Sdu Handle IDs
----------------------------------------------------------------------------- */
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_0b4e5ed3 0u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_0bfbcf81 1u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_0c44e225 2u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_0fc83382 3u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_1a4aae9e 4u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_2ed2671b 5u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_3cc05d5a 6u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_4d8c7144 7u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_6b50c215 8u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_6c38d736 9u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_6eda9336 10u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_9bc05ec2 11u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_9c809a2e 12u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_9fd60b61 13u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_05d82786 14u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_08e2bc01 15u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_15cc9c64 16u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_23c20b77 17u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_33cb3e9e 18u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_37a24ae1 19u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_53d85cb5 20u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_66ceed3b 21u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_67a44a47 22u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_68a1d221 23u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_72ac09b7 24u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_90b4ee1f 25u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_97d435bf 26u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_135f1a6f 27u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_155b67c1 28u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_160b460d 29u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_374c2f98 30u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_420eda30 31u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_835e9534 32u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_871bc7b1 33u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_917ba177 34u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_948ebe8b 35u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_1047cc45 36u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_7107e017 37u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_8729f19d 38u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_598434b3 39u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_2609472a 40u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_4374674a 41u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_8388957a 42u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_33326932 43u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_93056798 44u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_a8f40adf 45u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_a23f8213 46u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_a9722d89 47u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_b7bc20cf 48u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_b397195b 49u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_bc085821 50u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_bd494c6b 51u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_bf49619c 52u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_c3d81543 53u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_c8d90e4f 54u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_c9b42c98 55u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_c66f942c 56u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_c046cd74 57u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_c8855cd9 58u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_d7af4a84 59u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_d137db99 60u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_dbf2493d 61u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_de4f6c03 62u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_e0b16505 63u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_e66abd8d 64u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_e128ac5e 65u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_f0ca165a 66u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_f0daeab1 67u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_f5b24442 68u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_f5e91b1b 69u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_f7a6d049 70u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_f932f12a 71u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_fd5af81d 72u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_fe9c6258 73u 
#define CanTpConf_CanTpRxNSdu_CanTpRxNSdu_ff43a8b1 74u 

 
/* -----------------------------------------------------------------------------
    &&&~ Tx Sdu Handle IDs
----------------------------------------------------------------------------- */
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_0b4e5ed3 0u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_0b28b3c4 1u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_0f9a4f9c 2u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_0fc83382 3u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_1a4aae9e 4u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_2b6a0a70 5u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_2c8e4400 6u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_2c8eef3f 7u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_2ed2671b 8u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_4c161f1d 9u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_6b50c215 10u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_6eda9336 11u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_9bc05ec2 12u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_9fd60b61 13u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_9fe90810 14u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_05a0c60a 15u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_05d82786 16u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_15cc9c64 17u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_23c20b77 18u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_33cb3e9e 19u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_39c7c1db 20u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_66ceed3b 21u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_67a44a47 22u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_68a1d221 23u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_72ac09b7 24u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_85a43433 25u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_90b4ee1f 26u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_97d435bf 27u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_072ad05f 28u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_155b67c1 29u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_160b460d 30u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_338fdf39 31u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_374c2f98 32u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_420eda30 33u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_421e4ec4 34u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_638f93bd 35u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_917ba177 36u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_1047cc45 37u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_8729f19d 38u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_67565e28 39u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_81649aaa 40u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_2609472a 41u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_8388957a 42u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_40316546 43u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_93056798 44u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_a23f8213 45u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_ac62b136 46u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_b27caf41 47u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_b77300d6 48u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_b397195b 49u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_bb7fa63e 50u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_bc085821 51u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_bf49619c 52u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_c8d90e4f 53u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_c66f942c 54u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_c90c57b6 55u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_c046cd74 56u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_c8855cd9 57u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_cfd2742b 58u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_d7af4a84 59u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_d137db99 60u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_dbf2493d 61u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_de4f6c03 62u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_e128ac5e 63u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_f0ca165a 64u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_f0daeab1 65u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_f3c4bb5a 66u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_f5b24442 67u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_f5e91b1b 68u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_f7a6d049 69u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_f8c8b245 70u 
#define CanTpConf_CanTpTxNSdu_CanTpTxNSdu_ff43a8b1 71u 


/* -----------------------------------------------------------------------------
    &&&~ Rx Pdu IDs
 ----------------------------------------------------------------------------- */
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_16bfd7e5 0u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_34450317     0u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_29e880d4 1u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_0b125426     1u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_702e9b3a 2u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_52d44fc8     2u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_9b5bb09d 3u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_b9a1646f     3u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_eae0d404 4u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_c81a00f6     4u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_5eaf037d 5u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_7c55d78f     5u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_a8433250 6u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_8ab9e6a2     6u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_72406594 7u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_50bab166     7u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_d7b79970 8u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_f54d4d82     8u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_08f507d2 9u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_2a0fd320     9u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_67618176 10u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_459b5584     10u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_4c1bc602 11u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_6ee112f0     11u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_3a6fb7af 12u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_1895635d     12u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_cb71089b 13u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_e98bdc69     13u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_9d493ab9 14u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_bfb3ee4b     14u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_779a5c21 15u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_556088d3     15u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_05536183 16u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_27a9b571     16u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_561bc244 17u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_74e116b6     17u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_b93ef70d 18u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_9bc423ff     18u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_46dc57a6 19u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_64268354     19u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_5e62d968 20u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_7c980d9a     20u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_c00998cc 21u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_e2f34c3e     21u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_b1472493 22u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_93bdf061     22u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_52d3aa3f 23u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_70297ecd     23u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_e26c1fa4 24u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_c096cb56     24u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_47310e07 25u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_65cbdaf5     25u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_20d338ab 26u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_0229ec59     26u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_0a210417 27u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_28dbd0e5     27u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_b73de2d0 28u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_95c73622     28u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_5c415aeb 29u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_7ebb8e19     29u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_f7837c7d 30u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_d579a88f     30u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_65491b5b 31u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_47b3cfa9     31u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_0a7bda08 32u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_28810efa     32u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_e5f0678e 33u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_c70ab37c     33u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_2e22b0e3 34u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_0cd86411     34u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_6e9d45e7 35u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_4c679115     35u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_ae8fba73 36u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_8c756e81     36u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_3dcbb367 37u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_1f316795     37u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_7cc5ba27 38u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_5e3f6ed5     38u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_679e1c35 39u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_4564c8c7     39u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_e51b1aae 40u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_c7e1ce5c     40u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_14ebb75e 41u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_361163ac     41u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_79805470 42u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_5b7a8082     42u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_e015a674 43u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_c2ef7286     43u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_14ea7b0b 44u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_3610aff9     44u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_caf0e2e7 45u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_e80a3615     45u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_a2acba40 46u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_80566eb2     46u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_0630f603     47u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_fc653524     48u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_f5215d2b     49u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_8db8bc2c     50u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_2b3a4000     51u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_41bdbd59 52u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_0cd94d72 53u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_cf81800e 54u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_2242705f 55u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_89f8ff59 56u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_8b06b311 57u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_1b772fca 58u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_4322dc05 59u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_51ea11e5 60u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_9a489703 61u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_4ce4798a 62u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_2679860a 63u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_d3983306 64u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_cc0bb21d 65u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_7d9d2994 66u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_ae8e313f 67u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_1bcf3d59 68u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_16d15085 69u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_6def313d 70u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_adf550fb 71u 
#define CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_426219dc 72u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_9778b9b1     73u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_c0beb877     74u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_51e36fbf     75u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_715a9875     76u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_25603869     77u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_409d9ec2     78u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_ef1549ec     79u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_d667832b     80u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_ada8fbb1     81u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_629a014b     82u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_fdd87b12     83u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_465d75f5     84u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_904bcd5d     85u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_e9176fdb     86u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_421ce650     87u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_ddd13b60     88u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_b76b68d2     89u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_349983d6     90u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_21f32ee4     91u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_19b1d643     92u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_5a70888a     93u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_7f7d9999     94u 
#define CanTpConf_CanTpRxNPdu_CanTpRxNPdu_28a0dc1f     95u 


/* -----------------------------------------------------------------------------
    &&&~ TxConfirmation Pdu IDs
 ----------------------------------------------------------------------------- */
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_27a9b571 0u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_05536183     0u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_74e116b6 1u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_561bc244     1u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_9bc423ff 2u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_b93ef70d     2u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_64268354 3u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_46dc57a6     3u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_7c980d9a 4u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_5e62d968     4u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_e2f34c3e 5u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_c00998cc     5u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_93bdf061 6u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_b1472493     6u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_70297ecd 7u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_52d3aa3f     7u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_c096cb56 8u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_e26c1fa4     8u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_65cbdaf5 9u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_47310e07     9u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_0229ec59 10u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_20d338ab     10u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_28dbd0e5 11u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_0a210417     11u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_95c73622 12u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_b73de2d0     12u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_7ebb8e19 13u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_5c415aeb     13u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_34450317 14u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_16bfd7e5     14u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_0b125426 15u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_29e880d4     15u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_52d44fc8 16u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_702e9b3a     16u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_b9a1646f 17u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_9b5bb09d     17u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_c81a00f6 18u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_eae0d404     18u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_7c55d78f 19u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_5eaf037d     19u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_8ab9e6a2 20u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_a8433250     20u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_50bab166 21u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_72406594     21u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_f54d4d82 22u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_d7b79970     22u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_2a0fd320 23u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_08f507d2     23u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_459b5584 24u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_67618176     24u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_6ee112f0 25u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_4c1bc602     25u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_1895635d 26u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_3a6fb7af     26u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_e98bdc69 27u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_cb71089b     27u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_bfb3ee4b 28u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_9d493ab9     28u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_556088d3 29u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_779a5c21     29u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_c7e1ce5c 30u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_e51b1aae     30u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_4564c8c7 31u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_679e1c35     31u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_361163ac 32u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_14ebb75e     32u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_5b7a8082 33u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_79805470     33u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_c2ef7286 34u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_e015a674     34u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_3610aff9 35u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_14ea7b0b     35u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_e80a3615 36u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_caf0e2e7     36u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_80566eb2 37u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_a2acba40     37u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_28810efa 38u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_0a7bda08     38u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_d579a88f 39u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_f7837c7d     39u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_47b3cfa9 40u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_65491b5b     40u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_c70ab37c 41u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_e5f0678e     41u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_0cd86411 42u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_2e22b0e3     42u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_4c679115 43u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_6e9d45e7     43u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_8c756e81 44u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_ae8fba73     44u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_1f316795 45u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_3dcbb367     45u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_5e3f6ed5 46u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_7cc5ba27     46u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_0630f603 47u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_fc653524 48u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_f5215d2b 49u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_8db8bc2c 50u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_2b3a4000 51u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_41bdbd59     52u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_0cd94d72     53u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_cf81800e     54u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_2242705f     55u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_89f8ff59     56u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_8b06b311     57u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_1b772fca     58u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_4322dc05     59u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_51ea11e5     60u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_9a489703     61u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_4ce4798a     62u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_2679860a     63u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_d3983306     64u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_cc0bb21d     65u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_7d9d2994     66u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_ae8e313f     67u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_1bcf3d59     68u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_16d15085     69u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_6def313d     70u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_adf550fb     71u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_426219dc     72u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_9778b9b1 73u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_c0beb877 74u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_51e36fbf 75u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_715a9875 76u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_25603869 77u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_409d9ec2 78u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_ef1549ec 79u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_d667832b 80u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_ada8fbb1 81u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_629a014b 82u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_fdd87b12 83u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_465d75f5 84u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_904bcd5d 85u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_f3f911f3     86u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_2e007fb2     87u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_421ce650 88u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_ddd13b60 89u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_b76b68d2 90u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_349983d6 91u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_9516a986     92u 
#define CanTpConf_CanTpTxNPdu_CanTpTxNPdu_0fcb0b72     93u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_19b1d643 94u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_5a70888a 95u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_7f7d9999 96u 
#define CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_28a0dc1f 97u 




 /**********************************************************************************************************************
 * MACROS
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  CanTpPCDataSwitches  CanTp Data Switches  (PRE_COMPILE)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define CANTP_CALCBS                                                                                STD_ON
#define CANTP_COMPATIBILITYVERSION                                                                  STD_ON
#define CANTP_DYNFCPARAMETERS                                                                       STD_OFF  /**< Deactivateable: 'CanTp_DynFCParameters' Reason: 'Change Paramter Api is disabled' */
#define CANTP_FINALMAGICNUMBER                                                                      STD_OFF  /**< Deactivateable: 'CanTp_FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define CANTP_INITDATAHASHCODE                                                                      STD_OFF  /**< Deactivateable: 'CanTp_InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define CANTP_LOLAYERFCTS                                                                           STD_OFF  /**< Deactivateable: 'CanTp_LoLayerFcts' Reason: 'A function table is not required, since just one lower layer exists.' */
#define CANTP_CANCELTRANSMITFCTOFLOLAYERFCTS                                                        STD_OFF  /**< Deactivateable: 'CanTp_LoLayerFcts.CancelTransmitFct' Reason: 'A function table is not required, since just one lower layer exists.' */
#define CANTP_TRANSMITFCTOFLOLAYERFCTS                                                              STD_OFF  /**< Deactivateable: 'CanTp_LoLayerFcts.TransmitFct' Reason: 'A function table is not required, since just one lower layer exists.' */
#define CANTP_RXCHANNELMAP                                                                          STD_OFF  /**< Deactivateable: 'CanTp_RxChannelMap' Reason: 'Dynamic Channel Assignment is disabled' */
#define CANTP_RXPDUMAP                                                                              STD_ON
#define CANTP_ADDRESSINGFORMATOFRXPDUMAP                                                            STD_ON
#define CANTP_GENERICCONNECTIONOFRXPDUMAP                                                           STD_ON
#define CANTP_PDUMETADATALENGTHOFRXPDUMAP                                                           STD_ON
#define CANTP_RXSDUCFGINDENDIDXOFRXPDUMAP                                                           STD_ON
#define CANTP_RXSDUCFGINDSTARTIDXOFRXPDUMAP                                                         STD_ON
#define CANTP_RXSDUCFGINDUSEDOFRXPDUMAP                                                             STD_ON
#define CANTP_TXSDUCFGINDENDIDXOFRXPDUMAP                                                           STD_ON
#define CANTP_TXSDUCFGINDSTARTIDXOFRXPDUMAP                                                         STD_ON
#define CANTP_TXSDUCFGINDUSEDOFRXPDUMAP                                                             STD_ON
#define CANTP_RXSDUCFG                                                                              STD_ON
#define CANTP_BLOCKSIZEOFRXSDUCFG                                                                   STD_ON
#define CANTP_CANTYPEOFRXSDUCFG                                                                     STD_ON
#define CANTP_CHANNELMODEOFRXSDUCFG                                                                 STD_ON
#define CANTP_GENERICCONNECTIONOFRXSDUCFG                                                           STD_ON
#define CANTP_LOLAYERFCTSIDXOFRXSDUCFG                                                              STD_OFF  /**< Deactivateable: 'CanTp_RxSduCfg.LoLayerFctsIdx' Reason: 'All indirection targets are deactivated in all variants.' */
#define CANTP_LOLAYERTXFCPDUIDOFRXSDUCFG                                                            STD_ON
#define CANTP_NAROFRXSDUCFG                                                                         STD_ON
#define CANTP_NBROFRXSDUCFG                                                                         STD_ON
#define CANTP_NCROFRXSDUCFG                                                                         STD_ON
#define CANTP_PASSSDUMETADATAOFRXSDUCFG                                                             STD_ON
#define CANTP_PDURRXSDUIDOFRXSDUCFG                                                                 STD_ON
#define CANTP_RXADDRESSOFRXSDUCFG                                                                   STD_ON
#define CANTP_RXADDRESSINGFORMATOFRXSDUCFG                                                          STD_ON
#define CANTP_RXMAXPAYLOADLENGTHOFRXSDUCFG                                                          STD_ON
#define CANTP_RXPADDINGACTIVATIONOFRXSDUCFG                                                         STD_ON
#define CANTP_RXPDUIDOFRXSDUCFG                                                                     STD_ON
#define CANTP_RXTATYPEOFRXSDUCFG                                                                    STD_ON
#define CANTP_RXWFTMAXOFRXSDUCFG                                                                    STD_ON
#define CANTP_STMINOFRXSDUCFG                                                                       STD_ON
#define CANTP_TXFCADDRESSOFRXSDUCFG                                                                 STD_ON
#define CANTP_TXFCPDUCONFIRMATIONPDUIDOFRXSDUCFG                                                    STD_ON
#define CANTP_TXSDUCFGIDXOFRXSDUCFG                                                                 STD_ON
#define CANTP_TXSDUCFGUSEDOFRXSDUCFG                                                                STD_ON
#define CANTP_RXSDUCFGIND                                                                           STD_ON
#define CANTP_RXSDUSNV2HDL                                                                          STD_ON
#define CANTP_RXSDUCFGIDXOFRXSDUSNV2HDL                                                             STD_ON
#define CANTP_RXSDUCFGUSEDOFRXSDUSNV2HDL                                                            STD_ON
#define CANTP_RXSTATE                                                                               STD_ON
#define CANTP_SIZEOFCALCBS                                                                          STD_ON
#define CANTP_SIZEOFRXPDUMAP                                                                        STD_ON
#define CANTP_SIZEOFRXSDUCFG                                                                        STD_ON
#define CANTP_SIZEOFRXSDUCFGIND                                                                     STD_ON
#define CANTP_SIZEOFRXSDUSNV2HDL                                                                    STD_ON
#define CANTP_SIZEOFRXSTATE                                                                         STD_ON
#define CANTP_SIZEOFTXSDUCFG                                                                        STD_ON
#define CANTP_SIZEOFTXSDUCFGIND                                                                     STD_ON
#define CANTP_SIZEOFTXSDUSNV2HDL                                                                    STD_ON
#define CANTP_SIZEOFTXSEMAPHORES                                                                    STD_ON
#define CANTP_SIZEOFTXSTATE                                                                         STD_ON
#define CANTP_TXCHANNELMAP                                                                          STD_OFF  /**< Deactivateable: 'CanTp_TxChannelMap' Reason: 'Dynamic Channel Assignment is disabled' */
#define CANTP_TXQUEUE                                                                               STD_OFF  /**< Deactivateable: 'CanTp_TxQueue' Reason: 'Transmit Queue is disabled' */
#define CANTP_TXSDUCFG                                                                              STD_ON
#define CANTP_CANTYPEOFTXSDUCFG                                                                     STD_ON
#define CANTP_CHANNELMODEOFTXSDUCFG                                                                 STD_ON
#define CANTP_LOLAYERFCTSIDXOFTXSDUCFG                                                              STD_OFF  /**< Deactivateable: 'CanTp_TxSduCfg.LoLayerFctsIdx' Reason: 'All indirection targets are deactivated in all variants.' */
#define CANTP_LOLAYERTXPDUIDOFTXSDUCFG                                                              STD_ON
#define CANTP_NASOFTXSDUCFG                                                                         STD_ON
#define CANTP_NBSOFTXSDUCFG                                                                         STD_ON
#define CANTP_NCSOFTXSDUCFG                                                                         STD_ON
#define CANTP_PDURTXSDUIDOFTXSDUCFG                                                                 STD_ON
#define CANTP_RXFCADDRESSOFTXSDUCFG                                                                 STD_ON
#define CANTP_RXFCPDUIDOFTXSDUCFG                                                                   STD_ON
#define CANTP_RXSDUCFGIDXOFTXSDUCFG                                                                 STD_ON
#define CANTP_RXSDUCFGUSEDOFTXSDUCFG                                                                STD_ON
#define CANTP_SDUMETADATALENGTHOFTXSDUCFG                                                           STD_ON
#define CANTP_TRANSMITCANCELLATIONOFTXSDUCFG                                                        STD_ON
#define CANTP_TXADDRESSOFTXSDUCFG                                                                   STD_ON
#define CANTP_TXADDRESSINGFORMATOFTXSDUCFG                                                          STD_ON
#define CANTP_TXMAXPAYLOADLENGTHOFTXSDUCFG                                                          STD_ON
#define CANTP_TXPADDINGACTIVATIONOFTXSDUCFG                                                         STD_ON
#define CANTP_TXPDUCONFIRMATIONPDUIDOFTXSDUCFG                                                      STD_ON
#define CANTP_TXTATYPEOFTXSDUCFG                                                                    STD_ON
#define CANTP_TXSDUCFGIND                                                                           STD_ON
#define CANTP_TXSDUSNV2HDL                                                                          STD_ON
#define CANTP_TXSDUCFGIDXOFTXSDUSNV2HDL                                                             STD_ON
#define CANTP_TXSDUCFGUSEDOFTXSDUSNV2HDL                                                            STD_ON
#define CANTP_TXSEMAPHORES                                                                          STD_ON
#define CANTP_TXSTATE                                                                               STD_ON
#define CANTP_PCCONFIG                                                                              STD_ON
#define CANTP_CALCBSOFPCCONFIG                                                                      STD_ON
#define CANTP_COMPATIBILITYVERSIONOFPCCONFIG                                                        STD_ON
#define CANTP_FINALMAGICNUMBEROFPCCONFIG                                                            STD_OFF  /**< Deactivateable: 'CanTp_PCConfig.FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define CANTP_INITDATAHASHCODEOFPCCONFIG                                                            STD_OFF  /**< Deactivateable: 'CanTp_PCConfig.InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define CANTP_RXPDUMAPOFPCCONFIG                                                                    STD_ON
#define CANTP_RXSDUCFGINDOFPCCONFIG                                                                 STD_ON
#define CANTP_RXSDUCFGOFPCCONFIG                                                                    STD_ON
#define CANTP_RXSDUSNV2HDLOFPCCONFIG                                                                STD_ON
#define CANTP_RXSTATEOFPCCONFIG                                                                     STD_ON
#define CANTP_SIZEOFCALCBSOFPCCONFIG                                                                STD_ON
#define CANTP_SIZEOFRXPDUMAPOFPCCONFIG                                                              STD_ON
#define CANTP_SIZEOFRXSDUCFGINDOFPCCONFIG                                                           STD_ON
#define CANTP_SIZEOFRXSDUCFGOFPCCONFIG                                                              STD_ON
#define CANTP_SIZEOFRXSDUSNV2HDLOFPCCONFIG                                                          STD_ON
#define CANTP_SIZEOFRXSTATEOFPCCONFIG                                                               STD_ON
#define CANTP_SIZEOFTXSDUCFGINDOFPCCONFIG                                                           STD_ON
#define CANTP_SIZEOFTXSDUCFGOFPCCONFIG                                                              STD_ON
#define CANTP_SIZEOFTXSDUSNV2HDLOFPCCONFIG                                                          STD_ON
#define CANTP_SIZEOFTXSEMAPHORESOFPCCONFIG                                                          STD_ON
#define CANTP_SIZEOFTXSTATEOFPCCONFIG                                                               STD_ON
#define CANTP_TXSDUCFGINDOFPCCONFIG                                                                 STD_ON
#define CANTP_TXSDUCFGOFPCCONFIG                                                                    STD_ON
#define CANTP_TXSDUSNV2HDLOFPCCONFIG                                                                STD_ON
#define CANTP_TXSEMAPHORESOFPCCONFIG                                                                STD_ON
#define CANTP_TXSTATEOFPCCONFIG                                                                     STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  CanTpPCMinNumericValueDefines  CanTp Min Numeric Value Defines (PRE_COMPILE)
  \brief  These defines are used to implement against the minimum value in numerical based data.
  \{
*/ 
#define CANTP_MIN_CALCBS                                                                            0u
/** 
  \}
*/ 

/** 
  \defgroup  CanTpPCMaxNumericValueDefines  CanTp Max Numeric Value Defines (PRE_COMPILE)
  \brief  These defines are used to implement against the maximum value in numerical based data.
  \{
*/ 
#define CANTP_MAX_CALCBS                                                                            255u
/** 
  \}
*/ 

/** 
  \defgroup  CanTpPCNoReferenceDefines  CanTp No Reference Defines (PRE_COMPILE)
  \brief  These defines are used to indicate unused indexes in data relations.
  \{
*/ 
#define CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP                                                        255u
#define CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP                                                      255u
#define CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP                                                        255u
#define CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP                                                      255u
#define CANTP_NO_TXSDUCFGIDXOFRXSDUCFG                                                              255u
#define CANTP_NO_RXSDUCFGIDXOFRXSDUSNV2HDL                                                          255u
#define CANTP_NO_RXSDUCFGIDXOFTXSDUCFG                                                              255u
#define CANTP_NO_TXSDUCFGIDXOFTXSDUSNV2HDL                                                          255u
/** 
  \}
*/ 

/** 
  \defgroup  CanTpPCEnumExistsDefines  CanTp Enum Exists Defines (PRE_COMPILE)
  \brief  These defines can be used to deactivate enumeration based code sequences if the enumeration value does not exist in the configuration data.
  \{
*/ 
#define CANTP_EXISTS_NORMAL_ADDRESSING_ADDRESSINGFORMATOFRXPDUMAP                                   STD_ON
#define CANTP_EXISTS_EXTENDED_ADDRESSING_ADDRESSINGFORMATOFRXPDUMAP                                 STD_OFF
#define CANTP_EXISTS_MIXED11_ADDRESSING_ADDRESSINGFORMATOFRXPDUMAP                                  STD_OFF
#define CANTP_EXISTS_NORMALFIXED_ADDRESSING_ADDRESSINGFORMATOFRXPDUMAP                              STD_OFF
#define CANTP_EXISTS_MIXED29_ADDRESSING_ADDRESSINGFORMATOFRXPDUMAP                                  STD_OFF
#define CANTP_EXISTS_CAN20_CANTYPEOFRXSDUCFG                                                        STD_ON
#define CANTP_EXISTS_CANFD_CANTYPEOFRXSDUCFG                                                        STD_OFF
#define CANTP_EXISTS_HALF_DUPLEX_CHANNELMODEOFRXSDUCFG                                              STD_OFF
#define CANTP_EXISTS_FULL_DUPLEX_CHANNELMODEOFRXSDUCFG                                              STD_ON
#define CANTP_EXISTS_NORMAL_ADDRESSING_RXADDRESSINGFORMATOFRXSDUCFG                                 STD_ON
#define CANTP_EXISTS_EXTENDED_ADDRESSING_RXADDRESSINGFORMATOFRXSDUCFG                               STD_OFF
#define CANTP_EXISTS_MIXED11_ADDRESSING_RXADDRESSINGFORMATOFRXSDUCFG                                STD_OFF
#define CANTP_EXISTS_NORMALFIXED_ADDRESSING_RXADDRESSINGFORMATOFRXSDUCFG                            STD_OFF
#define CANTP_EXISTS_MIXED29_ADDRESSING_RXADDRESSINGFORMATOFRXSDUCFG                                STD_OFF
#define CANTP_EXISTS_PHYSICAL_RXTATYPEOFRXSDUCFG                                                    STD_ON
#define CANTP_EXISTS_FUNCTIONAL_RXTATYPEOFRXSDUCFG                                                  STD_ON
#define CANTP_EXISTS_CANFD_PHYSICAL_RXTATYPEOFRXSDUCFG                                              STD_ON
#define CANTP_EXISTS_CANFD_FUNCTIONAL_RXTATYPEOFRXSDUCFG                                            STD_ON
#define CANTP_EXISTS_CAN20_CANTYPEOFTXSDUCFG                                                        STD_ON
#define CANTP_EXISTS_CANFD_CANTYPEOFTXSDUCFG                                                        STD_OFF
#define CANTP_EXISTS_HALF_DUPLEX_CHANNELMODEOFTXSDUCFG                                              STD_OFF
#define CANTP_EXISTS_FULL_DUPLEX_CHANNELMODEOFTXSDUCFG                                              STD_ON
#define CANTP_EXISTS_NORMAL_ADDRESSING_TXADDRESSINGFORMATOFTXSDUCFG                                 STD_ON
#define CANTP_EXISTS_EXTENDED_ADDRESSING_TXADDRESSINGFORMATOFTXSDUCFG                               STD_OFF
#define CANTP_EXISTS_MIXED11_ADDRESSING_TXADDRESSINGFORMATOFTXSDUCFG                                STD_OFF
#define CANTP_EXISTS_NORMALFIXED_ADDRESSING_TXADDRESSINGFORMATOFTXSDUCFG                            STD_OFF
#define CANTP_EXISTS_MIXED29_ADDRESSING_TXADDRESSINGFORMATOFTXSDUCFG                                STD_OFF
#define CANTP_EXISTS_PHYSICAL_TXTATYPEOFTXSDUCFG                                                    STD_ON
#define CANTP_EXISTS_FUNCTIONAL_TXTATYPEOFTXSDUCFG                                                  STD_ON
#define CANTP_EXISTS_CANFD_PHYSICAL_TXTATYPEOFTXSDUCFG                                              STD_ON
#define CANTP_EXISTS_CANFD_FUNCTIONAL_TXTATYPEOFTXSDUCFG                                            STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  CanTpPCEnumDefines  CanTp Enum Defines (PRE_COMPILE)
  \brief  These defines are the enumeration values of enumeration based CONST and VAR data.
  \{
*/ 
#define CANTP_NORMAL_ADDRESSING_ADDRESSINGFORMATOFRXPDUMAP                                          0x00u
#define CANTP_CAN20_CANTYPEOFRXSDUCFG                                                               0x00u
#define CANTP_FULL_DUPLEX_CHANNELMODEOFRXSDUCFG                                                     0x01u
#define CANTP_NORMAL_ADDRESSING_RXADDRESSINGFORMATOFRXSDUCFG                                        0x00u
#define CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG                                                           0x00u
#define CANTP_FUNCTIONAL_RXTATYPEOFRXSDUCFG                                                         0x01u
#define CANTP_CANFD_PHYSICAL_RXTATYPEOFRXSDUCFG                                                     0x00u
#define CANTP_CANFD_FUNCTIONAL_RXTATYPEOFRXSDUCFG                                                   0x01u
#define CANTP_CAN20_CANTYPEOFTXSDUCFG                                                               0x00u
#define CANTP_FULL_DUPLEX_CHANNELMODEOFTXSDUCFG                                                     0x01u
#define CANTP_NORMAL_ADDRESSING_TXADDRESSINGFORMATOFTXSDUCFG                                        0x00u
#define CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG                                                           0x00u
#define CANTP_FUNCTIONAL_TXTATYPEOFTXSDUCFG                                                         0x01u
#define CANTP_CANFD_PHYSICAL_TXTATYPEOFTXSDUCFG                                                     0x00u
#define CANTP_CANFD_FUNCTIONAL_TXTATYPEOFTXSDUCFG                                                   0x01u
/** 
  \}
*/ 

/** 
  \defgroup  CanTpPCIsReducedToDefineDefines  CanTp Is Reduced To Define Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define is STD_ON else STD_OFF.
  \{
*/ 
#define CANTP_ISDEF_ADDRESSINGFORMATOFRXPDUMAP                                                      STD_ON
#define CANTP_ISDEF_GENERICCONNECTIONOFRXPDUMAP                                                     STD_ON
#define CANTP_ISDEF_PDUMETADATALENGTHOFRXPDUMAP                                                     STD_ON
#define CANTP_ISDEF_RXSDUCFGINDENDIDXOFRXPDUMAP                                                     STD_OFF
#define CANTP_ISDEF_RXSDUCFGINDSTARTIDXOFRXPDUMAP                                                   STD_OFF
#define CANTP_ISDEF_RXSDUCFGINDUSEDOFRXPDUMAP                                                       STD_OFF
#define CANTP_ISDEF_TXSDUCFGINDENDIDXOFRXPDUMAP                                                     STD_OFF
#define CANTP_ISDEF_TXSDUCFGINDSTARTIDXOFRXPDUMAP                                                   STD_OFF
#define CANTP_ISDEF_TXSDUCFGINDUSEDOFRXPDUMAP                                                       STD_OFF
#define CANTP_ISDEF_BLOCKSIZEOFRXSDUCFG                                                             STD_ON
#define CANTP_ISDEF_CANTYPEOFRXSDUCFG                                                               STD_ON
#define CANTP_ISDEF_CHANNELMODEOFRXSDUCFG                                                           STD_ON
#define CANTP_ISDEF_GENERICCONNECTIONOFRXSDUCFG                                                     STD_ON
#define CANTP_ISDEF_LOLAYERTXFCPDUIDOFRXSDUCFG                                                      STD_OFF
#define CANTP_ISDEF_NAROFRXSDUCFG                                                                   STD_OFF
#define CANTP_ISDEF_NBROFRXSDUCFG                                                                   STD_OFF
#define CANTP_ISDEF_NCROFRXSDUCFG                                                                   STD_OFF
#define CANTP_ISDEF_PASSSDUMETADATAOFRXSDUCFG                                                       STD_ON
#define CANTP_ISDEF_PDURRXSDUIDOFRXSDUCFG                                                           STD_OFF
#define CANTP_ISDEF_RXADDRESSOFRXSDUCFG                                                             STD_ON
#define CANTP_ISDEF_RXADDRESSINGFORMATOFRXSDUCFG                                                    STD_ON
#define CANTP_ISDEF_RXMAXPAYLOADLENGTHOFRXSDUCFG                                                    STD_ON
#define CANTP_ISDEF_RXPADDINGACTIVATIONOFRXSDUCFG                                                   STD_ON
#define CANTP_ISDEF_RXPDUIDOFRXSDUCFG                                                               STD_OFF
#define CANTP_ISDEF_RXTATYPEOFRXSDUCFG                                                              STD_OFF
#define CANTP_ISDEF_RXWFTMAXOFRXSDUCFG                                                              STD_OFF
#define CANTP_ISDEF_STMINOFRXSDUCFG                                                                 STD_ON
#define CANTP_ISDEF_TXFCADDRESSOFRXSDUCFG                                                           STD_ON
#define CANTP_ISDEF_TXFCPDUCONFIRMATIONPDUIDOFRXSDUCFG                                              STD_OFF
#define CANTP_ISDEF_TXSDUCFGIDXOFRXSDUCFG                                                           STD_OFF
#define CANTP_ISDEF_TXSDUCFGUSEDOFRXSDUCFG                                                          STD_OFF
#define CANTP_ISDEF_RXSDUCFGIND                                                                     STD_OFF
#define CANTP_ISDEF_RXSDUCFGIDXOFRXSDUSNV2HDL                                                       STD_OFF
#define CANTP_ISDEF_RXSDUCFGUSEDOFRXSDUSNV2HDL                                                      STD_ON
#define CANTP_ISDEF_CANTYPEOFTXSDUCFG                                                               STD_ON
#define CANTP_ISDEF_CHANNELMODEOFTXSDUCFG                                                           STD_ON
#define CANTP_ISDEF_LOLAYERTXPDUIDOFTXSDUCFG                                                        STD_OFF
#define CANTP_ISDEF_NASOFTXSDUCFG                                                                   STD_OFF
#define CANTP_ISDEF_NBSOFTXSDUCFG                                                                   STD_OFF
#define CANTP_ISDEF_NCSOFTXSDUCFG                                                                   STD_OFF
#define CANTP_ISDEF_PDURTXSDUIDOFTXSDUCFG                                                           STD_OFF
#define CANTP_ISDEF_RXFCADDRESSOFTXSDUCFG                                                           STD_ON
#define CANTP_ISDEF_RXFCPDUIDOFTXSDUCFG                                                             STD_OFF
#define CANTP_ISDEF_RXSDUCFGIDXOFTXSDUCFG                                                           STD_OFF
#define CANTP_ISDEF_RXSDUCFGUSEDOFTXSDUCFG                                                          STD_OFF
#define CANTP_ISDEF_SDUMETADATALENGTHOFTXSDUCFG                                                     STD_ON
#define CANTP_ISDEF_TRANSMITCANCELLATIONOFTXSDUCFG                                                  STD_OFF
#define CANTP_ISDEF_TXADDRESSOFTXSDUCFG                                                             STD_ON
#define CANTP_ISDEF_TXADDRESSINGFORMATOFTXSDUCFG                                                    STD_ON
#define CANTP_ISDEF_TXMAXPAYLOADLENGTHOFTXSDUCFG                                                    STD_ON
#define CANTP_ISDEF_TXPADDINGACTIVATIONOFTXSDUCFG                                                   STD_ON
#define CANTP_ISDEF_TXPDUCONFIRMATIONPDUIDOFTXSDUCFG                                                STD_OFF
#define CANTP_ISDEF_TXTATYPEOFTXSDUCFG                                                              STD_OFF
#define CANTP_ISDEF_TXSDUCFGIND                                                                     STD_OFF
#define CANTP_ISDEF_TXSDUCFGIDXOFTXSDUSNV2HDL                                                       STD_OFF
#define CANTP_ISDEF_TXSDUCFGUSEDOFTXSDUSNV2HDL                                                      STD_ON
#define CANTP_ISDEF_CALCBSOFPCCONFIG                                                                STD_ON
#define CANTP_ISDEF_RXPDUMAPOFPCCONFIG                                                              STD_ON
#define CANTP_ISDEF_RXSDUCFGINDOFPCCONFIG                                                           STD_ON
#define CANTP_ISDEF_RXSDUCFGOFPCCONFIG                                                              STD_ON
#define CANTP_ISDEF_RXSDUSNV2HDLOFPCCONFIG                                                          STD_ON
#define CANTP_ISDEF_RXSTATEOFPCCONFIG                                                               STD_ON
#define CANTP_ISDEF_TXSDUCFGINDOFPCCONFIG                                                           STD_ON
#define CANTP_ISDEF_TXSDUCFGOFPCCONFIG                                                              STD_ON
#define CANTP_ISDEF_TXSDUSNV2HDLOFPCCONFIG                                                          STD_ON
#define CANTP_ISDEF_TXSEMAPHORESOFPCCONFIG                                                          STD_ON
#define CANTP_ISDEF_TXSTATEOFPCCONFIG                                                               STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  CanTpPCEqualsAlwaysToDefines  CanTp Equals Always To Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define contains the always equals value.
  \{
*/ 
#define CANTP_EQ2_ADDRESSINGFORMATOFRXPDUMAP                                                        CANTP_NORMAL_ADDRESSING_ADDRESSINGFORMATOFRXPDUMAP
#define CANTP_EQ2_GENERICCONNECTIONOFRXPDUMAP                                                       FALSE
#define CANTP_EQ2_PDUMETADATALENGTHOFRXPDUMAP                                                       0u
#define CANTP_EQ2_RXSDUCFGINDENDIDXOFRXPDUMAP                                                       
#define CANTP_EQ2_RXSDUCFGINDSTARTIDXOFRXPDUMAP                                                     
#define CANTP_EQ2_RXSDUCFGINDUSEDOFRXPDUMAP                                                         
#define CANTP_EQ2_TXSDUCFGINDENDIDXOFRXPDUMAP                                                       
#define CANTP_EQ2_TXSDUCFGINDSTARTIDXOFRXPDUMAP                                                     
#define CANTP_EQ2_TXSDUCFGINDUSEDOFRXPDUMAP                                                         
#define CANTP_EQ2_BLOCKSIZEOFRXSDUCFG                                                               0u
#define CANTP_EQ2_CANTYPEOFRXSDUCFG                                                                 CANTP_CAN20_CANTYPEOFRXSDUCFG
#define CANTP_EQ2_CHANNELMODEOFRXSDUCFG                                                             CANTP_FULL_DUPLEX_CHANNELMODEOFRXSDUCFG
#define CANTP_EQ2_GENERICCONNECTIONOFRXSDUCFG                                                       FALSE
#define CANTP_EQ2_LOLAYERTXFCPDUIDOFRXSDUCFG                                                        
#define CANTP_EQ2_NAROFRXSDUCFG                                                                     
#define CANTP_EQ2_NBROFRXSDUCFG                                                                     
#define CANTP_EQ2_NCROFRXSDUCFG                                                                     
#define CANTP_EQ2_PASSSDUMETADATAOFRXSDUCFG                                                         FALSE
#define CANTP_EQ2_PDURRXSDUIDOFRXSDUCFG                                                             
#define CANTP_EQ2_RXADDRESSOFRXSDUCFG                                                               255u
#define CANTP_EQ2_RXADDRESSINGFORMATOFRXSDUCFG                                                      CANTP_NORMAL_ADDRESSING_RXADDRESSINGFORMATOFRXSDUCFG
#define CANTP_EQ2_RXMAXPAYLOADLENGTHOFRXSDUCFG                                                      7u
#define CANTP_EQ2_RXPADDINGACTIVATIONOFRXSDUCFG                                                     TRUE
#define CANTP_EQ2_RXPDUIDOFRXSDUCFG                                                                 
#define CANTP_EQ2_RXTATYPEOFRXSDUCFG                                                                
#define CANTP_EQ2_RXWFTMAXOFRXSDUCFG                                                                
#define CANTP_EQ2_STMINOFRXSDUCFG                                                                   0u
#define CANTP_EQ2_TXFCADDRESSOFRXSDUCFG                                                             255u
#define CANTP_EQ2_TXFCPDUCONFIRMATIONPDUIDOFRXSDUCFG                                                
#define CANTP_EQ2_TXSDUCFGIDXOFRXSDUCFG                                                             
#define CANTP_EQ2_TXSDUCFGUSEDOFRXSDUCFG                                                            
#define CANTP_EQ2_RXSDUCFGIND                                                                       
#define CANTP_EQ2_RXSDUCFGIDXOFRXSDUSNV2HDL                                                         
#define CANTP_EQ2_RXSDUCFGUSEDOFRXSDUSNV2HDL                                                        TRUE
#define CANTP_EQ2_CANTYPEOFTXSDUCFG                                                                 CANTP_CAN20_CANTYPEOFTXSDUCFG
#define CANTP_EQ2_CHANNELMODEOFTXSDUCFG                                                             CANTP_FULL_DUPLEX_CHANNELMODEOFTXSDUCFG
#define CANTP_EQ2_LOLAYERTXPDUIDOFTXSDUCFG                                                          
#define CANTP_EQ2_NASOFTXSDUCFG                                                                     
#define CANTP_EQ2_NBSOFTXSDUCFG                                                                     
#define CANTP_EQ2_NCSOFTXSDUCFG                                                                     
#define CANTP_EQ2_PDURTXSDUIDOFTXSDUCFG                                                             
#define CANTP_EQ2_RXFCADDRESSOFTXSDUCFG                                                             255u
#define CANTP_EQ2_RXFCPDUIDOFTXSDUCFG                                                               
#define CANTP_EQ2_RXSDUCFGIDXOFTXSDUCFG                                                             
#define CANTP_EQ2_RXSDUCFGUSEDOFTXSDUCFG                                                            
#define CANTP_EQ2_SDUMETADATALENGTHOFTXSDUCFG                                                       0u
#define CANTP_EQ2_TRANSMITCANCELLATIONOFTXSDUCFG                                                    
#define CANTP_EQ2_TXADDRESSOFTXSDUCFG                                                               255u
#define CANTP_EQ2_TXADDRESSINGFORMATOFTXSDUCFG                                                      CANTP_NORMAL_ADDRESSING_TXADDRESSINGFORMATOFTXSDUCFG
#define CANTP_EQ2_TXMAXPAYLOADLENGTHOFTXSDUCFG                                                      7u
#define CANTP_EQ2_TXPADDINGACTIVATIONOFTXSDUCFG                                                     TRUE
#define CANTP_EQ2_TXPDUCONFIRMATIONPDUIDOFTXSDUCFG                                                  
#define CANTP_EQ2_TXTATYPEOFTXSDUCFG                                                                
#define CANTP_EQ2_TXSDUCFGIND                                                                       
#define CANTP_EQ2_TXSDUCFGIDXOFTXSDUSNV2HDL                                                         
#define CANTP_EQ2_TXSDUCFGUSEDOFTXSDUSNV2HDL                                                        TRUE
#define CANTP_EQ2_CALCBSOFPCCONFIG                                                                  CanTp_CalcBS
#define CANTP_EQ2_RXPDUMAPOFPCCONFIG                                                                CanTp_RxPduMap
#define CANTP_EQ2_RXSDUCFGINDOFPCCONFIG                                                             CanTp_RxSduCfgInd
#define CANTP_EQ2_RXSDUCFGOFPCCONFIG                                                                CanTp_RxSduCfg
#define CANTP_EQ2_RXSDUSNV2HDLOFPCCONFIG                                                            CanTp_RxSduSnv2Hdl
#define CANTP_EQ2_RXSTATEOFPCCONFIG                                                                 CanTp_RxState
#define CANTP_EQ2_TXSDUCFGINDOFPCCONFIG                                                             CanTp_TxSduCfgInd
#define CANTP_EQ2_TXSDUCFGOFPCCONFIG                                                                CanTp_TxSduCfg
#define CANTP_EQ2_TXSDUSNV2HDLOFPCCONFIG                                                            CanTp_TxSduSnv2Hdl
#define CANTP_EQ2_TXSEMAPHORESOFPCCONFIG                                                            CanTp_TxSemaphores
#define CANTP_EQ2_TXSTATEOFPCCONFIG                                                                 CanTp_TxState
/** 
  \}
*/ 

/** 
  \defgroup  CanTpPCSymbolicInitializationPointers  CanTp Symbolic Initialization Pointers (PRE_COMPILE)
  \brief  Symbolic initialization pointers to be used in the call of a preinit or init function.
  \{
*/ 
#define CanTp_Config_Ptr                                                                            NULL_PTR  /**< symbolic identifier which shall be used to initialize 'CanTp' */
/** 
  \}
*/ 

/** 
  \defgroup  CanTpPCInitializationSymbols  CanTp Initialization Symbols (PRE_COMPILE)
  \brief  Symbolic initialization pointers which may be used in the call of a preinit or init function. Please note, that the defined value can be a 'NULL_PTR' and the address operator is not usable.
  \{
*/ 
#define CanTp_Config                                                                                NULL_PTR  /**< symbolic identifier which could be used to initialize 'CanTp */
/** 
  \}
*/ 

/** 
  \defgroup  CanTpPCGeneral  CanTp General (PRE_COMPILE)
  \brief  General constant defines not associated with a group of defines.
  \{
*/ 
#define CANTP_CHECK_INIT_POINTER                                                                    STD_OFF  /**< STD_ON if the init pointer shall not be used as NULL_PTR and a check shall validate this. */
#define CANTP_FINAL_MAGIC_NUMBER                                                                    0x231Eu  /**< the precompile constant to validate the size of the initialization structure at initialization time of CanTp */
#define CANTP_INDIVIDUAL_POSTBUILD                                                                  STD_OFF  /**< the precompile constant to check, that the module is individual postbuildable. The module 'CanTp' is not configured to be postbuild capable. */
#define CANTP_INIT_DATA                                                                             CANTP_CONST  /**< CompilerMemClassDefine for the initialization data. */
#define CANTP_INIT_DATA_HASH_CODE                                                                   835452236  /**< the precompile constant to validate the initialization structure at initialization time of CanTp with a hashcode. The seed value is '0x231Eu' */
#define CANTP_USE_ECUM_BSW_ERROR_HOOK                                                               STD_OFF  /**< STD_ON if the EcuM_BswErrorHook shall be called in the ConfigPtr check. */
#define CANTP_USE_INIT_POINTER                                                                      STD_OFF  /**< STD_ON if the init pointer CanTp shall be used. */
/** 
  \}
*/ 


/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
/** 
  \defgroup  CanTpPCGetConstantDuplicatedRootDataMacros  CanTp Get Constant Duplicated Root Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated by constance root data elements.
  \{
*/ 
#define CanTp_GetCalcBSOfPCConfig()                                                                 CanTp_CalcBS  /**< the pointer to CanTp_CalcBS */
#define CanTp_GetCompatibilityVersionOfPCConfig()                                                   380999u
#define CanTp_GetRxPduMapOfPCConfig()                                                               CanTp_RxPduMap  /**< the pointer to CanTp_RxPduMap */
#define CanTp_GetRxSduCfgIndOfPCConfig()                                                            CanTp_RxSduCfgInd  /**< the pointer to CanTp_RxSduCfgInd */
#define CanTp_GetRxSduCfgOfPCConfig()                                                               CanTp_RxSduCfg  /**< the pointer to CanTp_RxSduCfg */
#define CanTp_GetRxSduSnv2HdlOfPCConfig()                                                           CanTp_RxSduSnv2Hdl  /**< the pointer to CanTp_RxSduSnv2Hdl */
#define CanTp_GetRxStateOfPCConfig()                                                                CanTp_RxState  /**< the pointer to CanTp_RxState */
#define CanTp_GetSizeOfCalcBSOfPCConfig()                                                           75u  /**< the number of accomplishable value elements in CanTp_CalcBS */
#define CanTp_GetSizeOfRxPduMapOfPCConfig()                                                         96u  /**< the number of accomplishable value elements in CanTp_RxPduMap */
#define CanTp_GetSizeOfRxSduCfgIndOfPCConfig()                                                      75u  /**< the number of accomplishable value elements in CanTp_RxSduCfgInd */
#define CanTp_GetSizeOfRxSduCfgOfPCConfig()                                                         75u  /**< the number of accomplishable value elements in CanTp_RxSduCfg */
#define CanTp_GetSizeOfRxSduSnv2HdlOfPCConfig()                                                     75u  /**< the number of accomplishable value elements in CanTp_RxSduSnv2Hdl */
#define CanTp_GetSizeOfRxStateOfPCConfig()                                                          75u  /**< the number of accomplishable value elements in CanTp_RxState */
#define CanTp_GetSizeOfTxSduCfgIndOfPCConfig()                                                      68u  /**< the number of accomplishable value elements in CanTp_TxSduCfgInd */
#define CanTp_GetSizeOfTxSduCfgOfPCConfig()                                                         72u  /**< the number of accomplishable value elements in CanTp_TxSduCfg */
#define CanTp_GetSizeOfTxSduSnv2HdlOfPCConfig()                                                     72u  /**< the number of accomplishable value elements in CanTp_TxSduSnv2Hdl */
#define CanTp_GetSizeOfTxSemaphoresOfPCConfig()                                                     98u  /**< the number of accomplishable value elements in CanTp_TxSemaphores */
#define CanTp_GetSizeOfTxStateOfPCConfig()                                                          72u  /**< the number of accomplishable value elements in CanTp_TxState */
#define CanTp_GetTxSduCfgIndOfPCConfig()                                                            CanTp_TxSduCfgInd  /**< the pointer to CanTp_TxSduCfgInd */
#define CanTp_GetTxSduCfgOfPCConfig()                                                               CanTp_TxSduCfg  /**< the pointer to CanTp_TxSduCfg */
#define CanTp_GetTxSduSnv2HdlOfPCConfig()                                                           CanTp_TxSduSnv2Hdl  /**< the pointer to CanTp_TxSduSnv2Hdl */
#define CanTp_GetTxSemaphoresOfPCConfig()                                                           CanTp_TxSemaphores  /**< the pointer to CanTp_TxSemaphores */
#define CanTp_GetTxStateOfPCConfig()                                                                CanTp_TxState  /**< the pointer to CanTp_TxState */
/** 
  \}
*/ 

/** 
  \defgroup  CanTpPCGetDataMacros  CanTp Get Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read CONST and VAR data.
  \{
*/ 
#define CanTp_GetCalcBS(Index)                                                                      (CanTp_GetCalcBSOfPCConfig()[(Index)])
#define CanTp_GetRxSduCfgIndEndIdxOfRxPduMap(Index)                                                 (CanTp_GetRxPduMapOfPCConfig()[(Index)].RxSduCfgIndEndIdxOfRxPduMap)
#define CanTp_GetRxSduCfgIndStartIdxOfRxPduMap(Index)                                               (CanTp_GetRxPduMapOfPCConfig()[(Index)].RxSduCfgIndStartIdxOfRxPduMap)
#define CanTp_GetTxSduCfgIndEndIdxOfRxPduMap(Index)                                                 (CanTp_GetRxPduMapOfPCConfig()[(Index)].TxSduCfgIndEndIdxOfRxPduMap)
#define CanTp_GetTxSduCfgIndStartIdxOfRxPduMap(Index)                                               (CanTp_GetRxPduMapOfPCConfig()[(Index)].TxSduCfgIndStartIdxOfRxPduMap)
#define CanTp_GetLoLayerTxFcPduIdOfRxSduCfg(Index)                                                  (CanTp_GetRxSduCfgOfPCConfig()[(Index)].LoLayerTxFcPduIdOfRxSduCfg)
#define CanTp_GetNArOfRxSduCfg(Index)                                                               (CanTp_GetRxSduCfgOfPCConfig()[(Index)].NArOfRxSduCfg)
#define CanTp_GetNBrOfRxSduCfg(Index)                                                               (CanTp_GetRxSduCfgOfPCConfig()[(Index)].NBrOfRxSduCfg)
#define CanTp_GetNCrOfRxSduCfg(Index)                                                               (CanTp_GetRxSduCfgOfPCConfig()[(Index)].NCrOfRxSduCfg)
#define CanTp_GetPduRRxSduIdOfRxSduCfg(Index)                                                       (CanTp_GetRxSduCfgOfPCConfig()[(Index)].PduRRxSduIdOfRxSduCfg)
#define CanTp_GetRxPduIdOfRxSduCfg(Index)                                                           (CanTp_GetRxSduCfgOfPCConfig()[(Index)].RxPduIdOfRxSduCfg)
#define CanTp_GetRxTaTypeOfRxSduCfg(Index)                                                          (CanTp_GetRxSduCfgOfPCConfig()[(Index)].RxTaTypeOfRxSduCfg)
#define CanTp_GetRxWftMaxOfRxSduCfg(Index)                                                          (CanTp_GetRxSduCfgOfPCConfig()[(Index)].RxWftMaxOfRxSduCfg)
#define CanTp_GetTxFcPduConfirmationPduIdOfRxSduCfg(Index)                                          (CanTp_GetRxSduCfgOfPCConfig()[(Index)].TxFcPduConfirmationPduIdOfRxSduCfg)
#define CanTp_GetTxSduCfgIdxOfRxSduCfg(Index)                                                       (CanTp_GetRxSduCfgOfPCConfig()[(Index)].TxSduCfgIdxOfRxSduCfg)
#define CanTp_GetRxSduCfgInd(Index)                                                                 (CanTp_GetRxSduCfgIndOfPCConfig()[(Index)])
#define CanTp_GetRxSduCfgIdxOfRxSduSnv2Hdl(Index)                                                   (CanTp_GetRxSduSnv2HdlOfPCConfig()[(Index)].RxSduCfgIdxOfRxSduSnv2Hdl)
#define CanTp_GetRxState(Index)                                                                     (CanTp_GetRxStateOfPCConfig()[(Index)])
#define CanTp_GetLoLayerTxPduIdOfTxSduCfg(Index)                                                    (CanTp_GetTxSduCfgOfPCConfig()[(Index)].LoLayerTxPduIdOfTxSduCfg)
#define CanTp_GetNAsOfTxSduCfg(Index)                                                               (CanTp_GetTxSduCfgOfPCConfig()[(Index)].NAsOfTxSduCfg)
#define CanTp_GetNBsOfTxSduCfg(Index)                                                               (CanTp_GetTxSduCfgOfPCConfig()[(Index)].NBsOfTxSduCfg)
#define CanTp_GetNCsOfTxSduCfg(Index)                                                               (CanTp_GetTxSduCfgOfPCConfig()[(Index)].NCsOfTxSduCfg)
#define CanTp_GetPduRTxSduIdOfTxSduCfg(Index)                                                       (CanTp_GetTxSduCfgOfPCConfig()[(Index)].PduRTxSduIdOfTxSduCfg)
#define CanTp_GetRxFcPduIdOfTxSduCfg(Index)                                                         (CanTp_GetTxSduCfgOfPCConfig()[(Index)].RxFcPduIdOfTxSduCfg)
#define CanTp_GetRxSduCfgIdxOfTxSduCfg(Index)                                                       (CanTp_GetTxSduCfgOfPCConfig()[(Index)].RxSduCfgIdxOfTxSduCfg)
#define CanTp_IsTransmitCancellationOfTxSduCfg(Index)                                               ((CanTp_GetTxSduCfgOfPCConfig()[(Index)].TransmitCancellationOfTxSduCfg) != FALSE)
#define CanTp_GetTxPduConfirmationPduIdOfTxSduCfg(Index)                                            (CanTp_GetTxSduCfgOfPCConfig()[(Index)].TxPduConfirmationPduIdOfTxSduCfg)
#define CanTp_GetTxTaTypeOfTxSduCfg(Index)                                                          (CanTp_GetTxSduCfgOfPCConfig()[(Index)].TxTaTypeOfTxSduCfg)
#define CanTp_GetTxSduCfgInd(Index)                                                                 (CanTp_GetTxSduCfgIndOfPCConfig()[(Index)])
#define CanTp_GetTxSduCfgIdxOfTxSduSnv2Hdl(Index)                                                   (CanTp_GetTxSduSnv2HdlOfPCConfig()[(Index)].TxSduCfgIdxOfTxSduSnv2Hdl)
#define CanTp_GetTxSemaphores(Index)                                                                (CanTp_GetTxSemaphoresOfPCConfig()[(Index)])
#define CanTp_GetTxState(Index)                                                                     (CanTp_GetTxStateOfPCConfig()[(Index)])
/** 
  \}
*/ 

/** 
  \defgroup  CanTpPCGetDeduplicatedDataMacros  CanTp Get Deduplicated Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated data elements.
  \{
*/ 
#define CanTp_GetCompatibilityVersion()                                                             CanTp_GetCompatibilityVersionOfPCConfig()
#define CanTp_GetAddressingFormatOfRxPduMap(Index)                                                  CANTP_NORMAL_ADDRESSING_ADDRESSINGFORMATOFRXPDUMAP
#define CanTp_IsGenericConnectionOfRxPduMap(Index)                                                  (((FALSE)) != FALSE)
#define CanTp_GetPduMetadataLengthOfRxPduMap(Index)                                                 0u
#define CanTp_IsRxSduCfgIndUsedOfRxPduMap(Index)                                                    (((boolean)(CanTp_GetRxSduCfgIndStartIdxOfRxPduMap(Index) != CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to CanTp_RxSduCfgInd */
#define CanTp_IsTxSduCfgIndUsedOfRxPduMap(Index)                                                    (((boolean)(CanTp_GetTxSduCfgIndStartIdxOfRxPduMap(Index) != CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to CanTp_TxSduCfgInd */
#define CanTp_GetBlockSizeOfRxSduCfg(Index)                                                         0u
#define CanTp_GetCanTypeOfRxSduCfg(Index)                                                           CANTP_CAN20_CANTYPEOFRXSDUCFG
#define CanTp_GetChannelModeOfRxSduCfg(Index)                                                       CANTP_FULL_DUPLEX_CHANNELMODEOFRXSDUCFG
#define CanTp_IsGenericConnectionOfRxSduCfg(Index)                                                  (((FALSE)) != FALSE)
#define CanTp_IsPassSduMetadataOfRxSduCfg(Index)                                                    (((FALSE)) != FALSE)
#define CanTp_GetRxAddressOfRxSduCfg(Index)                                                         255u
#define CanTp_GetRxAddressingFormatOfRxSduCfg(Index)                                                CANTP_NORMAL_ADDRESSING_RXADDRESSINGFORMATOFRXSDUCFG
#define CanTp_GetRxMaxPayloadLengthOfRxSduCfg(Index)                                                7u
#define CanTp_IsRxPaddingActivationOfRxSduCfg(Index)                                                (((TRUE)) != FALSE)
#define CanTp_GetSTminOfRxSduCfg(Index)                                                             0u
#define CanTp_GetTxFcAddressOfRxSduCfg(Index)                                                       255u
#define CanTp_IsTxSduCfgUsedOfRxSduCfg(Index)                                                       (((boolean)(CanTp_GetTxSduCfgIdxOfRxSduCfg(Index) != CANTP_NO_TXSDUCFGIDXOFRXSDUCFG)) != FALSE)  /**< TRUE, if the 0:1 relation has minimum 1 relation pointing to CanTp_TxSduCfg */
#define CanTp_IsRxSduCfgUsedOfRxSduSnv2Hdl(Index)                                                   (((TRUE)) != FALSE)  /**< TRUE, if the 0:1 relation has minimum 1 relation pointing to CanTp_RxSduCfg */
#define CanTp_GetSizeOfCalcBS()                                                                     CanTp_GetSizeOfCalcBSOfPCConfig()
#define CanTp_GetSizeOfRxPduMap()                                                                   CanTp_GetSizeOfRxPduMapOfPCConfig()
#define CanTp_GetSizeOfRxSduCfg()                                                                   CanTp_GetSizeOfRxSduCfgOfPCConfig()
#define CanTp_GetSizeOfRxSduCfgInd()                                                                CanTp_GetSizeOfRxSduCfgIndOfPCConfig()
#define CanTp_GetSizeOfRxSduSnv2Hdl()                                                               CanTp_GetSizeOfRxSduSnv2HdlOfPCConfig()
#define CanTp_GetSizeOfRxState()                                                                    CanTp_GetSizeOfRxStateOfPCConfig()
#define CanTp_GetSizeOfTxSduCfg()                                                                   CanTp_GetSizeOfTxSduCfgOfPCConfig()
#define CanTp_GetSizeOfTxSduCfgInd()                                                                CanTp_GetSizeOfTxSduCfgIndOfPCConfig()
#define CanTp_GetSizeOfTxSduSnv2Hdl()                                                               CanTp_GetSizeOfTxSduSnv2HdlOfPCConfig()
#define CanTp_GetSizeOfTxSemaphores()                                                               CanTp_GetSizeOfTxSemaphoresOfPCConfig()
#define CanTp_GetSizeOfTxState()                                                                    CanTp_GetSizeOfTxStateOfPCConfig()
#define CanTp_GetCanTypeOfTxSduCfg(Index)                                                           CANTP_CAN20_CANTYPEOFTXSDUCFG
#define CanTp_GetChannelModeOfTxSduCfg(Index)                                                       CANTP_FULL_DUPLEX_CHANNELMODEOFTXSDUCFG
#define CanTp_GetRxFcAddressOfTxSduCfg(Index)                                                       255u
#define CanTp_IsRxSduCfgUsedOfTxSduCfg(Index)                                                       (((boolean)(CanTp_GetRxSduCfgIdxOfTxSduCfg(Index) != CANTP_NO_RXSDUCFGIDXOFTXSDUCFG)) != FALSE)  /**< TRUE, if the 0:1 relation has minimum 1 relation pointing to CanTp_RxSduCfg */
#define CanTp_GetSduMetadataLengthOfTxSduCfg(Index)                                                 0u
#define CanTp_GetTxAddressOfTxSduCfg(Index)                                                         255u
#define CanTp_GetTxAddressingFormatOfTxSduCfg(Index)                                                CANTP_NORMAL_ADDRESSING_TXADDRESSINGFORMATOFTXSDUCFG
#define CanTp_GetTxMaxPayloadLengthOfTxSduCfg(Index)                                                7u
#define CanTp_IsTxPaddingActivationOfTxSduCfg(Index)                                                (((TRUE)) != FALSE)
#define CanTp_IsTxSduCfgUsedOfTxSduSnv2Hdl(Index)                                                   (((TRUE)) != FALSE)  /**< TRUE, if the 0:1 relation has minimum 1 relation pointing to CanTp_TxSduCfg */
/** 
  \}
*/ 

/** 
  \defgroup  CanTpPCSetDataMacros  CanTp Set Data Macros (PRE_COMPILE)
  \brief  These macros can be used to write data.
  \{
*/ 
#define CanTp_SetCalcBS(Index, Value)                                                               CanTp_GetCalcBSOfPCConfig()[(Index)] = (Value)
#define CanTp_SetRxState(Index, Value)                                                              CanTp_GetRxStateOfPCConfig()[(Index)] = (Value)
#define CanTp_SetTxSemaphores(Index, Value)                                                         CanTp_GetTxSemaphoresOfPCConfig()[(Index)] = (Value)
#define CanTp_SetTxState(Index, Value)                                                              CanTp_GetTxStateOfPCConfig()[(Index)] = (Value)
/** 
  \}
*/ 

/** 
  \defgroup  CanTpPCHasMacros  CanTp Has Macros (PRE_COMPILE)
  \brief  These macros can be used to detect at runtime a deactivated piece of information. TRUE in the CONFIGURATION_VARIANT PRE-COMPILE, TRUE or FALSE in the CONFIGURATION_VARIANT POST-BUILD.
  \{
*/ 
#define CanTp_HasCalcBS()                                                                           (TRUE != FALSE)
#define CanTp_HasCompatibilityVersion()                                                             (TRUE != FALSE)
#define CanTp_HasRxPduMap()                                                                         (TRUE != FALSE)
#define CanTp_HasAddressingFormatOfRxPduMap()                                                       (TRUE != FALSE)
#define CanTp_HasGenericConnectionOfRxPduMap()                                                      (TRUE != FALSE)
#define CanTp_HasPduMetadataLengthOfRxPduMap()                                                      (TRUE != FALSE)
#define CanTp_HasRxSduCfgIndEndIdxOfRxPduMap()                                                      (TRUE != FALSE)
#define CanTp_HasRxSduCfgIndStartIdxOfRxPduMap()                                                    (TRUE != FALSE)
#define CanTp_HasRxSduCfgIndUsedOfRxPduMap()                                                        (TRUE != FALSE)
#define CanTp_HasTxSduCfgIndEndIdxOfRxPduMap()                                                      (TRUE != FALSE)
#define CanTp_HasTxSduCfgIndStartIdxOfRxPduMap()                                                    (TRUE != FALSE)
#define CanTp_HasTxSduCfgIndUsedOfRxPduMap()                                                        (TRUE != FALSE)
#define CanTp_HasRxSduCfg()                                                                         (TRUE != FALSE)
#define CanTp_HasBlockSizeOfRxSduCfg()                                                              (TRUE != FALSE)
#define CanTp_HasCanTypeOfRxSduCfg()                                                                (TRUE != FALSE)
#define CanTp_HasChannelModeOfRxSduCfg()                                                            (TRUE != FALSE)
#define CanTp_HasGenericConnectionOfRxSduCfg()                                                      (TRUE != FALSE)
#define CanTp_HasLoLayerTxFcPduIdOfRxSduCfg()                                                       (TRUE != FALSE)
#define CanTp_HasNArOfRxSduCfg()                                                                    (TRUE != FALSE)
#define CanTp_HasNBrOfRxSduCfg()                                                                    (TRUE != FALSE)
#define CanTp_HasNCrOfRxSduCfg()                                                                    (TRUE != FALSE)
#define CanTp_HasPassSduMetadataOfRxSduCfg()                                                        (TRUE != FALSE)
#define CanTp_HasPduRRxSduIdOfRxSduCfg()                                                            (TRUE != FALSE)
#define CanTp_HasRxAddressOfRxSduCfg()                                                              (TRUE != FALSE)
#define CanTp_HasRxAddressingFormatOfRxSduCfg()                                                     (TRUE != FALSE)
#define CanTp_HasRxMaxPayloadLengthOfRxSduCfg()                                                     (TRUE != FALSE)
#define CanTp_HasRxPaddingActivationOfRxSduCfg()                                                    (TRUE != FALSE)
#define CanTp_HasRxPduIdOfRxSduCfg()                                                                (TRUE != FALSE)
#define CanTp_HasRxTaTypeOfRxSduCfg()                                                               (TRUE != FALSE)
#define CanTp_HasRxWftMaxOfRxSduCfg()                                                               (TRUE != FALSE)
#define CanTp_HasSTminOfRxSduCfg()                                                                  (TRUE != FALSE)
#define CanTp_HasTxFcAddressOfRxSduCfg()                                                            (TRUE != FALSE)
#define CanTp_HasTxFcPduConfirmationPduIdOfRxSduCfg()                                               (TRUE != FALSE)
#define CanTp_HasTxSduCfgIdxOfRxSduCfg()                                                            (TRUE != FALSE)
#define CanTp_HasTxSduCfgUsedOfRxSduCfg()                                                           (TRUE != FALSE)
#define CanTp_HasRxSduCfgInd()                                                                      (TRUE != FALSE)
#define CanTp_HasRxSduSnv2Hdl()                                                                     (TRUE != FALSE)
#define CanTp_HasRxSduCfgIdxOfRxSduSnv2Hdl()                                                        (TRUE != FALSE)
#define CanTp_HasRxSduCfgUsedOfRxSduSnv2Hdl()                                                       (TRUE != FALSE)
#define CanTp_HasRxState()                                                                          (TRUE != FALSE)
#define CanTp_HasSizeOfCalcBS()                                                                     (TRUE != FALSE)
#define CanTp_HasSizeOfRxPduMap()                                                                   (TRUE != FALSE)
#define CanTp_HasSizeOfRxSduCfg()                                                                   (TRUE != FALSE)
#define CanTp_HasSizeOfRxSduCfgInd()                                                                (TRUE != FALSE)
#define CanTp_HasSizeOfRxSduSnv2Hdl()                                                               (TRUE != FALSE)
#define CanTp_HasSizeOfRxState()                                                                    (TRUE != FALSE)
#define CanTp_HasSizeOfTxSduCfg()                                                                   (TRUE != FALSE)
#define CanTp_HasSizeOfTxSduCfgInd()                                                                (TRUE != FALSE)
#define CanTp_HasSizeOfTxSduSnv2Hdl()                                                               (TRUE != FALSE)
#define CanTp_HasSizeOfTxSemaphores()                                                               (TRUE != FALSE)
#define CanTp_HasSizeOfTxState()                                                                    (TRUE != FALSE)
#define CanTp_HasTxSduCfg()                                                                         (TRUE != FALSE)
#define CanTp_HasCanTypeOfTxSduCfg()                                                                (TRUE != FALSE)
#define CanTp_HasChannelModeOfTxSduCfg()                                                            (TRUE != FALSE)
#define CanTp_HasLoLayerTxPduIdOfTxSduCfg()                                                         (TRUE != FALSE)
#define CanTp_HasNAsOfTxSduCfg()                                                                    (TRUE != FALSE)
#define CanTp_HasNBsOfTxSduCfg()                                                                    (TRUE != FALSE)
#define CanTp_HasNCsOfTxSduCfg()                                                                    (TRUE != FALSE)
#define CanTp_HasPduRTxSduIdOfTxSduCfg()                                                            (TRUE != FALSE)
#define CanTp_HasRxFcAddressOfTxSduCfg()                                                            (TRUE != FALSE)
#define CanTp_HasRxFcPduIdOfTxSduCfg()                                                              (TRUE != FALSE)
#define CanTp_HasRxSduCfgIdxOfTxSduCfg()                                                            (TRUE != FALSE)
#define CanTp_HasRxSduCfgUsedOfTxSduCfg()                                                           (TRUE != FALSE)
#define CanTp_HasSduMetadataLengthOfTxSduCfg()                                                      (TRUE != FALSE)
#define CanTp_HasTransmitCancellationOfTxSduCfg()                                                   (TRUE != FALSE)
#define CanTp_HasTxAddressOfTxSduCfg()                                                              (TRUE != FALSE)
#define CanTp_HasTxAddressingFormatOfTxSduCfg()                                                     (TRUE != FALSE)
#define CanTp_HasTxMaxPayloadLengthOfTxSduCfg()                                                     (TRUE != FALSE)
#define CanTp_HasTxPaddingActivationOfTxSduCfg()                                                    (TRUE != FALSE)
#define CanTp_HasTxPduConfirmationPduIdOfTxSduCfg()                                                 (TRUE != FALSE)
#define CanTp_HasTxTaTypeOfTxSduCfg()                                                               (TRUE != FALSE)
#define CanTp_HasTxSduCfgInd()                                                                      (TRUE != FALSE)
#define CanTp_HasTxSduSnv2Hdl()                                                                     (TRUE != FALSE)
#define CanTp_HasTxSduCfgIdxOfTxSduSnv2Hdl()                                                        (TRUE != FALSE)
#define CanTp_HasTxSduCfgUsedOfTxSduSnv2Hdl()                                                       (TRUE != FALSE)
#define CanTp_HasTxSemaphores()                                                                     (TRUE != FALSE)
#define CanTp_HasTxState()                                                                          (TRUE != FALSE)
#define CanTp_HasPCConfig()                                                                         (TRUE != FALSE)
#define CanTp_HasCalcBSOfPCConfig()                                                                 (TRUE != FALSE)
#define CanTp_HasCompatibilityVersionOfPCConfig()                                                   (TRUE != FALSE)
#define CanTp_HasRxPduMapOfPCConfig()                                                               (TRUE != FALSE)
#define CanTp_HasRxSduCfgIndOfPCConfig()                                                            (TRUE != FALSE)
#define CanTp_HasRxSduCfgOfPCConfig()                                                               (TRUE != FALSE)
#define CanTp_HasRxSduSnv2HdlOfPCConfig()                                                           (TRUE != FALSE)
#define CanTp_HasRxStateOfPCConfig()                                                                (TRUE != FALSE)
#define CanTp_HasSizeOfCalcBSOfPCConfig()                                                           (TRUE != FALSE)
#define CanTp_HasSizeOfRxPduMapOfPCConfig()                                                         (TRUE != FALSE)
#define CanTp_HasSizeOfRxSduCfgIndOfPCConfig()                                                      (TRUE != FALSE)
#define CanTp_HasSizeOfRxSduCfgOfPCConfig()                                                         (TRUE != FALSE)
#define CanTp_HasSizeOfRxSduSnv2HdlOfPCConfig()                                                     (TRUE != FALSE)
#define CanTp_HasSizeOfRxStateOfPCConfig()                                                          (TRUE != FALSE)
#define CanTp_HasSizeOfTxSduCfgIndOfPCConfig()                                                      (TRUE != FALSE)
#define CanTp_HasSizeOfTxSduCfgOfPCConfig()                                                         (TRUE != FALSE)
#define CanTp_HasSizeOfTxSduSnv2HdlOfPCConfig()                                                     (TRUE != FALSE)
#define CanTp_HasSizeOfTxSemaphoresOfPCConfig()                                                     (TRUE != FALSE)
#define CanTp_HasSizeOfTxStateOfPCConfig()                                                          (TRUE != FALSE)
#define CanTp_HasTxSduCfgIndOfPCConfig()                                                            (TRUE != FALSE)
#define CanTp_HasTxSduCfgOfPCConfig()                                                               (TRUE != FALSE)
#define CanTp_HasTxSduSnv2HdlOfPCConfig()                                                           (TRUE != FALSE)
#define CanTp_HasTxSemaphoresOfPCConfig()                                                           (TRUE != FALSE)
#define CanTp_HasTxStateOfPCConfig()                                                                (TRUE != FALSE)
/** 
  \}
*/ 

/** 
  \defgroup  CanTpPCIncrementDataMacros  CanTp Increment Data Macros (PRE_COMPILE)
  \brief  These macros can be used to increment VAR data with numerical nature.
  \{
*/ 
#define CanTp_IncCalcBS(Index)                                                                      CanTp_GetCalcBS(Index)++
#define CanTp_IncRxState(Index)                                                                     CanTp_GetRxState(Index)++
#define CanTp_IncTxSemaphores(Index)                                                                CanTp_GetTxSemaphores(Index)++
#define CanTp_IncTxState(Index)                                                                     CanTp_GetTxState(Index)++
/** 
  \}
*/ 

/** 
  \defgroup  CanTpPCDecrementDataMacros  CanTp Decrement Data Macros (PRE_COMPILE)
  \brief  These macros can be used to decrement VAR data with numerical nature.
  \{
*/ 
#define CanTp_DecCalcBS(Index)                                                                      CanTp_GetCalcBS(Index)--
#define CanTp_DecRxState(Index)                                                                     CanTp_GetRxState(Index)--
#define CanTp_DecTxSemaphores(Index)                                                                CanTp_GetTxSemaphores(Index)--
#define CanTp_DecTxState(Index)                                                                     CanTp_GetTxState(Index)--
/** 
  \}
*/ 

  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  CanTpLTDataSwitches  CanTp Data Switches  (LINK)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define CANTP_LTCONFIG                                                                              STD_OFF  /**< Deactivateable: 'CanTp_LTConfig' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
/** 
  \}
*/ 


/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  CanTpPBDataSwitches  CanTp Data Switches  (POST_BUILD)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define CANTP_PBCONFIG                                                                              STD_OFF  /**< Deactivateable: 'CanTp_PBConfig' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define CANTP_LTCONFIGIDXOFPBCONFIG                                                                 STD_OFF  /**< Deactivateable: 'CanTp_PBConfig.LTConfigIdx' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define CANTP_PCCONFIGIDXOFPBCONFIG                                                                 STD_OFF  /**< Deactivateable: 'CanTp_PBConfig.PCConfigIdx' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
/** 
  \}
*/ 


/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/


#endif /* CANTP_CFG_H */
