/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: CanNm
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: CanNm_Cfg.c
 *   Generation Time: 2020-08-20 13:43:06
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/


#define CANNM_CFG_SOURCE

/**********************************************************************************************************************
 * MISRA JUSTIFICATION
 *********************************************************************************************************************/
/* PRQA S 0779 EOF */ /* MD_MSR_Rule5.2_0779 */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "CanNm_Cfg.h"

#include "PduR_Cfg.h"

#include "CanIf_Cfg.h"

#include "Nm.h"

/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/


/**********************************************************************************************************************
  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CanNm_ChannelConfig
**********************************************************************************************************************/
/** 
  \var    CanNm_ChannelConfig
  \details
  Element               Description
  TimeoutTime           Timeout for the NM messages [ms].
  WaitBusSleepTime      Timeout for bus calm down phase [ms].
  NodeIdEnabled         Determines if Node Ids are enabled or not
  ChannelId             Channel ID configured for the respective instance of the NM.
  MsgCycleTime          Period of a NM message [ms]. It determines the periodic rate in the periodic transmission mode.
  MsgTimeoutTime        Transmission Timeout [ms] of NM message. If there is no transmission confirmation by the CAN Interface within this timeout, the CAN NM module shall give an error notification.
  PduNidPosition        Node ID Position in the PDU
  RemoteSleepIndTime    Timeout for Remote Sleep Indication [ms].
  RepeatMessageTime     Timeout for Repeat Message State [ms].
*/ 
#define CANNM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanNm_ChannelConfigType, CANNM_CONST) CanNm_ChannelConfig[3] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    TimeoutTime  WaitBusSleepTime  NodeIdEnabled  ChannelId                                          MsgCycleTime  MsgTimeoutTime  PduNidPosition    RemoteSleepIndTime  RepeatMessageTime        Referable Keys */
  { /*     0 */       1000u,            1001u,          TRUE,      NmConf_NmChannelConfig_CN_Backbone2_78967e2c,          40u,            12u, CANNM_PDU_BYTE_0,                 0u,               60u },  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone2_78967e2c] */
  { /*     1 */       1000u,            1001u,          TRUE,      NmConf_NmChannelConfig_CN_CabSubnet_9ea693f1,          40u,            12u, CANNM_PDU_BYTE_0,                 0u,               60u },  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_CabSubnet_9ea693f1] */
  { /*     2 */         12u,            1001u,          TRUE, NmConf_NmChannelConfig_CN_SecuritySubnet_e7a0ee54,           4u,             3u, CANNM_PDU_BYTE_0,                 0u,               12u }   /* [/ActiveEcuC/ComM/ComMConfigSet/CN_SecuritySubnet_e7a0ee54] */
};
#define CANNM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_PbChannelConfig
**********************************************************************************************************************/
/** 
  \var    CanNm_PbChannelConfig
  \details
  Element                  Description
  MsgCycleOffset       
  NodeId               
  RxMessageDataEndIdx      the end index of the 0:n relation pointing to CanNm_RxMessageData
  RxMessageDataLength      the number of relations pointing to CanNm_RxMessageData
  RxMessageDataStartIdx    the start index of the 0:n relation pointing to CanNm_RxMessageData
  RxMessageData_NIDIdx     the index of the 0:1 relation pointing to CanNm_RxMessageData
  TxMessageDataEndIdx      the end index of the 0:n relation pointing to CanNm_TxMessageData
  TxMessageDataLength      the number of relations pointing to CanNm_TxMessageData
  TxMessageDataStartIdx    the start index of the 0:n relation pointing to CanNm_TxMessageData
  TxMessageData_NIDIdx     the index of the 0:1 relation pointing to CanNm_TxMessageData
  TxPduId              
*/ 
#define CANNM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanNm_PbChannelConfigType, CANNM_CONST) CanNm_PbChannelConfig[3] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    MsgCycleOffset  NodeId  RxMessageDataEndIdx  RxMessageDataLength  RxMessageDataStartIdx  RxMessageData_NIDIdx  TxMessageDataEndIdx  TxMessageDataLength  TxMessageDataStartIdx  TxMessageData_NIDIdx  TxPduId                                                                   Referable Keys */
  { /*     0 */             0u,    64u,                  1u,                  1u,                    0u,                   0u,                  1u,                  1u,                    0u,                   0u,      CanIfConf_CanIfTxPduCfg_NmAsr_CIOM_BB2_oBackbone2_9b0338b4_Tx },  /* [/ActiveEcuC/CanNm/CanNmGlobalConfig/Backbone2_CAN_f911ddd5] */
  { /*     1 */             0u,    64u,                  2u,                  1u,                    1u,                   1u,                  2u,                  1u,                    1u,                   1u,      CanIfConf_CanIfTxPduCfg_NmAsr_CIOM_Cab_oCabSubnet_153d3c7d_Tx },  /* [/ActiveEcuC/CanNm/CanNmGlobalConfig/CabSubnet_CAN_1a3a8edc] */
  { /*     2 */             0u,    64u,                  3u,                  1u,                    2u,                   2u,                  3u,                  1u,                    2u,                   2u, CanIfConf_CanIfTxPduCfg_NmAsr_CIOM_Sec_oSecuritySubnet_dae88b7d_Tx }   /* [/ActiveEcuC/CanNm/CanNmGlobalConfig/SecuritySubnet_CAN_c9b1ce65] */
};
#define CANNM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_SysToNmChInd
**********************************************************************************************************************/
/** 
  \var    CanNm_SysToNmChInd
  \brief  Channel indirection: System Channel handle to NM channel handle
*/ 
#define CANNM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanNm_SysToNmChIndType, CANNM_CONST) CanNm_SysToNmChInd[14] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     SysToNmChInd           */
  /*     0 */                     0u,
  /*     1 */  CANNM_NO_SYSTONMCHIND,
  /*     2 */                     1u,
  /*     3 */                     2u,
  /*     4 */  CANNM_NO_SYSTONMCHIND,
  /*     5 */  CANNM_NO_SYSTONMCHIND,
  /*     6 */  CANNM_NO_SYSTONMCHIND,
  /*     7 */  CANNM_NO_SYSTONMCHIND,
  /*     8 */  CANNM_NO_SYSTONMCHIND,
  /*     9 */  CANNM_NO_SYSTONMCHIND,
  /*    10 */  CANNM_NO_SYSTONMCHIND,
  /*    11 */  CANNM_NO_SYSTONMCHIND,
  /*    12 */  CANNM_NO_SYSTONMCHIND,
  /*    13 */  CANNM_NO_SYSTONMCHIND
};
#define CANNM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_CommState
**********************************************************************************************************************/
/** 
  \var    CanNm_CommState
  \brief  Internal state for the application's need for communication.
*/ 
#define CANNM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanNm_CommStateUType, CANNM_VAR_NOINIT) CanNm_CommState;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone2_78967e2c] */
  /*     1 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_CabSubnet_9ea693f1] */
  /*     2 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_SecuritySubnet_e7a0ee54] */

#define CANNM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_CoordReadyToSleepState
**********************************************************************************************************************/
/** 
  \var    CanNm_CoordReadyToSleepState
  \brief  State for Rx of Coordinator Sleep Ready Bit:
*/ 
#define CANNM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanNm_CoordReadyToSleepStateUType, CANNM_VAR_NOINIT) CanNm_CoordReadyToSleepState;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone2_78967e2c] */
  /*     1 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_CabSubnet_9ea693f1] */
  /*     2 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_SecuritySubnet_e7a0ee54] */

#define CANNM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_MsgConfirmationFlag
**********************************************************************************************************************/
/** 
  \var    CanNm_MsgConfirmationFlag
  \brief  Message Confirmation Flag
*/ 
#define CANNM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanNm_MsgConfirmationFlagUType, CANNM_VAR_NOINIT) CanNm_MsgConfirmationFlag;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone2_78967e2c] */
  /*     1 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_CabSubnet_9ea693f1] */
  /*     2 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_SecuritySubnet_e7a0ee54] */

#define CANNM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_MsgIndicationFlag
**********************************************************************************************************************/
/** 
  \var    CanNm_MsgIndicationFlag
  \brief  Message Indication Flag
*/ 
#define CANNM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanNm_MsgIndicationFlagUType, CANNM_VAR_NOINIT) CanNm_MsgIndicationFlag;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone2_78967e2c] */
  /*     1 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_CabSubnet_9ea693f1] */
  /*     2 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_SecuritySubnet_e7a0ee54] */

#define CANNM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_MsgTimeoutTimer
**********************************************************************************************************************/
#define CANNM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanNm_MsgTimeoutTimerUType, CANNM_VAR_NOINIT) CanNm_MsgTimeoutTimer;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone2_78967e2c] */
  /*     1 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_CabSubnet_9ea693f1] */
  /*     2 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_SecuritySubnet_e7a0ee54] */

#define CANNM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_MsgTimer
**********************************************************************************************************************/
/** 
  \var    CanNm_MsgTimer
  \brief  Timer for NM message transmission.
*/ 
#define CANNM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanNm_MsgTimerUType, CANNM_VAR_NOINIT) CanNm_MsgTimer;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone2_78967e2c] */
  /*     1 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_CabSubnet_9ea693f1] */
  /*     2 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_SecuritySubnet_e7a0ee54] */

#define CANNM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_NetworkRestartFlag
**********************************************************************************************************************/
#define CANNM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanNm_NetworkRestartFlagUType, CANNM_VAR_NOINIT) CanNm_NetworkRestartFlag;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone2_78967e2c] */
  /*     1 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_CabSubnet_9ea693f1] */
  /*     2 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_SecuritySubnet_e7a0ee54] */

#define CANNM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_NmState
**********************************************************************************************************************/
/** 
  \var    CanNm_NmState
  \brief  Current state of the state machine
*/ 
#define CANNM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanNm_NmStateUType, CANNM_VAR_NOINIT) CanNm_NmState;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone2_78967e2c] */
  /*     1 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_CabSubnet_9ea693f1] */
  /*     2 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_SecuritySubnet_e7a0ee54] */

#define CANNM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_RepeatMsgTimer
**********************************************************************************************************************/
#define CANNM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanNm_RepeatMsgTimerUType, CANNM_VAR_NOINIT) CanNm_RepeatMsgTimer;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone2_78967e2c] */
  /*     1 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_CabSubnet_9ea693f1] */
  /*     2 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_SecuritySubnet_e7a0ee54] */

#define CANNM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_RxMessageData
**********************************************************************************************************************/
#define CANNM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanNm_RxMessageDataType, CANNM_VAR_NOINIT) CanNm_RxMessageData[3];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/CanNm/CanNmGlobalConfig/Backbone2_CAN_f911ddd5, /ActiveEcuC/CanNm/CanNmGlobalConfig/Backbone2_CAN_f911ddd5_NID] */
  /*     1 */  /* [/ActiveEcuC/CanNm/CanNmGlobalConfig/CabSubnet_CAN_1a3a8edc, /ActiveEcuC/CanNm/CanNmGlobalConfig/CabSubnet_CAN_1a3a8edc_NID] */
  /*     2 */  /* [/ActiveEcuC/CanNm/CanNmGlobalConfig/SecuritySubnet_CAN_c9b1ce65, /ActiveEcuC/CanNm/CanNmGlobalConfig/SecuritySubnet_CAN_c9b1ce65_NID] */

#define CANNM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_TimeoutTimer
**********************************************************************************************************************/
/** 
  \var    CanNm_TimeoutTimer
  \brief  Timer for NM Algorithm.
*/ 
#define CANNM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanNm_TimeoutTimerUType, CANNM_VAR_NOINIT) CanNm_TimeoutTimer;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone2_78967e2c] */
  /*     1 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_CabSubnet_9ea693f1] */
  /*     2 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_SecuritySubnet_e7a0ee54] */

#define CANNM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_TxControlState
**********************************************************************************************************************/
/** 
  \var    CanNm_TxControlState
  \brief  Message transmission control state.
*/ 
#define CANNM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanNm_TxControlStateUType, CANNM_VAR_NOINIT) CanNm_TxControlState;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone2_78967e2c] */
  /*     1 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_CabSubnet_9ea693f1] */
  /*     2 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_SecuritySubnet_e7a0ee54] */

#define CANNM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_TxControlStateRequest
**********************************************************************************************************************/
/** 
  \var    CanNm_TxControlStateRequest
  \brief  Message transmission control state request.
*/ 
#define CANNM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanNm_TxControlStateRequestUType, CANNM_VAR_NOINIT) CanNm_TxControlStateRequest;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone2_78967e2c] */
  /*     1 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_CabSubnet_9ea693f1] */
  /*     2 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_SecuritySubnet_e7a0ee54] */

#define CANNM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_TxMessageData
**********************************************************************************************************************/
#define CANNM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanNm_TxMessageDataType, CANNM_VAR_NOINIT) CanNm_TxMessageData[3];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/CanNm/CanNmGlobalConfig/Backbone2_CAN_f911ddd5, /ActiveEcuC/CanNm/CanNmGlobalConfig/Backbone2_CAN_f911ddd5_NID] */
  /*     1 */  /* [/ActiveEcuC/CanNm/CanNmGlobalConfig/CabSubnet_CAN_1a3a8edc, /ActiveEcuC/CanNm/CanNmGlobalConfig/CabSubnet_CAN_1a3a8edc_NID] */
  /*     2 */  /* [/ActiveEcuC/CanNm/CanNmGlobalConfig/SecuritySubnet_CAN_c9b1ce65, /ActiveEcuC/CanNm/CanNmGlobalConfig/SecuritySubnet_CAN_c9b1ce65_NID] */

#define CANNM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanNm_WaitBusSleepTimer
**********************************************************************************************************************/
#define CANNM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanNm_WaitBusSleepTimerUType, CANNM_VAR_NOINIT) CanNm_WaitBusSleepTimer;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone2_78967e2c] */
  /*     1 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_CabSubnet_9ea693f1] */
  /*     2 */  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_SecuritySubnet_e7a0ee54] */

#define CANNM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */



/**********************************************************************************************************************
  END OF FILE: CanNm_Cfg.c
**********************************************************************************************************************/

