/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: CanSM
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: CanSM_Lcfg.c
 *   Generation Time: 2020-08-20 13:43:06
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/




#define CANSM_LCFG_C


 /**********************************************************************************************************************
 *  Includes
 **********************************************************************************************************************/

#include "CanSM_EcuM.h"



/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CanSM_ChannelConfig
**********************************************************************************************************************/
/** 
  \var    CanSM_ChannelConfig
  \details
  Element                             Description
  ProdErrorDetect_ModeRequest_    
  Trcv_InActive_AtAll_NonPNCannels    Config feature which determines if Trvc is active at a Non PN channel
  ControllerId                    
  NetworkHandle                   
  TransceiverId                   
  DemEventId_BusOff_              
  DemEventId_ModeRequest_         
*/ 
#define CANSM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanSM_ChannelConfigType, CANSM_CONST) CanSM_ChannelConfig[6] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ProdErrorDetect_ModeRequest_  Trcv_InActive_AtAll_NonPNCannels  ControllerId  NetworkHandle  TransceiverId                          DemEventId_BusOff_                                                                                       DemEventId_ModeRequest_                                                                                              */
  { /*     0 */                         TRUE,                            FALSE,           1u,            0u,                                    0u, DemConf_DemEventParameter_AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_Backbone2     , DemConf_DemEventParameter_AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_Backbone2      },
  { /*     1 */                         TRUE,                             TRUE,           4u,            5u, CANSM_NO_TRANSCEIVERIDOFCHANNELCONFIG, DemConf_DemEventParameter_AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_FMSNet        , DemConf_DemEventParameter_AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_FMSNet         },
  { /*     2 */                         TRUE,                             TRUE,           0u,            4u, CANSM_NO_TRANSCEIVERIDOFCHANNELCONFIG, DemConf_DemEventParameter_AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_Backbone1     , DemConf_DemEventParameter_AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_Backbone1      },
  { /*     3 */                         TRUE,                            FALSE,           5u,            3u,                                    2u, DemConf_DemEventParameter_AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_SecuritySubnet, DemConf_DemEventParameter_AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_SecuritySubnet },
  { /*     4 */                         TRUE,                            FALSE,           3u,            2u,                                    1u, DemConf_DemEventParameter_AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_CabSubnet     , DemConf_DemEventParameter_AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_CabSubnet      },
  { /*     5 */                        FALSE,                             TRUE,           2u,            1u, CANSM_NO_TRANSCEIVERIDOFCHANNELCONFIG, FALSE                                                                                                  , FALSE                                                                                                                }
};
#define CANSM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanSM_ChannelVarRecord
**********************************************************************************************************************/
#define CANSM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanSM_ChannelVarRecordType, CANSM_VAR_NOINIT) CanSM_ChannelVarRecord[6];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define CANSM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */



/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/


/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTIONS
**********************************************************************************************************************/




 /**********************************************************************************************************************
 *  VAR DATA PROTOTYPES
 **********************************************************************************************************************/






/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 **********************************************************************************************************************/

 /* -----------------------------------------------
 BusOffIndicationFct  Begin
 ----------------------------------------------- */

#define CANSM_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_MemMap */

CONST(CanSM_BusOffBeginIndicationFctPtrType, CANSM_CONST) CanSM_BusOffBeginIndicationFctPtr = J1939Nm_GetBusOffDelay;  /* PRQA S 1533 */ /* MD_CANSM_Rule8.9 */

#define CANSM_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_MemMap */


 /* -----------------------------------------------
 BusOffIndicationFct  End
 ----------------------------------------------- */

#define CANSM_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_MemMap */

CONST(CanSM_BusOffEndIndicationFctPtrType, CANSM_CONST) CanSM_BusOffEndIndicationFctPtr = J1939Nm_BusOffEnd;   /* PRQA S 1505 */ /* MD_MSR_Rule8.7 */

#define CANSM_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_MemMap */





/* -----------------------------------------------------------------------------
    CanSM Functions
 ----------------------------------------------------------------------------- */

#define CANSM_START_SEC_CODE
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_MemMap */

FUNC(void, CANSM_CODE) CanSM_Dem_ReportErrorEvent(Dem_EventIdType CanSM_EventID, Dem_EventStatusType CanSM_EventStatus)
{
  (void)Dem_ReportErrorStatus( CanSM_EventID, CanSM_EventStatus );
}

#define CANSM_STOP_SEC_CODE
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_MemMap */


