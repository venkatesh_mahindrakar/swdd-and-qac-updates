/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Fee
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Fee_Lcfg.c
 *   Generation Time: 2020-08-20 13:43:06
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.65 SP5
 *
 *
 *********************************************************************************************************************/


/* macro identifying this compilation unit */
#define FEE_LCFG_C_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Fee_InitEx.h"
#include "Fee_Int.h"
#include "Fee_PartitionDefs.h"

/**********************************************************************************************************************
 * VERSION IDENTIFICATION
 *********************************************************************************************************************/
# define FEE_CFG_C_MAJOR_VERSION                                        (8u)
# define FEE_CFG_C_MINOR_VERSION                                        (4u)
# define FEE_CFG_C_PATCH_VERSION                                        (0u)

/**********************************************************************************************************************
 *  VERSION CHECK
 *********************************************************************************************************************/
#if (  (FEE_INTERNAL_MAJOR_VERSION != FEE_CFG_C_MAJOR_VERSION) \
    || (FEE_INTERNAL_MINOR_VERSION != FEE_CFG_C_MINOR_VERSION) )
# error "Vendor specific version numbers of Fee_Lcfg.c and Fee_Int.h are inconsistent"
#endif

#if (  (FEE_CFG_MAJOR_VERSION != FEE_CFG_C_MAJOR_VERSION) \
    || (FEE_CFG_MINOR_VERSION != FEE_CFG_C_MINOR_VERSION) )
# error "Version numbers of Fee_Lcfg.c and Fee_Cfg.h are inconsistent!"
#endif

#if (  (FEE_PRIVATE_CFG_MAJOR_VERSION != FEE_CFG_C_MAJOR_VERSION) \
    || (FEE_PRIVATE_CFG_MINOR_VERSION != FEE_CFG_C_MINOR_VERSION))
# error "Version numbers of Fee_Lcfg.c and Fee_PrivateCfg.h are inconsistent!"
#endif

#define FEE_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h"      /* PRQA S 5087 */ /* MD_MSR_19.1 */


static VAR(struct Fee_PartitionsRamDataStruct, FEE_VAR_NOINIT) Fee_PartitionRamData[FEE_NUMBER_OF_PARTITIONS];
#define FEE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h"      /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 *  GLOBAL DATA CONSTANT
 *********************************************************************************************************************/
#define FEE_START_SEC_APPL_CONFIG_UNSPECIFIED
#include "MemMap.h"     /* PRQA S 5087 */ /* MD_MSR_19.1 */

  /*<FEE_BLOCK_CONFIGURATION>*/
CONST(struct Fee_BlockConfigStruct, FEE_APPL_CONFIG) Fee_BlockConfig_at[] =
{
  { /*  Block: FeeApplication_Data_NVM_EngTraceHW_NvM  */ 
    16u /*  index of the block in the linktable  */ , 
    322u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeDemPrimaryDataBlock1  */ 
    3u /*  index of the block in the linktable  */ , 
    50u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartition_DEM_DET_Logging /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeDemPrimaryDataBlock2  */ 
    4u /*  index of the block in the linktable  */ , 
    50u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartition_DEM_DET_Logging /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeDemPrimaryDataBlock3  */ 
    5u /*  index of the block in the linktable  */ , 
    50u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartition_DEM_DET_Logging /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeDemPrimaryDataBlock4  */ 
    6u /*  index of the block in the linktable  */ , 
    50u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartition_DEM_DET_Logging /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeDemPrimaryDataBlock5  */ 
    7u /*  index of the block in the linktable  */ , 
    50u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartition_DEM_DET_Logging /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeDemPrimaryDataBlock6  */ 
    8u /*  index of the block in the linktable  */ , 
    50u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartition_DEM_DET_Logging /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeDemPrimaryDataBlock7  */ 
    9u /*  index of the block in the linktable  */ , 
    50u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartition_DEM_DET_Logging /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeDemPrimaryDataBlock8  */ 
    10u /*  index of the block in the linktable  */ , 
    50u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartition_DEM_DET_Logging /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeDemPrimaryDataBlock9  */ 
    11u /*  index of the block in the linktable  */ , 
    50u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartition_DEM_DET_Logging /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeCalibration_Data_NVM_FSP_NVM  */ 
    15u /*  index of the block in the linktable  */ , 
    30u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeApplication_Data_NVM_FSP_NVM  */ 
    2u /*  index of the block in the linktable  */ , 
    30u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeApplication_Data_NVM_DriverAuth2_Ctrl_NVM  */ 
    3u /*  index of the block in the linktable  */ , 
    6u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeApplication_Data_NVM_InCabLock_HMICtrl_NVM  */ 
    4u /*  index of the block in the linktable  */ , 
    6u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeApplication_Data_NVM_VehicleAccess_Ctrl_NVM  */ 
    5u /*  index of the block in the linktable  */ , 
    6u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeApplication_Data_NVM_InteriorLights_HMICtrl_NVM  */ 
    6u /*  index of the block in the linktable  */ , 
    6u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeApplication_Data_NVM_MUT_UICtrl_Traction_NVM  */ 
    7u /*  index of the block in the linktable  */ , 
    6u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeApplication_Data_NVM_RGW_HMICtrl_NVM  */ 
    8u /*  index of the block in the linktable  */ , 
    6u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeApplication_Data_NVM_SCM_HMICtrl_NVM  */ 
    9u /*  index of the block in the linktable  */ , 
    18u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeApplication_Data_NVM_VehicleModeDistribution_NVM  */ 
    10u /*  index of the block in the linktable  */ , 
    6u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeApplication_Data_NVM_VSC_UICtrl_NVM  */ 
    11u /*  index of the block in the linktable  */ , 
    6u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeApplication_Data_NVM_PinCode_ctrl_NVM  */ 
    12u /*  index of the block in the linktable  */ , 
    6u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeApplication_Data_NVM_MUT_UICtrl_Difflock_NVM  */ 
    13u /*  index of the block in the linktable  */ , 
    6u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeKeyfon_Radio_Com_NVM_KeyFobNVBlock  */ 
    14u /*  index of the block in the linktable  */ , 
    96u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeSCIM_KeyFobFSP_NVM_KeyFobNVBlock  */ 
    0u /*  index of the block in the linktable  */ , 
    96u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeSCIM_KeyFobFSP_NVM_FspNVBlock  */ 
    1u /*  index of the block in the linktable  */ , 
    28u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionApp /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeDemPrimaryDataBlock0  */ 
    2u /*  index of the block in the linktable  */ , 
    50u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartition_DEM_DET_Logging /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeDemStatusDataBlock  */ 
    1u /*  index of the block in the linktable  */ , 
    876u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartition_DEM_DET_Logging /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeDemAdminDataBlock  */ 
    0u /*  index of the block in the linktable  */ , 
    12u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartition_DEM_DET_Logging /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeFblBootFingerprint  */ 
    1u /*  index of the block in the linktable  */ , 
    19u /*  payload length  */ , 
    5u /*  number of datasets  */ , 
    FeePartitionSharePbl_MSW /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeFblVpmState  */ 
    11u /*  index of the block in the linktable  */ , 
    7u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionSharePbl_MSW /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeFblValidityFlags  */ 
    9u /*  index of the block in the linktable  */ , 
    3u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionSharePbl_MSW /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeFblSigValue  */ 
    7u /*  index of the block in the linktable  */ , 
    130u /*  payload length  */ , 
    3u /*  number of datasets  */ , 
    FeePartitionSharePbl_MSW /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeFblSigInfo  */ 
    5u /*  index of the block in the linktable  */ , 
    12u /*  payload length  */ , 
    3u /*  number of datasets  */ , 
    FeePartitionSharePbl_MSW /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeFblFingerprintNumber  */ 
    3u /*  index of the block in the linktable  */ , 
    5u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionSharePbl_MSW /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeFblDataFingerprint  */ 
    2u /*  index of the block in the linktable  */ , 
    19u /*  payload length  */ , 
    5u /*  number of datasets  */ , 
    FeePartitionSharePbl_MSW /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeFblVpmValidityFlagsCopy  */ 
    12u /*  index of the block in the linktable  */ , 
    3u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionSharePbl_MSW /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeFblResetFlags  */ 
    4u /*  index of the block in the linktable  */ , 
    7u /*  payload length  */ , 
    1u /*  number of datasets  */ , 
    FeePartitionSharePbl_MSW /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeFblSigSegments  */ 
    6u /*  index of the block in the linktable  */ , 
    10u /*  payload length  */ , 
    48u /*  number of datasets  */ , 
    FeePartitionSharePbl_MSW /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeFblSwFingerprint  */ 
    8u /*  index of the block in the linktable  */ , 
    19u /*  payload length  */ , 
    5u /*  number of datasets  */ , 
    FeePartitionSharePbl_MSW /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeFblVpmPatch  */ 
    10u /*  index of the block in the linktable  */ , 
    32u /*  payload length  */ , 
    8u /*  number of datasets  */ , 
    FeePartitionSharePbl_MSW /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }, 
  { /*  Block: FeeConfigBlock  */ 
    0u /*  index of the block in the linktable  */ , 
    4u /*  payload length  */ , 
    2u /*  number of datasets  */ , 
    FeePartitionSharePbl_MSW /*  partition where the block resides in  */ , 
    1u /*  the exponent of the number of instances per chunk (2^n)-1  */ , 
    FALSE /*  immediate data  */ , 
    FALSE /*  critical data  */ , 
    FALSE /*  look up table block  */ , 
    0u /*  base index of the block in the LookUpTable  */ 
  }
};
/*</FEE_BLOCK_CONFIGURATION>*/


/* Describe the partitions */
static CONST(struct Fee_PartitionConfigStruct, FEE_APPL_CONFIG) Fee_PartitionConfig_at[FEE_NUMBER_OF_PARTITIONS] =
{
  {
    &Fee_PartitionRamData[0], 
    {
      26528uL /*  FSS threshold  */ , 
      22432uL /*  BSS threshold  */ 
    }, 
    { /*  AssociatedSectors_at  */ 
      { /*  LowerSector  */ 
        0xFFFFFFFFuL /*  Fls Erase Value  */ , 
        { /*  physical sector  */ 
          0x00008000uL /*  physical sector start address  */ , 
          0x00008000uL /*  physical sector size  */ 
        }, 
        { /*  configured sector  */ 
          0x00008000uL /*  configured sector start address  */ , 
          0x00008000uL /*  configured sector size  */ 
        }, 
        { /*  alignments  */ 
          3u /*  write alignment  */ , 
          5u /*  address alignment  */ 
        }
      }, 
      { /*  UpperSector  */ 
        0xFFFFFFFFuL /*  Fls Erase Value  */ , 
        { /*  physical sector  */ 
          0x00010000uL /*  physical sector start address  */ , 
          0x00008000uL /*  physical sector size  */ 
        }, 
        { /*  configured sector  */ 
          0x00010000uL /*  configured sector start address  */ , 
          0x00008000uL /*  configured sector size  */ 
        }, 
        { /*  alignments  */ 
          3u /*  write alignment  */ , 
          5u /*  address alignment  */ 
        }
      }
    }, 
    (17u), 
    0u, 
    FALSE /*  No LookUpTable configured for this partition  */ , 
    0u /*  Index of LookUpTable configuration  */ 
  } /*  ramData_pt  */ , 
  {
    &Fee_PartitionRamData[1], 
    {
      2336uL /*  FSS threshold  */ , 
      160uL /*  BSS threshold  */ 
    }, 
    { /*  AssociatedSectors_at  */ 
      { /*  LowerSector  */ 
        0xFFFFFFFFuL /*  Fls Erase Value  */ , 
        { /*  physical sector  */ 
          0x00000000uL /*  physical sector start address  */ , 
          0x00004000uL /*  physical sector size  */ 
        }, 
        { /*  configured sector  */ 
          0x00000000uL /*  configured sector start address  */ , 
          0x00004000uL /*  configured sector size  */ 
        }, 
        { /*  alignments  */ 
          3u /*  write alignment  */ , 
          5u /*  address alignment  */ 
        }
      }, 
      { /*  UpperSector  */ 
        0xFFFFFFFFuL /*  Fls Erase Value  */ , 
        { /*  physical sector  */ 
          0x00004000uL /*  physical sector start address  */ , 
          0x00004000uL /*  physical sector size  */ 
        }, 
        { /*  configured sector  */ 
          0x00004000uL /*  configured sector start address  */ , 
          0x00004000uL /*  configured sector size  */ 
        }, 
        { /*  alignments  */ 
          3u /*  write alignment  */ , 
          5u /*  address alignment  */ 
        }
      }
    }, 
    (13u), 
    1u, 
    FALSE /*  No LookUpTable configured for this partition  */ , 
    0u /*  Index of LookUpTable configuration  */ 
  } /*  ramData_pt  */ , 
  {
    &Fee_PartitionRamData[2], 
    {
      59104uL /*  FSS threshold  */ , 
      55008uL /*  BSS threshold  */ 
    }, 
    { /*  AssociatedSectors_at  */ 
      { /*  LowerSector  */ 
        0xFFFFFFFFuL /*  Fls Erase Value  */ , 
        { /*  physical sector  */ 
          0x00018000uL /*  physical sector start address  */ , 
          0x00010000uL /*  physical sector size  */ 
        }, 
        { /*  configured sector  */ 
          0x00018000uL /*  configured sector start address  */ , 
          0x00010000uL /*  configured sector size  */ 
        }, 
        { /*  alignments  */ 
          3u /*  write alignment  */ , 
          5u /*  address alignment  */ 
        }
      }, 
      { /*  UpperSector  */ 
        0xFFFFFFFFuL /*  Fls Erase Value  */ , 
        { /*  physical sector  */ 
          0x00028000uL /*  physical sector start address  */ , 
          0x00010000uL /*  physical sector size  */ 
        }, 
        { /*  configured sector  */ 
          0x00028000uL /*  configured sector start address  */ , 
          0x00010000uL /*  configured sector size  */ 
        }, 
        { /*  alignments  */ 
          3u /*  write alignment  */ , 
          5u /*  address alignment  */ 
        }
      }
    }, 
    (12u), 
    2u, 
    FALSE /*  No LookUpTable configured for this partition  */ , 
    0u /*  Index of LookUpTable configuration  */ 
  } /*  ramData_pt  */ 
};

CONST(struct Fee_ConfigStruct, FEE_APPL_CONFIG) Fee_Config = /* PRQA S 1533 */ /* MD_FEE_8.9_fee_config_struct */
{
    Fee_BlockConfig_at,
    Fee_PartitionConfig_at,
    (0x002Au /*  number of blocks  */ ),
    FEE_NUMBER_OF_PARTITIONS,
    (6u /*  dataset selection bits  */ ),
    (FALSE /*  fbl config  */ )
};



#define FEE_STOP_SEC_APPL_CONFIG_UNSPECIFIED
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define FEE_START_SEC_CODE
#include "MemMap.h"      /* PRQA S 5087 */ /* MD_MSR_19.1 */

FUNC(Fee_ErrorCallbackReturnType, FEE_APPL_CODE) Fee_ErrorCallbackNotification(uint8 partitionId, Fee_SectorErrorType err) /* PRQA S 3206 */ /* MD_FEE_2.7_Parameter_Used_For_Other_Configs*/
{
#if (STD_ON == FEE_USE_APPL_ERROR_CALLBACK)
     return  Appl_FeeCriticalErrorCallback(partitionId, err);
#else
     /* check partitionId -> avoid waning about unused parameter */
     FEE_DUMMY_STATEMENT(partitionId);
     return (Fee_ErrorCallbackReturnType)(
                        (err != FEE_SECTOR_FORMAT_FAILED) ? FEE_ERRCBK_RESOLVE_AUTOMATICALLY : FEE_ERRCBK_REJECT_WRITE);
#endif
}

#define FEE_STOP_SEC_CODE
#include "MemMap.h"      /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *  END OF FILE: Fee_Lcfg.c
 *********************************************************************************************************************/
 

