/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TheftAlarm_HMI2_ctrl.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  TheftAlarm_HMI2_ctrl
 *  Generation Time:  2020-09-21 13:44:23
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <TheftAlarm_HMI2_ctrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   Boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * SEWS_TheftAlarmRequestDuration_X1CY8_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_TheftAlarm_HMI2_ctrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_TheftAlarmRequestDuration_X1CY8_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * AlarmStatus_stat_T: Enumeration of integer in interval [0...15] with enumerators
 *   AlarmStatus_stat_Idle (0U)
 *   AlarmStatus_stat_SetMode (1U)
 *   AlarmStatus_stat_ReduceSetMode (2U)
 *   AlarmStatus_stat_UnsetMode (3U)
 *   AlarmStatus_stat_PanicMode (4U)
 *   AlarmStatus_stat_AlarmMode (5U)
 *   AlarmStatus_stat_ServiceMode (6U)
 *   AlarmStatus_stat_TamperMode (7U)
 *   AlarmStatus_stat_Spare_01 (8U)
 *   AlarmStatus_stat_Spare_02 (9U)
 *   AlarmStatus_stat_Spare_03 (10U)
 *   AlarmStatus_stat_Spare_04 (11U)
 *   AlarmStatus_stat_Spare_05 (12U)
 *   AlarmStatus_stat_Spare_06 (13U)
 *   AlarmStatus_stat_Error (14U)
 *   AlarmStatus_stat_NotAvailable (15U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * KeyAuthentication_stat_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyAuthentication_stat_decrypt_KeyNotAuthenticated (0U)
 *   KeyAuthentication_stat_decrypt_KeyAuthenticated (1U)
 *   KeyAuthentication_stat_decrypt_Spare1 (2U)
 *   KeyAuthentication_stat_decrypt_Spare2 (3U)
 *   KeyAuthentication_stat_decrypt_Spare3 (4U)
 *   KeyAuthentication_stat_decrypt_Spare4 (5U)
 *   KeyAuthentication_stat_decrypt_Error (6U)
 *   KeyAuthentication_stat_decrypt_NotAvailable (7U)
 * KeyPosition_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyPosition_KeyOut (0U)
 *   KeyPosition_IgnitionKeyInOffPosition (1U)
 *   KeyPosition_IgnitionKeyInAccessoryPosition (2U)
 *   KeyPosition_IgnitionKeyIn15PositionNormalDrivingPosition (3U)
 *   KeyPosition_IgnitionKeyInPreheatPosition (4U)
 *   KeyPosition_IgnitionKeyInCrankPosition (5U)
 *   KeyPosition_ErrorIndicator (6U)
 *   KeyPosition_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * ReducedSetMode_rqst_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 *   ReducedSetMode_rqst_decrypt_Idle (0U)
 *   ReducedSetMode_rqst_decrypt_ReducedSetModeRequested (1U)
 *   ReducedSetMode_rqst_decrypt_ReducedSetModeNOT_Requested (2U)
 *   ReducedSetMode_rqst_decrypt_Spare1 (3U)
 *   ReducedSetMode_rqst_decrypt_Spare2 (4U)
 *   ReducedSetMode_rqst_decrypt_Spare3 (5U)
 *   ReducedSetMode_rqst_decrypt_Error (6U)
 *   ReducedSetMode_rqst_decrypt_NotAvailable (7U)
 * TheftAlarmAct_rqst_decrypt_I: Enumeration of integer in interval [0...7] with enumerators
 *   TheftAlarmAct_rqst_decrypt_Idle (0U)
 *   TheftAlarmAct_rqst_decrypt_TheftAlarmActReqstFromKeyfob (1U)
 *   TheftAlarmAct_rqst_decrypt_TheftAlarmDeactReqstFromKeyfob (2U)
 *   TheftAlarmAct_rqst_decrypt_TheftAlarmActivationRequestFromOtherSource (3U)
 *   TheftAlarmAct_rqst_decrypt_TheftAlarmDeactReqstFromOtherSource (4U)
 *   TheftAlarmAct_rqst_decrypt_Spare (5U)
 *   TheftAlarmAct_rqst_decrypt_Error (6U)
 *   TheftAlarmAct_rqst_decrypt_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Crypto_Function_serialized_T: Array with 12 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_TheftAlarmRequestDuration_X1CY8_T Rte_Prm_X1CY8_TheftAlarmRequestDuration_v(void)
 *   boolean Rte_Prm_P1B2T_AlarmInstalled_v(void)
 *
 *********************************************************************************************************************/


#define TheftAlarm_HMI2_ctrl_START_SEC_CODE
#include "TheftAlarm_HMI2_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: TheftAlarm_HMI2_ctrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AlarmStatus_stat_AlarmStatus_stat(AlarmStatus_stat_T *data)
 *   Std_ReturnType Rte_Read_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt(KeyAuthentication_stat_decrypt_T *data)
 *   Std_ReturnType Rte_Read_KeyPosition_KeyPosition(KeyPosition_T *data)
 *   Std_ReturnType Rte_Read_KeyfobLockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_KeyfobSuperLockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_KeyfobUnlockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_ReducedSetModeButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Parked_Parked(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ReducedSetMode_DevInd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_ReducedSetMode_rqst_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_ReducedSetMode_rqst_decrypt_ReducedSetMode_rqst_decrypt(ReducedSetMode_rqst_decrypt_T data)
 *   Std_ReturnType Rte_Write_ReducedSetMode_rqst_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_TheftAlarmAct_rqst_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_TheftAlarmAct_rqst_decrypt_TheftAlarmAct_rqst_decrypt(TheftAlarmAct_rqst_decrypt_I data)
 *   Std_ReturnType Rte_Write_TheftAlarmAct_rqst_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: TheftAlarm_HMI2_ctrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, TheftAlarm_HMI2_ctrl_CODE) TheftAlarm_HMI2_ctrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: TheftAlarm_HMI2_ctrl_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define TheftAlarm_HMI2_ctrl_STOP_SEC_CODE
#include "TheftAlarm_HMI2_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
