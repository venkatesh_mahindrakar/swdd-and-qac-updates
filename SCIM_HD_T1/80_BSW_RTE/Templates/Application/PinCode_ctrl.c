/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  PinCode_ctrl.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  PinCode_ctrl
 *  Generation Time:  2020-09-21 13:44:23
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <PinCode_ctrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_PinCode_ctrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Code32bit_T: Integer in interval [0...4294967295]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * PinCode_validity_time_T: Integer in interval [0...255]
 *   Unit: [h], Factor: 1, Offset: 1
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DynamicCode_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   DynamicCode_rqst_Idle (0U)
 *   DynamicCode_rqst_DynamicCodeRequest (1U)
 *   DynamicCode_rqst_Error (6U)
 *   DynamicCode_rqst_NotAvailable (7U)
 * PinCode_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   PinCode_rqst_Idle (0U)
 *   PinCode_rqst_PINCodeNotNeeded (1U)
 *   PinCode_rqst_StatusOfThePinCodeRequested (2U)
 *   PinCode_rqst_PinCodeNeeded (3U)
 *   PinCode_rqst_ResetPinCodeStatus (4U)
 *   PinCode_rqst_Spare (5U)
 *   PinCode_rqst_Error (6U)
 *   PinCode_rqst_NotAvailable (7U)
 * PinCode_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   PinCode_stat_Idle (0U)
 *   PinCode_stat_NoPinCodeEntered (1U)
 *   PinCode_stat_WrongPinCode (2U)
 *   PinCode_stat_GoodPinCode (3U)
 *   PinCode_stat_Error (6U)
 *   PinCode_stat_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 * Array Types:
 * ============
 * StandardNVM_T: Array with 4 element(s) of type uint8
 *
 *********************************************************************************************************************/


#define PinCode_ctrl_START_SEC_CODE
#include "PinCode_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: PinCode_ctrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_DynamicCode_rqst_DynamicCode_rqst(DynamicCode_rqst_T *data)
 *   Std_ReturnType Rte_Read_PinCodeEntered_value_PinCodeEntered_value(Code32bit_T *data)
 *   Std_ReturnType Rte_Read_PinCode_ctrl_NVM_I_PinCode_ctrl_NVM_I(uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Read_PinCode_rqst_PinCode_rqst(PinCode_rqst_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DynamicCode_value_DynamicCode_value(Code32bit_T data)
 *   Std_ReturnType Rte_Write_PinCode_ctrl_NVM_I_PinCode_ctrl_NVM_I(const uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Write_PinCode_stat_PinCode_stat(PinCode_stat_T data)
 *   Std_ReturnType Rte_Write_PinCode_validity_time_PinCode_validity_time(PinCode_validity_time_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: PinCode_ctrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PinCode_ctrl_CODE) PinCode_ctrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: PinCode_ctrl_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define PinCode_ctrl_STOP_SEC_CODE
#include "PinCode_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
