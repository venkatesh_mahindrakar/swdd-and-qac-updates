/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  LKS_HMICtrl.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  LKS_HMICtrl
 *  Generation Time:  2020-09-21 13:44:22
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <LKS_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_LKS_SwType_P1R5P_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_LKS_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_LKS_SwType_P1R5P_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DualDeviceIndication_T: Enumeration of integer in interval [0...15] with enumerators
 *   DualDeviceIndication_UpperOffLowerOff (0U)
 *   DualDeviceIndication_UpperOnLowerOff (1U)
 *   DualDeviceIndication_UpperBlinkLowerOff (2U)
 *   DualDeviceIndication_UpperDontCareLowerOff (3U)
 *   DualDeviceIndication_UpperOffLowerOn (4U)
 *   DualDeviceIndication_UpperOnLowerOn (5U)
 *   DualDeviceIndication_UpperBlinkLowerOn (6U)
 *   DualDeviceIndication_UpperDontCareLowerOn (7U)
 *   DualDeviceIndication_UpperOffLowerBlink (8U)
 *   DualDeviceIndication_UpperOnLowerBlink (9U)
 *   DualDeviceIndication_UpperBlinkLowerBlink (10U)
 *   DualDeviceIndication_UpperDontCareLowerBlink (11U)
 *   DualDeviceIndication_UpperOffLowerDontCare (12U)
 *   DualDeviceIndication_UpperOnLowerDontCare (13U)
 *   DualDeviceIndication_UpperBlinkLowerDontCare (14U)
 *   DualDeviceIndication_UpperDontCareLowerDontCare (15U)
 * LKSCorrectiveSteeringStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   LKSCorrectiveSteeringStatus_Off (0U)
 *   LKSCorrectiveSteeringStatus_Available (1U)
 *   LKSCorrectiveSteeringStatus_Steering (2U)
 *   LKSCorrectiveSteeringStatus_DriverTakeOverRqst (3U)
 *   LKSCorrectiveSteeringStatus_TempUnavailable (4U)
 *   LKSCorrectiveSteeringStatus_Spare2 (5U)
 *   LKSCorrectiveSteeringStatus_Error (6U)
 *   LKSCorrectiveSteeringStatus_NotAvailable (7U)
 * LKSStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   LKSStatus_OFF (0U)
 *   LKSStatus_INACTIVE (1U)
 *   LKSStatus_ACTIVE (2U)
 *   LKSStatus_Spare (3U)
 *   LKSStatus_Spare_01 (4U)
 *   LKSStatus_Spare_02 (5U)
 *   LKSStatus_Error (6U)
 *   LKSStatus_NotAvailable (7U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_LKS_SwType_P1R5P_T Rte_Prm_P1R5P_LKS_SwType_v(void)
 *   boolean Rte_Prm_P1BKI_LKS_Installed_v(void)
 *   boolean Rte_Prm_P1NQD_LKS_SwIndicationType_v(void)
 *
 *********************************************************************************************************************/


#define LKS_HMICtrl_START_SEC_CODE
#include "LKS_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: LKS_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_LKSApplicationStatus_LKSApplicationStatus(LKSStatus_T *data)
 *   Std_ReturnType Rte_Read_LKSCS_SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LKSCorrectiveSteeringStatus_LKSCorrectiveSteeringStatus(LKSCorrectiveSteeringStatus_T *data)
 *   Std_ReturnType Rte_Read_LKSPushButton_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LKS_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_LKSCSEnableSwitch_rqst_LKSEnableSwitch_rqst(OffOn_T data)
 *   Std_ReturnType Rte_Write_LKSCS_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LKSEnableSwitch_rqst_LKSEnableSwitch_rqst(OffOn_T data)
 *   Std_ReturnType Rte_Write_LKS_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: LKS_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LKS_HMICtrl_CODE) LKS_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: LKS_HMICtrl_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define LKS_HMICtrl_STOP_SEC_CODE
#include "LKS_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
