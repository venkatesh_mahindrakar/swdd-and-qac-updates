/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  LevelControl_HMICtrl.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  LevelControl_HMICtrl
 *  Generation Time:  2020-09-21 13:44:22
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <LevelControl_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_ECSActiveStateTimeout_P1CUE_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_ECSStandbyActivationTimeout_P1CUA_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_ECSStandbyExtendedActTimeout_P1CUB_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_ECS_StandbyBlinkTime_P1GCL_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FPBRSwitchRequestACKTime_P1LXR_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FerryFuncSwStuckedTimeout_P1EXK_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FrontSuspensionType_P1JBR_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_KneelButtonStuckedTimeout_P1DWD_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_LevelControl_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_ECSActiveStateTimeout_P1CUE_T: Integer in interval [0...65535]
 * SEWS_ECSStandbyActivationTimeout_P1CUA_T: Integer in interval [0...65535]
 * SEWS_ECSStandbyExtendedActTimeout_P1CUB_T: Integer in interval [0...65535]
 * SEWS_ECS_StandbyBlinkTime_P1GCL_T: Integer in interval [0...255]
 * SEWS_FPBRSwitchRequestACKTime_P1LXR_T: Integer in interval [0...255]
 * SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T: Integer in interval [0...255]
 * SEWS_FerryFuncSwStuckedTimeout_P1EXK_T: Integer in interval [0...255]
 * SEWS_FrontSuspensionType_P1JBR_T: Integer in interval [0...255]
 * SEWS_KneelButtonStuckedTimeout_P1DWD_T: Integer in interval [0...255]
 * SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T: Integer in interval [0...255]
 * SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * Ack2Bit_T: Enumeration of integer in interval [0...3] with enumerators
 *   Ack2Bit_NoAction (0U)
 *   Ack2Bit_ChangeAcknowledged (1U)
 *   Ack2Bit_Error (2U)
 *   Ack2Bit_NotAvailable (3U)
 * BackToDriveReqACK_T: Enumeration of integer in interval [0...3] with enumerators
 *   BackToDriveReqACK_TakNoAction (0U)
 *   BackToDriveReqACK_ChangeAcknowledged (1U)
 *   BackToDriveReqACK_Error (2U)
 *   BackToDriveReqACK_NotAvailable (3U)
 * BackToDriveReq_T: Enumeration of integer in interval [0...3] with enumerators
 *   BackToDriveReq_Idle (0U)
 *   BackToDriveReq_B2DRequested (1U)
 *   Requested_Error (2U)
 *   Requested_NotAvailable (3U)
 * ChangeKneelACK_T: Enumeration of integer in interval [0...3] with enumerators
 *   ChangeKneelACK_NoAction (0U)
 *   ChangeKneelACK_ChangeAcknowledged (1U)
 *   ChangeKneelACK_Error (2U)
 *   ChangeKneelACK_NotAvailable (3U)
 * ChangeRequest2Bit_T: Enumeration of integer in interval [0...3] with enumerators
 *   ChangeRequest2Bit_TakeNoAction (0U)
 *   ChangeRequest2Bit_Change (1U)
 *   ChangeRequest2Bit_Error (2U)
 *   ChangeRequest2Bit_NotAvailable (3U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * ECSStandByReq_T: Enumeration of integer in interval [0...7] with enumerators
 *   ECSStandByReq_Idle (0U)
 *   ECSStandByReq_StandbyRequested (1U)
 *   ECSStandByReq_StopStandby (2U)
 *   ECSStandByReq_Reserved (3U)
 *   ECSStandByReq_Reserved_01 (4U)
 *   ECSStandByReq_Reserved_02 (5U)
 *   ECSStandByReq_Error (6U)
 *   ECSStandByReq_NotAvailable (7U)
 * ECSStandByRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   ECSStandByRequest_NoRequest (0U)
 *   ECSStandByRequest_Initiate (1U)
 *   ECSStandByRequest_StandbyRequestedRCECS (2U)
 *   ECSStandByRequest_StandbyRequestedWRC (3U)
 *   ECSStandByRequest_Reserved (4U)
 *   ECSStandByRequest_Reserved_01 (5U)
 *   ECSStandByRequest_Error (6U)
 *   ECSStandByRequest_NotAvailable (7U)
 * ElectricalLoadReduction_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   ElectricalLoadReduction_rqst_NoRequest (0U)
 *   ElectricalLoadReduction_rqst_Level1Request (1U)
 *   ElectricalLoadReduction_rqst_Level2Request (2U)
 *   ElectricalLoadReduction_rqst_SpareValue (3U)
 *   ElectricalLoadReduction_rqst_SpareValue_01 (4U)
 *   ElectricalLoadReduction_rqst_SpareValue_02 (5U)
 *   ElectricalLoadReduction_rqst_Error (6U)
 *   ElectricalLoadReduction_rqst_NotAvailable (7U)
 * FPBRChangeReq_T: Enumeration of integer in interval [0...3] with enumerators
 *   FPBRChangeReq_TakeNoAction (0U)
 *   FPBRChangeReq_ChangeFPBRFunction (1U)
 *   FPBRChangeReq_Error (2U)
 *   FPBRChangeReq_NotAvailable (3U)
 * FPBRStatusInd_T: Enumeration of integer in interval [0...3] with enumerators
 *   FPBRStatusInd_Off (0U)
 *   FPBRStatusInd_On (1U)
 *   FPBRStatusInd_Changing (2U)
 *   FPBRStatusInd_NotAvailable (3U)
 * FalseTrue_T: Enumeration of integer in interval [0...3] with enumerators
 *   FalseTrue_False (0U)
 *   FalseTrue_True (1U)
 *   FalseTrue_Error (2U)
 *   FalseTrue_NotAvaiable (3U)
 * FerryFunctionStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   FerryFunctionStatus_FerryFunctionNotActive (0U)
 *   FerryFunctionStatus_FerryFunctionActiveLevelReached (1U)
 *   FerryFunctionStatus_FerryFunctionActiveLeveNotlReached (2U)
 *   FerryFunctionStatus_Spare_01 (3U)
 *   FerryFunctionStatus_Spare_02 (4U)
 *   FerryFunctionStatus_Spare_03 (5U)
 *   FerryFunctionStatus_Error (6U)
 *   FerryFunctionStatus_NotAvailable (7U)
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   InactiveActive_Inactive (0U)
 *   InactiveActive_Active (1U)
 *   InactiveActive_Error (2U)
 *   InactiveActive_NotAvailable (3U)
 * KneelingChangeRequest_T: Enumeration of integer in interval [0...3] with enumerators
 *   KneelingChangeRequest_TakeNoAction (0U)
 *   KneelingChangeRequest_ChangeKneelFunction (1U)
 *   KneelingChangeRequest_Error (2U)
 *   KneelingChangeRequest_NotAvailable (3U)
 * KneelingStatusHMI_T: Enumeration of integer in interval [0...3] with enumerators
 *   KneelingStatusHMI_Inactive (0U)
 *   KneelingStatusHMI_Active (1U)
 *   KneelingStatusHMI_Error (2U)
 *   KneelingStatusHMI_NotAvailable (3U)
 * LevelAdjustmentAction_T: Enumeration of integer in interval [0...15] with enumerators
 *   LevelAdjustmentAction_Idle (0U)
 *   LevelAdjustmentAction_UpBasic (1U)
 *   LevelAdjustmentAction_DownBasic (2U)
 *   LevelAdjustmentAction_UpShortMovement (3U)
 *   LevelAdjustmentAction_DownShortMovement (4U)
 *   LevelAdjustmentAction_Reserved (5U)
 *   LevelAdjustmentAction_GotoDriveLevel (6U)
 *   LevelAdjustmentAction_Reserved_01 (7U)
 *   LevelAdjustmentAction_Ferry (8U)
 *   LevelAdjustmentAction_Reserved_02 (9U)
 *   LevelAdjustmentAction_Reserved_03 (10U)
 *   LevelAdjustmentAction_Reserved_04 (11U)
 *   LevelAdjustmentAction_Reserved_05 (12U)
 *   LevelAdjustmentAction_Reserved_06 (13U)
 *   LevelAdjustmentAction_Error (14U)
 *   LevelAdjustmentAction_NotAvailable (15U)
 * LevelAdjustmentAxles_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelAdjustmentAxles_Rear (0U)
 *   LevelAdjustmentAxles_Front (1U)
 *   LevelAdjustmentAxles_Parallel (2U)
 *   LevelAdjustmentAxles_Reserved (3U)
 *   LevelAdjustmentAxles_Reserved_01 (4U)
 *   LevelAdjustmentAxles_Reserved_02 (5U)
 *   LevelAdjustmentAxles_Error (6U)
 *   LevelAdjustmentAxles_NotAvailable (7U)
 * LevelAdjustmentStroke_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelAdjustmentStroke_DriveStroke (0U)
 *   LevelAdjustmentStroke_DockingStroke (1U)
 *   LevelAdjustmentStroke_Reserved (2U)
 *   LevelAdjustmentStroke_Reserved_01 (3U)
 *   LevelAdjustmentStroke_Reserved_02 (4U)
 *   LevelAdjustmentStroke_Reserved_03 (5U)
 *   LevelAdjustmentStroke_Error (6U)
 *   LevelAdjustmentStroke_NotAvailable (7U)
 * LevelChangeRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelChangeRequest_TakeNoAction (0U)
 *   LevelChangeRequest_VehicleBodyUpLifting (1U)
 *   LevelChangeRequest_VehicleBodyDownLowering (2U)
 *   LevelChangeRequest_VehicleBodyUpMinimumMovementLifting (3U)
 *   LevelChangeRequest_VehicleBodyDownMinimumMovementLowering (4U)
 *   LevelChangeRequest_NotDefined (5U)
 *   LevelChangeRequest_ErrorIndicator (6U)
 *   LevelChangeRequest_NotAvailable (7U)
 * LevelStrokeRequest_T: Enumeration of integer in interval [0...3] with enumerators
 *   LevelStrokeRequest_DockingLevelControlStroke (0U)
 *   LevelStrokeRequest_DriveLevelControlStroke (1U)
 *   LevelStrokeRequest_ErrorIndicator (2U)
 *   LevelStrokeRequest_NotAvailable (3U)
 * LevelUserMemoryAction_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelUserMemoryAction_Inactive (0U)
 *   LevelUserMemoryAction_Recall (1U)
 *   LevelUserMemoryAction_Store (2U)
 *   LevelUserMemoryAction_Default (3U)
 *   LevelUserMemoryAction_Reserved (4U)
 *   LevelUserMemoryAction_Reserved_01 (5U)
 *   LevelUserMemoryAction_Error (6U)
 *   LevelUserMemoryAction_NotAvailable (7U)
 * LevelUserMemory_T: Enumeration of integer in interval [0...15] with enumerators
 *   LevelUserMemory_NotUsed (0U)
 *   LevelUserMemory_M1 (1U)
 *   LevelUserMemory_M2 (2U)
 *   LevelUserMemory_M3 (3U)
 *   LevelUserMemory_M4 (4U)
 *   LevelUserMemory_M5 (5U)
 *   LevelUserMemory_M6 (6U)
 *   LevelUserMemory_M7 (7U)
 *   LevelUserMemory_Spare (8U)
 *   LevelUserMemory_Spare01 (9U)
 *   LevelUserMemory_Spare02 (10U)
 *   LevelUserMemory_Spare03 (11U)
 *   LevelUserMemory_Spare04 (12U)
 *   LevelUserMemory_Spare05 (13U)
 *   LevelUserMemory_Error (14U)
 *   LevelUserMemory_NotAvailable (15U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * RampLevelRequest_T: Enumeration of integer in interval [0...15] with enumerators
 *   RampLevelRequest_TakeNoAction (0U)
 *   RampLevelRequest_RampLevelM1 (1U)
 *   RampLevelRequest_RampLevelM2 (2U)
 *   RampLevelRequest_RampLevelM3 (3U)
 *   RampLevelRequest_RampLevelM4 (4U)
 *   RampLevelRequest_RampLevelM5 (5U)
 *   RampLevelRequest_RampLevelM6 (6U)
 *   RampLevelRequest_RampLevelM7 (7U)
 *   RampLevelRequest_NotDefined_04 (8U)
 *   RampLevelRequest_NotDefined_05 (9U)
 *   RampLevelRequest_NotDefined_06 (10U)
 *   RampLevelRequest_NotDefined_07 (11U)
 *   RampLevelRequest_NotDefined_08 (12U)
 *   RampLevelRequest_NotDefined_09 (13U)
 *   RampLevelRequest_ErrorIndicator (14U)
 *   RampLevelRequest_NotAvailable (15U)
 * Request_T: Enumeration of integer in interval [0...3] with enumerators
 *   Request_NotRequested (0U)
 *   Request_RequestActive (1U)
 *   Request_Error (2U)
 *   Request_NotAvailable (3U)
 * RideHeightFunction_T: Enumeration of integer in interval [0...15] with enumerators
 *   RideHeightFunction_Inactive (0U)
 *   RideHeightFunction_StandardDrivePosition (1U)
 *   RideHeightFunction_AlternativeDrivePosition (2U)
 *   RideHeightFunction_SpeedDependentDrivePosition (3U)
 *   RideHeightFunction_HighDrivePosition (4U)
 *   RideHeightFunction_LowDrivePosition (5U)
 *   RideHeightFunction_AlternativeDrivePosition1 (6U)
 *   RideHeightFunction_AlternativeDrivePosition2 (7U)
 *   RideHeightFunction_NotDefined (8U)
 *   RideHeightFunction_NotDefined_01 (9U)
 *   RideHeightFunction_NotDefined_02 (10U)
 *   RideHeightFunction_NotDefined_03 (11U)
 *   RideHeightFunction_NotDefined_04 (12U)
 *   RideHeightFunction_NotDefined_05 (13U)
 *   RideHeightFunction_ErrorIndicator (14U)
 *   RideHeightFunction_NotAvailable (15U)
 * RideHeightStorageRequest_T: Enumeration of integer in interval [0...15] with enumerators
 *   RideHeightStorageRequest_TakeNoAction (0U)
 *   RideHeightStorageRequest_ResetToStoreFactoryDrivePosition (1U)
 *   RideHeightStorageRequest_StoreStandardUserDrivePosition (2U)
 *   RideHeightStorageRequest_StoreLowDrivePosition (3U)
 *   RideHeightStorageRequest_StoreHighDrivePosition (4U)
 *   RideHeightStorageRequest_ResetLowDrivePosition (5U)
 *   RideHeightStorageRequest_ResetHighDrivePosition (6U)
 *   RideHeightStorageRequest_NotDefined (7U)
 *   RideHeightStorageRequest_NotDefined_01 (8U)
 *   RideHeightStorageRequest_NotDefined_02 (9U)
 *   RideHeightStorageRequest_NotDefined_03 (10U)
 *   RideHeightStorageRequest_NotDefined_04 (11U)
 *   RideHeightStorageRequest_NotDefined_05 (12U)
 *   RideHeightStorageRequest_NotDefined_06 (13U)
 *   RideHeightStorageRequest_ErrorIndicator (14U)
 *   RideHeightStorageRequest_NotAvailable (15U)
 * RollRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   RollRequest_Idle (0U)
 *   RollRequest_Clockwise (1U)
 *   RollRequest_CounterClockwise (2U)
 *   RollRequest_ClockwiseShort (3U)
 *   RollRequest_CounterClockwiseShort (4U)
 *   RollRequest_Reserved (5U)
 *   RollRequest_Error (6U)
 *   RollRequest_NotAvailable (7U)
 * StopLevelChangeStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   StopLevelChangeStatus_NoStopRequest (0U)
 *   StopLevelChangeStatus_LevelChangedStopped (1U)
 *   StopLevelChangeStatus_ErrorIndicator (2U)
 *   StopLevelChangeStatus_NotAvailable (3U)
 * StorageAck_T: Enumeration of integer in interval [0...3] with enumerators
 *   StorageAck_TakeNoAction (0U)
 *   StorageAck_ChangeAcknowledged (1U)
 *   StorageAck_Error (2U)
 *   StorageAck_NotAvaiable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 * WiredLevelUserMemory_T: Enumeration of integer in interval [0...15] with enumerators
 *   WiredLevelUserMemory_MemOff (0U)
 *   WiredLevelUserMemory_M1 (1U)
 *   WiredLevelUserMemory_M2 (2U)
 *   WiredLevelUserMemory_M3 (3U)
 *   WiredLevelUserMemory_M4 (4U)
 *   WiredLevelUserMemory_M5 (5U)
 *   WiredLevelUserMemory_M6 (6U)
 *   WiredLevelUserMemory_M7 (7U)
 *   WiredLevelUserMemory_Spare (8U)
 *   WiredLevelUserMemory_Spare01 (9U)
 *   WiredLevelUserMemory_Spare02 (10U)
 *   WiredLevelUserMemory_Spare03 (11U)
 *   WiredLevelUserMemory_Spare04 (12U)
 *   WiredLevelUserMemory_Spare05 (13U)
 *   WiredLevelUserMemory_Error (14U)
 *   WiredLevelUserMemory_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 *
 * Record Types:
 * =============
 * FPBRMMIStat_T: Record with elements
 *   FPBRStatusInd_RE of type FPBRStatusInd_T
 *   FPBRChangeAck_RE of type Ack2Bit_T
 * LevelRequest_T: Record with elements
 *   FrontAxle_RE of type LevelChangeRequest_T
 *   RearAxle_RE of type LevelChangeRequest_T
 *   RollRequest_RE of type RollRequest_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_ECSStandbyActivationTimeout_P1CUA_T Rte_Prm_P1CUA_ECSStandbyActivationTimeout_v(void)
 *   SEWS_ECSStandbyExtendedActTimeout_P1CUB_T Rte_Prm_P1CUB_ECSStandbyExtendedActTimeout_v(void)
 *   SEWS_ECSActiveStateTimeout_P1CUE_T Rte_Prm_P1CUE_ECSActiveStateTimeout_v(void)
 *   SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T Rte_Prm_P1CUF_LoadingLevelSwStuckedTimeout_v(void)
 *   SEWS_KneelButtonStuckedTimeout_P1DWD_T Rte_Prm_P1DWD_KneelButtonStuckedTimeout_v(void)
 *   SEWS_FerryFuncSwStuckedTimeout_P1EXK_T Rte_Prm_P1EXK_FerryFuncSwStuckedTimeout_v(void)
 *   SEWS_ECS_StandbyBlinkTime_P1GCL_T Rte_Prm_P1GCL_ECS_StandbyBlinkTime_v(void)
 *   SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T Rte_Prm_P1IZ2_LoadingLevelAdjSwStuckTimeout_v(void)
 *   SEWS_FrontSuspensionType_P1JBR_T Rte_Prm_P1JBR_FrontSuspensionType_v(void)
 *   SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T Rte_Prm_P1LXQ_FPBRSwitchStuckedTimeout_v(void)
 *   SEWS_FPBRSwitchRequestACKTime_P1LXR_T Rte_Prm_P1LXR_FPBRSwitchRequestACKTime_v(void)
 *   boolean Rte_Prm_P1A12_ADL_Sw_v(void)
 *   boolean Rte_Prm_P1CT4_FerrySw_Installed_v(void)
 *   boolean Rte_Prm_P1CT9_LoadingLevelSw_Installed_v(void)
 *   boolean Rte_Prm_P1EXH_KneelingSwitchInstalled_v(void)
 *   boolean Rte_Prm_P1IZ1_LoadingLevelAdjSwitchInstalled_v(void)
 *   boolean Rte_Prm_P1LXP_FPBRSwitchInstalled_v(void)
 *   boolean Rte_Prm_P1ALT_ECS_PartialAirSystem_v(void)
 *   boolean Rte_Prm_P1ALU_ECS_FullAirSystem_v(void)
 *   boolean Rte_Prm_P1B9X_WirelessRC_Enable_v(void)
 *
 *********************************************************************************************************************/


#define LevelControl_HMICtrl_START_SEC_CODE
#include "LevelControl_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1DVZ_Data_P1DVZ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, LevelControl_HMICtrl_CODE) DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData(P2VAR(uint8, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: LevelControl_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AlternativeDriveLevelSw_stat_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_BackToDriveReqACK_BackToDriveReqACK(BackToDriveReqACK_T *data)
 *   Std_ReturnType Rte_Read_ChangeKneelACK_ChangeKneelACK(ChangeKneelACK_T *data)
 *   Std_ReturnType Rte_Read_ECSStandByReqRCECS_ECSStandByReqRCECS(ECSStandByReq_T *data)
 *   Std_ReturnType Rte_Read_ECSStandByReqWRC_ECSStandByReqWRC(ECSStandByReq_T *data)
 *   Std_ReturnType Rte_Read_ECSStandbyAllowed_ECSStandbyAllowed(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_ElectricalLoadReduction_rqst_ElectricalLoadReduction_rqst(ElectricalLoadReduction_rqst_T *data)
 *   Std_ReturnType Rte_Read_FPBRMMIStat_FPBRMMIStat(FPBRMMIStat_T *data)
 *   Std_ReturnType Rte_Read_FPBRSwitchStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_FerryFunctionStatus_FerryFunctionStatus(FerryFunctionStatus_T *data)
 *   Std_ReturnType Rte_Read_FerryFunctionSwitchChangeACK_FerryFunctionSwitchChangeACK(Ack2Bit_T *data)
 *   Std_ReturnType Rte_Read_FerryFunctionSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_KneelSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_KneelingStatusHMI_KneelingStatusHMI(KneelingStatusHMI_T *data)
 *   Std_ReturnType Rte_Read_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LoadingLevelSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_RampLevelRequestACK_RampLevelRequestACK(ChangeRequest2Bit_T *data)
 *   Std_ReturnType Rte_Read_RampLevelStorageAck_RampLevelStorageAck(StorageAck_T *data)
 *   Std_ReturnType Rte_Read_RideHeightStorageAck_RideHeightStorageAck(StorageAck_T *data)
 *   Std_ReturnType Rte_Read_StopLevelChangeAck_StopLevelChangeStatus(StopLevelChangeStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *   Std_ReturnType Rte_Read_WRCAirSuspensionStopRequest_AirSuspensionStopRequest(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_WRCLevelAdjustmentAction_LevelAdjustmentAction(LevelAdjustmentAction_T *data)
 *   Std_ReturnType Rte_Read_WRCLevelAdjustmentAxles_LevelAdjustmentAxles(LevelAdjustmentAxles_T *data)
 *   Std_ReturnType Rte_Read_WRCLevelAdjustmentStroke_LevelAdjustmentStroke(LevelAdjustmentStroke_T *data)
 *   Std_ReturnType Rte_Read_WRCLevelUserMemory_LevelUserMemory(LevelUserMemory_T *data)
 *   Std_ReturnType Rte_Read_WRCLevelUserMemoryAction_LevelUserMemoryAction(LevelUserMemoryAction_T *data)
 *   Std_ReturnType Rte_Read_WRCRollRequest_WRCRollRequest(RollRequest_T *data)
 *   Std_ReturnType Rte_Read_WiredAirSuspensionStopRequest_AirSuspensionStopRequest(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_WiredLevelAdjustmentAction_LevelAdjustmentAction(LevelAdjustmentAction_T *data)
 *   Std_ReturnType Rte_Read_WiredLevelAdjustmentAxles_LevelAdjustmentAxles(LevelAdjustmentAxles_T *data)
 *   Std_ReturnType Rte_Read_WiredLevelAdjustmentStroke_LevelAdjustmentStroke(LevelAdjustmentStroke_T *data)
 *   Std_ReturnType Rte_Read_WiredLevelUserMemory_WiredLevelUserMemory(WiredLevelUserMemory_T *data)
 *   Std_ReturnType Rte_Read_WiredLevelUserMemoryAction_LevelUserMemoryAction(LevelUserMemoryAction_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_BackToDriveReq_BackToDriveReq(BackToDriveReq_T data)
 *   Std_ReturnType Rte_Write_BlinkECSWiredLEDs_BlinkECSWiredLEDs(FalseTrue_T data)
 *   Std_ReturnType Rte_Write_ECSStandByRequest_ECSStandByRequest(ECSStandByRequest_T data)
 *   Std_ReturnType Rte_Write_ECSStandbyActive_ECSStandbyActive(FalseTrue_T data)
 *   Std_ReturnType Rte_Write_FPBRChangeReq_FPBRChangeReq(FPBRChangeReq_T data)
 *   Std_ReturnType Rte_Write_FPBR_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_FerryFunctionRequest_FerryFunctionRequest(Request_T data)
 *   Std_ReturnType Rte_Write_FerryFunctionSwitchChangeReq_FerryFunctionSwitchChangeReq(ChangeRequest2Bit_T data)
 *   Std_ReturnType Rte_Write_FerryFunction_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_InhibitWRCECSMenuCmd_InhibitWRCECSMenuCmd(InactiveActive_T data)
 *   Std_ReturnType Rte_Write_KneelDeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_KneelingChangeRequest_KneelingChangeRequest(KneelingChangeRequest_T data)
 *   Std_ReturnType Rte_Write_LevelRequest_LevelRequest(const LevelRequest_T *data)
 *   Std_ReturnType Rte_Write_LevelStrokeRequest_LevelStrokeRequest(LevelStrokeRequest_T data)
 *   Std_ReturnType Rte_Write_RampLevelRequest_RampLevelRequest(RampLevelRequest_T data)
 *   Std_ReturnType Rte_Write_RampLevelStorageRequest_RampLevelStorageRequest(RampLevelRequest_T data)
 *   Std_ReturnType Rte_Write_RideHeightFunctionRequest_RideHeightFunctionRequest(RideHeightFunction_T data)
 *   Std_ReturnType Rte_Write_RideHeightStorageRequest_RideHeightStorageRequest(RideHeightStorageRequest_T data)
 *   Std_ReturnType Rte_Write_StopLevelChangeRequest_StopLevelChangeRequest(Request_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BUO_63_LoadingLevelSw_stuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1CXA_63_FerryFlexibleSwitchStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1CXB_63_KneelingFlexibleSwitchStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1CXC_63_LoadingLevelAdjSwitchStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1DOO_63_FPBRSwitchStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ECSStandByActive_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ECSStandByActive_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: LevelControl_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LevelControl_HMICtrl_CODE) LevelControl_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: LevelControl_HMICtrl_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: LevelControl_HMICtrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: LevelControl_HMICtrl_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LevelControl_HMICtrl_CODE) LevelControl_HMICtrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: LevelControl_HMICtrl_Init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define LevelControl_HMICtrl_STOP_SEC_CODE
#include "LevelControl_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
