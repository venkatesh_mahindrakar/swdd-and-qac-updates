/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Keyfob_Mgr.c
 *           Config:  C:/GIT/scim_bsp_hd_t1_peps/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  Keyfob_Mgr
 *  Generation Time:  2020-11-03 12:57:43
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <Keyfob_Mgr>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   Boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_OpStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_CrankingLockActivation_P1DS3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_KeyMatchingIndicationTimeout_X1CV3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_KeyfobEncryptCode_P1DS4_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_Keyfob_Mgr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_CrankingLockActivation_P1DS3_T: Integer in interval [0...255]
 * SEWS_KeyMatchingIndicationTimeout_X1CV3_T: Integer in interval [0...255]
 * SEWS_KeyfobEncryptCode_P1DS4_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * DriverAuthDeviceMatching_T: Enumeration of integer in interval [0...3] with enumerators
 *   DriverAuthDeviceMatching_Idle (0U)
 *   DriverAuthDeviceMatching_DeviceToMatchIsPresent (1U)
 *   DriverAuthDeviceMatching_Error (2U)
 *   DriverAuthDeviceMatching_NotAvailable (3U)
 * KeyfobAuth_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobAuth_rqst_Idle (0U)
 *   KeyfobAuth_rqst_RequestByPassiveMechanism (1U)
 *   KeyfobAuth_rqst_RequestByImmobilizerMechanism (2U)
 *   KeyfobAuth_rqst_Spare1 (3U)
 *   KeyfobAuth_rqst_Spare2 (4U)
 *   KeyfobAuth_rqst_Spare3 (5U)
 *   KeyfobAuth_rqst_Error (6U)
 *   KeyfobAuth_rqst_NotAavailable (7U)
 * KeyfobAuth_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobAuth_stat_Idle (0U)
 *   KeyfobAuth_stat_NokeyfobAuthenticated (1U)
 *   KeyfobAuth_stat_KeyfobAuthenticated (2U)
 *   KeyfobAuth_stat_Spare1 (3U)
 *   KeyfobAuth_stat_Spare2 (4U)
 *   KeyfobAuth_stat_Spare3 (5U)
 *   KeyfobAuth_stat_Error (6U)
 *   KeyfobAuth_stat_NotAvailable (7U)
 * KeyfobInCabLocation_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobInCabLocation_stat_Idle (0U)
 *   KeyfobInCabLocation_stat_NotDetected_Incab (1U)
 *   KeyfobInCabLocation_stat_Detected_Incab (2U)
 *   KeyfobInCabLocation_stat_Spare1 (3U)
 *   KeyfobInCabLocation_stat_Spare2 (4U)
 *   KeyfobInCabLocation_stat_Spare3 (5U)
 *   KeyfobInCabLocation_stat_Error (6U)
 *   KeyfobInCabLocation_stat_NotAvailable (7U)
 * KeyfobInCabPresencePS_T: Enumeration of integer in interval [0...3] with enumerators
 *   KeyfobInCabPresencePS_rqst_NoRequest (0U)
 *   KeyfobInCabPresencePS_rqst_RequestDetectionInCab (1U)
 *   KeyfobInCabPresencePS_rqst_Error (2U)
 *   KeyfobInCabPresencePS_rqst_NotAvailable (3U)
 * KeyfobLocation_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 *   KeyfobLocation_rqst_NoRequest (0U)
 *   KeyfobLocation_rqst_Request (1U)
 *   KeyfobLocation_rqst_Error (2U)
 *   KeyfobLocation_rqst_NotAvailable (3U)
 * KeyfobOutsideLocation_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobOutsideLocation_stat_Idle (0U)
 *   KeyfobOutsideLocation_stat_NotDetectedOutside (1U)
 *   KeyfobOutsideLocation_stat_DetectedOnRightSide (2U)
 *   KeyfobOutsideLocation_stat_DetectedOnLeftSide (3U)
 *   KeyfobOutsideLocation_stat_DetectedOnBothSides (4U)
 *   KeyfobOutsideLocation_stat_DetectedOutOfDoorAreas (5U)
 *   KeyfobOutsideLocation_stat_Error (6U)
 *   KeyfobOutsideLocation_stat_NotAvailable (7U)
 * SCIM_ImmoDriver_ProcessingStatus_T: Enumeration of integer in interval [0...255] with enumerators
 *   ImmoDriver_ProcessingStatus_Idle (0U)
 *   ImmoDriver_ProcessingStatus_Ongoing (1U)
 *   ImmoDriver_ProcessingStatus_LfSent (2U)
 *   ImmoDriver_ProcessingStatus_DecryptedInList (3U)
 *   ImmoDriver_ProcessingStatus_DecryptedNotInList (4U)
 *   ImmoDriver_ProcessingStatus_NotDecrypted (5U)
 *   ImmoDriver_ProcessingStatus_HwError (6U)
 * SCIM_ImmoType_T: Enumeration of integer in interval [0...255] with enumerators
 *   ImmoType_ATA5702 (0U)
 *   ImmoType_ATA5577 (1U)
 * SCIM_PassiveDriver_ProcessingStatus_T: Enumeration of integer in interval [0...255] with enumerators
 *   PassiveDriver_ProcessingStatus_Idle (0U)
 *   PassiveDriver_ProcessingStatus_Ongoing (1U)
 *   PassiveDriver_ProcessingStatus_LfSent (2U)
 *   PassiveDriver_ProcessingStatus_Detected (3U)
 *   PassiveDriver_ProcessingStatus_NotDetected (4U)
 *   PassiveDriver_ProcessingStatus_HwError (5U)
 * SCIM_PassiveSearchCoverage_T: Enumeration of integer in interval [0...255] with enumerators
 *   PassiveSearchCoverage_PassiveStart2Ant (0U)
 *   PassiveSearchCoverage_PassiveStart1Ant (1U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data256ByteType: Array with 256 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 * Dcm_Data3ByteType: Array with 3 element(s) of type uint8
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 * SEWS_KeyfobEncryptCode_P1DS4_a_T: Array with 24 element(s) of type SEWS_KeyfobEncryptCode_P1DS4_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_KeyMatchingIndicationTimeout_X1CV3_T Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v(void)
 *   boolean Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v(void)
 *   boolean Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v(void)
 *   boolean Rte_Prm_P1B2U_KeyfobPresent_v(void)
 *   SEWS_KeyfobEncryptCode_P1DS4_T *Rte_Prm_P1DS4_KeyfobEncryptCode_v(void)
 *     Returnvalue: SEWS_KeyfobEncryptCode_P1DS4_T* is of type SEWS_KeyfobEncryptCode_P1DS4_a_T
 *   SEWS_CrankingLockActivation_P1DS3_T Rte_Prm_P1DS3_CrankingLockActivation_v(void)
 *   boolean Rte_Prm_P1C54_FactoryModeActive_v(void)
 *
 *********************************************************************************************************************/


#define Keyfob_Mgr_START_SEC_CODE
#include "Keyfob_Mgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchedKeyfobCount(void)
 *   uint8 Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchingStatus(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1B0T_Data_P1B0T_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1DS3_Data_P1DS3_CrankingLockActivation>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1DS3_Data_P1DS3_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1FDL_Data_P1FDL_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1FM6_Data_P1FM6_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data256ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1KAO_Data_P1KAO_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <WriteData> of PortPrototype <DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data256ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1KAO_Data_P1KAO_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData(P2CONST(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VKK_Data_P1VKK_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Keyfob_Mgr_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_DriverAuthDeviceMatching_DriverAuthDeviceMatching(DriverAuthDeviceMatching_T *data)
 *   Std_ReturnType Rte_Read_KeyfobAuth_rqst_KeyfobAuth_rqst(KeyfobAuth_rqst_T *data)
 *   Std_ReturnType Rte_Read_KeyfobInCabPresencePS_rqst_KeyfobInCabPresencePS_rqst(KeyfobInCabPresencePS_T *data)
 *   Std_ReturnType Rte_Read_KeyfobLocation_rqst_KeyfobLocation_rqst(KeyfobLocation_rqst_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_AddrParP1DS4_stat_dataP1DS4(const SEWS_KeyfobEncryptCode_P1DS4_T *data)
 *     Argument data: SEWS_KeyfobEncryptCode_P1DS4_T* is of type SEWS_KeyfobEncryptCode_P1DS4_a_T
 *   Std_ReturnType Rte_Write_KeyfobAuth_stat_KeyfobAuth_stat(KeyfobAuth_stat_T data)
 *   Std_ReturnType Rte_Write_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat(KeyfobInCabLocation_stat_T data)
 *   Std_ReturnType Rte_Write_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle(KeyfobOutsideLocation_stat_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF(void)
 *   uint8 Rte_IrvRead_Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount(void)
 *   uint8 Rte_IrvRead_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF(uint8 data)
 *   void Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount(uint8 data)
 *   void Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus(uint8 data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_ImmoProcessingRqst_GetImmoCircuitProcessingResult(SCIM_ImmoDriver_ProcessingStatus_T *ImmoProcessingStatus)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_ImmoProcessingRqst_ImmoCircuitProcessing(SCIM_ImmoType_T ImmoType, Boolean AuthRequestedbyImmo)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_KeyfobMatchingOperations_GetMatchingStatus(uint8 *matchingStatus, uint8 *matchedKeyfobCount)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_KeyfobMatchingOperations_MatchKeyfobByLF(void)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_KeyfobMatchingOperations_ReinitializeMatchingList(void)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult(SCIM_PassiveDriver_ProcessingStatus_T *ProcessingStatus, KeyfobInCabLocation_stat_T *KeyfobLocationbyPassive_Incab, KeyfobOutsideLocation_stat_T *KeyfobLocationbyPassive_Outcab)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_KeyfobPassiveSearchRqst_KeyfobPassiveSearch(SCIM_PassiveSearchCoverage_T PassiveSearchCoverage, boolean SearchRequestedbyPassive)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Keyfob_Mgr_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Keyfob_Mgr_CODE) Keyfob_Mgr_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Keyfob_Mgr_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_R1AAA_KeyfobMatchingRoutineByLF>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAA_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAA_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAA_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_R1AAA_KeyfobMatchingRoutineByLF>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAA_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAA_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAA_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_R1AAA_KeyfobMatchingRoutineByLF>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAA_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAA_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAA_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Y1ABD_ClearECU_Start
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Y1ABD_ClearECU>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_RoutineServices_Y1ABD_ClearECU_Start_FlagKeyfobMatchingByLF(void)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_KeyfobMatchingOperations_ReinitializeMatchingList(void)
 *     Synchronous Server Invocation. Timeout: None
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Y1ABD_ClearECU_Start(const uint8 *In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument In_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data4ByteType
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Y1ABD_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Y1ABD_DCM_E_PENDING
 *   RTE_E_RoutineServices_Y1ABD_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Y1ABD_ClearECU_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_Y1ABD_ClearECU_Start(P2CONST(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_DATA) In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Y1ABD_ClearECU_Start (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define Keyfob_Mgr_STOP_SEC_CODE
#include "Keyfob_Mgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
