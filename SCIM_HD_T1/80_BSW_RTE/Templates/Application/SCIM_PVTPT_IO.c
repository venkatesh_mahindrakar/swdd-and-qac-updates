/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  SCIM_PVTPT_IO.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  SCIM_PVTPT_IO
 *  Generation Time:  2020-09-21 13:44:23
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <SCIM_PVTPT_IO>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * IOHWAB_BOOL
 *   boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * IOHWAB_SINT8
 *   sint8 represents integers with a minimum value of -128 and a maximum value of 127.
 *      The order-relation on sint8 is: x < y if y - x is positive.
 *      sint8 has a lexical representation consisting of an optional sign followed 
 *      by a finite-length sequence of decimal digits (#x30-#x39). If the sign is 
 *      omitted, "+" is assumed. 
 *      
 *      For example: -1, 0, 12678, +10000.
 *
 * IOHWAB_UINT16
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * IOHWAB_UINT8
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_Pvt_ActivateReporting_X1C14_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinFaultStatus
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPinVoltage_0V2
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * VGTT_EcuPwmDutycycle
 *   sint8 represents integers with a minimum value of -128 and a maximum value of 127.
 *      The order-relation on sint8 is: x < y if y - x is positive.
 *      sint8 has a lexical representation consisting of an optional sign followed 
 *      by a finite-length sequence of decimal digits (#x30-#x39). If the sign is 
 *      omitted, "+" is assumed. 
 *      
 *      For example: -1, 0, 12678, +10000.
 *
 * VGTT_EcuPwmPeriod
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 *********************************************************************************************************************/

#include "Rte_SCIM_PVTPT_IO.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Debug_PVT_DOWHS1_ReportedValue: Boolean
 * Debug_PVT_DOWHS2_ReportedValue: Boolean
 * Debug_PVT_DOWLS2_ReportedValue: Boolean
 * Debug_PVT_DOWLS3_ReportedValue: Boolean
 * Debug_PVT_SCIM_Ctrl_Generic1: Integer in interval [0...3]
 * Debug_PVT_SCIM_RD_12VDCDCFault: Boolean
 * Debug_PVT_SCIM_RD_12VDCDCVolt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_12VLivingVolt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_12VParkedVolt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI01_7: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI02_8: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI03_9: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI04_10: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI05_11: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI06_12: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_BHS1_Volt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_BHS2_Volt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_BHS3_Volt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_BHS4_Volt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_BLS1_Volt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_DAI1_2: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_VBAT: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_WHS1_Freq: Integer in interval [0...2047]
 * Debug_PVT_SCIM_RD_WHS1_VD: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_WHS2_Freq: Integer in interval [0...2047]
 * Debug_PVT_SCIM_RD_WHS2_VD: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_WLS2_Freq: Integer in interval [0...2047]
 * Debug_PVT_SCIM_RD_WLS2_VD: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_WLS3_Freq: Integer in interval [0...2047]
 * Debug_PVT_SCIM_RD_WLS3_VD: Integer in interval [0...255]
 * Debug_PVT_SCIM_TSincePwrOn_Long: Integer in interval [0...65535]
 * Debug_PVT_SCIM_TSincePwrOn_Short: Integer in interval [0...255]
 * Debug_PVT_SCIM_TSinceWkUp_Short: Integer in interval [0...255]
 * Debug_PVT_ScimHwSelect_WHS1: Boolean
 * Debug_PVT_ScimHwSelect_WHS2: Boolean
 * Debug_PVT_ScimHwSelect_WLS2: Boolean
 * Debug_PVT_ScimHwSelect_WLS3: Boolean
 * Debug_PVT_ScimHw_W_Duty: Integer in interval [0...127]
 * Debug_PVT_ScimHw_W_Freq: Integer in interval [0...2047]
 * Debug_SCIM_RD_Generic1: Integer in interval [0...31]
 * Debug_SCIM_RD_Generic2: Integer in interval [0...15]
 * Debug_SCIM_RD_Generic3: Integer in interval [0...63]
 * Debug_SCIM_RD_Generic4: Integer in interval [0...255]
 * Debug_SCIM_RD_Generic5: Boolean
 * Debug_SCIM_RD_Generic6: Integer in interval [0...31]
 * IOHWAB_BOOL: Boolean
 * IOHWAB_SINT8: Integer in interval [-128...127]
 * IOHWAB_UINT16: Integer in interval [0...65535]
 * IOHWAB_UINT8: Integer in interval [0...255]
 * SEWS_Pvt_ActivateReporting_X1C14_T: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * VGTT_EcuPwmDutycycle: Integer in interval [-128...127]
 * VGTT_EcuPwmPeriod: Integer in interval [0...65535]
 * boolean: Boolean (standard type)
 * sint8: Integer in interval [-128...127] (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Debug_PVT_ADI_ReportGroup: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Ctrl_CyclicReport (0U)
 *   Cx1_ADI_01To06_DAI1 (1U)
 *   Cx2_ADI_07To12_DAI2 (2U)
 *   Cx3_ADI_13To16_DAI (3U)
 * Debug_PVT_ADI_ReportRequest: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Ctrl_CyclicReport (0U)
 *   Cx1_ADI_01To06_DAI1 (1U)
 *   Cx2_ADI_07To12_DAI2 (2U)
 *   Cx3_ADI_13To16_DAI (3U)
 * Debug_PVT_FlexDataRequest: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_Ctr_CyclicReport (0U)
 *   Cx1_Universal_debug_trace (1U)
 *   Cx2_Keyfob_RF_data1 (2U)
 *   Cx3_Keyfob_RF_data2 (3U)
 *   Cx4_Keyfob_LF_data1 (4U)
 *   Cx5_Keyfob_LF_data2 (5U)
 *   Cx6_SW_execution_statistics (6U)
 *   Cx7_SW_state_statistics (7U)
 * Debug_PVT_SCIM_Ctrl_12VDCDC: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_12VLiving: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_12VParked: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_BHS1: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_BHS2: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_BHS3: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_BHS4: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_BLS1: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_DAIPullUp: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_WHS1: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_WHS2: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_WLS2: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_Ctrl_WLS3: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_Deactivate (1U)
 *   Cx2_Activate (2U)
 * Debug_PVT_SCIM_RD_12VLivingFault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_12VParkedFault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_BHS1_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_BHS2_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_BHS3_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_BHS4_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_BLS1_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_VBAT_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_WHS1_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_PWM_failure (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_WHS2_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_PWM_failure (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_WLS2_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_PWM_failure (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * Debug_PVT_SCIM_RD_WLS3_Fault: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_None (0U)
 *   Cx1_Short_To_Ground (1U)
 *   Cx2_Short_To_Battery (2U)
 *   Cx3_Open_Circuit (3U)
 *   Cx4_PWM_failure (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Voltage_Too_Low (6U)
 *   Cx7_Voltage_Too_High (7U)
 * IOCtrlReq_T: Enumeration of integer in interval [0...255] with enumerators
 *   IOCtrl_AppRequest (0U)
 *   IOCtrl_DiagReturnCtrlToApp (1U)
 *   IOCtrl_DiagShortTermAdjust (2U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_Pvt_ActivateReporting_X1C14_T Rte_Prm_X1C14_Pvt_ActivateReporting_v(void)
 *
 *********************************************************************************************************************/


#define SCIM_PVTPT_IO_START_SEC_CODE
#include "SCIM_PVTPT_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Runnable_PVTPT_CtrlIo
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VDCDC(Debug_PVT_SCIM_Ctrl_12VDCDC *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VLiving(Debug_PVT_SCIM_Ctrl_12VLiving *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VParked(Debug_PVT_SCIM_Ctrl_12VParked *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS1(Debug_PVT_SCIM_Ctrl_BHS1 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS2(Debug_PVT_SCIM_Ctrl_BHS2 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS3(Debug_PVT_SCIM_Ctrl_BHS3 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS4(Debug_PVT_SCIM_Ctrl_BHS4 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BLS1(Debug_PVT_SCIM_Ctrl_BLS1 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_DAIPullUp(Debug_PVT_SCIM_Ctrl_DAIPullUp *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_Generic1(Debug_PVT_SCIM_Ctrl_Generic1 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS1(Debug_PVT_SCIM_Ctrl_WHS1 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS2(Debug_PVT_SCIM_Ctrl_WHS2 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS2(Debug_PVT_SCIM_Ctrl_WLS2 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS3(Debug_PVT_SCIM_Ctrl_WLS3 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS1(Debug_PVT_ScimHwSelect_WHS1 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS2(Debug_PVT_ScimHwSelect_WHS2 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS2(Debug_PVT_ScimHwSelect_WLS2 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS3(Debug_PVT_ScimHwSelect_WLS3 *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Duty(Debug_PVT_ScimHw_W_Duty *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Freq(Debug_PVT_ScimHw_W_Freq *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ScimPvtControl_P_Status(uint8 data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_SetPullUp_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL ActivateStrongPullUp, IOHWAB_BOOL ActivateWeakPullUp, IOHWAB_BOOL ActivateDAIPullUp)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Runnable_PVTPT_CtrlIo_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SCIM_PVTPT_IO_CODE) Runnable_PVTPT_CtrlIo(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Runnable_PVTPT_CtrlIo
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Runnable_PVTPT_ReadIo
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Debug_PVT_ADI_ReportRequest_Debug_PVT_ADI_ReportRequest(Debug_PVT_ADI_ReportRequest *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_ADI_ReportGroup(Debug_PVT_ADI_ReportGroup data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWHS1_ReportedValue(Debug_PVT_DOWHS1_ReportedValue data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWHS2_ReportedValue(Debug_PVT_DOWHS2_ReportedValue data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWLS2_ReportedValue(Debug_PVT_DOWLS2_ReportedValue data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWLS3_ReportedValue(Debug_PVT_DOWLS3_ReportedValue data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCFault(Debug_PVT_SCIM_RD_12VDCDCFault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCVolt(Debug_PVT_SCIM_RD_12VDCDCVolt data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingFault(Debug_PVT_SCIM_RD_12VLivingFault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingVolt(Debug_PVT_SCIM_RD_12VLivingVolt data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedFault(Debug_PVT_SCIM_RD_12VParkedFault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedVolt(Debug_PVT_SCIM_RD_12VParkedVolt data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI01_7(Debug_PVT_SCIM_RD_ADI01_7 data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI02_8(Debug_PVT_SCIM_RD_ADI02_8 data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI03_9(Debug_PVT_SCIM_RD_ADI03_9 data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI04_10(Debug_PVT_SCIM_RD_ADI04_10 data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI05_11(Debug_PVT_SCIM_RD_ADI05_11 data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI06_12(Debug_PVT_SCIM_RD_ADI06_12 data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Fault(Debug_PVT_SCIM_RD_BHS1_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Volt(Debug_PVT_SCIM_RD_BHS1_Volt data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Fault(Debug_PVT_SCIM_RD_BHS2_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Volt(Debug_PVT_SCIM_RD_BHS2_Volt data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Fault(Debug_PVT_SCIM_RD_BHS3_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Volt(Debug_PVT_SCIM_RD_BHS3_Volt data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Fault(Debug_PVT_SCIM_RD_BHS4_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Volt(Debug_PVT_SCIM_RD_BHS4_Volt data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Fault(Debug_PVT_SCIM_RD_BLS1_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Volt(Debug_PVT_SCIM_RD_BLS1_Volt data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_DAI1_2(Debug_PVT_SCIM_RD_DAI1_2 data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT(Debug_PVT_SCIM_RD_VBAT data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT_Fault(Debug_PVT_SCIM_RD_VBAT_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Fault(Debug_PVT_SCIM_RD_WHS1_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Freq(Debug_PVT_SCIM_RD_WHS1_Freq data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_VD(Debug_PVT_SCIM_RD_WHS1_VD data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Fault(Debug_PVT_SCIM_RD_WHS2_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Freq(Debug_PVT_SCIM_RD_WHS2_Freq data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_VD(Debug_PVT_SCIM_RD_WHS2_VD data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Fault(Debug_PVT_SCIM_RD_WLS2_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Freq(Debug_PVT_SCIM_RD_WLS2_Freq data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_VD(Debug_PVT_SCIM_RD_WLS2_VD data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Fault(Debug_PVT_SCIM_RD_WLS3_Fault data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Freq(Debug_PVT_SCIM_RD_WLS3_Freq data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_VD(Debug_PVT_SCIM_RD_WLS3_VD data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Long(Debug_PVT_SCIM_TSincePwrOn_Long data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Short(Debug_PVT_SCIM_TSincePwrOn_Short data)
 *   Std_ReturnType Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSinceWkUp_Short(Debug_PVT_SCIM_TSinceWkUp_Short data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetPullUpState_CS(IOHWAB_BOOL *isPullUpActive_Strong, IOHWAB_BOOL *isPullUpActive_Weak, IOHWAB_BOOL *isPullUpActive_DAI)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(VGTT_EcuPinVoltage_0V2 *DcDc12vRefVoltage, IOHWAB_BOOL *IsDcDc12vActivated, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_1_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_2_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_3_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_4_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_VbatInterface_P_GetVbatVoltage_CS(VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_VbatInterface_I_AdcInFailure, RTE_E_VbatInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Runnable_PVTPT_ReadIo_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SCIM_PVTPT_IO_CODE) Runnable_PVTPT_ReadIo(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Runnable_PVTPT_ReadIo
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define SCIM_PVTPT_IO_STOP_SEC_CODE
#include "SCIM_PVTPT_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
