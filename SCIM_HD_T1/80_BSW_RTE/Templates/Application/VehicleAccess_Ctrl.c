/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  VehicleAccess_Ctrl.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  VehicleAccess_Ctrl
 *  Generation Time:  2020-09-21 13:44:24
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <VehicleAccess_Ctrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   Boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * Issm_IssStateType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AlarmAutoRelockRequestDuration_X1CYA_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AutoAlarmReactivationTimeout_P1B2S_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DashboardLedTimeout_P1IZ4_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorAutoLockingSpeed_P1B2Q_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorIndicationReqDuration_P1DWP_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorLatchProtectMaxOperation_P1DXA_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorLatchProtectionRestingTime_P1DW9_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorLatchProtectionTimeWindow_P1DW8_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_DoorLockingFailureTimeout_X1CX9_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_LockFunctionHardwareInterface_P1MXZ_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_PassiveEntryFunction_Type_P1VKF_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_SpeedRelockingReinitThreshold_P1H55_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Speed16bit_T
 *   65024 - 65279 Error ; 65280 - 65535 Not available
 *
 *********************************************************************************************************************/

#include "Rte_VehicleAccess_Ctrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_AlarmAutoRelockRequestDuration_X1CYA_T: Integer in interval [0...255]
 * SEWS_AutoAlarmReactivationTimeout_P1B2S_T: Integer in interval [0...255]
 * SEWS_DashboardLedTimeout_P1IZ4_T: Integer in interval [0...255]
 * SEWS_DoorAutoLockingSpeed_P1B2Q_T: Integer in interval [0...255]
 * SEWS_DoorIndicationReqDuration_P1DWP_T: Integer in interval [0...255]
 * SEWS_DoorLatchProtectMaxOperation_P1DXA_T: Integer in interval [0...255]
 * SEWS_DoorLatchProtectionRestingTime_P1DW9_T: Integer in interval [0...255]
 * SEWS_DoorLatchProtectionTimeWindow_P1DW8_T: Integer in interval [0...255]
 * SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T: Integer in interval [0...65535]
 * SEWS_DoorLockingFailureTimeout_X1CX9_T: Integer in interval [0...255]
 * SEWS_LockFunctionHardwareInterface_P1MXZ_T: Integer in interval [0...255]
 * SEWS_PassiveEntryFunction_Type_P1VKF_T: Integer in interval [0...255]
 * SEWS_SpeedRelockingReinitThreshold_P1H55_T: Integer in interval [0...255]
 * Speed16bit_T: Integer in interval [0...65535]
 *   Unit: [Km_per_h], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * AutoRelock_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 *   AutoRelock_rqst_Idle (0U)
 *   AutoRelock_rqst_AutoRelockActivated (1U)
 *   AutoRelock_rqst_Error (2U)
 *   AutoRelock_rqst_NotAvailable (3U)
 * AutorelockingMovements_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   AutorelockingMovements_stat_Idle (0U)
 *   AutorelockingMovements_stat_NoMovementHasBeenDetected (1U)
 *   AutorelockingMovements_stat_MovementHasBeenDetected (2U)
 *   AutorelockingMovements_stat_Spare (3U)
 *   AutorelockingMovements_stat_Spare_01 (4U)
 *   AutorelockingMovements_stat_Spare_02 (5U)
 *   AutorelockingMovements_stat_Error (6U)
 *   AutorelockingMovements_stat_NotAvailable (7U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DoorAjar_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorAjar_stat_Idle (0U)
 *   DoorAjar_stat_DoorClosed (1U)
 *   DoorAjar_stat_DoorOpen (2U)
 *   DoorAjar_stat_Error (6U)
 *   DoorAjar_stat_NotAvailable (7U)
 * DoorLatch_rqst_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorLatch_rqst_decrypt_Idle (0U)
 *   DoorLatch_rqst_decrypt_LockDoorCommand (1U)
 *   DoorLatch_rqst_decrypt_UnlockDoorCommand (2U)
 *   DoorLatch_rqst_decrypt_EmergencyUnlockRequest (3U)
 *   DoorLatch_rqst_decrypt_Spare1 (4U)
 *   DoorLatch_rqst_decrypt_Spare2 (5U)
 *   DoorLatch_rqst_decrypt_Error (6U)
 *   DoorLatch_rqst_decrypt_NotAvailable (7U)
 * DoorLatch_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorLatch_stat_Idle (0U)
 *   DoorLatch_stat_LatchUnlockedFiltered (1U)
 *   DoorLatch_stat_LatchLockedFiltered (2U)
 *   DoorLatch_stat_Error (6U)
 *   DoorLatch_stat_NotAvailable (7U)
 * DoorLockUnlock_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorLockUnlock_Idle (0U)
 *   DoorLockUnlock_Unlock (1U)
 *   DoorLockUnlock_Lock (2U)
 *   DoorLockUnlock_MonoLockUnlock (3U)
 *   DoorLockUnlock_Spare1 (4U)
 *   DoorLockUnlock_Spare2 (5U)
 *   DoorLockUnlock_Error (6U)
 *   DoorLockUnlock_NotAvailable (7U)
 * DoorLock_stat_T: Enumeration of integer in interval [0...15] with enumerators
 *   DoorLock_stat_Idle (0U)
 *   DoorLock_stat_BothDoorsAreUnlocked (1U)
 *   DoorLock_stat_DriverDoorIsUnlocked (2U)
 *   DoorLock_stat_PassengerDoorIsUnlocked (3U)
 *   DoorLock_stat_BothDoorsAreLocked (4U)
 *   DoorLock_stat_Error (14U)
 *   DoorLock_stat_NotAvailable (15U)
 * DoorsAjar_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorsAjar_stat_Idle (0U)
 *   DoorsAjar_stat_BothDoorsAreClosed (1U)
 *   DoorsAjar_stat_DriverDoorIsOpen (2U)
 *   DoorsAjar_stat_PassengerDoorIsOpen (3U)
 *   DoorsAjar_stat_BothDoorsAreOpen (4U)
 *   DoorsAjar_stat_Spare (5U)
 *   DoorsAjar_stat_Error (6U)
 *   DoorsAjar_stat_NotAvailable (7U)
 * EmergencyDoorsUnlock_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   EmergencyDoorsUnlock_rqst_Idle (0U)
 *   EmergencyDoorsUnlock_rqst_Spare (1U)
 *   EmergencyDoorsUnlock_rqst_EmergencyUnlockRequest (2U)
 *   EmergencyDoorsUnlock_rqst_Error (6U)
 *   EmergencyDoorsUnlock_rqst_NotAvailable (7U)
 * FrontLidLatch_cmd_T: Enumeration of integer in interval [0...7] with enumerators
 *   FrontLidLatch_cmd_Idle (0U)
 *   FrontLidLatch_cmd_LockFrontLidCommand (1U)
 *   FrontLidLatch_cmd_UnlockFrontLidCommand (2U)
 *   FrontLidLatch_cmd_Error (6U)
 *   FrontLidLatch_cmd_NotAvailable (7U)
 * FrontLidLatch_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   FrontLidLatch_stat_Idle (0U)
 *   FrontLidLatch_stat_LatchUnlocked (1U)
 *   FrontLidLatch_stat_LatchLocked (2U)
 *   FrontLidLatch_stat_Error (6U)
 *   FrontLidLatch_stat_NotAvailable (7U)
 * Issm_IssStateType: Enumeration of integer in interval [0...2] with enumerators
 *   ISSM_STATE_INACTIVE (0U)
 *   ISSM_STATE_PENDING (1U)
 *   ISSM_STATE_ACTIVE (2U)
 * KeyfobInCabLocation_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobInCabLocation_stat_Idle (0U)
 *   KeyfobInCabLocation_stat_NotDetected_Incab (1U)
 *   KeyfobInCabLocation_stat_Detected_Incab (2U)
 *   KeyfobInCabLocation_stat_Spare1 (3U)
 *   KeyfobInCabLocation_stat_Spare2 (4U)
 *   KeyfobInCabLocation_stat_Spare3 (5U)
 *   KeyfobInCabLocation_stat_Error (6U)
 *   KeyfobInCabLocation_stat_NotAvailable (7U)
 * KeyfobLocation_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 *   KeyfobLocation_rqst_NoRequest (0U)
 *   KeyfobLocation_rqst_Request (1U)
 *   KeyfobLocation_rqst_Error (2U)
 *   KeyfobLocation_rqst_NotAvailable (3U)
 * KeyfobOutsideLocation_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobOutsideLocation_stat_Idle (0U)
 *   KeyfobOutsideLocation_stat_NotDetectedOutside (1U)
 *   KeyfobOutsideLocation_stat_DetectedOnRightSide (2U)
 *   KeyfobOutsideLocation_stat_DetectedOnLeftSide (3U)
 *   KeyfobOutsideLocation_stat_DetectedOnBothSides (4U)
 *   KeyfobOutsideLocation_stat_DetectedOutOfDoorAreas (5U)
 *   KeyfobOutsideLocation_stat_Error (6U)
 *   KeyfobOutsideLocation_stat_NotAvailable (7U)
 * LockingIndication_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   LockingIndication_rqst_Idle (0U)
 *   LockingIndication_rqst_Lock (1U)
 *   LockingIndication_rqst_Unlock (2U)
 *   LockingIndication_rqst_Spare (3U)
 *   LockingIndication_rqst_Spare01 (4U)
 *   LockingIndication_rqst_Spare02 (5U)
 *   LockingIndication_rqst_Error (6U)
 *   LockingIndication_rqst_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * SpeedLockingInhibition_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   SpeedLockingInhibition_stat_Idle (0U)
 *   SpeedLockingInhibition_stat_SpeedLockingActivate (1U)
 *   SpeedLockingInhibition_stat_SpeedLockingDeactivate (2U)
 *   SpeedLockingInhibition_stat_Error (6U)
 *   SpeedLockingInhibition_stat_NotAvailable (7U)
 * Synch_Unsynch_Mode_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   Synch_Unsynch_Mode_stat_Idle (0U)
 *   Synch_Unsynch_Mode_stat_SynchronizedMode (1U)
 *   Synch_Unsynch_Mode_stat_UnsynchronizedMode (2U)
 *   Synch_Unsynch_Mode_stat_Spare (3U)
 *   Synch_Unsynch_Mode_stat_Spare_01 (4U)
 *   Synch_Unsynch_Mode_stat_Spare_02 (5U)
 *   Synch_Unsynch_Mode_stat_Error (6U)
 *   Synch_Unsynch_Mode_stat_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Crypto_Function_serialized_T: Array with 12 element(s) of type uint8
 * StandardNVM_T: Array with 4 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_DoorLockingFailureTimeout_X1CX9_T Rte_Prm_X1CX9_DoorLockingFailureTimeout_v(void)
 *   SEWS_AlarmAutoRelockRequestDuration_X1CYA_T Rte_Prm_X1CYA_AlarmAutoRelockRequestDuration_v(void)
 *   SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T Rte_Prm_P1O8Q_DoorLockHazardIndicationRqstDelay_v(void)
 *   SEWS_DoorAutoLockingSpeed_P1B2Q_T Rte_Prm_P1B2Q_DoorAutoLockingSpeed_v(void)
 *   SEWS_AutoAlarmReactivationTimeout_P1B2S_T Rte_Prm_P1B2S_AutoAlarmReactivationTimeout_v(void)
 *   SEWS_DoorLatchProtectionTimeWindow_P1DW8_T Rte_Prm_P1DW8_DoorLatchProtectionTimeWindow_v(void)
 *   SEWS_DoorLatchProtectionRestingTime_P1DW9_T Rte_Prm_P1DW9_DoorLatchProtectionRestingTime_v(void)
 *   SEWS_DoorIndicationReqDuration_P1DWP_T Rte_Prm_P1DWP_DoorIndicationReqDuration_v(void)
 *   SEWS_DoorLatchProtectMaxOperation_P1DXA_T Rte_Prm_P1DXA_DoorLatchProtectMaxOperation_v(void)
 *   SEWS_SpeedRelockingReinitThreshold_P1H55_T Rte_Prm_P1H55_SpeedRelockingReinitThreshold_v(void)
 *   SEWS_DashboardLedTimeout_P1IZ4_T Rte_Prm_P1IZ4_DashboardLedTimeout_v(void)
 *   SEWS_LockFunctionHardwareInterface_P1MXZ_T Rte_Prm_P1MXZ_LockFunctionHardwareInterface_v(void)
 *   SEWS_PassiveEntryFunction_Type_P1VKF_T Rte_Prm_P1VKF_PassiveEntryFunction_Type_v(void)
 *   boolean Rte_Prm_P1NE9_KeyInsertDetection_Enabled_v(void)
 *   boolean Rte_Prm_P1NQE_LockModeHandling_v(void)
 *   boolean Rte_Prm_P1VKI_PassiveStart_Installed_v(void)
 *
 *********************************************************************************************************************/


#define VehicleAccess_Ctrl_START_SEC_CODE
#include "VehicleAccess_Ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VehicleAccess_Ctrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AutorelockingMovements_stat_AutorelockingMovements_stat(AutorelockingMovements_stat_T *data)
 *   Std_ReturnType Rte_Read_DriverDoorAjarInternal_stat_DoorAjar_stat(DoorAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_DriverDoorAjar_stat_DoorAjar_stat(DoorAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_DriverDoorLatchInternal_stat_DoorLatch_stat(DoorLatch_stat_T *data)
 *   Std_ReturnType Rte_Read_DriverDoorLatch_stat_DoorLatch_stat(DoorLatch_stat_T *data)
 *   Std_ReturnType Rte_Read_DrivrDrKeyCylTrn_st_serialized_Crypto_Function_serialized(uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Read_EmergencyDoorsUnlock_rqst_EmergencyDoorsUnlock_rqst(EmergencyDoorsUnlock_rqst_T *data)
 *   Std_ReturnType Rte_Read_FrontLidLatch_stat_FrontLidLatch_stat(FrontLidLatch_stat_T *data)
 *   Std_ReturnType Rte_Read_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst(DoorLockUnlock_T *data)
 *   Std_ReturnType Rte_Read_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat(KeyfobInCabLocation_stat_T *data)
 *   Std_ReturnType Rte_Read_KeyfobLockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle(KeyfobOutsideLocation_stat_T *data)
 *   Std_ReturnType Rte_Read_KeyfobSuperLockButton_Sta_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_KeyfobUnlockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LeftDoorButton_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_PassengerDoorAjar_stat_DoorAjar_stat(DoorAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_PassengerDoorLatch_stat_DoorLatch_stat(DoorLatch_stat_T *data)
 *   Std_ReturnType Rte_Read_PsgDoorKeyCylTrn_st_serialized_Crypto_Function_serialized(uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Read_PsngDoorLatchInternal_stat_DoorLatch_stat(DoorLatch_stat_T *data)
 *   Std_ReturnType Rte_Read_PsngrDoorAjarInternal_stat_DoorAjar_stat(DoorAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_RightDoorButton_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SpeedLockingInhibition_stat_SpeedLockingInhibition_stat(SpeedLockingInhibition_stat_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_Synch_Unsynch_Mode_stat_Synch_Unsynch_Mode_stat(Synch_Unsynch_Mode_stat_T *data)
 *   Std_ReturnType Rte_Read_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I(uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *   Std_ReturnType Rte_Read_WRCLockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_WRCUnlockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_AutoRelock_rqst_AutoRelock_rqst(AutoRelock_rqst_T data)
 *   Std_ReturnType Rte_Write_DashboardLockSwitch_Devic_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_DoorLock_stat_DoorLock_stat(DoorLock_stat_T data)
 *   Std_ReturnType Rte_Write_DoorsAjar_stat_DoorsAjar_stat(DoorsAjar_stat_T data)
 *   Std_ReturnType Rte_Write_DriverDoorLatch_rqst_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(DoorLatch_rqst_decrypt_T data)
 *   Std_ReturnType Rte_Write_DriverDoorLatch_rqt_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_FrontLidLatch_cmd_FrontLidLatch_cmd(FrontLidLatch_cmd_T data)
 *   Std_ReturnType Rte_Write_KeyfobLocation_rqst_KeyfobLocation_rqst(KeyfobLocation_rqst_T data)
 *   Std_ReturnType Rte_Write_LockingIndication_rqst_LockingIndication_rqst(LockingIndication_rqst_T data)
 *   Std_ReturnType Rte_Write_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(DoorLatch_rqst_decrypt_T data)
 *   Std_ReturnType Rte_Write_PsngrDoorLatch_rqst_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_PsngrDoorLatch_rqst_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I(const uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlActDeactivation_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlActDeactivation_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlActDeactivation_GetIssState(Issm_IssStateType *issState)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlCabRqst2_Pending_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlCabRqst2_Pending_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VehicleAccess_Ctrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VehicleAccess_Ctrl_CODE) VehicleAccess_Ctrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VehicleAccess_Ctrl_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VehicleAccess_Ctrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VehicleAccess_Ctrl_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VehicleAccess_Ctrl_CODE) VehicleAccess_Ctrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VehicleAccess_Ctrl_Init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define VehicleAccess_Ctrl_STOP_SEC_CODE
#include "VehicleAccess_Ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
