/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  DriverAuthentication2_Ctrl.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  DriverAuthentication2_Ctrl
 *  Generation Time:  2020-09-21 13:44:21
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <DriverAuthentication2_Ctrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   Boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * SEWS_CrankingLockActivation_P1DS3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1VKG_APM_Check_Active_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1VKG_DISPLAY_Check_Active_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1VKG_EMS_Check_Active_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1VKG_MVUC_Check_Active_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_P1VKG_TECU_Check_Active_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_DriverAuthentication2_Ctrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_CrankingLockActivation_P1DS3_T: Integer in interval [0...255]
 * SEWS_P1VKG_DISPLAY_Check_Active_T: Integer in interval [0...255]
 * SEWS_P1VKG_EMS_Check_Active_T: Integer in interval [0...255]
 * SEWS_P1VKG_MVUC_Check_Active_T: Integer in interval [0...255]
 * SEWS_P1VKG_TECU_Check_Active_T: Integer in interval [0...255]
 * VIN_rqst_T: Boolean
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DeviceAuthentication_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   DeviceAuthentication_rqst_Idle (0U)
 *   DeviceAuthentication_rqst_DeviceAuthenticationRequest (1U)
 *   DeviceAuthentication_rqst_DeviceDeauthenticationRequest (2U)
 *   DeviceAuthentication_rqst_DeviceMatching (3U)
 *   DeviceAuthentication_rqst_Spare1 (4U)
 *   DeviceAuthentication_rqst_Spare2 (5U)
 *   DeviceAuthentication_rqst_Error (6U)
 *   DeviceAuthentication_rqst_NotAvailable (7U)
 * DeviceInCab_stat_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceInCab_stat_Idle (0U)
 *   DeviceInCab_stat_DeviceNotAuthenticated (1U)
 *   DeviceInCab_stat_DeviceAuthenticated (2U)
 *   DeviceInCab_stat_NotAvailable (3U)
 * DoorsAjar_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorsAjar_stat_Idle (0U)
 *   DoorsAjar_stat_BothDoorsAreClosed (1U)
 *   DoorsAjar_stat_DriverDoorIsOpen (2U)
 *   DoorsAjar_stat_PassengerDoorIsOpen (3U)
 *   DoorsAjar_stat_BothDoorsAreOpen (4U)
 *   DoorsAjar_stat_Spare (5U)
 *   DoorsAjar_stat_Error (6U)
 *   DoorsAjar_stat_NotAvailable (7U)
 * EngineStartAuth_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   EngineStartAuth_rqst_StartingOfTheTruckNotRequested (0U)
 *   EngineStartAuth_rqst_StartingOfTheTruckRequested (1U)
 *   EngineStartAuth_rqst_Spare (2U)
 *   EngineStartAuth_rqst_Spare_01 (3U)
 *   EngineStartAuth_rqst_Spare_02 (4U)
 *   EngineStartAuth_rqst_Spare_03 (5U)
 *   EngineStartAuth_rqst_Error (6U)
 *   EngineStartAuth_rqst_NotAvailable (7U)
 * EngineStartAuth_stat_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 *   EngineStartAuth_stat_decrypt_Idle (0U)
 *   EngineStartAuth_stat_decrypt_CrankingIsAuthorized (1U)
 *   EngineStartAuth_stat_decrypt_CrankingIsProhibited (2U)
 *   EngineStartAuth_stat_decrypt_Spare1 (3U)
 *   EngineStartAuth_stat_decrypt_Spare2 (4U)
 *   EngineStartAuth_stat_decrypt_Spare3 (5U)
 *   EngineStartAuth_stat_decrypt_Error (6U)
 *   EngineStartAuth_stat_decrypt_NotAvailable (7U)
 * GearBoxUnlockAuth_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 *   GearBoxUnlockAuth_rqst_GearEngagementNotRequested (0U)
 *   GearBoxUnlockAuth_rqst_GearEngagementRequested (1U)
 *   GearBoxUnlockAuth_rqst_Error (2U)
 *   GearBoxUnlockAuth_rqst_NotAvaiable (3U)
 * GearboxUnlockAuth_stat_decrypt_T: Enumeration of integer in interval [0...3] with enumerators
 *   GearboxUnlockAuth_stat_decrypt_Idle (0U)
 *   GearboxUnlockAuth_stat_decrypt_GearEngagementAllowed (1U)
 *   GearboxUnlockAuth_stat_decrypt_GearEngagementRefused (2U)
 *   GearboxUnlockAuth_stat_decrypt_Spare1 (3U)
 *   GearboxUnlockAuth_stat_decrypt_Spare2 (4U)
 *   GearboxUnlockAuth_stat_decrypt_Spare3 (5U)
 *   GearboxUnlockAuth_stat_decrypt_Error (6U)
 *   GearboxUnlockAuth_stat_decrypt_NotAvailable (7U)
 * KeyAuthentication_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyAuthentication_rqst_KeyNotPresent (0U)
 *   KeyAuthentication_rqst_KeyIsInserted (1U)
 *   KeyAuthentication_rqst_RequestAuthentication (2U)
 *   KeyAuthentication_rqst_Spare (3U)
 *   KeyAuthentication_rqst_Spare01 (4U)
 *   KeyAuthentication_rqst_Spare02 (5U)
 *   KeyAuthentication_rqst_Error (6U)
 *   KeyAuthentication_rqst_NotAvailable (7U)
 * KeyAuthentication_stat_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyAuthentication_stat_decrypt_KeyNotAuthenticated (0U)
 *   KeyAuthentication_stat_decrypt_KeyAuthenticated (1U)
 *   KeyAuthentication_stat_decrypt_Spare1 (2U)
 *   KeyAuthentication_stat_decrypt_Spare2 (3U)
 *   KeyAuthentication_stat_decrypt_Spare3 (4U)
 *   KeyAuthentication_stat_decrypt_Spare4 (5U)
 *   KeyAuthentication_stat_decrypt_Error (6U)
 *   KeyAuthentication_stat_decrypt_NotAvailable (7U)
 * KeyNotValid_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyNotValid_Idle (0U)
 *   KeyNotValid_KeyNotValid (1U)
 *   KeyNotValid_Error (6U)
 *   KeyNotValid_NotAvailable (7U)
 * KeyfobAuth_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobAuth_rqst_Idle (0U)
 *   KeyfobAuth_rqst_RequestByPassiveMechanism (1U)
 *   KeyfobAuth_rqst_RequestByImmobilizerMechanism (2U)
 *   KeyfobAuth_rqst_Spare1 (3U)
 *   KeyfobAuth_rqst_Spare2 (4U)
 *   KeyfobAuth_rqst_Spare3 (5U)
 *   KeyfobAuth_rqst_Error (6U)
 *   KeyfobAuth_rqst_NotAavailable (7U)
 * KeyfobAuth_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobAuth_stat_Idle (0U)
 *   KeyfobAuth_stat_NokeyfobAuthenticated (1U)
 *   KeyfobAuth_stat_KeyfobAuthenticated (2U)
 *   KeyfobAuth_stat_Spare1 (3U)
 *   KeyfobAuth_stat_Spare2 (4U)
 *   KeyfobAuth_stat_Spare3 (5U)
 *   KeyfobAuth_stat_Error (6U)
 *   KeyfobAuth_stat_NotAvailable (7U)
 * PinCode_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   PinCode_rqst_Idle (0U)
 *   PinCode_rqst_PINCodeNotNeeded (1U)
 *   PinCode_rqst_StatusOfThePinCodeRequested (2U)
 *   PinCode_rqst_PinCodeNeeded (3U)
 *   PinCode_rqst_ResetPinCodeStatus (4U)
 *   PinCode_rqst_Spare (5U)
 *   PinCode_rqst_Error (6U)
 *   PinCode_rqst_NotAvailable (7U)
 * PinCode_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   PinCode_stat_Idle (0U)
 *   PinCode_stat_NoPinCodeEntered (1U)
 *   PinCode_stat_WrongPinCode (2U)
 *   PinCode_stat_GoodPinCode (3U)
 *   PinCode_stat_Error (6U)
 *   PinCode_stat_NotAvailable (7U)
 * SEWS_P1VKG_APM_Check_Active_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_P1VKG_APM_Check_Active_T_No (0U)
 *   SEWS_P1VKG_APM_Check_Active_T_Yes (1U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Crypto_Function_serialized_T: Array with 12 element(s) of type uint8
 * Dcm_Data5ByteType: Array with 5 element(s) of type uint8
 * SEWS_ChassisId_CHANO_T: Array with 16 element(s) of type uint8
 * StandardNVM_T: Array with 4 element(s) of type uint8
 * VIN_stat_T: Array with 11 element(s) of type uint8
 *
 * Record Types:
 * =============
 * SEWS_VINCheckProcessing_P1VKG_s_T: Record with elements
 *   APM_Check_Active of type SEWS_P1VKG_APM_Check_Active_T
 *   MVUC_Check_Active of type SEWS_P1VKG_MVUC_Check_Active_T
 *   EMS_Check_Active of type SEWS_P1VKG_EMS_Check_Active_T
 *   TECU_Check_Active of type SEWS_P1VKG_TECU_Check_Active_T
 *   DISPLAY_Check_Active of type SEWS_P1VKG_DISPLAY_Check_Active_T
 * VINCheckStatus_T: Record with elements
 *   isDISPLAY_CheckPassed of type boolean
 *   isAPM_CheckPassed of type boolean
 *   isVMCU_CheckPassed of type boolean
 *   isEMS_CheckPassed of type boolean
 *   isTECU_CheckPassed of type boolean
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1VKI_PassiveStart_Installed_v(void)
 *   SEWS_VINCheckProcessing_P1VKG_s_T *Rte_Prm_P1VKG_VINCheckProcessing_v(void)
 *   boolean Rte_Prm_P1TTA_GearBoxLockActivation_v(void)
 *   SEWS_CrankingLockActivation_P1DS3_T Rte_Prm_P1DS3_CrankingLockActivation_v(void)
 *   uint8 *Rte_Prm_CHANO_ChassisId_v(void)
 *     Returnvalue: uint8* is of type SEWS_ChassisId_CHANO_T
 *
 *********************************************************************************************************************/


#define DriverAuthentication2_Ctrl_START_SEC_CODE
#include "DriverAuthentication2_Ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VKH_Data_P1VKH_VINCheck_Status>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData_Vincheckstatus(VINCheckStatus_T *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data5ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VKH_Data_P1VKH_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DriverAuthentication2_Ctrl_CODE) DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DriverAuthentication2_Ctrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Receive_ECU1VIN_stat_VIN_stat(uint8 *data)
 *     Argument data: uint8* is of type VIN_stat_T
 *   Std_ReturnType Rte_Receive_ECU2VIN_stat_VIN_stat(uint8 *data)
 *     Argument data: uint8* is of type VIN_stat_T
 *   Std_ReturnType Rte_Receive_ECU3VIN_stat_VIN_stat(uint8 *data)
 *     Argument data: uint8* is of type VIN_stat_T
 *   Std_ReturnType Rte_Receive_ECU4VIN_stat_VIN_stat(uint8 *data)
 *     Argument data: uint8* is of type VIN_stat_T
 *   Std_ReturnType Rte_Receive_ECU5VIN_stat_VIN_stat(uint8 *data)
 *     Argument data: uint8* is of type VIN_stat_T
 *   Std_ReturnType Rte_Read_DeviceAuthentication_rqst_DeviceAuthentication_rqst(DeviceAuthentication_rqst_T *data)
 *   Std_ReturnType Rte_Read_DoorsAjar_stat_DoorsAjar_stat(DoorsAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM(uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Read_EngineStartAuth_rqst_EngineStartAuth_rqst(EngineStartAuth_rqst_T *data)
 *   Std_ReturnType Rte_Read_GearBoxUnlockAuth_rqst_GearBoxUnlockAuth_rqst(GearBoxUnlockAuth_rqst_T *data)
 *   Std_ReturnType Rte_Read_KeyAuthentication_rqst_KeyAuthentication_rqst(KeyAuthentication_rqst_T *data)
 *   Std_ReturnType Rte_Read_KeyfobAuth_stat_KeyfobAuth_stat(KeyfobAuth_stat_T *data)
 *   Std_ReturnType Rte_Read_PinCode_stat_PinCode_stat(PinCode_stat_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DeviceInCab_stat_DeviceInCab_stat(DeviceInCab_stat_T data)
 *   Std_ReturnType Rte_Write_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM(const uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Write_EngineStartAuth_st_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_EngineStartAuth_stat_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_EngineStartAuth_stat_decrypt_EngineStartAuth_stat_decrypt(EngineStartAuth_stat_decrypt_T data)
 *   Std_ReturnType Rte_Write_GearboxUnlockAuth_stat_decrypt_GearboxUnlockAuth_stat_decrypt(GearboxUnlockAuth_stat_decrypt_T data)
 *   Std_ReturnType Rte_Write_GrbxUnlockAuth_stat_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_GrbxUnlockAuth_stat_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_KeyAuth_stat_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_KeyAuth_stat_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt(KeyAuthentication_stat_decrypt_T data)
 *   Std_ReturnType Rte_Write_KeyNotValid_KeyNotValid(KeyNotValid_T data)
 *   Std_ReturnType Rte_Write_KeyfobAuth_rqst_KeyfobAuth_rqst(KeyfobAuth_rqst_T data)
 *   Std_ReturnType Rte_Write_PinCode_rqst_PinCode_rqst(PinCode_rqst_T data)
 *   Std_ReturnType Rte_Write_VIN_rqst_VIN_rqst(VIN_rqst_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DriverAuthentication2_Ctrl_20ms_runnable_Vincheckstatus(const VINCheckStatus_T *data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_UR_ANW_ImmobilizerPINCode_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ImmobilizerPINCode_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DriverAuthentication2_Ctrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DriverAuthentication2_Ctrl_CODE) DriverAuthentication2_Ctrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DriverAuthentication2_Ctrl_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define DriverAuthentication2_Ctrl_STOP_SEC_CODE
#include "DriverAuthentication2_Ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
