/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  MovingUnitTraction_UICtrl.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  MovingUnitTraction_UICtrl
 *  Generation Time:  2020-10-07 15:25:42
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <MovingUnitTraction_UICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AxleConfiguration_P1B16_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_DiffLockSinglePushSwitch_LogicSelection_P1NAK_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FrontAxleArrangement_P1CSH_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_RearAxleDifflockDisregardRequestVehicleSpeed_P1WEB_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_WheelDifferentialLockPushButtonType_P1UG1_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Speed16bit_T
 *   65024 - 65279 Error ; 65280 - 65535 Not available
 *
 *********************************************************************************************************************/

#include "Rte_MovingUnitTraction_UICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_AxleConfiguration_P1B16_T: Integer in interval [0...255]
 * SEWS_DiffLockSinglePushSwitch_LogicSelection_P1NAK_T: Integer in interval [0...255]
 * SEWS_FrontAxleArrangement_P1CSH_T: Integer in interval [0...255]
 * SEWS_RearAxleDifflockDisregardRequestVehicleSpeed_P1WEB_T: Integer in interval [0...255]
 * SEWS_WheelDifferentialLockPushButtonType_P1UG1_T: Integer in interval [0...255]
 * Speed16bit_T: Integer in interval [0...65535]
 *   Unit: [Km_per_h], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * DeactivateActivate_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeactivateActivate_Deactivate (0U)
 *   DeactivateActivate_Activate (1U)
 *   DeactivateActivate_Error (2U)
 *   DeactivateActivate_NotAvailable (3U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DisengageEngage_T: Enumeration of integer in interval [0...3] with enumerators
 *   DisengageEngage_Disengage (0U)
 *   DisengageEngage_Engage (1U)
 *   DisengageEngage_Error (2U)
 *   DisengageEngage_NotAvailable (3U)
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 *   FreeWheel_Status_NoMovement (0U)
 *   FreeWheel_Status_1StepClockwise (1U)
 *   FreeWheel_Status_2StepsClockwise (2U)
 *   FreeWheel_Status_3StepsClockwise (3U)
 *   FreeWheel_Status_4StepsClockwise (4U)
 *   FreeWheel_Status_5StepsClockwise (5U)
 *   FreeWheel_Status_6StepsClockwise (6U)
 *   FreeWheel_Status_1StepCounterClockwise (7U)
 *   FreeWheel_Status_2StepsCounterClockwise (8U)
 *   FreeWheel_Status_3StepsCounterClockwise (9U)
 *   FreeWheel_Status_4StepsCounterClockwise (10U)
 *   FreeWheel_Status_5StepsCounterClockwise (11U)
 *   FreeWheel_Status_6StepsCounterClockwise (12U)
 *   FreeWheel_Status_Spare (13U)
 *   FreeWheel_Status_Error (14U)
 *   FreeWheel_Status_NotAvailable (15U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * TractionControlDriverRqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   TractionControlDriverRqst_NoAction (0U)
 *   TractionControlDriverRqst_TractionON (1U)
 *   TractionControlDriverRqst_TractionOFF (2U)
 *   TractionControlDriverRqst_TractionOFFROAD (3U)
 *   TractionControlDriverRqst_Spare (4U)
 *   TractionControlDriverRqst_Spare_01 (5U)
 *   TractionControlDriverRqst_Error (6U)
 *   TractionControlDriverRqst_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 * Array Types:
 * ============
 * StandardNVM_T: Array with 4 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_AxleConfiguration_P1B16_T Rte_Prm_P1B16_AxleConfiguration_v(void)
 *   SEWS_FrontAxleArrangement_P1CSH_T Rte_Prm_P1CSH_FrontAxleArrangement_v(void)
 *   SEWS_DiffLockSinglePushSwitch_LogicSelection_P1NAK_T Rte_Prm_P1NAK_DiffLockSinglePushSwitch_LogicSelection_v(void)
 *   SEWS_WheelDifferentialLockPushButtonType_P1UG1_T Rte_Prm_P1UG1_WheelDifferentialLockPushButtonType_v(void)
 *   SEWS_RearAxleDifflockDisregardRequestVehicleSpeed_P1WEB_T Rte_Prm_P1WEB_RearAxleDifflockDisregardRequestVehicleSpeed_v(void)
 *   boolean Rte_Prm_P1B03_RearWheelDiffLockPushSw_v(void)
 *   boolean Rte_Prm_P1B04_DLFW_Installed_v(void)
 *   boolean Rte_Prm_P1CUC_ConstructionSw_Act_v(void)
 *   boolean Rte_Prm_P1F7J_ASROffRoadFullVersion_v(void)
 *   boolean Rte_Prm_P1FNW_ASROffButtonInstalled_v(void)
 *   boolean Rte_Prm_P1NQA_AutomaticFrontWheelDrive_Act_v(void)
 *   boolean Rte_Prm_P1SDA_OptitrackSystemInstalled_v(void)
 *
 *********************************************************************************************************************/


#define MovingUnitTraction_UICtrl_START_SEC_CODE
#include "MovingUnitTraction_UICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: MovingUnitTraction_UICtrl_Difflock_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_DifflockDeactivationBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_DifflockMode_Wheelstatus_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I(uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Read_RearAxleDiffLock_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_AutoFrontWheelDriveDrvReq_AutoFrontWheelDriveDrvReq(DisengageEngage_T data)
 *   Std_ReturnType Rte_Write_DifflockOnOff_Indication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_FrAxleDiffLockActvnDriverRqst_FrAxleDiffLockActvnDriverRqst(DisengageEngage_T data)
 *   Std_ReturnType Rte_Write_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I(const uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Write_RearAxleDiffLockActvnDrvrReq_RearAxleDiffLockActvnDrvrReq(DisengageEngage_T data)
 *   Std_ReturnType Rte_Write_RearDiffLock_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_RrInterAxlDiffLockActvnDrvrReq_RrInterAxlDiffLockActvnDrvrReq(DisengageEngage_T data)
 *   Std_ReturnType Rte_Write_SplitBoxDifflockActvnDrvrRqst_TransferBDifflockActvnDrvrRqst(DisengageEngage_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: MovingUnitTraction_UICtrl_Difflock_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, MovingUnitTraction_UICtrl_CODE) MovingUnitTraction_UICtrl_Difflock_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: MovingUnitTraction_UICtrl_Difflock_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: MovingUnitTraction_UICtrl_Traction_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ASROffButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_Construction_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FrtAxleHydroActive_Status_FrtAxleHydroActive_Status(DeactivateActivate_T *data)
 *   Std_ReturnType Rte_Read_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I(uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Read_Offroad_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ASROff_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_ConstructionSwitch_DeviceInd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_ConstructionSwitch_stat_ConstructionSwitch_stat(OffOn_T data)
 *   Std_ReturnType Rte_Write_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I(const uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Write_Offroad_Indication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_TractionControlDriverRqst_TractionControlDriverRqst(TractionControlDriverRqst_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BOW_63_TractionSw_Stuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: MovingUnitTraction_UICtrl_Traction_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, MovingUnitTraction_UICtrl_CODE) MovingUnitTraction_UICtrl_Traction_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: MovingUnitTraction_UICtrl_Traction_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define MovingUnitTraction_UICtrl_STOP_SEC_CODE
#include "MovingUnitTraction_UICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
