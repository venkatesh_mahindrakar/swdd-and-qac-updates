/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  VEC_RomTest.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  VEC_RomTest
 *  Generation Time:  2020-09-21 13:44:23
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <VEC_RomTest>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * AsymPublicKeyType
 *   This type is used for Csm key parameters of type AsymPublicKey
 *
 * Csm_ReturnType
 *   The data type Csm_ReturnType indicates the result of a service request.
 *
 * Csm_VerifyResultType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SignatureVerifyDataBuffer
 *   Used as Buffer for service.
 *
 *
 * Operation Prototypes:
 * =====================
 * SignatureVerifyFinish of Port Interface CsmSignatureVerify
 *   This interface shall be used to finish the signature verification service.
 *
 * SignatureVerifyStart of Port Interface CsmSignatureVerify
 *   This interface shall be used to initialize the signature verification service of the CSM module.
 *
 * SignatureVerifyUpdate of Port Interface CsmSignatureVerify
 *   This interface shall be used to feed the signature verification service with the input data.
 *
 *********************************************************************************************************************/

#include "Rte_VEC_RomTest.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * UInt8: Integer in interval [0.0...0.0]
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Csm_ReturnType: Enumeration of integer in interval [0...255] with enumerators
 *   CSM_E_OK (0U)
 *   CSM_E_NOT_OK (1U)
 *   CSM_E_BUSY (2U)
 *   CSM_E_SMALL_BUFFER (3U)
 *   CSM_E_ENTROPY_EXHAUSTION (4U)
 * Csm_VerifyResultType: Enumeration of integer in interval [0...1] with enumerators
 *   CSM_E_VER_OK (0U)
 *   CSM_E_VER_NOT_OK (1U)
 * tVecRomTestBlockStatus: Enumeration of integer in interval [0...255] with enumerators
 *   kVecRomTestValid (1U)
 *   kVecRomTestReadFailure (2U)
 *   kVecRomTestSignatureFailure (3U)
 *
 * Array Types:
 * ============
 * Rte_DT_AsymPublicKeyType_1: Array with 128 element(s) of type uint8
 * SignatureVerifyDataBuffer: Array with 128 element(s) of type uint8
 *
 * Record Types:
 * =============
 * AsymPublicKeyType: Record with elements
 *   length of type uint32
 *   data of type Rte_DT_AsymPublicKeyType_1
 *
 *********************************************************************************************************************/


#define VEC_RomTest_START_SEC_CODE
#include "VEC_RomTest_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CallbackNotification
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <JobFinished> of PortPrototype <CsmCallbackSignatureVerify>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_CallbackNotification(Csm_ReturnType retVal)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmCallback_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CallbackNotification_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_RomTest_CODE) VEC_CallbackNotification(Csm_ReturnType retVal) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CallbackNotification (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_GetBlockStatus
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetBlockStatus> of PortPrototype <VEC_RomTestStatus>
 *
 **********************************************************************************************************************
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_VEC_ExclusiveArea(void)
 *   void Rte_Exit_VEC_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_GetBlockStatus(UInt8 blockNr, tVecRomTestBlockStatus *status)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_VEC_RomTestStatus_RTE_E_VEC_BlockStatus_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_GetBlockStatus_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_RomTest_CODE) VEC_GetBlockStatus(UInt8 blockNr, P2VAR(tVecRomTestBlockStatus, AUTOMATIC, RTE_VEC_ROMTEST_APPL_VAR) status) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_GetBlockStatus (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_RomTest_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 **********************************************************************************************************************
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_VEC_ExclusiveArea(void)
 *   void Rte_Exit_VEC_ExclusiveArea(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_RomTest_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_RomTest_CODE) VEC_RomTest_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_RomTest_Init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_RomTest_MainFunction
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmSignatureVerify_SignatureVerifyFinish(const uint8 *signatureBuffer, uint32 signatureLength, Csm_VerifyResultType *resultBuffer)
 *     Argument signatureBuffer: uint8* is of type SignatureVerifyDataBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSignatureVerify_CSM_E_BUSY, RTE_E_CsmSignatureVerify_CSM_E_NOT_OK, RTE_E_CsmSignatureVerify_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmSignatureVerify_SignatureVerifyStart(const AsymPublicKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSignatureVerify_CSM_E_BUSY, RTE_E_CsmSignatureVerify_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmSignatureVerify_SignatureVerifyUpdate(const uint8 *dataBuffer, uint32 dataLength)
 *     Argument dataBuffer: uint8* is of type SignatureVerifyDataBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSignatureVerify_CSM_E_BUSY, RTE_E_CsmSignatureVerify_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_VEC_ExclusiveArea(void)
 *   void Rte_Exit_VEC_ExclusiveArea(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_RomTest_MainFunction_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_RomTest_CODE) VEC_RomTest_MainFunction(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_RomTest_MainFunction
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define VEC_RomTest_STOP_SEC_CODE
#include "VEC_RomTest_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
