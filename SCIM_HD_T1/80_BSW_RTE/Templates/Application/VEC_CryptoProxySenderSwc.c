/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  VEC_CryptoProxySenderSwc.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  VEC_CryptoProxySenderSwc
 *  Generation Time:  2020-09-21 13:44:20
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <VEC_CryptoProxySenderSwc>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   Boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * SEWS_ComCryptoKey_P1DLX_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SymDecryptDataBuffer
 *   Used as Buffer for service.
 *
 * SymDecryptResultBuffer
 *   Used as Buffer for service.
 *
 * SymEncryptDataBuffer
 *   Used as Buffer for service.
 *
 * SymEncryptResultBuffer
 *   Used as Buffer for service.
 *
 * SymKeyType
 *   This type is used for Csm key parameters of type SymKey
 *
 * UInt16
 *   UInt16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on UInt16 is: x < y if y - x is positive.
 *      UInt16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * UInt32
 *   UInt32 represents integers with a minimum value of 0 and a maximum value 
 *      of 4294967295. The order-relation on UInt32 is: x < y if y - x is positive.
 *      UInt32 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39). 
 *      
 *      For example: 1, 0, 12234567, 104400.
 *
 *
 * Operation Prototypes:
 * =====================
 * SymDecryptFinish of Port Interface CsmSymDecrypt
 *   This interface shall be used to finish the symmetrical decryption service.
 *
 * SymDecryptStart of Port Interface CsmSymDecrypt
 *   This interface shall be used to initialize the symmetrical decryption service of the CSM module.
 *
 * SymDecryptUpdate of Port Interface CsmSymDecrypt
 *   This interface shall be used to feed the symmetrical decryption service with the input data.
 *
 * SymEncryptFinish of Port Interface CsmSymEncrypt
 *   This interface shall be used to finish the symmetrical encryption service.
 *
 * SymEncryptStart of Port Interface CsmSymEncrypt
 *   This interface shall be used to initialize the symmetrical encryption service of the CSM module.
 *
 * SymEncryptUpdate of Port Interface CsmSymEncrypt
 *   This interface shall be used to feed the symmetrical encryption service with the input data.
 *
 *********************************************************************************************************************/

#include "Rte_VEC_CryptoProxySenderSwc.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_ComCryptoKey_P1DLX_T: Integer in interval [0...255]
 * SEWS_ComCryptoKey_P1DLX_T: Integer in interval [0...255]
 * UInt16: Integer in interval [0...65535]
 * UInt32: Integer in interval [0...4294967295]
 * UInt32_Length: Integer in interval [0...4294967295]
 * UInt8: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Array Types:
 * ============
 * CryptoIdKey_T: Array with 16 element(s) of type uint8
 * Encrypted128bit_T: Array with 16 element(s) of type uint8
 * Rte_DT_SymKeyType_1: Array with 256 element(s) of type UInt8
 * SEWS_ComCryptoKey_P1DLX_a_T: Array with 16 element(s) of type SEWS_ComCryptoKey_P1DLX_T
 * SEWS_ComCryptoKey_P1DLX_a_T: Array with 16 element(s) of type SEWS_ComCryptoKey_P1DLX_T
 * SymDecryptDataBuffer: Array with 128 element(s) of type UInt8
 * SymDecryptResultBuffer: Array with 128 element(s) of type UInt8
 * SymEncryptDataBuffer: Array with 128 element(s) of type UInt8
 * SymEncryptResultBuffer: Array with 128 element(s) of type UInt8
 * VEC_CryptoProxy_UserSignal: Array with 12 element(s) of type UInt8
 *
 * Record Types:
 * =============
 * SymKeyType: Record with elements
 *   length of type UInt32_Length
 *   data of type Rte_DT_SymKeyType_1
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   UInt16 *Rte_Pim_VEC_CryptoProxy_CycleTimer(Rte_Instance self)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   UInt16 Rte_CData_VEC_CryptoProxyCycleFactor(Rte_Instance self)
 *   UInt16 Rte_CData_VEC_CryptoProxyCycleOffset(Rte_Instance self)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_ComCryptoKey_P1DLX_T *Rte_Prm_ComCryptoKey_P1DLX_v(Rte_Instance self)
 *     Returnvalue: SEWS_ComCryptoKey_P1DLX_T* is of type SEWS_ComCryptoKey_P1DLX_a_T
 *
 *********************************************************************************************************************/


#define VEC_CryptoProxySenderSwc_START_SEC_CODE
#include "VEC_CryptoProxySenderSwc_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CryptoProxySenderConfirmation
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on DataSendCompletedEvent for DataElementPrototype <EncryptedSignal> of PortPrototype <VEC_EncryptedSignal>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   UInt32 Rte_IrvRead_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(Rte_Instance self)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(Rte_Instance self, UInt32 data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderConfirmation_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_CryptoProxySenderSwc_CODE) VEC_CryptoProxySenderConfirmation(Rte_Instance self) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderConfirmation
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CryptoProxySenderMainFunction
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(Rte_Instance self, uint8 *data)
 *     Argument data: uint8* is of type CryptoIdKey_T
 *   Std_ReturnType Rte_Read_CryptoTrigger_CryptoTrigger(Rte_Instance self, Boolean *data)
 *   Std_ReturnType Rte_Read_VEC_CryptoProxySerializedData_Crypto_Function_serialized(Rte_Instance self, UInt8 *data)
 *     Argument data: UInt8* is of type VEC_CryptoProxy_UserSignal
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Send_VEC_EncryptedSignal_EncryptedSignal(Rte_Instance self, const uint8 *data)
 *     Argument data: uint8* is of type Encrypted128bit_T
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   UInt32 Rte_IrvRead_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(Rte_Instance self)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(Rte_Instance self, UInt32 data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmSymEncrypt_SymEncryptFinish(Rte_Instance self, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type SymEncryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymEncrypt_CSM_E_BUSY, RTE_E_CsmSymEncrypt_CSM_E_NOT_OK, RTE_E_CsmSymEncrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmSymEncrypt_SymEncryptStart(Rte_Instance self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength)
 *     Argument InitVectorBuffer: UInt8* is of type SymEncryptDataBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymEncrypt_CSM_E_BUSY, RTE_E_CsmSymEncrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmSymEncrypt_SymEncryptUpdate(Rte_Instance self, const UInt8 *plainTextBuffer, UInt32_Length plainTextLength, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength)
 *     Argument plainTextBuffer: UInt8* is of type SymEncryptDataBuffer
 *     Argument cipherTextBuffer: UInt8* is of type SymEncryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymEncrypt_CSM_E_BUSY, RTE_E_CsmSymEncrypt_CSM_E_NOT_OK, RTE_E_CsmSymEncrypt_CSM_E_SMALL_BUFFER
 *
 * Status Interfaces:
 * ==================
 *   Tx Acknowledge:
 *   ----------------
 *   Std_ReturnType Rte_Feedback_VEC_EncryptedSignal_EncryptedSignal(Rte_Instance self)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderMainFunction_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_CryptoProxySenderSwc_CODE) VEC_CryptoProxySenderMainFunction(Rte_Instance self) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderMainFunction
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CryptoProxySenderReception
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on DataReceivedEvent for DataElementPrototype <CryptoIdKey> of PortPrototype <VEC_CryptoIdKey>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(Rte_Instance self, uint8 *data)
 *     Argument data: uint8* is of type CryptoIdKey_T
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   UInt32 Rte_IrvRead_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(Rte_Instance self)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(Rte_Instance self, UInt32 data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmSymDecrypt_SymDecryptFinish(Rte_Instance self, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type SymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymDecrypt_CSM_E_BUSY, RTE_E_CsmSymDecrypt_CSM_E_NOT_OK, RTE_E_CsmSymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmSymDecrypt_SymDecryptStart(Rte_Instance self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength)
 *     Argument InitVectorBuffer: UInt8* is of type SymDecryptDataBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymDecrypt_CSM_E_BUSY, RTE_E_CsmSymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmSymDecrypt_SymDecryptUpdate(Rte_Instance self, const UInt8 *cipherTextBuffer, UInt32_Length cipherTextLength, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type SymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type SymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymDecrypt_CSM_E_BUSY, RTE_E_CsmSymDecrypt_CSM_E_NOT_OK, RTE_E_CsmSymDecrypt_CSM_E_SMALL_BUFFER
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderReception_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_CryptoProxySenderSwc_CODE) VEC_CryptoProxySenderReception(Rte_Instance self) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderReception
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CryptoProxySenderSwc_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderSwc_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_CryptoProxySenderSwc_CODE) VEC_CryptoProxySenderSwc_Init(Rte_Instance self) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderSwc_Init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define VEC_CryptoProxySenderSwc_STOP_SEC_CODE
#include "VEC_CryptoProxySenderSwc_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
