/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  BunkUserInterfaceBasic_LINMaCtrl.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  BunkUserInterfaceBasic_LINMaCtrl
 *  Generation Time:  2020-09-21 13:44:20
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <BunkUserInterfaceBasic_LINMaCtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_BunkUserInterfaceBasic_LINMaCtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DiagInfo_T: Integer in interval [0...127]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * ResponseErrorLECMBasic_T: Boolean
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1CAQ_LECML_Installed_v(void)
 *
 *********************************************************************************************************************/


#define BunkUserInterfaceBasic_LINMaCtrl_START_SEC_CODE
#include "BunkUserInterfaceBasic_LINMaCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: BunkUserInterfaceBasic_LINMaCtrl_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DiagInfoLECMBasic_DiagInfo(DiagInfo_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkBAudioOnOff_ButtonStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkBIntLightActvnBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkBParkHeater_ButtonStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkBTempDec_ButtonStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkBTempInc_ButtonStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkBVolumeDown_ButtonStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkBVolumeUp_ButtonStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_ResponseErrorLECMBasic_ResponseErrorLECMBasic(ResponseErrorLECMBasic_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_BunkBAudioOnOff_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkBIntLightActvnBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkBParkHeater_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkBTempDec_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkBTempInc_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkBVolumeDown_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkBVolumeUp_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BKG_87_LECMLowLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOG_16_LECMLow_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOG_17_LECMLow_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOG_46_LECMLow_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOG_94_LECMLow_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_AudioRadio3_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_AudioRadio3_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights5_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights5_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_PHActMaintainLiving5_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_PHActMaintainLiving5_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: BunkUserInterfaceBasic_LINMaCtrl_10ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, BunkUserInterfaceBasic_LINMaCtrl_CODE) BunkUserInterfaceBasic_LINMaCtrl_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: BunkUserInterfaceBasic_LINMaCtrl_10ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define BunkUserInterfaceBasic_LINMaCtrl_STOP_SEC_CODE
#include "BunkUserInterfaceBasic_LINMaCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
