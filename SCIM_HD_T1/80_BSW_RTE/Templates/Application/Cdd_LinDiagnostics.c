/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Cdd_LinDiagnostics.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  Cdd_LinDiagnostics
 *  Generation Time:  2020-09-21 13:44:20
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <Cdd_LinDiagnostics>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_LIN_topology_P1AJR_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_Cdd_LinDiagnostics.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_LIN_topology_P1AJR_T: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * CddLinTp_Status: Enumeration of integer in interval [0...255] with enumerators
 *   CDD_LIN_NOT_OK (11U)
 *   CDD_LIN_TX_OK (0U)
 *   CDD_LIN_TX_BUSY (1U)
 *   CDD_LIN_TX_HEADER_ERROR (2U)
 *   CDD_LIN_TX_ERROR (3U)
 *   CDD_LIN_RX_OK (4U)
 *   CDD_LIN_RX_BUSY (5U)
 *   CDD_LIN_RX_ERROR (6U)
 *   CDD_LIN_RX_NO_RESPONSE (7U)
 *   CDD_LIN_NONE (9U)
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * LinDiagBusInfo: Enumeration of integer in interval [0...255] with enumerators
 *   None (0U)
 *   LinDiag_BUS1 (1U)
 *   LinDiag_BUS2 (2U)
 *   LinDiag_BUS3 (3U)
 *   LinDiag_BUS4 (4U)
 *   LinDiag_BUS5 (5U)
 *   LinDiag_SpareBUS1 (6U)
 *   LinDiag_SpareBUS2 (7U)
 *   LinDiag_ALL_BUSSES (8U)
 * LinDiagRequest_T: Enumeration of integer in interval [0...255] with enumerators
 *   NO_OPEARATION (0U)
 *   START_OPEARATION (1U)
 *   STOP_OPERATION (2U)
 * LinDiagServiceStatus: Enumeration of integer in interval [0...255] with enumerators
 *   LinDiagService_None (0U)
 *   LinDiagService_Pending (1U)
 *   LinDiagService_Completed (2U)
 *   LinDiagService_Error (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_LIN_topology_P1AJR_T Rte_Prm_P1AJR_LIN_topology_v(void)
 *
 *********************************************************************************************************************/


#define Cdd_LinDiagnostics_START_SEC_CODE
#include "Cdd_LinDiagnostics_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CddLinDiagServices_FSPAssignReq
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FSPAssignReq> of PortPrototype <CddLinDiagServices>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_LinDiagRequestFlag_CCNADRequest(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void CddLinDiagServices_FSPAssignReq(LinDiagBusInfo LinBusInfo, LinDiagRequest_T RequestType)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinDiagServices_FSPAssignReq_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinDiagnostics_CODE) CddLinDiagServices_FSPAssignReq(LinDiagBusInfo LinBusInfo, LinDiagRequest_T RequestType) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinDiagServices_FSPAssignReq
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CddLinDiagServices_FSPAssignResp
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FSPAssignResp> of PortPrototype <CddLinDiagServices>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void CddLinDiagServices_FSPAssignResp(LinDiagServiceStatus *pDiagServiceStatus, uint8 *pAvailableFSPCount, uint8 *pFspErrorStatus, uint8 *pFspNvData)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinDiagServices_FSPAssignResp_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinDiagnostics_CODE) CddLinDiagServices_FSPAssignResp(P2VAR(LinDiagServiceStatus, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) pDiagServiceStatus, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) pAvailableFSPCount, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) pFspErrorStatus, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) pFspNvData) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinDiagServices_FSPAssignResp
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CddLinDiagServices_SlaveNodePnSnReq
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SlaveNodePnSnReq> of PortPrototype <CddLinDiagServices>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_LinDiagRequestFlag_PNSNRequest(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void CddLinDiagServices_SlaveNodePnSnReq(LinDiagBusInfo LinBusInfo)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinDiagServices_SlaveNodePnSnReq_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinDiagnostics_CODE) CddLinDiagServices_SlaveNodePnSnReq(LinDiagBusInfo LinBusInfo) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinDiagServices_SlaveNodePnSnReq
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CddLinDiagServices_SlaveNodePnSnResp
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SlaveNodePnSnResp> of PortPrototype <CddLinDiagServices>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void CddLinDiagServices_SlaveNodePnSnResp(LinDiagServiceStatus *DiagServiceStatus, uint8 *NoOfLinSlaves, uint8 *LinDiagRespPNSN)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinDiagServices_SlaveNodePnSnResp_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinDiagnostics_CODE) CddLinDiagServices_SlaveNodePnSnResp(P2VAR(LinDiagServiceStatus, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) DiagServiceStatus, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) NoOfLinSlaves, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) LinDiagRespPNSN) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinDiagServices_SlaveNodePnSnResp
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CddLinRxHandling_ReceiveIndication
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReceiveIndication> of PortPrototype <CddLinRxHandling>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void CddLinRxHandling_ReceiveIndication(uint8 TxId, uint8 SubServiceId, uint8 *RxData, uint8 Length, CddLinTp_Status Status)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinRxHandling_ReceiveIndication_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinDiagnostics_CODE) CddLinRxHandling_ReceiveIndication(uint8 TxId, uint8 SubServiceId, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) RxData, uint8 Length, CddLinTp_Status Status) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CddLinRxHandling_ReceiveIndication
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinDiagnostics_10ms_Runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN2_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN3_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN5_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DiagActiveState_P_isDiagActive(DiagActiveState_T *data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_CddLinClearRequest_ClearRequest(void)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_CddLinTxHandling_Transmit(uint8 TxId, uint8 *TxData, uint8 Length)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinDiagnostics_10ms_Runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinDiagnostics_CODE) Cdd_LinDiagnostics_10ms_Runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinDiagnostics_10ms_Runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define Cdd_LinDiagnostics_STOP_SEC_CODE
#include "Cdd_LinDiagnostics_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
