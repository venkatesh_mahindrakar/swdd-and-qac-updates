/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  FMSGateway.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  FMSGateway
 *  Generation Time:  2020-10-07 15:25:40
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <FMSGateway>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * AxleLoad_T
 *   65024 - 65279 Error ; 65280 - 65535 Not available
 *
 * Blockid_T
 *   15: Don't care
 *
 * Distance32bit_T
 *   4261412864 - 4278190079 Error ; 4278190080 - 4294967295 Not available
 *
 * EngineTemp_T
 *   254 Error ; 255 Not available
 *
 * FuelRate_T
 *   65024 - 65279 Error ; 65280 - 65535 Not available
 *
 * GasRate_T
 *   65024 - 65279 Error ; 65280 - 65535 Not available
 *
 * InstantFuelEconomy_T
 *   64256 - 65023 Spare
 *      65024 - 65279 Error
 *      65280 - 65535 NotAvailable
 *
 * Int8Bit_T
 *   255 Not Available
 *
 * Percent8bit125NegOffset_T
 *   254 Error ; 255 Not available
 *
 * Percent8bitFactor04_T
 *   254 Error ; 255 Not available
 *
 * Percent8bitNoOffset_T
 *   254 Error ; 255 Not available
 *
 * PressureFactor8_T
 *   254 Error ; 255 Not available
 *
 * RemainDistOilChange_T
 *   65024 - 65279 Error ; 65280 - 65535 Not available
 *
 * RemainEngineTimeOilChange_T
 *   65024 - 65279 Error ; 65280 - 65535 Not available
 *
 * SEWS_CabHeightValue_P1R0P_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FuelTypeInformation_P1M7T_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_LNGTank1Volume_P1LX9_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_LNGTank2Volume_P1LYA_T
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * SEWS_WeightClassInformation_P1M7S_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Seconds32bitExtra_T
 *   4294967295 Not available
 *
 * ServiceDistance16BitFact5Offset_T
 *   64256 - 65023 Spare
 *      65024 - 65279 Error
 *      65280 - 65535 NotAvailable
 *
 * ServiceDistance_T
 *   4261412864 - 4278190079 Error ; 4278190080 - 4294967295 Not Available
 *
 * ServiceTime_T
 *   4261412864 - 4278190079 Error ; 4278190080 - 4294967295 Not available
 *
 * Speed16bit_T
 *   65024 - 65279 Error ; 65280 - 65535 Not available
 *
 * SpeedRpm16bit_T
 *   65024 - 65279 Error ; 65280 - 65535 Not available
 *
 * Temperature16bit_T
 *   65024 - 65279 Error ; 65280 - 65535 Not available
 *
 * TotalLngConsumed_T
 *   4211081216 - 4261412863 Spare; 4261412864 - 4278190079 Error; 4278190080 - 4294967295 NotAvailable
 *
 * VehicleWeight16bit_T
 *   65024 - 65279 Error ; 65280 - 65535 Not available
 *
 * Volume32BitFact001_T
 *   4211081216 - 4261412863 Spare
 *      4261412864 - 4278190079 Error
 *      4278190080 - 4294967295 NotAvailable
 *
 * Volume32bitFact05_T
 *   4211081216 - 4261412863 Spare
 *      4261412864 - 4278190079 Error
 *      4278190080 - 4294967295 Not Available
 *
 * Volume32bit_T
 *   4261412864 - 4278190079 Error ; 4278190080 - 4294967295 Not available
 *
 *********************************************************************************************************************/

#include "Rte_FMSGateway.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * AxleLoad_T: Integer in interval [0...65535]
 *   Unit: [kg], Factor: 1, Offset: 0
 * Blockid_T: Integer in interval [0...15]
 *   Factor: 1, Offset: 0
 * Distance32bit_T: Integer in interval [0...4294967295]
 *   Unit: [m], Factor: 1, Offset: 0
 * EngineTemp_T: Integer in interval [0...255]
 *   Unit: [DegreeC], Factor: 1, Offset: -40
 * EngineTotalHoursOfOperation_T: Integer in interval [0...4294967295]
 *   Unit: [h], Factor: 1, Offset: 0
 * FuelRate_T: Integer in interval [0...65535]
 *   Unit: [l_per_h], Factor: 1, Offset: 0
 * GasRate_T: Integer in interval [0...65535]
 *   Unit: [Kg_per_h], Factor: 1, Offset: 0
 * InstantFuelEconomy_T: Integer in interval [0...65535]
 *   Unit: [km_per_l], Factor: 1, Offset: 0
 * Int8Bit_T: Integer in interval [0...255]
 * MaintServiceID_T: Integer in interval [0...255]
 *   Unit: [ID], Factor: 1, Offset: 0
 * PGNRequest_T: Integer in interval [0...16777215]
 * Percent8bit125NegOffset_T: Integer in interval [0...255]
 *   Unit: [Percent], Factor: 1, Offset: -125
 * Percent8bitFactor04_T: Integer in interval [0...255]
 *   Unit: [Percent], Factor: 1, Offset: 0
 * Percent8bitNoOffset_T: Integer in interval [0...255]
 *   Unit: [Percent], Factor: 1, Offset: 0
 * PressureFactor8_T: Integer in interval [0...255]
 *   Unit: [kPa], Factor: 1, Offset: 0
 * RemainDistOilChange_T: Integer in interval [0...65535]
 *   Unit: [km], Factor: 1, Offset: 0
 * RemainEngineTimeOilChange_T: Integer in interval [0...65535]
 *   Unit: [h], Factor: 1, Offset: 0
 * SEWS_CabHeightValue_P1R0P_T: Integer in interval [0...255]
 * SEWS_FuelTypeInformation_P1M7T_T: Integer in interval [0...255]
 * SEWS_LNGTank1Volume_P1LX9_T: Integer in interval [0...65535]
 * SEWS_LNGTank2Volume_P1LYA_T: Integer in interval [0...65535]
 * SEWS_WeightClassInformation_P1M7S_T: Integer in interval [0...255]
 * Seconds32bitExtra_T: Integer in interval [0...4294967295]
 *   Unit: [s], Factor: 1, Offset: 0
 * ServiceDistance16BitFact5Offset_T: Integer in interval [0...65535]
 *   Unit: [km], Factor: 1, Offset: -160635
 * ServiceDistance_T: Integer in interval [0...4294967295]
 *   Unit: [m], Factor: 1, Offset: -21474836000
 * ServiceTime_T: Integer in interval [0...4294967295]
 *   Unit: [s], Factor: 1, Offset: -2147483600
 * Speed16bit_T: Integer in interval [0...65535]
 *   Unit: [Km_per_h], Factor: 1, Offset: 0
 * SpeedRpm16bit_T: Integer in interval [0...65535]
 *   Unit: [rpm], Factor: 1, Offset: 0
 * Temperature16bit_T: Integer in interval [0...65535]
 *   Unit: [DegreeC], Factor: 1, Offset: -273
 * TotalLngConsumed_T: Integer in interval [0...4294967295]
 *   Unit: [kg], Factor: 1, Offset: 0
 * VehicleWeight16bit_T: Integer in interval [0...65535]
 *   Unit: [kg], Factor: 1, Offset: 0
 * Volume32BitFact001_T: Integer in interval [0...4294967295]
 *   Unit: [l], Factor: 1, Offset: 0
 * Volume32bitFact05_T: Integer in interval [0...4294967295]
 *   Unit: [l], Factor: 1, Offset: 0
 * Volume32bit_T: Integer in interval [0...4294967295]
 *   Unit: [l], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BrakeSwitch_T: Enumeration of integer in interval [0...3] with enumerators
 *   BrakeSwitch_BrakePedalReleased (0U)
 *   BrakeSwitch_BrakePedalDepressed (1U)
 *   BrakeSwitch_Error (2U)
 *   BrakeSwitch_NotAvailable (3U)
 * ClutchSwitch_T: Enumeration of integer in interval [0...3] with enumerators
 *   ClutchSwitch_ClutchPedalReleased (0U)
 *   ClutchSwitch_ClutchPedalDepressed (1U)
 *   ClutchSwitch_Error (2U)
 *   ClutchSwitch_NotAvailable (3U)
 * DirectionIndicator_T: Enumeration of integer in interval [0...3] with enumerators
 *   DirectionIndicator_Forward (0U)
 *   DirectionIndicator_Reverse (1U)
 *   DirectionIndicator_Error (2U)
 *   DirectionIndicator_NotAvailable (3U)
 * Driver1TimeRelatedStates_T: Enumeration of integer in interval [0...15] with enumerators
 *   Driver1TimeRelatedStates_NormalNoLimitsReached (0U)
 *   Driver1TimeRelatedStates_15minBefore4h30min (1U)
 *   Driver1TimeRelatedStates_4h30minReached (2U)
 *   Driver1TimeRelatedStates_DailyDrivingTimePreWarning (3U)
 *   Driver1TimeRelatedStates_DailyDrivingTimeWarning (4U)
 *   Driver1TimeRelatedStates_DailyWeeklyRestPreWarning (5U)
 *   Driver1TimeRelatedStates_DailyWeeklyRestWarning (6U)
 *   Driver1TimeRelatedStates_WeeklyDrivingTimePreWarning (7U)
 *   Driver1TimeRelatedStates_WeeklyDrivingTimeWarning (8U)
 *   Driver1TimeRelatedStates_2WeekDrivingTimePreWarning (9U)
 *   Driver1TimeRelatedStates_2WeekDrivingTimeWarning (10U)
 *   Driver1TimeRelatedStates_Driver1CardExpiryWarning (11U)
 *   Driver1TimeRelatedStates_NextMandatoryDriver1CardDownload (12U)
 *   Driver1TimeRelatedStates_Other (13U)
 *   Driver1TimeRelatedStates_Error (14U)
 *   Driver1TimeRelatedStates_NotAvailable (15U)
 * DriverTimeRelatedStates_T: Enumeration of integer in interval [0...15] with enumerators
 *   DriverTimeRelatedStates_NormalNoLimitsReached (0U)
 *   DriverTimeRelatedStates_Limit1_15minBefore4h30min (1U)
 *   DriverTimeRelatedStates_Limit2_4h30minReached (2U)
 *   DriverTimeRelatedStates_Limit3_15MinutesBefore9h (3U)
 *   DriverTimeRelatedStates_Limit4_9hReached (4U)
 *   DriverTimeRelatedStates_Limit5_15MinutesBefore16h (5U)
 *   DriverTimeRelatedStates_Limit6_16hReached (6U)
 *   DriverTimeRelatedStates_Reserved (7U)
 *   DriverTimeRelatedStates_Reserved_01 (8U)
 *   DriverTimeRelatedStates_Reserved_02 (9U)
 *   DriverTimeRelatedStates_Reserved_03 (10U)
 *   DriverTimeRelatedStates_Reserved_04 (11U)
 *   DriverTimeRelatedStates_Reserved_05 (12U)
 *   DriverTimeRelatedStates_Other (13U)
 *   DriverTimeRelatedStates_Error (14U)
 *   DriverTimeRelatedStates_NotAvailable (15U)
 * DriverWorkingState_T: Enumeration of integer in interval [0...7] with enumerators
 *   DriverWorkingState_RestSleeping (0U)
 *   DriverWorkingState_DriverAvailableShortBreak (1U)
 *   DriverWorkingState_WorkLoadingUnloadingWorkingInAnOffice (2U)
 *   DriverWorkingState_DriveBehindWheel (3U)
 *   DriverWorkingState_Reserved (4U)
 *   DriverWorkingState_Reserved_01 (5U)
 *   DriverWorkingState_ErrorIndicator (6U)
 *   DriverWorkingState_NotAvailable (7U)
 * EngineRetarderTorqueMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   EngineRetarderTorqueMode_LowIdleGovernorNoRequestDefaultMode (0U)
 *   EngineRetarderTorqueMode_AcceleratorPedalOperatorSelection (1U)
 *   EngineRetarderTorqueMode_CruiseControl (2U)
 *   EngineRetarderTorqueMode_PTOGovernor (3U)
 *   EngineRetarderTorqueMode_RoadSpeedGovernor (4U)
 *   EngineRetarderTorqueMode_ASRControl (5U)
 *   EngineRetarderTorqueMode_TransmissionControl (6U)
 *   EngineRetarderTorqueMode_ABSControl (7U)
 *   EngineRetarderTorqueMode_TorqueLimiting (8U)
 *   EngineRetarderTorqueMode_HighSpeedGovernor (9U)
 *   EngineRetarderTorqueMode_BrakingSystem (10U)
 *   EngineRetarderTorqueMode_RemoteAccelerator (11U)
 *   EngineRetarderTorqueMode_ServiceProcedure (12U)
 *   EngineRetarderTorqueMode_NotDefined (13U)
 *   EngineRetarderTorqueMode_Other (14U)
 *   EngineRetarderTorqueMode_NotAvailable (15U)
 * FuelType_T: Enumeration of integer in interval [0...255] with enumerators
 *   FuelType_NotAvailable_NONE (0U)
 *   FuelType_GasolinePetrol_GAS (1U)
 *   FuelType_Methanol_METH (2U)
 *   FuelType_Ethanol_ETH (3U)
 *   FuelType_Diesel_DSL (4U)
 *   FuelType_LiquefiedPetroleumGas_LPG (5U)
 *   FuelType_CompressedNaturalGas_CNG (6U)
 *   FuelType_Propane_PROP (7U)
 *   FuelType_BatteryElectric_ELEC (8U)
 *   FuelType_BifuelVehicleGasoline_BI_GAS (9U)
 *   FuelType_BifuelVehicleMethanol_BI_METH (10U)
 *   FuelType_BifuelVehicleEthanol_BI_ETH (11U)
 *   FuelType_BifuelVehicleLPG_BI_LPG (12U)
 *   FuelType_BifuelVehicleCNG_BI_CNG (13U)
 *   FuelType_BifuelVehiclePropane_BI_PROP (14U)
 *   FuelType_BifuelVehicleBattery_BI_ELEC (15U)
 *   FuelType_BifuelVehicleBatteryCombustion_BI_MIX (16U)
 *   FuelType_HybridVehicleGasoline_HYB_GAS (17U)
 *   FuelType_HybridVehicleEthanol_HYB_ETH (18U)
 *   FuelType_HybridVehicleDiesel_HYB_DSL (19U)
 *   FuelType_HybridVehicleBattery_HYB_ELEC (20U)
 *   FuelType_HybridVehicleBatteryAndCombustion_HYB_MIX (21U)
 *   FuelType_HybridVehicleRegenerationMode_HYB_REG (22U)
 *   FuelType_NaturalGas_NG (23U)
 *   FuelType_BifuelVehicleNG_BI_NG (24U)
 *   FuelType_BifuelDiesel (25U)
 *   FuelType_NaturalGasCompressedOrLiquefied (26U)
 *   FuelType_DualFuel_DieselAndCNG (27U)
 *   FuelType_DualFuel_DieselAndLNG (28U)
 *   FuelType_Error (254U)
 *   FuelType_NotAvailable (255U)
 * HandlingInformation_T: Enumeration of integer in interval [0...3] with enumerators
 *   HandlingInformation_NoHandlingInformation (0U)
 *   HandlingInformation_HandlingInformation (1U)
 *   HandlingInformation_Error (2U)
 *   HandlingInformation_NotAvailable (3U)
 * NotDetected_T: Enumeration of integer in interval [0...3] with enumerators
 *   NotDetected_NotDetected (0U)
 *   NotDetected_Detected (1U)
 *   NotDetected_Error (2U)
 *   NotDetected_NotAvaliable (3U)
 * NotPresentPresent_T: Enumeration of integer in interval [0...3] with enumerators
 *   NotPresentPresent_NotPresent (0U)
 *   NotPresentPresent_Present (1U)
 *   NotPresentPresent_ErrorIndicator (2U)
 *   NotPresentPresent_NotAvailable (3U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * OilQuality_T: Enumeration of integer in interval [0...7] with enumerators
 *   OilQuality_NonVDS_VDS4WithFixedInterval (0U)
 *   OilQuality_VDSVDS3WithFixedInterval (1U)
 *   OilQuality_VDS5 (2U)
 *   OilQuality_VDS3 (3U)
 *   OilQuality_VDS4 (4U)
 *   OilQuality_NoSelectionMade (5U)
 *   OilQuality_Error (6U)
 *   OilQuality_NotAvailable (7U)
 * OilStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   OilStatus_NotActive (0U)
 *   OilStatus_ServiceOverDue (1U)
 *   OilStatus_ServiceDue (2U)
 *   OilStatus_ServiceNearDue (3U)
 *   OilStatus_Ok (4U)
 *   OilStatus_Spare (5U)
 *   OilStatus_Error (6U)
 *   OilStatus_NotAvailable (7U)
 * PtoState_T: Enumeration of integer in interval [0...31] with enumerators
 *   PtoState_OffDisabled (0U)
 *   PtoState_Hold (1U)
 *   PtoState_RemoteHold (2U)
 *   PtoState_Standby (3U)
 *   PtoState_RemoteStandby (4U)
 *   PtoState_Set (5U)
 *   PtoState_DecelerateCoast (6U)
 *   PtoState_Resume (7U)
 *   PtoState_Accelerate (8U)
 *   PtoState_AcceleratorOverride (9U)
 *   PtoState_PreprogramSetSpeed1 (10U)
 *   PtoState_PreprogramSetSpeed2 (11U)
 *   PtoState_PreprogramSetSpeed3 (12U)
 *   PtoState_PreprogramSetSpeed4 (13U)
 *   PtoState_PreprogramSetSpeed5 (14U)
 *   PtoState_PreprogramSetSpeed6 (15U)
 *   PtoState_PreprogramSetSpeed7 (16U)
 *   PtoState_PreprogramSetSpeed8 (17U)
 *   PtoState_PTOSetSpeedMemory1 (18U)
 *   PtoState_PTOSetSpeedMemory2 (19U)
 *   PtoState_NotDefined (20U)
 *   PtoState_NotDefined01 (21U)
 *   PtoState_NotDefined02 (22U)
 *   PtoState_NotDefined03 (23U)
 *   PtoState_NotDefined04 (24U)
 *   PtoState_NotDefined05 (25U)
 *   PtoState_NotDefined06 (26U)
 *   PtoState_NotDefined07 (27U)
 *   PtoState_NotDefined08 (28U)
 *   PtoState_NotDefined09 (29U)
 *   PtoState_NotDefined10 (30U)
 *   PtoState_NotAvailable (31U)
 * PtosStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PtosStatus_NoPTOEngaged (0U)
 *   PtosStatus_OneOrMorePTOEngaged (1U)
 *   PtosStatus_Error (2U)
 *   PtosStatus_NotAvailable (3U)
 * RetarderTorqueMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   RetarderTorqueMode_LowIdleGovernorNoRequestDefaultMode (0U)
 *   RetarderTorqueMode_AcceleratorPedalOperatorSelection (1U)
 *   RetarderTorqueMode_CruiseControl (2U)
 *   RetarderTorqueMode_PTOGovernor (3U)
 *   RetarderTorqueMode_RoadSpeedGovernor (4U)
 *   RetarderTorqueMode_ASRControl (5U)
 *   RetarderTorqueMode_TransmissionControl (6U)
 *   RetarderTorqueMode_ABSControl (7U)
 *   RetarderTorqueMode_TorqueLimiting (8U)
 *   RetarderTorqueMode_HighSpeedGovernor (9U)
 *   RetarderTorqueMode_BrakingSystem (10U)
 *   RetarderTorqueMode_RemoteAccelerator (11U)
 *   RetarderTorqueMode_ServiceProcedure (12U)
 *   RetarderTorqueMode_NotDefined (13U)
 *   RetarderTorqueMode_Other (14U)
 *   RetarderTorqueMode_NotAvailable (15U)
 * ReverseGearEngaged_T: Enumeration of integer in interval [0...3] with enumerators
 *   ReverseGearEngaged_ReverseGearNotEngaged (0U)
 *   ReverseGearEngaged_ReverseGearEngaged (1U)
 *   ReverseGearEngaged_Error (2U)
 *   ReverseGearEngaged_NotAvailable (3U)
 * Supported_T: Enumeration of integer in interval [0...3] with enumerators
 *   Supported_NotSupported (0U)
 *   Supported_Supported (1U)
 *   Supported_Reserved (2U)
 *   Supported_DontCare (3U)
 * SystemEvent_T: Enumeration of integer in interval [0...3] with enumerators
 *   SystemEvent_NoTachographEvent (0U)
 *   SystemEvent_TachographEvent (1U)
 *   SystemEvent_Error (2U)
 *   SystemEvent_NotAvailable (3U)
 * TachographPerformance_T: Enumeration of integer in interval [0...3] with enumerators
 *   TachographPerformance_NormalPerformance (0U)
 *   TachographPerformance_PerformanceAnalysis (1U)
 *   TachographPerformance_Error (2U)
 *   TachographPerformance_NotAvailable (3U)
 * TellTaleStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   TellTaleStatus_Off (0U)
 *   TellTaleStatus_Red (1U)
 *   TellTaleStatus_Yellow (2U)
 *   TellTaleStatus_Info (3U)
 *   TellTaleStatus_Reserved1 (4U)
 *   TellTaleStatus_Reserved2 (5U)
 *   TellTaleStatus_Reserved3 (6U)
 *   TellTaleStatus_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleOverspeed_T: Enumeration of integer in interval [0...3] with enumerators
 *   VehicleOverspeed_NoOverspeed (0U)
 *   VehicleOverspeed_Overspeed (1U)
 *   VehicleOverspeed_PreOverspeed (2U)
 *   VehicleOverspeed_NotAvailable (3U)
 * WeightClass_T: Enumeration of integer in interval [0...15] with enumerators
 *   WeightClass_Spare0 (0U)
 *   WeightClass_PassengerVehicleClassM1 (1U)
 *   WeightClass_PassengerVehicleClassM2 (2U)
 *   WeightClass_PassengerVehicleClassM3 (3U)
 *   WeightClass_LightCommercialVehiclesClassN1 (4U)
 *   WeightClass_HeavyDutyVehiclesClassN2 (5U)
 *   WeightClass_HeavyDutyVehiclesClassN3 (6U)
 *   WeightClass_Spare1 (7U)
 *   WeightClass_Spare2 (8U)
 *   WeightClass_Spare3 (9U)
 *   WeightClass_Spare4 (10U)
 *   WeightClass_Spare5 (11U)
 *   WeightClass_Spare6 (12U)
 *   WeightClass_Spare7 (13U)
 *   WeightClass_Error (14U)
 *   WeightClass_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Driver1Identification_T: Array with 14 element(s) of type uint8
 * DriversIdentifications_T: Array with 40 element(s) of type uint8
 * SEWS_VIN_VINNO_T: Array with 17 element(s) of type uint8
 * SoftwareVersionSupported_T: Array with 4 element(s) of type uint8
 * VehicleIdentNumber_T: Array with 17 element(s) of type uint8
 *
 * Record Types:
 * =============
 * FMS1_T: Record with elements
 *   Blockid_RE of type Blockid_T
 *   TellTaleStatus1_RE of type TellTaleStatus_T
 *   TellTaleStatus2_RE of type TellTaleStatus_T
 *   TellTaleStatus3_RE of type TellTaleStatus_T
 *   TellTaleStatus4_RE of type TellTaleStatus_T
 *   TellTaleStatus5_RE of type TellTaleStatus_T
 *   TellTaleStatus6_RE of type TellTaleStatus_T
 *   TellTaleStatus7_RE of type TellTaleStatus_T
 *   TellTaleStatus8_RE of type TellTaleStatus_T
 *   TellTaleStatus9_RE of type TellTaleStatus_T
 *   TellTaleStatus10_RE of type TellTaleStatus_T
 *   TellTaleStatus11_RE of type TellTaleStatus_T
 *   TellTaleStatus12_RE of type TellTaleStatus_T
 *   TellTaleStatus13_RE of type TellTaleStatus_T
 *   TellTaleStatus14_RE of type TellTaleStatus_T
 *   TellTaleStatus15_RE of type TellTaleStatus_T
 * MaintService_T: Record with elements
 *   ServiceDistance_RE of type ServiceDistance_T
 *   DistID_RE of type MaintServiceID_T
 *   ServiceCalendarTime_RE of type ServiceTime_T
 *   ServiceEngineTime_RE of type ServiceTime_T
 *   CalDateID_RE of type MaintServiceID_T
 *   EngTimeID_RE of type MaintServiceID_T
 * OilPrediction_T: Record with elements
 *   Status_RE of type OilStatus_T
 *   Quality_RE of type OilQuality_T
 *   RemainDist_RE of type RemainDistOilChange_T
 *   RemainTime_RE of type RemainEngineTimeOilChange_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_LNGTank1Volume_P1LX9_T Rte_Prm_P1LX9_LNGTank1Volume_v(void)
 *   SEWS_LNGTank2Volume_P1LYA_T Rte_Prm_P1LYA_LNGTank2Volume_v(void)
 *   SEWS_WeightClassInformation_P1M7S_T Rte_Prm_P1M7S_WeightClassInformation_v(void)
 *   SEWS_FuelTypeInformation_P1M7T_T Rte_Prm_P1M7T_FuelTypeInformation_v(void)
 *   SEWS_CabHeightValue_P1R0P_T Rte_Prm_P1R0P_CabHeightValue_v(void)
 *   boolean Rte_Prm_P1DXX_FMSgateway_Act_v(void)
 *   boolean Rte_Prm_P1LX8_EngineFuelTypeLNG_v(void)
 *   boolean Rte_Prm_P1M7Q_EraGlonassUnitInstalled_v(void)
 *   uint8 *Rte_Prm_VINNO_VIN_v(void)
 *     Returnvalue: uint8* is of type SEWS_VIN_VINNO_T
 *
 *********************************************************************************************************************/


#define FMSGateway_START_SEC_CODE
#include "FMSGateway_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: FMSGateway_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Receive_Driver1Identification_Driver1Identification(uint8 *data)
 *     Argument data: uint8* is of type Driver1Identification_T
 *   Std_ReturnType Rte_Receive_DriversIdentifications_DriversIdentifications(uint8 *data, uint16* length)
 *     Argument data: uint8* is of type DriversIdentifications_T
 *   Std_ReturnType Rte_Receive_MaintService_MaintService(MaintService_T *data)
 *   Std_ReturnType Rte_Read_AcceleratorPedalPosition1_AcceleratorPedalPosition1(Percent8bitFactor04_T *data)
 *   Std_ReturnType Rte_Read_ActualDrvlnRetdrPercentTorque_ActualDrvlnRetdrPercentTorque(Percent8bit125NegOffset_T *data)
 *   Std_ReturnType Rte_Read_ActualEnginePercentTorque_ActualEnginePercentTorque(Percent8bit125NegOffset_T *data)
 *   Std_ReturnType Rte_Read_ActualEngineRetarderPercentTrq_ActualEngineRetarderPercentTrq(Percent8bit125NegOffset_T *data)
 *   Std_ReturnType Rte_Read_AmbientAirTemperature_AmbientAirTemperature(Temperature16bit_T *data)
 *   Std_ReturnType Rte_Read_BrakeSwitch_BrakeSwitch(BrakeSwitch_T *data)
 *   Std_ReturnType Rte_Read_CCActive_CCActive(OffOn_T *data)
 *   Std_ReturnType Rte_Read_CatalystTankLevel_CatalystTankLevel(Percent8bitFactor04_T *data)
 *   Std_ReturnType Rte_Read_ClutchSwitch_ClutchSwitch(ClutchSwitch_T *data)
 *   Std_ReturnType Rte_Read_Driver1TimeRelatedStates_Driver1TimeRelatedStates(Driver1TimeRelatedStates_T *data)
 *   Std_ReturnType Rte_Read_Driver1WorkingState_Driver1WorkingState(DriverWorkingState_T *data)
 *   Std_ReturnType Rte_Read_Driver2TimeRelatedStates_Driver2TimeRelatedStates(DriverTimeRelatedStates_T *data)
 *   Std_ReturnType Rte_Read_Driver2WorkingState_Driver2WorkingState(DriverWorkingState_T *data)
 *   Std_ReturnType Rte_Read_DriverCardDriver1_DriverCardDriver1(NotPresentPresent_T *data)
 *   Std_ReturnType Rte_Read_DriverCardDriver2_DriverCardDriver2(NotPresentPresent_T *data)
 *   Std_ReturnType Rte_Read_EngineCoolantTemp_stat_EngineCoolantTemp_stat(EngineTemp_T *data)
 *   Std_ReturnType Rte_Read_EngineFuelRate_EngineFuelRate(FuelRate_T *data)
 *   Std_ReturnType Rte_Read_EngineGasRate_EngineGasRate(GasRate_T *data)
 *   Std_ReturnType Rte_Read_EnginePercentLoadAtCurrentSpd_EnginePercentLoadAtCurrentSpd(Percent8bitNoOffset_T *data)
 *   Std_ReturnType Rte_Read_EngineRetarderTorqueMode_EngineRetarderTorqueMode(EngineRetarderTorqueMode_T *data)
 *   Std_ReturnType Rte_Read_EngineRunningTime_EngineRunningTime(Seconds32bitExtra_T *data)
 *   Std_ReturnType Rte_Read_EngineSpeed_EngineSpeed(SpeedRpm16bit_T *data)
 *   Std_ReturnType Rte_Read_EngineTotalFuelConsumed_EngineTotalFuelConsumed(Volume32bit_T *data)
 *   Std_ReturnType Rte_Read_EngineTotalLngConsumed_EngineTotalLngConsumed(TotalLngConsumed_T *data)
 *   Std_ReturnType Rte_Read_FMS1_FMS1(FMS1_T *data)
 *   Std_ReturnType Rte_Read_FuelLevel_FuelLevel(Percent8bitFactor04_T *data)
 *   Std_ReturnType Rte_Read_GrossCombinationVehicleWeight_GrossCombinationVehicleWeight(VehicleWeight16bit_T *data)
 *   Std_ReturnType Rte_Read_HandlingInformation_HandlingInformation(HandlingInformation_T *data)
 *   Std_ReturnType Rte_Read_HighResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed(Volume32BitFact001_T *data)
 *   Std_ReturnType Rte_Read_InstantaneousFuelEconomy_FMSInstantFuelEconomy(InstantFuelEconomy_T *data)
 *   Std_ReturnType Rte_Read_LngTank1RemainingGasVolume_LngTank1RemainingGasVolume(Percent8bitFactor04_T *data)
 *   Std_ReturnType Rte_Read_LngTank2RemainingGasVolume_LngTank2RemainingGasVolume(Percent8bitFactor04_T *data)
 *   Std_ReturnType Rte_Read_OilPrediction_OilPrediction(OilPrediction_T *data)
 *   Std_ReturnType Rte_Read_PtosStatus_PtosStatus(PtosStatus_T *data)
 *   Std_ReturnType Rte_Read_RetarderTorqueMode_RetarderTorqueMode(RetarderTorqueMode_T *data)
 *   Std_ReturnType Rte_Read_ReverseGearEngaged_ReverseGearEngaged(ReverseGearEngaged_T *data)
 *   Std_ReturnType Rte_Read_ServiceBrakeAirPrsCircuit1_ServiceBrakeAirPrsCircuit1(PressureFactor8_T *data)
 *   Std_ReturnType Rte_Read_ServiceBrakeAirPrsCircuit2_ServiceBrakeAirPrsCircuit2(PressureFactor8_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_SystemEvent_SystemEvent(SystemEvent_T *data)
 *   Std_ReturnType Rte_Read_TachographPerformance_TachographPerformance(TachographPerformance_T *data)
 *   Std_ReturnType Rte_Read_TachographVehicleSpeed_TachographVehicleSpeed(Speed16bit_T *data)
 *   Std_ReturnType Rte_Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes(Distance32bit_T *data)
 *   Std_ReturnType Rte_Read_VehFrontAxle1CalibratedLoad_VehicleAxleCalibratedLoad(AxleLoad_T *data)
 *   Std_ReturnType Rte_Read_VehRearAxle1CalibratedLoad_VehicleAxleCalibratedLoad(AxleLoad_T *data)
 *   Std_ReturnType Rte_Read_VehRearAxle2CalibratedLoad_VehicleAxleCalibratedLoad(AxleLoad_T *data)
 *   Std_ReturnType Rte_Read_VehRearAxle3CalibratedLoad_VehicleAxleCalibratedLoad(AxleLoad_T *data)
 *   Std_ReturnType Rte_Read_VehicleMotion_VehicleMotion(NotDetected_T *data)
 *   Std_ReturnType Rte_Read_VehicleOverspeed_VehicleOverspeed(VehicleOverspeed_T *data)
 *   Std_ReturnType Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Send_FMSDriversIdentifications_DriversIdentifications(const uint8 *data, uint16 length)
 *     Argument data: uint8* is of type DriversIdentifications_T
 *   Std_ReturnType Rte_Send_FMSVehicleIdentNumber_VehicleIdentNumber(const uint8 *data)
 *     Argument data: uint8* is of type VehicleIdentNumber_T
 *   Std_ReturnType Rte_Write_AcceleratorPedalPosition1_fms_AcceleratorPedalPosition1(Percent8bitFactor04_T data)
 *   Std_ReturnType Rte_Write_ActualDrvlnRetdrPctTrq_fms_ActualDrvlnRetdrPercentTorque(Percent8bit125NegOffset_T data)
 *   Std_ReturnType Rte_Write_ActualEngRetdrPercentTrq_fms_ActualEngineRetarderPercentTrq(Percent8bit125NegOffset_T data)
 *   Std_ReturnType Rte_Write_ActualEnginePercentTorque_fms_ActualEnginePercentTorque(Percent8bit125NegOffset_T data)
 *   Std_ReturnType Rte_Write_AmbientAirTemperature_fms_AmbientAirTemperature(Temperature16bit_T data)
 *   Std_ReturnType Rte_Write_BrakeSwitch_fms_BrakeSwitch(BrakeSwitch_T data)
 *   Std_ReturnType Rte_Write_CCActive_fms_CCActive(OffOn_T data)
 *   Std_ReturnType Rte_Write_CatalystTankLevel_fms_CatalystTankLevel(Percent8bitFactor04_T data)
 *   Std_ReturnType Rte_Write_ClutchSwitch_fms_ClutchSwitch(ClutchSwitch_T data)
 *   Std_ReturnType Rte_Write_DirectionIndicator_fms_DirectionIndicator(DirectionIndicator_T data)
 *   Std_ReturnType Rte_Write_Driver1TimeRelatedStates_fms_Driver1TimeRelatedStates(Driver1TimeRelatedStates_T data)
 *   Std_ReturnType Rte_Write_Driver1WorkingState_fms_Driver1WorkingState(DriverWorkingState_T data)
 *   Std_ReturnType Rte_Write_Driver2TimeRelatedStates_fms_Driver2TimeRelatedStates(DriverTimeRelatedStates_T data)
 *   Std_ReturnType Rte_Write_Driver2WorkingState_fms_Driver2WorkingState(DriverWorkingState_T data)
 *   Std_ReturnType Rte_Write_DriverCardDriver1_fms_DriverCardDriver1(NotPresentPresent_T data)
 *   Std_ReturnType Rte_Write_DriverCardDriver2_fms_DriverCardDriver2(NotPresentPresent_T data)
 *   Std_ReturnType Rte_Write_EngineCoolantTemp_stat_fms_EngineCoolantTemp_stat(EngineTemp_T data)
 *   Std_ReturnType Rte_Write_EngineFuelRate_fms_EngineFuelRate(FuelRate_T data)
 *   Std_ReturnType Rte_Write_EnginePctLoadAtCurrentSpd_fms_EnginePercentLoadAtCurrentSpd(Percent8bitNoOffset_T data)
 *   Std_ReturnType Rte_Write_EngineRetarderTorqueMode_fms_EngineRetarderTorqueMode(EngineRetarderTorqueMode_T data)
 *   Std_ReturnType Rte_Write_EngineSpeed_fms_EngineSpeed(SpeedRpm16bit_T data)
 *   Std_ReturnType Rte_Write_FMS1_fms_FMS1(const FMS1_T *data)
 *   Std_ReturnType Rte_Write_FMSAtLeastOnePTOEngaged_AtLeastOnePTOEngaged(OffOn_T data)
 *   Std_ReturnType Rte_Write_FMSAxleLocation_AxleLocation(Int8Bit_T data)
 *   Std_ReturnType Rte_Write_FMSAxleWeight_AxleWeight(AxleLoad_T data)
 *   Std_ReturnType Rte_Write_FMSDiagnosisSupported_FMSDiagnosisSupported(Supported_T data)
 *   Std_ReturnType Rte_Write_FMSEngineTotHoursOfOp_EngineTotalHoursOfOperation(EngineTotalHoursOfOperation_T data)
 *   Std_ReturnType Rte_Write_FMSEngineTotalFuelUsed_FMSEngineTotalFuelUsed(Volume32bitFact05_T data)
 *   Std_ReturnType Rte_Write_FMSHiResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed(Volume32BitFact001_T data)
 *   Std_ReturnType Rte_Write_FMSInstantFuelEconomy_FMSInstantFuelEconomy(InstantFuelEconomy_T data)
 *   Std_ReturnType Rte_Write_FMSPtoState_PtoState(PtoState_T data)
 *   Std_ReturnType Rte_Write_FMSRequestsSupported_FMSRequestsSupported(Supported_T data)
 *   Std_ReturnType Rte_Write_FMSServiceDistance_FMSServiceDistance(ServiceDistance16BitFact5Offset_T data)
 *   Std_ReturnType Rte_Write_FMSSoftwareVersionSupported_FMSSoftwareVersionSupported(const uint8 *data)
 *     Argument data: uint8* is of type SoftwareVersionSupported_T
 *   Std_ReturnType Rte_Write_FuelLevel_fms_FuelLevel(Percent8bitFactor04_T data)
 *   Std_ReturnType Rte_Write_FuelType_FuelType(FuelType_T data)
 *   Std_ReturnType Rte_Write_GrossCombVehicleWeight_fms_GrossCombinationVehicleWeight(VehicleWeight16bit_T data)
 *   Std_ReturnType Rte_Write_HandlingInformation_fms_HandlingInformation(HandlingInformation_T data)
 *   Std_ReturnType Rte_Write_PGNReq_Tacho_CIOM_PGNRequest(PGNRequest_T data)
 *   Std_ReturnType Rte_Write_RetarderTorqueMode_fms_RetarderTorqueMode(RetarderTorqueMode_T data)
 *   Std_ReturnType Rte_Write_ServiceBrakeAirPrsCircuit1_fms_ServiceBrakeAirPrsCircuit1(PressureFactor8_T data)
 *   Std_ReturnType Rte_Write_ServiceBrakeAirPrsCircuit2_fms_ServiceBrakeAirPrsCircuit2(PressureFactor8_T data)
 *   Std_ReturnType Rte_Write_SystemEvent_fms_SystemEvent(SystemEvent_T data)
 *   Std_ReturnType Rte_Write_TachographPerformance_fms_TachographPerformance(TachographPerformance_T data)
 *   Std_ReturnType Rte_Write_TachographVehicleSpeed_fms_TachographVehicleSpeed(Speed16bit_T data)
 *   Std_ReturnType Rte_Write_TotalVehicleDistanceHiRes_fms_TotalVehicleDistanceHighRes(Distance32bit_T data)
 *   Std_ReturnType Rte_Write_VehicleMotion_fms_VehicleMotion(NotDetected_T data)
 *   Std_ReturnType Rte_Write_VehicleOverspeed_fms_VehicleOverspeed(VehicleOverspeed_T data)
 *   Std_ReturnType Rte_Write_WeightClass_WeightClass(WeightClass_T data)
 *   Std_ReturnType Rte_Write_WheelBasedVehicleSpeed_fms_WheelBasedVehicleSpeed(Speed16bit_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: FMSGateway_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, FMSGateway_CODE) FMSGateway_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: FMSGateway_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define FMSGateway_STOP_SEC_CODE
#include "FMSGateway_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
