/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  AuthenticationDevice_UICtrl.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  AuthenticationDevice_UICtrl
 *  Generation Time:  2020-09-21 13:44:19
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <AuthenticationDevice_UICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_AuthenticationDevice_UICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * boolean: Boolean (standard type)
 *
 * Enumeration Types:
 * ==================
 * ButtonAuth_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 *   ButtonAuth_rqst_Idle (0U)
 *   ButtonAuth_rqst_RequestDeviceAuthentication (1U)
 *   ButtonAuth_rqst_Error (2U)
 *   ButtonAuth_rqst_NotAvailable (3U)
 * DeviceAuthentication_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   DeviceAuthentication_rqst_Idle (0U)
 *   DeviceAuthentication_rqst_DeviceAuthenticationRequest (1U)
 *   DeviceAuthentication_rqst_DeviceDeauthenticationRequest (2U)
 *   DeviceAuthentication_rqst_DeviceMatching (3U)
 *   DeviceAuthentication_rqst_Spare1 (4U)
 *   DeviceAuthentication_rqst_Spare2 (5U)
 *   DeviceAuthentication_rqst_Error (6U)
 *   DeviceAuthentication_rqst_NotAvailable (7U)
 * DoorsAjar_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorsAjar_stat_Idle (0U)
 *   DoorsAjar_stat_BothDoorsAreClosed (1U)
 *   DoorsAjar_stat_DriverDoorIsOpen (2U)
 *   DoorsAjar_stat_PassengerDoorIsOpen (3U)
 *   DoorsAjar_stat_BothDoorsAreOpen (4U)
 *   DoorsAjar_stat_Spare (5U)
 *   DoorsAjar_stat_Error (6U)
 *   DoorsAjar_stat_NotAvailable (7U)
 * DriverAuthDeviceMatching_T: Enumeration of integer in interval [0...3] with enumerators
 *   DriverAuthDeviceMatching_Idle (0U)
 *   DriverAuthDeviceMatching_DeviceToMatchIsPresent (1U)
 *   DriverAuthDeviceMatching_Error (2U)
 *   DriverAuthDeviceMatching_NotAvailable (3U)
 * KeyPosition_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyPosition_KeyOut (0U)
 *   KeyPosition_IgnitionKeyInOffPosition (1U)
 *   KeyPosition_IgnitionKeyInAccessoryPosition (2U)
 *   KeyPosition_IgnitionKeyIn15PositionNormalDrivingPosition (3U)
 *   KeyPosition_IgnitionKeyInPreheatPosition (4U)
 *   KeyPosition_IgnitionKeyInCrankPosition (5U)
 *   KeyPosition_ErrorIndicator (6U)
 *   KeyPosition_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1T3W_VehSSButtonInstalled_v(void)
 *
 *********************************************************************************************************************/


#define AuthenticationDevice_UICtrl_START_SEC_CODE
#include "AuthenticationDevice_UICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AuthenticationDevice_UICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ButtonAuth_rqst_ButtonAuth_rqst(ButtonAuth_rqst_T *data)
 *   Std_ReturnType Rte_Read_DoorsAjar_stat_DoorsAjar_stat(DoorsAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_KeyPosition_KeyPosition(KeyPosition_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DeviceAuthentication_rqst_DeviceAuthentication_rqst(DeviceAuthentication_rqst_T data)
 *   Std_ReturnType Rte_Write_DriverAuthDeviceMatching_DriverAuthDeviceMatching(DriverAuthDeviceMatching_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AuthenticationDevice_UICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, AuthenticationDevice_UICtrl_CODE) AuthenticationDevice_UICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AuthenticationDevice_UICtrl_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define AuthenticationDevice_UICtrl_STOP_SEC_CODE
#include "AuthenticationDevice_UICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
