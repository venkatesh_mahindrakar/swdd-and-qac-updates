/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  FlexibleSwitchesRouter_Ctrl_LINMstr.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  FlexibleSwitchesRouter_Ctrl_LINMstr
 *  Generation Time:  2020-09-21 13:44:21
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <FlexibleSwitchesRouter_Ctrl_LINMstr>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   Boolean has the value space required to support the mathematical concept of 
 *      binary-valued logic: {true, false}.
 *
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_OpStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_EventStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_InitMonitorReasonType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FSPConfigSettingsLIN1_P1EWZ_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FSPConfigSettingsLIN2_P1EW0_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FSPConfigSettingsLIN3_P1EW1_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FSPConfigSettingsLIN4_P1EW2_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_FSPConfigSettingsLIN5_P1EW3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *
 * Runnable Entities:
 * ==================
 * FlexibleSwitchPanel_LINMastCtrl_20ms_runnable
 *   (*
 *       *
 *       * @par EntityName FlexibleSwitchPanel_LINMastCtrl_20ms_runnable
 *       *        
 *       * @par Purpose This runnable shall control input/output of flexible switch information from LIN bus.\n
 *       * 
 *       *  
 *       * @par Dataflow \n
 *       * Inputs : \n
 *       * Outputs : 
 *       *
 *       *@par Execution Event\n
 *       * Shall be executed if at least one of the following trigger conditions occurred:\n
 *       * -- triggered on TimingEvent every 20ms
 *       *
 *       *
 *       * @par Input Interfaces:
 *       * @par Explicit S/R API:
 *       *
 *       * @par Output Interfaces:
 *       * @par Explicit S/R API:
 *       *
 *       )
 *      
 *
 *********************************************************************************************************************/

#include "Rte_FlexibleSwitchesRouter_Ctrl_LINMstr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * FSP1ResponseErrorL1_T: Boolean
 * FSP1ResponseErrorL2_T: Boolean
 * FSP1ResponseErrorL3_T: Boolean
 * FSP1ResponseErrorL4_T: Boolean
 * FSP1ResponseErrorL5_T: Boolean
 * FSP2ResponseErrorL1_T: Boolean
 * FSP2ResponseErrorL2_T: Boolean
 * FSP2ResponseErrorL3_T: Boolean
 * FSP3ResponseErrorL2_T: Boolean
 * FSP4ResponseErrorL2_T: Boolean
 * FSPDiagInfo_T: Integer in interval [0...63]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * FSPIndicationCmd_T: Integer in interval [0...65535]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * FSPSwitchStatus_T: Integer in interval [0...255]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * SEWS_FSPConfigSettingsLIN1_P1EWZ_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN2_P1EW0_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN3_P1EW1_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN4_P1EW2_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN5_P1EW3_T: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * Dem_InitMonitorReasonType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_INIT_MONITOR_CLEAR (1U)
 *   DEM_INIT_MONITOR_RESTART (2U)
 *   DEM_INIT_MONITOR_REENABLED (3U)
 *   DEM_INIT_MONITOR_STORAGE_REENABLED (4U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * LinDiagBusInfo: Enumeration of integer in interval [0...255] with enumerators
 *   None (0U)
 *   LinDiag_BUS1 (1U)
 *   LinDiag_BUS2 (2U)
 *   LinDiag_BUS3 (3U)
 *   LinDiag_BUS4 (4U)
 *   LinDiag_BUS5 (5U)
 *   LinDiag_SpareBUS1 (6U)
 *   LinDiag_SpareBUS2 (7U)
 *   LinDiag_ALL_BUSSES (8U)
 * LinDiagRequest_T: Enumeration of integer in interval [0...255] with enumerators
 *   NO_OPEARATION (0U)
 *   START_OPEARATION (1U)
 *   STOP_OPERATION (2U)
 * LinDiagServiceStatus: Enumeration of integer in interval [0...255] with enumerators
 *   LinDiagService_None (0U)
 *   LinDiagService_Pending (1U)
 *   LinDiagService_Completed (2U)
 *   LinDiagService_Error (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 * Dcm_Data5ByteType: Array with 5 element(s) of type uint8
 * Dcm_Data6ByteType: Array with 6 element(s) of type uint8
 * Dcm_Data7ByteType: Array with 7 element(s) of type uint8
 * FSPIndicationCmdArray_T: Array with 8 element(s) of type DeviceIndication_T
 * FSPSwitchStatusArray_T: Array with 8 element(s) of type PushButtonStatus_T
 * FSP_Array5: Array with 5 element(s) of type uint8
 * FspNVM_T: Array with 28 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_FSPConfigSettingsLIN2_P1EW0_T Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v(void)
 *   SEWS_FSPConfigSettingsLIN3_P1EW1_T Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v(void)
 *   SEWS_FSPConfigSettingsLIN4_P1EW2_T Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v(void)
 *   SEWS_FSPConfigSettingsLIN5_P1EW3_T Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v(void)
 *   SEWS_FSPConfigSettingsLIN1_P1EWZ_T Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v(void)
 *
 *********************************************************************************************************************/


#define FlexibleSwitchesRouter_Ctrl_LINMstr_START_SEC_CODE
#include "FlexibleSwitchesRouter_Ctrl_LINMstr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <InitMonitorForEvent> of PortPrototype <CBInitEvt_D1BN8_79_FlexSwLostFCI>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BK9_87_FSP_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN8_16_FSP_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN8_17_FSP_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN8_44_FSP_MemoryFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN8_79_FlexSwLostFCI_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CallbackInitMonitorForEvent_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1C1R_Data_P1C1R_NbFSP>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData_Irv_NbOfFspByCCNAD(uint8 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data6ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1C1R_Data_P1C1R_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData_Irv_NbOfFspInFailure(uint8 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data5ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1DCU_Data_P1DCU_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData_Irv_IOCTL_FspLinCtrl(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_FspLinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_FspLinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: FlexibleSwitchPanel_LINMastCtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN2_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN3_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN5_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_FSP1DiagInfoL1_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP1DiagInfoL2_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP1DiagInfoL3_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP1DiagInfoL4_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP1DiagInfoL5_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP1IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP1ResponseErrorL1_FSP1ResponseErrorL1(FSP1ResponseErrorL1_T *data)
 *   Std_ReturnType Rte_Read_FSP1ResponseErrorL2_FSP1ResponseErrorL2(FSP1ResponseErrorL2_T *data)
 *   Std_ReturnType Rte_Read_FSP1ResponseErrorL3_FSP1ResponseErrorL3(FSP1ResponseErrorL3_T *data)
 *   Std_ReturnType Rte_Read_FSP1ResponseErrorL4_FSP1ResponseErrorL4(FSP1ResponseErrorL4_T *data)
 *   Std_ReturnType Rte_Read_FSP1ResponseErrorL5_FSP1ResponseErrorL5(FSP1ResponseErrorL5_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatusL1_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatusL2_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatusL3_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatusL4_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatusL5_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP2DiagInfoL1_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP2DiagInfoL2_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP2DiagInfoL3_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP2IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP2ResponseErrorL1_FSP2ResponseErrorL1(FSP2ResponseErrorL1_T *data)
 *   Std_ReturnType Rte_Read_FSP2ResponseErrorL2_FSP2ResponseErrorL2(FSP2ResponseErrorL2_T *data)
 *   Std_ReturnType Rte_Read_FSP2ResponseErrorL3_FSP2ResponseErrorL3(FSP2ResponseErrorL3_T *data)
 *   Std_ReturnType Rte_Read_FSP2SwitchStatusL1_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP2SwitchStatusL2_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP2SwitchStatusL3_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP3DiagInfoL2_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP3IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP3ResponseErrorL2_FSP3ResponseErrorL2(FSP3ResponseErrorL2_T *data)
 *   Std_ReturnType Rte_Read_FSP3SwitchStatusL2_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP4DiagInfoL2_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP4IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP4ResponseErrorL2_FSP4ResponseErrorL2(FSP4ResponseErrorL2_T *data)
 *   Std_ReturnType Rte_Read_FSP4SwitchStatusL2_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP5IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP6IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP7IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP8IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP9IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP_BIndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FspNV_PR_FspNV(uint8 *data)
 *     Argument data: uint8* is of type FspNVM_T
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_FSP1IndicationCmdL1_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP1IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP1IndicationCmdL3_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP1IndicationCmdL4_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP1IndicationCmdL5_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP1SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP2IndicationCmdL1_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP2IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP2IndicationCmdL3_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP2SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP3IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP3SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP4IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP4SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP5SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP6SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP7SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP8SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP9SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP_BSwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FspNV_PR_FspNV(const uint8 *data)
 *     Argument data: uint8* is of type FspNVM_T
 *   Std_ReturnType Rte_Write_Living12VResetRequest_Living12VResetRequest(Boolean data)
 *   Std_ReturnType Rte_Write_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted(Boolean data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl(void)
 *   void Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(uint8 *data)
 *   void Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(uint8 *data)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl(uint8 data)
 *   void Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(const uint8 *data)
 *   void Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(const uint8 *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) FlexibleSwitchPanel_LINMastCtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: FlexibleSwitchPanel_LINMastCtrl_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_R1AAI_FspAssignmentRoutine>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignReq(LinDiagBusInfo LinBusInfo, LinDiagRequest_T RequestType)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignResp(LinDiagServiceStatus *pDiagServiceStatus, uint8 *pAvailableFSPCount, uint8 *pFspErrorStatus, uint8 *pFspNvData)
 *     Synchronous Server Invocation. Timeout: None
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data7ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAI_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAI_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAI_FspAssignmentRoutine_Start
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_R1AAI_FspAssignmentRoutine>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignReq(LinDiagBusInfo LinBusInfo, LinDiagRequest_T RequestType)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignResp(LinDiagServiceStatus *pDiagServiceStatus, uint8 *pAvailableFSPCount, uint8 *pFspErrorStatus, uint8 *pFspNvData)
 *     Synchronous Server Invocation. Timeout: None
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAI_FspAssignmentRoutine_Start(uint8 In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAI_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAI_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_Start(uint8 In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_Start (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAI_FspAssignmentRoutine_Stop
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_R1AAI_FspAssignmentRoutine>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignReq(LinDiagBusInfo LinBusInfo, LinDiagRequest_T RequestType)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignResp(LinDiagServiceStatus *pDiagServiceStatus, uint8 *pAvailableFSPCount, uint8 *pFspErrorStatus, uint8 *pFspNvData)
 *     Synchronous Server Invocation. Timeout: None
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAI_FspAssignmentRoutine_Stop(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAI_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAI_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_Stop(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_Stop (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define FlexibleSwitchesRouter_Ctrl_LINMstr_STOP_SEC_CODE
#include "FlexibleSwitchesRouter_Ctrl_LINMstr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
