/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  PEPS.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  PEPS
 *  Generation Time:  2020-09-21 13:44:23
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <PEPS>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_OpStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * IOHWAB_UINT8
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AntMappingConfig_Gain_X1C03_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AntMappingConfig_Multi_X1CY3a_T
 *   SInt16 represents integers with a minimum value of -32768 and a maximum value 
 *      of 32767. The order-relation on sint16 is: x < y if y - x is positive.
 *      sint16 has a lexical representation consisting of an optional sign followed
 *      by a finite-length sequence of decimal digits (#x30-#x39). If the sign is
 *      omitted, "+" is assumed. 
 *      
 *      For example: -1, 0, -12678, +10000, 2500
 *
 * SEWS_KeyfobDetectionMappingConfig_P1WIR_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_KeyfobDetectionMappingSelection_P1WIP_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_X1CY5_AntennaP1_LimitRSSI_T
 *   SInt16 represents integers with a minimum value of -32768 and a maximum value 
 *      of 32767. The order-relation on sint16 is: x < y if y - x is positive.
 *      sint16 has a lexical representation consisting of an optional sign followed
 *      by a finite-length sequence of decimal digits (#x30-#x39). If the sign is
 *      omitted, "+" is assumed. 
 *      
 *      For example: -1, 0, -12678, +10000, 2500
 *
 * SEWS_X1CY5_AntennaP2_LimitRSSI_T
 *   SInt16 represents integers with a minimum value of -32768 and a maximum value 
 *      of 32767. The order-relation on sint16 is: x < y if y - x is positive.
 *      sint16 has a lexical representation consisting of an optional sign followed
 *      by a finite-length sequence of decimal digits (#x30-#x39). If the sign is
 *      omitted, "+" is assumed. 
 *      
 *      For example: -1, 0, -12678, +10000, 2500
 *
 * SEWS_X1CY5_AntennaP3_LimitRSSI_T
 *   SInt16 represents integers with a minimum value of -32768 and a maximum value 
 *      of 32767. The order-relation on sint16 is: x < y if y - x is positive.
 *      sint16 has a lexical representation consisting of an optional sign followed
 *      by a finite-length sequence of decimal digits (#x30-#x39). If the sign is
 *      omitted, "+" is assumed. 
 *      
 *      For example: -1, 0, -12678, +10000, 2500
 *
 * SEWS_X1CY5_AntennaP4_LimitRSSI_T
 *   SInt16 represents integers with a minimum value of -32768 and a maximum value 
 *      of 32767. The order-relation on sint16 is: x < y if y - x is positive.
 *      sint16 has a lexical representation consisting of an optional sign followed
 *      by a finite-length sequence of decimal digits (#x30-#x39). If the sign is
 *      omitted, "+" is assumed. 
 *      
 *      For example: -1, 0, -12678, +10000, 2500
 *
 * SEWS_X1CY5_AntennaPi_LimitRSSI_T
 *   SInt16 represents integers with a minimum value of -32768 and a maximum value 
 *      of 32767. The order-relation on sint16 is: x < y if y - x is positive.
 *      sint16 has a lexical representation consisting of an optional sign followed
 *      by a finite-length sequence of decimal digits (#x30-#x39). If the sign is
 *      omitted, "+" is assumed. 
 *      
 *      For example: -1, 0, -12678, +10000, 2500
 *
 *********************************************************************************************************************/

#include "Rte_PEPS.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IOHWAB_UINT8: Integer in interval [0...255]
 * SEWS_AntMappingConfig_Gain_X1C03_T: Integer in interval [0...255]
 * SEWS_AntMappingConfig_Multi_X1CY3a_T: Integer in interval [-32768...32767]
 * SEWS_KeyfobDetectionMappingConfig_P1WIR_T: Integer in interval [0...255]
 * SEWS_KeyfobDetectionMappingSelection_P1WIP_T: Integer in interval [0...255]
 * SEWS_X1CY5_AntennaP1_LimitRSSI_T: Integer in interval [-32768...32767]
 * SEWS_X1CY5_AntennaP2_LimitRSSI_T: Integer in interval [-32768...32767]
 * SEWS_X1CY5_AntennaP3_LimitRSSI_T: Integer in interval [-32768...32767]
 * SEWS_X1CY5_AntennaP4_LimitRSSI_T: Integer in interval [-32768...32767]
 * SEWS_X1CY5_AntennaPi_LimitRSSI_T: Integer in interval [-32768...32767]
 * boolean: Boolean (standard type)
 * sint16: Integer in interval [-32768...32767] (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * KeyfobInCabLocation_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobInCabLocation_stat_Idle (0U)
 *   KeyfobInCabLocation_stat_NotDetected_Incab (1U)
 *   KeyfobInCabLocation_stat_Detected_Incab (2U)
 *   KeyfobInCabLocation_stat_Spare1 (3U)
 *   KeyfobInCabLocation_stat_Spare2 (4U)
 *   KeyfobInCabLocation_stat_Spare3 (5U)
 *   KeyfobInCabLocation_stat_Error (6U)
 *   KeyfobInCabLocation_stat_NotAvailable (7U)
 * KeyfobOutsideLocation_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobOutsideLocation_stat_Idle (0U)
 *   KeyfobOutsideLocation_stat_NotDetectedOutside (1U)
 *   KeyfobOutsideLocation_stat_DetectedOnRightSide (2U)
 *   KeyfobOutsideLocation_stat_DetectedOnLeftSide (3U)
 *   KeyfobOutsideLocation_stat_DetectedOnBothSides (4U)
 *   KeyfobOutsideLocation_stat_DetectedOutOfDoorAreas (5U)
 *   KeyfobOutsideLocation_stat_Error (6U)
 *   KeyfobOutsideLocation_stat_NotAvailable (7U)
 * SCIM_PassiveDriver_ProcessingStatus_T: Enumeration of integer in interval [0...255] with enumerators
 *   PassiveDriver_ProcessingStatus_Idle (0U)
 *   PassiveDriver_ProcessingStatus_Ongoing (1U)
 *   PassiveDriver_ProcessingStatus_LfSent (2U)
 *   PassiveDriver_ProcessingStatus_Detected (3U)
 *   PassiveDriver_ProcessingStatus_NotDetected (4U)
 *   PassiveDriver_ProcessingStatus_HwError (5U)
 * SCIM_PassiveSearchCoverage_T: Enumeration of integer in interval [0...255] with enumerators
 *   PassiveSearchCoverage_PassiveStart2Ant (0U)
 *   PassiveSearchCoverage_PassiveStart1Ant (1U)
 *
 * Array Types:
 * ============
 * ArrayByteSize32: Array with 32 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 * Dcm_Data40ByteType: Array with 40 element(s) of type uint8
 * SEWS_AntMappingConfig_Gain_X1C03_a_T: Array with 5 element(s) of type SEWS_AntMappingConfig_Gain_X1C03_T
 * SEWS_AntMappingConfig_Multi_X1CY3_a_T: Array with 10 element(s) of type SEWS_AntMappingConfig_Multi_X1CY3a_a_T
 * SEWS_AntMappingConfig_Multi_X1CY3a_a_T: Array with 20 element(s) of type SEWS_AntMappingConfig_Multi_X1CY3a_T
 * SEWS_X1CY5_AntennaP1_LimitRSSI_a_T: Array with 2 element(s) of type SEWS_X1CY5_AntennaP1_LimitRSSI_T
 * SEWS_X1CY5_AntennaP2_LimitRSSI_a_T: Array with 2 element(s) of type SEWS_X1CY5_AntennaP2_LimitRSSI_T
 * SEWS_X1CY5_AntennaP3_LimitRSSI_a_T: Array with 2 element(s) of type SEWS_X1CY5_AntennaP3_LimitRSSI_T
 * SEWS_X1CY5_AntennaP4_LimitRSSI_a_T: Array with 2 element(s) of type SEWS_X1CY5_AntennaP4_LimitRSSI_T
 * SEWS_X1CY5_AntennaPi_LimitRSSI_a_T: Array with 2 element(s) of type SEWS_X1CY5_AntennaPi_LimitRSSI_T
 *
 * Record Types:
 * =============
 * LfRssi: Record with elements
 *   AntennaPi of type uint16
 *   AntennaP1 of type uint16
 *   AntennaP2 of type uint16
 *   AntennaP3 of type uint16
 *   AntennaP4 of type uint16
 * SEWS_AntMappingConfig_Single_X1CY5_s_T: Record with elements
 *   AntennaPi_LimitRSSI of type SEWS_X1CY5_AntennaPi_LimitRSSI_a_T
 *   AntennaP1_LimitRSSI of type SEWS_X1CY5_AntennaP1_LimitRSSI_a_T
 *   AntennaP2_LimitRSSI of type SEWS_X1CY5_AntennaP2_LimitRSSI_a_T
 *   AntennaP3_LimitRSSI of type SEWS_X1CY5_AntennaP3_LimitRSSI_a_T
 *   AntennaP4_LimitRSSI of type SEWS_X1CY5_AntennaP4_LimitRSSI_a_T
 * SEWS_AuxPassiveAntennasActivation_P1WIQ_s_T: Record with elements
 *   IsActiveAntenna_Pi of type boolean
 *   IsActiveAntenna_P1 of type boolean
 *   IsActiveAntenna_P2 of type boolean
 *   IsActiveAntenna_P3 of type boolean
 *   IsActiveAntenna_P4 of type boolean
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_AntMappingConfig_Gain_X1C03_T *Rte_Prm_X1C03_AntMappingConfig_Gain_v(void)
 *     Returnvalue: SEWS_AntMappingConfig_Gain_X1C03_T* is of type SEWS_AntMappingConfig_Gain_X1C03_a_T
 *   SEWS_AntMappingConfig_Multi_X1CY3a_T *Rte_Prm_X1CY3_AntMappingConfig_Multi_v(void)
 *     Returnvalue: SEWS_AntMappingConfig_Multi_X1CY3a_T* is of type SEWS_AntMappingConfig_Multi_X1CY3_a_T
 *   SEWS_AntMappingConfig_Single_X1CY5_s_T *Rte_Prm_X1CY5_AntMappingConfig_Single_v(void)
 *   SEWS_KeyfobDetectionMappingSelection_P1WIP_T Rte_Prm_P1WIP_KeyfobDetectionMappingSelection_v(void)
 *   SEWS_KeyfobDetectionMappingConfig_P1WIR_T Rte_Prm_P1WIR_KeyfobDetectionMappingConfig_v(void)
 *   SEWS_AuxPassiveAntennasActivation_P1WIQ_s_T *Rte_Prm_P1WIQ_AuxPassiveAntennasActivation_v(void)
 *
 *********************************************************************************************************************/


#define PEPS_START_SEC_CODE
#include "PEPS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data40ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1CY4_Data_X1CY4_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, PEPS_CODE) DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_ReadData(P2VAR(uint8, AUTOMATIC, RTE_PEPS_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_WriteData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <WriteData> of PortPrototype <DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_WriteData(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data40ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1CY4_Data_X1CY4_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_WriteData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, PEPS_CODE) DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_WriteData(P2CONST(uint8, AUTOMATIC, RTE_PEPS_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_PEPS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_WriteData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetKeyfobPassiveSearchResult> of PortPrototype <KeyfobPassiveSearchRqst>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void GetKeyfobPassiveSearchResult(SCIM_PassiveDriver_ProcessingStatus_T *ProcessingStatus, KeyfobInCabLocation_stat_T *KeyfobLocationbyPassive_Incab, KeyfobOutsideLocation_stat_T *KeyfobLocationbyPassive_Outcab)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PEPS_CODE) GetKeyfobPassiveSearchResult(P2VAR(SCIM_PassiveDriver_ProcessingStatus_T, AUTOMATIC, RTE_PEPS_APPL_VAR) ProcessingStatus, P2VAR(KeyfobInCabLocation_stat_T, AUTOMATIC, RTE_PEPS_APPL_VAR) KeyfobLocationbyPassive_Incab, P2VAR(KeyfobOutsideLocation_stat_T, AUTOMATIC, RTE_PEPS_APPL_VAR) KeyfobLocationbyPassive_Outcab) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: GetKeyfobPassiveSearchResult
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: KeyfobPassiveSearchRqst_KeyfobPassiveSearch
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <KeyfobPassiveSearch> of PortPrototype <KeyfobPassiveSearchRqst>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void KeyfobPassiveSearch(SCIM_PassiveSearchCoverage_T PassiveSearchCoverage, boolean SearchRequestedbyPassive)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: KeyfobPassiveSearchRqst_KeyfobPassiveSearch_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PEPS_CODE) KeyfobPassiveSearch(SCIM_PassiveSearchCoverage_T PassiveSearchCoverage, boolean SearchRequestedbyPassive) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: KeyfobPassiveSearch
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: PEPS_10ms_runnable
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_LFSearchCompleteFlag_Get_LFSearchCompleteFlag(uint8 *LFSearchCompleteFlag_PEPS)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_LFSearchCompleteFlag_Set_LFSearchCompleteFlag(uint8 LFSearchCompleteFlag_PEPS)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_LfICInit_CS(uint8 Gain_vehicleOption)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_RficDioInterface_P_Read(IOHWAB_UINT8 *ReadValue)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Dio_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_RficIRQActiveCheck_CS(uint8 kb_TerminalControlState)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_RficInit_CS(void)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_SearchSysMode_CS(uint8 RficSystemMode, uint8 RFIC_ReqCmd)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_SetupLfTelegram_CS(uint8 b_AntennaIndex, uint8 b_SearchPattern, uint16 Counter_LFRawDataBit, const uint8 *Buffer_LFRawData)
 *     Argument Buffer_LFRawData: uint8* is of type ArrayByteSize32
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_TimeoutTxTelegram_CS(void)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: PEPS_10ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PEPS_CODE) PEPS_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: PEPS_10ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RE_Check_Passive_Encryption
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CS> of PortPrototype <RficCheckPassiveEncryption>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void RE_Check_Passive_Encryption(uint8 *RcvData)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_Check_Passive_Encryption_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PEPS_CODE) RE_Check_Passive_Encryption(P2VAR(uint8, AUTOMATIC, RTE_PEPS_APPL_VAR) RcvData) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_Check_Passive_Encryption
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RE_Clear_HighFixCheckTimer
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CS> of PortPrototype <RficClearHighFixCheckTimer>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void RE_Clear_HighFixCheckTimer(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_Clear_HighFixCheckTimer_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PEPS_CODE) RE_Clear_HighFixCheckTimer(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_Clear_HighFixCheckTimer
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RE_Generate_Telegram_EncryptionKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CS> of PortPrototype <GenerateTelegramEncryptionKey>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void RE_Generate_Telegram_EncryptionKey(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_Generate_Telegram_EncryptionKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PEPS_CODE) RE_Generate_Telegram_EncryptionKey(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_Generate_Telegram_EncryptionKey
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RE_GetLfAntState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetLfAntState> of PortPrototype <LfInterface_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RE_GetLfAntState(LfRssi *LfRssiStatus, uint8 *FobFound, uint8 *FobLocation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_LfInterface_I_RadioApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_GetLfAntState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, PEPS_CODE) RE_GetLfAntState(P2VAR(LfRssi, AUTOMATIC, RTE_PEPS_APPL_VAR) LfRssiStatus, P2VAR(uint8, AUTOMATIC, RTE_PEPS_APPL_VAR) FobFound, P2VAR(uint8, AUTOMATIC, RTE_PEPS_APPL_VAR) FobLocation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_GetLfAntState (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RE_PassiveEncryptionManager
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_PassiveEncryptionManager_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PEPS_CODE) RE_PassiveEncryptionManager(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_PassiveEncryptionManager
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RE_Request_Antena_Check
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * This runnable is never executed by the RTE.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_Request_Antena_Check_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PEPS_CODE) RE_Request_Antena_Check(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_Request_Antena_Check
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RE_RficSysModeConfirm_PEPS
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * This runnable is never executed by the RTE.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_RficSysModeConfirm_PEPS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PEPS_CODE) RE_RficSysModeConfirm_PEPS(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_RficSysModeConfirm_PEPS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RE_Select_RF_WaitTime
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_Select_RF_WaitTime_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PEPS_CODE) RE_Select_RF_WaitTime(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_Select_RF_WaitTime
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RE_Set_ValidFobFoundResult
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CS> of PortPrototype <RficSetValidFobFoundResult>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void RE_Set_ValidFobFoundResult(uint8 fobnum)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_Set_ValidFobFoundResult_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PEPS_CODE) RE_Set_ValidFobFoundResult(uint8 fobnum) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RE_Set_ValidFobFoundResult
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Y1ABE_AntennaMappingByDidActivation_Start
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Y1ABE_AntennaMappingByDidActivation>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Y1ABE_AntennaMappingByDidActivation_Start(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Y1ABE_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Y1ABE_DCM_E_PENDING
 *   RTE_E_RoutineServices_Y1ABE_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Y1ABE_AntennaMappingByDidActivation_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, PEPS_CODE) RoutineServices_Y1ABE_AntennaMappingByDidActivation_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_PEPS_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_PEPS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Y1ABE_AntennaMappingByDidActivation_Start (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define PEPS_STOP_SEC_CODE
#include "PEPS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
