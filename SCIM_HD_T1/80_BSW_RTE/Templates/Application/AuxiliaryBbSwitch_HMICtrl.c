/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  AuxiliaryBbSwitch_HMICtrl.c
 *           Config:  C:/GIT/scim_ecu_hd_t1_rt_inwork/SCIM_HD_T1/90_DavinciCFG/SCIM_HD_T1.dpa
 *        SW-C Type:  AuxiliaryBbSwitch_HMICtrl
 *  Generation Time:  2020-09-21 13:44:20
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <AuxiliaryBbSwitch_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_AuxBBLoadStat_MaxInitTime_P1M93_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AuxBBSw_TimeoutForReq_P1DV1_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AuxBbSw1_Logic_P1DI2_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AuxBbSw2_Logic_P1DI3_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AuxBbSw3_Logic_P1DI4_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AuxBbSw4_Logic_P1DI5_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AuxBbSw5_Logic_P1DI6_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * SEWS_AuxBbSw6_Logic_P1DI7_T
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *********************************************************************************************************************/

#include "Rte_AuxiliaryBbSwitch_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_AuxBBLoadStat_MaxInitTime_P1M93_T: Integer in interval [0...255]
 * SEWS_AuxBBSw_TimeoutForReq_P1DV1_T: Integer in interval [0...255]
 * SEWS_AuxBbSw1_Logic_P1DI2_T: Integer in interval [0...255]
 * SEWS_AuxBbSw2_Logic_P1DI3_T: Integer in interval [0...255]
 * SEWS_AuxBbSw3_Logic_P1DI4_T: Integer in interval [0...255]
 * SEWS_AuxBbSw4_Logic_P1DI5_T: Integer in interval [0...255]
 * SEWS_AuxBbSw5_Logic_P1DI6_T: Integer in interval [0...255]
 * SEWS_AuxBbSw6_Logic_P1DI7_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   InactiveActive_Inactive (0U)
 *   InactiveActive_Active (1U)
 *   InactiveActive_Error (2U)
 *   InactiveActive_NotAvailable (3U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_AuxBBSw_TimeoutForReq_P1DV1_T Rte_Prm_P1DV1_AuxBBSw_TimeoutForReq_v(void)
 *   SEWS_AuxBBLoadStat_MaxInitTime_P1M93_T Rte_Prm_P1M93_AuxBBLoadStat_MaxInitTime_v(void)
 *   boolean Rte_Prm_P1DI0_AuxBBSw5_Act_v(void)
 *   boolean Rte_Prm_P1DI1_AuxBbSw6_Act_v(void)
 *   boolean Rte_Prm_P1DIW_AuxBbSw1_Act_v(void)
 *   boolean Rte_Prm_P1DIX_AuxBbSw2_Act_v(void)
 *   boolean Rte_Prm_P1DIY_AuxBbSw3_Act_v(void)
 *   boolean Rte_Prm_P1DIZ_AuxBbSw4_Act_v(void)
 *   SEWS_AuxBbSw1_Logic_P1DI2_T Rte_Prm_P1DI2_AuxBbSw1_Logic_v(void)
 *   SEWS_AuxBbSw2_Logic_P1DI3_T Rte_Prm_P1DI3_AuxBbSw2_Logic_v(void)
 *   SEWS_AuxBbSw3_Logic_P1DI4_T Rte_Prm_P1DI4_AuxBbSw3_Logic_v(void)
 *   SEWS_AuxBbSw4_Logic_P1DI5_T Rte_Prm_P1DI5_AuxBbSw4_Logic_v(void)
 *   SEWS_AuxBbSw5_Logic_P1DI6_T Rte_Prm_P1DI6_AuxBbSw5_Logic_v(void)
 *   SEWS_AuxBbSw6_Logic_P1DI7_T Rte_Prm_P1DI7_AuxBbSw6_Logic_v(void)
 *   boolean Rte_Prm_P1B9X_WirelessRC_Enable_v(void)
 *
 *********************************************************************************************************************/


#define AuxiliaryBbSwitch_HMICtrl_START_SEC_CODE
#include "AuxiliaryBbSwitch_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AuxiliaryBbSwitch_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AuxSwitch1SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_AuxSwitch2SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_AuxSwitch3SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_AuxSwitch4SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_AuxSwitch5SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_AuxSwitch6SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_AuxSwitchBbLoad1_Status_AuxSwitchBbLoad1_Status(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_AuxSwitchBbLoad2_Status_AuxSwitchBbLoad2_Status(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_AuxSwitchBbLoad3_Status_AuxSwitchBbLoad3_Status(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_AuxSwitchBbLoad4_Status_AuxSwitchBbLoad4_Status(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_AuxSwitchBbLoad5_Status_AuxSwitchBbLoad5_Status(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_AuxSwitchBbLoad6_Status_AuxSwitchBbLoad6_Status(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_WRCAux1Request_WRCAuxRequest(OffOn_T *data)
 *   Std_ReturnType Rte_Read_WRCAux2Request_WRCAuxRequest(OffOn_T *data)
 *   Std_ReturnType Rte_Read_WRCAux3Request_WRCAuxRequest(OffOn_T *data)
 *   Std_ReturnType Rte_Read_WRCAux4Request_WRCAuxRequest(OffOn_T *data)
 *   Std_ReturnType Rte_Read_WRCAux5Request_WRCAuxRequest(OffOn_T *data)
 *   Std_ReturnType Rte_Read_WRCAux6Request_WRCAuxRequest(OffOn_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_AuxBbSwitch1_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_AuxBbSwitch2_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_AuxBbSwitch3_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_AuxBbSwitch4_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_AuxBbSwitch5_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_AuxBbSwitch6_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_AuxSwitchBbLoad1_Request_AuxSwitchBbLoad1_Request(OffOn_T data)
 *   Std_ReturnType Rte_Write_AuxSwitchBbLoad2_Request_AuxSwitchBbLoad2_Request(OffOn_T data)
 *   Std_ReturnType Rte_Write_AuxSwitchBbLoad3_Request_AuxSwitchBbLoad3_Request(OffOn_T data)
 *   Std_ReturnType Rte_Write_AuxSwitchBbLoad4_Request_AuxSwitchBbLoad4_Request(OffOn_T data)
 *   Std_ReturnType Rte_Write_AuxSwitchBbLoad5_Request_AuxSwitchBbLoad5_Request(OffOn_T data)
 *   Std_ReturnType Rte_Write_AuxSwitchBbLoad6_Request_AuxSwitchBbLoad6_Request(OffOn_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AuxiliaryBbSwitch_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, AuxiliaryBbSwitch_HMICtrl_CODE) AuxiliaryBbSwitch_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AuxiliaryBbSwitch_HMICtrl_20ms_runnable
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AuxiliaryBbSwitch_HMICtrl_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AuxiliaryBbSwitch_HMICtrl_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, AuxiliaryBbSwitch_HMICtrl_CODE) AuxiliaryBbSwitch_HMICtrl_init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AuxiliaryBbSwitch_HMICtrl_init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define AuxiliaryBbSwitch_HMICtrl_STOP_SEC_CODE
#include "AuxiliaryBbSwitch_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
