/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: BswM
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: BswM_Callout_Stubs.c
 *   Generation Time: 2020-10-27 10:15:05
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK User Version>                           DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/*********************************************************************************************************************
    INCLUDES
 *********************************************************************************************************************/
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
#define BSWM_CALLOUT_STUBS_SOURCE
#include "BswM.h"
#include "BswM_Private_Cfg.h"



/**********************************************************************************************************************
 *  Additional configured User includes
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK User Includes>                          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/



/**********************************************************************************************************************
 *  CALLOUT FUNCTIONS
 *********************************************************************************************************************/
#define BSWM_START_SEC_CODE
#include "BswM_vMemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *  GENERIC CALLOUTS
 *********************************************************************************************************************/
FUNC(void, BSWM_CODE) BswM_AL_SetProgrammableInterrupts(void)
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK BswM_AL_SetProgrammableInterrupts>      DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

} /* End of BswM_AL_SetProgrammableInterrupts */


FUNC(void, BSWM_CODE) BswM_ESH_OnEnterPostRun(void)
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK BswM_ESH_OnEnterPostRun>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

} /* End of BswM_ESH_OnEnterPostRun */


FUNC(void, BSWM_CODE) BswM_ESH_OnEnterPrepShutdown(void)
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK BswM_ESH_OnEnterPrepShutdown>           DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

} /* End of BswM_ESH_OnEnterPrepShutdown */


FUNC(void, BSWM_CODE) BswM_ESH_OnEnterRun(void)
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK BswM_ESH_OnEnterRun>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

} /* End of BswM_ESH_OnEnterRun */


FUNC(void, BSWM_CODE) BswM_ESH_OnEnterShutdown(void)
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK BswM_ESH_OnEnterShutdown>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

} /* End of BswM_ESH_OnEnterShutdown */


FUNC(void, BSWM_CODE) BswM_ESH_OnEnterWaitForNvm(void)
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK BswM_ESH_OnEnterWaitForNvm>             DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

} /* End of BswM_ESH_OnEnterWaitForNvm */


FUNC(void, BSWM_CODE) BswM_ESH_OnEnterWakeup(void)
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK BswM_ESH_OnEnterWakeup>                 DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

} /* End of BswM_ESH_OnEnterWakeup */


FUNC(void, BSWM_CODE) BswM_INIT_NvMReadAll(void)
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK BswM_INIT_NvMReadAll>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

} /* End of BswM_INIT_NvMReadAll */


FUNC(void, BSWM_CODE) ESH_ComM_CheckPendingRequests(void)
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK ESH_ComM_CheckPendingRequests>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
  ComM_StateType CN_Backbone1J1939_0b1f4bae = COMM_NO_COM_NO_PENDING_REQUEST;
  ComM_StateType CN_LIN02_c2d7c6f3 = COMM_NO_COM_NO_PENDING_REQUEST;
  ComM_StateType CN_LIN04_2bb463c6 = COMM_NO_COM_NO_PENDING_REQUEST;
  ComM_StateType CN_CabSubnet_9ea693f1 = COMM_NO_COM_NO_PENDING_REQUEST;
  ComM_StateType CN_LIN03_b5d0f665 = COMM_NO_COM_NO_PENDING_REQUEST;
  ComM_StateType CN_LIN00_2cd9a7df = COMM_NO_COM_NO_PENDING_REQUEST;
  ComM_StateType CN_Backbone2_78967e2c = COMM_NO_COM_NO_PENDING_REQUEST;
  ComM_StateType CN_LIN01_5bde9749 = COMM_NO_COM_NO_PENDING_REQUEST;
  ComM_StateType CN_FMSNet_fce1aae5 = COMM_NO_COM_NO_PENDING_REQUEST;
  ComM_StateType CN_SecuritySubnet_e7a0ee54 = COMM_NO_COM_NO_PENDING_REQUEST;
  ComM_StateType CN_LIN06_c5ba02ea = COMM_NO_COM_NO_PENDING_REQUEST;
  ComM_StateType CN_LIN05_5cb35350 = COMM_NO_COM_NO_PENDING_REQUEST;
  ComM_StateType CN_LIN07_b2bd327c = COMM_NO_COM_NO_PENDING_REQUEST;
  ComM_StateType CN_CAN6_b040c073 = COMM_NO_COM_NO_PENDING_REQUEST;
  
  (void)ComM_GetState(ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae, &CN_Backbone1J1939_0b1f4bae); /* SBSW_BSWM_FCTCALL_LOCALVAR */
  (void)ComM_GetState(ComMConf_ComMChannel_CN_LIN02_c2d7c6f3, &CN_LIN02_c2d7c6f3); /* SBSW_BSWM_FCTCALL_LOCALVAR */
  (void)ComM_GetState(ComMConf_ComMChannel_CN_LIN04_2bb463c6, &CN_LIN04_2bb463c6); /* SBSW_BSWM_FCTCALL_LOCALVAR */
  (void)ComM_GetState(ComMConf_ComMChannel_CN_CabSubnet_9ea693f1, &CN_CabSubnet_9ea693f1); /* SBSW_BSWM_FCTCALL_LOCALVAR */
  (void)ComM_GetState(ComMConf_ComMChannel_CN_LIN03_b5d0f665, &CN_LIN03_b5d0f665); /* SBSW_BSWM_FCTCALL_LOCALVAR */
  (void)ComM_GetState(ComMConf_ComMChannel_CN_LIN00_2cd9a7df, &CN_LIN00_2cd9a7df); /* SBSW_BSWM_FCTCALL_LOCALVAR */
  (void)ComM_GetState(ComMConf_ComMChannel_CN_Backbone2_78967e2c, &CN_Backbone2_78967e2c); /* SBSW_BSWM_FCTCALL_LOCALVAR */
  (void)ComM_GetState(ComMConf_ComMChannel_CN_LIN01_5bde9749, &CN_LIN01_5bde9749); /* SBSW_BSWM_FCTCALL_LOCALVAR */
  (void)ComM_GetState(ComMConf_ComMChannel_CN_FMSNet_fce1aae5, &CN_FMSNet_fce1aae5); /* SBSW_BSWM_FCTCALL_LOCALVAR */
  (void)ComM_GetState(ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54, &CN_SecuritySubnet_e7a0ee54); /* SBSW_BSWM_FCTCALL_LOCALVAR */
  (void)ComM_GetState(ComMConf_ComMChannel_CN_LIN06_c5ba02ea, &CN_LIN06_c5ba02ea); /* SBSW_BSWM_FCTCALL_LOCALVAR */
  (void)ComM_GetState(ComMConf_ComMChannel_CN_LIN05_5cb35350, &CN_LIN05_5cb35350); /* SBSW_BSWM_FCTCALL_LOCALVAR */
  (void)ComM_GetState(ComMConf_ComMChannel_CN_LIN07_b2bd327c, &CN_LIN07_b2bd327c); /* SBSW_BSWM_FCTCALL_LOCALVAR */
  (void)ComM_GetState(ComMConf_ComMChannel_CN_CAN6_b040c073, &CN_CAN6_b040c073); /* SBSW_BSWM_FCTCALL_LOCALVAR */
  
  if((CN_Backbone1J1939_0b1f4bae != COMM_NO_COM_NO_PENDING_REQUEST) || (CN_LIN02_c2d7c6f3 != COMM_NO_COM_NO_PENDING_REQUEST) || (CN_LIN04_2bb463c6 != COMM_NO_COM_NO_PENDING_REQUEST) || (CN_CabSubnet_9ea693f1 != COMM_NO_COM_NO_PENDING_REQUEST) || (CN_LIN03_b5d0f665 != COMM_NO_COM_NO_PENDING_REQUEST) || (CN_LIN00_2cd9a7df != COMM_NO_COM_NO_PENDING_REQUEST) || (CN_Backbone2_78967e2c != COMM_NO_COM_NO_PENDING_REQUEST) || (CN_LIN01_5bde9749 != COMM_NO_COM_NO_PENDING_REQUEST) || (CN_FMSNet_fce1aae5 != COMM_NO_COM_NO_PENDING_REQUEST) || (CN_SecuritySubnet_e7a0ee54 != COMM_NO_COM_NO_PENDING_REQUEST) || (CN_LIN06_c5ba02ea != COMM_NO_COM_NO_PENDING_REQUEST) || (CN_LIN05_5cb35350 != COMM_NO_COM_NO_PENDING_REQUEST) || (CN_LIN07_b2bd327c != COMM_NO_COM_NO_PENDING_REQUEST) || (CN_CAN6_b040c073 != COMM_NO_COM_NO_PENDING_REQUEST))
  {
    BswM_RequestMode(BSWM_GENERIC_ESH_ComMPendingRequests, BSWM_GENERICVALUE_ESH_ComMPendingRequests_ESH_COMM_PENDING_REQUEST);
  }
  else
  {
    BswM_RequestMode(BSWM_GENERIC_ESH_ComMPendingRequests, BSWM_GENERICVALUE_ESH_ComMPendingRequests_ESH_COMM_NO_REQUEST);
  }
  
} /* End of ESH_ComM_CheckPendingRequests */


FUNC(void, BSWM_CODE) Wakeup_BSWModule_Init(void)
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK Wakeup_BSWModule_Init>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
     if (WakeupTimerFunc!=WAKEUPTOFLASH)
     {
        CanIf_Init(CanIf_Config_Ptr);
        LinIf_Init(LinIf_Config_Ptr);
        CanTp_Init(CanTp_Config_Ptr);
        J1939Tp_Init(J1939Tp_Config_Ptr);
        LinTp_Init(LinTp_Config_Ptr);
        PduR_Init(PduR_Config_Ptr);
        J1939Rm_Init(J1939Rm_Config_Ptr);
        Com_Init(Com_Config_Ptr);
        CanSM_Init(CanSM_Config_Ptr);
        CanNm_Init(CanNm_Config_Ptr);
        J1939Nm_Init(J1939Nm_Config_Ptr);
        Issm_Init(Issm_Config_Ptr);
        Dcm_Init(NULL_PTR);
        DcmAddOn_Init();
        LinSM_Init(LinSM_Config_Ptr);
        Issm_Init(Issm_Config_Ptr);
  
     	  (void)ComM_RequestComMode(ComMConf_ComMUser_CN_LIN00_ace1a6ba, COMM_FULL_COMMUNICATION);
  	  (void)ComM_RequestComMode(ComMConf_ComMUser_CN_LIN01_4323cd84, COMM_FULL_COMMUNICATION);
  	  (void)ComM_RequestComMode(ComMConf_ComMUser_CN_LIN02_a8147687, COMM_FULL_COMMUNICATION);
  	  (void)ComM_RequestComMode(ComMConf_ComMUser_CN_LIN03_47d61db9, COMM_FULL_COMMUNICATION);
  	  (void)ComM_RequestComMode(ComMConf_ComMUser_CN_LIN04_a50a06c0, COMM_FULL_COMMUNICATION);	  
     }
     else
     {
        CDD_WakeupStart();
     }
} /* End of Wakeup_BSWModule_Init */




#define BSWM_STOP_SEC_CODE
#include "BswM_vMemMap.h"  /* PRQA S 5087 */ /* MD_MSR_MemMap */

#if 0
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK Rte_BufferCopy>                         DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>                                       DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#endif


/**********************************************************************************************************************
 *  END OF FILE: BSWM_CALLOUT_STUBS.C
 *********************************************************************************************************************/


