/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Application_Cfg.h
 *   Generation Time: 2020-11-11 14:25:32
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

#ifndef OS_APPLICATION_CFG_H 
# define OS_APPLICATION_CFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
# include "Std_Types.h"


/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* Number of application objects: OsApplication_Trusted_Core0 */
# define OS_CFG_NUM_APP_OSAPPLICATION_TRUSTED_CORE0_ALARMS             (5uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_TRUSTED_CORE0_COUNTERS           (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_TRUSTED_CORE0_HOOKS              (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_TRUSTED_CORE0_ISRS               (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_TRUSTED_CORE0_CAT1ISRS           (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_TRUSTED_CORE0_SCHTS              (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_TRUSTED_CORE0_TASKS              (5uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_TRUSTED_CORE0_SERVICES           (0uL)

/* Number of application objects: OsApplication_Untrusted_Core0 */
# define OS_CFG_NUM_APP_OSAPPLICATION_UNTRUSTED_CORE0_ALARMS             (4uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_UNTRUSTED_CORE0_COUNTERS           (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_UNTRUSTED_CORE0_HOOKS              (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_UNTRUSTED_CORE0_ISRS               (78uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_UNTRUSTED_CORE0_CAT1ISRS           (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_UNTRUSTED_CORE0_SCHTS              (0uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_UNTRUSTED_CORE0_TASKS              (6uL)
# define OS_CFG_NUM_APP_OSAPPLICATION_UNTRUSTED_CORE0_SERVICES           (0uL)

/* Number of application objects: SystemApplication_OsCore0 */
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_ALARMS             (0uL)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_COUNTERS           (1uL)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_HOOKS              (0uL)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_ISRS               (2uL)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_CAT1ISRS           (0uL)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_SCHTS              (0uL)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_TASKS              (1uL)
# define OS_CFG_NUM_APP_SYSTEMAPPLICATION_OSCORE0_SERVICES           (0uL)


/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/


#endif /* OS_APPLICATION_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_Application_Cfg.h
 *********************************************************************************************************************/
