/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header file containing user defined AUTOSAR types and RTE structures
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_TYPE_H
# define _RTE_TYPE_H

# include "Rte.h"


/**********************************************************************************************************************
 * Data type definitions
 *********************************************************************************************************************/

# define Rte_TypeDef_AxleLoad_T
typedef uint16 AxleLoad_T;

# define Rte_TypeDef_Blockid_T
typedef uint8 Blockid_T;

# define Rte_TypeDef_Boolean
typedef boolean Boolean;

# define Rte_TypeDef_Code32bit_T
typedef uint32 Code32bit_T;

# define Rte_TypeDef_Days8bit_Fact025_T
typedef uint8 Days8bit_Fact025_T;

# define Rte_TypeDef_Debug_PVT_DOWHS1_ReportedValue
typedef boolean Debug_PVT_DOWHS1_ReportedValue;

# define Rte_TypeDef_Debug_PVT_DOWHS2_ReportedValue
typedef boolean Debug_PVT_DOWHS2_ReportedValue;

# define Rte_TypeDef_Debug_PVT_DOWLS2_ReportedValue
typedef boolean Debug_PVT_DOWLS2_ReportedValue;

# define Rte_TypeDef_Debug_PVT_DOWLS3_ReportedValue
typedef boolean Debug_PVT_DOWLS3_ReportedValue;

# define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_Generic1
typedef uint8 Debug_PVT_SCIM_Ctrl_Generic1;

# define Rte_TypeDef_Debug_PVT_SCIM_FlexArrayData1
typedef uint8 Debug_PVT_SCIM_FlexArrayData1;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_12VDCDCFault
typedef boolean Debug_PVT_SCIM_RD_12VDCDCFault;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_12VDCDCVolt
typedef uint8 Debug_PVT_SCIM_RD_12VDCDCVolt;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_12VLivingVolt
typedef uint8 Debug_PVT_SCIM_RD_12VLivingVolt;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_12VParkedVolt
typedef uint8 Debug_PVT_SCIM_RD_12VParkedVolt;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_ADI01_7
typedef uint8 Debug_PVT_SCIM_RD_ADI01_7;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_ADI02_8
typedef uint8 Debug_PVT_SCIM_RD_ADI02_8;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_ADI03_9
typedef uint8 Debug_PVT_SCIM_RD_ADI03_9;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_ADI04_10
typedef uint8 Debug_PVT_SCIM_RD_ADI04_10;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_ADI05_11
typedef uint8 Debug_PVT_SCIM_RD_ADI05_11;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_ADI06_12
typedef uint8 Debug_PVT_SCIM_RD_ADI06_12;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_BHS1_Volt
typedef uint8 Debug_PVT_SCIM_RD_BHS1_Volt;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_BHS2_Volt
typedef uint8 Debug_PVT_SCIM_RD_BHS2_Volt;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_BHS3_Volt
typedef uint8 Debug_PVT_SCIM_RD_BHS3_Volt;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_BHS4_Volt
typedef uint8 Debug_PVT_SCIM_RD_BHS4_Volt;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_BLS1_Volt
typedef uint8 Debug_PVT_SCIM_RD_BLS1_Volt;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_DAI1_2
typedef uint8 Debug_PVT_SCIM_RD_DAI1_2;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_VBAT
typedef uint8 Debug_PVT_SCIM_RD_VBAT;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_WHS1_Freq
typedef uint16 Debug_PVT_SCIM_RD_WHS1_Freq;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_WHS1_VD
typedef uint8 Debug_PVT_SCIM_RD_WHS1_VD;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_WHS2_Freq
typedef uint16 Debug_PVT_SCIM_RD_WHS2_Freq;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_WHS2_VD
typedef uint8 Debug_PVT_SCIM_RD_WHS2_VD;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_WLS2_Freq
typedef uint16 Debug_PVT_SCIM_RD_WLS2_Freq;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_WLS2_VD
typedef uint8 Debug_PVT_SCIM_RD_WLS2_VD;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_WLS3_Freq
typedef uint16 Debug_PVT_SCIM_RD_WLS3_Freq;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_WLS3_VD
typedef uint8 Debug_PVT_SCIM_RD_WLS3_VD;

# define Rte_TypeDef_Debug_PVT_SCIM_TSincePwrOn_Long
typedef uint16 Debug_PVT_SCIM_TSincePwrOn_Long;

# define Rte_TypeDef_Debug_PVT_SCIM_TSincePwrOn_Short
typedef uint8 Debug_PVT_SCIM_TSincePwrOn_Short;

# define Rte_TypeDef_Debug_PVT_SCIM_TSinceWkUp_Short
typedef uint8 Debug_PVT_SCIM_TSinceWkUp_Short;

# define Rte_TypeDef_Debug_PVT_ScimHwSelect_WHS1
typedef boolean Debug_PVT_ScimHwSelect_WHS1;

# define Rte_TypeDef_Debug_PVT_ScimHwSelect_WHS2
typedef boolean Debug_PVT_ScimHwSelect_WHS2;

# define Rte_TypeDef_Debug_PVT_ScimHwSelect_WLS2
typedef boolean Debug_PVT_ScimHwSelect_WLS2;

# define Rte_TypeDef_Debug_PVT_ScimHwSelect_WLS3
typedef boolean Debug_PVT_ScimHwSelect_WLS3;

# define Rte_TypeDef_Debug_PVT_ScimHw_W_Duty
typedef uint8 Debug_PVT_ScimHw_W_Duty;

# define Rte_TypeDef_Debug_PVT_ScimHw_W_Freq
typedef uint16 Debug_PVT_ScimHw_W_Freq;

# define Rte_TypeDef_Debug_SCIM_RD_Generic1
typedef uint8 Debug_SCIM_RD_Generic1;

# define Rte_TypeDef_Debug_SCIM_RD_Generic2
typedef uint8 Debug_SCIM_RD_Generic2;

# define Rte_TypeDef_Debug_SCIM_RD_Generic3
typedef uint8 Debug_SCIM_RD_Generic3;

# define Rte_TypeDef_Debug_SCIM_RD_Generic4
typedef uint8 Debug_SCIM_RD_Generic4;

# define Rte_TypeDef_Debug_SCIM_RD_Generic5
typedef boolean Debug_SCIM_RD_Generic5;

# define Rte_TypeDef_Debug_SCIM_RD_Generic6
typedef uint8 Debug_SCIM_RD_Generic6;

# define Rte_TypeDef_DiagInfo_T
typedef uint8 DiagInfo_T;

# define Rte_TypeDef_Distance32bit_T
typedef uint32 Distance32bit_T;

# define Rte_TypeDef_DtcIdA_T
typedef uint16 DtcIdA_T;

# define Rte_TypeDef_DtcIdB_T
typedef uint16 DtcIdB_T;

# define Rte_TypeDef_EcuAdr_T
typedef uint8 EcuAdr_T;

# define Rte_TypeDef_EngineTemp_T
typedef uint8 EngineTemp_T;

# define Rte_TypeDef_EngineTotalHoursOfOperation_T
typedef uint32 EngineTotalHoursOfOperation_T;

# define Rte_TypeDef_EventFlag_T
typedef boolean EventFlag_T;

# define Rte_TypeDef_FSP1ResponseErrorL1_T
typedef boolean FSP1ResponseErrorL1_T;

# define Rte_TypeDef_FSP1ResponseErrorL2_T
typedef boolean FSP1ResponseErrorL2_T;

# define Rte_TypeDef_FSP1ResponseErrorL3_T
typedef boolean FSP1ResponseErrorL3_T;

# define Rte_TypeDef_FSP1ResponseErrorL4_T
typedef boolean FSP1ResponseErrorL4_T;

# define Rte_TypeDef_FSP1ResponseErrorL5_T
typedef boolean FSP1ResponseErrorL5_T;

# define Rte_TypeDef_FSP2ResponseErrorL1_T
typedef boolean FSP2ResponseErrorL1_T;

# define Rte_TypeDef_FSP2ResponseErrorL2_T
typedef boolean FSP2ResponseErrorL2_T;

# define Rte_TypeDef_FSP2ResponseErrorL3_T
typedef boolean FSP2ResponseErrorL3_T;

# define Rte_TypeDef_FSP3ResponseErrorL2_T
typedef boolean FSP3ResponseErrorL2_T;

# define Rte_TypeDef_FSP4ResponseErrorL2_T
typedef boolean FSP4ResponseErrorL2_T;

# define Rte_TypeDef_FSPDiagInfo_T
typedef uint8 FSPDiagInfo_T;

# define Rte_TypeDef_FSPIndicationCmd_T
typedef uint16 FSPIndicationCmd_T;

# define Rte_TypeDef_FSPSwitchStatus_T
typedef uint8 FSPSwitchStatus_T;

# define Rte_TypeDef_FailTA_T
typedef uint8 FailTA_T;

# define Rte_TypeDef_FailTB_T
typedef uint8 FailTB_T;

# define Rte_TypeDef_FuelRate_T
typedef uint16 FuelRate_T;

# define Rte_TypeDef_GasRate_T
typedef uint16 GasRate_T;

# define Rte_TypeDef_Hours8bit_T
typedef uint8 Hours8bit_T;

# define Rte_TypeDef_InstantFuelEconomy_T
typedef uint16 InstantFuelEconomy_T;

# define Rte_TypeDef_Int8Bit_T
typedef uint8 Int8Bit_T;

# define Rte_TypeDef_IntLghtLvlIndScaled_cmd_T
typedef uint8 IntLghtLvlIndScaled_cmd_T;

# define Rte_TypeDef_MaintServiceID_T
typedef uint8 MaintServiceID_T;

# define Rte_TypeDef_Minutes8bit_T
typedef uint8 Minutes8bit_T;

# define Rte_TypeDef_Months8bit_T
typedef uint8 Months8bit_T;

# define Rte_TypeDef_PGNRequest_T
typedef uint32 PGNRequest_T;

# define Rte_TypeDef_Percent8bit125NegOffset_T
typedef uint8 Percent8bit125NegOffset_T;

# define Rte_TypeDef_Percent8bitFactor04_T
typedef uint8 Percent8bitFactor04_T;

# define Rte_TypeDef_Percent8bitNoOffset_T
typedef uint8 Percent8bitNoOffset_T;

# define Rte_TypeDef_PinCode_validity_time_T
typedef uint8 PinCode_validity_time_T;

# define Rte_TypeDef_PressureFactor8_T
typedef uint8 PressureFactor8_T;

# define Rte_TypeDef_RemainDistOilChange_T
typedef uint16 RemainDistOilChange_T;

# define Rte_TypeDef_RemainEngineTimeOilChange_T
typedef uint16 RemainEngineTimeOilChange_T;

# define Rte_TypeDef_ResponseErrorCCFW
typedef boolean ResponseErrorCCFW;

# define Rte_TypeDef_ResponseErrorDLFW_T
typedef boolean ResponseErrorDLFW_T;

# define Rte_TypeDef_ResponseErrorELCP1_T
typedef boolean ResponseErrorELCP1_T;

# define Rte_TypeDef_ResponseErrorELCP2_T
typedef boolean ResponseErrorELCP2_T;

# define Rte_TypeDef_ResponseErrorILCP1_T
typedef boolean ResponseErrorILCP1_T;

# define Rte_TypeDef_ResponseErrorILCP2_T
typedef boolean ResponseErrorILCP2_T;

# define Rte_TypeDef_ResponseErrorLECM2_T
typedef boolean ResponseErrorLECM2_T;

# define Rte_TypeDef_ResponseErrorLECMBasic_T
typedef boolean ResponseErrorLECMBasic_T;

# define Rte_TypeDef_ResponseErrorRCECS
typedef boolean ResponseErrorRCECS;

# define Rte_TypeDef_Rte_DT_EngTraceHWData_T_0
typedef uint8 Rte_DT_EngTraceHWData_T_0;

# define Rte_TypeDef_Rte_DT_EngTraceHWData_T_10
typedef uint8 Rte_DT_EngTraceHWData_T_10;

# define Rte_TypeDef_Rte_DT_EngTraceHWData_T_11
typedef uint8 Rte_DT_EngTraceHWData_T_11;

# define Rte_TypeDef_Rte_DT_EngTraceHWData_T_2
typedef uint8 Rte_DT_EngTraceHWData_T_2;

# define Rte_TypeDef_Rte_DT_EngTraceHWData_T_3
typedef uint8 Rte_DT_EngTraceHWData_T_3;

# define Rte_TypeDef_Rte_DT_EngTraceHWData_T_4
typedef uint8 Rte_DT_EngTraceHWData_T_4;

# define Rte_TypeDef_Rte_DT_EngTraceHWData_T_8
typedef uint16 Rte_DT_EngTraceHWData_T_8;

# define Rte_TypeDef_Rte_DT_EngTraceHWData_T_9
typedef uint8 Rte_DT_EngTraceHWData_T_9;

# define Rte_TypeDef_SEWS_ComCryptoKey_P1DLX_T
typedef uint8 SEWS_ComCryptoKey_P1DLX_T;

# define Rte_TypeDef_Seconds32bitExtra_T
typedef uint32 Seconds32bitExtra_T;

# define Rte_TypeDef_Seconds8bitFact025_T
typedef uint8 Seconds8bitFact025_T;

# define Rte_TypeDef_ServiceDistance16BitFact5Offset_T
typedef uint16 ServiceDistance16BitFact5Offset_T;

# define Rte_TypeDef_ServiceDistance_T
typedef uint32 ServiceDistance_T;

# define Rte_TypeDef_ServiceTime_T
typedef uint32 ServiceTime_T;

# define Rte_TypeDef_ShortPulseMaxLength_T
typedef uint8 ShortPulseMaxLength_T;

# define Rte_TypeDef_Speed16bit_T
typedef uint16 Speed16bit_T;

# define Rte_TypeDef_SpeedRpm16bit_T
typedef uint16 SpeedRpm16bit_T;

# define Rte_TypeDef_SwitchDetectionNeeded_T
typedef boolean SwitchDetectionNeeded_T;

# define Rte_TypeDef_Temperature16bit_T
typedef uint16 Temperature16bit_T;

# define Rte_TypeDef_TimeMinuteType_T
typedef uint8 TimeMinuteType_T;

# define Rte_TypeDef_TimesetHr_T
typedef uint8 TimesetHr_T;

# define Rte_TypeDef_TotalLngConsumed_T
typedef uint32 TotalLngConsumed_T;

# define Rte_TypeDef_UInt16
typedef uint16 UInt16;

# define Rte_TypeDef_UInt32
typedef uint32 UInt32;

# define Rte_TypeDef_UInt32_Length
typedef uint32 UInt32_Length;

# define Rte_TypeDef_UInt8
typedef uint8 UInt8;

# define Rte_TypeDef_VIN_rqst_T
typedef boolean VIN_rqst_T;

# define Rte_TypeDef_VehicleWeight16bit_T
typedef uint16 VehicleWeight16bit_T;

# define Rte_TypeDef_Volume32BitFact001_T
typedef uint32 Volume32BitFact001_T;

# define Rte_TypeDef_Volume32bitFact05_T
typedef uint32 Volume32bitFact05_T;

# define Rte_TypeDef_Volume32bit_T
typedef uint32 Volume32bit_T;

# define Rte_TypeDef_VolumeValueType_T
typedef uint8 VolumeValueType_T;

# define Rte_TypeDef_Years8bit_T
typedef uint8 Years8bit_T;

# define Rte_TypeDef_dtRef_VOID
typedef void * dtRef_VOID;

# define Rte_TypeDef_dtRef_const_VOID
typedef const void * dtRef_const_VOID;

# define Rte_TypeDef_A2PosSwitchStatus_T
typedef uint8 A2PosSwitchStatus_T;

# define Rte_TypeDef_A3PosSwitchStatus_T
typedef uint8 A3PosSwitchStatus_T;

# define Rte_TypeDef_AcceleratorPedalStatus_T
typedef uint8 AcceleratorPedalStatus_T;

# define Rte_TypeDef_Ack2Bit_T
typedef uint8 Ack2Bit_T;

# define Rte_TypeDef_AlarmClkID_T
typedef uint8 AlarmClkID_T;

# define Rte_TypeDef_AlarmClkStat_T
typedef uint8 AlarmClkStat_T;

# define Rte_TypeDef_AlarmClkType_T
typedef uint8 AlarmClkType_T;

# define Rte_TypeDef_AlarmStatus_stat_T
typedef uint8 AlarmStatus_stat_T;

# define Rte_TypeDef_AltLoadDistribution_rqst_T
typedef uint8 AltLoadDistribution_rqst_T;

# define Rte_TypeDef_AutoRelock_rqst_T
typedef uint8 AutoRelock_rqst_T;

# define Rte_TypeDef_AutorelockingMovements_stat_T
typedef uint8 AutorelockingMovements_stat_T;

# define Rte_TypeDef_BBNetworkExtraSideWrknLights_T
typedef uint8 BBNetworkExtraSideWrknLights_T;

# define Rte_TypeDef_BTStatus_T
typedef uint8 BTStatus_T;

# define Rte_TypeDef_BackToDriveReqACK_T
typedef uint8 BackToDriveReqACK_T;

# define Rte_TypeDef_BackToDriveReq_T
typedef uint8 BackToDriveReq_T;

# define Rte_TypeDef_BrakeSwitch_T
typedef uint8 BrakeSwitch_T;

# define Rte_TypeDef_ButtonAuth_rqst_T
typedef uint8 ButtonAuth_rqst_T;

# define Rte_TypeDef_ButtonStatus_T
typedef uint8 ButtonStatus_T;

# define Rte_TypeDef_CCIM_ACC_T
typedef uint8 CCIM_ACC_T;

# define Rte_TypeDef_CCStates_T
typedef uint8 CCStates_T;

# define Rte_TypeDef_CM_Status_T
typedef uint8 CM_Status_T;

# define Rte_TypeDef_CabExtraSideWorkingLight_rqst_T
typedef uint8 CabExtraSideWorkingLight_rqst_T;

# define Rte_TypeDef_CddLinTp_Status
typedef uint8 CddLinTp_Status;

# define Rte_TypeDef_ChangeKneelACK_T
typedef uint8 ChangeKneelACK_T;

# define Rte_TypeDef_ChangeRequest2Bit_T
typedef uint8 ChangeRequest2Bit_T;

# define Rte_TypeDef_ClosedOpen_T
typedef uint8 ClosedOpen_T;

# define Rte_TypeDef_ClutchSwitch_T
typedef uint8 ClutchSwitch_T;

# define Rte_TypeDef_CollSituationHMICtrlRequestVM_T
typedef uint8 CollSituationHMICtrlRequestVM_T;

# define Rte_TypeDef_ComMode_LIN_Type
typedef uint8 ComMode_LIN_Type;

# define Rte_TypeDef_Csm_ReturnType
typedef uint8 Csm_ReturnType;

# define Rte_TypeDef_DASSystemStatus_T
typedef uint8 DASSystemStatus_T;

# define Rte_TypeDef_Dcm_NegativeResponseCodeType
typedef uint8 Dcm_NegativeResponseCodeType;

# define Rte_TypeDef_Dcm_OpStatusType
typedef uint8 Dcm_OpStatusType;

# define Rte_TypeDef_DeactivateActivate_T
typedef uint8 DeactivateActivate_T;

# define Rte_TypeDef_Debug_PVT_ADI_ReportGroup
typedef uint8 Debug_PVT_ADI_ReportGroup;

# define Rte_TypeDef_Debug_PVT_ADI_ReportRequest
typedef uint8 Debug_PVT_ADI_ReportRequest;

# define Rte_TypeDef_Debug_PVT_FlexDataRequest
typedef uint8 Debug_PVT_FlexDataRequest;

# define Rte_TypeDef_Debug_PVT_LF_Trig
typedef uint8 Debug_PVT_LF_Trig;

# define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_12VDCDC
typedef uint8 Debug_PVT_SCIM_Ctrl_12VDCDC;

# define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_12VLiving
typedef uint8 Debug_PVT_SCIM_Ctrl_12VLiving;

# define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_12VParked
typedef uint8 Debug_PVT_SCIM_Ctrl_12VParked;

# define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_BHS1
typedef uint8 Debug_PVT_SCIM_Ctrl_BHS1;

# define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_BHS2
typedef uint8 Debug_PVT_SCIM_Ctrl_BHS2;

# define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_BHS3
typedef uint8 Debug_PVT_SCIM_Ctrl_BHS3;

# define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_BHS4
typedef uint8 Debug_PVT_SCIM_Ctrl_BHS4;

# define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_BLS1
typedef uint8 Debug_PVT_SCIM_Ctrl_BLS1;

# define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_DAIPullUp
typedef uint8 Debug_PVT_SCIM_Ctrl_DAIPullUp;

# define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_WHS1
typedef uint8 Debug_PVT_SCIM_Ctrl_WHS1;

# define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_WHS2
typedef uint8 Debug_PVT_SCIM_Ctrl_WHS2;

# define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_WLS2
typedef uint8 Debug_PVT_SCIM_Ctrl_WLS2;

# define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_WLS3
typedef uint8 Debug_PVT_SCIM_Ctrl_WLS3;

# define Rte_TypeDef_Debug_PVT_SCIM_FlexArrayDataId
typedef uint8 Debug_PVT_SCIM_FlexArrayDataId;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_12VLivingFault
typedef uint8 Debug_PVT_SCIM_RD_12VLivingFault;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_12VParkedFault
typedef uint8 Debug_PVT_SCIM_RD_12VParkedFault;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_BHS1_Fault
typedef uint8 Debug_PVT_SCIM_RD_BHS1_Fault;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_BHS2_Fault
typedef uint8 Debug_PVT_SCIM_RD_BHS2_Fault;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_BHS3_Fault
typedef uint8 Debug_PVT_SCIM_RD_BHS3_Fault;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_BHS4_Fault
typedef uint8 Debug_PVT_SCIM_RD_BHS4_Fault;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_BLS1_Fault
typedef uint8 Debug_PVT_SCIM_RD_BLS1_Fault;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_VBAT_Fault
typedef uint8 Debug_PVT_SCIM_RD_VBAT_Fault;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_WHS1_Fault
typedef uint8 Debug_PVT_SCIM_RD_WHS1_Fault;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_WHS2_Fault
typedef uint8 Debug_PVT_SCIM_RD_WHS2_Fault;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_WLS2_Fault
typedef uint8 Debug_PVT_SCIM_RD_WLS2_Fault;

# define Rte_TypeDef_Debug_PVT_SCIM_RD_WLS3_Fault
typedef uint8 Debug_PVT_SCIM_RD_WLS3_Fault;

# define Rte_TypeDef_DeviceAuthentication_rqst_T
typedef uint8 DeviceAuthentication_rqst_T;

# define Rte_TypeDef_DeviceInCab_stat_T
typedef uint8 DeviceInCab_stat_T;

# define Rte_TypeDef_DeviceIndication_T
typedef uint8 DeviceIndication_T;

# define Rte_TypeDef_DiagActiveState_T
typedef uint8 DiagActiveState_T;

# define Rte_TypeDef_DirectionIndicator_T
typedef uint8 DirectionIndicator_T;

# define Rte_TypeDef_DisableEnable_T
typedef uint8 DisableEnable_T;

# define Rte_TypeDef_DisengageEngage_T
typedef uint8 DisengageEngage_T;

# define Rte_TypeDef_DoorAjar_stat_T
typedef uint8 DoorAjar_stat_T;

# define Rte_TypeDef_DoorLatch_rqst_decrypt_T
typedef uint8 DoorLatch_rqst_decrypt_T;

# define Rte_TypeDef_DoorLatch_stat_T
typedef uint8 DoorLatch_stat_T;

# define Rte_TypeDef_DoorLockUnlock_T
typedef uint8 DoorLockUnlock_T;

# define Rte_TypeDef_DoorLock_stat_T
typedef uint8 DoorLock_stat_T;

# define Rte_TypeDef_DoorsAjar_stat_T
typedef uint8 DoorsAjar_stat_T;

# define Rte_TypeDef_Driver1TimeRelatedStates_T
typedef uint8 Driver1TimeRelatedStates_T;

# define Rte_TypeDef_DriverAuthDeviceMatching_T
typedef uint8 DriverAuthDeviceMatching_T;

# define Rte_TypeDef_DriverMemory_rqst_T
typedef uint8 DriverMemory_rqst_T;

# define Rte_TypeDef_DriverSeatBeltSwitch_T
typedef uint8 DriverSeatBeltSwitch_T;

# define Rte_TypeDef_DriverTimeRelatedStates_T
typedef uint8 DriverTimeRelatedStates_T;

# define Rte_TypeDef_DriverWorkingState_T
typedef uint8 DriverWorkingState_T;

# define Rte_TypeDef_DualDeviceIndication_T
typedef uint8 DualDeviceIndication_T;

# define Rte_TypeDef_DynamicCode_rqst_T
typedef uint8 DynamicCode_rqst_T;

# define Rte_TypeDef_ECSStandByReq_T
typedef uint8 ECSStandByReq_T;

# define Rte_TypeDef_ECSStandByRequest_T
typedef uint8 ECSStandByRequest_T;

# define Rte_TypeDef_ESCDriverReq_T
typedef uint8 ESCDriverReq_T;

# define Rte_TypeDef_ElectricalLoadReduction_rqst_T
typedef uint8 ElectricalLoadReduction_rqst_T;

# define Rte_TypeDef_EmergencyDoorsUnlock_rqst_T
typedef uint8 EmergencyDoorsUnlock_rqst_T;

# define Rte_TypeDef_EngineRetarderTorqueMode_T
typedef uint8 EngineRetarderTorqueMode_T;

# define Rte_TypeDef_EngineSpeedControlStatus_T
typedef uint8 EngineSpeedControlStatus_T;

# define Rte_TypeDef_EngineSpeedRequest_T
typedef uint8 EngineSpeedRequest_T;

# define Rte_TypeDef_EngineStartAuth_rqst_T
typedef uint8 EngineStartAuth_rqst_T;

# define Rte_TypeDef_EngineStartAuth_stat_decrypt_T
typedef uint8 EngineStartAuth_stat_decrypt_T;

# define Rte_TypeDef_EscActionRequest_T
typedef uint8 EscActionRequest_T;

# define Rte_TypeDef_EvalButtonRequest_T
typedef uint8 EvalButtonRequest_T;

# define Rte_TypeDef_ExtraBBCraneStatus_T
typedef uint8 ExtraBBCraneStatus_T;

# define Rte_TypeDef_FPBRChangeReq_T
typedef uint8 FPBRChangeReq_T;

# define Rte_TypeDef_FPBRStatusInd_T
typedef uint8 FPBRStatusInd_T;

# define Rte_TypeDef_FWSelectedSpeedControlMode_T
typedef uint8 FWSelectedSpeedControlMode_T;

# define Rte_TypeDef_FalseTrue_T
typedef uint8 FalseTrue_T;

# define Rte_TypeDef_FerryFunctionStatus_T
typedef uint8 FerryFunctionStatus_T;

# define Rte_TypeDef_FreeWheel_Status_T
typedef uint8 FreeWheel_Status_T;

# define Rte_TypeDef_Freewheel_Status_Ctr_T
typedef uint8 Freewheel_Status_Ctr_T;

# define Rte_TypeDef_FrontLidLatch_cmd_T
typedef uint8 FrontLidLatch_cmd_T;

# define Rte_TypeDef_FrontLidLatch_stat_T
typedef uint8 FrontLidLatch_stat_T;

# define Rte_TypeDef_Fsc_OperationalMode_T
typedef uint8 Fsc_OperationalMode_T;

# define Rte_TypeDef_FuelType_T
typedef uint8 FuelType_T;

# define Rte_TypeDef_GearBoxUnlockAuth_rqst_T
typedef uint8 GearBoxUnlockAuth_rqst_T;

# define Rte_TypeDef_GearboxUnlockAuth_stat_decrypt_T
typedef uint8 GearboxUnlockAuth_stat_decrypt_T;

# define Rte_TypeDef_HandlingInformation_T
typedef uint8 HandlingInformation_T;

# define Rte_TypeDef_IL_ModeReq_T
typedef uint8 IL_ModeReq_T;

# define Rte_TypeDef_IL_Mode_T
typedef uint8 IL_Mode_T;

# define Rte_TypeDef_IOCtrlReq_T
typedef uint8 IOCtrlReq_T;

# define Rte_TypeDef_InactiveActive_T
typedef uint8 InactiveActive_T;

# define Rte_TypeDef_IndicationCmd_T
typedef uint8 IndicationCmd_T;

# define Rte_TypeDef_Inhibit_T
typedef uint8 Inhibit_T;

# define Rte_TypeDef_InteriorLightDimming_rqst_T
typedef uint8 InteriorLightDimming_rqst_T;

# define Rte_TypeDef_KeyAuthentication_rqst_T
typedef uint8 KeyAuthentication_rqst_T;

# define Rte_TypeDef_KeyAuthentication_stat_decrypt_T
typedef uint8 KeyAuthentication_stat_decrypt_T;

# define Rte_TypeDef_KeyNotValid_T
typedef uint8 KeyNotValid_T;

# define Rte_TypeDef_KeyPosition_T
typedef uint8 KeyPosition_T;

# define Rte_TypeDef_KeyfobAuth_rqst_T
typedef uint8 KeyfobAuth_rqst_T;

# define Rte_TypeDef_KeyfobAuth_stat_T
typedef uint8 KeyfobAuth_stat_T;

# define Rte_TypeDef_KeyfobInCabLocation_stat_T
typedef uint8 KeyfobInCabLocation_stat_T;

# define Rte_TypeDef_KeyfobInCabPresencePS_T
typedef uint8 KeyfobInCabPresencePS_T;

# define Rte_TypeDef_KeyfobLocation_rqst_T
typedef uint8 KeyfobLocation_rqst_T;

# define Rte_TypeDef_KeyfobOutsideLocation_stat_T
typedef uint8 KeyfobOutsideLocation_stat_T;

# define Rte_TypeDef_KeyfobPanicButton_Status_T
typedef uint8 KeyfobPanicButton_Status_T;

# define Rte_TypeDef_KneelingChangeRequest_T
typedef uint8 KneelingChangeRequest_T;

# define Rte_TypeDef_KneelingStatusHMI_T
typedef uint8 KneelingStatusHMI_T;

# define Rte_TypeDef_LCSSystemStatus_T
typedef uint8 LCSSystemStatus_T;

# define Rte_TypeDef_LKSCorrectiveSteeringStatus_T
typedef uint8 LKSCorrectiveSteeringStatus_T;

# define Rte_TypeDef_LKSStatus_T
typedef uint8 LKSStatus_T;

# define Rte_TypeDef_LevelAdjustmentAction_T
typedef uint8 LevelAdjustmentAction_T;

# define Rte_TypeDef_LevelAdjustmentAxles_T
typedef uint8 LevelAdjustmentAxles_T;

# define Rte_TypeDef_LevelAdjustmentStroke_T
typedef uint8 LevelAdjustmentStroke_T;

# define Rte_TypeDef_LevelChangeRequest_T
typedef uint8 LevelChangeRequest_T;

# define Rte_TypeDef_LevelControlInformation_T
typedef uint8 LevelControlInformation_T;

# define Rte_TypeDef_LevelStrokeRequest_T
typedef uint8 LevelStrokeRequest_T;

# define Rte_TypeDef_LevelUserMemoryAction_T
typedef uint8 LevelUserMemoryAction_T;

# define Rte_TypeDef_LevelUserMemory_T
typedef uint8 LevelUserMemory_T;

# define Rte_TypeDef_LiftAxleLiftPositionRequest_T
typedef uint8 LiftAxleLiftPositionRequest_T;

# define Rte_TypeDef_LiftAxlePositionStatus_T
typedef uint8 LiftAxlePositionStatus_T;

# define Rte_TypeDef_LiftAxleUpRequestACK_T
typedef uint8 LiftAxleUpRequestACK_T;

# define Rte_TypeDef_LinDiagBusInfo
typedef uint8 LinDiagBusInfo;

# define Rte_TypeDef_LinDiagRequest_T
typedef uint8 LinDiagRequest_T;

# define Rte_TypeDef_LinDiagServiceStatus
typedef uint8 LinDiagServiceStatus;

# define Rte_TypeDef_LoadDistributionALDChangeACK_T
typedef uint8 LoadDistributionALDChangeACK_T;

# define Rte_TypeDef_LoadDistributionChangeACK_T
typedef uint8 LoadDistributionChangeACK_T;

# define Rte_TypeDef_LoadDistributionChangeRequest_T
typedef uint8 LoadDistributionChangeRequest_T;

# define Rte_TypeDef_LoadDistributionFuncSelected_T
typedef uint8 LoadDistributionFuncSelected_T;

# define Rte_TypeDef_LoadDistributionRequestedACK_T
typedef uint8 LoadDistributionRequestedACK_T;

# define Rte_TypeDef_LoadDistributionRequested_T
typedef uint8 LoadDistributionRequested_T;

# define Rte_TypeDef_LoadDistributionSelected_T
typedef uint8 LoadDistributionSelected_T;

# define Rte_TypeDef_LockingIndication_rqst_T
typedef uint8 LockingIndication_rqst_T;

# define Rte_TypeDef_LuggageCompart_stat_decrypt_T
typedef uint8 LuggageCompart_stat_decrypt_T;

# define Rte_TypeDef_MirrorFoldingRequest_T
typedef uint8 MirrorFoldingRequest_T;

# define Rte_TypeDef_MirrorHeat_T
typedef uint8 MirrorHeat_T;

# define Rte_TypeDef_NeutralPushed_T
typedef uint8 NeutralPushed_T;

# define Rte_TypeDef_NotDetected_T
typedef uint8 NotDetected_T;

# define Rte_TypeDef_NotEngagedEngaged_T
typedef uint8 NotEngagedEngaged_T;

# define Rte_TypeDef_NotPresentPresent_T
typedef uint8 NotPresentPresent_T;

# define Rte_TypeDef_OffOn_T
typedef uint8 OffOn_T;

# define Rte_TypeDef_OilQuality_T
typedef uint8 OilQuality_T;

# define Rte_TypeDef_OilStatus_T
typedef uint8 OilStatus_T;

# define Rte_TypeDef_ParkHeaterTimer_cmd_T
typedef uint8 ParkHeaterTimer_cmd_T;

# define Rte_TypeDef_PassengersSeatBelt_T
typedef uint8 PassengersSeatBelt_T;

# define Rte_TypeDef_PassiveActive_T
typedef uint8 PassiveActive_T;

# define Rte_TypeDef_PinCode_rqst_T
typedef uint8 PinCode_rqst_T;

# define Rte_TypeDef_PinCode_stat_T
typedef uint8 PinCode_stat_T;

# define Rte_TypeDef_PtoState_T
typedef uint8 PtoState_T;

# define Rte_TypeDef_PtosStatus_T
typedef uint8 PtosStatus_T;

# define Rte_TypeDef_PushButtonStatus_T
typedef uint8 PushButtonStatus_T;

# define Rte_TypeDef_RampLevelRequest_T
typedef uint8 RampLevelRequest_T;

# define Rte_TypeDef_RearAxleSteeringFunctionDsbl_T
typedef uint8 RearAxleSteeringFunctionDsbl_T;

# define Rte_TypeDef_RearAxleSteeringFunctionStatus_T
typedef uint8 RearAxleSteeringFunctionStatus_T;

# define Rte_TypeDef_ReducedSetMode_rqst_decrypt_T
typedef uint8 ReducedSetMode_rqst_decrypt_T;

# define Rte_TypeDef_Request_T
typedef uint8 Request_T;

# define Rte_TypeDef_RetarderTorqueMode_T
typedef uint8 RetarderTorqueMode_T;

# define Rte_TypeDef_ReverseGearEngaged_T
typedef uint8 ReverseGearEngaged_T;

# define Rte_TypeDef_ReverseWarning_rqst_T
typedef uint8 ReverseWarning_rqst_T;

# define Rte_TypeDef_RideHeightFunction_T
typedef uint8 RideHeightFunction_T;

# define Rte_TypeDef_RideHeightStorageRequest_T
typedef uint8 RideHeightStorageRequest_T;

# define Rte_TypeDef_RollRequest_T
typedef uint8 RollRequest_T;

# define Rte_TypeDef_RoofHatch_HMI_rqst_T
typedef uint8 RoofHatch_HMI_rqst_T;

# define Rte_TypeDef_SCIM_ImmoDriver_ProcessingStatus_T
typedef uint8 SCIM_ImmoDriver_ProcessingStatus_T;

# define Rte_TypeDef_SCIM_ImmoType_T
typedef uint8 SCIM_ImmoType_T;

# define Rte_TypeDef_SCIM_PassiveDriver_ProcessingStatus_T
typedef uint8 SCIM_PassiveDriver_ProcessingStatus_T;

# define Rte_TypeDef_SCIM_PassiveSearchCoverage_T
typedef uint8 SCIM_PassiveSearchCoverage_T;

# define Rte_TypeDef_SWSpdCtrlButtonsStatus1_T
typedef uint8 SWSpdCtrlButtonsStatus1_T;

# define Rte_TypeDef_SWSpeedControlAdjustMode_T
typedef uint8 SWSpeedControlAdjustMode_T;

# define Rte_TypeDef_SeatSwivelStatus_T
typedef uint8 SeatSwivelStatus_T;

# define Rte_TypeDef_SetCMOperation_T
typedef uint8 SetCMOperation_T;

# define Rte_TypeDef_SetFCWOperation_T
typedef uint8 SetFCWOperation_T;

# define Rte_TypeDef_SpeedLockingInhibition_stat_T
typedef uint8 SpeedLockingInhibition_stat_T;

# define Rte_TypeDef_StopLevelChangeStatus_T
typedef uint8 StopLevelChangeStatus_T;

# define Rte_TypeDef_StorageAck_T
typedef uint8 StorageAck_T;

# define Rte_TypeDef_Supported_T
typedef uint8 Supported_T;

# define Rte_TypeDef_Synch_Unsynch_Mode_stat_T
typedef uint8 Synch_Unsynch_Mode_stat_T;

# define Rte_TypeDef_SystemEvent_T
typedef uint8 SystemEvent_T;

# define Rte_TypeDef_TachographPerformance_T
typedef uint8 TachographPerformance_T;

# define Rte_TypeDef_TellTaleStatus_T
typedef uint8 TellTaleStatus_T;

# define Rte_TypeDef_TheftAlarmAct_rqst_decrypt_I
typedef uint8 TheftAlarmAct_rqst_decrypt_I;

# define Rte_TypeDef_Thumbwheel_stat_T
typedef uint8 Thumbwheel_stat_T;

# define Rte_TypeDef_TractionControlDriverRqst_T
typedef uint8 TractionControlDriverRqst_T;

# define Rte_TypeDef_TransferCaseNeutral_Req2_T
typedef uint8 TransferCaseNeutral_Req2_T;

# define Rte_TypeDef_TransferCaseNeutral_T
typedef uint8 TransferCaseNeutral_T;

# define Rte_TypeDef_TransferCaseNeutral_status_T
typedef uint8 TransferCaseNeutral_status_T;

# define Rte_TypeDef_TransmissionDrivingMode_T
typedef uint8 TransmissionDrivingMode_T;

# define Rte_TypeDef_VEC_CryptoProxy_IdentificationState_Type
typedef uint8 VEC_CryptoProxy_IdentificationState_Type;

# define Rte_TypeDef_VehicleModeDistribution_T
typedef uint8 VehicleModeDistribution_T;

# define Rte_TypeDef_VehicleMode_T
typedef uint8 VehicleMode_T;

# define Rte_TypeDef_VehicleOverspeed_T
typedef uint8 VehicleOverspeed_T;

# define Rte_TypeDef_WeightClass_T
typedef uint8 WeightClass_T;

# define Rte_TypeDef_WiredLevelUserMemory_T
typedef uint8 WiredLevelUserMemory_T;

# define Rte_TypeDef_XRSLStates_T
typedef uint8 XRSLStates_T;

# define Rte_TypeDef_tVecRomTestBlockStatus
typedef uint8 tVecRomTestBlockStatus;

# define Rte_TypeDef_ArrayByteSize32
typedef uint8 ArrayByteSize32[32];

# define Rte_TypeDef_AsymDecryptDataBuffer
typedef uint8 AsymDecryptDataBuffer[128];

# define Rte_TypeDef_AsymDecryptResultBuffer
typedef UInt8 AsymDecryptResultBuffer[128];

# define Rte_TypeDef_CryptoIdKey_T
typedef uint8 CryptoIdKey_T[16];

# define Rte_TypeDef_Crypto_Function_serialized_T
typedef uint8 Crypto_Function_serialized_T[12];

# define Rte_TypeDef_DataArrayType_uint8_1
typedef uint8 DataArrayType_uint8_1[1];

# define Rte_TypeDef_DataArrayType_uint8_2
typedef uint8 DataArrayType_uint8_2[2];

# define Rte_TypeDef_DataArrayType_uint8_4
typedef uint8 DataArrayType_uint8_4[4];

# define Rte_TypeDef_DataArrayType_uint8_5
typedef uint8 DataArrayType_uint8_5[5];

# define Rte_TypeDef_DataArrayType_uint8_6
typedef uint8 DataArrayType_uint8_6[6];

# define Rte_TypeDef_Dcm_Data116ByteType
typedef UInt8 Dcm_Data116ByteType[116];

# define Rte_TypeDef_Dcm_Data120ByteType
typedef uint8 Dcm_Data120ByteType[120];

# define Rte_TypeDef_Dcm_Data126ByteType
typedef uint8 Dcm_Data126ByteType[126];

# define Rte_TypeDef_Dcm_Data128ByteType
typedef UInt8 Dcm_Data128ByteType[128];

# define Rte_TypeDef_Dcm_Data12ByteType
typedef uint8 Dcm_Data12ByteType[12];

# define Rte_TypeDef_Dcm_Data130ByteType
typedef uint8 Dcm_Data130ByteType[130];

# define Rte_TypeDef_Dcm_Data16ByteType
typedef uint8 Dcm_Data16ByteType[16];

# define Rte_TypeDef_Dcm_Data17ByteType
typedef uint8 Dcm_Data17ByteType[17];

# define Rte_TypeDef_Dcm_Data1ByteType
typedef uint8 Dcm_Data1ByteType[1];

# define Rte_TypeDef_Dcm_Data2000ByteType
typedef uint8 Dcm_Data2000ByteType[2000];

# define Rte_TypeDef_Dcm_Data221ByteType
typedef uint8 Dcm_Data221ByteType[221];

# define Rte_TypeDef_Dcm_Data241ByteType
typedef uint8 Dcm_Data241ByteType[241];

# define Rte_TypeDef_Dcm_Data256ByteType
typedef uint8 Dcm_Data256ByteType[256];

# define Rte_TypeDef_Dcm_Data2ByteType
typedef uint8 Dcm_Data2ByteType[2];

# define Rte_TypeDef_Dcm_Data3ByteType
typedef uint8 Dcm_Data3ByteType[3];

# define Rte_TypeDef_Dcm_Data406ByteType
typedef uint8 Dcm_Data406ByteType[406];

# define Rte_TypeDef_Dcm_Data40ByteType
typedef uint8 Dcm_Data40ByteType[40];

# define Rte_TypeDef_Dcm_Data4ByteType
typedef uint8 Dcm_Data4ByteType[4];

# define Rte_TypeDef_Dcm_Data5ByteType
typedef uint8 Dcm_Data5ByteType[5];

# define Rte_TypeDef_Dcm_Data60ByteType
typedef uint8 Dcm_Data60ByteType[60];

# define Rte_TypeDef_Dcm_Data64ByteType
typedef uint8 Dcm_Data64ByteType[64];

# define Rte_TypeDef_Dcm_Data6ByteType
typedef uint8 Dcm_Data6ByteType[6];

# define Rte_TypeDef_Dcm_Data7ByteType
typedef uint8 Dcm_Data7ByteType[7];

# define Rte_TypeDef_Dcm_Data80ByteType
typedef uint8 Dcm_Data80ByteType[80];

# define Rte_TypeDef_Dcm_Data8ByteType
typedef uint8 Dcm_Data8ByteType[8];

# define Rte_TypeDef_Debug_PVT_SCIM_FlexArrayData
typedef uint8 Debug_PVT_SCIM_FlexArrayData[7];

# define Rte_TypeDef_Dem_MaxDataValueType
typedef uint8 Dem_MaxDataValueType[6];

# define Rte_TypeDef_Driver1Identification_T
typedef uint8 Driver1Identification_T[14];

# define Rte_TypeDef_DriversIdentifications_T
typedef uint8 DriversIdentifications_T[40];

# define Rte_TypeDef_DrivingMode_T
typedef uint8 DrivingMode_T[3];

# define Rte_TypeDef_Encrypted128bit_T
typedef uint8 Encrypted128bit_T[16];

# define Rte_TypeDef_FSPIndicationCmdArray_T
typedef DeviceIndication_T FSPIndicationCmdArray_T[8];

# define Rte_TypeDef_FSPSwitchStatusArray_T
typedef PushButtonStatus_T FSPSwitchStatusArray_T[8];

# define Rte_TypeDef_FSP_Array5
typedef uint8 FSP_Array5[5];

# define Rte_TypeDef_FSP_Array8
typedef uint8 FSP_Array8[8];

# define Rte_TypeDef_FlexibleSwDisableDiagPresence_Type
typedef uint8 FlexibleSwDisableDiagPresence_Type[5];

# define Rte_TypeDef_FspNVM_T
typedef uint8 FspNVM_T[28];

# define Rte_TypeDef_Issm_ActiveUserArrayType
typedef uint32 Issm_ActiveUserArrayType[2];

# define Rte_TypeDef_KeyFobNVM_T
typedef uint8 KeyFobNVM_T[96];

# define Rte_TypeDef_RandomGenerateResultBuffer
typedef uint8 RandomGenerateResultBuffer[128];

# define Rte_TypeDef_RandomSeedDataBuffer
typedef uint8 RandomSeedDataBuffer[128];

# define Rte_TypeDef_Rte_DT_AsymPrivateKeyType_1
typedef UInt8 Rte_DT_AsymPrivateKeyType_1[128];

# define Rte_TypeDef_Rte_DT_AsymPublicKeyType_1
typedef uint8 Rte_DT_AsymPublicKeyType_1[128];

# define Rte_TypeDef_Rte_DT_KeyExchangeBaseType_1
typedef uint8 Rte_DT_KeyExchangeBaseType_1[1];

# define Rte_TypeDef_Rte_DT_KeyExchangePrivateType_1
typedef uint8 Rte_DT_KeyExchangePrivateType_1[1];

# define Rte_TypeDef_Rte_DT_SymKeyType_1
typedef UInt8 Rte_DT_SymKeyType_1[256];

# define Rte_TypeDef_SEWS_ChassisId_CHANO_T
typedef uint8 SEWS_ChassisId_CHANO_T[16];

# define Rte_TypeDef_SEWS_ComCryptoKey_P1DLX_a_T
typedef SEWS_ComCryptoKey_P1DLX_T SEWS_ComCryptoKey_P1DLX_a_T[16];

# define Rte_TypeDef_SEWS_VIN_VINNO_T
typedef uint8 SEWS_VIN_VINNO_T[17];

# define Rte_TypeDef_SignatureVerifyDataBuffer
typedef uint8 SignatureVerifyDataBuffer[128];

# define Rte_TypeDef_SoftwareVersionSupported_T
typedef uint8 SoftwareVersionSupported_T[4];

# define Rte_TypeDef_SpeedControl_NVM_T
typedef uint8 SpeedControl_NVM_T[16];

# define Rte_TypeDef_StandardNVM_T
typedef uint8 StandardNVM_T[4];

# define Rte_TypeDef_SwitchDetectionResp_T
typedef uint8 SwitchDetectionResp_T[8];

# define Rte_TypeDef_SymDecryptDataBuffer
typedef UInt8 SymDecryptDataBuffer[128];

# define Rte_TypeDef_SymDecryptResultBuffer
typedef uint8 SymDecryptResultBuffer[128];

# define Rte_TypeDef_SymEncryptDataBuffer
typedef UInt8 SymEncryptDataBuffer[128];

# define Rte_TypeDef_SymEncryptResultBuffer
typedef UInt8 SymEncryptResultBuffer[128];

# define Rte_TypeDef_VEC_CryptoProxy_UserSignal
typedef UInt8 VEC_CryptoProxy_UserSignal[12];

# define Rte_TypeDef_VIN_stat_T
typedef uint8 VIN_stat_T[11];

# define Rte_TypeDef_VehicleIdentNumber_T
typedef uint8 VehicleIdentNumber_T[17];

# define Rte_TypeDef_rkedata
typedef uint8 rkedata[16];

# define Rte_TypeDef_AlmClkCurAlarm_stat_T
typedef struct
{
  AlarmClkID_T ID_RE;
  TimesetHr_T SetHr_RE;
  TimeMinuteType_T SetMin_RE;
  AlarmClkStat_T Stat_RE;
  AlarmClkType_T Type_RE;
} AlmClkCurAlarm_stat_T;

# define Rte_TypeDef_AlmClkSetCurAlm_rqst_T
typedef struct
{
  AlarmClkID_T ID_RE;
  TimesetHr_T SetHr_RE;
  TimeMinuteType_T SetMin_RE;
  AlarmClkType_T Type_RE;
  AlarmClkStat_T Stat_RE;
} AlmClkSetCurAlm_rqst_T;

# define Rte_TypeDef_ButtonStatus
typedef struct
{
  uint8 Button1ID;
  uint8 Button1PressCounter;
  uint16 Button1PressTime;
  uint8 Button2ID;
  uint8 Button2PressCounter;
  uint16 Button2PressTime;
} ButtonStatus;

# define Rte_TypeDef_DiagFaultStat_T
typedef struct
{
  EcuAdr_T EcuAdr_RE;
  DtcIdA_T DtcIdA_RE;
  FailTA_T FailTA_RE;
  DtcIdB_T DtcIdB_RE;
  FailTB_T FailTB_RE;
} DiagFaultStat_T;

# define Rte_TypeDef_EngTraceHWData_T
typedef struct
{
  Rte_DT_EngTraceHWData_T_0 Reset_Type;
  uint32 Timelog;
  Rte_DT_EngTraceHWData_T_2 TaskID;
  Rte_DT_EngTraceHWData_T_3 ServiceID;
  Rte_DT_EngTraceHWData_T_4 RunnableID;
  uint32 MemoryAddress;
  uint32 MC_RGM_FES_Reg;
  uint32 MC_RGM_DES_Reg;
  Rte_DT_EngTraceHWData_T_8 ModuleID;
  Rte_DT_EngTraceHWData_T_9 InstanceID;
  Rte_DT_EngTraceHWData_T_10 APIID;
  Rte_DT_EngTraceHWData_T_11 ErrorID;
} EngTraceHWData_T;

# define Rte_TypeDef_FMS1_T
typedef struct
{
  Blockid_T Blockid_RE;
  TellTaleStatus_T TellTaleStatus1_RE;
  TellTaleStatus_T TellTaleStatus2_RE;
  TellTaleStatus_T TellTaleStatus3_RE;
  TellTaleStatus_T TellTaleStatus4_RE;
  TellTaleStatus_T TellTaleStatus5_RE;
  TellTaleStatus_T TellTaleStatus6_RE;
  TellTaleStatus_T TellTaleStatus7_RE;
  TellTaleStatus_T TellTaleStatus8_RE;
  TellTaleStatus_T TellTaleStatus9_RE;
  TellTaleStatus_T TellTaleStatus10_RE;
  TellTaleStatus_T TellTaleStatus11_RE;
  TellTaleStatus_T TellTaleStatus12_RE;
  TellTaleStatus_T TellTaleStatus13_RE;
  TellTaleStatus_T TellTaleStatus14_RE;
  TellTaleStatus_T TellTaleStatus15_RE;
} FMS1_T;

# define Rte_TypeDef_FPBRMMIStat_T
typedef struct
{
  FPBRStatusInd_T FPBRStatusInd_RE;
  Ack2Bit_T FPBRChangeAck_RE;
} FPBRMMIStat_T;

# define Rte_TypeDef_FlexibleSwitchesinFailure_T
typedef struct
{
  uint8 FlexibleSwitchFailureType;
  uint8 FlexibleSwitchID;
  uint8 FlexibleSwitchPosition;
  uint8 FlexibleSwitchPanel;
  uint8 LINbus;
} FlexibleSwitchesinFailure_T;

# define Rte_TypeDef_InteriorLightMode_T
typedef struct
{
  IL_Mode_T IL_Mode_RE;
  EventFlag_T EventFlag_RE;
} InteriorLightMode_T;

# define Rte_TypeDef_LevelRequest_T
typedef struct
{
  LevelChangeRequest_T FrontAxle_RE;
  LevelChangeRequest_T RearAxle_RE;
  RollRequest_T RollRequest_RE;
} LevelRequest_T;

# define Rte_TypeDef_LfRssi
typedef struct
{
  uint16 AntennaPi;
  uint16 AntennaP1;
  uint16 AntennaP2;
  uint16 AntennaP3;
  uint16 AntennaP4;
} LfRssi;

# define Rte_TypeDef_MaintService_T
typedef struct
{
  ServiceDistance_T ServiceDistance_RE;
  MaintServiceID_T DistID_RE;
  ServiceTime_T ServiceCalendarTime_RE;
  ServiceTime_T ServiceEngineTime_RE;
  MaintServiceID_T CalDateID_RE;
  MaintServiceID_T EngTimeID_RE;
} MaintService_T;

# define Rte_TypeDef_OilPrediction_T
typedef struct
{
  OilStatus_T Status_RE;
  OilQuality_T Quality_RE;
  RemainDistOilChange_T RemainDist_RE;
  RemainEngineTimeOilChange_T RemainTime_RE;
} OilPrediction_T;

# define Rte_TypeDef_SEWS_AuxPassiveAntennasActivation_P1WIQ_s_T
typedef struct
{
  boolean IsActiveAntenna_Pi;
  boolean IsActiveAntenna_P1;
  boolean IsActiveAntenna_P2;
  boolean IsActiveAntenna_P3;
  boolean IsActiveAntenna_P4;
} SEWS_AuxPassiveAntennasActivation_P1WIQ_s_T;

# define Rte_TypeDef_SEWS_DAI_Installed_P1WMP_s_T
typedef struct
{
  boolean LeftDoor;
  boolean RightDoor;
} SEWS_DAI_Installed_P1WMP_s_T;

# define Rte_TypeDef_SEWS_PcbConfig_AdiPullUp_X1CX5_s_T
typedef struct
{
  boolean AdiPullupLiving;
  boolean AdiPullupParked;
} SEWS_PcbConfig_AdiPullUp_X1CX5_s_T;

# define Rte_TypeDef_SRS2_SNPN_T
typedef struct
{
  Int8Bit_T Byte0_RE;
  Int8Bit_T Byte1_RE;
  Int8Bit_T Byte2_RE;
  Int8Bit_T Byte3_RE;
  Int8Bit_T Byte4_RE;
} SRS2_SNPN_T;

# define Rte_TypeDef_SRS2_SN_T
typedef struct
{
  Int8Bit_T Byte0_RE;
  Int8Bit_T Byte1_RE;
  Int8Bit_T Byte2_RE;
  Int8Bit_T Byte3_RE;
} SRS2_SN_T;

# define Rte_TypeDef_SetParkHtrTmr_rqst_T
typedef struct
{
  ParkHeaterTimer_cmd_T Timer_cmd_RE;
  Hours8bit_T StartTimeHr_RE;
  Minutes8bit_T StartTimeMin_RE;
  Hours8bit_T DurnTimeHr_RE;
  Minutes8bit_T DurnTimeMin_RE;
} SetParkHtrTmr_rqst_T;

# define Rte_TypeDef_VINCheckStatus_T
typedef struct
{
  boolean isDISPLAY_CheckPassed;
  boolean isAPM_CheckPassed;
  boolean isVMCU_CheckPassed;
  boolean isEMS_CheckPassed;
  boolean isTECU_CheckPassed;
} VINCheckStatus_T;

# define Rte_TypeDef_AsymDecryptLengthBuffer
typedef uint32 AsymDecryptLengthBuffer;

# define Rte_TypeDef_ComM_InhibitionStatusType
typedef uint8 ComM_InhibitionStatusType;

# define Rte_TypeDef_ComM_UserHandleType
typedef uint8 ComM_UserHandleType;

# define Rte_TypeDef_Csm_ConfigIdType
typedef uint16 Csm_ConfigIdType;

# define Rte_TypeDef_Dem_DTCGroupType
typedef uint32 Dem_DTCGroupType;

# define Rte_TypeDef_Dem_DTCStatusMaskType
typedef uint8 Dem_DTCStatusMaskType;

# define Rte_TypeDef_Dem_EventIdType
typedef uint16 Dem_EventIdType;

# define Rte_TypeDef_Dem_OperationCycleIdType
typedef uint8 Dem_OperationCycleIdType;

# define Rte_TypeDef_Dem_RatioIdType
typedef uint16 Dem_RatioIdType;

# define Rte_TypeDef_EcuM_TimeType
typedef uint32 EcuM_TimeType;

# define Rte_TypeDef_EcuM_UserType
typedef uint8 EcuM_UserType;

# define Rte_TypeDef_IOHWAB_BOOL
typedef boolean IOHWAB_BOOL;

# define Rte_TypeDef_IOHWAB_SINT8
typedef sint8 IOHWAB_SINT8;

# define Rte_TypeDef_IOHWAB_UINT16
typedef uint16 IOHWAB_UINT16;

# define Rte_TypeDef_IOHWAB_UINT8
typedef uint8 IOHWAB_UINT8;

# define Rte_TypeDef_Issm_IssHandleType
typedef uint8 Issm_IssHandleType;

# define Rte_TypeDef_Issm_UserHandleType
typedef uint8 Issm_UserHandleType;

# define Rte_TypeDef_NvM_BlockIdType
typedef uint16 NvM_BlockIdType;

# define Rte_TypeDef_SEWS_ABS_Inhibit_SwType_P1SY6_T
typedef uint8 SEWS_ABS_Inhibit_SwType_P1SY6_T;

# define Rte_TypeDef_SEWS_AlarmAutoRelockRequestDuration_X1CYA_T
typedef uint8 SEWS_AlarmAutoRelockRequestDuration_X1CYA_T;

# define Rte_TypeDef_SEWS_AntMappingConfig_Gain_X1C03_T
typedef uint8 SEWS_AntMappingConfig_Gain_X1C03_T;

# define Rte_TypeDef_SEWS_AntMappingConfig_Multi_X1CY3a_T
typedef sint16 SEWS_AntMappingConfig_Multi_X1CY3a_T;

# define Rte_TypeDef_SEWS_AutoAlarmReactivationTimeout_P1B2S_T
typedef uint8 SEWS_AutoAlarmReactivationTimeout_P1B2S_T;

# define Rte_TypeDef_SEWS_AuxBBLoadStat_MaxInitTime_P1M93_T
typedef uint8 SEWS_AuxBBLoadStat_MaxInitTime_P1M93_T;

# define Rte_TypeDef_SEWS_AuxBBSw_TimeoutForReq_P1DV1_T
typedef uint8 SEWS_AuxBBSw_TimeoutForReq_P1DV1_T;

# define Rte_TypeDef_SEWS_AuxBbSw1_Logic_P1DI2_T
typedef uint8 SEWS_AuxBbSw1_Logic_P1DI2_T;

# define Rte_TypeDef_SEWS_AuxBbSw2_Logic_P1DI3_T
typedef uint8 SEWS_AuxBbSw2_Logic_P1DI3_T;

# define Rte_TypeDef_SEWS_AuxBbSw3_Logic_P1DI4_T
typedef uint8 SEWS_AuxBbSw3_Logic_P1DI4_T;

# define Rte_TypeDef_SEWS_AuxBbSw4_Logic_P1DI5_T
typedef uint8 SEWS_AuxBbSw4_Logic_P1DI5_T;

# define Rte_TypeDef_SEWS_AuxBbSw5_Logic_P1DI6_T
typedef uint8 SEWS_AuxBbSw5_Logic_P1DI6_T;

# define Rte_TypeDef_SEWS_AuxBbSw6_Logic_P1DI7_T
typedef uint8 SEWS_AuxBbSw6_Logic_P1DI7_T;

# define Rte_TypeDef_SEWS_AuxHornMaxActivationTime_X1CYZ_T
typedef uint16 SEWS_AuxHornMaxActivationTime_X1CYZ_T;

# define Rte_TypeDef_SEWS_AxleConfiguration_P1B16_T
typedef uint8 SEWS_AxleConfiguration_P1B16_T;

# define Rte_TypeDef_SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T
typedef uint8 SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T;

# define Rte_TypeDef_SEWS_BodybuilderAccessToAccelPedal_P1B72_T
typedef uint8 SEWS_BodybuilderAccessToAccelPedal_P1B72_T;

# define Rte_TypeDef_SEWS_CM_Configuration_P1LGD_T
typedef uint8 SEWS_CM_Configuration_P1LGD_T;

# define Rte_TypeDef_SEWS_CabHeightValue_P1R0P_T
typedef uint8 SEWS_CabHeightValue_P1R0P_T;

# define Rte_TypeDef_SEWS_CityHornMaxActivationTime_X1CY0_T
typedef uint16 SEWS_CityHornMaxActivationTime_X1CY0_T;

# define Rte_TypeDef_SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T
typedef uint8 SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T;

# define Rte_TypeDef_SEWS_ContainerUnlockHMIDeviceType_P1CXO_T
typedef uint8 SEWS_ContainerUnlockHMIDeviceType_P1CXO_T;

# define Rte_TypeDef_SEWS_CraneHMIDeviceType_P1CXA_T
typedef uint8 SEWS_CraneHMIDeviceType_P1CXA_T;

# define Rte_TypeDef_SEWS_CraneSwIndicationType_P1CXC_T
typedef uint8 SEWS_CraneSwIndicationType_P1CXC_T;

# define Rte_TypeDef_SEWS_CrankingLockActivation_P1DS3_T
typedef uint8 SEWS_CrankingLockActivation_P1DS3_T;

# define Rte_TypeDef_SEWS_DashboardLedTimeout_P1IZ4_T
typedef uint8 SEWS_DashboardLedTimeout_P1IZ4_T;

# define Rte_TypeDef_SEWS_Diag_Act_DOWHS01_P1V6O_T
typedef uint8 SEWS_Diag_Act_DOWHS01_P1V6O_T;

# define Rte_TypeDef_SEWS_Diag_Act_DOWHS02_P1V6P_T
typedef uint8 SEWS_Diag_Act_DOWHS02_P1V6P_T;

# define Rte_TypeDef_SEWS_Diag_Act_DOWLS02_P1V7E_T
typedef uint8 SEWS_Diag_Act_DOWLS02_P1V7E_T;

# define Rte_TypeDef_SEWS_Diag_Act_DOWLS03_P1V7F_T
typedef uint8 SEWS_Diag_Act_DOWLS03_P1V7F_T;

# define Rte_TypeDef_SEWS_DiffLockSinglePushSwitch_LogicSelection_P1NAK_T
typedef uint8 SEWS_DiffLockSinglePushSwitch_LogicSelection_P1NAK_T;

# define Rte_TypeDef_SEWS_DoorAutoLockingSpeed_P1B2Q_T
typedef uint8 SEWS_DoorAutoLockingSpeed_P1B2Q_T;

# define Rte_TypeDef_SEWS_DoorIndicationReqDuration_P1DWP_T
typedef uint8 SEWS_DoorIndicationReqDuration_P1DWP_T;

# define Rte_TypeDef_SEWS_DoorLatchProtectMaxOperation_P1DXA_T
typedef uint8 SEWS_DoorLatchProtectMaxOperation_P1DXA_T;

# define Rte_TypeDef_SEWS_DoorLatchProtectionRestingTime_P1DW9_T
typedef uint8 SEWS_DoorLatchProtectionRestingTime_P1DW9_T;

# define Rte_TypeDef_SEWS_DoorLatchProtectionTimeWindow_P1DW8_T
typedef uint8 SEWS_DoorLatchProtectionTimeWindow_P1DW8_T;

# define Rte_TypeDef_SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T
typedef uint16 SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T;

# define Rte_TypeDef_SEWS_DoorLockingFailureTimeout_X1CX9_T
typedef uint8 SEWS_DoorLockingFailureTimeout_X1CX9_T;

# define Rte_TypeDef_SEWS_DriverPosition_LHD_RHD_P1ALJ_T
typedef uint8 SEWS_DriverPosition_LHD_RHD_P1ALJ_T;

# define Rte_TypeDef_SEWS_DwmVehicleModes_P1BDU_T
typedef uint8 SEWS_DwmVehicleModes_P1BDU_T;

# define Rte_TypeDef_SEWS_ECSActiveStateTimeout_P1CUE_T
typedef uint16 SEWS_ECSActiveStateTimeout_P1CUE_T;

# define Rte_TypeDef_SEWS_ECSStandbyActivationTimeout_P1CUA_T
typedef uint16 SEWS_ECSStandbyActivationTimeout_P1CUA_T;

# define Rte_TypeDef_SEWS_ECSStandbyExtendedActTimeout_P1CUB_T
typedef uint16 SEWS_ECSStandbyExtendedActTimeout_P1CUB_T;

# define Rte_TypeDef_SEWS_ECSStopButtonHoldTimeout_P1DWI_T
typedef uint8 SEWS_ECSStopButtonHoldTimeout_P1DWI_T;

# define Rte_TypeDef_SEWS_ECS_StandbyBlinkTime_P1GCL_T
typedef uint8 SEWS_ECS_StandbyBlinkTime_P1GCL_T;

# define Rte_TypeDef_SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T
typedef uint8 SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T;

# define Rte_TypeDef_SEWS_FCW_ConfirmTimeout_P1LGF_T
typedef uint8 SEWS_FCW_ConfirmTimeout_P1LGF_T;

# define Rte_TypeDef_SEWS_FCW_LedLogic_P1LG1_T
typedef uint8 SEWS_FCW_LedLogic_P1LG1_T;

# define Rte_TypeDef_SEWS_FCW_SwPushThreshold_P1LGE_T
typedef uint8 SEWS_FCW_SwPushThreshold_P1LGE_T;

# define Rte_TypeDef_SEWS_FCW_SwStuckTimeout_P1LGG_T
typedef uint8 SEWS_FCW_SwStuckTimeout_P1LGG_T;

# define Rte_TypeDef_SEWS_FPBRSwitchRequestACKTime_P1LXR_T
typedef uint8 SEWS_FPBRSwitchRequestACKTime_P1LXR_T;

# define Rte_TypeDef_SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T
typedef uint8 SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T;

# define Rte_TypeDef_SEWS_FSC_TimeoutThreshold_X1CZR_T
typedef uint8 SEWS_FSC_TimeoutThreshold_X1CZR_T;

# define Rte_TypeDef_SEWS_FSPConfigSettingsLIN1_P1EWZ_T
typedef uint8 SEWS_FSPConfigSettingsLIN1_P1EWZ_T;

# define Rte_TypeDef_SEWS_FSPConfigSettingsLIN2_P1EW0_T
typedef uint8 SEWS_FSPConfigSettingsLIN2_P1EW0_T;

# define Rte_TypeDef_SEWS_FSPConfigSettingsLIN3_P1EW1_T
typedef uint8 SEWS_FSPConfigSettingsLIN3_P1EW1_T;

# define Rte_TypeDef_SEWS_FSPConfigSettingsLIN4_P1EW2_T
typedef uint8 SEWS_FSPConfigSettingsLIN4_P1EW2_T;

# define Rte_TypeDef_SEWS_FSPConfigSettingsLIN5_P1EW3_T
typedef uint8 SEWS_FSPConfigSettingsLIN5_P1EW3_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID001_ID009_P1EAA_T
typedef uint16 SEWS_FS_DiagAct_ID001_ID009_P1EAA_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID010_ID019_P1EAB_T
typedef uint16 SEWS_FS_DiagAct_ID010_ID019_P1EAB_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID02_ID029_P1EAC_T
typedef uint16 SEWS_FS_DiagAct_ID02_ID029_P1EAC_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID030_ID039_P1EAD_T
typedef uint16 SEWS_FS_DiagAct_ID030_ID039_P1EAD_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID040_ID049_P1EAE_T
typedef uint16 SEWS_FS_DiagAct_ID040_ID049_P1EAE_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID050_ID059_P1EAF_T
typedef uint16 SEWS_FS_DiagAct_ID050_ID059_P1EAF_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID060_ID069_P1EAG_T
typedef uint16 SEWS_FS_DiagAct_ID060_ID069_P1EAG_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID070_ID079_P1EAH_T
typedef uint16 SEWS_FS_DiagAct_ID070_ID079_P1EAH_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID080_ID089_P1EAI_T
typedef uint16 SEWS_FS_DiagAct_ID080_ID089_P1EAI_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T
typedef uint16 SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID100_ID109_P1EAK_T
typedef uint16 SEWS_FS_DiagAct_ID100_ID109_P1EAK_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID110_ID119_P1EAL_T
typedef uint16 SEWS_FS_DiagAct_ID110_ID119_P1EAL_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID120_ID129_P1EAM_T
typedef uint16 SEWS_FS_DiagAct_ID120_ID129_P1EAM_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID130_ID139_P1EAN_T
typedef uint16 SEWS_FS_DiagAct_ID130_ID139_P1EAN_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID140_ID149_P1EAO_T
typedef uint16 SEWS_FS_DiagAct_ID140_ID149_P1EAO_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID150_ID159_P1EAP_T
typedef uint16 SEWS_FS_DiagAct_ID150_ID159_P1EAP_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T
typedef uint16 SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID170_ID179_P1EAR_T
typedef uint16 SEWS_FS_DiagAct_ID170_ID179_P1EAR_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID180_ID189_P1EAS_T
typedef uint16 SEWS_FS_DiagAct_ID180_ID189_P1EAS_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID190_ID199_P1EAT_T
typedef uint16 SEWS_FS_DiagAct_ID190_ID199_P1EAT_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID200_ID209_P1EAU_T
typedef uint16 SEWS_FS_DiagAct_ID200_ID209_P1EAU_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID210_ID219_P1EAV_T
typedef uint16 SEWS_FS_DiagAct_ID210_ID219_P1EAV_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID220_ID229_P1EAW_T
typedef uint16 SEWS_FS_DiagAct_ID220_ID229_P1EAW_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID230_ID239_P1EAX_T
typedef uint16 SEWS_FS_DiagAct_ID230_ID239_P1EAX_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID240_ID249_P1EAY_T
typedef uint16 SEWS_FS_DiagAct_ID240_ID249_P1EAY_T;

# define Rte_TypeDef_SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T
typedef uint16 SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T;

# define Rte_TypeDef_SEWS_FerryFuncSwStuckedTimeout_P1EXK_T
typedef uint8 SEWS_FerryFuncSwStuckedTimeout_P1EXK_T;

# define Rte_TypeDef_SEWS_FrontAxleArrangement_P1CSH_T
typedef uint8 SEWS_FrontAxleArrangement_P1CSH_T;

# define Rte_TypeDef_SEWS_FrontSuspensionType_P1JBR_T
typedef uint8 SEWS_FrontSuspensionType_P1JBR_T;

# define Rte_TypeDef_SEWS_FuelTypeInformation_P1M7T_T
typedef uint8 SEWS_FuelTypeInformation_P1M7T_T;

# define Rte_TypeDef_SEWS_HeadwaySupport_P1BEX_T
typedef uint8 SEWS_HeadwaySupport_P1BEX_T;

# define Rte_TypeDef_SEWS_HwToleranceThreshold_X1C04_T
typedef uint8 SEWS_HwToleranceThreshold_X1C04_T;

# define Rte_TypeDef_SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T
typedef uint8 SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T;

# define Rte_TypeDef_SEWS_IL_CtrlDeviceTypeFront_P1DKH_T
typedef uint8 SEWS_IL_CtrlDeviceTypeFront_P1DKH_T;

# define Rte_TypeDef_SEWS_IL_LockingCmdDelayOff_P1K7E_T
typedef uint8 SEWS_IL_LockingCmdDelayOff_P1K7E_T;

# define Rte_TypeDef_SEWS_KeyMatchingIndicationTimeout_X1CV3_T
typedef uint8 SEWS_KeyMatchingIndicationTimeout_X1CV3_T;

# define Rte_TypeDef_SEWS_KeyfobDetectionMappingConfig_P1WIR_T
typedef uint8 SEWS_KeyfobDetectionMappingConfig_P1WIR_T;

# define Rte_TypeDef_SEWS_KeyfobDetectionMappingSelection_P1WIP_T
typedef uint8 SEWS_KeyfobDetectionMappingSelection_P1WIP_T;

# define Rte_TypeDef_SEWS_KeyfobEncryptCode_P1DS4_T
typedef uint8 SEWS_KeyfobEncryptCode_P1DS4_T;

# define Rte_TypeDef_SEWS_KeyfobType_P1VKL_T
typedef uint8 SEWS_KeyfobType_P1VKL_T;

# define Rte_TypeDef_SEWS_KneelButtonStuckedTimeout_P1DWD_T
typedef uint8 SEWS_KneelButtonStuckedTimeout_P1DWD_T;

# define Rte_TypeDef_SEWS_LIN_topology_P1AJR_T
typedef uint8 SEWS_LIN_topology_P1AJR_T;

# define Rte_TypeDef_SEWS_LKS_SwType_P1R5P_T
typedef uint8 SEWS_LKS_SwType_P1R5P_T;

# define Rte_TypeDef_SEWS_LNGTank1Volume_P1LX9_T
typedef uint16 SEWS_LNGTank1Volume_P1LX9_T;

# define Rte_TypeDef_SEWS_LNGTank2Volume_P1LYA_T
typedef uint16 SEWS_LNGTank2Volume_P1LYA_T;

# define Rte_TypeDef_SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T
typedef uint8 SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T;

# define Rte_TypeDef_SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T
typedef uint8 SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T;

# define Rte_TypeDef_SEWS_LockFunctionHardwareInterface_P1MXZ_T
typedef uint8 SEWS_LockFunctionHardwareInterface_P1MXZ_T;

# define Rte_TypeDef_SEWS_P1BWF_Level_Memorization_Min_Press_T
typedef uint8 SEWS_P1BWF_Level_Memorization_Min_Press_T;

# define Rte_TypeDef_SEWS_P1BWF_Memory_Recall_Max_Press_T
typedef uint8 SEWS_P1BWF_Memory_Recall_Max_Press_T;

# define Rte_TypeDef_SEWS_P1BWF_Memory_Recall_Min_Press_T
typedef uint8 SEWS_P1BWF_Memory_Recall_Min_Press_T;

# define Rte_TypeDef_SEWS_P1DKF_Long_press_threshold_T
typedef uint8 SEWS_P1DKF_Long_press_threshold_T;

# define Rte_TypeDef_SEWS_P1DKF_Shut_off_threshold_T
typedef uint8 SEWS_P1DKF_Shut_off_threshold_T;

# define Rte_TypeDef_SEWS_P1JSY_DriveStroke_T
typedef uint8 SEWS_P1JSY_DriveStroke_T;

# define Rte_TypeDef_SEWS_P1JSY_RampStroke_T
typedef uint8 SEWS_P1JSY_RampStroke_T;

# define Rte_TypeDef_SEWS_P1JSZ_DriveStroke_T
typedef uint8 SEWS_P1JSZ_DriveStroke_T;

# define Rte_TypeDef_SEWS_P1JSZ_RampStroke_T
typedef uint8 SEWS_P1JSZ_RampStroke_T;

# define Rte_TypeDef_SEWS_P1QR6_Threshold_VAT_T
typedef uint8 SEWS_P1QR6_Threshold_VAT_T;

# define Rte_TypeDef_SEWS_P1QR6_Threshold_VBT_T
typedef uint8 SEWS_P1QR6_Threshold_VBT_T;

# define Rte_TypeDef_SEWS_P1QR6_Threshold_VOR_T
typedef uint8 SEWS_P1QR6_Threshold_VOR_T;

# define Rte_TypeDef_SEWS_P1V60_ContactClosed_T
typedef uint8 SEWS_P1V60_ContactClosed_T;

# define Rte_TypeDef_SEWS_P1V60_ContactOpen_T
typedef uint8 SEWS_P1V60_ContactOpen_T;

# define Rte_TypeDef_SEWS_P1V61_ContactClosed_T
typedef uint8 SEWS_P1V61_ContactClosed_T;

# define Rte_TypeDef_SEWS_P1V61_ContactOpen_T
typedef uint8 SEWS_P1V61_ContactOpen_T;

# define Rte_TypeDef_SEWS_P1V62_ContactClosed_T
typedef uint8 SEWS_P1V62_ContactClosed_T;

# define Rte_TypeDef_SEWS_P1V62_ContactOpen_T
typedef uint8 SEWS_P1V62_ContactOpen_T;

# define Rte_TypeDef_SEWS_P1V63_ContactClosed_T
typedef uint8 SEWS_P1V63_ContactClosed_T;

# define Rte_TypeDef_SEWS_P1V63_ContactOpen_T
typedef uint8 SEWS_P1V63_ContactOpen_T;

# define Rte_TypeDef_SEWS_P1V64_ContactClosed_T
typedef uint8 SEWS_P1V64_ContactClosed_T;

# define Rte_TypeDef_SEWS_P1V64_ContactOpen_T
typedef uint8 SEWS_P1V64_ContactOpen_T;

# define Rte_TypeDef_SEWS_P1V65_ContactClosed_T
typedef uint8 SEWS_P1V65_ContactClosed_T;

# define Rte_TypeDef_SEWS_P1V65_ContactOpen_T
typedef uint8 SEWS_P1V65_ContactOpen_T;

# define Rte_TypeDef_SEWS_P1V66_ContactClosed_T
typedef uint8 SEWS_P1V66_ContactClosed_T;

# define Rte_TypeDef_SEWS_P1V66_ContactOpen_T
typedef uint8 SEWS_P1V66_ContactOpen_T;

# define Rte_TypeDef_SEWS_P1V67_ContactClosed_T
typedef uint8 SEWS_P1V67_ContactClosed_T;

# define Rte_TypeDef_SEWS_P1V67_ContactOpen_T
typedef uint8 SEWS_P1V67_ContactOpen_T;

# define Rte_TypeDef_SEWS_P1V68_ContactClosed_T
typedef uint8 SEWS_P1V68_ContactClosed_T;

# define Rte_TypeDef_SEWS_P1V68_ContactOpen_T
typedef uint8 SEWS_P1V68_ContactOpen_T;

# define Rte_TypeDef_SEWS_P1V69_ContactClosed_T
typedef uint8 SEWS_P1V69_ContactClosed_T;

# define Rte_TypeDef_SEWS_P1V69_ContactOpen_T
typedef uint8 SEWS_P1V69_ContactOpen_T;

# define Rte_TypeDef_SEWS_P1V6U_Threshold_OC_STB_T
typedef uint8 SEWS_P1V6U_Threshold_OC_STB_T;

# define Rte_TypeDef_SEWS_P1V6U_Threshold_STG_T
typedef uint8 SEWS_P1V6U_Threshold_STG_T;

# define Rte_TypeDef_SEWS_P1V6V_Threshold_OC_STB_T
typedef uint8 SEWS_P1V6V_Threshold_OC_STB_T;

# define Rte_TypeDef_SEWS_P1V6V_Threshold_STG_T
typedef uint8 SEWS_P1V6V_Threshold_STG_T;

# define Rte_TypeDef_SEWS_P1V6W_ContactClosed_T
typedef uint8 SEWS_P1V6W_ContactClosed_T;

# define Rte_TypeDef_SEWS_P1V6W_ContactOpen_T
typedef uint8 SEWS_P1V6W_ContactOpen_T;

# define Rte_TypeDef_SEWS_P1V6X_ContactClosed_T
typedef uint8 SEWS_P1V6X_ContactClosed_T;

# define Rte_TypeDef_SEWS_P1V6X_ContactOpen_T
typedef uint8 SEWS_P1V6X_ContactOpen_T;

# define Rte_TypeDef_SEWS_P1V6Y_ContactClosed_T
typedef uint8 SEWS_P1V6Y_ContactClosed_T;

# define Rte_TypeDef_SEWS_P1V6Y_ContactOpen_T
typedef uint8 SEWS_P1V6Y_ContactOpen_T;

# define Rte_TypeDef_SEWS_P1V6Z_ContactClosed_T
typedef uint8 SEWS_P1V6Z_ContactClosed_T;

# define Rte_TypeDef_SEWS_P1V6Z_ContactOpen_T
typedef uint8 SEWS_P1V6Z_ContactOpen_T;

# define Rte_TypeDef_SEWS_P1V79_P1Interface_T
typedef uint8 SEWS_P1V79_P1Interface_T;

# define Rte_TypeDef_SEWS_P1V79_P2Interface_T
typedef uint8 SEWS_P1V79_P2Interface_T;

# define Rte_TypeDef_SEWS_P1V79_P3Interface_T
typedef uint8 SEWS_P1V79_P3Interface_T;

# define Rte_TypeDef_SEWS_P1V79_P4Interface_T
typedef uint8 SEWS_P1V79_P4Interface_T;

# define Rte_TypeDef_SEWS_P1V8F_Threshold_VAT_T
typedef uint8 SEWS_P1V8F_Threshold_VAT_T;

# define Rte_TypeDef_SEWS_P1V8F_Threshold_VBT_T
typedef uint8 SEWS_P1V8F_Threshold_VBT_T;

# define Rte_TypeDef_SEWS_P1VKG_DISPLAY_Check_Active_T
typedef uint8 SEWS_P1VKG_DISPLAY_Check_Active_T;

# define Rte_TypeDef_SEWS_P1VKG_EMS_Check_Active_T
typedef uint8 SEWS_P1VKG_EMS_Check_Active_T;

# define Rte_TypeDef_SEWS_P1VKG_MVUC_Check_Active_T
typedef uint8 SEWS_P1VKG_MVUC_Check_Active_T;

# define Rte_TypeDef_SEWS_P1VKG_TECU_Check_Active_T
typedef uint8 SEWS_P1VKG_TECU_Check_Active_T;

# define Rte_TypeDef_SEWS_P1WMD_ThresholdHigh_T
typedef uint8 SEWS_P1WMD_ThresholdHigh_T;

# define Rte_TypeDef_SEWS_P1WMD_ThresholdLow_T
typedef uint8 SEWS_P1WMD_ThresholdLow_T;

# define Rte_TypeDef_SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T
typedef uint8 SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T;

# define Rte_TypeDef_SEWS_PTO_EmergencyFilteringTimer_P1BD3_T
typedef uint8 SEWS_PTO_EmergencyFilteringTimer_P1BD3_T;

# define Rte_TypeDef_SEWS_PTO_RequestFilteringTimer_P1BD4_T
typedef uint8 SEWS_PTO_RequestFilteringTimer_P1BD4_T;

# define Rte_TypeDef_SEWS_PassengersSeatBeltInstalled_P1VQB_T
typedef uint8 SEWS_PassengersSeatBeltInstalled_P1VQB_T;

# define Rte_TypeDef_SEWS_PassengersSeatBeltSensorType_P1VYK_T
typedef uint8 SEWS_PassengersSeatBeltSensorType_P1VYK_T;

# define Rte_TypeDef_SEWS_PassiveEntryFunction_Type_P1VKF_T
typedef uint8 SEWS_PassiveEntryFunction_Type_P1VKF_T;

# define Rte_TypeDef_SEWS_PcbConfig_DoorAccessIf_X1CX3_T
typedef uint8 SEWS_PcbConfig_DoorAccessIf_X1CX3_T;

# define Rte_TypeDef_SEWS_Pvt_ActivateReporting_X1C14_T
typedef uint8 SEWS_Pvt_ActivateReporting_X1C14_T;

# define Rte_TypeDef_SEWS_RAS_LEDFeedbackIndication_P1GCC_T
typedef uint8 SEWS_RAS_LEDFeedbackIndication_P1GCC_T;

# define Rte_TypeDef_SEWS_RCECSButtonStucked_P1DWJ_T
typedef uint8 SEWS_RCECSButtonStucked_P1DWJ_T;

# define Rte_TypeDef_SEWS_RCECSUpDownStucked_P1DWK_T
typedef uint8 SEWS_RCECSUpDownStucked_P1DWK_T;

# define Rte_TypeDef_SEWS_RCECS_HoldCircuitTimer_P1IUS_T
typedef uint8 SEWS_RCECS_HoldCircuitTimer_P1IUS_T;

# define Rte_TypeDef_SEWS_RearAxleDifflockDisregardRequestVehicleSpeed_P1WEB_T
typedef uint8 SEWS_RearAxleDifflockDisregardRequestVehicleSpeed_P1WEB_T;

# define Rte_TypeDef_SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T
typedef uint8 SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T;

# define Rte_TypeDef_SEWS_Slid5thWheelTimeoutForReq_P1DV9_T
typedef uint8 SEWS_Slid5thWheelTimeoutForReq_P1DV9_T;

# define Rte_TypeDef_SEWS_Slidable5thWheelHMIDeviceType_P1CXP_T
typedef uint8 SEWS_Slidable5thWheelHMIDeviceType_P1CXP_T;

# define Rte_TypeDef_SEWS_SpeedRelockingReinitThreshold_P1H55_T
typedef uint8 SEWS_SpeedRelockingReinitThreshold_P1H55_T;

# define Rte_TypeDef_SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T
typedef uint8 SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T;

# define Rte_TypeDef_SEWS_TailLiftHMIDeviceType_P1CW9_T
typedef uint8 SEWS_TailLiftHMIDeviceType_P1CW9_T;

# define Rte_TypeDef_SEWS_TailLiftTimeoutForRequest_P1DWA_T
typedef uint8 SEWS_TailLiftTimeoutForRequest_P1DWA_T;

# define Rte_TypeDef_SEWS_TailLift_Crane_Act_P1CXB_T
typedef uint8 SEWS_TailLift_Crane_Act_P1CXB_T;

# define Rte_TypeDef_SEWS_TheftAlarmRequestDuration_X1CY8_T
typedef uint8 SEWS_TheftAlarmRequestDuration_X1CY8_T;

# define Rte_TypeDef_SEWS_WeightClassInformation_P1M7S_T
typedef uint8 SEWS_WeightClassInformation_P1M7S_T;

# define Rte_TypeDef_SEWS_WheelDifferentialLockPushButtonType_P1UG1_T
typedef uint8 SEWS_WheelDifferentialLockPushButtonType_P1UG1_T;

# define Rte_TypeDef_SEWS_X1CX4_P1Interface_T
typedef uint8 SEWS_X1CX4_P1Interface_T;

# define Rte_TypeDef_SEWS_X1CX4_P2Interface_T
typedef uint8 SEWS_X1CX4_P2Interface_T;

# define Rte_TypeDef_SEWS_X1CX4_P3Interface_T
typedef uint8 SEWS_X1CX4_P3Interface_T;

# define Rte_TypeDef_SEWS_X1CX4_P4Interface_T
typedef uint8 SEWS_X1CX4_P4Interface_T;

# define Rte_TypeDef_SEWS_X1CY1_DigitalBiLevelHigh_T
typedef uint8 SEWS_X1CY1_DigitalBiLevelHigh_T;

# define Rte_TypeDef_SEWS_X1CY1_DigitalBiLevelLow_T
typedef uint8 SEWS_X1CY1_DigitalBiLevelLow_T;

# define Rte_TypeDef_SEWS_X1CY2_FastenedHigh_STB_T
typedef uint8 SEWS_X1CY2_FastenedHigh_STB_T;

# define Rte_TypeDef_SEWS_X1CY2_FastenedLow_T
typedef uint8 SEWS_X1CY2_FastenedLow_T;

# define Rte_TypeDef_SEWS_X1CY2_UnfastnedAndNotSeatHigh_T
typedef uint8 SEWS_X1CY2_UnfastnedAndNotSeatHigh_T;

# define Rte_TypeDef_SEWS_X1CY2_UnfastnedAndNotSeatLow_T
typedef uint8 SEWS_X1CY2_UnfastnedAndNotSeatLow_T;

# define Rte_TypeDef_SEWS_X1CY2_UnfastnedAndSeatHigh_T
typedef uint8 SEWS_X1CY2_UnfastnedAndSeatHigh_T;

# define Rte_TypeDef_SEWS_X1CY2_UnfastnedAndSeatLow_T
typedef uint8 SEWS_X1CY2_UnfastnedAndSeatLow_T;

# define Rte_TypeDef_SEWS_X1CY5_AntennaP1_LimitRSSI_T
typedef sint16 SEWS_X1CY5_AntennaP1_LimitRSSI_T;

# define Rte_TypeDef_SEWS_X1CY5_AntennaP2_LimitRSSI_T
typedef sint16 SEWS_X1CY5_AntennaP2_LimitRSSI_T;

# define Rte_TypeDef_SEWS_X1CY5_AntennaP3_LimitRSSI_T
typedef sint16 SEWS_X1CY5_AntennaP3_LimitRSSI_T;

# define Rte_TypeDef_SEWS_X1CY5_AntennaP4_LimitRSSI_T
typedef sint16 SEWS_X1CY5_AntennaP4_LimitRSSI_T;

# define Rte_TypeDef_SEWS_X1CY5_AntennaPi_LimitRSSI_T
typedef sint16 SEWS_X1CY5_AntennaPi_LimitRSSI_T;

# define Rte_TypeDef_SEWS_X1CZQ_Operating_T
typedef uint8 SEWS_X1CZQ_Operating_T;

# define Rte_TypeDef_SEWS_X1CZQ_Protecting_T
typedef uint8 SEWS_X1CZQ_Protecting_T;

# define Rte_TypeDef_SEWS_X1CZQ_Reduced_T
typedef uint8 SEWS_X1CZQ_Reduced_T;

# define Rte_TypeDef_SEWS_X1CZQ_ShutdownReady_T
typedef uint8 SEWS_X1CZQ_ShutdownReady_T;

# define Rte_TypeDef_SEWS_X1CZQ_Withstand_T
typedef uint8 SEWS_X1CZQ_Withstand_T;

# define Rte_TypeDef_SymDecryptLengthBuffer
typedef uint32 SymDecryptLengthBuffer;

# define Rte_TypeDef_SymEncryptLengthBuffer
typedef uint32 SymEncryptLengthBuffer;

# define Rte_TypeDef_TimeInMicrosecondsType
typedef uint32 TimeInMicrosecondsType;

# define Rte_TypeDef_VGTT_EcuPinVoltage_0V2
typedef IOHWAB_UINT8 VGTT_EcuPinVoltage_0V2;

# define Rte_TypeDef_VGTT_EcuPwmDutycycle
typedef IOHWAB_SINT8 VGTT_EcuPwmDutycycle;

# define Rte_TypeDef_VGTT_EcuPwmPeriod
typedef IOHWAB_UINT16 VGTT_EcuPwmPeriod;

# define Rte_TypeDef_BswM_BswMRteMDG_CanBusOff
typedef uint8 BswM_BswMRteMDG_CanBusOff;

# define Rte_TypeDef_BswM_BswMRteMDG_LIN1Schedule
typedef uint8 BswM_BswMRteMDG_LIN1Schedule;

# define Rte_TypeDef_BswM_BswMRteMDG_LIN2Schedule
typedef uint8 BswM_BswMRteMDG_LIN2Schedule;

# define Rte_TypeDef_BswM_BswMRteMDG_LIN3Schedule
typedef uint8 BswM_BswMRteMDG_LIN3Schedule;

# define Rte_TypeDef_BswM_BswMRteMDG_LIN4Schedule
typedef uint8 BswM_BswMRteMDG_LIN4Schedule;

# define Rte_TypeDef_BswM_BswMRteMDG_LIN5Schedule
typedef uint8 BswM_BswMRteMDG_LIN5Schedule;

# define Rte_TypeDef_BswM_BswMRteMDG_LIN6Schedule
typedef uint8 BswM_BswMRteMDG_LIN6Schedule;

# define Rte_TypeDef_BswM_BswMRteMDG_LIN7Schedule
typedef uint8 BswM_BswMRteMDG_LIN7Schedule;

# define Rte_TypeDef_BswM_BswMRteMDG_LIN8Schedule
typedef uint8 BswM_BswMRteMDG_LIN8Schedule;

# define Rte_TypeDef_BswM_BswMRteMDG_LINSchTableState
typedef uint8 BswM_BswMRteMDG_LINSchTableState;

# define Rte_TypeDef_BswM_BswMRteMDG_NvmWriteAllRequest
typedef uint8 BswM_BswMRteMDG_NvmWriteAllRequest;

# define Rte_TypeDef_BswM_BswMRteMDG_PvtReport_X1C14
typedef uint8 BswM_BswMRteMDG_PvtReport_X1C14;

# define Rte_TypeDef_BswM_BswMRteMDG_ResetProcess
typedef uint8 BswM_BswMRteMDG_ResetProcess;

# define Rte_TypeDef_BswM_ESH_Mode
typedef uint8 BswM_ESH_Mode;

# define Rte_TypeDef_ComM_ModeType
typedef uint8 ComM_ModeType;

# define Rte_TypeDef_Csm_VerifyResultType
typedef uint8 Csm_VerifyResultType;

# define Rte_TypeDef_Dcm_CommunicationModeType
typedef uint8 Dcm_CommunicationModeType;

# define Rte_TypeDef_Dcm_ConfirmationStatusType
typedef uint8 Dcm_ConfirmationStatusType;

# define Rte_TypeDef_Dcm_ControlDtcSettingType
typedef uint8 Dcm_ControlDtcSettingType;

# define Rte_TypeDef_Dcm_DiagnosticSessionControlType
typedef uint8 Dcm_DiagnosticSessionControlType;

# define Rte_TypeDef_Dcm_EcuResetType
typedef uint8 Dcm_EcuResetType;

# define Rte_TypeDef_Dcm_ProtocolType
typedef uint8 Dcm_ProtocolType;

# define Rte_TypeDef_Dcm_RequestKindType
typedef uint8 Dcm_RequestKindType;

# define Rte_TypeDef_Dcm_SecLevelType
typedef uint8 Dcm_SecLevelType;

# define Rte_TypeDef_Dcm_SesCtrlType
typedef uint8 Dcm_SesCtrlType;

# define Rte_TypeDef_Dem_DTCFormatType
typedef uint8 Dem_DTCFormatType;

# define Rte_TypeDef_Dem_DTCKindType
typedef uint8 Dem_DTCKindType;

# define Rte_TypeDef_Dem_DTCOriginType
typedef uint16 Dem_DTCOriginType;

# define Rte_TypeDef_Dem_DTCSeverityType
typedef uint8 Dem_DTCSeverityType;

# define Rte_TypeDef_Dem_DTRControlType
typedef uint8 Dem_DTRControlType;

# define Rte_TypeDef_Dem_DebounceResetStatusType
typedef uint8 Dem_DebounceResetStatusType;

# define Rte_TypeDef_Dem_DebouncingStateType
typedef uint8 Dem_DebouncingStateType;

# define Rte_TypeDef_Dem_EventStatusType
typedef uint8 Dem_EventStatusType;

# define Rte_TypeDef_Dem_IndicatorStatusType
typedef uint8 Dem_IndicatorStatusType;

# define Rte_TypeDef_Dem_InitMonitorReasonType
typedef uint8 Dem_InitMonitorReasonType;

# define Rte_TypeDef_Dem_IumprDenomCondIdType
typedef uint8 Dem_IumprDenomCondIdType;

# define Rte_TypeDef_Dem_IumprDenomCondStatusType
typedef uint8 Dem_IumprDenomCondStatusType;

# define Rte_TypeDef_Dem_IumprReadinessGroupType
typedef uint8 Dem_IumprReadinessGroupType;

# define Rte_TypeDef_Dem_MonitorStatusType
typedef uint8 Dem_MonitorStatusType;

# define Rte_TypeDef_Dem_OperationCycleStateType
typedef uint8 Dem_OperationCycleStateType;

# define Rte_TypeDef_Dem_UdsStatusByteType
typedef uint8 Dem_UdsStatusByteType;

# define Rte_TypeDef_EcuM_BootTargetType
typedef uint8 EcuM_BootTargetType;

# define Rte_TypeDef_EcuM_ModeType
typedef uint8 EcuM_ModeType;

# define Rte_TypeDef_EcuM_ShutdownCauseType
typedef uint8 EcuM_ShutdownCauseType;

# define Rte_TypeDef_EcuM_StateType
typedef uint8 EcuM_StateType;

# define Rte_TypeDef_Issm_IssStateType
typedef uint8 Issm_IssStateType;

# define Rte_TypeDef_J1939Rm_AckCode
typedef uint8 J1939Rm_AckCode;

# define Rte_TypeDef_J1939Rm_ExtIdType
typedef uint8 J1939Rm_ExtIdType;

# define Rte_TypeDef_Living12VPowerStability
typedef uint8 Living12VPowerStability;

# define Rte_TypeDef_NvM_RequestResultType
typedef uint8 NvM_RequestResultType;

# define Rte_TypeDef_PcbPopulatedInfo_T
typedef uint8 PcbPopulatedInfo_T;

# define Rte_TypeDef_Rte_DT_EcuHwDioCtrlArray_T_0
typedef IOHWAB_UINT8 Rte_DT_EcuHwDioCtrlArray_T_0;

# define Rte_TypeDef_Rte_DT_InteriorLightMode_rqst_T_1
typedef EventFlag_T Rte_DT_InteriorLightMode_rqst_T_1;

# define Rte_TypeDef_SEWS_P1V79_PiInterface_T
typedef uint8 SEWS_P1V79_PiInterface_T;

# define Rte_TypeDef_SEWS_P1VKG_APM_Check_Active_T
typedef uint8 SEWS_P1VKG_APM_Check_Active_T;

# define Rte_TypeDef_SEWS_PcbConfig_Adi_X1CXW_T
typedef uint8 SEWS_PcbConfig_Adi_X1CXW_T;

# define Rte_TypeDef_SEWS_PcbConfig_CanInterfaces_X1CX2_T
typedef uint8 SEWS_PcbConfig_CanInterfaces_X1CX2_T;

# define Rte_TypeDef_SEWS_PcbConfig_DOBHS_X1CXX_T
typedef uint8 SEWS_PcbConfig_DOBHS_X1CXX_T;

# define Rte_TypeDef_SEWS_PcbConfig_DOWHS_X1CXY_T
typedef uint8 SEWS_PcbConfig_DOWHS_X1CXY_T;

# define Rte_TypeDef_SEWS_PcbConfig_DOWLS_X1CXZ_T
typedef uint8 SEWS_PcbConfig_DOWLS_X1CXZ_T;

# define Rte_TypeDef_SEWS_PcbConfig_LinInterfaces_X1CX0_T
typedef uint8 SEWS_PcbConfig_LinInterfaces_X1CX0_T;

# define Rte_TypeDef_SEWS_X1CX4_PiInterface_T
typedef uint8 SEWS_X1CX4_PiInterface_T;

# define Rte_TypeDef_VGTT_EcuPinFaultStatus
typedef IOHWAB_UINT8 VGTT_EcuPinFaultStatus;

# define Rte_TypeDef_EcuHwDioCtrlArray_T
typedef Rte_DT_EcuHwDioCtrlArray_T_0 EcuHwDioCtrlArray_T[40];

# define Rte_TypeDef_EcuHwVoltageValues_T
typedef VGTT_EcuPinVoltage_0V2 EcuHwVoltageValues_T[40];

# define Rte_TypeDef_EngTraceHWArray
typedef EngTraceHWData_T EngTraceHWArray[10];

# define Rte_TypeDef_FSP_Array10_8
typedef FSP_Array8 FSP_Array10_8[10];

# define Rte_TypeDef_FlexibleSwitchesinFailure_Type
typedef FlexibleSwitchesinFailure_T FlexibleSwitchesinFailure_Type[24];

# define Rte_TypeDef_IoAsilCorePcbConfig_T
typedef PcbPopulatedInfo_T IoAsilCorePcbConfig_T[40];

# define Rte_TypeDef_SEWS_AntMappingConfig_Gain_X1C03_a_T
typedef SEWS_AntMappingConfig_Gain_X1C03_T SEWS_AntMappingConfig_Gain_X1C03_a_T[5];

# define Rte_TypeDef_SEWS_AntMappingConfig_Multi_X1CY3a_a_T
typedef SEWS_AntMappingConfig_Multi_X1CY3a_T SEWS_AntMappingConfig_Multi_X1CY3a_a_T[20];

# define Rte_TypeDef_SEWS_KeyfobEncryptCode_P1DS4_a_T
typedef SEWS_KeyfobEncryptCode_P1DS4_T SEWS_KeyfobEncryptCode_P1DS4_a_T[24];

# define Rte_TypeDef_SEWS_PcbConfig_Adi_X1CXW_a_T
typedef SEWS_PcbConfig_Adi_X1CXW_T SEWS_PcbConfig_Adi_X1CXW_a_T[19];

# define Rte_TypeDef_SEWS_PcbConfig_CanInterfaces_X1CX2_a_T
typedef SEWS_PcbConfig_CanInterfaces_X1CX2_T SEWS_PcbConfig_CanInterfaces_X1CX2_a_T[6];

# define Rte_TypeDef_SEWS_PcbConfig_DOBHS_X1CXX_a_T
typedef SEWS_PcbConfig_DOBHS_X1CXX_T SEWS_PcbConfig_DOBHS_X1CXX_a_T[4];

# define Rte_TypeDef_SEWS_PcbConfig_DOWHS_X1CXY_a_T
typedef SEWS_PcbConfig_DOWHS_X1CXY_T SEWS_PcbConfig_DOWHS_X1CXY_a_T[2];

# define Rte_TypeDef_SEWS_PcbConfig_DOWLS_X1CXZ_a_T
typedef SEWS_PcbConfig_DOWLS_X1CXZ_T SEWS_PcbConfig_DOWLS_X1CXZ_a_T[3];

# define Rte_TypeDef_SEWS_PcbConfig_LinInterfaces_X1CX0_a_T
typedef SEWS_PcbConfig_LinInterfaces_X1CX0_T SEWS_PcbConfig_LinInterfaces_X1CX0_a_T[7];

# define Rte_TypeDef_SEWS_X1CY5_AntennaP1_LimitRSSI_a_T
typedef SEWS_X1CY5_AntennaP1_LimitRSSI_T SEWS_X1CY5_AntennaP1_LimitRSSI_a_T[2];

# define Rte_TypeDef_SEWS_X1CY5_AntennaP2_LimitRSSI_a_T
typedef SEWS_X1CY5_AntennaP2_LimitRSSI_T SEWS_X1CY5_AntennaP2_LimitRSSI_a_T[2];

# define Rte_TypeDef_SEWS_X1CY5_AntennaP3_LimitRSSI_a_T
typedef SEWS_X1CY5_AntennaP3_LimitRSSI_T SEWS_X1CY5_AntennaP3_LimitRSSI_a_T[2];

# define Rte_TypeDef_SEWS_X1CY5_AntennaP4_LimitRSSI_a_T
typedef SEWS_X1CY5_AntennaP4_LimitRSSI_T SEWS_X1CY5_AntennaP4_LimitRSSI_a_T[2];

# define Rte_TypeDef_SEWS_X1CY5_AntennaPi_LimitRSSI_a_T
typedef SEWS_X1CY5_AntennaPi_LimitRSSI_T SEWS_X1CY5_AntennaPi_LimitRSSI_a_T[2];

# define Rte_TypeDef_AsymPrivateKeyType
typedef struct
{
  uint32 length;
  Rte_DT_AsymPrivateKeyType_1 data;
} AsymPrivateKeyType;

# define Rte_TypeDef_AsymPublicKeyType
typedef struct
{
  uint32 length;
  Rte_DT_AsymPublicKeyType_1 data;
} AsymPublicKeyType;

# define Rte_TypeDef_InteriorLightMode_rqst_T
typedef struct
{
  IL_ModeReq_T IL_Mode_RE;
  Rte_DT_InteriorLightMode_rqst_T_1 EventFlag_RE;
} InteriorLightMode_rqst_T;

# define Rte_TypeDef_J1939Rm_ExtIdInfoType
typedef struct
{
  J1939Rm_ExtIdType ExtIdType;
  uint8 ExtId1;
  uint8 ExtId2;
  uint8 ExtId3;
} J1939Rm_ExtIdInfoType;

# define Rte_TypeDef_KeyExchangeBaseType
typedef struct
{
  uint32 length;
  Rte_DT_KeyExchangeBaseType_1 data;
} KeyExchangeBaseType;

# define Rte_TypeDef_KeyExchangePrivateType
typedef struct
{
  uint32 length;
  Rte_DT_KeyExchangePrivateType_1 data;
} KeyExchangePrivateType;

# define Rte_TypeDef_SEWS_AdiWakeUpConfig_P1WMD_s_T
typedef struct
{
  SEWS_P1WMD_ThresholdHigh_T ThresholdHigh;
  SEWS_P1WMD_ThresholdLow_T ThresholdLow;
  boolean isActiveInLiving;
  boolean isActiveInParked;
} SEWS_AdiWakeUpConfig_P1WMD_s_T;

# define Rte_TypeDef_SEWS_AntMappingConfig_Single_X1CY5_s_T
typedef struct
{
  SEWS_X1CY5_AntennaPi_LimitRSSI_a_T AntennaPi_LimitRSSI;
  SEWS_X1CY5_AntennaP1_LimitRSSI_a_T AntennaP1_LimitRSSI;
  SEWS_X1CY5_AntennaP2_LimitRSSI_a_T AntennaP2_LimitRSSI;
  SEWS_X1CY5_AntennaP3_LimitRSSI_a_T AntennaP3_LimitRSSI;
  SEWS_X1CY5_AntennaP4_LimitRSSI_a_T AntennaP4_LimitRSSI;
} SEWS_AntMappingConfig_Single_X1CY5_s_T;

# define Rte_TypeDef_SEWS_Diag_Act_LF_P_P1V79_s_T
typedef struct
{
  SEWS_P1V79_PiInterface_T PiInterface;
  SEWS_P1V79_P1Interface_T P1Interface;
  SEWS_P1V79_P2Interface_T P2Interface;
  SEWS_P1V79_P3Interface_T P3Interface;
  SEWS_P1V79_P4Interface_T P4Interface;
} SEWS_Diag_Act_LF_P_P1V79_s_T;

# define Rte_TypeDef_SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T
typedef struct
{
  SEWS_X1CY1_DigitalBiLevelLow_T DigitalBiLevelLow;
  SEWS_X1CY1_DigitalBiLevelHigh_T DigitalBiLevelHigh;
} SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T;

# define Rte_TypeDef_SEWS_ECS_MemSwTimings_P1BWF_s_T
typedef struct
{
  SEWS_P1BWF_Level_Memorization_Min_Press_T Level_Memorization_Min_Press;
  SEWS_P1BWF_Memory_Recall_Min_Press_T Memory_Recall_Min_Press;
  SEWS_P1BWF_Memory_Recall_Max_Press_T Memory_Recall_Max_Press;
} SEWS_ECS_MemSwTimings_P1BWF_s_T;

# define Rte_TypeDef_SEWS_FSC_VoltageThreshold_X1CZQ_s_T
typedef struct
{
  SEWS_X1CZQ_ShutdownReady_T ShutdownReady;
  SEWS_X1CZQ_Reduced_T Reduced;
  SEWS_X1CZQ_Operating_T Operating;
  SEWS_X1CZQ_Protecting_T Protecting;
  SEWS_X1CZQ_Withstand_T Withstand;
} SEWS_FSC_VoltageThreshold_X1CZQ_s_T;

# define Rte_TypeDef_SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T
typedef struct
{
  SEWS_P1V8F_Threshold_VBT_T Threshold_VBT;
  SEWS_P1V8F_Threshold_VAT_T Threshold_VAT;
} SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T;

# define Rte_TypeDef_SEWS_Fault_Config_ADI01_P1V6U_s_T
typedef struct
{
  SEWS_P1V6U_Threshold_OC_STB_T Threshold_OC_STB;
  SEWS_P1V6U_Threshold_STG_T Threshold_STG;
} SEWS_Fault_Config_ADI01_P1V6U_s_T;

# define Rte_TypeDef_SEWS_Fault_Config_ADI02_P1V6V_s_T
typedef struct
{
  SEWS_P1V6V_Threshold_OC_STB_T Threshold_OC_STB;
  SEWS_P1V6V_Threshold_STG_T Threshold_STG;
} SEWS_Fault_Config_ADI02_P1V6V_s_T;

# define Rte_TypeDef_SEWS_Fault_Config_ADI03_P1V6W_s_T
typedef struct
{
  SEWS_P1V6W_ContactOpen_T ContactOpen;
  SEWS_P1V6W_ContactClosed_T ContactClosed;
} SEWS_Fault_Config_ADI03_P1V6W_s_T;

# define Rte_TypeDef_SEWS_Fault_Config_ADI04_P1V6X_s_T
typedef struct
{
  SEWS_P1V6X_ContactOpen_T ContactOpen;
  SEWS_P1V6X_ContactClosed_T ContactClosed;
} SEWS_Fault_Config_ADI04_P1V6X_s_T;

# define Rte_TypeDef_SEWS_Fault_Config_ADI05_P1V6Y_s_T
typedef struct
{
  SEWS_P1V6Y_ContactOpen_T ContactOpen;
  SEWS_P1V6Y_ContactClosed_T ContactClosed;
} SEWS_Fault_Config_ADI05_P1V6Y_s_T;

# define Rte_TypeDef_SEWS_Fault_Config_ADI06_P1V6Z_s_T
typedef struct
{
  SEWS_P1V6Z_ContactOpen_T ContactOpen;
  SEWS_P1V6Z_ContactClosed_T ContactClosed;
} SEWS_Fault_Config_ADI06_P1V6Z_s_T;

# define Rte_TypeDef_SEWS_Fault_Config_ADI07_P1V60_s_T
typedef struct
{
  SEWS_P1V60_ContactOpen_T ContactOpen;
  SEWS_P1V60_ContactClosed_T ContactClosed;
} SEWS_Fault_Config_ADI07_P1V60_s_T;

# define Rte_TypeDef_SEWS_Fault_Config_ADI08_P1V61_s_T
typedef struct
{
  SEWS_P1V61_ContactOpen_T ContactOpen;
  SEWS_P1V61_ContactClosed_T ContactClosed;
} SEWS_Fault_Config_ADI08_P1V61_s_T;

# define Rte_TypeDef_SEWS_Fault_Config_ADI09_P1V62_s_T
typedef struct
{
  SEWS_P1V62_ContactOpen_T ContactOpen;
  SEWS_P1V62_ContactClosed_T ContactClosed;
} SEWS_Fault_Config_ADI09_P1V62_s_T;

# define Rte_TypeDef_SEWS_Fault_Config_ADI10_P1V63_s_T
typedef struct
{
  SEWS_P1V63_ContactOpen_T ContactOpen;
  SEWS_P1V63_ContactClosed_T ContactClosed;
} SEWS_Fault_Config_ADI10_P1V63_s_T;

# define Rte_TypeDef_SEWS_Fault_Config_ADI11_P1V64_s_T
typedef struct
{
  SEWS_P1V64_ContactOpen_T ContactOpen;
  SEWS_P1V64_ContactClosed_T ContactClosed;
} SEWS_Fault_Config_ADI11_P1V64_s_T;

# define Rte_TypeDef_SEWS_Fault_Config_ADI12_P1V65_s_T
typedef struct
{
  SEWS_P1V65_ContactOpen_T ContactOpen;
  SEWS_P1V65_ContactClosed_T ContactClosed;
} SEWS_Fault_Config_ADI12_P1V65_s_T;

# define Rte_TypeDef_SEWS_Fault_Config_ADI13_P1V66_s_T
typedef struct
{
  SEWS_P1V66_ContactOpen_T ContactOpen;
  SEWS_P1V66_ContactClosed_T ContactClosed;
} SEWS_Fault_Config_ADI13_P1V66_s_T;

# define Rte_TypeDef_SEWS_Fault_Config_ADI14_P1V67_s_T
typedef struct
{
  SEWS_P1V67_ContactOpen_T ContactOpen;
  SEWS_P1V67_ContactClosed_T ContactClosed;
} SEWS_Fault_Config_ADI14_P1V67_s_T;

# define Rte_TypeDef_SEWS_Fault_Config_ADI15_P1V68_s_T
typedef struct
{
  SEWS_P1V68_ContactOpen_T ContactOpen;
  SEWS_P1V68_ContactClosed_T ContactClosed;
} SEWS_Fault_Config_ADI15_P1V68_s_T;

# define Rte_TypeDef_SEWS_Fault_Config_ADI16_P1V69_s_T
typedef struct
{
  SEWS_P1V69_ContactOpen_T ContactOpen;
  SEWS_P1V69_ContactClosed_T ContactClosed;
} SEWS_Fault_Config_ADI16_P1V69_s_T;

# define Rte_TypeDef_SEWS_ForcedPositionStatus_Front_P1JSY_s_T
typedef struct
{
  SEWS_P1JSY_DriveStroke_T DriveStroke;
  SEWS_P1JSY_RampStroke_T RampStroke;
} SEWS_ForcedPositionStatus_Front_P1JSY_s_T;

# define Rte_TypeDef_SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T
typedef struct
{
  SEWS_P1JSZ_DriveStroke_T DriveStroke;
  SEWS_P1JSZ_RampStroke_T RampStroke;
} SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T;

# define Rte_TypeDef_SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T
typedef struct
{
  SEWS_P1QR6_Threshold_VBT_T Threshold_VBT;
  SEWS_P1QR6_Threshold_VAT_T Threshold_VAT;
  SEWS_P1QR6_Threshold_VOR_T Threshold_VOR;
} SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T;

# define Rte_TypeDef_SEWS_IL_ShortLongPushThresholds_P1DKF_s_T
typedef struct
{
  SEWS_P1DKF_Long_press_threshold_T Long_press_threshold;
  SEWS_P1DKF_Shut_off_threshold_T Shut_off_threshold;
} SEWS_IL_ShortLongPushThresholds_P1DKF_s_T;

# define Rte_TypeDef_SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T
typedef struct
{
  SEWS_X1CX4_PiInterface_T PiInterface;
  SEWS_X1CX4_P1Interface_T P1Interface;
  SEWS_X1CX4_P2Interface_T P2Interface;
  SEWS_X1CX4_P3Interface_T P3Interface;
  SEWS_X1CX4_P4Interface_T P4Interface;
} SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T;

# define Rte_TypeDef_SEWS_VINCheckProcessing_P1VKG_s_T
typedef struct
{
  SEWS_P1VKG_APM_Check_Active_T APM_Check_Active;
  SEWS_P1VKG_MVUC_Check_Active_T MVUC_Check_Active;
  SEWS_P1VKG_EMS_Check_Active_T EMS_Check_Active;
  SEWS_P1VKG_TECU_Check_Active_T TECU_Check_Active;
  SEWS_P1VKG_DISPLAY_Check_Active_T DISPLAY_Check_Active;
} SEWS_VINCheckProcessing_P1VKG_s_T;

# define Rte_TypeDef_SEWS_X1CY2_Fastened_s_T
typedef struct
{
  SEWS_X1CY2_FastenedHigh_STB_T FastenedHigh_STB;
  SEWS_X1CY2_FastenedLow_T FastenedLow;
} SEWS_X1CY2_Fastened_s_T;

# define Rte_TypeDef_SEWS_X1CY2_UnfastnedAndNotSeat_s_T
typedef struct
{
  SEWS_X1CY2_UnfastnedAndNotSeatHigh_T UnfastnedAndNotSeatHigh;
  SEWS_X1CY2_UnfastnedAndNotSeatLow_T UnfastnedAndNotSeatLow;
} SEWS_X1CY2_UnfastnedAndNotSeat_s_T;

# define Rte_TypeDef_SEWS_X1CY2_UnfastnedAndSeat_s_T
typedef struct
{
  SEWS_X1CY2_UnfastnedAndSeatHigh_T UnfastnedAndSeatHigh;
  SEWS_X1CY2_UnfastnedAndSeatLow_T UnfastnedAndSeatLow;
} SEWS_X1CY2_UnfastnedAndSeat_s_T;

# define Rte_TypeDef_SymKeyType
typedef struct
{
  UInt32_Length length;
  Rte_DT_SymKeyType_1 data;
} SymKeyType;

# define Rte_TypeDef_st_RKEdata
typedef struct
{
  rkedata rkedata;
  uint32 rolling_cnt;
  uint8 rkecmd;
  uint8 v_low;
} st_RKEdata;

# define Rte_TypeDef_Rte_DT_EcuHwFaultValues_T_0
typedef VGTT_EcuPinFaultStatus Rte_DT_EcuHwFaultValues_T_0;

# define Rte_TypeDef_EcuHwFaultValues_T
typedef Rte_DT_EcuHwFaultValues_T_0 EcuHwFaultValues_T[40];

# define Rte_TypeDef_SEWS_AdiWakeUpConfig_P1WMD_a_T
typedef SEWS_AdiWakeUpConfig_P1WMD_s_T SEWS_AdiWakeUpConfig_P1WMD_a_T[16];

# define Rte_TypeDef_SEWS_AntMappingConfig_Multi_X1CY3_a_T
typedef SEWS_AntMappingConfig_Multi_X1CY3a_a_T SEWS_AntMappingConfig_Multi_X1CY3_a_T[10];

# define Rte_TypeDef_SEWS_PassengersSeatBeltVoltageLevels_X1CY2_s_T
typedef struct
{
  SEWS_X1CY2_Fastened_s_T Fastened;
  SEWS_X1CY2_UnfastnedAndNotSeat_s_T UnfastnedAndNotSeat;
  SEWS_X1CY2_UnfastnedAndSeat_s_T UnfastnedAndSeat;
} SEWS_PassengersSeatBeltVoltageLevels_X1CY2_s_T;


# ifndef RTE_SUPPRESS_UNUSED_DATATYPES
/**********************************************************************************************************************
 * Unused Data type definitions
 *********************************************************************************************************************/

#  define Rte_TypeDef_Debug_PVT_PWM_ReportChip
typedef boolean Debug_PVT_PWM_ReportChip;

#  define Rte_TypeDef_Debug_PVT_SCIM_RD_WCh1_Duty
typedef uint8 Debug_PVT_SCIM_RD_WCh1_Duty;

#  define Rte_TypeDef_Debug_PVT_SCIM_RD_WCh1_Volt
typedef uint8 Debug_PVT_SCIM_RD_WCh1_Volt;

#  define Rte_TypeDef_Debug_PVT_SCIM_RD_WCh2_Duty
typedef uint8 Debug_PVT_SCIM_RD_WCh2_Duty;

#  define Rte_TypeDef_Debug_PVT_SCIM_RD_WCh2_Volt
typedef uint8 Debug_PVT_SCIM_RD_WCh2_Volt;

#  define Rte_TypeDef_Debug_PVT_SCIM_RD_WLS1_Freq
typedef uint16 Debug_PVT_SCIM_RD_WLS1_Freq;

#  define Rte_TypeDef_Debug_PVT_SCIM_RD_WLS3_Duty
typedef uint8 Debug_PVT_SCIM_RD_WLS3_Duty;

#  define Rte_TypeDef_Debug_PVT_SCIM_RD_WLS3_Volt
typedef uint8 Debug_PVT_SCIM_RD_WLS3_Volt;

#  define Rte_TypeDef_Debug_PVT_ScimHwSelect_WLS1
typedef boolean Debug_PVT_ScimHwSelect_WLS1;

#  define Rte_TypeDef_DrivingMode
typedef uint16 DrivingMode;

#  define Rte_TypeDef_ResponseErrorDLFW
typedef boolean ResponseErrorDLFW;

#  define Rte_TypeDef_ResponseErrorELCP1
typedef boolean ResponseErrorELCP1;

#  define Rte_TypeDef_ResponseErrorELCP2
typedef boolean ResponseErrorELCP2;

#  define Rte_TypeDef_ResponseErrorILCP1
typedef boolean ResponseErrorILCP1;

#  define Rte_TypeDef_ResponseErrorILCP2
typedef boolean ResponseErrorILCP2;

#  define Rte_TypeDef_ResponseErrorLECM2
typedef boolean ResponseErrorLECM2;

#  define Rte_TypeDef_ResponseErrorLECMBasic
typedef boolean ResponseErrorLECMBasic;

#  define Rte_TypeDef_ResponseErrorTCP
typedef boolean ResponseErrorTCP;

#  define Rte_TypeDef_ResponseErrorTCP_T
typedef boolean ResponseErrorTCP_T;

#  define Rte_TypeDef_AutomaticOffOn_T
typedef uint8 AutomaticOffOn_T;

#  define Rte_TypeDef_Debug_PVT_PWM_CS
typedef uint8 Debug_PVT_PWM_CS;

#  define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_LivingPullUp
typedef uint8 Debug_PVT_SCIM_Ctrl_LivingPullUp;

#  define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_ParkedPullUp
typedef uint8 Debug_PVT_SCIM_Ctrl_ParkedPullUp;

#  define Rte_TypeDef_Debug_PVT_SCIM_Ctrl_WLS1
typedef uint8 Debug_PVT_SCIM_Ctrl_WLS1;

#  define Rte_TypeDef_Debug_PVT_SCIM_RD_WLS1_Fault
typedef uint8 Debug_PVT_SCIM_RD_WLS1_Fault;

#  define Rte_TypeDef_DoorKeyCylTrn_stat_decrypt_T
typedef uint8 DoorKeyCylTrn_stat_decrypt_T;

#  define Rte_TypeDef_ESCState_T
typedef uint8 ESCState_T;

#  define Rte_TypeDef_EcoBalancedSwitch_stat
typedef uint8 EcoBalancedSwitch_stat;

#  define Rte_TypeDef_FrtAxleHydroActive_rqst
typedef uint8 FrtAxleHydroActive_rqst;

#  define Rte_TypeDef_IllegalDiffLockSwapOperation_T
typedef uint8 IllegalDiffLockSwapOperation_T;

#  define Rte_TypeDef_Locking_SwitchStatus_T
typedef uint8 Locking_SwitchStatus_T;

#  define Rte_TypeDef_SeatSwivelWarning_rqst
typedef uint8 SeatSwivelWarning_rqst;

#  define Rte_TypeDef_TCPKnobPostionStatus_T
typedef uint8 TCPKnobPostionStatus_T;

#  define Rte_TypeDef_TransmissionDrivingMode
typedef uint8 TransmissionDrivingMode;

#  define Rte_TypeDef_VehicleMode
typedef uint8 VehicleMode;

#  define Rte_TypeDef_ArrayByteSize16
typedef uint8 ArrayByteSize16[16];

#  define Rte_TypeDef_ArrayByteSize3
typedef uint8 ArrayByteSize3[3];

#  define Rte_TypeDef_ArrayByteSize8
typedef uint8 ArrayByteSize8[8];

#  define Rte_TypeDef_Dcm_Data2040ByteType
typedef uint8 Dcm_Data2040ByteType[2040];

#  define Rte_TypeDef_DtcId_T
typedef uint32 DtcId_T[30];

#  define Rte_TypeDef_EncryptedGBUnlockAuth_stat
typedef uint8 EncryptedGBUnlockAuth_stat[16];

#  define Rte_TypeDef_FSP_Array9
typedef uint8 FSP_Array9[9];

#  define Rte_TypeDef_FSP_Boolean255
typedef Boolean FSP_Boolean255[255];

#  define Rte_TypeDef_FSP_array255
typedef uint8 FSP_array255[255];

#  define Rte_TypeDef_LuggageCompartment_stat
typedef uint8 LuggageCompartment_stat[16];

#  define Rte_TypeDef_TheftAlarmActivation_rqst
typedef uint8 TheftAlarmActivation_rqst[16];

#  define Rte_TypeDef_TheftAlarmActivation_rqst_1
typedef uint8 TheftAlarmActivation_rqst_1[16];

#  define Rte_TypeDef_TheftAlarmActivation_rqst_2
typedef uint8 TheftAlarmActivation_rqst_2[16];

#  define Rte_TypeDef_tVAR
typedef struct
{
  uint8 new_input;
  uint8 old_input;
} tVAR;

#  define Rte_TypeDef_SEWS_ExtraLightSignPosition_P1DQZ_T
typedef uint8 SEWS_ExtraLightSignPosition_P1DQZ_T;

#  define Rte_TypeDef_SEWS_ExtraLight_EquipementLightCabCtrl_P1TIU_T
typedef uint8 SEWS_ExtraLight_EquipementLightCabCtrl_P1TIU_T;

#  define Rte_TypeDef_SEWS_ExtraLight_EquipementLight_functiontype_P1SLL_T
typedef uint8 SEWS_ExtraLight_EquipementLight_functiontype_P1SLL_T;

#  define Rte_TypeDef_SEWS_ExtraLight_RoofLightSign_P1BYT_T
typedef uint8 SEWS_ExtraLight_RoofLightSign_P1BYT_T;

#  define Rte_TypeDef_SEWS_FCW_SwType_P1TN7_T
typedef uint8 SEWS_FCW_SwType_P1TN7_T;

#  define Rte_TypeDef_SEWS_FS_DiagAct_ID180_ID189_P1EAR_T
typedef uint16 SEWS_FS_DiagAct_ID180_ID189_P1EAR_T;

#  define Rte_TypeDef_SEWS_P1V6V_ContactClosed_T
typedef uint8 SEWS_P1V6V_ContactClosed_T;

#  define Rte_TypeDef_SEWS_P1V6V_ContactOpen_T
typedef uint8 SEWS_P1V6V_ContactOpen_T;

#  define Rte_TypeDef_SEWS_P1V7A_ContactClosed_T
typedef uint8 SEWS_P1V7A_ContactClosed_T;

#  define Rte_TypeDef_SEWS_P1V7A_ContactOpen_T
typedef uint8 SEWS_P1V7A_ContactOpen_T;

#  define Rte_TypeDef_SEWS_P1V7B_ContactClosed_T
typedef uint8 SEWS_P1V7B_ContactClosed_T;

#  define Rte_TypeDef_SEWS_P1V7B_ContactOpen_T
typedef uint8 SEWS_P1V7B_ContactOpen_T;

#  define Rte_TypeDef_SEWS_P1V7C_ContactClosed_T
typedef uint8 SEWS_P1V7C_ContactClosed_T;

#  define Rte_TypeDef_SEWS_P1V7C_ContactOpen_T
typedef uint8 SEWS_P1V7C_ContactOpen_T;

#  define Rte_TypeDef_SEWS_X1CYH_ApiId_T
typedef uint8 SEWS_X1CYH_ApiId_T;

#  define Rte_TypeDef_SEWS_X1CYH_EcuMode_T
typedef uint8 SEWS_X1CYH_EcuMode_T;

#  define Rte_TypeDef_SEWS_X1CYH_ErrorId_T
typedef uint8 SEWS_X1CYH_ErrorId_T;

#  define Rte_TypeDef_SEWS_X1CYH_EventId_T
typedef uint8 SEWS_X1CYH_EventId_T;

#  define Rte_TypeDef_SEWS_X1CYH_ExceptionContext_T
typedef uint32 SEWS_X1CYH_ExceptionContext_T;

#  define Rte_TypeDef_SEWS_X1CYH_ExecTimeSincePowerOn_Long_T
typedef uint16 SEWS_X1CYH_ExecTimeSincePowerOn_Long_T;

#  define Rte_TypeDef_SEWS_X1CYH_ExecTimeSincePowerOn_Short_T
typedef uint8 SEWS_X1CYH_ExecTimeSincePowerOn_Short_T;

#  define Rte_TypeDef_SEWS_X1CYH_ExecTimeSinceWakeUp_Short_T
typedef uint8 SEWS_X1CYH_ExecTimeSinceWakeUp_Short_T;

#  define Rte_TypeDef_SEWS_X1CYH_InstanceId_T
typedef uint8 SEWS_X1CYH_InstanceId_T;

#  define Rte_TypeDef_SEWS_X1CYH_MicrosarErrorCode_T
typedef uint8 SEWS_X1CYH_MicrosarErrorCode_T;

#  define Rte_TypeDef_SEWS_X1CYH_ModuleId_T
typedef uint16 SEWS_X1CYH_ModuleId_T;

#  define Rte_TypeDef_SEWS_X1CYH_RunnableId_T
typedef uint16 SEWS_X1CYH_RunnableId_T;

#  define Rte_TypeDef_VGTT_ScimAdiPinsState
typedef IOHWAB_UINT16 VGTT_ScimAdiPinsState;

#  define Rte_TypeDef_NvM_ServiceIdType
typedef uint8 NvM_ServiceIdType;

#  define Rte_TypeDef_PcbAdiPopulatedInfo_T
typedef uint8 PcbAdiPopulatedInfo_T;

#  define Rte_TypeDef_PcbDAIPopulatedInfo_T
typedef uint8 PcbDAIPopulatedInfo_T;

#  define Rte_TypeDef_PcbDobhsPopulatedInfo_T
typedef uint8 PcbDobhsPopulatedInfo_T;

#  define Rte_TypeDef_Rte_DT_EcuHwDioCtrl_T_0
typedef IOHWAB_UINT8 Rte_DT_EcuHwDioCtrl_T_0;

#  define Rte_TypeDef_Rte_DT_IoAsilDobhsPcbConfigInfo_T_0
typedef PcbDobhsPopulatedInfo_T Rte_DT_IoAsilDobhsPcbConfigInfo_T_0;

#  define Rte_TypeDef_Rte_DT_IoQmAdiPcbConfigInfo_T_0
typedef PcbAdiPopulatedInfo_T Rte_DT_IoQmAdiPcbConfigInfo_T_0;

#  define Rte_TypeDef_Rte_DT_IoQmAdiPullUpPcbConfigInfo_T_0
typedef PcbPopulatedInfo_T Rte_DT_IoQmAdiPullUpPcbConfigInfo_T_0;

#  define Rte_TypeDef_Rte_DT_IoQmCanPcbConfigInfo_T_0
typedef PcbPopulatedInfo_T Rte_DT_IoQmCanPcbConfigInfo_T_0;

#  define Rte_TypeDef_Rte_DT_IoQmDaiPcbConfigInfo_T_0
typedef PcbDAIPopulatedInfo_T Rte_DT_IoQmDaiPcbConfigInfo_T_0;

#  define Rte_TypeDef_Rte_DT_IoQmHsPwmPcbConfigInfo_T_0
typedef PcbPopulatedInfo_T Rte_DT_IoQmHsPwmPcbConfigInfo_T_0;

#  define Rte_TypeDef_Rte_DT_IoQmLinPcbConfigInfo_T_0
typedef PcbPopulatedInfo_T Rte_DT_IoQmLinPcbConfigInfo_T_0;

#  define Rte_TypeDef_Rte_DT_IoQmLsPwmPcbConfigInfo_T_0
typedef PcbPopulatedInfo_T Rte_DT_IoQmLsPwmPcbConfigInfo_T_0;

#  define Rte_TypeDef_SEWS_PcbConfig_DOWHS_X1CXZ_T
typedef uint8 SEWS_PcbConfig_DOWHS_X1CXZ_T;

#  define Rte_TypeDef_DobhsPcbPopulated_T
typedef PcbPopulatedInfo_T DobhsPcbPopulated_T[4];

#  define Rte_TypeDef_EcuHwDioCtrl_T
typedef Rte_DT_EcuHwDioCtrl_T_0 EcuHwDioCtrl_T[3];

#  define Rte_TypeDef_FSP_Array10_9
typedef FSP_Array9 FSP_Array10_9[10];

#  define Rte_TypeDef_IoAsilDobhsPcbConfigInfo_T
typedef Rte_DT_IoAsilDobhsPcbConfigInfo_T_0 IoAsilDobhsPcbConfigInfo_T[4];

#  define Rte_TypeDef_IoQmAdiPcbConfigInfo_T
typedef Rte_DT_IoQmAdiPcbConfigInfo_T_0 IoQmAdiPcbConfigInfo_T[19];

#  define Rte_TypeDef_IoQmAdiPullUpPcbConfigInfo_T
typedef Rte_DT_IoQmAdiPullUpPcbConfigInfo_T_0 IoQmAdiPullUpPcbConfigInfo_T[2];

#  define Rte_TypeDef_IoQmCanPcbConfigInfo_T
typedef Rte_DT_IoQmCanPcbConfigInfo_T_0 IoQmCanPcbConfigInfo_T[6];

#  define Rte_TypeDef_IoQmDaiPcbConfigInfo_T
typedef Rte_DT_IoQmDaiPcbConfigInfo_T_0 IoQmDaiPcbConfigInfo_T[1];

#  define Rte_TypeDef_IoQmHsPwmPcbConfigInfo_T
typedef Rte_DT_IoQmHsPwmPcbConfigInfo_T_0 IoQmHsPwmPcbConfigInfo_T[2];

#  define Rte_TypeDef_IoQmLinPcbConfigInfo_T
typedef Rte_DT_IoQmLinPcbConfigInfo_T_0 IoQmLinPcbConfigInfo_T[7];

#  define Rte_TypeDef_IoQmLsPwmPcbConfigInfo_T
typedef Rte_DT_IoQmLsPwmPcbConfigInfo_T_0 IoQmLsPwmPcbConfigInfo_T[3];

#  define Rte_TypeDef_SEWS_PcbConfig_DOWHS_X1CXZ_a_T
typedef SEWS_PcbConfig_DOWHS_X1CXZ_T SEWS_PcbConfig_DOWHS_X1CXZ_a_T[3];

#  define Rte_TypeDef_SEWS_X1CYH_ExceptionContext_a_T
typedef SEWS_X1CYH_ExceptionContext_T SEWS_X1CYH_ExceptionContext_a_T[32];

#  define Rte_TypeDef_SEWS_X1CYH_RunnableId_a_T
typedef SEWS_X1CYH_RunnableId_T SEWS_X1CYH_RunnableId_a_T[32];

#  define Rte_TypeDef_FaultEventGWRegisterDTC_T
typedef struct
{
  uint8 NoOfDTC;
  DtcId_T DtcId;
} FaultEventGWRegisterDTC_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI01_P1V6U_s_T
typedef struct
{
  SEWS_P1V6U_Threshold_OC_STB_T Threshold_OC_STB;
  SEWS_P1V6U_Threshold_STG_T Threshold_STG;
} SEWS_Diag_Act_ADI01_P1V6U_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI02_P1V6V_s_T
typedef struct
{
  SEWS_P1V6V_Threshold_OC_STB_T Threshold_OC_STB;
  SEWS_P1V6V_Threshold_STG_T Threshold_STG;
} SEWS_Diag_Act_ADI02_P1V6V_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI03_P1V6W_s_T
typedef struct
{
  SEWS_P1V6W_ContactOpen_T ContactOpen;
  SEWS_P1V6W_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI03_P1V6W_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI04_P1V6X_s_T
typedef struct
{
  SEWS_P1V6X_ContactOpen_T ContactOpen;
  SEWS_P1V6X_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI04_P1V6X_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI05_P1V6Y_s_T
typedef struct
{
  SEWS_P1V6Y_ContactOpen_T ContactOpen;
  SEWS_P1V6Y_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI05_P1V6Y_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI06_P1V6Z_s_T
typedef struct
{
  SEWS_P1V6Z_ContactOpen_T ContactOpen;
  SEWS_P1V6Z_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI06_P1V6Z_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI07_P1V60_s_T
typedef struct
{
  SEWS_P1V60_ContactOpen_T ContactOpen;
  SEWS_P1V60_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI07_P1V60_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI08_P1V61_s_T
typedef struct
{
  SEWS_P1V61_ContactOpen_T ContactOpen;
  SEWS_P1V61_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI08_P1V61_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI09_P1V62_s_T
typedef struct
{
  SEWS_P1V62_ContactOpen_T ContactOpen;
  SEWS_P1V62_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI09_P1V62_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI10_P1V63_s_T
typedef struct
{
  SEWS_P1V63_ContactOpen_T ContactOpen;
  SEWS_P1V63_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI10_P1V63_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI11_P1V64_s_T
typedef struct
{
  SEWS_P1V64_ContactOpen_T ContactOpen;
  SEWS_P1V64_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI11_P1V64_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI12_P1V65_s_T
typedef struct
{
  SEWS_P1V65_ContactOpen_T ContactOpen;
  SEWS_P1V65_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI12_P1V65_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI13_P1V66_s_T
typedef struct
{
  SEWS_P1V66_ContactOpen_T ContactOpen;
  SEWS_P1V66_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI13_P1V66_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI14_P1V67_s_T
typedef struct
{
  SEWS_P1V67_ContactOpen_T ContactOpen;
  SEWS_P1V67_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI14_P1V67_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI15_P1V68_s_T
typedef struct
{
  SEWS_P1V68_ContactOpen_T ContactOpen;
  SEWS_P1V68_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI15_P1V68_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI16_P1V69_s_T
typedef struct
{
  SEWS_P1V69_ContactOpen_T ContactOpen;
  SEWS_P1V69_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI16_P1V69_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI17_P1V7A_s_T
typedef struct
{
  SEWS_P1V7A_ContactOpen_T ContactOpen;
  SEWS_P1V7A_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI17_P1V7A_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI18_P1V7B_s_T
typedef struct
{
  SEWS_P1V7B_ContactOpen_T ContactOpen;
  SEWS_P1V7B_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI18_P1V7B_s_T;

#  define Rte_TypeDef_SEWS_Diag_Act_ADI19_P1V7C_s_T
typedef struct
{
  SEWS_P1V7C_ContactOpen_T ContactOpen;
  SEWS_P1V7C_ContactClosed_T ContactClosed;
} SEWS_Diag_Act_ADI19_P1V7C_s_T;

#  define Rte_TypeDef_SEWS_Diag_Cfg_PWR24V_P1V8F_s_T
typedef struct
{
  SEWS_P1V8F_Threshold_VBT_T Threshold_VBT;
  SEWS_P1V8F_Threshold_VAT_T Threshold_VAT;
} SEWS_Diag_Cfg_PWR24V_P1V8F_s_T;

#  define Rte_TypeDef_SEWS_HWIO_CfgDiag_PWR24V_P1QR6_s_T
typedef struct
{
  SEWS_P1QR6_Threshold_VBT_T Threshold_VBT;
  SEWS_P1QR6_Threshold_VAT_T Threshold_VAT;
  SEWS_P1QR6_Threshold_VOR_T Threshold_VOR;
} SEWS_HWIO_CfgDiag_PWR24V_P1QR6_s_T;

#  define Rte_TypeDef_SEWS_X1CYH_ExecutionTime_s_T
typedef struct
{
  SEWS_X1CYH_ExecTimeSincePowerOn_Short_T ExecTimeSincePowerOn_Short;
  SEWS_X1CYH_ExecTimeSincePowerOn_Long_T ExecTimeSincePowerOn_Long;
  SEWS_X1CYH_ExecTimeSinceWakeUp_Short_T ExecTimeSinceWakeUp_Short;
} SEWS_X1CYH_ExecutionTime_s_T;

#  define Rte_TypeDef_SEWS_SwExecProfile_LastFaults_X1CYH_s_T
typedef struct
{
  SEWS_X1CYH_ModuleId_T ModuleId;
  SEWS_X1CYH_InstanceId_T InstanceId;
  SEWS_X1CYH_ApiId_T ApiId;
  SEWS_X1CYH_ErrorId_T ErrorId;
  SEWS_X1CYH_EcuMode_T EcuMode;
  SEWS_X1CYH_EventId_T EventId;
  SEWS_X1CYH_MicrosarErrorCode_T MicrosarErrorCode;
  SEWS_X1CYH_RunnableId_a_T RunnableId;
  SEWS_X1CYH_ExecutionTime_s_T ExecutionTime;
  SEWS_X1CYH_ExceptionContext_a_T ExceptionContext;
} SEWS_SwExecProfile_LastFaults_X1CYH_s_T;

#  define Rte_TypeDef_SEWS_SwExecProfile_LastFaults_X1CYH_a_T
typedef SEWS_SwExecProfile_LastFaults_X1CYH_s_T SEWS_SwExecProfile_LastFaults_X1CYH_a_T[10];

# endif


/**********************************************************************************************************************
 * Per-Instance Memory User Types
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * Constant value definitions
 *********************************************************************************************************************/

# define RTE_START_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(Crypto_Function_serialized_T, RTE_CONST) Rte_C_DriverDoorLatch_rqt_serialized_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(Crypto_Function_serialized_T, RTE_CONST) Rte_C_DrivrDrKeyCylTrn_st_serialized_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(Crypto_Function_serialized_T, RTE_CONST) Rte_C_EngineStartAuth_st_serialized_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(Crypto_Function_serialized_T, RTE_CONST) Rte_C_GrbxUnlockAuth_stat_serialized_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(Crypto_Function_serialized_T, RTE_CONST) Rte_C_KeyAuth_stat_serialized_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(Crypto_Function_serialized_T, RTE_CONST) Rte_C_Locking_Switch_stat_serialized_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(Crypto_Function_serialized_T, RTE_CONST) Rte_C_LuggageCompart_stat_serialized_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(Crypto_Function_serialized_T, RTE_CONST) Rte_C_PsgDoorKeyCylTrn_st_serialized_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(Crypto_Function_serialized_T, RTE_CONST) Rte_C_PsngrDoorLatch_rqst_serialized_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(Crypto_Function_serialized_T, RTE_CONST) Rte_C_ReducedSetMode_rqst_serialized_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(Crypto_Function_serialized_T, RTE_CONST) Rte_C_TheftAlarmAct_rqst_serialized_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(Dcm_Data12ByteType, RTE_CONST) Rte_C_Dcm_Data12ByteType_0; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(Debug_PVT_SCIM_FlexArrayData, RTE_CONST) Rte_C_Debug_PVT_SCIM_FlexArrayData_0; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(DrivingMode_T, RTE_CONST) Rte_C_DrivingMode_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(EcuHwDioCtrlArray_T, RTE_CONST) Rte_C_EcuHwDioCtrlArray_T_0; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(EcuHwFaultValues_T, RTE_CONST) Rte_C_EcuHwFaultValues_T_0; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(EcuHwVoltageValues_T, RTE_CONST) Rte_C_EcuHwVoltageValues_T_0; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(EngTraceHWArray, RTE_CONST) Rte_C_EngTraceHWArray_0; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(FSPIndicationCmdArray_T, RTE_CONST) Rte_C_FSPIndicationCmdArray_T_0; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(FSPSwitchStatusArray_T, RTE_CONST) Rte_C_FSPSwitchStatusArray_T_0; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(FSPSwitchStatusArray_T, RTE_CONST) Rte_C_FSPSwitchStatusArray_T_1; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(FSP_Array5, RTE_CONST) Rte_C_FSP_Array5_0; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(FspNVM_T, RTE_CONST) Rte_C_FspNVM_T_0; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(IoAsilCorePcbConfig_T, RTE_CONST) Rte_C_IoAsilCorePcbConfig_T_0; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(KeyFobNVM_T, RTE_CONST) Rte_C_KeyfobNV_InitialValue; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(SEWS_KeyfobEncryptCode_P1DS4_a_T, RTE_CONST) Rte_C_SEWS_KeyfobEncryptCode_P1DS4_a_T_0; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(SoftwareVersionSupported_T, RTE_CONST) Rte_C_FMSSoftwareVersionSupported_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(SpeedControl_NVM_T, RTE_CONST) Rte_C_SpeedControl_NVM_T_0; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(StandardNVM_T, RTE_CONST) Rte_C_StandardNVM_T_0; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(StandardNVM_T, RTE_CONST) Rte_C_StandardNVM_T_1; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(SwitchDetectionResp_T, RTE_CONST) Rte_C_SwitchDetectionResp_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(VEC_CryptoProxy_UserSignal, RTE_CONST) Rte_VEC_CryptoProxyApplicationData_Init; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(AlmClkCurAlarm_stat_T, RTE_CONST) Rte_C_AlmClkCurAlarm_stat_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(AlmClkCurAlarm_stat_T, RTE_CONST) Rte_C_LIN_AlmClkCurAlarm_stat_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(AlmClkSetCurAlm_rqst_T, RTE_CONST) Rte_C_AlmClkSetCurAlm_rqst_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(AlmClkSetCurAlm_rqst_T, RTE_CONST) Rte_C_LIN_AlmClkSetCurAlm_rqst_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(DiagFaultStat_T, RTE_CONST) Rte_C_DiagFaultStat_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(FMS1_T, RTE_CONST) Rte_C_FMS1_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(FMS1_T, RTE_CONST) Rte_C_FMS1_fms_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(FPBRMMIStat_T, RTE_CONST) Rte_C_FPBRMMIStat_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(InteriorLightMode_T, RTE_CONST) Rte_C_BnkH1IntLghtMMenu_stat_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(InteriorLightMode_T, RTE_CONST) Rte_C_IntLghtModeInd_cmd_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(InteriorLightMode_T, RTE_CONST) Rte_C_LIN_IntLghtModeInd_cmd_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(InteriorLightMode_rqst_T, RTE_CONST) Rte_C_IntLightMode_CoreRqst_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(InteriorLightMode_rqst_T, RTE_CONST) Rte_C_InteriorLightMode_rqst_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(LevelRequest_T, RTE_CONST) Rte_C_LevelRequest_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(OilPrediction_T, RTE_CONST) Rte_C_OilPrediction_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(SRS2_SNPN_T, RTE_CONST) Rte_C_SRS2_SNPN_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(SRS2_SN_T, RTE_CONST) Rte_C_SRS2_SN_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(SetParkHtrTmr_rqst_T, RTE_CONST) Rte_C_BunkH2PHTimer_rqst_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(SetParkHtrTmr_rqst_T, RTE_CONST) Rte_C_LIN_BunkH2PHTi_rqs_IV; /* PRQA S 0850 */ /* MD_MSR_19.8 */

extern CONST(VINCheckStatus_T, RTE_CONST) Rte_C_VINCheckStatus_T_0; /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
# include "Rte_DataHandleType.h"

# ifdef RTE_MICROSAR_PIM_EXPORT


/**********************************************************************************************************************
 * Calibration component and SW-C local calibration parameters
 *********************************************************************************************************************/

#  define RTE_START_SEC_CONST_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoDriverDoorLatch_Tx_VEC_CryptoProxyCycleFactor; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoDriverDoorLatch_Tx_VEC_CryptoProxyCycleOffset; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoDriverKeyCyl_Rx_VEC_CryptoProxyTimeFactorResentIdentification; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoDriverKeyCyl_Rx_VEC_CryptoProxyTimeFactorTillNewIdentificationKey; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoEngineStart_Tx_VEC_CryptoProxyCycleFactor; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoEngineStart_Tx_VEC_CryptoProxyCycleOffset; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoGearboxLock_Tx_VEC_CryptoProxyCycleFactor; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoGearboxLock_Tx_VEC_CryptoProxyCycleOffset; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoKeyAuth_Tx_VEC_CryptoProxyCycleFactor; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoKeyAuth_Tx_VEC_CryptoProxyCycleOffset; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoLockingSwitch_Rx_VEC_CryptoProxyTimeFactorResentIdentification; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoLockingSwitch_Rx_VEC_CryptoProxyTimeFactorTillNewIdentificationKey; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoLuggageCompartment_Tx_VEC_CryptoProxyCycleFactor; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoLuggageCompartment_Tx_VEC_CryptoProxyCycleOffset; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoPsngDoorLatch_Tx_VEC_CryptoProxyCycleFactor; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoPsngDoorLatch_Tx_VEC_CryptoProxyCycleOffset; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoPsngKeyCyl_Rx_VEC_CryptoProxyTimeFactorResentIdentification; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoPsngKeyCyl_Rx_VEC_CryptoProxyTimeFactorTillNewIdentificationKey; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoReducedSetMode_Tx_VEC_CryptoProxyCycleFactor; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoReducedSetMode_Tx_VEC_CryptoProxyCycleOffset; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoTheftAlarmActivation_Tx_VEC_CryptoProxyCycleFactor; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoTheftAlarmActivation_Tx_VEC_CryptoProxyCycleOffset; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_CONST_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_AuxHornMaxActivationTime_X1CYZ_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CYZ_AuxHornMaxActivationTime_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_CityHornMaxActivationTime_X1CY0_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CY0_CityHornMaxActivationTime_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_HwToleranceThreshold_X1C04_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1C04_HwToleranceThreshold_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_DoorAccessIf_X1CX3_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX3_PcbConfig_DoorAccessIf_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FSC_TimeoutThreshold_X1CZR_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CZR_FSC_TimeoutThreshold_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_KeyMatchingIndicationTimeout_X1CV3_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CV3_KeyMatchingIndicationTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Pvt_ActivateReporting_X1C14_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1C14_Pvt_ActivateReporting_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_TheftAlarmRequestDuration_X1CY8_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CY8_TheftAlarmRequestDuration_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DoorLockingFailureTimeout_X1CX9_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX9_DoorLockingFailureTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AlarmAutoRelockRequestDuration_X1CYA_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CYA_AlarmAutoRelockRequestDuration_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1C05_DtcActivationVbatEnable_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CV4_KeyMatchingReinforcedAuth_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXE_isKeyfobSecurityFuseBlowAct_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CJT_EnableCustomDemCfgCrc_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_Adi_X1CXW_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXW_PcbConfig_Adi_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_DOBHS_X1CXX_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXX_PcbConfig_DOBHS_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_DOWHS_X1CXY_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXY_PcbConfig_DOWHS_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_DOWLS_X1CXZ_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXZ_PcbConfig_DOWLS_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX0_PcbConfig_LinInterfaces_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX2_PcbConfig_CanInterfaces_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CY1_DigitalBiLevelVoltageConfig_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_AdiPullUp_X1CX5_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX5_PcbConfig_AdiPullUp_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FSC_VoltageThreshold_X1CZQ_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CZQ_FSC_VoltageThreshold_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX4_PcbConfig_PassiveAntenna_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PassengersSeatBeltVoltageLevels_X1CY2_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CY2_PassengersSeatBeltVoltageLevels_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_LNGTank1Volume_P1LX9_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LX9_LNGTank1Volume_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_LNGTank2Volume_P1LYA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LYA_LNGTank2Volume_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID001_ID009_P1EAA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAA_FS_DiagAct_ID001_ID009_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID010_ID019_P1EAB_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAB_FS_DiagAct_ID010_ID019_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID02_ID029_P1EAC_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAC_FS_DiagAct_ID02_ID029_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID030_ID039_P1EAD_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAD_FS_DiagAct_ID030_ID039_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID040_ID049_P1EAE_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAE_FS_DiagAct_ID040_ID049_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID050_ID059_P1EAF_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAF_FS_DiagAct_ID050_ID059_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID060_ID069_P1EAG_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAG_FS_DiagAct_ID060_ID069_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID070_ID079_P1EAH_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAH_FS_DiagAct_ID070_ID079_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID080_ID089_P1EAI_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAI_FS_DiagAct_ID080_ID089_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAJ_FS_DiagAct_ID090_ID099_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID100_ID109_P1EAK_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAK_FS_DiagAct_ID100_ID109_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID110_ID119_P1EAL_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAL_FS_DiagAct_ID110_ID119_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID120_ID129_P1EAM_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAM_FS_DiagAct_ID120_ID129_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID130_ID139_P1EAN_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAN_FS_DiagAct_ID130_ID139_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID140_ID149_P1EAO_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAO_FS_DiagAct_ID140_ID149_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID150_ID159_P1EAP_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAP_FS_DiagAct_ID150_ID159_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAQ_FS_DiagAct_ID160_ID169_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID170_ID179_P1EAR_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAR_FS_DiagAct_ID170_ID179_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID180_ID189_P1EAS_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAS_FS_DiagAct_ID180_ID189_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID190_ID199_P1EAT_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAT_FS_DiagAct_ID190_ID199_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID200_ID209_P1EAU_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAU_FS_DiagAct_ID200_ID209_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID210_ID219_P1EAV_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAV_FS_DiagAct_ID210_ID219_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID220_ID229_P1EAW_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAW_FS_DiagAct_ID220_ID229_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID230_ID239_P1EAX_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAX_FS_DiagAct_ID230_ID239_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID240_ID249_P1EAY_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAY_FS_DiagAct_ID240_ID249_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAZ_FS_DiagAct_ID250_ID254_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ECSStandbyActivationTimeout_P1CUA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CUA_ECSStandbyActivationTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ECSStandbyExtendedActTimeout_P1CUB_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CUB_ECSStandbyExtendedActTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ECSActiveStateTimeout_P1CUE_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CUE_ECSActiveStateTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1O8Q_DoorLockHazardIndicationRqstDelay_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AuxBBSw_TimeoutForReq_P1DV1_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DV1_AuxBBSw_TimeoutForReq_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AuxBBLoadStat_MaxInitTime_P1M93_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1M93_AuxBBLoadStat_MaxInitTime_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1KN5_AxleLoad_CRideLEDIndicationType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_LIN_topology_P1AJR_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1AJR_LIN_topology_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_HeadwaySupport_P1BEX_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BEX_HeadwaySupport_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FCW_LedLogic_P1LG1_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LG1_FCW_LedLogic_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_CM_Configuration_P1LGD_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LGD_CM_Configuration_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FCW_SwPushThreshold_P1LGE_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LGE_FCW_SwPushThreshold_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FCW_ConfirmTimeout_P1LGF_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LGF_FCW_ConfirmTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FCW_SwStuckTimeout_P1LGG_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LGG_FCW_SwStuckTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1MOT_CollSituationHMICtrlRequestVM_Time_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Diag_Act_DOWHS01_P1V6O_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6O_Diag_Act_DOWHS01_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Diag_Act_DOWHS02_P1V6P_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6P_Diag_Act_DOWHS02_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Diag_Act_DOWLS02_P1V7E_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7E_Diag_Act_DOWLS02_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Diag_Act_DOWLS03_P1V7F_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7F_Diag_Act_DOWLS03_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DriverPosition_LHD_RHD_P1ALJ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1ALJ_DriverPosition_LHD_RHD_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1IZ3_ESC_InhibitionByPrimaryPedal_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ContainerUnlockHMIDeviceType_P1CXO_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CXO_ContainerUnlockHMIDeviceType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Slidable5thWheelHMIDeviceType_P1CXP_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CXP_Slidable5thWheelHMIDeviceType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Slid5thWheelTimeoutForReq_P1DV9_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DV9_Slid5thWheelTimeoutForReq_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_TailLiftHMIDeviceType_P1CW9_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CW9_TailLiftHMIDeviceType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_CraneHMIDeviceType_P1CXA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CXA_CraneHMIDeviceType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_TailLift_Crane_Act_P1CXB_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CXB_TailLift_Crane_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_CraneSwIndicationType_P1CXC_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CXC_CraneSwIndicationType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_TailLiftTimeoutForRequest_P1DWA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWA_TailLiftTimeoutForRequest_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_WeightClassInformation_P1M7S_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1M7S_WeightClassInformation_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FuelTypeInformation_P1M7T_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1M7T_FuelTypeInformation_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_CabHeightValue_P1R0P_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1R0P_CabHeightValue_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DwmVehicleModes_P1BDU_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BDU_DwmVehicleModes_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FSPConfigSettingsLIN2_P1EW0_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EW0_FSPConfigSettingsLIN2_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FSPConfigSettingsLIN3_P1EW1_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EW1_FSPConfigSettingsLIN3_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FSPConfigSettingsLIN4_P1EW2_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EW2_FSPConfigSettingsLIN4_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FSPConfigSettingsLIN5_P1EW3_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EW3_FSPConfigSettingsLIN5_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FSPConfigSettingsLIN1_P1EWZ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EWZ_FSPConfigSettingsLIN1_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_IL_LockingCmdDelayOff_P1K7E_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1K7E_IL_LockingCmdDelayOff_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_LockFunctionHardwareInterface_P1MXZ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1MXZ_LockFunctionHardwareInterface_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_KeyfobType_P1VKL_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VKL_KeyfobType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_LKS_SwType_P1R5P_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1R5P_LKS_SwType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CUF_LoadingLevelSwStuckedTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_KneelButtonStuckedTimeout_P1DWD_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWD_KneelButtonStuckedTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FerryFuncSwStuckedTimeout_P1EXK_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EXK_FerryFuncSwStuckedTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ECS_StandbyBlinkTime_P1GCL_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1GCL_ECS_StandbyBlinkTime_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1IZ2_LoadingLevelAdjSwStuckTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FrontSuspensionType_P1JBR_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1JBR_FrontSuspensionType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LXQ_FPBRSwitchStuckedTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FPBRSwitchRequestACKTime_P1LXR_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LXR_FPBRSwitchRequestACKTime_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AxleConfiguration_P1B16_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B16_AxleConfiguration_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FrontAxleArrangement_P1CSH_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CSH_FrontAxleArrangement_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DiffLockSinglePushSwitch_LogicSelection_P1NAK_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NAK_DiffLockSinglePushSwitch_LogicSelection_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_WheelDifferentialLockPushButtonType_P1UG1_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1UG1_WheelDifferentialLockPushButtonType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_RearAxleDifflockDisregardRequestVehicleSpeed_P1WEB_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WEB_RearAxleDifflockDisregardRequestVehicleSpeed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PassengersSeatBeltInstalled_P1VQB_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VQB_PassengersSeatBeltInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PassengersSeatBeltSensorType_P1VYK_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VYK_PassengersSeatBeltSensorType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BD2_PTO_EmergencyDeactivationTimer_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PTO_EmergencyFilteringTimer_P1BD3_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BD3_PTO_EmergencyFilteringTimer_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PTO_RequestFilteringTimer_P1BD4_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BD4_PTO_RequestFilteringTimer_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_RAS_LEDFeedbackIndication_P1GCC_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1GCC_RAS_LEDFeedbackIndication_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1GCD_RoofHatch_FlexibleSwitchLogic_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ABS_Inhibit_SwType_P1SY6_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1SY6_ABS_Inhibit_SwType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWQ_SwivelSeatCheck_SetSpeed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DoorAutoLockingSpeed_P1B2Q_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2Q_DoorAutoLockingSpeed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AutoAlarmReactivationTimeout_P1B2S_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2S_AutoAlarmReactivationTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DoorLatchProtectionTimeWindow_P1DW8_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DW8_DoorLatchProtectionTimeWindow_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DoorLatchProtectionRestingTime_P1DW9_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DW9_DoorLatchProtectionRestingTime_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DoorIndicationReqDuration_P1DWP_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWP_DoorIndicationReqDuration_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DoorLatchProtectMaxOperation_P1DXA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DXA_DoorLatchProtectMaxOperation_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_SpeedRelockingReinitThreshold_P1H55_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1H55_SpeedRelockingReinitThreshold_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DashboardLedTimeout_P1IZ4_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1IZ4_DashboardLedTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PassiveEntryFunction_Type_P1VKF_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VKF_PassiveEntryFunction_Type_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ECSStopButtonHoldTimeout_P1DWI_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWI_ECSStopButtonHoldTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_RCECSButtonStucked_P1DWJ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWJ_RCECSButtonStucked_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_RCECSUpDownStucked_P1DWK_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWK_RCECSUpDownStucked_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_RCECS_HoldCircuitTimer_P1IUS_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1IUS_RCECS_HoldCircuitTimer_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1T3W_VehSSButtonInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DI0_AuxBBSw5_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DI1_AuxBbSw6_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DIW_AuxBbSw1_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DIX_AuxBbSw2_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DIY_AuxBbSw3_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DIZ_AuxBbSw4_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BOS_AxleLoad_AccessoryBoggieALD_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BOV_AxleLoad_RatioALD_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BOW_AxleLoad_ArideLiftAxle_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BOX_AxleLoad_TridemFirstAxleLift_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZ0_AxleLoad_MaxTractionTag_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZ1_AxleLoad_RatioTagOrLoadDistrib_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZ2_AxleLoad_RatioPusherRocker_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZ3_AxleLoad_RatioTagRocker_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZW_AxleLoad_OneLiftPusher_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZX_AxleLoad_OneLiftAxleMaxTraction_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZY_AxleLoad_OneLiftTag_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZZ_AxleLoad_MaxTractionPusher_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1J6B_AxleLoad_AccessoryTridemALD_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1J6C_AxleLoad_BoggieDualRatio_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1J6D_AxleLoad_TridemDualRatio_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1KN2_AxleLoad_CRideLiftAxle_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1KN4_AxleLoad_CRideLEDlowerEnd_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1M5B_AxleLoad_RatioRoadGripPusher_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CAQ_LECML_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2G_LECMH_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NT1_CM_DeviceType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DXX_FMSgateway_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8I_BB2_active_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8J_BB1_active_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8K_CabSubnet_active_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8L_SecuritySubnet_active_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8M_CAN6_active_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6I_Diag_Act_AO12_P_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6K_Diag_Act_AO12_L_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6Q_Diag_Act_DOBHS01_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6R_Diag_Act_DOBHS02_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6S_Diag_Act_DOBHS03_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6T_Diag_Act_DOBHS04_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7D_Diag_Act_DOBLS01_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7G_Diag_Act_DAI01_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7H_Diag_Act_DAI02_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8E_Diag_Act_12VDCDC_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B04_DLFW_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VKI_PassiveStart_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VQ0_FuelEcoOffButton_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B0X_EngineSpeedControlSw_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VR3_ELCP1_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VR5_ELCP2_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LX8_EngineFuelTypeLNG_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1M7Q_EraGlonassUnitInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BWZ_DoubleRoofHatchSwConfig_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1SDA_OptitrackSystemInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VR1_ILCP1_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VR2_ILCP2_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WME_LowPowerPullUpAct_Parked_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMF_LowPowerPullUpAct_Living_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMN_LowPower12VOutputAct_Living_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMO_LowPower12VOutputAct_Parked_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2U_KeyfobPresent_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1Y1C_SuperlockInhibition_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BKI_LKS_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NQD_LKS_SwIndicationType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1A12_ADL_Sw_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CT4_FerrySw_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CT9_LoadingLevelSw_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EXH_KneelingSwitchInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1IZ1_LoadingLevelAdjSwitchInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LXP_FPBRSwitchInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B03_RearWheelDiffLockPushSw_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CUC_ConstructionSw_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1F7J_ASROffRoadFullVersion_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1FNW_ASROffButtonInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NQA_AutomaticFrontWheelDrive_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1GBT_RAS_SwitchInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1RRH_RAS_ToggleButton_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BXH_ReverseWarning_SwType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CW8_RoofHatchCtrl_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1A1R_HSA_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NTV_HSA_DefaultConfig_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1SY4_ABS_Inhibit_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2C_CCFW_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EXP_PersonalSettingsForCC_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2T_AlarmInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NE9_KeyInsertDetection_Enabled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NQE_LockModeHandling_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_KeyfobEncryptCode_P1DS4_a_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DS4_KeyfobEncryptCode_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AdiWakeUpConfig_P1WMD_a_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMD_AdiWakeUpConfig_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Diag_Act_LF_P_P1V79_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V79_Diag_Act_LF_P_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_VINCheckProcessing_P1VKG_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VKG_VINCheckProcessing_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_IL_ShortLongPushThresholds_P1DKF_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DKF_IL_ShortLongPushThresholds_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1QR6_HWIO_CfgFault_PWR24V_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI07_P1V60_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V60_Fault_Config_ADI07_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI08_P1V61_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V61_Fault_Config_ADI08_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI09_P1V62_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V62_Fault_Config_ADI09_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI10_P1V63_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V63_Fault_Config_ADI10_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI11_P1V64_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V64_Fault_Config_ADI11_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI12_P1V65_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V65_Fault_Config_ADI12_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI13_P1V66_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V66_Fault_Config_ADI13_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI14_P1V67_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V67_Fault_Config_ADI14_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI15_P1V68_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V68_Fault_Config_ADI15_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI16_P1V69_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V69_Fault_Config_ADI16_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI01_P1V6U_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6U_Fault_Config_ADI01_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI02_P1V6V_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6V_Fault_Config_ADI02_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI03_P1V6W_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6W_Fault_Config_ADI03_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI04_P1V6X_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6X_Fault_Config_ADI04_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI05_P1V6Y_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6Y_Fault_Config_ADI05_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI06_P1V6Z_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6Z_Fault_Config_ADI06_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8F_Fault_Cfg_DcDc12v_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DAI_Installed_P1WMP_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMP_DAI_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ECS_MemSwTimings_P1BWF_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BWF_ECS_MemSwTimings_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ForcedPositionStatus_Front_P1JSY_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1JSY_ForcedPositionStatus_Front_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1JSZ_ForcedPositionStatus_Rear_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_AuxBbSw1_Logic_P1DI2_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI2_AuxBbSw1_Logic_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AuxBbSw2_Logic_P1DI3_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI3_AuxBbSw2_Logic_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AuxBbSw3_Logic_P1DI4_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI4_AuxBbSw3_Logic_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AuxBbSw4_Logic_P1DI5_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI5_AuxBbSw4_Logic_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AuxBbSw5_Logic_P1DI6_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI6_AuxBbSw5_Logic_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AuxBbSw6_Logic_P1DI7_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI7_AuxBbSw6_Logic_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_BodybuilderAccessToAccelPedal_P1B72_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1B72_BodybuilderAccessToAccelPedal_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_IL_CtrlDeviceTypeFront_P1DKH_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DKH_IL_CtrlDeviceTypeFront_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DKI_IL_CtrlDeviceTypeBunk_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1B9X_WirelessRC_Enable_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1BOY_AxleLoad_TridemSecondAxleLift_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1BWE_CabTiltEnable_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AW6_IsEcuAvailableDDM_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AWY_IsEcuAvailableAlarm_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AXE_IsEcuAvailablePDM_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1ALT_ECS_PartialAirSystem_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1ALU_ECS_FullAirSystem_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1B0W_CrossCountryCC_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1A1T_CityHorn_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1A1U_AuxiliaryHorn_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1A1V_HornLivingMode_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AJL_PTO1_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AJM_PTO2_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1BBG_PTO3_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1BBH_PTO4_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AJJ_ReverseWarning_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1CUD_SwivelSeatWarning_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_CONST_SA_lvl_0x2D_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2D) Rte_AddrPar_0x2D_P1TTA_GearBoxLockActivation_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AntMappingConfig_Gain_X1C03_a_T, RTE_CONST_SA_lvl_0x2D) Rte_AddrPar_0x2D_X1C03_AntMappingConfig_Gain_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AntMappingConfig_Multi_X1CY3_a_T, RTE_CONST_SA_lvl_0x2D) Rte_AddrPar_0x2D_X1CY3_AntMappingConfig_Multi_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AntMappingConfig_Single_X1CY5_s_T, RTE_CONST_SA_lvl_0x2D) Rte_AddrPar_0x2D_X1CY5_AntMappingConfig_Single_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_CONST_SA_lvl_0x2D_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_CONST_SA_lvl_0x2D_and_0x37_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_CrankingLockActivation_P1DS3_T, RTE_CONST_SA_lvl_0x2D_and_0x37) Rte_AddrPar_0x2D_and_0x37_P1DS3_CrankingLockActivation_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_CONST_SA_lvl_0x2D_and_0x37_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_KeyfobDetectionMappingSelection_P1WIP_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1WIP_KeyfobDetectionMappingSelection_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_KeyfobDetectionMappingConfig_P1WIR_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1WIR_KeyfobDetectionMappingConfig_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1C54_FactoryModeActive_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1WPP_isSecurityLinActive_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ComCryptoKey_P1DLX_a_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1DLX_ComCryptoKey_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ChassisId_CHANO_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_CHANO_ChassisId_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_VIN_VINNO_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_VINNO_VIN_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AuxPassiveAntennasActivation_P1WIQ_s_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1WIQ_AuxPassiveAntennasActivation_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(UInt16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CryptoDriverDoorLatch_Tx_VEC_CryptoProxy_CycleTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(UInt16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CryptoDriverKeyCyl_Rx_VEC_CryptoProxy_DelayTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(UInt16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CryptoDriverKeyCyl_Rx_VEC_CryptoProxy_ResentTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(UInt16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CryptoEngineStart_Tx_VEC_CryptoProxy_CycleTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(UInt16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CryptoGearboxLock_Tx_VEC_CryptoProxy_CycleTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(UInt16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CryptoKeyAuth_Tx_VEC_CryptoProxy_CycleTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(UInt16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CryptoLockingSwitch_Rx_VEC_CryptoProxy_DelayTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(UInt16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CryptoLockingSwitch_Rx_VEC_CryptoProxy_ResentTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(UInt16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CryptoLuggageCompartment_Tx_VEC_CryptoProxy_CycleTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(UInt16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CryptoPsngDoorLatch_Tx_VEC_CryptoProxy_CycleTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(UInt16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CryptoPsngKeyCyl_Rx_VEC_CryptoProxy_DelayTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(UInt16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CryptoPsngKeyCyl_Rx_VEC_CryptoProxy_ResentTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(UInt16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CryptoReducedSetMode_Tx_VEC_CryptoProxy_CycleTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(UInt16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CryptoTheftAlarmActivation_Tx_VEC_CryptoProxy_CycleTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(uint8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_FlexibleSwitchesRouter_Ctrl_Pim_P1DCT_Info; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FlexibleSwDisableDiagPresence_Type, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_FlexibleSwitchesRouter_Ctrl_Pim_FlexibleSwDisableDiagPresence; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FlexibleSwitchesinFailure_Type, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_FlexibleSwitchesRouter_Ctrl_Pim_FlexibleSwFailureData; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FSP_Array10_8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_FlexibleSwitchesRouter_Ctrl_Pim_FlexibleSwStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FSP_Array10_8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_FlexibleSwitchesRouter_Ctrl_Pim_TableOfDetectedId; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# endif


/**********************************************************************************************************************
 * Component Data Structures and Port Data Structures
 *********************************************************************************************************************/

# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
typedef P2CONST(SEWS_ComCryptoKey_P1DLX_T, AUTOMATIC, RTE_CONST_SA_lvl_0x2F) Rte_VEC_CryptoProxySenderSwc_ComCryptoKey_P1DLX_R_Calprm_v_FncRetPtrType;
# else
typedef P2CONST(SEWS_ComCryptoKey_P1DLX_a_T, AUTOMATIC, RTE_CONST_SA_lvl_0x2F) Rte_VEC_CryptoProxySenderSwc_ComCryptoKey_P1DLX_R_Calprm_v_FncRetPtrType;
# endif

struct Rte_PDS_VEC_CryptoProxySenderSwc_ComCryptoKey_P1DLX_R
{
  P2FUNC(Rte_VEC_CryptoProxySenderSwc_ComCryptoKey_P1DLX_R_Calprm_v_FncRetPtrType, RTE_CODE, Prm_v) (void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
};

struct Rte_PDS_VEC_CryptoProxySenderSwc_CryptoIdKey_I_R
{
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Receive_CryptoIdKey) (P2VAR(uint8, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR)); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Receive_CryptoIdKey) (P2VAR(CryptoIdKey_T, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR)); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
};

struct Rte_PDS_VEC_CryptoProxySenderSwc_CryptoTrigger_I_R
{
  P2FUNC(Std_ReturnType, RTE_CODE, Read_CryptoTrigger) (P2VAR(Boolean, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR)); /* PRQA S 0850 */ /* MD_MSR_19.8 */
};

struct Rte_PDS_VEC_CryptoProxySenderSwc_CsmSymDecrypt_R
{
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymDecryptFinish) (P2VAR(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR) plainTextBuffer, P2VAR(UInt32_Length, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR) plainTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymDecryptFinish) (P2VAR(SymDecryptResultBuffer, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR) plainTextBuffer, P2VAR(UInt32_Length, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR) plainTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymDecryptStart) (P2CONST(SymKeyType, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_DATA) key, P2CONST(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_DATA) InitVectorBuffer, UInt32_Length InitVectorLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymDecryptStart) (P2CONST(SymKeyType, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_DATA) key, P2CONST(SymDecryptDataBuffer, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_DATA) InitVectorBuffer, UInt32_Length InitVectorLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymDecryptUpdate) (P2CONST(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_DATA) cipherTextBuffer, UInt32_Length cipherTextLength, P2VAR(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR) plainTextBuffer, P2VAR(UInt32_Length, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR) plainTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymDecryptUpdate) (P2CONST(SymDecryptDataBuffer, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_DATA) cipherTextBuffer, UInt32_Length cipherTextLength, P2VAR(SymDecryptResultBuffer, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR) plainTextBuffer, P2VAR(UInt32_Length, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR) plainTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
};

struct Rte_PDS_VEC_CryptoProxySenderSwc_CsmSymEncrypt_R
{
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymEncryptFinish) (P2VAR(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR) cipherTextBuffer, P2VAR(UInt32_Length, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR) cipherTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymEncryptFinish) (P2VAR(SymEncryptResultBuffer, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR) cipherTextBuffer, P2VAR(UInt32_Length, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR) cipherTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymEncryptStart) (P2CONST(SymKeyType, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_DATA) key, P2CONST(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_DATA) InitVectorBuffer, UInt32_Length InitVectorLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymEncryptStart) (P2CONST(SymKeyType, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_DATA) key, P2CONST(SymEncryptDataBuffer, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_DATA) InitVectorBuffer, UInt32_Length InitVectorLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymEncryptUpdate) (P2CONST(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_DATA) plainTextBuffer, UInt32_Length plainTextLength, P2VAR(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR) cipherTextBuffer, P2VAR(UInt32_Length, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR) cipherTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymEncryptUpdate) (P2CONST(SymEncryptDataBuffer, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_DATA) plainTextBuffer, UInt32_Length plainTextLength, P2VAR(SymEncryptResultBuffer, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR) cipherTextBuffer, P2VAR(UInt32_Length, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR) cipherTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
};

struct Rte_PDS_VEC_CryptoProxySenderSwc_EncryptedSignal_I_P
{
  P2FUNC(Std_ReturnType, RTE_CODE, Feedback_EncryptedSignal) (void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Send_EncryptedSignal) (P2CONST(uint8, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_DATA)); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Send_EncryptedSignal) (P2CONST(Encrypted128bit_T, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_DATA)); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
};

struct Rte_PDS_VEC_CryptoProxySenderSwc_VEC_CryptoProxySerializedData_R
{
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Read_Crypto_Function_serialized) (P2VAR(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR)); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Read_Crypto_Function_serialized) (P2VAR(VEC_CryptoProxy_UserSignal, AUTOMATIC, RTE_VEC_CRYPTOPROXYSENDERSWC_APPL_VAR)); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
};

/* Type for IRV API section */
typedef P2FUNC(UInt32, RTE_CODE, Rte_IrvRead_VEC_CryptoProxySenderSwc_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber_FncPtrType)(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

/* Type for IRV API section */
typedef P2FUNC(UInt32, RTE_CODE, Rte_IrvRead_VEC_CryptoProxySenderSwc_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber_FncPtrType)(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

/* Type for IRV API section */
typedef P2FUNC(UInt32, RTE_CODE, Rte_IrvRead_VEC_CryptoProxySenderSwc_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber_FncPtrType)(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

/* Type for IRV API section */
typedef P2FUNC(void, RTE_CODE, Rte_IrvWrite_VEC_CryptoProxySenderSwc_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber_FncPtrType)(UInt32 data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

/* Type for IRV API section */
typedef P2FUNC(void, RTE_CODE, Rte_IrvWrite_VEC_CryptoProxySenderSwc_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber_FncPtrType)(UInt32 data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

/* Type for IRV API section */
typedef P2FUNC(void, RTE_CODE, Rte_IrvWrite_VEC_CryptoProxySenderSwc_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber_FncPtrType)(UInt32 data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

struct Rte_CDS_VEC_CryptoProxySenderSwc
{
  /* PIM Handles section */
  P2VAR(UInt16, TYPEDEF, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Pim_VEC_CryptoProxy_CycleTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
  /* CalPrm Handles section */
  P2FUNC(UInt16, RTE_CODE, CData_VEC_CryptoProxyCycleFactor) (void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
  P2FUNC(UInt16, RTE_CODE, CData_VEC_CryptoProxyCycleOffset) (void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
  /* Port API section */
  struct Rte_PDS_VEC_CryptoProxySenderSwc_ComCryptoKey_P1DLX_R ComCryptoKey_P1DLX;
  struct Rte_PDS_VEC_CryptoProxySenderSwc_CryptoIdKey_I_R VEC_CryptoIdKey;
  struct Rte_PDS_VEC_CryptoProxySenderSwc_CryptoTrigger_I_R CryptoTrigger;
  struct Rte_PDS_VEC_CryptoProxySenderSwc_CsmSymDecrypt_R CsmSymDecrypt;
  struct Rte_PDS_VEC_CryptoProxySenderSwc_CsmSymEncrypt_R CsmSymEncrypt;
  struct Rte_PDS_VEC_CryptoProxySenderSwc_EncryptedSignal_I_P VEC_EncryptedSignal;
  struct Rte_PDS_VEC_CryptoProxySenderSwc_VEC_CryptoProxySerializedData_R VEC_CryptoProxySerializedData;
  /* IRV API section */
  Rte_IrvRead_VEC_CryptoProxySenderSwc_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber_FncPtrType IrvRead_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber;
  Rte_IrvRead_VEC_CryptoProxySenderSwc_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber_FncPtrType IrvRead_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber;
  Rte_IrvRead_VEC_CryptoProxySenderSwc_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber_FncPtrType IrvRead_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber;
  Rte_IrvWrite_VEC_CryptoProxySenderSwc_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber_FncPtrType IrvWrite_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber;
  Rte_IrvWrite_VEC_CryptoProxySenderSwc_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber_FncPtrType IrvWrite_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber;
  Rte_IrvWrite_VEC_CryptoProxySenderSwc_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber_FncPtrType IrvWrite_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber;
  /* Instance Id section */
  uint8 Instance_Id;
  /* Vendor specific section */
};

# define RTE_START_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, RTE_CONST) Rte_Instance_CryptoDriverDoorLatch_Tx; /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
typedef P2CONST(SEWS_ComCryptoKey_P1DLX_T, AUTOMATIC, RTE_CONST_SA_lvl_0x2F) Rte_VEC_CryptoProxyReceiverSwc_ComCryptoKey_P1DLX_R_Calprm_v_FncRetPtrType;
# else
typedef P2CONST(SEWS_ComCryptoKey_P1DLX_a_T, AUTOMATIC, RTE_CONST_SA_lvl_0x2F) Rte_VEC_CryptoProxyReceiverSwc_ComCryptoKey_P1DLX_R_Calprm_v_FncRetPtrType;
# endif

struct Rte_PDS_VEC_CryptoProxyReceiverSwc_ComCryptoKey_P1DLX_R
{
  P2FUNC(Rte_VEC_CryptoProxyReceiverSwc_ComCryptoKey_P1DLX_R_Calprm_v_FncRetPtrType, RTE_CODE, Prm_v) (void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
};

struct Rte_PDS_VEC_CryptoProxyReceiverSwc_CryptoIdKey_I_P
{
  P2FUNC(Std_ReturnType, RTE_CODE, Feedback_CryptoIdKey) (void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Send_CryptoIdKey) (P2CONST(uint8, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_DATA)); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Send_CryptoIdKey) (P2CONST(CryptoIdKey_T, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_DATA)); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
};

struct Rte_PDS_VEC_CryptoProxyReceiverSwc_CsmSymDecrypt_R
{
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymDecryptFinish) (P2VAR(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) plainTextBuffer, P2VAR(UInt32_Length, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) plainTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymDecryptFinish) (P2VAR(SymDecryptResultBuffer, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) plainTextBuffer, P2VAR(UInt32_Length, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) plainTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymDecryptStart) (P2CONST(SymKeyType, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_DATA) key, P2CONST(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_DATA) InitVectorBuffer, UInt32_Length InitVectorLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymDecryptStart) (P2CONST(SymKeyType, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_DATA) key, P2CONST(SymDecryptDataBuffer, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_DATA) InitVectorBuffer, UInt32_Length InitVectorLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymDecryptUpdate) (P2CONST(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_DATA) cipherTextBuffer, UInt32_Length cipherTextLength, P2VAR(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) plainTextBuffer, P2VAR(UInt32_Length, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) plainTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymDecryptUpdate) (P2CONST(SymDecryptDataBuffer, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_DATA) cipherTextBuffer, UInt32_Length cipherTextLength, P2VAR(SymDecryptResultBuffer, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) plainTextBuffer, P2VAR(UInt32_Length, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) plainTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
};

struct Rte_PDS_VEC_CryptoProxyReceiverSwc_CsmSymEncrypt_R
{
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymEncryptFinish) (P2VAR(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) cipherTextBuffer, P2VAR(UInt32_Length, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) cipherTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymEncryptFinish) (P2VAR(SymEncryptResultBuffer, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) cipherTextBuffer, P2VAR(UInt32_Length, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) cipherTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymEncryptStart) (P2CONST(SymKeyType, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_DATA) key, P2CONST(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_DATA) InitVectorBuffer, UInt32_Length InitVectorLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymEncryptStart) (P2CONST(SymKeyType, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_DATA) key, P2CONST(SymEncryptDataBuffer, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_DATA) InitVectorBuffer, UInt32_Length InitVectorLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymEncryptUpdate) (P2CONST(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_DATA) plainTextBuffer, UInt32_Length plainTextLength, P2VAR(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) cipherTextBuffer, P2VAR(UInt32_Length, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) cipherTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Call_SymEncryptUpdate) (P2CONST(SymEncryptDataBuffer, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_DATA) plainTextBuffer, UInt32_Length plainTextLength, P2VAR(SymEncryptResultBuffer, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) cipherTextBuffer, P2VAR(UInt32_Length, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) cipherTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
};

struct Rte_PDS_VEC_CryptoProxyReceiverSwc_EncryptedSignal_I_R
{
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Receive_EncryptedSignal) (P2VAR(uint8, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR)); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Receive_EncryptedSignal) (P2VAR(Encrypted128bit_T, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR)); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
};

struct Rte_PDS_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxySerializedData_P
{
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
  P2FUNC(Std_ReturnType, RTE_CODE, Write_Crypto_Function_serialized) (P2CONST(UInt8, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_DATA)); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
  P2FUNC(Std_ReturnType, RTE_CODE, Write_Crypto_Function_serialized) (P2CONST(VEC_CryptoProxy_UserSignal, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_DATA)); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
};

struct Rte_PDS_VEC_CryptoProxyReceiverSwc_VEC_IdentificationKeyInit_R
{
  P2FUNC(Std_ReturnType, RTE_CODE, Call_IdentificationKeyInit) (P2VAR(UInt32, AUTOMATIC, RTE_VEC_CRYPTOPROXYRECEIVERSWC_APPL_VAR) IdentificationKey); /* PRQA S 0850 */ /* MD_MSR_19.8 */
};

/* Type for IRV API section */
typedef P2FUNC(UInt32, RTE_CODE, Rte_IrvRead_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber_FncPtrType)(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

/* Type for IRV API section */
typedef P2FUNC(VEC_CryptoProxy_IdentificationState_Type, RTE_CODE, Rte_IrvRead_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState_FncPtrType)(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

/* Type for IRV API section */
typedef P2FUNC(UInt32, RTE_CODE, Rte_IrvRead_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber_FncPtrType)(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

/* Type for IRV API section */
typedef P2FUNC(VEC_CryptoProxy_IdentificationState_Type, RTE_CODE, Rte_IrvRead_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState_FncPtrType)(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

/* Type for IRV API section */
typedef P2FUNC(UInt32, RTE_CODE, Rte_IrvRead_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_NewReceivedIdentificationNumber_FncPtrType)(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

/* Type for IRV API section */
typedef P2FUNC(void, RTE_CODE, Rte_IrvWrite_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber_FncPtrType)(UInt32 data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

/* Type for IRV API section */
typedef P2FUNC(void, RTE_CODE, Rte_IrvWrite_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState_FncPtrType)(VEC_CryptoProxy_IdentificationState_Type data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

/* Type for IRV API section */
typedef P2FUNC(void, RTE_CODE, Rte_IrvWrite_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_NewReceivedIdentificationNumber_FncPtrType)(UInt32 data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

/* Type for IRV API section */
typedef P2FUNC(void, RTE_CODE, Rte_IrvWrite_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber_FncPtrType)(UInt32 data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

/* Type for IRV API section */
typedef P2FUNC(void, RTE_CODE, Rte_IrvWrite_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState_FncPtrType)(VEC_CryptoProxy_IdentificationState_Type data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

struct Rte_CDS_VEC_CryptoProxyReceiverSwc
{
  /* PIM Handles section */
  P2VAR(UInt16, TYPEDEF, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Pim_VEC_CryptoProxy_DelayTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
  P2VAR(UInt16, TYPEDEF, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Pim_VEC_CryptoProxy_ResentTimer; /* PRQA S 0850 */ /* MD_MSR_19.8 */
  /* CalPrm Handles section */
  P2FUNC(UInt16, RTE_CODE, CData_VEC_CryptoProxyTimeFactorResentIdentification) (void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
  P2FUNC(UInt16, RTE_CODE, CData_VEC_CryptoProxyTimeFactorTillNewIdentificationKey) (void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
  /* Port API section */
  struct Rte_PDS_VEC_CryptoProxyReceiverSwc_ComCryptoKey_P1DLX_R ComCryptoKey_P1DLX;
  struct Rte_PDS_VEC_CryptoProxyReceiverSwc_CryptoIdKey_I_P VEC_CryptoIdKey;
  struct Rte_PDS_VEC_CryptoProxyReceiverSwc_CsmSymDecrypt_R CsmSymDecrypt;
  struct Rte_PDS_VEC_CryptoProxyReceiverSwc_CsmSymEncrypt_R CsmSymEncrypt;
  struct Rte_PDS_VEC_CryptoProxyReceiverSwc_EncryptedSignal_I_R VEC_EncryptedSignal;
  struct Rte_PDS_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxySerializedData_P VEC_CryptoProxySerializedData;
  struct Rte_PDS_VEC_CryptoProxyReceiverSwc_VEC_IdentificationKeyInit_R VEC_IdentificationKeyInit;
  /* IRV API section */
  Rte_IrvRead_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber_FncPtrType IrvRead_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber;
  Rte_IrvRead_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState_FncPtrType IrvRead_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState;
  Rte_IrvRead_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber_FncPtrType IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber;
  Rte_IrvRead_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState_FncPtrType IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState;
  Rte_IrvRead_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_NewReceivedIdentificationNumber_FncPtrType IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_NewReceivedIdentificationNumber;
  Rte_IrvWrite_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber_FncPtrType IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber;
  Rte_IrvWrite_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState_FncPtrType IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState;
  Rte_IrvWrite_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_NewReceivedIdentificationNumber_FncPtrType IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_NewReceivedIdentificationNumber;
  Rte_IrvWrite_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber_FncPtrType IrvWrite_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber;
  Rte_IrvWrite_VEC_CryptoProxyReceiverSwc_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState_FncPtrType IrvWrite_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState;
  /* Instance Id section */
  uint8 Instance_Id;
  /* Vendor specific section */
};

# define RTE_START_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, RTE_CONST) Rte_Instance_CryptoDriverKeyCyl_Rx; /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# define RTE_START_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, RTE_CONST) Rte_Instance_CryptoEngineStart_Tx; /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# define RTE_START_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, RTE_CONST) Rte_Instance_CryptoGearboxLock_Tx; /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# define RTE_START_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, RTE_CONST) Rte_Instance_CryptoKeyAuth_Tx; /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# define RTE_START_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, RTE_CONST) Rte_Instance_CryptoLockingSwitch_Rx; /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# define RTE_START_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, RTE_CONST) Rte_Instance_CryptoLuggageCompartment_Tx; /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# define RTE_START_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, RTE_CONST) Rte_Instance_CryptoPsngDoorLatch_Tx; /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# define RTE_START_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, RTE_CONST) Rte_Instance_CryptoPsngKeyCyl_Rx; /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# define RTE_START_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, RTE_CONST) Rte_Instance_CryptoReducedSetMode_Tx; /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# define RTE_START_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, RTE_CONST) Rte_Instance_CryptoTheftAlarmActivation_Tx; /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

struct Rte_CDS_PanicFunction_Ctrl
{
  /* dummy entry */
  uint8 _dummy;
};

struct Rte_CDS_SideReverseLight_HMICtrl
{
  /* dummy entry */
  uint8 _dummy;
};

struct Rte_CDS_StaticCornerLight_HMICtrl
{
  /* dummy entry */
  uint8 _dummy;
};

struct Rte_CDS_TemporaryRSL_HMICtrl
{
  /* dummy entry */
  uint8 _dummy;
};

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

typedef unsigned int Rte_BitType;
/**********************************************************************************************************************
 * type and extern declarations of RTE internal variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Rte Init State Variable
 *********************************************************************************************************************/

# define RTE_STATE_UNINIT    (0U)
# define RTE_STATE_SCHM_INIT (1U)
# define RTE_STATE_INIT      (2U)

# ifdef RTE_CORE

/**********************************************************************************************************************
 * Calibration Parameters (SW-C local and calibration component calibration parameters)
 *********************************************************************************************************************/

#  define RTE_START_SEC_CONST_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoDriverDoorLatch_Tx_VEC_CryptoProxyCycleFactor; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoDriverDoorLatch_Tx_VEC_CryptoProxyCycleOffset; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoDriverKeyCyl_Rx_VEC_CryptoProxyTimeFactorResentIdentification; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoDriverKeyCyl_Rx_VEC_CryptoProxyTimeFactorTillNewIdentificationKey; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoEngineStart_Tx_VEC_CryptoProxyCycleFactor; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoEngineStart_Tx_VEC_CryptoProxyCycleOffset; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoGearboxLock_Tx_VEC_CryptoProxyCycleFactor; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoGearboxLock_Tx_VEC_CryptoProxyCycleOffset; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoKeyAuth_Tx_VEC_CryptoProxyCycleFactor; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoKeyAuth_Tx_VEC_CryptoProxyCycleOffset; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoLockingSwitch_Rx_VEC_CryptoProxyTimeFactorResentIdentification; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoLockingSwitch_Rx_VEC_CryptoProxyTimeFactorTillNewIdentificationKey; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoLuggageCompartment_Tx_VEC_CryptoProxyCycleFactor; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoLuggageCompartment_Tx_VEC_CryptoProxyCycleOffset; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoPsngDoorLatch_Tx_VEC_CryptoProxyCycleFactor; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoPsngDoorLatch_Tx_VEC_CryptoProxyCycleOffset; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoPsngKeyCyl_Rx_VEC_CryptoProxyTimeFactorResentIdentification; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoPsngKeyCyl_Rx_VEC_CryptoProxyTimeFactorTillNewIdentificationKey; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoReducedSetMode_Tx_VEC_CryptoProxyCycleFactor; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoReducedSetMode_Tx_VEC_CryptoProxyCycleOffset; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoTheftAlarmActivation_Tx_VEC_CryptoProxyCycleFactor; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern volatile CONST(UInt16, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CryptoTheftAlarmActivation_Tx_VEC_CryptoProxyCycleOffset; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */

#  define RTE_STOP_SEC_CONST_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_AuxHornMaxActivationTime_X1CYZ_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CYZ_AuxHornMaxActivationTime_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_CityHornMaxActivationTime_X1CY0_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CY0_CityHornMaxActivationTime_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_HwToleranceThreshold_X1C04_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1C04_HwToleranceThreshold_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_PcbConfig_DoorAccessIf_X1CX3_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX3_PcbConfig_DoorAccessIf_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FSC_TimeoutThreshold_X1CZR_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CZR_FSC_TimeoutThreshold_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_KeyMatchingIndicationTimeout_X1CV3_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CV3_KeyMatchingIndicationTimeout_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Pvt_ActivateReporting_X1C14_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1C14_Pvt_ActivateReporting_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_TheftAlarmRequestDuration_X1CY8_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CY8_TheftAlarmRequestDuration_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_DoorLockingFailureTimeout_X1CX9_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX9_DoorLockingFailureTimeout_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_AlarmAutoRelockRequestDuration_X1CYA_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CYA_AlarmAutoRelockRequestDuration_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1C05_DtcActivationVbatEnable_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CV4_KeyMatchingReinforcedAuth_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXE_isKeyfobSecurityFuseBlowAct_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CJT_EnableCustomDemCfgCrc_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_PcbConfig_Adi_X1CXW_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXW_PcbConfig_Adi_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_PcbConfig_DOBHS_X1CXX_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXX_PcbConfig_DOBHS_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_PcbConfig_DOWHS_X1CXY_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXY_PcbConfig_DOWHS_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_PcbConfig_DOWLS_X1CXZ_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXZ_PcbConfig_DOWLS_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX0_PcbConfig_LinInterfaces_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX2_PcbConfig_CanInterfaces_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CY1_DigitalBiLevelVoltageConfig_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_PcbConfig_AdiPullUp_X1CX5_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX5_PcbConfig_AdiPullUp_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FSC_VoltageThreshold_X1CZQ_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CZQ_FSC_VoltageThreshold_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX4_PcbConfig_PassiveAntenna_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_PassengersSeatBeltVoltageLevels_X1CY2_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CY2_PassengersSeatBeltVoltageLevels_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */

#  define RTE_STOP_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_LNGTank1Volume_P1LX9_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LX9_LNGTank1Volume_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_LNGTank2Volume_P1LYA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LYA_LNGTank2Volume_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID001_ID009_P1EAA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAA_FS_DiagAct_ID001_ID009_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID010_ID019_P1EAB_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAB_FS_DiagAct_ID010_ID019_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID02_ID029_P1EAC_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAC_FS_DiagAct_ID02_ID029_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID030_ID039_P1EAD_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAD_FS_DiagAct_ID030_ID039_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID040_ID049_P1EAE_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAE_FS_DiagAct_ID040_ID049_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID050_ID059_P1EAF_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAF_FS_DiagAct_ID050_ID059_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID060_ID069_P1EAG_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAG_FS_DiagAct_ID060_ID069_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID070_ID079_P1EAH_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAH_FS_DiagAct_ID070_ID079_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID080_ID089_P1EAI_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAI_FS_DiagAct_ID080_ID089_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAJ_FS_DiagAct_ID090_ID099_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID100_ID109_P1EAK_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAK_FS_DiagAct_ID100_ID109_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID110_ID119_P1EAL_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAL_FS_DiagAct_ID110_ID119_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID120_ID129_P1EAM_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAM_FS_DiagAct_ID120_ID129_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID130_ID139_P1EAN_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAN_FS_DiagAct_ID130_ID139_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID140_ID149_P1EAO_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAO_FS_DiagAct_ID140_ID149_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID150_ID159_P1EAP_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAP_FS_DiagAct_ID150_ID159_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAQ_FS_DiagAct_ID160_ID169_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID170_ID179_P1EAR_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAR_FS_DiagAct_ID170_ID179_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID180_ID189_P1EAS_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAS_FS_DiagAct_ID180_ID189_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID190_ID199_P1EAT_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAT_FS_DiagAct_ID190_ID199_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID200_ID209_P1EAU_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAU_FS_DiagAct_ID200_ID209_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID210_ID219_P1EAV_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAV_FS_DiagAct_ID210_ID219_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID220_ID229_P1EAW_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAW_FS_DiagAct_ID220_ID229_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID230_ID239_P1EAX_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAX_FS_DiagAct_ID230_ID239_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID240_ID249_P1EAY_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAY_FS_DiagAct_ID240_ID249_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAZ_FS_DiagAct_ID250_ID254_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_ECSStandbyActivationTimeout_P1CUA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CUA_ECSStandbyActivationTimeout_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_ECSStandbyExtendedActTimeout_P1CUB_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CUB_ECSStandbyExtendedActTimeout_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_ECSActiveStateTimeout_P1CUE_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CUE_ECSActiveStateTimeout_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1O8Q_DoorLockHazardIndicationRqstDelay_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_AuxBBSw_TimeoutForReq_P1DV1_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DV1_AuxBBSw_TimeoutForReq_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_AuxBBLoadStat_MaxInitTime_P1M93_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1M93_AuxBBLoadStat_MaxInitTime_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1KN5_AxleLoad_CRideLEDIndicationType_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_LIN_topology_P1AJR_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1AJR_LIN_topology_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_HeadwaySupport_P1BEX_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BEX_HeadwaySupport_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FCW_LedLogic_P1LG1_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LG1_FCW_LedLogic_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_CM_Configuration_P1LGD_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LGD_CM_Configuration_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FCW_SwPushThreshold_P1LGE_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LGE_FCW_SwPushThreshold_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FCW_ConfirmTimeout_P1LGF_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LGF_FCW_ConfirmTimeout_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FCW_SwStuckTimeout_P1LGG_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LGG_FCW_SwStuckTimeout_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1MOT_CollSituationHMICtrlRequestVM_Time_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Diag_Act_DOWHS01_P1V6O_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6O_Diag_Act_DOWHS01_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Diag_Act_DOWHS02_P1V6P_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6P_Diag_Act_DOWHS02_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Diag_Act_DOWLS02_P1V7E_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7E_Diag_Act_DOWLS02_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Diag_Act_DOWLS03_P1V7F_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7F_Diag_Act_DOWLS03_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_DriverPosition_LHD_RHD_P1ALJ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1ALJ_DriverPosition_LHD_RHD_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1IZ3_ESC_InhibitionByPrimaryPedal_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_ContainerUnlockHMIDeviceType_P1CXO_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CXO_ContainerUnlockHMIDeviceType_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Slidable5thWheelHMIDeviceType_P1CXP_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CXP_Slidable5thWheelHMIDeviceType_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Slid5thWheelTimeoutForReq_P1DV9_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DV9_Slid5thWheelTimeoutForReq_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_TailLiftHMIDeviceType_P1CW9_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CW9_TailLiftHMIDeviceType_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_CraneHMIDeviceType_P1CXA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CXA_CraneHMIDeviceType_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_TailLift_Crane_Act_P1CXB_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CXB_TailLift_Crane_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_CraneSwIndicationType_P1CXC_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CXC_CraneSwIndicationType_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_TailLiftTimeoutForRequest_P1DWA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWA_TailLiftTimeoutForRequest_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_WeightClassInformation_P1M7S_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1M7S_WeightClassInformation_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FuelTypeInformation_P1M7T_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1M7T_FuelTypeInformation_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_CabHeightValue_P1R0P_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1R0P_CabHeightValue_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_DwmVehicleModes_P1BDU_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BDU_DwmVehicleModes_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FSPConfigSettingsLIN2_P1EW0_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EW0_FSPConfigSettingsLIN2_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FSPConfigSettingsLIN3_P1EW1_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EW1_FSPConfigSettingsLIN3_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FSPConfigSettingsLIN4_P1EW2_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EW2_FSPConfigSettingsLIN4_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FSPConfigSettingsLIN5_P1EW3_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EW3_FSPConfigSettingsLIN5_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FSPConfigSettingsLIN1_P1EWZ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EWZ_FSPConfigSettingsLIN1_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_IL_LockingCmdDelayOff_P1K7E_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1K7E_IL_LockingCmdDelayOff_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_LockFunctionHardwareInterface_P1MXZ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1MXZ_LockFunctionHardwareInterface_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_KeyfobType_P1VKL_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VKL_KeyfobType_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_LKS_SwType_P1R5P_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1R5P_LKS_SwType_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CUF_LoadingLevelSwStuckedTimeout_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_KneelButtonStuckedTimeout_P1DWD_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWD_KneelButtonStuckedTimeout_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FerryFuncSwStuckedTimeout_P1EXK_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EXK_FerryFuncSwStuckedTimeout_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_ECS_StandbyBlinkTime_P1GCL_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1GCL_ECS_StandbyBlinkTime_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1IZ2_LoadingLevelAdjSwStuckTimeout_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FrontSuspensionType_P1JBR_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1JBR_FrontSuspensionType_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LXQ_FPBRSwitchStuckedTimeout_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FPBRSwitchRequestACKTime_P1LXR_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LXR_FPBRSwitchRequestACKTime_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_AxleConfiguration_P1B16_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B16_AxleConfiguration_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_FrontAxleArrangement_P1CSH_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CSH_FrontAxleArrangement_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_DiffLockSinglePushSwitch_LogicSelection_P1NAK_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NAK_DiffLockSinglePushSwitch_LogicSelection_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_WheelDifferentialLockPushButtonType_P1UG1_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1UG1_WheelDifferentialLockPushButtonType_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_RearAxleDifflockDisregardRequestVehicleSpeed_P1WEB_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WEB_RearAxleDifflockDisregardRequestVehicleSpeed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_PassengersSeatBeltInstalled_P1VQB_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VQB_PassengersSeatBeltInstalled_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_PassengersSeatBeltSensorType_P1VYK_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VYK_PassengersSeatBeltSensorType_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BD2_PTO_EmergencyDeactivationTimer_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_PTO_EmergencyFilteringTimer_P1BD3_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BD3_PTO_EmergencyFilteringTimer_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_PTO_RequestFilteringTimer_P1BD4_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BD4_PTO_RequestFilteringTimer_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_RAS_LEDFeedbackIndication_P1GCC_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1GCC_RAS_LEDFeedbackIndication_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1GCD_RoofHatch_FlexibleSwitchLogic_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_ABS_Inhibit_SwType_P1SY6_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1SY6_ABS_Inhibit_SwType_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWQ_SwivelSeatCheck_SetSpeed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_DoorAutoLockingSpeed_P1B2Q_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2Q_DoorAutoLockingSpeed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_AutoAlarmReactivationTimeout_P1B2S_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2S_AutoAlarmReactivationTimeout_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_DoorLatchProtectionTimeWindow_P1DW8_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DW8_DoorLatchProtectionTimeWindow_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_DoorLatchProtectionRestingTime_P1DW9_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DW9_DoorLatchProtectionRestingTime_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_DoorIndicationReqDuration_P1DWP_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWP_DoorIndicationReqDuration_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_DoorLatchProtectMaxOperation_P1DXA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DXA_DoorLatchProtectMaxOperation_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_SpeedRelockingReinitThreshold_P1H55_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1H55_SpeedRelockingReinitThreshold_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_DashboardLedTimeout_P1IZ4_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1IZ4_DashboardLedTimeout_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_PassiveEntryFunction_Type_P1VKF_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VKF_PassiveEntryFunction_Type_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_ECSStopButtonHoldTimeout_P1DWI_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWI_ECSStopButtonHoldTimeout_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_RCECSButtonStucked_P1DWJ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWJ_RCECSButtonStucked_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_RCECSUpDownStucked_P1DWK_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWK_RCECSUpDownStucked_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_RCECS_HoldCircuitTimer_P1IUS_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1IUS_RCECS_HoldCircuitTimer_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1T3W_VehSSButtonInstalled_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DI0_AuxBBSw5_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DI1_AuxBbSw6_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DIW_AuxBbSw1_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DIX_AuxBbSw2_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DIY_AuxBbSw3_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DIZ_AuxBbSw4_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BOS_AxleLoad_AccessoryBoggieALD_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BOV_AxleLoad_RatioALD_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BOW_AxleLoad_ArideLiftAxle_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BOX_AxleLoad_TridemFirstAxleLift_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZ0_AxleLoad_MaxTractionTag_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZ1_AxleLoad_RatioTagOrLoadDistrib_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZ2_AxleLoad_RatioPusherRocker_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZ3_AxleLoad_RatioTagRocker_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZW_AxleLoad_OneLiftPusher_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZX_AxleLoad_OneLiftAxleMaxTraction_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZY_AxleLoad_OneLiftTag_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZZ_AxleLoad_MaxTractionPusher_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1J6B_AxleLoad_AccessoryTridemALD_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1J6C_AxleLoad_BoggieDualRatio_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1J6D_AxleLoad_TridemDualRatio_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1KN2_AxleLoad_CRideLiftAxle_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1KN4_AxleLoad_CRideLEDlowerEnd_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1M5B_AxleLoad_RatioRoadGripPusher_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CAQ_LECML_Installed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2G_LECMH_Installed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NT1_CM_DeviceType_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DXX_FMSgateway_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8I_BB2_active_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8J_BB1_active_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8K_CabSubnet_active_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8L_SecuritySubnet_active_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8M_CAN6_active_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6I_Diag_Act_AO12_P_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6K_Diag_Act_AO12_L_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6Q_Diag_Act_DOBHS01_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6R_Diag_Act_DOBHS02_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6S_Diag_Act_DOBHS03_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6T_Diag_Act_DOBHS04_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7D_Diag_Act_DOBLS01_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7G_Diag_Act_DAI01_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7H_Diag_Act_DAI02_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8E_Diag_Act_12VDCDC_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B04_DLFW_Installed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VKI_PassiveStart_Installed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VQ0_FuelEcoOffButton_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B0X_EngineSpeedControlSw_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VR3_ELCP1_Installed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VR5_ELCP2_Installed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LX8_EngineFuelTypeLNG_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1M7Q_EraGlonassUnitInstalled_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BWZ_DoubleRoofHatchSwConfig_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1SDA_OptitrackSystemInstalled_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VR1_ILCP1_Installed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VR2_ILCP2_Installed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WME_LowPowerPullUpAct_Parked_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMF_LowPowerPullUpAct_Living_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMN_LowPower12VOutputAct_Living_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMO_LowPower12VOutputAct_Parked_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2U_KeyfobPresent_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1Y1C_SuperlockInhibition_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BKI_LKS_Installed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NQD_LKS_SwIndicationType_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1A12_ADL_Sw_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CT4_FerrySw_Installed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CT9_LoadingLevelSw_Installed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EXH_KneelingSwitchInstalled_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1IZ1_LoadingLevelAdjSwitchInstalled_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1LXP_FPBRSwitchInstalled_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B03_RearWheelDiffLockPushSw_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CUC_ConstructionSw_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1F7J_ASROffRoadFullVersion_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1FNW_ASROffButtonInstalled_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NQA_AutomaticFrontWheelDrive_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1GBT_RAS_SwitchInstalled_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1RRH_RAS_ToggleButton_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BXH_ReverseWarning_SwType_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CW8_RoofHatchCtrl_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1A1R_HSA_Installed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NTV_HSA_DefaultConfig_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1SY4_ABS_Inhibit_Installed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2C_CCFW_Installed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EXP_PersonalSettingsForCC_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2T_AlarmInstalled_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NE9_KeyInsertDetection_Enabled_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1NQE_LockModeHandling_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_KeyfobEncryptCode_P1DS4_a_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DS4_KeyfobEncryptCode_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_AdiWakeUpConfig_P1WMD_a_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMD_AdiWakeUpConfig_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Diag_Act_LF_P_P1V79_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V79_Diag_Act_LF_P_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_VINCheckProcessing_P1VKG_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VKG_VINCheckProcessing_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_IL_ShortLongPushThresholds_P1DKF_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DKF_IL_ShortLongPushThresholds_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1QR6_HWIO_CfgFault_PWR24V_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Config_ADI07_P1V60_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V60_Fault_Config_ADI07_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Config_ADI08_P1V61_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V61_Fault_Config_ADI08_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Config_ADI09_P1V62_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V62_Fault_Config_ADI09_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Config_ADI10_P1V63_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V63_Fault_Config_ADI10_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Config_ADI11_P1V64_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V64_Fault_Config_ADI11_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Config_ADI12_P1V65_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V65_Fault_Config_ADI12_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Config_ADI13_P1V66_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V66_Fault_Config_ADI13_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Config_ADI14_P1V67_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V67_Fault_Config_ADI14_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Config_ADI15_P1V68_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V68_Fault_Config_ADI15_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Config_ADI16_P1V69_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V69_Fault_Config_ADI16_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Config_ADI01_P1V6U_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6U_Fault_Config_ADI01_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Config_ADI02_P1V6V_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6V_Fault_Config_ADI02_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Config_ADI03_P1V6W_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6W_Fault_Config_ADI03_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Config_ADI04_P1V6X_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6X_Fault_Config_ADI04_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Config_ADI05_P1V6Y_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6Y_Fault_Config_ADI05_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Config_ADI06_P1V6Z_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6Z_Fault_Config_ADI06_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8F_Fault_Cfg_DcDc12v_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_DAI_Installed_P1WMP_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMP_DAI_Installed_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_ECS_MemSwTimings_P1BWF_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BWF_ECS_MemSwTimings_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_ForcedPositionStatus_Front_P1JSY_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1JSY_ForcedPositionStatus_Front_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1JSZ_ForcedPositionStatus_Rear_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */

#  define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_AuxBbSw1_Logic_P1DI2_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI2_AuxBbSw1_Logic_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_AuxBbSw2_Logic_P1DI3_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI3_AuxBbSw2_Logic_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_AuxBbSw3_Logic_P1DI4_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI4_AuxBbSw3_Logic_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_AuxBbSw4_Logic_P1DI5_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI5_AuxBbSw4_Logic_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_AuxBbSw5_Logic_P1DI6_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI6_AuxBbSw5_Logic_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_AuxBbSw6_Logic_P1DI7_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI7_AuxBbSw6_Logic_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_BodybuilderAccessToAccelPedal_P1B72_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1B72_BodybuilderAccessToAccelPedal_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_IL_CtrlDeviceTypeFront_P1DKH_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DKH_IL_CtrlDeviceTypeFront_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DKI_IL_CtrlDeviceTypeBunk_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1B9X_WirelessRC_Enable_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1BOY_AxleLoad_TridemSecondAxleLift_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1BWE_CabTiltEnable_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AW6_IsEcuAvailableDDM_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AWY_IsEcuAvailableAlarm_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AXE_IsEcuAvailablePDM_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1ALT_ECS_PartialAirSystem_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1ALU_ECS_FullAirSystem_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1B0W_CrossCountryCC_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1A1T_CityHorn_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1A1U_AuxiliaryHorn_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1A1V_HornLivingMode_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AJL_PTO1_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AJM_PTO2_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1BBG_PTO3_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1BBH_PTO4_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1AJJ_ReverseWarning_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1CUD_SwivelSeatWarning_Act_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */

#  define RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_CONST_SA_lvl_0x2D_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2D) Rte_AddrPar_0x2D_P1TTA_GearBoxLockActivation_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_AntMappingConfig_Gain_X1C03_a_T, RTE_CONST_SA_lvl_0x2D) Rte_AddrPar_0x2D_X1C03_AntMappingConfig_Gain_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_AntMappingConfig_Multi_X1CY3_a_T, RTE_CONST_SA_lvl_0x2D) Rte_AddrPar_0x2D_X1CY3_AntMappingConfig_Multi_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_AntMappingConfig_Single_X1CY5_s_T, RTE_CONST_SA_lvl_0x2D) Rte_AddrPar_0x2D_X1CY5_AntMappingConfig_Single_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */

#  define RTE_STOP_SEC_CONST_SA_lvl_0x2D_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_CONST_SA_lvl_0x2D_and_0x37_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_CrankingLockActivation_P1DS3_T, RTE_CONST_SA_lvl_0x2D_and_0x37) Rte_AddrPar_0x2D_and_0x37_P1DS3_CrankingLockActivation_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */

#  define RTE_STOP_SEC_CONST_SA_lvl_0x2D_and_0x37_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_KeyfobDetectionMappingSelection_P1WIP_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1WIP_KeyfobDetectionMappingSelection_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_KeyfobDetectionMappingConfig_P1WIR_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1WIR_KeyfobDetectionMappingConfig_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1C54_FactoryModeActive_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1WPP_isSecurityLinActive_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_ComCryptoKey_P1DLX_a_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1DLX_ComCryptoKey_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_ChassisId_CHANO_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_CHANO_ChassisId_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_VIN_VINNO_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_VINNO_VIN_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */
extern CONST(SEWS_AuxPassiveAntennasActivation_P1WIQ_s_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1WIQ_AuxPassiveAntennasActivation_v; /* PRQA S 0850, 3408 */ /* MD_MSR_19.8, MD_Rte_3408 */

#  define RTE_STOP_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Buffers for unqueued S/R
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(StandardNVM_T, RTE_VAR_NOINIT) Rte_Application_Data_NVM_DriverAuth2_Ctrl_NVM; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(EngTraceHWArray, RTE_VAR_NOINIT) Rte_Application_Data_NVM_EngTraceHW_NvM; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(StandardNVM_T, RTE_VAR_NOINIT) Rte_Application_Data_NVM_InteriorLights_HMICtrl_NVM; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(StandardNVM_T, RTE_VAR_NOINIT) Rte_Application_Data_NVM_MUT_UICtrl_Difflock_NVM; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(StandardNVM_T, RTE_VAR_NOINIT) Rte_Application_Data_NVM_MUT_UICtrl_Traction_NVM; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(StandardNVM_T, RTE_VAR_NOINIT) Rte_Application_Data_NVM_PinCode_ctrl_NVM; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(StandardNVM_T, RTE_VAR_NOINIT) Rte_Application_Data_NVM_RGW_HMICtrl_NVM; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(SpeedControl_NVM_T, RTE_VAR_NOINIT) Rte_Application_Data_NVM_SCM_HMICtrl_NVM; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(StandardNVM_T, RTE_VAR_NOINIT) Rte_Application_Data_NVM_VehicleAccess_Ctrl_NVM; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(StandardNVM_T, RTE_VAR_NOINIT) Rte_Application_Data_NVM_VehicleModeDistribution_NVM; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceAuthentication_rqst_T, RTE_VAR_NOINIT) Rte_AuthenticationDevice_UICtrl_DeviceAuthentication_rqst_DeviceAuthentication_rqst; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DriverAuthDeviceMatching_T, RTE_VAR_NOINIT) Rte_AuthenticationDevice_UICtrl_DriverAuthDeviceMatching_DriverAuthDeviceMatching; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_AuxHorn_Input_Hdlr_AH_PushButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch1_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch2_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch3_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch4_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch5_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch6_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_ALD_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DualDeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_BogieSwitch_DeviceIndication_DualDeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_MaxTract_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_Ratio1_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_Ratio2_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DualDeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_Ratio4_DeviceIndication_DualDeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DualDeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_Ratio5_DeviceIndication_DualDeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_Ratio6_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DualDeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_RatioALD_DualDeviceIndication_DualDeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_TridemALD_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceBasic_LINMaCtrl_BunkBIntLightActvnBtn_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightActvnBtn_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2LockButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FspNVM_T, RTE_VAR_NOINIT) Rte_Calibration_Data_NVM_FSP_NVM; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(uint8, RTE_VAR_NOINIT) Rte_Cdd_LinDiagnostics_LinDiagRequestFlag_CCNADRequest; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(uint8, RTE_VAR_NOINIT) Rte_Cdd_LinDiagnostics_LinDiagRequestFlag_PNSNRequest; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DoorLatch_stat_T, RTE_VAR_NOINIT) Rte_CentralDoorsLatch_Hdlr_DriverDoorLatchInternal_stat_DoorLatch_stat; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DoorLatch_stat_T, RTE_VAR_NOINIT) Rte_CentralDoorsLatch_Hdlr_PsngDoorLatchInternal_stat_DoorLatch_stat; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_CityHorn_Input_Hdlr_CH_PushButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_CollisionMitigation_HMICtrl_FCW_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(VEC_CryptoProxy_UserSignal, RTE_VAR_NOINIT) Rte_CryptoDriverKeyCyl_Rx_VEC_CryptoProxySerializedData_Crypto_Function_serialized; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(VEC_CryptoProxy_UserSignal, RTE_VAR_NOINIT) Rte_CryptoLockingSwitch_Rx_VEC_CryptoProxySerializedData_Crypto_Function_serialized; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(VEC_CryptoProxy_UserSignal, RTE_VAR_NOINIT) Rte_CryptoPsngKeyCyl_Rx_VEC_CryptoProxySerializedData_Crypto_Function_serialized; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_DAS_HMICtrl_DAS_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DiagActiveState_T, RTE_VAR_NOINIT) Rte_DiagnosticComponent_DiagActiveState_isDiagActive; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(uint32, RTE_VAR_NOINIT) Rte_DiagnosticComponent_LpModeRunTime_LpModeRunTime; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_DiffLockPanel_LINMasterCtrl_DifflockDeactivationBtn_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FreeWheel_Status_T, RTE_VAR_NOINIT) Rte_DiffLockPanel_LINMasterCtrl_DifflockMode_Wheelstatus_FreeWheel_Status; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_DiffLockPanel_LINMasterCtrl_EscButtonMuddySiteStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_DiffLockPanel_LINMasterCtrl_Offroad_ButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_DoubleSwitch_HMICtrl_SwitchStatus_combined_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Crypto_Function_serialized_T, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_EngineStartAuth_st_serialized_Crypto_Function_serialized; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_EngineStartAuth_stat_CryptTrig_CryptoTrigger; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_GrbxUnlockAuth_stat_CryptTrig_CryptoTrigger; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Crypto_Function_serialized_T, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_GrbxUnlockAuth_stat_serialized_Crypto_Function_serialized; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_KeyAuth_stat_CryptTrig_CryptoTrigger; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Crypto_Function_serialized_T, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_KeyAuth_stat_serialized_Crypto_Function_serialized; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(KeyAuthentication_stat_decrypt_T, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(KeyfobAuth_rqst_T, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_KeyfobAuth_rqst_KeyfobAuth_rqst; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PinCode_rqst_T, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_PinCode_rqst_PinCode_rqst; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_AdjustButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_BackButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_MemButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_SelectButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_StopButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(EvalButtonRequest_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_WRDownButtonStatus_EvalButtonRequest; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(EvalButtonRequest_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_WRUpButtonStatus_EvalButtonRequest; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_EconomyPower_HMICtrl_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_EngineSpeedControl_HMICtrl_EscButtonMuddySiteDeviceInd_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_EngineSpeedControl_HMICtrl_EscSwitchEnableDeviceInd_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_EngineSpeedControl_HMICtrl_EscSwitchMuddySiteDeviceInd_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FreeWheel_Status_T, RTE_VAR_NOINIT) Rte_ExteriorLightPanel_1_LINMastCtrl_LightMode_Status_1_FreeWheel_Status; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FreeWheel_Status_T, RTE_VAR_NOINIT) Rte_ExteriorLightPanel_2_LINMastCtrl_LightMode_Status_2_FreeWheel_Status; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ExteriorLightPanel_2_LINMastCtrl_RearWorkProjector_ButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraBbContOrSlid_HMICtrl_ContUnlock_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraBbContOrSlid_HMICtrl_Slid5thWheel_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraBbTailLiftCrane_HMICtrl_CraneSupply_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraBbTailLiftCrane_HMICtrl_TailLift_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_BeaconSRocker_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_Beacon_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_CabWorkingLight_DevInd_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_EquipmentLightInd_cmd_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_FifthWheelLightInd_cmd_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_LEDVega_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_PloughtLights_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_RearWorkProjector_Indication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_WorkingLight_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(BswM_BswMRteMDG_NvmWriteAllRequest, RTE_VAR_NOINIT) Rte_FaultEventGateway_ctrl_ModeRequest_NvmWriteAllRequest_requestedMode; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ABSInhibitSwitchStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AEBS_ButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ALDSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ASROffButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AlternativeDriveLevelSw_stat_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch1SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch2SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch3SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch4SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch5SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch6SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_BeaconSRocker_DeviceEvent_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Beacon_DeviceEven_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_CabWorkLight_ButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Construction_SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ContUnlockSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_CranePushButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_CraneSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_DAS_SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_DashboardLockButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EconomyPowerSwitch_status_EconomyPowerSwitch_status; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EquipmentLight_DeviceEvent_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchEnableStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchIncDecStatus_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchMuddySiteStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchResumeStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FCW_SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FPBRSwitchStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPIndicationCmdArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FSP1IndicationCmd_FSPIndicationCmdArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPIndicationCmdArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FSP2IndicationCmd_FSPIndicationCmdArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPIndicationCmdArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FSP3IndicationCmd_FSPIndicationCmdArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPIndicationCmdArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FSP4IndicationCmd_FSPIndicationCmdArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPIndicationCmdArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FSP5IndicationCmd_FSPIndicationCmdArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPIndicationCmdArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FSP6IndicationCmd_FSPIndicationCmdArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPIndicationCmdArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FSP7IndicationCmd_FSPIndicationCmdArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPIndicationCmdArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FSP8IndicationCmd_FSPIndicationCmdArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPIndicationCmdArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FSP9IndicationCmd_FSPIndicationCmdArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPIndicationCmdArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FSP_BIndicationCmd_FSPIndicationCmdArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FerryFunctionSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FifthWheelLight_DeviceEvent_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FrtAxleHydro_ButtonPush_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_HillStartAidButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_IntLghtActvnBtn_stat_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_IntLghtMaxModeFlxSw2_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_IntLghtNightModeBtn_stat_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_IntLghtNightModeFlxSw2_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_KneelSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LEDVega_DeviceEvent_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LKSCS_SwitchStatus_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LKS_SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1Switch2_Status_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1SwitchStatus_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle2SwitchStatus_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LoadingLevelSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_MirrorHeatingSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_PloughLight_DeviceEvent_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_PloughtLightsPushButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Pto1SwitchStatus_Pto1SwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Pto2SwitchStatus_Pto2SwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Pto3SwitchStatus_Pto3SwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Pto4SwitchStatus_Pto4SwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio1SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio2SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio3SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio4SwitchStatus_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio5SwitchStatus_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio6SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_RatioALDSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_RearAxleDiffLock_ButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_RearAxleSteering_DeviceEvent_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ReducedSetModeButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ReverseGearWarningBtn_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ReverseGearWarningSw_stat_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_RoofHatch_SwitchStatus_1_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_RoofHatch_SwitchStatus_2_A3PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Slid5thWheelSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_SpotlightFront_DeviceEvent_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_SpotlightRoof_DeviceEvent_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_TailLiftPushButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_TailLiftSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_TransferCaseNeutral_SwitchStat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_TridemALDSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_WorkLight_ButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPSwitchStatusArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP1SwitchStatus_FSPSwitchStatusArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPSwitchStatusArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP2SwitchStatus_FSPSwitchStatusArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPSwitchStatusArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP3SwitchStatus_FSPSwitchStatusArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPSwitchStatusArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP4SwitchStatus_FSPSwitchStatusArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPSwitchStatusArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP5SwitchStatus_FSPSwitchStatusArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPSwitchStatusArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP6SwitchStatus_FSPSwitchStatusArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPSwitchStatusArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP7SwitchStatus_FSPSwitchStatusArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPSwitchStatusArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP8SwitchStatus_FSPSwitchStatusArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPSwitchStatusArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP9SwitchStatus_FSPSwitchStatusArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSPSwitchStatusArray_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_FSP_BSwitchStatus_FSPSwitchStatusArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_Living12VResetRequest_Living12VResetRequest; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LINMstr_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FrontLidLatch_stat_T, RTE_VAR_NOINIT) Rte_FrontLidLatchSensor_hdlr_FrontLidLatch_stat_FrontLidLatch_stat; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_FrontPropulsion_UOCtrl_FrtAxleHydro_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_Horn_HMICtrl_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_Horn_HMICtrl_CityHorn_HMI_rqst_CityHorn_HMI_rqst; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(KeyFobNVM_T, RTE_VAR_NOINIT) Rte_Keyfob_Radio_Com_NVM_KeyFobNVBlock; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DoorLockUnlock_T, RTE_VAR_NOINIT) Rte_InCabLock_HMICtrl_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_1_LINMastCtrl_DoorAutoFuncBtn_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_1_LINMastCtrl_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_1_LINMastCtrl_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FreeWheel_Status_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_1_LINMastCtrl_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FreeWheel_Status_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtNightModeBtn2_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtRestModeBtnPnl2_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_DoorAutoFuncInd_cmd_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(IntLghtLvlIndScaled_cmd_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(InteriorLightMode_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_IntLghtModeInd_cmd_InteriorLightMode; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_IntLghtOffModeInd_cmd_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_IntLightMaxModeInd_cmd_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_IntLightNightModeInd_cmd_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_IntLightRestingModeInd_cmd_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DoorAjar_stat_T, RTE_VAR_NOINIT) Rte_InternalDoorsAjar_Hdlr_DriverDoorAjarInternal_stat_DoorAjar_stat; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DoorAjar_stat_T, RTE_VAR_NOINIT) Rte_InternalDoorsAjar_Hdlr_PsngrDoorAjarInternal_stat_DoorAjar_stat; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Fsc_OperationalMode_T, RTE_VAR_NOINIT) Rte_IoHwAb_ASIL_Core_Fsc_OperationalMode_P_Fsc_OperationalMode; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(SEWS_KeyfobEncryptCode_P1DS4_a_T, RTE_VAR_NOINIT) Rte_Keyfob_Mgr_AddrParP1DS4_stat_dataP1DS4; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(KeyfobAuth_stat_T, RTE_VAR_NOINIT) Rte_Keyfob_Mgr_KeyfobAuth_stat_KeyfobAuth_stat; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(KeyfobInCabLocation_stat_T, RTE_VAR_NOINIT) Rte_KeyfobInCabLocation_stat_oCIOM_BB2_01P_oBackbone2_69de562e_Tx; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(KeyfobOutsideLocation_stat_T, RTE_VAR_NOINIT) Rte_Keyfob_Mgr_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_Keyfob_UICtrl_ApproachLightButton_Statu_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_Keyfob_UICtrl_KeyfobLockButton_Status_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_Keyfob_UICtrl_KeyfobSuperLockButton_Sta_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_KeyfobUnlockButton_Status_oCIOM_BB2_02P_oBackbone2_7b99e4e7_Tx; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN1_ComMode_LIN; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN2_ComMode_LIN; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN3_ComMode_LIN; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN4_ComMode_LIN; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN5_ComMode_LIN; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(BswM_BswMRteMDG_LIN1Schedule, RTE_VAR_NOINIT) Rte_LINMgr_Request_LIN1_ScheduleTableRequestMode_requestedMode; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(BswM_BswMRteMDG_LIN2Schedule, RTE_VAR_NOINIT) Rte_LINMgr_Request_LIN2_ScheduleTableRequestMode_requestedMode; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(BswM_BswMRteMDG_LIN3Schedule, RTE_VAR_NOINIT) Rte_LINMgr_Request_LIN3_ScheduleTableRequestMode_requestedMode; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(BswM_BswMRteMDG_LIN4Schedule, RTE_VAR_NOINIT) Rte_LINMgr_Request_LIN4_ScheduleTableRequestMode_requestedMode; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(BswM_BswMRteMDG_LIN5Schedule, RTE_VAR_NOINIT) Rte_LINMgr_Request_LIN5_ScheduleTableRequestMode_requestedMode; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(BswM_BswMRteMDG_LIN6Schedule, RTE_VAR_NOINIT) Rte_LINMgr_Request_LIN6_ScheduleTableRequestMode_requestedMode; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(BswM_BswMRteMDG_LIN7Schedule, RTE_VAR_NOINIT) Rte_LINMgr_Request_LIN7_ScheduleTableRequestMode_requestedMode; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(BswM_BswMRteMDG_LIN8Schedule, RTE_VAR_NOINIT) Rte_LINMgr_Request_LIN8_ScheduleTableRequestMode_requestedMode; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DualDeviceIndication_T, RTE_VAR_NOINIT) Rte_LKS_HMICtrl_LKSCS_DeviceIndication_DualDeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_LKS_HMICtrl_LKS_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FalseTrue_T, RTE_VAR_NOINIT) Rte_LevelControl_HMICtrl_BlinkECSWiredLEDs_BlinkECSWiredLEDs; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(ECSStandByRequest_T, RTE_VAR_NOINIT) Rte_ECSStandByRequest_ISig_4_oCIOM_BB2_03P_oBackbone2_9b85740c_Tx; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_LevelControl_HMICtrl_FPBR_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_LevelControl_HMICtrl_FerryFunction_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_LevelControl_HMICtrl_KneelDeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(RampLevelRequest_T, RTE_VAR_NOINIT) Rte_RampLevelRequest_ISig_4_oCIOM_BB2_03P_oBackbone2_26c4f794_Tx; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_LuggageCompartements2_hdlr_LuggageCompart_stat_CryptTrig_CryptoTrigger; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Crypto_Function_serialized_T, RTE_VAR_NOINIT) Rte_LuggageCompartements2_hdlr_LuggageCompart_stat_serialized_Crypto_Function_serialized; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MirrorHeating_HMICtrl_MirrorHeatingDeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MovingUnitTraction_UICtrl_ASROff_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MovingUnitTraction_UICtrl_ConstructionSwitch_DeviceInd_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MovingUnitTraction_UICtrl_DifflockOnOff_Indication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MovingUnitTraction_UICtrl_Offroad_Indication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MovingUnitTraction_UICtrl_RearDiffLock_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_PassiveDoorButton_hdlr_LeftDoorButton_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_PassiveDoorButton_hdlr_RightDoorButton_stat_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PinCode_stat_T, RTE_VAR_NOINIT) Rte_PinCode_ctrl_PinCode_stat_PinCode_stat; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_PowerTakeOff_HMICtrl_PTO1_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_PowerTakeOff_HMICtrl_PTO2_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_PowerTakeOff_HMICtrl_PTO3_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_PowerTakeOff_HMICtrl_PTO4_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_RearAxleSteering_HMICtrl_RearAxleSteeringDeviceInd_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ReverseGearWarning_HMICtrl_ReverseWarningInd_cmd_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(BswM_BswMRteMDG_PvtReport_X1C14, RTE_VAR_NOINIT) Rte_SCIM_PVTPT_IO_Request_SwcModeRequest_PvtReportCtrl_requestedMode; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(uint8, RTE_VAR_NOINIT) Rte_SCIM_PVTPT_IO_ScimPvtControl_P_Status; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Living12VPowerStability, RTE_VAR_NOINIT) Rte_SCIM_PowerSupply12V_Hdlr_Living12VPowerStability_Living12VPowerStability; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ServiceBraking_HMICtrl_HillStartAid_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_SpeedControlFreeWheel_LINMasCtrl_FCWPushButton_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_SpeedControlFreeWheel_LINMasCtrl_LKSPushButton_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_SpeedControlFreeWheel_LINMasCtrl_SpeedControlModeButtonStatus_PushButtonStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FreeWheel_Status_T, RTE_VAR_NOINIT) Rte_SpeedControlFreeWheel_LINMasCtrl_SpeedControlModeWheelStatus_FreeWheel_Status; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_SpeedControlMode_HMICtrl_ACCOrCCIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_SpeedControlMode_HMICtrl_ASLIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(SeatSwivelStatus_T, RTE_VAR_NOINIT) Rte_SwivelSeatSwitch_hdlr_SeatSwivelStatus_SeatSwivelStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_TheftAlarm_HMI2_ctrl_ReducedSetMode_DevInd_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_TheftAlarm_HMI2_ctrl_ReducedSetMode_rqst_CryptTrig_CryptoTrigger; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Crypto_Function_serialized_T, RTE_VAR_NOINIT) Rte_TheftAlarm_HMI2_ctrl_ReducedSetMode_rqst_serialized_Crypto_Function_serialized; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_TheftAlarm_HMI2_ctrl_TheftAlarmAct_rqst_CryptTrig_CryptoTrigger; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Crypto_Function_serialized_T, RTE_VAR_NOINIT) Rte_TheftAlarm_HMI2_ctrl_TheftAlarmAct_rqst_serialized_Crypto_Function_serialized; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_TransferCase_HMICtrl_TransferCaseNeutral_DevInd_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_DashboardLockSwitch_Devic_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DoorsAjar_stat_T, RTE_VAR_NOINIT) Rte_DoorsAjar_stat_ISig_4_oCIOM_BB2_06P_oBackbone2_8810fc26_Tx; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_DriverDoorLatch_rqst_CryptTrig_CryptoTrigger; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DoorLatch_rqst_decrypt_T, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Crypto_Function_serialized_T, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_DriverDoorLatch_rqt_serialized_Crypto_Function_serialized; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FrontLidLatch_cmd_T, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_FrontLidLatch_cmd_FrontLidLatch_cmd; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(KeyfobLocation_rqst_T, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_KeyfobLocation_rqst_KeyfobLocation_rqst; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DoorLatch_rqst_decrypt_T, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_PsngrDoorLatch_rqst_CryptTrig_CryptoTrigger; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Crypto_Function_serialized_T, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_PsngrDoorLatch_rqst_serialized_Crypto_Function_serialized; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_EngineRun_EngineRun; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_LIN_SwcActivation_LIN; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Living_Living; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Parked_Parked; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(VehicleMode_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_Adjust_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_Down_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(ECSStandByReq_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_ECSStandByReqRCECS_ECSStandByReqRCECS; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_M1_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_M2_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_M3_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(ShortPulseMaxLength_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_ShortPulseMaxLength_ShortPulseMaxLength; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_Up_DeviceIndication_DeviceIndication; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FalseTrue_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredAirSuspensionStopRequest_AirSuspensionStopRequest; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(LevelAdjustmentAction_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredLevelAdjustmentAction_LevelAdjustmentAction; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(LevelAdjustmentAxles_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredLevelAdjustmentAxles_LevelAdjustmentAxles; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(LevelAdjustmentStroke_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredLevelAdjustmentStroke_LevelAdjustmentStroke; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(WiredLevelUserMemory_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredLevelUserMemory_WiredLevelUserMemory; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(LevelUserMemoryAction_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredLevelUserMemoryAction_LevelUserMemoryAction; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* queue ROM informations type */
typedef struct
{
  P2VAR(void, TYPEDEF, RTE_VAR_NOINIT) Rte_BasePtr; /* PRQA S 0850 */ /* MD_MSR_19.8 */
  uint16 Rte_BytesPerElement;
  uint8 Rte_MaxElements;
} Rte_QRomInfoType;

/* queue RAM informations type */
typedef struct
{
  uint8 Rte_ReadCtr;
  uint8 Rte_WriteCtr;
  uint8 Rte_ElementCtr;
} Rte_QRamInfoType;

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Rte_QRamInfoType, RTE_VAR_NOINIT) Rte_QRamInfo[19]; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_CONST_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(Rte_QRomInfoType, RTE_CONST) Rte_QRomInfo[19];

#  define RTE_STOP_SEC_CONST_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

typedef struct
{
  Rte_BitType Rte_b0 : 1;
  Rte_BitType Rte_b1 : 1;
  Rte_BitType Rte_b2 : 1;
  Rte_BitType Rte_b3 : 1;
  Rte_BitType Rte_b4 : 1;
  Rte_BitType Rte_b5 : 1;
  Rte_BitType Rte_b6 : 1;
  Rte_BitType Rte_b7 : 1;
  Rte_BitType Rte_b8 : 1;
  Rte_BitType Rte_b9 : 1;
  Rte_BitType Rte_b10 : 1;
  Rte_BitType Rte_b11 : 1;
  Rte_BitType Rte_b12 : 1;
  Rte_BitType Rte_b13 : 1;
  Rte_BitType Rte_b14 : 1;
  Rte_BitType Rte_b15 : 1;
  Rte_BitType Rte_b16 : 1;
  Rte_BitType Rte_b17 : 1;
  Rte_BitType Rte_b18 : 1;
} Rte_QOverflowType;

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Rte_QOverflowType, RTE_VAR_NOINIT) Rte_QOverflow; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

typedef struct
{
  Rte_BitType Rte_TxAck_CryptoDriverDoorLatch_Tx_VEC_EncryptedSignal_EncryptedSignal : 2;
  Rte_BitType Rte_TxAck_CryptoDriverKeyCyl_Rx_VEC_CryptoIdKey_CryptoIdKey : 2;
  Rte_BitType Rte_TxAck_CryptoEngineStart_Tx_VEC_EncryptedSignal_EncryptedSignal : 2;
  Rte_BitType Rte_TxAck_CryptoGearboxLock_Tx_VEC_EncryptedSignal_EncryptedSignal : 2;
  Rte_BitType Rte_TxAck_CryptoKeyAuth_Tx_VEC_EncryptedSignal_EncryptedSignal : 2;
  Rte_BitType Rte_TxAck_CryptoLockingSwitch_Rx_VEC_CryptoIdKey_CryptoIdKey : 2;
  Rte_BitType Rte_TxAck_CryptoLuggageCompartment_Tx_VEC_EncryptedSignal_EncryptedSignal : 2;
  Rte_BitType Rte_TxAck_CryptoPsngDoorLatch_Tx_VEC_EncryptedSignal_EncryptedSignal : 2;
  Rte_BitType Rte_TxAck_CryptoPsngKeyCyl_Rx_VEC_CryptoIdKey_CryptoIdKey : 2;
  Rte_BitType Rte_TxAck_CryptoReducedSetMode_Tx_VEC_EncryptedSignal_EncryptedSignal : 2;
  Rte_BitType Rte_TxAck_CryptoTheftAlarmActivation_Tx_VEC_EncryptedSignal_EncryptedSignal : 2;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_Dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_Dcm_DcmEcuReset_DcmEcuReset_Ack : 1;
} Rte_AckFlagsType;

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Rte_AckFlagsType, RTE_VAR_NOINIT) Rte_AckFlags; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

typedef struct
{
  Rte_BitType Rte_RxTimeout_ABSInhibit_DeviceIndication_oHMIIOM_BB2_07P_oBackbone2_ee1461e1_Rx : 1;
  Rte_BitType Rte_RxTimeout_ABSInhibitionStatus_ISig_3_oEBS_BB1_02P_oBackbone1J1939_14d96a66_Rx : 1;
  Rte_BitType Rte_RxTimeout_ACCEnableRqst_ISig_4_oHMIIOM_BB2_01P_oBackbone2_fe5931dd_Rx : 1;
  Rte_BitType Rte_RxTimeout_ASRHillHolderSwitch_ISig_3_oEBC1_X_EBS_oBackbone1J1939_ae8948e3_Rx : 1;
  Rte_BitType Rte_RxTimeout_AcceleratorPedalPosition1_ISig_3_oVMCU_BB1_01P_oBackbone1J1939_7025a200_Rx : 1;
  Rte_BitType Rte_RxTimeout_AcceleratorPedalStatus_ISig_4_oVMCU_BB2_52P_oBackbone2_c6b3ef9e_Rx : 1;
  Rte_BitType Rte_RxTimeout_ActualDrvlnRetdrPercentTorque_ISig_3_oERC1_X_RECU_oBackbone1J1939_b2bc1e25_Rx : 1;
  Rte_BitType Rte_RxTimeout_ActualEnginePercentTorque_ISig_3_oEEC1_X_EMS_oBackbone1J1939_cfb19d75_Rx : 1;
  Rte_BitType Rte_RxTimeout_ActualEngineRetarderPercentTrq_ISig_3_oERC1_X_EMSRet_oBackbone1J1939_d87209ba_Rx : 1;
  Rte_BitType Rte_RxTimeout_AmbientAirTemperature_ISig_3_oAMB_X_VMCU_oBackbone1J1939_a102c408_Rx : 1;
  Rte_BitType Rte_RxTimeout_AudioSystemStatus_ISig_4_oHMIIOM_BB2_07P_oBackbone2_4800fc7f_Rx : 1;
  Rte_BitType Rte_RxTimeout_AudioVolumeIndicationCmd_ISig_4_oHMIIOM_BB2_07P_oBackbone2_88a309c1_Rx : 1;
  Rte_BitType Rte_RxTimeout_AutorelockingMovements_stat_oAlarm_Sec_02P_oSecuritySubnet_c816460c_Rx : 1;
  Rte_BitType Rte_RxTimeout_AuxSwitchBbLoad1_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_68beafdf_Rx : 1;
  Rte_BitType Rte_RxTimeout_AuxSwitchBbLoad2_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_42021f57_Rx : 1;
  Rte_BitType Rte_RxTimeout_AuxSwitchBbLoad3_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_ed468d10_Rx : 1;
  Rte_BitType Rte_RxTimeout_AuxSwitchBbLoad4_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_177b7e47_Rx : 1;
  Rte_BitType Rte_RxTimeout_AuxSwitchBbLoad5_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_b83fec00_Rx : 1;
  Rte_BitType Rte_RxTimeout_AuxSwitchBbLoad6_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_92835c88_Rx : 1;
  Rte_BitType Rte_RxTimeout_BBNetwBeaconLight_stat_oBBM_BB2_02P_oBackbone2_0af6e9b5_Rx : 1;
  Rte_BitType Rte_RxTimeout_BBNetwBodyOrCabWrknLight_stat_oBBM_BB2_02P_oBackbone2_1a41cb1a_Rx : 1;
  Rte_BitType Rte_RxTimeout_BBNetwWrknLightChassis_stat_oBBM_BB2_02P_oBackbone2_d6b6577a_Rx : 1;
  Rte_BitType Rte_RxTimeout_BTStatus_oHMIIOM_BB2_07P_oBackbone2_2504e67b_Rx : 1;
  Rte_BitType Rte_RxTimeout_BackToDriveReqACK_ISig_4_oVMCU_BB2_01P_oBackbone2_220da73b_Rx : 1;
  Rte_BitType Rte_RxTimeout_BodyOrCabWorkingLightFdbk_stat_ISig_4_oVMCU_BB2_08P_oBackbone2_6dee2288_Rx : 1;
  Rte_BitType Rte_RxTimeout_BrakeBlending_DeviceIndication_oHMIIOM_BB2_07P_oBackbone2_22c13ed8_Rx : 1;
  Rte_BitType Rte_RxTimeout_BrakeSwitch_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_f9976b4d_Rx : 1;
  Rte_BitType Rte_RxTimeout_BunkH1IntLghtActvnBtn_stat_oLECM1_Cab_02P_oCabSubnet_20e1888b_Rx : 1;
  Rte_BitType Rte_RxTimeout_BunkH1IntLghtDirAccsDnBtn_stat_oLECM1_Cab_02P_oCabSubnet_9b517646_Rx : 1;
  Rte_BitType Rte_RxTimeout_BunkH1IntLghtDirAccsUpBtn_stat_oLECM1_Cab_02P_oCabSubnet_a6085fb2_Rx : 1;
  Rte_BitType Rte_RxTimeout_BunkH1LockButtonStatus_oLECM1_Cab_02P_oCabSubnet_493c9af1_Rx : 1;
  Rte_BitType Rte_RxTimeout_BunkH1RoofhatchCloseBtn_Stat_oLECM1_Cab_02P_oCabSubnet_fa799afe_Rx : 1;
  Rte_BitType Rte_RxTimeout_BunkH1RoofhatchOpenBtn_Stat_oLECM1_Cab_02P_oCabSubnet_fde56247_Rx : 1;
  Rte_BitType Rte_RxTimeout_BunkH1UnlockButtonStatus_oLECM1_Cab_02P_oCabSubnet_99f14132_Rx : 1;
  Rte_BitType Rte_RxTimeout_ButtonAuth_rqst_oVMCU_BB2_01P_oBackbone2_f5cc0ebb_Rx : 1;
  Rte_BitType Rte_RxTimeout_CCActive_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_143627fe_Rx : 1;
  Rte_BitType Rte_RxTimeout_CCEnableRequest_ISig_4_oHMIIOM_BB2_03P_oBackbone2_53582b1d_Rx : 1;
  Rte_BitType Rte_RxTimeout_CCStates_ISig_4_oVMCU_BB2_52P_oBackbone2_62c12527_Rx : 1;
  Rte_BitType Rte_RxTimeout_CM_Status_ISig_4_oDACU_BB2_02P_oBackbone2_1c558e89_Rx : 1;
  Rte_BitType Rte_RxTimeout_CabBeaconLightFeedback_Status_ISig_4_oVMCU_BB2_08P_oBackbone2_6f6463b5_Rx : 1;
  Rte_BitType Rte_RxTimeout_CabFrontSpotFeedback_Status_oVMCU_BB2_08P_oBackbone2_d9f3ed72_Rx : 1;
  Rte_BitType Rte_RxTimeout_CabRoofSignLightFeedback_stat_oVMCU_BB2_08P_oBackbone2_350ebaea_Rx : 1;
  Rte_BitType Rte_RxTimeout_CabRoofSpotFeedback_Status_oVMCU_BB2_08P_oBackbone2_ae9c92bc_Rx : 1;
  Rte_BitType Rte_RxTimeout_CabTrailerBodyLgthnFdbk_stat_oVMCU_BB2_08P_oBackbone2_8622cf32_Rx : 1;
  Rte_BitType Rte_RxTimeout_CatalystTankLevel_ISig_3_oACM_BB1_01P_oBackbone1J1939_036544ac_Rx : 1;
  Rte_BitType Rte_RxTimeout_ChangeKneelACK_ISig_4_oVMCU_BB2_20P_oBackbone2_9adade9e_Rx : 1;
  Rte_BitType Rte_RxTimeout_ClutchSwitch_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_29916b2c_Rx : 1;
  Rte_BitType Rte_RxTimeout_CtaBB_Horn_rqst_oBBM_BB2_01P_oBackbone2_6037b38a_Rx : 1;
  Rte_BitType Rte_RxTimeout_DASApplicationStatus_ISig_4_oDACU_BB2_02P_oBackbone2_f1591292_Rx : 1;
  Rte_BitType Rte_RxTimeout_DaytimeRunningLight_Indication_oVMCU_BB2_03P_oBackbone2_24b27437_Rx : 1;
  Rte_BitType Rte_RxTimeout_Driver1TimeRelatedStates_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_2d2093d1_Rx : 1;
  Rte_BitType Rte_RxTimeout_Driver1WorkingState_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_edae4574_Rx : 1;
  Rte_BitType Rte_RxTimeout_Driver2TimeRelatedStates_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_4b2446e1_Rx : 1;
  Rte_BitType Rte_RxTimeout_Driver2WorkingState_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_ba8c6926_Rx : 1;
  Rte_BitType Rte_RxTimeout_DriverCardDriver1_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_800ed341_Rx : 1;
  Rte_BitType Rte_RxTimeout_DriverCardDriver2_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_cde6d326_Rx : 1;
  Rte_BitType Rte_RxTimeout_DriverDoorAjar_stat_ISig_10_oDDM_Sec_01P_oSecuritySubnet_81afb630_Rx : 1;
  Rte_BitType Rte_RxTimeout_DriverDoorLatch_stat_oDDM_Sec_01P_oSecuritySubnet_68da2bcf_Rx : 1;
  Rte_BitType Rte_RxTimeout_DriverMemory_rqst_ISig_4_oHMIIOM_BB2_08P_oBackbone2_a1568d13_Rx : 1;
  Rte_BitType Rte_RxTimeout_DrivingLightPlus_Indication_oVMCU_BB2_03P_oBackbone2_a98387bf_Rx : 1;
  Rte_BitType Rte_RxTimeout_DrivingLight_Indication_oVMCU_BB2_03P_oBackbone2_db0b239c_Rx : 1;
  Rte_BitType Rte_RxTimeout_DynamicCode_rqst_oHMIIOM_BB2_09P_oBackbone2_3c032dbe_Rx : 1;
  Rte_BitType Rte_RxTimeout_ECSStandByReqWRC_oWRCS_Cab_01P_oCabSubnet_e31f608b_Rx : 1;
  Rte_BitType Rte_RxTimeout_ECSStandbyAllowed_ISig_4_oVMCU_BB2_20P_oBackbone2_38626bd2_Rx : 1;
  Rte_BitType Rte_RxTimeout_ElectricalLoadReduction_rqst_ISig_4_oVMCU_BB2_52P_oBackbone2_8e6be74b_Rx : 1;
  Rte_BitType Rte_RxTimeout_EmergencyDoorsUnlock_rqst_oSRS_Cab_01P_oCabSubnet_12455702_Rx : 1;
  Rte_BitType Rte_RxTimeout_EngineCoolantTemp_stat_ISig_3_oET1_X_EMS_oBackbone1J1939_01eda871_Rx : 1;
  Rte_BitType Rte_RxTimeout_EngineFuelRate_ISig_3_oLFE_X_EMS_oBackbone1J1939_dd8e08ba_Rx : 1;
  Rte_BitType Rte_RxTimeout_EngineGasRate_oEMS_BB2_05P_oBackbone2_96ec243f_Rx : 1;
  Rte_BitType Rte_RxTimeout_EnginePercentLoadAtCurrentSpd_ISig_3_oEEC2_X_EMS_oBackbone1J1939_2e4dbdde_Rx : 1;
  Rte_BitType Rte_RxTimeout_EngineRetarderTorqueMode_ISig_3_oERC1_X_EMSRet_oBackbone1J1939_7b2fe6f4_Rx : 1;
  Rte_BitType Rte_RxTimeout_EngineRunningTime_ISig_4_oEMS_BB2_08P_oBackbone2_50a8aaa0_Rx : 1;
  Rte_BitType Rte_RxTimeout_EngineSpeedControlStatus_ISig_4_oVMCU_BB2_04P_oBackbone2_c5c1a7aa_Rx : 1;
  Rte_BitType Rte_RxTimeout_EngineSpeed_ISig_3_oEEC1_X_EMS_oBackbone1J1939_56ecedf7_Rx : 1;
  Rte_BitType Rte_RxTimeout_EngineStartAuth_rqst_oEMS_BB2_01P_oBackbone2_285ce89b_Rx : 1;
  Rte_BitType Rte_RxTimeout_EngineTotalFuelConsumed_ISig_4_oEMS_BB2_04P_oBackbone2_ab6106b8_Rx : 1;
  Rte_BitType Rte_RxTimeout_EngineTotalLngConsumed_ISig_4_oEMS_BB2_13P_oBackbone2_cfe79fd2_Rx : 1;
  Rte_BitType Rte_RxTimeout_ExtraAxleSteeringFunctionStat_ISig_4_oVMCU_BB2_08P_oBackbone2_e7241d0d_Rx : 1;
  Rte_BitType Rte_RxTimeout_ExtraBBContainerUnlockStatus_oVMCU_BB2_54P_oBackbone2_0f9800fc_Rx : 1;
  Rte_BitType Rte_RxTimeout_ExtraBBCraneStatus_ISig_4_oVMCU_BB2_54P_oBackbone2_bd0882a3_Rx : 1;
  Rte_BitType Rte_RxTimeout_ExtraBBSlidable5thWheelStatus_oVMCU_BB2_54P_oBackbone2_723450b2_Rx : 1;
  Rte_BitType Rte_RxTimeout_ExtraBBTailLiftStatus_ISig_4_oVMCU_BB2_54P_oBackbone2_0401d03c_Rx : 1;
  Rte_BitType Rte_RxTimeout_ExtraSideMarkers_FunctStat_oVMCU_BB2_73P_oBackbone2_34f556e9_Rx : 1;
  Rte_BitType Rte_RxTimeout_FCW_Status_ISig_4_oDACU_BB2_02P_oBackbone2_da223606_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP1ResponseErrorL1_oFSP1_Frame_L1_oLIN00_b9e1fc6c_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP1ResponseErrorL2_oFSP1_Frame_L2_oLIN01_368cf224_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP1ResponseErrorL3_oFSP1_Frame_L3_oLIN02_f85c49d4_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP1ResponseErrorL4_oFSP1_Frame_L4_oLIN03_f327e8f5_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP1ResponseErrorL5_oFSP1_Frame_L5_oLIN04_3a9a971c_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP1SwitchStatusL1_oFSP1_Frame_L1_oLIN00_e78b81b2_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP1SwitchStatusL2_oFSP1_Frame_L2_oLIN01_68e68ffa_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP1SwitchStatusL3_oFSP1_Frame_L3_oLIN02_a636340a_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP1SwitchStatusL4_oFSP1_Frame_L4_oLIN03_ad4d952b_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP1SwitchStatusL5_oFSP1_Frame_L5_oLIN04_64f0eac2_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP2ResponseErrorL1_oFSP2_Frame_L1_oLIN00_a793a78f_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP2ResponseErrorL2_oFSP2_Frame_L2_oLIN01_28fea9c7_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP2ResponseErrorL3_oFSP2_Frame_L3_oLIN02_e62e1237_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP2SwitchStatusL1_oFSP2_Frame_L1_oLIN00_ee70e556_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP2SwitchStatusL2_oFSP2_Frame_L2_oLIN01_611deb1e_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP2SwitchStatusL3_oFSP2_Frame_L3_oLIN02_afcd50ee_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP3ResponseErrorL2_oFSP3_Frame_L2_oLIN01_22d09f66_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP3SwitchStatusL2_oFSP3_Frame_L2_oLIN01_664b3742_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP4ResponseErrorL2_oFSP4_Frame_L2_oLIN01_141a1e01_Rx : 1;
  Rte_BitType Rte_RxTimeout_FSP4SwitchStatusL2_oFSP4_Frame_L2_oLIN01_72eb22d6_Rx : 1;
  Rte_BitType Rte_RxTimeout_FerryFunctionStatus_ISig_4_oVMCU_BB2_20P_oBackbone2_5e04e3f4_Rx : 1;
  Rte_BitType Rte_RxTimeout_FerryFunctionSwitchChangeACK_ISig_4_oVMCU_BB2_01P_oBackbone2_71f009e5_Rx : 1;
  Rte_BitType Rte_RxTimeout_FrontFog_Indication_oVMCU_BB2_03P_oBackbone2_92113a12_Rx : 1;
  Rte_BitType Rte_RxTimeout_FrtAxleHydroActive_Status_oVMCU_BB2_20P_oBackbone2_280ed8ff_Rx : 1;
  Rte_BitType Rte_RxTimeout_FuelLevel_ISig_3_oVMCU_BB1_03P_oBackbone1J1939_0f23d9f2_Rx : 1;
  Rte_BitType Rte_RxTimeout_GearBoxUnlockAuth_rqst_oTECU_BB2_01P_oBackbone2_d45ce9c0_Rx : 1;
  Rte_BitType Rte_RxTimeout_GrossCombinationVehicleWeight_ISig_3_oCVW_X_EBS_oBackbone1J1939_81d6bcd8_Rx : 1;
  Rte_BitType Rte_RxTimeout_HandlingInformation_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_8cdf696e_Rx : 1;
  Rte_BitType Rte_RxTimeout_HeightAdjustmentAllowed_ISig_4_oVMCU_BB2_02P_oBackbone2_3a236070_Rx : 1;
  Rte_BitType Rte_RxTimeout_HighResEngineTotalFuelUsed_ISig_3_oHRLFC_X_EMS_oBackbone1J1939_9a2f5b32_Rx : 1;
  Rte_BitType Rte_RxTimeout_InstantaneousFuelEconomy_ISig_3_oLFE_X_EMS_oBackbone1J1939_a1c216ba_Rx : 1;
  Rte_BitType Rte_RxTimeout_InteriorLightLevelInd_cmd_oVMCU_BB2_03P_oBackbone2_3f259764_Rx : 1;
  Rte_BitType Rte_RxTimeout_KeyAuthentication_rqst_oVMCU_BB2_04P_oBackbone2_c5ca7983_Rx : 1;
  Rte_BitType Rte_RxTimeout_KeyPosition_ISig_4_oVMCU_BB2_01P_oBackbone2_666d9433_Rx : 1;
  Rte_BitType Rte_RxTimeout_KeyfobInCabPresencePS_rqst_oVMCU_BB2_82P_oBackbone2_c182d016_Rx : 1;
  Rte_BitType Rte_RxTimeout_KneelingStatusHMI_ISig_4_oVMCU_BB2_20P_oBackbone2_755dcb67_Rx : 1;
  Rte_BitType Rte_RxTimeout_LCSSystemStatus_oDACU_BB2_02P_oBackbone2_a47552fc_Rx : 1;
  Rte_BitType Rte_RxTimeout_LIN_BunkH2PowerWinCloseDSBtn_s_oLECM2toCIOM_FR2_L1_oLIN00_9fa2715d_Rx : 1;
  Rte_BitType Rte_RxTimeout_LIN_BunkH2PowerWinClosePSBtn_s_oLECM2toCIOM_FR2_L1_oLIN00_281d2c26_Rx : 1;
  Rte_BitType Rte_RxTimeout_LIN_BunkH2PowerWinOpenDSBtn_st_oLECM2toCIOM_FR2_L1_oLIN00_0df6d397_Rx : 1;
  Rte_BitType Rte_RxTimeout_LIN_BunkH2PowerWinOpenPSBtn_st_oLECM2toCIOM_FR2_L1_oLIN00_ca96c47e_Rx : 1;
  Rte_BitType Rte_RxTimeout_LKSApplicationStatus_ISig_4_oDACU_BB2_02P_oBackbone2_a1929cc2_Rx : 1;
  Rte_BitType Rte_RxTimeout_LKSCorrectiveSteeringStatus_ISig_4_oDACU_BB2_02P_oBackbone2_5bf2e494_Rx : 1;
  Rte_BitType Rte_RxTimeout_LevelControlInformation_ISig_4_oVMCU_BB2_02P_oBackbone2_c3136e85_Rx : 1;
  Rte_BitType Rte_RxTimeout_LiftAxle1PositionStatus_ISig_4_oVMCU_BB2_02P_oBackbone2_8f87d670_Rx : 1;
  Rte_BitType Rte_RxTimeout_LiftAxle1UpRequestACK_ISig_4_oVMCU_BB2_02P_oBackbone2_fa0577fd_Rx : 1;
  Rte_BitType Rte_RxTimeout_LiftAxle2UpRequestACK_ISig_4_oVMCU_BB2_02P_oBackbone2_c5ce0968_Rx : 1;
  Rte_BitType Rte_RxTimeout_LngTank1RemainingGasVolume_ISig_3_oACM_BB1_01P_oBackbone1J1939_8a2851cd_Rx : 1;
  Rte_BitType Rte_RxTimeout_LngTank2RemainingGasVolume_ISig_3_oACM_BB1_01P_oBackbone1J1939_ec2c84fd_Rx : 1;
  Rte_BitType Rte_RxTimeout_LoadDistributionALDChangeACK_ISig_4_oVMCU_BB2_54P_oBackbone2_0286104b_Rx : 1;
  Rte_BitType Rte_RxTimeout_LoadDistributionChangeACK_ISig_4_oVMCU_BB2_02P_oBackbone2_3f2946ee_Rx : 1;
  Rte_BitType Rte_RxTimeout_LoadDistributionFuncSelected_ISig_4_oVMCU_BB2_02P_oBackbone2_e30d445f_Rx : 1;
  Rte_BitType Rte_RxTimeout_LoadDistributionRequestedACK_ISig_4_oVMCU_BB2_52P_oBackbone2_27f055a3_Rx : 1;
  Rte_BitType Rte_RxTimeout_LoadDistributionSelected_ISig_4_oVMCU_BB2_02P_oBackbone2_91087134_Rx : 1;
  Rte_BitType Rte_RxTimeout_MinutesUTC_ISig_3_oTD_X_HMIIOM_oBackbone1J1939_b5b3e478_Rx : 1;
  Rte_BitType Rte_RxTimeout_MirrorHeat_rqst_ddm_oDDM_Sec_01P_oSecuritySubnet_f2c6892d_Rx : 1;
  Rte_BitType Rte_RxTimeout_MirrorHeatingMode_ISig_4_oVMCU_BB2_03P_oBackbone2_93dcd786_Rx : 1;
  Rte_BitType Rte_RxTimeout_ParkingLight_Indication_oVMCU_BB2_03P_oBackbone2_e64048d9_Rx : 1;
  Rte_BitType Rte_RxTimeout_PassengerDoorAjar_stat_ISig_10_oPDM_Sec_01P_oSecuritySubnet_15fc3894_Rx : 1;
  Rte_BitType Rte_RxTimeout_PassengerDoorLatch_stat_oPDM_Sec_01P_oSecuritySubnet_7ad7cbe3_Rx : 1;
  Rte_BitType Rte_RxTimeout_PhoneButtonIndication_cmd_oHMIIOM_BB2_07P_oBackbone2_1c062854_Rx : 1;
  Rte_BitType Rte_RxTimeout_PinCodeEntered_value_oHMIIOM_BB2_09P_oBackbone2_7f89cada_Rx : 1;
  Rte_BitType Rte_RxTimeout_PloughLampModeStatus_ISig_4_oBBM_BB2_01P_oBackbone2_ce1fdf4f_Rx : 1;
  Rte_BitType Rte_RxTimeout_PredeliveryModeIndication_cmd_ISig_4_oVMCU_BB2_04P_oBackbone2_eefcbaa0_Rx : 1;
  Rte_BitType Rte_RxTimeout_Pto1Status_ISig_4_oVMCU_BB2_07P_oBackbone2_c7aca240_Rx : 1;
  Rte_BitType Rte_RxTimeout_Pto2Indication_oVMCU_BB2_07P_oBackbone2_26780e74_Rx : 1;
  Rte_BitType Rte_RxTimeout_Pto2Status_ISig_4_oVMCU_BB2_07P_oBackbone2_1437bebb_Rx : 1;
  Rte_BitType Rte_RxTimeout_Pto4Status_ISig_4_oVMCU_BB2_07P_oBackbone2_6870810c_Rx : 1;
  Rte_BitType Rte_RxTimeout_PtosStatus_ISig_4_oVMCU_BB2_07P_oBackbone2_9464094a_Rx : 1;
  Rte_BitType Rte_RxTimeout_RSLMgrRptTRSLEnabledOpStat_oVMCU_BB2_08P_oBackbone2_748e4c57_Rx : 1;
  Rte_BitType Rte_RxTimeout_RampLevelRequestACK_ISig_4_oVMCU_BB2_07P_oBackbone2_a790dd8f_Rx : 1;
  Rte_BitType Rte_RxTimeout_RampLevelStorageAck_ISig_4_oVMCU_BB2_02P_oBackbone2_4026e627_Rx : 1;
  Rte_BitType Rte_RxTimeout_RearAxleSteeringFunctionStatus_oEBS_BB1_02P_oBackbone1J1939_101ad651_Rx : 1;
  Rte_BitType Rte_RxTimeout_RearFog_Indication_oVMCU_BB2_03P_oBackbone2_fa31dca4_Rx : 1;
  Rte_BitType Rte_RxTimeout_RegenerationIndication_oHMIIOM_BB2_24P_oBackbone2_777f5711_Rx : 1;
  Rte_BitType Rte_RxTimeout_Regeneration_DeviceInd_oHMIIOM_BB2_24P_oBackbone2_86c1e4d1_Rx : 1;
  Rte_BitType Rte_RxTimeout_ResponseErrorCCFW_oCCFWtoCIOM_L4_oLIN03_38ff59f8_Rx : 1;
  Rte_BitType Rte_RxTimeout_ResponseErrorDLFW_oDLFWtoCIOM_L4_oLIN03_a0bc7d9c_Rx : 1;
  Rte_BitType Rte_RxTimeout_ResponseErrorELCP1_oELCP1toCIOM_L4_oLIN03_a9bf457f_Rx : 1;
  Rte_BitType Rte_RxTimeout_ResponseErrorELCP2_oELCP2toCIOM_L4_oLIN03_a23bb7d3_Rx : 1;
  Rte_BitType Rte_RxTimeout_ResponseErrorILCP1_oILCP1toCIOM_L1_oLIN00_44250bf0_Rx : 1;
  Rte_BitType Rte_RxTimeout_ResponseErrorILCP2_oILCP2toCIOM_L4_oLIN03_9e48a682_Rx : 1;
  Rte_BitType Rte_RxTimeout_ResponseErrorLECM2_oLECM2toCIOM_FR1_L1_oLIN00_ed8050df_Rx : 1;
  Rte_BitType Rte_RxTimeout_ResponseErrorLECMBasic_oLECMBasic2CIOM_L1_oLIN00_9a0b7e5e_Rx : 1;
  Rte_BitType Rte_RxTimeout_ResponseErrorRCECS_oRCECStoCIOM_L5_oLIN04_07d94fb4_Rx : 1;
  Rte_BitType Rte_RxTimeout_RetarderTorqueMode_ISig_3_oERC1_X_RECU_oBackbone1J1939_68c292d8_Rx : 1;
  Rte_BitType Rte_RxTimeout_ReverseGearEngaged_ISig_4_oVMCU_BB2_01P_oBackbone2_ea2f14fc_Rx : 1;
  Rte_BitType Rte_RxTimeout_RideHeightStorageAck_ISig_4_oVMCU_BB2_02P_oBackbone2_af65dfcb_Rx : 1;
  Rte_BitType Rte_RxTimeout_SG_AlmClkCurAlarm_stat_sg_oHMIIOM_BB2_10P_oBackbone2_16a10c0a_Rx : 1;
  Rte_BitType Rte_RxTimeout_SG_BnkH1IntLghtMMenu_stat_sg_oLECM1_Cab_02P_oCabSubnet_72949da5_Rx : 1;
  Rte_BitType Rte_RxTimeout_SG_FMS1_sg_ISig_3_oFMS1_X_HMIIOM_oBackbone1J1939_a2ca12fb_Rx : 1;
  Rte_BitType Rte_RxTimeout_SG_FPBRMMIStat_sg_ISig_4_oVMCU_BB2_74P_oBackbone2_6ccb74d8_Rx : 1;
  Rte_BitType Rte_RxTimeout_SG_IntLightMode_CoreRqst_sg_oVMCU_BB2_03P_oBackbone2_fb45020d_Rx : 1;
  Rte_BitType Rte_RxTimeout_SG_LIN_BunkH2PHTi_rqs_sg_oLECM2toCIOM_FR3_L1_oLIN00_0bf5fe80_Rx : 1;
  Rte_BitType Rte_RxTimeout_SG_OilPrediction_sg_oEMS_BB2_06P_oBackbone2_f16954dd_Rx : 1;
  Rte_BitType Rte_RxTimeout_SWSpdCtrlButtonsStatus1_oHMIIOM_BB2_01P_oBackbone2_f235f469_Rx : 1;
  Rte_BitType Rte_RxTimeout_SWSpeedControlAdjustMode_oHMIIOM_BB2_01P_oBackbone2_4c33da86_Rx : 1;
  Rte_BitType Rte_RxTimeout_SecondsUTC_ISig_3_oTD_X_HMIIOM_oBackbone1J1939_5a9bc5ac_Rx : 1;
  Rte_BitType Rte_RxTimeout_ServiceBrakeAirPrsCircuit1_ISig_3_oAIR1_X_VMCU_oBackbone1J1939_e8caeb17_Rx : 1;
  Rte_BitType Rte_RxTimeout_ServiceBrakeAirPrsCircuit2_ISig_3_oAIR1_X_VMCU_oBackbone1J1939_b2abd477_Rx : 1;
  Rte_BitType Rte_RxTimeout_SpecialLightSituation_ind_oVMCU_BB2_52P_oBackbone2_bb95932a_Rx : 1;
  Rte_BitType Rte_RxTimeout_SpeedLockingInhibition_stat_oHMIIOM_BB2_08P_oBackbone2_0bfbe2ed_Rx : 1;
  Rte_BitType Rte_RxTimeout_StopLevelChangeAck_ISig_4_oVMCU_BB2_02P_oBackbone2_5eeeb44a_Rx : 1;
  Rte_BitType Rte_RxTimeout_Synch_Unsynch_Mode_stat_oHMIIOM_BB2_08P_oBackbone2_8a425ab2_Rx : 1;
  Rte_BitType Rte_RxTimeout_SystemEvent_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_5a5c5e53_Rx : 1;
  Rte_BitType Rte_RxTimeout_TachographPerformance_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_1bf17036_Rx : 1;
  Rte_BitType Rte_RxTimeout_TachographVehicleSpeed_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_78a7f516_Rx : 1;
  Rte_BitType Rte_RxTimeout_TotalVehicleDistanceHighRes_ISig_3_oVDHR_X_VMCU_oBackbone1J1939_6b802705_Rx : 1;
  Rte_BitType Rte_RxTimeout_TrailerBodyLampDI_stat_oVMCU_BB2_80P_oBackbone2_47992f30_Rx : 1;
  Rte_BitType Rte_RxTimeout_TrailerBodyLampFdbk_stat_oVMCU_BB2_80P_oBackbone2_fa358fbc_Rx : 1;
  Rte_BitType Rte_RxTimeout_TransferCaseNeutral_Ack_oVMCU_BB2_73P_oBackbone2_3029afb8_Rx : 1;
  Rte_BitType Rte_RxTimeout_TransferCaseNeutral_status_oVMCU_BB2_29P_oBackbone2_0936e4a4_Rx : 1;
  Rte_BitType Rte_RxTimeout_TransferCasePTOEngaged_ISig_4_oVMCU_BB2_29P_oBackbone2_9e9887d1_Rx : 1;
  Rte_BitType Rte_RxTimeout_VehFrontAxle1CalibratedLoad_ISig_4_oHMIIOM_BB2_11P_oBackbone2_b768cee7_Rx : 1;
  Rte_BitType Rte_RxTimeout_VehRearAxle1CalibratedLoad_ISig_4_oHMIIOM_BB2_12P_oBackbone2_6732f367_Rx : 1;
  Rte_BitType Rte_RxTimeout_VehRearAxle2CalibratedLoad_ISig_4_oHMIIOM_BB2_12P_oBackbone2_2dab8413_Rx : 1;
  Rte_BitType Rte_RxTimeout_VehRearAxle3CalibratedLoad_ISig_4_oHMIIOM_BB2_12P_oBackbone2_a2f3ab00_Rx : 1;
  Rte_BitType Rte_RxTimeout_VehicleMode_ISig_3_oVMCU_BB1_01P_oBackbone1J1939_997c94c6_Rx : 1;
  Rte_BitType Rte_RxTimeout_VehicleMotion_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_c4c069be_Rx : 1;
  Rte_BitType Rte_RxTimeout_VehicleOverspeed_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_b3d80892_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRC5thWheelRequest_oWRCS_Cab_02P_oCabSubnet_6e07aa44_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCAirSuspensionStopRequest_oWRCS_Cab_01P_oCabSubnet_18c85d80_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCAux1Request_oWRCS_Cab_01P_oCabSubnet_3d62fac1_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCAux2Request_oWRCS_Cab_01P_oCabSubnet_66754bd4_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCAux3Request_oWRCS_Cab_01P_oCabSubnet_5087db27_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCAux4Request_oWRCS_Cab_01P_oCabSubnet_d05a29fe_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCAux5Request_oWRCS_Cab_01P_oCabSubnet_e6a8b90d_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCAux6Request_oWRCS_Cab_01P_oCabSubnet_bdbf0818_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCBeaconRequest_oWRCS_Cab_02P_oCabSubnet_7ba25fea_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCCabBodyWLightsRqst_oWRCS_Cab_02P_oCabSubnet_b2a0874a_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCCraneRequest_oWRCS_Cab_01P_oCabSubnet_95c4579e_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCLevelAdjustmentAction_oWRCS_Cab_01P_oCabSubnet_810159ff_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCLevelAdjustmentAxles_oWRCS_Cab_01P_oCabSubnet_b1b2524c_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCLevelAdjustmentStroke_oWRCS_Cab_01P_oCabSubnet_e59ce5a1_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCLevelUserMemoryAction_oWRCS_Cab_02P_oCabSubnet_42c76a61_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCLevelUserMemory_oWRCS_Cab_02P_oCabSubnet_d79fc958_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCLockButtonStatus_oWRCS_Cab_01P_oCabSubnet_762ddecb_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCRollRequest_oWRCS_Cab_01P_oCabSubnet_47562199_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCTailLiftRequest_oWRCS_Cab_01P_oCabSubnet_a1fd4993_Rx : 1;
  Rte_BitType Rte_RxTimeout_WRCUnlockButtonStatus_oWRCS_Cab_01P_oCabSubnet_fd05435c_Rx : 1;
  Rte_BitType Rte_RxTimeout_WheelBasedVehicleSpeed_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_e3ff6d4c_Rx : 1;
  Rte_BitType Rte_RxTimeout_WrcEngSpdCtrlDecreaseButtStat_oWRCS_Cab_02P_oCabSubnet_f90375a0_Rx : 1;
  Rte_BitType Rte_RxTimeout_WrcEngSpdCtrlEnableStatus_oWRCS_Cab_02P_oCabSubnet_c15f6325_Rx : 1;
  Rte_BitType Rte_RxTimeout_WrcEngSpdCtrlIncreaseButtStat_oWRCS_Cab_02P_oCabSubnet_7a1f067c_Rx : 1;
  Rte_BitType Rte_RxTimeout_WrcEngSpdCtrlResumeButtonStat_oWRCS_Cab_02P_oCabSubnet_09c53237_Rx : 1;
  Rte_BitType Rte_RxTimeout_WrcPto1ButtonStatus_oWRCS_Cab_01P_oCabSubnet_9b7a6ac6_Rx : 1;
  Rte_BitType Rte_RxTimeout_WrcPto2ButtonStatus_oWRCS_Cab_01P_oCabSubnet_d6926aa1_Rx : 1;
  Rte_BitType Rte_RxTimeout_WrcPto3ButtonStatus_oWRCS_Cab_01P_oCabSubnet_5b1a9743_Rx : 1;
  Rte_BitType Rte_RxTimeout_WrcPto4ButtonStatus_oWRCS_Cab_01P_oCabSubnet_4d426a6f_Rx : 1;
  Rte_BitType Rte_RxTimeout_XRSLStates_oVMCU_BB2_51P_oBackbone2_06570c1b_Rx : 1;
} Rte_RxTimeoutFlagsType;

#  define RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Rte_RxTimeoutFlagsType, RTE_VAR_ZERO_INIT) Rte_RxTimeoutFlags;

#  define RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


typedef struct
{
  Rte_BitType Rte_RxNeverReceived_ABSInhibit_DeviceIndication_oHMIIOM_BB2_07P_oBackbone2_ee1461e1_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ABSInhibitionStatus_ISig_3_oEBS_BB1_02P_oBackbone1J1939_14d96a66_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ACCEnableRqst_ISig_4_oHMIIOM_BB2_01P_oBackbone2_fe5931dd_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ASRHillHolderSwitch_ISig_3_oEBC1_X_EBS_oBackbone1J1939_ae8948e3_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_AcceleratorPedalPosition1_ISig_3_oVMCU_BB1_01P_oBackbone1J1939_7025a200_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_AcceleratorPedalStatus_ISig_4_oVMCU_BB2_52P_oBackbone2_c6b3ef9e_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ActualDrvlnRetdrPercentTorque_ISig_3_oERC1_X_RECU_oBackbone1J1939_b2bc1e25_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ActualEnginePercentTorque_ISig_3_oEEC1_X_EMS_oBackbone1J1939_cfb19d75_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ActualEngineRetarderPercentTrq_ISig_3_oERC1_X_EMSRet_oBackbone1J1939_d87209ba_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_AmbientAirTemperature_ISig_3_oAMB_X_VMCU_oBackbone1J1939_a102c408_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_AudioSystemStatus_ISig_4_oHMIIOM_BB2_07P_oBackbone2_4800fc7f_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_AudioVolumeIndicationCmd_ISig_4_oHMIIOM_BB2_07P_oBackbone2_88a309c1_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_AutorelockingMovements_stat_oAlarm_Sec_02P_oSecuritySubnet_c816460c_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_AuxSwitchBbLoad1_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_68beafdf_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_AuxSwitchBbLoad2_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_42021f57_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_AuxSwitchBbLoad3_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_ed468d10_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_AuxSwitchBbLoad4_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_177b7e47_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_AuxSwitchBbLoad5_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_b83fec00_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_AuxSwitchBbLoad6_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_92835c88_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_BBNetwBeaconLight_stat_oBBM_BB2_02P_oBackbone2_0af6e9b5_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_BBNetwBodyOrCabWrknLight_stat_oBBM_BB2_02P_oBackbone2_1a41cb1a_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_BBNetwWrknLightChassis_stat_oBBM_BB2_02P_oBackbone2_d6b6577a_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_BTStatus_oHMIIOM_BB2_07P_oBackbone2_2504e67b_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_BackToDriveReqACK_ISig_4_oVMCU_BB2_01P_oBackbone2_220da73b_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_BodyOrCabWorkingLightFdbk_stat_ISig_4_oVMCU_BB2_08P_oBackbone2_6dee2288_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_BrakeBlending_DeviceIndication_oHMIIOM_BB2_07P_oBackbone2_22c13ed8_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_BrakeSwitch_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_f9976b4d_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_BunkH1IntLghtActvnBtn_stat_oLECM1_Cab_02P_oCabSubnet_20e1888b_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_BunkH1IntLghtDirAccsDnBtn_stat_oLECM1_Cab_02P_oCabSubnet_9b517646_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_BunkH1IntLghtDirAccsUpBtn_stat_oLECM1_Cab_02P_oCabSubnet_a6085fb2_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_BunkH1LockButtonStatus_oLECM1_Cab_02P_oCabSubnet_493c9af1_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_BunkH1RoofhatchCloseBtn_Stat_oLECM1_Cab_02P_oCabSubnet_fa799afe_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_BunkH1RoofhatchOpenBtn_Stat_oLECM1_Cab_02P_oCabSubnet_fde56247_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_BunkH1UnlockButtonStatus_oLECM1_Cab_02P_oCabSubnet_99f14132_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ButtonAuth_rqst_oVMCU_BB2_01P_oBackbone2_f5cc0ebb_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_CCActive_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_143627fe_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_CCEnableRequest_ISig_4_oHMIIOM_BB2_03P_oBackbone2_53582b1d_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_CCStates_ISig_4_oVMCU_BB2_52P_oBackbone2_62c12527_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_CM_Status_ISig_4_oDACU_BB2_02P_oBackbone2_1c558e89_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_CabBeaconLightFeedback_Status_ISig_4_oVMCU_BB2_08P_oBackbone2_6f6463b5_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_CabRoofSignLightFeedback_stat_oVMCU_BB2_08P_oBackbone2_350ebaea_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_CabTrailerBodyLgthnFdbk_stat_oVMCU_BB2_08P_oBackbone2_8622cf32_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_CatalystTankLevel_ISig_3_oACM_BB1_01P_oBackbone1J1939_036544ac_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ChangeKneelACK_ISig_4_oVMCU_BB2_20P_oBackbone2_9adade9e_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ClutchSwitch_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_29916b2c_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_CtaBB_Horn_rqst_oBBM_BB2_01P_oBackbone2_6037b38a_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_DASApplicationStatus_ISig_4_oDACU_BB2_02P_oBackbone2_f1591292_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_DaytimeRunningLight_Indication_oVMCU_BB2_03P_oBackbone2_24b27437_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_Driver1TimeRelatedStates_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_2d2093d1_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_Driver1WorkingState_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_edae4574_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_Driver2TimeRelatedStates_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_4b2446e1_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_Driver2WorkingState_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_ba8c6926_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_DriverCardDriver1_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_800ed341_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_DriverCardDriver2_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_cde6d326_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_DriverDoorAjar_stat_ISig_10_oDDM_Sec_01P_oSecuritySubnet_81afb630_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_DriverMemory_rqst_ISig_4_oHMIIOM_BB2_08P_oBackbone2_a1568d13_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_DrivingLightPlus_Indication_oVMCU_BB2_03P_oBackbone2_a98387bf_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_DrivingLight_Indication_oVMCU_BB2_03P_oBackbone2_db0b239c_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_DynamicCode_rqst_oHMIIOM_BB2_09P_oBackbone2_3c032dbe_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ECSStandByReqWRC_oWRCS_Cab_01P_oCabSubnet_e31f608b_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ECSStandbyAllowed_ISig_4_oVMCU_BB2_20P_oBackbone2_38626bd2_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ElectricalLoadReduction_rqst_ISig_4_oVMCU_BB2_52P_oBackbone2_8e6be74b_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_EmergencyDoorsUnlock_rqst_oSRS_Cab_01P_oCabSubnet_12455702_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_EngineCoolantTemp_stat_ISig_3_oET1_X_EMS_oBackbone1J1939_01eda871_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_EngineFuelRate_ISig_3_oLFE_X_EMS_oBackbone1J1939_dd8e08ba_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_EngineGasRate_oEMS_BB2_05P_oBackbone2_96ec243f_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_EnginePercentLoadAtCurrentSpd_ISig_3_oEEC2_X_EMS_oBackbone1J1939_2e4dbdde_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_EngineRetarderTorqueMode_ISig_3_oERC1_X_EMSRet_oBackbone1J1939_7b2fe6f4_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_EngineRunningTime_ISig_4_oEMS_BB2_08P_oBackbone2_50a8aaa0_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_EngineSpeedControlStatus_ISig_4_oVMCU_BB2_04P_oBackbone2_c5c1a7aa_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_EngineSpeed_ISig_3_oEEC1_X_EMS_oBackbone1J1939_56ecedf7_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_EngineStartAuth_rqst_oEMS_BB2_01P_oBackbone2_285ce89b_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_EngineTotalFuelConsumed_ISig_4_oEMS_BB2_04P_oBackbone2_ab6106b8_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_EngineTotalLngConsumed_ISig_4_oEMS_BB2_13P_oBackbone2_cfe79fd2_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ExtraAxleSteeringFunctionStat_ISig_4_oVMCU_BB2_08P_oBackbone2_e7241d0d_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ExtraBBContainerUnlockStatus_oVMCU_BB2_54P_oBackbone2_0f9800fc_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ExtraBBCraneStatus_ISig_4_oVMCU_BB2_54P_oBackbone2_bd0882a3_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ExtraBBSlidable5thWheelStatus_oVMCU_BB2_54P_oBackbone2_723450b2_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ExtraBBTailLiftStatus_ISig_4_oVMCU_BB2_54P_oBackbone2_0401d03c_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FCW_Status_ISig_4_oDACU_BB2_02P_oBackbone2_da223606_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP1ResponseErrorL1_oFSP1_Frame_L1_oLIN00_b9e1fc6c_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP1ResponseErrorL2_oFSP1_Frame_L2_oLIN01_368cf224_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP1ResponseErrorL3_oFSP1_Frame_L3_oLIN02_f85c49d4_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP1ResponseErrorL4_oFSP1_Frame_L4_oLIN03_f327e8f5_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP1ResponseErrorL5_oFSP1_Frame_L5_oLIN04_3a9a971c_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP1SwitchStatusL1_oFSP1_Frame_L1_oLIN00_e78b81b2_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP1SwitchStatusL3_oFSP1_Frame_L3_oLIN02_a636340a_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP1SwitchStatusL4_oFSP1_Frame_L4_oLIN03_ad4d952b_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP1SwitchStatusL5_oFSP1_Frame_L5_oLIN04_64f0eac2_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP2ResponseErrorL1_oFSP2_Frame_L1_oLIN00_a793a78f_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP2ResponseErrorL2_oFSP2_Frame_L2_oLIN01_28fea9c7_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP2ResponseErrorL3_oFSP2_Frame_L3_oLIN02_e62e1237_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP2SwitchStatusL1_oFSP2_Frame_L1_oLIN00_ee70e556_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP2SwitchStatusL2_oFSP2_Frame_L2_oLIN01_611deb1e_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP2SwitchStatusL3_oFSP2_Frame_L3_oLIN02_afcd50ee_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP3ResponseErrorL2_oFSP3_Frame_L2_oLIN01_22d09f66_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP3SwitchStatusL2_oFSP3_Frame_L2_oLIN01_664b3742_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP4ResponseErrorL2_oFSP4_Frame_L2_oLIN01_141a1e01_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FSP4SwitchStatusL2_oFSP4_Frame_L2_oLIN01_72eb22d6_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FerryFunctionStatus_ISig_4_oVMCU_BB2_20P_oBackbone2_5e04e3f4_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FerryFunctionSwitchChangeACK_ISig_4_oVMCU_BB2_01P_oBackbone2_71f009e5_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FrontFog_Indication_oVMCU_BB2_03P_oBackbone2_92113a12_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FrtAxleHydroActive_Status_oVMCU_BB2_20P_oBackbone2_280ed8ff_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_FuelLevel_ISig_3_oVMCU_BB1_03P_oBackbone1J1939_0f23d9f2_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_GearBoxUnlockAuth_rqst_oTECU_BB2_01P_oBackbone2_d45ce9c0_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_GrossCombinationVehicleWeight_ISig_3_oCVW_X_EBS_oBackbone1J1939_81d6bcd8_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_HandlingInformation_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_8cdf696e_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_HeightAdjustmentAllowed_ISig_4_oVMCU_BB2_02P_oBackbone2_3a236070_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_HighResEngineTotalFuelUsed_ISig_3_oHRLFC_X_EMS_oBackbone1J1939_9a2f5b32_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_InstantaneousFuelEconomy_ISig_3_oLFE_X_EMS_oBackbone1J1939_a1c216ba_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_InteriorLightLevelInd_cmd_oVMCU_BB2_03P_oBackbone2_3f259764_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_KeyAuthentication_rqst_oVMCU_BB2_04P_oBackbone2_c5ca7983_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_KeyPosition_ISig_4_oVMCU_BB2_01P_oBackbone2_666d9433_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_KeyfobInCabPresencePS_rqst_oVMCU_BB2_82P_oBackbone2_c182d016_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_KneelingStatusHMI_ISig_4_oVMCU_BB2_20P_oBackbone2_755dcb67_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LIN_BunkH2PowerWinCloseDSBtn_s_oLECM2toCIOM_FR2_L1_oLIN00_9fa2715d_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LIN_BunkH2PowerWinClosePSBtn_s_oLECM2toCIOM_FR2_L1_oLIN00_281d2c26_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LIN_BunkH2PowerWinOpenDSBtn_st_oLECM2toCIOM_FR2_L1_oLIN00_0df6d397_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LIN_BunkH2PowerWinOpenPSBtn_st_oLECM2toCIOM_FR2_L1_oLIN00_ca96c47e_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LKSApplicationStatus_ISig_4_oDACU_BB2_02P_oBackbone2_a1929cc2_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LKSCorrectiveSteeringStatus_ISig_4_oDACU_BB2_02P_oBackbone2_5bf2e494_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LevelControlInformation_ISig_4_oVMCU_BB2_02P_oBackbone2_c3136e85_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LiftAxle1PositionStatus_ISig_4_oVMCU_BB2_02P_oBackbone2_8f87d670_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LiftAxle1UpRequestACK_ISig_4_oVMCU_BB2_02P_oBackbone2_fa0577fd_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LiftAxle2UpRequestACK_ISig_4_oVMCU_BB2_02P_oBackbone2_c5ce0968_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LngTank1RemainingGasVolume_ISig_3_oACM_BB1_01P_oBackbone1J1939_8a2851cd_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LngTank2RemainingGasVolume_ISig_3_oACM_BB1_01P_oBackbone1J1939_ec2c84fd_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LoadDistributionALDChangeACK_ISig_4_oVMCU_BB2_54P_oBackbone2_0286104b_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LoadDistributionChangeACK_ISig_4_oVMCU_BB2_02P_oBackbone2_3f2946ee_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LoadDistributionFuncSelected_ISig_4_oVMCU_BB2_02P_oBackbone2_e30d445f_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LoadDistributionRequestedACK_ISig_4_oVMCU_BB2_52P_oBackbone2_27f055a3_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_LoadDistributionSelected_ISig_4_oVMCU_BB2_02P_oBackbone2_91087134_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_MinutesUTC_ISig_3_oTD_X_HMIIOM_oBackbone1J1939_b5b3e478_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_MirrorHeat_rqst_ddm_oDDM_Sec_01P_oSecuritySubnet_f2c6892d_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_MirrorHeatingMode_ISig_4_oVMCU_BB2_03P_oBackbone2_93dcd786_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ParkingLight_Indication_oVMCU_BB2_03P_oBackbone2_e64048d9_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_PassengerDoorAjar_stat_ISig_10_oPDM_Sec_01P_oSecuritySubnet_15fc3894_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_PassengerDoorLatch_stat_oPDM_Sec_01P_oSecuritySubnet_7ad7cbe3_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_PhoneButtonIndication_cmd_oHMIIOM_BB2_07P_oBackbone2_1c062854_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_PinCodeEntered_value_oHMIIOM_BB2_09P_oBackbone2_7f89cada_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_PloughLampModeStatus_ISig_4_oBBM_BB2_01P_oBackbone2_ce1fdf4f_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_PredeliveryModeIndication_cmd_ISig_4_oVMCU_BB2_04P_oBackbone2_eefcbaa0_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_Pto1Status_ISig_4_oVMCU_BB2_07P_oBackbone2_c7aca240_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_Pto2Indication_oVMCU_BB2_07P_oBackbone2_26780e74_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_Pto2Status_ISig_4_oVMCU_BB2_07P_oBackbone2_1437bebb_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_Pto4Status_ISig_4_oVMCU_BB2_07P_oBackbone2_6870810c_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_PtosStatus_ISig_4_oVMCU_BB2_07P_oBackbone2_9464094a_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_RampLevelStorageAck_ISig_4_oVMCU_BB2_02P_oBackbone2_4026e627_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_RearAxleSteeringFunctionStatus_oEBS_BB1_02P_oBackbone1J1939_101ad651_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_RearFog_Indication_oVMCU_BB2_03P_oBackbone2_fa31dca4_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_RegenerationIndication_oHMIIOM_BB2_24P_oBackbone2_777f5711_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_Regeneration_DeviceInd_oHMIIOM_BB2_24P_oBackbone2_86c1e4d1_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ResponseErrorCCFW_oCCFWtoCIOM_L4_oLIN03_38ff59f8_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ResponseErrorDLFW_oDLFWtoCIOM_L4_oLIN03_a0bc7d9c_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ResponseErrorELCP1_oELCP1toCIOM_L4_oLIN03_a9bf457f_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ResponseErrorELCP2_oELCP2toCIOM_L4_oLIN03_a23bb7d3_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ResponseErrorLECM2_oLECM2toCIOM_FR1_L1_oLIN00_ed8050df_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ResponseErrorLECMBasic_oLECMBasic2CIOM_L1_oLIN00_9a0b7e5e_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ResponseErrorRCECS_oRCECStoCIOM_L5_oLIN04_07d94fb4_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_RetarderTorqueMode_ISig_3_oERC1_X_RECU_oBackbone1J1939_68c292d8_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ReverseGearEngaged_ISig_4_oVMCU_BB2_01P_oBackbone2_ea2f14fc_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_RideHeightStorageAck_ISig_4_oVMCU_BB2_02P_oBackbone2_af65dfcb_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_SG_AlmClkCurAlarm_stat_sg_oHMIIOM_BB2_10P_oBackbone2_16a10c0a_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_SG_BnkH1IntLghtMMenu_stat_sg_oLECM1_Cab_02P_oCabSubnet_72949da5_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_SG_FMS1_sg_ISig_3_oFMS1_X_HMIIOM_oBackbone1J1939_a2ca12fb_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_SG_FPBRMMIStat_sg_ISig_4_oVMCU_BB2_74P_oBackbone2_6ccb74d8_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_SG_IntLightMode_CoreRqst_sg_oVMCU_BB2_03P_oBackbone2_fb45020d_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_SG_LIN_BunkH2PHTi_rqs_sg_oLECM2toCIOM_FR3_L1_oLIN00_0bf5fe80_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_SG_OilPrediction_sg_oEMS_BB2_06P_oBackbone2_f16954dd_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_SWSpdCtrlButtonsStatus1_oHMIIOM_BB2_01P_oBackbone2_f235f469_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_SWSpeedControlAdjustMode_oHMIIOM_BB2_01P_oBackbone2_4c33da86_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ServiceBrakeAirPrsCircuit1_ISig_3_oAIR1_X_VMCU_oBackbone1J1939_e8caeb17_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_ServiceBrakeAirPrsCircuit2_ISig_3_oAIR1_X_VMCU_oBackbone1J1939_b2abd477_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_SpecialLightSituation_ind_oVMCU_BB2_52P_oBackbone2_bb95932a_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_SpeedLockingInhibition_stat_oHMIIOM_BB2_08P_oBackbone2_0bfbe2ed_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_StopLevelChangeAck_ISig_4_oVMCU_BB2_02P_oBackbone2_5eeeb44a_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_Synch_Unsynch_Mode_stat_oHMIIOM_BB2_08P_oBackbone2_8a425ab2_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_SystemEvent_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_5a5c5e53_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_TachographPerformance_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_1bf17036_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_TachographVehicleSpeed_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_78a7f516_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_TotalVehicleDistanceHighRes_ISig_3_oVDHR_X_VMCU_oBackbone1J1939_6b802705_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_TrailerBodyLampDI_stat_oVMCU_BB2_80P_oBackbone2_47992f30_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_TrailerBodyLampFdbk_stat_oVMCU_BB2_80P_oBackbone2_fa358fbc_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_TransferCaseNeutral_Ack_oVMCU_BB2_73P_oBackbone2_3029afb8_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_TransferCaseNeutral_status_oVMCU_BB2_29P_oBackbone2_0936e4a4_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_TransferCasePTOEngaged_ISig_4_oVMCU_BB2_29P_oBackbone2_9e9887d1_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_VehFrontAxle1CalibratedLoad_ISig_4_oHMIIOM_BB2_11P_oBackbone2_b768cee7_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_VehRearAxle1CalibratedLoad_ISig_4_oHMIIOM_BB2_12P_oBackbone2_6732f367_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_VehRearAxle2CalibratedLoad_ISig_4_oHMIIOM_BB2_12P_oBackbone2_2dab8413_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_VehRearAxle3CalibratedLoad_ISig_4_oHMIIOM_BB2_12P_oBackbone2_a2f3ab00_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_VehicleMode_ISig_3_oVMCU_BB1_01P_oBackbone1J1939_997c94c6_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_VehicleMotion_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_c4c069be_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_VehicleOverspeed_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_b3d80892_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRC5thWheelRequest_oWRCS_Cab_02P_oCabSubnet_6e07aa44_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCAirSuspensionStopRequest_oWRCS_Cab_01P_oCabSubnet_18c85d80_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCAux1Request_oWRCS_Cab_01P_oCabSubnet_3d62fac1_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCAux2Request_oWRCS_Cab_01P_oCabSubnet_66754bd4_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCAux3Request_oWRCS_Cab_01P_oCabSubnet_5087db27_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCAux4Request_oWRCS_Cab_01P_oCabSubnet_d05a29fe_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCAux5Request_oWRCS_Cab_01P_oCabSubnet_e6a8b90d_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCAux6Request_oWRCS_Cab_01P_oCabSubnet_bdbf0818_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCBeaconRequest_oWRCS_Cab_02P_oCabSubnet_7ba25fea_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCCabBodyWLightsRqst_oWRCS_Cab_02P_oCabSubnet_b2a0874a_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCCraneRequest_oWRCS_Cab_01P_oCabSubnet_95c4579e_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCLevelAdjustmentAction_oWRCS_Cab_01P_oCabSubnet_810159ff_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCLevelAdjustmentAxles_oWRCS_Cab_01P_oCabSubnet_b1b2524c_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCLevelAdjustmentStroke_oWRCS_Cab_01P_oCabSubnet_e59ce5a1_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCLevelUserMemoryAction_oWRCS_Cab_02P_oCabSubnet_42c76a61_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCLevelUserMemory_oWRCS_Cab_02P_oCabSubnet_d79fc958_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCLockButtonStatus_oWRCS_Cab_01P_oCabSubnet_762ddecb_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCRollRequest_oWRCS_Cab_01P_oCabSubnet_47562199_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCTailLiftRequest_oWRCS_Cab_01P_oCabSubnet_a1fd4993_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WRCUnlockButtonStatus_oWRCS_Cab_01P_oCabSubnet_fd05435c_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WheelBasedVehicleSpeed_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_e3ff6d4c_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WrcEngSpdCtrlDecreaseButtStat_oWRCS_Cab_02P_oCabSubnet_f90375a0_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WrcEngSpdCtrlEnableStatus_oWRCS_Cab_02P_oCabSubnet_c15f6325_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WrcEngSpdCtrlIncreaseButtStat_oWRCS_Cab_02P_oCabSubnet_7a1f067c_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WrcEngSpdCtrlResumeButtonStat_oWRCS_Cab_02P_oCabSubnet_09c53237_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WrcPto2ButtonStatus_oWRCS_Cab_01P_oCabSubnet_d6926aa1_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WrcPto3ButtonStatus_oWRCS_Cab_01P_oCabSubnet_5b1a9743_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_WrcPto4ButtonStatus_oWRCS_Cab_01P_oCabSubnet_4d426a6f_Rx : 1;
  Rte_BitType Rte_RxNeverReceived_XRSLStates_oVMCU_BB2_51P_oBackbone2_06570c1b_Rx : 1;
} Rte_RxNeverReceivedFlagsType;

#  define RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Rte_RxNeverReceivedFlagsType, RTE_VAR_ZERO_INIT) Rte_RxNeverReceivedFlags;

#  define RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


typedef struct
{
  Rte_BitType Rte_DirtyFlag_Application_Data_NVM_DriverAuth2_Ctrl_NVM : 1;
  Rte_BitType Rte_DirtyFlag_Application_Data_NVM_EngTraceHW_NvM_w : 1;
  Rte_BitType Rte_DirtyFlag_Application_Data_NVM_MUT_UICtrl_Difflock_NVM : 1;
  Rte_BitType Rte_DirtyFlag_Application_Data_NVM_MUT_UICtrl_Traction_NVM : 1;
  Rte_BitType Rte_DirtyFlag_Application_Data_NVM_PinCode_ctrl_NVM : 1;
  Rte_BitType Rte_DirtyFlag_Application_Data_NVM_RGW_HMICtrl_NVM : 1;
  Rte_BitType Rte_DirtyFlag_Application_Data_NVM_SCM_HMICtrl_NVM : 1;
  Rte_BitType Rte_DirtyFlag_Application_Data_NVM_VehicleAccess_Ctrl_NVM : 1;
  Rte_BitType Rte_DirtyFlag_Application_Data_NVM_VehicleModeDistribution_NVM : 1;
  Rte_BitType Rte_DirtyFlag_Calibration_Data_NVM_FSP_NVM : 1;
  Rte_BitType Rte_DirtyFlag_Keyfob_Radio_Com_NVM_KeyFobNVBlock : 1;
} Rte_DirtyFlagsType;

#  define RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Rte_DirtyFlagsType, RTE_VAR_ZERO_INIT) Rte_DirtyFlags;

#  define RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


typedef struct
{
  Rte_BitType Rte_NvBlockPendingFlag_Application_Data_NVM_DriverAuth2_Ctrl_NVM : 1;
  Rte_BitType Rte_NvBlockPendingFlag_Application_Data_NVM_EngTraceHW_NvM : 1;
  Rte_BitType Rte_NvBlockPendingFlag_Application_Data_NVM_MUT_UICtrl_Difflock_NVM : 1;
  Rte_BitType Rte_NvBlockPendingFlag_Application_Data_NVM_MUT_UICtrl_Traction_NVM : 1;
  Rte_BitType Rte_NvBlockPendingFlag_Application_Data_NVM_PinCode_ctrl_NVM : 1;
  Rte_BitType Rte_NvBlockPendingFlag_Application_Data_NVM_RGW_HMICtrl_NVM : 1;
  Rte_BitType Rte_NvBlockPendingFlag_Application_Data_NVM_SCM_HMICtrl_NVM : 1;
  Rte_BitType Rte_NvBlockPendingFlag_Application_Data_NVM_VehicleAccess_Ctrl_NVM : 1;
  Rte_BitType Rte_NvBlockPendingFlag_Application_Data_NVM_VehicleModeDistribution_NVM : 1;
  Rte_BitType Rte_NvBlockPendingFlag_Calibration_Data_NVM_FSP_NVM : 1;
  Rte_BitType Rte_NvBlockPendingFlag_Keyfob_Radio_Com_NVM_KeyFobNVBlock : 1;
} Rte_NvBlockPendingFlagsType;

#  define RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Rte_NvBlockPendingFlagsType, RTE_VAR_ZERO_INIT) Rte_NvBlockPendingFlags;

#  define RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * Buffer for inter-runnable variables
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(UInt32, RTE_VAR_NOINIT) Rte_Irv_CryptoDriverDoorLatch_Tx_VEC_CryptoProxyIdentificationNumber; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(UInt32, RTE_VAR_NOINIT) Rte_Irv_CryptoDriverKeyCyl_Rx_VEC_CryptoProxy_IdentificationNumber; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(VEC_CryptoProxy_IdentificationState_Type, RTE_VAR_NOINIT) Rte_Irv_CryptoDriverKeyCyl_Rx_VEC_CryptoProxy_IdentificationState; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(UInt32, RTE_VAR_NOINIT) Rte_Irv_CryptoDriverKeyCyl_Rx_VEC_CryptoProxy_NewReceivedIdentificationNumber; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(UInt32, RTE_VAR_NOINIT) Rte_Irv_CryptoEngineStart_Tx_VEC_CryptoProxyIdentificationNumber; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(UInt32, RTE_VAR_NOINIT) Rte_Irv_CryptoGearboxLock_Tx_VEC_CryptoProxyIdentificationNumber; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(UInt32, RTE_VAR_NOINIT) Rte_Irv_CryptoKeyAuth_Tx_VEC_CryptoProxyIdentificationNumber; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(UInt32, RTE_VAR_NOINIT) Rte_Irv_CryptoLockingSwitch_Rx_VEC_CryptoProxy_IdentificationNumber; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(VEC_CryptoProxy_IdentificationState_Type, RTE_VAR_NOINIT) Rte_Irv_CryptoLockingSwitch_Rx_VEC_CryptoProxy_IdentificationState; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(UInt32, RTE_VAR_NOINIT) Rte_Irv_CryptoLockingSwitch_Rx_VEC_CryptoProxy_NewReceivedIdentificationNumber; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(UInt32, RTE_VAR_NOINIT) Rte_Irv_CryptoLuggageCompartment_Tx_VEC_CryptoProxyIdentificationNumber; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(UInt32, RTE_VAR_NOINIT) Rte_Irv_CryptoPsngDoorLatch_Tx_VEC_CryptoProxyIdentificationNumber; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(UInt32, RTE_VAR_NOINIT) Rte_Irv_CryptoPsngKeyCyl_Rx_VEC_CryptoProxy_IdentificationNumber; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(VEC_CryptoProxy_IdentificationState_Type, RTE_VAR_NOINIT) Rte_Irv_CryptoPsngKeyCyl_Rx_VEC_CryptoProxy_IdentificationState; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(UInt32, RTE_VAR_NOINIT) Rte_Irv_CryptoPsngKeyCyl_Rx_VEC_CryptoProxy_NewReceivedIdentificationNumber; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(UInt32, RTE_VAR_NOINIT) Rte_Irv_CryptoReducedSetMode_Tx_VEC_CryptoProxyIdentificationNumber; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(UInt32, RTE_VAR_NOINIT) Rte_Irv_CryptoTheftAlarmActivation_Tx_VEC_CryptoProxyIdentificationNumber; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Days8bit_Fact025_T, RTE_VAR_NOINIT) Rte_Irv_DiagnosticComponent_IrvDayUTC; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Hours8bit_T, RTE_VAR_NOINIT) Rte_Irv_DiagnosticComponent_IrvHoursUTC; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Minutes8bit_T, RTE_VAR_NOINIT) Rte_Irv_DiagnosticComponent_IrvMinutesUTC; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Months8bit_T, RTE_VAR_NOINIT) Rte_Irv_DiagnosticComponent_IrvMonthUTC; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Seconds8bitFact025_T, RTE_VAR_NOINIT) Rte_Irv_DiagnosticComponent_IrvSecondsUTC; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Years8bit_T, RTE_VAR_NOINIT) Rte_Irv_DiagnosticComponent_IrvYearUTC; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_DiffLockPanel_LINMasterCtrl_Irv_IOCTL_DiffLockLinCtrl; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(VINCheckStatus_T, RTE_VAR_NOINIT) Rte_Irv_DriverAuthentication2_Ctrl_Vincheckstatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_ECSWiredRemote_LINMasterCtrl_Irv_IOCTL_RCECSLinCtrl; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_ExteriorLightPanel_1_LINMastCtrl_Irv_IOCTL_ELCP1LinCtrl; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_ExteriorLightPanel_2_LINMastCtrl_Irv_IOCTL_ELCP2LinCtrl; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_FlexibleSwitchesRouter_Ctrl_LINMstr_Irv_IOCTL_FspLinCtrl; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSP_Array5, RTE_VAR_NOINIT) Rte_Irv_FlexibleSwitchesRouter_Ctrl_LINMstr_Irv_NbOfFspByCCNAD; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(FSP_Array5, RTE_VAR_NOINIT) Rte_Irv_FlexibleSwitchesRouter_Ctrl_LINMstr_Irv_NbOfFspInFailure; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Boolean, RTE_VAR_NOINIT) Rte_Irv_Immo_stopByUser; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_InteriorLightPanel_1_LINMastCtrl_Irv_IOCTL_ILCP1Lin; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(IOHWAB_UINT8, RTE_VAR_NOINIT) Rte_Irv_IoHwAb_ASIL_Core_FSCMode; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(VGTT_EcuPinFaultStatus, RTE_VAR_NOINIT) Rte_Irv_IoHwAb_ASIL_Core_IrvBatteryFaultStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(EcuHwVoltageValues_T, RTE_VAR_NOINIT) Rte_Irv_IoHwAb_ASIL_Core_IrvEcuVoltageValues; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(IoAsilCorePcbConfig_T, RTE_VAR_NOINIT) Rte_Irv_IoHwAb_ASIL_Core_IrvIsAdcPinPopulated; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(EcuHwFaultValues_T, RTE_VAR_NOINIT) Rte_Irv_IoHwAb_ASIL_Dobhs_IrvEcuFaultStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(EcuHwDioCtrlArray_T, RTE_VAR_NOINIT) Rte_Irv_IoHwAb_ASIL_Dobhs_IrvEcuHwDioCtrlArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(EcuHwDioCtrlArray_T, RTE_VAR_NOINIT) Rte_Irv_IoHwAb_QM_IO_IrvEcuHwDioCtrlArray; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(EcuHwFaultValues_T, RTE_VAR_NOINIT) Rte_Irv_IoHwAb_QM_IO_IrvEcuIoQmFaultStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_Keyfob_Mgr_FlagKeyfobMatchingByLF; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_Keyfob_Mgr_P1B0T_MatchedKeyfobCount; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_Keyfob_Mgr_P1B0T_MatchingStatus; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Dcm_Data12ByteType, RTE_VAR_NOINIT) Rte_Irv_Keyfob_UICtrl_Irv_KeyfobStatus_ReadData; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_LINMgr_Irv_DID_FCI; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_Irv_LINMgr_Irv_RID_SwitchDetectionTrig; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint32, RTE_VAR_NOINIT) Rte_Irv_LevelControl_HMICtrl_IRV_ECS_StandbyTimer; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_SpeedControlFreeWheel_LINMasCtrl_Irv_IOCTL_CCFWLinCtrl; /* PRQA S 0850, 3408, 1504 */ /* MD_MSR_19.8, MD_Rte_3408, MD_MSR_8.10 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * RTE internal IOC replacement
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_Queue[1U];

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_INIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_tail;

#  define RTE_STOP_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_ZERO_INIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_head;

#  define RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_Queue[1U];

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_INIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_tail;

#  define RTE_STOP_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_ZERO_INIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_head;

#  define RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_Queue[1U];

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_INIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_tail;

#  define RTE_STOP_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_ZERO_INIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_head;

#  define RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_Queue[1U];

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_INIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_tail;

#  define RTE_STOP_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_ZERO_INIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_head;

#  define RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_Queue[1U];

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_INIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_tail;

#  define RTE_STOP_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_ZERO_INIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_head;

#  define RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_Queue[1U];

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_INIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_tail;

#  define RTE_STOP_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_ZERO_INIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_head;

#  define RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_Queue[1U];

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_INIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_tail;

#  define RTE_STOP_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_ZERO_INIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_head;

#  define RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_Queue[1U];

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_INIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_tail;

#  define RTE_STOP_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_ZERO_INIT) Rte_ioc_Rte_M_BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_head;

#  define RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_ioc_Rte_M_BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode_Queue[1U];

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_INIT) Rte_ioc_Rte_M_BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode_tail;

#  define RTE_STOP_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_ZERO_INIT) Rte_ioc_Rte_M_BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode_head;

#  define RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_ioc_Rte_M_Dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl_Queue[1U];

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_INIT) Rte_ioc_Rte_M_Dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl_tail;

#  define RTE_STOP_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_ZERO_INIT) Rte_ioc_Rte_M_Dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl_head;

#  define RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */



#  define RTE_START_SEC_CONST_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(EventMaskType, RTE_CONST) Rte_ModeEntryEventMask_BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState[3];
extern CONST(EventMaskType, RTE_CONST) Rte_ModeEntryEventMask_BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState[3];
extern CONST(EventMaskType, RTE_CONST) Rte_ModeEntryEventMask_BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState[3];
extern CONST(EventMaskType, RTE_CONST) Rte_ModeEntryEventMask_BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState[3];
extern CONST(EventMaskType, RTE_CONST) Rte_ModeEntryEventMask_BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState[3];
extern CONST(EventMaskType, RTE_CONST) Rte_ModeEntryEventMask_BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState[3];
extern CONST(EventMaskType, RTE_CONST) Rte_ModeEntryEventMask_BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState[3];
extern CONST(EventMaskType, RTE_CONST) Rte_ModeEntryEventMask_BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState[3];
extern CONST(EventMaskType, RTE_CONST) Rte_ModeEntryEventMask_BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode[5];
extern CONST(EventMaskType, RTE_CONST) Rte_ModeExitEventMask_Dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl[3];
extern CONST(EventMaskType, RTE_CONST) Rte_ModeEntryEventMask_Dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl[3];

#  define RTE_STOP_SEC_CONST_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Data structures for mode management
 *********************************************************************************************************************/












# endif /* defined(RTE_CORE) */

/**********************************************************************************************************************
 * extern declaration of RTE Update Flags for optimized macro implementation
 *********************************************************************************************************************/
typedef struct
{
  Rte_BitType Rte_RxUpdate_DiagnosticComponent_SecondsUTC_SecondsUTC : 1;
  Rte_BitType Rte_RxUpdate_ExteriorLightPanel_1_LINMastCtrl_LIN_LightMode_Status_1_FreeWheel_Status : 1;
  Rte_BitType Rte_RxUpdate_ExteriorLightPanel_2_LINMastCtrl_LIN_LightMode_Status_2_FreeWheel_Status : 1;
  Rte_BitType Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp1_SwitchDetectionResp : 1;
  Rte_BitType Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp2_SwitchDetectionResp : 1;
  Rte_BitType Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp3_SwitchDetectionResp : 1;
  Rte_BitType Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp4_SwitchDetectionResp : 1;
  Rte_BitType Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp5_SwitchDetectionResp : 1;
  Rte_BitType Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp6_SwitchDetectionResp : 1;
  Rte_BitType Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp7_SwitchDetectionResp : 1;
  Rte_BitType Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp8_SwitchDetectionResp : 1;
  Rte_BitType Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp9_SwitchDetectionResp : 1;
  Rte_BitType Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionRespB_SwitchDetectionResp : 1;
  Rte_BitType Rte_RxUpdate_InteriorLightPanel_1_LINMastCtrl_LIN_IntLghtModeSelrFreeWheel_s_FreeWheel_Status : 1;
  Rte_BitType Rte_RxUpdate_InteriorLightPanel_2_LINMastCtrl_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status : 1;
  Rte_BitType Rte_RxUpdate_ExteriorLightPanels_Freewheel_Gw_LightMode_Status_1_FreeWheel_Status : 1;
  Rte_BitType Rte_RxUpdate_ExteriorLightPanels_Freewheel_Gw_LightMode_Status_2_FreeWheel_Status : 1;
  Rte_BitType Rte_RxUpdate_InteriorLights_HMICtrl_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status : 1;
} Rte_RxUpdateFlagsType;

# define RTE_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Rte_RxUpdateFlagsType, RTE_VAR_ZERO_INIT) Rte_RxUpdateFlags;

# define RTE_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


#endif /* _RTE_TYPE_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3408:  MISRA rule: 8.8
     Reason:     For the purpose of monitoring during calibration or debugging it is necessary to use non-static declarations.
                 This is covered in the MISRA C compliance section of the Rte specification.
     Risk:       No functional risk.
     Prevention: Not required.

*/
