/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Hal_Context_Lcfg.c
 *   Generation Time: 2020-11-11 14:25:32
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

/* PRQA S 0777, 0779, 0828 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2, MD_MSR_Dir1.1 */

#define OS_HAL_CONTEXT_LCFG_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
#include "Std_Types.h"

/* Os module declarations */
#include "Os_Hal_Context_Lcfg.h"
#include "Os_Hal_Context.h"

/* Os kernel module dependencies */
#include "OsInt.h"
#include "Os_Core.h"
#include "Os_Hook_Lcfg.h"
#include "Os_Hook.h"
#include "Os_Ioc_Lcfg.h"
#include "Os_Isr_Lcfg.h"
#include "Os_Lcfg.h"
#include "Os_Stack_Lcfg.h"
#include "Os_Task_Lcfg.h"
#include "Os_Task.h"

/* Os hal dependencies */
#include "Os_Hal_Core.h"


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

#define OS_START_SEC_CORE0_VAR_NOINIT_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! HAL dynamic hook context data: Os_CoreInitHook_OsCore0 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_Os_CoreInitHook_OsCore0_Dyn;

/*! HAL dynamic hook context data: ShutdownHook_OsCore0 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_ShutdownHook_OsCore0_Dyn;

/*! HAL dynamic hook context data: ErrorHook_OsCore0 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_ErrorHook_OsCore0_Dyn;

/*! HAL dynamic ISR2 level context data: Level1 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_OsCore0_Isr_Level1_Dyn;

/*! HAL dynamic ISR2 level context data: Level2 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_OsCore0_Isr_Level2_Dyn;

/*! HAL dynamic ISR2 level context data: Level3 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_OsCore0_Isr_Level3_Dyn;

/*! HAL dynamic ISR2 level context data: Level4 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_OsCore0_Isr_Level4_Dyn;

/*! HAL dynamic ISR2 level context data: Level5 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_OsCore0_Isr_Level5_Dyn;

/*! HAL dynamic ISR2 level context data: Level6 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_OsCore0_Isr_Level6_Dyn;

/*! HAL dynamic ISR2 level context data: Level7 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_OsCore0_Isr_Level7_Dyn;

/*! HAL dynamic ISR2 level context data: Level8 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_OsCore0_Isr_Level8_Dyn;

/*! HAL dynamic ISR2 level context data: Level9 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_OsCore0_Isr_Level9_Dyn;

/*! HAL dynamic ISR2 level context data: Level10 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_OsCore0_Isr_Level10_Dyn;

/*! HAL dynamic ISR2 level context data: Level11 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_OsCore0_Isr_Level11_Dyn;

/*! HAL dynamic ISR2 level context data: Level12 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_OsCore0_Isr_Level12_Dyn;

/*! HAL dynamic ISR2 level context data: Level13 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_OsCore0_Isr_Level13_Dyn;

/*! HAL dynamic ISR2 level context data: Level14 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_OsCore0_Isr_Level14_Dyn;

/*! HAL dynamic ISR2 level context data: Level15 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_OsCore0_Isr_Level15_Dyn;

/*! HAL dynamic task context data: ASW_10ms_Task */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_ASW_10ms_Task_Dyn;

/*! HAL dynamic task context data: ASW_20ms_Task */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_ASW_20ms_Task_Dyn;

/*! HAL dynamic task context data: ASW_Async_Task */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_ASW_Async_Task_Dyn;

/*! HAL dynamic task context data: ASW_Init_Task */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_ASW_Init_Task_Dyn;

/*! HAL dynamic task context data: BSW_10ms_Task */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_BSW_10ms_Task_Dyn;

/*! HAL dynamic task context data: BSW_5ms_Task */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_BSW_5ms_Task_Dyn;

/*! HAL dynamic task context data: BSW_Async_Task */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_BSW_Async_Task_Dyn;

/*! HAL dynamic task context data: BSW_Diag_Task */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_BSW_Diag_Task_Dyn;

/*! HAL dynamic task context data: BSW_Lin_Task */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_BSW_Lin_Task_Dyn;

/*! HAL dynamic task context data: CpuLoadIdleTask */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_CpuLoadIdleTask_Dyn;

/*! HAL dynamic task context data: IdleTask_OsCore0 */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_IdleTask_OsCore0_Dyn;

/*! HAL dynamic task context data: Init_Task */
VAR(Os_Hal_ContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_Init_Task_Dyn;

/*! HAL exception context data: OsCore0 */
VAR(Os_ExceptionContextType, OS_VAR_NOINIT) OsCfg_Hal_Context_OsCore0_ExceptionContext;

#define OS_STOP_SEC_CORE0_VAR_NOINIT_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA
 *********************************************************************************************************************/

#define OS_START_SEC_CORE0_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! HAL hook context configuration data: Os_CoreInitHook_OsCore0 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Os_CoreInitHook_OsCore0 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Init_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Init_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_HookWrapperOs_CoreInitHook,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapHookReturn,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL hook context configuration data: ShutdownHook_OsCore0 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_ShutdownHook_OsCore0 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Shutdown_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Shutdown_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_HookWrapperStatusHook,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapHookReturn,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL hook context configuration data: ErrorHook_OsCore0 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_ErrorHook_OsCore0 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Error_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Error_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_HookWrapperStatusHook,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapHookReturn,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_0_MB00To03 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_0_MB00To03 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_0,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_0_MB04To07 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_0_MB04To07 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_0,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_0_MB08To11 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_0_MB08To11 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_0,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_0_MB12To15 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_0_MB12To15 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_0,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_0_MB16To31 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_0_MB16To31 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_0,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_0_MB32To63 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_0_MB32To63 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_0,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_0_MB64To95 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_0_MB64To95 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_0,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_1_MB00To03 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_1_MB00To03 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_1,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_1_MB04To07 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_1_MB04To07 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_1,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_1_MB08To11 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_1_MB08To11 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_1,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_1_MB12To15 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_1_MB12To15 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_1,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_1_MB16To31 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_1_MB16To31 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_1,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_1_MB32To63 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_1_MB32To63 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_1,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_1_MB64To95 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_1_MB64To95 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_1,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_2_MB00To03 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_2_MB00To03 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_2,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_2_MB04To07 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_2_MB04To07 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_2,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_2_MB08To11 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_2_MB08To11 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_2,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_2_MB12To15 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_2_MB12To15 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_2,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_2_MB16To31 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_2_MB16To31 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_2,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_2_MB32To63 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_2_MB32To63 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_2,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_2_MB64To95 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_2_MB64To95 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_2,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_4_MB00To03 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_4_MB00To03 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_4,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_4_MB04To07 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_4_MB04To07 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_4,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_4_MB08To11 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_4_MB08To11 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_4,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_4_MB12To15 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_4_MB12To15 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_4,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_4_MB16To31 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_4_MB16To31 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_4,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_4_MB32To63 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_4_MB32To63 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_4,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_4_MB64To95 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_4_MB64To95 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_4,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_6_MB00To03 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_6_MB00To03 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_6,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_6_MB04To07 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_6_MB04To07 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_6,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_6_MB08To11 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_6_MB08To11 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_6,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_6_MB12To15 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_6_MB12To15 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_6,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_6_MB16To31 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_6_MB16To31 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_6,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_6_MB32To63 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_6_MB32To63 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_6,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_6_MB64To95 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_6_MB64To95 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_6,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_7_MB00To03 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_7_MB00To03 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_7,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_7_MB04To07 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_7_MB04To07 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_7,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_7_MB08To11 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_7_MB08To11 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_7,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_7_MB12To15 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_7_MB12To15 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_7,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_7_MB16To31 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_7_MB16To31 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_7,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_7_MB32To63 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_7_MB32To63 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_7,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CanIsr_7_MB64To95 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CanIsr_7_MB64To95 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_CanMailboxIsr_7,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: CounterIsr_SystemTimer */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CounterIsr_SystemTimer =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_Os_TimerPfrtIsr,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: DOWHS1_EMIOS0_CH3_ISR */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_DOWHS1_EMIOS0_CH3_ISR =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_EMIOS_0_CH_2_CH_3_ISR,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: DOWHS2_EMIOS0_CH5_ISR */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_DOWHS2_EMIOS0_CH5_ISR =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_EMIOS_0_CH_4_CH_5_ISR,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: DOWLS2_EMIOS0_CH13_ISR */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_DOWLS2_EMIOS0_CH13_ISR =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_EMIOS_0_CH_12_CH_13_ISR,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: DOWLS2_EMIOS0_CH9_ISR */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_DOWLS2_EMIOS0_CH9_ISR =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_EMIOS_0_CH_8_CH_9_ISR,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: DOWLS3_EMIOS0_CH14_ISR */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_DOWLS3_EMIOS0_CH14_ISR =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_EMIOS_0_CH_14_CH_15_ISR,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Gpt_PIT_0_TIMER_0_ISR */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Gpt_PIT_0_TIMER_0_ISR =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_Gpt_PIT_0_TIMER_0_ISR,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Gpt_PIT_0_TIMER_1_ISR */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Gpt_PIT_0_TIMER_1_ISR =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_Gpt_PIT_0_TIMER_1_ISR,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Gpt_PIT_0_TIMER_2_ISR */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Gpt_PIT_0_TIMER_2_ISR =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_Gpt_PIT_0_TIMER_2_ISR,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_0_ERR */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_0_ERR =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_0,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_0_RXI */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_0_RXI =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_0,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_0_TXI */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_0_TXI =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_0,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_10_ERR */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_10_ERR =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_10,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_10_RXI */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_10_RXI =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_10,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_10_TXI */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_10_TXI =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_10,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_1_ERR */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_1_ERR =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_1,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_1_RXI */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_1_RXI =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_1,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_1_TXI */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_1_TXI =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_1,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_4_ERR */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_4_ERR =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_4,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_4_RXI */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_4_RXI =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_4,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_4_TXI */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_4_TXI =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_4,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_6_ERR */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_6_ERR =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_6,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_6_RXI */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_6_RXI =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_6,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_6_TXI */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_6_TXI =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_6,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_7_ERR */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_7_ERR =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_7,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_7_RXI */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_7_RXI =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_7,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_7_TXI */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_7_TXI =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_7,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_8_ERR */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_8_ERR =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_8,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_8_RXI */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_8_RXI =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_8,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_8_TXI */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_8_TXI =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_8,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_9_ERR */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_9_ERR =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_9,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_9_RXI */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_9_RXI =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_9,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: Lin_Channel_9_TXI */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Lin_Channel_9_TXI =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_LinIsr_9,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: MCU_PLL_LossOfLock */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_MCU_PLL_LossOfLock =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_Mcu_PllDig_Pll0_LossOfLockIsr,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: WKUP_IRQ0 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_WKUP_IRQ0 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_WKPU_EXT_IRQ_0_7_ISR,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: WKUP_IRQ1 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_WKUP_IRQ1 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_WKPU_EXT_IRQ_8_15_ISR,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: WKUP_IRQ2 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_WKUP_IRQ2 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_WKPU_EXT_IRQ_16_23_ISR,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL ISR2 context configuration data: WKUP_IRQ3 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_WKUP_IRQ3 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Isr_Core_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .Pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Isr_WKPU_EXT_IRQ_24_31_ISR,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapIsrEpilogue,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)15
}
;

/*! HAL task context configuration data: ASW_10ms_Task */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_ASW_10ms_Task =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_ASW_10ms_Task_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_ASW_10ms_Task_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Task_ASW_10ms_Task,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapTaskMissingTerminateTask,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)0
}
;

/*! HAL task context configuration data: ASW_20ms_Task */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_ASW_20ms_Task =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_ASW_20ms_Task_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_ASW_20ms_Task_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Task_ASW_20ms_Task,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapTaskMissingTerminateTask,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)0
}
;

/*! HAL task context configuration data: ASW_Async_Task */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_ASW_Async_Task =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_ASW_Async_Task_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_ASW_Async_Task_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Task_ASW_Async_Task,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapTaskMissingTerminateTask,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)0
}
;

/*! HAL task context configuration data: ASW_Init_Task */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_ASW_Init_Task =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Task_Prio200_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Task_Prio200_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Task_ASW_Init_Task,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapTaskMissingTerminateTask,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)0
}
;

/*! HAL task context configuration data: BSW_10ms_Task */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_BSW_10ms_Task =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Task_Prio130_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Task_Prio130_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Task_BSW_10ms_Task,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapTaskMissingTerminateTask,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)0
}
;

/*! HAL task context configuration data: BSW_5ms_Task */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_BSW_5ms_Task =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Task_Prio140_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Task_Prio140_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Task_BSW_5ms_Task,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapTaskMissingTerminateTask,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)0
}
;

/*! HAL task context configuration data: BSW_Async_Task */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_BSW_Async_Task =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Task_Prio120_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Task_Prio120_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Task_BSW_Async_Task,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapTaskMissingTerminateTask,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)0
}
;

/*! HAL task context configuration data: BSW_Diag_Task */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_BSW_Diag_Task =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_BSW_Diag_Task_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_BSW_Diag_Task_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Task_BSW_Diag_Task,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapTaskMissingTerminateTask,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)0
}
;

/*! HAL task context configuration data: BSW_Lin_Task */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_BSW_Lin_Task =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_BSW_Lin_Task_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_BSW_Lin_Task_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Task_BSW_Lin_Task,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapTaskMissingTerminateTask,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)0
}
;

/*! HAL task context configuration data: CpuLoadIdleTask */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_CpuLoadIdleTask =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Task_Prio1_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Task_Prio1_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Task_CpuLoadIdleTask,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapTaskMissingTerminateTask,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)0
}
;

/*! HAL task context configuration data: IdleTask_OsCore0 */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_IdleTask_OsCore0 =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Task_Prio4294967295_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Task_Prio4294967295_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Task_Os_IdleTask,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapTaskMissingTerminateTask,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)0
}
;

/*! HAL task context configuration data: Init_Task */
CONST(Os_Hal_ContextConfigType, OS_CONST) OsCfg_Hal_Context_Init_Task =
{
  {
    /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Task_Prio201_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
    /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Task_Prio201_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  },
  /* .pid            = */ (uint32)0,
  /* .Entry          = */ (uint32)&Os_Task_Init_Task,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .ReturnAddress  = */ (uint32)&Os_TrapTaskMissingTerminateTask,  /* PRQA S 0305 */ /* MD_Os_Hal_Rule11.1_0305 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .MachineStatus  = */ (uint32)(OS_HAL_CORE_MSR_MASK_SPE | OS_HAL_CORE_MSR_MASK_ME | OS_HAL_CORE_MSR_MASK_EE),
  /* .InterruptLevel = */ (uint32)0
}
;

/*! HAL kernel stack configuration data: OsCore0_Kernel */
CONST(Os_Hal_ContextStackConfigType, OS_CONST) OsCfg_Hal_Stack_OsCore0_Kernel =
{
  /* .StackRegionStart = */ (uint32)OS_STACK_GETLOWADDRESS(OsCfg_Stack_OsCore0_Kernel_Dyn),  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .StackRegionEnd   = */ (uint32)(OS_STACK_GETHIGHADDRESS(OsCfg_Stack_OsCore0_Kernel_Dyn) + 1u)  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT
 */
}
;

#define OS_STOP_SEC_CORE0_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */



#define OS_START_SEC_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Object reference table for HAL exception context. */
CONSTP2VAR(Os_ExceptionContextType, AUTOMATIC, OS_CONST)
  OsCfg_Hal_Context_ExceptionContextRef[OS_CFG_COREPHYSICALID_COUNT + 1u] =
{
  &OsCfg_Hal_Context_OsCore0_ExceptionContext,
  NULL_PTR
};

#define OS_STOP_SEC_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  END OF FILE: Os_Hal_Context_Lcfg.c
 *********************************************************************************************************************/
