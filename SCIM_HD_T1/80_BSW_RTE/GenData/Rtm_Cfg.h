/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Rtm
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Rtm_Cfg.h
 *   Generation Time: 2020-11-11 14:25:33
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

/* PRQA S 883 EOF */ /* MD_Rtm_883 */

#if !defined(RTM_CFG_H)
#define RTM_CFG_H

/* -----------------------------------------------------------------------------
    &&&~ Includes
 ----------------------------------------------------------------------------- */

#include "Rtm_Cbk.h"
#include "SchM_Rtm.h"


/* -----------------------------------------------------------------------------
    &&&~ General Define Block
 ----------------------------------------------------------------------------- */

#ifndef RTM_USE_DUMMY_STATEMENT
#define RTM_USE_DUMMY_STATEMENT STD_OFF /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef RTM_DUMMY_STATEMENT
#define RTM_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef RTM_DUMMY_STATEMENT_CONST
#define RTM_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef RTM_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define RTM_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef RTM_ATOMIC_VARIABLE_ACCESS
#define RTM_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef RTM_PROCESSOR_MPC5746C
#define RTM_PROCESSOR_MPC5746C
#endif
#ifndef RTM_COMP_DIAB
#define RTM_COMP_DIAB
#endif
#ifndef RTM_GEN_GENERATOR_MSR
#define RTM_GEN_GENERATOR_MSR
#endif
#ifndef RTM_CPUTYPE_BITORDER_MSB2LSB
#define RTM_CPUTYPE_BITORDER_MSB2LSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef RTM_CONFIGURATION_VARIANT_PRECOMPILE
#define RTM_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef RTM_CONFIGURATION_VARIANT_LINKTIME
#define RTM_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef RTM_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define RTM_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef RTM_CONFIGURATION_VARIANT
#define RTM_CONFIGURATION_VARIANT RTM_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef RTM_POSTBUILD_VARIANT_SUPPORT
#define RTM_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif


 
/* -----------------------------------------------------------------------------
    &&&~ Global Constant Macros
----------------------------------------------------------------------------- */

#define RTM_CPU_LOAD_MEASUREMENT_ID            0x00000002uL


#define RTM_CPU_LOAD_CONTROL_OFF               0x0U
#define RTM_CPU_LOAD_CONTROL_C_API             0x1U
#define RTM_CPU_LOAD_CONTROL_XCP               0x2U

#define RTM_CPU_LOAD_CONTROL_MODE              RTM_CPU_LOAD_CONTROL_C_API

/* -----------------------------------------------------------------------------
    &&&~ Global Data Prototypes
 ----------------------------------------------------------------------------- */

# define RTM_START_SEC_VAR_NOINIT_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_19.1 */

extern VAR(uint8, RTM_VAR_NOINIT)              Rtm_CpuLoadMeasurementActive;
extern VAR(uint8, RTM_VAR_NOINIT)              Rtm_CpuLoadSendResult;

# define RTM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_19.1 */


/* -----------------------------------------------------------------------------
    &&&~ RtmMeasurementPoint
 ----------------------------------------------------------------------------- */

#define RtmConf_RtmMeasurementPoint_Rtm_Overhead_RespTime_EnableISRs (0UL)
#define RtmConf_RtmMeasurementPoint_Rtm_Overhead_RespTime_DisableISRs (1UL)
#define RtmConf_RtmMeasurementPoint_CANNM_SID_GETLOCALNODEIDENTIFIER (2uL)
#define RtmConf_RtmMeasurementPoint_CANNM_SID_GETNODEIDENTIFIER (3uL)
#define RtmConf_RtmMeasurementPoint_CANNM_SID_GETPDUDATA (4uL)
#define RtmConf_RtmMeasurementPoint_CANNM_SID_GETSTATE (5uL)
#define RtmConf_RtmMeasurementPoint_CANNM_SID_INIT (6uL)
#define RtmConf_RtmMeasurementPoint_CANNM_SID_MAINFUNCTION (7uL)
#define RtmConf_RtmMeasurementPoint_CANNM_SID_NETWORKRELEASE (8uL)
#define RtmConf_RtmMeasurementPoint_CANNM_SID_NETWORKREQUEST (9uL)
#define RtmConf_RtmMeasurementPoint_CANNM_SID_PASSIVESTARTUP (10uL)
#define RtmConf_RtmMeasurementPoint_CANNM_SID_RXINDICATION (11uL)
#define RtmConf_RtmMeasurementPoint_CANNM_SID_TXCONFIRMATION (12uL)
#define RtmConf_RtmMeasurementPoint_CanBusOffInterrupt (13uL)
#define RtmConf_RtmMeasurementPoint_CanMailboxInterrupt (14uL)
#define RtmConf_RtmMeasurementPoint_Can_Init (15uL)
#define RtmConf_RtmMeasurementPoint_Can_InitController (16uL)
#define RtmConf_RtmMeasurementPoint_Can_SetControllerMode (17uL)
#define RtmConf_RtmMeasurementPoint_Can_Write (18uL)
#define RtmConf_RtmMeasurementPoint_LinIf_Init (19uL)
#define RtmConf_RtmMeasurementPoint_LinTp_Init (20uL)
#define RtmConf_RtmMeasurementPoint_Lin_Init (21uL)
#define RtmConf_RtmMeasurementPoint_Lin_Interrupt (22uL)
#define RtmConf_RtmMeasurementPoint_NM_SID_BUSSLEEPMODE (23uL)
#define RtmConf_RtmMeasurementPoint_NM_SID_GETLOCALNODEIDENTIFIER (24uL)
#define RtmConf_RtmMeasurementPoint_NM_SID_GETNODEIDENTIFIER (25uL)
#define RtmConf_RtmMeasurementPoint_NM_SID_GETPDUDATA (26uL)
#define RtmConf_RtmMeasurementPoint_NM_SID_GETSTATE (27uL)
#define RtmConf_RtmMeasurementPoint_NM_SID_MAINFUNCTION (28uL)
#define RtmConf_RtmMeasurementPoint_NM_SID_NETWORKMODE (29uL)
#define RtmConf_RtmMeasurementPoint_NM_SID_NETWORKRELEASE (30uL)
#define RtmConf_RtmMeasurementPoint_NM_SID_NETWORKREQUEST (31uL)
#define RtmConf_RtmMeasurementPoint_NM_SID_NETWORKSTARTINDICATION (32uL)
#define RtmConf_RtmMeasurementPoint_NM_SID_PASSIVESTARTUP (33uL)
#define RtmConf_RtmMeasurementPoint_NM_SID_PREPAREBUSSLEEPMODE (34uL)
#define RtmConf_RtmMeasurementPoint_NM_SID_TXTIMEOUTEXCEPTION (35uL)
#define RtmConf_RtmMeasurementPoint_Rtm_CpuLoadMeasurement (36uL)
#define RtmConf_RtmMeasurementPoint_Xcp_Event (37uL)
#define RtmConf_RtmMeasurementPoint_Xcp_Init (38uL)
#define RtmConf_RtmMeasurementPoint_Xcp_MainFunction (39uL)
#define RtmConf_RtmMeasurementPoint_Xcp_TlRxIndication (40uL)
#define RtmConf_RtmMeasurementPoint_Xcp_TlTxConfirmation (41uL)

/* -----------------------------------------------------------------------------
    &&&~ CONFIGURATION DEFINES
 ----------------------------------------------------------------------------- */

#define RTM_CONTROL                            STD_ON

#define RTM_MEASUREMENT_MAX_VAL                (0xFFFFFFFFUL)
#define RTM_USE_32BIT_TIMER                    STD_ON
#define RTM_TICKS_PER_MILISECOND               0x00002710uL
#define RTM_MAINFCT_CYCLE_TIME                 0x0000000AuL

#define RTM_DEV_ERROR_DETECT                   STD_OFF
#define RTM_DEV_ERROR_REPORT                   STD_OFF
#define RTM_TIME_MEASUREMENT                   STD_ON
#define RTM_CTR_DIRECTION                      RTM_UP

#define RTM_CTRL_VECTOR_LEN                    0x00000001uL
#define RTM_MEAS_SECTION_COUNT                 0x00000003uL

#define RTM_GET_TIME_MEASUREMENT_FCT           Rtm_GetTimeMeasurement
#define EV_RTM                                 XcpConf_XcpEventChannel_Rtm_Evt  


#define RTM_TIMER_OVERRUN_SUPPORT              STD_OFF 
#define RTM_NET_RUNTIME_SUPPORT                STD_OFF
#define RTM_RESPONSE_TIME_AVAILABLE            STD_ON
#define RTM_EXECUTION_TIME_NESTED_AVAILABLE    STD_OFF
#define RTM_EXECUTION_TIME_NONNESTED_AVAILABLE STD_OFF
#define RTM_NESTING_COUNTER                    STD_ON

#define RTM_NUMBER_OF_TASKS                    12u
#define RTM_NUMBER_OF_ISRS                     81u
#define RTM_NUMBER_OF_THREADS                  (RTM_NUMBER_OF_TASKS + RTM_NUMBER_OF_ISRS)
#define RTM_NUMBER_OF_CORES                    1u
#define RTM_TASK_SWITCH_OVERHEAD               0u
#define RTM_INTERRUPT_OVERHEAD                 0u

#define RTM_MULTICORE_SUPPORT                  STD_OFF
#define RTM_CROSS_CORE_SUPPORT                 STD_OFF
#define RTM_MASK_OF_USED_CORES                 0x00000001uL
#define RTM_NUMBER_OF_DISABLED_INT             1

#define Rtm_Start_RtmConf_RtmMeasurementPoint_Rtm_Overhead_RespTime_EnableISRs  { \
    if (Rtm_ControlState == RTM_CONTROL_STATE_ENABLED) { \
      if ((0x01u) == ((0x01u /* ctrlMask */) & Rtm_Ctrl[(0x00000000uL /* ctrlVecIndex */)])) { \
        SchM_Enter_Rtm_RTM_EXCLUSIVE_AREA_0(); \
        Rtm_StartMeasurementFct(0x00000000uL /* measurementId */); \
        SchM_Exit_Rtm_RTM_EXCLUSIVE_AREA_0(); \
      } \
    } else { \
      Rtm_ControlDeniedCount++; \
    } \
  } /* PRQA S 3458 */ /* MD_MSR_19.4 */

#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_Rtm_Overhead_RespTime_EnableISRs  { \
    if (Rtm_ControlState == RTM_CONTROL_STATE_ENABLED) { \
      if (((0x01u) == ((0x01u /* ctrlMask */) & Rtm_Ctrl[(0x00000000uL /* ctrlVecIndex */)])) && (0 < Rtm_MeasurementNestingCtr[(0x00000000uL /* measurementId */)]) ){ \
        SchM_Enter_Rtm_RTM_EXCLUSIVE_AREA_0(); \
        Rtm_StopMeasurementFct(0x00000000uL /* measurementId */); \
        SchM_Exit_Rtm_RTM_EXCLUSIVE_AREA_0(); \
      } \
    } else { \
      Rtm_ControlDeniedCount++; \
    } \
  } /* PRQA S 3458 */ /* MD_MSR_19.4 */

#define Rtm_Start_RtmConf_RtmMeasurementPoint_Rtm_Overhead_RespTime_DisableISRs  { \
    if (Rtm_ControlState == RTM_CONTROL_STATE_ENABLED) { \
      if ((0x02u) == ((0x02u /* ctrlMask */) & Rtm_Ctrl[(0x00000000uL /* ctrlVecIndex */)])) { \
        SchM_Enter_Rtm_RTM_EXCLUSIVE_AREA_1(); \
        Rtm_StartMeasurementFct(0x00000001uL /* measurementId */); \
      } \
    } else { \
      Rtm_ControlDeniedCount++; \
    } \
  } /* PRQA S 3458 */ /* MD_MSR_19.4 */

#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_Rtm_Overhead_RespTime_DisableISRs  { \
    if (Rtm_ControlState == RTM_CONTROL_STATE_ENABLED) { \
      if (((0x02u) == ((0x02u /* ctrlMask */) & Rtm_Ctrl[(0x00000000uL /* ctrlVecIndex */)])) && (0 < Rtm_MeasurementNestingCtr[(0x00000001uL /* measurementId */)]) ){ \
        Rtm_StopMeasurementFct(0x00000001uL /* measurementId */); \
        SchM_Exit_Rtm_RTM_EXCLUSIVE_AREA_1(); \
      } \
    } else { \
      Rtm_ControlDeniedCount++; \
    } \
  } /* PRQA S 3458 */ /* MD_MSR_19.4 */

#define Rtm_Start_RtmConf_RtmMeasurementPoint_CANNM_SID_GETLOCALNODEIDENTIFIER    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_CANNM_SID_GETLOCALNODEIDENTIFIER 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_CANNM_SID_GETNODEIDENTIFIER    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_CANNM_SID_GETNODEIDENTIFIER 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_CANNM_SID_GETPDUDATA    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_CANNM_SID_GETPDUDATA 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_CANNM_SID_GETSTATE    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_CANNM_SID_GETSTATE 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_CANNM_SID_INIT    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_CANNM_SID_INIT 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_CANNM_SID_MAINFUNCTION    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_CANNM_SID_MAINFUNCTION 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_CANNM_SID_NETWORKRELEASE    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_CANNM_SID_NETWORKRELEASE 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_CANNM_SID_NETWORKREQUEST    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_CANNM_SID_NETWORKREQUEST 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_CANNM_SID_PASSIVESTARTUP    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_CANNM_SID_PASSIVESTARTUP 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_CANNM_SID_RXINDICATION    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_CANNM_SID_RXINDICATION 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_CANNM_SID_TXCONFIRMATION    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_CANNM_SID_TXCONFIRMATION 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_CanBusOffInterrupt    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_CanBusOffInterrupt 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_CanMailboxInterrupt    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_CanMailboxInterrupt 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_Can_Init    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_Can_Init 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_Can_InitController    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_Can_InitController 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_Can_SetControllerMode    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_Can_SetControllerMode 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_Can_Write    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_Can_Write 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_LinIf_Init    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_LinIf_Init 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_LinTp_Init    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_LinTp_Init 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_Lin_Init    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_Lin_Init 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_Lin_Interrupt    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_Lin_Interrupt 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_NM_SID_BUSSLEEPMODE    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_NM_SID_BUSSLEEPMODE 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_NM_SID_GETLOCALNODEIDENTIFIER    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_NM_SID_GETLOCALNODEIDENTIFIER 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_NM_SID_GETNODEIDENTIFIER    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_NM_SID_GETNODEIDENTIFIER 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_NM_SID_GETPDUDATA    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_NM_SID_GETPDUDATA 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_NM_SID_GETSTATE    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_NM_SID_GETSTATE 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_NM_SID_MAINFUNCTION    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_NM_SID_MAINFUNCTION 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_NM_SID_NETWORKMODE    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_NM_SID_NETWORKMODE 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_NM_SID_NETWORKRELEASE    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_NM_SID_NETWORKRELEASE 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_NM_SID_NETWORKREQUEST    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_NM_SID_NETWORKREQUEST 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_NM_SID_NETWORKSTARTINDICATION    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_NM_SID_NETWORKSTARTINDICATION 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_NM_SID_PASSIVESTARTUP    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_NM_SID_PASSIVESTARTUP 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_NM_SID_PREPAREBUSSLEEPMODE    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_NM_SID_PREPAREBUSSLEEPMODE 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_NM_SID_TXTIMEOUTEXCEPTION    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_NM_SID_TXTIMEOUTEXCEPTION 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_Rtm_CpuLoadMeasurement  { \
    if (Rtm_ControlState == RTM_CONTROL_STATE_ENABLED) { \
    Rtm_CpuLoadMeasurementActive++; \
    } else { \
      Rtm_ControlDeniedCount++; \
    } \
  } /* PRQA S 3458 */ /* MD_MSR_19.4 */

#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_Rtm_CpuLoadMeasurement  { \
    if (Rtm_ControlState == RTM_CONTROL_STATE_ENABLED) { \
      SchM_Enter_Rtm_RTM_EXCLUSIVE_AREA_0(); \
      if (Rtm_CpuLoadMeasurementActive > 0) { \
        Rtm_CpuLoadMeasurementActive--; \
        if (Rtm_CpuLoadMeasurementActive == 0) { \
          Rtm_CpuLoadWasStopped = 0x01; \
        } \
      } \
      SchM_Exit_Rtm_RTM_EXCLUSIVE_AREA_0(); \
    } else { \
      Rtm_ControlDeniedCount++; \
    } \
  } /* PRQA S 3458 */ /* MD_MSR_19.4 */

#define Rtm_Start_RtmConf_RtmMeasurementPoint_Xcp_Event    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_Xcp_Event 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_Xcp_Init    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_Xcp_Init 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_Xcp_MainFunction    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_Xcp_MainFunction 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_Xcp_TlRxIndication    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_Xcp_TlRxIndication 
#define Rtm_Start_RtmConf_RtmMeasurementPoint_Xcp_TlTxConfirmation    
#define  Rtm_Stop_RtmConf_RtmMeasurementPoint_Xcp_TlTxConfirmation 

#define Rtm_Start_CpuLoadMeasurement() Rtm_Start_RtmConf_RtmMeasurementPoint_Rtm_CpuLoadMeasurement
#define Rtm_Stop_CpuLoadMeasurement() Rtm_Stop_RtmConf_RtmMeasurementPoint_Rtm_CpuLoadMeasurement


/* Called by Rtm_MainFunction */
#define Rtm_CheckAutostartCpuLoad(sending)

/* Called by Rtm_Init() */
#define Rtm_ActivateCpuLoadMeasurementPoints() { \
    if (Rtm_ControlState == RTM_CONTROL_STATE_ENABLED) { \
      Rtm_CpuLoadMeasurementActive = 0x00; \
    } else { \
      Rtm_ControlDeniedCount++; \
    } \
  } /* PRQA S 3458 */ /* MD_MSR_19.4 */
  
/* Called by Rtm_GetMeasurementItem */
#define Rtm_CheckValidMeasurementId(measId_) { \
    if (Rtm_ControlState == RTM_CONTROL_STATE_ENABLED) { \
      switch(measId_) { \
        case(RTM_CPU_LOAD_MEASUREMENT_ID): break; \
        default: Rtm_DetReportError(RTM_SID_GETMEASUREMENTITEM, RTM_E_WRONG_PARAMETERS); /* PRQA S 3109 */  /* MD_MSR_14.3 */ \
          (measId_) = (RTM_MEAS_SECTION_COUNT); break; \
      } \
    } else { \
      Rtm_ControlDeniedCount++; \
    } \
  } /* PRQA S 3458 */ /* MD_MSR_19.4 */


 
/* -----------------------------------------------------------------------------
    &&&~ User Config File content
 ----------------------------------------------------------------------------- */
 
/* User Config File Start */
/* Content of user configuration file (example with 200 ticks)*/

#define Rtm_Hook_StopCalcCpuLoadThreshold() { \
Rtm_CpuLoadOverhead = 4000u; \
} /* PRQA S 3458 */ /* MD_MSR_19.4 */
/* User Config File End */


#endif /* RTM_CFG_H */


