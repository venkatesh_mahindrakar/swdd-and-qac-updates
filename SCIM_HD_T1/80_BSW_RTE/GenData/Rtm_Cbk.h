/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Rtm
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Rtm_Cbk.h
 *   Generation Time: 2020-11-11 14:25:33
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

#if !defined(RTM_CBK_H)
#define RTM_CBK_H

#include "Platform_Types.h"
#include "Rtm_Types.h"

typedef uint32 Rtm_TimestampType;

typedef struct
{
  const boolean interruptsDisabled; /* Defines if interrupts are disabled during MP execution. */
  const uint8   mpType;
} Rtm_MeasurementPointInfoType;


#define RTM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 1 */  /* MD_MSR_19.1 */

Rtm_TimestampType Rtm_GetTimeMeasurement(void); 



#define RTM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 1 */  /* MD_MSR_19.1 */

#endif /* RTM_CBK_H */


