/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Fee
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Fee_Cfg.h
 *   Generation Time: 2020-11-11 14:25:29
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

    
/**********************************************************************************************************************
 *  PUBLIC SECTION
 *********************************************************************************************************************/
#if !defined (FEE_CFG_H_PUBLIC)
# define FEE_CFG_H_PUBLIC

  /********************************************************************************************************************
   *  GLOBAL CONSTANT MACROS
   *******************************************************************************************************************/
  /****************************************************************************
   * VERSION IDENTIFICATION
   ***************************************************************************/
# define FEE_CFG_MAJOR_VERSION                    (8u)
# define FEE_CFG_MINOR_VERSION                    (4u)
# define FEE_CFG_PATCH_VERSION                    (0u)

  /****************************************************************************
   * API CONFIGURATION
   ***************************************************************************/
# define FEE_VERSION_INFO_API                     (STD_OFF)
# define FEE_GET_ERASE_CYCLE_API                  (STD_OFF)
# define FEE_GET_WRITE_CYCLE_API                  (STD_OFF)
# define FEE_FORCE_SECTOR_SWITCH_API              (STD_OFF)
# define FEE_FSS_CONTROL_API                   	  (STD_OFF)

# define FEE_DATA_CONVERSION_API                  (STD_OFF)
# define FEE_LOOKUPTABLE_MODE                     (STD_OFF)

  /****************************************************************************
   * DEVELOPMENT CONFIGURATION
   ***************************************************************************/
# define FEE_DEV_ERROR_DETECT                     (STD_OFF)

# define FEE_DEBUG_REPORTING                      (STD_OFF)

  /****************************************************************************
   * GENERAL CONFIGURATION PARAMETER
   ***************************************************************************/

#define FeeConf_FeeBlockConfiguration_FeeFblDataFingerprint (2304uL) 
#define FeeConf_FeeBlockConfiguration_FeeFblFingerprintNumber (2240uL) 
#define FeeConf_FeeBlockConfiguration_FeeFblResetFlags (2432uL) 
#define FeeConf_FeeBlockConfiguration_FeeFblSigInfo (2176uL) 
#define FeeConf_FeeBlockConfiguration_FeeFblSigSegments (2496uL) 
#define FeeConf_FeeBlockConfiguration_FeeFblSigValue (2112uL) 
#define FeeConf_FeeBlockConfiguration_FeeFblSwFingerprint (2560uL) 
#define FeeConf_FeeBlockConfiguration_FeeFblValidityFlags (2048uL) 
#define FeeConf_FeeBlockConfiguration_FeeFblVpmPatch (2624uL) 
#define FeeConf_FeeBlockConfiguration_FeeFblVpmState (1984uL) 
#define FeeConf_FeeBlockConfiguration_FeeFblVpmValidityFlagsCopy (2368uL) 
#define FeeConf_FeeBlockConfiguration_FeeFblBootFingerprint (1920uL) 
#define FeeConf_FeeBlockConfiguration_FeeConfigBlock (2688uL) 
#define FeeConf_FeeBlockConfiguration_FeeDemAdminDataBlock (1856uL) 
#define FeeConf_FeeBlockConfiguration_FeeDemStatusDataBlock (1792uL) 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock0 (1728uL) 
#define FeeConf_FeeBlockConfiguration_FeeSCIM_KeyFobFSP_NVM_KeyFobNVBlock (1600uL) 
#define FeeConf_FeeBlockConfiguration_FeeSCIM_KeyFobFSP_NVM_FspNVBlock (1664uL) 
#define FeeConf_FeeBlockConfiguration_FeeApplication_Data_NVM_FSP_NVM (768uL) 
#define FeeConf_FeeBlockConfiguration_FeeApplication_Data_NVM_DriverAuth2_Ctrl_NVM (832uL) 
#define FeeConf_FeeBlockConfiguration_FeeApplication_Data_NVM_InCabLock_HMICtrl_NVM (896uL) 
#define FeeConf_FeeBlockConfiguration_FeeApplication_Data_NVM_VehicleAccess_Ctrl_NVM (960uL) 
#define FeeConf_FeeBlockConfiguration_FeeApplication_Data_NVM_InteriorLights_HMICtrl_NVM (1024uL) 
#define FeeConf_FeeBlockConfiguration_FeeApplication_Data_NVM_MUT_UICtrl_Traction_NVM (1088uL) 
#define FeeConf_FeeBlockConfiguration_FeeApplication_Data_NVM_RGW_HMICtrl_NVM (1152uL) 
#define FeeConf_FeeBlockConfiguration_FeeApplication_Data_NVM_SCM_HMICtrl_NVM (1216uL) 
#define FeeConf_FeeBlockConfiguration_FeeApplication_Data_NVM_VehicleModeDistribution_NVM (1280uL) 
#define FeeConf_FeeBlockConfiguration_FeeApplication_Data_NVM_VSC_UICtrl_NVM (1344uL) 
#define FeeConf_FeeBlockConfiguration_FeeApplication_Data_NVM_PinCode_ctrl_NVM (1408uL) 
#define FeeConf_FeeBlockConfiguration_FeeApplication_Data_NVM_MUT_UICtrl_Difflock_NVM (1472uL) 
#define FeeConf_FeeBlockConfiguration_FeeKeyfon_Radio_Com_NVM_KeyFobNVBlock (1536uL) 
#define FeeConf_FeeBlockConfiguration_FeeCalibration_Data_NVM_FSP_NVM (704uL) 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock1 (128uL) 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock2 (192uL) 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock3 (256uL) 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock4 (320uL) 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock5 (384uL) 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock6 (448uL) 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock7 (512uL) 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock8 (576uL) 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock9 (640uL) 
#define FeeConf_FeeBlockConfiguration_FeeApplication_Data_NVM_EngTraceHW_NvM (64uL) 


#define FeePartitionApp              (0u) 
#define FeePartitionSharePbl_MSW     (1u) 
#define FeePartition_DEM_DET_Logging (2u) 


#define FEE_NUMBER_OF_PARTITIONS (3)

#endif /* FEE_CFG_H_PUBLIC */

/**********************************************************************************************************************
 *  END OF FILE: Fee_Cfg.h
 *********************************************************************************************************************/
 

