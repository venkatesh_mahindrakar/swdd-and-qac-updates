/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: PduR
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: PduR_Cfg.h
 *   Generation Time: 2020-11-11 14:25:28
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

#if !defined (PDUR_CFG_H)
# define PDUR_CFG_H

/**********************************************************************************************************************
 * MISRA JUSTIFICATION
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "PduR_Types.h"

/**********************************************************************************************************************
 * GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
#ifndef PDUR_USE_DUMMY_STATEMENT
#define PDUR_USE_DUMMY_STATEMENT STD_OFF /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef PDUR_DUMMY_STATEMENT
#define PDUR_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef PDUR_DUMMY_STATEMENT_CONST
#define PDUR_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef PDUR_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define PDUR_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef PDUR_ATOMIC_VARIABLE_ACCESS
#define PDUR_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef PDUR_PROCESSOR_MPC5746C
#define PDUR_PROCESSOR_MPC5746C
#endif
#ifndef PDUR_COMP_DIAB
#define PDUR_COMP_DIAB
#endif
#ifndef PDUR_GEN_GENERATOR_MSR
#define PDUR_GEN_GENERATOR_MSR
#endif
#ifndef PDUR_CPUTYPE_BITORDER_MSB2LSB
#define PDUR_CPUTYPE_BITORDER_MSB2LSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef PDUR_CONFIGURATION_VARIANT_PRECOMPILE
#define PDUR_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef PDUR_CONFIGURATION_VARIANT_LINKTIME
#define PDUR_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef PDUR_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define PDUR_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef PDUR_CONFIGURATION_VARIANT
#define PDUR_CONFIGURATION_VARIANT PDUR_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef PDUR_POSTBUILD_VARIANT_SUPPORT
#define PDUR_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif



#define PDUR_DEV_ERROR_DETECT STD_OFF  /**< /ActiveEcuC/PduR/PduRGeneral[0:PduRSafeBswChecks] || /ActiveEcuC/PduR/PduRGeneral[0:PduRDevErrorDetect] */
#define PDUR_DEV_ERROR_REPORT STD_OFF  /**< /ActiveEcuC/PduR/PduRGeneral[0:PduRDevErrorDetect] */

#define PDUR_METADATA_SUPPORT STD_OFF  /**< /ActiveEcuC/PduR/PduRGeneral[0:PduRMetaDataSupport] */
#define PDUR_VERSION_INFO_API STD_OFF  /**< /ActiveEcuC/PduR/PduRGeneral[0:PduRVersionInfoApi] */

#define PDUR_ERROR_NOTIFICATION STD_OFF

#define PDUR_MAIN_FUNCTION STD_OFF

#define PDUR_MULTICORE STD_OFF /**< /ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] */

#define PduR_PBConfigIdType uint32

 
 /*  DET Error define list  */ 
#define PDUR_FCT_LINTPRXIND 0x55u 
#define PDUR_FCT_LINTPTX 0x59u 
#define PDUR_FCT_LINTPTXCFM 0x58u 
#define PDUR_FCT_LINTPSOR 0x56u 
#define PDUR_FCT_LINTPCPYRX 0x54u 
#define PDUR_FCT_LINTPCPYTX 0x57u 
#define PDUR_FCT_CANIFRXIND 0x01u 
#define PDUR_FCT_CANIFTX 0x09u 
#define PDUR_FCT_CANIFTXCFM 0x02u 
#define PDUR_FCT_CANTPRXIND 0x05u 
#define PDUR_FCT_CANTPTX 0x09u 
#define PDUR_FCT_CANTPTXCFM 0x08u 
#define PDUR_FCT_CANTPSOR 0x06u 
#define PDUR_FCT_CANTPCPYRX 0x04u 
#define PDUR_FCT_CANTPCPYTX 0x07u 
#define PDUR_FCT_COMTX 0x89u 
#define PDUR_FCT_DCMTX 0x99u 
#define PDUR_FCT_DCMCTX 0x9Au 
#define PDUR_FCT_DCMCTX 0x9Au 
#define PDUR_FCT_J1939TPRXIND 0x25u 
#define PDUR_FCT_J1939TPTX 0x29u 
#define PDUR_FCT_J1939TPTXCFM 0x28u 
#define PDUR_FCT_J1939TPSOR 0x26u 
#define PDUR_FCT_J1939TPCPYRX 0x24u 
#define PDUR_FCT_J1939TPCPYTX 0x27u 
#define PDUR_FCT_LINIFRXIND 0x51u 
#define PDUR_FCT_LINIFTX 0x59u 
#define PDUR_FCT_LINIFTXCFM 0x52u 
#define PDUR_FCT_LINIFTT 0x53u 
#define PDUR_FCT_J1939RMTX 0x29u 
#define PDUR_FCT_CDDTX 0xC9u 
 /*   PduR_LinTpTpRxIndication  PduR_LinTpTransmit  PduR_LinTpTxConfirmation  PduR_LinTpStartOfReception  PduR_LinTpCopyRxData  PduR_LinTpCopyTxData  PduR_CanIfIfRxIndication  PduR_CanIfTransmit  PduR_CanIfTxConfirmation  PduR_CanTpTpRxIndication  PduR_CanTpTransmit  PduR_CanTpTxConfirmation  PduR_CanTpStartOfReception  PduR_CanTpCopyRxData  PduR_CanTpCopyTxData  PduR_ComTransmit  PduR_DcmTransmit  PduR_DcmCancelTransmit  PduR_DcmCancelTransmit  PduR_J1939TpTpRxIndication  PduR_J1939TpTransmit  PduR_J1939TpTxConfirmation  PduR_J1939TpStartOfReception  PduR_J1939TpCopyRxData  PduR_J1939TpCopyTxData  PduR_LinIfIfRxIndication  PduR_LinIfTransmit  PduR_LinIfTxConfirmation  PduR_LinIfTriggerTransmit  PduR_J1939RmTransmit  PduR_CddTransmit  */ 



/**
 * \defgroup PduRHandleIdsIfRxDest Handle IDs of handle space IfRxDest.
 * \brief Communication interface Rx destination PDUs
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define PduRConf_PduRDestPdu_ACM_BB1_01P_oBackbone1J1939_b0278a49_Rx_bb6c3932_Rx 0u
#define PduRConf_PduRDestPdu_AIR1_X_VMCU_oBackbone1J1939_276eaeb8_Rx_66b8580e_Rx 1u
#define PduRConf_PduRDestPdu_AMB_X_VMCU_oBackbone1J1939_52549913_Rx_00ee91b2_Rx 2u
#define PduRConf_PduRDestPdu_Alarm_Sec_02P_oSecuritySubnet_0cca1998_Rx_58f52ce6_Rx 3u
#define PduRConf_PduRDestPdu_AnmMsg_Alarm_SecuritySubnet_oSecuritySubnet_9d33373d_Rx_59c455d2_Rx 4u
#define PduRConf_PduRDestPdu_AnmMsg_BBM_Backbone2_oBackbone2_03be1070_Rx_dafcbadc_Rx 5u
#define PduRConf_PduRDestPdu_AnmMsg_CCM_CabSubnet_oCabSubnet_2f78a729_Rx_824648ca_Rx 6u
#define PduRConf_PduRDestPdu_AnmMsg_DACU_Backbone2_oBackbone2_dbe92592_Rx_4a41b928_Rx 7u
#define PduRConf_PduRDestPdu_AnmMsg_DDM_SecuritySubnet_oSecuritySubnet_c6114aa9_Rx_bd5e212b_Rx 8u
#define PduRConf_PduRDestPdu_AnmMsg_ECUspare1_Backbone2_oBackbone2_00fe11e7_Rx_06ff8b63_Rx 9u
#define PduRConf_PduRDestPdu_AnmMsg_ECUspare2_Backbone2_oBackbone2_e3b7a645_Rx_52e2e014_Rx 10u
#define PduRConf_PduRDestPdu_AnmMsg_ECUspare5_SecuritySubnet_oSecuritySubnet_40a00d45_Rx_42489524_Rx 11u
#define PduRConf_PduRDestPdu_AnmMsg_ECUspare6_CabSubnet_oCabSubnet_3fe72d0f_Rx_6c24f169_Rx 12u
#define PduRConf_PduRDestPdu_AnmMsg_EMS_Backbone2_oBackbone2_bf0abd7e_Rx_90682d55_Rx 13u
#define PduRConf_PduRDestPdu_AnmMsg_HMIIOM_Backbone2_oBackbone2_7fa4f733_Rx_b5a91039_Rx 14u
#define PduRConf_PduRDestPdu_AnmMsg_LECM1_CabSubnet_oCabSubnet_aef1a94e_Rx_93860a26_Rx 15u
#define PduRConf_PduRDestPdu_AnmMsg_PDM_SecuritySubnet_oSecuritySubnet_412a9e12_Rx_c2f29ed3_Rx 16u
#define PduRConf_PduRDestPdu_AnmMsg_SRS_CabSubnet_oCabSubnet_5d05fd8e_Rx_b4c75839_Rx 17u
#define PduRConf_PduRDestPdu_AnmMsg_TECU_Backbone2_oBackbone2_d3c774dc_Rx_0a3da473_Rx 18u
#define PduRConf_PduRDestPdu_AnmMsg_VMCU_Backbone2_oBackbone2_d29bb903_Rx_7e99d331_Rx 19u
#define PduRConf_PduRDestPdu_AnmMsg_WRCS_CabSubnet_oCabSubnet_dea22ca5_Rx_f9521ac4_Rx 20u
#define PduRConf_PduRDestPdu_BBM_BB2_01P_oBackbone2_679bb0c1_Rx_4aacb8db_Rx 21u
#define PduRConf_PduRDestPdu_BBM_BB2_02P_oBackbone2_ae84b87e_Rx_72b80dcb_Rx 22u
#define PduRConf_PduRDestPdu_BBM_BB2_06P_oBackbone2_df0ea254_Rx_a833036d_Rx 23u
#define PduRConf_PduRDestPdu_CCFWtoCIOM_L4_oLIN03_65063ac5_Rx_c27f66fb_Rx 24u
#define PduRConf_PduRDestPdu_CCM_Cab_01P_oCabSubnet_d6dd7ea3_Rx_191cbf98_Rx 25u
#define PduRConf_PduRDestPdu_CCM_Cab_04P_oCabSubnet_568d6123_Rx_24a721a5_Rx 26u
#define PduRConf_PduRDestPdu_CCM_Cab_06P_oCabSubnet_6e486c36_Rx_d0453951_Rx 27u
#define PduRConf_PduRDestPdu_CCM_Cab_07P_oCabSubnet_9f92699c_Rx_76f8d1a5_Rx 28u
#define PduRConf_PduRDestPdu_CCM_Cab_08P_oCabSubnet_c4134f5d_Rx_f641b15a_Rx 29u
#define PduRConf_PduRDestPdu_CCVS_X_VMCU_oBackbone1J1939_dd344e88_Rx_2b017bd1_Rx 30u
#define PduRConf_PduRDestPdu_CVW_X_EBS_oBackbone1J1939_7745bc8f_Rx_41192726_Rx 31u
#define PduRConf_PduRDestPdu_DACU_BB2_02P_oBackbone2_a741c3bf_Rx_da0bb296_Rx 32u
#define PduRConf_PduRDestPdu_DDM_Sec_01P_oSecuritySubnet_d2cc7529_Rx_68f2dfe0_Rx 33u
#define PduRConf_PduRDestPdu_DLFWtoCIOM_L4_oLIN03_66a36f0d_Rx_a0a38a40_Rx 34u
#define PduRConf_PduRDestPdu_DebugCtrl1_CIOM_BB2_oBackbone2_be87033c_Rx_3cefdff1_Rx 35u
#define PduRConf_PduRDestPdu_DiagFaultStat_Alarm_Sec_oSecuritySubnet_c6873827_Rx_0a2f5e94_Rx 36u
#define PduRConf_PduRDestPdu_DiagFaultStat_CCM_Cab_oCabSubnet_1ee92cda_Rx_b10ab994_Rx 37u
#define PduRConf_PduRDestPdu_DiagFaultStat_DDM_Sec_oSecuritySubnet_279347f5_Rx_b27c64ee_Rx 38u
#define PduRConf_PduRDestPdu_DiagFaultStat_LECM_Cab_oCabSubnet_3cdbae0b_Rx_375b9c49_Rx 39u
#define PduRConf_PduRDestPdu_DiagFaultStat_PDM_Sec_oSecuritySubnet_7823c535_Rx_2c5c2c9c_Rx 40u
#define PduRConf_PduRDestPdu_DiagFaultStat_SRS_Cab_oCabSubnet_418c5993_Rx_7803fcd2_Rx 41u
#define PduRConf_PduRDestPdu_DiagFaultStat_WRCS_Cab_oCabSubnet_6c860dc8_Rx_ca104489_Rx 42u
#define PduRConf_PduRDestPdu_EBC1_X_EBS_oBackbone1J1939_0f6d94fb_Rx_04f9144d_Rx 43u
#define PduRConf_PduRDestPdu_EBC2_X_EBS_oBackbone1J1939_b4e68954_Rx_c5f96cc3_Rx 44u
#define PduRConf_PduRDestPdu_EBC5_X_EBS_oBackbone1J1939_71f8bf2e_Rx_be9b2da1_Rx 45u
#define PduRConf_PduRDestPdu_EBS_BB1_01P_oBackbone1J1939_06650efd_Rx_3c266640_Rx 46u
#define PduRConf_PduRDestPdu_EBS_BB1_02P_oBackbone1J1939_3e5e858e_Rx_df13874e_Rx 47u
#define PduRConf_PduRDestPdu_EBS_BB1_05P_oBackbone1J1939_ef101786_Rx_50cf6a28_Rx 48u
#define PduRConf_PduRDestPdu_EEC1_X_EMS_oBackbone1J1939_cdd2f3bf_Rx_25a1aa73_Rx 49u
#define PduRConf_PduRDestPdu_EEC2_X_EMS_oBackbone1J1939_7659ee10_Rx_995d31c8_Rx 50u
#define PduRConf_PduRDestPdu_ELCP1toCIOM_L4_oLIN03_be1ef9f9_Rx_b63a736f_Rx 51u
#define PduRConf_PduRDestPdu_ELCP2toCIOM_L4_oLIN03_7701f146_Rx_97ced8ff_Rx 52u
#define PduRConf_PduRDestPdu_EMS_BB2_01P_oBackbone2_440afce7_Rx_ab107697_Rx 53u
#define PduRConf_PduRDestPdu_EMS_BB2_04P_oBackbone2_c45ae367_Rx_ace186fb_Rx 54u
#define PduRConf_PduRDestPdu_EMS_BB2_05P_oBackbone2_3580e6cd_Rx_8f53c5a4_Rx 55u
#define PduRConf_PduRDestPdu_EMS_BB2_06P_oBackbone2_fc9fee72_Rx_2a8059b0_Rx 56u
#define PduRConf_PduRDestPdu_EMS_BB2_08P_oBackbone2_56c4cd19_Rx_d383fbe5_Rx 57u
#define PduRConf_PduRDestPdu_EMS_BB2_13P_oBackbone2_4a3d6101_Rx_4530d6dc_Rx 58u
#define PduRConf_PduRDestPdu_ERC1_X_EMSRet_oBackbone1J1939_63f95975_Rx_42e414fe_Rx 59u
#define PduRConf_PduRDestPdu_ERC1_X_RECU_oBackbone1J1939_d09c878a_Rx_740336e5_Rx 60u
#define PduRConf_PduRDestPdu_ET1_X_EMS_oBackbone1J1939_2b7be601_Rx_89ee5878_Rx 61u
#define PduRConf_PduRDestPdu_FMS1_X_HMIIOM_oBackbone1J1939_496422f1_Rx_5c4fad53_Rx 62u
#define PduRConf_PduRDestPdu_FSP1_Frame_L1_oLIN00_7041c142_Rx_e89970ce_Rx 63u
#define PduRConf_PduRDestPdu_FSP1_Frame_L2_oLIN01_dfcb5978_Rx_8621b565_Rx 64u
#define PduRConf_PduRDestPdu_FSP1_Frame_L3_oLIN02_f47956e4_Rx_65a06375_Rx 65u
#define PduRConf_PduRDestPdu_FSP1_Frame_L4_oLIN03_5baf6f4d_Rx_7272736c_Rx 66u
#define PduRConf_PduRDestPdu_FSP1_Frame_L5_oLIN04_a341e84f_Rx_1d41a162_Rx 67u
#define PduRConf_PduRDestPdu_FSP1_SwitchDetResp_L1_oLIN00_a6b440c8_Rx_7615762b_Rx 68u
#define PduRConf_PduRDestPdu_FSP1_SwitchDetResp_L2_oLIN01_093ed8f2_Rx_d86f19ae_Rx 69u
#define PduRConf_PduRDestPdu_FSP1_SwitchDetResp_L3_oLIN02_228cd76e_Rx_7bebd14f_Rx 70u
#define PduRConf_PduRDestPdu_FSP1_SwitchDetResp_L4_oLIN03_8d5aeec7_Rx_d430c522_Rx 71u
#define PduRConf_PduRDestPdu_FSP1_SwitchDetResp_L5_oLIN04_75b469c5_Rx_c187c3bc_Rx 72u
#define PduRConf_PduRDestPdu_FSP2_Frame_L1_oLIN00_b95ec9fd_Rx_3e9ce6c9_Rx 73u
#define PduRConf_PduRDestPdu_FSP2_Frame_L2_oLIN01_16d451c7_Rx_1d5f459f_Rx 74u
#define PduRConf_PduRDestPdu_FSP2_Frame_L3_oLIN02_3d665e5b_Rx_e2611c34_Rx 75u
#define PduRConf_PduRDestPdu_FSP2_SwitchDetResp_L1_oLIN00_8c08f040_Rx_807c58f3_Rx 76u
#define PduRConf_PduRDestPdu_FSP2_SwitchDetResp_L2_oLIN01_2382687a_Rx_7877d56d_Rx 77u
#define PduRConf_PduRDestPdu_FSP2_SwitchDetResp_L3_oLIN02_083067e6_Rx_25794016_Rx 78u
#define PduRConf_PduRDestPdu_FSP3_Frame_L2_oLIN01_e70e546d_Rx_9649cb45_Rx 79u
#define PduRConf_PduRDestPdu_FSP3_SwitchDetResp_L2_oLIN01_8cc6fa3d_Rx_1d7b2b95_Rx 80u
#define PduRConf_PduRDestPdu_FSP4_Frame_L2_oLIN01_5f9b46f8_Rx_be569886_Rx 81u
#define PduRConf_PduRDestPdu_FSP4_SwitchDetResp_L2_oLIN01_76fb096a_Rx_21916aa6_Rx 82u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_01P_oBackbone2_0a8179ad_Rx_fd64c8e1_Rx 83u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_02P_oBackbone2_c39e7112_Rx_68056745_Rx 84u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_03P_oBackbone2_324474b8_Rx_72f29192_Rx 85u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_05P_oBackbone2_7b0b6387_Rx_4ff35f83_Rx 86u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_07P_oBackbone2_43ce6e92_Rx_81d2b46b_Rx 87u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_08P_oBackbone2_184f4853_Rx_284127f9_Rx 88u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_09P_oBackbone2_e9954df9_Rx_ebcbfcee_Rx 89u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_10P_oBackbone2_cda9ecf4_Rx_d5e1623c_Rx 90u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_11P_oBackbone2_3c73e95e_Rx_db97e616_Rx 91u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_12P_oBackbone2_f56ce1e1_Rx_81ebe5fa_Rx 92u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_13P_oBackbone2_04b6e44b_Rx_e5d733f1_Rx 93u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_14P_oBackbone2_bc23f6de_Rx_b3cb357b_Rx 94u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_15P_oBackbone2_4df9f374_Rx_8626e9b6_Rx 95u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_16P_oBackbone2_84e6fbcb_Rx_044f8724_Rx 96u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_17P_oBackbone2_753cfe61_Rx_f53e9dc3_Rx 97u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_18P_oBackbone2_2ebdd8a0_Rx_a5cafa44_Rx 98u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_22P_oBackbone2_ae7b50f4_Rx_a0b53fe4_Rx 99u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_23P_oBackbone2_5fa1555e_Rx_a0403af9_Rx 100u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_24P_oBackbone2_e73447cb_Rx_950739fa_Rx 101u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_25P_oBackbone2_16ee4261_Rx_f66bd68c_Rx 102u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_33P_oBackbone2_6953c5ad_Rx_ce11e24e_Rx 103u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_34S_oBackbone2_c2eeee4b_Rx_f28ba4c2_Rx 104u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_38P_oBackbone2_4358f946_Rx_71de9aba_Rx 105u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_39P_oBackbone2_b282fcec_Rx_9fa6bf08_Rx 106u
#define PduRConf_PduRDestPdu_HRLFC_X_EMS_oBackbone1J1939_04c7cc2f_Rx_1a2533ca_Rx 107u
#define PduRConf_PduRDestPdu_ILCP1toCIOM_L1_oLIN00_dd00be4f_Rx_c4212af7_Rx 108u
#define PduRConf_PduRDestPdu_ILCP2toCIOM_L4_oLIN03_3ff118ff_Rx_8f05a91a_Rx 109u
#define PduRConf_PduRDestPdu_LECM1_Cab_02P_oCabSubnet_7b05953d_Rx_bc4ff018_Rx 110u
#define PduRConf_PduRDestPdu_LECM1_Cab_03S_oCabSubnet_99f7a9e4_Rx_0a93f264_Rx 111u
#define PduRConf_PduRDestPdu_LECM1_Cab_04P_oCabSubnet_324a8202_Rx_406dd4e6_Rx 112u
#define PduRConf_PduRDestPdu_LECM1_Cab_05S_oCabSubnet_d0b8bedb_Rx_3319cd54_Rx 113u
#define PduRConf_PduRDestPdu_LECM2toCIOM_FR1_L1_oLIN00_5adbc53c_Rx_8cd02125_Rx 114u
#define PduRConf_PduRDestPdu_LECM2toCIOM_FR2_L1_oLIN00_a88c28c4_Rx_786aa0cd_Rx 115u
#define PduRConf_PduRDestPdu_LECM2toCIOM_FR3_L1_oLIN00_4f918e53_Rx_73b12bc9_Rx 116u
#define PduRConf_PduRDestPdu_LECMBasic2CIOM_L1_oLIN00_efdd1437_Rx_7332fc25_Rx 117u
#define PduRConf_PduRDestPdu_LFE_X_EMS_oBackbone1J1939_8a36c848_Rx_2d2f558b_Rx 118u
#define PduRConf_PduRDestPdu_LinSlave_Frame_L6_oLIN05_e9182918_Rx_562027d3_Rx 119u
#define PduRConf_PduRDestPdu_LinSlave_Frame_L7_oLIN06_c2aa2684_Rx_10738768_Rx 120u
#define PduRConf_PduRDestPdu_LinSlave_Frame_L8_oLIN07_6dc55c0b_Rx_b4f1c06a_Rx 121u
#define PduRConf_PduRDestPdu_PDM_Sec_01P_oSecuritySubnet_5921b61c_Rx_9d822126_Rx 122u
#define PduRConf_PduRDestPdu_PduRDestPdu_2                            125u
#define PduRConf_PduRDestPdu_PduRDestPdu_3                            126u
#define PduRConf_PduRDestPdu_RCECStoCIOM_FR2_L5_oLIN04_90f69d81_Rx_256e501a_Rx 123u
#define PduRConf_PduRDestPdu_RCECStoCIOM_L5_oLIN04_1ccecc00_Rx_5d6ab1b6_Rx 124u
#define PduRConf_PduRDestPdu_SRS_Cab_01P_oCabSubnet_81a51028_Rx_b0d30411_Rx 127u
#define PduRConf_PduRDestPdu_SRS_Cab_03P_oCabSubnet_b9601d3d_Rx_dc041688_Rx 128u
#define PduRConf_PduRDestPdu_SRS_Cab_04P_oCabSubnet_01f50fa8_Rx_3354072d_Rx 129u
#define PduRConf_PduRDestPdu_SRS_Cab_05P_oCabSubnet_f02f0a02_Rx_6aca3982_Rx 130u
#define PduRConf_PduRDestPdu_SRS_Cab_06P_oCabSubnet_393002bd_Rx_08468df3_Rx 131u
#define PduRConf_PduRDestPdu_TCO1_X_TACHO_oBackbone1J1939_3f1c366d_Rx_2b492f9e_Rx 132u
#define PduRConf_PduRDestPdu_TCPtoMaster_oLIN02_30042c71_Rx_93b09a1a_Rx 133u
#define PduRConf_PduRDestPdu_TD_X_HMIIOM_oBackbone1J1939_5060289e_Rx_69dcd8e6_Rx 134u
#define PduRConf_PduRDestPdu_TECU_BB2_01P_oBackbone2_6bd4ec68_Rx_8f00e2b8_Rx 135u
#define PduRConf_PduRDestPdu_TECU_BB2_02P_oBackbone2_a2cbe4d7_Rx_ae6f393e_Rx 136u
#define PduRConf_PduRDestPdu_Tester_CAN6toLIN_oCAN6_aba10671_Rx_03f5e325_Rx 137u
#define PduRConf_PduRDestPdu_VDC1_X_EBS_oBackbone1J1939_e351e41c_Rx_ec0493f7_Rx 138u
#define PduRConf_PduRDestPdu_VDC2_X_EBS_oBackbone1J1939_58daf9b3_Rx_ee4e9e34_Rx 139u
#define PduRConf_PduRDestPdu_VDHR_X_VMCU_oBackbone1J1939_27498c2c_Rx_44121a6e_Rx 140u
#define PduRConf_PduRDestPdu_VMCU_BB1_01P_oBackbone1J1939_73c9d3c7_Rx_dae9aba0_Rx 141u
#define PduRConf_PduRDestPdu_VMCU_BB1_03P_oBackbone1J1939_eacbdc5a_Rx_62ecd252_Rx 142u
#define PduRConf_PduRDestPdu_VMCU_BB2_01P_oBackbone2_dfae7ff9_Rx_aa24f793_Rx 143u
#define PduRConf_PduRDestPdu_VMCU_BB2_02P_oBackbone2_16b17746_Rx_1ab720b8_Rx 144u
#define PduRConf_PduRDestPdu_VMCU_BB2_03P_oBackbone2_e76b72ec_Rx_c5e31929_Rx 145u
#define PduRConf_PduRDestPdu_VMCU_BB2_04P_oBackbone2_5ffe6079_Rx_5090446e_Rx 146u
#define PduRConf_PduRDestPdu_VMCU_BB2_05P_oBackbone2_ae2465d3_Rx_20bae62b_Rx 147u
#define PduRConf_PduRDestPdu_VMCU_BB2_07P_oBackbone2_96e168c6_Rx_4731dc7b_Rx 148u
#define PduRConf_PduRDestPdu_VMCU_BB2_08P_oBackbone2_cd604e07_Rx_e56fc5e2_Rx 149u
#define PduRConf_PduRDestPdu_VMCU_BB2_20P_oBackbone2_43915bb5_Rx_96d8e8cf_Rx 150u
#define PduRConf_PduRDestPdu_VMCU_BB2_29P_oBackbone2_515f6a4b_Rx_35018803_Rx 151u
#define PduRConf_PduRDestPdu_VMCU_BB2_51P_oBackbone2_3296acc6_Rx_14bfec1c_Rx 152u
#define PduRConf_PduRDestPdu_VMCU_BB2_52P_oBackbone2_fb89a479_Rx_db8fea9c_Rx 153u
#define PduRConf_PduRDestPdu_VMCU_BB2_53P_oBackbone2_0a53a1d3_Rx_2574e9a5_Rx 154u
#define PduRConf_PduRDestPdu_VMCU_BB2_54P_oBackbone2_b2c6b346_Rx_a390fe54_Rx 155u
#define PduRConf_PduRDestPdu_VMCU_BB2_55P_oBackbone2_431cb6ec_Rx_91dd0e09_Rx 156u
#define PduRConf_PduRDestPdu_VMCU_BB2_58P_oBackbone2_20589d38_Rx_7e092eee_Rx 157u
#define PduRConf_PduRDestPdu_VMCU_BB2_73P_oBackbone2_67b68035_Rx_e01b8f9b_Rx 158u
#define PduRConf_PduRDestPdu_VMCU_BB2_74P_oBackbone2_df2392a0_Rx_fc4f7394_Rx 159u
#define PduRConf_PduRDestPdu_VMCU_BB2_80P_oBackbone2_4291fb8a_Rx_1d885997_Rx 160u
#define PduRConf_PduRDestPdu_VMCU_BB2_82P_oBackbone2_7a54f69f_Rx_df538218_Rx 161u
#define PduRConf_PduRDestPdu_VP232_X_ERAU_oFMSNet_67abd5b0_Rx_c6e87d5c_Rx 162u
#define PduRConf_PduRDestPdu_WRCS_Cab_01P_oCabSubnet_76b95594_Rx_098a7866_Rx 163u
#define PduRConf_PduRDestPdu_WRCS_Cab_02P_oCabSubnet_bfa65d2b_Rx_3253ad4f_Rx 164u
#define PduRConf_PduRDestPdu_WRCS_Cab_03P_oCabSubnet_4e7c5881_Rx_182e241a_Rx 165u
/**\} */

/**
 * \defgroup PduRHandleIdsIfRxSrc Handle IDs of handle space IfRxSrc.
 * \brief Communication interface Rx source PDUs
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0a2f5e94                       43u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0a3da473                       18u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0a93f264                       118u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1a2533ca                       114u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1ab720b8                       151u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1d5f459f                       81u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1d7b2b95                       87u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1d41a162                       74u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1d885997                       167u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1dcb13ac                       33u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2                              130u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2a8059b0                       63u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2b017bd1                       30u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2b492f9e                       139u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2c5c2c9c                       47u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2d2f558b                       125u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3                              131u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3c266640                       53u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3cefdff1                       42u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3e9ce6c9                       80u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_4a41b928                       7u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_4aacb8db                       21u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_4ff35f83                       93u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5c4fad53                       69u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5d6ab1b6                       133u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5dd96628                       31u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_6aca3982                       137u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_6c24f169                       12u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_7bebd14f                       77u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_7df464da                       32u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_7e99d331                       19u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_7e092eee                       164u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8a94d610                       35u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8cd02125                       121u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8f00e2b8                       142u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8f05a91a                       116u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8f53c5a4                       62u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_9d822126                       129u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_9fa6bf08                       113u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_00ee91b2                       2u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_03f5e325                       144u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_04f9144d                       50u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_06ff8b63                       9u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_14bfec1c                       159u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_20bae62b                       154u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_24a721a5                       26u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_25a1aa73                       56u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_42e414fe                       66u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_50cf6a28                       55u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_52e2e014                       10u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_58f52ce6                       3u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_59c455d2                       4u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_62ecd252                       149u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_65a06375                       72u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_66b8580e                       1u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_68f2dfe0                       40u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_69dcd8e6                       141u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_71de9aba                       112u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_72b80dcb                       22u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_72f29192                       92u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_73b12bc9                       123u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_76f8d1a5                       28u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_81d2b46b                       94u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_81ebe5fa                       99u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_89ee5878                       68u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_91dd0e09                       163u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_93b09a1a                       140u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_96d8e8cf                       157u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_97ced8ff                       59u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_044f8724                       103u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_098a7866                       170u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_182e241a                       172u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_191cbf98                       25u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_256e501a                       132u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_375b9c49                       46u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_406dd4e6                       119u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_786aa0cd                       122u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_807c58f3                       83u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_896e3989                       36u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_995d31c8                       57u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2574e9a5                       161u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3253ad4f                       171u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3319cd54                       120u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_4530d6dc                       65u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_4731dc7b                       155u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_7332fc25                       124u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_7803fcd2                       48u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_7877d56d                       84u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8621b565                       71u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8626e9b6                       102u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_9649cb45                       86u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_08468df3                       138u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_21916aa6                       89u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_44121a6e                       147u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_90682d55                       13u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_93860a26                       15u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_284127f9                       95u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_562027d3                       126u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_740336e5                       67u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_824648ca                       6u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_950739fa                       108u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3354072d                       136u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5090446e                       153u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_7272736c                       73u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_7615762b                       75u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_10738768                       127u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_25794016                       85u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_35018803                       158u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_41192726                       38u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_42489524                       11u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_68056745                       91u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a0a38a40                       41u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a0b53fe4                       106u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a5cafa44                       105u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a390fe54                       162u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a0403af9                       107u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a833036d                       23u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_aa24f793                       150u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ab107697                       60u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ace186fb                       61u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ae6f393e                       143u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b0d30411                       134u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b3cb357b                       101u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b4c75839                       17u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b4f1c06a                       128u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b5a91039                       14u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b10ab994                       44u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b27c64ee                       45u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b63a736f                       58u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_bb6c3932                       0u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_bc4ff018                       117u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_bd5e212b                       8u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_be9b2da1                       52u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_be569886                       88u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c2f29ed3                       16u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c5e31929                       152u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c5f96cc3                       51u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c6e87d5c                       169u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c27f66fb                       24u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c187c3bc                       79u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c4212af7                       115u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ca104489                       49u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ce11e24e                       110u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ce15f821                       34u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d5e1623c                       97u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d17a9913                       37u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d86f19ae                       76u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d383fbe5                       64u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d430c522                       78u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d0453951                       27u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_da0bb296                       39u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_dae9aba0                       148u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_dafcbadc                       5u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_db8fea9c                       160u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_db97e616                       98u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_dc041688                       135u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_df13874e                       54u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_df538218                       168u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e5d733f1                       100u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e01b8f9b                       165u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e56fc5e2                       156u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e2611c34                       82u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e89970ce                       70u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ebcbfcee                       96u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ec0493f7                       145u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ee4e9e34                       146u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f28ba4c2                       111u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f53e9dc3                       104u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f66bd68c                       109u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f641b15a                       29u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f9521ac4                       20u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_fc4f7394                       166u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_fd64c8e1                       90u
/**\} */

/**
 * \defgroup PduRHandleIdsIfTpTxSrc Handle IDs of handle space IfTpTxSrc.
 * \brief Communication interface and transport protocol Tx PDUs
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define PduRConf_PduRSrcPdu_PduRSrcPdu                                262u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0a421c29                       32u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0af615bd                       54u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0b03aa0d                       114u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0b32cf77                       234u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0bb1cb67                       19u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0c1b703d                       46u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0f0671d5                       27u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0fa3c683                       100u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1                              263u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1ac1c61f                       243u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1afac4d5                       78u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1bed3579                       191u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1c80e599                       77u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1edf09de                       239u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1f9a8949                       84u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2b407fd5                       76u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2bd9ef52                       242u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2bda7b06                       119u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2c4f8116                       45u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2e7a3026                       124u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2f1cafc0                       237u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2f32e214                       66u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3b966455                       88u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3bdd78a6                       149u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3c43bde2                       30u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3cd6432e                       233u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3d06fbde                       135u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3d78f7a3                       72u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3ec0669e                       110u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3edf9753                       150u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3f422fc3                       115u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3fb3dd7e                       93u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_4a8c70b8                       82u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_4d8d89cb                       268u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_4de8eb8d                       195u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_4e85369a                       171u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_4e724430                       203u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_4f0d025c                       177u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5a8b564b                       141u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5a647c4b                       187u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5bb7f426                       7u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5f2267d8                       29u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5fe91aff                       91u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_6a8e199b                       75u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_6bb04367                       138u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_6bd99b39                       61u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_6ca67470                       68u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_6cf0fd3c                       175u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_6d3fc1f8                       172u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_7a80252e                       23u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_7ad18be8                       193u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_7f47f3d8                       104u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8a6bf288                       226u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8ad2861d                       173u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8c275581                       139u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8f4e749f                       99u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8fec9c64                       248u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_9afa723d                       161u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_9d0a2dde                       38u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_9d48ed4d                       196u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_9de0f43a                       94u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_9eb2e6b1                       212u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_9f126e32                       47u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_01acc4de                       126u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_01ba327b                       167u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_01d86c9d                       185u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_03e2b654                       258u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_04f9241b                       69u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_05d2c983                       42u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_06f4c24e                       62u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_07bbf63b                       17u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_09be0918                       264u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_09e3f854                       98u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_12ac6f81                       18u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_13dbaa10                       102u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_14f3876f                       112u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_18b1f013                       197u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_18bb5a35                       272u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_18f55cec                       14u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_20ff6e11                       174u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_21c3b394                       259u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_23a300e9                       55u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_23f01529                       194u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_25d1c4f5                       92u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_29cdd88d                       65u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_30fd7928                       224u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_35a1b692                       221u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_42d23fef                       33u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_44bef04e                       37u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_44e5925a                       34u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_47b56dbe                       249u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_48abb014                       146u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_51adf99a                       235u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_52be8b22                       10u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_55c8fe9a                       232u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_59fd94e4                       113u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_60af6526                       31u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_62dfcb6f                       202u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_96efa9e2                       109u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_017c679d                       79u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_061b4d4d                       121u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_100ee8e0                       64u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_146f13b5                       4u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_224a6cfa                       136u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_289cd2f3                       222u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_289e8325                       2u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_379abf8d                       199u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_415f4d5f                       132u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_433f0139                       238u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_477d6a44                       152u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_509ea690                       255u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_543d450a                       183u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_589f7780                       273u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_667ae06c                       214u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_710a49e6                       176u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_787b56aa                       70u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_811e958b                       246u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_918dff20                       71u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_927d3065                       252u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_942ead34                       0u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_946b283b                       108u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_973cde10                       25u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_976d8e31                       229u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_991b9435                       206u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0460a299                       274u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1345fc62                       35u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1651d15b                       145u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1885f5b6                       231u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1948bc78                       162u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2428ced7                       131u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2584d1ef                       151u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3436a065                       210u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3835a861                       8u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_4205e179                       22u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_4290d944                       134u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_4819b774                       142u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5221f1e9                       137u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5547c4e8                       140u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8286f31d                       240u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8822de73                       215u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8844b462                       188u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8888e39c                       118u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_03636d0a                       261u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_36322f96                       223u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_42404c3c                       129u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_42462b26                       209u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_58768f62                       21u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_60849a09                       15u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_77877d8c                       106u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_78074b0a                       24u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_80304e06                       53u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_80862b22                       39u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_86064c1e                       9u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_044119da                       105u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_108627d2                       186u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_118133ce                       270u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_166169a8                       13u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_206364a8                       204u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_292228a9                       170u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_324776e1                       16u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_692170b2                       87u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_973737d6                       36u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5862727a                       251u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8829348e                       20u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_27339506                       166u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_30324227                       201u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_54916946                       97u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_89789648                       26u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a2b3fcad                       207u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a3a2462e                       271u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a3b679cc                       169u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a3dfff04                       247u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a9b97fd8                       120u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a03b4124                       52u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a18d3920                       192u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a28cb0c8                       51u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a47b7d57                       143u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a71a8e0d                       184u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a74cbc04                       200u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a145b9df                       198u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a305ad93                       163u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a501c7de                       241u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a812ddd3                       266u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a3478da4                       220u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a15156f5                       257u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a24245d0                       250u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_aa6f2d0d                       28u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_abb677a0                       59u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ac05acdd                       160u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ac05d3d4                       148u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ad4a6c2f                       1u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_aedbbc7e                       117u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b0d5198a                       216u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b2d43218                       230u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b4c3edc8                       154u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b5ff5295                       3u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b7adda5b                       90u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b8e6d439                       144u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b58a23ae                       127u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b359c6a9                       73u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b7904bda                       156u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b93638dc                       101u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ba710b90                       180u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ba427217                       6u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_bc8ff73d                       256u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_bc75d45f                       41u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_bcbb5e6c                       56u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_bcf724a3                       95u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_be3fbdb6                       128u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_bfa5c35c                       57u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_bfa99e31                       219u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c1ab151b                       111u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c2aa600c                       74u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c4d27be7                       190u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c8bc4c29                       130u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c21cbda2                       260u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c731ca07                       125u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c980bc3d                       12u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c0583aba                       83u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c8760d26                       205u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ca0ceb23                       63u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_cad5212d                       85u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_cb9cbc04                       116u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ccdd7242                       153u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ccfd4d5a                       245u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_cd66d79f                       40u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_cdef0f36                       44u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ce8b3ea0                       50u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_cff258d1                       86u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d0cf429b                       96u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d9b333e4                       178u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d25a31a4                       168u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d29bec1e                       267u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d71ba6b1                       67u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d065da29                       213u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d582f029                       179u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d620f52d                       157u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d6070b6b                       133u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d8682d70                       5u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d58909f4                       254u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d5491018                       122u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_da78cefd                       265u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_dbfd746c                       80u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_dd74b571                       181u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_dde2c294                       60u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_df90de79                       81u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_dfdfd2a3                       155u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_dfef8e98                       228u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e1be2636                       103u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e7e7cd78                       189u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e9b90537                       225u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e9cb21a0                       147u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e38dbced                       253u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e73ae741                       165u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e163cbd8                       11u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e612e27f                       58u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e908cc2f                       269u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e918ef53                       107u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e97535e6                       123u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e513302a                       158u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ea1d6e05                       182u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ec4815db                       236u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ee7f0bf8                       159u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_effbe257                       211u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f1d29dcd                       49u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f40bc261                       89u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f008f462                       244u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f258e735                       217u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f438a6b7                       218u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f0799c66                       43u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f4695c57                       48u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_fa91c439                       164u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_fd379ced                       208u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_fde47258                       227u
/**\} */

/**
 * \defgroup PduRHandleIdsIfTxDest Handle IDs of handle space IfTxDest.
 * \brief Communication interface Tx destination PDUs
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define PduRConf_PduRDestPdu_AIR1_X_CIOMFMS_oFMSNet_94b967fe_Tx       0u
#define PduRConf_PduRDestPdu_AMB_X_CIOMFMS_oFMSNet_7d255a73_Tx        1u
#define PduRConf_PduRDestPdu_AT1T1I1_X_CIOMFMS_oFMSNet_a4250889_Tx    2u
#define PduRConf_PduRDestPdu_AnmMsg_CIOM_Backbone2_oBackbone2_cf5ab87f_Tx 5u
#define PduRConf_PduRDestPdu_AnmMsg_CIOM_CabSubnet_oCabSubnet_05b67d33_Tx 6u
#define PduRConf_PduRDestPdu_AnmMsg_CIOM_SecuritySubnet_oSecuritySubnet_e244ca05_Tx 7u
#define PduRConf_PduRDestPdu_Backlight_FuncInd_L1_oLIN00_841aab0e_Tx  8u
#define PduRConf_PduRDestPdu_Backlight_FuncInd_L2_oLIN01_2b903334_Tx  9u
#define PduRConf_PduRDestPdu_Backlight_FuncInd_L3_oLIN02_00223ca8_Tx  10u
#define PduRConf_PduRDestPdu_Backlight_FuncInd_L4_oLIN03_aff40501_Tx  11u
#define PduRConf_PduRDestPdu_Backlight_FuncInd_L5_oLIN04_571a8203_Tx  12u
#define PduRConf_PduRDestPdu_Backlight_FuncInd_L6_oLIN05_f8901a39_Tx  13u
#define PduRConf_PduRDestPdu_Backlight_FuncInd_L7_oLIN06_d32215a5_Tx  14u
#define PduRConf_PduRDestPdu_Backlight_FuncInd_L8_oLIN07_7c4d6f2a_Tx  15u
#define PduRConf_PduRDestPdu_CCVS_X_CIOMFMS_oFMSNet_559ff1c7_Tx       16u
#define PduRConf_PduRDestPdu_CIOM_3da9a113_Tx                         95u
#define PduRConf_PduRDestPdu_CIOM_8ae1f502_Tx                         92u
#define PduRConf_PduRDestPdu_CIOM_03a719b0_Tx                         97u
#define PduRConf_PduRDestPdu_CIOM_838783a7_Tx                         93u
#define PduRConf_PduRDestPdu_CIOM_91559201_Tx                         90u
#define PduRConf_PduRDestPdu_CIOM_BB1_01P_oBackbone1J1939_6c85c28e_Tx 17u
#define PduRConf_PduRDestPdu_CIOM_BB2_01P_oBackbone2_12f7ffb9_Tx      18u
#define PduRConf_PduRDestPdu_CIOM_BB2_02P_oBackbone2_dbe8f706_Tx      19u
#define PduRConf_PduRDestPdu_CIOM_BB2_03P_oBackbone2_2a32f2ac_Tx      20u
#define PduRConf_PduRDestPdu_CIOM_BB2_04P_oBackbone2_92a7e039_Tx      21u
#define PduRConf_PduRDestPdu_CIOM_BB2_05P_oBackbone2_637de593_Tx      22u
#define PduRConf_PduRDestPdu_CIOM_BB2_06P_oBackbone2_aa62ed2c_Tx      23u
#define PduRConf_PduRDestPdu_CIOM_BB2_07P_oBackbone2_5bb8e886_Tx      24u
#define PduRConf_PduRDestPdu_CIOM_BB2_10P_oBackbone2_d5df6ae0_Tx      25u
#define PduRConf_PduRDestPdu_CIOM_BB2_11P_oBackbone2_24056f4a_Tx      26u
#define PduRConf_PduRDestPdu_CIOM_BB2_20S_oBackbone2_9de0e286_Tx      27u
#define PduRConf_PduRDestPdu_CIOM_BB2_21S_oBackbone2_6c3ae72c_Tx      28u
#define PduRConf_PduRDestPdu_CIOM_BB2_23S_oBackbone2_54ffea39_Tx      29u
#define PduRConf_PduRDestPdu_CIOM_BB2_24S_oBackbone2_ec6af8ac_Tx      30u
#define PduRConf_PduRDestPdu_CIOM_BB2_25P_oBackbone2_0e98c475_Tx      31u
#define PduRConf_PduRDestPdu_CIOM_BB2_26S_oBackbone2_d4aff5b9_Tx      32u
#define PduRConf_PduRDestPdu_CIOM_BB2_27S_oBackbone2_2575f013_Tx      33u
#define PduRConf_PduRDestPdu_CIOM_BB2_28P_oBackbone2_6ddcefa1_Tx      34u
#define PduRConf_PduRDestPdu_CIOM_BB2_29P_oBackbone2_9c06ea0b_Tx      35u
#define PduRConf_PduRDestPdu_CIOM_Cab_01P_oCabSubnet_e4e2333d_Tx      36u
#define PduRConf_PduRDestPdu_CIOM_Cab_02P_oCabSubnet_2dfd3b82_Tx      37u
#define PduRConf_PduRDestPdu_CIOM_Cab_03P_oCabSubnet_dc273e28_Tx      38u
#define PduRConf_PduRDestPdu_CIOM_Cab_06P_oCabSubnet_5c7721a8_Tx      39u
#define PduRConf_PduRDestPdu_CIOM_Cab_07P_oCabSubnet_adad2402_Tx      40u
#define PduRConf_PduRDestPdu_CIOM_Cab_08P_oCabSubnet_f62c02c3_Tx      41u
#define PduRConf_PduRDestPdu_CIOM_Cab_09P_oCabSubnet_07f60769_Tx      42u
#define PduRConf_PduRDestPdu_CIOM_Cab_10P_oCabSubnet_23caa664_Tx      43u
#define PduRConf_PduRDestPdu_CIOM_Cab_12P_oCabSubnet_1b0fab71_Tx      44u
#define PduRConf_PduRDestPdu_CIOM_Cab_14P_oCabSubnet_5240bc4e_Tx      45u
#define PduRConf_PduRDestPdu_CIOM_Cab_15P_oCabSubnet_a39ab9e4_Tx      46u
#define PduRConf_PduRDestPdu_CIOM_Cab_16P_oCabSubnet_6a85b15b_Tx      47u
#define PduRConf_PduRDestPdu_CIOM_Cab_17P_oCabSubnet_9b5fb4f1_Tx      48u
#define PduRConf_PduRDestPdu_CIOM_Cab_18P_oCabSubnet_c0de9230_Tx      49u
#define PduRConf_PduRDestPdu_CIOM_Cab_19P_oCabSubnet_3104979a_Tx      50u
#define PduRConf_PduRDestPdu_CIOM_Cab_21P_oCabSubnet_890712db_Tx      51u
#define PduRConf_PduRDestPdu_CIOM_Cab_22P_oCabSubnet_40181a64_Tx      52u
#define PduRConf_PduRDestPdu_CIOM_Cab_23P_oCabSubnet_b1c21fce_Tx      53u
#define PduRConf_PduRDestPdu_CIOM_Cab_24P_oCabSubnet_09570d5b_Tx      54u
#define PduRConf_PduRDestPdu_CIOM_Cab_26P_oCabSubnet_3192004e_Tx      55u
#define PduRConf_PduRDestPdu_CIOM_Cab_27P_oCabSubnet_c04805e4_Tx      56u
#define PduRConf_PduRDestPdu_CIOM_Cab_28S_oCabSubnet_88e11a56_Tx      57u
#define PduRConf_PduRDestPdu_CIOM_Cab_31P_oCabSubnet_bff58228_Tx      58u
#define PduRConf_PduRDestPdu_CIOM_Cab_32P_oCabSubnet_76ea8a97_Tx      59u
#define PduRConf_PduRDestPdu_CIOM_Cab_33P_oCabSubnet_87308f3d_Tx      60u
#define PduRConf_PduRDestPdu_CIOM_Sec_01P_oSecuritySubnet_703c4de6_Tx 61u
#define PduRConf_PduRDestPdu_CIOM_Sec_02P_oSecuritySubnet_4807c695_Tx 62u
#define PduRConf_PduRDestPdu_CIOM_Sec_03P_oSecuritySubnet_e93e427b_Tx 63u
#define PduRConf_PduRDestPdu_CIOM_Sec_04P_oSecuritySubnet_3870d073_Tx 64u
#define PduRConf_PduRDestPdu_CIOM_b67a9f0a_Tx                         96u
#define PduRConf_PduRDestPdu_CIOM_f237b6db_Tx                         94u
#define PduRConf_PduRDestPdu_CIOMtoSlaves1_L1_oLIN00_09e24f2a_Tx      65u
#define PduRConf_PduRDestPdu_CIOMtoSlaves1_L4_oLIN03_220ce125_Tx      66u
#define PduRConf_PduRDestPdu_CIOMtoSlaves2_FR1_L1_oLIN00_573c3f76_Tx  67u
#define PduRConf_PduRDestPdu_CIOMtoSlaves2_FR2_L1_oLIN00_a56bd28e_Tx  68u
#define PduRConf_PduRDestPdu_CIOMtoSlaves2_FR3_L1_oLIN00_42767419_Tx  69u
#define PduRConf_PduRDestPdu_CIOMtoSlaves2_L4_oLIN03_d05b0cdd_Tx      70u
#define PduRConf_PduRDestPdu_CIOMtoSlaves_FR2_L5_oLIN04_7c0e76b4_Tx   71u
#define PduRConf_PduRDestPdu_CIOMtoSlaves_L5_oLIN04_251caba0_Tx       72u
#define PduRConf_PduRDestPdu_CL_X_CIOMFMS_oFMSNet_5e9df939_Tx         73u
#define PduRConf_PduRDestPdu_CVW_X_CIOMFMS_oFMSNet_5a4bdaa1_Tx        74u
#define PduRConf_PduRDestPdu_DD_X_CIOMFMS_oFMSNet_c8687b55_Tx         75u
#define PduRConf_PduRDestPdu_Debug02_CIOM_BB2_oBackbone2_a77a3695_Tx  76u
#define PduRConf_PduRDestPdu_Debug03_CIOM_BB2_oBackbone2_083ea4d2_Tx  77u
#define PduRConf_PduRDestPdu_Debug04_CIOM_BB2_oBackbone2_f2035785_Tx  78u
#define PduRConf_PduRDestPdu_Debug05_CIOM_BB2_oBackbone2_5d47c5c2_Tx  79u
#define PduRConf_PduRDestPdu_Debug06_CIOM_BB2_oBackbone2_77fb754a_Tx  80u
#define PduRConf_PduRDestPdu_Debug07_CIOM_BB2_oBackbone2_d8bfe70d_Tx  81u
#define PduRConf_PduRDestPdu_DiagFaultStat_Alarm_BB2_oBackbone2_8c5e38cc_Tx 82u
#define PduRConf_PduRDestPdu_DiagFaultStat_CCM_BB2_oBackbone2_9a44ed2c_Tx 83u
#define PduRConf_PduRDestPdu_DiagFaultStat_CIOM_BB2_oBackbone2_e34a7a3f_Tx 84u
#define PduRConf_PduRDestPdu_DiagFaultStat_DDM_BB2_oBackbone2_f2c6a79a_Tx 85u
#define PduRConf_PduRDestPdu_DiagFaultStat_LECM_BB2_oBackbone2_b8766ffd_Tx 86u
#define PduRConf_PduRDestPdu_DiagFaultStat_PDM_BB2_oBackbone2_fea9ad98_Tx 87u
#define PduRConf_PduRDestPdu_DiagFaultStat_SRS_BB2_oBackbone2_c5219865_Tx 88u
#define PduRConf_PduRDestPdu_DiagFaultStat_WRCS_BB2_oBackbone2_e82bcc3e_Tx 89u
#define PduRConf_PduRDestPdu_DiagUUDTRespMsg1_F2_40_BB2_oBackbone2_e836b7de_Tx 91u
#define PduRConf_PduRDestPdu_EEC1_X_CIOMFMS_oFMSNet_09cdd7bd_Tx       98u
#define PduRConf_PduRDestPdu_EEC2_X_CIOMFMS_oFMSNet_821ee9a4_Tx       99u
#define PduRConf_PduRDestPdu_EEC14_X_CIOMFMS_oFMSNet_2007104d_Tx      100u
#define PduRConf_PduRDestPdu_ERC1_x_EMSRetFMS_oFMSNet_04fe6f08_Tx     101u
#define PduRConf_PduRDestPdu_ERC1_x_RECUFMS_oFMSNet_f10a8a51_Tx       102u
#define PduRConf_PduRDestPdu_ET1_X_CIOMFMS_oFMSNet_a8a2ee82_Tx        103u
#define PduRConf_PduRDestPdu_FMS1_X_CIOMFMS_oFMSNet_79c218f8_Tx       104u
#define PduRConf_PduRDestPdu_FMS_X_CIOMFMS_oFMSNet_c973602f_Tx        105u
#define PduRConf_PduRDestPdu_FSP_1_2_Req_L1_oLIN00_bfd0179c_Tx        106u
#define PduRConf_PduRDestPdu_FSP_1_2_Req_L2_oLIN01_105a8fa6_Tx        107u
#define PduRConf_PduRDestPdu_FSP_1_2_Req_L3_oLIN02_3be8803a_Tx        108u
#define PduRConf_PduRDestPdu_FSP_1_2_Req_L4_oLIN03_943eb993_Tx        109u
#define PduRConf_PduRDestPdu_FSP_1_2_Req_L5_oLIN04_6cd03e91_Tx        110u
#define PduRConf_PduRDestPdu_FSP_3_4_Req_L2_oLIN01_a1ddc883_Tx        111u
#define PduRConf_PduRDestPdu_HOURS_X_CIOMFMS_oFMSNet_012e8f51_Tx      112u
#define PduRConf_PduRDestPdu_HRLFC_X_CIOMFMS_oFMSNet_7d441e5f_Tx      113u
#define PduRConf_PduRDestPdu_LFC_X_CIOMFMS_oFMSNet_94fe6bb5_Tx        114u
#define PduRConf_PduRDestPdu_LFE_X_CIOMFMS_oFMSNet_582911c6_Tx        115u
#define PduRConf_PduRDestPdu_MastertoTCP_oLIN02_7c7ed3d1_Tx           116u
#define PduRConf_PduRDestPdu_PTODE_X_CIOMFMS_oFMSNet_abdf0c74_Tx      117u
#define PduRConf_PduRDestPdu_PduRDestPdu                              3u
#define PduRConf_PduRDestPdu_PduRDestPdu_1                            4u
#define PduRConf_PduRDestPdu_RQST_TACHO_CIOM_oBackbone1J1939_751fcc8d_Tx 118u
#define PduRConf_PduRDestPdu_SCIM_BB1toCAN6_oCAN6_918f88f7_Tx         119u
#define PduRConf_PduRDestPdu_SCIM_BB2toCAN6_oCAN6_221ba534_Tx         120u
#define PduRConf_PduRDestPdu_SCIM_LINtoCAN6_oCAN6_f12ab4e5_Tx         121u
#define PduRConf_PduRDestPdu_SERV_X_CIOMFMS_oFMSNet_2bbb6097_Tx       122u
#define PduRConf_PduRDestPdu_TCO1_X_CIOMFMS_oFMSNet_cdc54e4b_Tx       123u
#define PduRConf_PduRDestPdu_VDHR_X_CIOMFMS_oFMSNet_9598589a_Tx       124u
#define PduRConf_PduRDestPdu_VP236_X_CIOMFMS_oFMSNet_634950b3_Tx      125u
#define PduRConf_PduRDestPdu_VW_X_CIOMFMS_oFMSNet_1c90cc30_Tx         126u
/**\} */

/**
 * \defgroup PduRHandleIdsTpRxDest Handle IDs of handle space TpRxDest.
 * \brief Transport protocol Rx destination PDUs
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define PduRConf_PduRDestPdu_Alarm_Sec_03S_oSecuritySubnet_4eba2ad4_Rx_304197c1_Rx 0u
#define PduRConf_PduRDestPdu_Alarm_Sec_06S_oSecuritySubnet_06f6b741_Rx_eb982057_Rx 1u
#define PduRConf_PduRDestPdu_Alarm_Sec_07S_oSecuritySubnet_a7cf33af_Rx_26a1cbb9_Rx 2u
#define PduRConf_PduRDestPdu_BBM_BB2_03S_CIOM_oBackbone2_a3c4774b_Rx_bd0d068c_Rx 3u
#define PduRConf_PduRDestPdu_CCM_Cab_03P_oCabSubnet_ee1873b6_Rx_8a32f8f6_Rx 4u
#define PduRConf_PduRDestPdu_CIOM_85d628ec_Rx_f1553bf5_Rx             152u
#define PduRConf_PduRDestPdu_CIOM_d63e5ea1_Rx_446f53c1_Rx             21u
#define PduRConf_PduRDestPdu_DDM_Sec_03S_oSecuritySubnet_a887cd16_Rx_fdae10f0_Rx 5u
#define PduRConf_PduRDestPdu_DDM_Sec_04S_oSecuritySubnet_79c95f1e_Rx_06bdd5c2_Rx 6u
#define PduRConf_PduRDestPdu_DDM_Sec_05S_oSecuritySubnet_d8f0dbf0_Rx_0948f4eb_Rx 7u
#define PduRConf_PduRDestPdu_DI_X_TACHO_oBackbone1J1939_6121ec8b_Rx_408ae78e_Rx 8u
#define PduRConf_PduRDestPdu_DiagReqMsgIntHMIIOM_40_F3_BB2_oBackbone2_ce0d5f06_Rx_8771577a_Rx 9u
#define PduRConf_PduRDestPdu_DiagReqMsgIntTGW2_40_F4_BB2_oBackbone2_0048db9b_Rx_59f7faf1_Rx 10u
#define PduRConf_PduRDestPdu_EMS_BB2_09S_oBackbone2_b436f1c0_Rx_7ab1f990_Rx 11u
#define PduRConf_PduRDestPdu_EMS_BB2_11S_oBackbone2_61d05567_Rx_ab3d4554_Rx 12u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_04S_oBackbone2_99f95f5e_Rx_5cdf6731_Rx 13u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_06S_oBackbone2_a13c524b_Rx_d6988d26_Rx 14u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_19P_CIOM_oBackbone2_dafe7cda_Rx_75cfe088_Rx 15u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_20S_oBackbone2_85966492_Rx_fed5ac9b_Rx 16u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_21S_oBackbone2_744c6138_Rx_be2dd0ee_Rx 17u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_27S_oBackbone2_3d037607_Rx_7040bac2_Rx 18u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_35S_oBackbone2_3334ebe1_Rx_99763ffd_Rx 19u
#define PduRConf_PduRDestPdu_HMIIOM_BB2_36S_oBackbone2_fa2be35e_Rx_270fccd3_Rx 20u
#define PduRConf_PduRDestPdu_PDM_Sec_03S_oSecuritySubnet_236a0e23_Rx_c4655c6f_Rx 22u
#define PduRConf_PduRDestPdu_PDM_Sec_04S_oSecuritySubnet_f2249c2b_Rx_26b85268_Rx 23u
#define PduRConf_PduRDestPdu_PhysDiagReqMsg_40_F2_BB2_oBackbone2_01e49dc9_Rx_d62010ae_Rx 24u
#define PduRConf_PduRDestPdu_PropTCO2_X_TACHO_oBackbone1J1939_4169b979_Rx_9d957b7c_Rx 25u
#define PduRConf_PduRDestPdu_SlaveResp_CCFW_oLIN03_1d1ae08e_Rx_5b587d0d_Rx 26u
#define PduRConf_PduRDestPdu_SlaveResp_DLFW_oLIN03_2782f4b7_Rx_f46eac1f_Rx 27u
#define PduRConf_PduRDestPdu_SlaveResp_ELCP1_oLIN03_ecc8397c_Rx_903f7ca8_Rx 28u
#define PduRConf_PduRDestPdu_SlaveResp_ELCP2_oLIN03_9a2d0041_Rx_1e24ebac_Rx 29u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_4A_oLIN00_cacf2500_Rx_78104876_Rx 30u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_4B_oLIN00_bc2a1c3d_Rx_95de453f_Rx 31u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_4C_oLIN00_2759f6e9_Rx_ee5fa3bc_Rx 32u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_4D_oLIN00_51e06e47_Rx_0a403810_Rx 33u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_4E_oLIN00_ca938493_Rx_bcced411_Rx 34u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_4F_oLIN00_bc76bdae_Rx_3b1029aa_Rx 35u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_5A_oLIN00_4bea4027_Rx_f0ad078f_Rx 36u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_5B_oLIN00_3d0f791a_Rx_f600435a_Rx 37u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_5C_oLIN00_a67c93ce_Rx_1ec07ae6_Rx 38u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_5D_oLIN00_d0c50b60_Rx_6150cbe1_Rx 39u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_5E_oLIN00_4bb6e1b4_Rx_e19a2a43_Rx 40u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_5F_oLIN00_3d53d889_Rx_3574755d_Rx 41u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_40_oLIN00_57e15c30_Rx_829de581_Rx 42u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_41_oLIN00_cc92b6e4_Rx_5fe7b952_Rx 43u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_42_oLIN00_ba778fd9_Rx_f13661f0_Rx 44u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_43_oLIN00_2104650d_Rx_91c4d480_Rx 45u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_44_oLIN00_57bdfda3_Rx_ea896e64_Rx 46u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_45_oLIN00_ccce1777_Rx_209270f6_Rx 47u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_46_oLIN00_ba2b2e4a_Rx_fcc4f722_Rx 48u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_47_oLIN00_2158c49e_Rx_cd0c7f78_Rx 49u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_48_oLIN00_57581f16_Rx_8972d072_Rx 50u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_49_oLIN00_cc2bf5c2_Rx_cdbffe3a_Rx 51u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_50_oLIN00_d6c43917_Rx_599d0d59_Rx 52u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_51_oLIN00_4db7d3c3_Rx_ea09514d_Rx 53u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_52_oLIN00_3b52eafe_Rx_fee126de_Rx 54u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_53_oLIN00_a021002a_Rx_1895a63c_Rx 55u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_54_oLIN00_d6989884_Rx_d72a79d3_Rx 56u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_55_oLIN00_4deb7250_Rx_a1c49f75_Rx 57u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_56_oLIN00_3b0e4b6d_Rx_26e14a14_Rx 58u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_57_oLIN00_a07da1b9_Rx_797d2ca8_Rx 59u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_58_oLIN00_d67d7a31_Rx_f5e79a62_Rx 60u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_59_oLIN00_4d0e90e5_Rx_52c84a11_Rx 61u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_60_oLIN00_8eda903f_Rx_f1ad5858_Rx 62u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_L1_oLIN00_b9fef1a8_Rx_99747a0b_Rx 63u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_L2_oLIN01_16746992_Rx_afe1a735_Rx 64u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_L3_oLIN02_3dc6660e_Rx_55c6efb1_Rx 65u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_L4_oLIN03_92105fa7_Rx_fc6b6d63_Rx 66u
#define PduRConf_PduRDestPdu_SlaveResp_FSP1_L5_oLIN04_6afed8a5_Rx_377e0be2_Rx 67u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_4A_oLIN01_e1f769ff_Rx_8b61dcaf_Rx 68u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_4B_oLIN01_971250c2_Rx_9fc9d0ca_Rx 69u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_4C_oLIN01_0c61ba16_Rx_37e39e86_Rx 70u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_4D_oLIN01_7ad822b8_Rx_8e21deb6_Rx 71u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_4E_oLIN01_e1abc86c_Rx_09578dd1_Rx 72u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_4F_oLIN01_974ef151_Rx_b8d73109_Rx 73u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_5A_oLIN01_60d20cd8_Rx_e3e49c92_Rx 74u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_5B_oLIN01_163735e5_Rx_128e965d_Rx 75u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_5C_oLIN01_8d44df31_Rx_0ff2af9d_Rx 76u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_5D_oLIN01_fbfd479f_Rx_388f0802_Rx 77u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_5E_oLIN01_608ead4b_Rx_6288cdc0_Rx 78u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_5F_oLIN01_166b9476_Rx_34787a47_Rx 79u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_40_oLIN01_7cd910cf_Rx_adc354c3_Rx 80u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_41_oLIN01_e7aafa1b_Rx_78cc87f9_Rx 81u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_42_oLIN01_914fc326_Rx_8a6e9cc9_Rx 82u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_43_oLIN01_0a3c29f2_Rx_671943d6_Rx 83u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_44_oLIN01_7c85b15c_Rx_790bf261_Rx 84u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_45_oLIN01_e7f65b88_Rx_30aaf785_Rx 85u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_46_oLIN01_911362b5_Rx_03c5baed_Rx 86u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_47_oLIN01_0a608861_Rx_fd40f52b_Rx 87u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_48_oLIN01_7c6053e9_Rx_1d064c43_Rx 88u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_49_oLIN01_e713b93d_Rx_cb7f46b3_Rx 89u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_50_oLIN01_fdfc75e8_Rx_146a20f0_Rx 90u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_51_oLIN01_668f9f3c_Rx_3758b7ab_Rx 91u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_52_oLIN01_106aa601_Rx_44c35ff0_Rx 92u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_53_oLIN01_8b194cd5_Rx_6af528f4_Rx 93u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_54_oLIN01_fda0d47b_Rx_da4d01a7_Rx 94u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_55_oLIN01_66d33eaf_Rx_a7fc524b_Rx 95u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_56_oLIN01_10360792_Rx_4e01f700_Rx 96u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_57_oLIN01_8b45ed46_Rx_8b212133_Rx 97u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_58_oLIN01_fd4536ce_Rx_82a01191_Rx 98u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_59_oLIN01_6636dc1a_Rx_bff68e2d_Rx 99u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_60_oLIN01_a5e2dcc0_Rx_328650f4_Rx 100u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_L1_oLIN00_4ba91c50_Rx_f7959d8f_Rx 101u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_L2_oLIN01_e423846a_Rx_e28df3db_Rx 102u
#define PduRConf_PduRDestPdu_SlaveResp_FSP2_L3_oLIN02_cf918bf6_Rx_1b1ab2ff_Rx 103u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_4A_oLIN02_b62b2a20_Rx_63202e6b_Rx 104u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_4B_oLIN02_c0ce131d_Rx_1c94c14e_Rx 105u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_4C_oLIN02_5bbdf9c9_Rx_b395104c_Rx 106u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_4D_oLIN02_2d046167_Rx_a0dc157f_Rx 107u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_4E_oLIN02_b6778bb3_Rx_72de301a_Rx 108u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_4F_oLIN02_c092b28e_Rx_862b3ebb_Rx 109u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_5A_oLIN02_370e4f07_Rx_17f98beb_Rx 110u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_5B_oLIN02_41eb763a_Rx_3ad59601_Rx 111u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_5C_oLIN02_da989cee_Rx_fad5e462_Rx 112u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_5D_oLIN02_ac210440_Rx_8e9adf0f_Rx 113u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_5E_oLIN02_3752ee94_Rx_50f38210_Rx 114u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_5F_oLIN02_41b7d7a9_Rx_9894765a_Rx 115u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_40_oLIN02_2b055310_Rx_b3908af8_Rx 116u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_41_oLIN02_b076b9c4_Rx_731eaf66_Rx 117u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_42_oLIN02_c69380f9_Rx_0a252841_Rx 118u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_43_oLIN02_5de06a2d_Rx_3b58f9ef_Rx 119u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_44_oLIN02_2b59f283_Rx_2681dcb1_Rx 120u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_45_oLIN02_b02a1857_Rx_54004ea5_Rx 121u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_46_oLIN02_c6cf216a_Rx_e7f4913f_Rx 122u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_47_oLIN02_5dbccbbe_Rx_bdab974a_Rx 123u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_48_oLIN02_2bbc1036_Rx_ccd695fa_Rx 124u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_49_oLIN02_b0cffae2_Rx_2ae49e9d_Rx 125u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_50_oLIN02_aa203637_Rx_943de0f2_Rx 126u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_51_oLIN02_3153dce3_Rx_0072b072_Rx 127u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_52_oLIN02_47b6e5de_Rx_7a8b2337_Rx 128u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_53_oLIN02_dcc50f0a_Rx_f75fb2d2_Rx 129u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_54_oLIN02_aa7c97a4_Rx_74a4056f_Rx 130u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_55_oLIN02_310f7d70_Rx_46901311_Rx 131u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_56_oLIN02_47ea444d_Rx_c1b5c670_Rx 132u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_57_oLIN02_dc99ae99_Rx_09fe52bd_Rx 133u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_58_oLIN02_aa997511_Rx_b98ce9b7_Rx 134u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_59_oLIN02_31ea9fc5_Rx_401c60b5_Rx 135u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_60_oLIN02_f23e9f1f_Rx_3f09cf26_Rx 136u
#define PduRConf_PduRDestPdu_SlaveResp_FSP3_L2_oLIN01_033e22fd_Rx_6b1d1eb8_Rx 137u
#define PduRConf_PduRDestPdu_SlaveResp_FSP4_40_oLIN03_2aa98931_Rx_c33e4609_Rx 138u
#define PduRConf_PduRDestPdu_SlaveResp_FSP4_L2_oLIN01_dbfd59db_Rx_87b10390_Rx 139u
#define PduRConf_PduRDestPdu_SlaveResp_FSP5_40_oLIN04_ae294270_Rx_660d977b_Rx 140u
#define PduRConf_PduRDestPdu_SlaveResp_ILCP1_oLIN00_ffca61fb_Rx_a0f8dbfb_Rx 141u
#define PduRConf_PduRDestPdu_SlaveResp_ILCP2_oLIN03_39eebd8e_Rx_95103cdf_Rx 142u
#define PduRConf_PduRDestPdu_SlaveResp_LECM2_oLIN00_71931b12_Rx_8389a02c_Rx 143u
#define PduRConf_PduRDestPdu_SlaveResp_LECMBasic_oLIN00_30a28bed_Rx_3b8e8c69_Rx 144u
#define PduRConf_PduRDestPdu_SlaveResp_LinSlave_L8_oLIN05_bdbf23c2_Rx_1c42292e_Rx 145u
#define PduRConf_PduRDestPdu_SlaveResp_LinSlave_L8_oLIN06_0d7ec68a_Rx_74040ff9_Rx 146u
#define PduRConf_PduRDestPdu_SlaveResp_LinSlave_L8_oLIN07_d411678d_Rx_f7f89fa3_Rx 147u
#define PduRConf_PduRDestPdu_SlaveResp_RCECS_oLIN04_28099311_Rx_0e5523b8_Rx 148u
#define PduRConf_PduRDestPdu_SlaveResp_TCP_oLIN02_cb8ee454_Rx_68bca152_Rx 149u
#define PduRConf_PduRDestPdu_TECU_BB2_05S_oBackbone2_0976cf31_Rx_0aeda82a_Rx 150u
#define PduRConf_PduRDestPdu_TECU_BB2_06S_oBackbone2_c069c78e_Rx_187ffacf_Rx 151u
#define PduRConf_PduRDestPdu_VMCU_BB2_31S_oBackbone2_9791f79f_Rx_962aaefb_Rx 153u
#define PduRConf_PduRDestPdu_VMCU_BB2_32S_oBackbone2_5e8eff20_Rx_8c4f8f58_Rx 154u
#define PduRConf_PduRDestPdu_VMCU_BB2_34S_oBackbone2_17c1e81f_Rx_656dbd2a_Rx 155u
#define PduRConf_PduRDestPdu_VMCU_BB2_57P_oBackbone2_7bd9bbf9_Rx_46a397e3_Rx 156u
/**\} */

/**
 * \defgroup PduRHandleIdsTpRxSrc Handle IDs of handle space TpRxSrc.
 * \brief Transport protocol Rx source PDUs
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0a252841                       163u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0a403810                       78u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0aeda82a                       195u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0e5523b8                       193u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0ff2af9d                       121u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1ac6f203                       20u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1b1ab2ff                       148u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1b947dce                       36u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1c94c14e                       150u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1c42292e                       190u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1d064c43                       133u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1e24ebac                       74u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1ec07ae6                       83u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2ae49e9d                       170u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2e6f76ae                       33u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3ad59601                       156u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3b8e8c69                       189u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3b58f9ef                       164u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3b1029aa                       80u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3c553d77                       49u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3f09cf26                       181u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_4e01f700                       141u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5b587d0d                       71u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5cdf6731                       59u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5cfa406e                       29u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5fe7b952                       88u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_6af528f4                       138u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_6b1d1eb8                       182u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_6ea67099                       24u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_7a8b2337                       173u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_7ab1f990                       57u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8a6e9cc9                       127u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8a32f8f6                       4u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8b61dcaf                       113u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8b212133                       142u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8c4f8f58                       198u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8c488735                       44u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8e9adf0f                       158u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8e21deb6                       116u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_9d957b7c                       70u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_9e5d6760                       50u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_9fc9d0ca                       114u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_03c5baed                       131u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_06bdd5c2                       52u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_09fe52bd                       178u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_17f98beb                       155u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_25a59123                       48u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_26a1cbb9                       2u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_26b85268                       68u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_26e14a14                       103u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_30aaf785                       130u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_30b4cd32                       35u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_37e39e86                       115u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_39fe25b5                       46u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_44c35ff0                       137u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_46a397e3                       200u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_50f38210                       159u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_52c84a11                       106u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_55c6efb1                       110u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_59f7faf1                       56u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_62ae1653                       37u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_68bca152                       194u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_71b786d1                       38u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_72de301a                       153u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_74a4056f                       175u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_75cfe088                       61u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_78cc87f9                       126u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_82a01191                       143u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_86e04d33                       5u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_87b10390                       184u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_91c4d480                       90u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_95de453f                       76u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_99e30330                       40u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_064dc9ab                       28u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_128e965d                       120u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_146a20f0                       135u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_187ffacf                       196u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_246f1e3b                       16u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_270fccd3                       66u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_304f257a                       30u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_377e0be2                       112u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_388f0802                       122u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_401c60b5                       180u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_408ae78e                       54u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_446f53c1                       47u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_599d0d59                       97u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_656dbd2a                       199u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_660d977b                       185u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_731eaf66                       162u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_790bf261                       129u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_797d2ca8                       104u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_829de581                       87u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_862b3ebb                       154u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_903f7ca8                       73u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_943de0f2                       171u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_962aaefb                       197u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0072b072                       172u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0948f4eb                       53u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1895a63c                       100u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2681dcb1                       165u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3758b7ab                       136u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_6150cbe1                       84u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_6288cdc0                       123u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_7040bac2                       64u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8259d96a                       45u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8389a02c                       188u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8972d072                       95u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_09578dd1                       117u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_34787a47                       124u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_54004ea5                       166u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_63202e6b                       149u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_74040ff9                       191u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_89263a8e                       27u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_95103cdf                       187u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_99747a0b                       108u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_99763ffd                       65u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_209270f6                       92u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_266542d7                       17u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_304197c1                       0u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_328650f4                       145u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_671943d6                       128u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3574755d                       86u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8771577a                       55u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_9894765a                       160u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_01528206                       13u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_46901311                       176u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_63947842                       10u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_78104876                       75u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a0dc157f                       152u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a0f8dbfb                       186u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a1c49f75                       102u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a7fc524b                       140u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a40032de                       25u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ab3d4554                       58u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ac4babc0                       21u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_adc354c3                       125u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_afe1a735                       109u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b4b812c0                       42u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b8d73109                       118u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b9d033d4                       26u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b18fc6c3                       23u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b98ce9b7                       179u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b3908af8                       161u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b395104c                       151u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_bbc023c9                       9u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_bcced411                       79u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_bd0d068c                       3u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_bd1ce89a                       43u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_bdab974a                       168u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_be2dd0ee                       63u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_bfda77cf                       34u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_bff68e2d                       144u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c1b5c670                       177u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c6acd63d                       11u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c28e0d5d                       14u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c33e4609                       183u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c050dec8                       7u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c0680d6c                       31u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c4655c6f                       67u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_cb7f46b3                       134u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ccd695fa                       169u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ccea7767                       6u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_cd0c7f78                       94u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_cd1487f2                       18u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_cdbffe3a                       96u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d9d30309                       32u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d72a79d3                       101u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d91d7573                       8u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d6988d26                       60u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d62010ae                       69u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_da0a8dca                       39u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_da4d01a7                       139u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_df9a30f3                       12u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_dfacef7f                       41u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e3e49c92                       119u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e7f4913f                       167u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e19a2a43                       85u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e28df3db                       147u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ea4e2140                       15u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ea896e64                       91u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ea09514d                       98u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_eb982057                       1u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ee5fa3bc                       77u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f0ad078f                       81u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f1ad5858                       107u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f5e79a62                       105u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f7f89fa3                       192u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f46eac1f                       72u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f75fb2d2                       174u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f1553bf5                       22u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f7959d8f                       146u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f13661f0                       89u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f600435a                       82u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_fad5e462                       157u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_fc6b6d63                       111u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_fcc4f722                       93u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_fd40f52b                       132u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_fdae10f0                       51u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_fed5ac9b                       62u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_fee126de                       99u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ff6a6468                       19u
/**\} */

/**
 * \defgroup PduRHandleIdsTpTxDest Handle IDs of handle space TpTxDest.
 * \brief Transport protocol Tx PDUs
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define PduRConf_PduRDestPdu_CIOM_0fefd926_Tx                         187u
#define PduRConf_PduRDestPdu_CIOM_1a4fac9a_Tx                         34u
#define PduRConf_PduRDestPdu_CIOM_1f5874a4_Tx                         197u
#define PduRConf_PduRDestPdu_CIOM_2b6cb3ad_Tx                         28u
#define PduRConf_PduRDestPdu_CIOM_2dc3f245_Tx                         184u
#define PduRConf_PduRDestPdu_CIOM_3fb186a1_Tx                         48u
#define PduRConf_PduRDestPdu_CIOM_4e01b3dd_Tx                         47u
#define PduRConf_PduRDestPdu_CIOM_5cd3a27b_Tx                         44u
#define PduRConf_PduRDestPdu_CIOM_5d61a178_Tx                         32u
#define PduRConf_PduRDestPdu_CIOM_6e041ac5_Tx                         43u
#define PduRConf_PduRDestPdu_CIOM_7bfcaf70_Tx                         50u
#define PduRConf_PduRDestPdu_CIOM_9aa9598b_Tx                         53u
#define PduRConf_PduRDestPdu_CIOM_9f94b5ae_Tx                         40u
#define PduRConf_PduRDestPdu_CIOM_14cb3836_Tx                         23u
#define PduRConf_PduRDestPdu_CIOM_16ccbda9_Tx                         24u
#define PduRConf_PduRDestPdu_CIOM_42e6f8eb_Tx                         183u
#define PduRConf_PduRDestPdu_CIOM_50cc5439_Tx                         189u
#define PduRConf_PduRDestPdu_CIOM_60d04da4_Tx                         33u
#define PduRConf_PduRDestPdu_CIOM_98a0f91f_Tx                         35u
#define PduRConf_PduRDestPdu_CIOM_017be91a_Tx                         200u
#define PduRConf_PduRDestPdu_CIOM_305ca23a_Tx                         201u
#define PduRConf_PduRDestPdu_CIOM_325e35fa_Tx                         188u
#define PduRConf_PduRDestPdu_CIOM_500aa266_Tx                         41u
#define PduRConf_PduRDestPdu_CIOM_948b4abd_Tx                         198u
#define PduRConf_PduRDestPdu_CIOM_2156cc07_Tx                         199u
#define PduRConf_PduRDestPdu_CIOM_4767c578_Tx                         46u
#define PduRConf_PduRDestPdu_CIOM_52939f57_Tx                         52u
#define PduRConf_PduRDestPdu_CIOM_08650048_Tx                         29u
#define PduRConf_PduRDestPdu_CIOM_BB2_12S_oBackbone2_fe325e86_Tx      0u
#define PduRConf_PduRDestPdu_CIOM_BB2_13S_oBackbone2_0fe85b2c_Tx      1u
#define PduRConf_PduRDestPdu_CIOM_BB2_22S_oBackbone2_a525ef93_Tx      2u
#define PduRConf_PduRDestPdu_CIOM_BB2_30S_oBackbone2_ab127275_Tx      3u
#define PduRConf_PduRDestPdu_CIOM_Cab_04S_oCabSubnet_779a15ce_Tx      4u
#define PduRConf_PduRDestPdu_CIOM_Cab_05S_oCabSubnet_86401064_Tx      5u
#define PduRConf_PduRDestPdu_CIOM_Cab_11S_oCabSubnet_c1389abd_Tx      6u
#define PduRConf_PduRDestPdu_CIOM_Cab_13S_oCabSubnet_f9fd97a8_Tx      7u
#define PduRConf_PduRDestPdu_CIOM_Cab_20S_oCabSubnet_6bf52e02_Tx      8u
#define PduRConf_PduRDestPdu_CIOM_Cab_25S_oCabSubnet_eba53182_Tx      9u
#define PduRConf_PduRDestPdu_CIOM_Cab_29S_oCabSubnet_793b1ffc_Tx      10u
#define PduRConf_PduRDestPdu_CIOM_Cab_30S_oCabSubnet_5d07bef1_Tx      11u
#define PduRConf_PduRDestPdu_CIOM_Cab_34P_oCabSubnet_3fa59da8_Tx      12u
#define PduRConf_PduRDestPdu_CIOM_Sec_05S_oSecuritySubnet_7a00e33f_Tx 13u
#define PduRConf_PduRDestPdu_CIOM_Sec_06S_oSecuritySubnet_423b684c_Tx 14u
#define PduRConf_PduRDestPdu_CIOM_Sec_07S_oSecuritySubnet_e302eca2_Tx 15u
#define PduRConf_PduRDestPdu_CIOM_Sec_08S_oSecuritySubnet_3bd74a1d_Tx 16u
#define PduRConf_PduRDestPdu_CIOM_Sec_09S_oSecuritySubnet_9aeecef3_Tx 17u
#define PduRConf_PduRDestPdu_CIOM_Sec_10S_oSecuritySubnet_755f8851_Tx 18u
#define PduRConf_PduRDestPdu_CIOM_Sec_11S_oSecuritySubnet_d4660cbf_Tx 19u
#define PduRConf_PduRDestPdu_CIOM_Sec_12S_oSecuritySubnet_ec5d87cc_Tx 20u
#define PduRConf_PduRDestPdu_CIOM_a610cc5c_Tx                         185u
#define PduRConf_PduRDestPdu_CIOM_a41749c3_Tx                         186u
#define PduRConf_PduRDestPdu_CIOM_a81020b5_Tx                         194u
#define PduRConf_PduRDestPdu_CIOM_a1765610_Tx                         195u
#define PduRConf_PduRDestPdu_CIOM_b3a447b6_Tx                         192u
#define PduRConf_PduRDestPdu_CIOM_ca2e8141_Tx                         190u
#define PduRConf_PduRDestPdu_CIOM_ce2129ca_Tx                         51u
#define PduRConf_PduRDestPdu_CIOM_d0c6636c_Tx                         196u
#define PduRConf_PduRDestPdu_CIOM_d31c0716_Tx                         26u
#define PduRConf_PduRDestPdu_CIOM_dbd99c7f_Tx                         42u
#define PduRConf_PduRDestPdu_CIOM_e5f820d2_Tx                         191u
#define PduRConf_PduRDestPdu_CIOM_e217a053_Tx                         27u
#define PduRConf_PduRDestPdu_CIOM_e742f677_Tx                         38u
#define PduRConf_PduRDestPdu_CIOM_ec9334ff_Tx                         30u
#define PduRConf_PduRDestPdu_CIOM_ee94b160_Tx                         31u
#define PduRConf_PduRDestPdu_CIOM_ee2480d2_Tx                         39u
#define PduRConf_PduRDestPdu_CIOM_eeadebca_Tx                         25u
#define PduRConf_PduRDestPdu_CIOM_f02f9169_Tx                         49u
#define PduRConf_PduRDestPdu_CIOM_f03d0c81_Tx                         22u
#define PduRConf_PduRDestPdu_CIOM_fcf69174_Tx                         36u
#define PduRConf_PduRDestPdu_DI_X_CIOMFMS_oFMSNet_45299e3a_Tx         21u
#define PduRConf_PduRDestPdu_DiagRespMsgIntHMIIOM_F3_40_BB2_oBackbone2_f7679e64_Tx 37u
#define PduRConf_PduRDestPdu_DiagRespMsgIntTGW2_F4_40_BB2_oBackbone2_3b72250e_Tx 45u
#define PduRConf_PduRDestPdu_MasterReq_CCFW_oLIN03_fcabea98_Tx        54u
#define PduRConf_PduRDestPdu_MasterReq_DLFW_oLIN03_c633fea1_Tx        55u
#define PduRConf_PduRDestPdu_MasterReq_ELCP1_oLIN03_18fd3d27_Tx       56u
#define PduRConf_PduRDestPdu_MasterReq_ELCP2_oLIN03_6e18041a_Tx       57u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_4A_oLIN00_94ed91b3_Tx     58u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_4B_oLIN00_e208a88e_Tx     59u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_4C_oLIN00_797b425a_Tx     60u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_4D_oLIN00_0fc2daf4_Tx     61u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_4E_oLIN00_94b13020_Tx     62u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_4F_oLIN00_e254091d_Tx     63u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_5A_oLIN00_15c8f494_Tx     64u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_5B_oLIN00_632dcda9_Tx     65u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_5C_oLIN00_f85e277d_Tx     66u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_5D_oLIN00_8ee7bfd3_Tx     67u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_5E_oLIN00_15945507_Tx     68u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_5F_oLIN00_63716c3a_Tx     69u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_40_oLIN00_09c3e883_Tx     70u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_41_oLIN00_92b00257_Tx     71u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_42_oLIN00_e4553b6a_Tx     72u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_43_oLIN00_7f26d1be_Tx     73u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_44_oLIN00_099f4910_Tx     74u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_45_oLIN00_92eca3c4_Tx     75u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_46_oLIN00_e4099af9_Tx     76u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_47_oLIN00_7f7a702d_Tx     77u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_48_oLIN00_097aaba5_Tx     78u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_49_oLIN00_92094171_Tx     79u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_50_oLIN00_88e68da4_Tx     80u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_51_oLIN00_13956770_Tx     81u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_52_oLIN00_65705e4d_Tx     82u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_53_oLIN00_fe03b499_Tx     83u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_54_oLIN00_88ba2c37_Tx     84u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_55_oLIN00_13c9c6e3_Tx     85u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_56_oLIN00_652cffde_Tx     86u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_57_oLIN00_fe5f150a_Tx     87u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_58_oLIN00_885fce82_Tx     88u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_59_oLIN00_132c2456_Tx     89u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_60_oLIN00_d0f8248c_Tx     90u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_L1_oLIN00_e7dc451b_Tx     91u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_L2_oLIN01_4856dd21_Tx     92u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_L3_oLIN02_63e4d2bd_Tx     93u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_L4_oLIN03_cc32eb14_Tx     94u
#define PduRConf_PduRDestPdu_MasterReq_FSP1_L5_oLIN04_34dc6c16_Tx     95u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_4A_oLIN01_bfd5dd4c_Tx     96u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_4B_oLIN01_c930e471_Tx     97u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_4C_oLIN01_52430ea5_Tx     98u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_4D_oLIN01_24fa960b_Tx     99u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_4E_oLIN01_bf897cdf_Tx     100u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_4F_oLIN01_c96c45e2_Tx     101u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_5A_oLIN01_3ef0b86b_Tx     102u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_5B_oLIN01_48158156_Tx     103u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_5C_oLIN01_d3666b82_Tx     104u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_5D_oLIN01_a5dff32c_Tx     105u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_5E_oLIN01_3eac19f8_Tx     106u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_5F_oLIN01_484920c5_Tx     107u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_40_oLIN01_22fba47c_Tx     108u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_41_oLIN01_b9884ea8_Tx     109u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_42_oLIN01_cf6d7795_Tx     110u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_43_oLIN01_541e9d41_Tx     111u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_44_oLIN01_22a705ef_Tx     112u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_45_oLIN01_b9d4ef3b_Tx     113u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_46_oLIN01_cf31d606_Tx     114u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_47_oLIN01_54423cd2_Tx     115u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_48_oLIN01_2242e75a_Tx     116u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_49_oLIN01_b9310d8e_Tx     117u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_50_oLIN01_a3dec15b_Tx     118u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_51_oLIN01_38ad2b8f_Tx     119u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_52_oLIN01_4e4812b2_Tx     120u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_53_oLIN01_d53bf866_Tx     121u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_54_oLIN01_a38260c8_Tx     122u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_55_oLIN01_38f18a1c_Tx     123u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_56_oLIN01_4e14b321_Tx     124u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_57_oLIN01_d56759f5_Tx     125u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_58_oLIN01_a367827d_Tx     126u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_59_oLIN01_381468a9_Tx     127u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_60_oLIN01_fbc06873_Tx     128u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_L1_oLIN00_158ba8e3_Tx     129u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_L2_oLIN01_ba0130d9_Tx     130u
#define PduRConf_PduRDestPdu_MasterReq_FSP2_L3_oLIN02_91b33f45_Tx     131u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_4A_oLIN02_e8099e93_Tx     132u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_4B_oLIN02_9eeca7ae_Tx     133u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_4C_oLIN02_059f4d7a_Tx     134u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_4D_oLIN02_7326d5d4_Tx     135u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_4E_oLIN02_e8553f00_Tx     136u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_4F_oLIN02_9eb0063d_Tx     137u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_5A_oLIN02_692cfbb4_Tx     138u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_5B_oLIN02_1fc9c289_Tx     139u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_5C_oLIN02_84ba285d_Tx     140u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_5D_oLIN02_f203b0f3_Tx     141u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_5E_oLIN02_69705a27_Tx     142u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_5F_oLIN02_1f95631a_Tx     143u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_40_oLIN02_7527e7a3_Tx     144u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_41_oLIN02_ee540d77_Tx     145u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_42_oLIN02_98b1344a_Tx     146u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_43_oLIN02_03c2de9e_Tx     147u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_44_oLIN02_757b4630_Tx     148u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_45_oLIN02_ee08ace4_Tx     149u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_46_oLIN02_98ed95d9_Tx     150u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_47_oLIN02_039e7f0d_Tx     151u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_48_oLIN02_759ea485_Tx     152u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_49_oLIN02_eeed4e51_Tx     153u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_50_oLIN02_f4028284_Tx     154u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_51_oLIN02_6f716850_Tx     155u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_52_oLIN02_1994516d_Tx     156u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_53_oLIN02_82e7bbb9_Tx     157u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_54_oLIN02_f45e2317_Tx     158u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_55_oLIN02_6f2dc9c3_Tx     159u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_56_oLIN02_19c8f0fe_Tx     160u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_57_oLIN02_82bb1a2a_Tx     161u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_58_oLIN02_f4bbc1a2_Tx     162u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_59_oLIN02_6fc82b76_Tx     163u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_60_oLIN02_ac1c2bac_Tx     164u
#define PduRConf_PduRDestPdu_MasterReq_FSP3_L2_oLIN01_5d1c964e_Tx     165u
#define PduRConf_PduRDestPdu_MasterReq_FSP4_40_oLIN03_748b3d82_Tx     166u
#define PduRConf_PduRDestPdu_MasterReq_FSP4_L2_oLIN01_85dfed68_Tx     167u
#define PduRConf_PduRDestPdu_MasterReq_FSP5_40_oLIN04_f00bf6c3_Tx     168u
#define PduRConf_PduRDestPdu_MasterReq_ILCP1_oLIN00_0bff65a0_Tx       169u
#define PduRConf_PduRDestPdu_MasterReq_ILCP2_oLIN03_cddbb9d5_Tx       170u
#define PduRConf_PduRDestPdu_MasterReq_LECM2_oLIN00_85a61f49_Tx       171u
#define PduRConf_PduRDestPdu_MasterReq_LECMBasic_oLIN00_33465f21_Tx   172u
#define PduRConf_PduRDestPdu_MasterReq_RCECS_oLIN04_dc3c974a_Tx       173u
#define PduRConf_PduRDestPdu_MasterReq_TCP_oLIN02_741cea1f_Tx         174u
#define PduRConf_PduRDestPdu_MasterReq_oLIN00_3234fe1b_Tx             175u
#define PduRConf_PduRDestPdu_MasterReq_oLIN01_eb5b5f1c_Tx             176u
#define PduRConf_PduRDestPdu_MasterReq_oLIN02_5b9aba54_Tx             177u
#define PduRConf_PduRDestPdu_MasterReq_oLIN03_82f51b53_Tx             178u
#define PduRConf_PduRDestPdu_MasterReq_oLIN04_e1687685_Tx             179u
#define PduRConf_PduRDestPdu_MasterReq_oLIN05_3807d782_Tx             180u
#define PduRConf_PduRDestPdu_MasterReq_oLIN06_88c632ca_Tx             181u
#define PduRConf_PduRDestPdu_MasterReq_oLIN07_51a993cd_Tx             182u
#define PduRConf_PduRDestPdu_PhysDiagRespMsg_F2_40_BB2_oBackbone2_1ef42df7_Tx 193u
#define PduRConf_PduRDestPdu_VI_X_CIOMFMS_oFMSNet_55f8530d_Tx         202u
/**\} */


/* User Config File Start */

/* User Config File End */


/**********************************************************************************************************************
 * GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

#endif  /* PDUR_CFG_H */
/**********************************************************************************************************************
 * END OF FILE: PduR_Cfg.h
 *********************************************************************************************************************/

