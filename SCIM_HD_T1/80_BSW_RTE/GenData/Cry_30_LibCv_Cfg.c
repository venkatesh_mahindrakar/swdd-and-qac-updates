/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Cry_30_LibCv
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Cry_30_LibCv_Cfg.c
 *   Generation Time: 2020-11-11 14:25:34
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

#define CRY_30_LIBCV_CFG_SOURCE

/* PRQA S 0779 EOF */ /* MD_CSL_0779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Cry_30_LibCv_Cfg.h"
#include "Cry_30_LibCv.h"



/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
#if !defined (STATIC)
# define STATIC static
#endif

#if !defined (CRY_30_LIBCV_LOCAL)
# define CRY_30_LIBCV_LOCAL static
#endif

#if !defined (CRY_30_LIBCV_LOCAL_INLINE)
# define CRY_30_LIBCV_LOCAL_INLINE LOCAL_INLINE
#endif




/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/






/**********************************************************************************************************************
 *  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/




/**********************************************************************************************************************
 *  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  Cry_30_LibCv_AesDecrypt128Config
**********************************************************************************************************************/
/** 
  \var    Cry_30_LibCv_AesDecrypt128Config
  \brief  Contains parameters of /MICROSAR/Cry_30_LibCv/Cry/CryAesDecrypt128/CryAesDecrypt128Config.
  \details
  Element                      Description
  BlockMode                    Contains values of DefinitionRef: /MICROSAR/Cry_30_LibCv/Cry/CryAesDecrypt128/CryAesDecrypt128Config/CryAesDecrypt128BlockMode.
  PaddingMode                  Contains values of DefinitionRef: /MICROSAR/Cry_30_LibCv/Cry/CryAesDecrypt128/CryAesDecrypt128Config/CryAesDecrypt128PaddingMode.
  AesDecrypt128WorkSpaceIdx    the index of the 1:1 relation pointing to Cry_30_LibCv_AesDecrypt128WorkSpace
*/ 
#define CRY_30_LIBCV_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Cry_30_LibCv_AesDecrypt128ConfigType, CRY_30_LIBCV_CONST) Cry_30_LibCv_AesDecrypt128Config[1] = {
    /* Index    BlockMode                      PaddingMode                        AesDecrypt128WorkSpaceIdx        Referable Keys */
  { /*     0 */ CRY_30_LIBCV_AESBLOCKMODE_CBC, CRY_30_LIBCV_AESPADDINGMODE_PKCS5,                        0u }   /* [/ActiveEcuC/Cry/CryAesDecrypt128/CryAesDecrypt128Config, /ActiveEcuC/Cry/CryAesDecrypt128, /ActiveEcuC/Csm/CsmSymDecrypt/CsmSymDecryptConfig:CsmSymDecryptCryRef] */
};
#define CRY_30_LIBCV_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Cry_30_LibCv_AesEncrypt128Config
**********************************************************************************************************************/
/** 
  \var    Cry_30_LibCv_AesEncrypt128Config
  \brief  Contains parameters of /MICROSAR/Cry_30_LibCv/Cry/CryAesEncrypt128/CryAesEncrypt128Config.
  \details
  Element                      Description
  BlockMode                    Contains values of DefinitionRef: /MICROSAR/Cry_30_LibCv/Cry/CryAesEncrypt128/CryAesEncrypt128Config/CryAesEncrypt128BlockMode.
  PaddingMode                  Contains values of DefinitionRef: /MICROSAR/Cry_30_LibCv/Cry/CryAesEncrypt128/CryAesEncrypt128Config/CryAesEncrypt128PaddingMode.
  AesEncrypt128WorkSpaceIdx    the index of the 1:1 relation pointing to Cry_30_LibCv_AesEncrypt128WorkSpace
*/ 
#define CRY_30_LIBCV_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Cry_30_LibCv_AesEncrypt128ConfigType, CRY_30_LIBCV_CONST) Cry_30_LibCv_AesEncrypt128Config[1] = {
    /* Index    BlockMode                      PaddingMode                        AesEncrypt128WorkSpaceIdx        Referable Keys */
  { /*     0 */ CRY_30_LIBCV_AESBLOCKMODE_CBC, CRY_30_LIBCV_AESPADDINGMODE_PKCS5,                        0u }   /* [/ActiveEcuC/Cry/CryAesEncrypt128/CryAesEncrypt128Config, /ActiveEcuC/Cry/CryAesEncrypt128, /ActiveEcuC/Csm/CsmSymEncrypt/CsmSymEncryptConfig:CsmSymEncryptCryRef] */
};
#define CRY_30_LIBCV_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Cry_30_LibCv_Fips186Config
**********************************************************************************************************************/
/** 
  \var    Cry_30_LibCv_Fips186Config
  \brief  Contains parameters of /MICROSAR/Cry_30_LibCv/Cry/CryFips186/CryFips186Config.
  \details
  Element                Description
  SaveState              Contains values of DefinitionRef: /MICROSAR/Cry_30_LibCv/Cry/CryFips186/CryFips186Config/CrySaveState.
  Fips186WorkSpaceIdx    the index of the 1:1 relation pointing to Cry_30_LibCv_Fips186WorkSpace
*/ 
#define CRY_30_LIBCV_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Cry_30_LibCv_Fips186ConfigType, CRY_30_LIBCV_CONST) Cry_30_LibCv_Fips186Config[1] = {
    /* Index    SaveState  Fips186WorkSpaceIdx        Referable Keys */
  { /*     0 */      TRUE,                  0u }   /* [/ActiveEcuC/Cry/CryFips186/CryFips186Config, /ActiveEcuC/Cry/CryFips186, /ActiveEcuC/Csm/CsmRandomGenerate/CsmRandomGenerateConfig:CsmRandomGenerateCryRef, /ActiveEcuC/Csm/CsmRandomSeed/CsmRandomSeedConfig:CsmRandomSeedCryRef] */
};
#define CRY_30_LIBCV_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Cry_30_LibCv_RsaDecryptConfig
**********************************************************************************************************************/
/** 
  \var    Cry_30_LibCv_RsaDecryptConfig
  \brief  Contains parameters of /MICROSAR/Cry_30_LibCv/Cry/CryRsaDecrypt/CryRsaDecryptConfig.
  \details
  Element                   Description
  UsePrivateKey             Contains values of DefinitionRef: /MICROSAR/Cry_30_LibCv/Cry/CryRsaDecrypt/CryRsaDecryptConfig/CryRsaDecryptUsePrivateKey.
  RsaDecryptWorkSpaceIdx    the index of the 1:1 relation pointing to Cry_30_LibCv_RsaDecryptWorkSpace
*/ 
#define CRY_30_LIBCV_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Cry_30_LibCv_RsaDecryptConfigType, CRY_30_LIBCV_CONST) Cry_30_LibCv_RsaDecryptConfig[1] = {
    /* Index    UsePrivateKey  RsaDecryptWorkSpaceIdx        Referable Keys */
  { /*     0 */         FALSE,                     0u }   /* [/ActiveEcuC/Cry/CryRsaDecrypt/CryRsaDecryptConfig, /ActiveEcuC/Cry/CryRsaDecrypt, /ActiveEcuC/Csm/CsmAsymDecrypt/CsmAsymDecryptConfig:CsmAsymDecryptCryRef] */
};
#define CRY_30_LIBCV_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Cry_30_LibCv_RsaSha1SigVerConfig
**********************************************************************************************************************/
/** 
  \var    Cry_30_LibCv_RsaSha1SigVerConfig
  \brief  Contains parameters of /MICROSAR/Cry_30_LibCv/Cry/CryRsaSha1SigVer/CryRsaSha1SigVerConfig.
  \details
  Element                      Description
  RsaSha1SigVerWorkSpaceIdx    the index of the 1:1 relation pointing to Cry_30_LibCv_RsaSha1SigVerWorkSpace
*/ 
#define CRY_30_LIBCV_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Cry_30_LibCv_RsaSha1SigVerConfigType, CRY_30_LIBCV_CONST) Cry_30_LibCv_RsaSha1SigVerConfig[1] = {
    /* Index    RsaSha1SigVerWorkSpaceIdx        Referable Keys */
  { /*     0 */                        0u }   /* [/ActiveEcuC/Cry/CryRsaSha1SigVer/CryRsaSha1SigVerConfig] */
};
#define CRY_30_LIBCV_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Cry_30_LibCv_AesDecrypt128WorkSpace
**********************************************************************************************************************/
#define CRY_30_LIBCV_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Cry_30_LibCv_AesDecrypt128WorkSpaceUType, CRY_30_LIBCV_VAR_NOINIT) Cry_30_LibCv_AesDecrypt128WorkSpace;  /* PRQA S 0759 */  /* MD_CSL_18.4 */
  /* Index        Referable Keys  */
  /*     0 */  /* [/ActiveEcuC/Cry/CryAesDecrypt128/CryAesDecrypt128Config] */

#define CRY_30_LIBCV_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Cry_30_LibCv_AesEncrypt128WorkSpace
**********************************************************************************************************************/
#define CRY_30_LIBCV_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Cry_30_LibCv_AesEncrypt128WorkSpaceUType, CRY_30_LIBCV_VAR_NOINIT) Cry_30_LibCv_AesEncrypt128WorkSpace;  /* PRQA S 0759 */  /* MD_CSL_18.4 */
  /* Index        Referable Keys  */
  /*     0 */  /* [/ActiveEcuC/Cry/CryAesEncrypt128/CryAesEncrypt128Config] */

#define CRY_30_LIBCV_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Cry_30_LibCv_Fips186WorkSpace
**********************************************************************************************************************/
#define CRY_30_LIBCV_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Cry_30_LibCv_Fips186WorkSpaceUType, CRY_30_LIBCV_VAR_NOINIT) Cry_30_LibCv_Fips186WorkSpace;  /* PRQA S 0759 */  /* MD_CSL_18.4 */
  /* Index        Referable Keys  */
  /*     0 */  /* [/ActiveEcuC/Cry/CryFips186/CryFips186Config] */

#define CRY_30_LIBCV_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Cry_30_LibCv_RsaDecryptWorkSpace
**********************************************************************************************************************/
#define CRY_30_LIBCV_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Cry_30_LibCv_RsaDecryptWorkSpaceUType, CRY_30_LIBCV_VAR_NOINIT) Cry_30_LibCv_RsaDecryptWorkSpace;  /* PRQA S 0759 */  /* MD_CSL_18.4 */
  /* Index        Referable Keys  */
  /*     0 */  /* [/ActiveEcuC/Cry/CryRsaDecrypt/CryRsaDecryptConfig] */

#define CRY_30_LIBCV_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Cry_30_LibCv_RsaSha1SigVerWorkSpace
**********************************************************************************************************************/
#define CRY_30_LIBCV_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Cry_30_LibCv_RsaSha1SigVerWorkSpaceUType, CRY_30_LIBCV_VAR_NOINIT) Cry_30_LibCv_RsaSha1SigVerWorkSpace;  /* PRQA S 0759 */  /* MD_CSL_18.4 */
  /* Index        Referable Keys  */
  /*     0 */  /* [/ActiveEcuC/Cry/CryRsaSha1SigVer/CryRsaSha1SigVerConfig] */

#define CRY_30_LIBCV_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */





/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/



 
/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/





