/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: BswM
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: BswM_Cfg.h
 *   Generation Time: 2020-11-11 14:25:28
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/


#if !defined(BSWM_CFG_H)
#define BSWM_CFG_H

/* -----------------------------------------------------------------------------
    &&&~ INCLUDE
 ----------------------------------------------------------------------------- */
#include "Std_Types.h"
#include "ComStack_Types.h" 
#include "BswM_ComM.h"
#include "BswM_CanSM.h"
#include "BswM_LinSM.h"
#include "BswM_Dcm.h"
#include "BswM_EcuM.h"
#include "BswM_LinTp.h"
#include "BswM_NvM.h"
#include "BswM_J1939Nm.h"
#include "Rte_BswM_Type.h"






/* -----------------------------------------------------------------------------
    &&&~ GENERAL DEFINES
 ----------------------------------------------------------------------------- */
#ifndef BSWM_DEV_ERROR_DETECT
#define BSWM_DEV_ERROR_DETECT STD_OFF
#endif
#ifndef BSWM_DEV_ERROR_REPORT
#define BSWM_DEV_ERROR_REPORT STD_OFF
#endif
#ifndef BSWM_USE_DUMMY_STATEMENT
#define BSWM_USE_DUMMY_STATEMENT STD_OFF /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef BSWM_DUMMY_STATEMENT
#define BSWM_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef BSWM_DUMMY_STATEMENT_CONST
#define BSWM_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef BSWM_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define BSWM_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef BSWM_ATOMIC_VARIABLE_ACCESS
#define BSWM_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef BSWM_PROCESSOR_MPC5746C
#define BSWM_PROCESSOR_MPC5746C
#endif
#ifndef BSWM_COMP_DIAB
#define BSWM_COMP_DIAB
#endif
#ifndef BSWM_GEN_GENERATOR_MSR
#define BSWM_GEN_GENERATOR_MSR
#endif
#ifndef BSWM_CPUTYPE_BITORDER_MSB2LSB
#define BSWM_CPUTYPE_BITORDER_MSB2LSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef BSWM_CONFIGURATION_VARIANT_PRECOMPILE
#define BSWM_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef BSWM_CONFIGURATION_VARIANT_LINKTIME
#define BSWM_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef BSWM_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define BSWM_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef BSWM_CONFIGURATION_VARIANT
#define BSWM_CONFIGURATION_VARIANT BSWM_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef BSWM_POSTBUILD_VARIANT_SUPPORT
#define BSWM_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif


#if !defined (BSWM_DUMMY_STATEMENT)
# define BSWM_DUMMY_STATEMENT(statement) (void)statement
#endif

/* -----------------------------------------------------------------------------
    &&&~ CONFIGURATION DEFINES
 ----------------------------------------------------------------------------- */

/* START of Checksum include for */
/* START of Checksum include for - SysService_Asr4BswMCfg5PrecompileCRC */

#define BSWM_MODE_CHECK                      STD_ON
#define BSWM_ENABLE_CANSM                    STD_ON
#define BSWM_ENABLE_FRSM                     STD_OFF
#define BSWM_ENABLE_LINSM                    STD_ON
#define BSWM_ENABLE_ETHIF                    STD_OFF
#define BSWM_ENABLE_ETHSM                    STD_OFF
#define BSWM_ENABLE_LINTP                    STD_ON
#define BSWM_ENABLE_DCM                      STD_ON
#define BSWM_ENABLE_NVM                      STD_ON
#define BSWM_ENABLE_ECUM                     STD_ON
#define BSWM_ENABLE_COMM                     STD_ON
#define BSWM_ENABLE_J1939DCM                 STD_OFF
#define BSWM_ENABLE_J1939NM                  STD_ON
#define BSWM_ENABLE_SD                       STD_OFF
#define BSWM_ENABLE_NM                       STD_OFF
#define BSWM_ENABLE_PDUR                     STD_OFF
#define BSWM_ENABLE_WDGM                     STD_OFF
#define BSWM_ENABLE_RULE_CONTROL             STD_ON
#define BSWM_VERSION_INFO_API                STD_OFF
#define BSWM_COMM_PNC_SUPPORT                STD_OFF
#define BSWM_COMM_INITIATE_RESET             STD_OFF
#define BSWM_CHANNEL_COUNT                   14u
#define BSWM_WAKEUP_SOURCE_COUNT             22u
#define BSWM_IPDU_GROUP_CONTROL              STD_ON
#define BSWM_ECUM_MODE_HANDLING              STD_ON
#define BSWM_IPDUGROUPVECTORSIZE             5u
#define BSWM_MULTIPARTITION                  STD_OFF



/* END of Checksum include for - SysService_Asr4BswMCfg5PrecompileCRC */

/* END of Checksum include for */

/* -----------------------------------------------------------------------------
    &&&~ RULE DEFINES
 ----------------------------------------------------------------------------- */
#define BswMConf_BswMRule_CC_Rule_LIN1_SchTableStartInd_Table1 (43) 
#define BswMConf_BswMRule_CC_Rule_LIN2_SchTableStartInd_MSTable0 (55) 
#define BswMConf_BswMRule_CC_Rule_LIN3_SchTableStartInd_MSTable (64) 
#define BswMConf_BswMRule_CC_Rule_LIN4_SchTableStartInd_MSTable (78) 
#define BswMConf_BswMRule_CC_Rule_LIN5_SchTableStartInd_MSTable (92) 
#define BswMConf_BswMRule_CC_Rule_LIN1_SchTableStartInd_Table2 (44) 
#define BswMConf_BswMRule_CC_Rule_LIN1_SchTableStartInd_TableE (45) 
#define BswMConf_BswMRule_CC_Rule_LIN1_SchTableStartInd_MSTable1 (41) 
#define BswMConf_BswMRule_CC_Rule_LIN1_SchTableStartInd_MSTable2 (42) 
#define BswMConf_BswMRule_CC_Rule_LIN2_SchTableStartInd_Table0 (56) 
#define BswMConf_BswMRule_CC_Rule_LIN2_SchTableStartInd_TableE (57) 
#define BswMConf_BswMRule_CC_Rule_LIN1_SchTableStartInd_MSTable (40) 
#define BswMConf_BswMRule_CC_Rule_LIN2_SchTableStartInd_MSTable (54) 
#define BswMConf_BswMRule_CC_Rule_LIN3_SchTableStartInd_MSTable1 (65) 
#define BswMConf_BswMRule_CC_Rule_LIN3_SchTableStartInd_MSTable2 (66) 
#define BswMConf_BswMRule_CC_Rule_LIN3_SchTableStartInd_Table1 (67) 
#define BswMConf_BswMRule_CC_Rule_LIN3_SchTableStartInd_Table2 (68) 
#define BswMConf_BswMRule_CC_Rule_LIN3_SchTableStartInd_TableE (69) 
#define BswMConf_BswMRule_CC_Rule_LIN4_SchTableStartInd_MSTable1 (79) 
#define BswMConf_BswMRule_CC_Rule_LIN4_SchTableStartInd_MSTable2 (80) 
#define BswMConf_BswMRule_CC_Rule_LIN4_SchTableStartInd_Table1 (81) 
#define BswMConf_BswMRule_CC_Rule_LIN4_SchTableStartInd_Table2 (82) 
#define BswMConf_BswMRule_CC_Rule_LIN4_SchTableStartInd_TableE (83) 
#define BswMConf_BswMRule_CC_Rule_LIN5_SchTableStartInd_MSTable2 (94) 
#define BswMConf_BswMRule_CC_Rule_LIN5_SchTableStartInd_MSTable1 (93) 
#define BswMConf_BswMRule_CC_Rule_LIN5_SchTableStartInd_Table1 (95) 
#define BswMConf_BswMRule_CC_Rule_LIN5_SchTableStartInd_Table2 (96) 
#define BswMConf_BswMRule_CC_Rule_LIN5_SchTableStartInd_TableE (97) 
#define BswMConf_BswMRule_CC_Rule_DcmEcuReset_Execute (36) 
#define BswMConf_BswMRule_CC_Rule_DcmEcuReset_Trigger (38) 
#define BswMConf_BswMRule_CC_Rule_LIN3_Schedule_To_Table1 (75) 
#define BswMConf_BswMRule_CC_Rule_LIN3_Schedule_To_NULL (74) 
#define BswMConf_BswMRule_CC_Rule_LIN3_Schedule_To_Table2 (76) 
#define BswMConf_BswMRule_CC_Rule_LIN3_Schedule_To_Table_E (77) 
#define BswMConf_BswMRule_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp (71) 
#define BswMConf_BswMRule_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table1 (72) 
#define BswMConf_BswMRule_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table2 (73) 
#define BswMConf_BswMRule_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp (47) 
#define BswMConf_BswMRule_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table1 (48) 
#define BswMConf_BswMRule_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table2 (49) 
#define BswMConf_BswMRule_CC_Rule_LIN1_Schedule_To_NULL (50) 
#define BswMConf_BswMRule_CC_Rule_LIN1_Schedule_To_Table1 (51) 
#define BswMConf_BswMRule_CC_Rule_LIN1_Schedule_To_Table2 (52) 
#define BswMConf_BswMRule_CC_Rule_LIN1_Schedule_To_Table_E (53) 
#define BswMConf_BswMRule_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp (59) 
#define BswMConf_BswMRule_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp_Table0 (60) 
#define BswMConf_BswMRule_CC_Rule_LIN2_Schedule_To_NULL (61) 
#define BswMConf_BswMRule_CC_Rule_LIN2_Schedule_To_Table0 (62) 
#define BswMConf_BswMRule_CC_Rule_LIN2_Schedule_To_Table_E (63) 
#define BswMConf_BswMRule_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp (85) 
#define BswMConf_BswMRule_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table1 (86) 
#define BswMConf_BswMRule_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table2 (87) 
#define BswMConf_BswMRule_CC_Rule_LIN4_Schedule_To_NULL (88) 
#define BswMConf_BswMRule_CC_Rule_LIN4_Schedule_To_Table1 (89) 
#define BswMConf_BswMRule_CC_Rule_LIN4_Schedule_To_Table2 (90) 
#define BswMConf_BswMRule_CC_Rule_LIN4_Schedule_To_Table_E (91) 
#define BswMConf_BswMRule_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp (99) 
#define BswMConf_BswMRule_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table1 (100) 
#define BswMConf_BswMRule_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table2 (101) 
#define BswMConf_BswMRule_CC_Rule_LIN5_Schedule_To_NULL (102) 
#define BswMConf_BswMRule_CC_Rule_LIN5_Schedule_To_Table1 (103) 
#define BswMConf_BswMRule_CC_Rule_LIN5_Schedule_To_Table2 (104) 
#define BswMConf_BswMRule_CC_Rule_LIN5_Schedule_To_Table_E (105) 
#define BswMConf_BswMRule_CC_CN_CabSubnet_9ea693f1_RX_DM (12) 
#define BswMConf_BswMRule_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289 (1) 
#define BswMConf_BswMRule_CC_CN_Backbone2_78967e2c_RX_DM (6) 
#define BswMConf_BswMRule_CC_CN_LIN02_c2d7c6f3 (21) 
#define BswMConf_BswMRule_CC_CN_SecuritySubnet_e7a0ee54_RX (27) 
#define BswMConf_BswMRule_CC_CN_SecuritySubnet_e7a0ee54_RX_DM (28) 
#define BswMConf_BswMRule_CC_CN_FMSNet_fce1aae5_TX (18) 
#define BswMConf_BswMRule_CC_CN_Backbone2_78967e2c_RX (5) 
#define BswMConf_BswMRule_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289 (14) 
#define BswMConf_BswMRule_CC_CN_CabSubnet_9ea693f1_RX (11) 
#define BswMConf_BswMRule_CC_CN_LIN03_b5d0f665 (22) 
#define BswMConf_BswMRule_CC_CN_FMSNet_fce1aae5_RX (16) 
#define BswMConf_BswMRule_CC_CN_LIN01_5bde9749 (20) 
#define BswMConf_BswMRule_CC_CN_LIN04_2bb463c6 (23) 
#define BswMConf_BswMRule_CC_CN_Backbone1J1939_0b1f4bae_RX (3) 
#define BswMConf_BswMRule_CC_CN_FMSNet_fce1aae5_RX_DM (17) 
#define BswMConf_BswMRule_CC_CN_Backbone1J1939_0b1f4bae_RX_DM (4) 
#define BswMConf_BswMRule_CC_CN_LIN00_2cd9a7df (19) 
#define BswMConf_BswMRule_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX (15) 
#define BswMConf_BswMRule_ESH_RunToPostRun (134) 
#define BswMConf_BswMRule_ESH_RunToPostRunNested (135) 
#define BswMConf_BswMRule_ESH_WaitToShutdown (136) 
#define BswMConf_BswMRule_ESH_WakeupToPrep (138) 
#define BswMConf_BswMRule_ESH_WaitToWakeup (137) 
#define BswMConf_BswMRule_ESH_WakeupToRun (139) 
#define BswMConf_BswMRule_ESH_InitToWakeup (130) 
#define BswMConf_BswMRule_ESH_PostRunNested (132) 
#define BswMConf_BswMRule_ESH_PostRun (131) 
#define BswMConf_BswMRule_ESH_PrepToWait (133) 
#define BswMConf_BswMRule_ESH_DemInit (129) 
#define BswMConf_BswMRule_CC_CN_SecuritySubnet_e7a0ee54_TX (29) 
#define BswMConf_BswMRule_CC_CN_CabSubnet_9ea693f1_TX (13) 
#define BswMConf_BswMRule_CC_CN_Backbone2_78967e2c_TX (7) 
#define BswMConf_BswMRule_CC_Rule_LIN1_ScheduleTableEndNotification (46) 
#define BswMConf_BswMRule_CC_Rule_LIN2_ScheduleTableEndNotification (58) 
#define BswMConf_BswMRule_CC_Rule_LIN3_ScheduleTableEndNotification (70) 
#define BswMConf_BswMRule_CC_Rule_LIN4_ScheduleTableEndNotification (84) 
#define BswMConf_BswMRule_CC_Rule_LIN5_ScheduleTableEndNotification (98) 
#define BswMConf_BswMRule_CC_CN_LIN06_c5ba02ea (25) 
#define BswMConf_BswMRule_CC_CN_LIN05_5cb35350 (24) 
#define BswMConf_BswMRule_CC_CN_LIN07_b2bd327c (26) 
#define BswMConf_BswMRule_CC_CN_CAN6_b040c073_RX (8) 
#define BswMConf_BswMRule_CC_CN_CAN6_b040c073_TX (10) 
#define BswMConf_BswMRule_CC_CN_CAN6_b040c073_RX_DM (9) 
#define BswMConf_BswMRule_CC_Rule_LIN6_ScheduleTableEndNotification (109) 
#define BswMConf_BswMRule_CC_Rule_LIN7_ScheduleTableEndNotification (116) 
#define BswMConf_BswMRule_CC_Rule_LIN8_ScheduleTableEndNotification (123) 
#define BswMConf_BswMRule_CC_Rule_LIN6_SchTableStartInd_MSTable0 (107) 
#define BswMConf_BswMRule_CC_Rule_LIN6_SchTableStartInd_MSTable (106) 
#define BswMConf_BswMRule_CC_Rule_LIN6_SchTableStartInd_Table0 (108) 
#define BswMConf_BswMRule_CC_Rule_LIN7_SchTableStartInd_MSTable (113) 
#define BswMConf_BswMRule_CC_Rule_LIN8_SchTableStartInd_MSTable (120) 
#define BswMConf_BswMRule_CC_Rule_LIN7_SchTableStartInd_MSTable0 (114) 
#define BswMConf_BswMRule_CC_Rule_LIN8_SchTableStartInd_MSTable0 (121) 
#define BswMConf_BswMRule_CC_Rule_LIN7_SchTableStartInd_Table0 (115) 
#define BswMConf_BswMRule_CC_Rule_LIN8_SchTableStartInd_Table0 (122) 
#define BswMConf_BswMRule_CC_Rule_LIN6_Schedule_To_Table0 (112) 
#define BswMConf_BswMRule_CC_Rule_LIN7_Schedule_To_Table0 (119) 
#define BswMConf_BswMRule_CC_Rule_LIN8_Schedule_To_Table0 (126) 
#define BswMConf_BswMRule_CC_Rule_LIN6_Schedule_To_MSTable (110) 
#define BswMConf_BswMRule_CC_Rule_LIN7_Schedule_To_MSTable (117) 
#define BswMConf_BswMRule_CC_Rule_LIN8_Schedule_To_MSTable (124) 
#define BswMConf_BswMRule_CC_Rule_LIN6_Schedule_To_MSTable0 (111) 
#define BswMConf_BswMRule_CC_Rule_LIN7_Schedule_To_MSTable0 (118) 
#define BswMConf_BswMRule_CC_Rule_LIN8_Schedule_To_MSTable0 (125) 
#define BswMConf_BswMRule_CC_DCMResetProcess_Started (31) 
#define BswMConf_BswMRule_CC_DCMResetProcess_InProgress (30) 
#define BswMConf_BswMRule_CC_Rule_DcmEcuReset_JumpToBTL (37) 
#define BswMConf_BswMRule_CC_Rule_NvmWriteAll_Request (127) 
#define BswMConf_BswMRule_CC_Rule_BB1_BusOff_Indication (32) 
#define BswMConf_BswMRule_CC_Rule_BB2_BusOff_Indication (33) 
#define BswMConf_BswMRule_CC_Rule_CAN6_BusOff_Indication (34) 
#define BswMConf_BswMRule_CC_Rule_CabSubnet_BusOff_Indication (35) 
#define BswMConf_BswMRule_CC_Rule_FMSNet_BusOff_Indication (39) 
#define BswMConf_BswMRule_CC_Rule_SecuritySubnet_BusOff_Indication (128) 
#define BswMConf_BswMRule_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX (2) 
#define BswMConf_BswMRule_CC_Rule_PvtReportCtrl (0) 


/* -----------------------------------------------------------------------------
    &&&~ GENERIC DEFINES
 ----------------------------------------------------------------------------- */
#define BSWM_GENERIC_ESH_State               230u 
#define BSWM_GENERIC_ESH_ComMPendingRequests 232u 
#define BSWM_GENERIC_ISSM_RunRequest         0u 
#define BSWM_GENERIC_ESH_DemInitStatus       231u 
#define BSWM_GENERIC_DCMResetProcess         200u 

#define BSWM_GENERICVALUE_ESH_State_ESH_INIT                               0x0000u 
#define BSWM_GENERICVALUE_ESH_State_ESH_POST_RUN                           0x0002u 
#define BSWM_GENERICVALUE_ESH_State_ESH_PREP_SHUTDOWN                      0x0003u 
#define BSWM_GENERICVALUE_ESH_State_ESH_RUN                                0x0001u 
#define BSWM_GENERICVALUE_ESH_State_ESH_SHUTDOWN                           0x0005u 
#define BSWM_GENERICVALUE_ESH_State_ESH_WAIT_FOR_NVM                       0x0004u 
#define BSWM_GENERICVALUE_ESH_State_ESH_WAKEUP                             0x0006u 
#define BSWM_GENERICVALUE_ESH_ComMPendingRequests_ESH_COMM_NO_REQUEST      0x0000u 
#define BSWM_GENERICVALUE_ESH_ComMPendingRequests_ESH_COMM_PENDING_REQUEST 0x0001u 
#define BSWM_GENERICVALUE_ISSM_RunRequest_ISSM_NO_RUN_REQUEST              0x0000u 
#define BSWM_GENERICVALUE_ISSM_RunRequest_ISSM_RUN_REQUEST                 0x0001u 
#define BSWM_GENERICVALUE_ESH_DemInitStatus_DEM_INITIALIZED                0x0000u 
#define BSWM_GENERICVALUE_ESH_DemInitStatus_DEM_NOT_INITIALIZED            0x0001u 
#define BSWM_GENERICVALUE_DCMResetProcess_ResetProcess_Completed           0x0003u 
#define BSWM_GENERICVALUE_DCMResetProcess_ResetProcess_Idle                0x0000u 
#define BSWM_GENERICVALUE_DCMResetProcess_ResetProcess_Inprogress          0x0002u 
#define BSWM_GENERICVALUE_DCMResetProcess_ResetProcess_Started             0x0001u 


/* -----------------------------------------------------------------------------
    &&&~ TIMER DEFINES
 ----------------------------------------------------------------------------- */
#define BSWM_TMR_ESH_NvM_WriteAllTimer       1u 
#define BSWM_TMR_ESH_NvM_CancelWriteAllTimer 0u 
#define BSWM_TMR_ESH_SelfRunRequestTimer     2u 



/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  BswMPCDataSwitches  BswM Data Switches  (PRE_COMPILE)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define BSWM_ACTIONLISTPRIORITYQUEUE                                                                STD_OFF  /**< Deactivateable: 'BswM_ActionListPriorityQueue' Reason: 'Action List Queue Search Algorithm is not equal to PRIORITY_QUEUE' */
#define BSWM_ACTIONLISTQUEUE                                                                        STD_ON
#define BSWM_ACTIONLISTS                                                                            STD_ON
#define BSWM_FCTPTROFACTIONLISTS                                                                    STD_ON
#define BSWM_CANSMCHANNELMAPPING                                                                    STD_ON
#define BSWM_EXTERNALIDOFCANSMCHANNELMAPPING                                                        STD_ON
#define BSWM_IMMEDIATEUSERENDIDXOFCANSMCHANNELMAPPING                                               STD_ON
#define BSWM_IMMEDIATEUSERSTARTIDXOFCANSMCHANNELMAPPING                                             STD_ON
#define BSWM_IMMEDIATEUSERUSEDOFCANSMCHANNELMAPPING                                                 STD_ON
#define BSWM_INITVALUEOFCANSMCHANNELMAPPING                                                         STD_ON
#define BSWM_CANSMCHANNELSTATE                                                                      STD_ON
#define BSWM_COMMCHANNELMAPPING                                                                     STD_ON
#define BSWM_EXTERNALIDOFCOMMCHANNELMAPPING                                                         STD_ON
#define BSWM_IMMEDIATEUSERENDIDXOFCOMMCHANNELMAPPING                                                STD_OFF  /**< Deactivateable: 'BswM_ComMChannelMapping.ImmediateUserEndIdx' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfComMChannelMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFCOMMCHANNELMAPPING                                              STD_OFF  /**< Deactivateable: 'BswM_ComMChannelMapping.ImmediateUserStartIdx' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfComMChannelMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_IMMEDIATEUSERUSEDOFCOMMCHANNELMAPPING                                                  STD_OFF  /**< Deactivateable: 'BswM_ComMChannelMapping.ImmediateUserUsed' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfComMChannelMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_INITVALUEOFCOMMCHANNELMAPPING                                                          STD_ON
#define BSWM_COMMCHANNELSTATE                                                                       STD_ON
#define BSWM_COMMINITIATERESETMAPPING                                                               STD_OFF  /**< Deactivateable: 'BswM_ComMInitiateResetMapping' Reason: 'No Mode Request for BswMComMInitiateReset configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFCOMMINITIATERESETMAPPING                                          STD_OFF  /**< Deactivateable: 'BswM_ComMInitiateResetMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMComMInitiateReset configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFCOMMINITIATERESETMAPPING                                        STD_OFF  /**< Deactivateable: 'BswM_ComMInitiateResetMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMComMInitiateReset configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFCOMMINITIATERESETMAPPING                                            STD_OFF  /**< Deactivateable: 'BswM_ComMInitiateResetMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMComMInitiateReset configured.' */
#define BSWM_INITVALUEOFCOMMINITIATERESETMAPPING                                                    STD_OFF  /**< Deactivateable: 'BswM_ComMInitiateResetMapping.InitValue' Reason: 'No Mode Request for BswMComMInitiateReset configured.' */
#define BSWM_COMMINITIATERESETSTATE                                                                 STD_OFF  /**< Deactivateable: 'BswM_ComMInitiateResetState' Reason: 'No Mode Request for BswMComMInitiateReset configured.' */
#define BSWM_COMMPNCMAPPING                                                                         STD_OFF  /**< Deactivateable: 'BswM_ComMPncMapping' Reason: 'No Mode Request for BswMComMPncRequest configured.' */
#define BSWM_EXTERNALIDOFCOMMPNCMAPPING                                                             STD_OFF  /**< Deactivateable: 'BswM_ComMPncMapping.ExternalId' Reason: 'No Mode Request for BswMComMPncRequest configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFCOMMPNCMAPPING                                                    STD_OFF  /**< Deactivateable: 'BswM_ComMPncMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMComMPncRequest configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFCOMMPNCMAPPING                                                  STD_OFF  /**< Deactivateable: 'BswM_ComMPncMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMComMPncRequest configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFCOMMPNCMAPPING                                                      STD_OFF  /**< Deactivateable: 'BswM_ComMPncMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMComMPncRequest configured.' */
#define BSWM_INITVALUEOFCOMMPNCMAPPING                                                              STD_OFF  /**< Deactivateable: 'BswM_ComMPncMapping.InitValue' Reason: 'No Mode Request for BswMComMPncRequest configured.' */
#define BSWM_COMMPNCSTATE                                                                           STD_OFF  /**< Deactivateable: 'BswM_ComMPncState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_DCMAPPLUPDATEMAPPING                                                                   STD_OFF  /**< Deactivateable: 'BswM_DcmApplUpdateMapping' Reason: 'No Mode Request for BswMDcmApplicationUpdatedIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFDCMAPPLUPDATEMAPPING                                              STD_OFF  /**< Deactivateable: 'BswM_DcmApplUpdateMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMDcmApplicationUpdatedIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFDCMAPPLUPDATEMAPPING                                            STD_OFF  /**< Deactivateable: 'BswM_DcmApplUpdateMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMDcmApplicationUpdatedIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFDCMAPPLUPDATEMAPPING                                                STD_OFF  /**< Deactivateable: 'BswM_DcmApplUpdateMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMDcmApplicationUpdatedIndication configured.' */
#define BSWM_INITVALUEOFDCMAPPLUPDATEMAPPING                                                        STD_OFF  /**< Deactivateable: 'BswM_DcmApplUpdateMapping.InitValue' Reason: 'No Mode Request for BswMDcmApplicationUpdatedIndication configured.' */
#define BSWM_DCMAPPLUPDATESTATE                                                                     STD_OFF  /**< Deactivateable: 'BswM_DcmApplUpdateState' Reason: 'No Mode Request for BswMDcmApplicationUpdatedIndication configured.' */
#define BSWM_DCMCOMMAPPING                                                                          STD_ON
#define BSWM_EXTERNALIDOFDCMCOMMAPPING                                                              STD_ON
#define BSWM_IMMEDIATEUSERENDIDXOFDCMCOMMAPPING                                                     STD_ON
#define BSWM_IMMEDIATEUSERSTARTIDXOFDCMCOMMAPPING                                                   STD_ON
#define BSWM_IMMEDIATEUSERUSEDOFDCMCOMMAPPING                                                       STD_ON
#define BSWM_INITVALUEOFDCMCOMMAPPING                                                               STD_ON
#define BSWM_DCMCOMSTATE                                                                            STD_ON
#define BSWM_DEFERREDRULES                                                                          STD_ON
#define BSWM_RULESIDXOFDEFERREDRULES                                                                STD_ON
#define BSWM_ECUMMODEMAPPING                                                                        STD_OFF  /**< Deactivateable: 'BswM_EcuMModeMapping' Reason: 'No Mode Request for BswMEcuMIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFECUMMODEMAPPING                                                   STD_OFF  /**< Deactivateable: 'BswM_EcuMModeMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMEcuMIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFECUMMODEMAPPING                                                 STD_OFF  /**< Deactivateable: 'BswM_EcuMModeMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMEcuMIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFECUMMODEMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_EcuMModeMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMEcuMIndication configured.' */
#define BSWM_INITVALUEOFECUMMODEMAPPING                                                             STD_OFF  /**< Deactivateable: 'BswM_EcuMModeMapping.InitValue' Reason: 'No Mode Request for BswMEcuMIndication configured.' */
#define BSWM_ECUMMODESTATE                                                                          STD_OFF  /**< Deactivateable: 'BswM_EcuMModeState' Reason: 'No Mode Request for BswMEcuMIndication configured.' */
#define BSWM_ECUMRUNREQUESTMAPPING                                                                  STD_OFF  /**< Deactivateable: 'BswM_EcuMRunRequestMapping' Reason: 'No Mode Request for BswMEcuMRUNRequestIndication configured.' */
#define BSWM_EXTERNALIDOFECUMRUNREQUESTMAPPING                                                      STD_OFF  /**< Deactivateable: 'BswM_EcuMRunRequestMapping.ExternalId' Reason: 'No Mode Request for BswMEcuMRUNRequestIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFECUMRUNREQUESTMAPPING                                             STD_OFF  /**< Deactivateable: 'BswM_EcuMRunRequestMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMEcuMRUNRequestIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFECUMRUNREQUESTMAPPING                                           STD_OFF  /**< Deactivateable: 'BswM_EcuMRunRequestMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMEcuMRUNRequestIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFECUMRUNREQUESTMAPPING                                               STD_OFF  /**< Deactivateable: 'BswM_EcuMRunRequestMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMEcuMRUNRequestIndication configured.' */
#define BSWM_INITVALUEOFECUMRUNREQUESTMAPPING                                                       STD_OFF  /**< Deactivateable: 'BswM_EcuMRunRequestMapping.InitValue' Reason: 'No Mode Request for BswMEcuMRUNRequestIndication configured.' */
#define BSWM_ECUMRUNREQUESTSTATE                                                                    STD_OFF  /**< Deactivateable: 'BswM_EcuMRunRequestState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_ECUMWAKEUPMAPPING                                                                      STD_OFF  /**< Deactivateable: 'BswM_EcuMWakeupMapping' Reason: 'No Mode Request for BswMEcuMWakeupSource configured.' */
#define BSWM_EXTERNALIDOFECUMWAKEUPMAPPING                                                          STD_OFF  /**< Deactivateable: 'BswM_EcuMWakeupMapping.ExternalId' Reason: 'No Mode Request for BswMEcuMWakeupSource configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFECUMWAKEUPMAPPING                                                 STD_OFF  /**< Deactivateable: 'BswM_EcuMWakeupMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMEcuMWakeupSource configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFECUMWAKEUPMAPPING                                               STD_OFF  /**< Deactivateable: 'BswM_EcuMWakeupMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMEcuMWakeupSource configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFECUMWAKEUPMAPPING                                                   STD_OFF  /**< Deactivateable: 'BswM_EcuMWakeupMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMEcuMWakeupSource configured.' */
#define BSWM_INITVALUEOFECUMWAKEUPMAPPING                                                           STD_OFF  /**< Deactivateable: 'BswM_EcuMWakeupMapping.InitValue' Reason: 'No Mode Request for BswMEcuMWakeupSource configured.' */
#define BSWM_ECUMWAKEUPSTATE                                                                        STD_OFF  /**< Deactivateable: 'BswM_EcuMWakeupState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_ETHIFPORTMAPPING                                                                       STD_OFF  /**< Deactivateable: 'BswM_EthIfPortMapping' Reason: 'No Mode Request for BswMEthIfPortGroupLinkStateChg configured.' */
#define BSWM_EXTERNALIDOFETHIFPORTMAPPING                                                           STD_OFF  /**< Deactivateable: 'BswM_EthIfPortMapping.ExternalId' Reason: 'No Mode Request for BswMEthIfPortGroupLinkStateChg configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFETHIFPORTMAPPING                                                  STD_OFF  /**< Deactivateable: 'BswM_EthIfPortMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMEthIfPortGroupLinkStateChg configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFETHIFPORTMAPPING                                                STD_OFF  /**< Deactivateable: 'BswM_EthIfPortMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMEthIfPortGroupLinkStateChg configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFETHIFPORTMAPPING                                                    STD_OFF  /**< Deactivateable: 'BswM_EthIfPortMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMEthIfPortGroupLinkStateChg configured.' */
#define BSWM_INITVALUEOFETHIFPORTMAPPING                                                            STD_OFF  /**< Deactivateable: 'BswM_EthIfPortMapping.InitValue' Reason: 'No Mode Request for BswMEthIfPortGroupLinkStateChg configured.' */
#define BSWM_ETHIFPORTSTATE                                                                         STD_OFF  /**< Deactivateable: 'BswM_EthIfPortState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_ETHSMMAPPING                                                                           STD_OFF  /**< Deactivateable: 'BswM_EthSMMapping' Reason: 'No Mode Request for BswMEthSMIndication configured.' */
#define BSWM_EXTERNALIDOFETHSMMAPPING                                                               STD_OFF  /**< Deactivateable: 'BswM_EthSMMapping.ExternalId' Reason: 'No Mode Request for BswMEthSMIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFETHSMMAPPING                                                      STD_OFF  /**< Deactivateable: 'BswM_EthSMMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMEthSMIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFETHSMMAPPING                                                    STD_OFF  /**< Deactivateable: 'BswM_EthSMMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMEthSMIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFETHSMMAPPING                                                        STD_OFF  /**< Deactivateable: 'BswM_EthSMMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMEthSMIndication configured.' */
#define BSWM_INITVALUEOFETHSMMAPPING                                                                STD_OFF  /**< Deactivateable: 'BswM_EthSMMapping.InitValue' Reason: 'No Mode Request for BswMEthSMIndication configured.' */
#define BSWM_ETHSMSTATE                                                                             STD_OFF  /**< Deactivateable: 'BswM_EthSMState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_FINALMAGICNUMBER                                                                       STD_OFF  /**< Deactivateable: 'BswM_FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define BSWM_FORCEDACTIONLISTPRIORITY                                                               STD_ON
#define BSWM_FRSMMAPPING                                                                            STD_OFF  /**< Deactivateable: 'BswM_FrSMMapping' Reason: 'No Mode Request for BswMFrSMIndication configured.' */
#define BSWM_EXTERNALIDOFFRSMMAPPING                                                                STD_OFF  /**< Deactivateable: 'BswM_FrSMMapping.ExternalId' Reason: 'No Mode Request for BswMFrSMIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFFRSMMAPPING                                                       STD_OFF  /**< Deactivateable: 'BswM_FrSMMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMFrSMIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFFRSMMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_FrSMMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMFrSMIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFFRSMMAPPING                                                         STD_OFF  /**< Deactivateable: 'BswM_FrSMMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMFrSMIndication configured.' */
#define BSWM_INITVALUEOFFRSMMAPPING                                                                 STD_OFF  /**< Deactivateable: 'BswM_FrSMMapping.InitValue' Reason: 'No Mode Request for BswMFrSMIndication configured.' */
#define BSWM_FRSMSTATE                                                                              STD_OFF  /**< Deactivateable: 'BswM_FrSMState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_GENERICMAPPING                                                                         STD_ON
#define BSWM_EXTERNALIDOFGENERICMAPPING                                                             STD_ON
#define BSWM_IMMEDIATEUSERENDIDXOFGENERICMAPPING                                                    STD_ON
#define BSWM_IMMEDIATEUSERSTARTIDXOFGENERICMAPPING                                                  STD_ON
#define BSWM_IMMEDIATEUSERUSEDOFGENERICMAPPING                                                      STD_ON
#define BSWM_INITVALUEOFGENERICMAPPING                                                              STD_ON
#define BSWM_GENERICSTATE                                                                           STD_ON
#define BSWM_IMMEDIATEUSER                                                                          STD_ON
#define BSWM_FORCEDOFIMMEDIATEUSER                                                                  STD_OFF  /**< Deactivateable: 'BswM_ImmediateUser.Forced' Reason: 'the value of BswM_ForcedOfImmediateUser is always 'false' due to this, the array is deactivated.' */
#define BSWM_MASKEDBITSOFIMMEDIATEUSER                                                              STD_ON
#define BSWM_ONINITOFIMMEDIATEUSER                                                                  STD_ON
#define BSWM_RULESINDENDIDXOFIMMEDIATEUSER                                                          STD_ON
#define BSWM_RULESINDSTARTIDXOFIMMEDIATEUSER                                                        STD_ON
#define BSWM_RULESINDUSEDOFIMMEDIATEUSER                                                            STD_ON
#define BSWM_INITDATAHASHCODE                                                                       STD_OFF  /**< Deactivateable: 'BswM_InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define BSWM_INITGENVARANDINITAL                                                                    STD_ON
#define BSWM_INITIALIZED                                                                            STD_ON
#define BSWM_J1939DCMMAPPING                                                                        STD_OFF  /**< Deactivateable: 'BswM_J1939DcmMapping' Reason: 'No Mode Request for BswMJ1939DcmBroadcastStatus configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFJ1939DCMMAPPING                                                   STD_OFF  /**< Deactivateable: 'BswM_J1939DcmMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMJ1939DcmBroadcastStatus configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFJ1939DCMMAPPING                                                 STD_OFF  /**< Deactivateable: 'BswM_J1939DcmMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMJ1939DcmBroadcastStatus configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFJ1939DCMMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_J1939DcmMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMJ1939DcmBroadcastStatus configured.' */
#define BSWM_INITVALUEOFJ1939DCMMAPPING                                                             STD_OFF  /**< Deactivateable: 'BswM_J1939DcmMapping.InitValue' Reason: 'No Mode Request for BswMJ1939DcmBroadcastStatus configured.' */
#define BSWM_J1939DCMSTATE                                                                          STD_OFF  /**< Deactivateable: 'BswM_J1939DcmState' Reason: 'No Mode Request for BswMJ1939DcmBroadcastStatus configured.' */
#define BSWM_J1939NMMAPPING                                                                         STD_ON
#define BSWM_EXTERNALIDOFJ1939NMMAPPING                                                             STD_ON
#define BSWM_IMMEDIATEUSERENDIDXOFJ1939NMMAPPING                                                    STD_ON
#define BSWM_IMMEDIATEUSERSTARTIDXOFJ1939NMMAPPING                                                  STD_ON
#define BSWM_IMMEDIATEUSERUSEDOFJ1939NMMAPPING                                                      STD_ON
#define BSWM_INITVALUEOFJ1939NMMAPPING                                                              STD_ON
#define BSWM_J1939NMSTATE                                                                           STD_ON
#define BSWM_LENGTHOFACTIONLISTPRIORITYQUEUE                                                        STD_OFF  /**< Deactivateable: 'BswM_LengthOfActionListPriorityQueue' Reason: 'ActionListPriorityQueue not enabled' */
#define BSWM_LINSMMAPPING                                                                           STD_ON
#define BSWM_EXTERNALIDOFLINSMMAPPING                                                               STD_ON
#define BSWM_IMMEDIATEUSERENDIDXOFLINSMMAPPING                                                      STD_ON
#define BSWM_IMMEDIATEUSERSTARTIDXOFLINSMMAPPING                                                    STD_ON
#define BSWM_IMMEDIATEUSERUSEDOFLINSMMAPPING                                                        STD_ON
#define BSWM_INITVALUEOFLINSMMAPPING                                                                STD_ON
#define BSWM_LINSMSTATE                                                                             STD_ON
#define BSWM_LINSCHEDULEENDMAPPING                                                                  STD_ON
#define BSWM_EXTERNALIDOFLINSCHEDULEENDMAPPING                                                      STD_ON
#define BSWM_IMMEDIATEUSERENDIDXOFLINSCHEDULEENDMAPPING                                             STD_ON
#define BSWM_IMMEDIATEUSERSTARTIDXOFLINSCHEDULEENDMAPPING                                           STD_ON
#define BSWM_IMMEDIATEUSERUSEDOFLINSCHEDULEENDMAPPING                                               STD_ON
#define BSWM_INITVALUEOFLINSCHEDULEENDMAPPING                                                       STD_ON
#define BSWM_LINSCHEDULEENDSTATE                                                                    STD_ON
#define BSWM_LINSCHEDULEMAPPING                                                                     STD_ON
#define BSWM_EXTERNALIDOFLINSCHEDULEMAPPING                                                         STD_ON
#define BSWM_IMMEDIATEUSERENDIDXOFLINSCHEDULEMAPPING                                                STD_ON
#define BSWM_IMMEDIATEUSERSTARTIDXOFLINSCHEDULEMAPPING                                              STD_ON
#define BSWM_IMMEDIATEUSERUSEDOFLINSCHEDULEMAPPING                                                  STD_ON
#define BSWM_INITVALUEOFLINSCHEDULEMAPPING                                                          STD_ON
#define BSWM_LINSCHEDULESTATE                                                                       STD_ON
#define BSWM_LINTPMAPPING                                                                           STD_OFF  /**< Deactivateable: 'BswM_LinTPMapping' Reason: 'No Mode Request for BswMLinTpModeRequest configured.' */
#define BSWM_EXTERNALIDOFLINTPMAPPING                                                               STD_OFF  /**< Deactivateable: 'BswM_LinTPMapping.ExternalId' Reason: 'No Mode Request for BswMLinTpModeRequest configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFLINTPMAPPING                                                      STD_OFF  /**< Deactivateable: 'BswM_LinTPMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMLinTpModeRequest configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFLINTPMAPPING                                                    STD_OFF  /**< Deactivateable: 'BswM_LinTPMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMLinTpModeRequest configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFLINTPMAPPING                                                        STD_OFF  /**< Deactivateable: 'BswM_LinTPMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMLinTpModeRequest configured.' */
#define BSWM_INITVALUEOFLINTPMAPPING                                                                STD_OFF  /**< Deactivateable: 'BswM_LinTPMapping.InitValue' Reason: 'No Mode Request for BswMLinTpModeRequest configured.' */
#define BSWM_LINTPSTATE                                                                             STD_OFF  /**< Deactivateable: 'BswM_LinTPState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_MODENOTIFICATIONFCT                                                                    STD_ON
#define BSWM_MODENOTIFICATIONMAPPING                                                                STD_OFF  /**< Deactivateable: 'BswM_ModeNotificationMapping' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define BSWM_IMMEDIATEUSERENDIDXOFMODENOTIFICATIONMAPPING                                           STD_OFF  /**< Deactivateable: 'BswM_ModeNotificationMapping.ImmediateUserEndIdx' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfModeNotificationMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFMODENOTIFICATIONMAPPING                                         STD_OFF  /**< Deactivateable: 'BswM_ModeNotificationMapping.ImmediateUserStartIdx' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfModeNotificationMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_IMMEDIATEUSERUSEDOFMODENOTIFICATIONMAPPING                                             STD_OFF  /**< Deactivateable: 'BswM_ModeNotificationMapping.ImmediateUserUsed' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfModeNotificationMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_MODEREQUESTMAPPING                                                                     STD_ON
#define BSWM_IMMEDIATEUSERENDIDXOFMODEREQUESTMAPPING                                                STD_ON
#define BSWM_IMMEDIATEUSERSTARTIDXOFMODEREQUESTMAPPING                                              STD_ON
#define BSWM_IMMEDIATEUSERUSEDOFMODEREQUESTMAPPING                                                  STD_ON
#define BSWM_MODEREQUESTQUEUE                                                                       STD_ON
#define BSWM_NMMAPPING                                                                              STD_OFF  /**< Deactivateable: 'BswM_NmMapping' Reason: 'No Mode Request for BswMNmIndication configured.' */
#define BSWM_EXTERNALIDOFNMMAPPING                                                                  STD_OFF  /**< Deactivateable: 'BswM_NmMapping.ExternalId' Reason: 'No Mode Request for BswMNmIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFNMMAPPING                                                         STD_OFF  /**< Deactivateable: 'BswM_NmMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMNmIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFNMMAPPING                                                       STD_OFF  /**< Deactivateable: 'BswM_NmMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMNmIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFNMMAPPING                                                           STD_OFF  /**< Deactivateable: 'BswM_NmMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMNmIndication configured.' */
#define BSWM_INITVALUEOFNMMAPPING                                                                   STD_OFF  /**< Deactivateable: 'BswM_NmMapping.InitValue' Reason: 'No Mode Request for BswMNmIndication configured.' */
#define BSWM_NMSTATE                                                                                STD_OFF  /**< Deactivateable: 'BswM_NmState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_NVMBLOCKMAPPING                                                                        STD_OFF  /**< Deactivateable: 'BswM_NvMBlockMapping' Reason: 'No Mode Request for BswMNvMRequest configured.' */
#define BSWM_EXTERNALIDOFNVMBLOCKMAPPING                                                            STD_OFF  /**< Deactivateable: 'BswM_NvMBlockMapping.ExternalId' Reason: 'No Mode Request for BswMNvMRequest configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFNVMBLOCKMAPPING                                                   STD_OFF  /**< Deactivateable: 'BswM_NvMBlockMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMNvMRequest configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFNVMBLOCKMAPPING                                                 STD_OFF  /**< Deactivateable: 'BswM_NvMBlockMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMNvMRequest configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFNVMBLOCKMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_NvMBlockMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMNvMRequest configured.' */
#define BSWM_INITVALUEOFNVMBLOCKMAPPING                                                             STD_OFF  /**< Deactivateable: 'BswM_NvMBlockMapping.InitValue' Reason: 'No Mode Request for BswMNvMRequest configured.' */
#define BSWM_NVMBLOCKSTATE                                                                          STD_OFF  /**< Deactivateable: 'BswM_NvMBlockState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_NVMJOBMAPPING                                                                          STD_ON
#define BSWM_EXTERNALIDOFNVMJOBMAPPING                                                              STD_ON
#define BSWM_IMMEDIATEUSERENDIDXOFNVMJOBMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_NvMJobMapping.ImmediateUserEndIdx' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfNvMJobMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFNVMJOBMAPPING                                                   STD_OFF  /**< Deactivateable: 'BswM_NvMJobMapping.ImmediateUserStartIdx' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfNvMJobMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_IMMEDIATEUSERUSEDOFNVMJOBMAPPING                                                       STD_OFF  /**< Deactivateable: 'BswM_NvMJobMapping.ImmediateUserUsed' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfNvMJobMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_INITVALUEOFNVMJOBMAPPING                                                               STD_ON
#define BSWM_NVMJOBSTATE                                                                            STD_ON
#define BSWM_PARTITIONIDENTIFIERS                                                                   STD_ON
#define BSWM_PCPARTITIONCONFIGIDXOFPARTITIONIDENTIFIERS                                             STD_ON
#define BSWM_PARTITIONSNVOFPARTITIONIDENTIFIERS                                                     STD_ON
#define BSWM_PDURRXINDICATIONMAPPING                                                                STD_OFF  /**< Deactivateable: 'BswM_PduRRxIndicationMapping' Reason: 'No Mode Request for BswMPduRRxIndication configured.' */
#define BSWM_EXTERNALIDOFPDURRXINDICATIONMAPPING                                                    STD_OFF  /**< Deactivateable: 'BswM_PduRRxIndicationMapping.ExternalId' Reason: 'No Mode Request for BswMPduRRxIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFPDURRXINDICATIONMAPPING                                           STD_OFF  /**< Deactivateable: 'BswM_PduRRxIndicationMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMPduRRxIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFPDURRXINDICATIONMAPPING                                         STD_OFF  /**< Deactivateable: 'BswM_PduRRxIndicationMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMPduRRxIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFPDURRXINDICATIONMAPPING                                             STD_OFF  /**< Deactivateable: 'BswM_PduRRxIndicationMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMPduRRxIndication configured.' */
#define BSWM_INITVALUEOFPDURRXINDICATIONMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_PduRRxIndicationMapping.InitValue' Reason: 'No Mode Request for BswMPduRRxIndication configured.' */
#define BSWM_PDURRXINDICATIONSTATEIDXOFPDURRXINDICATIONMAPPING                                      STD_OFF  /**< Deactivateable: 'BswM_PduRRxIndicationMapping.PduRRxIndicationStateIdx' Reason: 'No Mode Request for BswMPduRRxIndication configured.' */
#define BSWM_PDURRXINDICATIONSTATE                                                                  STD_OFF  /**< Deactivateable: 'BswM_PduRRxIndicationState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_PDURTPRXINDICATIONMAPPING                                                              STD_OFF  /**< Deactivateable: 'BswM_PduRTpRxIndicationMapping' Reason: 'No Mode Request for BswMPduRTpRxIndication configured.' */
#define BSWM_EXTERNALIDOFPDURTPRXINDICATIONMAPPING                                                  STD_OFF  /**< Deactivateable: 'BswM_PduRTpRxIndicationMapping.ExternalId' Reason: 'No Mode Request for BswMPduRTpRxIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFPDURTPRXINDICATIONMAPPING                                         STD_OFF  /**< Deactivateable: 'BswM_PduRTpRxIndicationMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMPduRTpRxIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFPDURTPRXINDICATIONMAPPING                                       STD_OFF  /**< Deactivateable: 'BswM_PduRTpRxIndicationMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMPduRTpRxIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFPDURTPRXINDICATIONMAPPING                                           STD_OFF  /**< Deactivateable: 'BswM_PduRTpRxIndicationMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMPduRTpRxIndication configured.' */
#define BSWM_INITVALUEOFPDURTPRXINDICATIONMAPPING                                                   STD_OFF  /**< Deactivateable: 'BswM_PduRTpRxIndicationMapping.InitValue' Reason: 'No Mode Request for BswMPduRTpRxIndication configured.' */
#define BSWM_PDURTPRXINDICATIONSTATEIDXOFPDURTPRXINDICATIONMAPPING                                  STD_OFF  /**< Deactivateable: 'BswM_PduRTpRxIndicationMapping.PduRTpRxIndicationStateIdx' Reason: 'No Mode Request for BswMPduRTpRxIndication configured.' */
#define BSWM_PDURTPRXINDICATIONSTATE                                                                STD_OFF  /**< Deactivateable: 'BswM_PduRTpRxIndicationState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_PDURTPSTARTOFRECEPTIONMAPPING                                                          STD_OFF  /**< Deactivateable: 'BswM_PduRTpStartOfReceptionMapping' Reason: 'No Mode Request for BswMPduRTpStartOfReception configured.' */
#define BSWM_EXTERNALIDOFPDURTPSTARTOFRECEPTIONMAPPING                                              STD_OFF  /**< Deactivateable: 'BswM_PduRTpStartOfReceptionMapping.ExternalId' Reason: 'No Mode Request for BswMPduRTpStartOfReception configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFPDURTPSTARTOFRECEPTIONMAPPING                                     STD_OFF  /**< Deactivateable: 'BswM_PduRTpStartOfReceptionMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMPduRTpStartOfReception configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFPDURTPSTARTOFRECEPTIONMAPPING                                   STD_OFF  /**< Deactivateable: 'BswM_PduRTpStartOfReceptionMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMPduRTpStartOfReception configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFPDURTPSTARTOFRECEPTIONMAPPING                                       STD_OFF  /**< Deactivateable: 'BswM_PduRTpStartOfReceptionMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMPduRTpStartOfReception configured.' */
#define BSWM_INITVALUEOFPDURTPSTARTOFRECEPTIONMAPPING                                               STD_OFF  /**< Deactivateable: 'BswM_PduRTpStartOfReceptionMapping.InitValue' Reason: 'No Mode Request for BswMPduRTpStartOfReception configured.' */
#define BSWM_PDURTPSTARTOFRECEPTIONSTATEIDXOFPDURTPSTARTOFRECEPTIONMAPPING                          STD_OFF  /**< Deactivateable: 'BswM_PduRTpStartOfReceptionMapping.PduRTpStartOfReceptionStateIdx' Reason: 'No Mode Request for BswMPduRTpStartOfReception configured.' */
#define BSWM_PDURTPSTARTOFRECEPTIONSTATE                                                            STD_OFF  /**< Deactivateable: 'BswM_PduRTpStartOfReceptionState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_PDURTPTXCONFIRMATIONMAPPING                                                            STD_OFF  /**< Deactivateable: 'BswM_PduRTpTxConfirmationMapping' Reason: 'No Mode Request for BswMPduRTpTxConfirmation configured.' */
#define BSWM_EXTERNALIDOFPDURTPTXCONFIRMATIONMAPPING                                                STD_OFF  /**< Deactivateable: 'BswM_PduRTpTxConfirmationMapping.ExternalId' Reason: 'No Mode Request for BswMPduRTpTxConfirmation configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFPDURTPTXCONFIRMATIONMAPPING                                       STD_OFF  /**< Deactivateable: 'BswM_PduRTpTxConfirmationMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMPduRTpTxConfirmation configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFPDURTPTXCONFIRMATIONMAPPING                                     STD_OFF  /**< Deactivateable: 'BswM_PduRTpTxConfirmationMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMPduRTpTxConfirmation configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFPDURTPTXCONFIRMATIONMAPPING                                         STD_OFF  /**< Deactivateable: 'BswM_PduRTpTxConfirmationMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMPduRTpTxConfirmation configured.' */
#define BSWM_INITVALUEOFPDURTPTXCONFIRMATIONMAPPING                                                 STD_OFF  /**< Deactivateable: 'BswM_PduRTpTxConfirmationMapping.InitValue' Reason: 'No Mode Request for BswMPduRTpTxConfirmation configured.' */
#define BSWM_PDURTPTXCONFIRMATIONSTATEIDXOFPDURTPTXCONFIRMATIONMAPPING                              STD_OFF  /**< Deactivateable: 'BswM_PduRTpTxConfirmationMapping.PduRTpTxConfirmationStateIdx' Reason: 'No Mode Request for BswMPduRTpTxConfirmation configured.' */
#define BSWM_PDURTPTXCONFIRMATIONSTATE                                                              STD_OFF  /**< Deactivateable: 'BswM_PduRTpTxConfirmationState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_PDURTRANSMITMAPPING                                                                    STD_OFF  /**< Deactivateable: 'BswM_PduRTransmitMapping' Reason: 'No Mode Request for BswMPduRTransmit configured.' */
#define BSWM_EXTERNALIDOFPDURTRANSMITMAPPING                                                        STD_OFF  /**< Deactivateable: 'BswM_PduRTransmitMapping.ExternalId' Reason: 'No Mode Request for BswMPduRTransmit configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFPDURTRANSMITMAPPING                                               STD_OFF  /**< Deactivateable: 'BswM_PduRTransmitMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMPduRTransmit configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFPDURTRANSMITMAPPING                                             STD_OFF  /**< Deactivateable: 'BswM_PduRTransmitMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMPduRTransmit configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFPDURTRANSMITMAPPING                                                 STD_OFF  /**< Deactivateable: 'BswM_PduRTransmitMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMPduRTransmit configured.' */
#define BSWM_INITVALUEOFPDURTRANSMITMAPPING                                                         STD_OFF  /**< Deactivateable: 'BswM_PduRTransmitMapping.InitValue' Reason: 'No Mode Request for BswMPduRTransmit configured.' */
#define BSWM_PDURTRANSMITSTATEIDXOFPDURTRANSMITMAPPING                                              STD_OFF  /**< Deactivateable: 'BswM_PduRTransmitMapping.PduRTransmitStateIdx' Reason: 'No Mode Request for BswMPduRTransmit configured.' */
#define BSWM_PDURTRANSMITSTATE                                                                      STD_OFF  /**< Deactivateable: 'BswM_PduRTransmitState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_PDURTXCONFIRMATIONMAPPING                                                              STD_OFF  /**< Deactivateable: 'BswM_PduRTxConfirmationMapping' Reason: 'No Mode Request for BswMPduRTxConfirmation configured.' */
#define BSWM_EXTERNALIDOFPDURTXCONFIRMATIONMAPPING                                                  STD_OFF  /**< Deactivateable: 'BswM_PduRTxConfirmationMapping.ExternalId' Reason: 'No Mode Request for BswMPduRTxConfirmation configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFPDURTXCONFIRMATIONMAPPING                                         STD_OFF  /**< Deactivateable: 'BswM_PduRTxConfirmationMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMPduRTxConfirmation configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFPDURTXCONFIRMATIONMAPPING                                       STD_OFF  /**< Deactivateable: 'BswM_PduRTxConfirmationMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMPduRTxConfirmation configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFPDURTXCONFIRMATIONMAPPING                                           STD_OFF  /**< Deactivateable: 'BswM_PduRTxConfirmationMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMPduRTxConfirmation configured.' */
#define BSWM_INITVALUEOFPDURTXCONFIRMATIONMAPPING                                                   STD_OFF  /**< Deactivateable: 'BswM_PduRTxConfirmationMapping.InitValue' Reason: 'No Mode Request for BswMPduRTxConfirmation configured.' */
#define BSWM_PDURTXCONFIRMATIONSTATEIDXOFPDURTXCONFIRMATIONMAPPING                                  STD_OFF  /**< Deactivateable: 'BswM_PduRTxConfirmationMapping.PduRTxConfirmationStateIdx' Reason: 'No Mode Request for BswMPduRTxConfirmation configured.' */
#define BSWM_PDURTXCONFIRMATIONSTATE                                                                STD_OFF  /**< Deactivateable: 'BswM_PduRTxConfirmationState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_QUEUESEMAPHORE                                                                         STD_ON
#define BSWM_QUEUEWRITTEN                                                                           STD_ON
#define BSWM_RULESTATES                                                                             STD_ON
#define BSWM_RULES                                                                                  STD_ON
#define BSWM_FCTPTROFRULES                                                                          STD_ON
#define BSWM_IDOFRULES                                                                              STD_ON
#define BSWM_INITOFRULES                                                                            STD_ON
#define BSWM_RULESTATESIDXOFRULES                                                                   STD_ON
#define BSWM_RULESIND                                                                               STD_ON
#define BSWM_SDCLIENTSERVICEMAPPING                                                                 STD_OFF  /**< Deactivateable: 'BswM_SdClientServiceMapping' Reason: 'No Mode Request for BswMSdClientServiceCurrentState configured.' */
#define BSWM_EXTERNALIDOFSDCLIENTSERVICEMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_SdClientServiceMapping.ExternalId' Reason: 'No Mode Request for BswMSdClientServiceCurrentState configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFSDCLIENTSERVICEMAPPING                                            STD_OFF  /**< Deactivateable: 'BswM_SdClientServiceMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMSdClientServiceCurrentState configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFSDCLIENTSERVICEMAPPING                                          STD_OFF  /**< Deactivateable: 'BswM_SdClientServiceMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMSdClientServiceCurrentState configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFSDCLIENTSERVICEMAPPING                                              STD_OFF  /**< Deactivateable: 'BswM_SdClientServiceMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMSdClientServiceCurrentState configured.' */
#define BSWM_INITVALUEOFSDCLIENTSERVICEMAPPING                                                      STD_OFF  /**< Deactivateable: 'BswM_SdClientServiceMapping.InitValue' Reason: 'No Mode Request for BswMSdClientServiceCurrentState configured.' */
#define BSWM_SDCLIENTSERVICESTATE                                                                   STD_OFF  /**< Deactivateable: 'BswM_SdClientServiceState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_SDCONSUMEDEVENTMAPPING                                                                 STD_OFF  /**< Deactivateable: 'BswM_SdConsumedEventMapping' Reason: 'No Mode Request for BswMSdConsumedEventGroupCurrentState configured.' */
#define BSWM_EXTERNALIDOFSDCONSUMEDEVENTMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_SdConsumedEventMapping.ExternalId' Reason: 'No Mode Request for BswMSdConsumedEventGroupCurrentState configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFSDCONSUMEDEVENTMAPPING                                            STD_OFF  /**< Deactivateable: 'BswM_SdConsumedEventMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMSdConsumedEventGroupCurrentState configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFSDCONSUMEDEVENTMAPPING                                          STD_OFF  /**< Deactivateable: 'BswM_SdConsumedEventMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMSdConsumedEventGroupCurrentState configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFSDCONSUMEDEVENTMAPPING                                              STD_OFF  /**< Deactivateable: 'BswM_SdConsumedEventMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMSdConsumedEventGroupCurrentState configured.' */
#define BSWM_INITVALUEOFSDCONSUMEDEVENTMAPPING                                                      STD_OFF  /**< Deactivateable: 'BswM_SdConsumedEventMapping.InitValue' Reason: 'No Mode Request for BswMSdConsumedEventGroupCurrentState configured.' */
#define BSWM_SDCONSUMEDEVENTSTATE                                                                   STD_OFF  /**< Deactivateable: 'BswM_SdConsumedEventState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_SDEVENTHANDLERMAPPING                                                                  STD_OFF  /**< Deactivateable: 'BswM_SdEventHandlerMapping' Reason: 'No Mode Request for BswMSdEventHandlerCurrentState configured.' */
#define BSWM_EXTERNALIDOFSDEVENTHANDLERMAPPING                                                      STD_OFF  /**< Deactivateable: 'BswM_SdEventHandlerMapping.ExternalId' Reason: 'No Mode Request for BswMSdEventHandlerCurrentState configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFSDEVENTHANDLERMAPPING                                             STD_OFF  /**< Deactivateable: 'BswM_SdEventHandlerMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMSdEventHandlerCurrentState configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFSDEVENTHANDLERMAPPING                                           STD_OFF  /**< Deactivateable: 'BswM_SdEventHandlerMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMSdEventHandlerCurrentState configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFSDEVENTHANDLERMAPPING                                               STD_OFF  /**< Deactivateable: 'BswM_SdEventHandlerMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMSdEventHandlerCurrentState configured.' */
#define BSWM_INITVALUEOFSDEVENTHANDLERMAPPING                                                       STD_OFF  /**< Deactivateable: 'BswM_SdEventHandlerMapping.InitValue' Reason: 'No Mode Request for BswMSdEventHandlerCurrentState configured.' */
#define BSWM_SDEVENTHANDLERSTATE                                                                    STD_OFF  /**< Deactivateable: 'BswM_SdEventHandlerState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_SIZEOFACTIONLISTQUEUE                                                                  STD_ON
#define BSWM_SIZEOFACTIONLISTS                                                                      STD_ON
#define BSWM_SIZEOFCANSMCHANNELMAPPING                                                              STD_ON
#define BSWM_SIZEOFCANSMCHANNELSTATE                                                                STD_ON
#define BSWM_SIZEOFCOMMCHANNELMAPPING                                                               STD_ON
#define BSWM_SIZEOFCOMMCHANNELSTATE                                                                 STD_ON
#define BSWM_SIZEOFDCMCOMMAPPING                                                                    STD_ON
#define BSWM_SIZEOFDCMCOMSTATE                                                                      STD_ON
#define BSWM_SIZEOFDEFERREDRULES                                                                    STD_ON
#define BSWM_SIZEOFGENERICMAPPING                                                                   STD_ON
#define BSWM_SIZEOFGENERICSTATE                                                                     STD_ON
#define BSWM_SIZEOFIMMEDIATEUSER                                                                    STD_ON
#define BSWM_SIZEOFINITGENVARANDINITAL                                                              STD_ON
#define BSWM_SIZEOFJ1939NMMAPPING                                                                   STD_ON
#define BSWM_SIZEOFJ1939NMSTATE                                                                     STD_ON
#define BSWM_SIZEOFLINSMMAPPING                                                                     STD_ON
#define BSWM_SIZEOFLINSMSTATE                                                                       STD_ON
#define BSWM_SIZEOFLINSCHEDULEENDMAPPING                                                            STD_ON
#define BSWM_SIZEOFLINSCHEDULEENDSTATE                                                              STD_ON
#define BSWM_SIZEOFLINSCHEDULEMAPPING                                                               STD_ON
#define BSWM_SIZEOFLINSCHEDULESTATE                                                                 STD_ON
#define BSWM_SIZEOFMODENOTIFICATIONFCT                                                              STD_ON
#define BSWM_SIZEOFMODEREQUESTMAPPING                                                               STD_ON
#define BSWM_SIZEOFMODEREQUESTQUEUE                                                                 STD_ON
#define BSWM_SIZEOFNVMJOBMAPPING                                                                    STD_ON
#define BSWM_SIZEOFNVMJOBSTATE                                                                      STD_ON
#define BSWM_SIZEOFPARTITIONIDENTIFIERS                                                             STD_ON
#define BSWM_SIZEOFRULESTATES                                                                       STD_ON
#define BSWM_SIZEOFRULES                                                                            STD_ON
#define BSWM_SIZEOFRULESIND                                                                         STD_ON
#define BSWM_SIZEOFSWCMODEREQUESTUPDATEFCT                                                          STD_ON
#define BSWM_SIZEOFTIMERSTATE                                                                       STD_ON
#define BSWM_SIZEOFTIMERVALUE                                                                       STD_ON
#define BSWM_SWCMODEREQUESTUPDATEFCT                                                                STD_ON
#define BSWM_TIMERSTATE                                                                             STD_ON
#define BSWM_TIMERVALUE                                                                             STD_ON
#define BSWM_WDGMMAPPING                                                                            STD_OFF  /**< Deactivateable: 'BswM_WdgMMapping' Reason: 'No Mode Request for BswMWdgMRequestPartitionReset configured.' */
#define BSWM_EXTERNALIDOFWDGMMAPPING                                                                STD_OFF  /**< Deactivateable: 'BswM_WdgMMapping.ExternalId' Reason: 'No Mode Request for BswMWdgMRequestPartitionReset configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFWDGMMAPPING                                                       STD_OFF  /**< Deactivateable: 'BswM_WdgMMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMWdgMRequestPartitionReset configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFWDGMMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_WdgMMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMWdgMRequestPartitionReset configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFWDGMMAPPING                                                         STD_OFF  /**< Deactivateable: 'BswM_WdgMMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMWdgMRequestPartitionReset configured.' */
#define BSWM_INITVALUEOFWDGMMAPPING                                                                 STD_OFF  /**< Deactivateable: 'BswM_WdgMMapping.InitValue' Reason: 'No Mode Request for BswMWdgMRequestPartitionReset configured.' */
#define BSWM_WDGMSTATE                                                                              STD_OFF  /**< Deactivateable: 'BswM_WdgMState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_PCCONFIG                                                                               STD_ON
#define BSWM_FINALMAGICNUMBEROFPCCONFIG                                                             STD_OFF  /**< Deactivateable: 'BswM_PCConfig.FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define BSWM_INITDATAHASHCODEOFPCCONFIG                                                             STD_OFF  /**< Deactivateable: 'BswM_PCConfig.InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define BSWM_PCPARTITIONCONFIGOFPCCONFIG                                                            STD_ON
#define BSWM_PARTITIONIDENTIFIERSOFPCCONFIG                                                         STD_ON
#define BSWM_SIZEOFPARTITIONIDENTIFIERSOFPCCONFIG                                                   STD_ON
#define BSWM_PCPARTITIONCONFIG                                                                      STD_ON
#define BSWM_ACTIONLISTQUEUEOFPCPARTITIONCONFIG                                                     STD_ON
#define BSWM_ACTIONLISTSOFPCPARTITIONCONFIG                                                         STD_ON
#define BSWM_CANSMCHANNELMAPPINGOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_CANSMCHANNELSTATEOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_COMMCHANNELMAPPINGOFPCPARTITIONCONFIG                                                  STD_ON
#define BSWM_COMMCHANNELSTATEOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_DCMCOMMAPPINGOFPCPARTITIONCONFIG                                                       STD_ON
#define BSWM_DCMCOMSTATEOFPCPARTITIONCONFIG                                                         STD_ON
#define BSWM_DEFERREDRULESOFPCPARTITIONCONFIG                                                       STD_ON
#define BSWM_FORCEDACTIONLISTPRIORITYOFPCPARTITIONCONFIG                                            STD_ON
#define BSWM_GENERICMAPPINGOFPCPARTITIONCONFIG                                                      STD_ON
#define BSWM_GENERICSTATEOFPCPARTITIONCONFIG                                                        STD_ON
#define BSWM_IMMEDIATEUSEROFPCPARTITIONCONFIG                                                       STD_ON
#define BSWM_INITGENVARANDINITALOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_INITIALIZEDOFPCPARTITIONCONFIG                                                         STD_ON
#define BSWM_J1939NMMAPPINGOFPCPARTITIONCONFIG                                                      STD_ON
#define BSWM_J1939NMSTATEOFPCPARTITIONCONFIG                                                        STD_ON
#define BSWM_LINSMMAPPINGOFPCPARTITIONCONFIG                                                        STD_ON
#define BSWM_LINSMSTATEOFPCPARTITIONCONFIG                                                          STD_ON
#define BSWM_LINSCHEDULEENDMAPPINGOFPCPARTITIONCONFIG                                               STD_ON
#define BSWM_LINSCHEDULEENDSTATEOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_LINSCHEDULEMAPPINGOFPCPARTITIONCONFIG                                                  STD_ON
#define BSWM_LINSCHEDULESTATEOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_MODENOTIFICATIONFCTOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_MODEREQUESTMAPPINGOFPCPARTITIONCONFIG                                                  STD_ON
#define BSWM_MODEREQUESTQUEUEOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_NVMJOBMAPPINGOFPCPARTITIONCONFIG                                                       STD_ON
#define BSWM_NVMJOBSTATEOFPCPARTITIONCONFIG                                                         STD_ON
#define BSWM_QUEUESEMAPHOREOFPCPARTITIONCONFIG                                                      STD_ON
#define BSWM_QUEUEWRITTENOFPCPARTITIONCONFIG                                                        STD_ON
#define BSWM_RULESTATESOFPCPARTITIONCONFIG                                                          STD_ON
#define BSWM_RULESINDOFPCPARTITIONCONFIG                                                            STD_ON
#define BSWM_RULESOFPCPARTITIONCONFIG                                                               STD_ON
#define BSWM_SIZEOFACTIONLISTQUEUEOFPCPARTITIONCONFIG                                               STD_ON
#define BSWM_SIZEOFACTIONLISTSOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_SIZEOFCANSMCHANNELMAPPINGOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_SIZEOFCANSMCHANNELSTATEOFPCPARTITIONCONFIG                                             STD_ON
#define BSWM_SIZEOFCOMMCHANNELMAPPINGOFPCPARTITIONCONFIG                                            STD_ON
#define BSWM_SIZEOFCOMMCHANNELSTATEOFPCPARTITIONCONFIG                                              STD_ON
#define BSWM_SIZEOFDCMCOMMAPPINGOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_SIZEOFDCMCOMSTATEOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_SIZEOFDEFERREDRULESOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_SIZEOFGENERICMAPPINGOFPCPARTITIONCONFIG                                                STD_ON
#define BSWM_SIZEOFGENERICSTATEOFPCPARTITIONCONFIG                                                  STD_ON
#define BSWM_SIZEOFIMMEDIATEUSEROFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_SIZEOFINITGENVARANDINITALOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_SIZEOFJ1939NMMAPPINGOFPCPARTITIONCONFIG                                                STD_ON
#define BSWM_SIZEOFJ1939NMSTATEOFPCPARTITIONCONFIG                                                  STD_ON
#define BSWM_SIZEOFLINSMMAPPINGOFPCPARTITIONCONFIG                                                  STD_ON
#define BSWM_SIZEOFLINSMSTATEOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_SIZEOFLINSCHEDULEENDMAPPINGOFPCPARTITIONCONFIG                                         STD_ON
#define BSWM_SIZEOFLINSCHEDULEENDSTATEOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_SIZEOFLINSCHEDULEMAPPINGOFPCPARTITIONCONFIG                                            STD_ON
#define BSWM_SIZEOFLINSCHEDULESTATEOFPCPARTITIONCONFIG                                              STD_ON
#define BSWM_SIZEOFMODENOTIFICATIONFCTOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_SIZEOFMODEREQUESTMAPPINGOFPCPARTITIONCONFIG                                            STD_ON
#define BSWM_SIZEOFMODEREQUESTQUEUEOFPCPARTITIONCONFIG                                              STD_ON
#define BSWM_SIZEOFNVMJOBMAPPINGOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_SIZEOFNVMJOBSTATEOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_SIZEOFRULESTATESOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_SIZEOFRULESINDOFPCPARTITIONCONFIG                                                      STD_ON
#define BSWM_SIZEOFRULESOFPCPARTITIONCONFIG                                                         STD_ON
#define BSWM_SIZEOFSWCMODEREQUESTUPDATEFCTOFPCPARTITIONCONFIG                                       STD_ON
#define BSWM_SIZEOFTIMERSTATEOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_SIZEOFTIMERVALUEOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_SWCMODEREQUESTUPDATEFCTOFPCPARTITIONCONFIG                                             STD_ON
#define BSWM_TIMERSTATEOFPCPARTITIONCONFIG                                                          STD_ON
#define BSWM_TIMERVALUEOFPCPARTITIONCONFIG                                                          STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCMinNumericValueDefines  BswM Min Numeric Value Defines (PRE_COMPILE)
  \brief  These defines are used to implement against the minimum value in numerical based data.
  \{
*/ 
#define BSWM_MIN_ACTIONLISTQUEUE                                                                    0u
#define BSWM_MIN_FORCEDACTIONLISTPRIORITY                                                           0u
#define BSWM_MIN_MODEREQUESTQUEUE                                                                   0u
#define BSWM_MIN_QUEUESEMAPHORE                                                                     0u
#define BSWM_MIN_RULESTATES                                                                         0u
#define BSWM_MIN_TIMERSTATE                                                                         0u
#define BSWM_MIN_TIMERVALUE                                                                         0u
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCMaxNumericValueDefines  BswM Max Numeric Value Defines (PRE_COMPILE)
  \brief  These defines are used to implement against the maximum value in numerical based data.
  \{
*/ 
#define BSWM_MAX_ACTIONLISTQUEUE                                                                    255u
#define BSWM_MAX_FORCEDACTIONLISTPRIORITY                                                           255u
#define BSWM_MAX_MODEREQUESTQUEUE                                                                   255u
#define BSWM_MAX_QUEUESEMAPHORE                                                                     255u
#define BSWM_MAX_RULESTATES                                                                         255u
#define BSWM_MAX_TIMERSTATE                                                                         255u
#define BSWM_MAX_TIMERVALUE                                                                         4294967295u
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCNoReferenceDefines  BswM No Reference Defines (PRE_COMPILE)
  \brief  These defines are used to indicate unused indexes in data relations.
  \{
*/ 
#define BSWM_NO_IMMEDIATEUSERENDIDXOFCANSMCHANNELMAPPING                                            255u
#define BSWM_NO_IMMEDIATEUSERSTARTIDXOFCANSMCHANNELMAPPING                                          255u
#define BSWM_NO_IMMEDIATEUSERENDIDXOFDCMCOMMAPPING                                                  255u
#define BSWM_NO_IMMEDIATEUSERSTARTIDXOFDCMCOMMAPPING                                                255u
#define BSWM_NO_IMMEDIATEUSERENDIDXOFGENERICMAPPING                                                 255u
#define BSWM_NO_IMMEDIATEUSERSTARTIDXOFGENERICMAPPING                                               255u
#define BSWM_NO_RULESINDENDIDXOFIMMEDIATEUSER                                                       255u
#define BSWM_NO_RULESINDSTARTIDXOFIMMEDIATEUSER                                                     255u
#define BSWM_NO_IMMEDIATEUSERENDIDXOFJ1939NMMAPPING                                                 255u
#define BSWM_NO_IMMEDIATEUSERSTARTIDXOFJ1939NMMAPPING                                               255u
#define BSWM_NO_IMMEDIATEUSERENDIDXOFLINSMMAPPING                                                   255u
#define BSWM_NO_IMMEDIATEUSERSTARTIDXOFLINSMMAPPING                                                 255u
#define BSWM_NO_IMMEDIATEUSERENDIDXOFLINSCHEDULEENDMAPPING                                          255u
#define BSWM_NO_IMMEDIATEUSERSTARTIDXOFLINSCHEDULEENDMAPPING                                        255u
#define BSWM_NO_IMMEDIATEUSERENDIDXOFLINSCHEDULEMAPPING                                             255u
#define BSWM_NO_IMMEDIATEUSERSTARTIDXOFLINSCHEDULEMAPPING                                           255u
#define BSWM_NO_IMMEDIATEUSERENDIDXOFMODEREQUESTMAPPING                                             255u
#define BSWM_NO_IMMEDIATEUSERSTARTIDXOFMODEREQUESTMAPPING                                           255u
#define BSWM_NO_IDOFRULES                                                                           255u
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCMaskedBitDefines  BswM Masked Bit Defines (PRE_COMPILE)
  \brief  These defines are masks to extract packed boolean data.
  \{
*/ 
#define BSWM_ONINITOFIMMEDIATEUSER_MASK                                                             0x02u
#define BSWM_RULESINDUSEDOFIMMEDIATEUSER_MASK                                                       0x01u
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCIsReducedToDefineDefines  BswM Is Reduced To Define Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define is STD_ON else STD_OFF.
  \{
*/ 
#define BSWM_ISDEF_FCTPTROFACTIONLISTS                                                              STD_OFF
#define BSWM_ISDEF_EXTERNALIDOFCANSMCHANNELMAPPING                                                  STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERENDIDXOFCANSMCHANNELMAPPING                                         STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERSTARTIDXOFCANSMCHANNELMAPPING                                       STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERUSEDOFCANSMCHANNELMAPPING                                           STD_ON
#define BSWM_ISDEF_INITVALUEOFCANSMCHANNELMAPPING                                                   STD_ON
#define BSWM_ISDEF_EXTERNALIDOFCOMMCHANNELMAPPING                                                   STD_OFF
#define BSWM_ISDEF_INITVALUEOFCOMMCHANNELMAPPING                                                    STD_ON
#define BSWM_ISDEF_EXTERNALIDOFDCMCOMMAPPING                                                        STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERENDIDXOFDCMCOMMAPPING                                               STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERSTARTIDXOFDCMCOMMAPPING                                             STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERUSEDOFDCMCOMMAPPING                                                 STD_ON
#define BSWM_ISDEF_INITVALUEOFDCMCOMMAPPING                                                         STD_ON
#define BSWM_ISDEF_RULESIDXOFDEFERREDRULES                                                          STD_OFF
#define BSWM_ISDEF_EXTERNALIDOFGENERICMAPPING                                                       STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERENDIDXOFGENERICMAPPING                                              STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERSTARTIDXOFGENERICMAPPING                                            STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERUSEDOFGENERICMAPPING                                                STD_OFF
#define BSWM_ISDEF_INITVALUEOFGENERICMAPPING                                                        STD_OFF
#define BSWM_ISDEF_MASKEDBITSOFIMMEDIATEUSER                                                        STD_OFF
#define BSWM_ISDEF_ONINITOFIMMEDIATEUSER                                                            STD_OFF
#define BSWM_ISDEF_RULESINDENDIDXOFIMMEDIATEUSER                                                    STD_OFF
#define BSWM_ISDEF_RULESINDSTARTIDXOFIMMEDIATEUSER                                                  STD_OFF
#define BSWM_ISDEF_RULESINDUSEDOFIMMEDIATEUSER                                                      STD_OFF
#define BSWM_ISDEF_INITGENVARANDINITAL                                                              STD_OFF
#define BSWM_ISDEF_EXTERNALIDOFJ1939NMMAPPING                                                       STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERENDIDXOFJ1939NMMAPPING                                              STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERSTARTIDXOFJ1939NMMAPPING                                            STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERUSEDOFJ1939NMMAPPING                                                STD_ON
#define BSWM_ISDEF_INITVALUEOFJ1939NMMAPPING                                                        STD_ON
#define BSWM_ISDEF_EXTERNALIDOFLINSMMAPPING                                                         STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERENDIDXOFLINSMMAPPING                                                STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERSTARTIDXOFLINSMMAPPING                                              STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERUSEDOFLINSMMAPPING                                                  STD_ON
#define BSWM_ISDEF_INITVALUEOFLINSMMAPPING                                                          STD_ON
#define BSWM_ISDEF_EXTERNALIDOFLINSCHEDULEENDMAPPING                                                STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERENDIDXOFLINSCHEDULEENDMAPPING                                       STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERSTARTIDXOFLINSCHEDULEENDMAPPING                                     STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERUSEDOFLINSCHEDULEENDMAPPING                                         STD_ON
#define BSWM_ISDEF_INITVALUEOFLINSCHEDULEENDMAPPING                                                 STD_ON
#define BSWM_ISDEF_EXTERNALIDOFLINSCHEDULEMAPPING                                                   STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERENDIDXOFLINSCHEDULEMAPPING                                          STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERSTARTIDXOFLINSCHEDULEMAPPING                                        STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERUSEDOFLINSCHEDULEMAPPING                                            STD_ON
#define BSWM_ISDEF_INITVALUEOFLINSCHEDULEMAPPING                                                    STD_ON
#define BSWM_ISDEF_MODENOTIFICATIONFCT                                                              STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERENDIDXOFMODEREQUESTMAPPING                                          STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERSTARTIDXOFMODEREQUESTMAPPING                                        STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERUSEDOFMODEREQUESTMAPPING                                            STD_OFF
#define BSWM_ISDEF_EXTERNALIDOFNVMJOBMAPPING                                                        STD_ON
#define BSWM_ISDEF_INITVALUEOFNVMJOBMAPPING                                                         STD_ON
#define BSWM_ISDEF_PCPARTITIONCONFIGIDXOFPARTITIONIDENTIFIERS                                       STD_OFF
#define BSWM_ISDEF_PARTITIONSNVOFPARTITIONIDENTIFIERS                                               STD_OFF
#define BSWM_ISDEF_FCTPTROFRULES                                                                    STD_OFF
#define BSWM_ISDEF_IDOFRULES                                                                        STD_OFF
#define BSWM_ISDEF_INITOFRULES                                                                      STD_ON
#define BSWM_ISDEF_RULESTATESIDXOFRULES                                                             STD_OFF
#define BSWM_ISDEF_RULESIND                                                                         STD_OFF
#define BSWM_ISDEF_SWCMODEREQUESTUPDATEFCT                                                          STD_OFF
#define BSWM_ISDEF_PCPARTITIONCONFIGOFPCCONFIG                                                      STD_ON
#define BSWM_ISDEF_PARTITIONIDENTIFIERSOFPCCONFIG                                                   STD_ON
#define BSWM_ISDEF_ACTIONLISTQUEUEOFPCPARTITIONCONFIG                                               STD_ON
#define BSWM_ISDEF_ACTIONLISTSOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_ISDEF_CANSMCHANNELMAPPINGOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_ISDEF_CANSMCHANNELSTATEOFPCPARTITIONCONFIG                                             STD_ON
#define BSWM_ISDEF_COMMCHANNELMAPPINGOFPCPARTITIONCONFIG                                            STD_ON
#define BSWM_ISDEF_COMMCHANNELSTATEOFPCPARTITIONCONFIG                                              STD_ON
#define BSWM_ISDEF_DCMCOMMAPPINGOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_ISDEF_DCMCOMSTATEOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_ISDEF_DEFERREDRULESOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_ISDEF_FORCEDACTIONLISTPRIORITYOFPCPARTITIONCONFIG                                      STD_ON
#define BSWM_ISDEF_GENERICMAPPINGOFPCPARTITIONCONFIG                                                STD_ON
#define BSWM_ISDEF_GENERICSTATEOFPCPARTITIONCONFIG                                                  STD_ON
#define BSWM_ISDEF_IMMEDIATEUSEROFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_ISDEF_INITGENVARANDINITALOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_ISDEF_INITIALIZEDOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_ISDEF_J1939NMMAPPINGOFPCPARTITIONCONFIG                                                STD_ON
#define BSWM_ISDEF_J1939NMSTATEOFPCPARTITIONCONFIG                                                  STD_ON
#define BSWM_ISDEF_LINSMMAPPINGOFPCPARTITIONCONFIG                                                  STD_ON
#define BSWM_ISDEF_LINSMSTATEOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_ISDEF_LINSCHEDULEENDMAPPINGOFPCPARTITIONCONFIG                                         STD_ON
#define BSWM_ISDEF_LINSCHEDULEENDSTATEOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_ISDEF_LINSCHEDULEMAPPINGOFPCPARTITIONCONFIG                                            STD_ON
#define BSWM_ISDEF_LINSCHEDULESTATEOFPCPARTITIONCONFIG                                              STD_ON
#define BSWM_ISDEF_MODENOTIFICATIONFCTOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_ISDEF_MODEREQUESTMAPPINGOFPCPARTITIONCONFIG                                            STD_ON
#define BSWM_ISDEF_MODEREQUESTQUEUEOFPCPARTITIONCONFIG                                              STD_ON
#define BSWM_ISDEF_NVMJOBMAPPINGOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_ISDEF_NVMJOBSTATEOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_ISDEF_QUEUESEMAPHOREOFPCPARTITIONCONFIG                                                STD_ON
#define BSWM_ISDEF_QUEUEWRITTENOFPCPARTITIONCONFIG                                                  STD_ON
#define BSWM_ISDEF_RULESTATESOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_ISDEF_RULESINDOFPCPARTITIONCONFIG                                                      STD_ON
#define BSWM_ISDEF_RULESOFPCPARTITIONCONFIG                                                         STD_ON
#define BSWM_ISDEF_SWCMODEREQUESTUPDATEFCTOFPCPARTITIONCONFIG                                       STD_ON
#define BSWM_ISDEF_TIMERSTATEOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_ISDEF_TIMERVALUEOFPCPARTITIONCONFIG                                                    STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCEqualsAlwaysToDefines  BswM Equals Always To Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define contains the always equals value.
  \{
*/ 
#define BSWM_EQ2_FCTPTROFACTIONLISTS                                                                
#define BSWM_EQ2_EXTERNALIDOFCANSMCHANNELMAPPING                                                    
#define BSWM_EQ2_IMMEDIATEUSERENDIDXOFCANSMCHANNELMAPPING                                           
#define BSWM_EQ2_IMMEDIATEUSERSTARTIDXOFCANSMCHANNELMAPPING                                         
#define BSWM_EQ2_IMMEDIATEUSERUSEDOFCANSMCHANNELMAPPING                                             TRUE
#define BSWM_EQ2_INITVALUEOFCANSMCHANNELMAPPING                                                     CANSM_BSWM_NO_COMMUNICATION
#define BSWM_EQ2_EXTERNALIDOFCOMMCHANNELMAPPING                                                     
#define BSWM_EQ2_INITVALUEOFCOMMCHANNELMAPPING                                                      COMM_NO_COMMUNICATION
#define BSWM_EQ2_EXTERNALIDOFDCMCOMMAPPING                                                          
#define BSWM_EQ2_IMMEDIATEUSERENDIDXOFDCMCOMMAPPING                                                 
#define BSWM_EQ2_IMMEDIATEUSERSTARTIDXOFDCMCOMMAPPING                                               
#define BSWM_EQ2_IMMEDIATEUSERUSEDOFDCMCOMMAPPING                                                   TRUE
#define BSWM_EQ2_INITVALUEOFDCMCOMMAPPING                                                           DCM_ENABLE_RX_TX_NORM
#define BSWM_EQ2_RULESIDXOFDEFERREDRULES                                                            
#define BSWM_EQ2_EXTERNALIDOFGENERICMAPPING                                                         
#define BSWM_EQ2_IMMEDIATEUSERENDIDXOFGENERICMAPPING                                                
#define BSWM_EQ2_IMMEDIATEUSERSTARTIDXOFGENERICMAPPING                                              
#define BSWM_EQ2_IMMEDIATEUSERUSEDOFGENERICMAPPING                                                  
#define BSWM_EQ2_INITVALUEOFGENERICMAPPING                                                          
#define BSWM_EQ2_MASKEDBITSOFIMMEDIATEUSER                                                          
#define BSWM_EQ2_ONINITOFIMMEDIATEUSER                                                              
#define BSWM_EQ2_RULESINDENDIDXOFIMMEDIATEUSER                                                      
#define BSWM_EQ2_RULESINDSTARTIDXOFIMMEDIATEUSER                                                    
#define BSWM_EQ2_RULESINDUSEDOFIMMEDIATEUSER                                                        
#define BSWM_EQ2_INITGENVARANDINITAL                                                                
#define BSWM_EQ2_EXTERNALIDOFJ1939NMMAPPING                                                         
#define BSWM_EQ2_IMMEDIATEUSERENDIDXOFJ1939NMMAPPING                                                
#define BSWM_EQ2_IMMEDIATEUSERSTARTIDXOFJ1939NMMAPPING                                              
#define BSWM_EQ2_IMMEDIATEUSERUSEDOFJ1939NMMAPPING                                                  TRUE
#define BSWM_EQ2_INITVALUEOFJ1939NMMAPPING                                                          NM_STATE_BUS_SLEEP
#define BSWM_EQ2_EXTERNALIDOFLINSMMAPPING                                                           
#define BSWM_EQ2_IMMEDIATEUSERENDIDXOFLINSMMAPPING                                                  
#define BSWM_EQ2_IMMEDIATEUSERSTARTIDXOFLINSMMAPPING                                                
#define BSWM_EQ2_IMMEDIATEUSERUSEDOFLINSMMAPPING                                                    TRUE
#define BSWM_EQ2_INITVALUEOFLINSMMAPPING                                                            LINSM_BSWM_NO_COM
#define BSWM_EQ2_EXTERNALIDOFLINSCHEDULEENDMAPPING                                                  
#define BSWM_EQ2_IMMEDIATEUSERENDIDXOFLINSCHEDULEENDMAPPING                                         
#define BSWM_EQ2_IMMEDIATEUSERSTARTIDXOFLINSCHEDULEENDMAPPING                                       
#define BSWM_EQ2_IMMEDIATEUSERUSEDOFLINSCHEDULEENDMAPPING                                           TRUE
#define BSWM_EQ2_INITVALUEOFLINSCHEDULEENDMAPPING                                                   0
#define BSWM_EQ2_EXTERNALIDOFLINSCHEDULEMAPPING                                                     
#define BSWM_EQ2_IMMEDIATEUSERENDIDXOFLINSCHEDULEMAPPING                                            
#define BSWM_EQ2_IMMEDIATEUSERSTARTIDXOFLINSCHEDULEMAPPING                                          
#define BSWM_EQ2_IMMEDIATEUSERUSEDOFLINSCHEDULEMAPPING                                              TRUE
#define BSWM_EQ2_INITVALUEOFLINSCHEDULEMAPPING                                                      0
#define BSWM_EQ2_MODENOTIFICATIONFCT                                                                
#define BSWM_EQ2_IMMEDIATEUSERENDIDXOFMODEREQUESTMAPPING                                            
#define BSWM_EQ2_IMMEDIATEUSERSTARTIDXOFMODEREQUESTMAPPING                                          
#define BSWM_EQ2_IMMEDIATEUSERUSEDOFMODEREQUESTMAPPING                                              
#define BSWM_EQ2_EXTERNALIDOFNVMJOBMAPPING                                                          NVM_SERVICE_ID_WRITEALL
#define BSWM_EQ2_INITVALUEOFNVMJOBMAPPING                                                           NVM_REQ_OK
#define BSWM_EQ2_PCPARTITIONCONFIGIDXOFPARTITIONIDENTIFIERS                                         
#define BSWM_EQ2_PARTITIONSNVOFPARTITIONIDENTIFIERS                                                 
#define BSWM_EQ2_FCTPTROFRULES                                                                      
#define BSWM_EQ2_IDOFRULES                                                                          
#define BSWM_EQ2_INITOFRULES                                                                        BSWM_FALSE
#define BSWM_EQ2_RULESTATESIDXOFRULES                                                               
#define BSWM_EQ2_RULESIND                                                                           
#define BSWM_EQ2_SWCMODEREQUESTUPDATEFCT                                                            
#define BSWM_EQ2_PCPARTITIONCONFIGOFPCCONFIG                                                        BswM_PCPartitionConfig
#define BSWM_EQ2_PARTITIONIDENTIFIERSOFPCCONFIG                                                     BswM_PartitionIdentifiers
#define BSWM_EQ2_ACTIONLISTQUEUEOFPCPARTITIONCONFIG                                                 BswM_ActionListQueue.raw
#define BSWM_EQ2_ACTIONLISTSOFPCPARTITIONCONFIG                                                     BswM_ActionLists
#define BSWM_EQ2_CANSMCHANNELMAPPINGOFPCPARTITIONCONFIG                                             BswM_CanSMChannelMapping
#define BSWM_EQ2_CANSMCHANNELSTATEOFPCPARTITIONCONFIG                                               BswM_CanSMChannelState
#define BSWM_EQ2_COMMCHANNELMAPPINGOFPCPARTITIONCONFIG                                              BswM_ComMChannelMapping
#define BSWM_EQ2_COMMCHANNELSTATEOFPCPARTITIONCONFIG                                                BswM_ComMChannelState
#define BSWM_EQ2_DCMCOMMAPPINGOFPCPARTITIONCONFIG                                                   BswM_DcmComMapping
#define BSWM_EQ2_DCMCOMSTATEOFPCPARTITIONCONFIG                                                     BswM_DcmComState
#define BSWM_EQ2_DEFERREDRULESOFPCPARTITIONCONFIG                                                   BswM_DeferredRules
#define BSWM_EQ2_FORCEDACTIONLISTPRIORITYOFPCPARTITIONCONFIG                                        (&(BswM_ForcedActionListPriority))
#define BSWM_EQ2_GENERICMAPPINGOFPCPARTITIONCONFIG                                                  BswM_GenericMapping
#define BSWM_EQ2_GENERICSTATEOFPCPARTITIONCONFIG                                                    BswM_GenericState
#define BSWM_EQ2_IMMEDIATEUSEROFPCPARTITIONCONFIG                                                   BswM_ImmediateUser
#define BSWM_EQ2_INITGENVARANDINITALOFPCPARTITIONCONFIG                                             BswM_InitGenVarAndInitAL
#define BSWM_EQ2_INITIALIZEDOFPCPARTITIONCONFIG                                                     (&(BswM_Initialized))
#define BSWM_EQ2_J1939NMMAPPINGOFPCPARTITIONCONFIG                                                  BswM_J1939NmMapping
#define BSWM_EQ2_J1939NMSTATEOFPCPARTITIONCONFIG                                                    BswM_J1939NmState
#define BSWM_EQ2_LINSMMAPPINGOFPCPARTITIONCONFIG                                                    BswM_LinSMMapping
#define BSWM_EQ2_LINSMSTATEOFPCPARTITIONCONFIG                                                      BswM_LinSMState
#define BSWM_EQ2_LINSCHEDULEENDMAPPINGOFPCPARTITIONCONFIG                                           BswM_LinScheduleEndMapping
#define BSWM_EQ2_LINSCHEDULEENDSTATEOFPCPARTITIONCONFIG                                             BswM_LinScheduleEndState
#define BSWM_EQ2_LINSCHEDULEMAPPINGOFPCPARTITIONCONFIG                                              BswM_LinScheduleMapping
#define BSWM_EQ2_LINSCHEDULESTATEOFPCPARTITIONCONFIG                                                BswM_LinScheduleState
#define BSWM_EQ2_MODENOTIFICATIONFCTOFPCPARTITIONCONFIG                                             BswM_ModeNotificationFct
#define BSWM_EQ2_MODEREQUESTMAPPINGOFPCPARTITIONCONFIG                                              BswM_ModeRequestMapping
#define BSWM_EQ2_MODEREQUESTQUEUEOFPCPARTITIONCONFIG                                                BswM_ModeRequestQueue
#define BSWM_EQ2_NVMJOBMAPPINGOFPCPARTITIONCONFIG                                                   BswM_NvMJobMapping
#define BSWM_EQ2_NVMJOBSTATEOFPCPARTITIONCONFIG                                                     BswM_NvMJobState
#define BSWM_EQ2_QUEUESEMAPHOREOFPCPARTITIONCONFIG                                                  (&(BswM_QueueSemaphore))
#define BSWM_EQ2_QUEUEWRITTENOFPCPARTITIONCONFIG                                                    (&(BswM_QueueWritten))
#define BSWM_EQ2_RULESTATESOFPCPARTITIONCONFIG                                                      BswM_RuleStates.raw
#define BSWM_EQ2_RULESINDOFPCPARTITIONCONFIG                                                        BswM_RulesInd
#define BSWM_EQ2_RULESOFPCPARTITIONCONFIG                                                           BswM_Rules
#define BSWM_EQ2_SWCMODEREQUESTUPDATEFCTOFPCPARTITIONCONFIG                                         BswM_SwcModeRequestUpdateFct
#define BSWM_EQ2_TIMERSTATEOFPCPARTITIONCONFIG                                                      BswM_TimerState.raw
#define BSWM_EQ2_TIMERVALUEOFPCPARTITIONCONFIG                                                      BswM_TimerValue.raw
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCSymbolicInitializationPointers  BswM Symbolic Initialization Pointers (PRE_COMPILE)
  \brief  Symbolic initialization pointers to be used in the call of a preinit or init function.
  \{
*/ 
#define BswM_Config_Ptr                                                                             NULL_PTR  /**< symbolic identifier which shall be used to initialize 'BswM' */
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCInitializationSymbols  BswM Initialization Symbols (PRE_COMPILE)
  \brief  Symbolic initialization pointers which may be used in the call of a preinit or init function. Please note, that the defined value can be a 'NULL_PTR' and the address operator is not usable.
  \{
*/ 
#define BswM_Config                                                                                 NULL_PTR  /**< symbolic identifier which could be used to initialize 'BswM */
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCGeneral  BswM General (PRE_COMPILE)
  \brief  General constant defines not associated with a group of defines.
  \{
*/ 
#define BSWM_CHECK_INIT_POINTER                                                                     STD_OFF  /**< STD_ON if the init pointer shall not be used as NULL_PTR and a check shall validate this. */
#define BSWM_FINAL_MAGIC_NUMBER                                                                     0x2A1Eu  /**< the precompile constant to validate the size of the initialization structure at initialization time of BswM */
#define BSWM_INDIVIDUAL_POSTBUILD                                                                   STD_OFF  /**< the precompile constant to check, that the module is individual postbuildable. The module 'BswM' is not configured to be postbuild capable. */
#define BSWM_INIT_DATA                                                                              BSWM_CONST  /**< CompilerMemClassDefine for the initialization data. */
#define BSWM_INIT_DATA_HASH_CODE                                                                    -1519484662  /**< the precompile constant to validate the initialization structure at initialization time of BswM with a hashcode. The seed value is '0x2A1Eu' */
#define BSWM_USE_ECUM_BSW_ERROR_HOOK                                                                STD_OFF  /**< STD_ON if the EcuM_BswErrorHook shall be called in the ConfigPtr check. */
#define BSWM_USE_INIT_POINTER                                                                       STD_OFF  /**< STD_ON if the init pointer BswM shall be used. */
/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  BswMPBDataSwitches  BswM Data Switches  (POST_BUILD)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define BSWM_PBCONFIG                                                                               STD_OFF  /**< Deactivateable: 'BswM_PBConfig' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define BSWM_LTCONFIGIDXOFPBCONFIG                                                                  STD_OFF  /**< Deactivateable: 'BswM_PBConfig.LTConfigIdx' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define BSWM_PBPARTITIONCONFIGOFPBCONFIG                                                            STD_OFF  /**< Deactivateable: 'BswM_PBConfig.PBPartitionConfig' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define BSWM_PCCONFIGIDXOFPBCONFIG                                                                  STD_OFF  /**< Deactivateable: 'BswM_PBConfig.PCConfigIdx' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define BSWM_PBPARTITIONCONFIG                                                                      STD_OFF  /**< Deactivateable: 'BswM_PBPartitionConfig' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
/** 
  \}
*/ 



/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/* PRQA S 0639, 0779 PRECOMPILEGLOBALDATATYPES */ /* MD_MSR_1.1_639, MD_MSR_Rule5.2_0779 */
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  BswMPCIterableTypes  BswM Iterable Types (PRE_COMPILE)
  \brief  These type definitions are used to iterate over an array with least processor cycles for variable access as possible.
  \{
*/ 
/**   \brief  type used to iterate BswM_ActionLists */
typedef uint8_least BswM_ActionListsIterType;

/**   \brief  type used to iterate BswM_CanSMChannelMapping */
typedef uint8_least BswM_CanSMChannelMappingIterType;

/**   \brief  type used to iterate BswM_CanSMChannelState */
typedef uint8_least BswM_CanSMChannelStateIterType;

/**   \brief  type used to iterate BswM_ComMChannelMapping */
typedef uint8_least BswM_ComMChannelMappingIterType;

/**   \brief  type used to iterate BswM_ComMChannelState */
typedef uint8_least BswM_ComMChannelStateIterType;

/**   \brief  type used to iterate BswM_DcmComMapping */
typedef uint8_least BswM_DcmComMappingIterType;

/**   \brief  type used to iterate BswM_DcmComState */
typedef uint8_least BswM_DcmComStateIterType;

/**   \brief  type used to iterate BswM_DeferredRules */
typedef uint8_least BswM_DeferredRulesIterType;

/**   \brief  type used to iterate BswM_GenericMapping */
typedef uint8_least BswM_GenericMappingIterType;

/**   \brief  type used to iterate BswM_GenericState */
typedef uint8_least BswM_GenericStateIterType;

/**   \brief  type used to iterate BswM_ImmediateUser */
typedef uint8_least BswM_ImmediateUserIterType;

/**   \brief  type used to iterate BswM_InitGenVarAndInitAL */
typedef uint8_least BswM_InitGenVarAndInitALIterType;

/**   \brief  type used to iterate BswM_J1939NmMapping */
typedef uint8_least BswM_J1939NmMappingIterType;

/**   \brief  type used to iterate BswM_J1939NmState */
typedef uint8_least BswM_J1939NmStateIterType;

/**   \brief  type used to iterate BswM_LinSMMapping */
typedef uint8_least BswM_LinSMMappingIterType;

/**   \brief  type used to iterate BswM_LinSMState */
typedef uint8_least BswM_LinSMStateIterType;

/**   \brief  type used to iterate BswM_LinScheduleEndMapping */
typedef uint8_least BswM_LinScheduleEndMappingIterType;

/**   \brief  type used to iterate BswM_LinScheduleEndState */
typedef uint8_least BswM_LinScheduleEndStateIterType;

/**   \brief  type used to iterate BswM_LinScheduleMapping */
typedef uint8_least BswM_LinScheduleMappingIterType;

/**   \brief  type used to iterate BswM_LinScheduleState */
typedef uint8_least BswM_LinScheduleStateIterType;

/**   \brief  type used to iterate BswM_ModeNotificationFct */
typedef uint8_least BswM_ModeNotificationFctIterType;

/**   \brief  type used to iterate BswM_ModeRequestMapping */
typedef uint8_least BswM_ModeRequestMappingIterType;

/**   \brief  type used to iterate BswM_NvMJobMapping */
typedef uint8_least BswM_NvMJobMappingIterType;

/**   \brief  type used to iterate BswM_NvMJobState */
typedef uint8_least BswM_NvMJobStateIterType;

/**   \brief  type used to iterate BswM_PartitionIdentifiers */
typedef uint8_least BswM_PartitionIdentifiersIterType;

/**   \brief  type used to iterate BswM_RuleStates */
typedef uint8_least BswM_RuleStatesIterType;

/**   \brief  type used to iterate BswM_Rules */
typedef uint8_least BswM_RulesIterType;

/**   \brief  type used to iterate BswM_RulesInd */
typedef uint8_least BswM_RulesIndIterType;

/**   \brief  type used to iterate BswM_SwcModeRequestUpdateFct */
typedef uint8_least BswM_SwcModeRequestUpdateFctIterType;

/**   \brief  type used to iterate BswM_TimerState */
typedef uint8_least BswM_TimerStateIterType;

/**   \brief  type used to iterate BswM_TimerValue */
typedef uint8_least BswM_TimerValueIterType;

/**   \brief  type used to iterate BswM_PCPartitionConfig */
typedef uint8_least BswM_PCPartitionConfigIterType;

/** 
  \}
*/ 

/** 
  \defgroup  BswMPCIterableTypesWithSizeRelations  BswM Iterable Types With Size Relations (PRE_COMPILE)
  \brief  These type definitions are used to iterate over a VAR based array with the same iterator as the related CONST array.
  \{
*/ 
/**   \brief  type used to iterate BswM_ActionListQueue */
typedef BswM_ActionListsIterType BswM_ActionListQueueIterType;

/**   \brief  type used to iterate BswM_ModeRequestQueue */
typedef BswM_ImmediateUserIterType BswM_ModeRequestQueueIterType;

/** 
  \}
*/ 

/** 
  \defgroup  BswMPCValueTypes  BswM Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value based data representations.
  \{
*/ 
/**   \brief  value based type definition for BswM_ActionListQueue */
typedef uint8 BswM_ActionListQueueType;

/**   \brief  value based type definition for BswM_ExternalIdOfCanSMChannelMapping */
typedef uint32 BswM_ExternalIdOfCanSMChannelMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserEndIdxOfCanSMChannelMapping */
typedef uint8 BswM_ImmediateUserEndIdxOfCanSMChannelMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserStartIdxOfCanSMChannelMapping */
typedef uint8 BswM_ImmediateUserStartIdxOfCanSMChannelMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserUsedOfCanSMChannelMapping */
typedef boolean BswM_ImmediateUserUsedOfCanSMChannelMappingType;

/**   \brief  value based type definition for BswM_ExternalIdOfComMChannelMapping */
typedef uint32 BswM_ExternalIdOfComMChannelMappingType;

/**   \brief  value based type definition for BswM_ExternalIdOfDcmComMapping */
typedef uint32 BswM_ExternalIdOfDcmComMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserEndIdxOfDcmComMapping */
typedef uint8 BswM_ImmediateUserEndIdxOfDcmComMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserStartIdxOfDcmComMapping */
typedef uint8 BswM_ImmediateUserStartIdxOfDcmComMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserUsedOfDcmComMapping */
typedef boolean BswM_ImmediateUserUsedOfDcmComMappingType;

/**   \brief  value based type definition for BswM_RulesIdxOfDeferredRules */
typedef uint8 BswM_RulesIdxOfDeferredRulesType;

/**   \brief  value based type definition for BswM_ForcedActionListPriority */
typedef uint8 BswM_ForcedActionListPriorityType;

/**   \brief  value based type definition for BswM_ExternalIdOfGenericMapping */
typedef uint32 BswM_ExternalIdOfGenericMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserEndIdxOfGenericMapping */
typedef uint8 BswM_ImmediateUserEndIdxOfGenericMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserStartIdxOfGenericMapping */
typedef uint8 BswM_ImmediateUserStartIdxOfGenericMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserUsedOfGenericMapping */
typedef boolean BswM_ImmediateUserUsedOfGenericMappingType;

/**   \brief  value based type definition for BswM_MaskedBitsOfImmediateUser */
typedef uint8 BswM_MaskedBitsOfImmediateUserType;

/**   \brief  value based type definition for BswM_OnInitOfImmediateUser */
typedef boolean BswM_OnInitOfImmediateUserType;

/**   \brief  value based type definition for BswM_RulesIndEndIdxOfImmediateUser */
typedef uint8 BswM_RulesIndEndIdxOfImmediateUserType;

/**   \brief  value based type definition for BswM_RulesIndStartIdxOfImmediateUser */
typedef uint8 BswM_RulesIndStartIdxOfImmediateUserType;

/**   \brief  value based type definition for BswM_RulesIndUsedOfImmediateUser */
typedef boolean BswM_RulesIndUsedOfImmediateUserType;

/**   \brief  value based type definition for BswM_Initialized */
typedef boolean BswM_InitializedType;

/**   \brief  value based type definition for BswM_ExternalIdOfJ1939NmMapping */
typedef uint32 BswM_ExternalIdOfJ1939NmMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserEndIdxOfJ1939NmMapping */
typedef uint8 BswM_ImmediateUserEndIdxOfJ1939NmMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserStartIdxOfJ1939NmMapping */
typedef uint8 BswM_ImmediateUserStartIdxOfJ1939NmMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserUsedOfJ1939NmMapping */
typedef boolean BswM_ImmediateUserUsedOfJ1939NmMappingType;

/**   \brief  value based type definition for BswM_ExternalIdOfLinSMMapping */
typedef uint32 BswM_ExternalIdOfLinSMMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserEndIdxOfLinSMMapping */
typedef uint8 BswM_ImmediateUserEndIdxOfLinSMMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserStartIdxOfLinSMMapping */
typedef uint8 BswM_ImmediateUserStartIdxOfLinSMMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserUsedOfLinSMMapping */
typedef boolean BswM_ImmediateUserUsedOfLinSMMappingType;

/**   \brief  value based type definition for BswM_ExternalIdOfLinScheduleEndMapping */
typedef uint32 BswM_ExternalIdOfLinScheduleEndMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserEndIdxOfLinScheduleEndMapping */
typedef uint8 BswM_ImmediateUserEndIdxOfLinScheduleEndMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserStartIdxOfLinScheduleEndMapping */
typedef uint8 BswM_ImmediateUserStartIdxOfLinScheduleEndMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserUsedOfLinScheduleEndMapping */
typedef boolean BswM_ImmediateUserUsedOfLinScheduleEndMappingType;

/**   \brief  value based type definition for BswM_ExternalIdOfLinScheduleMapping */
typedef uint32 BswM_ExternalIdOfLinScheduleMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserEndIdxOfLinScheduleMapping */
typedef uint8 BswM_ImmediateUserEndIdxOfLinScheduleMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserStartIdxOfLinScheduleMapping */
typedef uint8 BswM_ImmediateUserStartIdxOfLinScheduleMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserUsedOfLinScheduleMapping */
typedef boolean BswM_ImmediateUserUsedOfLinScheduleMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserEndIdxOfModeRequestMapping */
typedef uint8 BswM_ImmediateUserEndIdxOfModeRequestMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserStartIdxOfModeRequestMapping */
typedef uint8 BswM_ImmediateUserStartIdxOfModeRequestMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserUsedOfModeRequestMapping */
typedef boolean BswM_ImmediateUserUsedOfModeRequestMappingType;

/**   \brief  value based type definition for BswM_ModeRequestQueue */
typedef uint8 BswM_ModeRequestQueueType;

/**   \brief  value based type definition for BswM_ExternalIdOfNvMJobMapping */
typedef uint32 BswM_ExternalIdOfNvMJobMappingType;

/**   \brief  value based type definition for BswM_PCPartitionConfigIdxOfPartitionIdentifiers */
typedef uint8 BswM_PCPartitionConfigIdxOfPartitionIdentifiersType;

/**   \brief  value based type definition for BswM_PartitionSNVOfPartitionIdentifiers */
typedef uint32 BswM_PartitionSNVOfPartitionIdentifiersType;

/**   \brief  value based type definition for BswM_QueueSemaphore */
typedef uint8 BswM_QueueSemaphoreType;

/**   \brief  value based type definition for BswM_QueueWritten */
typedef boolean BswM_QueueWrittenType;

/**   \brief  value based type definition for BswM_RuleStates */
typedef uint8 BswM_RuleStatesType;

/**   \brief  value based type definition for BswM_IdOfRules */
typedef uint8 BswM_IdOfRulesType;

/**   \brief  value based type definition for BswM_InitOfRules */
typedef uint8 BswM_InitOfRulesType;

/**   \brief  value based type definition for BswM_RuleStatesIdxOfRules */
typedef uint8 BswM_RuleStatesIdxOfRulesType;

/**   \brief  value based type definition for BswM_RulesInd */
typedef uint8 BswM_RulesIndType;

/**   \brief  value based type definition for BswM_SizeOfActionListQueue */
typedef uint8 BswM_SizeOfActionListQueueType;

/**   \brief  value based type definition for BswM_SizeOfActionLists */
typedef uint8 BswM_SizeOfActionListsType;

/**   \brief  value based type definition for BswM_SizeOfCanSMChannelMapping */
typedef uint8 BswM_SizeOfCanSMChannelMappingType;

/**   \brief  value based type definition for BswM_SizeOfCanSMChannelState */
typedef uint8 BswM_SizeOfCanSMChannelStateType;

/**   \brief  value based type definition for BswM_SizeOfComMChannelMapping */
typedef uint8 BswM_SizeOfComMChannelMappingType;

/**   \brief  value based type definition for BswM_SizeOfComMChannelState */
typedef uint8 BswM_SizeOfComMChannelStateType;

/**   \brief  value based type definition for BswM_SizeOfDcmComMapping */
typedef uint8 BswM_SizeOfDcmComMappingType;

/**   \brief  value based type definition for BswM_SizeOfDcmComState */
typedef uint8 BswM_SizeOfDcmComStateType;

/**   \brief  value based type definition for BswM_SizeOfDeferredRules */
typedef uint8 BswM_SizeOfDeferredRulesType;

/**   \brief  value based type definition for BswM_SizeOfGenericMapping */
typedef uint8 BswM_SizeOfGenericMappingType;

/**   \brief  value based type definition for BswM_SizeOfGenericState */
typedef uint8 BswM_SizeOfGenericStateType;

/**   \brief  value based type definition for BswM_SizeOfImmediateUser */
typedef uint8 BswM_SizeOfImmediateUserType;

/**   \brief  value based type definition for BswM_SizeOfInitGenVarAndInitAL */
typedef uint8 BswM_SizeOfInitGenVarAndInitALType;

/**   \brief  value based type definition for BswM_SizeOfJ1939NmMapping */
typedef uint8 BswM_SizeOfJ1939NmMappingType;

/**   \brief  value based type definition for BswM_SizeOfJ1939NmState */
typedef uint8 BswM_SizeOfJ1939NmStateType;

/**   \brief  value based type definition for BswM_SizeOfLinSMMapping */
typedef uint8 BswM_SizeOfLinSMMappingType;

/**   \brief  value based type definition for BswM_SizeOfLinSMState */
typedef uint8 BswM_SizeOfLinSMStateType;

/**   \brief  value based type definition for BswM_SizeOfLinScheduleEndMapping */
typedef uint8 BswM_SizeOfLinScheduleEndMappingType;

/**   \brief  value based type definition for BswM_SizeOfLinScheduleEndState */
typedef uint8 BswM_SizeOfLinScheduleEndStateType;

/**   \brief  value based type definition for BswM_SizeOfLinScheduleMapping */
typedef uint8 BswM_SizeOfLinScheduleMappingType;

/**   \brief  value based type definition for BswM_SizeOfLinScheduleState */
typedef uint8 BswM_SizeOfLinScheduleStateType;

/**   \brief  value based type definition for BswM_SizeOfModeNotificationFct */
typedef uint8 BswM_SizeOfModeNotificationFctType;

/**   \brief  value based type definition for BswM_SizeOfModeRequestMapping */
typedef uint8 BswM_SizeOfModeRequestMappingType;

/**   \brief  value based type definition for BswM_SizeOfModeRequestQueue */
typedef uint8 BswM_SizeOfModeRequestQueueType;

/**   \brief  value based type definition for BswM_SizeOfNvMJobMapping */
typedef uint8 BswM_SizeOfNvMJobMappingType;

/**   \brief  value based type definition for BswM_SizeOfNvMJobState */
typedef uint8 BswM_SizeOfNvMJobStateType;

/**   \brief  value based type definition for BswM_SizeOfPartitionIdentifiers */
typedef uint8 BswM_SizeOfPartitionIdentifiersType;

/**   \brief  value based type definition for BswM_SizeOfRuleStates */
typedef uint8 BswM_SizeOfRuleStatesType;

/**   \brief  value based type definition for BswM_SizeOfRules */
typedef uint8 BswM_SizeOfRulesType;

/**   \brief  value based type definition for BswM_SizeOfRulesInd */
typedef uint8 BswM_SizeOfRulesIndType;

/**   \brief  value based type definition for BswM_SizeOfSwcModeRequestUpdateFct */
typedef uint8 BswM_SizeOfSwcModeRequestUpdateFctType;

/**   \brief  value based type definition for BswM_SizeOfTimerState */
typedef uint8 BswM_SizeOfTimerStateType;

/**   \brief  value based type definition for BswM_SizeOfTimerValue */
typedef uint8 BswM_SizeOfTimerValueType;

/**   \brief  value based type definition for BswM_TimerState */
typedef uint8 BswM_TimerStateType;

/**   \brief  value based type definition for BswM_TimerValue */
typedef uint32 BswM_TimerValueType;

/** 
  \}
*/ 


/* PRQA L:PRECOMPILEGLOBALDATATYPES */

typedef uint8 BswM_UserDomainType; /* user domain: CanSM, LinSM... */

/* It is possible to configure any user number from 0 to 65535 */
typedef uint16 BswM_UserType;
typedef uint16 BswM_ModeType;
typedef uint8 BswM_HandleType;
typedef P2FUNC (void, BSWM_CODE, BswM_InitGenVarAndInitALType)(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
typedef P2FUNC (BswM_HandleType, BSWM_CODE, BswM_RuleTableFctPtrType)(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
typedef P2FUNC (Std_ReturnType, BSWM_CODE, BswM_ActionListFuncType)(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);

typedef P2FUNC (void, BSWM_CODE, BswM_PartitionFunctionType)(void);

/* PRQA S 0639, 0779 PRECOMPILEGLOBALDATATYPES */ /* MD_MSR_1.1_639, MD_MSR_Rule5.2_0779 */
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  BswMPCStructTypes  BswM Struct Types (PRE_COMPILE)
  \brief  These type definitions are used for structured data representations.
  \{
*/ 
/**   \brief  type used in BswM_ActionLists */
typedef struct sBswM_ActionListsType
{
  BswM_ActionListFuncType FctPtrOfActionLists;  /**< Pointer to the array list function. */
} BswM_ActionListsType;

/**   \brief  type used in BswM_CanSMChannelMapping */
typedef struct sBswM_CanSMChannelMappingType
{
  BswM_ExternalIdOfCanSMChannelMappingType ExternalIdOfCanSMChannelMapping;  /**< External id of BswMCanSMIndication. */
  BswM_ImmediateUserEndIdxOfCanSMChannelMappingType ImmediateUserEndIdxOfCanSMChannelMapping;  /**< the end index of the 0:n relation pointing to BswM_ImmediateUser */
  BswM_ImmediateUserStartIdxOfCanSMChannelMappingType ImmediateUserStartIdxOfCanSMChannelMapping;  /**< the start index of the 0:n relation pointing to BswM_ImmediateUser */
} BswM_CanSMChannelMappingType;

/**   \brief  type used in BswM_ComMChannelMapping */
typedef struct sBswM_ComMChannelMappingType
{
  BswM_ExternalIdOfComMChannelMappingType ExternalIdOfComMChannelMapping;  /**< External id of BswMComMIndication. */
} BswM_ComMChannelMappingType;

/**   \brief  type used in BswM_DcmComMapping */
typedef struct sBswM_DcmComMappingType
{
  BswM_ExternalIdOfDcmComMappingType ExternalIdOfDcmComMapping;  /**< External id of BswMDcmComModeRequest. */
  BswM_ImmediateUserEndIdxOfDcmComMappingType ImmediateUserEndIdxOfDcmComMapping;  /**< the end index of the 0:n relation pointing to BswM_ImmediateUser */
  BswM_ImmediateUserStartIdxOfDcmComMappingType ImmediateUserStartIdxOfDcmComMapping;  /**< the start index of the 0:n relation pointing to BswM_ImmediateUser */
} BswM_DcmComMappingType;

/**   \brief  type used in BswM_DeferredRules */
typedef struct sBswM_DeferredRulesType
{
  BswM_RulesIdxOfDeferredRulesType RulesIdxOfDeferredRules;  /**< the index of the 1:1 relation pointing to BswM_Rules */
} BswM_DeferredRulesType;

/**   \brief  type used in BswM_GenericMapping */
typedef struct sBswM_GenericMappingType
{
  BswM_ExternalIdOfGenericMappingType ExternalIdOfGenericMapping;  /**< External id of BswMGenericRequest. */
  BswM_ImmediateUserEndIdxOfGenericMappingType ImmediateUserEndIdxOfGenericMapping;  /**< the end index of the 0:n relation pointing to BswM_ImmediateUser */
  BswM_ImmediateUserStartIdxOfGenericMappingType ImmediateUserStartIdxOfGenericMapping;  /**< the start index of the 0:n relation pointing to BswM_ImmediateUser */
  BswM_ModeType InitValueOfGenericMapping;  /**< Initialization value of port. */
} BswM_GenericMappingType;

/**   \brief  type used in BswM_ImmediateUser */
typedef struct sBswM_ImmediateUserType
{
  BswM_MaskedBitsOfImmediateUserType MaskedBitsOfImmediateUser;  /**< contains bitcoded the boolean data of BswM_OnInitOfImmediateUser, BswM_RulesIndUsedOfImmediateUser */
  BswM_RulesIndEndIdxOfImmediateUserType RulesIndEndIdxOfImmediateUser;  /**< the end index of the 0:n relation pointing to BswM_RulesInd */
  BswM_RulesIndStartIdxOfImmediateUserType RulesIndStartIdxOfImmediateUser;  /**< the start index of the 0:n relation pointing to BswM_RulesInd */
} BswM_ImmediateUserType;

/**   \brief  type used in BswM_J1939NmMapping */
typedef struct sBswM_J1939NmMappingType
{
  BswM_ExternalIdOfJ1939NmMappingType ExternalIdOfJ1939NmMapping;  /**< External id of BswMJ1939NmIndication. */
  BswM_ImmediateUserEndIdxOfJ1939NmMappingType ImmediateUserEndIdxOfJ1939NmMapping;  /**< the end index of the 0:n relation pointing to BswM_ImmediateUser */
  BswM_ImmediateUserStartIdxOfJ1939NmMappingType ImmediateUserStartIdxOfJ1939NmMapping;  /**< the start index of the 0:n relation pointing to BswM_ImmediateUser */
} BswM_J1939NmMappingType;

/**   \brief  type used in BswM_LinSMMapping */
typedef struct sBswM_LinSMMappingType
{
  BswM_ExternalIdOfLinSMMappingType ExternalIdOfLinSMMapping;  /**< External id of BswMLinSMIndication. */
  BswM_ImmediateUserEndIdxOfLinSMMappingType ImmediateUserEndIdxOfLinSMMapping;  /**< the end index of the 0:n relation pointing to BswM_ImmediateUser */
  BswM_ImmediateUserStartIdxOfLinSMMappingType ImmediateUserStartIdxOfLinSMMapping;  /**< the start index of the 0:n relation pointing to BswM_ImmediateUser */
} BswM_LinSMMappingType;

/**   \brief  type used in BswM_LinScheduleEndMapping */
typedef struct sBswM_LinScheduleEndMappingType
{
  BswM_ExternalIdOfLinScheduleEndMappingType ExternalIdOfLinScheduleEndMapping;  /**< External id of BswMLinScheduleEndNotification. */
  BswM_ImmediateUserEndIdxOfLinScheduleEndMappingType ImmediateUserEndIdxOfLinScheduleEndMapping;  /**< the end index of the 0:n relation pointing to BswM_ImmediateUser */
  BswM_ImmediateUserStartIdxOfLinScheduleEndMappingType ImmediateUserStartIdxOfLinScheduleEndMapping;  /**< the start index of the 0:n relation pointing to BswM_ImmediateUser */
} BswM_LinScheduleEndMappingType;

/**   \brief  type used in BswM_LinScheduleMapping */
typedef struct sBswM_LinScheduleMappingType
{
  BswM_ExternalIdOfLinScheduleMappingType ExternalIdOfLinScheduleMapping;  /**< External id of BswMLinScheduleIndication. */
  BswM_ImmediateUserEndIdxOfLinScheduleMappingType ImmediateUserEndIdxOfLinScheduleMapping;  /**< the end index of the 0:n relation pointing to BswM_ImmediateUser */
  BswM_ImmediateUserStartIdxOfLinScheduleMappingType ImmediateUserStartIdxOfLinScheduleMapping;  /**< the start index of the 0:n relation pointing to BswM_ImmediateUser */
} BswM_LinScheduleMappingType;

/**   \brief  type used in BswM_ModeRequestMapping */
typedef struct sBswM_ModeRequestMappingType
{
  BswM_ImmediateUserEndIdxOfModeRequestMappingType ImmediateUserEndIdxOfModeRequestMapping;  /**< the end index of the 0:n relation pointing to BswM_ImmediateUser */
  BswM_ImmediateUserStartIdxOfModeRequestMappingType ImmediateUserStartIdxOfModeRequestMapping;  /**< the start index of the 0:n relation pointing to BswM_ImmediateUser */
} BswM_ModeRequestMappingType;

/**   \brief  type used in BswM_NvMJobMapping */
typedef struct sBswM_NvMJobMappingType
{
  uint8 BswM_NvMJobMappingNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} BswM_NvMJobMappingType;

/**   \brief  type used in BswM_PartitionIdentifiers */
typedef struct sBswM_PartitionIdentifiersType
{
  BswM_PartitionSNVOfPartitionIdentifiersType PartitionSNVOfPartitionIdentifiers;
  BswM_PCPartitionConfigIdxOfPartitionIdentifiersType PCPartitionConfigIdxOfPartitionIdentifiers;  /**< the index of the 1:1 relation pointing to BswM_PCPartitionConfig */
} BswM_PartitionIdentifiersType;

/**   \brief  type used in BswM_Rules */
typedef struct sBswM_RulesType
{
  BswM_IdOfRulesType IdOfRules;  /**< External id of rule. */
  BswM_RuleStatesIdxOfRulesType RuleStatesIdxOfRules;  /**< the index of the 1:1 relation pointing to BswM_RuleStates */
  BswM_RuleTableFctPtrType FctPtrOfRules;  /**< Pointer to the rule function which does the arbitration. */
} BswM_RulesType;

/** 
  \}
*/ 

/** 
  \defgroup  BswMPCSymbolicStructTypes  BswM Symbolic Struct Types (PRE_COMPILE)
  \brief  These structs are used in unions to have a symbol based data representation style.
  \{
*/ 
/**   \brief  type to be used as symbolic data element access to BswM_ActionListQueue in the partition context  */
typedef struct BswM_ActionListQueueStructSTag
{
  BswM_ActionListQueueType AL_CC_AL_LIN1_SchTableStartInd_MSTable1;
  BswM_ActionListQueueType AL_CC_AL_LIN2_SchTableStartInd_MSTable0;
  BswM_ActionListQueueType AL_CC_AL_LIN3_SchTableStartInd_MSTable;
  BswM_ActionListQueueType AL_CC_AL_LIN4_SchTableStartInd_MSTable1;
  BswM_ActionListQueueType AL_CC_AL_LIN5_SchTableStartInd_MSTable2;
  BswM_ActionListQueueType AL_CC_AL_LIN1_SchTableStartInd_MSTable2;
  BswM_ActionListQueueType AL_CC_AL_LIN1_SchTableStartInd_Table1;
  BswM_ActionListQueueType AL_CC_AL_LIN1_SchTableStartInd_Table2;
  BswM_ActionListQueueType AL_CC_AL_LIN1_SchTableStartInd_TableE;
  BswM_ActionListQueueType AL_CC_AL_LIN2_SchTableStartInd_Table0;
  BswM_ActionListQueueType AL_CC_AL_LIN2_SchTableStartInd_TableE;
  BswM_ActionListQueueType AL_CC_AL_LIN1_SchTableStartInd_MSTable;
  BswM_ActionListQueueType AL_CC_AL_LIN2_SchTableStartInd_MSTable;
  BswM_ActionListQueueType AL_CC_AL_LIN3_SchTableStartInd_MSTable1;
  BswM_ActionListQueueType AL_CC_AL_LIN3_SchTableStartInd_MSTable2;
  BswM_ActionListQueueType AL_CC_AL_LIN3_SchTableStartInd_Table1;
  BswM_ActionListQueueType AL_CC_AL_LIN3_SchTableStartInd_Table2;
  BswM_ActionListQueueType AL_CC_AL_LIN3_SchTableStartInd_TableE;
  BswM_ActionListQueueType AL_CC_AL_LIN4_SchTableStartInd_MSTable2;
  BswM_ActionListQueueType AL_CC_AL_LIN4_SchTableStartInd_MSTable;
  BswM_ActionListQueueType AL_CC_AL_LIN4_SchTableStartInd_Table1;
  BswM_ActionListQueueType AL_CC_AL_LIN4_SchTableStartInd_Table2;
  BswM_ActionListQueueType AL_CC_AL_LIN4_SchTableStartInd_TableE;
  BswM_ActionListQueueType AL_CC_AL_LIN5_SchTableStartInd_MSTable1;
  BswM_ActionListQueueType AL_CC_AL_LIN5_SchTableStartInd_MSTable;
  BswM_ActionListQueueType AL_CC_AL_LIN5_SchTableStartInd_Table1;
  BswM_ActionListQueueType AL_CC_AL_LIN5_SchTableStartInd_Table2;
  BswM_ActionListQueueType AL_CC_AL_LIN5_SchTableStartInd_TableE;
  BswM_ActionListQueueType AL_CC_AL_DcmEcuReset_Trigger;
  BswM_ActionListQueueType AL_CC_AL_LIN5_ScheduleTable_to_Table2;
  BswM_ActionListQueueType AL_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp;
  BswM_ActionListQueueType AL_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp;
  BswM_ActionListQueueType AL_CC_AL_LIN2_ScheduleTable_to_Table_E;
  BswM_ActionListQueueType AL_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_1;
  BswM_ActionListQueueType AL_CC_AL_LIN3_ScheduleTable_to_Table1;
  BswM_ActionListQueueType AL_CC_AL_LIN3_ScheduleTable_to_NULL;
  BswM_ActionListQueueType AL_CC_AL_LIN3_ScheduleTable_to_Table2;
  BswM_ActionListQueueType AL_CC_AL_LIN3_ScheduleTable_to_Table_E;
  BswM_ActionListQueueType AL_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp;
  BswM_ActionListQueueType AL_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_1;
  BswM_ActionListQueueType AL_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_2;
  BswM_ActionListQueueType AL_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp;
  BswM_ActionListQueueType AL_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp;
  BswM_ActionListQueueType AL_CC_AL_LIN1_ScheduleTable_to_Table2;
  BswM_ActionListQueueType AL_CC_AL_LIN5_ScheduleTable_to_Table_E;
  BswM_ActionListQueueType AL_CC_AL_LIN4_ScheduleTable_to_Table_E;
  BswM_ActionListQueueType AL_CC_AL_LIN1_ScheduleTable_to_Table_E;
  BswM_ActionListQueueType AL_CC_AL_LIN4_ScheduleTable_to_Table2;
  BswM_ActionListQueueType AL_CC_AL_LIN5_ScheduleTable_to_Table1;
  BswM_ActionListQueueType AL_CC_AL_LIN4_ScheduleTable_to_Table1;
  BswM_ActionListQueueType AL_CC_AL_LIN2_ScheduleTable_to_Table0;
  BswM_ActionListQueueType AL_CC_AL_LIN1_ScheduleTable_to_Table1;
  BswM_ActionListQueueType AL_CC_AL_LIN5_ScheduleTable_to_NULL;
  BswM_ActionListQueueType AL_CC_AL_LIN4_ScheduleTable_to_NULL;
  BswM_ActionListQueueType AL_CC_AL_LIN2_ScheduleTable_to_NULL;
  BswM_ActionListQueueType AL_CC_AL_LIN1_ScheduleTable_to_NULL;
  BswM_ActionListQueueType AL_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_2;
  BswM_ActionListQueueType AL_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_2;
  BswM_ActionListQueueType AL_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_2;
  BswM_ActionListQueueType AL_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp_Table_0;
  BswM_ActionListQueueType AL_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_1;
  BswM_ActionListQueueType AL_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_1;
  BswM_ActionListQueueType AL_CC_AL_CN_CabSubnet_9ea693f1_Disable_DM;
  BswM_ActionListQueueType AL_CC_AL_CN_CabSubnet_9ea693f1_Enable_DM;
  BswM_ActionListQueueType AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Offline;
  BswM_ActionListQueueType AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Online;
  BswM_ActionListQueueType AL_CC_AL_CN_Backbone2_78967e2c_Disable_DM;
  BswM_ActionListQueueType AL_CC_AL_CN_Backbone2_78967e2c_Enable_DM;
  BswM_ActionListQueueType AL_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_EnableNoinit;
  BswM_ActionListQueueType AL_CC_AL_CN_LIN02_c2d7c6f3_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_LIN02_c2d7c6f3_Enable;
  BswM_ActionListQueueType AL_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_EnableNoinit;
  BswM_ActionListQueueType AL_CC_AL_CN_SecuritySubnet_e7a0ee54_Disable_DM;
  BswM_ActionListQueueType AL_CC_AL_CN_SecuritySubnet_e7a0ee54_Enable_DM;
  BswM_ActionListQueueType AL_CC_AL_CN_FMSNet_fce1aae5_TX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_FMSNet_fce1aae5_TX_EnableNoinit;
  BswM_ActionListQueueType AL_CC_AL_CN_Backbone2_78967e2c_RX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_Backbone2_78967e2c_RX_EnableNoinit;
  BswM_ActionListQueueType AL_CC_AL_CN_CabSubnet_9ea693f1_TX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_CabSubnet_9ea693f1_TX_EnableNoinit;
  BswM_ActionListQueueType AL_CC_AL_CN_Backbone2_78967e2c_TX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_Backbone2_78967e2c_TX_EnableNoinit;
  BswM_ActionListQueueType AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Offline;
  BswM_ActionListQueueType AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Online;
  BswM_ActionListQueueType AL_CC_AL_CN_CabSubnet_9ea693f1_RX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_CabSubnet_9ea693f1_RX_EnableNoinit;
  BswM_ActionListQueueType AL_CC_AL_CN_LIN03_b5d0f665_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_LIN03_b5d0f665_Enable;
  BswM_ActionListQueueType AL_CC_AL_CN_FMSNet_fce1aae5_RX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_FMSNet_fce1aae5_RX_EnableNoinit;
  BswM_ActionListQueueType AL_CC_AL_CN_LIN01_5bde9749_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_LIN01_5bde9749_Enable;
  BswM_ActionListQueueType AL_CC_AL_CN_LIN04_2bb463c6_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_LIN04_2bb463c6_Enable;
  BswM_ActionListQueueType AL_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_EnableNoinit;
  BswM_ActionListQueueType AL_CC_AL_CN_FMSNet_fce1aae5_Disable_DM;
  BswM_ActionListQueueType AL_CC_AL_CN_FMSNet_fce1aae5_Enable_DM;
  BswM_ActionListQueueType AL_CC_AL_CN_Backbone1J1939_0b1f4bae_Disable_DM;
  BswM_ActionListQueueType AL_CC_AL_CN_Backbone1J1939_0b1f4bae_Enable_DM;
  BswM_ActionListQueueType AL_CC_AL_CN_LIN00_2cd9a7df_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_LIN00_2cd9a7df_Enable;
  BswM_ActionListQueueType AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_EnableNoinit;
  BswM_ActionListQueueType AL_ESH_AL_ExitRun;
  BswM_ActionListQueueType AL_ESH_AL_RunToPostRun;
  BswM_ActionListQueueType AL_ESH_AL_WaitForNvMToShutdown;
  BswM_ActionListQueueType AL_ESH_AL_WakeupToPrep;
  BswM_ActionListQueueType AL_ESH_AL_WaitForNvMWakeup;
  BswM_ActionListQueueType AL_ESH_AL_WakeupToRun;
  BswM_ActionListQueueType AL_ESH_AL_InitToWakeup;
  BswM_ActionListQueueType AL_ESH_AL_PostRunToPrepShutdown;
  BswM_ActionListQueueType AL_ESH_AL_PostRunToRun;
  BswM_ActionListQueueType AL_ESH_AL_ExitPostRun;
  BswM_ActionListQueueType AL_ESH_AL_PrepShutdownToWaitForNvM;
  BswM_ActionListQueueType AL_INIT_AL_Initialize;
  BswM_ActionListQueueType AL_ESH_AL_DemInit;
  BswM_ActionListQueueType AL_CC_AL_LIN1_ScheduleTableEndNotification;
  BswM_ActionListQueueType AL_CC_AL_LIN2_ScheduleTableEndNotification;
  BswM_ActionListQueueType AL_CC_AL_LIN3_ScheduleTableEndNotification;
  BswM_ActionListQueueType AL_CC_AL_LIN4_ScheduleTableEndNotification;
  BswM_ActionListQueueType AL_CC_AL_LIN5_ScheduleTableEndNotification;
  BswM_ActionListQueueType AL_CC_AL_CN_LIN06_c5ba02ea_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_LIN06_c5ba02ea_Enable;
  BswM_ActionListQueueType AL_CC_AL_CN_LIN05_5cb35350_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_LIN05_5cb35350_Enable;
  BswM_ActionListQueueType AL_CC_AL_CN_LIN07_b2bd327c_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_LIN07_b2bd327c_Enable;
  BswM_ActionListQueueType AL_CC_AL_CN_CAN6_b040c073_RX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_CAN6_b040c073_RX_EnableNoinit;
  BswM_ActionListQueueType AL_CC_AL_CN_CAN6_b040c073_TX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_CAN6_b040c073_TX_EnableNoinit;
  BswM_ActionListQueueType AL_CC_AL_CN_CAN6_b040c073_Disable_DM;
  BswM_ActionListQueueType AL_CC_AL_CN_CAN6_b040c073_Enable_DM;
  BswM_ActionListQueueType AL_CC_AL_LIN6_ScheduleTableEndNotification;
  BswM_ActionListQueueType AL_CC_AL_LIN7_ScheduleTableEndNotification;
  BswM_ActionListQueueType AL_CC_AL_LIN8_ScheduleTableEndNotification;
  BswM_ActionListQueueType AL_CC_AL_LIN6_SchTableStartInd_Table0;
  BswM_ActionListQueueType AL_CC_AL_LIN7_SchTableStartInd_Table0;
  BswM_ActionListQueueType AL_CC_AL_LIN8_SchTableStartInd_Table0;
  BswM_ActionListQueueType AL_CC_AL_LIN6_SchTableStartInd_MSTable;
  BswM_ActionListQueueType AL_CC_AL_LIN7_SchTableStartInd_MSTable;
  BswM_ActionListQueueType AL_CC_AL_LIN8_SchTableStartInd_MSTable;
  BswM_ActionListQueueType AL_CC_AL_LIN6_SchTableStartInd_MSTable0;
  BswM_ActionListQueueType AL_CC_AL_LIN7_SchTableStartInd_MSTable0;
  BswM_ActionListQueueType AL_CC_AL_LIN8_SchTableStartInd_MSTable0;
  BswM_ActionListQueueType AL_CC_AL_LIN6_ScheduleTable_to_Table0;
  BswM_ActionListQueueType AL_CC_AL_LIN7_ScheduleTable_to_Table0;
  BswM_ActionListQueueType AL_CC_AL_LIN8_ScheduleTable_to_Table0;
  BswM_ActionListQueueType AL_CC_AL_LIN6_ScheduleTable_to_MSTable0;
  BswM_ActionListQueueType AL_CC_AL_LIN6_ScheduleTable_to_MSTable;
  BswM_ActionListQueueType AL_CC_AL_LIN7_ScheduleTable_to_MSTable0;
  BswM_ActionListQueueType AL_CC_AL_LIN7_ScheduleTable_to_MSTable;
  BswM_ActionListQueueType AL_CC_AL_LIN8_ScheduleTable_to_MSTable0;
  BswM_ActionListQueueType AL_CC_AL_LIN8_ScheduleTable_to_MSTable;
  BswM_ActionListQueueType AL_CC_AL_DCMResetProcess_Started;
  BswM_ActionListQueueType AL_CC_AL_DCMResetProcess_InProgress;
  BswM_ActionListQueueType AL_CC_AL_RunToECUReset;
  BswM_ActionListQueueType AL_CC_AL_DcmEcuReset_Execute;
  BswM_ActionListQueueType AL_CC_AL_DcmEcuReset_JumpToBTL;
  BswM_ActionListQueueType AL_CC_AL_DCMResetPostRun;
  BswM_ActionListQueueType AL_CC_AL_PrepShutdown_NvmWriteAll;
  BswM_ActionListQueueType AL_CC_True_AL_BB1_BusOff_Indication;
  BswM_ActionListQueueType AL_CC_False_AL_BB1_BusOff_Indication;
  BswM_ActionListQueueType AL_CC_True_AL_BB2_BusOff_Indication;
  BswM_ActionListQueueType AL_CC_False_AL_CAN6_BusOff_Indication;
  BswM_ActionListQueueType AL_CC_True_AL_CAN6_BusOff_Indication;
  BswM_ActionListQueueType AL_CC_False_AL_BB2_BusOff_Indication;
  BswM_ActionListQueueType AL_CC_False_AL_CabSubnet_BusOff_Indication;
  BswM_ActionListQueueType AL_CC_False_AL_FMSNet_BusOff_Indication;
  BswM_ActionListQueueType AL_CC_True_AL_CabSubnet_BusOff_Indication;
  BswM_ActionListQueueType AL_CC_True_AL_FMSNet_BusOff_Indication;
  BswM_ActionListQueueType AL_CC_False_AL_SecuritySubnet_BusOff_Indication;
  BswM_ActionListQueueType AL_CC_True_AL_SecuritySubnet_BusOff_Indication;
  BswM_ActionListQueueType AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_EnableNoinit;
  BswM_ActionListQueueType AL_AL_PvtReport_Enabled;
  BswM_ActionListQueueType AL_AL_PvtReport_Disabled;
} BswM_ActionListQueueStructSType;  /* PRQA S 0639 */  /* MD_MSR_Dir1.1 */

/**   \brief  type to be used as symbolic data element access to BswM_RuleStates in the partition context  */
typedef struct BswM_RuleStatesStructSTag
{
  BswM_RuleStatesType R_CC_Rule_LIN1_SchTableStartInd_Table1;
  BswM_RuleStatesType R_CC_Rule_LIN2_SchTableStartInd_MSTable0;
  BswM_RuleStatesType R_CC_Rule_LIN3_SchTableStartInd_MSTable;
  BswM_RuleStatesType R_CC_Rule_LIN4_SchTableStartInd_MSTable;
  BswM_RuleStatesType R_CC_Rule_LIN5_SchTableStartInd_MSTable;
  BswM_RuleStatesType R_CC_Rule_LIN1_SchTableStartInd_Table2;
  BswM_RuleStatesType R_CC_Rule_LIN1_SchTableStartInd_TableE;
  BswM_RuleStatesType R_CC_Rule_LIN1_SchTableStartInd_MSTable1;
  BswM_RuleStatesType R_CC_Rule_LIN1_SchTableStartInd_MSTable2;
  BswM_RuleStatesType R_CC_Rule_LIN2_SchTableStartInd_Table0;
  BswM_RuleStatesType R_CC_Rule_LIN2_SchTableStartInd_TableE;
  BswM_RuleStatesType R_CC_Rule_LIN1_SchTableStartInd_MSTable;
  BswM_RuleStatesType R_CC_Rule_LIN2_SchTableStartInd_MSTable;
  BswM_RuleStatesType R_CC_Rule_LIN3_SchTableStartInd_MSTable1;
  BswM_RuleStatesType R_CC_Rule_LIN3_SchTableStartInd_MSTable2;
  BswM_RuleStatesType R_CC_Rule_LIN3_SchTableStartInd_Table1;
  BswM_RuleStatesType R_CC_Rule_LIN3_SchTableStartInd_Table2;
  BswM_RuleStatesType R_CC_Rule_LIN3_SchTableStartInd_TableE;
  BswM_RuleStatesType R_CC_Rule_LIN4_SchTableStartInd_MSTable1;
  BswM_RuleStatesType R_CC_Rule_LIN4_SchTableStartInd_MSTable2;
  BswM_RuleStatesType R_CC_Rule_LIN4_SchTableStartInd_Table1;
  BswM_RuleStatesType R_CC_Rule_LIN4_SchTableStartInd_Table2;
  BswM_RuleStatesType R_CC_Rule_LIN4_SchTableStartInd_TableE;
  BswM_RuleStatesType R_CC_Rule_LIN5_SchTableStartInd_MSTable2;
  BswM_RuleStatesType R_CC_Rule_LIN5_SchTableStartInd_MSTable1;
  BswM_RuleStatesType R_CC_Rule_LIN5_SchTableStartInd_Table1;
  BswM_RuleStatesType R_CC_Rule_LIN5_SchTableStartInd_Table2;
  BswM_RuleStatesType R_CC_Rule_LIN5_SchTableStartInd_TableE;
  BswM_RuleStatesType R_CC_Rule_DcmEcuReset_Execute;
  BswM_RuleStatesType R_CC_Rule_DcmEcuReset_Trigger;
  BswM_RuleStatesType R_CC_Rule_LIN3_Schedule_To_Table1;
  BswM_RuleStatesType R_CC_Rule_LIN3_Schedule_To_NULL;
  BswM_RuleStatesType R_CC_Rule_LIN3_Schedule_To_Table2;
  BswM_RuleStatesType R_CC_Rule_LIN3_Schedule_To_Table_E;
  BswM_RuleStatesType R_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp;
  BswM_RuleStatesType R_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table1;
  BswM_RuleStatesType R_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table2;
  BswM_RuleStatesType R_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp;
  BswM_RuleStatesType R_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table1;
  BswM_RuleStatesType R_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table2;
  BswM_RuleStatesType R_CC_Rule_LIN1_Schedule_To_NULL;
  BswM_RuleStatesType R_CC_Rule_LIN1_Schedule_To_Table1;
  BswM_RuleStatesType R_CC_Rule_LIN1_Schedule_To_Table2;
  BswM_RuleStatesType R_CC_Rule_LIN1_Schedule_To_Table_E;
  BswM_RuleStatesType R_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp;
  BswM_RuleStatesType R_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp_Table0;
  BswM_RuleStatesType R_CC_Rule_LIN2_Schedule_To_NULL;
  BswM_RuleStatesType R_CC_Rule_LIN2_Schedule_To_Table0;
  BswM_RuleStatesType R_CC_Rule_LIN2_Schedule_To_Table_E;
  BswM_RuleStatesType R_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp;
  BswM_RuleStatesType R_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table1;
  BswM_RuleStatesType R_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table2;
  BswM_RuleStatesType R_CC_Rule_LIN4_Schedule_To_NULL;
  BswM_RuleStatesType R_CC_Rule_LIN4_Schedule_To_Table1;
  BswM_RuleStatesType R_CC_Rule_LIN4_Schedule_To_Table2;
  BswM_RuleStatesType R_CC_Rule_LIN4_Schedule_To_Table_E;
  BswM_RuleStatesType R_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp;
  BswM_RuleStatesType R_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table1;
  BswM_RuleStatesType R_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table2;
  BswM_RuleStatesType R_CC_Rule_LIN5_Schedule_To_NULL;
  BswM_RuleStatesType R_CC_Rule_LIN5_Schedule_To_Table1;
  BswM_RuleStatesType R_CC_Rule_LIN5_Schedule_To_Table2;
  BswM_RuleStatesType R_CC_Rule_LIN5_Schedule_To_Table_E;
  BswM_RuleStatesType R_CC_CN_CabSubnet_9ea693f1_RX_DM;
  BswM_RuleStatesType R_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289;
  BswM_RuleStatesType R_CC_CN_Backbone2_78967e2c_RX_DM;
  BswM_RuleStatesType R_CC_CN_LIN02_c2d7c6f3;
  BswM_RuleStatesType R_CC_CN_SecuritySubnet_e7a0ee54_RX;
  BswM_RuleStatesType R_CC_CN_SecuritySubnet_e7a0ee54_RX_DM;
  BswM_RuleStatesType R_CC_CN_FMSNet_fce1aae5_TX;
  BswM_RuleStatesType R_CC_CN_Backbone2_78967e2c_RX;
  BswM_RuleStatesType R_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289;
  BswM_RuleStatesType R_CC_CN_CabSubnet_9ea693f1_RX;
  BswM_RuleStatesType R_CC_CN_LIN03_b5d0f665;
  BswM_RuleStatesType R_CC_CN_FMSNet_fce1aae5_RX;
  BswM_RuleStatesType R_CC_CN_LIN01_5bde9749;
  BswM_RuleStatesType R_CC_CN_LIN04_2bb463c6;
  BswM_RuleStatesType R_CC_CN_Backbone1J1939_0b1f4bae_RX;
  BswM_RuleStatesType R_CC_CN_FMSNet_fce1aae5_RX_DM;
  BswM_RuleStatesType R_CC_CN_Backbone1J1939_0b1f4bae_RX_DM;
  BswM_RuleStatesType R_CC_CN_LIN00_2cd9a7df;
  BswM_RuleStatesType R_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX;
  BswM_RuleStatesType R_ESH_RunToPostRun;
  BswM_RuleStatesType R_ESH_RunToPostRunNested;
  BswM_RuleStatesType R_ESH_WaitToShutdown;
  BswM_RuleStatesType R_ESH_WakeupToPrep;
  BswM_RuleStatesType R_ESH_WaitToWakeup;
  BswM_RuleStatesType R_ESH_WakeupToRun;
  BswM_RuleStatesType R_ESH_InitToWakeup;
  BswM_RuleStatesType R_ESH_PostRunNested;
  BswM_RuleStatesType R_ESH_PostRun;
  BswM_RuleStatesType R_ESH_PrepToWait;
  BswM_RuleStatesType R_ESH_DemInit;
  BswM_RuleStatesType R_CC_CN_SecuritySubnet_e7a0ee54_TX;
  BswM_RuleStatesType R_CC_CN_CabSubnet_9ea693f1_TX;
  BswM_RuleStatesType R_CC_CN_Backbone2_78967e2c_TX;
  BswM_RuleStatesType R_CC_Rule_LIN1_ScheduleTableEndNotification;
  BswM_RuleStatesType R_CC_Rule_LIN2_ScheduleTableEndNotification;
  BswM_RuleStatesType R_CC_Rule_LIN3_ScheduleTableEndNotification;
  BswM_RuleStatesType R_CC_Rule_LIN4_ScheduleTableEndNotification;
  BswM_RuleStatesType R_CC_Rule_LIN5_ScheduleTableEndNotification;
  BswM_RuleStatesType R_CC_CN_LIN06_c5ba02ea;
  BswM_RuleStatesType R_CC_CN_LIN05_5cb35350;
  BswM_RuleStatesType R_CC_CN_LIN07_b2bd327c;
  BswM_RuleStatesType R_CC_CN_CAN6_b040c073_RX;
  BswM_RuleStatesType R_CC_CN_CAN6_b040c073_TX;
  BswM_RuleStatesType R_CC_CN_CAN6_b040c073_RX_DM;
  BswM_RuleStatesType R_CC_Rule_LIN6_ScheduleTableEndNotification;
  BswM_RuleStatesType R_CC_Rule_LIN7_ScheduleTableEndNotification;
  BswM_RuleStatesType R_CC_Rule_LIN8_ScheduleTableEndNotification;
  BswM_RuleStatesType R_CC_Rule_LIN6_SchTableStartInd_MSTable0;
  BswM_RuleStatesType R_CC_Rule_LIN6_SchTableStartInd_MSTable;
  BswM_RuleStatesType R_CC_Rule_LIN6_SchTableStartInd_Table0;
  BswM_RuleStatesType R_CC_Rule_LIN7_SchTableStartInd_MSTable;
  BswM_RuleStatesType R_CC_Rule_LIN8_SchTableStartInd_MSTable;
  BswM_RuleStatesType R_CC_Rule_LIN7_SchTableStartInd_MSTable0;
  BswM_RuleStatesType R_CC_Rule_LIN8_SchTableStartInd_MSTable0;
  BswM_RuleStatesType R_CC_Rule_LIN7_SchTableStartInd_Table0;
  BswM_RuleStatesType R_CC_Rule_LIN8_SchTableStartInd_Table0;
  BswM_RuleStatesType R_CC_Rule_LIN6_Schedule_To_Table0;
  BswM_RuleStatesType R_CC_Rule_LIN7_Schedule_To_Table0;
  BswM_RuleStatesType R_CC_Rule_LIN8_Schedule_To_Table0;
  BswM_RuleStatesType R_CC_Rule_LIN6_Schedule_To_MSTable;
  BswM_RuleStatesType R_CC_Rule_LIN7_Schedule_To_MSTable;
  BswM_RuleStatesType R_CC_Rule_LIN8_Schedule_To_MSTable;
  BswM_RuleStatesType R_CC_Rule_LIN6_Schedule_To_MSTable0;
  BswM_RuleStatesType R_CC_Rule_LIN7_Schedule_To_MSTable0;
  BswM_RuleStatesType R_CC_Rule_LIN8_Schedule_To_MSTable0;
  BswM_RuleStatesType R_CC_DCMResetProcess_Started;
  BswM_RuleStatesType R_CC_DCMResetProcess_InProgress;
  BswM_RuleStatesType R_CC_Rule_DcmEcuReset_JumpToBTL;
  BswM_RuleStatesType R_CC_Rule_NvmWriteAll_Request;
  BswM_RuleStatesType R_CC_Rule_BB1_BusOff_Indication;
  BswM_RuleStatesType R_CC_Rule_BB2_BusOff_Indication;
  BswM_RuleStatesType R_CC_Rule_CAN6_BusOff_Indication;
  BswM_RuleStatesType R_CC_Rule_CabSubnet_BusOff_Indication;
  BswM_RuleStatesType R_CC_Rule_FMSNet_BusOff_Indication;
  BswM_RuleStatesType R_CC_Rule_SecuritySubnet_BusOff_Indication;
  BswM_RuleStatesType R_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX;
  BswM_RuleStatesType R_CC_Rule_PvtReportCtrl;
} BswM_RuleStatesStructSType;  /* PRQA S 0639 */  /* MD_MSR_Dir1.1 */

/**   \brief  type to be used as symbolic data element access to BswM_TimerState in the partition context  */
typedef struct BswM_TimerStateStructSTag
{
  BswM_TimerStateType MRP_ESH_NvM_CancelWriteAllTimer;
  BswM_TimerStateType MRP_ESH_NvM_WriteAllTimer;
  BswM_TimerStateType MRP_ESH_SelfRunRequestTimer;
} BswM_TimerStateStructSType;

/**   \brief  type to be used as symbolic data element access to BswM_TimerValue in the partition context  */
typedef struct BswM_TimerValueStructSTag
{
  BswM_TimerValueType MRP_ESH_NvM_CancelWriteAllTimer;
  BswM_TimerValueType MRP_ESH_NvM_WriteAllTimer;
  BswM_TimerValueType MRP_ESH_SelfRunRequestTimer;
} BswM_TimerValueStructSType;

/** 
  \}
*/ 

/** 
  \defgroup  BswMPCUnionIndexAndSymbolTypes  BswM Union Index And Symbol Types (PRE_COMPILE)
  \brief  These unions are used to access arrays in an index and symbol based style.
  \{
*/ 
/**   \brief  type to access BswM_ActionListQueue in an index and symbol based style. */
typedef union BswM_ActionListQueueUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  BswM_ActionListQueueType raw[180];
  BswM_ActionListQueueStructSType str;
} BswM_ActionListQueueUType;

/**   \brief  type to access BswM_RuleStates in an index and symbol based style. */
typedef union BswM_RuleStatesUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  BswM_RuleStatesType raw[140];
  BswM_RuleStatesStructSType str;
} BswM_RuleStatesUType;

/**   \brief  type to access BswM_TimerState in an index and symbol based style. */
typedef union BswM_TimerStateUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  BswM_TimerStateType raw[3];
  BswM_TimerStateStructSType str;
} BswM_TimerStateUType;

/**   \brief  type to access BswM_TimerValue in an index and symbol based style. */
typedef union BswM_TimerValueUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  BswM_TimerValueType raw[3];
  BswM_TimerValueStructSType str;
} BswM_TimerValueUType;

/** 
  \}
*/ 

/** 
  \defgroup  BswMPCRootPointerTypes  BswM Root Pointer Types (PRE_COMPILE)
  \brief  These type definitions are used to point from the config root to symbol instances.
  \{
*/ 
/**   \brief  type used to point to BswM_ActionListQueue */
typedef P2VAR(BswM_ActionListQueueType, TYPEDEF, BSWM_VAR_NOINIT) BswM_ActionListQueuePtrType;

/**   \brief  type used to point to BswM_ActionLists */
typedef P2CONST(BswM_ActionListsType, TYPEDEF, BSWM_CONST) BswM_ActionListsPtrType;

/**   \brief  type used to point to BswM_CanSMChannelMapping */
typedef P2CONST(BswM_CanSMChannelMappingType, TYPEDEF, BSWM_CONST) BswM_CanSMChannelMappingPtrType;

/**   \brief  type used to point to BswM_CanSMChannelState */
typedef P2VAR(CanSM_BswMCurrentStateType, TYPEDEF, BSWM_VAR_NOINIT) BswM_CanSMChannelStatePtrType;

/**   \brief  type used to point to BswM_ComMChannelMapping */
typedef P2CONST(BswM_ComMChannelMappingType, TYPEDEF, BSWM_CONST) BswM_ComMChannelMappingPtrType;

/**   \brief  type used to point to BswM_ComMChannelState */
typedef P2VAR(ComM_ModeType, TYPEDEF, BSWM_VAR_NOINIT) BswM_ComMChannelStatePtrType;

/**   \brief  type used to point to BswM_DcmComMapping */
typedef P2CONST(BswM_DcmComMappingType, TYPEDEF, BSWM_CONST) BswM_DcmComMappingPtrType;

/**   \brief  type used to point to BswM_DcmComState */
typedef P2VAR(Dcm_CommunicationModeType, TYPEDEF, BSWM_VAR_NOINIT) BswM_DcmComStatePtrType;

/**   \brief  type used to point to BswM_DeferredRules */
typedef P2CONST(BswM_DeferredRulesType, TYPEDEF, BSWM_CONST) BswM_DeferredRulesPtrType;

/**   \brief  type used to point to BswM_ForcedActionListPriority */
typedef P2VAR(BswM_ForcedActionListPriorityType, TYPEDEF, BSWM_VAR_NOINIT) BswM_ForcedActionListPriorityPtrType;

/**   \brief  type used to point to BswM_GenericMapping */
typedef P2CONST(BswM_GenericMappingType, TYPEDEF, BSWM_CONST) BswM_GenericMappingPtrType;

/**   \brief  type used to point to BswM_GenericState */
typedef P2VAR(BswM_ModeType, TYPEDEF, BSWM_VAR_NOINIT) BswM_GenericStatePtrType;

/**   \brief  type used to point to BswM_ImmediateUser */
typedef P2CONST(BswM_ImmediateUserType, TYPEDEF, BSWM_CONST) BswM_ImmediateUserPtrType;

/**   \brief  type used to point to BswM_InitGenVarAndInitAL */
typedef P2CONST(BswM_InitGenVarAndInitALType, TYPEDEF, BSWM_CONST) BswM_InitGenVarAndInitALPtrType;

/**   \brief  type used to point to BswM_Initialized */
typedef P2VAR(BswM_InitializedType, TYPEDEF, BSWM_VAR_NOINIT) BswM_InitializedPtrType;

/**   \brief  type used to point to BswM_J1939NmMapping */
typedef P2CONST(BswM_J1939NmMappingType, TYPEDEF, BSWM_CONST) BswM_J1939NmMappingPtrType;

/**   \brief  type used to point to BswM_J1939NmState */
typedef P2VAR(Nm_StateType, TYPEDEF, BSWM_VAR_NOINIT) BswM_J1939NmStatePtrType;

/**   \brief  type used to point to BswM_LinSMMapping */
typedef P2CONST(BswM_LinSMMappingType, TYPEDEF, BSWM_CONST) BswM_LinSMMappingPtrType;

/**   \brief  type used to point to BswM_LinSMState */
typedef P2VAR(LinSM_ModeType, TYPEDEF, BSWM_VAR_NOINIT) BswM_LinSMStatePtrType;

/**   \brief  type used to point to BswM_LinScheduleEndMapping */
typedef P2CONST(BswM_LinScheduleEndMappingType, TYPEDEF, BSWM_CONST) BswM_LinScheduleEndMappingPtrType;

/**   \brief  type used to point to BswM_LinScheduleEndState */
typedef P2VAR(LinIf_SchHandleType, TYPEDEF, BSWM_VAR_NOINIT) BswM_LinScheduleEndStatePtrType;

/**   \brief  type used to point to BswM_LinScheduleMapping */
typedef P2CONST(BswM_LinScheduleMappingType, TYPEDEF, BSWM_CONST) BswM_LinScheduleMappingPtrType;

/**   \brief  type used to point to BswM_LinScheduleState */
typedef P2VAR(LinIf_SchHandleType, TYPEDEF, BSWM_VAR_NOINIT) BswM_LinScheduleStatePtrType;

/**   \brief  type used to point to BswM_ModeNotificationFct */
typedef P2CONST(BswM_PartitionFunctionType, TYPEDEF, BSWM_CONST) BswM_ModeNotificationFctPtrType;

/**   \brief  type used to point to BswM_ModeRequestMapping */
typedef P2CONST(BswM_ModeRequestMappingType, TYPEDEF, BSWM_CONST) BswM_ModeRequestMappingPtrType;

/**   \brief  type used to point to BswM_ModeRequestQueue */
typedef P2VAR(BswM_ModeRequestQueueType, TYPEDEF, BSWM_VAR_NOINIT) BswM_ModeRequestQueuePtrType;

/**   \brief  type used to point to BswM_NvMJobMapping */
typedef P2CONST(BswM_NvMJobMappingType, TYPEDEF, BSWM_CONST) BswM_NvMJobMappingPtrType;

/**   \brief  type used to point to BswM_NvMJobState */
typedef P2VAR(NvM_RequestResultType, TYPEDEF, BSWM_VAR_NOINIT) BswM_NvMJobStatePtrType;

/**   \brief  type used to point to BswM_PartitionIdentifiers */
typedef P2CONST(BswM_PartitionIdentifiersType, TYPEDEF, BSWM_CONST) BswM_PartitionIdentifiersPtrType;

/**   \brief  type used to point to BswM_QueueSemaphore */
typedef P2VAR(BswM_QueueSemaphoreType, TYPEDEF, BSWM_VAR_NOINIT) BswM_QueueSemaphorePtrType;

/**   \brief  type used to point to BswM_QueueWritten */
typedef P2VAR(BswM_QueueWrittenType, TYPEDEF, BSWM_VAR_NOINIT) BswM_QueueWrittenPtrType;

/**   \brief  type used to point to BswM_RuleStates */
typedef P2VAR(BswM_RuleStatesType, TYPEDEF, BSWM_VAR_NOINIT) BswM_RuleStatesPtrType;

/**   \brief  type used to point to BswM_Rules */
typedef P2CONST(BswM_RulesType, TYPEDEF, BSWM_CONST) BswM_RulesPtrType;

/**   \brief  type used to point to BswM_RulesInd */
typedef P2CONST(BswM_RulesIndType, TYPEDEF, BSWM_CONST) BswM_RulesIndPtrType;

/**   \brief  type used to point to BswM_SwcModeRequestUpdateFct */
typedef P2CONST(BswM_PartitionFunctionType, TYPEDEF, BSWM_CONST) BswM_SwcModeRequestUpdateFctPtrType;

/**   \brief  type used to point to BswM_TimerState */
typedef P2VAR(BswM_TimerStateType, TYPEDEF, BSWM_VAR_NOINIT) BswM_TimerStatePtrType;

/**   \brief  type used to point to BswM_TimerValue */
typedef P2VAR(BswM_TimerValueType, TYPEDEF, BSWM_VAR_NOINIT) BswM_TimerValuePtrType;

/** 
  \}
*/ 

/** 
  \defgroup  BswMPCPartitionRootPointer  BswM Partition Root Pointer (PRE_COMPILE)
  \brief  This type definitions are used for partition specific instance.
  \{
*/ 
/**   \brief  type used in BswM_PCPartitionConfig */
typedef struct sBswM_PCPartitionConfigType
{
  uint8 BswM_PCPartitionConfigNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} BswM_PCPartitionConfigType;

/**   \brief  type used to point to BswM_PCPartitionConfig */
typedef P2CONST(BswM_PCPartitionConfigType, TYPEDEF, BSWM_CONST) BswM_PCPartitionConfigPtrType;

/** 
  \}
*/ 

/** 
  \defgroup  BswMPCRootValueTypes  BswM Root Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value representations in root arrays.
  \{
*/ 
/**   \brief  type used in BswM_PCConfig */
typedef struct sBswM_PCConfigType
{
  uint8 BswM_PCConfigNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} BswM_PCConfigType;

typedef BswM_PCConfigType BswM_ConfigType;  /**< A structure type is present for data in each configuration class. This typedef redefines the probably different name to the specified one. */

/** 
  \}
*/ 


/* PRQA L:PRECOMPILEGLOBALDATATYPES */





/* PRQA S 0639, 0779 POSTBUILDGLOBALDATATYPES */ /* MD_MSR_1.1_639, MD_MSR_Rule5.2_0779 */ 
/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/* PRQA L:POSTBUILDGLOBALDATATYPES */

/**********************************************************************************************************************
  GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/* PRQA S 3449, 3451 EXTERNDECLARATIONS */ /* MD_BSWM_3449, MD_BSWM_3451 */ 

/* PRQA L:EXTERNDECLARATIONS */



/**********************************************************************************************************************
 *  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  BswM_OsApplicationToBswM
**********************************************************************************************************************/


#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

extern VAR(Rte_ModeType_BswMRteMDG_LIN2Schedule, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule;
extern VAR(Rte_ModeType_BswMRteMDG_LIN3Schedule, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule;
extern VAR(Rte_ModeType_BswMRteMDG_LIN4Schedule, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule;
extern VAR(Rte_ModeType_BswMRteMDG_LIN5Schedule, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule;
extern VAR(Rte_ModeType_BswMRteMDG_LIN1Schedule, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule;
extern VAR(Rte_ModeType_ESH_Mode, BSWM_VAR_NOINIT) BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode;
extern VAR(Rte_ModeType_BswMRteMDG_LINSchTableState, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState;
extern VAR(Rte_ModeType_BswMRteMDG_LINSchTableState, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState;
extern VAR(Rte_ModeType_BswMRteMDG_LINSchTableState, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState;
extern VAR(Rte_ModeType_BswMRteMDG_LINSchTableState, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState;
extern VAR(Rte_ModeType_BswMRteMDG_LINSchTableState, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState;
extern VAR(Rte_ModeType_BswMRteMDG_LINSchTableState, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState;
extern VAR(Rte_ModeType_BswMRteMDG_LINSchTableState, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState;
extern VAR(Rte_ModeType_BswMRteMDG_LINSchTableState, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState;
extern VAR(Rte_ModeType_BswMRteMDG_LIN6Schedule, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule;
extern VAR(Rte_ModeType_BswMRteMDG_LIN8Schedule, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule;
extern VAR(Rte_ModeType_BswMRteMDG_LIN7Schedule, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule;
extern VAR(Rte_ModeType_BswMRteMDG_CanBusOff, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff;
extern VAR(Rte_ModeType_BswMRteMDG_CanBusOff, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff;
extern VAR(Rte_ModeType_BswMRteMDG_CanBusOff, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff;
extern VAR(Rte_ModeType_BswMRteMDG_CanBusOff, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff;
extern VAR(Rte_ModeType_BswMRteMDG_CanBusOff, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff;
extern VAR(Rte_ModeType_BswMRteMDG_CanBusOff, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff;
extern VAR(BswM_BswMRteMDG_LIN3Schedule, BSWM_VAR_NOINIT) Request_LIN3_ScheduleTableRequestMode_requestedMode;
extern VAR(BswM_BswMRteMDG_LIN1Schedule, BSWM_VAR_NOINIT) Request_LIN1_ScheduleTableRequestMode_requestedMode;
extern VAR(BswM_BswMRteMDG_LIN2Schedule, BSWM_VAR_NOINIT) Request_LIN2_ScheduleTableRequestMode_requestedMode;
extern VAR(BswM_BswMRteMDG_LIN4Schedule, BSWM_VAR_NOINIT) Request_LIN4_ScheduleTableRequestMode_requestedMode;
extern VAR(BswM_BswMRteMDG_LIN5Schedule, BSWM_VAR_NOINIT) Request_LIN5_ScheduleTableRequestMode_requestedMode;
extern VAR(BswM_BswMRteMDG_LIN6Schedule, BSWM_VAR_NOINIT) Request_LIN6_ScheduleTableRequestMode_requestedMode;
extern VAR(BswM_BswMRteMDG_LIN7Schedule, BSWM_VAR_NOINIT) Request_LIN7_ScheduleTableRequestMode_requestedMode;
extern VAR(BswM_BswMRteMDG_LIN8Schedule, BSWM_VAR_NOINIT) Request_LIN8_ScheduleTableRequestMode_requestedMode;
extern VAR(BswM_BswMRteMDG_NvmWriteAllRequest, BSWM_VAR_NOINIT) Request_SwcModeRequest_NvmWriteAllRequest_requestedMode;
extern VAR(BswM_BswMRteMDG_PvtReport_X1C14, BSWM_VAR_NOINIT) Request_SwcModeRequest_PvtReportCtrl_requestedMode;
extern VAR(Rte_ModeType_ESH_Mode, BSWM_VAR_NOINIT) BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode;
extern VAR(Rte_ModeType_DcmEcuReset, BSWM_VAR_NOINIT) BswM_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset;


extern VAR(boolean, BSWM_VAR_NOINIT) BswM_PreInitialized;
#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"


/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/
#define BSWM_START_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

/* -----------------------------------------------------------------------------
&&&~ USER CALLOUT DECLARATIONS
----------------------------------------------------------------------------- */
/* PRQA S 0777 CALLOUTDECLARATIONS */ /* MD_MSR_Rule5.2_0779 */
extern FUNC(void, BSWM_CODE) BswM_AL_SetProgrammableInterrupts(void);
extern FUNC(void, BSWM_CODE) BswM_ESH_OnEnterPostRun(void);
extern FUNC(void, BSWM_CODE) BswM_ESH_OnEnterPrepShutdown(void);
extern FUNC(void, BSWM_CODE) BswM_ESH_OnEnterRun(void);
extern FUNC(void, BSWM_CODE) BswM_ESH_OnEnterShutdown(void);
extern FUNC(void, BSWM_CODE) BswM_ESH_OnEnterWaitForNvm(void);
extern FUNC(void, BSWM_CODE) BswM_ESH_OnEnterWakeup(void);
extern FUNC(void, BSWM_CODE) BswM_INIT_NvMReadAll(void);
extern FUNC(void, BSWM_CODE) ESH_ComM_CheckPendingRequests(void);
extern FUNC(void, BSWM_CODE) Wakeup_BSWModule_Init(void);
/* PRQA L:CALLOUTDECLARATIONS */

/* -----------------------------------------------------------------------------
&&&~ USER CALLOUT DECLARATIONS
----------------------------------------------------------------------------- */
#define BSWM_STOP_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

#endif /* BSWM_CFG_H */

